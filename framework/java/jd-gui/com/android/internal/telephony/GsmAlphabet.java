package com.android.internal.telephony;

import android.content.res.Resources;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseIntArray;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class GsmAlphabet
{
    public static final byte GSM_EXTENDED_ESCAPE = 27;
    private static final String TAG = "GSM";
    public static final int UDH_SEPTET_COST_CONCATENATED_MESSAGE = 6;
    public static final int UDH_SEPTET_COST_LENGTH = 1;
    public static final int UDH_SEPTET_COST_ONE_SHIFT_TABLE = 4;
    public static final int UDH_SEPTET_COST_TWO_SHIFT_TABLES = 7;
    private static final SparseIntArray[] sCharsToGsmTables;
    private static final SparseIntArray[] sCharsToShiftTables;
    private static int[] sEnabledLockingShiftTables;
    private static int[] sEnabledSingleShiftTables;
    private static int sHighestEnabledSingleShiftCode;
    private static final String[] sLanguageShiftTables;
    private static final String[] sLanguageTables;

    static
    {
        String[] arrayOfString1 = new String[14];
        arrayOfString1[0] = "@£$¥èéùìòÇ\nØø\rÅåΔ_ΦΓΛΩΠΨΣΘΞ￿ÆæßÉ !\"#¤%&'()*+,-./0123456789:;<=>?¡ABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÑÜ§¿abcdefghijklmnopqrstuvwxyzäöñüà";
        arrayOfString1[1] = "@£$¥€éùıòÇ\nĞğ\rÅåΔ_ΦΓΛΩΠΨΣΘΞ￿ŞşßÉ !\"#¤%&'()*+,-./0123456789:;<=>?İABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÑÜ§çabcdefghijklmnopqrstuvwxyzäöñüà";
        arrayOfString1[2] = "";
        arrayOfString1[3] = "@£$¥êéúíóç\nÔô\rÁáΔ_ªÇÀ∞^\\€Ó|￿ÂâÊÉ !\"#º%&'()*+,-./0123456789:;<=>?ÍABCDEFGHIJKLMNOPQRSTUVWXYZÃÕÚÜ§~abcdefghijklmnopqrstuvwxyzãõ`üà";
        arrayOfString1[4] = "ঁংঃঅআইঈউঊঋ\nঌ \r এঐ    ওঔকখগঘঙচ￿ছজঝঞ !টঠডঢণত)(থদ,ধ.ন0123456789:; পফ?বভমযর ল     শষসহ়ঽািীুূৃৄ    েৈ    োৌ্ৎabcdefghijklmnopqrstuvwxyzৗড়ঢ়ৰৱ";
        arrayOfString1[5] = "ઁંઃઅઆઇઈઉઊઋ\nઌઍ\r એઐઑ ઓઔકખગઘઙચ￿છજઝઞ !ટઠડઢણત)(થદ,ધ.ન0123456789:; પફ?બભમયર લળ વશષસહ઼ઽાિીુૂૃૄૅ ેૈૉ ોૌ્ૐabcdefghijklmnopqrstuvwxyzૠૡૢૣ૱";
        arrayOfString1[6] = "ँंःअआइईउऊऋ\nऌऍ\rऎएऐऑऒओऔकखगघङच￿छजझञ !टठडढणत)(थद,ध.न0123456789:;ऩपफ?बभमयरऱलळऴवशषसह़ऽािीुूृॄॅॆेैॉॊोौ्ॐabcdefghijklmnopqrstuvwxyzॲॻॼॾॿ";
        arrayOfString1[7] = " ಂಃಅಆಇಈಉಊಋ\nಌ \rಎಏಐ ಒಓಔಕಖಗಘಙಚ￿ಛಜಝಞ !ಟಠಡಢಣತ)(ಥದ,ಧ.ನ0123456789:; ಪಫ?ಬಭಮಯರಱಲಳ ವಶಷಸಹ಼ಽಾಿೀುೂೃೄ ೆೇೈ ೊೋೌ್ೕabcdefghijklmnopqrstuvwxyzೖೠೡೢೣ";
        arrayOfString1[8] = " ംഃഅആഇഈഉഊഋ\nഌ \rഎഏഐ ഒഓഔകഖഗഘങച￿ഛജഝഞ !ടഠഡഢണത)(ഥദ,ധ.ന0123456789:; പഫ?ബഭമയരറലളഴവശഷസഹ ഽാിീുൂൃൄ െേൈ ൊോൌ്ൗabcdefghijklmnopqrstuvwxyzൠൡൢൣ൹";
        arrayOfString1[9] = "ଁଂଃଅଆଇଈଉଊଋ\nଌ \r ଏଐ    ଓଔକଖଗଘଙଚ￿ଛଜଝଞ !ଟଠଡଢଣତ)(ଥଦ,ଧ.ନ0123456789:; ପଫ?ବଭମଯର ଲଳ ଵଶଷସହ଼ଽାିୀୁୂୃୄ    େୈ    ୋୌ୍ୖabcdefghijklmnopqrstuvwxyzୗୠୡୢୣ";
        arrayOfString1[10] = "ਁਂਃਅਆਇਈਉਊ \n    \r ਏਐ    ਓਔਕਖਗਘਙਚ￿ਛਜਝਞ !ਟਠਡਢਣਤ)(ਥਦ,ਧ.ਨ0123456789:; ਪਫ?ਬਭਮਯਰ ਲਲ਼ ਵਸ਼ ਸਹ਼ ਾਿੀੁੂ        ੇੈ    ੋੌ੍ੑabcdefghijklmnopqrstuvwxyzੰੱੲੳੴ";
        arrayOfString1[11] = " ஂஃஅஆஇஈஉஊ \n    \rஎஏஐ ஒஓஔக     ஙச￿ ஜ ஞ !ட     ணத)(    , .ந0123456789:;னப ?    மயரறலளழவஶஷஸஹ    ாிீுூ     ெேை ொோௌ்ௐabcdefghijklmnopqrstuvwxyzௗ௰௱௲௹";
        arrayOfString1[12] = "ఁంఃఅఆఇఈఉఊఋ\nఌ \rఎఏఐ ఒఓఔకఖగఘఙచ￿ఛజఝఞ !టఠడఢణత)(థద,ధ.న0123456789:; పఫ?బభమయరఱలళ వశషసహ ఽాిీుూృౄ ెేై ొోౌ్ౕabcdefghijklmnopqrstuvwxyzౖౠౡౢౣ";
        arrayOfString1[13] = "اآبٻڀپڦتۂٿ\nٹٽ\rٺټثجځڄڃڅچڇحخد￿ڌڈډڊ !ڏڍذرڑړ)(ڙز,ږ.ژ0123456789:;ښسش?صضطظعفقکڪګگڳڱلمنںڻڼوۄەہھءیېےٍُِٗٔabcdefghijklmnopqrstuvwxyzّٰٕٖٓ";
        sLanguageTables = arrayOfString1;
        String[] arrayOfString2 = new String[14];
        arrayOfString2[0] = "                    \f                 ^                                     {}         \\                        [~] |                                                                        €                                                    ";
        arrayOfString2[1] = "                    \f                 ^                                     {}         \\                        [~] |            Ğ İ                 Ş                             ç € ğ ı                 ş                        ";
        arrayOfString2[2] = "                 ç\f                 ^                                     {}         \\                        [~] |Á             Í         Ó         Ú                     á     €     í         ó         ú                    ";
        arrayOfString2[3] = "         ê     ç\fÔô Áá    ΦΓ^ΩΠΨΣΘ         Ê                {}         \\                        [~] |À             Í         Ó         Ú         ÃÕ        Â     €     í         ó         ú         ãõ    â";
        arrayOfString2[4] = "@£$¥¿\"¤%&'\f*+ -/<=>¡^¡_#*০১ ২৩৪৫৬৭৮৯য়ৠৡৢ{}ৣ৲৳৴৵\\৶৷৸৹৺             [~] |ABCDEFGHIJKLMNOPQRSTUVWXYZ                    €                                                    ";
        arrayOfString2[5] = "@£$¥¿\"¤%&'\f*+ -/<=>¡^¡_#*।॥ ૦૧૨૩૪૫૬૭૮૯    {}         \\                        [~] |ABCDEFGHIJKLMNOPQRSTUVWXYZ                    €                                                    ";
        arrayOfString2[6] = "@£$¥¿\"¤%&'\f*+ -/<=>¡^¡_#*।॥ ०१२३४५६७८९॒॑{}॓॔क़ख़ग़\\ज़ड़ढ़फ़य़ॠॡॢॣ॰ॱ [~] |ABCDEFGHIJKLMNOPQRSTUVWXYZ                    €                                                    ";
        arrayOfString2[7] = "@£$¥¿\"¤%&'\f*+ -/<=>¡^¡_#*।॥ ೦೧೨೩೪೫೬೭೮೯ೞೱ{}ೲ        \\                        [~] |ABCDEFGHIJKLMNOPQRSTUVWXYZ                    €                                                    ";
        arrayOfString2[8] = "@£$¥¿\"¤%&'\f*+ -/<=>¡^¡_#*।॥ ൦൧൨൩൪൫൬൭൮൯൰൱{}൲൳൴൵ൺ\\ൻർൽൾൿ             [~] |ABCDEFGHIJKLMNOPQRSTUVWXYZ                    €                                                    ";
        arrayOfString2[9] = "@£$¥¿\"¤%&'\f*+ -/<=>¡^¡_#*।॥ ୦୧୨୩୪୫୬୭୮୯ଡ଼ଢ଼{}ୟ୰ୱ    \\                        [~] |ABCDEFGHIJKLMNOPQRSTUVWXYZ                    €                                                    ";
        arrayOfString2[10] = "@£$¥¿\"¤%&'\f*+ -/<=>¡^¡_#*।॥ ੦੧੨੩੪੫੬੭੮੯ਖ਼ਗ਼{}ਜ਼ੜਫ਼ੵ \\                        [~] |ABCDEFGHIJKLMNOPQRSTUVWXYZ                    €                                                    ";
        arrayOfString2[11] = "@£$¥¿\"¤%&'\f*+ -/<=>¡^¡_#*।॥ ௦௧௨௩௪௫௬௭௮௯௳௴{}௵௶௷௸௺\\                        [~] |ABCDEFGHIJKLMNOPQRSTUVWXYZ                    €                                                    ";
        arrayOfString2[12] = "@£$¥¿\"¤%&'\f*+ -/<=>¡^¡_#*     ౦౧౨౩౪౫౬౭౮౯ౘౙ{}౸౹౺౻౼\\౽౾౿                 [~] |ABCDEFGHIJKLMNOPQRSTUVWXYZ                    €                                                    ";
        arrayOfString2[13] = "@£$¥¿\"¤%&'\f*+ -/<=>¡^¡_#*؀؁ ۰۱۲۳۴۵۶۷۸۹،؍{}؎؏ؐؑؒ\\ؓؔ؛؟ـْ٘٫٬ٲٳۍ[~]۔|ABCDEFGHIJKLMNOPQRSTUVWXYZ                    €                                                    ";
        sLanguageShiftTables = arrayOfString2;
        Resources localResources = Resources.getSystem();
        sEnabledSingleShiftTables = localResources.getIntArray(17236012);
        sEnabledLockingShiftTables = localResources.getIntArray(17236013);
        int i = sLanguageTables.length;
        int j = sLanguageShiftTables.length;
        if (i != j)
            Log.e("GSM", "Error: language tables array length " + i + " != shift tables array length " + j);
        if (sEnabledSingleShiftTables.length > 0)
        {
            sHighestEnabledSingleShiftCode = sEnabledSingleShiftTables[(-1 + sEnabledSingleShiftTables.length)];
            sCharsToGsmTables = new SparseIntArray[i];
        }
        for (int k = 0; ; k++)
        {
            if (k >= i)
                break label418;
            String str2 = sLanguageTables[k];
            int i3 = str2.length();
            if ((i3 != 0) && (i3 != 128))
                Log.e("GSM", "Error: language tables index " + k + " length " + i3 + " (expected 128 or 0)");
            SparseIntArray localSparseIntArray2 = new SparseIntArray(i3);
            sCharsToGsmTables[k] = localSparseIntArray2;
            int i4 = 0;
            while (true)
                if (i4 < i3)
                {
                    localSparseIntArray2.put(str2.charAt(i4), i4);
                    i4++;
                    continue;
                    sHighestEnabledSingleShiftCode = 0;
                    break;
                }
        }
        label418: sCharsToShiftTables = new SparseIntArray[i];
        for (int m = 0; m < j; m++)
        {
            String str1 = sLanguageShiftTables[m];
            int n = str1.length();
            if ((n != 0) && (n != 128))
                Log.e("GSM", "Error: language shift tables index " + m + " length " + n + " (expected 128 or 0)");
            SparseIntArray localSparseIntArray1 = new SparseIntArray(n);
            sCharsToShiftTables[m] = localSparseIntArray1;
            for (int i1 = 0; i1 < n; i1++)
            {
                int i2 = str1.charAt(i1);
                if (i2 != 32)
                    localSparseIntArray1.put(i2, i1);
            }
        }
    }

    public static int charToGsm(char paramChar)
    {
        try
        {
            int j = charToGsm(paramChar, false);
            i = j;
            return i;
        }
        catch (EncodeException localEncodeException)
        {
            while (true)
                int i = sCharsToGsmTables[0].get(32, 32);
        }
    }

    public static int charToGsm(char paramChar, boolean paramBoolean)
        throws EncodeException
    {
        int i = sCharsToGsmTables[0].get(paramChar, -1);
        int j;
        if (i == -1)
            if (sCharsToShiftTables[0].get(paramChar, -1) == -1)
            {
                if (paramBoolean)
                    throw new EncodeException(paramChar);
                j = sCharsToGsmTables[0].get(32, 32);
            }
        while (true)
        {
            return j;
            j = 27;
            continue;
            j = i;
        }
    }

    public static int charToGsmExtended(char paramChar)
    {
        int i = sCharsToShiftTables[0].get(paramChar, -1);
        if (i == -1)
            i = sCharsToGsmTables[0].get(32, 32);
        return i;
    }

    public static int countGsmSeptets(char paramChar)
    {
        int i = 0;
        try
        {
            int j = countGsmSeptets(paramChar, false);
            i = j;
            label10: return i;
        }
        catch (EncodeException localEncodeException)
        {
            break label10;
        }
    }

    public static int countGsmSeptets(char paramChar, boolean paramBoolean)
        throws EncodeException
    {
        int i = 1;
        if (sCharsToGsmTables[0].get(paramChar, -1) != -1);
        do
            while (true)
            {
                return i;
                if (sCharsToShiftTables[0].get(paramChar, -1) == -1)
                    break;
                i = 2;
            }
        while (!paramBoolean);
        throw new EncodeException(paramChar);
    }

    public static SmsMessageBase.TextEncodingDetails countGsmSeptets(CharSequence paramCharSequence, boolean paramBoolean)
    {
        SmsMessageBase.TextEncodingDetails localTextEncodingDetails;
        int i;
        if (sEnabledSingleShiftTables.length + sEnabledLockingShiftTables.length == 0)
        {
            localTextEncodingDetails = new SmsMessageBase.TextEncodingDetails();
            i = countGsmSeptetsUsingTables(paramCharSequence, paramBoolean, 0, 0);
            if (i == -1)
                localTextEncodingDetails = null;
        }
        while (true)
        {
            return localTextEncodingDetails;
            localTextEncodingDetails.codeUnitSize = 1;
            localTextEncodingDetails.codeUnitCount = i;
            int m;
            if (i > 160)
            {
                int k = (i + 152) / 153;
                localTextEncodingDetails.msgCount = k;
                m = 153 * localTextEncodingDetails.msgCount - i;
            }
            int j;
            for (localTextEncodingDetails.codeUnitsRemaining = m; ; localTextEncodingDetails.codeUnitsRemaining = j)
            {
                localTextEncodingDetails.codeUnitSize = 1;
                break;
                localTextEncodingDetails.msgCount = 1;
                j = 160 - i;
            }
            int n = sHighestEnabledSingleShiftCode;
            ArrayList localArrayList = new ArrayList(1 + sEnabledLockingShiftTables.length);
            localArrayList.add(new LanguagePairCount(0));
            for (int i20 : sEnabledLockingShiftTables)
                if ((i20 != 0) && (!sLanguageTables[i20].isEmpty()))
                {
                    LanguagePairCount localLanguagePairCount3 = new LanguagePairCount(i20);
                    localArrayList.add(localLanguagePairCount3);
                }
            int i3 = paramCharSequence.length();
            int i4 = 0;
            if ((i4 < i3) && (!localArrayList.isEmpty()))
            {
                int i17 = paramCharSequence.charAt(i4);
                if (i17 == 27)
                    Log.w("GSM", "countGsmSeptets() string contains Escape character, ignoring!");
                while (true)
                {
                    i4++;
                    break;
                    Iterator localIterator2 = localArrayList.iterator();
                    while (localIterator2.hasNext())
                    {
                        LanguagePairCount localLanguagePairCount2 = (LanguagePairCount)localIterator2.next();
                        if (sCharsToGsmTables[localLanguagePairCount2.languageCode].get(i17, -1) == -1)
                        {
                            int i19 = 0;
                            label340: if (i19 <= n)
                                if (localLanguagePairCount2.septetCounts[i19] != -1)
                                {
                                    if (sCharsToShiftTables[i19].get(i17, -1) != -1)
                                        break label439;
                                    if (!paramBoolean)
                                        break label426;
                                    int[] arrayOfInt4 = localLanguagePairCount2.septetCounts;
                                    arrayOfInt4[i19] = (1 + arrayOfInt4[i19]);
                                    int[] arrayOfInt5 = localLanguagePairCount2.unencodableCounts;
                                    arrayOfInt5[i19] = (1 + arrayOfInt5[i19]);
                                }
                            while (true)
                            {
                                i19++;
                                break label340;
                                break;
                                label426: localLanguagePairCount2.septetCounts[i19] = -1;
                                continue;
                                label439: int[] arrayOfInt3 = localLanguagePairCount2.septetCounts;
                                arrayOfInt3[i19] = (2 + arrayOfInt3[i19]);
                            }
                        }
                        for (int i18 = 0; i18 <= n; i18++)
                            if (localLanguagePairCount2.septetCounts[i18] != -1)
                            {
                                int[] arrayOfInt2 = localLanguagePairCount2.septetCounts;
                                arrayOfInt2[i18] = (1 + arrayOfInt2[i18]);
                            }
                    }
                }
            }
            localTextEncodingDetails = new SmsMessageBase.TextEncodingDetails();
            localTextEncodingDetails.msgCount = 2147483647;
            localTextEncodingDetails.codeUnitSize = 1;
            int i5 = 2147483647;
            Iterator localIterator1 = localArrayList.iterator();
            while (localIterator1.hasNext())
            {
                LanguagePairCount localLanguagePairCount1 = (LanguagePairCount)localIterator1.next();
                int i7;
                for (int i6 = 0; i6 <= n; i6++)
                {
                    i7 = localLanguagePairCount1.septetCounts[i6];
                    if (i7 != -1)
                        break label598;
                }
                continue;
                label598: int i8;
                label615: int i16;
                int i9;
                if ((localLanguagePairCount1.languageCode != 0) && (i6 != 0))
                {
                    i8 = 8;
                    if (i7 + i8 <= 160)
                        break label808;
                    if (i8 == 0)
                        i8 = 1;
                    i16 = 160 - (i8 + 6);
                    i9 = (-1 + (i7 + i16)) / i16;
                }
                for (int i10 = i9 * i16 - i7; ; i10 = 160 - i8 - i7)
                {
                    int i11 = localLanguagePairCount1.unencodableCounts[i6];
                    if ((paramBoolean) && (i11 > i5))
                        break;
                    if (((!paramBoolean) || (i11 >= i5)) && (i9 >= localTextEncodingDetails.msgCount))
                    {
                        if (i9 != localTextEncodingDetails.msgCount)
                            break;
                        int i15 = localTextEncodingDetails.codeUnitsRemaining;
                        if (i10 <= i15)
                            break;
                    }
                    i5 = i11;
                    localTextEncodingDetails.msgCount = i9;
                    localTextEncodingDetails.codeUnitCount = i7;
                    int i12 = i10;
                    localTextEncodingDetails.codeUnitsRemaining = i12;
                    int i13 = localLanguagePairCount1.languageCode;
                    localTextEncodingDetails.languageTable = i13;
                    int i14 = i6;
                    localTextEncodingDetails.languageShiftTable = i14;
                    break;
                    if ((localLanguagePairCount1.languageCode != 0) || (i6 != 0))
                    {
                        i8 = 5;
                        break label615;
                    }
                    i8 = 0;
                    break label615;
                    label808: i9 = 1;
                }
            }
            if (localTextEncodingDetails.msgCount == 2147483647)
                localTextEncodingDetails = null;
        }
    }

    public static int countGsmSeptetsUsingTables(CharSequence paramCharSequence, boolean paramBoolean, int paramInt1, int paramInt2)
    {
        int i = 0;
        int j = paramCharSequence.length();
        SparseIntArray localSparseIntArray1 = sCharsToGsmTables[paramInt1];
        SparseIntArray localSparseIntArray2 = sCharsToShiftTables[paramInt2];
        int k = 0;
        if (k < j)
        {
            int m = paramCharSequence.charAt(k);
            if (m == 27)
                Log.w("GSM", "countGsmSeptets() string contains Escape character, skipping.");
            while (true)
            {
                k++;
                break;
                if (localSparseIntArray1.get(m, -1) != -1)
                {
                    i++;
                }
                else if (localSparseIntArray2.get(m, -1) != -1)
                {
                    i += 2;
                }
                else
                {
                    if (!paramBoolean)
                        break label117;
                    i++;
                }
            }
            label117: i = -1;
        }
        return i;
    }

    public static int findGsmSeptetLimitIndex(String paramString, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        int i = 0;
        int j = paramString.length();
        SparseIntArray localSparseIntArray1 = sCharsToGsmTables[paramInt3];
        SparseIntArray localSparseIntArray2 = sCharsToShiftTables[paramInt4];
        int k = paramInt1;
        if (k < j)
            if (localSparseIntArray1.get(paramString.charAt(k), -1) == -1)
                if (localSparseIntArray2.get(paramString.charAt(k), -1) == -1)
                {
                    i++;
                    label73: if (i <= paramInt2)
                        break label94;
                }
        while (true)
        {
            return k;
            i += 2;
            break label73;
            i++;
            break label73;
            label94: k++;
            break;
            k = j;
        }
    }

    /** @deprecated */
    static int[] getEnabledLockingShiftTables()
    {
        try
        {
            int[] arrayOfInt = sEnabledLockingShiftTables;
            return arrayOfInt;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    static int[] getEnabledSingleShiftTables()
    {
        try
        {
            int[] arrayOfInt = sEnabledSingleShiftTables;
            return arrayOfInt;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public static String gsm7BitPackedToString(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    {
        return gsm7BitPackedToString(paramArrayOfByte, paramInt1, paramInt2, 0, 0, 0);
    }

    public static String gsm7BitPackedToString(byte[] paramArrayOfByte, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
    {
        StringBuilder localStringBuilder = new StringBuilder(paramInt2);
        if (paramInt4 >= 0)
        {
            int i3 = sLanguageTables.length;
            if (paramInt4 <= i3);
        }
        else
        {
            Log.w("GSM", "unknown language table " + paramInt4 + ", using default");
            paramInt4 = 0;
        }
        if (paramInt5 >= 0)
        {
            int i2 = sLanguageShiftTables.length;
            if (paramInt5 <= i2);
        }
        else
        {
            Log.w("GSM", "unknown single shift table " + paramInt5 + ", using default");
            paramInt5 = 0;
        }
        int i = 0;
        int j;
        int i1;
        while (true)
        {
            String str2;
            char c;
            String str1;
            try
            {
                str2 = sLanguageTables[paramInt4];
                String str3 = sLanguageShiftTables[paramInt5];
                if (str2.isEmpty())
                {
                    Log.w("GSM", "no language table for code " + paramInt4 + ", using default");
                    str2 = sLanguageTables[0];
                }
                if (!str3.isEmpty())
                    break label426;
                Log.w("GSM", "no single shift table for code " + paramInt5 + ", using default");
                str3 = sLanguageShiftTables[0];
                break label426;
                if (j < paramInt2)
                {
                    int k = paramInt3 + j * 7;
                    int m = k / 8;
                    int n = k % 8;
                    i1 = 0x7F & paramArrayOfByte[(paramInt1 + m)] >> n;
                    if (n > 1)
                        i1 = i1 & 127 >> n - 1 | 0x7F & paramArrayOfByte[(1 + (paramInt1 + m))] << 8 - n;
                    if (i == 0)
                        break label441;
                    if (i1 == 27)
                    {
                        localStringBuilder.append(' ');
                    }
                    else
                    {
                        c = str3.charAt(i1);
                        if (c == ' ')
                            localStringBuilder.append(str2.charAt(i1));
                    }
                }
            }
            catch (RuntimeException localRuntimeException)
            {
                Log.e("GSM", "Error GSM 7 bit packed: ", localRuntimeException);
                str1 = null;
            }
            while (true)
            {
                return str1;
                localStringBuilder.append(c);
                break;
                label400: localStringBuilder.append(str2.charAt(i1));
                break label435;
                str1 = localStringBuilder.toString();
            }
            label426: j = 0;
        }
        for (i = 0; ; i = 1)
        {
            label435: j++;
            break;
            label441: if (i1 != 27)
                break label400;
        }
    }

    public static String gsm8BitUnpackedToString(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    {
        return gsm8BitUnpackedToString(paramArrayOfByte, paramInt1, paramInt2, "");
    }

    public static String gsm8BitUnpackedToString(byte[] paramArrayOfByte, int paramInt1, int paramInt2, String paramString)
    {
        int i = 0;
        Charset localCharset = null;
        ByteBuffer localByteBuffer = null;
        if ((!TextUtils.isEmpty(paramString)) && (!paramString.equalsIgnoreCase("us-ascii")) && (Charset.isSupported(paramString)))
        {
            i = 1;
            localCharset = Charset.forName(paramString);
            localByteBuffer = ByteBuffer.allocate(2);
        }
        String str1 = sLanguageTables[0];
        String str2 = sLanguageShiftTables[0];
        StringBuilder localStringBuilder = new StringBuilder(paramInt2);
        int j = 0;
        int k = paramInt1;
        int m;
        if (k < paramInt1 + paramInt2)
        {
            m = 0xFF & paramArrayOfByte[k];
            if (m != 255);
        }
        else
        {
            return localStringBuilder.toString();
        }
        int n;
        if (m == 27)
        {
            if (j != 0)
            {
                localStringBuilder.append(' ');
                j = 0;
            }
            for (n = k; ; n = k)
            {
                k = n + 1;
                break;
                j = 1;
            }
        }
        char c;
        if (j != 0)
        {
            c = str2.charAt(m);
            if (c == ' ')
            {
                localStringBuilder.append(str1.charAt(m));
                label190: n = k;
            }
        }
        while (true)
        {
            j = 0;
            break;
            localStringBuilder.append(c);
            break label190;
            if ((i == 0) || (m < 128) || (k + 1 >= paramInt1 + paramInt2))
            {
                localStringBuilder.append(str1.charAt(m));
                n = k;
            }
            else
            {
                localByteBuffer.clear();
                n = k + 1;
                localByteBuffer.put(paramArrayOfByte, k, 2);
                localByteBuffer.flip();
                localStringBuilder.append(localCharset.decode(localByteBuffer).toString());
            }
        }
    }

    public static char gsmExtendedToChar(int paramInt)
    {
        char c;
        if (paramInt == 27)
            c = ' ';
        while (true)
        {
            return c;
            if ((paramInt >= 0) && (paramInt < 128))
            {
                c = sLanguageShiftTables[0].charAt(paramInt);
                if (c == ' ')
                    c = sLanguageTables[0].charAt(paramInt);
            }
            else
            {
                c = ' ';
            }
        }
    }

    public static char gsmToChar(int paramInt)
    {
        if ((paramInt >= 0) && (paramInt < 128));
        for (char c = sLanguageTables[0].charAt(paramInt); ; c = ' ')
            return c;
    }

    private static void packSmsChar(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    {
        int i = paramInt1 / 8;
        int j = paramInt1 % 8;
        int k = i + 1;
        paramArrayOfByte[k] = ((byte)(paramArrayOfByte[k] | paramInt2 << j));
        if (j > 1)
            paramArrayOfByte[(k + 1)] = ((byte)(paramInt2 >> 8 - j));
    }

    /** @deprecated */
    static void setEnabledLockingShiftTables(int[] paramArrayOfInt)
    {
        try
        {
            sEnabledLockingShiftTables = paramArrayOfInt;
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    static void setEnabledSingleShiftTables(int[] paramArrayOfInt)
    {
        try
        {
            sEnabledSingleShiftTables = paramArrayOfInt;
            if (paramArrayOfInt.length > 0);
            for (sHighestEnabledSingleShiftCode = paramArrayOfInt[(-1 + paramArrayOfInt.length)]; ; sHighestEnabledSingleShiftCode = 0)
                return;
        }
        finally
        {
        }
    }

    public static byte[] stringToGsm7BitPacked(String paramString)
        throws EncodeException
    {
        return stringToGsm7BitPacked(paramString, 0, true, 0, 0);
    }

    public static byte[] stringToGsm7BitPacked(String paramString, int paramInt1, int paramInt2)
        throws EncodeException
    {
        return stringToGsm7BitPacked(paramString, 0, true, paramInt1, paramInt2);
    }

    public static byte[] stringToGsm7BitPacked(String paramString, int paramInt1, boolean paramBoolean, int paramInt2, int paramInt3)
        throws EncodeException
    {
        int i = paramString.length();
        if (!paramBoolean);
        int j;
        for (boolean bool = true; ; bool = false)
        {
            j = countGsmSeptetsUsingTables(paramString, bool, paramInt2, paramInt3);
            if (j != -1)
                break;
            throw new EncodeException("countGsmSeptetsUsingTables(): unencodable char");
        }
        int k = j + paramInt1;
        if (k > 255)
            throw new EncodeException("Payload cannot exceed 255 septets");
        byte[] arrayOfByte = new byte[1 + (7 + k * 7) / 8];
        SparseIntArray localSparseIntArray1 = sCharsToGsmTables[paramInt2];
        SparseIntArray localSparseIntArray2 = sCharsToShiftTables[paramInt3];
        int m = 0;
        int n = paramInt1;
        int i1 = paramInt1 * 7;
        if ((m < i) && (n < k))
        {
            int i2 = paramString.charAt(m);
            int i3 = localSparseIntArray1.get(i2, -1);
            if (i3 == -1)
            {
                i3 = localSparseIntArray2.get(i2, -1);
                if (i3 != -1)
                    break label222;
                if (paramBoolean)
                    throw new EncodeException("stringToGsm7BitPacked(): unencodable char");
                i3 = localSparseIntArray1.get(32, 32);
            }
            while (true)
            {
                packSmsChar(arrayOfByte, i1, i3);
                n++;
                m++;
                i1 += 7;
                break;
                label222: packSmsChar(arrayOfByte, i1, 27);
                i1 += 7;
                n++;
            }
        }
        arrayOfByte[0] = ((byte)k);
        return arrayOfByte;
    }

    public static byte[] stringToGsm7BitPackedWithHeader(String paramString, byte[] paramArrayOfByte)
        throws EncodeException
    {
        return stringToGsm7BitPackedWithHeader(paramString, paramArrayOfByte, 0, 0);
    }

    public static byte[] stringToGsm7BitPackedWithHeader(String paramString, byte[] paramArrayOfByte, int paramInt1, int paramInt2)
        throws EncodeException
    {
        byte[] arrayOfByte;
        if ((paramArrayOfByte == null) || (paramArrayOfByte.length == 0))
            arrayOfByte = stringToGsm7BitPacked(paramString, paramInt1, paramInt2);
        while (true)
        {
            return arrayOfByte;
            arrayOfByte = stringToGsm7BitPacked(paramString, (6 + 8 * (1 + paramArrayOfByte.length)) / 7, true, paramInt1, paramInt2);
            arrayOfByte[1] = ((byte)paramArrayOfByte.length);
            System.arraycopy(paramArrayOfByte, 0, arrayOfByte, 2, paramArrayOfByte.length);
        }
    }

    public static byte[] stringToGsm8BitPacked(String paramString)
    {
        byte[] arrayOfByte = new byte[countGsmSeptetsUsingTables(paramString, true, 0, 0)];
        stringToGsm8BitUnpackedField(paramString, arrayOfByte, 0, arrayOfByte.length);
        return arrayOfByte;
    }

    public static void stringToGsm8BitUnpackedField(String paramString, byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    {
        SparseIntArray localSparseIntArray1 = sCharsToGsmTables[0];
        SparseIntArray localSparseIntArray2 = sCharsToShiftTables[0];
        int i = 0;
        int j = paramString.length();
        int k = paramInt1;
        int i1;
        int i2;
        if ((i < j) && (k - paramInt1 < paramInt2))
        {
            int n = paramString.charAt(i);
            i1 = localSparseIntArray1.get(n, -1);
            if (i1 != -1)
                break label172;
            i1 = localSparseIntArray2.get(n, -1);
            if (i1 == -1)
            {
                i1 = localSparseIntArray1.get(32, 32);
                i2 = k;
            }
        }
        while (true)
        {
            k = i2 + 1;
            paramArrayOfByte[i2] = ((byte)i1);
            i++;
            break;
            if (k + 1 - paramInt1 >= paramInt2)
                while (k - paramInt1 < paramInt2)
                {
                    int m = k + 1;
                    paramArrayOfByte[k] = -1;
                    k = m;
                }
            i2 = k + 1;
            paramArrayOfByte[k] = 27;
            continue;
            return;
            label172: i2 = k;
        }
    }

    private static class LanguagePairCount
    {
        final int languageCode;
        final int[] septetCounts;
        final int[] unencodableCounts;

        LanguagePairCount(int paramInt)
        {
            this.languageCode = paramInt;
            int i = GsmAlphabet.sHighestEnabledSingleShiftCode;
            this.septetCounts = new int[i + 1];
            this.unencodableCounts = new int[i + 1];
            int j = 1;
            int k = 0;
            if (j <= i)
            {
                if (GsmAlphabet.sEnabledSingleShiftTables[k] == j)
                    k++;
                while (true)
                {
                    j++;
                    break;
                    this.septetCounts[j] = -1;
                }
            }
            if ((paramInt == 1) && (i >= 1))
                this.septetCounts[1] = -1;
            while (true)
            {
                return;
                if ((paramInt == 3) && (i >= 2))
                    this.septetCounts[2] = -1;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.GsmAlphabet
 * JD-Core Version:        0.6.2
 */