package com.android.internal.telephony;

public class ApnSetting
{
    static final String V2_FORMAT_REGEX = "^\\[ApnSettingV2\\]\\s*";
    public final String apn;
    public final int authType;
    public final int bearer;
    public final String carrier;
    public final boolean carrierEnabled;
    public final int id;
    public final String mmsPort;
    public final String mmsProxy;
    public final String mmsc;
    public final String numeric;
    public final String password;
    public final String port;
    public final String protocol;
    public final String proxy;
    public final String roamingProtocol;
    public final String[] types;
    public final String user;

    public ApnSetting(int paramInt1, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, String paramString9, String paramString10, int paramInt2, String[] paramArrayOfString, String paramString11, String paramString12, boolean paramBoolean, int paramInt3)
    {
        this.id = paramInt1;
        this.numeric = paramString1;
        this.carrier = paramString2;
        this.apn = paramString3;
        this.proxy = paramString4;
        this.port = paramString5;
        this.mmsc = paramString6;
        this.mmsProxy = paramString7;
        this.mmsPort = paramString8;
        this.user = paramString9;
        this.password = paramString10;
        this.authType = paramInt2;
        this.types = paramArrayOfString;
        this.protocol = paramString11;
        this.roamingProtocol = paramString12;
        this.carrierEnabled = paramBoolean;
        this.bearer = paramInt3;
    }

    public static ApnSetting fromString(String paramString)
    {
        ApnSetting localApnSetting;
        if (paramString == null)
        {
            localApnSetting = null;
            return localApnSetting;
        }
        int i;
        if (paramString.matches("^\\[ApnSettingV2\\]\\s*.*"))
        {
            i = 2;
            paramString = paramString.replaceFirst("^\\[ApnSettingV2\\]\\s*", "");
        }
        String[] arrayOfString1;
        while (true)
        {
            arrayOfString1 = paramString.split("\\s*,\\s*");
            if (arrayOfString1.length >= 14)
                break label55;
            localApnSetting = null;
            break;
            i = 1;
        }
        while (true)
        {
            label55: boolean bool1;
            int k;
            try
            {
                int m = Integer.parseInt(arrayOfString1[12]);
                j = m;
                if (i == 1)
                {
                    arrayOfString2 = new String[-13 + arrayOfString1.length];
                    System.arraycopy(arrayOfString1, 13, arrayOfString2, 0, -13 + arrayOfString1.length);
                    str1 = "IP";
                    str2 = "IP";
                    bool1 = true;
                    k = 0;
                    localApnSetting = new ApnSetting(-1, arrayOfString1[10] + arrayOfString1[11], arrayOfString1[0], arrayOfString1[1], arrayOfString1[2], arrayOfString1[3], arrayOfString1[7], arrayOfString1[8], arrayOfString1[9], arrayOfString1[4], arrayOfString1[5], j, arrayOfString2, str1, str2, bool1, k);
                }
            }
            catch (Exception localException1)
            {
                int j = 0;
                continue;
                if (arrayOfString1.length < 18)
                {
                    localApnSetting = null;
                    break;
                }
                String[] arrayOfString2 = arrayOfString1[13].split("\\s*\\|\\s*");
                String str1 = arrayOfString1[14];
                String str2 = arrayOfString1[15];
            }
            try
            {
                boolean bool2 = Boolean.parseBoolean(arrayOfString1[16]);
                bool1 = bool2;
                k = Integer.parseInt(arrayOfString1[17]);
            }
            catch (Exception localException2)
            {
                while (true)
                    bool1 = true;
            }
        }
    }

    public boolean canHandleType(String paramString)
    {
        String[] arrayOfString = this.types;
        int i = arrayOfString.length;
        int j = 0;
        if (j < i)
        {
            String str = arrayOfString[j];
            if ((!str.equalsIgnoreCase(paramString)) && (!str.equalsIgnoreCase("*")) && ((!str.equalsIgnoreCase("default")) || (!paramString.equalsIgnoreCase("hipri"))));
        }
        for (boolean bool = true; ; bool = false)
        {
            return bool;
            j++;
            break;
        }
    }

    public boolean equals(Object paramObject)
    {
        if (!(paramObject instanceof ApnSetting));
        for (boolean bool = false; ; bool = toString().equals(paramObject.toString()))
            return bool;
    }

    public String toString()
    {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("[ApnSettingV2] ").append(this.carrier).append(", ").append(this.id).append(", ").append(this.numeric).append(", ").append(this.apn).append(", ").append(this.proxy).append(", ").append(this.mmsc).append(", ").append(this.mmsProxy).append(", ").append(this.mmsPort).append(", ").append(this.port).append(", ").append(this.authType).append(", ");
        for (int i = 0; i < this.types.length; i++)
        {
            localStringBuilder.append(this.types[i]);
            if (i < -1 + this.types.length)
                localStringBuilder.append(" | ");
        }
        localStringBuilder.append(", ").append(this.protocol);
        localStringBuilder.append(", ").append(this.roamingProtocol);
        localStringBuilder.append(", ").append(this.carrierEnabled);
        localStringBuilder.append(", ").append(this.bearer);
        return localStringBuilder.toString();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.ApnSetting
 * JD-Core Version:        0.6.2
 */