package com.android.internal.telephony;

import android.app.ActivityManagerNative;
import android.app.AlarmManager;
import android.app.IActivityManager;
import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.net.wifi.WifiManager;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.text.TextUtils;
import android.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;
import libcore.icu.TimeZones;

public final class MccTable
{
    static final String LOG_TAG = "MccTable";
    static ArrayList<MccEntry> table = new ArrayList(240);

    static
    {
        table.add(new MccEntry(202, "gr", 2));
        table.add(new MccEntry(204, "nl", 2, "nl"));
        table.add(new MccEntry(206, "be", 2));
        table.add(new MccEntry(208, "fr", 2, "fr"));
        table.add(new MccEntry(212, "mc", 2));
        table.add(new MccEntry(213, "ad", 2));
        table.add(new MccEntry(214, "es", 2, "es"));
        table.add(new MccEntry(216, "hu", 2));
        table.add(new MccEntry(218, "ba", 2));
        table.add(new MccEntry(219, "hr", 2));
        table.add(new MccEntry(220, "rs", 2));
        table.add(new MccEntry(222, "it", 2, "it"));
        table.add(new MccEntry(225, "va", 2, "it"));
        table.add(new MccEntry(226, "ro", 2));
        table.add(new MccEntry(228, "ch", 2, "de"));
        table.add(new MccEntry(230, "cz", 2, "cs"));
        table.add(new MccEntry(231, "sk", 2));
        table.add(new MccEntry(232, "at", 2, "de"));
        table.add(new MccEntry(234, "gb", 2, "en"));
        table.add(new MccEntry(235, "gb", 2, "en"));
        table.add(new MccEntry(238, "dk", 2));
        table.add(new MccEntry(240, "se", 2));
        table.add(new MccEntry(242, "no", 2));
        table.add(new MccEntry(244, "fi", 2));
        table.add(new MccEntry(246, "lt", 2));
        table.add(new MccEntry(247, "lv", 2));
        table.add(new MccEntry(248, "ee", 2));
        table.add(new MccEntry(250, "ru", 2));
        table.add(new MccEntry(255, "ua", 2));
        table.add(new MccEntry(257, "by", 2));
        table.add(new MccEntry(259, "md", 2));
        table.add(new MccEntry(260, "pl", 2));
        table.add(new MccEntry(262, "de", 2, "de"));
        table.add(new MccEntry(266, "gi", 2));
        table.add(new MccEntry(268, "pt", 2));
        table.add(new MccEntry(270, "lu", 2));
        table.add(new MccEntry(272, "ie", 2, "en"));
        table.add(new MccEntry(274, "is", 2));
        table.add(new MccEntry(276, "al", 2));
        table.add(new MccEntry(278, "mt", 2));
        table.add(new MccEntry(280, "cy", 2));
        table.add(new MccEntry(282, "ge", 2));
        table.add(new MccEntry(283, "am", 2));
        table.add(new MccEntry(284, "bg", 2));
        table.add(new MccEntry(286, "tr", 2));
        table.add(new MccEntry(288, "fo", 2));
        table.add(new MccEntry(289, "ge", 2));
        table.add(new MccEntry(290, "gl", 2));
        table.add(new MccEntry(292, "sm", 2));
        table.add(new MccEntry(293, "si", 2));
        table.add(new MccEntry(294, "mk", 2));
        table.add(new MccEntry(295, "li", 2));
        table.add(new MccEntry(297, "me", 2));
        table.add(new MccEntry(302, "ca", 3, ""));
        table.add(new MccEntry(308, "pm", 2));
        table.add(new MccEntry(310, "us", 3, "en"));
        table.add(new MccEntry(311, "us", 3, "en"));
        table.add(new MccEntry(312, "us", 3, "en"));
        table.add(new MccEntry(313, "us", 3, "en"));
        table.add(new MccEntry(314, "us", 3, "en"));
        table.add(new MccEntry(315, "us", 3, "en"));
        table.add(new MccEntry(316, "us", 3, "en"));
        table.add(new MccEntry(330, "pr", 2));
        table.add(new MccEntry(332, "vi", 2));
        table.add(new MccEntry(334, "mx", 3));
        table.add(new MccEntry(338, "jm", 3));
        table.add(new MccEntry(340, "gp", 2));
        table.add(new MccEntry(342, "bb", 3));
        table.add(new MccEntry(344, "ag", 3));
        table.add(new MccEntry(346, "ky", 3));
        table.add(new MccEntry(348, "vg", 3));
        table.add(new MccEntry(350, "bm", 2));
        table.add(new MccEntry(352, "gd", 2));
        table.add(new MccEntry(354, "ms", 2));
        table.add(new MccEntry(356, "kn", 2));
        table.add(new MccEntry(358, "lc", 2));
        table.add(new MccEntry(360, "vc", 2));
        table.add(new MccEntry(362, "ai", 2));
        table.add(new MccEntry(363, "aw", 2));
        table.add(new MccEntry(364, "bs", 2));
        table.add(new MccEntry(365, "ai", 3));
        table.add(new MccEntry(366, "dm", 2));
        table.add(new MccEntry(368, "cu", 2));
        table.add(new MccEntry(370, "do", 2));
        table.add(new MccEntry(372, "ht", 2));
        table.add(new MccEntry(374, "tt", 2));
        table.add(new MccEntry(376, "tc", 2));
        table.add(new MccEntry(400, "az", 2));
        table.add(new MccEntry(401, "kz", 2));
        table.add(new MccEntry(402, "bt", 2));
        table.add(new MccEntry(404, "in", 2));
        table.add(new MccEntry(405, "in", 2));
        table.add(new MccEntry(410, "pk", 2));
        table.add(new MccEntry(412, "af", 2));
        table.add(new MccEntry(413, "lk", 2));
        table.add(new MccEntry(414, "mm", 2));
        table.add(new MccEntry(415, "lb", 2));
        table.add(new MccEntry(416, "jo", 2));
        table.add(new MccEntry(417, "sy", 2));
        table.add(new MccEntry(418, "iq", 2));
        table.add(new MccEntry(419, "kw", 2));
        table.add(new MccEntry(420, "sa", 2));
        table.add(new MccEntry(421, "ye", 2));
        table.add(new MccEntry(422, "om", 2));
        table.add(new MccEntry(423, "ps", 2));
        table.add(new MccEntry(424, "ae", 2));
        table.add(new MccEntry(425, "il", 2));
        table.add(new MccEntry(426, "bh", 2));
        table.add(new MccEntry(427, "qa", 2));
        table.add(new MccEntry(428, "mn", 2));
        table.add(new MccEntry(429, "np", 2));
        table.add(new MccEntry(430, "ae", 2));
        table.add(new MccEntry(431, "ae", 2));
        table.add(new MccEntry(432, "ir", 2));
        table.add(new MccEntry(434, "uz", 2));
        table.add(new MccEntry(436, "tj", 2));
        table.add(new MccEntry(437, "kg", 2));
        table.add(new MccEntry(438, "tm", 2));
        table.add(new MccEntry(440, "jp", 2, "ja"));
        table.add(new MccEntry(441, "jp", 2, "ja"));
        table.add(new MccEntry(450, "kr", 2, "ko"));
        table.add(new MccEntry(452, "vn", 2));
        table.add(new MccEntry(454, "hk", 2));
        table.add(new MccEntry(455, "mo", 2));
        table.add(new MccEntry(456, "kh", 2));
        table.add(new MccEntry(457, "la", 2));
        table.add(new MccEntry(460, "cn", 2, "zh"));
        table.add(new MccEntry(461, "cn", 2, "zh"));
        table.add(new MccEntry(466, "tw", 2));
        table.add(new MccEntry(467, "kp", 2));
        table.add(new MccEntry(470, "bd", 2));
        table.add(new MccEntry(472, "mv", 2));
        table.add(new MccEntry(502, "my", 2));
        table.add(new MccEntry(505, "au", 2, "en"));
        table.add(new MccEntry(510, "id", 2));
        table.add(new MccEntry(514, "tl", 2));
        table.add(new MccEntry(515, "ph", 2));
        table.add(new MccEntry(520, "th", 2));
        table.add(new MccEntry(525, "sg", 2, "en"));
        table.add(new MccEntry(528, "bn", 2));
        table.add(new MccEntry(530, "nz", 2, "en"));
        table.add(new MccEntry(534, "mp", 2));
        table.add(new MccEntry(535, "gu", 2));
        table.add(new MccEntry(536, "nr", 2));
        table.add(new MccEntry(537, "pg", 2));
        table.add(new MccEntry(539, "to", 2));
        table.add(new MccEntry(540, "sb", 2));
        table.add(new MccEntry(541, "vu", 2));
        table.add(new MccEntry(542, "fj", 2));
        table.add(new MccEntry(543, "wf", 2));
        table.add(new MccEntry(544, "as", 2));
        table.add(new MccEntry(545, "ki", 2));
        table.add(new MccEntry(546, "nc", 2));
        table.add(new MccEntry(547, "pf", 2));
        table.add(new MccEntry(548, "ck", 2));
        table.add(new MccEntry(549, "ws", 2));
        table.add(new MccEntry(550, "fm", 2));
        table.add(new MccEntry(551, "mh", 2));
        table.add(new MccEntry(552, "pw", 2));
        table.add(new MccEntry(602, "eg", 2));
        table.add(new MccEntry(603, "dz", 2));
        table.add(new MccEntry(604, "ma", 2));
        table.add(new MccEntry(605, "tn", 2));
        table.add(new MccEntry(606, "ly", 2));
        table.add(new MccEntry(607, "gm", 2));
        table.add(new MccEntry(608, "sn", 2));
        table.add(new MccEntry(609, "mr", 2));
        table.add(new MccEntry(610, "ml", 2));
        table.add(new MccEntry(611, "gn", 2));
        table.add(new MccEntry(612, "ci", 2));
        table.add(new MccEntry(613, "bf", 2));
        table.add(new MccEntry(614, "ne", 2));
        table.add(new MccEntry(615, "tg", 2));
        table.add(new MccEntry(616, "bj", 2));
        table.add(new MccEntry(617, "mu", 2));
        table.add(new MccEntry(618, "lr", 2));
        table.add(new MccEntry(619, "sl", 2));
        table.add(new MccEntry(620, "gh", 2));
        table.add(new MccEntry(621, "ng", 2));
        table.add(new MccEntry(622, "td", 2));
        table.add(new MccEntry(623, "cf", 2));
        table.add(new MccEntry(624, "cm", 2));
        table.add(new MccEntry(625, "cv", 2));
        table.add(new MccEntry(626, "st", 2));
        table.add(new MccEntry(627, "gq", 2));
        table.add(new MccEntry(628, "ga", 2));
        table.add(new MccEntry(629, "cg", 2));
        table.add(new MccEntry(630, "cg", 2));
        table.add(new MccEntry(631, "ao", 2));
        table.add(new MccEntry(632, "gw", 2));
        table.add(new MccEntry(633, "sc", 2));
        table.add(new MccEntry(634, "sd", 2));
        table.add(new MccEntry(635, "rw", 2));
        table.add(new MccEntry(636, "et", 2));
        table.add(new MccEntry(637, "so", 2));
        table.add(new MccEntry(638, "dj", 2));
        table.add(new MccEntry(639, "ke", 2));
        table.add(new MccEntry(640, "tz", 2));
        table.add(new MccEntry(641, "ug", 2));
        table.add(new MccEntry(642, "bi", 2));
        table.add(new MccEntry(643, "mz", 2));
        table.add(new MccEntry(645, "zm", 2));
        table.add(new MccEntry(646, "mg", 2));
        table.add(new MccEntry(647, "re", 2));
        table.add(new MccEntry(648, "zw", 2));
        table.add(new MccEntry(649, "na", 2));
        table.add(new MccEntry(650, "mw", 2));
        table.add(new MccEntry(651, "ls", 2));
        table.add(new MccEntry(652, "bw", 2));
        table.add(new MccEntry(653, "sz", 2));
        table.add(new MccEntry(654, "km", 2));
        table.add(new MccEntry(655, "za", 2, "en"));
        table.add(new MccEntry(657, "er", 2));
        table.add(new MccEntry(702, "bz", 2));
        table.add(new MccEntry(704, "gt", 2));
        table.add(new MccEntry(706, "sv", 2));
        table.add(new MccEntry(708, "hn", 3));
        table.add(new MccEntry(710, "ni", 2));
        table.add(new MccEntry(712, "cr", 2));
        table.add(new MccEntry(714, "pa", 2));
        table.add(new MccEntry(716, "pe", 2));
        table.add(new MccEntry(722, "ar", 3));
        table.add(new MccEntry(724, "br", 2));
        table.add(new MccEntry(730, "cl", 2));
        table.add(new MccEntry(732, "co", 3));
        table.add(new MccEntry(734, "ve", 2));
        table.add(new MccEntry(736, "bo", 2));
        table.add(new MccEntry(738, "gy", 2));
        table.add(new MccEntry(740, "ec", 2));
        table.add(new MccEntry(742, "gf", 2));
        table.add(new MccEntry(744, "py", 2));
        table.add(new MccEntry(746, "sr", 2));
        table.add(new MccEntry(748, "uy", 2));
        table.add(new MccEntry(750, "fk", 2));
        Collections.sort(table);
    }

    public static String countryCodeForMcc(int paramInt)
    {
        MccEntry localMccEntry = entryForMcc(paramInt);
        if (localMccEntry == null);
        for (String str = ""; ; str = localMccEntry.iso)
            return str;
    }

    public static String defaultLanguageForMcc(int paramInt)
    {
        MccEntry localMccEntry = entryForMcc(paramInt);
        if (localMccEntry == null);
        for (String str = null; ; str = localMccEntry.language)
            return str;
    }

    public static String defaultTimeZoneForMcc(int paramInt)
    {
        String str = null;
        MccEntry localMccEntry = entryForMcc(paramInt);
        if ((localMccEntry == null) || (localMccEntry.iso == null))
            return str;
        if (localMccEntry.language == null);
        for (Locale localLocale = new Locale(localMccEntry.iso); ; localLocale = new Locale(localMccEntry.language, localMccEntry.iso))
        {
            String[] arrayOfString = TimeZones.forLocale(localLocale);
            if (arrayOfString.length == 0)
                break;
            str = arrayOfString[0];
            break;
        }
    }

    private static MccEntry entryForMcc(int paramInt)
    {
        MccEntry localMccEntry1 = null;
        MccEntry localMccEntry2 = new MccEntry(paramInt, null, 0);
        int i = Collections.binarySearch(table, localMccEntry2);
        if (i < 0);
        while (true)
        {
            return localMccEntry1;
            localMccEntry1 = (MccEntry)table.get(i);
        }
    }

    private static void setLocaleFromMccIfNeeded(Context paramContext, int paramInt)
    {
        if (BaseCommands.getLteOnCdmaModeStatic() == 1);
        while (true)
        {
            return;
            String str1 = defaultLanguageForMcc(paramInt);
            String str2 = countryCodeForMcc(paramInt);
            Log.d("MccTable", "locale set to " + str1 + "_" + str2);
            setSystemLocale(paramContext, str1, str2);
        }
    }

    public static void setSystemLocale(Context paramContext, String paramString1, String paramString2)
    {
        String str1 = SystemProperties.get("persist.sys.language");
        String str2 = SystemProperties.get("persist.sys.country");
        if (paramString1 == null);
        while (true)
        {
            return;
            String str3 = paramString1.toLowerCase();
            if (paramString2 == null)
                paramString2 = "";
            String str4 = paramString2.toUpperCase();
            if (((str1 == null) || (str1.length() == 0)) && ((str2 == null) || (str2.length() == 0)))
                try
                {
                    String[] arrayOfString = paramContext.getAssets().getLocales();
                    int i = arrayOfString.length;
                    String str5 = null;
                    for (int j = 0; ; j++)
                    {
                        if (j < i)
                        {
                            if ((arrayOfString[j] == null) || (arrayOfString[j].length() < 5) || (!arrayOfString[j].substring(0, 2).equals(str3)))
                                continue;
                            if (arrayOfString[j].substring(3, 5).equals(str4))
                                str5 = arrayOfString[j];
                        }
                        else
                        {
                            if (str5 == null)
                                break;
                            IActivityManager localIActivityManager = ActivityManagerNative.getDefault();
                            Configuration localConfiguration = localIActivityManager.getConfiguration();
                            localConfiguration.locale = new Locale(str5.substring(0, 2), str5.substring(3, 5));
                            localConfiguration.userSetLocale = true;
                            localIActivityManager.updateConfiguration(localConfiguration);
                            break;
                        }
                        if (str5 == null)
                            str5 = arrayOfString[j];
                    }
                }
                catch (Exception localException)
                {
                }
        }
    }

    private static void setTimezoneFromMccIfNeeded(Context paramContext, int paramInt)
    {
        String str1 = SystemProperties.get("persist.sys.timezone");
        if ((str1 == null) || (str1.length() == 0))
        {
            String str2 = defaultTimeZoneForMcc(paramInt);
            if ((str2 != null) && (str2.length() > 0))
            {
                ((AlarmManager)paramContext.getSystemService("alarm")).setTimeZone(str2);
                Log.d("MccTable", "timezone set to " + str2);
            }
        }
    }

    private static void setWifiCountryCodeFromMcc(Context paramContext, int paramInt)
    {
        String str = countryCodeForMcc(paramInt);
        if (!str.isEmpty())
        {
            Log.d("MccTable", "WIFI_COUNTRY_CODE set to " + str);
            ((WifiManager)paramContext.getSystemService("wifi")).setCountryCode(str, true);
        }
    }

    public static int smallestDigitsMccForMnc(int paramInt)
    {
        MccEntry localMccEntry = entryForMcc(paramInt);
        if (localMccEntry == null);
        for (int i = 2; ; i = localMccEntry.smallestDigitsMnc)
            return i;
    }

    public static void updateMccMncConfiguration(Context paramContext, String paramString)
    {
        if (!TextUtils.isEmpty(paramString));
        try
        {
            i = Integer.parseInt(paramString.substring(0, 3));
            j = Integer.parseInt(paramString.substring(3));
            Log.d("MccTable", "updateMccMncConfiguration: mcc=" + i + ", mnc=" + j);
            if (i != 0)
            {
                setTimezoneFromMccIfNeeded(paramContext, i);
                setLocaleFromMccIfNeeded(paramContext, i);
                setWifiCountryCodeFromMcc(paramContext, i);
            }
        }
        catch (NumberFormatException localNumberFormatException)
        {
            try
            {
                int i;
                int j;
                Configuration localConfiguration = ActivityManagerNative.getDefault().getConfiguration();
                if (i != 0)
                    localConfiguration.mcc = i;
                if (j != 0)
                    localConfiguration.mnc = j;
                ActivityManagerNative.getDefault().updateConfiguration(localConfiguration);
                while (true)
                {
                    return;
                    localNumberFormatException = localNumberFormatException;
                    Log.e("MccTable", "Error parsing IMSI");
                }
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    Log.e("MccTable", "Can't update configuration", localRemoteException);
            }
        }
    }

    static class MccEntry
        implements Comparable<MccEntry>
    {
        String iso;
        String language;
        int mcc;
        int smallestDigitsMnc;

        MccEntry(int paramInt1, String paramString, int paramInt2)
        {
            this(paramInt1, paramString, paramInt2, null);
        }

        MccEntry(int paramInt1, String paramString1, int paramInt2, String paramString2)
        {
            this.mcc = paramInt1;
            this.iso = paramString1;
            this.smallestDigitsMnc = paramInt2;
            this.language = paramString2;
        }

        public int compareTo(MccEntry paramMccEntry)
        {
            return this.mcc - paramMccEntry.mcc;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.MccTable
 * JD-Core Version:        0.6.2
 */