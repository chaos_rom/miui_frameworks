package com.android.internal.telephony;

import android.os.AsyncResult;
import android.os.Message;
import android.os.Parcel;
import android.util.Log;

class RILRequest
{
    static final String LOG_TAG = "RILJ";
    private static final int MAX_POOL_SIZE = 4;
    static int sNextSerial = 0;
    private static RILRequest sPool = null;
    private static int sPoolSize = 0;
    private static Object sPoolSync;
    static Object sSerialMonitor = new Object();
    RILRequest mNext;
    int mRequest;
    Message mResult;
    int mSerial;
    Parcel mp;

    static
    {
        sPoolSync = new Object();
    }

    static RILRequest obtain(int paramInt, Message paramMessage)
    {
        RILRequest localRILRequest = null;
        synchronized (sPoolSync)
        {
            if (sPool != null)
            {
                localRILRequest = sPool;
                sPool = localRILRequest.mNext;
                localRILRequest.mNext = null;
                sPoolSize = -1 + sPoolSize;
            }
            if (localRILRequest == null)
                localRILRequest = new RILRequest();
        }
        synchronized (sSerialMonitor)
        {
            int i = sNextSerial;
            sNextSerial = i + 1;
            localRILRequest.mSerial = i;
            localRILRequest.mRequest = paramInt;
            localRILRequest.mResult = paramMessage;
            localRILRequest.mp = Parcel.obtain();
            if ((paramMessage != null) && (paramMessage.getTarget() == null))
            {
                throw new NullPointerException("Message target must not be null");
                localObject2 = finally;
                throw localObject2;
            }
        }
        localRILRequest.mp.writeInt(paramInt);
        localRILRequest.mp.writeInt(localRILRequest.mSerial);
        return localRILRequest;
    }

    static void resetSerial()
    {
        synchronized (sSerialMonitor)
        {
            sNextSerial = 0;
            return;
        }
    }

    void onError(int paramInt, Object paramObject)
    {
        CommandException localCommandException = CommandException.fromRilErrno(paramInt);
        Log.d("RILJ", serialString() + "< " + RIL.requestToString(this.mRequest) + " error: " + localCommandException);
        if (this.mResult != null)
        {
            AsyncResult.forMessage(this.mResult, paramObject, localCommandException);
            this.mResult.sendToTarget();
        }
        if (this.mp != null)
        {
            this.mp.recycle();
            this.mp = null;
        }
    }

    void release()
    {
        synchronized (sPoolSync)
        {
            if (sPoolSize < 4)
            {
                this.mNext = sPool;
                sPool = this;
                sPoolSize = 1 + sPoolSize;
                this.mResult = null;
            }
            return;
        }
    }

    String serialString()
    {
        StringBuilder localStringBuilder = new StringBuilder(8);
        String str = Integer.toString(this.mSerial);
        localStringBuilder.append('[');
        int i = 0;
        int j = str.length();
        while (i < 4 - j)
        {
            localStringBuilder.append('0');
            i++;
        }
        localStringBuilder.append(str);
        localStringBuilder.append(']');
        return localStringBuilder.toString();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.RILRequest
 * JD-Core Version:        0.6.2
 */