package com.android.internal.telephony;

import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface IWapPushManager extends IInterface
{
    public abstract boolean addPackage(String paramString1, String paramString2, String paramString3, String paramString4, int paramInt, boolean paramBoolean1, boolean paramBoolean2)
        throws RemoteException;

    public abstract boolean deletePackage(String paramString1, String paramString2, String paramString3, String paramString4)
        throws RemoteException;

    public abstract int processMessage(String paramString1, String paramString2, Intent paramIntent)
        throws RemoteException;

    public abstract boolean updatePackage(String paramString1, String paramString2, String paramString3, String paramString4, int paramInt, boolean paramBoolean1, boolean paramBoolean2)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IWapPushManager
    {
        private static final String DESCRIPTOR = "com.android.internal.telephony.IWapPushManager";
        static final int TRANSACTION_addPackage = 2;
        static final int TRANSACTION_deletePackage = 4;
        static final int TRANSACTION_processMessage = 1;
        static final int TRANSACTION_updatePackage = 3;

        public Stub()
        {
            attachInterface(this, "com.android.internal.telephony.IWapPushManager");
        }

        public static IWapPushManager asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("com.android.internal.telephony.IWapPushManager");
                if ((localIInterface != null) && ((localIInterface instanceof IWapPushManager)))
                    localObject = (IWapPushManager)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            int i = 0;
            int j = 1;
            switch (paramInt1)
            {
            default:
                j = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            }
            while (true)
            {
                return j;
                paramParcel2.writeString("com.android.internal.telephony.IWapPushManager");
                continue;
                paramParcel1.enforceInterface("com.android.internal.telephony.IWapPushManager");
                String str9 = paramParcel1.readString();
                String str10 = paramParcel1.readString();
                if (paramParcel1.readInt() != 0);
                for (Intent localIntent = (Intent)Intent.CREATOR.createFromParcel(paramParcel1); ; localIntent = null)
                {
                    int n = processMessage(str9, str10, localIntent);
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(n);
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.telephony.IWapPushManager");
                String str5 = paramParcel1.readString();
                String str6 = paramParcel1.readString();
                String str7 = paramParcel1.readString();
                String str8 = paramParcel1.readString();
                int m = paramParcel1.readInt();
                boolean bool5;
                if (paramParcel1.readInt() != 0)
                {
                    bool5 = j;
                    label196: if (paramParcel1.readInt() == 0)
                        break label255;
                }
                label255: for (boolean bool6 = j; ; bool6 = false)
                {
                    boolean bool7 = addPackage(str5, str6, str7, str8, m, bool5, bool6);
                    paramParcel2.writeNoException();
                    if (bool7)
                        i = j;
                    paramParcel2.writeInt(i);
                    break;
                    bool5 = false;
                    break label196;
                }
                paramParcel1.enforceInterface("com.android.internal.telephony.IWapPushManager");
                String str1 = paramParcel1.readString();
                String str2 = paramParcel1.readString();
                String str3 = paramParcel1.readString();
                String str4 = paramParcel1.readString();
                int k = paramParcel1.readInt();
                boolean bool2;
                if (paramParcel1.readInt() != 0)
                {
                    bool2 = j;
                    label308: if (paramParcel1.readInt() == 0)
                        break label367;
                }
                label367: for (boolean bool3 = j; ; bool3 = false)
                {
                    boolean bool4 = updatePackage(str1, str2, str3, str4, k, bool2, bool3);
                    paramParcel2.writeNoException();
                    if (bool4)
                        i = j;
                    paramParcel2.writeInt(i);
                    break;
                    bool2 = false;
                    break label308;
                }
                paramParcel1.enforceInterface("com.android.internal.telephony.IWapPushManager");
                boolean bool1 = deletePackage(paramParcel1.readString(), paramParcel1.readString(), paramParcel1.readString(), paramParcel1.readString());
                paramParcel2.writeNoException();
                if (bool1)
                    i = j;
                paramParcel2.writeInt(i);
            }
        }

        private static class Proxy
            implements IWapPushManager
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public boolean addPackage(String paramString1, String paramString2, String paramString3, String paramString4, int paramInt, boolean paramBoolean1, boolean paramBoolean2)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.telephony.IWapPushManager");
                    localParcel1.writeString(paramString1);
                    localParcel1.writeString(paramString2);
                    localParcel1.writeString(paramString3);
                    localParcel1.writeString(paramString4);
                    localParcel1.writeInt(paramInt);
                    if (paramBoolean1)
                    {
                        int j = i;
                        localParcel1.writeInt(j);
                        if (!paramBoolean2)
                            break label136;
                    }
                    label136: int n;
                    for (int m = i; ; n = 0)
                    {
                        localParcel1.writeInt(m);
                        this.mRemote.transact(2, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int i1 = localParcel2.readInt();
                        if (i1 == 0)
                            break label142;
                        return i;
                        int k = 0;
                        break;
                    }
                    label142: i = 0;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public boolean deletePackage(String paramString1, String paramString2, String paramString3, String paramString4)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.telephony.IWapPushManager");
                    localParcel1.writeString(paramString1);
                    localParcel1.writeString(paramString2);
                    localParcel1.writeString(paramString3);
                    localParcel1.writeString(paramString4);
                    this.mRemote.transact(4, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "com.android.internal.telephony.IWapPushManager";
            }

            public int processMessage(String paramString1, String paramString2, Intent paramIntent)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.telephony.IWapPushManager");
                    localParcel1.writeString(paramString1);
                    localParcel1.writeString(paramString2);
                    if (paramIntent != null)
                    {
                        localParcel1.writeInt(1);
                        paramIntent.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(1, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int i = localParcel2.readInt();
                        return i;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean updatePackage(String paramString1, String paramString2, String paramString3, String paramString4, int paramInt, boolean paramBoolean1, boolean paramBoolean2)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.telephony.IWapPushManager");
                    localParcel1.writeString(paramString1);
                    localParcel1.writeString(paramString2);
                    localParcel1.writeString(paramString3);
                    localParcel1.writeString(paramString4);
                    localParcel1.writeInt(paramInt);
                    if (paramBoolean1)
                    {
                        int j = i;
                        localParcel1.writeInt(j);
                        if (!paramBoolean2)
                            break label136;
                    }
                    label136: int n;
                    for (int m = i; ; n = 0)
                    {
                        localParcel1.writeInt(m);
                        this.mRemote.transact(3, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int i1 = localParcel2.readInt();
                        if (i1 == 0)
                            break label142;
                        return i;
                        int k = 0;
                        break;
                    }
                    label142: i = 0;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.IWapPushManager
 * JD-Core Version:        0.6.2
 */