package com.android.internal.telephony.ims;

import android.os.AsyncResult;
import android.os.Handler;
import android.util.Log;
import com.android.internal.telephony.IccFileHandler;
import com.android.internal.telephony.IccRecords.IccRecordLoaded;
import com.android.internal.telephony.gsm.SimTlv;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;

public final class IsimUiccRecords
    implements IsimRecords
{
    private static final boolean DBG = true;
    private static final boolean DUMP_RECORDS = false;
    protected static final String LOG_TAG = "GSM";
    private static final int TAG_ISIM_VALUE = 128;
    private String mIsimDomain;
    private String mIsimImpi;
    private String[] mIsimImpu;

    private static String isimTlvToString(byte[] paramArrayOfByte)
    {
        SimTlv localSimTlv = new SimTlv(paramArrayOfByte, 0, paramArrayOfByte.length);
        if (localSimTlv.getTag() == 128);
        for (String str = new String(localSimTlv.getData(), Charset.forName("UTF-8")); ; str = null)
        {
            return str;
            if (localSimTlv.nextObject())
                break;
            Log.e("GSM", "[ISIM] can't find TLV tag in ISIM record, returning null");
        }
    }

    public int fetchIsimRecords(IccFileHandler paramIccFileHandler, Handler paramHandler)
    {
        paramIccFileHandler.loadEFTransparent(28418, paramHandler.obtainMessage(100, new EfIsimImpiLoaded(null)));
        paramIccFileHandler.loadEFLinearFixedAll(28420, paramHandler.obtainMessage(100, new EfIsimImpuLoaded(null)));
        paramIccFileHandler.loadEFTransparent(28419, paramHandler.obtainMessage(100, new EfIsimDomainLoaded(null)));
        return 3;
    }

    public String getIsimDomain()
    {
        return this.mIsimDomain;
    }

    public String getIsimImpi()
    {
        return this.mIsimImpi;
    }

    public String[] getIsimImpu()
    {
        if (this.mIsimImpu != null);
        for (String[] arrayOfString = (String[])this.mIsimImpu.clone(); ; arrayOfString = null)
            return arrayOfString;
    }

    void log(String paramString)
    {
        Log.d("GSM", "[ISIM] " + paramString);
    }

    void loge(String paramString)
    {
        Log.e("GSM", "[ISIM] " + paramString);
    }

    private class EfIsimDomainLoaded
        implements IccRecords.IccRecordLoaded
    {
        private EfIsimDomainLoaded()
        {
        }

        public String getEfName()
        {
            return "EF_ISIM_DOMAIN";
        }

        public void onRecordLoaded(AsyncResult paramAsyncResult)
        {
            byte[] arrayOfByte = (byte[])paramAsyncResult.result;
            IsimUiccRecords.access$302(IsimUiccRecords.this, IsimUiccRecords.isimTlvToString(arrayOfByte));
        }
    }

    private class EfIsimImpuLoaded
        implements IccRecords.IccRecordLoaded
    {
        private EfIsimImpuLoaded()
        {
        }

        public String getEfName()
        {
            return "EF_ISIM_IMPU";
        }

        public void onRecordLoaded(AsyncResult paramAsyncResult)
        {
            ArrayList localArrayList = (ArrayList)paramAsyncResult.result;
            IsimUiccRecords.this.log("EF_IMPU record count: " + localArrayList.size());
            IsimUiccRecords.access$202(IsimUiccRecords.this, new String[localArrayList.size()]);
            int i = 0;
            Iterator localIterator = localArrayList.iterator();
            while (localIterator.hasNext())
            {
                String str = IsimUiccRecords.isimTlvToString((byte[])localIterator.next());
                String[] arrayOfString = IsimUiccRecords.this.mIsimImpu;
                int j = i + 1;
                arrayOfString[i] = str;
                i = j;
            }
        }
    }

    private class EfIsimImpiLoaded
        implements IccRecords.IccRecordLoaded
    {
        private EfIsimImpiLoaded()
        {
        }

        public String getEfName()
        {
            return "EF_ISIM_IMPI";
        }

        public void onRecordLoaded(AsyncResult paramAsyncResult)
        {
            byte[] arrayOfByte = (byte[])paramAsyncResult.result;
            IsimUiccRecords.access$002(IsimUiccRecords.this, IsimUiccRecords.isimTlvToString(arrayOfByte));
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.ims.IsimUiccRecords
 * JD-Core Version:        0.6.2
 */