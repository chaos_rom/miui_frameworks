package com.android.internal.telephony.ims;

public abstract interface IsimRecords
{
    public abstract String getIsimDomain();

    public abstract String getIsimImpi();

    public abstract String[] getIsimImpu();
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.ims.IsimRecords
 * JD-Core Version:        0.6.2
 */