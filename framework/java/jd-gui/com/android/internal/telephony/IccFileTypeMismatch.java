package com.android.internal.telephony;

public class IccFileTypeMismatch extends IccException
{
    public IccFileTypeMismatch()
    {
    }

    public IccFileTypeMismatch(String paramString)
    {
        super(paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.IccFileTypeMismatch
 * JD-Core Version:        0.6.2
 */