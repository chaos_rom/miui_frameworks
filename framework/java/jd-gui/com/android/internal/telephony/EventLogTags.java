package com.android.internal.telephony;

import android.util.EventLog;

public class EventLogTags
{
    public static final int BAD_IP_ADDRESS = 50117;
    public static final int CALL_DROP = 50106;
    public static final int CDMA_DATA_DROP = 50111;
    public static final int CDMA_DATA_SETUP_FAILED = 50110;
    public static final int CDMA_DATA_STATE_CHANGE = 50115;
    public static final int CDMA_SERVICE_STATE_CHANGE = 50116;
    public static final int DATA_NETWORK_REGISTRATION_FAIL = 50107;
    public static final int DATA_NETWORK_STATUS_ON_RADIO_OFF = 50108;
    public static final int DATA_STALL_RECOVERY_CLEANUP = 50119;
    public static final int DATA_STALL_RECOVERY_GET_DATA_CALL_LIST = 50118;
    public static final int DATA_STALL_RECOVERY_RADIO_RESTART = 50121;
    public static final int DATA_STALL_RECOVERY_RADIO_RESTART_WITH_PROP = 50122;
    public static final int DATA_STALL_RECOVERY_REREGISTER = 50120;
    public static final int GSM_DATA_STATE_CHANGE = 50113;
    public static final int GSM_RAT_SWITCHED = 50112;
    public static final int GSM_SERVICE_STATE_CHANGE = 50114;
    public static final int PDP_BAD_DNS_ADDRESS = 50100;
    public static final int PDP_CONTEXT_RESET = 50103;
    public static final int PDP_NETWORK_DROP = 50109;
    public static final int PDP_RADIO_RESET = 50102;
    public static final int PDP_RADIO_RESET_COUNTDOWN_TRIGGERED = 50101;
    public static final int PDP_REREGISTER_NETWORK = 50104;
    public static final int PDP_SETUP_FAIL = 50105;

    public static void writeBadIpAddress(String paramString)
    {
        EventLog.writeEvent(50117, paramString);
    }

    public static void writeCallDrop(int paramInt1, int paramInt2, int paramInt3)
    {
        Object[] arrayOfObject = new Object[3];
        arrayOfObject[0] = Integer.valueOf(paramInt1);
        arrayOfObject[1] = Integer.valueOf(paramInt2);
        arrayOfObject[2] = Integer.valueOf(paramInt3);
        EventLog.writeEvent(50106, arrayOfObject);
    }

    public static void writeCdmaDataDrop(int paramInt1, int paramInt2)
    {
        Object[] arrayOfObject = new Object[2];
        arrayOfObject[0] = Integer.valueOf(paramInt1);
        arrayOfObject[1] = Integer.valueOf(paramInt2);
        EventLog.writeEvent(50111, arrayOfObject);
    }

    public static void writeCdmaDataSetupFailed(int paramInt1, int paramInt2, int paramInt3)
    {
        Object[] arrayOfObject = new Object[3];
        arrayOfObject[0] = Integer.valueOf(paramInt1);
        arrayOfObject[1] = Integer.valueOf(paramInt2);
        arrayOfObject[2] = Integer.valueOf(paramInt3);
        EventLog.writeEvent(50110, arrayOfObject);
    }

    public static void writeCdmaDataStateChange(String paramString1, String paramString2)
    {
        Object[] arrayOfObject = new Object[2];
        arrayOfObject[0] = paramString1;
        arrayOfObject[1] = paramString2;
        EventLog.writeEvent(50115, arrayOfObject);
    }

    public static void writeCdmaServiceStateChange(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        Object[] arrayOfObject = new Object[4];
        arrayOfObject[0] = Integer.valueOf(paramInt1);
        arrayOfObject[1] = Integer.valueOf(paramInt2);
        arrayOfObject[2] = Integer.valueOf(paramInt3);
        arrayOfObject[3] = Integer.valueOf(paramInt4);
        EventLog.writeEvent(50116, arrayOfObject);
    }

    public static void writeDataNetworkRegistrationFail(int paramInt1, int paramInt2)
    {
        Object[] arrayOfObject = new Object[2];
        arrayOfObject[0] = Integer.valueOf(paramInt1);
        arrayOfObject[1] = Integer.valueOf(paramInt2);
        EventLog.writeEvent(50107, arrayOfObject);
    }

    public static void writeDataNetworkStatusOnRadioOff(String paramString, int paramInt)
    {
        Object[] arrayOfObject = new Object[2];
        arrayOfObject[0] = paramString;
        arrayOfObject[1] = Integer.valueOf(paramInt);
        EventLog.writeEvent(50108, arrayOfObject);
    }

    public static void writeDataStallRecoveryCleanup(int paramInt)
    {
        EventLog.writeEvent(50119, paramInt);
    }

    public static void writeDataStallRecoveryGetDataCallList(int paramInt)
    {
        EventLog.writeEvent(50118, paramInt);
    }

    public static void writeDataStallRecoveryRadioRestart(int paramInt)
    {
        EventLog.writeEvent(50121, paramInt);
    }

    public static void writeDataStallRecoveryRadioRestartWithProp(int paramInt)
    {
        EventLog.writeEvent(50122, paramInt);
    }

    public static void writeDataStallRecoveryReregister(int paramInt)
    {
        EventLog.writeEvent(50120, paramInt);
    }

    public static void writeGsmDataStateChange(String paramString1, String paramString2)
    {
        Object[] arrayOfObject = new Object[2];
        arrayOfObject[0] = paramString1;
        arrayOfObject[1] = paramString2;
        EventLog.writeEvent(50113, arrayOfObject);
    }

    public static void writeGsmRatSwitched(int paramInt1, int paramInt2, int paramInt3)
    {
        Object[] arrayOfObject = new Object[3];
        arrayOfObject[0] = Integer.valueOf(paramInt1);
        arrayOfObject[1] = Integer.valueOf(paramInt2);
        arrayOfObject[2] = Integer.valueOf(paramInt3);
        EventLog.writeEvent(50112, arrayOfObject);
    }

    public static void writeGsmServiceStateChange(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        Object[] arrayOfObject = new Object[4];
        arrayOfObject[0] = Integer.valueOf(paramInt1);
        arrayOfObject[1] = Integer.valueOf(paramInt2);
        arrayOfObject[2] = Integer.valueOf(paramInt3);
        arrayOfObject[3] = Integer.valueOf(paramInt4);
        EventLog.writeEvent(50114, arrayOfObject);
    }

    public static void writePdpBadDnsAddress(String paramString)
    {
        EventLog.writeEvent(50100, paramString);
    }

    public static void writePdpContextReset(int paramInt)
    {
        EventLog.writeEvent(50103, paramInt);
    }

    public static void writePdpNetworkDrop(int paramInt1, int paramInt2)
    {
        Object[] arrayOfObject = new Object[2];
        arrayOfObject[0] = Integer.valueOf(paramInt1);
        arrayOfObject[1] = Integer.valueOf(paramInt2);
        EventLog.writeEvent(50109, arrayOfObject);
    }

    public static void writePdpRadioReset(int paramInt)
    {
        EventLog.writeEvent(50102, paramInt);
    }

    public static void writePdpRadioResetCountdownTriggered(int paramInt)
    {
        EventLog.writeEvent(50101, paramInt);
    }

    public static void writePdpReregisterNetwork(int paramInt)
    {
        EventLog.writeEvent(50104, paramInt);
    }

    public static void writePdpSetupFail(int paramInt1, int paramInt2, int paramInt3)
    {
        Object[] arrayOfObject = new Object[3];
        arrayOfObject[0] = Integer.valueOf(paramInt1);
        arrayOfObject[1] = Integer.valueOf(paramInt2);
        arrayOfObject[2] = Integer.valueOf(paramInt3);
        EventLog.writeEvent(50105, arrayOfObject);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.EventLogTags
 * JD-Core Version:        0.6.2
 */