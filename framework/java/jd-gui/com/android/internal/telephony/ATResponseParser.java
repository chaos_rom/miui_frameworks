package com.android.internal.telephony;

public class ATResponseParser
{
    private String line;
    private int next = 0;
    private int tokEnd;
    private int tokStart;

    public ATResponseParser(String paramString)
    {
        this.line = paramString;
    }

    private void nextTok()
    {
        int i = this.line.length();
        if (this.next == 0)
            skipPrefix();
        if (this.next >= i)
            throw new ATParseEx();
        char c1;
        try
        {
            String str1 = this.line;
            int j = this.next;
            this.next = (j + 1);
            c1 = skipWhiteSpace(str1.charAt(j));
            if (c1 != '"')
                break label259;
            if (this.next >= i)
                throw new ATParseEx();
        }
        catch (StringIndexOutOfBoundsException localStringIndexOutOfBoundsException)
        {
            throw new ATParseEx();
        }
        String str3 = this.line;
        int m = this.next;
        this.next = (m + 1);
        int n = str3.charAt(m);
        this.tokStart = (-1 + this.next);
        while ((n != 34) && (this.next < i))
        {
            String str5 = this.line;
            int i2 = this.next;
            this.next = (i2 + 1);
            n = str5.charAt(i2);
        }
        if (n != 34)
            throw new ATParseEx();
        this.tokEnd = (-1 + this.next);
        if (this.next < i)
        {
            String str4 = this.line;
            int i1 = this.next;
            this.next = (i1 + 1);
            if (str4.charAt(i1) != ',')
            {
                throw new ATParseEx();
                label259: this.tokStart = (-1 + this.next);
                this.tokEnd = this.tokStart;
                while (c1 != ',')
                {
                    if (!Character.isWhitespace(c1))
                        this.tokEnd = this.next;
                    if (this.next == i)
                        break;
                    String str2 = this.line;
                    int k = this.next;
                    this.next = (k + 1);
                    char c2 = str2.charAt(k);
                    c1 = c2;
                }
            }
        }
    }

    private void skipPrefix()
    {
        this.next = 0;
        int i = this.line.length();
        while (this.next < i)
        {
            String str = this.line;
            int j = this.next;
            this.next = (j + 1);
            if (str.charAt(j) == ':')
                return;
        }
        throw new ATParseEx("missing prefix");
    }

    private char skipWhiteSpace(char paramChar)
    {
        int i = this.line.length();
        while ((this.next < i) && (Character.isWhitespace(paramChar)))
        {
            String str = this.line;
            int j = this.next;
            this.next = (j + 1);
            paramChar = str.charAt(j);
        }
        if (Character.isWhitespace(paramChar))
            throw new ATParseEx();
        return paramChar;
    }

    public boolean hasMore()
    {
        if (this.next < this.line.length());
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean nextBoolean()
    {
        int i = 1;
        nextTok();
        if (this.tokEnd - this.tokStart > i)
            throw new ATParseEx();
        int j = this.line.charAt(this.tokStart);
        if (j == 48)
            i = 0;
        while (j == 49)
            return i;
        throw new ATParseEx();
    }

    public int nextInt()
    {
        int i = 0;
        nextTok();
        for (int j = this.tokStart; j < this.tokEnd; j++)
        {
            int k = this.line.charAt(j);
            if ((k < 48) || (k > 57))
                throw new ATParseEx();
            i = i * 10 + (k + -48);
        }
        return i;
    }

    public String nextString()
    {
        nextTok();
        return this.line.substring(this.tokStart, this.tokEnd);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.ATResponseParser
 * JD-Core Version:        0.6.2
 */