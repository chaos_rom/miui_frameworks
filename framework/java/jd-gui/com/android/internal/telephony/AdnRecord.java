package com.android.internal.telephony;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.util.Log;
import java.util.Arrays;

public class AdnRecord
    implements Parcelable
{
    static final int ADN_BCD_NUMBER_LENGTH = 0;
    static final int ADN_CAPABILITY_ID = 12;
    static final int ADN_DIALING_NUMBER_END = 11;
    static final int ADN_DIALING_NUMBER_START = 2;
    static final int ADN_EXTENSION_ID = 13;
    static final int ADN_TON_AND_NPI = 1;
    public static final Parcelable.Creator<AdnRecord> CREATOR = new Parcelable.Creator()
    {
        public AdnRecord createFromParcel(Parcel paramAnonymousParcel)
        {
            return new AdnRecord(paramAnonymousParcel.readInt(), paramAnonymousParcel.readInt(), paramAnonymousParcel.readString(), paramAnonymousParcel.readString(), paramAnonymousParcel.readStringArray());
        }

        public AdnRecord[] newArray(int paramAnonymousInt)
        {
            return new AdnRecord[paramAnonymousInt];
        }
    };
    static final int EXT_RECORD_LENGTH_BYTES = 13;
    static final int EXT_RECORD_TYPE_ADDITIONAL_DATA = 2;
    static final int EXT_RECORD_TYPE_MASK = 3;
    static final int FOOTER_SIZE_BYTES = 14;
    static final String LOG_TAG = "GSM";
    static final int MAX_EXT_CALLED_PARTY_LENGTH = 10;
    static final int MAX_NUMBER_SIZE_BYTES = 255;
    String alphaTag = null;
    int efid;
    String[] emails;
    int extRecord = 255;
    String number = null;
    int recordNumber;

    public AdnRecord(int paramInt1, int paramInt2, String paramString1, String paramString2)
    {
        this.efid = paramInt1;
        this.recordNumber = paramInt2;
        this.alphaTag = paramString1;
        this.number = paramString2;
        this.emails = null;
    }

    public AdnRecord(int paramInt1, int paramInt2, String paramString1, String paramString2, String[] paramArrayOfString)
    {
        this.efid = paramInt1;
        this.recordNumber = paramInt2;
        this.alphaTag = paramString1;
        this.number = paramString2;
        this.emails = paramArrayOfString;
    }

    public AdnRecord(int paramInt1, int paramInt2, byte[] paramArrayOfByte)
    {
        this.efid = paramInt1;
        this.recordNumber = paramInt2;
        parseRecord(paramArrayOfByte);
    }

    public AdnRecord(String paramString1, String paramString2)
    {
        this(0, 0, paramString1, paramString2);
    }

    public AdnRecord(String paramString1, String paramString2, String[] paramArrayOfString)
    {
        this(0, 0, paramString1, paramString2, paramArrayOfString);
    }

    public AdnRecord(byte[] paramArrayOfByte)
    {
        this(0, 0, paramArrayOfByte);
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    private void parseRecord(byte[] paramArrayOfByte)
    {
        try
        {
            this.alphaTag = IccUtils.adnStringFieldToString(paramArrayOfByte, 0, -14 + paramArrayOfByte.length);
            int i = -14 + paramArrayOfByte.length;
            int j = 0xFF & paramArrayOfByte[i];
            if (j > 255)
            {
                this.number = "";
            }
            else
            {
                this.number = PhoneNumberUtils.calledPartyBCDToString(paramArrayOfByte, i + 1, j);
                this.extRecord = (0xFF & paramArrayOfByte[(-1 + paramArrayOfByte.length)]);
                this.emails = null;
            }
        }
        catch (RuntimeException localRuntimeException)
        {
            Log.w("GSM", Injector.adnRecordError(this, "Error parsing AdnRecord"), localRuntimeException);
            this.number = "";
            this.alphaTag = "";
            this.emails = null;
        }
    }

    private static boolean stringCompareNullEqualsEmpty(String paramString1, String paramString2)
    {
        if (paramString1 == paramString2);
        for (boolean bool = true; ; bool = paramString1.equals(paramString2))
        {
            return bool;
            if (paramString1 == null)
                paramString1 = "";
            if (paramString2 == null)
                paramString2 = "";
        }
    }

    public void appendExtRecord(byte[] paramArrayOfByte)
    {
        try
        {
            if ((paramArrayOfByte.length == 13) && ((0x3 & paramArrayOfByte[0]) == 2) && ((0xFF & paramArrayOfByte[1]) <= 10))
                this.number += PhoneNumberUtils.calledPartyBCDFragmentToString(paramArrayOfByte, 2, 0xFF & paramArrayOfByte[1]);
        }
        catch (RuntimeException localRuntimeException)
        {
            Log.w("GSM", "Error parsing AdnRecord ext record", localRuntimeException);
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public byte[] buildAdnString(int paramInt)
    {
        int i = paramInt - 14;
        byte[] arrayOfByte1 = new byte[paramInt];
        for (int j = 0; j < paramInt; j++)
            arrayOfByte1[j] = -1;
        if (TextUtils.isEmpty(this.number))
            Log.w("GSM", "[buildAdnString] Empty dialing number");
        while (true)
        {
            return arrayOfByte1;
            if (this.number.length() > 20)
            {
                Log.w("GSM", "[buildAdnString] Max length of dialing number is 20");
                arrayOfByte1 = null;
            }
            else if ((this.alphaTag != null) && (this.alphaTag.length() > i))
            {
                Log.w("GSM", "[buildAdnString] Max length of tag is " + i);
                arrayOfByte1 = null;
            }
            else
            {
                byte[] arrayOfByte2 = PhoneNumberUtils.numberToCalledPartyBCD(this.number);
                System.arraycopy(arrayOfByte2, 0, arrayOfByte1, i + 1, arrayOfByte2.length);
                arrayOfByte1[(i + 0)] = ((byte)arrayOfByte2.length);
                arrayOfByte1[(i + 12)] = -1;
                arrayOfByte1[(i + 13)] = -1;
                if (!TextUtils.isEmpty(this.alphaTag))
                {
                    byte[] arrayOfByte3 = GsmAlphabet.stringToGsm8BitPacked(this.alphaTag);
                    Injector.arraycopy(arrayOfByte3, 0, arrayOfByte1, 0, arrayOfByte3.length);
                    MiuiAdnUtils.encodeAlphaTag(arrayOfByte1, this.alphaTag, i);
                }
            }
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public String getAlphaTag()
    {
        return this.alphaTag;
    }

    public String[] getEmails()
    {
        return this.emails;
    }

    public String getNumber()
    {
        return this.number;
    }

    public boolean hasExtendedRecord()
    {
        if ((this.extRecord != 0) && (this.extRecord != 255));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isEmpty()
    {
        if ((TextUtils.isEmpty(this.alphaTag)) && (TextUtils.isEmpty(this.number)) && (this.emails == null));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isEqual(AdnRecord paramAdnRecord)
    {
        if ((stringCompareNullEqualsEmpty(this.alphaTag, paramAdnRecord.alphaTag)) && (stringCompareNullEqualsEmpty(this.number, paramAdnRecord.number)) && (Arrays.equals(this.emails, paramAdnRecord.emails)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void setEmails(String[] paramArrayOfString)
    {
        this.emails = paramArrayOfString;
    }

    public String toString()
    {
        return "ADN Record '" + this.alphaTag + "' '" + this.number + " " + this.emails + "'";
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeInt(this.efid);
        paramParcel.writeInt(this.recordNumber);
        paramParcel.writeString(this.alphaTag);
        paramParcel.writeString(this.number);
        paramParcel.writeStringArray(this.emails);
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        static String adnRecordError(AdnRecord paramAdnRecord, String paramString)
        {
            return paramString + " - [" + paramAdnRecord.number + "," + paramAdnRecord.alphaTag + "," + paramAdnRecord.emails + "]";
        }

        static void arraycopy(Object paramObject1, int paramInt1, Object paramObject2, int paramInt2, int paramInt3)
        {
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.AdnRecord
 * JD-Core Version:        0.6.2
 */