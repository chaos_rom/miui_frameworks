package com.android.internal.telephony;

import android.content.Context;
import android.os.Binder;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import com.android.internal.telephony.ims.IsimRecords;
import java.io.FileDescriptor;
import java.io.PrintWriter;

public class PhoneSubInfo extends IPhoneSubInfo.Stub
{
    private static final String CALL_PRIVILEGED = "android.permission.CALL_PRIVILEGED";
    static final String LOG_TAG = "PHONE";
    private static final String READ_PHONE_STATE = "android.permission.READ_PHONE_STATE";
    private static final String READ_PRIVILEGED_PHONE_STATE = "android.permission.READ_PRIVILEGED_PHONE_STATE";
    private Context mContext;
    private Phone mPhone;

    public PhoneSubInfo(Phone paramPhone)
    {
        this.mPhone = paramPhone;
        this.mContext = paramPhone.getContext();
    }

    public void dispose()
    {
    }

    protected void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        if (this.mContext.checkCallingOrSelfPermission("android.permission.DUMP") != 0)
            paramPrintWriter.println("Permission Denial: can't dump PhoneSubInfo from from pid=" + Binder.getCallingPid() + ", uid=" + Binder.getCallingUid());
        while (true)
        {
            return;
            paramPrintWriter.println("Phone Subscriber Info:");
            paramPrintWriter.println("    Phone Type = " + this.mPhone.getPhoneName());
            paramPrintWriter.println("    Device ID = " + this.mPhone.getDeviceId());
        }
    }

    protected void finalize()
    {
        try
        {
            super.finalize();
            Log.d("PHONE", "PhoneSubInfo finalized");
            return;
        }
        catch (Throwable localThrowable)
        {
            while (true)
                Log.e("PHONE", "Error while finalizing:", localThrowable);
        }
    }

    public String getCompleteVoiceMailNumber()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CALL_PRIVILEGED", "Requires CALL_PRIVILEGED");
        String str = this.mPhone.getVoiceMailNumber();
        Log.d("PHONE", "VM: PhoneSubInfo.getCompleteVoiceMailNUmber: ");
        return str;
    }

    public String getDeviceId()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.READ_PHONE_STATE", "Requires READ_PHONE_STATE");
        return this.mPhone.getDeviceId();
    }

    public String getDeviceSvn()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.READ_PHONE_STATE", "Requires READ_PHONE_STATE");
        return this.mPhone.getDeviceSvn();
    }

    public String getIccSerialNumber()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.READ_PHONE_STATE", "Requires READ_PHONE_STATE");
        return this.mPhone.getIccSerialNumber();
    }

    public String getIsimDomain()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.READ_PRIVILEGED_PHONE_STATE", "Requires READ_PRIVILEGED_PHONE_STATE");
        IsimRecords localIsimRecords = this.mPhone.getIsimRecords();
        if (localIsimRecords != null);
        for (String str = localIsimRecords.getIsimDomain(); ; str = null)
            return str;
    }

    public String getIsimImpi()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.READ_PRIVILEGED_PHONE_STATE", "Requires READ_PRIVILEGED_PHONE_STATE");
        IsimRecords localIsimRecords = this.mPhone.getIsimRecords();
        if (localIsimRecords != null);
        for (String str = localIsimRecords.getIsimImpi(); ; str = null)
            return str;
    }

    public String[] getIsimImpu()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.READ_PRIVILEGED_PHONE_STATE", "Requires READ_PRIVILEGED_PHONE_STATE");
        IsimRecords localIsimRecords = this.mPhone.getIsimRecords();
        if (localIsimRecords != null);
        for (String[] arrayOfString = localIsimRecords.getIsimImpu(); ; arrayOfString = null)
            return arrayOfString;
    }

    public String getLine1AlphaTag()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.READ_PHONE_STATE", "Requires READ_PHONE_STATE");
        return this.mPhone.getLine1AlphaTag();
    }

    public String getLine1Number()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.READ_PHONE_STATE", "Requires READ_PHONE_STATE");
        return this.mPhone.getLine1Number();
    }

    public String getMsisdn()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.READ_PHONE_STATE", "Requires READ_PHONE_STATE");
        return this.mPhone.getMsisdn();
    }

    public String getSubscriberId()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.READ_PHONE_STATE", "Requires READ_PHONE_STATE");
        return this.mPhone.getSubscriberId();
    }

    public String getVoiceMailAlphaTag()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.READ_PHONE_STATE", "Requires READ_PHONE_STATE");
        return this.mPhone.getVoiceMailAlphaTag();
    }

    public String getVoiceMailNumber()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.READ_PHONE_STATE", "Requires READ_PHONE_STATE");
        String str = PhoneNumberUtils.extractNetworkPortion(this.mPhone.getVoiceMailNumber());
        Log.d("PHONE", "VM: PhoneSubInfo.getVoiceMailNUmber: ");
        return str;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.PhoneSubInfo
 * JD-Core Version:        0.6.2
 */