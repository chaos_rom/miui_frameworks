package com.android.internal.telephony;

import android.content.Context;
import android.net.LinkCapabilities;
import android.net.LinkProperties;
import android.os.Handler;
import android.os.Message;
import android.telephony.CellLocation;
import android.telephony.ServiceState;
import android.telephony.SignalStrength;
import com.android.internal.telephony.gsm.UsimServiceTable;
import com.android.internal.telephony.ims.IsimRecords;
import com.android.internal.telephony.test.SimulatedRadioControl;
import java.util.List;

public abstract interface Phone
{
    public static final int APN_ALREADY_ACTIVE = 0;
    public static final int APN_ALREADY_INACTIVE = 4;
    public static final int APN_REQUEST_FAILED = 3;
    public static final int APN_REQUEST_STARTED = 1;
    public static final String APN_TYPE_ALL = "*";
    public static final String APN_TYPE_CBS = "cbs";
    public static final String APN_TYPE_DEFAULT = "default";
    public static final String APN_TYPE_DUN = "dun";
    public static final String APN_TYPE_FOTA = "fota";
    public static final String APN_TYPE_HIPRI = "hipri";
    public static final String APN_TYPE_IMS = "ims";
    public static final String APN_TYPE_MMS = "mms";
    public static final int APN_TYPE_NOT_AVAILABLE = 2;
    public static final String APN_TYPE_SUPL = "supl";
    public static final int BM_AUS2_BAND = 5;
    public static final int BM_AUS_BAND = 4;
    public static final int BM_BOUNDARY = 6;
    public static final int BM_EURO_BAND = 1;
    public static final int BM_JPN_BAND = 3;
    public static final int BM_UNSPECIFIED = 0;
    public static final int BM_US_BAND = 2;
    public static final int CDMA_OTA_PROVISION_STATUS_A_KEY_EXCHANGED = 2;
    public static final int CDMA_OTA_PROVISION_STATUS_COMMITTED = 8;
    public static final int CDMA_OTA_PROVISION_STATUS_IMSI_DOWNLOADED = 6;
    public static final int CDMA_OTA_PROVISION_STATUS_MDN_DOWNLOADED = 5;
    public static final int CDMA_OTA_PROVISION_STATUS_NAM_DOWNLOADED = 4;
    public static final int CDMA_OTA_PROVISION_STATUS_OTAPA_ABORTED = 11;
    public static final int CDMA_OTA_PROVISION_STATUS_OTAPA_STARTED = 9;
    public static final int CDMA_OTA_PROVISION_STATUS_OTAPA_STOPPED = 10;
    public static final int CDMA_OTA_PROVISION_STATUS_PRL_DOWNLOADED = 7;
    public static final int CDMA_OTA_PROVISION_STATUS_SPC_RETRIES_EXCEEDED = 1;
    public static final int CDMA_OTA_PROVISION_STATUS_SPL_UNLOCKED = 0;
    public static final int CDMA_OTA_PROVISION_STATUS_SSD_UPDATED = 3;
    public static final int CDMA_RM_AFFILIATED = 1;
    public static final int CDMA_RM_ANY = 2;
    public static final int CDMA_RM_HOME = 0;
    public static final int CDMA_SUBSCRIPTION_NV = 1;
    public static final int CDMA_SUBSCRIPTION_RUIM_SIM = 0;
    public static final String DATA_APN_KEY = "apn";
    public static final String DATA_APN_TYPE_KEY = "apnType";
    public static final String DATA_IFACE_NAME_KEY = "iface";
    public static final String DATA_LINK_CAPABILITIES_KEY = "linkCapabilities";
    public static final String DATA_LINK_PROPERTIES_KEY = "linkProperties";
    public static final String DATA_NETWORK_ROAMING_KEY = "networkRoaming";
    public static final boolean DEBUG_PHONE = true;
    public static final String FAILURE_REASON_KEY = "reason";
    public static final String FEATURE_ENABLE_CBS = "enableCBS";
    public static final String FEATURE_ENABLE_DUN = "enableDUN";
    public static final String FEATURE_ENABLE_DUN_ALWAYS = "enableDUNAlways";
    public static final String FEATURE_ENABLE_FOTA = "enableFOTA";
    public static final String FEATURE_ENABLE_HIPRI = "enableHIPRI";
    public static final String FEATURE_ENABLE_IMS = "enableIMS";
    public static final String FEATURE_ENABLE_MMS = "enableMMS";
    public static final String FEATURE_ENABLE_SUPL = "enableSUPL";
    public static final int LTE_ON_CDMA_FALSE = 0;
    public static final int LTE_ON_CDMA_TRUE = 1;
    public static final int LTE_ON_CDMA_UNKNOWN = -1;
    public static final String NETWORK_UNAVAILABLE_KEY = "networkUnvailable";
    public static final int NT_MODE_CDMA = 4;
    public static final int NT_MODE_CDMA_NO_EVDO = 5;
    public static final int NT_MODE_EVDO_NO_CDMA = 6;
    public static final int NT_MODE_GLOBAL = 7;
    public static final int NT_MODE_GSM_ONLY = 1;
    public static final int NT_MODE_GSM_UMTS = 3;
    public static final int NT_MODE_LTE_ONLY = 11;
    public static final int NT_MODE_WCDMA_ONLY = 2;
    public static final int NT_MODE_WCDMA_PREF = 0;
    public static final String PHONE_IN_ECM_STATE = "phoneinECMState";
    public static final String PHONE_NAME_KEY = "phoneName";
    public static final int PHONE_TYPE_CDMA = 2;
    public static final int PHONE_TYPE_GSM = 1;
    public static final int PHONE_TYPE_NONE = 0;
    public static final int PHONE_TYPE_SIP = 3;
    public static final int PREFERRED_CDMA_SUBSCRIPTION = 1;
    public static final int PREFERRED_NT_MODE = 0;
    public static final String REASON_APN_CHANGED = "apnChanged";
    public static final String REASON_APN_FAILED = "apnFailed";
    public static final String REASON_APN_SWITCHED = "apnSwitched";
    public static final String REASON_CDMA_DATA_ATTACHED = "cdmaDataAttached";
    public static final String REASON_CDMA_DATA_DETACHED = "cdmaDataDetached";
    public static final String REASON_DATA_ATTACHED = "dataAttached";
    public static final String REASON_DATA_DEPENDENCY_MET = "dependencyMet";
    public static final String REASON_DATA_DEPENDENCY_UNMET = "dependencyUnmet";
    public static final String REASON_DATA_DETACHED = "dataDetached";
    public static final String REASON_DATA_DISABLED = "dataDisabled";
    public static final String REASON_DATA_ENABLED = "dataEnabled";
    public static final String REASON_LINK_PROPERTIES_CHANGED = "linkPropertiesChanged";
    public static final String REASON_NW_TYPE_CHANGED = "nwTypeChanged";
    public static final String REASON_PDP_RESET = "pdpReset";
    public static final String REASON_PS_RESTRICT_DISABLED = "psRestrictDisabled";
    public static final String REASON_PS_RESTRICT_ENABLED = "psRestrictEnabled";
    public static final String REASON_RADIO_TURNED_OFF = "radioTurnedOff";
    public static final String REASON_RESTORE_DEFAULT_APN = "restoreDefaultApn";
    public static final String REASON_ROAMING_OFF = "roamingOff";
    public static final String REASON_ROAMING_ON = "roamingOn";
    public static final String REASON_SIM_LOADED = "simLoaded";
    public static final String REASON_VOICE_CALL_ENDED = "2GVoiceCallEnded";
    public static final String REASON_VOICE_CALL_STARTED = "2GVoiceCallStarted";
    public static final String STATE_CHANGE_REASON_KEY = "reason";
    public static final String STATE_KEY = "state";
    public static final int TTY_MODE_FULL = 1;
    public static final int TTY_MODE_HCO = 2;
    public static final int TTY_MODE_OFF = 0;
    public static final int TTY_MODE_VCO = 3;

    public abstract void acceptCall()
        throws CallStateException;

    public abstract void activateCellBroadcastSms(int paramInt, Message paramMessage);

    public abstract boolean canConference();

    public abstract boolean canTransfer();

    public abstract void clearDisconnected();

    public abstract void conference()
        throws CallStateException;

    public abstract Connection dial(String paramString)
        throws CallStateException;

    public abstract Connection dial(String paramString, UUSInfo paramUUSInfo)
        throws CallStateException;

    public abstract int disableApnType(String paramString);

    public abstract void disableDnsCheck(boolean paramBoolean);

    public abstract void disableLocationUpdates();

    public abstract void dispose();

    public abstract int enableApnType(String paramString);

    public abstract void enableEnhancedVoicePrivacy(boolean paramBoolean, Message paramMessage);

    public abstract void enableLocationUpdates();

    public abstract void exitEmergencyCallbackMode();

    public abstract void explicitCallTransfer()
        throws CallStateException;

    public abstract String getActiveApnHost(String paramString);

    public abstract String[] getActiveApnTypes();

    public abstract void getAvailableNetworks(Message paramMessage);

    public abstract Call getBackgroundCall();

    public abstract boolean getCallForwardingIndicator();

    public abstract void getCallForwardingOption(int paramInt, Message paramMessage);

    public abstract void getCallWaiting(Message paramMessage);

    public abstract int getCdmaEriIconIndex();

    public abstract int getCdmaEriIconMode();

    public abstract String getCdmaEriText();

    public abstract String getCdmaMin();

    public abstract String getCdmaPrlVersion();

    public abstract void getCellBroadcastSmsConfig(Message paramMessage);

    public abstract CellLocation getCellLocation();

    public abstract Context getContext();

    public abstract DataActivityState getDataActivityState();

    public abstract void getDataCallList(Message paramMessage);

    public abstract DataState getDataConnectionState();

    public abstract DataState getDataConnectionState(String paramString);

    public abstract boolean getDataRoamingEnabled();

    public abstract String getDeviceId();

    public abstract String getDeviceSvn();

    public abstract void getEnhancedVoicePrivacy(Message paramMessage);

    public abstract String getEsn();

    public abstract Call getForegroundCall();

    public abstract IccCard getIccCard();

    public abstract IccPhoneBookInterfaceManager getIccPhoneBookInterfaceManager();

    public abstract boolean getIccRecordsLoaded();

    public abstract String getIccSerialNumber();

    public abstract IccSmsInterfaceManager getIccSmsInterfaceManager();

    public abstract String getImei();

    public abstract IsimRecords getIsimRecords();

    public abstract String getLine1AlphaTag();

    public abstract String getLine1Number();

    public abstract LinkCapabilities getLinkCapabilities(String paramString);

    public abstract LinkProperties getLinkProperties(String paramString);

    public abstract int getLteOnCdmaMode();

    public abstract String getMeid();

    public abstract boolean getMessageWaitingIndicator();

    public abstract String getMsisdn();

    public abstract boolean getMute();

    public abstract void getNeighboringCids(Message paramMessage);

    public abstract void getOutgoingCallerIdDisplay(Message paramMessage);

    public abstract List<? extends MmiCode> getPendingMmiCodes();

    public abstract String getPhoneName();

    public abstract PhoneSubInfo getPhoneSubInfo();

    public abstract int getPhoneType();

    public abstract void getPreferredNetworkType(Message paramMessage);

    public abstract Call getRingingCall();

    public abstract ServiceState getServiceState();

    public abstract SignalStrength getSignalStrength();

    public abstract SimulatedRadioControl getSimulatedRadioControl();

    public abstract void getSmscAddress(Message paramMessage);

    public abstract State getState();

    public abstract String getSubscriberId();

    public abstract boolean getUnitTestMode();

    public abstract UsimServiceTable getUsimServiceTable();

    public abstract String getVoiceMailAlphaTag();

    public abstract String getVoiceMailNumber();

    public abstract int getVoiceMessageCount();

    public abstract boolean handleInCallMmiCommands(String paramString)
        throws CallStateException;

    public abstract boolean handlePinMmi(String paramString);

    public abstract void invokeOemRilRequestRaw(byte[] paramArrayOfByte, Message paramMessage);

    public abstract void invokeOemRilRequestStrings(String[] paramArrayOfString, Message paramMessage);

    public abstract boolean isCspPlmnEnabled();

    public abstract boolean isDataConnectivityPossible();

    public abstract boolean isDataConnectivityPossible(String paramString);

    public abstract boolean isDnsCheckDisabled();

    public abstract boolean isMinInfoReady();

    public abstract boolean isOtaSpNumber(String paramString);

    public abstract boolean needsOtaServiceProvisioning();

    public abstract void notifyDataActivity();

    public abstract void queryAvailableBandMode(Message paramMessage);

    public abstract void queryCdmaRoamingPreference(Message paramMessage);

    public abstract void queryTTYMode(Message paramMessage);

    public abstract void registerFoT53ClirlInfo(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForCallWaiting(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForCdmaOtaStatusChange(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForDisconnect(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForDisplayInfo(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForEcmTimerReset(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForInCallVoicePrivacyOff(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForInCallVoicePrivacyOn(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForIncomingRing(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForLineControlInfo(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForMmiComplete(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForMmiInitiate(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForNewRingingConnection(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForNumberInfo(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForPreciseCallStateChanged(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForRedirectedNumberInfo(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForResendIncallMute(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForRingbackTone(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForServiceStateChanged(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForSignalInfo(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForSubscriptionInfoReady(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForSuppServiceFailed(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForSuppServiceNotification(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForT53AudioControlInfo(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForUnknownConnection(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void rejectCall()
        throws CallStateException;

    public abstract void removeReferences();

    public abstract void requestIsimAuthentication(String paramString, Message paramMessage);

    public abstract void selectNetworkManually(OperatorInfo paramOperatorInfo, Message paramMessage);

    public abstract void sendBurstDtmf(String paramString, int paramInt1, int paramInt2, Message paramMessage);

    public abstract void sendDtmf(char paramChar);

    public abstract void sendUssdResponse(String paramString);

    public abstract void setBandMode(int paramInt, Message paramMessage);

    public abstract void setCallForwardingOption(int paramInt1, int paramInt2, String paramString, int paramInt3, Message paramMessage);

    public abstract void setCallWaiting(boolean paramBoolean, Message paramMessage);

    public abstract void setCdmaRoamingPreference(int paramInt, Message paramMessage);

    public abstract void setCdmaSubscription(int paramInt, Message paramMessage);

    public abstract void setCellBroadcastSmsConfig(int[] paramArrayOfInt, Message paramMessage);

    public abstract void setDataRoamingEnabled(boolean paramBoolean);

    public abstract void setEchoSuppressionEnabled(boolean paramBoolean);

    public abstract void setLine1Number(String paramString1, String paramString2, Message paramMessage);

    public abstract void setMute(boolean paramBoolean);

    public abstract void setNetworkSelectionModeAutomatic(Message paramMessage);

    public abstract void setOnEcbModeExitResponse(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void setOnPostDialCharacter(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void setOutgoingCallerIdDisplay(int paramInt, Message paramMessage);

    public abstract void setPreferredNetworkType(int paramInt, Message paramMessage);

    public abstract void setRadioPower(boolean paramBoolean);

    public abstract void setSmscAddress(String paramString, Message paramMessage);

    public abstract void setTTYMode(int paramInt, Message paramMessage);

    public abstract void setUnitTestMode(boolean paramBoolean);

    public abstract void setVoiceMailNumber(String paramString1, String paramString2, Message paramMessage);

    public abstract void setVoiceMessageWaiting(int paramInt1, int paramInt2);

    public abstract void startDtmf(char paramChar);

    public abstract void stopDtmf();

    public abstract void switchHoldingAndActive()
        throws CallStateException;

    public abstract void unregisterForCallWaiting(Handler paramHandler);

    public abstract void unregisterForCdmaOtaStatusChange(Handler paramHandler);

    public abstract void unregisterForDisconnect(Handler paramHandler);

    public abstract void unregisterForDisplayInfo(Handler paramHandler);

    public abstract void unregisterForEcmTimerReset(Handler paramHandler);

    public abstract void unregisterForInCallVoicePrivacyOff(Handler paramHandler);

    public abstract void unregisterForInCallVoicePrivacyOn(Handler paramHandler);

    public abstract void unregisterForIncomingRing(Handler paramHandler);

    public abstract void unregisterForLineControlInfo(Handler paramHandler);

    public abstract void unregisterForMmiComplete(Handler paramHandler);

    public abstract void unregisterForMmiInitiate(Handler paramHandler);

    public abstract void unregisterForNewRingingConnection(Handler paramHandler);

    public abstract void unregisterForNumberInfo(Handler paramHandler);

    public abstract void unregisterForPreciseCallStateChanged(Handler paramHandler);

    public abstract void unregisterForRedirectedNumberInfo(Handler paramHandler);

    public abstract void unregisterForResendIncallMute(Handler paramHandler);

    public abstract void unregisterForRingbackTone(Handler paramHandler);

    public abstract void unregisterForServiceStateChanged(Handler paramHandler);

    public abstract void unregisterForSignalInfo(Handler paramHandler);

    public abstract void unregisterForSubscriptionInfoReady(Handler paramHandler);

    public abstract void unregisterForSuppServiceFailed(Handler paramHandler);

    public abstract void unregisterForSuppServiceNotification(Handler paramHandler);

    public abstract void unregisterForT53AudioControlInfo(Handler paramHandler);

    public abstract void unregisterForT53ClirInfo(Handler paramHandler);

    public abstract void unregisterForUnknownConnection(Handler paramHandler);

    public abstract void unsetOnEcbModeExitResponse(Handler paramHandler);

    public abstract void updateServiceLocation();

    public static enum SuppService
    {
        static
        {
            SWITCH = new SuppService("SWITCH", 1);
            SEPARATE = new SuppService("SEPARATE", 2);
            TRANSFER = new SuppService("TRANSFER", 3);
            CONFERENCE = new SuppService("CONFERENCE", 4);
            REJECT = new SuppService("REJECT", 5);
            HANGUP = new SuppService("HANGUP", 6);
            SuppService[] arrayOfSuppService = new SuppService[7];
            arrayOfSuppService[0] = UNKNOWN;
            arrayOfSuppService[1] = SWITCH;
            arrayOfSuppService[2] = SEPARATE;
            arrayOfSuppService[3] = TRANSFER;
            arrayOfSuppService[4] = CONFERENCE;
            arrayOfSuppService[5] = REJECT;
            arrayOfSuppService[6] = HANGUP;
        }
    }

    public static enum DataActivityState
    {
        static
        {
            DATAIN = new DataActivityState("DATAIN", 1);
            DATAOUT = new DataActivityState("DATAOUT", 2);
            DATAINANDOUT = new DataActivityState("DATAINANDOUT", 3);
            DORMANT = new DataActivityState("DORMANT", 4);
            DataActivityState[] arrayOfDataActivityState = new DataActivityState[5];
            arrayOfDataActivityState[0] = NONE;
            arrayOfDataActivityState[1] = DATAIN;
            arrayOfDataActivityState[2] = DATAOUT;
            arrayOfDataActivityState[3] = DATAINANDOUT;
            arrayOfDataActivityState[4] = DORMANT;
        }
    }

    public static enum DataState
    {
        static
        {
            DataState[] arrayOfDataState = new DataState[4];
            arrayOfDataState[0] = CONNECTED;
            arrayOfDataState[1] = CONNECTING;
            arrayOfDataState[2] = DISCONNECTED;
            arrayOfDataState[3] = SUSPENDED;
        }
    }

    public static enum State
    {
        static
        {
            OFFHOOK = new State("OFFHOOK", 2);
            State[] arrayOfState = new State[3];
            arrayOfState[0] = IDLE;
            arrayOfState[1] = RINGING;
            arrayOfState[2] = OFFHOOK;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.Phone
 * JD-Core Version:        0.6.2
 */