package com.android.internal.telephony;

public final class IccVmFixedException extends IccException
{
    IccVmFixedException()
    {
    }

    public IccVmFixedException(String paramString)
    {
        super(paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.IccVmFixedException
 * JD-Core Version:        0.6.2
 */