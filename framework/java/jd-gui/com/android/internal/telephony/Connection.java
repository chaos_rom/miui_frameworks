package com.android.internal.telephony;

import android.util.Log;

public abstract class Connection
{
    private static String LOG_TAG = "TelephonyConnection";
    public static int PRESENTATION_ALLOWED = 1;
    public static int PRESENTATION_PAYPHONE;
    public static int PRESENTATION_RESTRICTED = 2;
    public static int PRESENTATION_UNKNOWN = 3;
    Object userData;

    static
    {
        PRESENTATION_PAYPHONE = 4;
    }

    public abstract void cancelPostDial();

    public void clearUserData()
    {
        this.userData = null;
    }

    public abstract String getAddress();

    public abstract Call getCall();

    public String getCnapName()
    {
        return null;
    }

    public int getCnapNamePresentation()
    {
        return 0;
    }

    public abstract long getConnectTime();

    public abstract long getCreateTime();

    public abstract DisconnectCause getDisconnectCause();

    public abstract long getDisconnectTime();

    public abstract long getDurationMillis();

    public abstract long getHoldDurationMillis();

    public abstract int getNumberPresentation();

    public String getOrigDialString()
    {
        return null;
    }

    public abstract PostDialState getPostDialState();

    public abstract String getRemainingPostDialString();

    public Call.State getState()
    {
        Call localCall = getCall();
        if (localCall == null);
        for (Call.State localState = Call.State.IDLE; ; localState = localCall.getState())
            return localState;
    }

    public abstract UUSInfo getUUSInfo();

    public Object getUserData()
    {
        return this.userData;
    }

    public abstract void hangup()
        throws CallStateException;

    public boolean isAlive()
    {
        return getState().isAlive();
    }

    public abstract boolean isIncoming();

    public boolean isRinging()
    {
        return getState().isRinging();
    }

    public abstract void proceedAfterWaitChar();

    public abstract void proceedAfterWildChar(String paramString);

    public abstract void separate()
        throws CallStateException;

    public void setUserData(Object paramObject)
    {
        this.userData = paramObject;
    }

    public String toString()
    {
        StringBuilder localStringBuilder = new StringBuilder(128);
        if (Log.isLoggable(LOG_TAG, 3))
            localStringBuilder.append("addr: " + getAddress()).append(" pres.: " + getNumberPresentation()).append(" dial: " + getOrigDialString()).append(" postdial: " + getRemainingPostDialString()).append(" cnap name: " + getCnapName()).append("(" + getCnapNamePresentation() + ")");
        localStringBuilder.append(" incoming: " + isIncoming()).append(" state: " + getState()).append(" post dial state: " + getPostDialState());
        return localStringBuilder.toString();
    }

    public static enum PostDialState
    {
        static
        {
            COMPLETE = new PostDialState("COMPLETE", 4);
            CANCELLED = new PostDialState("CANCELLED", 5);
            PAUSE = new PostDialState("PAUSE", 6);
            PostDialState[] arrayOfPostDialState = new PostDialState[7];
            arrayOfPostDialState[0] = NOT_STARTED;
            arrayOfPostDialState[1] = STARTED;
            arrayOfPostDialState[2] = WAIT;
            arrayOfPostDialState[3] = WILD;
            arrayOfPostDialState[4] = COMPLETE;
            arrayOfPostDialState[5] = CANCELLED;
            arrayOfPostDialState[6] = PAUSE;
        }
    }

    public static enum DisconnectCause
    {
        static
        {
            INCOMING_MISSED = new DisconnectCause("INCOMING_MISSED", 1);
            NORMAL = new DisconnectCause("NORMAL", 2);
            LOCAL = new DisconnectCause("LOCAL", 3);
            BUSY = new DisconnectCause("BUSY", 4);
            CONGESTION = new DisconnectCause("CONGESTION", 5);
            MMI = new DisconnectCause("MMI", 6);
            INVALID_NUMBER = new DisconnectCause("INVALID_NUMBER", 7);
            NUMBER_UNREACHABLE = new DisconnectCause("NUMBER_UNREACHABLE", 8);
            SERVER_UNREACHABLE = new DisconnectCause("SERVER_UNREACHABLE", 9);
            INVALID_CREDENTIALS = new DisconnectCause("INVALID_CREDENTIALS", 10);
            OUT_OF_NETWORK = new DisconnectCause("OUT_OF_NETWORK", 11);
            SERVER_ERROR = new DisconnectCause("SERVER_ERROR", 12);
            TIMED_OUT = new DisconnectCause("TIMED_OUT", 13);
            LOST_SIGNAL = new DisconnectCause("LOST_SIGNAL", 14);
            LIMIT_EXCEEDED = new DisconnectCause("LIMIT_EXCEEDED", 15);
            INCOMING_REJECTED = new DisconnectCause("INCOMING_REJECTED", 16);
            POWER_OFF = new DisconnectCause("POWER_OFF", 17);
            OUT_OF_SERVICE = new DisconnectCause("OUT_OF_SERVICE", 18);
            ICC_ERROR = new DisconnectCause("ICC_ERROR", 19);
            CALL_BARRED = new DisconnectCause("CALL_BARRED", 20);
            FDN_BLOCKED = new DisconnectCause("FDN_BLOCKED", 21);
            CS_RESTRICTED = new DisconnectCause("CS_RESTRICTED", 22);
            CS_RESTRICTED_NORMAL = new DisconnectCause("CS_RESTRICTED_NORMAL", 23);
            CS_RESTRICTED_EMERGENCY = new DisconnectCause("CS_RESTRICTED_EMERGENCY", 24);
            UNOBTAINABLE_NUMBER = new DisconnectCause("UNOBTAINABLE_NUMBER", 25);
            CDMA_LOCKED_UNTIL_POWER_CYCLE = new DisconnectCause("CDMA_LOCKED_UNTIL_POWER_CYCLE", 26);
            CDMA_DROP = new DisconnectCause("CDMA_DROP", 27);
            CDMA_INTERCEPT = new DisconnectCause("CDMA_INTERCEPT", 28);
            CDMA_REORDER = new DisconnectCause("CDMA_REORDER", 29);
            CDMA_SO_REJECT = new DisconnectCause("CDMA_SO_REJECT", 30);
            CDMA_RETRY_ORDER = new DisconnectCause("CDMA_RETRY_ORDER", 31);
            CDMA_ACCESS_FAILURE = new DisconnectCause("CDMA_ACCESS_FAILURE", 32);
            CDMA_PREEMPTED = new DisconnectCause("CDMA_PREEMPTED", 33);
            CDMA_NOT_EMERGENCY = new DisconnectCause("CDMA_NOT_EMERGENCY", 34);
            CDMA_ACCESS_BLOCKED = new DisconnectCause("CDMA_ACCESS_BLOCKED", 35);
            ERROR_UNSPECIFIED = new DisconnectCause("ERROR_UNSPECIFIED", 36);
            DisconnectCause[] arrayOfDisconnectCause = new DisconnectCause[37];
            arrayOfDisconnectCause[0] = NOT_DISCONNECTED;
            arrayOfDisconnectCause[1] = INCOMING_MISSED;
            arrayOfDisconnectCause[2] = NORMAL;
            arrayOfDisconnectCause[3] = LOCAL;
            arrayOfDisconnectCause[4] = BUSY;
            arrayOfDisconnectCause[5] = CONGESTION;
            arrayOfDisconnectCause[6] = MMI;
            arrayOfDisconnectCause[7] = INVALID_NUMBER;
            arrayOfDisconnectCause[8] = NUMBER_UNREACHABLE;
            arrayOfDisconnectCause[9] = SERVER_UNREACHABLE;
            arrayOfDisconnectCause[10] = INVALID_CREDENTIALS;
            arrayOfDisconnectCause[11] = OUT_OF_NETWORK;
            arrayOfDisconnectCause[12] = SERVER_ERROR;
            arrayOfDisconnectCause[13] = TIMED_OUT;
            arrayOfDisconnectCause[14] = LOST_SIGNAL;
            arrayOfDisconnectCause[15] = LIMIT_EXCEEDED;
            arrayOfDisconnectCause[16] = INCOMING_REJECTED;
            arrayOfDisconnectCause[17] = POWER_OFF;
            arrayOfDisconnectCause[18] = OUT_OF_SERVICE;
            arrayOfDisconnectCause[19] = ICC_ERROR;
            arrayOfDisconnectCause[20] = CALL_BARRED;
            arrayOfDisconnectCause[21] = FDN_BLOCKED;
            arrayOfDisconnectCause[22] = CS_RESTRICTED;
            arrayOfDisconnectCause[23] = CS_RESTRICTED_NORMAL;
            arrayOfDisconnectCause[24] = CS_RESTRICTED_EMERGENCY;
            arrayOfDisconnectCause[25] = UNOBTAINABLE_NUMBER;
            arrayOfDisconnectCause[26] = CDMA_LOCKED_UNTIL_POWER_CYCLE;
            arrayOfDisconnectCause[27] = CDMA_DROP;
            arrayOfDisconnectCause[28] = CDMA_INTERCEPT;
            arrayOfDisconnectCause[29] = CDMA_REORDER;
            arrayOfDisconnectCause[30] = CDMA_SO_REJECT;
            arrayOfDisconnectCause[31] = CDMA_RETRY_ORDER;
            arrayOfDisconnectCause[32] = CDMA_ACCESS_FAILURE;
            arrayOfDisconnectCause[33] = CDMA_PREEMPTED;
            arrayOfDisconnectCause[34] = CDMA_NOT_EMERGENCY;
            arrayOfDisconnectCause[35] = CDMA_ACCESS_BLOCKED;
            arrayOfDisconnectCause[36] = ERROR_UNSPECIFIED;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.Connection
 * JD-Core Version:        0.6.2
 */