package com.android.internal.telephony.cat;

import android.graphics.Bitmap;

class PlayToneParams extends CommandParams
{
    ToneSettings settings;
    TextMessage textMsg;

    PlayToneParams(CommandDetails paramCommandDetails, TextMessage paramTextMessage, Tone paramTone, Duration paramDuration, boolean paramBoolean)
    {
        super(paramCommandDetails);
        this.textMsg = paramTextMessage;
        this.settings = new ToneSettings(paramDuration, paramTone, paramBoolean);
    }

    boolean setIcon(Bitmap paramBitmap)
    {
        if ((paramBitmap != null) && (this.textMsg != null))
            this.textMsg.icon = paramBitmap;
        for (boolean bool = true; ; bool = false)
            return bool;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cat.PlayToneParams
 * JD-Core Version:        0.6.2
 */