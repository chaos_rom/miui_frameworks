package com.android.internal.telephony.cat;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class CatCmdMessage
    implements Parcelable
{
    public static final Parcelable.Creator<CatCmdMessage> CREATOR = new Parcelable.Creator()
    {
        public CatCmdMessage createFromParcel(Parcel paramAnonymousParcel)
        {
            return new CatCmdMessage(paramAnonymousParcel);
        }

        public CatCmdMessage[] newArray(int paramAnonymousInt)
        {
            return new CatCmdMessage[paramAnonymousInt];
        }
    };
    private BrowserSettings mBrowserSettings = null;
    private CallSettings mCallSettings = null;
    CommandDetails mCmdDet;
    private Input mInput;
    private Menu mMenu;
    private TextMessage mTextMsg;
    private ToneSettings mToneSettings = null;

    public CatCmdMessage(Parcel paramParcel)
    {
        this.mCmdDet = ((CommandDetails)paramParcel.readParcelable(null));
        this.mTextMsg = ((TextMessage)paramParcel.readParcelable(null));
        this.mMenu = ((Menu)paramParcel.readParcelable(null));
        this.mInput = ((Input)paramParcel.readParcelable(null));
        switch (2.$SwitchMap$com$android$internal$telephony$cat$AppInterface$CommandType[getCmdType().ordinal()])
        {
        default:
        case 11:
        case 12:
        case 13:
        }
        while (true)
        {
            return;
            this.mBrowserSettings = new BrowserSettings();
            this.mBrowserSettings.url = paramParcel.readString();
            this.mBrowserSettings.mode = LaunchBrowserMode.values()[paramParcel.readInt()];
            continue;
            this.mToneSettings = ((ToneSettings)paramParcel.readParcelable(null));
            continue;
            this.mCallSettings = new CallSettings();
            this.mCallSettings.confirmMsg = ((TextMessage)paramParcel.readParcelable(null));
            this.mCallSettings.callMsg = ((TextMessage)paramParcel.readParcelable(null));
        }
    }

    CatCmdMessage(CommandParams paramCommandParams)
    {
        this.mCmdDet = paramCommandParams.cmdDet;
        switch (2.$SwitchMap$com$android$internal$telephony$cat$AppInterface$CommandType[getCmdType().ordinal()])
        {
        default:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
        case 10:
        case 11:
        case 12:
        case 13:
        case 14:
        case 15:
        case 16:
        case 17:
        }
        while (true)
        {
            return;
            this.mMenu = ((SelectItemParams)paramCommandParams).menu;
            continue;
            this.mTextMsg = ((DisplayTextParams)paramCommandParams).textMsg;
            continue;
            this.mInput = ((GetInputParams)paramCommandParams).input;
            continue;
            this.mTextMsg = ((LaunchBrowserParams)paramCommandParams).confirmMsg;
            this.mBrowserSettings = new BrowserSettings();
            this.mBrowserSettings.url = ((LaunchBrowserParams)paramCommandParams).url;
            this.mBrowserSettings.mode = ((LaunchBrowserParams)paramCommandParams).mode;
            continue;
            PlayToneParams localPlayToneParams = (PlayToneParams)paramCommandParams;
            this.mToneSettings = localPlayToneParams.settings;
            this.mTextMsg = localPlayToneParams.textMsg;
            continue;
            this.mCallSettings = new CallSettings();
            this.mCallSettings.confirmMsg = ((CallSetupParams)paramCommandParams).confirmMsg;
            this.mCallSettings.callMsg = ((CallSetupParams)paramCommandParams).callMsg;
            continue;
            this.mTextMsg = ((BIPClientParams)paramCommandParams).textMsg;
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public Input geInput()
    {
        return this.mInput;
    }

    public TextMessage geTextMessage()
    {
        return this.mTextMsg;
    }

    public BrowserSettings getBrowserSettings()
    {
        return this.mBrowserSettings;
    }

    public CallSettings getCallSettings()
    {
        return this.mCallSettings;
    }

    public AppInterface.CommandType getCmdType()
    {
        return AppInterface.CommandType.fromInt(this.mCmdDet.typeOfCommand);
    }

    public Menu getMenu()
    {
        return this.mMenu;
    }

    public ToneSettings getToneSettings()
    {
        return this.mToneSettings;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeParcelable(this.mCmdDet, 0);
        paramParcel.writeParcelable(this.mTextMsg, 0);
        paramParcel.writeParcelable(this.mMenu, 0);
        paramParcel.writeParcelable(this.mInput, 0);
        switch (2.$SwitchMap$com$android$internal$telephony$cat$AppInterface$CommandType[getCmdType().ordinal()])
        {
        default:
        case 11:
        case 12:
        case 13:
        }
        while (true)
        {
            return;
            paramParcel.writeString(this.mBrowserSettings.url);
            paramParcel.writeInt(this.mBrowserSettings.mode.ordinal());
            continue;
            paramParcel.writeParcelable(this.mToneSettings, 0);
            continue;
            paramParcel.writeParcelable(this.mCallSettings.confirmMsg, 0);
            paramParcel.writeParcelable(this.mCallSettings.callMsg, 0);
        }
    }

    public class CallSettings
    {
        public TextMessage callMsg;
        public TextMessage confirmMsg;

        public CallSettings()
        {
        }
    }

    public class BrowserSettings
    {
        public LaunchBrowserMode mode;
        public String url;

        public BrowserSettings()
        {
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cat.CatCmdMessage
 * JD-Core Version:        0.6.2
 */