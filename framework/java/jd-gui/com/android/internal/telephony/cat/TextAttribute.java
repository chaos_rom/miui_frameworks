package com.android.internal.telephony.cat;

public class TextAttribute
{
    public TextAlignment align;
    public boolean bold;
    public TextColor color;
    public boolean italic;
    public int length;
    public FontSize size;
    public int start;
    public boolean strikeThrough;
    public boolean underlined;

    public TextAttribute(int paramInt1, int paramInt2, TextAlignment paramTextAlignment, FontSize paramFontSize, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, TextColor paramTextColor)
    {
        this.start = paramInt1;
        this.length = paramInt2;
        this.align = paramTextAlignment;
        this.size = paramFontSize;
        this.bold = paramBoolean1;
        this.italic = paramBoolean2;
        this.underlined = paramBoolean3;
        this.strikeThrough = paramBoolean4;
        this.color = paramTextColor;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cat.TextAttribute
 * JD-Core Version:        0.6.2
 */