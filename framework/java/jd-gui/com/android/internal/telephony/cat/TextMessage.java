package com.android.internal.telephony.cat;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class TextMessage
    implements Parcelable
{
    public static final Parcelable.Creator<TextMessage> CREATOR = new Parcelable.Creator()
    {
        public TextMessage createFromParcel(Parcel paramAnonymousParcel)
        {
            return new TextMessage(paramAnonymousParcel, null);
        }

        public TextMessage[] newArray(int paramAnonymousInt)
        {
            return new TextMessage[paramAnonymousInt];
        }
    };
    public Duration duration;
    public Bitmap icon = null;
    public boolean iconSelfExplanatory = false;
    public boolean isHighPriority = false;
    public boolean responseNeeded;
    public String text = null;
    public String title = "";
    public boolean userClear;

    TextMessage()
    {
        this.responseNeeded = true;
        this.userClear = false;
        this.duration = null;
    }

    private TextMessage(Parcel paramParcel)
    {
        this.responseNeeded = i;
        this.userClear = false;
        this.duration = null;
        this.title = paramParcel.readString();
        this.text = paramParcel.readString();
        this.icon = ((Bitmap)paramParcel.readParcelable(null));
        int j;
        int k;
        label101: int m;
        if (paramParcel.readInt() == i)
        {
            j = i;
            this.iconSelfExplanatory = j;
            if (paramParcel.readInt() != i)
                break label155;
            k = i;
            this.isHighPriority = k;
            if (paramParcel.readInt() != i)
                break label161;
            m = i;
            label118: this.responseNeeded = m;
            if (paramParcel.readInt() != i)
                break label167;
        }
        while (true)
        {
            this.userClear = i;
            this.duration = ((Duration)paramParcel.readParcelable(null));
            return;
            j = 0;
            break;
            label155: k = 0;
            break label101;
            label161: m = 0;
            break label118;
            label167: i = 0;
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        int i = 1;
        paramParcel.writeString(this.title);
        paramParcel.writeString(this.text);
        paramParcel.writeParcelable(this.icon, 0);
        int j;
        int k;
        label53: int m;
        if (this.iconSelfExplanatory)
        {
            j = i;
            paramParcel.writeInt(j);
            if (!this.isHighPriority)
                break label103;
            k = i;
            paramParcel.writeInt(k);
            if (!this.responseNeeded)
                break label109;
            m = i;
            label69: paramParcel.writeInt(m);
            if (!this.userClear)
                break label115;
        }
        while (true)
        {
            paramParcel.writeInt(i);
            paramParcel.writeParcelable(this.duration, 0);
            return;
            j = 0;
            break;
            label103: k = 0;
            break label53;
            label109: m = 0;
            break label69;
            label115: i = 0;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cat.TextMessage
 * JD-Core Version:        0.6.2
 */