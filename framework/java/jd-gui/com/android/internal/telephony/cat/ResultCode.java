package com.android.internal.telephony.cat;

public enum ResultCode
{
    private int mCode;

    static
    {
        PRFRMD_WITH_MISSING_INFO = new ResultCode("PRFRMD_WITH_MISSING_INFO", 2, 2);
        PRFRMD_WITH_ADDITIONAL_EFS_READ = new ResultCode("PRFRMD_WITH_ADDITIONAL_EFS_READ", 3, 3);
        PRFRMD_ICON_NOT_DISPLAYED = new ResultCode("PRFRMD_ICON_NOT_DISPLAYED", 4, 4);
        PRFRMD_MODIFIED_BY_NAA = new ResultCode("PRFRMD_MODIFIED_BY_NAA", 5, 5);
        PRFRMD_LIMITED_SERVICE = new ResultCode("PRFRMD_LIMITED_SERVICE", 6, 6);
        PRFRMD_WITH_MODIFICATION = new ResultCode("PRFRMD_WITH_MODIFICATION", 7, 7);
        PRFRMD_NAA_NOT_ACTIVE = new ResultCode("PRFRMD_NAA_NOT_ACTIVE", 8, 8);
        PRFRMD_TONE_NOT_PLAYED = new ResultCode("PRFRMD_TONE_NOT_PLAYED", 9, 9);
        UICC_SESSION_TERM_BY_USER = new ResultCode("UICC_SESSION_TERM_BY_USER", 10, 16);
        BACKWARD_MOVE_BY_USER = new ResultCode("BACKWARD_MOVE_BY_USER", 11, 17);
        NO_RESPONSE_FROM_USER = new ResultCode("NO_RESPONSE_FROM_USER", 12, 18);
        HELP_INFO_REQUIRED = new ResultCode("HELP_INFO_REQUIRED", 13, 19);
        USSD_SS_SESSION_TERM_BY_USER = new ResultCode("USSD_SS_SESSION_TERM_BY_USER", 14, 20);
        TERMINAL_CRNTLY_UNABLE_TO_PROCESS = new ResultCode("TERMINAL_CRNTLY_UNABLE_TO_PROCESS", 15, 32);
        NETWORK_CRNTLY_UNABLE_TO_PROCESS = new ResultCode("NETWORK_CRNTLY_UNABLE_TO_PROCESS", 16, 33);
        USER_NOT_ACCEPT = new ResultCode("USER_NOT_ACCEPT", 17, 34);
        USER_CLEAR_DOWN_CALL = new ResultCode("USER_CLEAR_DOWN_CALL", 18, 35);
        CONTRADICTION_WITH_TIMER = new ResultCode("CONTRADICTION_WITH_TIMER", 19, 36);
        NAA_CALL_CONTROL_TEMPORARY = new ResultCode("NAA_CALL_CONTROL_TEMPORARY", 20, 37);
        LAUNCH_BROWSER_ERROR = new ResultCode("LAUNCH_BROWSER_ERROR", 21, 38);
        MMS_TEMPORARY = new ResultCode("MMS_TEMPORARY", 22, 39);
        BEYOND_TERMINAL_CAPABILITY = new ResultCode("BEYOND_TERMINAL_CAPABILITY", 23, 48);
        CMD_TYPE_NOT_UNDERSTOOD = new ResultCode("CMD_TYPE_NOT_UNDERSTOOD", 24, 49);
        CMD_DATA_NOT_UNDERSTOOD = new ResultCode("CMD_DATA_NOT_UNDERSTOOD", 25, 50);
        CMD_NUM_NOT_KNOWN = new ResultCode("CMD_NUM_NOT_KNOWN", 26, 51);
        SS_RETURN_ERROR = new ResultCode("SS_RETURN_ERROR", 27, 52);
        SMS_RP_ERROR = new ResultCode("SMS_RP_ERROR", 28, 53);
        REQUIRED_VALUES_MISSING = new ResultCode("REQUIRED_VALUES_MISSING", 29, 54);
        USSD_RETURN_ERROR = new ResultCode("USSD_RETURN_ERROR", 30, 55);
        MULTI_CARDS_CMD_ERROR = new ResultCode("MULTI_CARDS_CMD_ERROR", 31, 56);
        USIM_CALL_CONTROL_PERMANENT = new ResultCode("USIM_CALL_CONTROL_PERMANENT", 32, 57);
        BIP_ERROR = new ResultCode("BIP_ERROR", 33, 58);
        ACCESS_TECH_UNABLE_TO_PROCESS = new ResultCode("ACCESS_TECH_UNABLE_TO_PROCESS", 34, 59);
        FRAMES_ERROR = new ResultCode("FRAMES_ERROR", 35, 60);
        MMS_ERROR = new ResultCode("MMS_ERROR", 36, 61);
        ResultCode[] arrayOfResultCode = new ResultCode[37];
        arrayOfResultCode[0] = OK;
        arrayOfResultCode[1] = PRFRMD_WITH_PARTIAL_COMPREHENSION;
        arrayOfResultCode[2] = PRFRMD_WITH_MISSING_INFO;
        arrayOfResultCode[3] = PRFRMD_WITH_ADDITIONAL_EFS_READ;
        arrayOfResultCode[4] = PRFRMD_ICON_NOT_DISPLAYED;
        arrayOfResultCode[5] = PRFRMD_MODIFIED_BY_NAA;
        arrayOfResultCode[6] = PRFRMD_LIMITED_SERVICE;
        arrayOfResultCode[7] = PRFRMD_WITH_MODIFICATION;
        arrayOfResultCode[8] = PRFRMD_NAA_NOT_ACTIVE;
        arrayOfResultCode[9] = PRFRMD_TONE_NOT_PLAYED;
        arrayOfResultCode[10] = UICC_SESSION_TERM_BY_USER;
        arrayOfResultCode[11] = BACKWARD_MOVE_BY_USER;
        arrayOfResultCode[12] = NO_RESPONSE_FROM_USER;
        arrayOfResultCode[13] = HELP_INFO_REQUIRED;
        arrayOfResultCode[14] = USSD_SS_SESSION_TERM_BY_USER;
        arrayOfResultCode[15] = TERMINAL_CRNTLY_UNABLE_TO_PROCESS;
        arrayOfResultCode[16] = NETWORK_CRNTLY_UNABLE_TO_PROCESS;
        arrayOfResultCode[17] = USER_NOT_ACCEPT;
        arrayOfResultCode[18] = USER_CLEAR_DOWN_CALL;
        arrayOfResultCode[19] = CONTRADICTION_WITH_TIMER;
        arrayOfResultCode[20] = NAA_CALL_CONTROL_TEMPORARY;
        arrayOfResultCode[21] = LAUNCH_BROWSER_ERROR;
        arrayOfResultCode[22] = MMS_TEMPORARY;
        arrayOfResultCode[23] = BEYOND_TERMINAL_CAPABILITY;
        arrayOfResultCode[24] = CMD_TYPE_NOT_UNDERSTOOD;
        arrayOfResultCode[25] = CMD_DATA_NOT_UNDERSTOOD;
        arrayOfResultCode[26] = CMD_NUM_NOT_KNOWN;
        arrayOfResultCode[27] = SS_RETURN_ERROR;
        arrayOfResultCode[28] = SMS_RP_ERROR;
        arrayOfResultCode[29] = REQUIRED_VALUES_MISSING;
        arrayOfResultCode[30] = USSD_RETURN_ERROR;
        arrayOfResultCode[31] = MULTI_CARDS_CMD_ERROR;
        arrayOfResultCode[32] = USIM_CALL_CONTROL_PERMANENT;
        arrayOfResultCode[33] = BIP_ERROR;
        arrayOfResultCode[34] = ACCESS_TECH_UNABLE_TO_PROCESS;
        arrayOfResultCode[35] = FRAMES_ERROR;
        arrayOfResultCode[36] = MMS_ERROR;
    }

    private ResultCode(int paramInt)
    {
        this.mCode = paramInt;
    }

    public static ResultCode fromInt(int paramInt)
    {
        ResultCode[] arrayOfResultCode = values();
        int i = arrayOfResultCode.length;
        int j = 0;
        ResultCode localResultCode;
        if (j < i)
        {
            localResultCode = arrayOfResultCode[j];
            if (localResultCode.mCode != paramInt);
        }
        while (true)
        {
            return localResultCode;
            j++;
            break;
            localResultCode = null;
        }
    }

    public int value()
    {
        return this.mCode;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cat.ResultCode
 * JD-Core Version:        0.6.2
 */