package com.android.internal.telephony.cat;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class Item
    implements Parcelable
{
    public static final Parcelable.Creator<Item> CREATOR = new Parcelable.Creator()
    {
        public Item createFromParcel(Parcel paramAnonymousParcel)
        {
            return new Item(paramAnonymousParcel);
        }

        public Item[] newArray(int paramAnonymousInt)
        {
            return new Item[paramAnonymousInt];
        }
    };
    public Bitmap icon;
    public int id;
    public String text;

    public Item(int paramInt, String paramString)
    {
        this.id = paramInt;
        this.text = paramString;
        this.icon = null;
    }

    public Item(Parcel paramParcel)
    {
        this.id = paramParcel.readInt();
        this.text = paramParcel.readString();
        this.icon = ((Bitmap)paramParcel.readParcelable(null));
    }

    public int describeContents()
    {
        return 0;
    }

    public String toString()
    {
        return this.text;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeInt(this.id);
        paramParcel.writeString(this.text);
        paramParcel.writeParcelable(this.icon, paramInt);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cat.Item
 * JD-Core Version:        0.6.2
 */