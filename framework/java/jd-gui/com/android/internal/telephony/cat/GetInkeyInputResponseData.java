package com.android.internal.telephony.cat;

import com.android.internal.telephony.EncodeException;
import com.android.internal.telephony.GsmAlphabet;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;

class GetInkeyInputResponseData extends ResponseData
{
    protected static final byte GET_INKEY_NO = 0;
    protected static final byte GET_INKEY_YES = 1;
    public String mInData;
    private boolean mIsPacked;
    private boolean mIsUcs2;
    private boolean mIsYesNo;
    private boolean mYesNoResponse;

    public GetInkeyInputResponseData(String paramString, boolean paramBoolean1, boolean paramBoolean2)
    {
        this.mIsUcs2 = paramBoolean1;
        this.mIsPacked = paramBoolean2;
        this.mInData = paramString;
        this.mIsYesNo = false;
    }

    public GetInkeyInputResponseData(boolean paramBoolean)
    {
        this.mIsUcs2 = false;
        this.mIsPacked = false;
        this.mInData = "";
        this.mIsYesNo = true;
        this.mYesNoResponse = paramBoolean;
    }

    public void format(ByteArrayOutputStream paramByteArrayOutputStream)
    {
        int i = 1;
        if (paramByteArrayOutputStream == null)
            return;
        paramByteArrayOutputStream.write(0x80 | ComprehensionTlvTag.TEXT_STRING.value());
        Object localObject1;
        if (this.mIsYesNo)
        {
            localObject1 = new byte[i];
            if (this.mYesNoResponse)
            {
                label39: localObject1[0] = i;
                label43: writeLength(paramByteArrayOutputStream, 1 + localObject1.length);
                if (!this.mIsUcs2)
                    break label224;
                paramByteArrayOutputStream.write(8);
            }
        }
        while (true)
        {
            while (true)
            {
                Object localObject2 = localObject1;
                int j = localObject2.length;
                for (int k = 0; k < j; k++)
                    paramByteArrayOutputStream.write(localObject2[k]);
                break;
                i = 0;
                break label39;
                if ((this.mInData == null) || (this.mInData.length() <= 0))
                    break label217;
                try
                {
                    if (this.mIsUcs2)
                    {
                        localObject1 = this.mInData.getBytes("UTF-16");
                        break label43;
                    }
                    if (this.mIsPacked)
                    {
                        int m = this.mInData.length();
                        byte[] arrayOfByte2 = GsmAlphabet.stringToGsm7BitPacked(this.mInData, 0, 0);
                        localObject1 = new byte[m];
                        System.arraycopy(arrayOfByte2, 1, localObject1, 0, m);
                    }
                }
                catch (UnsupportedEncodingException localUnsupportedEncodingException)
                {
                    localObject1 = new byte[0];
                    break label43;
                    byte[] arrayOfByte1 = GsmAlphabet.stringToGsm8BitPacked(this.mInData);
                    localObject1 = arrayOfByte1;
                }
                catch (EncodeException localEncodeException)
                {
                    localObject1 = new byte[0];
                }
            }
            break label43;
            label217: localObject1 = new byte[0];
            break label43;
            label224: if (this.mIsPacked)
                paramByteArrayOutputStream.write(0);
            else
                paramByteArrayOutputStream.write(4);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cat.GetInkeyInputResponseData
 * JD-Core Version:        0.6.2
 */