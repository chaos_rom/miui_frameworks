package com.android.internal.telephony.cat;

import android.graphics.Bitmap;

class CommandParams
{
    CommandDetails cmdDet;

    CommandParams(CommandDetails paramCommandDetails)
    {
        this.cmdDet = paramCommandDetails;
    }

    AppInterface.CommandType getCommandType()
    {
        return AppInterface.CommandType.fromInt(this.cmdDet.typeOfCommand);
    }

    boolean setIcon(Bitmap paramBitmap)
    {
        return true;
    }

    public String toString()
    {
        return this.cmdDet.toString();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cat.CommandParams
 * JD-Core Version:        0.6.2
 */