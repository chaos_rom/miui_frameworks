package com.android.internal.telephony.cat;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.ArrayList;
import java.util.List;

public class Menu
    implements Parcelable
{
    public static final Parcelable.Creator<Menu> CREATOR = new Parcelable.Creator()
    {
        public Menu createFromParcel(Parcel paramAnonymousParcel)
        {
            return new Menu(paramAnonymousParcel, null);
        }

        public Menu[] newArray(int paramAnonymousInt)
        {
            return new Menu[paramAnonymousInt];
        }
    };
    public int defaultItem;
    public boolean helpAvailable;
    public List<Item> items;
    public boolean itemsIconSelfExplanatory;
    public PresentationType presentationType;
    public boolean softKeyPreferred;
    public String title;
    public List<TextAttribute> titleAttrs;
    public Bitmap titleIcon;
    public boolean titleIconSelfExplanatory;

    public Menu()
    {
        this.items = new ArrayList();
        this.title = null;
        this.titleAttrs = null;
        this.defaultItem = 0;
        this.softKeyPreferred = false;
        this.helpAvailable = false;
        this.titleIconSelfExplanatory = false;
        this.itemsIconSelfExplanatory = false;
        this.titleIcon = null;
        this.presentationType = PresentationType.NAVIGATION_OPTIONS;
    }

    private Menu(Parcel paramParcel)
    {
        this.title = paramParcel.readString();
        this.titleIcon = ((Bitmap)paramParcel.readParcelable(null));
        this.items = new ArrayList();
        int j = paramParcel.readInt();
        for (int k = 0; k < j; k++)
        {
            Item localItem = (Item)paramParcel.readParcelable(null);
            this.items.add(localItem);
        }
        this.defaultItem = paramParcel.readInt();
        int m;
        int n;
        label115: int i1;
        if (paramParcel.readInt() == i)
        {
            m = i;
            this.softKeyPreferred = m;
            if (paramParcel.readInt() != i)
                break label170;
            n = i;
            this.helpAvailable = n;
            if (paramParcel.readInt() != i)
                break label176;
            i1 = i;
            label132: this.titleIconSelfExplanatory = i1;
            if (paramParcel.readInt() != i)
                break label182;
        }
        while (true)
        {
            this.itemsIconSelfExplanatory = i;
            this.presentationType = PresentationType.values()[paramParcel.readInt()];
            return;
            m = 0;
            break;
            label170: n = 0;
            break label115;
            label176: i1 = 0;
            break label132;
            label182: i = 0;
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        int i = 1;
        paramParcel.writeString(this.title);
        paramParcel.writeParcelable(this.titleIcon, paramInt);
        int j = this.items.size();
        paramParcel.writeInt(j);
        for (int k = 0; k < j; k++)
            paramParcel.writeParcelable((Parcelable)this.items.get(k), paramInt);
        paramParcel.writeInt(this.defaultItem);
        int m;
        int n;
        label105: int i1;
        if (this.softKeyPreferred)
        {
            m = i;
            paramParcel.writeInt(m);
            if (!this.helpAvailable)
                break label157;
            n = i;
            paramParcel.writeInt(n);
            if (!this.titleIconSelfExplanatory)
                break label163;
            i1 = i;
            label121: paramParcel.writeInt(i1);
            if (!this.itemsIconSelfExplanatory)
                break label169;
        }
        while (true)
        {
            paramParcel.writeInt(i);
            paramParcel.writeInt(this.presentationType.ordinal());
            return;
            m = 0;
            break;
            label157: n = 0;
            break label105;
            label163: i1 = 0;
            break label121;
            label169: i = 0;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cat.Menu
 * JD-Core Version:        0.6.2
 */