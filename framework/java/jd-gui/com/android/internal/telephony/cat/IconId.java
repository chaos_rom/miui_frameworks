package com.android.internal.telephony.cat;

class IconId extends ValueObject
{
    int recordNumber;
    boolean selfExplanatory;

    ComprehensionTlvTag getTag()
    {
        return ComprehensionTlvTag.ICON_ID;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cat.IconId
 * JD-Core Version:        0.6.2
 */