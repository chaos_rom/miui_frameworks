package com.android.internal.telephony.cat;

import android.graphics.Bitmap;
import java.util.Iterator;
import java.util.List;

class SelectItemParams extends CommandParams
{
    boolean loadTitleIcon = false;
    Menu menu = null;

    SelectItemParams(CommandDetails paramCommandDetails, Menu paramMenu, boolean paramBoolean)
    {
        super(paramCommandDetails);
        this.menu = paramMenu;
        this.loadTitleIcon = paramBoolean;
    }

    boolean setIcon(Bitmap paramBitmap)
    {
        if ((paramBitmap != null) && (this.menu != null))
            if ((this.loadTitleIcon) && (this.menu.titleIcon == null))
                this.menu.titleIcon = paramBitmap;
        label36: for (boolean bool = true; ; bool = false)
        {
            return bool;
            Iterator localIterator = this.menu.items.iterator();
            if (!localIterator.hasNext())
                break label36;
            Item localItem = (Item)localIterator.next();
            if (localItem.icon != null)
                break;
            localItem.icon = paramBitmap;
            break label36;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cat.SelectItemParams
 * JD-Core Version:        0.6.2
 */