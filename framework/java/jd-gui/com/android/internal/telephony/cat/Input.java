package com.android.internal.telephony.cat;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class Input
    implements Parcelable
{
    public static final Parcelable.Creator<Input> CREATOR = new Parcelable.Creator()
    {
        public Input createFromParcel(Parcel paramAnonymousParcel)
        {
            return new Input(paramAnonymousParcel, null);
        }

        public Input[] newArray(int paramAnonymousInt)
        {
            return new Input[paramAnonymousInt];
        }
    };
    public String defaultText;
    public boolean digitOnly;
    public Duration duration;
    public boolean echo;
    public boolean helpAvailable;
    public Bitmap icon;
    public int maxLen;
    public int minLen;
    public boolean packed;
    public String text;
    public boolean ucs2;
    public boolean yesNo;

    Input()
    {
        this.text = "";
        this.defaultText = null;
        this.icon = null;
        this.minLen = 0;
        this.maxLen = 1;
        this.ucs2 = false;
        this.packed = false;
        this.digitOnly = false;
        this.echo = false;
        this.yesNo = false;
        this.helpAvailable = false;
        this.duration = null;
    }

    private Input(Parcel paramParcel)
    {
        this.text = paramParcel.readString();
        this.defaultText = paramParcel.readString();
        this.icon = ((Bitmap)paramParcel.readParcelable(null));
        this.minLen = paramParcel.readInt();
        this.maxLen = paramParcel.readInt();
        int j;
        int k;
        label76: int m;
        label93: int n;
        label110: int i1;
        if (paramParcel.readInt() == i)
        {
            j = i;
            this.ucs2 = j;
            if (paramParcel.readInt() != i)
                break label164;
            k = i;
            this.packed = k;
            if (paramParcel.readInt() != i)
                break label170;
            m = i;
            this.digitOnly = m;
            if (paramParcel.readInt() != i)
                break label176;
            n = i;
            this.echo = n;
            if (paramParcel.readInt() != i)
                break label182;
            i1 = i;
            label127: this.yesNo = i1;
            if (paramParcel.readInt() != i)
                break label188;
        }
        while (true)
        {
            this.helpAvailable = i;
            this.duration = ((Duration)paramParcel.readParcelable(null));
            return;
            j = 0;
            break;
            label164: k = 0;
            break label76;
            label170: m = 0;
            break label93;
            label176: n = 0;
            break label110;
            label182: i1 = 0;
            break label127;
            label188: i = 0;
        }
    }

    public int describeContents()
    {
        return 0;
    }

    boolean setIcon(Bitmap paramBitmap)
    {
        return true;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        int i = 1;
        paramParcel.writeString(this.text);
        paramParcel.writeString(this.defaultText);
        paramParcel.writeParcelable(this.icon, 0);
        paramParcel.writeInt(this.minLen);
        paramParcel.writeInt(this.maxLen);
        int j;
        int k;
        label69: int m;
        label85: int n;
        label101: int i1;
        if (this.ucs2)
        {
            j = i;
            paramParcel.writeInt(j);
            if (!this.packed)
                break label151;
            k = i;
            paramParcel.writeInt(k);
            if (!this.digitOnly)
                break label157;
            m = i;
            paramParcel.writeInt(m);
            if (!this.echo)
                break label163;
            n = i;
            paramParcel.writeInt(n);
            if (!this.yesNo)
                break label169;
            i1 = i;
            label117: paramParcel.writeInt(i1);
            if (!this.helpAvailable)
                break label175;
        }
        while (true)
        {
            paramParcel.writeInt(i);
            paramParcel.writeParcelable(this.duration, 0);
            return;
            j = 0;
            break;
            label151: k = 0;
            break label69;
            label157: m = 0;
            break label85;
            label163: n = 0;
            break label101;
            label169: i1 = 0;
            break label117;
            label175: i = 0;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cat.Input
 * JD-Core Version:        0.6.2
 */