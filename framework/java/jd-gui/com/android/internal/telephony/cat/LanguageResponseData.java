package com.android.internal.telephony.cat;

import com.android.internal.telephony.GsmAlphabet;
import java.io.ByteArrayOutputStream;

class LanguageResponseData extends ResponseData
{
    private String lang;

    public LanguageResponseData(String paramString)
    {
        this.lang = paramString;
    }

    public void format(ByteArrayOutputStream paramByteArrayOutputStream)
    {
        if (paramByteArrayOutputStream == null)
            return;
        paramByteArrayOutputStream.write(0x80 | ComprehensionTlvTag.LANGUAGE.value());
        if ((this.lang != null) && (this.lang.length() > 0));
        for (byte[] arrayOfByte1 = GsmAlphabet.stringToGsm8BitPacked(this.lang); ; arrayOfByte1 = new byte[0])
        {
            paramByteArrayOutputStream.write(arrayOfByte1.length);
            byte[] arrayOfByte2 = arrayOfByte1;
            int i = arrayOfByte2.length;
            for (int j = 0; j < i; j++)
                paramByteArrayOutputStream.write(arrayOfByte2[j]);
            break;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cat.LanguageResponseData
 * JD-Core Version:        0.6.2
 */