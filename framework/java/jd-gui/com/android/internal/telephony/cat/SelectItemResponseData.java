package com.android.internal.telephony.cat;

import java.io.ByteArrayOutputStream;

class SelectItemResponseData extends ResponseData
{
    private int id;

    public SelectItemResponseData(int paramInt)
    {
        this.id = paramInt;
    }

    public void format(ByteArrayOutputStream paramByteArrayOutputStream)
    {
        paramByteArrayOutputStream.write(0x80 | ComprehensionTlvTag.ITEM_ID.value());
        paramByteArrayOutputStream.write(1);
        paramByteArrayOutputStream.write(this.id);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cat.SelectItemResponseData
 * JD-Core Version:        0.6.2
 */