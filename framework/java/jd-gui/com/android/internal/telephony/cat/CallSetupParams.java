package com.android.internal.telephony.cat;

import android.graphics.Bitmap;

class CallSetupParams extends CommandParams
{
    TextMessage callMsg;
    TextMessage confirmMsg;

    CallSetupParams(CommandDetails paramCommandDetails, TextMessage paramTextMessage1, TextMessage paramTextMessage2)
    {
        super(paramCommandDetails);
        this.confirmMsg = paramTextMessage1;
        this.callMsg = paramTextMessage2;
    }

    boolean setIcon(Bitmap paramBitmap)
    {
        boolean bool = false;
        if (paramBitmap == null);
        while (true)
        {
            return bool;
            if ((this.confirmMsg != null) && (this.confirmMsg.icon == null))
            {
                this.confirmMsg.icon = paramBitmap;
                bool = true;
            }
            else if ((this.callMsg != null) && (this.callMsg.icon == null))
            {
                this.callMsg.icon = paramBitmap;
                bool = true;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cat.CallSetupParams
 * JD-Core Version:        0.6.2
 */