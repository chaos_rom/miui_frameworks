package com.android.internal.telephony.cat;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Color;
import android.os.AsyncResult;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import com.android.internal.telephony.IccFileHandler;
import java.util.HashMap;

class IconLoader extends Handler
{
    private static final int CLUT_ENTRY_SIZE = 3;
    private static final int CLUT_LOCATION_OFFSET = 4;
    private static final int EVENT_READ_CLUT_DONE = 3;
    private static final int EVENT_READ_EF_IMG_RECOED_DONE = 1;
    private static final int EVENT_READ_ICON_DONE = 2;
    private static final int STATE_MULTI_ICONS = 2;
    private static final int STATE_SINGLE_ICON = 1;
    private static IconLoader sLoader = null;
    private Bitmap mCurrentIcon = null;
    private int mCurrentRecordIndex = 0;
    private Message mEndMsg = null;
    private byte[] mIconData = null;
    private Bitmap[] mIcons = null;
    private HashMap<Integer, Bitmap> mIconsCache = null;
    private ImageDescriptor mId = null;
    private int mRecordNumber;
    private int[] mRecordNumbers = null;
    private IccFileHandler mSimFH = null;
    private int mState = 1;

    private IconLoader(Looper paramLooper, IccFileHandler paramIccFileHandler)
    {
        super(paramLooper);
        this.mSimFH = paramIccFileHandler;
        this.mIconsCache = new HashMap(50);
    }

    private static int bitToBnW(int paramInt)
    {
        if (paramInt == 1);
        for (int i = -1; ; i = -16777216)
            return i;
    }

    static IconLoader getInstance(Handler paramHandler, IccFileHandler paramIccFileHandler)
    {
        IconLoader localIconLoader;
        if (sLoader != null)
            localIconLoader = sLoader;
        while (true)
        {
            return localIconLoader;
            if (paramIccFileHandler != null)
            {
                HandlerThread localHandlerThread = new HandlerThread("Cat Icon Loader");
                localHandlerThread.start();
                localIconLoader = new IconLoader(localHandlerThread.getLooper(), paramIccFileHandler);
            }
            else
            {
                localIconLoader = null;
            }
        }
    }

    private static int getMask(int paramInt)
    {
        int i = 0;
        switch (paramInt)
        {
        default:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        }
        while (true)
        {
            return i;
            i = 1;
            continue;
            i = 3;
            continue;
            i = 7;
            continue;
            i = 15;
            continue;
            i = 31;
            continue;
            i = 63;
            continue;
            i = 127;
            continue;
            i = 255;
        }
    }

    private boolean handleImageDescriptor(byte[] paramArrayOfByte)
    {
        int i = 1;
        this.mId = ImageDescriptor.parse(paramArrayOfByte, i);
        if (this.mId == null)
            i = 0;
        return i;
    }

    public static Bitmap parseToBnW(byte[] paramArrayOfByte, int paramInt)
    {
        int i = 0 + 1;
        int j = 0xFF & paramArrayOfByte[0];
        int k = i + 1;
        int m = 0xFF & paramArrayOfByte[i];
        int n = j * m;
        int[] arrayOfInt = new int[n];
        int i1 = 7;
        int i2 = 0;
        int i3 = 0;
        int i4 = k;
        int i5;
        if (i3 < n)
        {
            if (i3 % 8 != 0)
                break label151;
            i5 = i4 + 1;
            i2 = paramArrayOfByte[i4];
            i1 = 7;
        }
        while (true)
        {
            int i6 = i3 + 1;
            int i7 = i1 - 1;
            arrayOfInt[i3] = bitToBnW(0x1 & i2 >> i1);
            i1 = i7;
            i3 = i6;
            i4 = i5;
            break;
            if (i3 != n)
                CatLog.d("IconLoader", "parseToBnW; size error");
            return Bitmap.createBitmap(arrayOfInt, j, m, Bitmap.Config.ARGB_8888);
            label151: i5 = i4;
        }
    }

    public static Bitmap parseToRGB(byte[] paramArrayOfByte1, int paramInt, boolean paramBoolean, byte[] paramArrayOfByte2)
    {
        int i = 0 + 1;
        int j = 0xFF & paramArrayOfByte1[0];
        int k = i + 1;
        int m = 0xFF & paramArrayOfByte1[i];
        int n = k + 1;
        int i1 = 0xFF & paramArrayOfByte1[k];
        (n + 1);
        int i2 = 0xFF & paramArrayOfByte1[n];
        if (true == paramBoolean)
            paramArrayOfByte2[(i2 - 1)] = 0;
        int i3 = j * m;
        int[] arrayOfInt = new int[i3];
        int i4 = 8 - i1;
        int i5 = i4;
        int i6 = 6 + 1;
        int i7 = paramArrayOfByte1[6];
        int i8 = getMask(i1);
        int i9;
        int i10;
        label130: int i11;
        if (8 % i1 == 0)
        {
            i9 = 1;
            i10 = 0;
            if (i10 >= i3)
                break label239;
            if (i5 >= 0)
                break label252;
            i11 = i6 + 1;
            i7 = paramArrayOfByte1[i6];
            if (i9 == 0)
                break label229;
            i5 = i4;
        }
        while (true)
        {
            int i12 = 3 * (i8 & i7 >> i5);
            int i13 = i10 + 1;
            arrayOfInt[i10] = Color.rgb(paramArrayOfByte2[i12], paramArrayOfByte2[(i12 + 1)], paramArrayOfByte2[(i12 + 2)]);
            i5 -= i1;
            i10 = i13;
            i6 = i11;
            break label130;
            i9 = 0;
            break;
            label229: i5 *= -1;
            continue;
            label239: return Bitmap.createBitmap(arrayOfInt, j, m, Bitmap.Config.ARGB_8888);
            label252: i11 = i6;
        }
    }

    private void postIcon()
    {
        if (this.mState == 1)
        {
            this.mEndMsg.obj = this.mCurrentIcon;
            this.mEndMsg.sendToTarget();
        }
        while (true)
        {
            return;
            if (this.mState == 2)
            {
                Bitmap[] arrayOfBitmap = this.mIcons;
                int i = this.mCurrentRecordIndex;
                this.mCurrentRecordIndex = (i + 1);
                arrayOfBitmap[i] = this.mCurrentIcon;
                if (this.mCurrentRecordIndex < this.mRecordNumbers.length)
                {
                    startLoadingIcon(this.mRecordNumbers[this.mCurrentRecordIndex]);
                }
                else
                {
                    this.mEndMsg.obj = this.mIcons;
                    this.mEndMsg.sendToTarget();
                }
            }
        }
    }

    private void readClut()
    {
        int i = 3 * this.mIconData[3];
        Message localMessage = obtainMessage(3);
        this.mSimFH.loadEFImgTransparent(this.mId.imageId, this.mIconData[4], this.mIconData[5], i, localMessage);
    }

    private void readIconData()
    {
        Message localMessage = obtainMessage(2);
        this.mSimFH.loadEFImgTransparent(this.mId.imageId, 0, 0, this.mId.length, localMessage);
    }

    private void readId()
    {
        if (this.mRecordNumber < 0)
        {
            this.mCurrentIcon = null;
            postIcon();
        }
        while (true)
        {
            return;
            Message localMessage = obtainMessage(1);
            this.mSimFH.loadEFImgLinearFixed(this.mRecordNumber, localMessage);
        }
    }

    private void startLoadingIcon(int paramInt)
    {
        this.mId = null;
        this.mIconData = null;
        this.mCurrentIcon = null;
        this.mRecordNumber = paramInt;
        if (this.mIconsCache.containsKey(Integer.valueOf(paramInt)))
        {
            this.mCurrentIcon = ((Bitmap)this.mIconsCache.get(Integer.valueOf(paramInt)));
            postIcon();
        }
        while (true)
        {
            return;
            readId();
        }
    }

    public void handleMessage(Message paramMessage)
    {
        try
        {
            switch (paramMessage.what)
            {
            case 1:
                if (handleImageDescriptor((byte[])((AsyncResult)paramMessage.obj).result))
                    readIconData();
                break;
            case 2:
            case 3:
            }
        }
        catch (Exception localException)
        {
            CatLog.d(this, "Icon load failed");
            postIcon();
        }
        throw new Exception("Unable to parse image descriptor");
        byte[] arrayOfByte2 = (byte[])((AsyncResult)paramMessage.obj).result;
        if (this.mId.codingScheme == 17)
        {
            this.mCurrentIcon = parseToBnW(arrayOfByte2, arrayOfByte2.length);
            this.mIconsCache.put(Integer.valueOf(this.mRecordNumber), this.mCurrentIcon);
            postIcon();
        }
        else if (this.mId.codingScheme == 33)
        {
            this.mIconData = arrayOfByte2;
            readClut();
            return;
            byte[] arrayOfByte1 = (byte[])((AsyncResult)paramMessage.obj).result;
            this.mCurrentIcon = parseToRGB(this.mIconData, this.mIconData.length, false, arrayOfByte1);
            this.mIconsCache.put(Integer.valueOf(this.mRecordNumber), this.mCurrentIcon);
            postIcon();
        }
    }

    void loadIcon(int paramInt, Message paramMessage)
    {
        if (paramMessage == null);
        while (true)
        {
            return;
            this.mEndMsg = paramMessage;
            this.mState = 1;
            startLoadingIcon(paramInt);
        }
    }

    void loadIcons(int[] paramArrayOfInt, Message paramMessage)
    {
        if ((paramArrayOfInt == null) || (paramArrayOfInt.length == 0) || (paramMessage == null));
        while (true)
        {
            return;
            this.mEndMsg = paramMessage;
            this.mIcons = new Bitmap[paramArrayOfInt.length];
            this.mRecordNumbers = paramArrayOfInt;
            this.mCurrentRecordIndex = 0;
            this.mState = 2;
            startLoadingIcon(paramArrayOfInt[0]);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cat.IconLoader
 * JD-Core Version:        0.6.2
 */