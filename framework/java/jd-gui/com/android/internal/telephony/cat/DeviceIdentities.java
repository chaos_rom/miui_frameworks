package com.android.internal.telephony.cat;

class DeviceIdentities extends ValueObject
{
    public int destinationId;
    public int sourceId;

    ComprehensionTlvTag getTag()
    {
        return ComprehensionTlvTag.DEVICE_IDENTITIES;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cat.DeviceIdentities
 * JD-Core Version:        0.6.2
 */