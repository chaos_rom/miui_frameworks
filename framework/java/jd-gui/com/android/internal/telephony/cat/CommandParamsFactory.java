package com.android.internal.telephony.cat;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import com.android.internal.telephony.GsmAlphabet;
import com.android.internal.telephony.IccFileHandler;
import java.util.Iterator;
import java.util.List;

class CommandParamsFactory extends Handler
{
    static final int DTTZ_SETTING = 3;
    static final int LANGUAGE_SETTING = 4;
    static final int LOAD_MULTI_ICONS = 2;
    static final int LOAD_NO_ICON = 0;
    static final int LOAD_SINGLE_ICON = 1;
    static final int MSG_ID_LOAD_ICON_DONE = 1;
    static final int REFRESH_NAA_INIT = 3;
    static final int REFRESH_NAA_INIT_AND_FILE_CHANGE = 2;
    static final int REFRESH_NAA_INIT_AND_FULL_FILE_CHANGE = 0;
    static final int REFRESH_UICC_RESET = 4;
    private static CommandParamsFactory sInstance = null;
    private RilMessageDecoder mCaller = null;
    private CommandParams mCmdParams = null;
    private int mIconLoadState = 0;
    private IconLoader mIconLoader;

    private CommandParamsFactory(RilMessageDecoder paramRilMessageDecoder, IccFileHandler paramIccFileHandler)
    {
        this.mCaller = paramRilMessageDecoder;
        this.mIconLoader = IconLoader.getInstance(this, paramIccFileHandler);
    }

    /** @deprecated */
    static CommandParamsFactory getInstance(RilMessageDecoder paramRilMessageDecoder, IccFileHandler paramIccFileHandler)
    {
        while (true)
        {
            try
            {
                if (sInstance != null)
                {
                    localCommandParamsFactory = sInstance;
                    return localCommandParamsFactory;
                }
                if (paramIccFileHandler != null)
                {
                    localCommandParamsFactory = new CommandParamsFactory(paramRilMessageDecoder, paramIccFileHandler);
                    continue;
                }
            }
            finally
            {
            }
            CommandParamsFactory localCommandParamsFactory = null;
        }
    }

    private boolean processBIPClient(CommandDetails paramCommandDetails, List<ComprehensionTlv> paramList)
        throws ResultException
    {
        int i = 1;
        AppInterface.CommandType localCommandType = AppInterface.CommandType.fromInt(paramCommandDetails.typeOfCommand);
        if (localCommandType != null)
            CatLog.d(this, "process " + localCommandType.name());
        TextMessage localTextMessage = new TextMessage();
        IconId localIconId = null;
        boolean bool = false;
        ComprehensionTlv localComprehensionTlv1 = searchForTag(ComprehensionTlvTag.ALPHA_ID, paramList);
        if (localComprehensionTlv1 != null)
        {
            localTextMessage.text = ValueParser.retrieveAlphaId(localComprehensionTlv1);
            CatLog.d(this, "alpha TLV text=" + localTextMessage.text);
            bool = true;
        }
        ComprehensionTlv localComprehensionTlv2 = searchForTag(ComprehensionTlvTag.ICON_ID, paramList);
        if (localComprehensionTlv2 != null)
        {
            localIconId = ValueParser.retrieveIconId(localComprehensionTlv2);
            localTextMessage.iconSelfExplanatory = localIconId.selfExplanatory;
        }
        localTextMessage.responseNeeded = false;
        this.mCmdParams = new BIPClientParams(paramCommandDetails, localTextMessage, bool);
        if (localIconId != null)
        {
            this.mIconLoadState = i;
            this.mIconLoader.loadIcon(localIconId.recordNumber, obtainMessage(i));
        }
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    private CommandDetails processCommandDetails(List<ComprehensionTlv> paramList)
    {
        Object localObject = null;
        ComprehensionTlv localComprehensionTlv;
        if (paramList != null)
        {
            localComprehensionTlv = searchForTag(ComprehensionTlvTag.COMMAND_DETAILS, paramList);
            if (localComprehensionTlv == null);
        }
        try
        {
            CommandDetails localCommandDetails = ValueParser.retrieveCommandDetails(localComprehensionTlv);
            localObject = localCommandDetails;
            return localObject;
        }
        catch (ResultException localResultException)
        {
            while (true)
                CatLog.d(this, "processCommandDetails: Failed to procees command details e=" + localResultException);
        }
    }

    private boolean processDisplayText(CommandDetails paramCommandDetails, List<ComprehensionTlv> paramList)
        throws ResultException
    {
        int i = 1;
        CatLog.d(this, "process DisplayText");
        TextMessage localTextMessage = new TextMessage();
        IconId localIconId = null;
        ComprehensionTlv localComprehensionTlv1 = searchForTag(ComprehensionTlvTag.TEXT_STRING, paramList);
        if (localComprehensionTlv1 != null)
            localTextMessage.text = ValueParser.retrieveTextString(localComprehensionTlv1);
        if (localTextMessage.text == null)
            throw new ResultException(ResultCode.CMD_DATA_NOT_UNDERSTOOD);
        if (searchForTag(ComprehensionTlvTag.IMMEDIATE_RESPONSE, paramList) != null)
            localTextMessage.responseNeeded = false;
        ComprehensionTlv localComprehensionTlv2 = searchForTag(ComprehensionTlvTag.ICON_ID, paramList);
        if (localComprehensionTlv2 != null)
        {
            localIconId = ValueParser.retrieveIconId(localComprehensionTlv2);
            localTextMessage.iconSelfExplanatory = localIconId.selfExplanatory;
        }
        ComprehensionTlv localComprehensionTlv3 = searchForTag(ComprehensionTlvTag.DURATION, paramList);
        if (localComprehensionTlv3 != null)
            localTextMessage.duration = ValueParser.retrieveDuration(localComprehensionTlv3);
        if ((0x1 & paramCommandDetails.commandQualifier) != 0)
        {
            int k = i;
            localTextMessage.isHighPriority = k;
            if ((0x80 & paramCommandDetails.commandQualifier) == 0)
                break label227;
            int n = i;
            label171: localTextMessage.userClear = n;
            this.mCmdParams = new DisplayTextParams(paramCommandDetails, localTextMessage);
            if (localIconId == null)
                break label233;
            this.mIconLoadState = i;
            this.mIconLoader.loadIcon(localIconId.recordNumber, obtainMessage(i));
        }
        while (true)
        {
            return i;
            int m = 0;
            break;
            label227: int i1 = 0;
            break label171;
            label233: int j = 0;
        }
    }

    private boolean processEventNotify(CommandDetails paramCommandDetails, List<ComprehensionTlv> paramList)
        throws ResultException
    {
        int i = 1;
        CatLog.d(this, "process EventNotify");
        TextMessage localTextMessage = new TextMessage();
        IconId localIconId = null;
        localTextMessage.text = ValueParser.retrieveAlphaId(searchForTag(ComprehensionTlvTag.ALPHA_ID, paramList));
        ComprehensionTlv localComprehensionTlv = searchForTag(ComprehensionTlvTag.ICON_ID, paramList);
        if (localComprehensionTlv != null)
        {
            localIconId = ValueParser.retrieveIconId(localComprehensionTlv);
            localTextMessage.iconSelfExplanatory = localIconId.selfExplanatory;
        }
        localTextMessage.responseNeeded = false;
        this.mCmdParams = new DisplayTextParams(paramCommandDetails, localTextMessage);
        if (localIconId != null)
        {
            this.mIconLoadState = i;
            this.mIconLoader.loadIcon(localIconId.recordNumber, obtainMessage(i));
        }
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    private boolean processGetInkey(CommandDetails paramCommandDetails, List<ComprehensionTlv> paramList)
        throws ResultException
    {
        int i = 1;
        CatLog.d(this, "process GetInkey");
        Input localInput = new Input();
        IconId localIconId = null;
        ComprehensionTlv localComprehensionTlv1 = searchForTag(ComprehensionTlvTag.TEXT_STRING, paramList);
        if (localComprehensionTlv1 != null)
        {
            localInput.text = ValueParser.retrieveTextString(localComprehensionTlv1);
            ComprehensionTlv localComprehensionTlv2 = searchForTag(ComprehensionTlvTag.ICON_ID, paramList);
            if (localComprehensionTlv2 != null)
                localIconId = ValueParser.retrieveIconId(localComprehensionTlv2);
            ComprehensionTlv localComprehensionTlv3 = searchForTag(ComprehensionTlvTag.DURATION, paramList);
            if (localComprehensionTlv3 != null)
                localInput.duration = ValueParser.retrieveDuration(localComprehensionTlv3);
            localInput.minLen = i;
            localInput.maxLen = i;
            if ((0x1 & paramCommandDetails.commandQualifier) != 0)
                break label242;
            int k = i;
            localInput.digitOnly = k;
            if ((0x2 & paramCommandDetails.commandQualifier) == 0)
                break label248;
            int n = i;
            label135: localInput.ucs2 = n;
            if ((0x4 & paramCommandDetails.commandQualifier) == 0)
                break label254;
            int i2 = i;
            label154: localInput.yesNo = i2;
            if ((0x80 & paramCommandDetails.commandQualifier) == 0)
                break label260;
            int i4 = i;
            label175: localInput.helpAvailable = i4;
            localInput.echo = i;
            this.mCmdParams = new GetInputParams(paramCommandDetails, localInput);
            if (localIconId == null)
                break label266;
            this.mIconLoadState = i;
            this.mIconLoader.loadIcon(localIconId.recordNumber, obtainMessage(i));
        }
        while (true)
        {
            return i;
            throw new ResultException(ResultCode.REQUIRED_VALUES_MISSING);
            label242: int m = 0;
            break;
            label248: int i1 = 0;
            break label135;
            label254: int i3 = 0;
            break label154;
            label260: int i5 = 0;
            break label175;
            label266: int j = 0;
        }
    }

    private boolean processGetInput(CommandDetails paramCommandDetails, List<ComprehensionTlv> paramList)
        throws ResultException
    {
        int i = 1;
        CatLog.d(this, "process GetInput");
        Input localInput = new Input();
        IconId localIconId = null;
        ComprehensionTlv localComprehensionTlv1 = searchForTag(ComprehensionTlvTag.TEXT_STRING, paramList);
        ComprehensionTlv localComprehensionTlv2;
        if (localComprehensionTlv1 != null)
        {
            localInput.text = ValueParser.retrieveTextString(localComprehensionTlv1);
            localComprehensionTlv2 = searchForTag(ComprehensionTlvTag.RESPONSE_LENGTH, paramList);
            if (localComprehensionTlv2 == null)
                break label317;
        }
        while (true)
        {
            try
            {
                byte[] arrayOfByte = localComprehensionTlv2.getRawValue();
                int k = localComprehensionTlv2.getValueIndex();
                localInput.minLen = (0xFF & arrayOfByte[k]);
                localInput.maxLen = (0xFF & arrayOfByte[(k + 1)]);
                ComprehensionTlv localComprehensionTlv3 = searchForTag(ComprehensionTlvTag.DEFAULT_TEXT, paramList);
                if (localComprehensionTlv3 != null)
                    localInput.defaultText = ValueParser.retrieveTextString(localComprehensionTlv3);
                ComprehensionTlv localComprehensionTlv4 = searchForTag(ComprehensionTlvTag.ICON_ID, paramList);
                if (localComprehensionTlv4 != null)
                    localIconId = ValueParser.retrieveIconId(localComprehensionTlv4);
                if ((0x1 & paramCommandDetails.commandQualifier) != 0)
                    break label328;
                int m = i;
                localInput.digitOnly = m;
                if ((0x2 & paramCommandDetails.commandQualifier) == 0)
                    break label334;
                int i1 = i;
                localInput.ucs2 = i1;
                if ((0x4 & paramCommandDetails.commandQualifier) != 0)
                    break label340;
                int i3 = i;
                localInput.echo = i3;
                if ((0x8 & paramCommandDetails.commandQualifier) == 0)
                    break label346;
                int i5 = i;
                localInput.packed = i5;
                if ((0x80 & paramCommandDetails.commandQualifier) == 0)
                    break label352;
                int i7 = i;
                localInput.helpAvailable = i7;
                this.mCmdParams = new GetInputParams(paramCommandDetails, localInput);
                if (localIconId == null)
                    break label358;
                this.mIconLoadState = i;
                this.mIconLoader.loadIcon(localIconId.recordNumber, obtainMessage(i));
                return i;
                throw new ResultException(ResultCode.REQUIRED_VALUES_MISSING);
            }
            catch (IndexOutOfBoundsException localIndexOutOfBoundsException)
            {
                throw new ResultException(ResultCode.CMD_DATA_NOT_UNDERSTOOD);
            }
            label317: throw new ResultException(ResultCode.REQUIRED_VALUES_MISSING);
            label328: int n = 0;
            continue;
            label334: int i2 = 0;
            continue;
            label340: int i4 = 0;
            continue;
            label346: int i6 = 0;
            continue;
            label352: int i8 = 0;
            continue;
            label358: int j = 0;
        }
    }

    private boolean processLaunchBrowser(CommandDetails paramCommandDetails, List<ComprehensionTlv> paramList)
        throws ResultException
    {
        int i = 1;
        CatLog.d(this, "process LaunchBrowser");
        TextMessage localTextMessage = new TextMessage();
        IconId localIconId = null;
        Object localObject = null;
        ComprehensionTlv localComprehensionTlv1 = searchForTag(ComprehensionTlvTag.URL, paramList);
        if (localComprehensionTlv1 != null);
        while (true)
        {
            try
            {
                byte[] arrayOfByte = localComprehensionTlv1.getRawValue();
                int k = localComprehensionTlv1.getValueIndex();
                int m = localComprehensionTlv1.getLength();
                if (m > 0)
                {
                    String str = GsmAlphabet.gsm8BitUnpackedToString(arrayOfByte, k, m);
                    localObject = str;
                    localTextMessage.text = ValueParser.retrieveAlphaId(searchForTag(ComprehensionTlvTag.ALPHA_ID, paramList));
                    ComprehensionTlv localComprehensionTlv2 = searchForTag(ComprehensionTlvTag.ICON_ID, paramList);
                    if (localComprehensionTlv2 != null)
                    {
                        localIconId = ValueParser.retrieveIconId(localComprehensionTlv2);
                        localTextMessage.iconSelfExplanatory = localIconId.selfExplanatory;
                    }
                }
                switch (paramCommandDetails.commandQualifier)
                {
                default:
                    localLaunchBrowserMode = LaunchBrowserMode.LAUNCH_IF_NOT_ALREADY_LAUNCHED;
                    this.mCmdParams = new LaunchBrowserParams(paramCommandDetails, localTextMessage, localObject, localLaunchBrowserMode);
                    if (localIconId == null)
                        break label243;
                    this.mIconLoadState = i;
                    this.mIconLoader.loadIcon(localIconId.recordNumber, obtainMessage(i));
                    return i;
                    localObject = null;
                    continue;
                case 2:
                case 3:
                }
            }
            catch (IndexOutOfBoundsException localIndexOutOfBoundsException)
            {
                throw new ResultException(ResultCode.CMD_DATA_NOT_UNDERSTOOD);
            }
            LaunchBrowserMode localLaunchBrowserMode = LaunchBrowserMode.USE_EXISTING_BROWSER;
            continue;
            localLaunchBrowserMode = LaunchBrowserMode.LAUNCH_NEW_BROWSER;
            continue;
            label243: int j = 0;
        }
    }

    private boolean processPlayTone(CommandDetails paramCommandDetails, List<ComprehensionTlv> paramList)
        throws ResultException
    {
        CatLog.d(this, "process PlayTone");
        Object localObject = null;
        TextMessage localTextMessage = new TextMessage();
        Duration localDuration = null;
        IconId localIconId = null;
        ComprehensionTlv localComprehensionTlv1 = searchForTag(ComprehensionTlvTag.TONE, paramList);
        if ((localComprehensionTlv1 != null) && (localComprehensionTlv1.getLength() > 0));
        while (true)
        {
            try
            {
                Tone localTone = Tone.fromInt(localComprehensionTlv1.getRawValue()[localComprehensionTlv1.getValueIndex()]);
                localObject = localTone;
                ComprehensionTlv localComprehensionTlv2 = searchForTag(ComprehensionTlvTag.ALPHA_ID, paramList);
                if (localComprehensionTlv2 != null)
                    localTextMessage.text = ValueParser.retrieveAlphaId(localComprehensionTlv2);
                ComprehensionTlv localComprehensionTlv3 = searchForTag(ComprehensionTlvTag.DURATION, paramList);
                if (localComprehensionTlv3 != null)
                    localDuration = ValueParser.retrieveDuration(localComprehensionTlv3);
                ComprehensionTlv localComprehensionTlv4 = searchForTag(ComprehensionTlvTag.ICON_ID, paramList);
                if (localComprehensionTlv4 != null)
                {
                    localIconId = ValueParser.retrieveIconId(localComprehensionTlv4);
                    localTextMessage.iconSelfExplanatory = localIconId.selfExplanatory;
                }
                if ((0x1 & paramCommandDetails.commandQualifier) != 0)
                {
                    bool1 = true;
                    localTextMessage.responseNeeded = false;
                    this.mCmdParams = new PlayToneParams(paramCommandDetails, localTextMessage, localObject, localDuration, bool1);
                    if (localIconId == null)
                        break label234;
                    this.mIconLoadState = 1;
                    this.mIconLoader.loadIcon(localIconId.recordNumber, obtainMessage(1));
                    bool2 = true;
                    return bool2;
                }
            }
            catch (IndexOutOfBoundsException localIndexOutOfBoundsException)
            {
                throw new ResultException(ResultCode.CMD_DATA_NOT_UNDERSTOOD);
            }
            boolean bool1 = false;
            continue;
            label234: boolean bool2 = false;
        }
    }

    private boolean processProvideLocalInfo(CommandDetails paramCommandDetails, List<ComprehensionTlv> paramList)
        throws ResultException
    {
        CatLog.d(this, "process ProvideLocalInfo");
        switch (paramCommandDetails.commandQualifier)
        {
        default:
            CatLog.d(this, "PLI[" + paramCommandDetails.commandQualifier + "] Command Not Supported");
            this.mCmdParams = new CommandParams(paramCommandDetails);
            throw new ResultException(ResultCode.BEYOND_TERMINAL_CAPABILITY);
        case 3:
            CatLog.d(this, "PLI [DTTZ_SETTING]");
        case 4:
        }
        for (this.mCmdParams = new CommandParams(paramCommandDetails); ; this.mCmdParams = new CommandParams(paramCommandDetails))
        {
            return false;
            CatLog.d(this, "PLI [LANGUAGE_SETTING]");
        }
    }

    private boolean processRefresh(CommandDetails paramCommandDetails, List<ComprehensionTlv> paramList)
    {
        CatLog.d(this, "process Refresh");
        switch (paramCommandDetails.commandQualifier)
        {
        case 1:
        default:
        case 0:
        case 2:
        case 3:
        case 4:
        }
        while (true)
        {
            return false;
            this.mCmdParams = new DisplayTextParams(paramCommandDetails, null);
        }
    }

    private boolean processSelectItem(CommandDetails paramCommandDetails, List<ComprehensionTlv> paramList)
        throws ResultException
    {
        boolean bool1 = false;
        CatLog.d(this, "process SelectItem");
        Menu localMenu = new Menu();
        IconId localIconId = null;
        ItemsIconId localItemsIconId = null;
        Iterator localIterator = paramList.iterator();
        ComprehensionTlv localComprehensionTlv1 = searchForTag(ComprehensionTlvTag.ALPHA_ID, paramList);
        if (localComprehensionTlv1 != null)
            localMenu.title = ValueParser.retrieveAlphaId(localComprehensionTlv1);
        while (true)
        {
            ComprehensionTlv localComprehensionTlv2 = searchForNextTag(ComprehensionTlvTag.ITEM, localIterator);
            if (localComprehensionTlv2 == null)
                break;
            localMenu.items.add(ValueParser.retrieveItem(localComprehensionTlv2));
        }
        if (localMenu.items.size() == 0)
            throw new ResultException(ResultCode.REQUIRED_VALUES_MISSING);
        ComprehensionTlv localComprehensionTlv3 = searchForTag(ComprehensionTlvTag.ITEM_ID, paramList);
        if (localComprehensionTlv3 != null)
            localMenu.defaultItem = (-1 + ValueParser.retrieveItemId(localComprehensionTlv3));
        ComprehensionTlv localComprehensionTlv4 = searchForTag(ComprehensionTlvTag.ICON_ID, paramList);
        if (localComprehensionTlv4 != null)
        {
            this.mIconLoadState = 1;
            localIconId = ValueParser.retrieveIconId(localComprehensionTlv4);
            localMenu.titleIconSelfExplanatory = localIconId.selfExplanatory;
        }
        ComprehensionTlv localComprehensionTlv5 = searchForTag(ComprehensionTlvTag.ITEM_ICON_ID_LIST, paramList);
        if (localComprehensionTlv5 != null)
        {
            this.mIconLoadState = 2;
            localItemsIconId = ValueParser.retrieveItemsIconId(localComprehensionTlv5);
            localMenu.itemsIconSelfExplanatory = localItemsIconId.selfExplanatory;
        }
        int i;
        label252: boolean bool2;
        label264: boolean bool3;
        label285: boolean bool4;
        if ((0x1 & paramCommandDetails.commandQualifier) != 0)
        {
            i = 1;
            if (i != 0)
            {
                if ((0x2 & paramCommandDetails.commandQualifier) != 0)
                    break label358;
                localMenu.presentationType = PresentationType.DATA_VALUES;
            }
            if ((0x4 & paramCommandDetails.commandQualifier) == 0)
                break label369;
            bool2 = true;
            localMenu.softKeyPreferred = bool2;
            if ((0x80 & paramCommandDetails.commandQualifier) == 0)
                break label375;
            bool3 = true;
            localMenu.helpAvailable = bool3;
            if (localIconId == null)
                break label381;
            bool4 = true;
            label300: this.mCmdParams = new SelectItemParams(paramCommandDetails, localMenu, bool4);
            switch (this.mIconLoadState)
            {
            default:
            case 0:
            case 1:
            case 2:
            }
        }
        while (true)
        {
            bool1 = true;
            return bool1;
            i = 0;
            break;
            label358: localMenu.presentationType = PresentationType.NAVIGATION_OPTIONS;
            break label252;
            label369: bool2 = false;
            break label264;
            label375: bool3 = false;
            break label285;
            label381: bool4 = false;
            break label300;
            this.mIconLoader.loadIcon(localIconId.recordNumber, obtainMessage(1));
            continue;
            int[] arrayOfInt = localItemsIconId.recordNumbers;
            if (localIconId != null)
            {
                arrayOfInt = new int[1 + localItemsIconId.recordNumbers.length];
                arrayOfInt[bool1] = localIconId.recordNumber;
                System.arraycopy(localItemsIconId.recordNumbers, 0, arrayOfInt, 1, localItemsIconId.recordNumbers.length);
            }
            this.mIconLoader.loadIcons(arrayOfInt, obtainMessage(1));
        }
    }

    private boolean processSetUpEventList(CommandDetails paramCommandDetails, List<ComprehensionTlv> paramList)
    {
        CatLog.d(this, "process SetUpEventList");
        return true;
    }

    private boolean processSetUpIdleModeText(CommandDetails paramCommandDetails, List<ComprehensionTlv> paramList)
        throws ResultException
    {
        int i = 1;
        CatLog.d(this, "process SetUpIdleModeText");
        TextMessage localTextMessage = new TextMessage();
        IconId localIconId = null;
        ComprehensionTlv localComprehensionTlv1 = searchForTag(ComprehensionTlvTag.TEXT_STRING, paramList);
        if (localComprehensionTlv1 != null)
            localTextMessage.text = ValueParser.retrieveTextString(localComprehensionTlv1);
        if (localTextMessage.text != null)
        {
            ComprehensionTlv localComprehensionTlv2 = searchForTag(ComprehensionTlvTag.ICON_ID, paramList);
            if (localComprehensionTlv2 != null)
            {
                localIconId = ValueParser.retrieveIconId(localComprehensionTlv2);
                localTextMessage.iconSelfExplanatory = localIconId.selfExplanatory;
            }
        }
        this.mCmdParams = new DisplayTextParams(paramCommandDetails, localTextMessage);
        if (localIconId != null)
        {
            this.mIconLoadState = i;
            this.mIconLoader.loadIcon(localIconId.recordNumber, obtainMessage(i));
        }
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    private boolean processSetupCall(CommandDetails paramCommandDetails, List<ComprehensionTlv> paramList)
        throws ResultException
    {
        int i = -1;
        CatLog.d(this, "process SetupCall");
        Iterator localIterator = paramList.iterator();
        TextMessage localTextMessage1 = new TextMessage();
        TextMessage localTextMessage2 = new TextMessage();
        IconId localIconId1 = null;
        IconId localIconId2 = null;
        localTextMessage1.text = ValueParser.retrieveAlphaId(searchForNextTag(ComprehensionTlvTag.ALPHA_ID, localIterator));
        ComprehensionTlv localComprehensionTlv1 = searchForTag(ComprehensionTlvTag.ICON_ID, paramList);
        if (localComprehensionTlv1 != null)
        {
            localIconId1 = ValueParser.retrieveIconId(localComprehensionTlv1);
            localTextMessage1.iconSelfExplanatory = localIconId1.selfExplanatory;
        }
        ComprehensionTlv localComprehensionTlv2 = searchForNextTag(ComprehensionTlvTag.ALPHA_ID, localIterator);
        if (localComprehensionTlv2 != null)
            localTextMessage2.text = ValueParser.retrieveAlphaId(localComprehensionTlv2);
        ComprehensionTlv localComprehensionTlv3 = searchForTag(ComprehensionTlvTag.ICON_ID, paramList);
        if (localComprehensionTlv3 != null)
        {
            localIconId2 = ValueParser.retrieveIconId(localComprehensionTlv3);
            localTextMessage2.iconSelfExplanatory = localIconId2.selfExplanatory;
        }
        this.mCmdParams = new CallSetupParams(paramCommandDetails, localTextMessage1, localTextMessage2);
        int j;
        if ((localIconId1 != null) || (localIconId2 != null))
        {
            this.mIconLoadState = 2;
            int[] arrayOfInt = new int[2];
            if (localIconId1 != null)
            {
                j = localIconId1.recordNumber;
                arrayOfInt[0] = j;
                if (localIconId2 != null)
                    i = localIconId2.recordNumber;
                arrayOfInt[1] = i;
                this.mIconLoader.loadIcons(arrayOfInt, obtainMessage(1));
            }
        }
        for (boolean bool = true; ; bool = false)
        {
            return bool;
            j = i;
            break;
        }
    }

    private ComprehensionTlv searchForNextTag(ComprehensionTlvTag paramComprehensionTlvTag, Iterator<ComprehensionTlv> paramIterator)
    {
        int i = paramComprehensionTlvTag.value();
        ComprehensionTlv localComprehensionTlv;
        do
        {
            if (!paramIterator.hasNext())
                break;
            localComprehensionTlv = (ComprehensionTlv)paramIterator.next();
        }
        while (localComprehensionTlv.getTag() != i);
        while (true)
        {
            return localComprehensionTlv;
            localComprehensionTlv = null;
        }
    }

    private ComprehensionTlv searchForTag(ComprehensionTlvTag paramComprehensionTlvTag, List<ComprehensionTlv> paramList)
    {
        return searchForNextTag(paramComprehensionTlvTag, paramList.iterator());
    }

    private void sendCmdParams(ResultCode paramResultCode)
    {
        this.mCaller.sendMsgParamsDecoded(paramResultCode, this.mCmdParams);
    }

    private ResultCode setIcons(Object paramObject)
    {
        ResultCode localResultCode;
        if (paramObject == null)
        {
            localResultCode = ResultCode.PRFRMD_ICON_NOT_DISPLAYED;
            return localResultCode;
        }
        switch (this.mIconLoadState)
        {
        default:
        case 1:
        case 2:
        }
        while (true)
        {
            localResultCode = ResultCode.OK;
            break;
            this.mCmdParams.setIcon((Bitmap)paramObject);
            continue;
            for (Bitmap localBitmap : (Bitmap[])paramObject)
                this.mCmdParams.setIcon(localBitmap);
        }
    }

    public void handleMessage(Message paramMessage)
    {
        switch (paramMessage.what)
        {
        default:
        case 1:
        }
        while (true)
        {
            return;
            sendCmdParams(setIcons(paramMessage.obj));
        }
    }

    void make(BerTlv paramBerTlv)
    {
        if (paramBerTlv == null);
        label432: 
        while (true)
        {
            return;
            this.mCmdParams = null;
            this.mIconLoadState = 0;
            if (paramBerTlv.getTag() != 208)
            {
                sendCmdParams(ResultCode.CMD_TYPE_NOT_UNDERSTOOD);
            }
            else
            {
                List localList = paramBerTlv.getComprehensionTlvs();
                CommandDetails localCommandDetails = processCommandDetails(localList);
                if (localCommandDetails == null)
                {
                    sendCmdParams(ResultCode.CMD_TYPE_NOT_UNDERSTOOD);
                }
                else
                {
                    AppInterface.CommandType localCommandType = AppInterface.CommandType.fromInt(localCommandDetails.typeOfCommand);
                    if (localCommandType == null)
                    {
                        this.mCmdParams = new CommandParams(localCommandDetails);
                        sendCmdParams(ResultCode.BEYOND_TERMINAL_CAPABILITY);
                    }
                    else
                    {
                        try
                        {
                            switch (1.$SwitchMap$com$android$internal$telephony$cat$AppInterface$CommandType[localCommandType.ordinal()])
                            {
                            default:
                                this.mCmdParams = new CommandParams(localCommandDetails);
                                sendCmdParams(ResultCode.BEYOND_TERMINAL_CAPABILITY);
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                            case 5:
                            case 6:
                            case 7:
                            case 8:
                            case 9:
                            case 10:
                            case 11:
                            case 12:
                            case 13:
                            case 14:
                            case 15:
                            case 16:
                            case 17:
                            case 18:
                            case 19:
                            }
                        }
                        catch (ResultException localResultException)
                        {
                            CatLog.d(this, "make: caught ResultException e=" + localResultException);
                            this.mCmdParams = new CommandParams(localCommandDetails);
                            sendCmdParams(localResultException.result());
                        }
                        continue;
                        boolean bool3 = processSelectItem(localCommandDetails, localList);
                        boolean bool2 = bool3;
                        while (true)
                        {
                            if (bool2)
                                break label432;
                            sendCmdParams(ResultCode.OK);
                            break;
                            bool2 = processSelectItem(localCommandDetails, localList);
                            continue;
                            bool2 = processDisplayText(localCommandDetails, localList);
                            continue;
                            bool2 = processSetUpIdleModeText(localCommandDetails, localList);
                            continue;
                            bool2 = processGetInkey(localCommandDetails, localList);
                            continue;
                            bool2 = processGetInput(localCommandDetails, localList);
                            continue;
                            bool2 = processEventNotify(localCommandDetails, localList);
                            continue;
                            bool2 = processSetupCall(localCommandDetails, localList);
                            continue;
                            processRefresh(localCommandDetails, localList);
                            bool2 = false;
                            continue;
                            bool2 = processLaunchBrowser(localCommandDetails, localList);
                            continue;
                            bool2 = processPlayTone(localCommandDetails, localList);
                            continue;
                            bool2 = processProvideLocalInfo(localCommandDetails, localList);
                            continue;
                            boolean bool1 = processBIPClient(localCommandDetails, localList);
                            bool2 = bool1;
                        }
                    }
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cat.CommandParamsFactory
 * JD-Core Version:        0.6.2
 */