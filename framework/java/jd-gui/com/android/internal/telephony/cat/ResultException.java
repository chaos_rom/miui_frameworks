package com.android.internal.telephony.cat;

public class ResultException extends CatException
{
    private int mAdditionalInfo;
    private String mExplanation;
    private ResultCode mResult;

    public ResultException(ResultCode paramResultCode)
    {
        switch (1.$SwitchMap$com$android$internal$telephony$cat$ResultCode[paramResultCode.ordinal()])
        {
        default:
            this.mResult = paramResultCode;
            this.mAdditionalInfo = -1;
            this.mExplanation = "";
            return;
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        }
        throw new AssertionError("For result code, " + paramResultCode + ", additional information must be given!");
    }

    public ResultException(ResultCode paramResultCode, int paramInt)
    {
        this(paramResultCode);
        if (paramInt < 0)
            throw new AssertionError("Additional info must be greater than zero!");
        this.mAdditionalInfo = paramInt;
    }

    public ResultException(ResultCode paramResultCode, int paramInt, String paramString)
    {
        this(paramResultCode, paramInt);
        this.mExplanation = paramString;
    }

    public ResultException(ResultCode paramResultCode, String paramString)
    {
        this(paramResultCode);
        this.mExplanation = paramString;
    }

    public int additionalInfo()
    {
        return this.mAdditionalInfo;
    }

    public String explanation()
    {
        return this.mExplanation;
    }

    public boolean hasAdditionalInfo()
    {
        if (this.mAdditionalInfo >= 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public ResultCode result()
    {
        return this.mResult;
    }

    public String toString()
    {
        return "result=" + this.mResult + " additionalInfo=" + this.mAdditionalInfo + " explantion=" + this.mExplanation;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cat.ResultException
 * JD-Core Version:        0.6.2
 */