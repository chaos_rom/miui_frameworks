package com.android.internal.telephony.cat;

public enum FontSize
{
    private int mValue;

    static
    {
        LARGE = new FontSize("LARGE", 1, 1);
        SMALL = new FontSize("SMALL", 2, 2);
        FontSize[] arrayOfFontSize = new FontSize[3];
        arrayOfFontSize[0] = NORMAL;
        arrayOfFontSize[1] = LARGE;
        arrayOfFontSize[2] = SMALL;
    }

    private FontSize(int paramInt)
    {
        this.mValue = paramInt;
    }

    public static FontSize fromInt(int paramInt)
    {
        FontSize[] arrayOfFontSize = values();
        int i = arrayOfFontSize.length;
        int j = 0;
        FontSize localFontSize;
        if (j < i)
        {
            localFontSize = arrayOfFontSize[j];
            if (localFontSize.mValue != paramInt);
        }
        while (true)
        {
            return localFontSize;
            j++;
            break;
            localFontSize = null;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cat.FontSize
 * JD-Core Version:        0.6.2
 */