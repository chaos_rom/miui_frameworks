package com.android.internal.telephony.cat;

public enum ComprehensionTlvTag
{
    private int mValue;

    static
    {
        DURATION = new ComprehensionTlvTag("DURATION", 3, 4);
        ALPHA_ID = new ComprehensionTlvTag("ALPHA_ID", 4, 5);
        ADDRESS = new ComprehensionTlvTag("ADDRESS", 5, 6);
        USSD_STRING = new ComprehensionTlvTag("USSD_STRING", 6, 10);
        SMS_TPDU = new ComprehensionTlvTag("SMS_TPDU", 7, 11);
        TEXT_STRING = new ComprehensionTlvTag("TEXT_STRING", 8, 13);
        TONE = new ComprehensionTlvTag("TONE", 9, 14);
        ITEM = new ComprehensionTlvTag("ITEM", 10, 15);
        ITEM_ID = new ComprehensionTlvTag("ITEM_ID", 11, 16);
        RESPONSE_LENGTH = new ComprehensionTlvTag("RESPONSE_LENGTH", 12, 17);
        FILE_LIST = new ComprehensionTlvTag("FILE_LIST", 13, 18);
        HELP_REQUEST = new ComprehensionTlvTag("HELP_REQUEST", 14, 21);
        DEFAULT_TEXT = new ComprehensionTlvTag("DEFAULT_TEXT", 15, 23);
        EVENT_LIST = new ComprehensionTlvTag("EVENT_LIST", 16, 25);
        ICON_ID = new ComprehensionTlvTag("ICON_ID", 17, 30);
        ITEM_ICON_ID_LIST = new ComprehensionTlvTag("ITEM_ICON_ID_LIST", 18, 31);
        IMMEDIATE_RESPONSE = new ComprehensionTlvTag("IMMEDIATE_RESPONSE", 19, 43);
        LANGUAGE = new ComprehensionTlvTag("LANGUAGE", 20, 45);
        URL = new ComprehensionTlvTag("URL", 21, 49);
        BROWSER_TERMINATION_CAUSE = new ComprehensionTlvTag("BROWSER_TERMINATION_CAUSE", 22, 52);
        TEXT_ATTRIBUTE = new ComprehensionTlvTag("TEXT_ATTRIBUTE", 23, 80);
        ComprehensionTlvTag[] arrayOfComprehensionTlvTag = new ComprehensionTlvTag[24];
        arrayOfComprehensionTlvTag[0] = COMMAND_DETAILS;
        arrayOfComprehensionTlvTag[1] = DEVICE_IDENTITIES;
        arrayOfComprehensionTlvTag[2] = RESULT;
        arrayOfComprehensionTlvTag[3] = DURATION;
        arrayOfComprehensionTlvTag[4] = ALPHA_ID;
        arrayOfComprehensionTlvTag[5] = ADDRESS;
        arrayOfComprehensionTlvTag[6] = USSD_STRING;
        arrayOfComprehensionTlvTag[7] = SMS_TPDU;
        arrayOfComprehensionTlvTag[8] = TEXT_STRING;
        arrayOfComprehensionTlvTag[9] = TONE;
        arrayOfComprehensionTlvTag[10] = ITEM;
        arrayOfComprehensionTlvTag[11] = ITEM_ID;
        arrayOfComprehensionTlvTag[12] = RESPONSE_LENGTH;
        arrayOfComprehensionTlvTag[13] = FILE_LIST;
        arrayOfComprehensionTlvTag[14] = HELP_REQUEST;
        arrayOfComprehensionTlvTag[15] = DEFAULT_TEXT;
        arrayOfComprehensionTlvTag[16] = EVENT_LIST;
        arrayOfComprehensionTlvTag[17] = ICON_ID;
        arrayOfComprehensionTlvTag[18] = ITEM_ICON_ID_LIST;
        arrayOfComprehensionTlvTag[19] = IMMEDIATE_RESPONSE;
        arrayOfComprehensionTlvTag[20] = LANGUAGE;
        arrayOfComprehensionTlvTag[21] = URL;
        arrayOfComprehensionTlvTag[22] = BROWSER_TERMINATION_CAUSE;
        arrayOfComprehensionTlvTag[23] = TEXT_ATTRIBUTE;
    }

    private ComprehensionTlvTag(int paramInt)
    {
        this.mValue = paramInt;
    }

    public static ComprehensionTlvTag fromInt(int paramInt)
    {
        ComprehensionTlvTag[] arrayOfComprehensionTlvTag = values();
        int i = arrayOfComprehensionTlvTag.length;
        int j = 0;
        ComprehensionTlvTag localComprehensionTlvTag;
        if (j < i)
        {
            localComprehensionTlvTag = arrayOfComprehensionTlvTag[j];
            if (localComprehensionTlvTag.mValue != paramInt);
        }
        while (true)
        {
            return localComprehensionTlvTag;
            j++;
            break;
            localComprehensionTlvTag = null;
        }
    }

    public int value()
    {
        return this.mValue;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cat.ComprehensionTlvTag
 * JD-Core Version:        0.6.2
 */