package com.android.internal.telephony.cat;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public enum Tone
    implements Parcelable
{
    public static final Parcelable.Creator<Tone> CREATOR = new Parcelable.Creator()
    {
        public Tone createFromParcel(Parcel paramAnonymousParcel)
        {
            return Tone.values()[paramAnonymousParcel.readInt()];
        }

        public Tone[] newArray(int paramAnonymousInt)
        {
            return new Tone[paramAnonymousInt];
        }
    };
    private int mValue;

    static
    {
        BUSY = new Tone("BUSY", 1, 2);
        CONGESTION = new Tone("CONGESTION", 2, 3);
        RADIO_PATH_ACK = new Tone("RADIO_PATH_ACK", 3, 4);
        RADIO_PATH_NOT_AVAILABLE = new Tone("RADIO_PATH_NOT_AVAILABLE", 4, 5);
        ERROR_SPECIAL_INFO = new Tone("ERROR_SPECIAL_INFO", 5, 6);
        CALL_WAITING = new Tone("CALL_WAITING", 6, 7);
        RINGING = new Tone("RINGING", 7, 8);
        GENERAL_BEEP = new Tone("GENERAL_BEEP", 8, 16);
        POSITIVE_ACK = new Tone("POSITIVE_ACK", 9, 17);
        NEGATIVE_ACK = new Tone("NEGATIVE_ACK", 10, 18);
        INCOMING_SPEECH_CALL = new Tone("INCOMING_SPEECH_CALL", 11, 19);
        INCOMING_SMS = new Tone("INCOMING_SMS", 12, 20);
        CRITICAL_ALERT = new Tone("CRITICAL_ALERT", 13, 21);
        VIBRATE_ONLY = new Tone("VIBRATE_ONLY", 14, 32);
        HAPPY = new Tone("HAPPY", 15, 48);
        SAD = new Tone("SAD", 16, 49);
        URGENT = new Tone("URGENT", 17, 50);
        QUESTION = new Tone("QUESTION", 18, 51);
        MESSAGE_RECEIVED = new Tone("MESSAGE_RECEIVED", 19, 52);
        MELODY_1 = new Tone("MELODY_1", 20, 64);
        MELODY_2 = new Tone("MELODY_2", 21, 65);
        MELODY_3 = new Tone("MELODY_3", 22, 66);
        MELODY_4 = new Tone("MELODY_4", 23, 67);
        MELODY_5 = new Tone("MELODY_5", 24, 68);
        MELODY_6 = new Tone("MELODY_6", 25, 69);
        MELODY_7 = new Tone("MELODY_7", 26, 70);
        MELODY_8 = new Tone("MELODY_8", 27, 71);
        Tone[] arrayOfTone = new Tone[28];
        arrayOfTone[0] = DIAL;
        arrayOfTone[1] = BUSY;
        arrayOfTone[2] = CONGESTION;
        arrayOfTone[3] = RADIO_PATH_ACK;
        arrayOfTone[4] = RADIO_PATH_NOT_AVAILABLE;
        arrayOfTone[5] = ERROR_SPECIAL_INFO;
        arrayOfTone[6] = CALL_WAITING;
        arrayOfTone[7] = RINGING;
        arrayOfTone[8] = GENERAL_BEEP;
        arrayOfTone[9] = POSITIVE_ACK;
        arrayOfTone[10] = NEGATIVE_ACK;
        arrayOfTone[11] = INCOMING_SPEECH_CALL;
        arrayOfTone[12] = INCOMING_SMS;
        arrayOfTone[13] = CRITICAL_ALERT;
        arrayOfTone[14] = VIBRATE_ONLY;
        arrayOfTone[15] = HAPPY;
        arrayOfTone[16] = SAD;
        arrayOfTone[17] = URGENT;
        arrayOfTone[18] = QUESTION;
        arrayOfTone[19] = MESSAGE_RECEIVED;
        arrayOfTone[20] = MELODY_1;
        arrayOfTone[21] = MELODY_2;
        arrayOfTone[22] = MELODY_3;
        arrayOfTone[23] = MELODY_4;
        arrayOfTone[24] = MELODY_5;
        arrayOfTone[25] = MELODY_6;
        arrayOfTone[26] = MELODY_7;
        arrayOfTone[27] = MELODY_8;
    }

    private Tone(int paramInt)
    {
        this.mValue = paramInt;
    }

    private Tone(Parcel paramParcel)
    {
        this.mValue = paramParcel.readInt();
    }

    public static Tone fromInt(int paramInt)
    {
        Tone[] arrayOfTone = values();
        int i = arrayOfTone.length;
        int j = 0;
        Tone localTone;
        if (j < i)
        {
            localTone = arrayOfTone[j];
            if (localTone.mValue != paramInt);
        }
        while (true)
        {
            return localTone;
            j++;
            break;
            localTone = null;
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeInt(ordinal());
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cat.Tone
 * JD-Core Version:        0.6.2
 */