package com.android.internal.telephony.cat;

import android.graphics.Bitmap;

class LaunchBrowserParams extends CommandParams
{
    TextMessage confirmMsg;
    LaunchBrowserMode mode;
    String url;

    LaunchBrowserParams(CommandDetails paramCommandDetails, TextMessage paramTextMessage, String paramString, LaunchBrowserMode paramLaunchBrowserMode)
    {
        super(paramCommandDetails);
        this.confirmMsg = paramTextMessage;
        this.mode = paramLaunchBrowserMode;
        this.url = paramString;
    }

    boolean setIcon(Bitmap paramBitmap)
    {
        if ((paramBitmap != null) && (this.confirmMsg != null))
            this.confirmMsg.icon = paramBitmap;
        for (boolean bool = true; ; bool = false)
            return bool;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cat.LaunchBrowserParams
 * JD-Core Version:        0.6.2
 */