package com.android.internal.telephony.cat;

import android.graphics.Bitmap;

class DisplayTextParams extends CommandParams
{
    TextMessage textMsg;

    DisplayTextParams(CommandDetails paramCommandDetails, TextMessage paramTextMessage)
    {
        super(paramCommandDetails);
        this.textMsg = paramTextMessage;
    }

    boolean setIcon(Bitmap paramBitmap)
    {
        if ((paramBitmap != null) && (this.textMsg != null))
            this.textMsg.icon = paramBitmap;
        for (boolean bool = true; ; bool = false)
            return bool;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cat.DisplayTextParams
 * JD-Core Version:        0.6.2
 */