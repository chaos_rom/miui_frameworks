package com.android.internal.telephony.cat;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class ToneSettings
    implements Parcelable
{
    public static final Parcelable.Creator<ToneSettings> CREATOR = new Parcelable.Creator()
    {
        public ToneSettings createFromParcel(Parcel paramAnonymousParcel)
        {
            return new ToneSettings(paramAnonymousParcel, null);
        }

        public ToneSettings[] newArray(int paramAnonymousInt)
        {
            return new ToneSettings[paramAnonymousInt];
        }
    };
    public Duration duration;
    public Tone tone;
    public boolean vibrate;

    private ToneSettings(Parcel paramParcel)
    {
        this.duration = ((Duration)paramParcel.readParcelable(null));
        this.tone = ((Tone)paramParcel.readParcelable(null));
        if (paramParcel.readInt() == 1);
        for (boolean bool = true; ; bool = false)
        {
            this.vibrate = bool;
            return;
        }
    }

    public ToneSettings(Duration paramDuration, Tone paramTone, boolean paramBoolean)
    {
        this.duration = paramDuration;
        this.tone = paramTone;
        this.vibrate = paramBoolean;
    }

    public int describeContents()
    {
        return 0;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        int i = 0;
        paramParcel.writeParcelable(this.duration, 0);
        paramParcel.writeParcelable(this.tone, 0);
        if (this.vibrate)
            i = 1;
        paramParcel.writeInt(i);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cat.ToneSettings
 * JD-Core Version:        0.6.2
 */