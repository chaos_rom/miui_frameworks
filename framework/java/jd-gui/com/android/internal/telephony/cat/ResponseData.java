package com.android.internal.telephony.cat;

import java.io.ByteArrayOutputStream;

abstract class ResponseData
{
    public static void writeLength(ByteArrayOutputStream paramByteArrayOutputStream, int paramInt)
    {
        if (paramInt > 127)
            paramByteArrayOutputStream.write(129);
        paramByteArrayOutputStream.write(paramInt);
    }

    public abstract void format(ByteArrayOutputStream paramByteArrayOutputStream);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cat.ResponseData
 * JD-Core Version:        0.6.2
 */