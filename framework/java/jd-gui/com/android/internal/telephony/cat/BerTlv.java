package com.android.internal.telephony.cat;

import java.util.List;

class BerTlv
{
    public static final int BER_EVENT_DOWNLOAD_TAG = 214;
    public static final int BER_MENU_SELECTION_TAG = 211;
    public static final int BER_PROACTIVE_COMMAND_TAG = 208;
    public static final int BER_UNKNOWN_TAG;
    private List<ComprehensionTlv> mCompTlvs = null;
    private int mTag = 0;

    private BerTlv(int paramInt, List<ComprehensionTlv> paramList)
    {
        this.mTag = paramInt;
        this.mCompTlvs = paramList;
    }

    // ERROR //
    public static BerTlv decode(byte[] paramArrayOfByte)
        throws ResultException
    {
        // Byte code:
        //     0: aload_0
        //     1: arraylength
        //     2: istore_1
        //     3: iconst_0
        //     4: istore_2
        //     5: iconst_0
        //     6: iconst_1
        //     7: iadd
        //     8: istore_3
        //     9: aload_0
        //     10: iconst_0
        //     11: baload
        //     12: istore 8
        //     14: iload 8
        //     16: sipush 255
        //     19: iand
        //     20: istore 9
        //     22: iload 9
        //     24: sipush 208
        //     27: if_icmpne +283 -> 310
        //     30: iload_3
        //     31: iconst_1
        //     32: iadd
        //     33: istore 7
        //     35: aload_0
        //     36: iload_3
        //     37: baload
        //     38: istore 12
        //     40: iload 12
        //     42: sipush 255
        //     45: iand
        //     46: istore 13
        //     48: iload 13
        //     50: sipush 128
        //     53: if_icmpge +63 -> 116
        //     56: iload 13
        //     58: istore_2
        //     59: iload_1
        //     60: iload 7
        //     62: isub
        //     63: iload_2
        //     64: if_icmpge +294 -> 358
        //     67: new 29	com/android/internal/telephony/cat/ResultException
        //     70: dup
        //     71: getstatic 37	com/android/internal/telephony/cat/ResultCode:CMD_DATA_NOT_UNDERSTOOD	Lcom/android/internal/telephony/cat/ResultCode;
        //     74: new 39	java/lang/StringBuilder
        //     77: dup
        //     78: invokespecial 40	java/lang/StringBuilder:<init>	()V
        //     81: ldc 42
        //     83: invokevirtual 46	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     86: iload_1
        //     87: invokevirtual 49	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     90: ldc 51
        //     92: invokevirtual 46	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     95: iload 7
        //     97: invokevirtual 49	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     100: ldc 53
        //     102: invokevirtual 46	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     105: iload_2
        //     106: invokevirtual 49	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     109: invokevirtual 57	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     112: invokespecial 60	com/android/internal/telephony/cat/ResultException:<init>	(Lcom/android/internal/telephony/cat/ResultCode;Ljava/lang/String;)V
        //     115: athrow
        //     116: iload 13
        //     118: sipush 129
        //     121: if_icmpne +131 -> 252
        //     124: iload 7
        //     126: iconst_1
        //     127: iadd
        //     128: istore_3
        //     129: sipush 255
        //     132: aload_0
        //     133: iload 7
        //     135: baload
        //     136: iand
        //     137: istore 14
        //     139: iload 14
        //     141: sipush 128
        //     144: if_icmpge +99 -> 243
        //     147: new 29	com/android/internal/telephony/cat/ResultException
        //     150: dup
        //     151: getstatic 37	com/android/internal/telephony/cat/ResultCode:CMD_DATA_NOT_UNDERSTOOD	Lcom/android/internal/telephony/cat/ResultCode;
        //     154: new 39	java/lang/StringBuilder
        //     157: dup
        //     158: invokespecial 40	java/lang/StringBuilder:<init>	()V
        //     161: ldc 62
        //     163: invokevirtual 46	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     166: iconst_0
        //     167: invokestatic 68	java/lang/Integer:toHexString	(I)Ljava/lang/String;
        //     170: invokevirtual 46	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     173: ldc 51
        //     175: invokevirtual 46	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     178: iload_3
        //     179: invokevirtual 49	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     182: ldc 70
        //     184: invokevirtual 46	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     187: iload_1
        //     188: invokevirtual 49	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     191: invokevirtual 57	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     194: invokespecial 60	com/android/internal/telephony/cat/ResultException:<init>	(Lcom/android/internal/telephony/cat/ResultCode;Ljava/lang/String;)V
        //     197: athrow
        //     198: astore 6
        //     200: iload_3
        //     201: istore 7
        //     203: new 29	com/android/internal/telephony/cat/ResultException
        //     206: dup
        //     207: getstatic 73	com/android/internal/telephony/cat/ResultCode:REQUIRED_VALUES_MISSING	Lcom/android/internal/telephony/cat/ResultCode;
        //     210: new 39	java/lang/StringBuilder
        //     213: dup
        //     214: invokespecial 40	java/lang/StringBuilder:<init>	()V
        //     217: ldc 75
        //     219: invokevirtual 46	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     222: iload 7
        //     224: invokevirtual 49	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     227: ldc 70
        //     229: invokevirtual 46	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     232: iload_1
        //     233: invokevirtual 49	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     236: invokevirtual 57	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     239: invokespecial 60	com/android/internal/telephony/cat/ResultException:<init>	(Lcom/android/internal/telephony/cat/ResultCode;Ljava/lang/String;)V
        //     242: athrow
        //     243: iload 14
        //     245: istore_2
        //     246: iload_3
        //     247: istore 7
        //     249: goto -190 -> 59
        //     252: new 29	com/android/internal/telephony/cat/ResultException
        //     255: dup
        //     256: getstatic 37	com/android/internal/telephony/cat/ResultCode:CMD_DATA_NOT_UNDERSTOOD	Lcom/android/internal/telephony/cat/ResultCode;
        //     259: new 39	java/lang/StringBuilder
        //     262: dup
        //     263: invokespecial 40	java/lang/StringBuilder:<init>	()V
        //     266: ldc 77
        //     268: invokevirtual 46	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     271: iload 13
        //     273: invokestatic 68	java/lang/Integer:toHexString	(I)Ljava/lang/String;
        //     276: invokevirtual 46	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     279: ldc 51
        //     281: invokevirtual 46	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     284: iload 7
        //     286: invokevirtual 49	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     289: ldc 70
        //     291: invokevirtual 46	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     294: iload_1
        //     295: invokevirtual 49	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     298: invokevirtual 57	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     301: invokespecial 60	com/android/internal/telephony/cat/ResultException:<init>	(Lcom/android/internal/telephony/cat/ResultCode;Ljava/lang/String;)V
        //     304: athrow
        //     305: astore 11
        //     307: goto -104 -> 203
        //     310: getstatic 83	com/android/internal/telephony/cat/ComprehensionTlvTag:COMMAND_DETAILS	Lcom/android/internal/telephony/cat/ComprehensionTlvTag;
        //     313: invokevirtual 87	com/android/internal/telephony/cat/ComprehensionTlvTag:value	()I
        //     316: istore 10
        //     318: iload 10
        //     320: iload 9
        //     322: sipush -129
        //     325: iand
        //     326: if_icmpne +53 -> 379
        //     329: iconst_0
        //     330: istore 9
        //     332: iconst_0
        //     333: istore 7
        //     335: goto -276 -> 59
        //     338: astore 4
        //     340: iload_3
        //     341: pop
        //     342: new 29	com/android/internal/telephony/cat/ResultException
        //     345: dup
        //     346: getstatic 37	com/android/internal/telephony/cat/ResultCode:CMD_DATA_NOT_UNDERSTOOD	Lcom/android/internal/telephony/cat/ResultCode;
        //     349: aload 4
        //     351: invokevirtual 90	com/android/internal/telephony/cat/ResultException:explanation	()Ljava/lang/String;
        //     354: invokespecial 60	com/android/internal/telephony/cat/ResultException:<init>	(Lcom/android/internal/telephony/cat/ResultCode;Ljava/lang/String;)V
        //     357: athrow
        //     358: new 2	com/android/internal/telephony/cat/BerTlv
        //     361: dup
        //     362: iload 9
        //     364: aload_0
        //     365: iload 7
        //     367: invokestatic 96	com/android/internal/telephony/cat/ComprehensionTlv:decodeMany	([BI)Ljava/util/List;
        //     370: invokespecial 98	com/android/internal/telephony/cat/BerTlv:<init>	(ILjava/util/List;)V
        //     373: areturn
        //     374: astore 4
        //     376: goto -34 -> 342
        //     379: iload_3
        //     380: istore 7
        //     382: goto -323 -> 59
        //
        // Exception table:
        //     from	to	target	type
        //     9	14	198	java/lang/IndexOutOfBoundsException
        //     129	198	198	java/lang/IndexOutOfBoundsException
        //     310	318	198	java/lang/IndexOutOfBoundsException
        //     35	40	305	java/lang/IndexOutOfBoundsException
        //     252	305	305	java/lang/IndexOutOfBoundsException
        //     9	14	338	com/android/internal/telephony/cat/ResultException
        //     129	198	338	com/android/internal/telephony/cat/ResultException
        //     310	318	338	com/android/internal/telephony/cat/ResultException
        //     35	40	374	com/android/internal/telephony/cat/ResultException
        //     252	305	374	com/android/internal/telephony/cat/ResultException
    }

    public List<ComprehensionTlv> getComprehensionTlvs()
    {
        return this.mCompTlvs;
    }

    public int getTag()
    {
        return this.mTag;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cat.BerTlv
 * JD-Core Version:        0.6.2
 */