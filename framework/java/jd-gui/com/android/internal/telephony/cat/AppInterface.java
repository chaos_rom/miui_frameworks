package com.android.internal.telephony.cat;

public abstract interface AppInterface
{
    public static final String CAT_CMD_ACTION = "android.intent.action.stk.command";
    public static final String CAT_SESSION_END_ACTION = "android.intent.action.stk.session_end";

    public abstract void onCmdResponse(CatResponseMessage paramCatResponseMessage);

    public static enum CommandType
    {
        private int mValue;

        static
        {
            SEND_SMS = new CommandType("SEND_SMS", 9, 19);
            SEND_DTMF = new CommandType("SEND_DTMF", 10, 20);
            SET_UP_EVENT_LIST = new CommandType("SET_UP_EVENT_LIST", 11, 5);
            SET_UP_IDLE_MODE_TEXT = new CommandType("SET_UP_IDLE_MODE_TEXT", 12, 40);
            SET_UP_MENU = new CommandType("SET_UP_MENU", 13, 37);
            SET_UP_CALL = new CommandType("SET_UP_CALL", 14, 16);
            PROVIDE_LOCAL_INFORMATION = new CommandType("PROVIDE_LOCAL_INFORMATION", 15, 38);
            OPEN_CHANNEL = new CommandType("OPEN_CHANNEL", 16, 64);
            CLOSE_CHANNEL = new CommandType("CLOSE_CHANNEL", 17, 65);
            RECEIVE_DATA = new CommandType("RECEIVE_DATA", 18, 66);
            SEND_DATA = new CommandType("SEND_DATA", 19, 67);
            CommandType[] arrayOfCommandType = new CommandType[20];
            arrayOfCommandType[0] = DISPLAY_TEXT;
            arrayOfCommandType[1] = GET_INKEY;
            arrayOfCommandType[2] = GET_INPUT;
            arrayOfCommandType[3] = LAUNCH_BROWSER;
            arrayOfCommandType[4] = PLAY_TONE;
            arrayOfCommandType[5] = REFRESH;
            arrayOfCommandType[6] = SELECT_ITEM;
            arrayOfCommandType[7] = SEND_SS;
            arrayOfCommandType[8] = SEND_USSD;
            arrayOfCommandType[9] = SEND_SMS;
            arrayOfCommandType[10] = SEND_DTMF;
            arrayOfCommandType[11] = SET_UP_EVENT_LIST;
            arrayOfCommandType[12] = SET_UP_IDLE_MODE_TEXT;
            arrayOfCommandType[13] = SET_UP_MENU;
            arrayOfCommandType[14] = SET_UP_CALL;
            arrayOfCommandType[15] = PROVIDE_LOCAL_INFORMATION;
            arrayOfCommandType[16] = OPEN_CHANNEL;
            arrayOfCommandType[17] = CLOSE_CHANNEL;
            arrayOfCommandType[18] = RECEIVE_DATA;
            arrayOfCommandType[19] = SEND_DATA;
        }

        private CommandType(int paramInt)
        {
            this.mValue = paramInt;
        }

        public static CommandType fromInt(int paramInt)
        {
            CommandType[] arrayOfCommandType = values();
            int i = arrayOfCommandType.length;
            int j = 0;
            CommandType localCommandType;
            if (j < i)
            {
                localCommandType = arrayOfCommandType[j];
                if (localCommandType.mValue != paramInt);
            }
            while (true)
            {
                return localCommandType;
                j++;
                break;
                localCommandType = null;
            }
        }

        public int value()
        {
            return this.mValue;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cat.AppInterface
 * JD-Core Version:        0.6.2
 */