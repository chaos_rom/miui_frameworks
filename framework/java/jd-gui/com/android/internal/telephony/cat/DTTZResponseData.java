package com.android.internal.telephony.cat;

import android.os.SystemProperties;
import android.text.TextUtils;
import java.io.ByteArrayOutputStream;
import java.util.Calendar;
import java.util.TimeZone;

class DTTZResponseData extends ResponseData
{
    private Calendar calendar;

    public DTTZResponseData(Calendar paramCalendar)
    {
        this.calendar = paramCalendar;
    }

    private byte byteToBCD(int paramInt)
    {
        if ((paramInt < 0) && (paramInt > 99))
            CatLog.d(this, "Err: byteToBCD conversion Value is " + paramInt + " Value has to be between 0 and 99");
        for (byte b = 0; ; b = (byte)(paramInt / 10 | paramInt % 10 << 4))
            return b;
    }

    private byte getTZOffSetByte(long paramLong)
    {
        int i = 1;
        int j;
        byte b;
        if (paramLong < 0L)
        {
            j = i;
            long l = paramLong / 900000L;
            if (j != 0)
                i = -1;
            b = byteToBCD((int)(l * i));
            if (j == 0)
                break label62;
            b |= 8;
        }
        label62: 
        while (true)
        {
            return b;
            j = 0;
            break;
        }
    }

    public void format(ByteArrayOutputStream paramByteArrayOutputStream)
    {
        if (paramByteArrayOutputStream == null)
            return;
        paramByteArrayOutputStream.write(0x80 | AppInterface.CommandType.PROVIDE_LOCAL_INFORMATION.value());
        byte[] arrayOfByte = new byte[8];
        arrayOfByte[0] = 7;
        if (this.calendar == null)
            this.calendar = Calendar.getInstance();
        arrayOfByte[1] = byteToBCD(this.calendar.get(1) % 100);
        arrayOfByte[2] = byteToBCD(1 + this.calendar.get(2));
        arrayOfByte[3] = byteToBCD(this.calendar.get(5));
        arrayOfByte[4] = byteToBCD(this.calendar.get(11));
        arrayOfByte[5] = byteToBCD(this.calendar.get(12));
        arrayOfByte[6] = byteToBCD(this.calendar.get(13));
        String str = SystemProperties.get("persist.sys.timezone", "");
        if (TextUtils.isEmpty(str))
            arrayOfByte[7] = -1;
        while (true)
        {
            int i = arrayOfByte.length;
            for (int j = 0; j < i; j++)
                paramByteArrayOutputStream.write(arrayOfByte[j]);
            break;
            TimeZone localTimeZone = TimeZone.getTimeZone(str);
            arrayOfByte[7] = getTZOffSetByte(localTimeZone.getRawOffset() + localTimeZone.getDSTSavings());
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cat.DTTZResponseData
 * JD-Core Version:        0.6.2
 */