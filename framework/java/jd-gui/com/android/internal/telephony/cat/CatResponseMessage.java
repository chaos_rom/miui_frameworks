package com.android.internal.telephony.cat;

public class CatResponseMessage
{
    CommandDetails cmdDet = null;
    ResultCode resCode = ResultCode.OK;
    boolean usersConfirm = false;
    String usersInput = null;
    int usersMenuSelection = 0;
    boolean usersYesNoSelection = false;

    public CatResponseMessage(CatCmdMessage paramCatCmdMessage)
    {
        this.cmdDet = paramCatCmdMessage.mCmdDet;
    }

    CommandDetails getCmdDetails()
    {
        return this.cmdDet;
    }

    public void setConfirmation(boolean paramBoolean)
    {
        this.usersConfirm = paramBoolean;
    }

    public void setInput(String paramString)
    {
        this.usersInput = paramString;
    }

    public void setMenuSelection(int paramInt)
    {
        this.usersMenuSelection = paramInt;
    }

    public void setResultCode(ResultCode paramResultCode)
    {
        this.resCode = paramResultCode;
    }

    public void setYesNo(boolean paramBoolean)
    {
        this.usersYesNoSelection = paramBoolean;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cat.CatResponseMessage
 * JD-Core Version:        0.6.2
 */