package com.android.internal.telephony.cat;

import java.util.ArrayList;
import java.util.List;

class ComprehensionTlv
{
    private static final String LOG_TAG = "ComprehensionTlv";
    private boolean mCr;
    private int mLength;
    private byte[] mRawValue;
    private int mTag;
    private int mValueIndex;

    protected ComprehensionTlv(int paramInt1, boolean paramBoolean, int paramInt2, byte[] paramArrayOfByte, int paramInt3)
    {
        this.mTag = paramInt1;
        this.mCr = paramBoolean;
        this.mLength = paramInt2;
        this.mValueIndex = paramInt3;
        this.mRawValue = paramArrayOfByte;
    }

    // ERROR //
    public static ComprehensionTlv decode(byte[] paramArrayOfByte, int paramInt)
        throws ResultException
    {
        // Byte code:
        //     0: iconst_1
        //     1: istore_2
        //     2: aload_0
        //     3: arraylength
        //     4: istore_3
        //     5: iload_1
        //     6: iconst_1
        //     7: iadd
        //     8: istore 4
        //     10: aload_0
        //     11: iload_1
        //     12: baload
        //     13: istore 7
        //     15: iload 7
        //     17: sipush 255
        //     20: iand
        //     21: istore 8
        //     23: iload 8
        //     25: lookupswitch	default:+43->68, 0:+108->133, 127:+174->199, 128:+108->133, 255:+108->133
        //     69: iconst_5
        //     70: sipush 128
        //     73: iand
        //     74: ifeq +616 -> 690
        //     77: iload 8
        //     79: sipush -129
        //     82: iand
        //     83: istore 10
        //     85: iload 4
        //     87: iconst_1
        //     88: iadd
        //     89: istore 6
        //     91: sipush 255
        //     94: aload_0
        //     95: iload 4
        //     97: baload
        //     98: iand
        //     99: istore 12
        //     101: iload 12
        //     103: sipush 128
        //     106: if_icmpge +128 -> 234
        //     109: iload 12
        //     111: istore 13
        //     113: new 2	com/android/internal/telephony/cat/ComprehensionTlv
        //     116: dup
        //     117: iload 10
        //     119: iload_2
        //     120: iload 13
        //     122: aload_0
        //     123: iload 6
        //     125: invokespecial 39	com/android/internal/telephony/cat/ComprehensionTlv:<init>	(IZI[BI)V
        //     128: astore 14
        //     130: aload 14
        //     132: areturn
        //     133: ldc 41
        //     135: new 43	java/lang/StringBuilder
        //     138: dup
        //     139: invokespecial 44	java/lang/StringBuilder:<init>	()V
        //     142: ldc 46
        //     144: invokevirtual 50	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     147: iload 8
        //     149: invokestatic 56	java/lang/Integer:toHexString	(I)Ljava/lang/String;
        //     152: invokevirtual 50	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     155: ldc 58
        //     157: invokevirtual 50	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     160: iload_1
        //     161: invokevirtual 61	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     164: ldc 63
        //     166: invokevirtual 50	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     169: iload 4
        //     171: invokevirtual 61	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     174: ldc 65
        //     176: invokevirtual 50	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     179: iload_3
        //     180: invokevirtual 61	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     183: invokevirtual 69	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     186: invokestatic 75	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
        //     189: pop
        //     190: aconst_null
        //     191: astore 14
        //     193: iload 4
        //     195: pop
        //     196: goto -66 -> 130
        //     199: sipush 255
        //     202: aload_0
        //     203: iload 4
        //     205: baload
        //     206: iand
        //     207: bipush 8
        //     209: ishl
        //     210: sipush 255
        //     213: aload_0
        //     214: iload 4
        //     216: iconst_1
        //     217: iadd
        //     218: baload
        //     219: iand
        //     220: ior
        //     221: istore 9
        //     223: ldc 76
        //     225: iload 9
        //     227: iand
        //     228: ifeq +457 -> 685
        //     231: goto +441 -> 672
        //     234: iload 12
        //     236: sipush 129
        //     239: if_icmpne +144 -> 383
        //     242: iload 6
        //     244: iconst_1
        //     245: iadd
        //     246: istore 4
        //     248: sipush 255
        //     251: aload_0
        //     252: iload 6
        //     254: baload
        //     255: iand
        //     256: istore 13
        //     258: iload 13
        //     260: sipush 128
        //     263: if_icmpge +402 -> 665
        //     266: new 35	com/android/internal/telephony/cat/ResultException
        //     269: dup
        //     270: getstatic 82	com/android/internal/telephony/cat/ResultCode:CMD_DATA_NOT_UNDERSTOOD	Lcom/android/internal/telephony/cat/ResultCode;
        //     273: new 43	java/lang/StringBuilder
        //     276: dup
        //     277: invokespecial 44	java/lang/StringBuilder:<init>	()V
        //     280: ldc 84
        //     282: invokevirtual 50	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     285: iload 13
        //     287: invokestatic 56	java/lang/Integer:toHexString	(I)Ljava/lang/String;
        //     290: invokevirtual 50	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     293: ldc 86
        //     295: invokevirtual 50	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     298: iload_1
        //     299: invokevirtual 61	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     302: ldc 63
        //     304: invokevirtual 50	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     307: iload 4
        //     309: invokevirtual 61	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     312: ldc 65
        //     314: invokevirtual 50	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     317: iload_3
        //     318: invokevirtual 61	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     321: invokevirtual 69	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     324: invokespecial 89	com/android/internal/telephony/cat/ResultException:<init>	(Lcom/android/internal/telephony/cat/ResultCode;Ljava/lang/String;)V
        //     327: athrow
        //     328: astore 5
        //     330: iload 4
        //     332: istore 6
        //     334: new 35	com/android/internal/telephony/cat/ResultException
        //     337: dup
        //     338: getstatic 82	com/android/internal/telephony/cat/ResultCode:CMD_DATA_NOT_UNDERSTOOD	Lcom/android/internal/telephony/cat/ResultCode;
        //     341: new 43	java/lang/StringBuilder
        //     344: dup
        //     345: invokespecial 44	java/lang/StringBuilder:<init>	()V
        //     348: ldc 91
        //     350: invokevirtual 50	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     353: iload_1
        //     354: invokevirtual 61	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     357: ldc 63
        //     359: invokevirtual 50	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     362: iload 6
        //     364: invokevirtual 61	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     367: ldc 65
        //     369: invokevirtual 50	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     372: iload_3
        //     373: invokevirtual 61	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     376: invokevirtual 69	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     379: invokespecial 89	com/android/internal/telephony/cat/ResultException:<init>	(Lcom/android/internal/telephony/cat/ResultCode;Ljava/lang/String;)V
        //     382: athrow
        //     383: iload 12
        //     385: sipush 130
        //     388: if_icmpne +100 -> 488
        //     391: sipush 255
        //     394: aload_0
        //     395: iload 6
        //     397: baload
        //     398: iand
        //     399: bipush 8
        //     401: ishl
        //     402: sipush 255
        //     405: aload_0
        //     406: iload 6
        //     408: iconst_1
        //     409: iadd
        //     410: baload
        //     411: iand
        //     412: ior
        //     413: istore 13
        //     415: iinc 6 2
        //     418: iload 13
        //     420: sipush 256
        //     423: if_icmpge -310 -> 113
        //     426: new 35	com/android/internal/telephony/cat/ResultException
        //     429: dup
        //     430: getstatic 82	com/android/internal/telephony/cat/ResultCode:CMD_DATA_NOT_UNDERSTOOD	Lcom/android/internal/telephony/cat/ResultCode;
        //     433: new 43	java/lang/StringBuilder
        //     436: dup
        //     437: invokespecial 44	java/lang/StringBuilder:<init>	()V
        //     440: ldc 93
        //     442: invokevirtual 50	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     445: iload 13
        //     447: invokestatic 56	java/lang/Integer:toHexString	(I)Ljava/lang/String;
        //     450: invokevirtual 50	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     453: ldc 86
        //     455: invokevirtual 50	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     458: iload_1
        //     459: invokevirtual 61	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     462: ldc 63
        //     464: invokevirtual 50	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     467: iload 6
        //     469: invokevirtual 61	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     472: ldc 65
        //     474: invokevirtual 50	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     477: iload_3
        //     478: invokevirtual 61	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     481: invokevirtual 69	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     484: invokespecial 89	com/android/internal/telephony/cat/ResultException:<init>	(Lcom/android/internal/telephony/cat/ResultCode;Ljava/lang/String;)V
        //     487: athrow
        //     488: iload 12
        //     490: sipush 131
        //     493: if_icmpne +113 -> 606
        //     496: sipush 255
        //     499: aload_0
        //     500: iload 6
        //     502: baload
        //     503: iand
        //     504: bipush 16
        //     506: ishl
        //     507: sipush 255
        //     510: aload_0
        //     511: iload 6
        //     513: iconst_1
        //     514: iadd
        //     515: baload
        //     516: iand
        //     517: bipush 8
        //     519: ishl
        //     520: ior
        //     521: sipush 255
        //     524: aload_0
        //     525: iload 6
        //     527: iconst_2
        //     528: iadd
        //     529: baload
        //     530: iand
        //     531: ior
        //     532: istore 13
        //     534: iinc 6 3
        //     537: iload 13
        //     539: ldc 94
        //     541: if_icmpge -428 -> 113
        //     544: new 35	com/android/internal/telephony/cat/ResultException
        //     547: dup
        //     548: getstatic 82	com/android/internal/telephony/cat/ResultCode:CMD_DATA_NOT_UNDERSTOOD	Lcom/android/internal/telephony/cat/ResultCode;
        //     551: new 43	java/lang/StringBuilder
        //     554: dup
        //     555: invokespecial 44	java/lang/StringBuilder:<init>	()V
        //     558: ldc 96
        //     560: invokevirtual 50	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     563: iload 13
        //     565: invokestatic 56	java/lang/Integer:toHexString	(I)Ljava/lang/String;
        //     568: invokevirtual 50	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     571: ldc 86
        //     573: invokevirtual 50	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     576: iload_1
        //     577: invokevirtual 61	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     580: ldc 63
        //     582: invokevirtual 50	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     585: iload 6
        //     587: invokevirtual 61	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     590: ldc 65
        //     592: invokevirtual 50	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     595: iload_3
        //     596: invokevirtual 61	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     599: invokevirtual 69	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     602: invokespecial 89	com/android/internal/telephony/cat/ResultException:<init>	(Lcom/android/internal/telephony/cat/ResultCode;Ljava/lang/String;)V
        //     605: athrow
        //     606: new 35	com/android/internal/telephony/cat/ResultException
        //     609: dup
        //     610: getstatic 82	com/android/internal/telephony/cat/ResultCode:CMD_DATA_NOT_UNDERSTOOD	Lcom/android/internal/telephony/cat/ResultCode;
        //     613: new 43	java/lang/StringBuilder
        //     616: dup
        //     617: invokespecial 44	java/lang/StringBuilder:<init>	()V
        //     620: ldc 98
        //     622: invokevirtual 50	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     625: iload 12
        //     627: invokevirtual 61	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     630: ldc 86
        //     632: invokevirtual 50	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     635: iload_1
        //     636: invokevirtual 61	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     639: ldc 63
        //     641: invokevirtual 50	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     644: iload 6
        //     646: invokevirtual 61	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     649: ldc 65
        //     651: invokevirtual 50	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     654: iload_3
        //     655: invokevirtual 61	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     658: invokevirtual 69	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     661: invokespecial 89	com/android/internal/telephony/cat/ResultException:<init>	(Lcom/android/internal/telephony/cat/ResultCode;Ljava/lang/String;)V
        //     664: athrow
        //     665: iload 4
        //     667: istore 6
        //     669: goto -556 -> 113
        //     672: iload 9
        //     674: ldc 99
        //     676: iand
        //     677: istore 10
        //     679: iinc 4 2
        //     682: goto -597 -> 85
        //     685: iconst_0
        //     686: istore_2
        //     687: goto -15 -> 672
        //     690: iconst_0
        //     691: istore_2
        //     692: goto -615 -> 77
        //     695: astore 11
        //     697: goto -363 -> 334
        //
        // Exception table:
        //     from	to	target	type
        //     10	15	328	java/lang/IndexOutOfBoundsException
        //     133	328	328	java/lang/IndexOutOfBoundsException
        //     91	130	695	java/lang/IndexOutOfBoundsException
        //     391	665	695	java/lang/IndexOutOfBoundsException
    }

    public static List<ComprehensionTlv> decodeMany(byte[] paramArrayOfByte, int paramInt)
        throws ResultException
    {
        ArrayList localArrayList = new ArrayList();
        int i = paramArrayOfByte.length;
        while (paramInt < i)
        {
            ComprehensionTlv localComprehensionTlv = decode(paramArrayOfByte, paramInt);
            if (localComprehensionTlv != null)
            {
                localArrayList.add(localComprehensionTlv);
                paramInt = localComprehensionTlv.mValueIndex + localComprehensionTlv.mLength;
            }
            else
            {
                CatLog.d("ComprehensionTlv", "decodeMany: ctlv is null, stop decoding");
            }
        }
        return localArrayList;
    }

    public int getLength()
    {
        return this.mLength;
    }

    public byte[] getRawValue()
    {
        return this.mRawValue;
    }

    public int getTag()
    {
        return this.mTag;
    }

    public int getValueIndex()
    {
        return this.mValueIndex;
    }

    public boolean isComprehensionRequired()
    {
        return this.mCr;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cat.ComprehensionTlv
 * JD-Core Version:        0.6.2
 */