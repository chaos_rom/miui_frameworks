package com.android.internal.telephony.cat;

class RilMessage
{
    Object mData;
    int mId;
    ResultCode mResCode;

    RilMessage(int paramInt, String paramString)
    {
        this.mId = paramInt;
        this.mData = paramString;
    }

    RilMessage(RilMessage paramRilMessage)
    {
        this.mId = paramRilMessage.mId;
        this.mData = paramRilMessage.mData;
        this.mResCode = paramRilMessage.mResCode;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cat.RilMessage
 * JD-Core Version:        0.6.2
 */