package com.android.internal.telephony.cat;

public enum TextColor
{
    private int mValue;

    static
    {
        DARK_GREEN = new TextColor("DARK_GREEN", 4, 4);
        DARK_CYAN = new TextColor("DARK_CYAN", 5, 5);
        DARK_BLUE = new TextColor("DARK_BLUE", 6, 6);
        DARK_MAGENTA = new TextColor("DARK_MAGENTA", 7, 7);
        GRAY = new TextColor("GRAY", 8, 8);
        WHITE = new TextColor("WHITE", 9, 9);
        BRIGHT_RED = new TextColor("BRIGHT_RED", 10, 10);
        BRIGHT_YELLOW = new TextColor("BRIGHT_YELLOW", 11, 11);
        BRIGHT_GREEN = new TextColor("BRIGHT_GREEN", 12, 12);
        BRIGHT_CYAN = new TextColor("BRIGHT_CYAN", 13, 13);
        BRIGHT_BLUE = new TextColor("BRIGHT_BLUE", 14, 14);
        BRIGHT_MAGENTA = new TextColor("BRIGHT_MAGENTA", 15, 15);
        TextColor[] arrayOfTextColor = new TextColor[16];
        arrayOfTextColor[0] = BLACK;
        arrayOfTextColor[1] = DARK_GRAY;
        arrayOfTextColor[2] = DARK_RED;
        arrayOfTextColor[3] = DARK_YELLOW;
        arrayOfTextColor[4] = DARK_GREEN;
        arrayOfTextColor[5] = DARK_CYAN;
        arrayOfTextColor[6] = DARK_BLUE;
        arrayOfTextColor[7] = DARK_MAGENTA;
        arrayOfTextColor[8] = GRAY;
        arrayOfTextColor[9] = WHITE;
        arrayOfTextColor[10] = BRIGHT_RED;
        arrayOfTextColor[11] = BRIGHT_YELLOW;
        arrayOfTextColor[12] = BRIGHT_GREEN;
        arrayOfTextColor[13] = BRIGHT_CYAN;
        arrayOfTextColor[14] = BRIGHT_BLUE;
        arrayOfTextColor[15] = BRIGHT_MAGENTA;
    }

    private TextColor(int paramInt)
    {
        this.mValue = paramInt;
    }

    public static TextColor fromInt(int paramInt)
    {
        TextColor[] arrayOfTextColor = values();
        int i = arrayOfTextColor.length;
        int j = 0;
        TextColor localTextColor;
        if (j < i)
        {
            localTextColor = arrayOfTextColor[j];
            if (localTextColor.mValue != paramInt);
        }
        while (true)
        {
            return localTextColor;
            j++;
            break;
            localTextColor = null;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cat.TextColor
 * JD-Core Version:        0.6.2
 */