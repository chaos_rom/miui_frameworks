package com.android.internal.telephony.cat;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

class CommandDetails extends ValueObject
    implements Parcelable
{
    public static final Parcelable.Creator<CommandDetails> CREATOR = new Parcelable.Creator()
    {
        public CommandDetails createFromParcel(Parcel paramAnonymousParcel)
        {
            return new CommandDetails(paramAnonymousParcel);
        }

        public CommandDetails[] newArray(int paramAnonymousInt)
        {
            return new CommandDetails[paramAnonymousInt];
        }
    };
    public int commandNumber;
    public int commandQualifier;
    public boolean compRequired;
    public int typeOfCommand;

    CommandDetails()
    {
    }

    public CommandDetails(Parcel paramParcel)
    {
        this.compRequired = true;
        this.commandNumber = paramParcel.readInt();
        this.typeOfCommand = paramParcel.readInt();
        this.commandQualifier = paramParcel.readInt();
    }

    public boolean compareTo(CommandDetails paramCommandDetails)
    {
        if ((this.compRequired == paramCommandDetails.compRequired) && (this.commandNumber == paramCommandDetails.commandNumber) && (this.commandQualifier == paramCommandDetails.commandQualifier) && (this.typeOfCommand == paramCommandDetails.typeOfCommand));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public int describeContents()
    {
        return 0;
    }

    public ComprehensionTlvTag getTag()
    {
        return ComprehensionTlvTag.COMMAND_DETAILS;
    }

    public String toString()
    {
        return "CmdDetails: compRequired=" + this.compRequired + " commandNumber=" + this.commandNumber + " typeOfCommand=" + this.typeOfCommand + " commandQualifier=" + this.commandQualifier;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeInt(this.commandNumber);
        paramParcel.writeInt(this.typeOfCommand);
        paramParcel.writeInt(this.commandQualifier);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cat.CommandDetails
 * JD-Core Version:        0.6.2
 */