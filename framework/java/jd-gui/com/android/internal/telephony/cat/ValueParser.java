package com.android.internal.telephony.cat;

import com.android.internal.telephony.GsmAlphabet;
import com.android.internal.telephony.IccUtils;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

abstract class ValueParser
{
    static String retrieveAlphaId(ComprehensionTlv paramComprehensionTlv)
        throws ResultException
    {
        byte[] arrayOfByte;
        int i;
        int j;
        if (paramComprehensionTlv != null)
        {
            arrayOfByte = paramComprehensionTlv.getRawValue();
            i = paramComprehensionTlv.getValueIndex();
            j = paramComprehensionTlv.getLength();
            if (j == 0);
        }
        while (true)
        {
            try
            {
                String str2 = IccUtils.adnStringFieldToString(arrayOfByte, i, j);
                str1 = str2;
                return str1;
            }
            catch (IndexOutOfBoundsException localIndexOutOfBoundsException)
            {
                throw new ResultException(ResultCode.CMD_DATA_NOT_UNDERSTOOD);
            }
            String str1 = "Defualt Message";
            continue;
            str1 = "Defualt Message";
        }
    }

    static CommandDetails retrieveCommandDetails(ComprehensionTlv paramComprehensionTlv)
        throws ResultException
    {
        CommandDetails localCommandDetails = new CommandDetails();
        byte[] arrayOfByte = paramComprehensionTlv.getRawValue();
        int i = paramComprehensionTlv.getValueIndex();
        try
        {
            localCommandDetails.compRequired = paramComprehensionTlv.isComprehensionRequired();
            localCommandDetails.commandNumber = (0xFF & arrayOfByte[i]);
            localCommandDetails.typeOfCommand = (0xFF & arrayOfByte[(i + 1)]);
            localCommandDetails.commandQualifier = (0xFF & arrayOfByte[(i + 2)]);
            return localCommandDetails;
        }
        catch (IndexOutOfBoundsException localIndexOutOfBoundsException)
        {
        }
        throw new ResultException(ResultCode.CMD_DATA_NOT_UNDERSTOOD);
    }

    static DeviceIdentities retrieveDeviceIdentities(ComprehensionTlv paramComprehensionTlv)
        throws ResultException
    {
        DeviceIdentities localDeviceIdentities = new DeviceIdentities();
        byte[] arrayOfByte = paramComprehensionTlv.getRawValue();
        int i = paramComprehensionTlv.getValueIndex();
        try
        {
            localDeviceIdentities.sourceId = (0xFF & arrayOfByte[i]);
            localDeviceIdentities.destinationId = (0xFF & arrayOfByte[(i + 1)]);
            return localDeviceIdentities;
        }
        catch (IndexOutOfBoundsException localIndexOutOfBoundsException)
        {
        }
        throw new ResultException(ResultCode.REQUIRED_VALUES_MISSING);
    }

    static Duration retrieveDuration(ComprehensionTlv paramComprehensionTlv)
        throws ResultException
    {
        byte[] arrayOfByte = paramComprehensionTlv.getRawValue();
        int i = paramComprehensionTlv.getValueIndex();
        try
        {
            Duration.TimeUnit localTimeUnit = Duration.TimeUnit.values()[(0xFF & arrayOfByte[i])];
            int j = arrayOfByte[(i + 1)];
            return new Duration(j & 0xFF, localTimeUnit);
        }
        catch (IndexOutOfBoundsException localIndexOutOfBoundsException)
        {
        }
        throw new ResultException(ResultCode.CMD_DATA_NOT_UNDERSTOOD);
    }

    static IconId retrieveIconId(ComprehensionTlv paramComprehensionTlv)
        throws ResultException
    {
        IconId localIconId = new IconId();
        byte[] arrayOfByte = paramComprehensionTlv.getRawValue();
        int i = paramComprehensionTlv.getValueIndex();
        int j = i + 1;
        try
        {
            if ((0xFF & arrayOfByte[i]) == 0);
            for (boolean bool = true; ; bool = false)
            {
                localIconId.selfExplanatory = bool;
                localIconId.recordNumber = (0xFF & arrayOfByte[j]);
                return localIconId;
            }
        }
        catch (IndexOutOfBoundsException localIndexOutOfBoundsException)
        {
        }
        throw new ResultException(ResultCode.CMD_DATA_NOT_UNDERSTOOD);
    }

    static Item retrieveItem(ComprehensionTlv paramComprehensionTlv)
        throws ResultException
    {
        Item localItem = null;
        byte[] arrayOfByte = paramComprehensionTlv.getRawValue();
        int i = paramComprehensionTlv.getValueIndex();
        int j = paramComprehensionTlv.getLength();
        int k;
        if (j != 0)
            k = j - 1;
        try
        {
            localItem = new Item(0xFF & arrayOfByte[i], IccUtils.adnStringFieldToString(arrayOfByte, i + 1, k));
            return localItem;
        }
        catch (IndexOutOfBoundsException localIndexOutOfBoundsException)
        {
        }
        throw new ResultException(ResultCode.CMD_DATA_NOT_UNDERSTOOD);
    }

    static int retrieveItemId(ComprehensionTlv paramComprehensionTlv)
        throws ResultException
    {
        byte[] arrayOfByte = paramComprehensionTlv.getRawValue();
        int i = paramComprehensionTlv.getValueIndex();
        try
        {
            int j = arrayOfByte[i];
            return j & 0xFF;
        }
        catch (IndexOutOfBoundsException localIndexOutOfBoundsException)
        {
        }
        throw new ResultException(ResultCode.CMD_DATA_NOT_UNDERSTOOD);
    }

    // ERROR //
    static ItemsIconId retrieveItemsIconId(ComprehensionTlv paramComprehensionTlv)
        throws ResultException
    {
        // Byte code:
        //     0: ldc 122
        //     2: ldc 124
        //     4: invokestatic 130	com/android/internal/telephony/cat/CatLog:d	(Ljava/lang/String;Ljava/lang/String;)V
        //     7: new 132	com/android/internal/telephony/cat/ItemsIconId
        //     10: dup
        //     11: invokespecial 133	com/android/internal/telephony/cat/ItemsIconId:<init>	()V
        //     14: astore_1
        //     15: aload_0
        //     16: invokevirtual 20	com/android/internal/telephony/cat/ComprehensionTlv:getRawValue	()[B
        //     19: astore_2
        //     20: aload_0
        //     21: invokevirtual 24	com/android/internal/telephony/cat/ComprehensionTlv:getValueIndex	()I
        //     24: istore_3
        //     25: bipush 255
        //     27: aload_0
        //     28: invokevirtual 27	com/android/internal/telephony/cat/ComprehensionTlv:getLength	()I
        //     31: iadd
        //     32: istore 4
        //     34: aload_1
        //     35: iload 4
        //     37: newarray int
        //     39: putfield 137	com/android/internal/telephony/cat/ItemsIconId:recordNumbers	[I
        //     42: iload_3
        //     43: iconst_1
        //     44: iadd
        //     45: istore 5
        //     47: sipush 255
        //     50: aload_2
        //     51: iload_3
        //     52: baload
        //     53: iand
        //     54: ifne +60 -> 114
        //     57: iconst_1
        //     58: istore 8
        //     60: aload_1
        //     61: iload 8
        //     63: putfield 138	com/android/internal/telephony/cat/ItemsIconId:selfExplanatory	Z
        //     66: iconst_0
        //     67: istore 9
        //     69: iload 9
        //     71: iload 4
        //     73: if_icmpge +63 -> 136
        //     76: aload_1
        //     77: getfield 137	com/android/internal/telephony/cat/ItemsIconId:recordNumbers	[I
        //     80: astore 10
        //     82: iload 9
        //     84: iconst_1
        //     85: iadd
        //     86: istore 11
        //     88: iload 5
        //     90: iconst_1
        //     91: iadd
        //     92: istore 12
        //     94: aload 10
        //     96: iload 9
        //     98: aload_2
        //     99: iload 5
        //     101: baload
        //     102: iastore
        //     103: iload 11
        //     105: istore 9
        //     107: iload 12
        //     109: istore 5
        //     111: goto -42 -> 69
        //     114: iconst_0
        //     115: istore 8
        //     117: goto -57 -> 60
        //     120: astore 6
        //     122: iload 5
        //     124: pop
        //     125: new 12	com/android/internal/telephony/cat/ResultException
        //     128: dup
        //     129: getstatic 39	com/android/internal/telephony/cat/ResultCode:CMD_DATA_NOT_UNDERSTOOD	Lcom/android/internal/telephony/cat/ResultCode;
        //     132: invokespecial 42	com/android/internal/telephony/cat/ResultException:<init>	(Lcom/android/internal/telephony/cat/ResultCode;)V
        //     135: athrow
        //     136: aload_1
        //     137: areturn
        //     138: astore 13
        //     140: goto -15 -> 125
        //
        // Exception table:
        //     from	to	target	type
        //     47	82	120	java/lang/IndexOutOfBoundsException
        //     94	103	138	java/lang/IndexOutOfBoundsException
    }

    static List<TextAttribute> retrieveTextAttribute(ComprehensionTlv paramComprehensionTlv)
        throws ResultException
    {
        ArrayList localArrayList = new ArrayList();
        byte[] arrayOfByte = paramComprehensionTlv.getRawValue();
        int i = paramComprehensionTlv.getValueIndex();
        int j = paramComprehensionTlv.getLength();
        int m;
        if (j != 0)
        {
            int k = j / 4;
            m = 0;
            if (m >= k)
                break label205;
        }
        while (true)
        {
            int i2;
            boolean bool1;
            boolean bool2;
            boolean bool3;
            boolean bool4;
            try
            {
                int n = 0xFF & arrayOfByte[i];
                int i1 = 0xFF & arrayOfByte[(i + 1)];
                i2 = 0xFF & arrayOfByte[(i + 2)];
                int i3 = 0xFF & arrayOfByte[(i + 3)];
                TextAlignment localTextAlignment = TextAlignment.fromInt(i2 & 0x3);
                FontSize localFontSize = FontSize.fromInt(0x3 & i2 >> 2);
                if (localFontSize != null)
                    break label207;
                localFontSize = FontSize.NORMAL;
                break label207;
                TextAttribute localTextAttribute = new TextAttribute(n, i1, localTextAlignment, localFontSize, bool1, bool2, bool3, bool4, TextColor.fromInt(i3));
                localArrayList.add(localTextAttribute);
                m++;
                i += 4;
                break;
                bool1 = false;
                break label218;
                bool2 = false;
                break label229;
                bool3 = false;
                break label240;
                bool4 = false;
                continue;
            }
            catch (IndexOutOfBoundsException localIndexOutOfBoundsException)
            {
                throw new ResultException(ResultCode.CMD_DATA_NOT_UNDERSTOOD);
            }
            localArrayList = null;
            label205: return localArrayList;
            label207: if ((i2 & 0x10) != 0)
            {
                bool1 = true;
                label218: if ((i2 & 0x20) != 0)
                {
                    bool2 = true;
                    label229: if ((i2 & 0x40) != 0)
                    {
                        bool3 = true;
                        label240: if ((i2 & 0x80) != 0)
                            bool4 = true;
                    }
                }
            }
        }
    }

    static String retrieveTextString(ComprehensionTlv paramComprehensionTlv)
        throws ResultException
    {
        byte[] arrayOfByte = paramComprehensionTlv.getRawValue();
        int i = paramComprehensionTlv.getValueIndex();
        int j = paramComprehensionTlv.getLength();
        if (j == 0);
        String str;
        for (Object localObject = null; ; localObject = str)
        {
            return localObject;
            int k = j - 1;
            try
            {
                int m = (byte)(0xC & arrayOfByte[i]);
                if (m == 0)
                    str = GsmAlphabet.gsm7BitPackedToString(arrayOfByte, i + 1, k * 8 / 7);
                else if (m == 4)
                    str = GsmAlphabet.gsm8BitUnpackedToString(arrayOfByte, i + 1, k);
                else if (m == 8)
                    str = new String(arrayOfByte, i + 1, k, "UTF-16");
                else
                    throw new ResultException(ResultCode.CMD_DATA_NOT_UNDERSTOOD);
            }
            catch (IndexOutOfBoundsException localIndexOutOfBoundsException)
            {
                throw new ResultException(ResultCode.CMD_DATA_NOT_UNDERSTOOD);
            }
            catch (UnsupportedEncodingException localUnsupportedEncodingException)
            {
                throw new ResultException(ResultCode.CMD_DATA_NOT_UNDERSTOOD);
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cat.ValueParser
 * JD-Core Version:        0.6.2
 */