package com.android.internal.telephony.cat;

public enum LaunchBrowserMode
{
    static
    {
        LAUNCH_NEW_BROWSER = new LaunchBrowserMode("LAUNCH_NEW_BROWSER", 2);
        LaunchBrowserMode[] arrayOfLaunchBrowserMode = new LaunchBrowserMode[3];
        arrayOfLaunchBrowserMode[0] = LAUNCH_IF_NOT_ALREADY_LAUNCHED;
        arrayOfLaunchBrowserMode[1] = USE_EXISTING_BROWSER;
        arrayOfLaunchBrowserMode[2] = LAUNCH_NEW_BROWSER;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cat.LaunchBrowserMode
 * JD-Core Version:        0.6.2
 */