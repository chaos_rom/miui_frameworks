package com.android.internal.telephony.cat;

import android.util.Log;

public abstract class CatLog
{
    static final boolean DEBUG = true;

    public static void d(Object paramObject, String paramString)
    {
        String str = paramObject.getClass().getName();
        Log.d("CAT", str.substring(1 + str.lastIndexOf('.')) + ": " + paramString);
    }

    public static void d(String paramString1, String paramString2)
    {
        Log.d("CAT", paramString1 + ": " + paramString2);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cat.CatLog
 * JD-Core Version:        0.6.2
 */