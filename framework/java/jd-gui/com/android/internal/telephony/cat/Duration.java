package com.android.internal.telephony.cat;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class Duration
    implements Parcelable
{
    public static final Parcelable.Creator<Duration> CREATOR = new Parcelable.Creator()
    {
        public Duration createFromParcel(Parcel paramAnonymousParcel)
        {
            return new Duration(paramAnonymousParcel, null);
        }

        public Duration[] newArray(int paramAnonymousInt)
        {
            return new Duration[paramAnonymousInt];
        }
    };
    public int timeInterval;
    public TimeUnit timeUnit;

    public Duration(int paramInt, TimeUnit paramTimeUnit)
    {
        this.timeInterval = paramInt;
        this.timeUnit = paramTimeUnit;
    }

    private Duration(Parcel paramParcel)
    {
        this.timeInterval = paramParcel.readInt();
        this.timeUnit = TimeUnit.values()[paramParcel.readInt()];
    }

    public int describeContents()
    {
        return 0;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeInt(this.timeInterval);
        paramParcel.writeInt(this.timeUnit.ordinal());
    }

    public static enum TimeUnit
    {
        private int mValue;

        static
        {
            TimeUnit[] arrayOfTimeUnit = new TimeUnit[3];
            arrayOfTimeUnit[0] = MINUTE;
            arrayOfTimeUnit[1] = SECOND;
            arrayOfTimeUnit[2] = TENTH_SECOND;
        }

        private TimeUnit(int paramInt)
        {
            this.mValue = paramInt;
        }

        public int value()
        {
            return this.mValue;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cat.Duration
 * JD-Core Version:        0.6.2
 */