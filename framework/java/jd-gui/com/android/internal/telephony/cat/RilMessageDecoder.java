package com.android.internal.telephony.cat;

import android.os.Handler;
import android.os.Message;
import com.android.internal.telephony.IccFileHandler;
import com.android.internal.util.State;
import com.android.internal.util.StateMachine;

class RilMessageDecoder extends StateMachine
{
    private static final int CMD_PARAMS_READY = 2;
    private static final int CMD_START = 1;
    private static RilMessageDecoder sInstance = null;
    private Handler mCaller = null;
    private CommandParamsFactory mCmdParamsFactory = null;
    private RilMessage mCurrentRilMessage = null;
    private StateCmdParamsReady mStateCmdParamsReady = new StateCmdParamsReady(null);
    private StateStart mStateStart = new StateStart(null);

    private RilMessageDecoder(Handler paramHandler, IccFileHandler paramIccFileHandler)
    {
        super("RilMessageDecoder");
        addState(this.mStateStart);
        addState(this.mStateCmdParamsReady);
        setInitialState(this.mStateStart);
        this.mCaller = paramHandler;
        this.mCmdParamsFactory = CommandParamsFactory.getInstance(this, paramIccFileHandler);
    }

    // ERROR //
    private boolean decodeMessageParams(RilMessage paramRilMessage)
    {
        // Byte code:
        //     0: aload_0
        //     1: aload_1
        //     2: putfield 44	com/android/internal/telephony/cat/RilMessageDecoder:mCurrentRilMessage	Lcom/android/internal/telephony/cat/RilMessage;
        //     5: aload_1
        //     6: getfield 107	com/android/internal/telephony/cat/RilMessage:mId	I
        //     9: tableswitch	default:+35 -> 44, 1:+39->48, 2:+62->71, 3:+62->71, 4:+39->48, 5:+62->71
        //     45: istore_3
        //     46: iload_3
        //     47: ireturn
        //     48: aload_0
        //     49: getfield 44	com/android/internal/telephony/cat/RilMessageDecoder:mCurrentRilMessage	Lcom/android/internal/telephony/cat/RilMessage;
        //     52: getstatic 113	com/android/internal/telephony/cat/ResultCode:OK	Lcom/android/internal/telephony/cat/ResultCode;
        //     55: putfield 116	com/android/internal/telephony/cat/RilMessage:mResCode	Lcom/android/internal/telephony/cat/ResultCode;
        //     58: aload_0
        //     59: aload_0
        //     60: getfield 44	com/android/internal/telephony/cat/RilMessageDecoder:mCurrentRilMessage	Lcom/android/internal/telephony/cat/RilMessage;
        //     63: invokespecial 89	com/android/internal/telephony/cat/RilMessageDecoder:sendCmdForExecution	(Lcom/android/internal/telephony/cat/RilMessage;)V
        //     66: iconst_0
        //     67: istore_3
        //     68: goto -22 -> 46
        //     71: aload_1
        //     72: getfield 120	com/android/internal/telephony/cat/RilMessage:mData	Ljava/lang/Object;
        //     75: checkcast 122	java/lang/String
        //     78: invokestatic 128	com/android/internal/telephony/IccUtils:hexStringToBytes	(Ljava/lang/String;)[B
        //     81: astore 4
        //     83: aload_0
        //     84: getfield 42	com/android/internal/telephony/cat/RilMessageDecoder:mCmdParamsFactory	Lcom/android/internal/telephony/cat/CommandParamsFactory;
        //     87: aload 4
        //     89: invokestatic 134	com/android/internal/telephony/cat/BerTlv:decode	([B)Lcom/android/internal/telephony/cat/BerTlv;
        //     92: invokevirtual 138	com/android/internal/telephony/cat/CommandParamsFactory:make	(Lcom/android/internal/telephony/cat/BerTlv;)V
        //     95: iconst_1
        //     96: istore_3
        //     97: goto -51 -> 46
        //     100: astore_2
        //     101: aload_0
        //     102: ldc 140
        //     104: invokestatic 146	com/android/internal/telephony/cat/CatLog:d	(Ljava/lang/Object;Ljava/lang/String;)V
        //     107: iconst_0
        //     108: istore_3
        //     109: goto -63 -> 46
        //     112: astore 5
        //     114: aload_0
        //     115: new 148	java/lang/StringBuilder
        //     118: dup
        //     119: invokespecial 150	java/lang/StringBuilder:<init>	()V
        //     122: ldc 152
        //     124: invokevirtual 156	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     127: aload 5
        //     129: invokevirtual 159	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     132: invokevirtual 163	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     135: invokestatic 146	com/android/internal/telephony/cat/CatLog:d	(Ljava/lang/Object;Ljava/lang/String;)V
        //     138: aload_0
        //     139: getfield 44	com/android/internal/telephony/cat/RilMessageDecoder:mCurrentRilMessage	Lcom/android/internal/telephony/cat/RilMessage;
        //     142: aload 5
        //     144: invokevirtual 167	com/android/internal/telephony/cat/ResultException:result	()Lcom/android/internal/telephony/cat/ResultCode;
        //     147: putfield 116	com/android/internal/telephony/cat/RilMessage:mResCode	Lcom/android/internal/telephony/cat/ResultCode;
        //     150: aload_0
        //     151: aload_0
        //     152: getfield 44	com/android/internal/telephony/cat/RilMessageDecoder:mCurrentRilMessage	Lcom/android/internal/telephony/cat/RilMessage;
        //     155: invokespecial 89	com/android/internal/telephony/cat/RilMessageDecoder:sendCmdForExecution	(Lcom/android/internal/telephony/cat/RilMessage;)V
        //     158: iconst_0
        //     159: istore_3
        //     160: goto -114 -> 46
        //
        // Exception table:
        //     from	to	target	type
        //     71	83	100	java/lang/Exception
        //     83	95	112	com/android/internal/telephony/cat/ResultException
    }

    /** @deprecated */
    public static RilMessageDecoder getInstance(Handler paramHandler, IccFileHandler paramIccFileHandler)
    {
        try
        {
            if (sInstance == null)
            {
                sInstance = new RilMessageDecoder(paramHandler, paramIccFileHandler);
                sInstance.start();
            }
            RilMessageDecoder localRilMessageDecoder = sInstance;
            return localRilMessageDecoder;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private void sendCmdForExecution(RilMessage paramRilMessage)
    {
        this.mCaller.obtainMessage(10, new RilMessage(paramRilMessage)).sendToTarget();
    }

    public void sendMsgParamsDecoded(ResultCode paramResultCode, CommandParams paramCommandParams)
    {
        Message localMessage = obtainMessage(2);
        localMessage.arg1 = paramResultCode.value();
        localMessage.obj = paramCommandParams;
        sendMessage(localMessage);
    }

    public void sendStartDecodingMessageParams(RilMessage paramRilMessage)
    {
        Message localMessage = obtainMessage(1);
        localMessage.obj = paramRilMessage;
        sendMessage(localMessage);
    }

    private class StateCmdParamsReady extends State
    {
        private StateCmdParamsReady()
        {
        }

        public boolean processMessage(Message paramMessage)
        {
            if (paramMessage.what == 2)
            {
                RilMessageDecoder.this.mCurrentRilMessage.mResCode = ResultCode.fromInt(paramMessage.arg1);
                RilMessageDecoder.this.mCurrentRilMessage.mData = paramMessage.obj;
                RilMessageDecoder.this.sendCmdForExecution(RilMessageDecoder.this.mCurrentRilMessage);
                RilMessageDecoder.this.transitionTo(RilMessageDecoder.this.mStateStart);
            }
            while (true)
            {
                return true;
                CatLog.d(this, "StateCmdParamsReady expecting CMD_PARAMS_READY=2 got " + paramMessage.what);
                RilMessageDecoder.this.deferMessage(paramMessage);
            }
        }
    }

    private class StateStart extends State
    {
        private StateStart()
        {
        }

        public boolean processMessage(Message paramMessage)
        {
            if (paramMessage.what == 1)
                if (RilMessageDecoder.this.decodeMessageParams((RilMessage)paramMessage.obj))
                    RilMessageDecoder.this.transitionTo(RilMessageDecoder.this.mStateCmdParamsReady);
            while (true)
            {
                return true;
                CatLog.d(this, "StateStart unexpected expecting START=1 got " + paramMessage.what);
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cat.RilMessageDecoder
 * JD-Core Version:        0.6.2
 */