package com.android.internal.telephony.cat;

public enum PresentationType
{
    static
    {
        DATA_VALUES = new PresentationType("DATA_VALUES", 1);
        NAVIGATION_OPTIONS = new PresentationType("NAVIGATION_OPTIONS", 2);
        PresentationType[] arrayOfPresentationType = new PresentationType[3];
        arrayOfPresentationType[0] = NOT_SPECIFIED;
        arrayOfPresentationType[1] = DATA_VALUES;
        arrayOfPresentationType[2] = NAVIGATION_OPTIONS;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cat.PresentationType
 * JD-Core Version:        0.6.2
 */