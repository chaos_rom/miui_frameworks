package com.android.internal.telephony.cat;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncResult;
import android.os.Handler;
import android.os.Message;
import android.os.SystemProperties;
import com.android.internal.telephony.CommandsInterface;
import com.android.internal.telephony.IccCard;
import com.android.internal.telephony.IccFileHandler;
import com.android.internal.telephony.IccRecords;
import com.android.internal.telephony.IccUtils;
import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.Locale;

public class CatService extends Handler
    implements AppInterface
{
    private static final int DEV_ID_DISPLAY = 2;
    private static final int DEV_ID_EARPIECE = 3;
    private static final int DEV_ID_KEYPAD = 1;
    private static final int DEV_ID_NETWORK = 131;
    private static final int DEV_ID_TERMINAL = 130;
    private static final int DEV_ID_UICC = 129;
    static final int MSG_ID_CALL_SETUP = 4;
    static final int MSG_ID_EVENT_NOTIFY = 3;
    private static final int MSG_ID_ICC_RECORDS_LOADED = 20;
    static final int MSG_ID_PROACTIVE_COMMAND = 2;
    static final int MSG_ID_REFRESH = 5;
    static final int MSG_ID_RESPONSE = 6;
    static final int MSG_ID_RIL_MSG_DECODED = 10;
    static final int MSG_ID_SESSION_END = 1;
    static final int MSG_ID_SIM_READY = 7;
    static final String STK_DEFAULT = "Defualt Message";
    private static IccRecords mIccRecords;
    private static CatService sInstance;
    private static final Object sInstanceLock = new Object();
    private CommandsInterface mCmdIf;
    private Context mContext;
    private CatCmdMessage mCurrntCmd = null;
    private CatCmdMessage mMenuCmd = null;
    private RilMessageDecoder mMsgDecoder = null;
    private boolean mStkAppInstalled = false;

    private CatService(CommandsInterface paramCommandsInterface, IccRecords paramIccRecords, Context paramContext, IccFileHandler paramIccFileHandler, IccCard paramIccCard)
    {
        if ((paramCommandsInterface == null) || (paramIccRecords == null) || (paramContext == null) || (paramIccFileHandler == null) || (paramIccCard == null))
            throw new NullPointerException("Service: Input parameters must not be null");
        this.mCmdIf = paramCommandsInterface;
        this.mContext = paramContext;
        this.mMsgDecoder = RilMessageDecoder.getInstance(this, paramIccFileHandler);
        this.mCmdIf.setOnCatSessionEnd(this, 1, null);
        this.mCmdIf.setOnCatProactiveCmd(this, 2, null);
        this.mCmdIf.setOnCatEvent(this, 3, null);
        this.mCmdIf.setOnCatCallSetUp(this, 4, null);
        mIccRecords = paramIccRecords;
        paramIccCard.registerForReady(this, 7, null);
        mIccRecords.registerForRecordsLoaded(this, 20, null);
        this.mStkAppInstalled = isStkAppInstalled();
        CatLog.d(this, "Running CAT service. STK app installed:" + this.mStkAppInstalled);
    }

    private void encodeOptionalTags(CommandDetails paramCommandDetails, ResultCode paramResultCode, Input paramInput, ByteArrayOutputStream paramByteArrayOutputStream)
    {
        AppInterface.CommandType localCommandType = AppInterface.CommandType.fromInt(paramCommandDetails.typeOfCommand);
        if (localCommandType != null)
            switch (1.$SwitchMap$com$android$internal$telephony$cat$AppInterface$CommandType[localCommandType.ordinal()])
            {
            default:
                CatLog.d(this, "encodeOptionalTags() Unsupported Cmd details=" + paramCommandDetails);
            case 9:
            case 5:
            }
        while (true)
        {
            return;
            if ((paramResultCode.value() == ResultCode.NO_RESPONSE_FROM_USER.value()) && (paramInput != null) && (paramInput.duration != null))
            {
                getInKeyResponse(paramByteArrayOutputStream, paramInput);
                continue;
                if ((paramCommandDetails.commandQualifier == 4) && (paramResultCode.value() == ResultCode.OK.value()))
                {
                    getPliResponse(paramByteArrayOutputStream);
                    continue;
                    CatLog.d(this, "encodeOptionalTags() bad Cmd details=" + paramCommandDetails);
                }
            }
        }
    }

    private void eventDownload(int paramInt1, int paramInt2, int paramInt3, byte[] paramArrayOfByte, boolean paramBoolean)
    {
        ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
        localByteArrayOutputStream.write(214);
        localByteArrayOutputStream.write(0);
        localByteArrayOutputStream.write(0x80 | ComprehensionTlvTag.EVENT_LIST.value());
        localByteArrayOutputStream.write(1);
        localByteArrayOutputStream.write(paramInt1);
        localByteArrayOutputStream.write(0x80 | ComprehensionTlvTag.DEVICE_IDENTITIES.value());
        localByteArrayOutputStream.write(2);
        localByteArrayOutputStream.write(paramInt2);
        localByteArrayOutputStream.write(paramInt3);
        if (paramArrayOfByte != null)
        {
            int i = paramArrayOfByte.length;
            for (int j = 0; j < i; j++)
                localByteArrayOutputStream.write(paramArrayOfByte[j]);
        }
        byte[] arrayOfByte = localByteArrayOutputStream.toByteArray();
        arrayOfByte[1] = ((byte)(-2 + arrayOfByte.length));
        String str = IccUtils.bytesToHexString(arrayOfByte);
        this.mCmdIf.sendEnvelope(str, null);
    }

    private void getInKeyResponse(ByteArrayOutputStream paramByteArrayOutputStream, Input paramInput)
    {
        paramByteArrayOutputStream.write(ComprehensionTlvTag.DURATION.value());
        paramByteArrayOutputStream.write(2);
        paramByteArrayOutputStream.write(Duration.TimeUnit.SECOND.value());
        paramByteArrayOutputStream.write(paramInput.duration.timeInterval);
    }

    public static AppInterface getInstance()
    {
        return getInstance(null, null, null, null, null);
    }

    // ERROR //
    public static CatService getInstance(CommandsInterface paramCommandsInterface, IccRecords paramIccRecords, Context paramContext, IccFileHandler paramIccFileHandler, IccCard paramIccCard)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore 5
        //     3: getstatic 66	com/android/internal/telephony/cat/CatService:sInstanceLock	Ljava/lang/Object;
        //     6: astore 6
        //     8: aload 6
        //     10: monitorenter
        //     11: getstatic 260	com/android/internal/telephony/cat/CatService:sInstance	Lcom/android/internal/telephony/cat/CatService;
        //     14: ifnonnull +87 -> 101
        //     17: aload_0
        //     18: ifnull +20 -> 38
        //     21: aload_1
        //     22: ifnull +16 -> 38
        //     25: aload_2
        //     26: ifnull +12 -> 38
        //     29: aload_3
        //     30: ifnull +8 -> 38
        //     33: aload 4
        //     35: ifnonnull +9 -> 44
        //     38: aload 6
        //     40: monitorexit
        //     41: goto +120 -> 161
        //     44: new 262	android/os/HandlerThread
        //     47: dup
        //     48: ldc_w 264
        //     51: invokespecial 265	android/os/HandlerThread:<init>	(Ljava/lang/String;)V
        //     54: invokevirtual 268	android/os/HandlerThread:start	()V
        //     57: new 2	com/android/internal/telephony/cat/CatService
        //     60: dup
        //     61: aload_0
        //     62: aload_1
        //     63: aload_2
        //     64: aload_3
        //     65: aload 4
        //     67: invokespecial 270	com/android/internal/telephony/cat/CatService:<init>	(Lcom/android/internal/telephony/CommandsInterface;Lcom/android/internal/telephony/IccRecords;Landroid/content/Context;Lcom/android/internal/telephony/IccFileHandler;Lcom/android/internal/telephony/IccCard;)V
        //     70: putstatic 260	com/android/internal/telephony/cat/CatService:sInstance	Lcom/android/internal/telephony/cat/CatService;
        //     73: getstatic 260	com/android/internal/telephony/cat/CatService:sInstance	Lcom/android/internal/telephony/cat/CatService;
        //     76: ldc_w 272
        //     79: invokestatic 146	com/android/internal/telephony/cat/CatLog:d	(Ljava/lang/Object;Ljava/lang/String;)V
        //     82: getstatic 260	com/android/internal/telephony/cat/CatService:sInstance	Lcom/android/internal/telephony/cat/CatService;
        //     85: astore 5
        //     87: aload 6
        //     89: monitorexit
        //     90: goto +71 -> 161
        //     93: astore 7
        //     95: aload 6
        //     97: monitorexit
        //     98: aload 7
        //     100: athrow
        //     101: aload_1
        //     102: ifnull +47 -> 149
        //     105: getstatic 110	com/android/internal/telephony/cat/CatService:mIccRecords	Lcom/android/internal/telephony/IccRecords;
        //     108: aload_1
        //     109: if_acmpeq +40 -> 149
        //     112: getstatic 260	com/android/internal/telephony/cat/CatService:sInstance	Lcom/android/internal/telephony/cat/CatService;
        //     115: ldc_w 274
        //     118: invokestatic 146	com/android/internal/telephony/cat/CatLog:d	(Ljava/lang/Object;Ljava/lang/String;)V
        //     121: aload_1
        //     122: putstatic 110	com/android/internal/telephony/cat/CatService:mIccRecords	Lcom/android/internal/telephony/IccRecords;
        //     125: getstatic 110	com/android/internal/telephony/cat/CatService:mIccRecords	Lcom/android/internal/telephony/IccRecords;
        //     128: getstatic 260	com/android/internal/telephony/cat/CatService:sInstance	Lcom/android/internal/telephony/cat/CatService;
        //     131: bipush 20
        //     133: aconst_null
        //     134: invokevirtual 120	com/android/internal/telephony/IccRecords:registerForRecordsLoaded	(Landroid/os/Handler;ILjava/lang/Object;)V
        //     137: getstatic 260	com/android/internal/telephony/cat/CatService:sInstance	Lcom/android/internal/telephony/cat/CatService;
        //     140: ldc_w 276
        //     143: invokestatic 146	com/android/internal/telephony/cat/CatLog:d	(Ljava/lang/Object;Ljava/lang/String;)V
        //     146: goto -64 -> 82
        //     149: getstatic 260	com/android/internal/telephony/cat/CatService:sInstance	Lcom/android/internal/telephony/cat/CatService;
        //     152: ldc_w 278
        //     155: invokestatic 146	com/android/internal/telephony/cat/CatLog:d	(Ljava/lang/Object;Ljava/lang/String;)V
        //     158: goto -76 -> 82
        //     161: aload 5
        //     163: areturn
        //
        // Exception table:
        //     from	to	target	type
        //     11	98	93	finally
        //     105	158	93	finally
    }

    private void getPliResponse(ByteArrayOutputStream paramByteArrayOutputStream)
    {
        String str = SystemProperties.get("persist.sys.language");
        if (str != null)
        {
            paramByteArrayOutputStream.write(ComprehensionTlvTag.LANGUAGE.value());
            ResponseData.writeLength(paramByteArrayOutputStream, str.length());
            paramByteArrayOutputStream.write(str.getBytes(), 0, str.length());
        }
    }

    private void handleCmdResponse(CatResponseMessage paramCatResponseMessage)
    {
        if (!validateResponse(paramCatResponseMessage));
        Object localObject;
        CommandDetails localCommandDetails;
        while (true)
        {
            return;
            localObject = null;
            i = 0;
            localCommandDetails = paramCatResponseMessage.getCmdDetails();
            switch (1.$SwitchMap$com$android$internal$telephony$cat$ResultCode[paramCatResponseMessage.resCode.ordinal()])
            {
            default:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            }
        }
        int i = 1;
        switch (1.$SwitchMap$com$android$internal$telephony$cat$AppInterface$CommandType[AppInterface.CommandType.fromInt(localCommandDetails.typeOfCommand).ordinal()])
        {
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 10:
        case 11:
        case 12:
        case 13:
        case 14:
        default:
        case 1:
        case 7:
        case 8:
        case 9:
        case 15:
        }
        while (true)
        {
            sendTerminalResponse(localCommandDetails, paramCatResponseMessage.resCode, false, 0, (ResponseData)localObject);
            this.mCurrntCmd = null;
            break;
            if (paramCatResponseMessage.resCode == ResultCode.HELP_INFO_REQUIRED);
            for (boolean bool = true; ; bool = false)
            {
                sendMenuSelection(paramCatResponseMessage.usersMenuSelection, bool);
                break;
            }
            localObject = new SelectItemResponseData(paramCatResponseMessage.usersMenuSelection);
            continue;
            Input localInput = this.mCurrntCmd.geInput();
            if (!localInput.yesNo)
            {
                if (i == 0)
                    localObject = new GetInkeyInputResponseData(paramCatResponseMessage.usersInput, localInput.ucs2, localInput.packed);
            }
            else
            {
                localObject = new GetInkeyInputResponseData(paramCatResponseMessage.usersYesNoSelection);
                continue;
                this.mCmdIf.handleCallSetupRequestFromSim(paramCatResponseMessage.usersConfirm, null);
                this.mCurrntCmd = null;
                break;
                localObject = null;
            }
        }
    }

    private void handleCommand(CommandParams paramCommandParams, boolean paramBoolean)
    {
        CatLog.d(this, paramCommandParams.getCommandType().name());
        CatCmdMessage localCatCmdMessage = new CatCmdMessage(paramCommandParams);
        switch (1.$SwitchMap$com$android$internal$telephony$cat$AppInterface$CommandType[paramCommandParams.getCommandType().ordinal()])
        {
        default:
            CatLog.d(this, "Unsupported command");
            return;
        case 1:
            if (removeMenu(localCatCmdMessage.getMenu()))
            {
                this.mMenuCmd = null;
                label144: sendTerminalResponse(paramCommandParams.cmdDet, ResultCode.OK, false, 0, null);
            }
            break;
        case 7:
        case 8:
        case 9:
        case 14:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 10:
        case 11:
        case 12:
        case 13:
        case 15:
        case 16:
        case 17:
        case 18:
        case 19:
        }
        while (true)
        {
            this.mCurrntCmd = localCatCmdMessage;
            Intent localIntent = new Intent("android.intent.action.stk.command");
            localIntent.putExtra("STK CMD", localCatCmdMessage);
            this.mContext.sendBroadcast(localIntent);
            break;
            this.mMenuCmd = localCatCmdMessage;
            break label144;
            if (!localCatCmdMessage.geTextMessage().responseNeeded)
            {
                sendTerminalResponse(paramCommandParams.cmdDet, ResultCode.OK, false, 0, null);
                continue;
                paramCommandParams.cmdDet.typeOfCommand = AppInterface.CommandType.SET_UP_IDLE_MODE_TEXT.value();
                continue;
                sendTerminalResponse(paramCommandParams.cmdDet, ResultCode.OK, false, 0, null);
                continue;
                switch (paramCommandParams.cmdDet.commandQualifier)
                {
                default:
                    sendTerminalResponse(paramCommandParams.cmdDet, ResultCode.OK, false, 0, null);
                    break;
                case 3:
                    DTTZResponseData localDTTZResponseData = new DTTZResponseData(null);
                    sendTerminalResponse(paramCommandParams.cmdDet, ResultCode.OK, false, 0, localDTTZResponseData);
                    break;
                case 4:
                    LanguageResponseData localLanguageResponseData = new LanguageResponseData(Locale.getDefault().getLanguage());
                    sendTerminalResponse(paramCommandParams.cmdDet, ResultCode.OK, false, 0, localLanguageResponseData);
                    break;
                    if ((((LaunchBrowserParams)paramCommandParams).confirmMsg.text != null) && (((LaunchBrowserParams)paramCommandParams).confirmMsg.text.equals("Defualt Message")))
                    {
                        CharSequence localCharSequence3 = this.mContext.getText(17040633);
                        ((LaunchBrowserParams)paramCommandParams).confirmMsg.text = localCharSequence3.toString();
                        continue;
                        if ((((DisplayTextParams)paramCommandParams).textMsg.text != null) && (((DisplayTextParams)paramCommandParams).textMsg.text.equals("Defualt Message")))
                        {
                            CharSequence localCharSequence2 = this.mContext.getText(17040632);
                            ((DisplayTextParams)paramCommandParams).textMsg.text = localCharSequence2.toString();
                            continue;
                            if ((((CallSetupParams)paramCommandParams).confirmMsg.text != null) && (((CallSetupParams)paramCommandParams).confirmMsg.text.equals("Defualt Message")))
                            {
                                CharSequence localCharSequence1 = this.mContext.getText(17040634);
                                ((CallSetupParams)paramCommandParams).confirmMsg.text = localCharSequence1.toString();
                                continue;
                                BIPClientParams localBIPClientParams = (BIPClientParams)paramCommandParams;
                                if ((localBIPClientParams.bHasAlphaId) && (localBIPClientParams.textMsg.text == null))
                                {
                                    CatLog.d(this, "cmd " + paramCommandParams.getCommandType() + " with null alpha id");
                                    if (!paramBoolean)
                                        break;
                                    sendTerminalResponse(paramCommandParams.cmdDet, ResultCode.OK, false, 0, null);
                                    break;
                                }
                                if (!this.mStkAppInstalled)
                                {
                                    CatLog.d(this, "No STK application found.");
                                    if (paramBoolean)
                                    {
                                        sendTerminalResponse(paramCommandParams.cmdDet, ResultCode.BEYOND_TERMINAL_CAPABILITY, false, 0, null);
                                        break;
                                    }
                                }
                                if ((paramBoolean) && ((paramCommandParams.getCommandType() == AppInterface.CommandType.CLOSE_CHANNEL) || (paramCommandParams.getCommandType() == AppInterface.CommandType.RECEIVE_DATA) || (paramCommandParams.getCommandType() == AppInterface.CommandType.SEND_DATA)))
                                    sendTerminalResponse(paramCommandParams.cmdDet, ResultCode.OK, false, 0, null);
                            }
                        }
                    }
                    break;
                }
            }
        }
    }

    private void handleRilMsg(RilMessage paramRilMessage)
    {
        if (paramRilMessage == null);
        while (true)
        {
            return;
            switch (paramRilMessage.mId)
            {
            case 4:
            default:
                break;
            case 1:
                handleSessionEnd();
                break;
            case 3:
                if (paramRilMessage.mResCode == ResultCode.OK)
                {
                    CommandParams localCommandParams3 = (CommandParams)paramRilMessage.mData;
                    if (localCommandParams3 != null)
                        handleCommand(localCommandParams3, false);
                }
                break;
            case 2:
                CommandParams localCommandParams2;
                try
                {
                    localCommandParams2 = (CommandParams)paramRilMessage.mData;
                    if (localCommandParams2 == null)
                        continue;
                    if (paramRilMessage.mResCode != ResultCode.OK)
                        break label157;
                    handleCommand(localCommandParams2, true);
                }
                catch (ClassCastException localClassCastException)
                {
                    CatLog.d(this, "Fail to parse proactive command");
                }
                if (this.mCurrntCmd != null)
                {
                    sendTerminalResponse(this.mCurrntCmd.mCmdDet, ResultCode.CMD_DATA_NOT_UNDERSTOOD, false, 0, null);
                    continue;
                    sendTerminalResponse(localCommandParams2.cmdDet, paramRilMessage.mResCode, false, 0, null);
                }
                break;
            case 5:
                label157: CommandParams localCommandParams1 = (CommandParams)paramRilMessage.mData;
                if (localCommandParams1 != null)
                    handleCommand(localCommandParams1, false);
                break;
            }
        }
    }

    private void handleSessionEnd()
    {
        CatLog.d(this, "SESSION END");
        this.mCurrntCmd = this.mMenuCmd;
        Intent localIntent = new Intent("android.intent.action.stk.session_end");
        this.mContext.sendBroadcast(localIntent);
    }

    private boolean isStkAppInstalled()
    {
        boolean bool = false;
        Intent localIntent = new Intent("android.intent.action.stk.command");
        List localList = this.mContext.getPackageManager().queryBroadcastReceivers(localIntent, 128);
        if (localList == null);
        for (int i = 0; ; i = localList.size())
        {
            if (i > 0)
                bool = true;
            return bool;
        }
    }

    private boolean removeMenu(Menu paramMenu)
    {
        int i = 1;
        try
        {
            if (paramMenu.items.size() == i)
            {
                Object localObject = paramMenu.items.get(0);
                if (localObject == null)
                    return i;
            }
        }
        catch (NullPointerException localNullPointerException)
        {
            while (true)
            {
                CatLog.d(this, "Unable to get Menu's items size");
                continue;
                int j = 0;
            }
        }
    }

    private void sendMenuSelection(int paramInt, boolean paramBoolean)
    {
        ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
        localByteArrayOutputStream.write(211);
        localByteArrayOutputStream.write(0);
        localByteArrayOutputStream.write(0x80 | ComprehensionTlvTag.DEVICE_IDENTITIES.value());
        localByteArrayOutputStream.write(2);
        localByteArrayOutputStream.write(1);
        localByteArrayOutputStream.write(129);
        localByteArrayOutputStream.write(0x80 | ComprehensionTlvTag.ITEM_ID.value());
        localByteArrayOutputStream.write(1);
        localByteArrayOutputStream.write(paramInt);
        if (paramBoolean)
        {
            localByteArrayOutputStream.write(ComprehensionTlvTag.HELP_REQUEST.value());
            localByteArrayOutputStream.write(0);
        }
        byte[] arrayOfByte = localByteArrayOutputStream.toByteArray();
        arrayOfByte[1] = ((byte)(-2 + arrayOfByte.length));
        String str = IccUtils.bytesToHexString(arrayOfByte);
        this.mCmdIf.sendEnvelope(str, null);
    }

    private void sendTerminalResponse(CommandDetails paramCommandDetails, ResultCode paramResultCode, boolean paramBoolean, int paramInt, ResponseData paramResponseData)
    {
        int i = 2;
        if (paramCommandDetails == null)
            return;
        ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
        Input localInput = null;
        if (this.mCurrntCmd != null)
            localInput = this.mCurrntCmd.geInput();
        int j = ComprehensionTlvTag.COMMAND_DETAILS.value();
        if (paramCommandDetails.compRequired)
            j |= 128;
        localByteArrayOutputStream.write(j);
        localByteArrayOutputStream.write(3);
        localByteArrayOutputStream.write(paramCommandDetails.commandNumber);
        localByteArrayOutputStream.write(paramCommandDetails.typeOfCommand);
        localByteArrayOutputStream.write(paramCommandDetails.commandQualifier);
        localByteArrayOutputStream.write(ComprehensionTlvTag.DEVICE_IDENTITIES.value());
        localByteArrayOutputStream.write(i);
        localByteArrayOutputStream.write(130);
        localByteArrayOutputStream.write(129);
        localByteArrayOutputStream.write(0x80 | ComprehensionTlvTag.RESULT.value());
        if (paramBoolean)
        {
            label152: localByteArrayOutputStream.write(i);
            localByteArrayOutputStream.write(paramResultCode.value());
            if (paramBoolean)
                localByteArrayOutputStream.write(paramInt);
            if (paramResponseData == null)
                break label222;
            paramResponseData.format(localByteArrayOutputStream);
        }
        while (true)
        {
            String str = IccUtils.bytesToHexString(localByteArrayOutputStream.toByteArray());
            this.mCmdIf.sendTerminalResponse(str, null);
            break;
            i = 1;
            break label152;
            label222: encodeOptionalTags(paramCommandDetails, paramResultCode, localInput, localByteArrayOutputStream);
        }
    }

    private boolean validateResponse(CatResponseMessage paramCatResponseMessage)
    {
        if (this.mCurrntCmd != null);
        for (boolean bool = paramCatResponseMessage.cmdDet.compareTo(this.mCurrntCmd.mCmdDet); ; bool = false)
            return bool;
    }

    public void dispose()
    {
        mIccRecords.unregisterForRecordsLoaded(this);
        this.mCmdIf.unSetOnCatSessionEnd(this);
        this.mCmdIf.unSetOnCatProactiveCmd(this);
        this.mCmdIf.unSetOnCatEvent(this);
        this.mCmdIf.unSetOnCatCallSetUp(this);
        removeCallbacksAndMessages(null);
    }

    protected void finalize()
    {
        CatLog.d(this, "Service finalized");
    }

    public void handleMessage(Message paramMessage)
    {
        String str;
        AsyncResult localAsyncResult;
        switch (paramMessage.what)
        {
        case 8:
        case 9:
        case 11:
        case 12:
        case 13:
        case 14:
        case 15:
        case 16:
        case 17:
        case 18:
        case 19:
        default:
            throw new AssertionError("Unrecognized CAT command: " + paramMessage.what);
        case 1:
        case 2:
        case 3:
        case 5:
            CatLog.d(this, "ril message arrived");
            str = null;
            if (paramMessage.obj != null)
            {
                localAsyncResult = (AsyncResult)paramMessage.obj;
                if ((localAsyncResult == null) || (localAsyncResult.result == null))
                    break;
            }
            break;
        case 20:
        case 4:
        case 10:
        case 6:
        case 7:
        }
        try
        {
            str = (String)localAsyncResult.result;
            this.mMsgDecoder.sendStartDecodingMessageParams(new RilMessage(paramMessage.what, str));
            return;
        }
        catch (ClassCastException localClassCastException)
        {
            while (true)
            {
                continue;
                this.mMsgDecoder.sendStartDecodingMessageParams(new RilMessage(paramMessage.what, null));
                continue;
                handleRilMsg((RilMessage)paramMessage.obj);
                continue;
                handleCmdResponse((CatResponseMessage)paramMessage.obj);
                continue;
                CatLog.d(this, "SIM ready. Reporting STK service running now...");
                this.mCmdIf.reportStkServiceIsRunning(null);
            }
        }
    }

    /** @deprecated */
    public void onCmdResponse(CatResponseMessage paramCatResponseMessage)
    {
        if (paramCatResponseMessage == null);
        while (true)
        {
            return;
            try
            {
                obtainMessage(6, paramCatResponseMessage).sendToTarget();
            }
            finally
            {
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cat.CatService
 * JD-Core Version:        0.6.2
 */