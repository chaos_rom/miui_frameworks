package com.android.internal.telephony.cat;

public class ImageDescriptor
{
    static final int CODING_SCHEME_BASIC = 17;
    static final int CODING_SCHEME_COLOUR = 33;
    int codingScheme = 0;
    int height = 0;
    int highOffset = 0;
    int imageId = 0;
    int length = 0;
    int lowOffset = 0;
    int width = 0;

    // ERROR //
    static ImageDescriptor parse(byte[] paramArrayOfByte, int paramInt)
    {
        // Byte code:
        //     0: new 2	com/android/internal/telephony/cat/ImageDescriptor
        //     3: dup
        //     4: invokespecial 39	com/android/internal/telephony/cat/ImageDescriptor:<init>	()V
        //     7: astore_2
        //     8: iload_1
        //     9: iconst_1
        //     10: iadd
        //     11: istore_3
        //     12: aload_2
        //     13: sipush 255
        //     16: aload_0
        //     17: iload_1
        //     18: baload
        //     19: iand
        //     20: putfield 22	com/android/internal/telephony/cat/ImageDescriptor:width	I
        //     23: iload_3
        //     24: iconst_1
        //     25: iadd
        //     26: istore 6
        //     28: aload_2
        //     29: sipush 255
        //     32: aload_0
        //     33: iload_3
        //     34: baload
        //     35: iand
        //     36: putfield 24	com/android/internal/telephony/cat/ImageDescriptor:height	I
        //     39: iload 6
        //     41: iconst_1
        //     42: iadd
        //     43: istore_3
        //     44: aload_2
        //     45: sipush 255
        //     48: aload_0
        //     49: iload 6
        //     51: baload
        //     52: iand
        //     53: putfield 26	com/android/internal/telephony/cat/ImageDescriptor:codingScheme	I
        //     56: iload_3
        //     57: iconst_1
        //     58: iadd
        //     59: istore 8
        //     61: aload_2
        //     62: sipush 255
        //     65: aload_0
        //     66: iload_3
        //     67: baload
        //     68: iand
        //     69: bipush 8
        //     71: ishl
        //     72: putfield 28	com/android/internal/telephony/cat/ImageDescriptor:imageId	I
        //     75: aload_2
        //     76: getfield 28	com/android/internal/telephony/cat/ImageDescriptor:imageId	I
        //     79: istore 9
        //     81: iload 8
        //     83: iconst_1
        //     84: iadd
        //     85: istore_3
        //     86: aload_2
        //     87: iload 9
        //     89: sipush 255
        //     92: aload_0
        //     93: iload 8
        //     95: baload
        //     96: iand
        //     97: ior
        //     98: putfield 28	com/android/internal/telephony/cat/ImageDescriptor:imageId	I
        //     101: iload_3
        //     102: iconst_1
        //     103: iadd
        //     104: istore 10
        //     106: aload_2
        //     107: sipush 255
        //     110: aload_0
        //     111: iload_3
        //     112: baload
        //     113: iand
        //     114: putfield 30	com/android/internal/telephony/cat/ImageDescriptor:highOffset	I
        //     117: iload 10
        //     119: iconst_1
        //     120: iadd
        //     121: istore_3
        //     122: aload_2
        //     123: sipush 255
        //     126: aload_0
        //     127: iload 10
        //     129: baload
        //     130: iand
        //     131: putfield 32	com/android/internal/telephony/cat/ImageDescriptor:lowOffset	I
        //     134: iload_3
        //     135: iconst_1
        //     136: iadd
        //     137: istore 11
        //     139: aload_0
        //     140: iload_3
        //     141: baload
        //     142: istore 12
        //     144: iload 12
        //     146: sipush 255
        //     149: iand
        //     150: bipush 8
        //     152: ishl
        //     153: istore 13
        //     155: iload 11
        //     157: iconst_1
        //     158: iadd
        //     159: istore_3
        //     160: aload_2
        //     161: iload 13
        //     163: sipush 255
        //     166: aload_0
        //     167: iload 11
        //     169: baload
        //     170: iand
        //     171: ior
        //     172: putfield 34	com/android/internal/telephony/cat/ImageDescriptor:length	I
        //     175: iload_3
        //     176: pop
        //     177: aload_2
        //     178: areturn
        //     179: astore 4
        //     181: iload_3
        //     182: pop
        //     183: ldc 41
        //     185: ldc 43
        //     187: invokestatic 49	com/android/internal/telephony/cat/CatLog:d	(Ljava/lang/String;Ljava/lang/String;)V
        //     190: aconst_null
        //     191: astore_2
        //     192: goto -15 -> 177
        //     195: astore 7
        //     197: goto -14 -> 183
        //
        // Exception table:
        //     from	to	target	type
        //     12	23	179	java/lang/IndexOutOfBoundsException
        //     44	56	179	java/lang/IndexOutOfBoundsException
        //     86	101	179	java/lang/IndexOutOfBoundsException
        //     122	134	179	java/lang/IndexOutOfBoundsException
        //     160	175	179	java/lang/IndexOutOfBoundsException
        //     28	39	195	java/lang/IndexOutOfBoundsException
        //     61	81	195	java/lang/IndexOutOfBoundsException
        //     106	117	195	java/lang/IndexOutOfBoundsException
        //     139	144	195	java/lang/IndexOutOfBoundsException
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cat.ImageDescriptor
 * JD-Core Version:        0.6.2
 */