package com.android.internal.telephony.cat;

class ItemsIconId extends ValueObject
{
    int[] recordNumbers;
    boolean selfExplanatory;

    ComprehensionTlvTag getTag()
    {
        return ComprehensionTlvTag.ITEM_ICON_ID_LIST;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cat.ItemsIconId
 * JD-Core Version:        0.6.2
 */