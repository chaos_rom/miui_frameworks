package com.android.internal.telephony.cat;

import android.graphics.Bitmap;

class GetInputParams extends CommandParams
{
    Input input = null;

    GetInputParams(CommandDetails paramCommandDetails, Input paramInput)
    {
        super(paramCommandDetails);
        this.input = paramInput;
    }

    boolean setIcon(Bitmap paramBitmap)
    {
        if ((paramBitmap != null) && (this.input != null))
            this.input.icon = paramBitmap;
        return true;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cat.GetInputParams
 * JD-Core Version:        0.6.2
 */