package com.android.internal.telephony;

public class IccFileNotFound extends IccException
{
    IccFileNotFound()
    {
    }

    IccFileNotFound(int paramInt)
    {
        super("ICC EF Not Found 0x" + Integer.toHexString(paramInt));
    }

    IccFileNotFound(String paramString)
    {
        super(paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.IccFileNotFound
 * JD-Core Version:        0.6.2
 */