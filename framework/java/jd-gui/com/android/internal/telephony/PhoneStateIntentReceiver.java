package com.android.internal.telephony;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Message;
import android.telephony.ServiceState;
import android.telephony.SignalStrength;
import android.util.Log;

@Deprecated
public final class PhoneStateIntentReceiver extends BroadcastReceiver
{
    private static final boolean DBG = false;
    private static final String LOG_TAG = "PHONE";
    private static final int NOTIF_MAX = 32;
    private static final int NOTIF_PHONE = 1;
    private static final int NOTIF_SERVICE = 2;
    private static final int NOTIF_SIGNAL = 4;
    private int mAsuEventWhat;
    private Context mContext;
    private IntentFilter mFilter = new IntentFilter();
    private int mLocationEventWhat;
    Phone.State mPhoneState = Phone.State.IDLE;
    private int mPhoneStateEventWhat;
    ServiceState mServiceState = new ServiceState();
    private int mServiceStateEventWhat;
    SignalStrength mSignalStrength = new SignalStrength();
    private Handler mTarget;
    private int mWants;

    public PhoneStateIntentReceiver()
    {
    }

    public PhoneStateIntentReceiver(Context paramContext, Handler paramHandler)
    {
        this();
        setContext(paramContext);
        setTarget(paramHandler);
    }

    public boolean getNotifyPhoneCallState()
    {
        if ((0x1 & this.mWants) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean getNotifyServiceState()
    {
        if ((0x2 & this.mWants) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean getNotifySignalStrength()
    {
        if ((0x4 & this.mWants) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public Phone.State getPhoneState()
    {
        if ((0x1 & this.mWants) == 0)
            throw new RuntimeException("client must call notifyPhoneCallState(int)");
        return this.mPhoneState;
    }

    public ServiceState getServiceState()
    {
        if ((0x2 & this.mWants) == 0)
            throw new RuntimeException("client must call notifyServiceState(int)");
        return this.mServiceState;
    }

    public int getSignalStrengthDbm()
    {
        if ((0x4 & this.mWants) == 0)
            throw new RuntimeException("client must call notifySignalStrength(int)");
        return this.mSignalStrength.getDbm();
    }

    public int getSignalStrengthLevelAsu()
    {
        if ((0x4 & this.mWants) == 0)
            throw new RuntimeException("client must call notifySignalStrength(int)");
        return this.mSignalStrength.getAsuLevel();
    }

    public void notifyPhoneCallState(int paramInt)
    {
        this.mWants = (0x1 | this.mWants);
        this.mPhoneStateEventWhat = paramInt;
        this.mFilter.addAction("android.intent.action.PHONE_STATE");
    }

    public void notifyServiceState(int paramInt)
    {
        this.mWants = (0x2 | this.mWants);
        this.mServiceStateEventWhat = paramInt;
        this.mFilter.addAction("android.intent.action.SERVICE_STATE");
    }

    public void notifySignalStrength(int paramInt)
    {
        this.mWants = (0x4 | this.mWants);
        this.mAsuEventWhat = paramInt;
        this.mFilter.addAction("android.intent.action.SIG_STR");
    }

    public void onReceive(Context paramContext, Intent paramIntent)
    {
        String str = paramIntent.getAction();
        try
        {
            if ("android.intent.action.SIG_STR".equals(str))
            {
                this.mSignalStrength = SignalStrength.newFromBundle(paramIntent.getExtras());
                if ((this.mTarget != null) && (getNotifySignalStrength()))
                {
                    Message localMessage3 = Message.obtain(this.mTarget, this.mAsuEventWhat);
                    this.mTarget.sendMessage(localMessage3);
                }
            }
            else if ("android.intent.action.PHONE_STATE".equals(str))
            {
                this.mPhoneState = ((Phone.State)Enum.valueOf(Phone.State.class, paramIntent.getStringExtra("state")));
                if ((this.mTarget == null) || (!getNotifyPhoneCallState()))
                    return;
                Message localMessage2 = Message.obtain(this.mTarget, this.mPhoneStateEventWhat);
                this.mTarget.sendMessage(localMessage2);
            }
        }
        catch (Exception localException)
        {
            Log.e("PHONE", "[PhoneStateIntentRecv] caught " + localException);
            localException.printStackTrace();
        }
        if ("android.intent.action.SERVICE_STATE".equals(str))
        {
            this.mServiceState = ServiceState.newFromBundle(paramIntent.getExtras());
            if ((this.mTarget != null) && (getNotifyServiceState()))
            {
                Message localMessage1 = Message.obtain(this.mTarget, this.mServiceStateEventWhat);
                this.mTarget.sendMessage(localMessage1);
            }
        }
    }

    public void registerIntent()
    {
        this.mContext.registerReceiver(this, this.mFilter);
    }

    public void setContext(Context paramContext)
    {
        this.mContext = paramContext;
    }

    public void setTarget(Handler paramHandler)
    {
        this.mTarget = paramHandler;
    }

    public void unregisterIntent()
    {
        this.mContext.unregisterReceiver(this);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.PhoneStateIntentReceiver
 * JD-Core Version:        0.6.2
 */