package com.android.internal.telephony;

import android.content.Context;
import android.media.AudioManager;
import android.os.AsyncResult;
import android.os.Handler;
import android.os.Message;
import android.os.Registrant;
import android.os.RegistrantList;
import android.telephony.ServiceState;
import android.util.Log;
import com.android.internal.telephony.sip.SipPhone;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public final class CallManager
{
    private static final boolean DBG = true;
    private static final int EVENT_CALL_WAITING = 108;
    private static final int EVENT_CDMA_OTA_STATUS_CHANGE = 111;
    private static final int EVENT_DISCONNECT = 100;
    private static final int EVENT_DISPLAY_INFO = 109;
    private static final int EVENT_ECM_TIMER_RESET = 115;
    private static final int EVENT_INCOMING_RING = 104;
    private static final int EVENT_IN_CALL_VOICE_PRIVACY_OFF = 107;
    private static final int EVENT_IN_CALL_VOICE_PRIVACY_ON = 106;
    private static final int EVENT_MMI_COMPLETE = 114;
    private static final int EVENT_MMI_INITIATE = 113;
    private static final int EVENT_NEW_RINGING_CONNECTION = 102;
    private static final int EVENT_POST_DIAL_CHARACTER = 119;
    private static final int EVENT_PRECISE_CALL_STATE_CHANGED = 101;
    private static final int EVENT_RESEND_INCALL_MUTE = 112;
    private static final int EVENT_RINGBACK_TONE = 105;
    private static final int EVENT_SERVICE_STATE_CHANGED = 118;
    private static final int EVENT_SIGNAL_INFO = 110;
    private static final int EVENT_SUBSCRIPTION_INFO_READY = 116;
    private static final int EVENT_SUPP_SERVICE_FAILED = 117;
    private static final int EVENT_UNKNOWN_CONNECTION = 103;
    private static final CallManager INSTANCE = new CallManager();
    private static final String LOG_TAG = "CallManager";
    private static final boolean VDBG;
    private final ArrayList<Connection> emptyConnections = new ArrayList();
    private final ArrayList<Call> mBackgroundCalls = new ArrayList();
    protected final RegistrantList mCallWaitingRegistrants = new RegistrantList();
    protected final RegistrantList mCdmaOtaStatusChangeRegistrants = new RegistrantList();
    private Phone mDefaultPhone = null;
    protected final RegistrantList mDisconnectRegistrants = new RegistrantList();
    protected final RegistrantList mDisplayInfoRegistrants = new RegistrantList();
    protected final RegistrantList mEcmTimerResetRegistrants = new RegistrantList();
    private final ArrayList<Call> mForegroundCalls = new ArrayList();
    private Handler mHandler = new Handler()
    {
        public void handleMessage(Message paramAnonymousMessage)
        {
            switch (paramAnonymousMessage.what)
            {
            default:
            case 100:
            case 101:
            case 102:
            case 103:
            case 104:
            case 105:
            case 106:
            case 107:
            case 108:
            case 109:
            case 110:
            case 111:
            case 112:
            case 113:
            case 114:
            case 115:
            case 116:
            case 117:
            case 118:
            case 119:
            }
            while (true)
            {
                return;
                CallManager.this.mDisconnectRegistrants.notifyRegistrants((AsyncResult)paramAnonymousMessage.obj);
                continue;
                CallManager.this.mPreciseCallStateRegistrants.notifyRegistrants((AsyncResult)paramAnonymousMessage.obj);
                continue;
                if ((CallManager.this.getActiveFgCallState().isDialing()) || (CallManager.this.hasMoreThanOneRingingCall()))
                {
                    Connection localConnection = (Connection)((AsyncResult)paramAnonymousMessage.obj).result;
                    try
                    {
                        Log.d("CallManager", "silently drop incoming call: " + localConnection.getCall());
                        localConnection.getCall().hangup();
                    }
                    catch (CallStateException localCallStateException)
                    {
                        Log.w("CallManager", "new ringing connection", localCallStateException);
                    }
                }
                else
                {
                    CallManager.this.mNewRingingConnectionRegistrants.notifyRegistrants((AsyncResult)paramAnonymousMessage.obj);
                    continue;
                    CallManager.this.mUnknownConnectionRegistrants.notifyRegistrants((AsyncResult)paramAnonymousMessage.obj);
                    continue;
                    if (!CallManager.this.hasActiveFgCall())
                    {
                        CallManager.this.mIncomingRingRegistrants.notifyRegistrants((AsyncResult)paramAnonymousMessage.obj);
                        continue;
                        CallManager.this.mRingbackToneRegistrants.notifyRegistrants((AsyncResult)paramAnonymousMessage.obj);
                        continue;
                        CallManager.this.mInCallVoicePrivacyOnRegistrants.notifyRegistrants((AsyncResult)paramAnonymousMessage.obj);
                        continue;
                        CallManager.this.mInCallVoicePrivacyOffRegistrants.notifyRegistrants((AsyncResult)paramAnonymousMessage.obj);
                        continue;
                        CallManager.this.mCallWaitingRegistrants.notifyRegistrants((AsyncResult)paramAnonymousMessage.obj);
                        continue;
                        CallManager.this.mDisplayInfoRegistrants.notifyRegistrants((AsyncResult)paramAnonymousMessage.obj);
                        continue;
                        CallManager.this.mSignalInfoRegistrants.notifyRegistrants((AsyncResult)paramAnonymousMessage.obj);
                        continue;
                        CallManager.this.mCdmaOtaStatusChangeRegistrants.notifyRegistrants((AsyncResult)paramAnonymousMessage.obj);
                        continue;
                        CallManager.this.mResendIncallMuteRegistrants.notifyRegistrants((AsyncResult)paramAnonymousMessage.obj);
                        continue;
                        CallManager.this.mMmiInitiateRegistrants.notifyRegistrants((AsyncResult)paramAnonymousMessage.obj);
                        continue;
                        CallManager.this.mMmiCompleteRegistrants.notifyRegistrants((AsyncResult)paramAnonymousMessage.obj);
                        continue;
                        CallManager.this.mEcmTimerResetRegistrants.notifyRegistrants((AsyncResult)paramAnonymousMessage.obj);
                        continue;
                        CallManager.this.mSubscriptionInfoReadyRegistrants.notifyRegistrants((AsyncResult)paramAnonymousMessage.obj);
                        continue;
                        CallManager.this.mSuppServiceFailedRegistrants.notifyRegistrants((AsyncResult)paramAnonymousMessage.obj);
                        continue;
                        CallManager.this.mServiceStateChangedRegistrants.notifyRegistrants((AsyncResult)paramAnonymousMessage.obj);
                        continue;
                        for (int i = 0; i < CallManager.this.mPostDialCharacterRegistrants.size(); i++)
                        {
                            Message localMessage = ((Registrant)CallManager.this.mPostDialCharacterRegistrants.get(i)).messageForRegistrant();
                            localMessage.obj = paramAnonymousMessage.obj;
                            localMessage.arg1 = paramAnonymousMessage.arg1;
                            localMessage.sendToTarget();
                        }
                    }
                }
            }
        }
    };
    protected final RegistrantList mInCallVoicePrivacyOffRegistrants = new RegistrantList();
    protected final RegistrantList mInCallVoicePrivacyOnRegistrants = new RegistrantList();
    protected final RegistrantList mIncomingRingRegistrants = new RegistrantList();
    protected final RegistrantList mMmiCompleteRegistrants = new RegistrantList();
    protected final RegistrantList mMmiInitiateRegistrants = new RegistrantList();
    protected final RegistrantList mMmiRegistrants = new RegistrantList();
    protected final RegistrantList mNewRingingConnectionRegistrants = new RegistrantList();
    private final ArrayList<Phone> mPhones = new ArrayList();
    protected final RegistrantList mPostDialCharacterRegistrants = new RegistrantList();
    protected final RegistrantList mPreciseCallStateRegistrants = new RegistrantList();
    protected final RegistrantList mResendIncallMuteRegistrants = new RegistrantList();
    protected final RegistrantList mRingbackToneRegistrants = new RegistrantList();
    private final ArrayList<Call> mRingingCalls = new ArrayList();
    protected final RegistrantList mServiceStateChangedRegistrants = new RegistrantList();
    protected final RegistrantList mSignalInfoRegistrants = new RegistrantList();
    protected final RegistrantList mSubscriptionInfoReadyRegistrants = new RegistrantList();
    protected final RegistrantList mSuppServiceFailedRegistrants = new RegistrantList();
    protected final RegistrantList mUnknownConnectionRegistrants = new RegistrantList();

    private boolean canDial(Phone paramPhone)
    {
        int i = paramPhone.getServiceState().getState();
        boolean bool1 = hasActiveRingingCall();
        boolean bool2 = hasActiveFgCall();
        boolean bool3 = hasActiveBgCall();
        boolean bool4;
        Call.State localState;
        if ((bool2) && (bool3))
        {
            bool4 = true;
            localState = getActiveFgCallState();
            if ((i == 3) || (bool1) || (bool4) || ((localState != Call.State.ACTIVE) && (localState != Call.State.IDLE) && (localState != Call.State.DISCONNECTED)))
                break label175;
        }
        label175: for (boolean bool5 = true; ; bool5 = false)
        {
            if (!bool5)
                Log.d("CallManager", "canDial serviceState=" + i + " hasRingingCall=" + bool1 + " hasActiveCall=" + bool2 + " hasHoldingCall=" + bool3 + " allLinesTaken=" + bool4 + " fgCallState=" + localState);
            return bool5;
            bool4 = false;
            break;
        }
    }

    private Context getContext()
    {
        Phone localPhone = getDefaultPhone();
        if (localPhone == null);
        for (Context localContext = null; ; localContext = localPhone.getContext())
            return localContext;
    }

    private Call getFirstActiveCall(ArrayList<Call> paramArrayList)
    {
        Iterator localIterator = paramArrayList.iterator();
        Call localCall;
        do
        {
            if (!localIterator.hasNext())
                break;
            localCall = (Call)localIterator.next();
        }
        while (localCall.isIdle());
        while (true)
        {
            return localCall;
            localCall = null;
        }
    }

    private Call getFirstCallOfState(ArrayList<Call> paramArrayList, Call.State paramState)
    {
        Iterator localIterator = paramArrayList.iterator();
        Call localCall;
        do
        {
            if (!localIterator.hasNext())
                break;
            localCall = (Call)localIterator.next();
        }
        while (localCall.getState() != paramState);
        while (true)
        {
            return localCall;
            localCall = null;
        }
    }

    private Call getFirstNonIdleCall(List<Call> paramList)
    {
        Object localObject1 = null;
        Iterator localIterator = paramList.iterator();
        Object localObject2;
        if (localIterator.hasNext())
        {
            localObject2 = (Call)localIterator.next();
            if (((Call)localObject2).isIdle());
        }
        while (true)
        {
            return localObject2;
            if ((((Call)localObject2).getState() == Call.State.IDLE) || (localObject1 != null))
                break;
            localObject1 = localObject2;
            break;
            localObject2 = localObject1;
        }
    }

    public static CallManager getInstance()
    {
        return INSTANCE;
    }

    private static Phone getPhoneBase(Phone paramPhone)
    {
        if ((paramPhone instanceof PhoneProxy))
            paramPhone = paramPhone.getForegroundCall().getPhone();
        return paramPhone;
    }

    private boolean hasMoreThanOneRingingCall()
    {
        int i = 1;
        int j = 0;
        Iterator localIterator = this.mRingingCalls.iterator();
        do
        {
            do
                if (!localIterator.hasNext())
                    break;
            while (!((Call)localIterator.next()).getState().isRinging());
            j += 1;
        }
        while (j <= i);
        while (true)
        {
            return i;
            i = 0;
        }
    }

    public static boolean isSamePhone(Phone paramPhone1, Phone paramPhone2)
    {
        if (getPhoneBase(paramPhone1) == getPhoneBase(paramPhone2));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private void registerForPhoneStates(Phone paramPhone)
    {
        paramPhone.registerForPreciseCallStateChanged(this.mHandler, 101, null);
        paramPhone.registerForDisconnect(this.mHandler, 100, null);
        paramPhone.registerForNewRingingConnection(this.mHandler, 102, null);
        paramPhone.registerForUnknownConnection(this.mHandler, 103, null);
        paramPhone.registerForIncomingRing(this.mHandler, 104, null);
        paramPhone.registerForRingbackTone(this.mHandler, 105, null);
        paramPhone.registerForInCallVoicePrivacyOn(this.mHandler, 106, null);
        paramPhone.registerForInCallVoicePrivacyOff(this.mHandler, 107, null);
        paramPhone.registerForDisplayInfo(this.mHandler, 109, null);
        paramPhone.registerForSignalInfo(this.mHandler, 110, null);
        paramPhone.registerForResendIncallMute(this.mHandler, 112, null);
        paramPhone.registerForMmiInitiate(this.mHandler, 113, null);
        paramPhone.registerForMmiComplete(this.mHandler, 114, null);
        paramPhone.registerForSuppServiceFailed(this.mHandler, 117, null);
        paramPhone.registerForServiceStateChanged(this.mHandler, 118, null);
        if ((paramPhone.getPhoneType() == 1) || (paramPhone.getPhoneType() == 2))
            paramPhone.setOnPostDialCharacter(this.mHandler, 119, null);
        if (paramPhone.getPhoneType() == 2)
        {
            paramPhone.registerForCdmaOtaStatusChange(this.mHandler, 111, null);
            paramPhone.registerForSubscriptionInfoReady(this.mHandler, 116, null);
            paramPhone.registerForCallWaiting(this.mHandler, 108, null);
            paramPhone.registerForEcmTimerReset(this.mHandler, 115, null);
        }
    }

    private void unregisterForPhoneStates(Phone paramPhone)
    {
        paramPhone.unregisterForPreciseCallStateChanged(this.mHandler);
        paramPhone.unregisterForDisconnect(this.mHandler);
        paramPhone.unregisterForNewRingingConnection(this.mHandler);
        paramPhone.unregisterForUnknownConnection(this.mHandler);
        paramPhone.unregisterForIncomingRing(this.mHandler);
        paramPhone.unregisterForRingbackTone(this.mHandler);
        paramPhone.unregisterForInCallVoicePrivacyOn(this.mHandler);
        paramPhone.unregisterForInCallVoicePrivacyOff(this.mHandler);
        paramPhone.unregisterForDisplayInfo(this.mHandler);
        paramPhone.unregisterForSignalInfo(this.mHandler);
        paramPhone.unregisterForResendIncallMute(this.mHandler);
        paramPhone.unregisterForMmiInitiate(this.mHandler);
        paramPhone.unregisterForMmiComplete(this.mHandler);
        paramPhone.unregisterForSuppServiceFailed(this.mHandler);
        paramPhone.unregisterForServiceStateChanged(this.mHandler);
        if ((paramPhone.getPhoneType() == 1) || (paramPhone.getPhoneType() == 2))
            paramPhone.setOnPostDialCharacter(null, 119, null);
        if (paramPhone.getPhoneType() == 2)
        {
            paramPhone.unregisterForCdmaOtaStatusChange(this.mHandler);
            paramPhone.unregisterForSubscriptionInfoReady(this.mHandler);
            paramPhone.unregisterForCallWaiting(this.mHandler);
            paramPhone.unregisterForEcmTimerReset(this.mHandler);
        }
    }

    public void acceptCall(Call paramCall)
        throws CallStateException
    {
        int i = 1;
        Phone localPhone1 = paramCall.getPhone();
        Phone localPhone2;
        int j;
        if (hasActiveFgCall())
        {
            localPhone2 = getActiveFgCall().getPhone();
            if (localPhone2.getBackgroundCall().isIdle())
                break label68;
            j = i;
            if (localPhone2 != localPhone1)
                break label74;
            label45: if ((i == 0) || (j == 0))
                break label79;
            getActiveFgCall().hangup();
        }
        while (true)
        {
            localPhone1.acceptCall();
            return;
            label68: j = 0;
            break;
            label74: i = 0;
            break label45;
            label79: if ((i == 0) && (j == 0))
                localPhone2.switchHoldingAndActive();
            else if ((i == 0) && (j != 0))
                getActiveFgCall().hangup();
        }
    }

    public boolean canConference(Call paramCall)
    {
        Phone localPhone1 = null;
        Phone localPhone2 = null;
        if (hasActiveFgCall())
            localPhone1 = getActiveFgCall().getPhone();
        if (paramCall != null)
            localPhone2 = paramCall.getPhone();
        return localPhone2.getClass().equals(localPhone1.getClass());
    }

    public boolean canTransfer(Call paramCall)
    {
        Phone localPhone1 = null;
        Phone localPhone2 = null;
        if (hasActiveFgCall())
            localPhone1 = getActiveFgCall().getPhone();
        if (paramCall != null)
            localPhone2 = paramCall.getPhone();
        if ((localPhone2 == localPhone1) && (localPhone1.canTransfer()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void clearDisconnected()
    {
        Iterator localIterator = this.mPhones.iterator();
        while (localIterator.hasNext())
            ((Phone)localIterator.next()).clearDisconnected();
    }

    public void conference(Call paramCall)
        throws CallStateException
    {
        Phone localPhone = getFgPhone();
        if ((localPhone instanceof SipPhone))
            ((SipPhone)localPhone).conference(paramCall);
        while (true)
        {
            return;
            if (!canConference(paramCall))
                break;
            localPhone.conference();
        }
        throw new CallStateException("Can't conference foreground and selected background call");
    }

    public Connection dial(Phone paramPhone, String paramString)
        throws CallStateException
    {
        boolean bool1 = true;
        Phone localPhone1 = getPhoneBase(paramPhone);
        if (!canDial(paramPhone))
            throw new CallStateException("cannot dial in current state");
        Phone localPhone2;
        boolean bool2;
        if (hasActiveFgCall())
        {
            localPhone2 = getActiveFgCall().getPhone();
            if (localPhone2.getBackgroundCall().isIdle())
                break label144;
            bool2 = bool1;
            StringBuilder localStringBuilder = new StringBuilder().append("hasBgCall: ").append(bool2).append(" sameChannel:");
            if (localPhone2 != localPhone1)
                break label150;
            label92: Log.d("CallManager", bool1);
            if (localPhone2 != localPhone1)
            {
                if (!bool2)
                    break label155;
                Log.d("CallManager", "Hangup");
                getActiveFgCall().hangup();
            }
        }
        while (true)
        {
            return localPhone1.dial(paramString);
            label144: bool2 = false;
            break;
            label150: bool1 = false;
            break label92;
            label155: Log.d("CallManager", "Switch");
            localPhone2.switchHoldingAndActive();
        }
    }

    public Connection dial(Phone paramPhone, String paramString, UUSInfo paramUUSInfo)
        throws CallStateException
    {
        return paramPhone.dial(paramString, paramUUSInfo);
    }

    public void explicitCallTransfer(Call paramCall)
        throws CallStateException
    {
        if (canTransfer(paramCall))
            paramCall.getPhone().explicitCallTransfer();
    }

    public Call getActiveFgCall()
    {
        Call localCall = getFirstNonIdleCall(this.mForegroundCalls);
        if (localCall == null)
            if (this.mDefaultPhone != null)
                break label24;
        label24: for (localCall = null; ; localCall = this.mDefaultPhone.getForegroundCall())
            return localCall;
    }

    public Call.State getActiveFgCallState()
    {
        Call localCall = getActiveFgCall();
        if (localCall != null);
        for (Call.State localState = localCall.getState(); ; localState = Call.State.IDLE)
            return localState;
    }

    public List<Phone> getAllPhones()
    {
        return Collections.unmodifiableList(this.mPhones);
    }

    public List<Call> getBackgroundCalls()
    {
        return Collections.unmodifiableList(this.mBackgroundCalls);
    }

    public List<Connection> getBgCallConnections()
    {
        Call localCall = getFirstActiveBgCall();
        if (localCall != null);
        for (Object localObject = localCall.getConnections(); ; localObject = this.emptyConnections)
            return localObject;
    }

    public Phone getBgPhone()
    {
        return getFirstActiveBgCall().getPhone();
    }

    public Phone getDefaultPhone()
    {
        return this.mDefaultPhone;
    }

    public List<Connection> getFgCallConnections()
    {
        Call localCall = getActiveFgCall();
        if (localCall != null);
        for (Object localObject = localCall.getConnections(); ; localObject = this.emptyConnections)
            return localObject;
    }

    public Connection getFgCallLatestConnection()
    {
        Call localCall = getActiveFgCall();
        if (localCall != null);
        for (Connection localConnection = localCall.getLatestConnection(); ; localConnection = null)
            return localConnection;
    }

    public Phone getFgPhone()
    {
        return getActiveFgCall().getPhone();
    }

    public Call getFirstActiveBgCall()
    {
        Call localCall = getFirstNonIdleCall(this.mBackgroundCalls);
        if (localCall == null)
            if (this.mDefaultPhone != null)
                break label24;
        label24: for (localCall = null; ; localCall = this.mDefaultPhone.getBackgroundCall())
            return localCall;
    }

    public Call getFirstActiveRingingCall()
    {
        Call localCall = getFirstNonIdleCall(this.mRingingCalls);
        if (localCall == null)
            if (this.mDefaultPhone != null)
                break label24;
        label24: for (localCall = null; ; localCall = this.mDefaultPhone.getRingingCall())
            return localCall;
    }

    public List<Call> getForegroundCalls()
    {
        return Collections.unmodifiableList(this.mForegroundCalls);
    }

    public boolean getMute()
    {
        boolean bool;
        if (hasActiveFgCall())
            bool = getActiveFgCall().getPhone().getMute();
        while (true)
        {
            return bool;
            if (hasActiveBgCall())
                bool = getFirstActiveBgCall().getPhone().getMute();
            else
                bool = false;
        }
    }

    public List<? extends MmiCode> getPendingMmiCodes(Phone paramPhone)
    {
        Log.e("CallManager", "getPendingMmiCodes not implemented");
        return null;
    }

    public List<Call> getRingingCalls()
    {
        return Collections.unmodifiableList(this.mRingingCalls);
    }

    public Phone getRingingPhone()
    {
        return getFirstActiveRingingCall().getPhone();
    }

    public int getServiceState()
    {
        int i = 1;
        Iterator localIterator = this.mPhones.iterator();
        while (true)
        {
            int j;
            if (localIterator.hasNext())
            {
                j = ((Phone)localIterator.next()).getServiceState().getState();
                if (j == 0)
                    i = j;
            }
            else
            {
                return i;
            }
            if (j == 1)
            {
                if ((i == 2) || (i == 3))
                    i = j;
            }
            else if ((j == 2) && (i == 3))
                i = j;
        }
    }

    public Phone.State getState()
    {
        Phone.State localState = Phone.State.IDLE;
        Iterator localIterator = this.mPhones.iterator();
        while (localIterator.hasNext())
        {
            Phone localPhone = (Phone)localIterator.next();
            if (localPhone.getState() == Phone.State.RINGING)
                localState = Phone.State.RINGING;
            else if ((localPhone.getState() == Phone.State.OFFHOOK) && (localState == Phone.State.IDLE))
                localState = Phone.State.OFFHOOK;
        }
        return localState;
    }

    public void hangupForegroundResumeBackground(Call paramCall)
        throws CallStateException
    {
        if (hasActiveFgCall())
        {
            Phone localPhone = getFgPhone();
            if (paramCall != null)
            {
                if (localPhone != paramCall.getPhone())
                    break label32;
                getActiveFgCall().hangup();
            }
        }
        while (true)
        {
            return;
            label32: getActiveFgCall().hangup();
            switchHoldingAndActive(paramCall);
        }
    }

    public boolean hasActiveBgCall()
    {
        if (getFirstActiveCall(this.mBackgroundCalls) != null);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean hasActiveFgCall()
    {
        if (getFirstActiveCall(this.mForegroundCalls) != null);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean hasActiveRingingCall()
    {
        if (getFirstActiveCall(this.mRingingCalls) != null);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean hasDisconnectedBgCall()
    {
        if (getFirstCallOfState(this.mBackgroundCalls, Call.State.DISCONNECTED) != null);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean hasDisconnectedFgCall()
    {
        if (getFirstCallOfState(this.mForegroundCalls, Call.State.DISCONNECTED) != null);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void registerForCallWaiting(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mCallWaitingRegistrants.addUnique(paramHandler, paramInt, paramObject);
    }

    public void registerForCdmaOtaStatusChange(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mCdmaOtaStatusChangeRegistrants.addUnique(paramHandler, paramInt, paramObject);
    }

    public void registerForDisconnect(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mDisconnectRegistrants.addUnique(paramHandler, paramInt, paramObject);
    }

    public void registerForDisplayInfo(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mDisplayInfoRegistrants.addUnique(paramHandler, paramInt, paramObject);
    }

    public void registerForEcmTimerReset(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mEcmTimerResetRegistrants.addUnique(paramHandler, paramInt, paramObject);
    }

    public void registerForInCallVoicePrivacyOff(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mInCallVoicePrivacyOffRegistrants.addUnique(paramHandler, paramInt, paramObject);
    }

    public void registerForInCallVoicePrivacyOn(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mInCallVoicePrivacyOnRegistrants.addUnique(paramHandler, paramInt, paramObject);
    }

    public void registerForIncomingRing(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mIncomingRingRegistrants.addUnique(paramHandler, paramInt, paramObject);
    }

    public void registerForMmiComplete(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mMmiCompleteRegistrants.addUnique(paramHandler, paramInt, paramObject);
    }

    public void registerForMmiInitiate(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mMmiInitiateRegistrants.addUnique(paramHandler, paramInt, paramObject);
    }

    public void registerForNewRingingConnection(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mNewRingingConnectionRegistrants.addUnique(paramHandler, paramInt, paramObject);
    }

    public void registerForPostDialCharacter(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mPostDialCharacterRegistrants.addUnique(paramHandler, paramInt, paramObject);
    }

    public void registerForPreciseCallStateChanged(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mPreciseCallStateRegistrants.addUnique(paramHandler, paramInt, paramObject);
    }

    public void registerForResendIncallMute(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mResendIncallMuteRegistrants.addUnique(paramHandler, paramInt, paramObject);
    }

    public void registerForRingbackTone(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mRingbackToneRegistrants.addUnique(paramHandler, paramInt, paramObject);
    }

    public void registerForServiceStateChanged(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mServiceStateChangedRegistrants.addUnique(paramHandler, paramInt, paramObject);
    }

    public void registerForSignalInfo(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mSignalInfoRegistrants.addUnique(paramHandler, paramInt, paramObject);
    }

    public void registerForSubscriptionInfoReady(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mSubscriptionInfoReadyRegistrants.addUnique(paramHandler, paramInt, paramObject);
    }

    public void registerForSuppServiceFailed(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mSuppServiceFailedRegistrants.addUnique(paramHandler, paramInt, paramObject);
    }

    public void registerForUnknownConnection(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mUnknownConnectionRegistrants.addUnique(paramHandler, paramInt, paramObject);
    }

    public boolean registerPhone(Phone paramPhone)
    {
        Phone localPhone = getPhoneBase(paramPhone);
        if ((localPhone != null) && (!this.mPhones.contains(localPhone)))
        {
            Log.d("CallManager", "registerPhone(" + paramPhone.getPhoneName() + " " + paramPhone + ")");
            if (this.mPhones.isEmpty())
                this.mDefaultPhone = localPhone;
            this.mPhones.add(localPhone);
            this.mRingingCalls.add(localPhone.getRingingCall());
            this.mBackgroundCalls.add(localPhone.getBackgroundCall());
            this.mForegroundCalls.add(localPhone.getForegroundCall());
            registerForPhoneStates(localPhone);
        }
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void rejectCall(Call paramCall)
        throws CallStateException
    {
        paramCall.getPhone().rejectCall();
    }

    public boolean sendBurstDtmf(String paramString, int paramInt1, int paramInt2, Message paramMessage)
    {
        if (hasActiveFgCall())
            getActiveFgCall().getPhone().sendBurstDtmf(paramString, paramInt1, paramInt2, paramMessage);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean sendDtmf(char paramChar)
    {
        boolean bool = false;
        if (hasActiveFgCall())
        {
            getActiveFgCall().getPhone().sendDtmf(paramChar);
            bool = true;
        }
        return bool;
    }

    public boolean sendUssdResponse(Phone paramPhone, String paramString)
    {
        Log.e("CallManager", "sendUssdResponse not implemented");
        return false;
    }

    public void setAudioMode()
    {
        Context localContext = getContext();
        if (localContext == null);
        while (true)
        {
            return;
            AudioManager localAudioManager = (AudioManager)localContext.getSystemService("audio");
            switch (2.$SwitchMap$com$android$internal$telephony$Phone$State[getState().ordinal()])
            {
            default:
                break;
            case 1:
                if (localAudioManager.getMode() != 1)
                {
                    if (localAudioManager.getStreamVolume(2) > 0)
                        localAudioManager.requestAudioFocusForCall(2, 2);
                    localAudioManager.setMode(1);
                }
                break;
            case 2:
                Phone localPhone = getFgPhone();
                if (getActiveFgCallState() == Call.State.IDLE)
                    localPhone = getBgPhone();
                int i = 2;
                if ((localPhone instanceof SipPhone))
                    i = 3;
                if (localAudioManager.getMode() != i)
                {
                    localAudioManager.requestAudioFocusForCall(0, 2);
                    localAudioManager.setMode(i);
                }
                break;
            case 3:
                if (localAudioManager.getMode() != 0)
                {
                    localAudioManager.setMode(0);
                    localAudioManager.abandonAudioFocusForCall();
                }
                break;
            }
        }
    }

    public void setEchoSuppressionEnabled(boolean paramBoolean)
    {
        if (hasActiveFgCall())
            getActiveFgCall().getPhone().setEchoSuppressionEnabled(paramBoolean);
    }

    public void setMute(boolean paramBoolean)
    {
        if (hasActiveFgCall())
            getActiveFgCall().getPhone().setMute(paramBoolean);
    }

    public boolean startDtmf(char paramChar)
    {
        boolean bool = false;
        if (hasActiveFgCall())
        {
            getActiveFgCall().getPhone().startDtmf(paramChar);
            bool = true;
        }
        return bool;
    }

    public void stopDtmf()
    {
        if (hasActiveFgCall())
            getFgPhone().stopDtmf();
    }

    public void switchHoldingAndActive(Call paramCall)
        throws CallStateException
    {
        Phone localPhone1 = null;
        Phone localPhone2 = null;
        if (hasActiveFgCall())
            localPhone1 = getActiveFgCall().getPhone();
        if (paramCall != null)
            localPhone2 = paramCall.getPhone();
        if (localPhone1 != null)
            localPhone1.switchHoldingAndActive();
        if ((localPhone2 != null) && (localPhone2 != localPhone1))
            localPhone2.switchHoldingAndActive();
    }

    public String toString()
    {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("CallManager {");
        localStringBuilder.append("\nstate = " + getState());
        Call localCall1 = getActiveFgCall();
        localStringBuilder.append("\n- Foreground: " + getActiveFgCallState());
        localStringBuilder.append(" from " + localCall1.getPhone());
        localStringBuilder.append("\n    Conn: ").append(getFgCallConnections());
        Call localCall2 = getFirstActiveBgCall();
        localStringBuilder.append("\n- Background: " + localCall2.getState());
        localStringBuilder.append(" from " + localCall2.getPhone());
        localStringBuilder.append("\n    Conn: ").append(getBgCallConnections());
        Call localCall3 = getFirstActiveRingingCall();
        localStringBuilder.append("\n- Ringing: " + localCall3.getState());
        localStringBuilder.append(" from " + localCall3.getPhone());
        Iterator localIterator = getAllPhones().iterator();
        while (localIterator.hasNext())
        {
            Phone localPhone = (Phone)localIterator.next();
            if (localPhone != null)
            {
                localStringBuilder.append("\nPhone: " + localPhone + ", name = " + localPhone.getPhoneName() + ", state = " + localPhone.getState());
                Call localCall4 = localPhone.getForegroundCall();
                localStringBuilder.append("\n- Foreground: ").append(localCall4);
                Call localCall5 = localPhone.getBackgroundCall();
                localStringBuilder.append(" Background: ").append(localCall5);
                Call localCall6 = localPhone.getRingingCall();
                localStringBuilder.append(" Ringing: ").append(localCall6);
            }
        }
        localStringBuilder.append("\n}");
        return localStringBuilder.toString();
    }

    public void unregisterForCallWaiting(Handler paramHandler)
    {
        this.mCallWaitingRegistrants.remove(paramHandler);
    }

    public void unregisterForCdmaOtaStatusChange(Handler paramHandler)
    {
        this.mCdmaOtaStatusChangeRegistrants.remove(paramHandler);
    }

    public void unregisterForDisconnect(Handler paramHandler)
    {
        this.mDisconnectRegistrants.remove(paramHandler);
    }

    public void unregisterForDisplayInfo(Handler paramHandler)
    {
        this.mDisplayInfoRegistrants.remove(paramHandler);
    }

    public void unregisterForEcmTimerReset(Handler paramHandler)
    {
        this.mEcmTimerResetRegistrants.remove(paramHandler);
    }

    public void unregisterForInCallVoicePrivacyOff(Handler paramHandler)
    {
        this.mInCallVoicePrivacyOffRegistrants.remove(paramHandler);
    }

    public void unregisterForInCallVoicePrivacyOn(Handler paramHandler)
    {
        this.mInCallVoicePrivacyOnRegistrants.remove(paramHandler);
    }

    public void unregisterForIncomingRing(Handler paramHandler)
    {
        this.mIncomingRingRegistrants.remove(paramHandler);
    }

    public void unregisterForMmiComplete(Handler paramHandler)
    {
        this.mMmiCompleteRegistrants.remove(paramHandler);
    }

    public void unregisterForMmiInitiate(Handler paramHandler)
    {
        this.mMmiInitiateRegistrants.remove(paramHandler);
    }

    public void unregisterForNewRingingConnection(Handler paramHandler)
    {
        this.mNewRingingConnectionRegistrants.remove(paramHandler);
    }

    public void unregisterForPostDialCharacter(Handler paramHandler)
    {
        this.mPostDialCharacterRegistrants.remove(paramHandler);
    }

    public void unregisterForPreciseCallStateChanged(Handler paramHandler)
    {
        this.mPreciseCallStateRegistrants.remove(paramHandler);
    }

    public void unregisterForResendIncallMute(Handler paramHandler)
    {
        this.mResendIncallMuteRegistrants.remove(paramHandler);
    }

    public void unregisterForRingbackTone(Handler paramHandler)
    {
        this.mRingbackToneRegistrants.remove(paramHandler);
    }

    public void unregisterForServiceStateChanged(Handler paramHandler)
    {
        this.mServiceStateChangedRegistrants.remove(paramHandler);
    }

    public void unregisterForSignalInfo(Handler paramHandler)
    {
        this.mSignalInfoRegistrants.remove(paramHandler);
    }

    public void unregisterForSubscriptionInfoReady(Handler paramHandler)
    {
        this.mSubscriptionInfoReadyRegistrants.remove(paramHandler);
    }

    public void unregisterForSuppServiceFailed(Handler paramHandler)
    {
        this.mSuppServiceFailedRegistrants.remove(paramHandler);
    }

    public void unregisterForUnknownConnection(Handler paramHandler)
    {
        this.mUnknownConnectionRegistrants.remove(paramHandler);
    }

    public void unregisterPhone(Phone paramPhone)
    {
        Phone localPhone = getPhoneBase(paramPhone);
        if ((localPhone != null) && (this.mPhones.contains(localPhone)))
        {
            Log.d("CallManager", "unregisterPhone(" + paramPhone.getPhoneName() + " " + paramPhone + ")");
            this.mPhones.remove(localPhone);
            this.mRingingCalls.remove(localPhone.getRingingCall());
            this.mBackgroundCalls.remove(localPhone.getBackgroundCall());
            this.mForegroundCalls.remove(localPhone.getForegroundCall());
            unregisterForPhoneStates(localPhone);
            if (localPhone == this.mDefaultPhone)
                if (!this.mPhones.isEmpty())
                    break label147;
        }
        label147: for (this.mDefaultPhone = null; ; this.mDefaultPhone = ((Phone)this.mPhones.get(0)))
            return;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.CallManager
 * JD-Core Version:        0.6.2
 */