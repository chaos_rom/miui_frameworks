package com.android.internal.telephony;

import android.util.Log;

public class CommandException extends RuntimeException
{
    private Error e;

    public CommandException(Error paramError)
    {
        super(paramError.toString());
        this.e = paramError;
    }

    public static CommandException fromRilErrno(int paramInt)
    {
        CommandException localCommandException;
        switch (paramInt)
        {
        case 7:
        default:
            Log.e("GSM", "Unrecognized RIL errno " + paramInt);
            localCommandException = new CommandException(Error.INVALID_RESPONSE);
        case 0:
        case -1:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 8:
        case 9:
        case 10:
        case 11:
        case 12:
        case 13:
        case 14:
        case 15:
        }
        while (true)
        {
            return localCommandException;
            localCommandException = null;
            continue;
            localCommandException = new CommandException(Error.INVALID_RESPONSE);
            continue;
            localCommandException = new CommandException(Error.RADIO_NOT_AVAILABLE);
            continue;
            localCommandException = new CommandException(Error.GENERIC_FAILURE);
            continue;
            localCommandException = new CommandException(Error.PASSWORD_INCORRECT);
            continue;
            localCommandException = new CommandException(Error.SIM_PIN2);
            continue;
            localCommandException = new CommandException(Error.SIM_PUK2);
            continue;
            localCommandException = new CommandException(Error.REQUEST_NOT_SUPPORTED);
            continue;
            localCommandException = new CommandException(Error.OP_NOT_ALLOWED_DURING_VOICE_CALL);
            continue;
            localCommandException = new CommandException(Error.OP_NOT_ALLOWED_BEFORE_REG_NW);
            continue;
            localCommandException = new CommandException(Error.SMS_FAIL_RETRY);
            continue;
            localCommandException = new CommandException(Error.SIM_ABSENT);
            continue;
            localCommandException = new CommandException(Error.SUBSCRIPTION_NOT_AVAILABLE);
            continue;
            localCommandException = new CommandException(Error.MODE_NOT_SUPPORTED);
            continue;
            localCommandException = new CommandException(Error.FDN_CHECK_FAILURE);
            continue;
            localCommandException = new CommandException(Error.ILLEGAL_SIM_OR_ME);
        }
    }

    public Error getCommandError()
    {
        return this.e;
    }

    public static enum Error
    {
        static
        {
            GENERIC_FAILURE = new Error("GENERIC_FAILURE", 2);
            PASSWORD_INCORRECT = new Error("PASSWORD_INCORRECT", 3);
            SIM_PIN2 = new Error("SIM_PIN2", 4);
            SIM_PUK2 = new Error("SIM_PUK2", 5);
            REQUEST_NOT_SUPPORTED = new Error("REQUEST_NOT_SUPPORTED", 6);
            OP_NOT_ALLOWED_DURING_VOICE_CALL = new Error("OP_NOT_ALLOWED_DURING_VOICE_CALL", 7);
            OP_NOT_ALLOWED_BEFORE_REG_NW = new Error("OP_NOT_ALLOWED_BEFORE_REG_NW", 8);
            SMS_FAIL_RETRY = new Error("SMS_FAIL_RETRY", 9);
            SIM_ABSENT = new Error("SIM_ABSENT", 10);
            SUBSCRIPTION_NOT_AVAILABLE = new Error("SUBSCRIPTION_NOT_AVAILABLE", 11);
            MODE_NOT_SUPPORTED = new Error("MODE_NOT_SUPPORTED", 12);
            FDN_CHECK_FAILURE = new Error("FDN_CHECK_FAILURE", 13);
            ILLEGAL_SIM_OR_ME = new Error("ILLEGAL_SIM_OR_ME", 14);
            Error[] arrayOfError = new Error[15];
            arrayOfError[0] = INVALID_RESPONSE;
            arrayOfError[1] = RADIO_NOT_AVAILABLE;
            arrayOfError[2] = GENERIC_FAILURE;
            arrayOfError[3] = PASSWORD_INCORRECT;
            arrayOfError[4] = SIM_PIN2;
            arrayOfError[5] = SIM_PUK2;
            arrayOfError[6] = REQUEST_NOT_SUPPORTED;
            arrayOfError[7] = OP_NOT_ALLOWED_DURING_VOICE_CALL;
            arrayOfError[8] = OP_NOT_ALLOWED_BEFORE_REG_NW;
            arrayOfError[9] = SMS_FAIL_RETRY;
            arrayOfError[10] = SIM_ABSENT;
            arrayOfError[11] = SUBSCRIPTION_NOT_AVAILABLE;
            arrayOfError[12] = MODE_NOT_SUPPORTED;
            arrayOfError[13] = FDN_CHECK_FAILURE;
            arrayOfError[14] = ILLEGAL_SIM_OR_ME;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.CommandException
 * JD-Core Version:        0.6.2
 */