package com.android.internal.telephony;

public class DataCallState
{
    private final boolean DBG = true;
    private final String LOG_TAG = "GSM";
    public int active = 0;
    public String[] addresses = new String[0];
    public int cid = 0;
    public String[] dnses = new String[0];
    public String[] gateways = new String[0];
    public String ifname = "";
    public int status = 0;
    public int suggestedRetryTime = -1;
    public String type = "";
    public int version = 0;

    // ERROR //
    public SetupResult setLinkProperties(android.net.LinkProperties paramLinkProperties, boolean paramBoolean)
    {
        // Byte code:
        //     0: aload_1
        //     1: ifnonnull +129 -> 130
        //     4: new 65	android/net/LinkProperties
        //     7: dup
        //     8: invokespecial 66	android/net/LinkProperties:<init>	()V
        //     11: astore_1
        //     12: aload_0
        //     13: getfield 37	com/android/internal/telephony/DataCallState:status	I
        //     16: getstatic 72	com/android/internal/telephony/DataConnection$FailCause:NONE	Lcom/android/internal/telephony/DataConnection$FailCause;
        //     19: invokevirtual 76	com/android/internal/telephony/DataConnection$FailCause:getErrorCode	()I
        //     22: if_icmpne +863 -> 885
        //     25: new 78	java/lang/StringBuilder
        //     28: dup
        //     29: invokespecial 79	java/lang/StringBuilder:<init>	()V
        //     32: ldc 81
        //     34: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     37: aload_0
        //     38: getfield 47	com/android/internal/telephony/DataCallState:ifname	Ljava/lang/String;
        //     41: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     44: ldc 87
        //     46: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     49: invokevirtual 91	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     52: astore 5
        //     54: aload_0
        //     55: getfield 47	com/android/internal/telephony/DataCallState:ifname	Ljava/lang/String;
        //     58: astore 8
        //     60: aload_1
        //     61: aload 8
        //     63: invokevirtual 95	android/net/LinkProperties:setInterfaceName	(Ljava/lang/String;)V
        //     66: aload_0
        //     67: getfield 51	com/android/internal/telephony/DataCallState:addresses	[Ljava/lang/String;
        //     70: ifnull +315 -> 385
        //     73: aload_0
        //     74: getfield 51	com/android/internal/telephony/DataCallState:addresses	[Ljava/lang/String;
        //     77: arraylength
        //     78: ifle +307 -> 385
        //     81: aload_0
        //     82: getfield 51	com/android/internal/telephony/DataCallState:addresses	[Ljava/lang/String;
        //     85: astore 9
        //     87: aload 9
        //     89: arraylength
        //     90: istore 10
        //     92: iconst_0
        //     93: istore 11
        //     95: iload 11
        //     97: iload 10
        //     99: if_icmpge +316 -> 415
        //     102: aload 9
        //     104: iload 11
        //     106: aaload
        //     107: invokevirtual 98	java/lang/String:trim	()Ljava/lang/String;
        //     110: astore 34
        //     112: aload 34
        //     114: invokevirtual 102	java/lang/String:isEmpty	()Z
        //     117: istore 35
        //     119: iload 35
        //     121: ifeq +16 -> 137
        //     124: iinc 11 1
        //     127: goto -32 -> 95
        //     130: aload_1
        //     131: invokevirtual 105	android/net/LinkProperties:clear	()V
        //     134: goto -122 -> 12
        //     137: aload 34
        //     139: ldc 107
        //     141: invokevirtual 111	java/lang/String:split	(Ljava/lang/String;)[Ljava/lang/String;
        //     144: astore 36
        //     146: aload 36
        //     148: arraylength
        //     149: iconst_2
        //     150: if_icmpne +199 -> 349
        //     153: aload 36
        //     155: iconst_0
        //     156: aaload
        //     157: astore 34
        //     159: aload 36
        //     161: iconst_1
        //     162: aaload
        //     163: invokestatic 117	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     166: istore 42
        //     168: iload 42
        //     170: istore 37
        //     172: aload 34
        //     174: invokestatic 123	android/net/NetworkUtils:numericToInetAddress	(Ljava/lang/String;)Ljava/net/InetAddress;
        //     177: astore 39
        //     179: aload 39
        //     181: invokevirtual 128	java/net/InetAddress:isAnyLocalAddress	()Z
        //     184: ifne -60 -> 124
        //     187: iload 37
        //     189: ifne +15 -> 204
        //     192: aload 39
        //     194: instanceof 130
        //     197: ifeq +710 -> 907
        //     200: bipush 32
        //     202: istore 37
        //     204: ldc 31
        //     206: new 78	java/lang/StringBuilder
        //     209: dup
        //     210: invokespecial 79	java/lang/StringBuilder:<init>	()V
        //     213: ldc 132
        //     215: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     218: aload 34
        //     220: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     223: ldc 107
        //     225: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     228: iload 37
        //     230: invokevirtual 135	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     233: invokevirtual 91	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     236: invokestatic 141	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
        //     239: pop
        //     240: new 143	android/net/LinkAddress
        //     243: dup
        //     244: aload 39
        //     246: iload 37
        //     248: invokespecial 146	android/net/LinkAddress:<init>	(Ljava/net/InetAddress;I)V
        //     251: astore 41
        //     253: aload_1
        //     254: aload 41
        //     256: invokevirtual 150	android/net/LinkProperties:addLinkAddress	(Landroid/net/LinkAddress;)V
        //     259: goto -135 -> 124
        //     262: astore 6
        //     264: ldc 31
        //     266: new 78	java/lang/StringBuilder
        //     269: dup
        //     270: invokespecial 79	java/lang/StringBuilder:<init>	()V
        //     273: ldc 152
        //     275: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     278: aload 6
        //     280: invokevirtual 155	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     283: invokevirtual 91	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     286: invokestatic 141	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
        //     289: pop
        //     290: aload 6
        //     292: invokevirtual 158	java/net/UnknownHostException:printStackTrace	()V
        //     295: getstatic 162	com/android/internal/telephony/DataCallState$SetupResult:ERR_UnacceptableParameter	Lcom/android/internal/telephony/DataCallState$SetupResult;
        //     298: astore_3
        //     299: aload_3
        //     300: getstatic 165	com/android/internal/telephony/DataCallState$SetupResult:SUCCESS	Lcom/android/internal/telephony/DataCallState$SetupResult;
        //     303: if_acmpeq +44 -> 347
        //     306: ldc 31
        //     308: new 78	java/lang/StringBuilder
        //     311: dup
        //     312: invokespecial 79	java/lang/StringBuilder:<init>	()V
        //     315: ldc 167
        //     317: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     320: aload_0
        //     321: getfield 37	com/android/internal/telephony/DataCallState:status	I
        //     324: invokevirtual 135	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     327: ldc 169
        //     329: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     332: aload_3
        //     333: invokevirtual 155	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     336: invokevirtual 91	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     339: invokestatic 141	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
        //     342: pop
        //     343: aload_1
        //     344: invokevirtual 105	android/net/LinkProperties:clear	()V
        //     347: aload_3
        //     348: areturn
        //     349: iconst_0
        //     350: istore 37
        //     352: goto -180 -> 172
        //     355: astore 38
        //     357: new 61	java/net/UnknownHostException
        //     360: dup
        //     361: new 78	java/lang/StringBuilder
        //     364: dup
        //     365: invokespecial 79	java/lang/StringBuilder:<init>	()V
        //     368: ldc 171
        //     370: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     373: aload 34
        //     375: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     378: invokevirtual 91	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     381: invokespecial 173	java/net/UnknownHostException:<init>	(Ljava/lang/String;)V
        //     384: athrow
        //     385: new 61	java/net/UnknownHostException
        //     388: dup
        //     389: new 78	java/lang/StringBuilder
        //     392: dup
        //     393: invokespecial 79	java/lang/StringBuilder:<init>	()V
        //     396: ldc 175
        //     398: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     401: aload_0
        //     402: getfield 47	com/android/internal/telephony/DataCallState:ifname	Ljava/lang/String;
        //     405: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     408: invokevirtual 91	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     411: invokespecial 173	java/net/UnknownHostException:<init>	(Ljava/lang/String;)V
        //     414: athrow
        //     415: aload_0
        //     416: getfield 53	com/android/internal/telephony/DataCallState:dnses	[Ljava/lang/String;
        //     419: ifnull +114 -> 533
        //     422: aload_0
        //     423: getfield 53	com/android/internal/telephony/DataCallState:dnses	[Ljava/lang/String;
        //     426: arraylength
        //     427: ifle +106 -> 533
        //     430: aload_0
        //     431: getfield 53	com/android/internal/telephony/DataCallState:dnses	[Ljava/lang/String;
        //     434: astore 27
        //     436: aload 27
        //     438: arraylength
        //     439: istore 28
        //     441: iconst_0
        //     442: istore 29
        //     444: iload 29
        //     446: iload 28
        //     448: if_icmpge +256 -> 704
        //     451: aload 27
        //     453: iload 29
        //     455: aaload
        //     456: invokevirtual 98	java/lang/String:trim	()Ljava/lang/String;
        //     459: astore 30
        //     461: aload 30
        //     463: invokevirtual 102	java/lang/String:isEmpty	()Z
        //     466: istore 31
        //     468: iload 31
        //     470: ifeq +9 -> 479
        //     473: iinc 29 1
        //     476: goto -32 -> 444
        //     479: aload 30
        //     481: invokestatic 123	android/net/NetworkUtils:numericToInetAddress	(Ljava/lang/String;)Ljava/net/InetAddress;
        //     484: astore 33
        //     486: aload 33
        //     488: invokevirtual 128	java/net/InetAddress:isAnyLocalAddress	()Z
        //     491: ifne -18 -> 473
        //     494: aload_1
        //     495: aload 33
        //     497: invokevirtual 179	android/net/LinkProperties:addDns	(Ljava/net/InetAddress;)V
        //     500: goto -27 -> 473
        //     503: astore 32
        //     505: new 61	java/net/UnknownHostException
        //     508: dup
        //     509: new 78	java/lang/StringBuilder
        //     512: dup
        //     513: invokespecial 79	java/lang/StringBuilder:<init>	()V
        //     516: ldc 181
        //     518: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     521: aload 30
        //     523: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     526: invokevirtual 91	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     529: invokespecial 173	java/net/UnknownHostException:<init>	(Ljava/lang/String;)V
        //     532: athrow
        //     533: iload_2
        //     534: ifeq +160 -> 694
        //     537: iconst_2
        //     538: anewarray 49	java/lang/String
        //     541: astore 12
        //     543: aload 12
        //     545: iconst_0
        //     546: new 78	java/lang/StringBuilder
        //     549: dup
        //     550: invokespecial 79	java/lang/StringBuilder:<init>	()V
        //     553: aload 5
        //     555: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     558: ldc 183
        //     560: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     563: invokevirtual 91	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     566: invokestatic 189	android/os/SystemProperties:get	(Ljava/lang/String;)Ljava/lang/String;
        //     569: aastore
        //     570: aload 12
        //     572: iconst_1
        //     573: new 78	java/lang/StringBuilder
        //     576: dup
        //     577: invokespecial 79	java/lang/StringBuilder:<init>	()V
        //     580: aload 5
        //     582: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     585: ldc 191
        //     587: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     590: invokevirtual 91	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     593: invokestatic 189	android/os/SystemProperties:get	(Ljava/lang/String;)Ljava/lang/String;
        //     596: aastore
        //     597: aload 12
        //     599: arraylength
        //     600: istore 13
        //     602: iconst_0
        //     603: istore 14
        //     605: iload 14
        //     607: iload 13
        //     609: if_icmpge +95 -> 704
        //     612: aload 12
        //     614: iload 14
        //     616: aaload
        //     617: invokevirtual 98	java/lang/String:trim	()Ljava/lang/String;
        //     620: astore 23
        //     622: aload 23
        //     624: invokevirtual 102	java/lang/String:isEmpty	()Z
        //     627: istore 24
        //     629: iload 24
        //     631: ifeq +9 -> 640
        //     634: iinc 14 1
        //     637: goto -32 -> 605
        //     640: aload 23
        //     642: invokestatic 123	android/net/NetworkUtils:numericToInetAddress	(Ljava/lang/String;)Ljava/net/InetAddress;
        //     645: astore 26
        //     647: aload 26
        //     649: invokevirtual 128	java/net/InetAddress:isAnyLocalAddress	()Z
        //     652: ifne -18 -> 634
        //     655: aload_1
        //     656: aload 26
        //     658: invokevirtual 179	android/net/LinkProperties:addDns	(Ljava/net/InetAddress;)V
        //     661: goto -27 -> 634
        //     664: astore 25
        //     666: new 61	java/net/UnknownHostException
        //     669: dup
        //     670: new 78	java/lang/StringBuilder
        //     673: dup
        //     674: invokespecial 79	java/lang/StringBuilder:<init>	()V
        //     677: ldc 181
        //     679: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     682: aload 23
        //     684: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     687: invokevirtual 91	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     690: invokespecial 173	java/net/UnknownHostException:<init>	(Ljava/lang/String;)V
        //     693: athrow
        //     694: new 61	java/net/UnknownHostException
        //     697: dup
        //     698: ldc 193
        //     700: invokespecial 173	java/net/UnknownHostException:<init>	(Ljava/lang/String;)V
        //     703: athrow
        //     704: aload_0
        //     705: getfield 55	com/android/internal/telephony/DataCallState:gateways	[Ljava/lang/String;
        //     708: ifnull +11 -> 719
        //     711: aload_0
        //     712: getfield 55	com/android/internal/telephony/DataCallState:gateways	[Ljava/lang/String;
        //     715: arraylength
        //     716: ifne +44 -> 760
        //     719: new 78	java/lang/StringBuilder
        //     722: dup
        //     723: invokespecial 79	java/lang/StringBuilder:<init>	()V
        //     726: aload 5
        //     728: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     731: ldc 195
        //     733: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     736: invokevirtual 91	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     739: invokestatic 189	android/os/SystemProperties:get	(Ljava/lang/String;)Ljava/lang/String;
        //     742: astore 15
        //     744: aload 15
        //     746: ifnull +56 -> 802
        //     749: aload_0
        //     750: aload 15
        //     752: ldc 197
        //     754: invokevirtual 111	java/lang/String:split	(Ljava/lang/String;)[Ljava/lang/String;
        //     757: putfield 55	com/android/internal/telephony/DataCallState:gateways	[Ljava/lang/String;
        //     760: aload_0
        //     761: getfield 55	com/android/internal/telephony/DataCallState:gateways	[Ljava/lang/String;
        //     764: astore 16
        //     766: aload 16
        //     768: arraylength
        //     769: istore 17
        //     771: iconst_0
        //     772: istore 18
        //     774: iload 18
        //     776: iload 17
        //     778: if_icmpge +100 -> 878
        //     781: aload 16
        //     783: iload 18
        //     785: aaload
        //     786: invokevirtual 98	java/lang/String:trim	()Ljava/lang/String;
        //     789: astore 19
        //     791: aload 19
        //     793: invokevirtual 102	java/lang/String:isEmpty	()Z
        //     796: ifeq +17 -> 813
        //     799: goto +116 -> 915
        //     802: aload_0
        //     803: iconst_0
        //     804: anewarray 49	java/lang/String
        //     807: putfield 55	com/android/internal/telephony/DataCallState:gateways	[Ljava/lang/String;
        //     810: goto -50 -> 760
        //     813: aload 19
        //     815: invokestatic 123	android/net/NetworkUtils:numericToInetAddress	(Ljava/lang/String;)Ljava/net/InetAddress;
        //     818: astore 21
        //     820: aload 21
        //     822: invokevirtual 128	java/net/InetAddress:isAnyLocalAddress	()Z
        //     825: ifne +90 -> 915
        //     828: new 199	android/net/RouteInfo
        //     831: dup
        //     832: aload 21
        //     834: invokespecial 201	android/net/RouteInfo:<init>	(Ljava/net/InetAddress;)V
        //     837: astore 22
        //     839: aload_1
        //     840: aload 22
        //     842: invokevirtual 205	android/net/LinkProperties:addRoute	(Landroid/net/RouteInfo;)V
        //     845: goto +70 -> 915
        //     848: astore 20
        //     850: new 61	java/net/UnknownHostException
        //     853: dup
        //     854: new 78	java/lang/StringBuilder
        //     857: dup
        //     858: invokespecial 79	java/lang/StringBuilder:<init>	()V
        //     861: ldc 207
        //     863: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     866: aload 19
        //     868: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     871: invokevirtual 91	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     874: invokespecial 173	java/net/UnknownHostException:<init>	(Ljava/lang/String;)V
        //     877: athrow
        //     878: getstatic 165	com/android/internal/telephony/DataCallState$SetupResult:SUCCESS	Lcom/android/internal/telephony/DataCallState$SetupResult;
        //     881: astore_3
        //     882: goto -583 -> 299
        //     885: aload_0
        //     886: getfield 35	com/android/internal/telephony/DataCallState:version	I
        //     889: iconst_4
        //     890: if_icmpge +10 -> 900
        //     893: getstatic 210	com/android/internal/telephony/DataCallState$SetupResult:ERR_GetLastErrorFromRil	Lcom/android/internal/telephony/DataCallState$SetupResult;
        //     896: astore_3
        //     897: goto -598 -> 299
        //     900: getstatic 213	com/android/internal/telephony/DataCallState$SetupResult:ERR_RilError	Lcom/android/internal/telephony/DataCallState$SetupResult;
        //     903: astore_3
        //     904: goto -605 -> 299
        //     907: sipush 128
        //     910: istore 37
        //     912: goto -708 -> 204
        //     915: iinc 18 1
        //     918: goto -144 -> 774
        //
        // Exception table:
        //     from	to	target	type
        //     54	119	262	java/net/UnknownHostException
        //     137	168	262	java/net/UnknownHostException
        //     172	179	262	java/net/UnknownHostException
        //     179	259	262	java/net/UnknownHostException
        //     357	468	262	java/net/UnknownHostException
        //     479	486	262	java/net/UnknownHostException
        //     486	629	262	java/net/UnknownHostException
        //     640	647	262	java/net/UnknownHostException
        //     647	810	262	java/net/UnknownHostException
        //     813	820	262	java/net/UnknownHostException
        //     820	882	262	java/net/UnknownHostException
        //     172	179	355	java/lang/IllegalArgumentException
        //     479	486	503	java/lang/IllegalArgumentException
        //     640	647	664	java/lang/IllegalArgumentException
        //     813	820	848	java/lang/IllegalArgumentException
    }

    public String toString()
    {
        StringBuffer localStringBuffer = new StringBuffer();
        localStringBuffer.append("DataCallState: {").append("version=").append(this.version).append(" status=").append(this.status).append(" retry=").append(this.suggestedRetryTime).append(" cid=").append(this.cid).append(" active=").append(this.active).append(" type=").append(this.type).append("' ifname='").append(this.ifname);
        localStringBuffer.append("' addresses=[");
        String[] arrayOfString1 = this.addresses;
        int i = arrayOfString1.length;
        for (int j = 0; j < i; j++)
        {
            localStringBuffer.append(arrayOfString1[j]);
            localStringBuffer.append(",");
        }
        if (this.addresses.length > 0)
            localStringBuffer.deleteCharAt(-1 + localStringBuffer.length());
        localStringBuffer.append("] dnses=[");
        String[] arrayOfString2 = this.dnses;
        int k = arrayOfString2.length;
        for (int m = 0; m < k; m++)
        {
            localStringBuffer.append(arrayOfString2[m]);
            localStringBuffer.append(",");
        }
        if (this.dnses.length > 0)
            localStringBuffer.deleteCharAt(-1 + localStringBuffer.length());
        localStringBuffer.append("] gateways=[");
        String[] arrayOfString3 = this.gateways;
        int n = arrayOfString3.length;
        for (int i1 = 0; i1 < n; i1++)
        {
            localStringBuffer.append(arrayOfString3[i1]);
            localStringBuffer.append(",");
        }
        if (this.gateways.length > 0)
            localStringBuffer.deleteCharAt(-1 + localStringBuffer.length());
        localStringBuffer.append("]}");
        return localStringBuffer.toString();
    }

    public static enum SetupResult
    {
        public DataConnection.FailCause mFailCause = DataConnection.FailCause.fromInt(0);

        static
        {
            ERR_BadCommand = new SetupResult("ERR_BadCommand", 1);
            ERR_UnacceptableParameter = new SetupResult("ERR_UnacceptableParameter", 2);
            ERR_GetLastErrorFromRil = new SetupResult("ERR_GetLastErrorFromRil", 3);
            ERR_Stale = new SetupResult("ERR_Stale", 4);
            ERR_RilError = new SetupResult("ERR_RilError", 5);
            SetupResult[] arrayOfSetupResult = new SetupResult[6];
            arrayOfSetupResult[0] = SUCCESS;
            arrayOfSetupResult[1] = ERR_BadCommand;
            arrayOfSetupResult[2] = ERR_UnacceptableParameter;
            arrayOfSetupResult[3] = ERR_GetLastErrorFromRil;
            arrayOfSetupResult[4] = ERR_Stale;
            arrayOfSetupResult[5] = ERR_RilError;
        }

        public String toString()
        {
            return name() + "    SetupResult.mFailCause=" + this.mFailCause;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.DataCallState
 * JD-Core Version:        0.6.2
 */