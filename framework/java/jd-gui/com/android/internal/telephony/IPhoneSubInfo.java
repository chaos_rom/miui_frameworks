package com.android.internal.telephony;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract interface IPhoneSubInfo extends IInterface
{
    public abstract String getCompleteVoiceMailNumber()
        throws RemoteException;

    public abstract String getDeviceId()
        throws RemoteException;

    public abstract String getDeviceSvn()
        throws RemoteException;

    public abstract String getIccSerialNumber()
        throws RemoteException;

    public abstract String getIsimDomain()
        throws RemoteException;

    public abstract String getIsimImpi()
        throws RemoteException;

    public abstract String[] getIsimImpu()
        throws RemoteException;

    public abstract String getLine1AlphaTag()
        throws RemoteException;

    public abstract String getLine1Number()
        throws RemoteException;

    public abstract String getMsisdn()
        throws RemoteException;

    public abstract String getSubscriberId()
        throws RemoteException;

    public abstract String getVoiceMailAlphaTag()
        throws RemoteException;

    public abstract String getVoiceMailNumber()
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IPhoneSubInfo
    {
        private static final String DESCRIPTOR = "com.android.internal.telephony.IPhoneSubInfo";
        static final int TRANSACTION_getCompleteVoiceMailNumber = 9;
        static final int TRANSACTION_getDeviceId = 1;
        static final int TRANSACTION_getDeviceSvn = 2;
        static final int TRANSACTION_getIccSerialNumber = 4;
        static final int TRANSACTION_getIsimDomain = 12;
        static final int TRANSACTION_getIsimImpi = 11;
        static final int TRANSACTION_getIsimImpu = 13;
        static final int TRANSACTION_getLine1AlphaTag = 6;
        static final int TRANSACTION_getLine1Number = 5;
        static final int TRANSACTION_getMsisdn = 7;
        static final int TRANSACTION_getSubscriberId = 3;
        static final int TRANSACTION_getVoiceMailAlphaTag = 10;
        static final int TRANSACTION_getVoiceMailNumber = 8;

        public Stub()
        {
            attachInterface(this, "com.android.internal.telephony.IPhoneSubInfo");
        }

        public static IPhoneSubInfo asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("com.android.internal.telephony.IPhoneSubInfo");
                if ((localIInterface != null) && ((localIInterface instanceof IPhoneSubInfo)))
                    localObject = (IPhoneSubInfo)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool = true;
            switch (paramInt1)
            {
            default:
                bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            }
            while (true)
            {
                return bool;
                paramParcel2.writeString("com.android.internal.telephony.IPhoneSubInfo");
                continue;
                paramParcel1.enforceInterface("com.android.internal.telephony.IPhoneSubInfo");
                String str12 = getDeviceId();
                paramParcel2.writeNoException();
                paramParcel2.writeString(str12);
                continue;
                paramParcel1.enforceInterface("com.android.internal.telephony.IPhoneSubInfo");
                String str11 = getDeviceSvn();
                paramParcel2.writeNoException();
                paramParcel2.writeString(str11);
                continue;
                paramParcel1.enforceInterface("com.android.internal.telephony.IPhoneSubInfo");
                String str10 = getSubscriberId();
                paramParcel2.writeNoException();
                paramParcel2.writeString(str10);
                continue;
                paramParcel1.enforceInterface("com.android.internal.telephony.IPhoneSubInfo");
                String str9 = getIccSerialNumber();
                paramParcel2.writeNoException();
                paramParcel2.writeString(str9);
                continue;
                paramParcel1.enforceInterface("com.android.internal.telephony.IPhoneSubInfo");
                String str8 = getLine1Number();
                paramParcel2.writeNoException();
                paramParcel2.writeString(str8);
                continue;
                paramParcel1.enforceInterface("com.android.internal.telephony.IPhoneSubInfo");
                String str7 = getLine1AlphaTag();
                paramParcel2.writeNoException();
                paramParcel2.writeString(str7);
                continue;
                paramParcel1.enforceInterface("com.android.internal.telephony.IPhoneSubInfo");
                String str6 = getMsisdn();
                paramParcel2.writeNoException();
                paramParcel2.writeString(str6);
                continue;
                paramParcel1.enforceInterface("com.android.internal.telephony.IPhoneSubInfo");
                String str5 = getVoiceMailNumber();
                paramParcel2.writeNoException();
                paramParcel2.writeString(str5);
                continue;
                paramParcel1.enforceInterface("com.android.internal.telephony.IPhoneSubInfo");
                String str4 = getCompleteVoiceMailNumber();
                paramParcel2.writeNoException();
                paramParcel2.writeString(str4);
                continue;
                paramParcel1.enforceInterface("com.android.internal.telephony.IPhoneSubInfo");
                String str3 = getVoiceMailAlphaTag();
                paramParcel2.writeNoException();
                paramParcel2.writeString(str3);
                continue;
                paramParcel1.enforceInterface("com.android.internal.telephony.IPhoneSubInfo");
                String str2 = getIsimImpi();
                paramParcel2.writeNoException();
                paramParcel2.writeString(str2);
                continue;
                paramParcel1.enforceInterface("com.android.internal.telephony.IPhoneSubInfo");
                String str1 = getIsimDomain();
                paramParcel2.writeNoException();
                paramParcel2.writeString(str1);
                continue;
                paramParcel1.enforceInterface("com.android.internal.telephony.IPhoneSubInfo");
                String[] arrayOfString = getIsimImpu();
                paramParcel2.writeNoException();
                paramParcel2.writeStringArray(arrayOfString);
            }
        }

        private static class Proxy
            implements IPhoneSubInfo
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getCompleteVoiceMailNumber()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.telephony.IPhoneSubInfo");
                    this.mRemote.transact(9, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String str = localParcel2.readString();
                    return str;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getDeviceId()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.telephony.IPhoneSubInfo");
                    this.mRemote.transact(1, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String str = localParcel2.readString();
                    return str;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getDeviceSvn()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.telephony.IPhoneSubInfo");
                    this.mRemote.transact(2, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String str = localParcel2.readString();
                    return str;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getIccSerialNumber()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.telephony.IPhoneSubInfo");
                    this.mRemote.transact(4, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String str = localParcel2.readString();
                    return str;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "com.android.internal.telephony.IPhoneSubInfo";
            }

            public String getIsimDomain()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.telephony.IPhoneSubInfo");
                    this.mRemote.transact(12, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String str = localParcel2.readString();
                    return str;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getIsimImpi()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.telephony.IPhoneSubInfo");
                    this.mRemote.transact(11, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String str = localParcel2.readString();
                    return str;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String[] getIsimImpu()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.telephony.IPhoneSubInfo");
                    this.mRemote.transact(13, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String[] arrayOfString = localParcel2.createStringArray();
                    return arrayOfString;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getLine1AlphaTag()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.telephony.IPhoneSubInfo");
                    this.mRemote.transact(6, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String str = localParcel2.readString();
                    return str;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getLine1Number()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.telephony.IPhoneSubInfo");
                    this.mRemote.transact(5, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String str = localParcel2.readString();
                    return str;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getMsisdn()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.telephony.IPhoneSubInfo");
                    this.mRemote.transact(7, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String str = localParcel2.readString();
                    return str;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getSubscriberId()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.telephony.IPhoneSubInfo");
                    this.mRemote.transact(3, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String str = localParcel2.readString();
                    return str;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getVoiceMailAlphaTag()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.telephony.IPhoneSubInfo");
                    this.mRemote.transact(10, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String str = localParcel2.readString();
                    return str;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getVoiceMailNumber()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.telephony.IPhoneSubInfo");
                    this.mRemote.transact(8, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String str = localParcel2.readString();
                    return str;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.IPhoneSubInfo
 * JD-Core Version:        0.6.2
 */