package com.android.internal.telephony;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public abstract interface IIccPhoneBook extends IInterface
{
    public abstract int getAdnCapacity()
        throws RemoteException;

    public abstract List<AdnRecord> getAdnRecordsInEf(int paramInt)
        throws RemoteException;

    public abstract int[] getAdnRecordsSize(int paramInt)
        throws RemoteException;

    public abstract int getFreeAdn()
        throws RemoteException;

    public abstract boolean updateAdnRecordsInEfByIndex(int paramInt1, String paramString1, String paramString2, int paramInt2, String paramString3)
        throws RemoteException;

    public abstract boolean updateAdnRecordsInEfBySearch(int paramInt, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IIccPhoneBook
    {
        private static final String DESCRIPTOR = "com.android.internal.telephony.IIccPhoneBook";
        static final int TRANSACTION_getAdnCapacity = 6;
        static final int TRANSACTION_getAdnRecordsInEf = 1;
        static final int TRANSACTION_getAdnRecordsSize = 4;
        static final int TRANSACTION_getFreeAdn = 5;
        static final int TRANSACTION_updateAdnRecordsInEfByIndex = 3;
        static final int TRANSACTION_updateAdnRecordsInEfBySearch = 2;

        public Stub()
        {
            attachInterface(this, "com.android.internal.telephony.IIccPhoneBook");
        }

        public static IIccPhoneBook asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("com.android.internal.telephony.IIccPhoneBook");
                if ((localIInterface != null) && ((localIInterface instanceof IIccPhoneBook)))
                    localObject = (IIccPhoneBook)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            int i = 0;
            int j = 1;
            switch (paramInt1)
            {
            default:
                j = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            }
            while (true)
            {
                return j;
                paramParcel2.writeString("com.android.internal.telephony.IIccPhoneBook");
                continue;
                paramParcel1.enforceInterface("com.android.internal.telephony.IIccPhoneBook");
                List localList = getAdnRecordsInEf(paramParcel1.readInt());
                paramParcel2.writeNoException();
                paramParcel2.writeTypedList(localList);
                continue;
                paramParcel1.enforceInterface("com.android.internal.telephony.IIccPhoneBook");
                boolean bool2 = updateAdnRecordsInEfBySearch(paramParcel1.readInt(), paramParcel1.readString(), paramParcel1.readString(), paramParcel1.readString(), paramParcel1.readString(), paramParcel1.readString());
                paramParcel2.writeNoException();
                if (bool2);
                int i1;
                for (int n = j; ; i1 = 0)
                {
                    paramParcel2.writeInt(n);
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.telephony.IIccPhoneBook");
                boolean bool1 = updateAdnRecordsInEfByIndex(paramParcel1.readInt(), paramParcel1.readString(), paramParcel1.readString(), paramParcel1.readInt(), paramParcel1.readString());
                paramParcel2.writeNoException();
                if (bool1)
                    i = j;
                paramParcel2.writeInt(i);
                continue;
                paramParcel1.enforceInterface("com.android.internal.telephony.IIccPhoneBook");
                int[] arrayOfInt = getAdnRecordsSize(paramParcel1.readInt());
                paramParcel2.writeNoException();
                paramParcel2.writeIntArray(arrayOfInt);
                continue;
                paramParcel1.enforceInterface("com.android.internal.telephony.IIccPhoneBook");
                int m = getFreeAdn();
                paramParcel2.writeNoException();
                paramParcel2.writeInt(m);
                continue;
                paramParcel1.enforceInterface("com.android.internal.telephony.IIccPhoneBook");
                int k = getAdnCapacity();
                paramParcel2.writeNoException();
                paramParcel2.writeInt(k);
            }
        }

        private static class Proxy
            implements IIccPhoneBook
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public int getAdnCapacity()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.telephony.IIccPhoneBook");
                    this.mRemote.transact(6, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public List<AdnRecord> getAdnRecordsInEf(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.telephony.IIccPhoneBook");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(1, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    ArrayList localArrayList = localParcel2.createTypedArrayList(AdnRecord.CREATOR);
                    return localArrayList;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int[] getAdnRecordsSize(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.telephony.IIccPhoneBook");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(4, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int[] arrayOfInt = localParcel2.createIntArray();
                    return arrayOfInt;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getFreeAdn()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.telephony.IIccPhoneBook");
                    this.mRemote.transact(5, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "com.android.internal.telephony.IIccPhoneBook";
            }

            public boolean updateAdnRecordsInEfByIndex(int paramInt1, String paramString1, String paramString2, int paramInt2, String paramString3)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.telephony.IIccPhoneBook");
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeString(paramString1);
                    localParcel1.writeString(paramString2);
                    localParcel1.writeInt(paramInt2);
                    localParcel1.writeString(paramString3);
                    this.mRemote.transact(3, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean updateAdnRecordsInEfBySearch(int paramInt, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.telephony.IIccPhoneBook");
                    localParcel1.writeInt(paramInt);
                    localParcel1.writeString(paramString1);
                    localParcel1.writeString(paramString2);
                    localParcel1.writeString(paramString3);
                    localParcel1.writeString(paramString4);
                    localParcel1.writeString(paramString5);
                    this.mRemote.transact(2, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.IIccPhoneBook
 * JD-Core Version:        0.6.2
 */