package com.android.internal.telephony;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class OperatorInfo
    implements Parcelable
{
    public static final Parcelable.Creator<OperatorInfo> CREATOR = new Parcelable.Creator()
    {
        public OperatorInfo createFromParcel(Parcel paramAnonymousParcel)
        {
            return new OperatorInfo(paramAnonymousParcel.readString(), paramAnonymousParcel.readString(), paramAnonymousParcel.readString(), (OperatorInfo.State)paramAnonymousParcel.readSerializable());
        }

        public OperatorInfo[] newArray(int paramAnonymousInt)
        {
            return new OperatorInfo[paramAnonymousInt];
        }
    };
    private String operatorAlphaLong;
    private String operatorAlphaShort;
    private String operatorNumeric;
    private State state = State.UNKNOWN;

    OperatorInfo(String paramString1, String paramString2, String paramString3, State paramState)
    {
        this.operatorAlphaLong = paramString1;
        this.operatorAlphaShort = paramString2;
        this.operatorNumeric = paramString3;
        this.state = paramState;
    }

    public OperatorInfo(String paramString1, String paramString2, String paramString3, String paramString4)
    {
        this(paramString1, paramString2, paramString3, rilStateToState(paramString4));
    }

    private static State rilStateToState(String paramString)
    {
        State localState;
        if (paramString.equals("unknown"))
            localState = State.UNKNOWN;
        while (true)
        {
            return localState;
            if (paramString.equals("available"))
            {
                localState = State.AVAILABLE;
            }
            else if (paramString.equals("current"))
            {
                localState = State.CURRENT;
            }
            else
            {
                if (!paramString.equals("forbidden"))
                    break;
                localState = State.FORBIDDEN;
            }
        }
        throw new RuntimeException("RIL impl error: Invalid network state '" + paramString + "'");
    }

    public int describeContents()
    {
        return 0;
    }

    public String getOperatorAlphaLong()
    {
        return this.operatorAlphaLong;
    }

    public String getOperatorAlphaShort()
    {
        return this.operatorAlphaShort;
    }

    public String getOperatorNumeric()
    {
        return this.operatorNumeric;
    }

    public State getState()
    {
        return this.state;
    }

    public String toString()
    {
        return "OperatorInfo " + this.operatorAlphaLong + "/" + this.operatorAlphaShort + "/" + this.operatorNumeric + "/" + this.state;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeString(this.operatorAlphaLong);
        paramParcel.writeString(this.operatorAlphaShort);
        paramParcel.writeString(this.operatorNumeric);
        paramParcel.writeSerializable(this.state);
    }

    public static enum State
    {
        static
        {
            AVAILABLE = new State("AVAILABLE", 1);
            CURRENT = new State("CURRENT", 2);
            FORBIDDEN = new State("FORBIDDEN", 3);
            State[] arrayOfState = new State[4];
            arrayOfState[0] = UNKNOWN;
            arrayOfState[1] = AVAILABLE;
            arrayOfState[2] = CURRENT;
            arrayOfState[3] = FORBIDDEN;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.OperatorInfo
 * JD-Core Version:        0.6.2
 */