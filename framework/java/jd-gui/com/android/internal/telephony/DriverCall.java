package com.android.internal.telephony;

import android.telephony.PhoneNumberUtils;
import android.util.Log;

public class DriverCall
    implements Comparable
{
    static final String LOG_TAG = "RILB";
    public int TOA;
    public int als;
    public int index;
    public boolean isMT;
    public boolean isMpty;
    public boolean isVoice;
    public boolean isVoicePrivacy;
    public String name;
    public int namePresentation;
    public String number;
    public int numberPresentation;
    public State state;
    public UUSInfo uusInfo;

    static DriverCall fromCLCCLine(String paramString)
    {
        DriverCall localDriverCall = new DriverCall();
        ATResponseParser localATResponseParser = new ATResponseParser(paramString);
        try
        {
            localDriverCall.index = localATResponseParser.nextInt();
            localDriverCall.isMT = localATResponseParser.nextBoolean();
            localDriverCall.state = stateFromCLCC(localATResponseParser.nextInt());
            if (localATResponseParser.nextInt() == 0);
            for (boolean bool = true; ; bool = false)
            {
                localDriverCall.isVoice = bool;
                localDriverCall.isMpty = localATResponseParser.nextBoolean();
                localDriverCall.numberPresentation = Connection.PRESENTATION_ALLOWED;
                if (localATResponseParser.hasMore())
                {
                    localDriverCall.number = PhoneNumberUtils.extractNetworkPortionAlt(localATResponseParser.nextString());
                    if (localDriverCall.number.length() == 0)
                        localDriverCall.number = null;
                    localDriverCall.TOA = localATResponseParser.nextInt();
                    localDriverCall.number = PhoneNumberUtils.stringFromStringAndTOA(localDriverCall.number, localDriverCall.TOA);
                }
                return localDriverCall;
            }
        }
        catch (ATParseEx localATParseEx)
        {
            while (true)
            {
                Log.e("RILB", "Invalid CLCC line: '" + paramString + "'");
                localDriverCall = null;
            }
        }
    }

    public static int presentationFromCLIP(int paramInt)
        throws ATParseEx
    {
        int i;
        switch (paramInt)
        {
        default:
            throw new ATParseEx("illegal presentation " + paramInt);
        case 0:
            i = Connection.PRESENTATION_ALLOWED;
        case 1:
        case 2:
        case 3:
        }
        while (true)
        {
            return i;
            i = Connection.PRESENTATION_RESTRICTED;
            continue;
            i = Connection.PRESENTATION_UNKNOWN;
            continue;
            i = Connection.PRESENTATION_PAYPHONE;
        }
    }

    public static State stateFromCLCC(int paramInt)
        throws ATParseEx
    {
        State localState;
        switch (paramInt)
        {
        default:
            throw new ATParseEx("illegal call state " + paramInt);
        case 0:
            localState = State.ACTIVE;
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        }
        while (true)
        {
            return localState;
            localState = State.HOLDING;
            continue;
            localState = State.DIALING;
            continue;
            localState = State.ALERTING;
            continue;
            localState = State.INCOMING;
            continue;
            localState = State.WAITING;
        }
    }

    public int compareTo(Object paramObject)
    {
        DriverCall localDriverCall = (DriverCall)paramObject;
        int i;
        if (this.index < localDriverCall.index)
            i = -1;
        while (true)
        {
            return i;
            if (this.index == localDriverCall.index)
                i = 0;
            else
                i = 1;
        }
    }

    public String toString()
    {
        StringBuilder localStringBuilder1 = new StringBuilder().append("id=").append(this.index).append(",").append(this.state).append(",").append("toa=").append(this.TOA).append(",");
        String str1;
        String str2;
        label86: String str3;
        label122: StringBuilder localStringBuilder4;
        if (this.isMpty)
        {
            str1 = "conf";
            StringBuilder localStringBuilder2 = localStringBuilder1.append(str1).append(",");
            if (!this.isMT)
                break label198;
            str2 = "mt";
            StringBuilder localStringBuilder3 = localStringBuilder2.append(str2).append(",").append(this.als).append(",");
            if (!this.isVoice)
                break label205;
            str3 = "voc";
            localStringBuilder4 = localStringBuilder3.append(str3).append(",");
            if (!this.isVoicePrivacy)
                break label212;
        }
        label198: label205: label212: for (String str4 = "evp"; ; str4 = "noevp")
        {
            return str4 + "," + ",cli=" + this.numberPresentation + "," + "," + this.namePresentation;
            str1 = "norm";
            break;
            str2 = "mo";
            break label86;
            str3 = "nonvoc";
            break label122;
        }
    }

    public static enum State
    {
        static
        {
            DIALING = new State("DIALING", 2);
            ALERTING = new State("ALERTING", 3);
            INCOMING = new State("INCOMING", 4);
            WAITING = new State("WAITING", 5);
            State[] arrayOfState = new State[6];
            arrayOfState[0] = ACTIVE;
            arrayOfState[1] = HOLDING;
            arrayOfState[2] = DIALING;
            arrayOfState[3] = ALERTING;
            arrayOfState[4] = INCOMING;
            arrayOfState[5] = WAITING;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.DriverCall
 * JD-Core Version:        0.6.2
 */