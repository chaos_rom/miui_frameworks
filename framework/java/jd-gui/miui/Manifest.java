package miui;

public final class Manifest
{
    public static final class permission
    {
        public static final String ACCESS_NOTE = "com.miui.notes.permission.ACCESS_NOTE";
        public static final String ACTIVATE_MIMSG = "com.xiaomi.permission.ACTIVATE_MIMSG";
        public static final String AUTO_BACKUP = "com.miui.backup.permission.AUTO_BACKUP";
        public static final String CLOUD_MANAGER = "com.xiaomi.permission.CLOUD_MANAGER";
        public static final String FIND_DEVICE = "com.miui.cloudservice.permission.FIND_DEVICE";
        public static final String PAYMENT = "com.xiaomi.xmsf.permission.PAYMENT";
        public static final String SEND_PUSH = "com.xiaomi.permission.SEND_PUSH";
        public static final String SHELL = "miui.permission.SHELL";
        public static final String TIGGER_TOGGLE = "com.android.SystemUI.permission.TIGGER_TOGGLE";
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         miui.Manifest
 * JD-Core Version:        0.6.2
 */