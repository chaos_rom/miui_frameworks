package com.android.thememanager.view;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ResolveInfo.DisplayNameComparator;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import java.util.Collections;
import java.util.List;

public class ThirdAppPicker extends Activity
  implements AdapterView.OnItemClickListener
{
  private List<ResolveInfo> mAllApps;
  private FileListAdapter mListAdapter;
  private ListView mListView;
  private PackageManager mPackageManager;

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903041);
    getActionBar().setTitle(101449755);
    getActionBar().setHomeButtonEnabled(true);
    this.mPackageManager = getPackageManager();
    Intent localIntent = new Intent("android.intent.action.MAIN", null);
    localIntent.addCategory("android.intent.category.LAUNCHER");
    this.mAllApps = this.mPackageManager.queryIntentActivities(localIntent, 0);
    Collections.sort(this.mAllApps, new ResolveInfo.DisplayNameComparator(this.mPackageManager));
    this.mAllApps.add(null);
    this.mListView = ((ListView)findViewById(2131165189));
    this.mListAdapter = new FileListAdapter(this, 2130903040, this.mAllApps);
    this.mListView.setAdapter(this.mListAdapter);
    this.mListView.setOnItemClickListener(this);
  }

  public void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    ResolveInfo localResolveInfo = (ResolveInfo)this.mAllApps.get(paramInt);
    Intent localIntent = new Intent();
    if (localResolveInfo != null)
    {
      localIntent.putExtra("name", localResolveInfo.loadLabel(this.mPackageManager));
      localIntent.setClassName(localResolveInfo.activityInfo.packageName, localResolveInfo.activityInfo.name);
    }
    if (localResolveInfo == null)
      localIntent = null;
    setResult(-1, localIntent);
    finish();
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    if (paramMenuItem.getItemId() == 16908332)
      finish();
    return super.onOptionsItemSelected(paramMenuItem);
  }

  private class FileListAdapter extends ArrayAdapter<ResolveInfo>
  {
    private LayoutInflater mInflater;

    public FileListAdapter(int paramList, List<ResolveInfo> arg3)
    {
      super(i, localList);
      this.mInflater = LayoutInflater.from(paramList);
    }

    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      View localView;
      if (paramView == null)
        localView = this.mInflater.inflate(2130903040, paramViewGroup, false);
      else
        localView = paramView;
      ResolveInfo localResolveInfo = (ResolveInfo)ThirdAppPicker.this.mAllApps.get(paramInt);
      ImageView localImageView = (ImageView)localView.findViewById(2131165187);
      TextView localTextView = (TextView)localView.findViewById(2131165188);
      Drawable localDrawable;
      if (localResolveInfo != null)
        localDrawable = localResolveInfo.loadIcon(ThirdAppPicker.this.mPackageManager);
      else
        localDrawable = null;
      localImageView.setImageDrawable(localDrawable);
      if (localResolveInfo == null)
        localTextView.setText(2131427339);
      else
        localTextView.setText(localResolveInfo.loadLabel(ThirdAppPicker.this.mPackageManager));
      return localView;
    }
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     com.android.thememanager.view.ThirdAppPicker
 * JD-Core Version:    0.6.0
 */