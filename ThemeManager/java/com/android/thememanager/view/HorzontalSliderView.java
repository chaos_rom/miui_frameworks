package com.android.thememanager.view;

import android.R.styleable;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.LinearLayout;

public class HorzontalSliderView extends LinearLayout
{
  private SliderMoveListener mMoveListener;
  private int mSliderCenterPositionX = -1;
  private Drawable mSliderDrawable;
  private int mSliderLeft = -1;
  private int mSliderMaxLeft = -1;
  private int mSliderMinLeft = -1;
  private int mSliderStartMoveX = -1;

  public HorzontalSliderView(Context paramContext)
  {
    this(paramContext, null);
  }

  public HorzontalSliderView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }

  public HorzontalSliderView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    this.mSliderDrawable = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ImageView).getDrawable(0);
    this.mSliderDrawable.setBounds(0, 0, this.mSliderDrawable.getIntrinsicWidth(), this.mSliderDrawable.getIntrinsicHeight());
    if (this.mSliderDrawable != null)
      return;
    throw new IllegalArgumentException("HorzontalSliderView() must have android:src attribute.");
  }

  private int getCenterPostionForSlider()
  {
    if (this.mSliderCenterPositionX < 0)
    {
      this.mSliderMinLeft = getPaddingLeft();
      this.mSliderMaxLeft = (getWidth() - this.mSliderDrawable.getIntrinsicWidth() - getPaddingRight());
      this.mSliderCenterPositionX = ((this.mSliderMaxLeft + this.mSliderMinLeft) / 2);
    }
    return this.mSliderCenterPositionX;
  }

  private int getSliderCanMoveDistance()
  {
    return (this.mSliderMaxLeft - this.mSliderMinLeft) / 2;
  }

  private boolean inSliderDrawableArea(int paramInt)
  {
    int i;
    if ((this.mSliderLeft > paramInt) || (paramInt > this.mSliderLeft + this.mSliderDrawable.getIntrinsicWidth()))
      i = 0;
    else
      i = 1;
    return i;
  }

  private void updateSliderPostion(int paramInt, boolean paramBoolean)
  {
    int i = this.mSliderLeft;
    int j = paramInt + getCenterPostionForSlider();
    if (j < this.mSliderMinLeft)
      j = this.mSliderMinLeft;
    if (j > this.mSliderMaxLeft)
      j = this.mSliderMaxLeft;
    if (i != j)
    {
      this.mSliderLeft = j;
      invalidate();
      if (this.mMoveListener != null)
        this.mMoveListener.movePercent(1.0F * paramInt / getSliderCanMoveDistance(), paramBoolean);
    }
  }

  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    if (this.mSliderLeft < 0)
      this.mSliderLeft = getCenterPostionForSlider();
    int i = paramCanvas.getSaveCount();
    paramCanvas.translate(this.mSliderLeft, getPaddingTop());
    this.mSliderDrawable.draw(paramCanvas);
    paramCanvas.restoreToCount(i);
  }

  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    int i = (int)paramMotionEvent.getX();
    switch (paramMotionEvent.getAction())
    {
    case 0:
      if (!inSliderDrawableArea(i))
        break;
      this.mSliderStartMoveX = i;
      break;
    case 1:
      if (this.mSliderStartMoveX < 0)
        break;
      updateSliderPostion(0, true);
      this.mSliderStartMoveX = -1;
      break;
    case 2:
      if (this.mSliderStartMoveX < 0)
        break;
      updateSliderPostion(i - this.mSliderStartMoveX, false);
    }
    return true;
  }

  public void regeisterMoveListener(SliderMoveListener paramSliderMoveListener)
  {
    this.mMoveListener = paramSliderMoveListener;
  }

  public static abstract interface SliderMoveListener
  {
    public abstract void movePercent(float paramFloat, boolean paramBoolean);
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     com.android.thememanager.view.HorzontalSliderView
 * JD-Core Version:    0.6.0
 */