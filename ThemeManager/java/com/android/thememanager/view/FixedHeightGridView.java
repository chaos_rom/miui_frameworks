package com.android.thememanager.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AbsListView.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.GridView;

public class FixedHeightGridView extends GridView
{
  private int mChildMaxHeight = 0;

  public FixedHeightGridView(Context paramContext)
  {
    super(paramContext);
  }

  public FixedHeightGridView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  public FixedHeightGridView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }

  public int getChildMaxHeight()
  {
    return this.mChildMaxHeight;
  }

  protected void layoutChildren()
  {
    super.layoutChildren();
    updateGridViewHeight();
  }

  protected void onMeasure(int paramInt1, int paramInt2)
  {
    super.onMeasure(paramInt1, View.MeasureSpec.makeMeasureSpec(536870911, -2147483648));
  }

  public void updateGridViewHeight()
  {
    int i = 0;
    for (int k = 0; ; k++)
    {
      View localView;
      if (k >= getChildCount())
      {
        if (i == this.mChildMaxHeight)
          k = 0;
        else
          k = 1;
        if (k != 0)
          this.mChildMaxHeight = i;
        for (k = 0; ; k++)
        {
          if (k >= getChildCount())
            return;
          localView = getChildAt(k);
          ViewGroup.LayoutParams localLayoutParams = localView.getLayoutParams();
          localLayoutParams.height = this.mChildMaxHeight;
          localView.setLayoutParams(localLayoutParams);
        }
      }
      int j = Math.max(localView, getChildAt(k).getHeight());
    }
  }

  public static abstract class FixedGridAdapter extends BaseAdapter
  {
    FixedHeightGridView mFixGridView;

    public FixedGridAdapter(FixedHeightGridView paramFixedHeightGridView)
    {
      this.mFixGridView = paramFixedHeightGridView;
    }

    public abstract View getChildView(int paramInt, View paramView, ViewGroup paramViewGroup);

    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      View localView = getChildView(paramInt, paramView, paramViewGroup);
      if ((localView != null) && (this.mFixGridView.getChildMaxHeight() != 0))
      {
        Object localObject = localView.getLayoutParams();
        if (localObject == null)
        {
          localObject = new AbsListView.LayoutParams(-1, -2, 0);
          localView.setLayoutParams((ViewGroup.LayoutParams)localObject);
        }
        ((ViewGroup.LayoutParams)localObject).height = this.mFixGridView.getChildMaxHeight();
      }
      return (View)localView;
    }
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     com.android.thememanager.view.FixedHeightGridView
 * JD-Core Version:    0.6.0
 */