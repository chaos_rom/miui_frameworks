package com.android.thememanager.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.AnimatedRotateDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.Callback;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import com.android.thememanager.util.MoveAnimation;
import com.android.thememanager.util.ThemeHelper;

public class WallpaperView extends View
{
  private int mContainingBitmapNeedHeight;
  private int mContainingBitmapNeedWidth;
  private Rect mCurrentVisiableArea = null;
  private final WallpaperBitmap mCurrentWallpaper = new WallpaperBitmap();
  private final WallpaperBitmap mNextWallpaper = new WallpaperBitmap();
  private final WallpaperBitmap mPreviousWallpaper = new WallpaperBitmap();
  private Point mScreenSize;
  private WallpaperSwitchListener mSwitchListener;
  private boolean mThumbnailMode;

  public WallpaperView(Context paramContext)
  {
    this(paramContext, null);
  }

  public WallpaperView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  private Rect getDefaultVisiableArea()
  {
    int i;
    if (!this.mThumbnailMode)
      i = 0;
    else
      i = getThumbnailModeVerticalPadding();
    return new Rect(0, i, getWidth(), getHeight() - i);
  }

  private int getThumbnailModeVerticalPadding()
  {
    return (getHeight() - this.mCurrentWallpaper.getThumbnailHeight()) / 2;
  }

  private WallpaperBitmap getWallpaperBitmap(int paramInt)
  {
    WallpaperBitmap localWallpaperBitmap;
    if (paramInt >= 0)
    {
      if (paramInt <= 0)
        localWallpaperBitmap = this.mCurrentWallpaper;
      else
        localWallpaperBitmap = this.mNextWallpaper;
    }
    else
      localWallpaperBitmap = this.mPreviousWallpaper;
    return localWallpaperBitmap;
  }

  private void resetWallpaperShowingState()
  {
    this.mPreviousWallpaper.reset();
    this.mCurrentWallpaper.reset();
    this.mNextWallpaper.reset();
    this.mCurrentVisiableArea = getDefaultVisiableArea();
  }

  public void autoSwitchCurreentWallpaper()
  {
    int m = this.mCurrentVisiableArea.left;
    if (m != 0)
    {
      int j = Math.abs(m);
      int n = this.mCurrentVisiableArea.width();
      int k = j;
      int i = -m / j;
      if ((j > 0.25F * n) && (((m > 0) && ((this.mPreviousWallpaper.determinateBitmap != null) || (this.mPreviousWallpaper.showIndeterminateBitmap))) || ((m < 0) && ((this.mNextWallpaper.determinateBitmap != null) || (this.mNextWallpaper.showIndeterminateBitmap)))))
      {
        k = n - j;
        i = m / j;
      }
      new MoveAnimation()
      {
        public void onFinish(int paramInt)
        {
          int i = WallpaperView.this.mCurrentVisiableArea.left;
          if (i >= 0)
          {
            if (i <= 0)
            {
              if (WallpaperView.this.mSwitchListener != null)
                WallpaperView.this.mSwitchListener.switchNone();
            }
            else if (WallpaperView.this.mSwitchListener != null)
              WallpaperView.this.mSwitchListener.switchPrevious();
          }
          else if (WallpaperView.this.mSwitchListener != null)
            WallpaperView.this.mSwitchListener.switchNext();
          WallpaperView.this.resetWallpaperShowingState();
          WallpaperView.this.invalidate();
        }

        public void onMove(int paramInt)
        {
          Rect localRect = WallpaperView.this.mCurrentVisiableArea;
          localRect.left = (paramInt + localRect.left);
          localRect = WallpaperView.this.mCurrentVisiableArea;
          localRect.right = (paramInt + localRect.right);
          WallpaperView.this.invalidate();
        }
      }
      .start(i * k);
    }
  }

  public int getUserGivenId(int paramInt)
  {
    return getWallpaperBitmap(paramInt).id;
  }

  public boolean hasBeenInitied()
  {
    int i;
    if ((this.mCurrentVisiableArea == null) || (getWidth() == 0))
      i = 0;
    else
      i = 1;
    return i;
  }

  public void horizontalMove(int paramInt)
  {
    if (paramInt != 0)
    {
      Rect localRect = this.mCurrentVisiableArea;
      localRect.left = (paramInt + localRect.left);
      localRect = this.mCurrentVisiableArea;
      localRect.right = (paramInt + localRect.right);
      invalidate();
    }
  }

  public void initScanMode(boolean paramBoolean)
  {
    this.mThumbnailMode = paramBoolean;
  }

  public boolean isThumbnailScanMode()
  {
    return this.mThumbnailMode;
  }

  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    if (this.mCurrentVisiableArea == null)
      this.mCurrentVisiableArea = getDefaultVisiableArea();
    Rect localRect;
    if (0 + this.mCurrentVisiableArea.left > 0)
    {
      localRect = new Rect();
      localRect.right = (0 + this.mCurrentVisiableArea.left);
      localRect.left = (localRect.right - this.mCurrentVisiableArea.width());
      localRect.top = this.mCurrentVisiableArea.top;
      localRect.bottom = (localRect.top + this.mCurrentVisiableArea.height());
      this.mPreviousWallpaper.draw(paramCanvas, localRect);
    }
    this.mCurrentWallpaper.draw(paramCanvas, this.mCurrentVisiableArea);
    if (0 + this.mCurrentVisiableArea.right <= getWidth())
    {
      localRect = new Rect();
      localRect.left = (0 + this.mCurrentVisiableArea.right);
      localRect.right = (localRect.left + this.mCurrentVisiableArea.width());
      localRect.top = this.mCurrentVisiableArea.top;
      localRect.bottom = (localRect.top + this.mCurrentVisiableArea.height());
      this.mNextWallpaper.draw(paramCanvas, localRect);
    }
  }

  protected void onMeasure(int paramInt1, int paramInt2)
  {
    DisplayMetrics localDisplayMetrics = ThemeHelper.getScreenDisplayMetrics(this.mContext);
    setMeasuredDimension(localDisplayMetrics.widthPixels, localDisplayMetrics.heightPixels);
  }

  public void regeisterSwitchListener(WallpaperSwitchListener paramWallpaperSwitchListener)
  {
    this.mSwitchListener = paramWallpaperSwitchListener;
  }

  public void reset()
  {
    setBitmapInfo(0, null, -2147483648, false, false);
    setBitmapInfo(1, null, -2147483648, false, false);
    setBitmapInfo(-1, null, -2147483648, false, false);
  }

  public void setBitmapInfo(int paramInt1, Bitmap paramBitmap, int paramInt2, boolean paramBoolean1, boolean paramBoolean2)
  {
    WallpaperBitmap localWallpaperBitmap = getWallpaperBitmap(paramInt1);
    localWallpaperBitmap.setBitmap(paramBitmap);
    localWallpaperBitmap.showIndeterminateBitmap = paramBoolean1;
    WallpaperBitmap.access$002(localWallpaperBitmap, paramBoolean2);
    WallpaperBitmap.access$102(localWallpaperBitmap, paramInt2);
  }

  public void setContainingBitmapSize(int paramInt1, int paramInt2)
  {
    this.mContainingBitmapNeedWidth = paramInt1;
    this.mContainingBitmapNeedHeight = paramInt2;
    if (this.mScreenSize == null)
    {
      this.mScreenSize = new Point();
      this.mScreenSize.x = this.mContext.getResources().getDisplayMetrics().widthPixels;
      this.mScreenSize.y = this.mContext.getResources().getDisplayMetrics().heightPixels;
    }
  }

  public void setScanMode(boolean paramBoolean)
  {
    if (paramBoolean != this.mThumbnailMode)
    {
      int i = getThumbnailModeVerticalPadding();
      if (this.mThumbnailMode)
        i = -i;
      if (i != 0)
        new MoveAnimation()
        {
          public void onFinish(int paramInt)
          {
            WallpaperView localWallpaperView = WallpaperView.this;
            boolean bool;
            if (WallpaperView.this.mThumbnailMode)
              bool = false;
            else
              bool = true;
            WallpaperView.access$802(localWallpaperView, bool);
            WallpaperView.this.resetWallpaperShowingState();
            WallpaperView.this.invalidate();
          }

          public void onMove(int paramInt)
          {
            Rect localRect = WallpaperView.this.mCurrentVisiableArea;
            localRect.top = (paramInt + localRect.top);
            localRect = WallpaperView.this.mCurrentVisiableArea;
            localRect.bottom -= paramInt;
            int i = WallpaperView.this.mContainingBitmapNeedHeight;
            int m = WallpaperView.this.mContainingBitmapNeedWidth;
            int k = WallpaperView.this.mCurrentVisiableArea.height();
            int j = WallpaperView.this.mCurrentVisiableArea.width();
            m = (m - j * i / k) / 2;
            j = m + j * i / k;
            WallpaperView.this.mCurrentWallpaper.setDrawingArea(m, 0, j, i);
          }
        }
        .start(i, 5, 10);
    }
  }

  public boolean showingDeterminateFg(int paramInt)
  {
    WallpaperBitmap localWallpaperBitmap = getWallpaperBitmap(paramInt);
    int i;
    if ((localWallpaperBitmap.determinateBitmap == null) || (!localWallpaperBitmap.showDeterminateFgImage))
      i = 0;
    else
      i = 1;
    return i;
  }

  public void updateCurrentWallpaperShowingArea(float paramFloat, boolean paramBoolean)
  {
    this.mCurrentWallpaper.udpateShowingArea(paramFloat, paramBoolean);
  }

  public static abstract interface WallpaperSwitchListener
  {
    public abstract void switchNext();

    public abstract void switchNone();

    public abstract void switchPrevious();
  }

  class WallpaperBitmap
  {
    private int bottom;
    private Rect canvasVisiableArea;
    public Bitmap determinateBitmap;
    private AnimatedRotateDrawable determinateFgDrawable;
    private float horizontalRatio;
    private int id;
    private Drawable inderterminateDrawable = WallpaperView.this.mContext.getResources().getDrawable(2130837546);
    private String inderterminateText;
    private boolean initialized;
    private int left;
    private Rect mTmpRect = new Rect();
    private int right;
    private WallpaperHandler scheduleHandler;
    private boolean showDeterminateFgImage;
    public boolean showIndeterminateBitmap;
    private int top;
    private float verticalRatio;

    public WallpaperBitmap()
    {
      this.inderterminateDrawable.setBounds(0, 0, this.inderterminateDrawable.getIntrinsicWidth(), this.inderterminateDrawable.getIntrinsicHeight());
      this.inderterminateText = WallpaperView.this.mContext.getString(2131427400);
      this.showIndeterminateBitmap = true;
      this.determinateFgDrawable = ((AnimatedRotateDrawable)WallpaperView.this.mContext.getResources().getDrawable(2130837515));
      this.determinateFgDrawable.setBounds(0, 0, this.determinateFgDrawable.getIntrinsicWidth(), this.determinateFgDrawable.getIntrinsicHeight());
      this.scheduleHandler = new WallpaperHandler();
      this.determinateFgDrawable.setCallback(this.scheduleHandler);
    }

    private int getDefaultLeftPostion()
    {
      return (WallpaperView.this.mContainingBitmapNeedWidth - getShowingWidth()) / 2;
    }

    private Rect getDrawingArea()
    {
      init();
      this.mTmpRect.left = (int)(0.5F + this.left * this.horizontalRatio);
      this.mTmpRect.right = (int)(0.5F + this.right * this.horizontalRatio);
      this.mTmpRect.top = (int)(0.5F + this.top * this.verticalRatio);
      this.mTmpRect.bottom = (int)(0.5F + this.bottom * this.verticalRatio);
      return this.mTmpRect;
    }

    private int getShowingWidth()
    {
      int i;
      if (!WallpaperView.this.mThumbnailMode)
        i = WallpaperView.this.getWidth();
      else
        i = WallpaperView.this.mContainingBitmapNeedWidth;
      return i;
    }

    private void horizontallyMoveOffset(int paramInt)
    {
      if (paramInt != 0)
      {
        this.left = (paramInt + this.left);
        this.right = (paramInt + this.right);
        WallpaperView.this.invalidate();
      }
    }

    private void init()
    {
      if ((!this.initialized) && (WallpaperView.this.getWidth() > 0))
      {
        this.left = getDefaultLeftPostion();
        this.right = (this.left + getShowingWidth());
        this.top = 0;
        this.bottom = WallpaperView.this.mContainingBitmapNeedHeight;
        this.initialized = true;
      }
    }

    public void draw(Canvas paramCanvas, Rect paramRect)
    {
      this.canvasVisiableArea = paramRect;
      if ((this.canvasVisiableArea.right > 0) && (this.canvasVisiableArea.left < WallpaperView.this.mScreenSize.x) && (this.canvasVisiableArea.bottom > 0) && (this.canvasVisiableArea.top < WallpaperView.this.mScreenSize.y))
      {
        if (this.determinateBitmap == null)
        {
          if (this.showIndeterminateBitmap)
          {
            paramCanvas.save();
            int k = this.inderterminateDrawable.getIntrinsicWidth();
            int i = this.inderterminateDrawable.getIntrinsicHeight();
            int n = this.canvasVisiableArea.left + (this.canvasVisiableArea.width() - k) / 2;
            k = this.canvasVisiableArea.top + (this.canvasVisiableArea.height() - i - 15) / 2;
            paramCanvas.translate(n, k);
            this.inderterminateDrawable.draw(paramCanvas);
            paramCanvas.translate(0.0F, 15);
            Paint localPaint = new Paint();
            localPaint.setTextSize(18.0F);
            localPaint.setColor(-1);
            paramCanvas.drawText(this.inderterminateText, 0.0F, i, localPaint);
            paramCanvas.restore();
          }
        }
        else
        {
          paramCanvas.drawBitmap(this.determinateBitmap, getDrawingArea(), this.canvasVisiableArea, null);
          if (this.showDeterminateFgImage)
          {
            paramCanvas.save();
            int m = this.canvasVisiableArea.left + (this.canvasVisiableArea.width() - this.determinateFgDrawable.getIntrinsicWidth()) / 2;
            int j = this.canvasVisiableArea.top + (this.canvasVisiableArea.height() - this.determinateFgDrawable.getIntrinsicHeight()) / 2;
            paramCanvas.translate(m, j);
            this.determinateFgDrawable.draw(paramCanvas);
            this.determinateFgDrawable.start();
            paramCanvas.restore();
          }
        }
        if ((!this.showDeterminateFgImage) && (this.determinateFgDrawable.isRunning()))
          this.determinateFgDrawable.stop();
      }
    }

    public int getThumbnailHeight()
    {
      return WallpaperView.this.mContainingBitmapNeedHeight * WallpaperView.this.getWidth() / WallpaperView.this.mContainingBitmapNeedWidth;
    }

    public void reset()
    {
      this.initialized = false;
      this.determinateFgDrawable.stop();
    }

    public void setBitmap(Bitmap paramBitmap)
    {
      if (this.determinateBitmap != paramBitmap)
      {
        this.horizontalRatio = 1.0F;
        this.verticalRatio = 1.0F;
        if ((paramBitmap != null) && ((paramBitmap.getWidth() != WallpaperView.this.mContainingBitmapNeedWidth) || (paramBitmap.getHeight() != WallpaperView.this.mContainingBitmapNeedHeight)))
        {
          this.horizontalRatio = (1.0F * paramBitmap.getWidth() / WallpaperView.this.mContainingBitmapNeedWidth);
          this.verticalRatio = (1.0F * paramBitmap.getHeight() / WallpaperView.this.mContainingBitmapNeedHeight);
          Log.i("decoder", "bitmap size is not match: (" + paramBitmap.getWidth() + ", " + paramBitmap.getHeight() + ")" + " needed: (" + WallpaperView.this.mContainingBitmapNeedWidth + ", " + WallpaperView.this.mContainingBitmapNeedHeight + ")");
        }
        this.determinateBitmap = paramBitmap;
        reset();
      }
    }

    public void setDrawingArea(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
      this.left = paramInt1;
      this.top = paramInt2;
      this.right = paramInt3;
      this.bottom = paramInt4;
      WallpaperView.this.invalidate();
    }

    public void udpateShowingArea(float paramFloat, boolean paramBoolean)
    {
      int i = (int)((WallpaperView.this.mContainingBitmapNeedWidth - getShowingWidth()) * (1.0F + paramFloat) / 2.0F);
      int j = this.left;
      if (!paramBoolean)
        horizontallyMoveOffset(i - j);
      else
        new MoveAnimation()
        {
          public void onMove(int paramInt)
          {
            WallpaperView.WallpaperBitmap.this.horizontallyMoveOffset(paramInt);
          }
        }
        .start(i - j);
    }

    class WallpaperHandler extends Handler
      implements Drawable.Callback
    {
      public WallpaperHandler()
      {
        if (getLooper() == Looper.getMainLooper())
          return;
        throw new RuntimeException("You must create WallpaperHander in main thread.");
      }

      public void invalidateDrawable(Drawable paramDrawable)
      {
        if (WallpaperView.WallpaperBitmap.this.canvasVisiableArea != null)
        {
          Rect localRect = paramDrawable.getBounds();
          int i = WallpaperView.WallpaperBitmap.this.canvasVisiableArea.left + (WallpaperView.WallpaperBitmap.this.canvasVisiableArea.width() - localRect.width()) / 2;
          int j = WallpaperView.WallpaperBitmap.this.canvasVisiableArea.top + (WallpaperView.WallpaperBitmap.this.canvasVisiableArea.height() - localRect.height()) / 2;
          WallpaperView.this.invalidate(i, j, i + localRect.width(), j + localRect.height());
        }
      }

      public void scheduleDrawable(Drawable paramDrawable, Runnable paramRunnable, long paramLong)
      {
        if ((paramDrawable != null) && (paramRunnable != null))
          postAtTime(paramRunnable, paramDrawable, paramLong);
      }

      public void unscheduleDrawable(Drawable paramDrawable, Runnable paramRunnable)
      {
        if ((paramDrawable != null) && (paramRunnable != null))
          removeCallbacks(paramRunnable, paramDrawable);
      }
    }
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     com.android.thememanager.view.WallpaperView
 * JD-Core Version:    0.6.0
 */