package com.android.thememanager.view;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.android.thememanager.activity.ThemeTabActivity;
import com.android.thememanager.util.ConstantsHelper;
import com.android.thememanager.util.ThemeHelper;
import com.android.thememanager.util.UIHelper;
import java.util.List;

public class ComponentCategoryView extends LinearLayout
{
  private FixedHeightGridView mGridView;
  private TextView mTitle;

  public ComponentCategoryView(Context paramContext)
  {
    this(paramContext, null);
  }

  public ComponentCategoryView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }

  public ComponentCategoryView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    init();
  }

  private void init()
  {
    if (this.mTitle == null)
    {
      this.mTitle = ((TextView)findViewById(2131165194));
      this.mGridView = ((FixedHeightGridView)findViewById(2131165195));
    }
  }

  public void refresh()
  {
    ((BaseAdapter)this.mGridView.getAdapter()).notifyDataSetInvalidated();
  }

  public void setComponentItems(List<Long> paramList)
  {
    init();
    if (this.mGridView.getAdapter() == null)
    {
      this.mGridView.setAdapter(new GridAdapter(getContext(), paramList, this.mGridView));
      this.mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener()
      {
        public void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
        {
          long l = ((Long)ComponentCategoryView.this.mGridView.getItemAtPosition(paramInt)).longValue();
          Intent localIntent = new Intent(ComponentCategoryView.this.getContext(), ThemeTabActivity.class);
          localIntent.putExtra("REQUEST_RESOURCE_TYPE", l);
          ComponentCategoryView.this.getContext().startActivity(localIntent);
        }
      });
      return;
    }
    throw new RuntimeException("GridView has been set adater.");
  }

  public void setTitle(int paramInt)
  {
    init();
    this.mTitle.setText(paramInt);
  }

  private class GridAdapter extends FixedHeightGridView.FixedGridAdapter
  {
    private Context mContext;
    private List<Long> mFlags;

    public GridAdapter(List<Long> paramFixedHeightGridView, FixedHeightGridView arg3)
    {
      super();
      this.mContext = paramFixedHeightGridView;
      Object localObject;
      this.mFlags = localObject;
    }

    public View getChildView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      View localView = paramView;
      if (localView == null)
        localView = LayoutInflater.from(this.mContext).inflate(2130903045, null);
      long l = ((Long)this.mFlags.get(paramInt)).longValue();
      ((TextView)localView.findViewById(2131165194)).setText(ConstantsHelper.getTitleResId(l));
      TextView localTextView = (TextView)localView.findViewById(2131165197);
      String str = ThemeHelper.loadUserPreferenceName(l);
      if (TextUtils.isEmpty(str))
        str = this.mContext.getString(2131427339);
      if (((UIHelper.isAudioResource(l)) || (UIHelper.isImageResource(l))) && (!TextUtils.equals(UIHelper.computeCurrentUsingPath(ComponentCategoryView.this.mContext, l), ThemeHelper.loadUserPreferencePath(l))))
        str = this.mContext.getString(2131427340);
      localTextView.setText(str);
      localTextView.setVisibility(0);
      return localView;
    }

    public int getCount()
    {
      return this.mFlags.size();
    }

    public Object getItem(int paramInt)
    {
      return this.mFlags.get(paramInt);
    }

    public long getItemId(int paramInt)
    {
      return paramInt;
    }
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     com.android.thememanager.view.ComponentCategoryView
 * JD-Core Version:    0.6.0
 */