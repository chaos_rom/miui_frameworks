package com.android.thememanager.view;

import android.app.ActionBar;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import com.android.thememanager.ThemeResourceConstants;
import com.android.thememanager.util.ThemeHelper;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import miui.app.screenelement.util.ConfigFile;
import miui.app.screenelement.util.Task;
import miui.app.screenelement.util.Utils;
import miui.content.res.ThemeResources;
import miui.content.res.ThemeResourcesSystem;
import miui.content.res.ThemeZipFile.ThemeFileInfo;
import miui.os.ExtraFileUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class LockscreenConfigSettings extends PreferenceActivity
  implements Preference.OnPreferenceChangeListener, Preference.OnPreferenceClickListener, ThemeResourceConstants
{
  private String mConfigPath;
  private int mNextRequestCode = 100;
  private HashMap<String, Item> mPreferenceMap = new HashMap();
  private HashMap<Integer, Object> mRequestCodeObjMap = new HashMap();
  private ConfigFile mSavedConfig;

  public static boolean containConfigFile(Context paramContext)
  {
    InputStream localInputStream = getConfigFileStreamFromCurrentTheme(paramContext);
    if (localInputStream != null);
    try
    {
      localInputStream.close();
      label13: for (int i = 1; ; i = 0)
        return i;
    }
    catch (IOException localIOException)
    {
      break label13;
    }
  }

  private void createGroup(PreferenceScreen paramPreferenceScreen, Element paramElement)
  {
    if (paramElement != null)
    {
      PreferenceCategory localPreferenceCategory = new PreferenceCategory(this);
      paramPreferenceScreen.addPreference(localPreferenceCategory);
      localPreferenceCategory.setTitle(paramElement.getAttribute("text"));
      localPreferenceCategory.setSummary(paramElement.getAttribute("summary"));
      XmlUtils.traverseElementChildren(paramElement, null, new LockscreenConfigSettings.XmlUtils.TraverseListener(localPreferenceCategory)
      {
        public void onChild(Element paramElement)
        {
          LockscreenConfigSettings.Item localItem = LockscreenConfigSettings.this.createItem(paramElement.getNodeName());
          if ((localItem != null) && (localItem.build(this.val$category, paramElement)))
            LockscreenConfigSettings.this.mPreferenceMap.put(localItem.mId, localItem);
        }
      });
    }
  }

  private Item createItem(String paramString)
  {
    Object localObject;
    if (!"StringInput".equals(paramString))
    {
      if (!"CheckBox".equals(paramString))
      {
        if (!"NumberInput".equals(paramString))
        {
          if (!"StringChoice".equals(paramString))
          {
            if (!"NumberChoice".equals(paramString))
            {
              if (!"AppPicker".equals(paramString))
                localObject = null;
              else
                localObject = new AppPickerItem(null);
            }
            else
              localObject = new NumberChoiceItem(null);
          }
          else
            localObject = new StringChoiceItem(null);
        }
        else
          localObject = new NumberInputItem(null);
      }
      else
        localObject = new CheckboxItem(null);
    }
    else
      localObject = new StringInputItem(null);
    return (Item)localObject;
  }

  private void createPreferenceScreen()
  {
    PreferenceScreen localPreferenceScreen = getPreferenceManager().createPreferenceScreen(this);
    setPreferenceScreen(localPreferenceScreen);
    Object localObject = DocumentBuilderFactory.newInstance();
    try
    {
      DocumentBuilder localDocumentBuilder = ((DocumentBuilderFactory)localObject).newDocumentBuilder();
      localObject = getConfigFileStreamFromCurrentTheme(this);
      if (localObject != null)
      {
        localObject = localDocumentBuilder.parse((InputStream)localObject).getDocumentElement();
        if ((localObject != null) && (((Element)localObject).getNodeName().equals("Config")))
          XmlUtils.traverseElementChildren((Element)localObject, "Group", new LockscreenConfigSettings.XmlUtils.TraverseListener(localPreferenceScreen)
          {
            public void onChild(Element paramElement)
            {
              LockscreenConfigSettings.this.createGroup(this.val$rootScreen, paramElement);
            }
          });
      }
    }
    catch (ParserConfigurationException localParserConfigurationException)
    {
      localParserConfigurationException.printStackTrace();
    }
    catch (SAXException localSAXException)
    {
      localSAXException.printStackTrace();
    }
    catch (IOException localIOException)
    {
      localIOException.printStackTrace();
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
  }

  private String getAppliedSDCardPath()
  {
    String str = ThemeHelper.loadUserPreferencePath(4096L);
    if (str != null)
      str = ThemeHelper.getLockstyleSDCardConfigPathFromThemePath(str);
    else
      str = null;
    return str;
  }

  private static InputStream getConfigFileStreamFromCurrentTheme(Context paramContext)
  {
    Object localObject = "config_" + ThemeHelper.getCurrentLanguage(paramContext) + ".xml";
    localObject = ThemeResources.getSystem().getAwesomeLockscreenFileStream((String)localObject);
    if (localObject == null)
      localObject = ThemeResources.getSystem().getAwesomeLockscreenFileStream("config.xml");
    if (localObject == null)
      localObject = null;
    else
      localObject = ((ThemeZipFile.ThemeFileInfo)localObject).mInput;
    return (InputStream)localObject;
  }

  private int getNextRequestCode()
  {
    int i = this.mNextRequestCode;
    this.mNextRequestCode = (i + 1);
    return i;
  }

  private Object getRequestCodeObj(int paramInt)
  {
    return this.mRequestCodeObjMap.get(Integer.valueOf(paramInt));
  }

  private void putRequestCodeObj(int paramInt, Object paramObject)
  {
    this.mRequestCodeObjMap.put(Integer.valueOf(paramInt), paramObject);
  }

  private void saveAndApply()
  {
    if ((this.mSavedConfig != null) && (this.mConfigPath != null))
    {
      this.mSavedConfig.save();
      File localFile = new File(getAppliedSDCardPath());
      if (!localFile.getParentFile().exists())
        ExtraFileUtils.mkdirs(localFile.getParentFile(), 509, -1, -1);
      this.mSavedConfig.save(localFile.getAbsolutePath());
    }
  }

  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    Object localObject = getRequestCodeObj(paramInt1);
    if ((localObject != null) && ((localObject instanceof AppPickerItem)) && (((AppPickerItem)localObject).onActivityResult(paramInt2, paramIntent)))
      saveAndApply();
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    if (containConfigFile(this))
    {
      setContentView(2130903046);
      getActionBar().setTitle(2131427371);
      getActionBar().setHomeButtonEnabled(true);
      this.mConfigPath = ThemeResources.sAppliedLockstyleConfigPath;
      this.mSavedConfig = new ConfigFile();
      this.mSavedConfig.load(this.mConfigPath);
      createPreferenceScreen();
      findViewById(2131165198).setVisibility(8);
    }
    else
    {
      finish();
    }
  }

  public void onDestroy()
  {
    saveAndApply();
    super.onDestroy();
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    if (paramMenuItem.getItemId() == 16908332)
      finish();
    return super.onOptionsItemSelected(paramMenuItem);
  }

  public void onPause()
  {
    super.onPause();
    saveAndApply();
  }

  public boolean onPreferenceChange(Preference paramPreference, Object paramObject)
  {
    Object localObject = paramPreference.getKey();
    localObject = (Item)this.mPreferenceMap.get(localObject);
    boolean bool;
    if (localObject != null)
    {
      bool = ((Item)localObject).OnValueChange(paramObject);
      if (bool)
        saveAndApply();
    }
    else
    {
      bool = false;
    }
    return bool;
  }

  public boolean onPreferenceClick(Preference paramPreference)
  {
    Object localObject = paramPreference.getKey();
    localObject = (Item)this.mPreferenceMap.get(localObject);
    boolean bool;
    if (localObject != null)
      bool = ((Item)localObject).onClick();
    else
      bool = false;
    return bool;
  }

  public static class XmlUtils
  {
    public static void traverseElementChildren(Element paramElement, String paramString, TraverseListener paramTraverseListener)
    {
      NodeList localNodeList = paramElement.getChildNodes();
      for (int i = 0; ; i++)
      {
        if (i >= localNodeList.getLength())
          return;
        Node localNode = localNodeList.item(i);
        if ((localNode.getNodeType() != 1) || ((paramString != null) && (!TextUtils.equals(localNode.getNodeName(), paramString))))
          continue;
        paramTraverseListener.onChild((Element)localNode);
      }
    }

    public static abstract interface TraverseListener
    {
      public abstract void onChild(Element paramElement);
    }
  }

  private class AppPickerItem extends LockscreenConfigSettings.Item
  {
    private int mRequestCode;

    private AppPickerItem()
    {
      super(null);
    }

    public boolean OnValueChange(Object paramObject)
    {
      Object localObject = (Task)paramObject;
      LockscreenConfigSettings.this.mSavedConfig.putTask((Task)localObject);
      if ((localObject == null) || (((Task)localObject).name == null))
        localObject = "";
      else
        localObject = ((Task)localObject).name;
      setValuePreview((String)localObject);
      return true;
    }

    protected Preference createPreference(Context paramContext)
    {
      return new Preference(paramContext);
    }

    public boolean onActivityResult(int paramInt, Intent paramIntent)
    {
      boolean bool;
      if (paramInt == -1)
      {
        Task localTask = new Task();
        localTask.id = this.mId;
        if (paramIntent == null)
        {
          localTask.name = null;
          localTask.packageName = null;
          localTask.className = null;
          localTask.action = null;
        }
        else
        {
          localTask.name = paramIntent.getStringExtra("name");
          localTask.packageName = paramIntent.getComponent().getPackageName();
          localTask.className = paramIntent.getComponent().getClassName();
          localTask.action = "android.intent.action.MAIN";
          Log.i("AppPickerItem", "selected component: " + localTask.packageName + " " + localTask.className);
        }
        bool = OnValueChange(localTask);
      }
      else
      {
        bool = false;
      }
      return bool;
    }

    protected void onBuild(Element paramElement)
    {
      this.mPreference.setOnPreferenceClickListener(LockscreenConfigSettings.this);
      this.mRequestCode = LockscreenConfigSettings.this.getNextRequestCode();
      LockscreenConfigSettings.this.putRequestCodeObj(this.mRequestCode, this);
    }

    public boolean onClick()
    {
      Intent localIntent = new Intent(LockscreenConfigSettings.this, ThirdAppPicker.class);
      LockscreenConfigSettings.this.startActivityForResult(localIntent, this.mRequestCode);
      return true;
    }

    public void updateValue()
    {
      Object localObject = LockscreenConfigSettings.this.mSavedConfig.getTask(this.mId);
      if (localObject == null)
        localObject = "";
      else
        localObject = ((Task)localObject).name;
      setValuePreview((String)localObject);
    }
  }

  private class NumberInputItem extends LockscreenConfigSettings.VariableItem
  {
    private NumberInputItem()
    {
      super(null);
    }

    private String getValueString(String paramString)
    {
      double d = 0.0D;
      try
      {
        d = Double.parseDouble(paramString);
        d = d;
        label11: return Utils.doubleToString(d);
      }
      catch (NumberFormatException localNumberFormatException)
      {
        break label11;
      }
    }

    public boolean OnValueChange(Object paramObject)
    {
      try
      {
        double d = Double.parseDouble((String)paramObject);
        String str = Utils.doubleToString(d);
        LockscreenConfigSettings.this.mSavedConfig.putNumber(this.mId, str);
        setValuePreview(str);
        i = 1;
        return i;
      }
      catch (NumberFormatException localNumberFormatException)
      {
        while (true)
          int i = 0;
      }
    }

    protected Preference createPreference(Context paramContext)
    {
      return new EditTextPreference(paramContext);
    }

    protected void setValue(String paramString)
    {
      String str = getValueString(paramString);
      ((EditTextPreference)this.mPreference).setText(str);
      setValuePreview(str);
    }
  }

  private class NumberChoiceItem extends LockscreenConfigSettings.ValueChoiceItem
  {
    private NumberChoiceItem()
    {
      super(null);
    }

    public boolean OnValueChange(Object paramObject)
    {
      super.OnValueChange(paramObject);
      LockscreenConfigSettings.this.mSavedConfig.putNumber(this.mId, (String)paramObject);
      return true;
    }
  }

  private class StringChoiceItem extends LockscreenConfigSettings.ValueChoiceItem
  {
    private StringChoiceItem()
    {
      super(null);
    }

    public boolean OnValueChange(Object paramObject)
    {
      super.OnValueChange(paramObject);
      LockscreenConfigSettings.this.mSavedConfig.putString(this.mId, (String)paramObject);
      return true;
    }
  }

  private abstract class ValueChoiceItem extends LockscreenConfigSettings.VariableItem
  {
    protected ArrayList<String> mItemsText = new ArrayList();
    protected ArrayList<String> mItemsValue = new ArrayList();

    private ValueChoiceItem()
    {
      super(null);
    }

    public boolean OnValueChange(Object paramObject)
    {
      String str = (String)paramObject;
      ((ListPreference)this.mPreference).setValue(str);
      setValuePreview(str);
      return true;
    }

    protected Preference createPreference(Context paramContext)
    {
      return new ListPreference(paramContext);
    }

    protected void onBuild(Element paramElement)
    {
      this.mItemsText.clear();
      this.mItemsValue.clear();
      LockscreenConfigSettings.XmlUtils.traverseElementChildren(paramElement, "Item", new LockscreenConfigSettings.XmlUtils.TraverseListener()
      {
        public void onChild(Element paramElement)
        {
          LockscreenConfigSettings.ValueChoiceItem.this.mItemsText.add(paramElement.getAttribute("text"));
          LockscreenConfigSettings.ValueChoiceItem.this.mItemsValue.add(paramElement.getAttribute("value"));
        }
      });
      ListPreference localListPreference = (ListPreference)this.mPreference;
      localListPreference.setEntries((CharSequence[])this.mItemsText.toArray(new String[this.mItemsText.size()]));
      localListPreference.setEntryValues((CharSequence[])this.mItemsValue.toArray(new String[this.mItemsValue.size()]));
    }

    protected void setValue(String paramString)
    {
      ListPreference localListPreference = (ListPreference)this.mPreference;
      int i = localListPreference.findIndexOfValue(paramString);
      if (i != -1)
      {
        localListPreference.setValueIndex(i);
        setValuePreview(paramString);
      }
    }
  }

  private class StringInputItem extends LockscreenConfigSettings.VariableItem
  {
    private StringInputItem()
    {
      super(null);
    }

    public boolean OnValueChange(Object paramObject)
    {
      String str = (String)paramObject;
      LockscreenConfigSettings.this.mSavedConfig.putString(this.mId, str);
      setValuePreview(str);
      return true;
    }

    protected Preference createPreference(Context paramContext)
    {
      return new EditTextPreference(paramContext);
    }

    protected void setValue(String paramString)
    {
      ((EditTextPreference)this.mPreference).setText(paramString);
      setValuePreview(paramString);
    }
  }

  private class CheckboxItem extends LockscreenConfigSettings.VariableItem
  {
    private CheckboxItem()
    {
      super(null);
    }

    public boolean OnValueChange(Object paramObject)
    {
      boolean bool = false;
      if (!(paramObject instanceof Boolean))
      {
        if ((paramObject instanceof String))
          bool = "1".equals((String)paramObject);
      }
      else
        bool = ((Boolean)paramObject).booleanValue();
      ConfigFile localConfigFile = LockscreenConfigSettings.this.mSavedConfig;
      String str1 = this.mId;
      String str2;
      if (!bool)
        str2 = "0";
      else
        str2 = "1";
      localConfigFile.putNumber(str1, str2);
      return true;
    }

    protected Preference createPreference(Context paramContext)
    {
      return new CheckBoxPreference(paramContext);
    }

    protected void setValue(String paramString)
    {
      ((CheckBoxPreference)this.mPreference).setChecked("1".equals(paramString));
    }
  }

  private abstract class VariableItem extends LockscreenConfigSettings.Item
  {
    private VariableItem()
    {
      super(null);
    }

    protected abstract void setValue(String paramString);

    public void updateValue()
    {
      String str = LockscreenConfigSettings.this.mSavedConfig.getVariable(this.mId);
      if (str == null)
      {
        setValue(this.mDefaultValue);
        OnValueChange(this.mDefaultValue);
      }
      else
      {
        setValue(str);
      }
    }
  }

  private abstract class Item
  {
    protected String mDefaultValue;
    protected String mId;
    protected Preference mPreference;
    protected String mSummary;
    protected String mTitle;

    private Item()
    {
    }

    public abstract boolean OnValueChange(Object paramObject);

    public final boolean build(PreferenceCategory paramPreferenceCategory, Element paramElement)
    {
      this.mId = paramElement.getAttribute("id");
      this.mTitle = paramElement.getAttribute("text");
      this.mSummary = paramElement.getAttribute("summary");
      this.mDefaultValue = paramElement.getAttribute("default");
      this.mPreference = createPreference(LockscreenConfigSettings.this);
      int i;
      if (this.mPreference != null)
      {
        paramPreferenceCategory.addPreference(this.mPreference);
        this.mPreference.setTitle(this.mTitle);
        this.mPreference.setSummary(this.mSummary);
        this.mPreference.setKey(this.mId);
        this.mPreference.setOnPreferenceChangeListener(LockscreenConfigSettings.this);
        onBuild(paramElement);
        updateValue();
        i = 1;
      }
      else
      {
        i = 0;
      }
      return i;
    }

    protected abstract Preference createPreference(Context paramContext);

    protected void onBuild(Element paramElement)
    {
    }

    public boolean onClick()
    {
      return false;
    }

    protected void setValuePreview(String paramString)
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append(paramString);
      if (!TextUtils.isEmpty(this.mSummary))
        localStringBuilder.append("\n").append(this.mSummary);
      this.mPreference.setSummary(localStringBuilder.toString());
    }

    public abstract void updateValue();
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     com.android.thememanager.view.LockscreenConfigSettings
 * JD-Core Version:    0.6.0
 */