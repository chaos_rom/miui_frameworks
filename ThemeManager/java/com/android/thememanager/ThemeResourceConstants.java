package com.android.thememanager;

import I;
import J;
import android.os.SystemProperties;
import miui.resourcebrowser.ResourceConstants;

public abstract interface ThemeResourceConstants extends ResourceConstants
{
  public static final String ASYNC_IMPORT_BOOT_AUDIO_PATH;
  public static final String ASYNC_IMPORT_GADGET_CLOCK_PATH;
  public static final String ASYNC_IMPORT_GADGET_PHOTO_FRAME_PATH;
  public static final String ASYNC_IMPORT_RESOURCE_PATH;
  public static final String ASYNC_IMPORT_THEME_PATH;
  public static final String BUILDIN_IMAGE_BOOT_AUDIO_PATH;
  public static final String BUILDIN_IMAGE_GADGET_CLOCK_PATH;
  public static final String BUILDIN_IMAGE_GADGET_PHOTO_FRAME_PATH;
  public static final String BUILDIN_IMAGE_RESOURCE_PATH;
  public static final String BUILDIN_IMAGE_THEME_PATH;
  public static final String[] COMPONENT_CODES;
  public static final String[] COMPONENT_IDENTITIES;
  public static final int[] COMPONENT_PLATFORMS;
  public static final String[] COMPONENT_PREVIEW_PREFIX;
  public static final long[] COMPONENT_PREVIEW_SHOW_ORDER;
  public static final long[] COMPONENT_SELECT_ORDER;
  public static final String[] COMPONENT_STAMPS;
  public static final int[] COMPONENT_TITLES;
  public static final String CONTENT_BOOT_AUDIO_PATH;
  public static final String CONTENT_GADGET_CLOCK_PATH;
  public static final String CONTENT_GADGET_PHOTO_FRAME_PATH;
  public static final String CONTENT_RESOURCE_PATH;
  public static final String CONTENT_THEME_PATH;
  public static final String DEFAULT_ALARM_FILE_PATH;
  public static final String DEFAULT_NOTIFICATION_FILE_PATH;
  public static final String DEFAULT_RINGTONE_FILE_PATH;
  public static final String DOWNLOADED_BOOT_AUDIO_PATH;
  public static final String DOWNLOADED_GADGET_CLOCK_PATH;
  public static final String DOWNLOADED_GADGET_PHOTO_FRAME_PATH;
  public static final String DOWNLOADED_RESOURCE_PATH;
  public static final String DOWNLOADED_THEME_PATH;
  public static final String INDEX_BOOT_AUDIO_PATH;
  public static final String INDEX_GADGET_CLOCK_PATH;
  public static final String INDEX_GADGET_PHOTO_FRAME_PATH;
  public static final String INDEX_RESOURCE_PATH;
  public static final String INDEX_THEME_PATH;
  public static final String META_BOOT_AUDIO_PATH;
  public static final String META_GADGET_CLOCK_PATH;
  public static final String META_GADGET_PHOTO_FRAME_PATH;
  public static final String META_RESOURCE_PATH;
  public static final String META_THEME_PATH;
  public static final String RIGHTS_BOOT_AUDIO_PATH;
  public static final String RIGHTS_GADGET_CLOCK_PATH;
  public static final String RIGHTS_GADGET_PHOTO_FRAME_PATH;
  public static final String RIGHTS_RESOURCE_PATH;
  public static final String RIGHTS_THEME_PATH;
  public static final String[] RUNTIME_PATHS;
  public static final String USER_BOOT_AUDIO_PATH;
  public static final String USER_GADGET_CLOCK_PATH;
  public static final String USER_GADGET_PHOTO_FRAME_PATH;
  public static final String USER_RESOURCE_PATH;
  public static final String USER_THEME_PATH;

  static
  {
    Object localObject = new String[19];
    localObject[0] = "framework-res";
    localObject[1] = "wallpaper/default_wallpaper.jpg";
    localObject[2] = "wallpaper/default_lock_wallpaper.jpg";
    localObject[3] = "icons";
    localObject[4] = "fonts/Roboto-Regular.ttf";
    localObject[5] = "boots/bootanimation.zip";
    localObject[6] = "boots/bootaudio.mp3";
    localObject[7] = "com.android.mms";
    localObject[8] = "ringtones/ringtone.mp3";
    localObject[9] = "ringtones/notification.mp3";
    localObject[10] = "ringtones/alarm.mp3";
    localObject[11] = "com.android.contacts";
    localObject[12] = "lockscreen";
    localObject[13] = "com.android.systemui";
    localObject[14] = "com.miui.home";
    localObject[15] = "audioeffect";
    localObject[16] = "clock_";
    localObject[17] = "photoframe_";
    localObject[18] = "fonts/DroidSansFallback.ttf";
    COMPONENT_IDENTITIES = (String)localObject;
    localObject = new String[19];
    localObject[0] = "/data/system/theme/framework-res";
    localObject[1] = "/data/data/com.android.settings/files/wallpaper";
    localObject[2] = "/data/system/theme/lock_wallpaper";
    localObject[3] = "/data/system/theme/icons";
    localObject[4] = "/data/system/theme/fonts/Roboto-Regular.ttf";
    localObject[5] = "/data/local/bootanimation.zip";
    localObject[6] = "/data/system/theme/boots/bootaudio.mp3";
    localObject[7] = "/data/system/theme/com.android.mms";
    localObject[8] = "/data/system/theme/ringtones/ringtone.mp3";
    localObject[9] = "/data/system/theme/ringtones/notification.mp3";
    localObject[10] = "/data/system/theme/ringtones/alarm.mp3";
    localObject[11] = "/data/system/theme/com.android.contacts";
    localObject[12] = "/data/system/theme/lockscreen";
    localObject[13] = "/data/system/theme/com.android.systemui";
    localObject[14] = "/data/system/theme/com.miui.home";
    localObject[15] = "/data/system/theme/audioeffect";
    localObject[16] = "/data/system/theme/clock_";
    localObject[17] = "/data/system/theme/photoframe_";
    localObject[18] = "/data/system/theme/fonts/DroidSansFallback.ttf";
    RUNTIME_PATHS = (String)localObject;
    localObject = new String[19];
    localObject[0] = "framework";
    localObject[1] = "wallpaper";
    localObject[2] = "lockscreen";
    localObject[3] = "icons";
    localObject[4] = "fonts";
    localObject[5] = "bootanimation";
    localObject[6] = "bootaudio";
    localObject[7] = "mms";
    localObject[8] = "ringtone";
    localObject[9] = "notification";
    localObject[10] = "alarm";
    localObject[11] = "contact";
    localObject[12] = "lockstyle";
    localObject[13] = "statusbar";
    localObject[14] = "launcher";
    localObject[15] = "audioeffect";
    localObject[16] = "clock";
    localObject[17] = "photoframe";
    localObject[18] = "fonts_fallback";
    COMPONENT_CODES = (String)localObject;
    localObject = new String[19];
    localObject[0] = "FrameWork";
    localObject[1] = "DeskWallpaper";
    localObject[2] = "LockScreenWallpaper";
    localObject[3] = "Icon";
    localObject[4] = "Font";
    localObject[5] = "BootAnimation";
    localObject[6] = "BootAudio";
    localObject[7] = "Mms";
    localObject[8] = "RingtoneAudio";
    localObject[9] = "NotificationAudio";
    localObject[10] = "AlarmAudio";
    localObject[11] = "Contact";
    localObject[12] = "LockStyle";
    localObject[13] = "StatusBar";
    localObject[14] = "Launcher";
    localObject[15] = "AudioEffect";
    localObject[16] = "Clock";
    localObject[17] = "PhotoFrame";
    localObject[18] = "FontFallback";
    COMPONENT_STAMPS = (String)localObject;
    localObject = new int[19];
    localObject[0] = 0;
    localObject[1] = 0;
    localObject[2] = 0;
    localObject[3] = 0;
    localObject[4] = 0;
    localObject[5] = 0;
    localObject[6] = 0;
    localObject[7] = 2;
    localObject[8] = 0;
    localObject[9] = 0;
    localObject[10] = 0;
    localObject[11] = 2;
    localObject[12] = 0;
    localObject[13] = 2;
    localObject[14] = 1;
    localObject[15] = 0;
    localObject[16] = 0;
    localObject[17] = 0;
    localObject[18] = 0;
    COMPONENT_PLATFORMS = (I)localObject;
    localObject = new int[19];
    localObject[0] = 2131427344;
    localObject[1] = 2131427346;
    localObject[2] = 2131427347;
    localObject[3] = 2131427348;
    localObject[4] = 2131427351;
    localObject[5] = 2131427349;
    localObject[6] = 2131427350;
    localObject[7] = 2131427345;
    localObject[8] = 2131427352;
    localObject[9] = 2131427353;
    localObject[10] = 2131427354;
    localObject[11] = 2131427355;
    localObject[12] = 2131427356;
    localObject[13] = 2131427357;
    localObject[14] = 2131427358;
    localObject[15] = 2131427359;
    localObject[16] = 2131427360;
    localObject[17] = 2131427361;
    localObject[18] = 2131427351;
    COMPONENT_TITLES = (I)localObject;
    localObject = new String[19];
    localObject[0] = "preview_";
    localObject[1] = null;
    localObject[2] = null;
    localObject[3] = "preview_icons_";
    localObject[4] = "preview_fonts_";
    localObject[5] = "preview_animation_";
    localObject[6] = null;
    localObject[7] = "preview_mms_";
    localObject[8] = null;
    localObject[9] = null;
    localObject[10] = null;
    localObject[11] = "preview_contact_";
    localObject[12] = "preview_lockscreen_";
    localObject[13] = "preview_statusbar_";
    localObject[14] = "preview_launcher_";
    localObject[15] = null;
    localObject[16] = "preview_clock_";
    localObject[17] = "preview_photo_frame_";
    localObject[18] = null;
    COMPONENT_PREVIEW_PREFIX = (String)localObject;
    localObject = new long[12];
    localObject[0] = 4096L;
    localObject[1] = 16384L;
    localObject[2] = 1L;
    localObject[3] = 8192L;
    localObject[4] = 2048L;
    localObject[5] = 128L;
    localObject[6] = 8L;
    localObject[7] = 65536L;
    localObject[8] = 131072L;
    localObject[9] = 32L;
    localObject[10] = 16L;
    localObject[11] = 32768L;
    COMPONENT_PREVIEW_SHOW_ORDER = (J)localObject;
    localObject = new long[18];
    localObject[0] = 1L;
    localObject[1] = 32L;
    localObject[2] = 64L;
    localObject[3] = 4096L;
    localObject[4] = 4L;
    localObject[5] = 8192L;
    localObject[6] = 8L;
    localObject[7] = 16384L;
    localObject[8] = 65536L;
    localObject[9] = 131072L;
    localObject[10] = 2L;
    localObject[11] = 16L;
    localObject[12] = 128L;
    localObject[13] = 2048L;
    localObject[14] = 256L;
    localObject[15] = 512L;
    localObject[16] = 1024L;
    localObject[17] = 32768L;
    COMPONENT_SELECT_ORDER = (J)localObject;
    USER_RESOURCE_PATH = USER_ROOT_PATH + "theme/.db/meta/";
    USER_THEME_PATH = USER_ROOT_PATH + "theme/.db/meta/theme/";
    USER_GADGET_CLOCK_PATH = USER_ROOT_PATH + "theme/.db/meta/clock_%s/";
    USER_GADGET_PHOTO_FRAME_PATH = USER_ROOT_PATH + "theme/.db/meta/photo_frame_%s/";
    USER_BOOT_AUDIO_PATH = USER_ROOT_PATH + "ringtone/";
    DOWNLOADED_RESOURCE_PATH = DOWNLOADED_ROOT_PATH + "theme/";
    DOWNLOADED_THEME_PATH = DOWNLOADED_ROOT_PATH + "theme/";
    DOWNLOADED_GADGET_CLOCK_PATH = DOWNLOADED_ROOT_PATH + "theme/";
    DOWNLOADED_GADGET_PHOTO_FRAME_PATH = DOWNLOADED_ROOT_PATH + "theme/";
    DOWNLOADED_BOOT_AUDIO_PATH = DOWNLOADED_ROOT_PATH + "ringtone/";
    META_RESOURCE_PATH = META_ROOT_PATH + "theme/.db/meta/theme/";
    META_THEME_PATH = META_ROOT_PATH + "theme/.db/meta/theme/";
    META_GADGET_CLOCK_PATH = META_ROOT_PATH + "theme/.db/meta/theme/";
    META_GADGET_PHOTO_FRAME_PATH = META_ROOT_PATH + "theme/.db/meta/theme/";
    META_BOOT_AUDIO_PATH = META_ROOT_PATH + "ringtone/";
    CONTENT_RESOURCE_PATH = CONTENT_ROOT_PATH + "theme/.db/content/theme/";
    CONTENT_THEME_PATH = CONTENT_ROOT_PATH + "theme/.db/content/theme/";
    CONTENT_GADGET_CLOCK_PATH = CONTENT_ROOT_PATH + "theme/.db/content/theme/";
    CONTENT_GADGET_PHOTO_FRAME_PATH = CONTENT_ROOT_PATH + "theme/.db/content/theme/";
    CONTENT_BOOT_AUDIO_PATH = CONTENT_ROOT_PATH + "ringtone/";
    RIGHTS_RESOURCE_PATH = RIGHTS_ROOT_PATH + "theme/.db/rights/theme/";
    RIGHTS_THEME_PATH = RIGHTS_ROOT_PATH + "theme/.db/rights/theme/";
    RIGHTS_GADGET_CLOCK_PATH = RIGHTS_ROOT_PATH + "theme/.db/rights/theme/";
    RIGHTS_GADGET_PHOTO_FRAME_PATH = RIGHTS_ROOT_PATH + "theme/.db/rights/theme/";
    RIGHTS_BOOT_AUDIO_PATH = RIGHTS_ROOT_PATH + "ringtone/";
    BUILDIN_IMAGE_RESOURCE_PATH = BUILDIN_IMAGE_ROOT_PATH + "theme/.db/preview/theme/";
    BUILDIN_IMAGE_THEME_PATH = BUILDIN_IMAGE_ROOT_PATH + "theme/.db/preview/theme/";
    BUILDIN_IMAGE_GADGET_CLOCK_PATH = BUILDIN_IMAGE_ROOT_PATH + "theme/.db/preview/theme/";
    BUILDIN_IMAGE_GADGET_PHOTO_FRAME_PATH = BUILDIN_IMAGE_ROOT_PATH + "theme/.db/preview/theme/";
    BUILDIN_IMAGE_BOOT_AUDIO_PATH = BUILDIN_IMAGE_ROOT_PATH + "ringtone/";
    INDEX_RESOURCE_PATH = INDEX_ROOT_PATH + "theme/.db/index/theme/";
    INDEX_THEME_PATH = INDEX_ROOT_PATH + "theme/.db/index/theme/";
    INDEX_GADGET_CLOCK_PATH = INDEX_ROOT_PATH + "theme/.db/index/theme/";
    INDEX_GADGET_PHOTO_FRAME_PATH = INDEX_ROOT_PATH + "theme/.db/index/theme/";
    INDEX_BOOT_AUDIO_PATH = INDEX_ROOT_PATH + "ringtone/";
    ASYNC_IMPORT_RESOURCE_PATH = ASYNC_IMPORT_ROOT_PATH + "theme/.db/import/theme/";
    ASYNC_IMPORT_THEME_PATH = ASYNC_IMPORT_ROOT_PATH + "theme/.db/import/theme/";
    ASYNC_IMPORT_GADGET_CLOCK_PATH = ASYNC_IMPORT_ROOT_PATH + "theme/.db/import/theme/";
    ASYNC_IMPORT_GADGET_PHOTO_FRAME_PATH = ASYNC_IMPORT_ROOT_PATH + "theme/.db/import/theme/";
    ASYNC_IMPORT_BOOT_AUDIO_PATH = ASYNC_IMPORT_ROOT_PATH + "ringtone/";
    DEFAULT_RINGTONE_FILE_PATH = "/system/media/audio/ringtones/" + SystemProperties.get("ro.config.ringtone");
    DEFAULT_NOTIFICATION_FILE_PATH = "/system/media/audio/notifications/" + SystemProperties.get("ro.config.notification_sound");
    DEFAULT_ALARM_FILE_PATH = "/system/media/audio/alarms/" + SystemProperties.get("ro.config.alarm_alert");
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     com.android.thememanager.ThemeResourceConstants
 * JD-Core Version:    0.6.0
 */