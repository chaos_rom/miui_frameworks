package com.android.thememanager.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.os.PowerManager;
import com.android.thememanager.ThemeResourceConstants;
import miui.content.res.ThemeResources;
import miui.content.res.ThemeResourcesSystem;
import miui.resourcebrowser.ResourceContext;
import miui.resourcebrowser.model.Resource;

public class ThemeApplyUtils
  implements ThemeResourceConstants
{
  public static void applyResource(Activity paramActivity, ResourceContext paramResourceContext, Resource paramResource, ThemeApplyParameters paramThemeApplyParameters)
  {
    int i = 0;
    int j = 1;
    long l1 = ((Long)paramResourceContext.getExtraMeta("EXTRA_CTX_RESOURCE_TYPE", Long.valueOf(-1L))).longValue();
    String str1 = paramResource.getExtraMeta("modulesFlag");
    long l2;
    if (str1 == null)
      l2 = 0L;
    else
      l2 = Long.parseLong(l2);
    String str2 = paramResource.getLocalPath();
    boolean bool;
    if (l1 != 2L)
    {
      if (l1 != 4L)
      {
        if (l1 != 64L)
        {
          if (l1 != 256L)
          {
            if (l1 != 512L)
            {
              if (l1 != 1024L)
              {
                long l3 = paramThemeApplyParameters.applyFlags;
                if (!ThemeHelper.supportReplaceFont())
                  l3 &= -262161L;
                if (!ThemeHelper.supportReplaceAudioEffect())
                  l3 &= -32769L;
                l3 = l3;
                long l4;
                if (!paramThemeApplyParameters.removeOthers)
                  l4 = l3;
                else
                  l4 = l3 | 0xFFFFF8FF;
                if (ThemeHelper.getComponentNumber(l3) > 0)
                {
                  if (!ThemeHelper.needsFontChange(l4, str2, l2))
                  {
                    j = 0;
                    applyTheme(paramActivity, paramResourceContext, paramResource, l4, l3, null);
                  }
                  else
                  {
                    AlertDialog.Builder localBuilder = new AlertDialog.Builder(paramActivity);
                    localBuilder.setMessage(2131427384).setIconAttribute(16843605).setPositiveButton(2131427385, new DialogInterface.OnClickListener(paramActivity, paramResourceContext, paramResource, l4, l3)
                    {
                      public void onClick(DialogInterface paramDialogInterface, int paramInt)
                      {
                        1 local1 = new Runnable()
                        {
                          public void run()
                          {
                            ((PowerManager)ThemeApplyUtils.this.getSystemService("power")).reboot("font files change");
                          }
                        };
                        ThemeApplyUtils.access$000(ThemeApplyUtils.this, this.val$resContext, this.val$resource, this.val$removeFlags, this.val$applyFlags, local1);
                      }
                    }).setNegativeButton(17039360, null);
                    if (l1 != 16L)
                      localBuilder.setNeutralButton(2131427386, new DialogInterface.OnClickListener(paramActivity, paramResourceContext, paramResource, l4, l3)
                      {
                        public void onClick(DialogInterface paramDialogInterface, int paramInt)
                        {
                          ThemeApplyUtils.access$000(ThemeApplyUtils.this, this.val$resContext, this.val$resource, 0xFFFBFFEF & this.val$removeFlags, 0xFFFBFFEF & this.val$applyFlags, null);
                        }
                      });
                    localBuilder.show();
                    return;
                  }
                }
                else
                {
                  new AlertDialog.Builder(paramActivity).setMessage(2131427364).setIconAttribute(16843605).setPositiveButton(17039370, null).show();
                  return;
                }
              }
              else
              {
                bool = ThemeHelper.setRingtone(paramActivity, 4, str2);
              }
            }
            else
              bool = ThemeHelper.setRingtone(paramActivity, 2, str2);
          }
          else
            bool = ThemeHelper.setRingtone(paramActivity, 1, str2);
        }
        else
          bool = ThemeHelper.applyBootAudio(paramActivity, str2);
      }
      else
      {
        j = 0;
        giveTipForLockscreenPaper(paramActivity, l1, str2);
      }
    }
    else
    {
      j = 0;
      WallpaperUtils.cropAndApplyWallpaper(paramActivity, l1, str2, false, false);
    }
    if (j != 0)
      ThemeHelper.showThemeChangedToast(paramActivity, bool);
  }

  private static void applyTheme(Context paramContext, ResourceContext paramResourceContext, Resource paramResource, long paramLong1, long paramLong2, Runnable paramRunnable)
  {
    ApplyThemeTask localApplyThemeTask = new ApplyThemeTask(paramContext, paramResourceContext, paramResource, paramLong1, paramLong2);
    if (paramRunnable != null)
      localApplyThemeTask.setPostRunnable(paramRunnable);
    localApplyThemeTask.execute(new Void[0]);
  }

  private static void giveTipForLockscreenPaper(Activity paramActivity, long paramLong, String paramString)
  {
    if ((paramLong != 4L) || (!ThemeResources.getSystem().hasAwesomeLockscreen()))
      WallpaperUtils.cropAndApplyWallpaper(paramActivity, 4L, paramString, false, false);
    else
      new AlertDialog.Builder(paramActivity).setMessage(2131427365).setIconAttribute(16843605).setPositiveButton(17039370, null).show().setOnDismissListener(new DialogInterface.OnDismissListener(paramActivity, paramString)
      {
        public void onDismiss(DialogInterface paramDialogInterface)
        {
          WallpaperUtils.cropAndApplyWallpaper(ThemeApplyUtils.this, 4L, this.val$localPath, false, false);
        }
      });
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     com.android.thememanager.util.ThemeApplyUtils
 * JD-Core Version:    0.6.0
 */