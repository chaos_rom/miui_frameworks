package com.android.thememanager.util;

import android.os.Handler;
import android.os.Message;

public abstract class MoveAnimation
{
  public void onFinish(int paramInt)
  {
  }

  public abstract void onMove(int paramInt);

  public void start(int paramInt)
  {
    start(paramInt, 10, 5);
  }

  public void start(int paramInt1, int paramInt2, int paramInt3)
  {
    new Handler(paramInt1, paramInt2, paramInt3)
    {
      private int mAnimCnt = 0;

      public void handleMessage(Message paramMessage)
      {
        int i;
        do
        {
          this.mAnimCnt = (1 + this.mAnimCnt);
          i = this.val$moveOffset * this.mAnimCnt / this.val$animMostCnt - this.val$moveOffset * (-1 + this.mAnimCnt) / this.val$animMostCnt;
        }
        while ((i == 0) && (this.mAnimCnt < this.val$animMostCnt));
        if (this.mAnimCnt <= this.val$animMostCnt)
        {
          if (i != 0)
            MoveAnimation.this.onMove(i);
          sendEmptyMessageDelayed(0, this.val$animInternal);
        }
        else
        {
          MoveAnimation.this.onFinish(this.val$moveOffset);
        }
      }
    }
    .sendEmptyMessage(0);
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     com.android.thememanager.util.MoveAnimation
 * JD-Core Version:    0.6.0
 */