package com.android.thememanager.util;

import android.app.Activity;
import android.app.Fragment;
import android.app.WallpaperManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory.Options;
import android.net.Uri;
import android.util.DisplayMetrics;
import android.util.Pair;
import com.android.thememanager.ThemeResourceConstants;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import miui.util.ImageUtils;
import miui.util.InputStreamLoader;

public class WallpaperUtils
  implements ThemeResourceConstants
{
  private static Uri sLastCropWallpaperUri;

  public static void cropAndApplyWallpaper(Activity paramActivity, long paramLong, String paramString, boolean paramBoolean1, boolean paramBoolean2)
  {
    cropAndApplyWallpaper(paramActivity, null, paramLong, Uri.parse("file://" + paramString), paramBoolean1, paramBoolean2);
  }

  public static boolean cropAndApplyWallpaper(Activity paramActivity, Fragment paramFragment, long paramLong, Uri paramUri, boolean paramBoolean1, boolean paramBoolean2)
  {
    int i2;
    label124: Object localObject;
    label215: int i;
    if (!paramBoolean1)
      if (paramLong == 4L)
      {
        boolean bool1 = true;
        Pair localPair = getWallpaperExpectedSize(paramActivity, bool1);
        int k = ((Integer)localPair.first).intValue();
        int m = ((Integer)localPair.second).intValue();
        int n = (int)(1.1F * k);
        int i1 = (int)(1.1F * m);
        boolean bool4 = paramBoolean2;
        if (!bool4)
        {
          InputStreamLoader localInputStreamLoader = new InputStreamLoader(paramActivity, paramUri);
          BitmapFactory.Options localOptions = ImageUtils.getBitmapSize(localInputStreamLoader);
          localInputStreamLoader.close();
          if ((localOptions.outWidth <= n) && (localOptions.outHeight <= i1))
            break label264;
          i2 = 1;
        }
        if (i2 == 0)
          break label307;
        Intent localIntent = new Intent("com.android.camera.action.CROP").setClassName("com.miui.gallery", "com.miui.gallery.app.CropImage").setDataAndType(paramUri, "image/*").putExtra("outputX", k).putExtra("outputY", m).putExtra("aspectX", k).putExtra("aspectY", m).putExtra("scale", true).putExtra("isInitFullSelection", true).putExtra("noFaceDetection", true).putExtra("is-large-image", true);
        if (!bool1)
          break label270;
        localObject = "set-as-lockscreen";
        localObject = localIntent.putExtra((String)localObject, true);
        if (paramFragment == null)
          break label285;
        if (!bool1)
          break label277;
        i = 28674;
      }
    while (true)
    {
      try
      {
        paramFragment.startActivityForResult((Intent)localObject, i);
        sLastCropWallpaperUri = paramUri;
        i = 0;
        return i;
        i = 0;
        break;
        label264: i2 = 0;
        break label124;
        label270: localObject = "set-as-wallpaper";
        break label215;
        label277: i = 28673;
        continue;
        label285: if (i == 0)
          break label426;
        i = 28674;
        paramActivity.startActivityForResult((Intent)localObject, i);
        continue;
      }
      catch (Exception localException)
      {
      }
      label307: i = 1;
      boolean bool3 = true;
      boolean bool2;
      if ((0x4 & paramLong) != 0L)
        bool2 = saveLockWallpaperByDisplay(paramActivity, null, paramUri);
      if ((0x2 & paramLong) != 0L)
        bool3 = saveDeskWallpaperByDisplay(paramActivity, null, paramUri);
      if (!bool2)
        ThemeHelper.showThemeChangedToast(paramActivity, false, paramActivity.getString(ConstantsHelper.getTitleResId(4L)));
      if (!bool3)
        ThemeHelper.showThemeChangedToast(paramActivity, false, paramActivity.getString(ConstantsHelper.getTitleResId(2L)));
      if ((bool2) && (bool3))
        ThemeHelper.showThemeChangedToast(paramActivity, true);
      if ((bool3) && (bool2))
      {
        bool2 = true;
        continue;
        label426: j = 28673;
        continue;
      }
      int j = 0;
    }
  }

  public static void dealCropWallpaperResult(Context paramContext, int paramInt1, int paramInt2)
  {
    if ((paramInt2 == -1) && ((paramInt1 == 28674) || (paramInt1 == 28673)))
    {
      long l;
      if (paramInt1 != 28674)
        l = 2L;
      else
        l = 4L;
      String str;
      if (sLastCropWallpaperUri == null)
        str = "";
      else
        str = sLastCropWallpaperUri.getPath();
      ThemeHelper.saveUserPreference(l, str, paramContext.getString(2131427340));
      ThemeHelper.showThemeChangedToast(paramContext, true);
    }
  }

  private static Pair<Integer, Integer> getWallpaperExpectedSize(Context paramContext, boolean paramBoolean)
  {
    DisplayMetrics localDisplayMetrics = ThemeHelper.getScreenDisplayMetrics(paramContext);
    int j = localDisplayMetrics.widthPixels;
    int k;
    if (!paramBoolean)
      k = 2;
    else
      k = 1;
    j *= k;
    int i = localDisplayMetrics.heightPixels;
    return new Pair(Integer.valueOf(j), Integer.valueOf(i));
  }

  public static boolean saveDeskWallpaperByDisplay(Context paramContext, Bitmap paramBitmap, Uri paramUri)
  {
    return saveWallpaperByDisplay(paramContext, paramBitmap, paramUri, false);
  }

  public static boolean saveLockWallpaperByDisplay(Context paramContext, Bitmap paramBitmap, Uri paramUri)
  {
    return saveWallpaperByDisplay(paramContext, paramBitmap, paramUri, true);
  }

  private static boolean saveWallpaperByDisplay(Context paramContext, Bitmap paramBitmap, Uri paramUri, boolean paramBoolean)
  {
    Pair localPair = getWallpaperExpectedSize(paramContext, paramBoolean);
    int i = ((Integer)localPair.first).intValue();
    int j = ((Integer)localPair.second).intValue();
    Object localObject1 = null;
    if (!paramBoolean)
      localObject1 = (WallpaperManager)paramContext.getSystemService("wallpaper");
    boolean bool = false;
    Object localObject3;
    if (paramBitmap != null)
      localObject3 = null;
    try
    {
      if ((paramBitmap.getWidth() >= i) && (paramBitmap.getHeight() >= j))
      {
        localObject3 = ImageUtils.scaleBitmapToDesire(paramBitmap, i, j, false);
        break label335;
        if (paramBoolean)
        {
          bool = ImageUtils.saveToFile((Bitmap)localObject3, "/data/system/theme/lock_wallpaper");
          if ((localObject3 != null) && (localObject3 != paramBitmap))
            ((Bitmap)localObject3).recycle();
        }
        while (true)
        {
          Object localObject2;
          if (bool)
          {
            if (paramBoolean)
              ThemeHelper.updateLockWallpaperInfo(paramContext, paramUri.getPath());
          }
          else
          {
            return bool;
            localObject2 = new ByteArrayOutputStream();
            ((Bitmap)localObject3).compress(Bitmap.CompressFormat.JPEG, 100, (OutputStream)localObject2);
            ((WallpaperManager)localObject1).setStream(new ByteArrayInputStream(((ByteArrayOutputStream)localObject2).toByteArray()));
            ((ByteArrayOutputStream)localObject2).close();
            bool = true;
            break;
            if (paramUri == null)
              continue;
            if (paramBoolean)
            {
              bool = ImageUtils.saveBitmapToLocal(new InputStreamLoader(paramContext, paramUri), "/data/system/theme/lock_wallpaper", localObject2, j);
              continue;
            }
            localObject3 = new File(paramContext.getFilesDir(), "tmp_desk_wallpaper");
            ImageUtils.saveBitmapToLocal(new InputStreamLoader(paramContext, paramUri), ((File)localObject3).getAbsolutePath(), localObject2, j);
            if (!((File)localObject3).exists())
              continue;
          }
          try
          {
            localObject2 = new FileInputStream((File)localObject3);
            ((WallpaperManager)localObject1).setStream((InputStream)localObject2);
            ((FileInputStream)localObject2).close();
            bool = true;
            label295: ((File)localObject3).delete();
            continue;
            localObject1 = paramContext.getString(2131427340);
            ThemeHelper.saveUserPreference(2L, paramUri.getPath(), (String)localObject1);
          }
          catch (Exception localException1)
          {
            break label295;
          }
        }
      }
    }
    catch (Exception localException2)
    {
      while (true)
      {
        continue;
        label335: if (localObject3 != null)
          continue;
        localObject3 = paramBitmap;
      }
    }
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     com.android.thememanager.util.WallpaperUtils
 * JD-Core Version:    0.6.0
 */