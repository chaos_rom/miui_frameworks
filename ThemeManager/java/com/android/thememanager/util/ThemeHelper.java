package com.android.thememanager.util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ApplicationInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.media.ExtraRingtoneManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.FileUtils;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import android.widget.Toast;
import com.android.thememanager.ThemeResourceConstants;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import miui.content.res.ThemeResources;
import miui.os.ExtraFileUtils;
import miui.os.Shell;
import miui.resourcebrowser.model.Resource;
import miui.resourcebrowser.util.ResourceHelper;
import miui.util.ImageUtils;
import miui.util.InputStreamLoader;

public class ThemeHelper
  implements ThemeResourceConstants
{
  public static final String[] GADGET_SIZE_ARRAY;
  private static final double LOG2 = Math.log(2.0D);
  public static final int THEME_FLAG_COUNT;
  private static Map<String, String> sAudioEffectMap;
  private static List<String> sAudioEffectOrder;
  private static Set<String> sDisablePackagesReplaceSet;
  private static SharedPreferences.Editor sEditor;
  private static boolean sPlatformSupportReplaceFont;
  private static SharedPreferences sSharedPreference;

  static
  {
    String[] arrayOfString = new String[4];
    arrayOfString[0] = "1x2";
    arrayOfString[1] = "2x2";
    arrayOfString[2] = "2x4";
    arrayOfString[3] = "4x4";
    GADGET_SIZE_ARRAY = arrayOfString;
    THEME_FLAG_COUNT = 1 + getComponentIndex(262144L);
    sPlatformSupportReplaceFont = true;
    sDisablePackagesReplaceSet = new HashSet();
    sAudioEffectMap = new HashMap();
    sAudioEffectOrder = new ArrayList();
  }

  public static boolean applyBootAudio(Context paramContext, String paramString)
  {
    String str = paramString;
    if (TextUtils.isEmpty(paramString))
    {
      localFile = new File(paramContext.getFilesDir(), "silent_bootaudio");
      if (!localFile.exists())
        ResourceHelper.writeTo(new ByteArrayInputStream(new byte[0]), localFile.getAbsolutePath());
      str = localFile.getAbsolutePath();
    }
    File localFile = new File("/data/system/theme/boots/bootaudio.mp3");
    localFile.delete();
    try
    {
      ResourceHelper.writeTo(new FileInputStream(str), localFile.getAbsolutePath());
      label84: boolean bool = localFile.exists();
      if (bool)
        saveUserPreference(64L, paramString, paramContext.getString(2131427340));
      return bool;
    }
    catch (Exception localException)
    {
      break label84;
    }
  }

  // ERROR //
  public static void applyDeskWallpaperOfThemeFile(Context paramContext, String paramString)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_3
    //   2: new 91	java/io/File
    //   5: dup
    //   6: aload_1
    //   7: invokespecial 126	java/io/File:<init>	(Ljava/lang/String;)V
    //   10: astore_2
    //   11: aload_2
    //   12: invokevirtual 106	java/io/File:exists	()Z
    //   15: ifeq +34 -> 49
    //   18: new 131	java/io/FileInputStream
    //   21: dup
    //   22: aload_2
    //   23: invokespecial 150	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   26: astore_3
    //   27: aload_0
    //   28: ldc 152
    //   30: invokevirtual 156	android/content/Context:getSystemService	(Ljava/lang/String;)Ljava/lang/Object;
    //   33: checkcast 158	android/app/WallpaperManager
    //   36: aload_3
    //   37: invokevirtual 162	android/app/WallpaperManager:setStream	(Ljava/io/InputStream;)V
    //   40: aload_3
    //   41: ifnull +7 -> 48
    //   44: aload_3
    //   45: invokevirtual 167	java/io/InputStream:close	()V
    //   48: return
    //   49: aload_0
    //   50: invokevirtual 171	android/content/Context:getResources	()Landroid/content/res/Resources;
    //   53: ldc 172
    //   55: invokevirtual 178	android/content/res/Resources:openRawResource	(I)Ljava/io/InputStream;
    //   58: astore_3
    //   59: ldc2_w 179
    //   62: ldc 182
    //   64: invokestatic 185	com/android/thememanager/util/ThemeHelper:saveUserPreference	(JLjava/lang/String;)V
    //   67: goto -40 -> 27
    //   70: pop
    //   71: aload_3
    //   72: ifnull -24 -> 48
    //   75: aload_3
    //   76: invokevirtual 167	java/io/InputStream:close	()V
    //   79: goto -31 -> 48
    //   82: astore_2
    //   83: aload_2
    //   84: invokevirtual 188	java/io/IOException:printStackTrace	()V
    //   87: goto -39 -> 48
    //   90: astore_2
    //   91: aload_2
    //   92: invokevirtual 188	java/io/IOException:printStackTrace	()V
    //   95: goto -47 -> 48
    //   98: astore_2
    //   99: aload_3
    //   100: ifnull +7 -> 107
    //   103: aload_3
    //   104: invokevirtual 167	java/io/InputStream:close	()V
    //   107: aload_2
    //   108: athrow
    //   109: astore_3
    //   110: aload_3
    //   111: invokevirtual 188	java/io/IOException:printStackTrace	()V
    //   114: goto -7 -> 107
    //
    // Exception table:
    //   from	to	target	type
    //   2	40	70	java/lang/Exception
    //   49	67	70	java/lang/Exception
    //   75	79	82	java/io/IOException
    //   44	48	90	java/io/IOException
    //   2	40	98	finally
    //   49	67	98	finally
    //   103	107	109	java/io/IOException
  }

  public static void applyLockWallpaperOfThemeFile(Context paramContext, String paramString)
  {
    new File("/data/system/theme/lock_wallpaper").delete();
    if (!new File(paramString).exists())
    {
      saveUserPreference(4L, "");
    }
    else
    {
      DisplayMetrics localDisplayMetrics = getScreenDisplayMetrics(paramContext);
      ImageUtils.saveBitmapToLocal(new InputStreamLoader(paramString), "/data/system/theme/lock_wallpaper", localDisplayMetrics.widthPixels, localDisplayMetrics.heightPixels);
      FileUtils.setPermissions("/data/system/theme/lock_wallpaper", 509, -1, -1);
    }
  }

  public static void applyRingtone(Context paramContext, int paramInt, String paramString)
  {
    if (!new File(paramString).exists())
      if (paramInt != 1)
      {
        if (paramInt != 2)
        {
          if (paramInt == 4)
            paramString = DEFAULT_ALARM_FILE_PATH;
        }
        else
          paramString = DEFAULT_NOTIFICATION_FILE_PATH;
      }
      else
        paramString = DEFAULT_RINGTONE_FILE_PATH;
    setRingtone(paramContext, paramInt, paramString);
  }

  public static String avoidNullString(String paramString)
  {
    if (paramString == null)
      paramString = "Undefined";
    return paramString;
  }

  private static void clearUserPreference(long paramLong)
  {
    sEditor.remove("path-" + paramLong);
    sEditor.remove("name-" + paramLong);
    sEditor.remove("time-" + paramLong);
  }

  public static void createRuntimeFolder(Context paramContext)
  {
    if (!new File("/data/system/theme/").exists())
      Shell.mkdirs("/data/system/theme/");
    int i = paramContext.getApplicationInfo().uid;
    Shell.chown("/data/system/theme/", i, i);
    ExtraFileUtils.mkdirs(new File(DOWNLOADED_THEME_PATH), 511, -1, -1);
  }

  public static long getAllComponentsCombineFlag()
  {
    return 524287L;
  }

  public static long getCompatibleFlag(int paramInt, long paramLong)
  {
    long l = 1L;
    while (true)
    {
      if (l > 262144L)
        return paramLong;
      if (paramInt < ConstantsHelper.getPlatform(l))
        paramLong &= (0xFFFFFFFF ^ l);
      l <<= 1;
    }
  }

  public static int getComponentIndex(long paramLong)
  {
    int i;
    if (paramLong != -1L)
      i = (int)(0.1D + Math.log(paramLong) / LOG2);
    else
      i = 0;
    return i;
  }

  public static int getComponentNumber(long paramLong)
  {
    int i = 0;
    if (paramLong != -1L)
      while (true)
      {
        if (paramLong == 0L)
        {
          i = i;
          break;
        }
        paramLong &= paramLong - 1L;
        i++;
      }
    i = 1 + THEME_FLAG_COUNT;
    return i;
  }

  public static String getCurrentLanguage(Context paramContext)
  {
    return paramContext.getResources().getConfiguration().locale.getLanguage();
  }

  public static String getLockstyleAppliedConfigFilePath()
  {
    return ThemeResources.sAppliedLockstyleConfigPath;
  }

  public static String getLockstyleSDCardConfigPathFromThemePath(String paramString)
  {
    String str;
    if (paramString != null)
    {
      int j = paramString.lastIndexOf("/");
      int i = paramString.lastIndexOf(".");
      if (i > j)
        str = paramString.substring(j + 1, i);
      else
        str = paramString.substring(j + 1);
      str = DOWNLOADED_THEME_PATH + File.separator + ".lockstyle_config" + File.separator + str + ".config";
    }
    else
    {
      str = null;
    }
    return str;
  }

  public static String getNameForAudioEffect(String paramString)
  {
    String str = paramString.substring(1 + paramString.lastIndexOf(File.separatorChar));
    if (sAudioEffectMap.containsKey(str))
      str = (String)sAudioEffectMap.get(str);
    if (str.indexOf('.') > 0)
      str = str.substring(0, str.lastIndexOf('.'));
    return str;
  }

  public static String getResourceId(Resource paramResource)
  {
    String str = paramResource.getOnlineId();
    if ((str == null) || (TextUtils.isEmpty(str)))
      str = getResourceIdFromPath(paramResource.getLocalPath());
    return str;
  }

  public static String getResourceIdFromPath(String paramString)
  {
    Object localObject = null;
    if (paramString != null)
    {
      String str2 = ResourceHelper.getFileName(paramString);
      int i = str2.lastIndexOf(".");
      if (i != -1)
      {
        String str1 = str2.substring(0, i);
        if ((str1.matches("[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}")) || (str1.matches("default")))
          localObject = str1;
      }
    }
    return localObject;
  }

  public static DisplayMetrics getScreenDisplayMetrics(Context paramContext)
  {
    DisplayMetrics localDisplayMetrics = Resources.getSystem().getDisplayMetrics();
    if ((localDisplayMetrics.widthPixels == 0) || (localDisplayMetrics.heightPixels == 0))
      localDisplayMetrics = new DisplayMetrics();
    try
    {
      ((Activity)paramContext).getWindowManager().getDefaultDisplay().getMetrics(localDisplayMetrics);
      label45: return localDisplayMetrics;
    }
    catch (Exception localException)
    {
      break label45;
    }
  }

  public static long identifyComponents(ZipFile paramZipFile)
  {
    return identifyComponents(paramZipFile, -1);
  }

  public static long identifyComponents(ZipFile paramZipFile, int paramInt)
  {
    long l = 0L;
    for (int i = 1; i < THEME_FLAG_COUNT; i++)
    {
      if (paramZipFile.getEntry(COMPONENT_IDENTITIES[i]) == null)
        continue;
      l |= 1 << i;
    }
    Enumeration localEnumeration = paramZipFile.entries();
    Object localObject;
    if (localEnumeration.hasMoreElements())
    {
      ZipEntry localZipEntry = (ZipEntry)localEnumeration.nextElement();
      localObject = localZipEntry.getName();
      int k = 0;
      label80: if (k < THEME_FLAG_COUNT)
      {
        if (((l & 1L << k) != 0L) || (!((String)localObject).startsWith(COMPONENT_IDENTITIES[k])))
          break label147;
        if ((k != 0) || (!localZipEntry.isDirectory()))
          break label136;
      }
      while (true)
      {
        k++;
        break label80;
        break;
        label136: l |= 1 << k;
        continue;
        label147: if (!((String)localObject).startsWith("com.android.launcher"))
          continue;
        l |= 16384L;
      }
    }
    if (paramInt < 0)
    {
      localObject = ResourceHelper.getDescription(paramZipFile, "description.xml");
      if (localObject == null)
        break label212;
    }
    try
    {
      int j = Integer.parseInt((String)((Map)localObject).get("uiVersion"));
      label212: for (paramInt = j; ; paramInt = 0)
        return getCompatibleFlag(paramInt, l);
    }
    catch (NumberFormatException localNumberFormatException)
    {
      while (true)
        paramInt = 0;
    }
  }

  private static void initAudioEffectMap(Resources paramResources)
  {
    CharSequence[] arrayOfCharSequence2 = paramResources.getTextArray(2131099648);
    CharSequence[] arrayOfCharSequence1 = paramResources.getTextArray(2131099649);
    if ((arrayOfCharSequence2 != null) && (arrayOfCharSequence1 != null) && (arrayOfCharSequence2.length == arrayOfCharSequence1.length))
      sAudioEffectMap.clear();
    for (int i = 0; ; i++)
    {
      if (i >= arrayOfCharSequence2.length)
        return;
      sAudioEffectMap.put(arrayOfCharSequence2[i].toString(), arrayOfCharSequence1[i].toString());
      sAudioEffectOrder.add(arrayOfCharSequence2[i].toString());
    }
  }

  private static void initPlatformSupport(Context paramContext)
  {
    Object localObject = paramContext.getResources();
    sPlatformSupportReplaceFont = ((Resources)localObject).getBoolean(2131296256);
    localObject = ((Resources)localObject).getTextArray(2131099650);
    for (int i = 0; ; i++)
    {
      if (i >= localObject.length)
        return;
      sDisablePackagesReplaceSet.add(localObject[i].toString());
    }
  }

  public static void initResource(Context paramContext)
  {
    initSharedPreference(paramContext);
    initPlatformSupport(paramContext);
    initAudioEffectMap(paramContext.getResources());
  }

  private static void initSharedPreference(Context paramContext)
  {
    sSharedPreference = PreferenceManager.getDefaultSharedPreferences(paramContext);
    sEditor = sSharedPreference.edit();
  }

  public static boolean isDisablePkgName(String paramString)
  {
    return sDisablePackagesReplaceSet.contains(paramString);
  }

  public static boolean isWallpaperPrefOlderThanSystem(Context paramContext, long paramLong)
  {
    File localFile = null;
    if (paramLong != 2L)
    {
      if (paramLong == 4L)
        localFile = new File("/data/system/theme/lock_wallpaper");
    }
    else
      localFile = new File("/data/data/com.android.settings/files/wallpaper");
    int i;
    if ((localFile == null) || (loadUserPreferenceTime(paramLong) >= localFile.lastModified()))
      i = 0;
    else
      i = 1;
    return i;
  }

  public static String loadUserPreferenceName(long paramLong)
  {
    return sSharedPreference.getString("name-" + paramLong, "");
  }

  public static String loadUserPreferencePath(long paramLong)
  {
    return sSharedPreference.getString("path-" + paramLong, "");
  }

  public static long loadUserPreferenceTime(long paramLong)
  {
    return sSharedPreference.getLong("time-" + paramLong, 0L);
  }

  // ERROR //
  public static boolean needsFontChange(long paramLong1, String paramString, long paramLong2)
  {
    // Byte code:
    //   0: ldc2_w 575
    //   3: lload_0
    //   4: land
    //   5: ldc2_w 319
    //   8: lcmp
    //   9: ifeq +55 -> 64
    //   12: iconst_1
    //   13: istore 5
    //   15: iload 5
    //   17: ifeq +44 -> 61
    //   20: new 91	java/io/File
    //   23: dup
    //   24: ldc_w 578
    //   27: invokespecial 126	java/io/File:<init>	(Ljava/lang/String;)V
    //   30: astore 6
    //   32: ldc2_w 575
    //   35: lload_3
    //   36: land
    //   37: ldc2_w 319
    //   40: lcmp
    //   41: ifeq +13 -> 54
    //   44: ldc_w 580
    //   47: aload_2
    //   48: invokevirtual 583	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   51: ifeq +19 -> 70
    //   54: aload 6
    //   56: invokevirtual 106	java/io/File:exists	()Z
    //   59: istore 5
    //   61: iload 5
    //   63: ireturn
    //   64: iconst_0
    //   65: istore 5
    //   67: goto -52 -> 15
    //   70: getstatic 588	com/android/thememanager/util/BackupThemeTask:BACKUP_THEME_PATH	Ljava/lang/String;
    //   73: aload_2
    //   74: invokevirtual 583	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   77: ifeq -16 -> 61
    //   80: aload 6
    //   82: invokevirtual 106	java/io/File:exists	()Z
    //   85: ifne -24 -> 61
    //   88: aconst_null
    //   89: astore 6
    //   91: new 447	java/util/zip/ZipFile
    //   94: dup
    //   95: getstatic 588	com/android/thememanager/util/BackupThemeTask:BACKUP_THEME_PATH	Ljava/lang/String;
    //   98: invokespecial 589	java/util/zip/ZipFile:<init>	(Ljava/lang/String;)V
    //   101: astore 6
    //   103: aload 6
    //   105: invokevirtual 455	java/util/zip/ZipFile:entries	()Ljava/util/Enumeration;
    //   108: astore 7
    //   110: aload 7
    //   112: invokeinterface 460 1 0
    //   117: ifeq +32 -> 149
    //   120: aload 7
    //   122: invokeinterface 464 1 0
    //   127: checkcast 466	java/util/zip/ZipEntry
    //   130: invokevirtual 469	java/util/zip/ZipEntry:getName	()Ljava/lang/String;
    //   133: ldc_w 591
    //   136: invokevirtual 472	java/lang/String:startsWith	(Ljava/lang/String;)Z
    //   139: istore 8
    //   141: iload 8
    //   143: ifeq -33 -> 110
    //   146: iconst_1
    //   147: istore 5
    //   149: aload 6
    //   151: ifnull -90 -> 61
    //   154: aload 6
    //   156: invokevirtual 592	java/util/zip/ZipFile:close	()V
    //   159: goto -98 -> 61
    //   162: astore 6
    //   164: aload 6
    //   166: invokevirtual 188	java/io/IOException:printStackTrace	()V
    //   169: goto -108 -> 61
    //   172: pop
    //   173: aload 6
    //   175: ifnull -114 -> 61
    //   178: aload 6
    //   180: invokevirtual 592	java/util/zip/ZipFile:close	()V
    //   183: goto -122 -> 61
    //   186: astore 6
    //   188: aload 6
    //   190: invokevirtual 188	java/io/IOException:printStackTrace	()V
    //   193: goto -132 -> 61
    //   196: astore 5
    //   198: aload 6
    //   200: ifnull +8 -> 208
    //   203: aload 6
    //   205: invokevirtual 592	java/util/zip/ZipFile:close	()V
    //   208: aload 5
    //   210: athrow
    //   211: astore 6
    //   213: aload 6
    //   215: invokevirtual 188	java/io/IOException:printStackTrace	()V
    //   218: goto -10 -> 208
    //   221: astore 5
    //   223: aload 6
    //   225: astore 6
    //   227: goto -29 -> 198
    //   230: pop
    //   231: aload 6
    //   233: astore 6
    //   235: goto -62 -> 173
    //
    // Exception table:
    //   from	to	target	type
    //   154	159	162	java/io/IOException
    //   91	103	172	java/lang/Exception
    //   178	183	186	java/io/IOException
    //   91	103	196	finally
    //   203	208	211	java/io/IOException
    //   103	141	221	finally
    //   103	141	230	java/lang/Exception
  }

  public static void saveUserPreference(long paramLong, String paramString)
  {
    saveUserPreference(paramLong, paramString, null);
  }

  public static void saveUserPreference(long paramLong, String paramString1, String paramString2)
  {
    synchronized (sSharedPreference)
    {
      clearUserPreference(paramLong);
      sEditor.putString("path-" + paramLong, paramString1);
      if (paramString2 != null)
        sEditor.putString("name-" + paramLong, paramString2);
      sEditor.putLong("time-" + paramLong, System.currentTimeMillis());
      sEditor.commit();
      return;
    }
  }

  public static void saveUserPreference(Resource paramResource, long paramLong1, long paramLong2)
  {
    SharedPreferences localSharedPreferences = sSharedPreference;
    monitorenter;
    long l1 = 1L;
    while (true)
    {
      if ((l1 > 262144L) || ((l1 & paramLong1) != 0L));
      label103: long l2;
      try
      {
        setUserPreference(paramResource, l1);
        break label103;
        if ((l1 & paramLong2) != 0L)
          clearUserPreference(l1);
      }
      finally
      {
        monitorexit;
        throw localObject;
        if ((0x1 & (paramLong1 | paramLong2)) != 0L)
          setUserPreference(paramResource, -1L);
        sEditor.commit();
        monitorexit;
      }
    }
  }

  public static void saveUserPreferenceTime(long paramLong1, long paramLong2)
  {
    synchronized (sSharedPreference)
    {
      sEditor.putLong("time-" + paramLong1, paramLong2);
      sEditor.commit();
      return;
    }
  }

  public static void setMusicVolumeType(Activity paramActivity, long paramLong)
  {
    int i;
    if (paramLong != 256L)
    {
      if (paramLong != 512L)
      {
        if (paramLong != 1024L)
        {
          if (paramLong != 32768L)
            i = 3;
          else
            i = 1;
        }
        else
          i = 4;
      }
      else
        i = 5;
    }
    else
      i = 2;
    if (i >= 0)
      paramActivity.setVolumeControlStream(i);
  }

  public static boolean setRingtone(Context paramContext, int paramInt, String paramString)
  {
    Uri localUri = null;
    if (!TextUtils.isEmpty(paramString))
    {
      localUri = Uri.fromFile(new File(paramString));
      ExtraRingtoneManager.copyRingtone(paramString, paramInt);
    }
    RingtoneManager.setActualDefaultRingtoneUri(paramContext, paramInt, localUri);
    long l = 256L;
    switch (paramInt)
    {
    case 1:
      l = 256L;
      break;
    case 2:
      l = 512L;
      break;
    case 4:
      l = 1024L;
    case 3:
    }
    saveUserPreference(l, paramString, paramContext.getString(2131427340));
    return true;
  }

  private static void setUserPreference(Resource paramResource, long paramLong)
  {
    sEditor.putString("path-" + paramLong, paramResource.getFilePath());
    sEditor.putString("name-" + paramLong, paramResource.getTitle());
    sEditor.putLong("time-" + paramLong, System.currentTimeMillis());
  }

  public static void showThemeChangedToast(Context paramContext, boolean paramBoolean)
  {
    showThemeChangedToast(paramContext, paramBoolean, "");
  }

  public static void showThemeChangedToast(Context paramContext, boolean paramBoolean, String paramString)
  {
    int i;
    if (!paramBoolean)
      i = 2131427332;
    else
      i = 2131427331;
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = paramString;
    Toast.makeText(paramContext, paramContext.getString(i, arrayOfObject), 1).show();
  }

  public static boolean supportReplaceAudioEffect()
  {
    return false;
  }

  public static boolean supportReplaceFont()
  {
    return sPlatformSupportReplaceFont;
  }

  public static void updateLockWallpaperInfo(Context paramContext, String paramString)
  {
    FileUtils.setPermissions("/data/system/theme/lock_wallpaper", 509, -1, -1);
    saveUserPreference(4L, paramString, paramContext.getString(2131427340));
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     com.android.thememanager.util.ThemeHelper
 * JD-Core Version:    0.6.0
 */