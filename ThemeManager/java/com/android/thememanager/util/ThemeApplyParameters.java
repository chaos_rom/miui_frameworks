package com.android.thememanager.util;

import java.io.Serializable;

public class ThemeApplyParameters
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  public long applyFlags;
  public boolean removeOthers;
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     com.android.thememanager.util.ThemeApplyParameters
 * JD-Core Version:    0.6.0
 */