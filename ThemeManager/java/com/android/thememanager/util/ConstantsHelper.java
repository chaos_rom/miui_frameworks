package com.android.thememanager.util;

import com.android.thememanager.ThemeResourceConstants;
import java.util.HashMap;
import java.util.Map;

public class ConstantsHelper
  implements ThemeResourceConstants
{
  private static Map<String, Long> sCodeTypeMap = new HashMap();
  private static Map<String, Long> sIdentityTypeMap = new HashMap();

  static
  {
    sCodeTypeMap.put("framework", Long.valueOf(1L));
    sCodeTypeMap.put("bootanimation", Long.valueOf(32L));
    sCodeTypeMap.put("bootaudio", Long.valueOf(64L));
    sCodeTypeMap.put("ringtone", Long.valueOf(256L));
    sCodeTypeMap.put("notification", Long.valueOf(512L));
    sCodeTypeMap.put("alarm", Long.valueOf(1024L));
    sCodeTypeMap.put("wallpaper", Long.valueOf(2L));
    sCodeTypeMap.put("lockscreen", Long.valueOf(4L));
    sCodeTypeMap.put("lockstyle", Long.valueOf(4096L));
    sCodeTypeMap.put("fonts", Long.valueOf(16L));
    sCodeTypeMap.put("fonts_fallback", Long.valueOf(262144L));
    sCodeTypeMap.put("icons", Long.valueOf(8L));
    sCodeTypeMap.put("launcher", Long.valueOf(16384L));
    sCodeTypeMap.put("statusbar", Long.valueOf(8192L));
    sCodeTypeMap.put("contact", Long.valueOf(2048L));
    sCodeTypeMap.put("mms", Long.valueOf(128L));
    sCodeTypeMap.put("audioeffect", Long.valueOf(32768L));
    sCodeTypeMap.put("clock", Long.valueOf(65536L));
    sCodeTypeMap.put("photoframe", Long.valueOf(131072L));
    sCodeTypeMap.put("theme", Long.valueOf(-1L));
    sIdentityTypeMap.put("framework-res", Long.valueOf(1L));
    sIdentityTypeMap.put("boots/bootanimation.zip", Long.valueOf(32L));
    sIdentityTypeMap.put("boots/bootaudio.mp3", Long.valueOf(64L));
    sIdentityTypeMap.put("ringtones/ringtone.mp3", Long.valueOf(256L));
    sIdentityTypeMap.put("ringtones/notification.mp3", Long.valueOf(512L));
    sIdentityTypeMap.put("ringtones/alarm.mp3", Long.valueOf(1024L));
    sIdentityTypeMap.put("wallpaper/default_wallpaper.jpg", Long.valueOf(2L));
    sIdentityTypeMap.put("wallpaper/default_lock_wallpaper.jpg", Long.valueOf(4L));
    sIdentityTypeMap.put("lockscreen", Long.valueOf(4096L));
    sIdentityTypeMap.put("fonts/Roboto-Regular.ttf", Long.valueOf(16L));
    sIdentityTypeMap.put("fonts/DroidSansFallback.ttf", Long.valueOf(262144L));
    sIdentityTypeMap.put("icons", Long.valueOf(8L));
    sIdentityTypeMap.put("com.miui.home", Long.valueOf(16384L));
    sIdentityTypeMap.put("com.android.systemui", Long.valueOf(8192L));
    sIdentityTypeMap.put("com.android.contacts", Long.valueOf(2048L));
    sIdentityTypeMap.put("com.android.mms", Long.valueOf(128L));
    sIdentityTypeMap.put("audioeffect", Long.valueOf(32768L));
    sIdentityTypeMap.put("clock_", Long.valueOf(65536L));
    sIdentityTypeMap.put("photoframe_", Long.valueOf(131072L));
  }

  public static int getPlatform(long paramLong)
  {
    int i;
    if (paramLong != -1L)
    {
      if ((1L > paramLong) || (paramLong > 262144L))
        i = 0;
      else
        i = COMPONENT_PLATFORMS[ThemeHelper.getComponentIndex(paramLong)];
    }
    else
      i = 2;
    return i;
  }

  public static String getPreviewPrefix(long paramLong)
  {
    String str;
    if (paramLong != -1L)
    {
      if ((1L > paramLong) || (paramLong > 262144L))
        str = "";
      else
        str = COMPONENT_PREVIEW_PREFIX[ThemeHelper.getComponentIndex(paramLong)];
    }
    else
      str = "";
    return str;
  }

  public static String getResourceCode(long paramLong)
  {
    String str;
    if (paramLong != -1L)
    {
      if ((1L > paramLong) || (paramLong > 262144L))
        str = "";
      else
        str = COMPONENT_CODES[ThemeHelper.getComponentIndex(paramLong)];
    }
    else
      str = "theme";
    return str;
  }

  public static String getResourceIdentity(long paramLong)
  {
    String str;
    if (paramLong != -1L)
    {
      if ((1L > paramLong) || (paramLong > 262144L))
        str = "";
      else
        str = COMPONENT_IDENTITIES[ThemeHelper.getComponentIndex(paramLong)];
    }
    else
      str = "";
    return str;
  }

  public static String getResourceRuntimePath(long paramLong)
  {
    String str;
    if (paramLong != -1L)
    {
      if ((1L > paramLong) || (paramLong > 262144L))
        str = "";
      else
        str = RUNTIME_PATHS[ThemeHelper.getComponentIndex(paramLong)];
    }
    else
      str = "";
    return str;
  }

  public static String getResourceStamp(long paramLong)
  {
    String str;
    if (paramLong != -1L)
    {
      if ((1L > paramLong) || (paramLong > 262144L))
        str = "";
      else
        str = COMPONENT_STAMPS[ThemeHelper.getComponentIndex(paramLong)];
    }
    else
      str = "Compound";
    return str;
  }

  public static Long getResourceTypeByCode(String paramString)
  {
    return (Long)sCodeTypeMap.get(paramString);
  }

  public static Long getResourceTypeByIdentity(String paramString)
  {
    return (Long)sIdentityTypeMap.get(paramString);
  }

  public static int getTitleResId(long paramLong)
  {
    int i;
    if (paramLong != -1L)
    {
      if (paramLong != 268435456L)
      {
        if ((1L > paramLong) || (paramLong > 262144L))
          i = 0;
        else
          i = COMPONENT_TITLES[ThemeHelper.getComponentIndex(paramLong)];
      }
      else
        i = 2131427362;
    }
    else
      i = 2131427363;
    return i;
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     com.android.thememanager.util.ConstantsHelper
 * JD-Core Version:    0.6.0
 */