package com.android.thememanager.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import java.util.HashMap;
import java.util.Map;
import miui.analytics.XiaomiAnalytics;
import miui.resourcebrowser.ResourceContext;
import miui.resourcebrowser.model.Resource;
import miui.resourcebrowser.view.ResourceOperationHandler;
import miui.resourcebrowser.view.ResourceOperationView;

public class ThemeOperationHandler extends ResourceOperationHandler
{
  private ThemeApplyParameters mApplyParams;
  private XiaomiAnalytics mXiaomiAnalytics = XiaomiAnalytics.getInstance();

  public ThemeOperationHandler(Context paramContext, ResourceContext paramResourceContext, ResourceOperationView paramResourceOperationView)
  {
    super(paramContext, paramResourceContext, paramResourceOperationView);
  }

  private void dataAnalyticsOnApply()
  {
    HashMap localHashMap = new HashMap();
    String str3 = ThemeHelper.getResourceId(this.mResource);
    String str2 = this.mResource.getTitle();
    Object localObject = "" + this.mApplyParams.applyFlags;
    String str1 = "" + ConstantsHelper.getResourceTypeByCode(this.mResContext.getResourceCode());
    localHashMap.put("theme_id", ThemeHelper.avoidNullString(str3));
    localHashMap.put("theme_name", ThemeHelper.avoidNullString(str2));
    localHashMap.put("apply_modules", ThemeHelper.avoidNullString((String)localObject));
    localHashMap.put("apply_entry", ThemeHelper.avoidNullString(str1));
    localObject = XiaomiAnalytics.getInstance();
    ((XiaomiAnalytics)localObject).startSession(this.mContext);
    ((XiaomiAnalytics)localObject).trackEvent("apply_theme", localHashMap);
    ((XiaomiAnalytics)localObject).endSession();
  }

  private void dataAnalyticsOnDownload()
  {
    if (this.mResource != null)
    {
      HashMap localHashMap = new HashMap();
      localHashMap.put("theme_id", ThemeHelper.avoidNullString(ThemeHelper.getResourceId(this.mResource)));
      localHashMap.put("theme_name", ThemeHelper.avoidNullString(this.mResource.getTitle()));
      this.mXiaomiAnalytics.startSession(this.mContext);
      this.mXiaomiAnalytics.trackEvent("download_request", localHashMap);
      this.mXiaomiAnalytics.endSession();
    }
  }

  private void dataAnalyticsOnDownloadSuccessful(String paramString)
  {
    if (this.mResource != null)
    {
      HashMap localHashMap = new HashMap();
      localHashMap.put("theme_id", ThemeHelper.avoidNullString(paramString));
      this.mXiaomiAnalytics.startSession(this.mContext);
      this.mXiaomiAnalytics.trackEvent("download_request_finish", localHashMap);
      this.mXiaomiAnalytics.endSession();
    }
  }

  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if ((paramInt1 != 28673) && (paramInt1 != 28674))
      super.onActivityResult(paramInt1, paramInt2, paramIntent);
    else
      WallpaperUtils.dealCropWallpaperResult(this.mContext, paramInt1, paramInt2);
  }

  public void onDownloadEventPerformed()
  {
    dataAnalyticsOnDownload();
    super.onDownloadEventPerformed();
  }

  public void onDownloadSuccessful(String paramString1, String paramString2)
  {
    dataAnalyticsOnDownloadSuccessful(paramString2);
    super.onDownloadSuccessful(paramString1, paramString2);
  }

  protected void onRealApplyResourceEvent()
  {
    if (isLegal())
    {
      ThemeApplyUtils.applyResource((Activity)this.mContext, this.mResContext, this.mResource, this.mApplyParams);
      dataAnalyticsOnApply();
    }
  }

  public void setApplyParameters(ThemeApplyParameters paramThemeApplyParameters)
  {
    this.mApplyParams = paramThemeApplyParameters;
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     com.android.thememanager.util.ThemeOperationHandler
 * JD-Core Version:    0.6.0
 */