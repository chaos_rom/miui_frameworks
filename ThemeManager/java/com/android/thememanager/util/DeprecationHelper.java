package com.android.thememanager.util;

import com.android.thememanager.ThemeResourceConstants;
import java.io.File;
import miui.os.Shell;

public class DeprecationHelper
  implements ThemeResourceConstants
{
  public static void deleteDeprecatedPreviewCache()
  {
    String str = MIUI_PATH + ".cache/ThemeManager";
    if (new File(str).exists())
      Shell.remove(str);
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     com.android.thememanager.util.DeprecationHelper
 * JD-Core Version:    0.6.0
 */