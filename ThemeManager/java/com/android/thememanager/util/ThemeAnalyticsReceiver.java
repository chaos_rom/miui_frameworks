package com.android.thememanager.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build.VERSION;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import miui.analytics.XiaomiAnalytics;
import miui.os.Build;

public class ThemeAnalyticsReceiver extends BroadcastReceiver
{
  private String getScreenType(WindowManager paramWindowManager)
  {
    Object localObject = new DisplayMetrics();
    Display localDisplay = paramWindowManager.getDefaultDisplay();
    localDisplay.getMetrics((DisplayMetrics)localObject);
    int i = localDisplay.getRotation();
    if ((i != 0) && (i != 2))
      localObject = ((DisplayMetrics)localObject).widthPixels + "*" + ((DisplayMetrics)localObject).heightPixels;
    else
      localObject = ((DisplayMetrics)localObject).heightPixels + "*" + ((DisplayMetrics)localObject).widthPixels;
    return (String)localObject;
  }

  private String getThemeId(SharedPreferences paramSharedPreferences)
  {
    return ThemeHelper.getResourceIdFromPath(ThemeHelper.loadUserPreferencePath(-1L));
  }

  private String getThemeNum()
  {
    Object localObject = "Undefined";
    int i = 0;
    File localFile1 = new File("/data/system/theme/");
    int j;
    if ((localFile1.exists()) && (localFile1.isDirectory()))
    {
      localObject = localFile1.list();
      if (localObject != null)
        j = localObject.length;
    }
    for (int m = 0; ; m++)
    {
      if (m >= j)
      {
        localObject = "" + i;
        File localFile2 = new File("/system/media/theme/.data/meta/theme/");
        if ((localFile2.exists()) && (localFile2.isDirectory()))
        {
          localObject = localFile2.list();
          if (localObject != null)
            m = localObject.length;
        }
        for (int k = 0; ; k++)
        {
          if (k >= m)
          {
            localObject = "" + i;
            return localObject;
          }
          if (!localObject[k].endsWith(".mtz"))
            continue;
          i++;
        }
      }
      if (!localObject[m].endsWith(".mtz"))
        continue;
      i++;
    }
  }

  public void onReceive(Context paramContext, Intent paramIntent)
  {
    XiaomiAnalytics localXiaomiAnalytics = XiaomiAnalytics.getInstance();
    Object localObject1 = (WindowManager)paramContext.getSystemService("window");
    Object localObject2 = PreferenceManager.getDefaultSharedPreferences(paramContext);
    String str1 = "MIUI-" + Build.VERSION.INCREMENTAL + " " + Build.ID;
    localObject1 = getScreenType((WindowManager)localObject1);
    localObject2 = getThemeId((SharedPreferences)localObject2);
    String str3 = ThemeHelper.loadUserPreferenceName(-1L);
    String str2 = getThemeNum();
    HashMap localHashMap = new HashMap();
    localHashMap.put("os_type", ThemeHelper.avoidNullString(str1));
    localHashMap.put("screen_type", ThemeHelper.avoidNullString((String)localObject1));
    localHashMap.put("theme_id", ThemeHelper.avoidNullString((String)localObject2));
    localHashMap.put("theme_name", ThemeHelper.avoidNullString(str3));
    localHashMap.put("theme_num", ThemeHelper.avoidNullString(str2));
    localXiaomiAnalytics.startSession(paramContext);
    localXiaomiAnalytics.trackEvent("daily_data", localHashMap);
    localXiaomiAnalytics.endSession();
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     com.android.thememanager.util.ThemeAnalyticsReceiver
 * JD-Core Version:    0.6.0
 */