package com.android.thememanager.util;

import android.app.ActivityManagerNative;
import android.app.IActivityManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.FileUtils;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.provider.Settings.System;
import android.view.View;
import android.view.Window;
import com.android.thememanager.ThemeResourceConstants;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import miui.content.res.ExtraConfiguration;
import miui.content.res.IconCustomizer;
import miui.content.res.IconCustomizer.CustomizedIconsListener;
import miui.content.res.ThemeResources;
import miui.content.res.ThemeResourcesSystem;
import miui.os.ExtraFileUtils;
import miui.os.Shell;
import miui.resourcebrowser.ResourceContext;
import miui.resourcebrowser.controller.local.LocalJSONDataParser;
import miui.resourcebrowser.controller.local.PersistenceException;
import miui.resourcebrowser.model.RelatedResource;
import miui.resourcebrowser.model.Resource;
import miui.util.CommandLineUtils;

public class ApplyThemeTask extends AsyncTask<Void, Void, Void>
  implements ThemeResourceConstants
{
  private long mApplyFlags;
  private Context mContext;
  private Runnable mFinishRunnable;
  private long mModuleFlags;
  private ProgressDialog mProgress;
  private long mRemoveFlags;
  private ResourceContext mResContext;
  private Resource mResource;

  public ApplyThemeTask(Context paramContext, ResourceContext paramResourceContext, Resource paramResource, long paramLong1, long paramLong2)
  {
    this.mContext = paramContext;
    this.mResContext = paramResourceContext;
    this.mResource = paramResource;
    RelatedResource localRelatedResource;
    LocalJSONDataParser localLocalJSONDataParser;
    if ((paramResource.getSubResources().size() == 0) && (paramResource.getParentResources().size() != 0))
    {
      localRelatedResource = (RelatedResource)paramResource.getParentResources().get(0);
      localLocalJSONDataParser = new LocalJSONDataParser(this.mResContext);
    }
    try
    {
      this.mResource = localLocalJSONDataParser.loadResource(new File(localRelatedResource.getLocalPath()));
      if ((0x10 & paramLong2) != 0L)
        paramLong2 |= 262144L;
      long l = ThemeHelper.getCompatibleFlag(paramResource.getPlatform(), paramLong2);
      this.mModuleFlags = Long.parseLong(paramResource.getExtraMeta("modulesFlag"));
      this.mRemoveFlags = (paramLong1 | l);
      this.mApplyFlags = l;
      return;
    }
    catch (PersistenceException localPersistenceException)
    {
      while (true)
        localPersistenceException.printStackTrace();
    }
  }

  private void apply(Resource paramResource, long paramLong1, long paramLong2)
    throws IOException
  {
    int i;
    if ((0x8 & paramLong1) == 0L)
      i = 0;
    else
      i = 10;
    this.mProgress.setMax(i + (8 + paramResource.getSubResources().size()));
    ThemeHelper.createRuntimeFolder(this.mContext);
    this.mProgress.setProgress(1 + this.mProgress.getProgress());
    List localList = getComponentFilterPath(paramLong1);
    boolean bool;
    if ((0x10000000 & paramLong1) == 0L)
      bool = false;
    else
      bool = true;
    delete(localList, bool);
    this.mProgress.setProgress(1 + this.mProgress.getProgress());
    copyResources(paramResource, paramLong2);
    this.mProgress.setProgress(1 + this.mProgress.getProgress());
    convertResourcesNames(paramLong1, paramLong2);
    this.mProgress.setProgress(1 + this.mProgress.getProgress());
    if ((0x8000 & paramLong1) != 0L);
    this.mProgress.setProgress(1 + this.mProgress.getProgress());
    if ((0x8 & paramLong1) != 0L)
    {
      IconCustomizer.clearCustomizedIcons(null);
      this.mProgress.setProgress(1 + this.mProgress.getProgress());
      ThemeResources.getSystem().resetIcons();
      this.mProgress.setProgress(1 + this.mProgress.getProgress());
      IconCustomizer.prepareCustomizedIcons(this.mContext, new IconCustomizer.CustomizedIconsListener(i)
      {
        int originProgress = 0;
        int totalIcon = 0;

        public void beforePrepareIcon(int paramInt)
        {
          if (paramInt <= 0)
            paramInt = 1;
          this.totalIcon = paramInt;
          this.originProgress = ApplyThemeTask.this.mProgress.getProgress();
        }

        public void finishAllIcons()
        {
          ApplyThemeTask.this.mProgress.setProgress(this.originProgress + this.val$iconSteps);
        }

        public void finishPrepareIcon(int paramInt)
        {
          int i = this.originProgress + paramInt * this.val$iconSteps / this.totalIcon;
          if (i != ApplyThemeTask.this.mProgress.getProgress())
            ApplyThemeTask.this.mProgress.setProgress(i);
        }
      });
      this.mProgress.setProgress(1 + this.mProgress.getProgress());
    }
    if ((0x1000 & paramLong2) != 0L)
    {
      File localFile2 = new File(ThemeHelper.getLockstyleAppliedConfigFilePath());
      if (localFile2.exists())
        localFile2.delete();
      File localFile1 = new File(ThemeHelper.getLockstyleSDCardConfigPathFromThemePath(paramResource.getFilePath()));
      if (localFile1.exists())
      {
        FileUtils.copyFile(localFile1, localFile2);
        FileUtils.setPermissions(localFile2.getAbsolutePath(), 509, -1, -1);
      }
    }
    this.mProgress.setProgress(1 + this.mProgress.getProgress());
  }

  // ERROR //
  private boolean containsLockscreenThemeValue()
  {
    // Byte code:
    //   0: iconst_0
    //   1: istore_1
    //   2: aconst_null
    //   3: astore_2
    //   4: new 215	java/util/zip/ZipFile
    //   7: dup
    //   8: ldc 217
    //   10: invokespecial 218	java/util/zip/ZipFile:<init>	(Ljava/lang/String;)V
    //   13: astore_2
    //   14: aload_2
    //   15: ldc 220
    //   17: invokevirtual 224	java/util/zip/ZipFile:getEntry	(Ljava/lang/String;)Ljava/util/zip/ZipEntry;
    //   20: astore_1
    //   21: aload_1
    //   22: ifnull +15 -> 37
    //   25: iconst_1
    //   26: istore_1
    //   27: aload_2
    //   28: ifnull +77 -> 105
    //   31: aload_2
    //   32: invokevirtual 227	java/util/zip/ZipFile:close	()V
    //   35: iload_1
    //   36: ireturn
    //   37: iconst_0
    //   38: istore_1
    //   39: goto -12 -> 27
    //   42: astore_2
    //   43: aload_2
    //   44: invokevirtual 228	java/io/IOException:printStackTrace	()V
    //   47: goto -12 -> 35
    //   50: astore_3
    //   51: aload_3
    //   52: invokevirtual 229	java/lang/Exception:printStackTrace	()V
    //   55: aload_2
    //   56: ifnull -21 -> 35
    //   59: aload_2
    //   60: invokevirtual 227	java/util/zip/ZipFile:close	()V
    //   63: goto -28 -> 35
    //   66: astore_2
    //   67: aload_2
    //   68: invokevirtual 228	java/io/IOException:printStackTrace	()V
    //   71: goto -36 -> 35
    //   74: astore_1
    //   75: aload_2
    //   76: ifnull +7 -> 83
    //   79: aload_2
    //   80: invokevirtual 227	java/util/zip/ZipFile:close	()V
    //   83: aload_1
    //   84: athrow
    //   85: astore_2
    //   86: aload_2
    //   87: invokevirtual 228	java/io/IOException:printStackTrace	()V
    //   90: goto -7 -> 83
    //   93: astore_1
    //   94: aload_2
    //   95: astore_2
    //   96: goto -21 -> 75
    //   99: astore_3
    //   100: aload_2
    //   101: astore_2
    //   102: goto -51 -> 51
    //   105: goto -70 -> 35
    //
    // Exception table:
    //   from	to	target	type
    //   31	35	42	java/io/IOException
    //   4	14	50	java/lang/Exception
    //   59	63	66	java/io/IOException
    //   4	14	74	finally
    //   51	55	74	finally
    //   79	83	85	java/io/IOException
    //   14	21	93	finally
    //   14	21	99	java/lang/Exception
  }

  private void convertResourcesNames(long paramLong1, long paramLong2)
  {
    if ((paramLong1 & 0x40010) != 0L)
      createFontFileLinkForPatchRomDevices("/system/fonts");
    if ((paramLong2 & 0x40010) != 0L)
    {
      String str2 = ExtraFileUtils.standardizeFolderPath("/data/system/theme/fonts");
      if ((0x10 & this.mModuleFlags) != 0L)
      {
        String str1 = ExtraFileUtils.standardizeFolderPath("/data/system/theme/fonts/Roboto-Regular.ttf");
        File localFile = new File(str1);
        if ((!localFile.exists()) || (!localFile.isFile()) || (localFile.length() <= 0L))
        {
          Shell.remove(str1);
        }
        else
        {
          Shell.link(str1, str2 + "Roboto-Bold.ttf");
          Shell.link(str1, str2 + "Roboto-Italic.ttf");
          Shell.link(str1, str2 + "Roboto-BoldItalic.ttf");
        }
      }
      createFontFileLinkForPatchRomDevices(str2);
    }
    Shell.move("/data/system/theme/com.android.launcher", "/data/system/theme/com.miui.home");
  }

  private void copyResources(Resource paramResource, long paramLong)
  {
    while (true)
    {
      Object localObject2;
      Long localLong;
      try
      {
        getComponentFilterPath(paramLong);
        if ((0x10000000 & paramLong) == 0L)
          break label311;
        localObject2 = paramResource.getSubResources();
        if (!((List)localObject2).isEmpty())
          continue;
        long l1 = paramLong;
        if ((0x1000 & l1) == 0L)
          break label314;
        l1 = 4096L;
        localObject1 = new RelatedResource();
        ((RelatedResource)localObject1).setFilePath(paramResource.getFilePath());
        ((RelatedResource)localObject1).setResourceCode(ConstantsHelper.getResourceCode(l1));
        ((List)localObject2).add(localObject1);
        Iterator localIterator = ((List)localObject2).iterator();
        if (localIterator.hasNext())
        {
          localObject2 = (RelatedResource)localIterator.next();
          if (this.mProgress == null)
            continue;
          this.mProgress.setProgress(1 + this.mProgress.getProgress());
          localObject1 = ((RelatedResource)localObject2).getResourceCode();
          localLong = ConstantsHelper.getResourceTypeByCode((String)localObject1);
          localObject3 = null;
          if (localLong == null)
            break label530;
          localObject3 = ConstantsHelper.getResourceIdentity(localLong.longValue());
          break label530;
          if (localLong == null)
            break label542;
          if ((paramLong & localLong.longValue()) == 0L)
            continue;
          break label542;
          if (ThemeHelper.isDisablePkgName((String)localObject3))
            continue;
          String str = null;
          if (localLong == null)
            continue;
          str = ConstantsHelper.getResourceRuntimePath(localLong.longValue());
          if (str != null)
            continue;
          str = "/data/system/theme/" + (String)localObject3;
          localObject2 = ((RelatedResource)localObject2).getFilePath();
          if ((!"wallpaper".equals(localObject1)) || ((0x2 & this.mApplyFlags) == 0L))
            break label344;
          ThemeHelper.applyDeskWallpaperOfThemeFile(this.mContext, (String)localObject2);
          continue;
        }
      }
      catch (Exception localException)
      {
        localException.printStackTrace();
      }
      return;
      label311: continue;
      label314: long l2;
      while ((localException & localException - 1L) != 0L)
        localException &= localException - 1L;
      label344: if (("lockscreen".equals(localObject1)) && ((0x4 & this.mApplyFlags) != 0L))
      {
        ThemeHelper.applyLockWallpaperOfThemeFile(this.mContext, (String)localObject2);
        continue;
      }
      if (("ringtone".equals(localObject1)) && ((0x100 & this.mApplyFlags) != 0L))
      {
        ThemeHelper.applyRingtone(this.mContext, 1, (String)localObject2);
        continue;
      }
      if (("notification".equals(localObject1)) && ((0x200 & this.mApplyFlags) != 0L))
      {
        ThemeHelper.applyRingtone(this.mContext, 2, (String)localObject2);
        continue;
      }
      if (("alarm".equals(localObject1)) && ((0x400 & this.mApplyFlags) != 0L))
      {
        ThemeHelper.applyRingtone(this.mContext, 4, (String)localObject2);
        continue;
      }
      Object localObject1 = new File(l2);
      Shell.mkdirs(((File)localObject1).getParent());
      Shell.copy((String)localObject2, l2);
      continue;
      label530: if (localObject3 != null)
        continue;
      Object localObject3 = localObject1;
      continue;
      label542: if (localLong != null)
        continue;
      if ((0x10000000 & paramLong) == 0L)
        continue;
    }
  }

  private void createFontFileLinkForPatchRomDevices(String paramString)
  {
    File[] arrayOfFile;
    int j;
    if ((paramString != null) && (SystemProperties.getInt("ro.skia.use_data_fonts", 0) == 1))
    {
      arrayOfFile = new File(paramString).listFiles();
      if (arrayOfFile != null)
        j = arrayOfFile.length;
    }
    for (int i = 0; ; i++)
    {
      if (i >= j)
        return;
      File localFile = arrayOfFile[i];
      Shell.link(localFile.toString(), "/data/fonts/" + localFile.getName());
    }
  }

  private void delete(List<String> paramList, boolean paramBoolean)
  {
    if (paramBoolean)
      paramList.addAll(getAllThirdAppResourcePath());
    Iterator localIterator = paramList.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      Shell.remove((String)localIterator.next());
    }
  }

  private List<String> getAllThirdAppResourcePath()
  {
    ArrayList localArrayList = new ArrayList();
    String[] arrayOfString = new File("/data/system/theme/").list();
    int m = arrayOfString.length;
    for (int k = 0; ; k++)
    {
      if (k >= m)
        return localArrayList;
      String str = arrayOfString[k];
      if ((str.startsWith("preview")) || (str.startsWith("description.xml")) || (str.startsWith("fonts")) || (str.startsWith("lock_wallpaper")))
        continue;
      int i = 1;
      int j = 0;
      while (j < COMPONENT_IDENTITIES.length)
      {
        if (!str.startsWith(COMPONENT_IDENTITIES[j]))
        {
          j++;
          continue;
        }
        i = 0;
      }
      if (i == 0)
        continue;
      localArrayList.add("/data/system/theme/" + str);
    }
  }

  private List<String> getComponentFilterPath(long paramLong)
  {
    ArrayList localArrayList = new ArrayList();
    if ((paramLong == -1L) || (paramLong == -1793L))
    {
      localArrayList.add("/data/system/theme/*");
      localArrayList.add("/data/local/bootanimation.zip");
    }
    for (int i = 0; ; i++)
    {
      if (i >= ThemeHelper.THEME_FLAG_COUNT)
      {
        if ((0x40010 & paramLong) != 0L)
          localArrayList.add("/data/system/theme/fonts");
        if ((paramLong & 0x1) != 0L)
          localArrayList.add("/data/system/theme/description.xml");
        return localArrayList;
      }
      long l = 1L << i;
      if ((paramLong & l) == 0L)
        continue;
      localArrayList.add(RUNTIME_PATHS[i]);
      if (l == 16384L)
        localArrayList.add("/data/system/theme/com.android.launcher");
      if (COMPONENT_PREVIEW_PREFIX[i] == null)
        continue;
      Object[] arrayOfObject = new Object[3];
      arrayOfObject[0] = "/data/system/theme/";
      arrayOfObject[1] = "preview";
      arrayOfObject[2] = COMPONENT_PREVIEW_PREFIX[i];
      localArrayList.add(String.format("%s%s/%s", arrayOfObject));
    }
  }

  private void sendThemeConfigurationChangeMsg(long paramLong)
  {
    if (paramLong != 0L);
    try
    {
      Configuration localConfiguration = ActivityManagerNative.getDefault().getConfiguration();
      localConfiguration.extraConfig.updateTheme(paramLong);
      ActivityManagerNative.getDefault().updateConfiguration(localConfiguration);
      return;
    }
    catch (RemoteException localRemoteException)
    {
      while (true)
        localRemoteException.printStackTrace();
    }
  }

  protected Void doInBackground(Void[] paramArrayOfVoid)
  {
    try
    {
      View localView = this.mProgress.getWindow().getDecorView();
      while (localView.getWindowToken() == null)
        Thread.sleep(10L);
      apply(this.mResource, this.mRemoveFlags, this.mApplyFlags);
      this.mProgress.setProgress(1 + this.mProgress.getProgress());
    }
    catch (Exception localException)
    {
    }
    return null;
  }

  protected void onPostExecute(Void paramVoid)
  {
    this.mProgress.dismiss();
    saveUserPreference(this.mResource, this.mRemoveFlags, this.mApplyFlags);
    if ((0x10000 & this.mRemoveFlags) != 0L)
    {
      l = System.currentTimeMillis();
      Settings.System.putLong(this.mContext.getContentResolver(), "clock_changed_time_" + "1x2", l);
      Settings.System.putLong(this.mContext.getContentResolver(), "clock_changed_time_" + "2x2", l);
      Settings.System.putLong(this.mContext.getContentResolver(), "clock_changed_time_" + "2x4", l);
      Settings.System.putLong(this.mContext.getContentResolver(), "clock_changed_time_" + "4x4", l);
    }
    long l = this.mApplyFlags;
    if (((0x2000 & this.mApplyFlags) == 0L) && ((0x1000 & this.mRemoveFlags) != 0L) && (containsLockscreenThemeValue()))
      l |= 8192L;
    if ((0x30000 & l) != 0L)
      l |= 16384L;
    if ((0x40010 & l) != 0L)
    {
      Object[] arrayOfObject = new Object[1];
      arrayOfObject[0] = Long.valueOf(System.currentTimeMillis() / 1000L);
      CommandLineUtils.run("root", "setprop debug.skia.free_cache %s", arrayOfObject);
    }
    sendThemeConfigurationChangeMsg(l & 0x10047899);
    ThemeHelper.showThemeChangedToast(this.mContext, true);
    if (this.mFinishRunnable != null)
      this.mFinishRunnable.run();
  }

  protected void onPreExecute()
  {
    this.mProgress = new ProgressDialog(this.mContext);
    this.mProgress.setProgressStyle(1);
    this.mProgress.setMessage(this.mContext.getString(2131427330));
    this.mProgress.setCancelable(false);
    this.mProgress.show();
    this.mProgress.setProgressNumberFormat("");
  }

  public void saveUserPreference(Resource paramResource, long paramLong1, long paramLong2)
  {
    long l;
    if (this.mModuleFlags != -1L)
      l = this.mModuleFlags;
    else
      l = ThemeHelper.getAllComponentsCombineFlag();
    ThemeHelper.saveUserPreference(paramResource, paramLong2 & l, paramLong1);
  }

  public void setPostRunnable(Runnable paramRunnable)
  {
    this.mFinishRunnable = paramRunnable;
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     com.android.thememanager.util.ApplyThemeTask
 * JD-Core Version:    0.6.0
 */