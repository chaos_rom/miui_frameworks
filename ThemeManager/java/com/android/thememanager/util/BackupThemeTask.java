package com.android.thememanager.util;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.TextUtils;
import com.android.thememanager.ThemeResourceConstants;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.zip.ZipOutputStream;
import miui.resourcebrowser.util.ResourceHelper;

public class BackupThemeTask extends AsyncTask<Void, Void, Void>
  implements ThemeResourceConstants
{
  public static final String BACKUP_PATH = DOWNLOADED_THEME_PATH;
  public static final String BACKUP_THEME_PATH = BACKUP_PATH + "/backup.mtz";
  private Context mContext;
  private ProgressDialog mProgress;

  public BackupThemeTask(Context paramContext)
  {
    this.mContext = paramContext;
  }

  private void backupRingtone(ZipOutputStream paramZipOutputStream, int paramInt, String paramString)
  {
    Object localObject = RingtoneManager.getActualDefaultRingtoneUri(this.mContext, paramInt);
    localObject = ResourceHelper.getPathByUri(this.mContext, (Uri)localObject);
    if (!TextUtils.isEmpty((CharSequence)localObject))
      ResourceHelper.zip(paramZipOutputStream, new File((String)localObject), paramString);
  }

  private void writeDescriptionInfo(ZipOutputStream paramZipOutputStream)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
    localStringBuilder.append("\n");
    localStringBuilder.append("<theme>");
    localStringBuilder.append("\n");
    localStringBuilder.append("\t<title>");
    localStringBuilder.append(this.mContext.getString(2131427338));
    localStringBuilder.append("</title>");
    localStringBuilder.append("\n");
    localStringBuilder.append("\t<designer></designer>");
    localStringBuilder.append("\n");
    localStringBuilder.append("\t<author></author>");
    localStringBuilder.append("\n");
    localStringBuilder.append("\t<version>1.0</version>");
    localStringBuilder.append("\n");
    localStringBuilder.append("\t<platform>");
    localStringBuilder.append(2);
    localStringBuilder.append("</platform>");
    localStringBuilder.append("\n");
    localStringBuilder.append("</theme>");
    Object localObject = new Object[1];
    localObject[0] = this.mContext.getFilesDir();
    localObject = String.format("%s/description.xml", localObject);
    ResourceHelper.writeTo(new ByteArrayInputStream(localStringBuilder.toString().getBytes()), (String)localObject);
    ResourceHelper.zip(paramZipOutputStream, new File((String)localObject), "description.xml");
  }

  // ERROR //
  protected Void doInBackground(Void[] paramArrayOfVoid)
  {
    // Byte code:
    //   0: new 64	java/io/File
    //   3: dup
    //   4: getstatic 21	com/android/thememanager/util/BackupThemeTask:BACKUP_PATH	Ljava/lang/String;
    //   7: ldc 147
    //   9: invokespecial 150	java/io/File:<init>	(Ljava/lang/String;Ljava/lang/String;)V
    //   12: astore_3
    //   13: aload_3
    //   14: invokevirtual 154	java/io/File:delete	()Z
    //   17: pop
    //   18: aload_3
    //   19: invokevirtual 157	java/io/File:getParentFile	()Ljava/io/File;
    //   22: sipush 511
    //   25: bipush 255
    //   27: bipush 255
    //   29: invokestatic 163	miui/os/ExtraFileUtils:mkdirs	(Ljava/io/File;III)Z
    //   32: pop
    //   33: aconst_null
    //   34: astore_2
    //   35: new 165	java/util/zip/ZipOutputStream
    //   38: dup
    //   39: new 167	java/io/BufferedOutputStream
    //   42: dup
    //   43: new 169	java/io/FileOutputStream
    //   46: dup
    //   47: aload_3
    //   48: invokespecial 172	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   51: invokespecial 175	java/io/BufferedOutputStream:<init>	(Ljava/io/OutputStream;)V
    //   54: invokespecial 176	java/util/zip/ZipOutputStream:<init>	(Ljava/io/OutputStream;)V
    //   57: astore_2
    //   58: aload_2
    //   59: iconst_0
    //   60: invokevirtual 180	java/util/zip/ZipOutputStream:setMethod	(I)V
    //   63: aload_0
    //   64: aload_2
    //   65: invokespecial 182	com/android/thememanager/util/BackupThemeTask:writeDescriptionInfo	(Ljava/util/zip/ZipOutputStream;)V
    //   68: ldc 184
    //   70: ldc 186
    //   72: invokestatic 192	miui/os/Shell:copy	(Ljava/lang/String;Ljava/lang/String;)Z
    //   75: pop
    //   76: aload_2
    //   77: new 64	java/io/File
    //   80: dup
    //   81: ldc 186
    //   83: invokespecial 67	java/io/File:<init>	(Ljava/lang/String;)V
    //   86: ldc 194
    //   88: invokestatic 71	miui/resourcebrowser/util/ResourceHelper:zip	(Ljava/util/zip/ZipOutputStream;Ljava/io/File;Ljava/lang/String;)Z
    //   91: pop
    //   92: ldc 186
    //   94: invokestatic 198	miui/os/Shell:remove	(Ljava/lang/String;)Z
    //   97: pop
    //   98: aload_2
    //   99: new 64	java/io/File
    //   102: dup
    //   103: ldc 200
    //   105: invokespecial 67	java/io/File:<init>	(Ljava/lang/String;)V
    //   108: ldc 202
    //   110: invokestatic 71	miui/resourcebrowser/util/ResourceHelper:zip	(Ljava/util/zip/ZipOutputStream;Ljava/io/File;Ljava/lang/String;)Z
    //   113: pop
    //   114: aload_0
    //   115: aload_2
    //   116: iconst_1
    //   117: ldc 204
    //   119: invokespecial 206	com/android/thememanager/util/BackupThemeTask:backupRingtone	(Ljava/util/zip/ZipOutputStream;ILjava/lang/String;)V
    //   122: aload_0
    //   123: aload_2
    //   124: iconst_2
    //   125: ldc 208
    //   127: invokespecial 206	com/android/thememanager/util/BackupThemeTask:backupRingtone	(Ljava/util/zip/ZipOutputStream;ILjava/lang/String;)V
    //   130: aload_0
    //   131: aload_2
    //   132: iconst_4
    //   133: ldc 210
    //   135: invokespecial 206	com/android/thememanager/util/BackupThemeTask:backupRingtone	(Ljava/util/zip/ZipOutputStream;ILjava/lang/String;)V
    //   138: new 64	java/io/File
    //   141: dup
    //   142: ldc 212
    //   144: invokespecial 67	java/io/File:<init>	(Ljava/lang/String;)V
    //   147: invokevirtual 215	java/io/File:exists	()Z
    //   150: ifne +19 -> 169
    //   153: aload_2
    //   154: new 64	java/io/File
    //   157: dup
    //   158: ldc 212
    //   160: invokespecial 67	java/io/File:<init>	(Ljava/lang/String;)V
    //   163: ldc 217
    //   165: invokestatic 71	miui/resourcebrowser/util/ResourceHelper:zip	(Ljava/util/zip/ZipOutputStream;Ljava/io/File;Ljava/lang/String;)Z
    //   168: pop
    //   169: aload_2
    //   170: new 64	java/io/File
    //   173: dup
    //   174: ldc 219
    //   176: invokespecial 67	java/io/File:<init>	(Ljava/lang/String;)V
    //   179: ldc 221
    //   181: invokestatic 71	miui/resourcebrowser/util/ResourceHelper:zip	(Ljava/util/zip/ZipOutputStream;Ljava/io/File;Ljava/lang/String;)Z
    //   184: pop
    //   185: new 64	java/io/File
    //   188: dup
    //   189: getstatic 38	com/android/thememanager/util/BackupThemeTask:BACKUP_THEME_PATH	Ljava/lang/String;
    //   192: invokespecial 67	java/io/File:<init>	(Ljava/lang/String;)V
    //   195: astore 4
    //   197: aload 4
    //   199: invokevirtual 154	java/io/File:delete	()Z
    //   202: pop
    //   203: aload_3
    //   204: aload 4
    //   206: invokevirtual 225	java/io/File:renameTo	(Ljava/io/File;)Z
    //   209: pop
    //   210: aload_2
    //   211: ifnull +7 -> 218
    //   214: aload_2
    //   215: invokevirtual 228	java/util/zip/ZipOutputStream:close	()V
    //   218: aconst_null
    //   219: areturn
    //   220: astore_2
    //   221: aload_2
    //   222: invokevirtual 231	java/io/IOException:printStackTrace	()V
    //   225: goto -7 -> 218
    //   228: astore_3
    //   229: aload_3
    //   230: invokevirtual 232	java/lang/Exception:printStackTrace	()V
    //   233: new 234	java/lang/RuntimeException
    //   236: dup
    //   237: invokespecial 235	java/lang/RuntimeException:<init>	()V
    //   240: athrow
    //   241: astore_3
    //   242: aload_2
    //   243: ifnull +7 -> 250
    //   246: aload_2
    //   247: invokevirtual 228	java/util/zip/ZipOutputStream:close	()V
    //   250: aload_3
    //   251: athrow
    //   252: astore_2
    //   253: aload_2
    //   254: invokevirtual 231	java/io/IOException:printStackTrace	()V
    //   257: goto -7 -> 250
    //   260: astore_3
    //   261: aload_2
    //   262: astore_2
    //   263: goto -21 -> 242
    //   266: astore_3
    //   267: aload_2
    //   268: astore_2
    //   269: goto -40 -> 229
    //
    // Exception table:
    //   from	to	target	type
    //   214	218	220	java/io/IOException
    //   35	58	228	java/lang/Exception
    //   35	58	241	finally
    //   229	241	241	finally
    //   246	250	252	java/io/IOException
    //   58	210	260	finally
    //   58	210	266	java/lang/Exception
  }

  protected void onPostExecute(Void paramVoid)
  {
    this.mProgress.dismiss();
    new AlertDialog.Builder(this.mContext).setMessage(2131427337).setPositiveButton(17039370, null).create().show();
  }

  protected void onPreExecute()
  {
    this.mProgress = new ProgressDialog(this.mContext);
    this.mProgress.setIndeterminate(true);
    this.mProgress.setMessage(this.mContext.getString(2131427336));
    this.mProgress.setCancelable(false);
    this.mProgress.show();
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     com.android.thememanager.util.BackupThemeTask
 * JD-Core Version:    0.6.0
 */