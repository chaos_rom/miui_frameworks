package com.android.thememanager.util;

import android.app.WallpaperManager;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import com.android.thememanager.ThemeResourceConstants;
import com.android.thememanager.activity.ThemeDetailActivity;
import com.android.thememanager.activity.ThemeRecommendListActivity;
import com.android.thememanager.activity.ThemeSearchListActivity;
import com.android.thememanager.activity.ThemeTabActivity;
import com.android.thememanager.activity.WallpaperDetailActivity;
import java.util.ArrayList;
import java.util.List;
import miui.os.ExtraFileUtils;
import miui.resourcebrowser.ResourceContext;
import miui.resourcebrowser.util.ResourceHelper;

public class UIHelper
  implements ThemeResourceConstants
{
  public static ResourceContext buildResourceContext(ResourceContext paramResourceContext, Intent paramIntent, Context paramContext)
  {
    String str1 = paramIntent.getAction();
    long l = -1L;
    if (!"android.intent.action.RINGTONE_PICKER".equals(str1))
    {
      if (!"android.intent.action.SET_WALLPAPER".equals(str1))
      {
        if (!"android.intent.action.PICK_GADGET".equals(str1))
        {
          l = Long.valueOf(paramIntent.getLongExtra("REQUEST_RESOURCE_TYPE", -1L)).longValue();
        }
        else
        {
          paramResourceContext.setPicker(true);
          paramResourceContext.setCurrentUsingPath(paramIntent.getStringExtra("REQUEST_CURRENT_USING_PATH"));
          paramResourceContext.setTrackId(paramIntent.getStringExtra("REQUEST_TRACK_ID"));
          if (!"clock".equals(paramIntent.getStringExtra("REQUEST_GADGET_NAME")))
            l = 131072L;
          else
            l = 65536L;
          paramResourceContext.putExtraMeta("EXTRA_CTX_GADGET_FLAG", paramIntent.getStringExtra("REQUEST_GADGET_SIZE"));
        }
      }
      else
        l = 2L;
    }
    else
    {
      paramResourceContext.setPicker(true);
      int i = paramIntent.getIntExtra("android.intent.extra.ringtone.TYPE", 1);
      if (i == 7)
        i = 2;
      switch (i)
      {
      case 1:
        l = 256L;
        break;
      case 2:
        l = 512L;
        break;
      case 4:
        l = 1024L;
      case 3:
      }
    }
    paramResourceContext.putExtraMeta("android.intent.extra.ringtone.TYPE", Integer.valueOf(getRingtoneType(l)));
    paramResourceContext.putExtraMeta("EXTRA_CTX_RESOURCE_TYPE", Long.valueOf(l));
    if (l == 32768L)
    {
      paramResourceContext.putExtraMeta("android.intent.extra.ringtone.SHOW_SILENT", Boolean.valueOf(false));
      paramResourceContext.putExtraMeta("android.intent.extra.ringtone.SHOW_DEFAULT", Boolean.valueOf(false));
      paramResourceContext.putExtraMeta("EXTRA_CTX_SHOW_RINGTONE_NAME", Boolean.valueOf(true));
    }
    paramResourceContext.setDisplayType(getDisplayType(l));
    String str2 = paramContext.getString(ConstantsHelper.getTitleResId(l));
    paramResourceContext.setResourceTitle(str2);
    paramResourceContext.setResourceFormat(getResourceFormat(l));
    paramResourceContext.setResourceExtension(getResourceExtension(l));
    paramResourceContext.setSelfDescribing(isSelfDescribing(l));
    paramResourceContext.setResourceStamp(ConstantsHelper.getResourceStamp(l));
    if ("android.intent.action.PICK_GADGET".equals(str1))
      new StringBuilder().append(str2).append(paramIntent.getStringExtra("REQUEST_GADGET_SIZE")).toString();
    str2 = ConstantsHelper.getResourceCode(l);
    paramResourceContext.setResourceCode(str2);
    paramResourceContext.setRecommendSupported(isRecommendSupported(l));
    paramResourceContext.setCategorySupported(isCategorySupported(l));
    if ((l == 65536L) || (l == 131072L))
    {
      if (paramResourceContext.isPicker())
        bool = false;
      else
        bool = true;
      paramResourceContext.setCategorySupported(bool);
    }
    paramResourceContext.setVersionSupported(isVersionSupported(l));
    boolean bool = isPlatformSupported(l);
    paramResourceContext.setPlatformSupported(bool);
    if (bool)
      paramResourceContext.setCurrentPlatform(ConstantsHelper.getPlatform(l));
    ArrayList localArrayList = new ArrayList();
    if (l != -1L)
      localArrayList.add(ConstantsHelper.getPreviewPrefix(l));
    Object localObject1;
    for (int j = 0; ; localObject1++)
    {
      if (j >= COMPONENT_PREVIEW_SHOW_ORDER.length)
      {
        paramResourceContext.setBuildInImagePrefixes(localArrayList);
        localArrayList = new ArrayList();
        Object localObject2;
        String str3;
        String str5;
        String str4;
        String str6;
        if (l != 2L)
        {
          if (l != 4L)
          {
            if (l != 256L)
            {
              if (l != 512L)
              {
                if (l != 1024L)
                {
                  if (l != 64L)
                  {
                    if (l != 65536L)
                    {
                      if (l != 131072L)
                      {
                        if (l != -1L)
                        {
                          str2 = ExtraFileUtils.standardizeFolderPath(str2);
                          localArrayList.add("/system/media/theme/.data/meta/" + str2);
                          localArrayList.add("/data/media/theme/.data/meta/" + str2);
                          localArrayList.add(USER_RESOURCE_PATH + str2);
                          str2 = DOWNLOADED_RESOURCE_PATH;
                          localObject1 = META_RESOURCE_PATH;
                          localObject2 = CONTENT_RESOURCE_PATH;
                          str3 = RIGHTS_RESOURCE_PATH;
                          str5 = BUILDIN_IMAGE_RESOURCE_PATH;
                          str4 = INDEX_RESOURCE_PATH;
                          str6 = ASYNC_IMPORT_RESOURCE_PATH;
                        }
                        else
                        {
                          localArrayList.add("/system/media/theme/.data/meta/theme/");
                          localArrayList.add("/data/media/theme/.data/meta/theme/");
                          localArrayList.add(USER_THEME_PATH);
                          str2 = DOWNLOADED_THEME_PATH;
                          localObject1 = META_THEME_PATH;
                          localObject2 = CONTENT_THEME_PATH;
                          str3 = RIGHTS_THEME_PATH;
                          str5 = BUILDIN_IMAGE_THEME_PATH;
                          str4 = INDEX_THEME_PATH;
                          str6 = ASYNC_IMPORT_THEME_PATH;
                        }
                      }
                      else
                      {
                        str2 = paramIntent.getStringExtra("REQUEST_GADGET_SIZE");
                        localObject1 = new Object[1];
                        localObject1[0] = str2;
                        localArrayList.add(String.format("/system/media/theme/.data/meta/gadget/photo_frame_%s/", localObject1));
                        localObject1 = new Object[1];
                        localObject1[0] = str2;
                        localArrayList.add(String.format("/data/media/theme/.data/meta/gadget/photo_frame_%s/", localObject1));
                        localObject2 = USER_GADGET_PHOTO_FRAME_PATH;
                        localObject1 = new Object[1];
                        localObject1[0] = str2;
                        localArrayList.add(String.format((String)localObject2, localObject1));
                        str2 = DOWNLOADED_GADGET_PHOTO_FRAME_PATH;
                        localObject1 = META_GADGET_PHOTO_FRAME_PATH;
                        localObject2 = CONTENT_GADGET_PHOTO_FRAME_PATH;
                        str3 = RIGHTS_GADGET_PHOTO_FRAME_PATH;
                        str5 = BUILDIN_IMAGE_GADGET_PHOTO_FRAME_PATH;
                        str4 = INDEX_GADGET_PHOTO_FRAME_PATH;
                        str6 = ASYNC_IMPORT_GADGET_PHOTO_FRAME_PATH;
                      }
                    }
                    else
                    {
                      str2 = paramIntent.getStringExtra("REQUEST_GADGET_SIZE");
                      localObject1 = new Object[1];
                      localObject1[0] = str2;
                      localArrayList.add(String.format("/system/media/theme/.data/meta/gadget/clock_%s/", localObject1));
                      localObject1 = new Object[1];
                      localObject1[0] = str2;
                      localArrayList.add(String.format("/data/media/theme/.data/meta/gadget/clock_%s/", localObject1));
                      localObject1 = USER_GADGET_CLOCK_PATH;
                      localObject2 = new Object[1];
                      localObject2[0] = str2;
                      localArrayList.add(String.format((String)localObject1, localObject2));
                      str2 = DOWNLOADED_GADGET_CLOCK_PATH;
                      localObject1 = META_GADGET_CLOCK_PATH;
                      localObject2 = CONTENT_GADGET_CLOCK_PATH;
                      str3 = RIGHTS_GADGET_CLOCK_PATH;
                      str5 = BUILDIN_IMAGE_GADGET_CLOCK_PATH;
                      str4 = INDEX_GADGET_CLOCK_PATH;
                      str6 = ASYNC_IMPORT_GADGET_CLOCK_PATH;
                    }
                  }
                  else
                  {
                    localArrayList.add(USER_BOOT_AUDIO_PATH);
                    localArrayList.add("/data/media/audio/ringtones/");
                    localArrayList.add("/system/media/audio/ringtones/");
                    str2 = DOWNLOADED_BOOT_AUDIO_PATH;
                    localObject1 = META_BOOT_AUDIO_PATH;
                    localObject2 = CONTENT_BOOT_AUDIO_PATH;
                    str3 = RIGHTS_BOOT_AUDIO_PATH;
                    str5 = BUILDIN_IMAGE_BOOT_AUDIO_PATH;
                    str4 = INDEX_BOOT_AUDIO_PATH;
                    str6 = ASYNC_IMPORT_BOOT_AUDIO_PATH;
                  }
                }
                else
                {
                  localArrayList.add(USER_ALARM_PATH);
                  localArrayList.add("/data/media/audio/alarms/");
                  localArrayList.add("/system/media/audio/alarms/");
                  str2 = DOWNLOADED_ALARM_PATH;
                  localObject1 = META_ALARM_PATH;
                  localObject2 = CONTENT_ALARM_PATH;
                  str3 = RIGHTS_ALARM_PATH;
                  str5 = BUILDIN_IMAGE_ALARM_PATH;
                  str4 = INDEX_ALARM_PATH;
                  str6 = ASYNC_IMPORT_ALARM_PATH;
                }
              }
              else
              {
                localArrayList.add(USER_NOTIFICATION_PATH);
                localArrayList.add("/data/media/audio/notifications/");
                localArrayList.add("/system/media/audio/notifications/");
                str2 = DOWNLOADED_NOTIFICATION_PATH;
                localObject1 = META_NOTIFICATION_PATH;
                localObject2 = CONTENT_NOTIFICATION_PATH;
                str3 = RIGHTS_NOTIFICATION_PATH;
                str5 = BUILDIN_IMAGE_NOTIFICATION_PATH;
                str4 = INDEX_NOTIFICATION_PATH;
                str6 = ASYNC_IMPORT_NOTIFICATION_PATH;
              }
            }
            else
            {
              localArrayList.add(USER_RINGTONE_PATH);
              localArrayList.add("/data/media/audio/ringtones/");
              localArrayList.add("/system/media/audio/ringtones/");
              str2 = DOWNLOADED_RINGTONE_PATH;
              localObject1 = META_RINGTONE_PATH;
              localObject2 = CONTENT_RINGTONE_PATH;
              str3 = RIGHTS_RINGTONE_PATH;
              str5 = BUILDIN_IMAGE_RINGTONE_PATH;
              str4 = INDEX_RINGTONE_PATH;
              str6 = ASYNC_IMPORT_RINGTONE_PATH;
            }
          }
          else
          {
            localArrayList.add("/system/media/lockscreen/");
            localArrayList.add("/data/media/lockscreen/");
            localArrayList.add(USER_LOCKSCREEN_PATH);
            str2 = DOWNLOADED_LOCKSCREEN_PATH;
            localObject1 = META_LOCKSCREEN_PATH;
            localObject2 = CONTENT_LOCKSCREEN_PATH;
            str3 = RIGHTS_LOCKSCREEN_PATH;
            str5 = BUILDIN_IMAGE_LOCKSCREEN_PATH;
            str4 = INDEX_LOCKSCREEN_PATH;
            str6 = ASYNC_IMPORT_LOCKSCREEN_PATH;
          }
        }
        else
        {
          localArrayList.add("/system/media/wallpaper/");
          localArrayList.add("/data/media/wallpaper/");
          localArrayList.add(USER_WALLPAPER_PATH);
          str2 = DOWNLOADED_WALLPAPER_PATH;
          localObject1 = META_WALLPAPER_PATH;
          localObject2 = CONTENT_WALLPAPER_PATH;
          str3 = RIGHTS_WALLPAPER_PATH;
          str5 = BUILDIN_IMAGE_WALLPAPER_PATH;
          str4 = INDEX_WALLPAPER_PATH;
          str6 = ASYNC_IMPORT_WALLPAPER_PATH;
        }
        paramResourceContext.setSourceFolders(localArrayList);
        paramResourceContext.setDownloadFolder(str2);
        paramResourceContext.setMetaFolder((String)localObject1);
        paramResourceContext.setContentFolder((String)localObject2);
        paramResourceContext.setRightsFolder(str3);
        paramResourceContext.setBuildInImageFolder(str5);
        paramResourceContext.setIndexFolder(str4);
        paramResourceContext.setAsyncImportFolder(str6);
        paramResourceContext.setTabActivityPackage(paramContext.getPackageName());
        paramResourceContext.setTabActivityClass(ThemeTabActivity.class.getName());
        paramResourceContext.setSearchActivityPackage(paramContext.getPackageName());
        paramResourceContext.setSearchActivityClass(ThemeSearchListActivity.class.getName());
        paramResourceContext.setRecommendActivityPackage(paramContext.getPackageName());
        paramResourceContext.setRecommendActivityClass(ThemeRecommendListActivity.class.getName());
        paramResourceContext.setDetailActivityPackage(paramContext.getPackageName());
        if ((l != 2L) && (l != 4L))
          paramResourceContext.setDetailActivityClass(ThemeDetailActivity.class.getName());
        else
          paramResourceContext.setDetailActivityClass(WallpaperDetailActivity.class.getName());
        return paramResourceContext;
      }
      localArrayList.add(ConstantsHelper.getPreviewPrefix(COMPONENT_PREVIEW_SHOW_ORDER[localObject1]));
    }
  }

  public static String computeCurrentUsingPath(Context paramContext, long paramLong)
  {
    String str;
    if (paramLong != 256L)
    {
      if (paramLong != 512L)
      {
        if (paramLong != 1024L)
        {
          str = ThemeHelper.loadUserPreferencePath(paramLong);
          if (str == null)
            str = "/system/media/theme/.data/meta/theme/default.mrm";
          else if (isImageResource(paramLong))
            if ((paramLong != 2L) || (((WallpaperManager)paramContext.getSystemService("wallpaper")).getWallpaperInfo() == null))
            {
              if (ThemeHelper.isWallpaperPrefOlderThanSystem(paramContext, paramLong))
                str = null;
            }
            else
              str = null;
        }
        else
        {
          str = ResourceHelper.getPathByUri(paramContext, RingtoneManager.getActualDefaultRingtoneUri(paramContext, 4));
        }
      }
      else
        str = ResourceHelper.getPathByUri(paramContext, RingtoneManager.getActualDefaultRingtoneUri(paramContext, 2));
    }
    else
      str = ResourceHelper.getPathByUri(paramContext, RingtoneManager.getActualDefaultRingtoneUri(paramContext, 1));
    return str;
  }

  public static int getDisplayType(long paramLong)
  {
    int i;
    if ((paramLong != -1L) && (paramLong != 1L) && (paramLong != 32L) && (paramLong != 128L) && (paramLong != 2048L) && (paramLong != 8192L) && (paramLong != 4096L) && (paramLong != 16384L) && (paramLong != 65536L) && (paramLong != 131072L))
    {
      if (paramLong != 2L)
      {
        if (paramLong != 4L)
        {
          if (!isFontResource(paramLong))
          {
            if (paramLong != 8L)
            {
              if (!isAudioResource(paramLong))
                i = 1;
              else
                i = 4;
            }
            else
              i = 8;
          }
          else
            i = 7;
        }
        else
          i = 9;
      }
      else
        i = 6;
    }
    else
      i = 11;
    return i;
  }

  public static String getResourceExtension(long paramLong)
  {
    String str;
    if (!isBundleResource(paramLong))
    {
      if (!isImageResource(paramLong))
      {
        if (!isAudioResource(paramLong))
        {
          if ((!isZipResource(paramLong)) && (!isFontResource(paramLong)))
            str = ".mtz";
          else
            str = ".mtz";
        }
        else
          str = ".mp3";
      }
      else
        str = ".jpg";
    }
    else
      str = ".mtz";
    return str;
  }

  public static int getResourceFormat(long paramLong)
  {
    int i;
    if (!isBundleResource(paramLong))
    {
      if (!isImageResource(paramLong))
      {
        if (!isAudioResource(paramLong))
        {
          if (!isZipResource(paramLong))
            i = 5;
          else
            i = 4;
        }
        else
          i = 3;
      }
      else
        i = 2;
    }
    else
      i = 1;
    return i;
  }

  public static int getRingtoneType(long paramLong)
  {
    int i = 1;
    if (paramLong != 256L)
      if (paramLong != 512L)
      {
        if (paramLong != 1024L)
        {
          if (paramLong == 64L)
            i = 32;
        }
        else
          i = 4;
      }
      else
        i = 2;
    return i;
  }

  public static boolean isAudioResource(long paramLong)
  {
    int i;
    if ((paramLong != 64L) && (paramLong != 256L) && (paramLong != 512L) && (paramLong != 1024L) && (paramLong != 32768L))
      i = 0;
    else
      i = 1;
    return i;
  }

  public static boolean isBundleResource(long paramLong)
  {
    int i;
    if (paramLong != -1L)
      i = 0;
    else
      i = 1;
    return i;
  }

  public static boolean isCategorySupported(long paramLong)
  {
    int i;
    if (((paramLong == 32768L) || (!isAudioResource(paramLong))) && (!isImageResource(paramLong)) && (!isFontResource(paramLong)) && (paramLong != 65536L) && (paramLong != 131072L))
      i = 0;
    else
      i = 1;
    return i;
  }

  public static boolean isFontResource(long paramLong)
  {
    int i;
    if ((paramLong != 16L) && (paramLong != 262144L))
      i = 0;
    else
      i = 1;
    return i;
  }

  public static boolean isImageResource(long paramLong)
  {
    int i;
    if ((paramLong != 2L) && (paramLong != 4L))
      i = 0;
    else
      i = 1;
    return i;
  }

  public static boolean isPlatformSupported(long paramLong)
  {
    int i;
    if ((isAudioResource(paramLong)) || (isImageResource(paramLong)))
      i = 0;
    else
      i = 1;
    return i;
  }

  public static boolean isRecommendSupported(long paramLong)
  {
    return true;
  }

  public static boolean isSelfDescribing(long paramLong)
  {
    int i = getResourceFormat(paramLong);
    if ((i != 3) && (i != 2))
      i = 0;
    else
      i = 1;
    return i;
  }

  public static boolean isVersionSupported(long paramLong)
  {
    int i;
    if ((isAudioResource(paramLong)) || (isImageResource(paramLong)))
      i = 0;
    else
      i = 1;
    return i;
  }

  public static boolean isZipResource(long paramLong)
  {
    int i;
    if ((paramLong != 1L) && (paramLong != 8L) && (!isFontResource(paramLong)) && (paramLong != 4096L) && (paramLong != 32L) && (paramLong != 8192L) && (paramLong != 16384L) && (paramLong != 2048L) && (paramLong != 128L) && (paramLong != 65536L) && (paramLong != 131072L))
      i = 0;
    else
      i = 1;
    return i;
  }

  public static boolean supportImportMenu(long paramLong)
  {
    int i;
    if ((paramLong != 32768L) && ((isAudioResource(paramLong)) || (isImageResource(paramLong))))
      i = 0;
    else
      i = 1;
    return i;
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     com.android.thememanager.util.UIHelper
 * JD-Core Version:    0.6.0
 */