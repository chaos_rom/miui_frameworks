package com.android.thememanager.util;

import miui.resourcebrowser.ResourceContext;
import miui.resourcebrowser.activity.ResourceAdapter;
import miui.resourcebrowser.activity.ResourceListFragment;
import miui.resourcebrowser.model.Resource;
import miui.resourcebrowser.util.AudioBatchResourceHandler;

public class ThemeAudioBatchHandler extends AudioBatchResourceHandler
{
  private long mResourceType;

  public ThemeAudioBatchHandler(ResourceListFragment paramResourceListFragment, ResourceAdapter paramResourceAdapter, ResourceContext paramResourceContext, long paramLong)
  {
    super(paramResourceListFragment, paramResourceAdapter, paramResourceContext);
    this.mResourceType = paramLong;
  }

  protected void handleApplyEvent(Resource paramResource)
  {
    ThemeApplyParameters localThemeApplyParameters = new ThemeApplyParameters();
    localThemeApplyParameters.applyFlags = this.mResourceType;
    localThemeApplyParameters.removeOthers = false;
    ThemeApplyUtils.applyResource(this.mActivity, this.mResContext, paramResource, localThemeApplyParameters);
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     com.android.thememanager.util.ThemeAudioBatchHandler
 * JD-Core Version:    0.6.0
 */