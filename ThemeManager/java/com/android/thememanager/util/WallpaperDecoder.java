package com.android.thememanager.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory.Options;
import android.util.Log;
import miui.resourcebrowser.util.ImageCacheDecoder;
import miui.resourcebrowser.util.ImageCacheDecoder.BitmapCache;
import miui.util.ImageUtils;
import miui.util.InputStreamLoader;

public class WallpaperDecoder extends ImageCacheDecoder
{
  public WallpaperDecoder()
  {
    this(3);
  }

  public WallpaperDecoder(int paramInt)
  {
    super(paramInt);
  }

  private int computeSampleSize(String paramString, int paramInt1, int paramInt2)
  {
    BitmapFactory.Options localOptions = ImageUtils.getBitmapSize(new InputStreamLoader(paramString));
    int i = Math.min(localOptions.outWidth / paramInt1, localOptions.outHeight / paramInt2);
    if (i < 1);
    for (i = 1; ; i++)
      if (4 * (localOptions.outWidth * localOptions.outHeight) / (i * i) <= 15728640)
        return i;
  }

  // ERROR //
  private Bitmap decodeOriginBitmapWithNativeMemory(String paramString, int paramInt1, int paramInt2)
  {
    // Byte code:
    //   0: invokestatic 49	miui/util/ImageUtils:getDefaultOptions	()Landroid/graphics/BitmapFactory$Options;
    //   3: astore 5
    //   5: aload 5
    //   7: aload_0
    //   8: aload_1
    //   9: iload_2
    //   10: iload_3
    //   11: invokespecial 51	com/android/thememanager/util/WallpaperDecoder:computeSampleSize	(Ljava/lang/String;II)I
    //   14: putfield 54	android/graphics/BitmapFactory$Options:inSampleSize	I
    //   17: aload 5
    //   19: iconst_1
    //   20: putfield 58	android/graphics/BitmapFactory$Options:inPurgeable	Z
    //   23: aload 5
    //   25: iconst_1
    //   26: putfield 61	android/graphics/BitmapFactory$Options:inInputShareable	Z
    //   29: aconst_null
    //   30: astore 4
    //   32: iconst_0
    //   33: istore 7
    //   35: iload 7
    //   37: iconst_1
    //   38: iadd
    //   39: istore 6
    //   41: iload 7
    //   43: iconst_3
    //   44: if_icmpge +53 -> 97
    //   47: aload 4
    //   49: ifnonnull +48 -> 97
    //   52: aconst_null
    //   53: astore 7
    //   55: new 63	java/io/FileInputStream
    //   58: dup
    //   59: aload_1
    //   60: invokespecial 64	java/io/FileInputStream:<init>	(Ljava/lang/String;)V
    //   63: astore 7
    //   65: aload 7
    //   67: invokevirtual 68	java/io/FileInputStream:getFD	()Ljava/io/FileDescriptor;
    //   70: aconst_null
    //   71: aload 5
    //   73: invokestatic 74	android/graphics/BitmapFactory:decodeFileDescriptor	(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    //   76: astore 4
    //   78: aload 4
    //   80: astore 4
    //   82: aload 4
    //   84: ifnonnull +16 -> 100
    //   87: aload 7
    //   89: ifnull +8 -> 97
    //   92: aload 7
    //   94: invokevirtual 77	java/io/FileInputStream:close	()V
    //   97: aload 4
    //   99: areturn
    //   100: aload 7
    //   102: ifnull +152 -> 254
    //   105: aload 7
    //   107: invokevirtual 77	java/io/FileInputStream:close	()V
    //   110: iload 6
    //   112: istore 7
    //   114: goto -79 -> 35
    //   117: pop
    //   118: goto -8 -> 110
    //   121: pop
    //   122: ldc 79
    //   124: new 81	java/lang/StringBuilder
    //   127: dup
    //   128: invokespecial 83	java/lang/StringBuilder:<init>	()V
    //   131: ldc 85
    //   133: invokevirtual 89	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   136: iload 6
    //   138: invokevirtual 92	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   141: aload_1
    //   142: invokevirtual 89	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   145: invokevirtual 96	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   148: invokestatic 102	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
    //   151: pop
    //   152: aload 5
    //   154: iconst_1
    //   155: aload 5
    //   157: getfield 54	android/graphics/BitmapFactory$Options:inSampleSize	I
    //   160: iadd
    //   161: putfield 54	android/graphics/BitmapFactory$Options:inSampleSize	I
    //   164: aload 7
    //   166: ifnull -56 -> 110
    //   169: aload 7
    //   171: invokevirtual 77	java/io/FileInputStream:close	()V
    //   174: goto -64 -> 110
    //   177: pop
    //   178: goto -68 -> 110
    //   181: astore 5
    //   183: aload 5
    //   185: invokevirtual 105	java/lang/Exception:printStackTrace	()V
    //   188: aload 7
    //   190: ifnull -93 -> 97
    //   193: aload 7
    //   195: invokevirtual 77	java/io/FileInputStream:close	()V
    //   198: goto -101 -> 97
    //   201: pop
    //   202: goto -105 -> 97
    //   205: astore 4
    //   207: aload 7
    //   209: ifnull +8 -> 217
    //   212: aload 7
    //   214: invokevirtual 77	java/io/FileInputStream:close	()V
    //   217: aload 4
    //   219: athrow
    //   220: pop
    //   221: goto -124 -> 97
    //   224: pop
    //   225: goto -8 -> 217
    //   228: astore 4
    //   230: aload 7
    //   232: astore 7
    //   234: goto -27 -> 207
    //   237: astore 5
    //   239: aload 7
    //   241: astore 7
    //   243: goto -60 -> 183
    //   246: pop
    //   247: aload 7
    //   249: astore 7
    //   251: goto -129 -> 122
    //   254: goto -144 -> 110
    //
    // Exception table:
    //   from	to	target	type
    //   105	110	117	java/lang/Exception
    //   55	65	121	java/lang/OutOfMemoryError
    //   169	174	177	java/lang/Exception
    //   55	65	181	java/lang/Exception
    //   193	198	201	java/lang/Exception
    //   55	65	205	finally
    //   122	164	205	finally
    //   183	188	205	finally
    //   92	97	220	java/lang/Exception
    //   212	217	224	java/lang/Exception
    //   65	78	228	finally
    //   65	78	237	java/lang/Exception
    //   65	78	246	java/lang/OutOfMemoryError
  }

  /** @deprecated */
  private Bitmap getDesiredBitmap(Bitmap paramBitmap, String paramString, int paramInt, boolean paramBoolean)
  {
    monitorenter;
    while (true)
    {
      try
      {
        Bitmap localBitmap = this.mBitmapCache.removeIdleCache(true, paramInt);
        if (localBitmap != null)
          continue;
        int k = this.mDecodedWidth;
        int i = this.mDecodedHeight;
        bool = false;
        int j = bool + true;
        if (bool >= true)
          continue;
        StringBuilder localStringBuilder;
        try
        {
          localBitmap = Bitmap.createBitmap(k, i, paramBitmap.getConfig());
          localBitmap = localBitmap;
          bool = j;
        }
        catch (OutOfMemoryError localOutOfMemoryError)
        {
          k *= 2;
          k /= 3;
          i = i * 2 / 3;
          localStringBuilder = new StringBuilder().append("should not occur OOM:  currentUsing = ");
          if (paramInt != getCurrentUseBitmapIndex())
            break label204;
        }
        bool = true;
        Log.i("decoder", bool + "  resize to: (" + k + ", " + i + ")");
        bool = j;
        continue;
        if (localBitmap == null)
          continue;
        ImageUtils.cropBitmapToAnother(paramBitmap, localBitmap, true);
        if (!paramBoolean)
          continue;
        this.mBitmapCache.add(paramString, localBitmap, paramInt);
        return localBitmap;
      }
      finally
      {
        monitorexit;
      }
      label204: boolean bool = false;
    }
  }

  public Bitmap decodeLocalImage(String paramString, int paramInt, boolean paramBoolean)
  {
    Bitmap localBitmap2 = getBitmap(paramString);
    if (localBitmap2 == null)
    {
      Bitmap localBitmap1 = decodeOriginBitmapWithNativeMemory(paramString, this.mDecodedWidth, this.mDecodedHeight);
      if (localBitmap1 != null)
      {
        if (this.mBitmapCache.inCacheScope(paramInt))
          localBitmap2 = getDesiredBitmap(localBitmap1, paramString, paramInt, paramBoolean);
        localBitmap1.recycle();
      }
    }
    return localBitmap2;
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     com.android.thememanager.util.WallpaperDecoder
 * JD-Core Version:    0.6.0
 */