package com.android.thememanager.activity;

import android.app.WallpaperInfo;
import android.app.WallpaperManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.preference.Preference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import com.android.thememanager.R.styleable;
import com.android.thememanager.ThemeResourceConstants;
import java.io.File;
import java.text.Collator;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import miui.content.res.ThemeResources;

public class WallpaperPreference extends Preference
  implements ThemeResourceConstants
{
  private Context mContext;
  private Drawable mDesktopMask;
  private int mHeight;
  private Drawable mLockScreenMask;
  private Drawable mNonePreviewImage;
  private WallpaperManager mWallpaperManager;
  private int mWallpaperType;
  private int mWidth;

  public WallpaperPreference(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    setLayoutResource(2130903049);
    this.mContext = paramContext;
    this.mWallpaperManager = ((WallpaperManager)paramContext.getSystemService("wallpaper"));
    TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.WallpaperPreference, 0, 0);
    this.mWallpaperType = localTypedArray.getInt(0, 0);
    localTypedArray.recycle();
    this.mLockScreenMask = paramContext.getResources().getDrawable(2130837519);
    this.mDesktopMask = paramContext.getResources().getDrawable(2130837512);
    this.mNonePreviewImage = paramContext.getResources().getDrawable(2130837524);
    this.mWidth = this.mLockScreenMask.getIntrinsicWidth();
    this.mHeight = this.mLockScreenMask.getIntrinsicHeight();
    setWallpaperIntent();
  }

  private Drawable getFirstLiveWallpapersThumbnail()
  {
    Object localObject = this.mContext.getPackageManager();
    List localList = ((PackageManager)localObject).queryIntentServices(new Intent("android.service.wallpaper.WallpaperService"), 128);
    Collections.sort(localList, new Comparator((PackageManager)localObject)
    {
      final Collator mCollator = Collator.getInstance();

      public int compare(ResolveInfo paramResolveInfo1, ResolveInfo paramResolveInfo2)
      {
        return this.mCollator.compare(paramResolveInfo1.loadLabel(this.val$packageManager), paramResolveInfo2.loadLabel(this.val$packageManager));
      }
    });
    WallpaperInfo localWallpaperInfo = null;
    if (localList.size() > 0);
    try
    {
      localWallpaperInfo = new WallpaperInfo(this.mContext, (ResolveInfo)localList.get(0));
      localWallpaperInfo = localWallpaperInfo;
      if (localWallpaperInfo != null)
      {
        localObject = localWallpaperInfo.loadThumbnail((PackageManager)localObject);
        return localObject;
      }
    }
    catch (Exception localException)
    {
      while (true)
      {
        localException.printStackTrace();
        continue;
        localObject = null;
      }
    }
  }

  private Drawable getFirstStaticWallpapersThumbnail()
  {
    Object localObject = "/system/media/wallpaper/";
    String[] arrayOfString = new File((String)localObject).list();
    if ((arrayOfString == null) || (arrayOfString.length == 0))
    {
      localObject = DOWNLOADED_WALLPAPER_PATH;
      arrayOfString = new File((String)localObject).list();
    }
    if ((arrayOfString == null) || (arrayOfString.length <= 0))
    {
      localObject = null;
    }
    else
    {
      Arrays.sort(arrayOfString);
      localObject = Drawable.createFromPath((String)localObject + "/" + arrayOfString[0]);
    }
    return (Drawable)localObject;
  }

  private Drawable getFirstWallpapersThumbnail(int paramInt)
  {
    Drawable localDrawable = null;
    if (paramInt != 0)
    {
      if (paramInt == 2)
        localDrawable = getFirstLiveWallpapersThumbnail();
    }
    else
      localDrawable = getFirstStaticWallpapersThumbnail();
    return localDrawable;
  }

  private void setWallpaperIntent()
  {
    Intent localIntent = new Intent();
    if (this.mWallpaperType != 0)
    {
      if (this.mWallpaperType != 1)
      {
        localIntent.setClassName("com.android.wallpaper.livepicker", "com.android.wallpaper.livepicker.LiveWallpaperActivity");
      }
      else
      {
        localIntent.putExtra("REQUEST_RESOURCE_TYPE", 4L);
        localIntent.setClassName(this.mContext, ThemeTabActivity.class.getName());
      }
    }
    else
    {
      localIntent.putExtra("REQUEST_RESOURCE_TYPE", 2L);
      localIntent.setClassName(this.mContext, ThemeTabActivity.class.getName());
    }
    setIntent(localIntent);
  }

  protected void onBindView(View paramView)
  {
    super.onBindView(paramView);
    ImageView localImageView2 = (ImageView)paramView.findViewById(2131165202);
    ImageView localImageView1 = (ImageView)paramView.findViewById(2131165203);
    localImageView2.getLayoutParams().width = this.mWidth;
    localImageView2.getLayoutParams().height = this.mHeight;
    localImageView2.setScaleType(ImageView.ScaleType.CENTER_CROP);
    if (this.mWallpaperType != 1)
    {
      WallpaperInfo localWallpaperInfo = this.mWallpaperManager.getWallpaperInfo();
      Drawable localDrawable = null;
      int i = 0;
      if (this.mWallpaperType != 0)
      {
        if (localWallpaperInfo != null)
        {
          localDrawable = localWallpaperInfo.loadThumbnail(this.mContext.getPackageManager());
          localImageView1.setImageDrawable(this.mDesktopMask);
          i = 1;
        }
      }
      else if (localWallpaperInfo == null)
      {
        localDrawable = this.mWallpaperManager.getDrawable();
        localImageView1.setImageDrawable(this.mDesktopMask);
        i = 1;
      }
      if (i == 0)
        localDrawable = getFirstWallpapersThumbnail(this.mWallpaperType);
      if (localDrawable == null)
        localDrawable = this.mNonePreviewImage;
      localImageView2.setImageDrawable(localDrawable);
    }
    else
    {
      localImageView2.setImageDrawable(ThemeResources.getLockWallpaperCache(getContext()));
      localImageView1.setImageDrawable(this.mLockScreenMask);
    }
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     com.android.thememanager.activity.WallpaperPreference
 * JD-Core Version:    0.6.0
 */