package com.android.thememanager.activity;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.thememanager.ThemeResourceConstants;
import com.android.thememanager.util.ConstantsHelper;
import com.android.thememanager.util.ThemeApplyParameters;
import com.android.thememanager.util.ThemeHelper;
import com.android.thememanager.util.ThemeOperationHandler;
import com.android.thememanager.view.FixedHeightGridView;
import com.android.thememanager.view.FixedHeightGridView.FixedGridAdapter;
import java.util.ArrayList;
import miui.app.ObservableActivity;
import miui.app.SDCardMonitor;
import miui.app.SDCardMonitor.SDCardStatusListener;
import miui.resourcebrowser.AppInnerContext;
import miui.resourcebrowser.ResourceContext;
import miui.resourcebrowser.model.Resource;
import miui.resourcebrowser.util.ResourceDownloadHandler.ResourceDownloadListener;
import miui.resourcebrowser.util.ResourceHelper;
import miui.resourcebrowser.view.ResourceOperationHandler;
import miui.resourcebrowser.view.ResourceOperationView;
import miui.resourcebrowser.view.ResourceOperationView.ResourceOperationListener;

public class ThemeComponentApplyActivity extends ObservableActivity
  implements ThemeResourceConstants, SDCardMonitor.SDCardStatusListener, ResourceDownloadHandler.ResourceDownloadListener, ResourceOperationView.ResourceOperationListener
{
  private FixedHeightGridView mComponentGridView;
  private TextView mComponentNumText;
  private ResourceOperationHandler mOperationHandler;
  private ResourceContext mResContext;
  private Resource mResource;
  private long mResourceType;
  private SDCardMonitor mSDCardMonitor;
  private int mSourceType;

  private long getGridComponentShowingFlags()
  {
    int i = this.mResource.getPlatform();
    String str = this.mResource.getExtraMeta("modulesFlag");
    long l;
    if (str == null)
      l = 0L;
    else
      l = Long.parseLong(l);
    if (l == -1L)
      l = ThemeHelper.getAllComponentsCombineFlag();
    if (this.mResourceType == -1L)
      l |= 268435456L;
    if (!ThemeHelper.supportReplaceFont())
      l &= -262161L;
    if (!ThemeHelper.supportReplaceAudioEffect())
      l &= -32769L;
    return ThemeHelper.getCompatibleFlag(i, l);
  }

  private ThemeOperationHandler getOperationHandler(ResourceOperationView paramResourceOperationView)
  {
    return new ThemeOperationHandler(this, this.mResContext, paramResourceOperationView);
  }

  private long getSelectComponentFlags()
  {
    long l = -1L;
    ComponentGridsAdapter localComponentGridsAdapter = (ComponentGridsAdapter)this.mComponentGridView.getAdapter();
    if ((this.mResourceType != l) || (!localComponentGridsAdapter.isSelectAllComponent()))
      l = localComponentGridsAdapter.getSelectComponentFlag();
    return l;
  }

  private ThemeApplyParameters getThemeApplyParameters()
  {
    ThemeApplyParameters localThemeApplyParameters = new ThemeApplyParameters();
    localThemeApplyParameters.applyFlags = getSelectComponentFlags();
    localThemeApplyParameters.removeOthers = needRemoveAllOldTheme();
    return localThemeApplyParameters;
  }

  private boolean needRemoveAllOldTheme()
  {
    boolean bool;
    if ((this.mComponentGridView != null) && (this.mResourceType == -1L))
      bool = ((ComponentGridsAdapter)this.mComponentGridView.getAdapter()).needRemoveAllOldTheme();
    else
      bool = false;
    return bool;
  }

  private void setComponentGridViewClickable(boolean paramBoolean)
  {
    ((ComponentGridsAdapter)this.mComponentGridView.getAdapter()).setClickable(paramBoolean);
  }

  private void setupComponentGridView()
  {
    this.mComponentGridView = ((FixedHeightGridView)findViewById(2131165193));
    this.mComponentGridView.setAdapter(new ComponentGridsAdapter(getGridComponentShowingFlags(), this.mOperationHandler.isLocalResource(), this.mComponentGridView));
    this.mComponentGridView.setEnabled(false);
    this.mComponentGridView.setSelector(new ColorDrawable(0));
    this.mComponentGridView.setNumColumns(3);
    this.mComponentGridView.setVisibility(0);
    updateComponentNumber();
  }

  private void setupUI()
  {
    Object localObject = getActionBar();
    ((ActionBar)localObject).setHomeButtonEnabled(true);
    ((ActionBar)localObject).setTitle(this.mResource.getTitle());
    localObject = (ResourceOperationView)findViewById(101384281);
    ((ResourceOperationView)localObject).setMagicButtonResource(100794796);
    ((ResourceOperationView)localObject).setResourceOperationListener(this);
    this.mOperationHandler = getOperationHandler((ResourceOperationView)localObject);
    this.mOperationHandler.setResourceController(AppInnerContext.getInstance().getResourceController());
    addObserver(this.mOperationHandler);
    this.mOperationHandler.setResource(this.mResource);
    setupComponentGridView();
  }

  private void updateComponentNumber()
  {
    this.mComponentNumText = ((TextView)findViewById(2131165192));
    int i = ThemeHelper.getComponentNumber(((ComponentGridsAdapter)this.mComponentGridView.getAdapter()).getSelectComponentFlag());
    TextView localTextView;
    Object[] arrayOfObject;
    if (!this.mOperationHandler.isLocalResource())
    {
      localTextView = this.mComponentNumText;
      arrayOfObject = new Object[1];
      arrayOfObject[0] = Integer.valueOf(i);
      localTextView.setText(getString(2131427379, arrayOfObject));
    }
    else
    {
      localTextView = this.mComponentNumText;
      arrayOfObject = new Object[1];
      arrayOfObject[0] = Integer.valueOf(i);
      localTextView.setText(getString(2131427378, arrayOfObject));
    }
  }

  public void finish()
  {
    super.finish();
    overridePendingTransition(0, 0);
  }

  public void onApplyEventPerformed()
  {
    ((ThemeOperationHandler)this.mOperationHandler).setApplyParameters(getThemeApplyParameters());
  }

  public void onBuyEventPerformed()
  {
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903043);
    Intent localIntent = getIntent();
    this.mResContext = AppInnerContext.getInstance().getResourceContext();
    this.mResource = ((Resource)localIntent.getSerializableExtra("REQUEST_SELECTING_THEME"));
    if (this.mResource != null)
    {
      this.mResourceType = ((Long)this.mResContext.getExtraMeta("EXTRA_CTX_RESOURCE_TYPE", Long.valueOf(-1L))).longValue();
      this.mSourceType = localIntent.getIntExtra("REQUEST_SOURCE_TYPE", 1);
      this.mSDCardMonitor = SDCardMonitor.getSDCardMonitor(this);
      this.mSDCardMonitor.addListener(this);
      setupUI();
    }
    else
    {
      finish();
    }
  }

  public void onDeleteEventPerformed()
  {
    if (this.mSourceType != 1)
    {
      setComponentGridViewClickable(false);
    }
    else
    {
      Intent localIntent = new Intent();
      localIntent.putExtra("RESPONSE_NEEDS_DELETE", true);
      setResult(104, localIntent);
      finish();
    }
  }

  protected void onDestroy()
  {
    if (this.mSDCardMonitor != null)
      this.mSDCardMonitor.removeListener(this);
    super.onDestroy();
  }

  public void onDownloadEventPerformed()
  {
  }

  public void onDownloadFailed(String paramString1, String paramString2)
  {
  }

  public void onDownloadProgressUpdated(String paramString1, String paramString2, int paramInt1, int paramInt2)
  {
  }

  public void onDownloadSuccessful(String paramString1, String paramString2)
  {
    setComponentGridViewClickable(true);
  }

  public void onMagicEventPerformed()
  {
    finish();
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    if (paramMenuItem.getItemId() == 16908332)
      finish();
    return super.onOptionsItemSelected(paramMenuItem);
  }

  public void onPickEventPerformed()
  {
  }

  public void onStatusChanged(boolean paramBoolean)
  {
    ResourceHelper.exit(this);
  }

  public void onUpdateEventPerformed()
  {
  }

  public class ComponentGridsAdapter extends FixedHeightGridView.FixedGridAdapter
  {
    private long mExistFlag;
    private int mItemCount;
    private ArrayList<Long> mOrderComponentFlag;
    private boolean mResourceLocal;
    private long mSelectFlag;

    public ComponentGridsAdapter(long arg2, boolean paramFixedHeightGridView, FixedHeightGridView arg5)
    {
      super();
      this.mExistFlag = ???;
      this.mOrderComponentFlag = new ArrayList();
      for (int i = 0; ; i++)
      {
        if (i >= ThemeResourceConstants.COMPONENT_SELECT_ORDER.length)
        {
          if ((0x10000000 & this.mExistFlag) != 0L)
            this.mOrderComponentFlag.add(Long.valueOf(268435456L));
          this.mItemCount = (3 * ((-1 + (3 + this.mOrderComponentFlag.size())) / 3));
          setClickable(paramFixedHeightGridView);
          return;
        }
        long l = ThemeResourceConstants.COMPONENT_SELECT_ORDER[i];
        if ((l & this.mExistFlag) == 0L)
          continue;
        this.mOrderComponentFlag.add(Long.valueOf(l));
      }
    }

    public View getChildView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      View localView;
      if (paramView != null)
        localView = paramView;
      else
        localView = ThemeComponentApplyActivity.this.getLayoutInflater().inflate(2130903045, null);
      int i = this.mOrderComponentFlag.size();
      long l;
      if (paramInt < i)
        l = ((Long)this.mOrderComponentFlag.get(paramInt)).longValue();
      else
        l = 0L;
      int m;
      if (paramInt >= i)
        m = 0;
      else
        m = 1;
      if ((m == 0) || (!this.mResourceLocal))
        i = 0;
      else
        i = 1;
      if ((i == 0) || ((l & this.mSelectFlag) == 0L))
        i = 0;
      else
        i = 1;
      TextView localTextView = (TextView)localView.findViewById(2131165194);
      if (m == 0)
        m = 8;
      else
        m = 0;
      localTextView.setVisibility(m);
      int j = ConstantsHelper.getTitleResId(l);
      if (j == 0)
        localTextView.setText("");
      else
        localTextView.setText(j);
      ImageView localImageView = (ImageView)localView.findViewById(2131165196);
      int k;
      if (i == 0)
        k = 4;
      else
        k = 0;
      localImageView.setVisibility(k);
      localImageView.setSelected(i);
      localView.setTag(Integer.valueOf(paramInt));
      localView.setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramView)
        {
          int i = ((Integer)paramView.getTag()).intValue();
          if (i < ThemeComponentApplyActivity.ComponentGridsAdapter.this.mOrderComponentFlag.size())
          {
            if (!((ImageView)paramView.findViewById(2131165196)).isSelected())
              ThemeComponentApplyActivity.ComponentGridsAdapter.access$178(ThemeComponentApplyActivity.ComponentGridsAdapter.this, ((Long)ThemeComponentApplyActivity.ComponentGridsAdapter.this.mOrderComponentFlag.get(i)).longValue());
            else
              ThemeComponentApplyActivity.ComponentGridsAdapter.access$174(ThemeComponentApplyActivity.ComponentGridsAdapter.this, 0xFFFFFFFF ^ ((Long)ThemeComponentApplyActivity.ComponentGridsAdapter.this.mOrderComponentFlag.get(i)).longValue());
            ThemeComponentApplyActivity.ComponentGridsAdapter.this.notifyDataSetInvalidated();
            ThemeComponentApplyActivity.this.updateComponentNumber();
          }
        }
      });
      if (i == 0)
        localView.setBackgroundResource(2130837507);
      else
        localView.setBackgroundResource(2130837508);
      return localView;
    }

    public final int getCount()
    {
      return this.mItemCount;
    }

    public final Object getItem(int paramInt)
    {
      return Integer.valueOf(0);
    }

    public final long getItemId(int paramInt)
    {
      return paramInt;
    }

    public long getSelectComponentFlag()
    {
      return this.mSelectFlag;
    }

    public boolean isSelectAllComponent()
    {
      int i;
      if (this.mSelectFlag != this.mExistFlag)
        i = 0;
      else
        i = 1;
      return i;
    }

    public boolean needRemoveAllOldTheme()
    {
      int i;
      if ((0xFFFFF8FF & this.mSelectFlag) != (0xFFFFF8FF & this.mExistFlag))
        i = 0;
      else
        i = 1;
      return i;
    }

    public void setClickable(boolean paramBoolean)
    {
      this.mResourceLocal = paramBoolean;
      this.mSelectFlag = this.mExistFlag;
      if (this.mResourceLocal)
        this.mSelectFlag = (0xFFFFF8FF & this.mSelectFlag);
      notifyDataSetInvalidated();
    }
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     com.android.thememanager.activity.ThemeComponentApplyActivity
 * JD-Core Version:    0.6.0
 */