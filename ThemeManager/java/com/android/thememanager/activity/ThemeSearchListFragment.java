package com.android.thememanager.activity;

import com.android.thememanager.ThemeResourceConstants;
import com.android.thememanager.util.ThemeAudioBatchHandler;
import com.android.thememanager.util.ThemeHelper;
import com.android.thememanager.util.UIHelper;
import miui.resourcebrowser.ResourceContext;
import miui.resourcebrowser.activity.ResourceAdapter;
import miui.resourcebrowser.activity.ResourceSearchListFragment;
import miui.resourcebrowser.util.BatchResourceHandler;

public class ThemeSearchListFragment extends ResourceSearchListFragment
  implements ThemeResourceConstants
{
  private long mResourceType;

  private void refreshCurrentUsingFlags()
  {
    String str = UIHelper.computeCurrentUsingPath(this.mActivity, this.mResourceType);
    this.mResContext.setCurrentUsingPath(str);
    this.mAdapter.notifyDataSetChanged();
  }

  protected BatchResourceHandler getBatchOperationHandler()
  {
    Object localObject;
    if (!UIHelper.isAudioResource(this.mResourceType))
      localObject = super.getBatchOperationHandler();
    else
      localObject = new ThemeAudioBatchHandler(this, this.mAdapter, this.mResContext, this.mResourceType);
    return (BatchResourceHandler)localObject;
  }

  protected void initParams()
  {
    this.mResourceType = ((Long)this.mResContext.getExtraMeta("EXTRA_CTX_RESOURCE_TYPE", Long.valueOf(-1L))).longValue();
    ThemeHelper.setMusicVolumeType(this.mActivity, this.mResourceType);
    super.initParams();
  }

  protected void onVisible()
  {
    super.onVisible();
    refreshCurrentUsingFlags();
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     com.android.thememanager.activity.ThemeSearchListFragment
 * JD-Core Version:    0.6.0
 */