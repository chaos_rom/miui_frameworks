package com.android.thememanager.activity;

import android.app.Activity;
import android.content.Intent;
import com.android.thememanager.ThemeResourceConstants;
import com.android.thememanager.util.ThemeAudioBatchHandler;
import com.android.thememanager.util.ThemeHelper;
import com.android.thememanager.util.UIHelper;
import miui.resourcebrowser.ResourceContext;
import miui.resourcebrowser.activity.OnlineResourceListFragment;
import miui.resourcebrowser.activity.ResourceAdapter;
import miui.resourcebrowser.controller.online.OnlineService;
import miui.resourcebrowser.util.BatchResourceHandler;

public class OnlineThemeListFragment extends OnlineResourceListFragment
  implements ThemeResourceConstants
{
  private long mResourceType;

  private void refreshCurrentUsingFlags()
  {
    String str = UIHelper.computeCurrentUsingPath(this.mActivity, this.mResourceType);
    this.mResContext.setCurrentUsingPath(str);
    this.mAdapter.notifyDataSetChanged();
  }

  protected BatchResourceHandler getBatchOperationHandler()
  {
    Object localObject;
    if (!UIHelper.isAudioResource(this.mResourceType))
      localObject = super.getBatchOperationHandler();
    else
      localObject = new ThemeAudioBatchHandler(this, this.mAdapter, this.mResContext, this.mResourceType);
    return (BatchResourceHandler)localObject;
  }

  protected void initParams()
  {
    this.mResourceType = ((Long)this.mResContext.getExtraMeta("EXTRA_CTX_RESOURCE_TYPE", Long.valueOf(-1L))).longValue();
    ThemeHelper.setMusicVolumeType(this.mActivity, this.mResourceType);
    super.initParams();
    String str = (String)this.mResContext.getExtraMeta("EXTRA_CTX_GADGET_FLAG");
    if (!this.mIsRecommendList)
    {
      this.mResContext.setListUrl(this.mService.getCommonListUrl(str));
    }
    else
    {
      this.mRecommendId = this.mActivity.getIntent().getStringExtra("REQUEST_RECOMMEND_ID");
      this.mResContext.setListUrl(this.mService.getRecommendListUrl(this.mRecommendId, str));
    }
  }

  protected void onVisible()
  {
    super.onVisible();
    refreshCurrentUsingFlags();
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     com.android.thememanager.activity.OnlineThemeListFragment
 * JD-Core Version:    0.6.0
 */