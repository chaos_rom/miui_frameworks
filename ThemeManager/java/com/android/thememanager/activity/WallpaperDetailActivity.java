package com.android.thememanager.activity;

import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Pair;
import android.util.Pair<Ljava.lang.String;Ljava.lang.Boolean;>;
import android.view.Display;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.TextView;
import android.widget.Toast;
import com.android.thememanager.ThemeResourceConstants;
import com.android.thememanager.util.ThemeHelper;
import com.android.thememanager.util.WallpaperDecoder;
import com.android.thememanager.util.WallpaperUtils;
import com.android.thememanager.view.HorzontalSliderView;
import com.android.thememanager.view.HorzontalSliderView.SliderMoveListener;
import com.android.thememanager.view.WallpaperView;
import com.android.thememanager.view.WallpaperView.WallpaperSwitchListener;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import miui.resourcebrowser.ResourceContext;
import miui.resourcebrowser.activity.ResourceDetailActivity;
import miui.resourcebrowser.controller.LocalDataManager;
import miui.resourcebrowser.controller.ResourceController;
import miui.resourcebrowser.model.PathEntry;
import miui.resourcebrowser.model.Resource;
import miui.resourcebrowser.util.ImageCacheDecoder;
import miui.resourcebrowser.util.ImageCacheDecoder.ImageDecodingListener;
import miui.resourcebrowser.util.ResourceHelper;
import miui.resourcebrowser.widget.DataGroup;

public class WallpaperDetailActivity extends ResourceDetailActivity
  implements ThemeResourceConstants
{
  private TextView mDownloadBtn;
  private GestureDetector mGestureDetector;
  private ImageCacheDecoder mImageAsyncDecoder;
  private boolean mIsLockscreen;
  private ImageView mMoreMenuBtn;
  private View mOperateBarView;
  private PopupWindow mPopupWindow;
  private ImageView mPreviewMaskView;
  private long mResourceType;
  private HorzontalSliderView mSliderView;
  private boolean mThumbnailModeOfWallpaperBeforePreview;
  private View mTitleAreaView;
  private int mWallpaperHeight;
  private WallpaperView mWallpaperView;
  private int mWallpaperWidth;

  private void autoUpdateSliderViewState(boolean paramBoolean)
  {
    if ((!paramBoolean) || (!this.mWallpaperView.isThumbnailScanMode()))
      updateSliderViewState(paramBoolean);
  }

  private Pair<String, Boolean> cacheWallpaperResource(int paramInt, boolean paramBoolean)
  {
    Object localObject1 = null;
    Resource localResource = getAdjResource(paramInt);
    Pair localPair;
    if (localResource != null)
    {
      int i = paramInt + this.mResourceIndex;
      String str1 = localResource.getLocalPath();
      if ((!new File(str1).exists()) && (this.mIsOnlineResourceSet))
        str1 = getLocalPreviewPath(localResource);
      boolean bool = false;
      Object localObject2 = str1;
      if ((str1 == null) || (!new File(str1).exists()))
      {
        String str2 = getLocalThumbnailPath(localResource);
        if ((this.mIsOnlineResourceSet) && (str2 != null) && (new File(str2).exists()))
        {
          this.mImageAsyncDecoder.decodeImageAsync(str2, getOnlineThumbnailUrl(localResource), i);
          localObject2 = str2;
          bool = true;
        }
        if (!TextUtils.isEmpty(getOnlinePreviewUrl(localResource)))
          this.mImageAsyncDecoder.decodeImageAsync(str1, getOnlinePreviewUrl(localResource), i);
      }
      else if (!paramBoolean)
      {
        this.mImageAsyncDecoder.decodeLocalImage(str1, i, true);
      }
      else
      {
        this.mImageAsyncDecoder.decodeImageAsync(str1, null, i);
      }
      localPair = new Pair(localObject2, Boolean.valueOf(bool));
    }
    return (Pair<String, Boolean>)localPair;
  }

  private String checkInfoValue(String paramString)
  {
    if (TextUtils.isEmpty(paramString))
      paramString = getString(101449740);
    return paramString;
  }

  private void doApplyWallpaper(long paramLong, Resource paramResource)
  {
    String str = paramResource.getLocalPath();
    Bitmap localBitmap = this.mImageAsyncDecoder.getBitmap(str);
    if (localBitmap == null)
      localBitmap = this.mImageAsyncDecoder.getBitmap(getLocalPreviewPath(paramResource));
    boolean bool1 = true;
    boolean bool2 = true;
    if ((0x2 & paramLong) != 0L);
    try
    {
      bool1 = WallpaperUtils.saveDeskWallpaperByDisplay(this, localBitmap, Uri.fromFile(new File(str)));
      if ((0x4 & paramLong) != 0L)
      {
        bool2 = WallpaperUtils.saveLockWallpaperByDisplay(this, localBitmap, Uri.fromFile(new File(str)));
        break label126;
        while (true)
        {
          ThemeHelper.showThemeChangedToast(this, bool1);
          return;
          bool1 = false;
        }
      }
    }
    catch (Exception localException)
    {
      while (true)
      {
        continue;
        label126: if ((!bool1) || (!bool2))
          continue;
        bool1 = true;
      }
    }
  }

  private void enterPreviewMode()
  {
    this.mThumbnailModeOfWallpaperBeforePreview = this.mWallpaperView.isThumbnailScanMode();
    if (this.mThumbnailModeOfWallpaperBeforePreview)
    {
      WallpaperView localWallpaperView = this.mWallpaperView;
      boolean bool;
      if (this.mThumbnailModeOfWallpaperBeforePreview)
        bool = false;
      else
        bool = true;
      localWallpaperView.setScanMode(bool);
    }
    this.mPreviewMaskView.startAnimation(getAnimation(false, true, this.mPreviewMaskView.getWidth(), 0, 300L));
    this.mPreviewMaskView.setVisibility(0);
    updateTitleAndOperateBarState(false);
    updateSliderViewState(false);
  }

  private Resource getAdjResource(int paramInt)
  {
    int i = paramInt + this.mResourceIndex;
    Resource localResource;
    if ((i < 0) || (i >= this.mGroupDataSet.size()))
      localResource = null;
    else
      localResource = (Resource)this.mGroupDataSet.get(localResource);
    return localResource;
  }

  private Animation getAnimation(boolean paramBoolean1, boolean paramBoolean2, int paramInt1, int paramInt2)
  {
    return getAnimation(paramBoolean1, paramBoolean2, paramInt1, paramInt2, 200L);
  }

  private Animation getAnimation(boolean paramBoolean1, boolean paramBoolean2, int paramInt1, int paramInt2, long paramLong)
  {
    AlphaAnimation localAlphaAnimation;
    if (!paramBoolean1)
    {
      int i;
      if (!paramBoolean2)
        i = 0;
      else
        i = paramInt1;
      int m = paramInt1 - i;
      int j;
      if (!paramBoolean2)
        j = 0;
      else
        j = paramInt2;
      int k = paramInt2 - j;
      TranslateAnimation localTranslateAnimation = new TranslateAnimation(i, m, j, k);
    }
    else
    {
      float f;
      if (!paramBoolean2)
        f = 1.0F;
      else
        f = 0.0F;
      localAlphaAnimation = new AlphaAnimation(f, 1.0F - f);
    }
    localAlphaAnimation.setDuration(paramLong);
    return localAlphaAnimation;
  }

  private ImageCacheDecoder.ImageDecodingListener getImageDecodingListener()
  {
    return new ImageCacheDecoder.ImageDecodingListener()
    {
      public void handleDecodingResult(boolean paramBoolean, String paramString1, String paramString2)
      {
        if (!paramBoolean)
        {
          if (TextUtils.equals(WallpaperDetailActivity.this.mResource.getLocalPath(), paramString1))
            Toast.makeText(WallpaperDetailActivity.this, 2131427401, 0).show();
        }
        else if (WallpaperDetailActivity.this.isVisiableImagePath(paramString1))
          WallpaperDetailActivity.this.initWallpaperViewBitmap();
      }

      public void handleDownloadResult(boolean paramBoolean, String paramString1, String paramString2)
      {
        boolean bool = TextUtils.equals(paramString2, WallpaperDetailActivity.this.getOnlinePreviewUrl(WallpaperDetailActivity.this.mResource));
        if (!paramBoolean)
        {
          if (bool)
            Toast.makeText(WallpaperDetailActivity.this, 101449760, 0).show();
        }
        else
        {
          if (WallpaperDetailActivity.this.isVisiableImagePath(paramString1))
            WallpaperDetailActivity.this.initWallpaperViewBitmap();
          if ((bool) && (WallpaperDetailActivity.this.mDownloadBtn.getText().equals(WallpaperDetailActivity.this.getString(101449752))))
            WallpaperDetailActivity.this.mDownloadBtn.performClick();
        }
      }
    };
  }

  private String getLocalPreviewPath(Resource paramResource)
  {
    Object localObject = paramResource.getPreviews();
    if ((localObject != null) && (((List)localObject).size() > 0))
    {
      localObject = (PathEntry)((List)localObject).get(0);
      if (localObject != null);
    }
    else
    {
      localObject = null;
      break label43;
    }
    localObject = ((PathEntry)localObject).getLocalPath();
    label43: return (String)localObject;
  }

  private String getLocalThumbnailPath(Resource paramResource)
  {
    Object localObject = paramResource.getThumbnails();
    if ((localObject != null) && (((List)localObject).size() > 0))
    {
      localObject = (PathEntry)((List)localObject).get(0);
      if (localObject != null);
    }
    else
    {
      localObject = null;
      break label43;
    }
    localObject = ((PathEntry)localObject).getLocalPath();
    label43: return (String)localObject;
  }

  private String getOnlinePreviewUrl(Resource paramResource)
  {
    Object localObject = paramResource.getPreviews();
    if ((localObject != null) && (((List)localObject).size() > 0))
    {
      localObject = (PathEntry)((List)localObject).get(0);
      if (localObject != null);
    }
    else
    {
      localObject = null;
      break label43;
    }
    localObject = ((PathEntry)localObject).getOnlinePath();
    label43: return (String)localObject;
  }

  private String getOnlineThumbnailUrl(Resource paramResource)
  {
    Object localObject = paramResource.getThumbnails();
    if ((localObject != null) && (((List)localObject).size() > 0))
    {
      localObject = (PathEntry)((List)localObject).get(0);
      if (localObject != null);
    }
    else
    {
      localObject = null;
      break label43;
    }
    localObject = ((PathEntry)localObject).getOnlinePath();
    label43: return (String)localObject;
  }

  private HorzontalSliderView.SliderMoveListener getSliderMoveListener()
  {
    return new HorzontalSliderView.SliderMoveListener()
    {
      public void movePercent(float paramFloat, boolean paramBoolean)
      {
        WallpaperDetailActivity.this.mWallpaperView.updateCurrentWallpaperShowingArea(paramFloat, paramBoolean);
      }
    };
  }

  private WallpaperView.WallpaperSwitchListener getWallpaperSwitchListener()
  {
    return new WallpaperView.WallpaperSwitchListener()
    {
      public void switchNext()
      {
        WallpaperDetailActivity.this.navigateToNextResource();
      }

      public void switchNone()
      {
      }

      public void switchPrevious()
      {
        WallpaperDetailActivity.this.navigateToPreviousResource();
      }
    };
  }

  private void initWallpaperViewBitmap()
  {
    if (this.mImageAsyncDecoder != null)
    {
      this.mImageAsyncDecoder.setCurrentUseBitmapIndex(this.mResourceIndex);
      initWallpaperViewBitmap(0, true);
      initWallpaperViewBitmap(1, true);
      initWallpaperViewBitmap(-1, true);
      this.mWallpaperView.invalidate();
    }
  }

  private void initWallpaperViewBitmap(int paramInt, boolean paramBoolean)
  {
    Object localObject = cacheWallpaperResource(paramInt, paramBoolean);
    boolean bool1;
    if (localObject == null)
      bool1 = false;
    else
      bool1 = ((Boolean)((Pair)localObject).second).booleanValue();
    ImageCacheDecoder localImageCacheDecoder = this.mImageAsyncDecoder;
    if (localObject == null)
      localObject = null;
    else
      localObject = (String)((Pair)localObject).first;
    Bitmap localBitmap = localImageCacheDecoder.getBitmap((String)localObject);
    int i = paramInt + this.mResourceIndex;
    if ((localBitmap != null) || (this.mWallpaperView.getUserGivenId(paramInt) != i) || (!this.mWallpaperView.showingDeterminateFg(paramInt)))
    {
      boolean bool2;
      if ((i < 0) || (i >= this.mDataSet.size()))
        bool2 = false;
      else
        bool2 = true;
      this.mWallpaperView.setBitmapInfo(paramInt, localBitmap, i, bool2, bool1);
    }
  }

  private boolean isVisiableImagePath(String paramString)
  {
    int i = 0;
    if ((pointSameImage(getAdjResource(0), paramString)) || (pointSameImage(getAdjResource(1), paramString)) || (pointSameImage(getAdjResource(-1), paramString)))
      i = 1;
    return i;
  }

  private boolean pointSameImage(Resource paramResource, String paramString)
  {
    int i = 0;
    if (paramResource != null)
      if (!TextUtils.equals(paramResource.getLocalPath(), paramString))
      {
        if (!TextUtils.equals(getLocalPreviewPath(paramResource), paramString))
        {
          if (TextUtils.equals(getLocalThumbnailPath(paramResource), paramString))
            i = 1;
        }
        else
          i = 1;
      }
      else
        i = 1;
    return i;
  }

  private void quitPreviewMode(boolean paramBoolean)
  {
    this.mPreviewMaskView.setVisibility(4);
    if (paramBoolean)
      this.mPreviewMaskView.startAnimation(getAnimation(false, false, this.mPreviewMaskView.getWidth(), 0, 200L));
    updateTitleAndOperateBarState(true);
    if (!this.mThumbnailModeOfWallpaperBeforePreview)
      updateSliderViewState(true);
    else
      this.mWallpaperView.setScanMode(this.mThumbnailModeOfWallpaperBeforePreview);
  }

  private void saveUserScanMode()
  {
    SharedPreferences.Editor localEditor = getSharedPreferences("wallpaper_scan_config", 0).edit();
    localEditor.putBoolean("scan_mode_thumbnail", this.mWallpaperView.isThumbnailScanMode());
    localEditor.commit();
  }

  private void updateSliderViewState(boolean paramBoolean)
  {
    int i = 4;
    if (!this.mIsLockscreen)
    {
      if (paramBoolean)
        i = 0;
      if (this.mSliderView.getVisibility() != i)
      {
        this.mSliderView.startAnimation(getAnimation(true, paramBoolean, 0, 0));
        this.mSliderView.setVisibility(i);
      }
    }
    else
    {
      this.mSliderView.setVisibility(i);
    }
  }

  private void updateTitleAndOperateBarState(boolean paramBoolean)
  {
    int i = 0;
    this.mTitleAreaView.startAnimation(getAnimation(false, paramBoolean, 0, -this.mTitleAreaView.getHeight()));
    View localView2 = this.mTitleAreaView;
    int j;
    if (!paramBoolean)
      j = 4;
    else
      j = 0;
    localView2.setVisibility(j);
    this.mOperateBarView.startAnimation(getAnimation(false, paramBoolean, 0, this.mOperateBarView.getTop()));
    View localView1 = this.mOperateBarView;
    if (!paramBoolean)
      i = 4;
    localView1.setVisibility(i);
  }

  private boolean userUseThumbnailScanModeLastTime()
  {
    return getSharedPreferences("wallpaper_scan_config", 0).getBoolean("scan_mode_thumbnail", false);
  }

  protected void bindScreenView()
  {
    initWallpaperViewBitmap();
  }

  protected void changeCurrentResource()
  {
    requestResourceDetail(this.mResourceIndex);
    requestResourceDetail(1 + this.mResourceIndex);
    requestResourceDetail(-1 + this.mResourceIndex);
    super.changeCurrentResource();
    TextView localTextView = (TextView)findViewById(2131165194);
    if (localTextView != null)
      localTextView.setText(this.mResource.getTitle());
  }

  protected int getContentView()
  {
    return 2130903050;
  }

  protected void initParams()
  {
    super.initParams();
    this.mResourceType = ((Long)this.mResContext.getExtraMeta("EXTRA_CTX_RESOURCE_TYPE", Long.valueOf(-1L))).longValue();
    boolean bool;
    if (this.mResourceType != 4L)
      bool = false;
    else
      bool = true;
    this.mIsLockscreen = bool;
  }

  public void onBackPressed()
  {
    if (this.mPreviewMaskView.getVisibility() != 0)
      super.onBackPressed();
    else
      quitPreviewMode(true);
  }

  protected void onDestroy()
  {
    super.onDestroy();
    if (this.mImageAsyncDecoder != null)
      this.mImageAsyncDecoder.clean(true);
    saveUserScanMode();
  }

  protected void onStart()
  {
    super.onStart();
    initWallpaperViewBitmap();
  }

  protected void onStop()
  {
    super.onStop();
    if (this.mImageAsyncDecoder != null)
    {
      this.mImageAsyncDecoder.clean(false);
      this.mWallpaperView.reset();
    }
  }

  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    boolean bool;
    if (this.mWallpaperView.hasBeenInitied())
    {
      bool = this.mGestureDetector.onTouchEvent(paramMotionEvent);
      if (paramMotionEvent.getAction() == 1)
        this.mWallpaperView.autoSwitchCurreentWallpaper();
    }
    else
    {
      bool = false;
    }
    return bool;
  }

  protected void requestResourceDetail(int paramInt)
  {
    if ((paramInt >= 0) && (paramInt < this.mGroupDataSet.size()))
    {
      Resource localResource = (Resource)this.mGroupDataSet.get(paramInt);
      if ((localResource != null) && ((localResource.getPreviews() == null) || (localResource.getPreviews().isEmpty())))
        super.requestResourceDetail(paramInt);
    }
  }

  protected void setResourceStatus()
  {
    if (this.mDownloadBtn != null)
    {
      int i;
      if (!new File(this.mResource.getLocalPath()).exists())
      {
        i = 101449751;
        this.mMoreMenuBtn.setEnabled(false);
      }
      else
      {
        i = 101449753;
        this.mMoreMenuBtn.setEnabled(true);
      }
      this.mDownloadBtn.setText(i);
      this.mDownloadBtn.setEnabled(true);
    }
  }

  protected void setupNavigationButton()
  {
    this.mPreviousItem = findViewById(2131165209);
    this.mNextItem = findViewById(2131165210);
    if (this.mPreviousItem != null)
      initNavigationState();
  }

  protected void setupUI()
  {
    Object localObject = new Point();
    getWindowManager().getDefaultDisplay().getSize((Point)localObject);
    int i;
    if (!this.mIsLockscreen)
      i = 2;
    else
      i = 1;
    this.mWallpaperWidth = (i * ((Point)localObject).x);
    this.mWallpaperHeight = ((Point)localObject).y;
    this.mWallpaperView = ((WallpaperView)findViewById(2131165205));
    this.mWallpaperView.regeisterSwitchListener(getWallpaperSwitchListener());
    this.mWallpaperView.setContainingBitmapSize(this.mWallpaperWidth, this.mWallpaperHeight);
    this.mWallpaperView.initScanMode(userUseThumbnailScanModeLastTime());
    this.mSliderView = ((HorzontalSliderView)findViewById(2131165212));
    this.mSliderView.regeisterMoveListener(getSliderMoveListener());
    localObject = this.mSliderView;
    if ((!this.mIsLockscreen) && (!this.mWallpaperView.isThumbnailScanMode()))
      i = 0;
    else
      i = 4;
    ((HorzontalSliderView)localObject).setVisibility(i);
    this.mGestureDetector = new GestureDetector(this, new WallpaperGestureListener());
    this.mImageAsyncDecoder = new WallpaperDecoder(3);
    this.mImageAsyncDecoder.setScaledSize(this.mWallpaperWidth, this.mWallpaperHeight);
    this.mImageAsyncDecoder.registerListener(getImageDecodingListener());
    this.mResContext.setPreviewImageWidth(this.mWallpaperWidth);
    setupNavigationButton();
    changeCurrentResource();
    this.mTitleAreaView = findViewById(2131165207);
    this.mTitleAreaView.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramView)
      {
      }
    });
    this.mOperateBarView = findViewById(2131165213);
    this.mOperateBarView.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramView)
      {
      }
    });
    this.mPreviewMaskView = ((ImageView)findViewById(2131165206));
    this.mPreviewMaskView.setVisibility(4);
    localObject = this.mPreviewMaskView;
    if (!this.mIsLockscreen)
      i = 2130837526;
    else
      i = 2130837530;
    ((ImageView)localObject).setImageResource(i);
    this.mPreviewMaskView.setScaleType(ImageView.ScaleType.FIT_XY);
    this.mPreviewMaskView.setOnTouchListener(new View.OnTouchListener()
    {
      public boolean onTouch(View paramView, MotionEvent paramMotionEvent)
      {
        if (paramMotionEvent.getAction() == 0)
          WallpaperDetailActivity.this.quitPreviewMode(false);
        return true;
      }
    });
    findViewById(2131165208).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramView)
      {
        WallpaperDetailActivity.this.finish();
      }
    });
    findViewById(2131165215).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramView)
      {
        WallpaperDetailActivity.this.enterPreviewMode();
      }
    });
    this.mDownloadBtn = ((TextView)findViewById(2131165216));
    this.mDownloadBtn.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramView)
      {
        Object localObject1 = (TextView)paramView;
        if (((TextView)localObject1).getText() == WallpaperDetailActivity.this.getString(101449753))
          WallpaperDetailActivity.this.doApplyWallpaper(WallpaperDetailActivity.this.mResourceType, WallpaperDetailActivity.this.mResource);
        while (true)
        {
          return;
          Object localObject2 = WallpaperDetailActivity.this.getLocalPreviewPath(WallpaperDetailActivity.this.mResource);
          if (localObject2 != null)
          {
            localObject2 = new File((String)localObject2);
            label71: localObject3 = WallpaperDetailActivity.this.mResource.getLocalPath();
            if (localObject3 == null)
              break label127;
          }
          label127: for (Object localObject3 = new File((String)localObject3); ; localObject3 = null)
          {
            if ((localObject2 != null) && (localObject3 != null))
              break label133;
            Toast.makeText(WallpaperDetailActivity.this, 101449763, 0);
            break;
            localObject2 = null;
            break label71;
          }
          label133: if (((File)localObject2).exists());
          try
          {
            ResourceHelper.writeTo(new FileInputStream((File)localObject2), ((File)localObject3).getAbsolutePath());
            label157: WallpaperDetailActivity.this.setResourceStatus();
            localObject1 = WallpaperDetailActivity.this;
            if (((File)localObject3).exists());
            for (int i = 2131427403; ; i = 101449763)
            {
              Toast.makeText((Context)localObject1, i, 0).show();
              break;
            }
            ((TextView)localObject1).setText(101449752);
            ((TextView)localObject1).setEnabled(false);
          }
          catch (Exception localException)
          {
            break label157;
          }
        }
      }
    });
    findViewById(2131165214).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramView)
      {
        View localView = WallpaperDetailActivity.this.getLayoutInflater().inflate(2130903051, null);
        String str;
        if (WallpaperDetailActivity.this.mHasInfoView)
        {
          ((TextView)localView.findViewById(2131165218)).setText(WallpaperDetailActivity.this.checkInfoValue(WallpaperDetailActivity.this.mResource.getAuthor()));
          str = "";
        }
        try
        {
          str = ResourceHelper.formatFileSize(WallpaperDetailActivity.this.mResource.getSize());
          str = str;
          label72: ((TextView)localView.findViewById(2131165219)).setText(str);
          ((TextView)localView.findViewById(2131165221)).setText(WallpaperDetailActivity.this.checkInfoValue(String.valueOf(WallpaperDetailActivity.this.mResource.getUpdatedTime())));
          if (!WallpaperDetailActivity.this.mIsOnlineResourceSet)
            ((TextView)localView.findViewById(2131165220)).setText(101450233);
          ((TextView)localView.findViewById(2131165222)).setText(WallpaperDetailActivity.this.checkInfoValue(String.valueOf(WallpaperDetailActivity.this.mResource.getDownloadCount())));
          new AlertDialog.Builder(WallpaperDetailActivity.this).setTitle(101449750).setCancelable(true).setView(localView).setPositiveButton(2131427402, null).show();
          return;
        }
        catch (Exception localException)
        {
          break label72;
        }
      }
    });
    this.mMoreMenuBtn = ((ImageView)findViewById(2131165217));
    this.mMoreMenuBtn.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramView)
      {
        View localView = WallpaperDetailActivity.this.getLayoutInflater().inflate(2130903053, null);
        ListView localListView = (ListView)localView.findViewById(2131165223);
        WallpaperDetailActivity.MenuAdapter localMenuAdapter = new WallpaperDetailActivity.MenuAdapter(WallpaperDetailActivity.this);
        localListView.setAdapter(localMenuAdapter);
        localListView.setOnItemClickListener(localMenuAdapter);
        WallpaperDetailActivity.access$1502(WallpaperDetailActivity.this, new PopupWindow(localView, -2, -2, true));
        WallpaperDetailActivity.this.mPopupWindow.setOutsideTouchable(true);
        WallpaperDetailActivity.this.mPopupWindow.setBackgroundDrawable(new BitmapDrawable(WallpaperDetailActivity.this.getResources()));
        WallpaperDetailActivity.this.mPopupWindow.setAnimationStyle(2131492868);
        WallpaperDetailActivity.this.mPopupWindow.setOnDismissListener(new PopupWindow.OnDismissListener()
        {
          public void onDismiss()
          {
          }
        });
        WallpaperDetailActivity.this.mPopupWindow.update();
        int i = WallpaperDetailActivity.this.mOperateBarView.getHeight() - WallpaperDetailActivity.this.mOperateBarView.getPaddingTop();
        WallpaperDetailActivity.this.mPopupWindow.showAtLocation(WallpaperDetailActivity.this.mOperateBarView.getWindowToken(), 85, 0, i);
      }
    });
    setResourceStatus();
  }

  private class MenuAdapter extends BaseAdapter
    implements AdapterView.OnItemClickListener
  {
    private ArrayList<Integer> mCmdList = new ArrayList();

    public MenuAdapter()
    {
      if (WallpaperDetailActivity.this.mResourceType != 2L)
        this.mCmdList.add(Integer.valueOf(2131427397));
      else
        this.mCmdList.add(Integer.valueOf(2131427398));
      this.mCmdList.add(Integer.valueOf(2131427396));
      this.mCmdList.add(Integer.valueOf(2131427399));
      if (!ResourceHelper.isSystemResource(WallpaperDetailActivity.this.mResource.getLocalPath()))
        this.mCmdList.add(Integer.valueOf(101450102));
    }

    public int getCount()
    {
      return this.mCmdList.size();
    }

    public Object getItem(int paramInt)
    {
      return this.mCmdList.get(paramInt);
    }

    public long getItemId(int paramInt)
    {
      return 0L;
    }

    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      if (paramView == null)
        paramView = WallpaperDetailActivity.this.getLayoutInflater().inflate(2130903052, null);
      ((TextView)paramView.findViewById(2131165194)).setText(((Integer)this.mCmdList.get(paramInt)).intValue());
      return paramView;
    }

    public void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
    {
      int i = ((Integer)this.mCmdList.get(paramInt)).intValue();
      if (i != 2131427397)
      {
        if (i != 2131427398)
        {
          if (i != 2131427396)
          {
            if (i != 2131427399)
            {
              if (i == 101450102)
                new AlertDialog.Builder(WallpaperDetailActivity.this).setTitle(101450102).setIconAttribute(16843605).setMessage(101449766).setNegativeButton(17039360, null).setPositiveButton(17039370, new DialogInterface.OnClickListener()
                {
                  public void onClick(DialogInterface paramDialogInterface, int paramInt)
                  {
                    WallpaperDetailActivity.this.mResController.getLocalDataManager().removeResource(WallpaperDetailActivity.this.mResource);
                    if (!WallpaperDetailActivity.this.mIsOnlineResourceSet)
                      WallpaperDetailActivity.this.finish();
                    else
                      WallpaperDetailActivity.this.setResourceStatus();
                  }
                }).show();
            }
            else
              WallpaperUtils.cropAndApplyWallpaper(WallpaperDetailActivity.this, WallpaperDetailActivity.this.mResourceType, WallpaperDetailActivity.this.mResource.getLocalPath(), false, true);
          }
          else
            WallpaperDetailActivity.this.doApplyWallpaper(6L, WallpaperDetailActivity.this.mResource);
        }
        else
          WallpaperDetailActivity.this.doApplyWallpaper(4L, WallpaperDetailActivity.this.mResource);
      }
      else
        WallpaperDetailActivity.this.doApplyWallpaper(2L, WallpaperDetailActivity.this.mResource);
      if ((WallpaperDetailActivity.this.mPopupWindow != null) && (WallpaperDetailActivity.this.mPopupWindow.isShowing()))
        WallpaperDetailActivity.this.mPopupWindow.dismiss();
    }
  }

  class WallpaperGestureListener extends GestureDetector.SimpleOnGestureListener
  {
    WallpaperGestureListener()
    {
    }

    public boolean onDoubleTap(MotionEvent paramMotionEvent)
    {
      WallpaperDetailActivity.this.updateSliderViewState(WallpaperDetailActivity.this.mWallpaperView.isThumbnailScanMode());
      WallpaperView localWallpaperView = WallpaperDetailActivity.this.mWallpaperView;
      boolean bool;
      if (WallpaperDetailActivity.this.mWallpaperView.isThumbnailScanMode())
        bool = false;
      else
        bool = true;
      localWallpaperView.setScanMode(bool);
      return true;
    }

    public boolean onScroll(MotionEvent paramMotionEvent1, MotionEvent paramMotionEvent2, float paramFloat1, float paramFloat2)
    {
      int i = (int)(-paramFloat1);
      WallpaperDetailActivity.this.mWallpaperView.horizontalMove(i);
      return true;
    }

    public boolean onSingleTapConfirmed(MotionEvent paramMotionEvent)
    {
      boolean bool;
      if (WallpaperDetailActivity.this.mTitleAreaView.getVisibility() == 0)
        bool = false;
      else
        bool = true;
      WallpaperDetailActivity.this.updateTitleAndOperateBarState(bool);
      WallpaperDetailActivity.this.autoUpdateSliderViewState(bool);
      return true;
    }
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     com.android.thememanager.activity.WallpaperDetailActivity
 * JD-Core Version:    0.6.0
 */