package com.android.thememanager.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;
import com.android.thememanager.ThemeResourceConstants;
import com.android.thememanager.util.ThemeAudioBatchHandler;
import com.android.thememanager.util.ThemeHelper;
import com.android.thememanager.util.UIHelper;
import com.android.thememanager.util.WallpaperUtils;
import com.android.thememanager.view.LockscreenConfigSettings;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import miui.resourcebrowser.ResourceContext;
import miui.resourcebrowser.activity.LocalResourceListFragment;
import miui.resourcebrowser.activity.ResourceAdapter;
import miui.resourcebrowser.controller.local.ImportResourceTask;
import miui.resourcebrowser.util.BatchResourceHandler;
import miui.resourcebrowser.util.ResourceHelper;

public class LocalThemeListFragment extends LocalResourceListFragment
  implements ThemeResourceConstants
{
  private int mListHeaderViewHeight = 0;
  private MenuItem mLockstyleSettingMenu;
  private Intent mPickerIntent;
  private long mResourceType;
  private String mThemeSourcePath = Environment.getExternalStorageDirectory().getAbsolutePath();
  private ArrayList<ResolveInfo> mThirdAppInfoList;

  private View getRingtoneFiltingHeader()
  {
    LinearLayout localLinearLayout1 = new LinearLayout(this.mActivity);
    localLinearLayout1.setEnabled(false);
    localLinearLayout1.setBackgroundResource(2130837522);
    this.mListHeaderViewHeight = getResources().getDrawable(2130837522).getIntrinsicHeight();
    int[] arrayOfInt1 = new int[3];
    arrayOfInt1[0] = 2131427392;
    arrayOfInt1[1] = 2131427393;
    arrayOfInt1[2] = 2131427394;
    int[] arrayOfInt2 = new int[3];
    arrayOfInt2[0] = 5000;
    arrayOfInt2[1] = 0;
    arrayOfInt2[2] = 0;
    int[] arrayOfInt3 = new int[3];
    arrayOfInt3[0] = 2147483647;
    arrayOfInt3[1] = 5000;
    arrayOfInt3[2] = 2147483647;
    boolean[] arrayOfBoolean = new boolean[3];
    int i;
    if (this.mResourceType == 512L)
      i = 0;
    else
      i = 1;
    arrayOfBoolean[0] = i;
    if (this.mResourceType != 512L)
      i = 0;
    else
      i = 1;
    arrayOfBoolean[1] = i;
    arrayOfBoolean[2] = false;
    Drawable[] arrayOfDrawable = new Drawable[3];
    arrayOfDrawable[0] = getResources().getDrawable(2130837523);
    arrayOfDrawable[1] = getResources().getDrawable(2130837523);
    arrayOfDrawable[2] = null;
    2 local2 = new Handler()
    {
      public void handleMessage(Message paramMessage)
      {
        int j = paramMessage.arg1;
        int i = paramMessage.arg2;
        LocalThemeListFragment.this.mResContext.putExtraMeta("resourcebrowser.RINGTONE_MIN_DURATION_LIMIT", Integer.valueOf(j));
        LocalThemeListFragment.this.mResContext.putExtraMeta("resourcebrowser.RINGTONE_MAX_DURATION_LIMIT", Integer.valueOf(i));
        LocalThemeListFragment.this.mAdapter.loadData();
      }
    };
    for (int j = 0; ; j++)
    {
      if (j >= arrayOfInt1.length)
        return localLinearLayout1;
      TextView localTextView = new TextView(this.mActivity, null, 16843509);
      localTextView.setGravity(17);
      localTextView.setText(arrayOfInt1[j]);
      localTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, arrayOfDrawable[j], null);
      LinearLayout localLinearLayout2 = new LinearLayout(this.mActivity);
      localLinearLayout2.addView(localTextView, new LinearLayout.LayoutParams(-1, -1));
      localLinearLayout2.setSelected(arrayOfBoolean[j]);
      localLinearLayout2.setOnClickListener(new View.OnClickListener(local2, arrayOfInt2[j], arrayOfInt3[j])
      {
        public void onClick(View paramView)
        {
          ViewGroup localViewGroup;
          if (!paramView.isSelected())
          {
            LocalThemeListFragment.this.mAdapter.clearDataSet();
            LocalThemeListFragment.this.mAdapter.notifyDataSetInvalidated();
            this.val$handler.sendMessage(this.val$handler.obtainMessage(0, this.val$minDuration, this.val$maxDuration));
            localViewGroup = (ViewGroup)paramView.getParent();
          }
          for (int i = 0; ; i++)
          {
            if (i >= localViewGroup.getChildCount())
            {
              paramView.setSelected(true);
              return;
            }
            localViewGroup.getChildAt(i).setSelected(false);
          }
        }
      });
      localLinearLayout1.addView(localLinearLayout2, new LinearLayout.LayoutParams(0, -1, 1.0F));
    }
  }

  private void refreshCurrentUsingFlags()
  {
    String str = UIHelper.computeCurrentUsingPath(this.mActivity, this.mResourceType);
    this.mResContext.setCurrentUsingPath(str);
    this.mAdapter.notifyDataSetChanged();
  }

  private ArrayList<ResolveInfo> resolveIntent(Intent paramIntent, ArrayList<String> paramArrayList)
  {
    PackageManager localPackageManager = this.mActivity.getPackageManager();
    List localList = localPackageManager.queryIntentActivities(paramIntent, 65536);
    if (paramArrayList != null);
    for (int i = -1 + localList.size(); ; i--)
    {
      if (i < 0)
      {
        if (localList.size() > 0)
          Collections.sort(localList, new NameComparator(localPackageManager));
        return new ArrayList(localList);
      }
      ResolveInfo localResolveInfo = (ResolveInfo)localList.get(i);
      Iterator localIterator = paramArrayList.iterator();
      while (localIterator.hasNext())
      {
        String str = (String)localIterator.next();
        if (localResolveInfo.activityInfo.name.indexOf(str) < 0)
          continue;
        localList.remove(i);
      }
    }
  }

  private void resolveIntent()
  {
    if (this.mThirdAppInfoList == null)
    {
      ArrayList localArrayList = new ArrayList();
      localArrayList.add(ThemeTabActivity.class.getName());
      if ((this.mResourceType != 2L) && (this.mResourceType != 4L))
      {
        this.mPickerIntent = new Intent("android.intent.action.RINGTONE_PICKER");
        this.mPickerIntent.putExtra("android.intent.extra.ringtone.SHOW_DEFAULT", false);
        this.mPickerIntent.putExtra("android.intent.extra.ringtone.SHOW_SILENT", true);
        this.mPickerIntent.putExtra("android.intent.extra.ringtone.TYPE", UIHelper.getRingtoneType(this.mResourceType));
        localArrayList.add("RingtonePickerActivity");
      }
      else
      {
        this.mPickerIntent = new Intent("android.intent.action.GET_CONTENT");
        this.mPickerIntent.addCategory("android.intent.category.OPENABLE");
        this.mPickerIntent.setType("image/*");
      }
      this.mThirdAppInfoList = resolveIntent(this.mPickerIntent, localArrayList);
    }
  }

  private void updateLockstyleSettingMenuStatus()
  {
    if (this.mLockstyleSettingMenu != null)
    {
      boolean bool = LockscreenConfigSettings.containConfigFile(this.mActivity);
      this.mLockstyleSettingMenu.setEnabled(bool);
    }
  }

  protected BatchResourceHandler getBatchOperationHandler()
  {
    Object localObject;
    if (!UIHelper.isAudioResource(this.mResourceType))
      localObject = super.getBatchOperationHandler();
    else
      localObject = new ThemeAudioBatchHandler(this, this.mAdapter, this.mResContext, this.mResourceType);
    return (BatchResourceHandler)localObject;
  }

  protected View getHeaderView()
  {
    View localView;
    if (!UIHelper.isAudioResource(this.mResourceType))
      localView = super.getHeaderView();
    else
      localView = getRingtoneFiltingHeader();
    return localView;
  }

  protected void initParams()
  {
    this.mResourceType = ((Long)this.mResContext.getExtraMeta("EXTRA_CTX_RESOURCE_TYPE", Long.valueOf(-1L))).longValue();
    ThemeHelper.setMusicVolumeType(this.mActivity, this.mResourceType);
    super.initParams();
  }

  public void initializeDataSet()
  {
    super.initializeDataSet();
    if (UIHelper.isAudioResource(this.mResourceType))
      if (this.mAdapter.getCount() <= 0)
        this.mListView.scrollBy(0, this.mListHeaderViewHeight);
      else
        this.mListView.setSelection(1);
  }

  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if ((!this.mResContext.isPicker()) || (paramIntent == null));
    Object localObject2;
    Object localObject1;
    switch (paramInt1)
    {
    default:
      break;
    case 100:
      if (paramIntent == null)
        break;
      String str = paramIntent.getData().getPath();
      if (paramInt2 != -1)
        break;
      localObject2 = new ImportResourceTask(this.mResContext, "import-" + str)
      {
        protected void onPostExecute(Integer paramInteger)
        {
          LocalThemeListFragment.this.mProgressDialog.setProgress(100);
          if ((paramInteger != null) && (paramInteger.intValue() > 0))
          {
            LocalThemeListFragment.this.mAdapter.notifyDataSetChanged();
            LocalThemeListFragment.this.mAdapter.loadData();
          }
          if (LocalThemeListFragment.this.mWakeLock.isHeld())
            LocalThemeListFragment.this.mWakeLock.release();
          LocalThemeListFragment.this.mProgressBar.setVisibility(8);
          if (LocalThemeListFragment.this.mProgressDialog.isShowing())
            LocalThemeListFragment.this.mProgressDialog.dismiss();
        }

        protected void onPreExecute()
        {
          super.onPreExecute();
          LocalThemeListFragment.access$002(LocalThemeListFragment.this, ((PowerManager)LocalThemeListFragment.this.mActivity.getSystemService("power")).newWakeLock(268435462, "Resource Import Tag"));
          LocalThemeListFragment.this.mWakeLock.acquire();
          LocalThemeListFragment.this.mProgressBar.setVisibility(0);
          LocalThemeListFragment.access$402(LocalThemeListFragment.this, new ProgressDialog(LocalThemeListFragment.this.mActivity));
          LocalThemeListFragment.this.mProgressDialog.setProgressStyle(1);
          LocalThemeListFragment.this.mProgressDialog.setMessage(LocalThemeListFragment.this.mActivity.getString(2131427404));
          LocalThemeListFragment.this.mProgressDialog.setCancelable(false);
          LocalThemeListFragment.this.mProgressDialog.setProgressNumberFormat("");
          LocalThemeListFragment.this.mProgressDialog.setMax(100);
          LocalThemeListFragment.this.mProgressDialog.show();
        }
      };
      localObject1 = new String[2];
      localObject1[0] = ImportResourceTask.IMPORT_BY_PATH;
      localObject1[1] = str;
      ((1)localObject2).execute(localObject1);
      this.mThemeSourcePath = str.substring(0, str.lastIndexOf("/"));
      break;
    case 101:
      if (paramIntent == null)
        break;
      boolean bool = true;
      localObject2 = (Uri)paramIntent.getParcelableExtra("android.intent.extra.ringtone.PICKED_URI");
      localObject1 = ResourceHelper.getPathByUri(this.mActivity, (Uri)localObject2);
      if (this.mResourceType != 64L)
      {
        if (this.mResourceType != 256L)
        {
          if (this.mResourceType != 512L)
          {
            if (this.mResourceType == 1024L)
              RingtoneManager.setActualDefaultRingtoneUri(this.mActivity, 4, (Uri)localObject2);
          }
          else
            RingtoneManager.setActualDefaultRingtoneUri(this.mActivity, 2, (Uri)localObject2);
        }
        else
          RingtoneManager.setActualDefaultRingtoneUri(this.mActivity, 1, (Uri)localObject2);
      }
      else
        bool = ThemeHelper.applyBootAudio(this.mActivity, (String)localObject1);
      if (bool)
      {
        localObject2 = getString(2131427340);
        ThemeHelper.saveUserPreference(this.mResourceType, (String)localObject1, (String)localObject2);
      }
      ThemeHelper.showThemeChangedToast(this.mActivity, bool);
      break;
    case 102:
      if (paramIntent == null)
        break;
      localObject1 = paramIntent.getData();
      WallpaperUtils.cropAndApplyWallpaper(this.mActivity, this, this.mResourceType, (Uri)localObject1, false, false);
      break;
    case 28673:
    case 28674:
      WallpaperUtils.dealCropWallpaperResult(this.mActivity, paramInt1, paramInt2);
      break;
      super.onActivityResult(paramInt1, paramInt2, paramIntent);
    }
  }

  public List<Integer> onFragmentCreateOptionsMenu(Menu paramMenu)
  {
    ArrayList localArrayList = new ArrayList();
    int i;
    MenuItem localMenuItem;
    if (this.mResourceType != 4096L)
    {
      if (!UIHelper.supportImportMenu(this.mResourceType))
      {
        i = 2131427368;
        localMenuItem = paramMenu.add(0, i, 0, i);
        localMenuItem.setShowAsAction(-2147483646);
        localMenuItem.setIcon(2130837520);
      }
      else
      {
        i = 2131427367;
        paramMenu.add(0, i, 0, i);
      }
    }
    else
    {
      i = 2131427370;
      localMenuItem = paramMenu.add(0, i, 0, i);
      localMenuItem.setShowAsAction(2);
      this.mLockstyleSettingMenu = localMenuItem;
      updateLockstyleSettingMenuStatus();
    }
    localArrayList.add(Integer.valueOf(i));
    return localArrayList;
  }

  public void onFragmentOptionsItemSelected(MenuItem paramMenuItem)
  {
    Intent localIntent;
    if (paramMenuItem.getItemId() != 2131427367)
    {
      if (paramMenuItem.getItemId() != 2131427368)
      {
        if (paramMenuItem.getItemId() == 2131427370)
          startActivity(new Intent(this.mActivity, LockscreenConfigSettings.class));
      }
      else
      {
        resolveIntent();
        localIntent = new Intent(this.mActivity, ThirdPartyPickersActivity.class);
        localIntent.setAction("android.intent.action.MAIN");
        localIntent.putExtra("android.intent.extra.INTENT", this.mPickerIntent);
        localIntent.putExtra("extra_resource_type", this.mResourceType);
        localIntent.putParcelableArrayListExtra("extra_resolve_info_list", this.mThirdAppInfoList);
        if (!UIHelper.isAudioResource(this.mResourceType))
        {
          if ((4L != this.mResourceType) && (2L != this.mResourceType))
            startActivity(localIntent);
          else
            startActivityForResult(localIntent, 102);
        }
        else
          startActivityForResult(localIntent, 101);
      }
    }
    else
    {
      localIntent = new Intent("android.intent.action.PICK");
      localIntent.setData(Uri.fromFile(new File(this.mThemeSourcePath)));
      localIntent.putExtra("root_directory", "/");
      String[] arrayOfString = new String[2];
      arrayOfString[0] = "zip";
      arrayOfString[1] = "mtz";
      localIntent.putExtra("ext_filter", arrayOfString);
      localIntent.putExtra("ext_file_first", true);
      localIntent.putExtra("back_to_parent_directory", false);
      startActivityForResult(localIntent, 100);
    }
  }

  protected void onVisible()
  {
    super.onVisible();
    refreshCurrentUsingFlags();
    updateLockstyleSettingMenuStatus();
  }

  private static class NameComparator
    implements Comparator<ResolveInfo>
  {
    private PackageManager mPM;

    public NameComparator(PackageManager paramPackageManager)
    {
      this.mPM = paramPackageManager;
    }

    public final int compare(ResolveInfo paramResolveInfo1, ResolveInfo paramResolveInfo2)
    {
      String str2 = paramResolveInfo1.activityInfo.packageName;
      String str1 = paramResolveInfo2.activityInfo.packageName;
      Object localObject2 = paramResolveInfo1.loadLabel(this.mPM);
      Object localObject1 = paramResolveInfo2.loadLabel(this.mPM);
      if (localObject2 == null)
        localObject2 = paramResolveInfo1.activityInfo.name;
      if (localObject1 == null)
        localObject1 = paramResolveInfo2.activityInfo.name;
      int i = str2.compareTo(str1);
      if (i == 0)
        i = localObject2.toString().compareTo(localObject1.toString());
      return i;
    }
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     com.android.thememanager.activity.LocalThemeListFragment
 * JD-Core Version:    0.6.0
 */