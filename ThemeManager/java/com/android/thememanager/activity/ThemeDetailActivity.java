package com.android.thememanager.activity;

import android.content.Intent;
import android.os.Handler;
import com.android.thememanager.ThemeResourceConstants;
import com.android.thememanager.controller.ThemeController;
import com.android.thememanager.util.ThemeApplyParameters;
import com.android.thememanager.util.ThemeHelper;
import com.android.thememanager.util.ThemeOperationHandler;
import com.android.thememanager.util.UIHelper;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.List<Ljava.lang.String;>;
import java.util.zip.ZipFile;
import miui.resourcebrowser.ResourceContext;
import miui.resourcebrowser.activity.ResourceDetailActivity;
import miui.resourcebrowser.controller.ResourceController;
import miui.resourcebrowser.model.PathEntry;
import miui.resourcebrowser.model.Resource;
import miui.resourcebrowser.util.ResourceHelper;
import miui.resourcebrowser.view.ResourceOperationHandler;
import miui.resourcebrowser.view.ResourceOperationView;

public class ThemeDetailActivity extends ResourceDetailActivity
  implements ThemeResourceConstants
{
  private long mModules;
  private long mResourceType;

  private long getApplyComponentFlags()
  {
    long l;
    if (this.mResourceType != -1L)
    {
      if ((this.mResourceType != 4096L) || ((0x4 & this.mModules) == 0L))
        l = this.mResourceType;
      else
        l = 4100L;
    }
    else
      l = 0xFFFFF8FF & this.mResourceType;
    return l;
  }

  private ThemeApplyParameters getThemeApplyParameters()
  {
    ThemeApplyParameters localThemeApplyParameters = new ThemeApplyParameters();
    localThemeApplyParameters.applyFlags = getApplyComponentFlags();
    boolean bool;
    if (this.mResourceType != -1L)
      bool = false;
    else
      bool = true;
    localThemeApplyParameters.removeOthers = bool;
    return localThemeApplyParameters;
  }

  protected ResourceContext buildResourceContext(ResourceContext paramResourceContext)
  {
    return super.buildResourceContext(UIHelper.buildResourceContext(paramResourceContext, getIntent(), this));
  }

  protected Resource getExternalResource()
  {
    return super.getExternalResource();
  }

  protected String getFormatPlayingRingtoneName(String paramString, int paramInt1, int paramInt2)
  {
    String str;
    if (this.mResourceType == 32768L)
    {
      str = ThemeHelper.getNameForAudioEffect(paramString);
      Object[] arrayOfObject = new Object[3];
      arrayOfObject[0] = str;
      arrayOfObject[1] = Integer.valueOf(paramInt1 + 1);
      arrayOfObject[2] = Integer.valueOf(paramInt2);
      str = String.format("%s (%d/%d)", arrayOfObject);
    }
    else
    {
      str = super.getFormatPlayingRingtoneName(paramString, paramInt1, paramInt2);
    }
    return str;
  }

  protected String getFormatTitleBeforePlayingRingtone()
  {
    Object localObject;
    if (this.mResourceType != 32768L)
    {
      localObject = super.getFormatTitleBeforePlayingRingtone();
    }
    else
    {
      localObject = new Object[1];
      localObject[0] = Integer.valueOf(getMusicPlayList(this.mResource).size());
      localObject = getString(2131427366, localObject);
    }
    return (String)localObject;
  }

  protected List<String> getMusicPlayList(Resource paramResource)
  {
    if (this.mResourceType == 32768L)
    {
      boolean bool = ResourceHelper.isLocalResource(paramResource.getStatus());
      Object localObject2 = paramResource.getPreviews();
      localObject1 = new ArrayList();
      localObject2 = ((List)localObject2).iterator();
      while (((Iterator)localObject2).hasNext())
      {
        Object localObject3 = (PathEntry)((Iterator)localObject2).next();
        if (!bool)
          localObject3 = ((PathEntry)localObject3).getOnlinePath();
        else
          localObject3 = ((PathEntry)localObject3).getLocalPath();
        ((List)localObject1).add(localObject3);
      }
    }
    Object localObject1 = super.getMusicPlayList(paramResource);
    return (List<String>)(List<String>)(List<String>)localObject1;
  }

  protected ResourceController getResourceController(ResourceContext paramResourceContext)
  {
    return new ThemeController(paramResourceContext);
  }

  protected ResourceOperationHandler getResourceOperationHandler(ResourceOperationView paramResourceOperationView)
  {
    return new ThemeOperationHandler(this, this.mResContext, paramResourceOperationView);
  }

  protected void initParams()
  {
    super.initParams();
    this.mResourceType = ((Long)this.mResContext.getExtraMeta("EXTRA_CTX_RESOURCE_TYPE", Long.valueOf(-1L))).longValue();
    ThemeHelper.setMusicVolumeType(this, this.mResourceType);
    new Handler().postDelayed(new Runnable()
    {
      public void run()
      {
        ThemeDetailActivity.this.invalidateOptionsMenu();
      }
    }
    , 800L);
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    if ((paramInt1 == 103) && (paramInt2 == 104))
    {
      boolean bool = paramIntent.getBooleanExtra("RESPONSE_NEEDS_DELETE", false);
      if ((!this.mIsOnlineResourceSet) && (bool))
      {
        super.onDeleteEventPerformed();
        finish();
      }
    }
  }

  public void onApplyEventPerformed()
  {
    super.onApplyEventPerformed();
    ((ThemeOperationHandler)this.mOperationHandler).setApplyParameters(getThemeApplyParameters());
  }

  public void onMagicEventPerformed()
  {
    super.onMagicEventPerformed();
    Intent localIntent = new Intent(this, ThemeComponentApplyActivity.class);
    localIntent.putExtra("REQUEST_SELECTING_THEME", this.mResource);
    localIntent.putExtra("REQUEST_APPLY_PARAMS", getThemeApplyParameters());
    localIntent.putExtra("REQUEST_SOURCE_TYPE", this.mSourceType);
    startActivityForResult(localIntent, 103);
    overridePendingTransition(0, 0);
  }

  protected void setResourceInfo()
  {
    super.setResourceInfo();
    if (((UIHelper.isAudioResource(this.mResourceType)) && (this.mResourceType != 32768L)) || (UIHelper.isImageResource(this.mResourceType)));
    while (true)
    {
      return;
      try
      {
        String str = this.mResource.getExtraMeta("modulesFlag");
        if (str != null)
        {
          long l1 = Long.parseLong(str);
          label56: this.mModules = l1;
          File localFile = new File(this.mResource.getDownloadPath());
          if ((this.mModules == 0L) || ((this.mIsOnlineResourceSet) && (localFile.exists())))
            if (!"/system/media/theme/.data/meta/theme/default.mrm".equals(this.mResource.getLocalPath()))
              break label169;
        }
        long l2;
        label169: for (this.mModules = -1L; ; this.mModules = ThemeHelper.identifyComponents(new ZipFile(l2)))
        {
          if (this.mResourceType != 4096L)
            break label198;
          this.mModules = 4100L;
          this.mResource.putExtraMeta("modulesFlag", String.valueOf(this.mModules));
          break;
          l2 = 0L;
          break label56;
        }
      }
      catch (Exception localException)
      {
        while (true)
        {
          this.mModules = 0L;
          continue;
          label198: if (this.mResourceType == -1L)
            continue;
          this.mModules = this.mResourceType;
        }
      }
    }
  }

  protected void setupUI()
  {
    super.setupUI();
    if (this.mResourceType == 16L)
      setScreenViewBackground(100794413);
    if ((this.mResourceType == -1L) || (this.mResourceType == 4096L))
      this.mOperationView.setMagicButtonResource(100794799);
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     com.android.thememanager.activity.ThemeDetailActivity
 * JD-Core Version:    0.6.0
 */