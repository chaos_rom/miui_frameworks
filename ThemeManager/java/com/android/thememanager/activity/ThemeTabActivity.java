package com.android.thememanager.activity;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.os.Bundle;
import android.os.Handler;
import com.android.thememanager.ThemeResourceConstants;
import com.android.thememanager.controller.ThemeController;
import com.android.thememanager.util.ConstantsHelper;
import com.android.thememanager.util.UIHelper;
import java.util.ArrayList;
import java.util.List;
import miui.resourcebrowser.ResourceContext;
import miui.resourcebrowser.activity.BaseFragment;
import miui.resourcebrowser.activity.ResourceTabActivity;
import miui.resourcebrowser.controller.ResourceController;

public class ThemeTabActivity extends ResourceTabActivity
  implements ThemeResourceConstants
{
  private long mResourceType;

  protected ResourceContext buildResourceContext(ResourceContext paramResourceContext)
  {
    ResourceContext localResourceContext = UIHelper.buildResourceContext(paramResourceContext, getIntent(), this);
    this.mResourceType = ((Long)localResourceContext.getExtraMeta("EXTRA_CTX_RESOURCE_TYPE", Long.valueOf(-1L))).longValue();
    return super.buildResourceContext(localResourceContext);
  }

  protected List<ActionBar.Tab> getActionBarTabs()
  {
    ArrayList localArrayList = new ArrayList();
    ActionBar localActionBar = getActionBar();
    localArrayList.add(localActionBar.newTab().setText(getString(2131427387)));
    localArrayList.add(localActionBar.newTab().setText(getString(2131427388)));
    if (this.mResourceType == -1L)
      localArrayList.add(localActionBar.newTab().setText(getString(2131427389)));
    return localArrayList;
  }

  protected ResourceController getResourceController(ResourceContext paramResourceContext)
  {
    return new ThemeController(paramResourceContext);
  }

  protected BaseFragment initTabFragment(int paramInt)
  {
    Object localObject;
    if (paramInt != 0)
    {
      if (paramInt != 1)
      {
        if (paramInt != 2)
          localObject = null;
        else
          localObject = new ComponentFragment();
      }
      else
        localObject = new OnlineThemeListFragment();
    }
    else
      localObject = new LocalThemeListFragment();
    return (BaseFragment)localObject;
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    ActionBar localActionBar = getActionBar();
    int i = ConstantsHelper.getTitleResId(this.mResourceType);
    if (this.mResourceType == -1L)
      i = 2131427363;
    localActionBar.setTitle(i);
    new Handler().postDelayed(new Runnable()
    {
      public void run()
      {
        ThemeTabActivity.this.invalidateOptionsMenu();
      }
    }
    , 800L);
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     com.android.thememanager.activity.ThemeTabActivity
 * JD-Core Version:    0.6.0
 */