package com.android.thememanager.activity;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import com.android.thememanager.util.ConstantsHelper;
import java.util.List;
import miui.preference.BasePreferenceActivity;

public class ThirdPartyPickersActivity extends BasePreferenceActivity
{
  public void finish()
  {
    super.finish();
    overridePendingTransition(17432576, 100925445);
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Object localObject1 = getIntent();
    Object localObject2 = ((Intent)localObject1).getParcelableExtra("android.intent.extra.INTENT");
    long l = ((Intent)localObject1).getLongExtra("extra_resource_type", 0L);
    localObject1 = ((Intent)localObject1).getParcelableArrayListExtra("extra_resolve_info_list");
    PackageManager localPackageManager;
    PreferenceScreen localPreferenceScreen;
    int i;
    int j;
    if (((localObject2 instanceof Intent)) && (l != 0L) && (localObject1 != null) && (((List)localObject1).size() != 0))
    {
      localObject2 = (Intent)localObject2;
      localPackageManager = getPackageManager();
      localPreferenceScreen = getPreferenceManager().createPreferenceScreen(this);
      Object[] arrayOfObject = new Object[1];
      arrayOfObject[0] = getString(ConstantsHelper.getTitleResId(l));
      localPreferenceScreen.setTitle(getString(2131427369, arrayOfObject));
      i = ((List)localObject1).size();
      j = 0;
    }
    while (true)
      if (j >= i)
      {
        setPreferenceScreen(localPreferenceScreen);
      }
      else
      {
        ResolveInfo localResolveInfo = (ResolveInfo)((List)localObject1).get(j);
        Intent localIntent = new Intent((Intent)localObject2);
        localIntent.addFlags(50331648);
        Object localObject3 = ((ResolveInfo)((List)localObject1).get(j)).activityInfo;
        localIntent.setComponent(new ComponentName(((ActivityInfo)localObject3).packageName, ((ActivityInfo)localObject3).name));
        localObject3 = new PreferenceScreen(this, null);
        ((PreferenceScreen)localObject3).setIntent(localIntent);
        ((PreferenceScreen)localObject3).setTitle(localResolveInfo.loadLabel(localPackageManager));
        ((PreferenceScreen)localObject3).setIcon(localResolveInfo.loadIcon(localPackageManager));
        localPreferenceScreen.addPreference((Preference)localObject3);
        j++;
        continue;
        finish();
      }
  }

  public boolean onPreferenceTreeClick(PreferenceScreen paramPreferenceScreen, Preference paramPreference)
  {
    startActivity(paramPreference.getIntent());
    finish();
    return true;
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     com.android.thememanager.activity.ThirdPartyPickersActivity
 * JD-Core Version:    0.6.0
 */