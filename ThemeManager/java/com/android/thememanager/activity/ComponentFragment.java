package com.android.thememanager.activity;

import I;
import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import com.android.thememanager.ThemeResourceConstants;
import com.android.thememanager.util.BackupThemeTask;
import com.android.thememanager.util.ThemeHelper;
import com.android.thememanager.view.ComponentCategoryView;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import miui.resourcebrowser.activity.BaseFragment;

public class ComponentFragment extends BaseFragment
  implements ThemeResourceConstants
{
  private static final int[] ComponentCategoryTitles;
  private static final Long[][] ComponentFlags;
  private ArrayList<ComponentCategoryView> mCates = new ArrayList();

  static
  {
    Object localObject = new int[2];
    localObject[0] = 2131427342;
    localObject[1] = 2131427343;
    ComponentCategoryTitles = (I)localObject;
    localObject = new Long[2][];
    Long[] arrayOfLong = new Long[6];
    arrayOfLong[0] = Long.valueOf(4096L);
    arrayOfLong[1] = Long.valueOf(4L);
    arrayOfLong[2] = Long.valueOf(2L);
    arrayOfLong[3] = Long.valueOf(8L);
    arrayOfLong[4] = Long.valueOf(256L);
    arrayOfLong[5] = Long.valueOf(512L);
    localObject[0] = arrayOfLong;
    arrayOfLong = new Long[7];
    arrayOfLong[0] = Long.valueOf(16L);
    arrayOfLong[1] = Long.valueOf(8192L);
    arrayOfLong[2] = Long.valueOf(16384L);
    arrayOfLong[3] = Long.valueOf(128L);
    arrayOfLong[4] = Long.valueOf(2048L);
    arrayOfLong[5] = Long.valueOf(32L);
    arrayOfLong[6] = Long.valueOf(64L);
    localObject[1] = arrayOfLong;
    ComponentFlags = (Long)localObject;
  }

  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    Object localObject = getActivity();
    LinearLayout localLinearLayout = new LinearLayout((Context)localObject);
    localLinearLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
    localLinearLayout.setOrientation(1);
    localLinearLayout.setGravity(17);
    localLinearLayout.setPadding(0, 0, 0, (int)TypedValue.applyDimension(1, 10.0F, ((Activity)localObject).getResources().getDisplayMetrics()));
    for (int i = 0; ; i++)
    {
      if (i >= ComponentCategoryTitles.length)
      {
        localObject = new ScrollView(getActivity());
        ((ScrollView)localObject).addView(localLinearLayout);
        return localObject;
      }
      ArrayList localArrayList = new ArrayList();
      Collections.addAll(localArrayList, ComponentFlags[i]);
      if (!ThemeHelper.supportReplaceFont())
        localArrayList.remove(Long.valueOf(16L));
      if (!ThemeHelper.supportReplaceAudioEffect())
        localArrayList.remove(Long.valueOf(32768L));
      if (localArrayList.isEmpty())
        continue;
      localObject = (ComponentCategoryView)paramLayoutInflater.inflate(2130903044, null);
      ((ComponentCategoryView)localObject).setTitle(ComponentCategoryTitles[i]);
      ((ComponentCategoryView)localObject).setComponentItems(localArrayList);
      localLinearLayout.addView((View)localObject);
      this.mCates.add(localObject);
    }
  }

  public List<Integer> onFragmentCreateOptionsMenu(Menu paramMenu)
  {
    paramMenu.add(0, 2131427333, 0, 2131427333);
    ArrayList localArrayList = new ArrayList();
    localArrayList.add(Integer.valueOf(2131427333));
    return localArrayList;
  }

  public void onFragmentOptionsItemSelected(MenuItem paramMenuItem)
  {
    Activity localActivity = getActivity();
    String str = new File(BackupThemeTask.BACKUP_THEME_PATH).getAbsolutePath();
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(localActivity);
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = str;
    localBuilder.setMessage(localActivity.getString(2131427335, arrayOfObject)).setNegativeButton(17039360, null).setPositiveButton(2131427334, new DialogInterface.OnClickListener(localActivity)
    {
      public void onClick(DialogInterface paramDialogInterface, int paramInt)
      {
        new BackupThemeTask(this.val$context).execute(new Void[0]);
      }
    }).show();
  }

  public void onResume()
  {
    super.onResume();
    Iterator localIterator = this.mCates.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      ((ComponentCategoryView)localIterator.next()).refresh();
    }
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     com.android.thememanager.activity.ComponentFragment
 * JD-Core Version:    0.6.0
 */