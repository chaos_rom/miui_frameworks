package com.android.thememanager.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceScreen;
import android.widget.BaseAdapter;
import java.util.List;
import miui.app.SDCardMonitor;
import miui.app.SDCardMonitor.SDCardStatusListener;
import miui.preference.BasePreferenceActivity;
import miui.resourcebrowser.util.ResourceHelper;

public class WallpaperSettings extends BasePreferenceActivity
  implements SDCardMonitor.SDCardStatusListener
{
  private SDCardMonitor mSDCardMonitor;

  private void checkLiveWallpaperPicker()
  {
    Object localObject = new Intent();
    ((Intent)localObject).setClassName("com.android.wallpaper.livepicker", "com.android.wallpaper.livepicker.LiveWallpaperActivity");
    localObject = getPackageManager().queryIntentActivities((Intent)localObject, 0);
    if ((localObject == null) || (((List)localObject).isEmpty()))
    {
      localObject = findPreference("live_wallpaper");
      getPreferenceScreen().removePreference((Preference)localObject);
    }
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    addPreferencesFromResource(2130903054);
    checkLiveWallpaperPicker();
    this.mSDCardMonitor = SDCardMonitor.getSDCardMonitor(this);
    this.mSDCardMonitor.addListener(this);
  }

  protected void onDestroy()
  {
    this.mSDCardMonitor.removeListener(this);
    super.onDestroy();
  }

  protected void onResume()
  {
    super.onResume();
    ((BaseAdapter)getPreferenceScreen().getRootAdapter()).notifyDataSetChanged();
  }

  public void onStatusChanged(boolean paramBoolean)
  {
    ResourceHelper.exit(this);
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     com.android.thememanager.activity.WallpaperSettings
 * JD-Core Version:    0.6.0
 */