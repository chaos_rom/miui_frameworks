package com.android.thememanager.activity;

import com.android.thememanager.controller.ThemeController;
import miui.resourcebrowser.ResourceContext;
import miui.resourcebrowser.activity.BaseFragment;
import miui.resourcebrowser.activity.ResourceSearchListActivity;
import miui.resourcebrowser.controller.ResourceController;

public class ThemeSearchListActivity extends ResourceSearchListActivity
{
  protected BaseFragment getFragment()
  {
    return new ThemeSearchListFragment();
  }

  protected ResourceController getResourceController(ResourceContext paramResourceContext)
  {
    return new ThemeController(paramResourceContext);
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     com.android.thememanager.activity.ThemeSearchListActivity
 * JD-Core Version:    0.6.0
 */