package com.android.thememanager.controller;

import miui.resourcebrowser.ResourceContext;
import miui.resourcebrowser.controller.ImportManager;
import miui.resourcebrowser.controller.ResourceController;

public class ThemeController extends ResourceController
{
  public ThemeController(ResourceContext paramResourceContext)
  {
    super(paramResourceContext);
  }

  protected ImportManager newImportManager()
  {
    return new ThemeImportManager(this.context);
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     com.android.thememanager.controller.ThemeController
 * JD-Core Version:    0.6.0
 */