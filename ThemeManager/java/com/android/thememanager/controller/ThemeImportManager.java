package com.android.thememanager.controller;

import com.android.thememanager.ThemeResourceConstants;
import com.android.thememanager.util.ConstantsHelper;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import miui.os.Shell;
import miui.resourcebrowser.ResourceContext;
import miui.resourcebrowser.controller.ImportManager;
import miui.resourcebrowser.controller.LocalDataManager;
import miui.resourcebrowser.controller.ResourceController;
import miui.resourcebrowser.controller.local.LocalJSONDataParser;
import miui.resourcebrowser.controller.local.PersistenceException;
import miui.resourcebrowser.controller.strategy.IdGenerationStrategy;
import miui.resourcebrowser.model.RelatedResource;
import miui.resourcebrowser.model.Resource;
import miui.resourcebrowser.util.ResourceHelper;

public class ThemeImportManager extends ImportManager
  implements ThemeResourceConstants
{
  public ThemeImportManager(ResourceContext paramResourceContext)
  {
    super(paramResourceContext);
  }

  private Resource buildBundle(File paramFile, Resource paramResource1, Resource paramResource2)
  {
    Object localObject1 = paramResource2.getParentResources().iterator();
    while (true)
    {
      Object localObject2;
      LocalJSONDataParser localLocalJSONDataParser;
      if (((Iterator)localObject1).hasNext())
      {
        localObject2 = (RelatedResource)((Iterator)localObject1).next();
        if (!((RelatedResource)localObject2).getResourceCode().equals("theme"))
          continue;
        localLocalJSONDataParser = new LocalJSONDataParser(this.context);
      }
      try
      {
        localObject2 = localLocalJSONDataParser.loadResource(new File(((RelatedResource)localObject2).getLocalPath()));
        if (localObject2 == null)
          continue;
        paramResource2.updateFrom((Resource)localObject2);
        copyInheritedProperties(paramResource1, paramResource2);
        localObject1 = new File(paramResource2.getDownloadPath());
        localObject2 = paramResource2.getLocalId();
        if (localObject2 == null)
          localObject2 = this.idGenerationStrategy.nextId();
        paramResource1.setLocalId((String)localObject2);
        paramResource1.setLocalPath(getComponentLocalPath((File)localObject1, (String)localObject2, "theme"));
        localObject2 = getBundleFilePath((File)localObject1, (String)localObject2);
        createBundleContentFile((String)localObject2);
        paramResource1.setFilePath((String)localObject2);
        paramResource1.setHash(ResourceHelper.getFileHash(((File)localObject1).getAbsolutePath()));
        paramResource1.setSize(((File)localObject1).length());
        localObject1 = new ArrayList();
        for (int i = 0; i < COMPONENT_PREVIEW_SHOW_ORDER.length; i++)
          ((List)localObject1).add(ConstantsHelper.getPreviewPrefix(COMPONENT_PREVIEW_SHOW_ORDER[i]));
      }
      catch (PersistenceException localPersistenceException)
      {
        localPersistenceException.printStackTrace();
      }
    }
    resolveBuildInImages(paramFile, paramResource1, (List)localObject1);
    return (Resource)(Resource)paramResource1;
  }

  private Resource buildComponent(File paramFile1, File paramFile2, Resource paramResource1, Resource paramResource2, String paramString)
  {
    Object localObject = paramResource2.getLocalId();
    if (localObject == null)
      localObject = this.idGenerationStrategy.nextId();
    paramResource2.setLocalId((String)localObject);
    paramResource2.setLocalPath(getComponentLocalPath(paramFile2, (String)localObject, paramString));
    paramResource2.setFilePath(getComponentFilePath(paramFile2, (String)localObject, paramString));
    paramResource2.setHash(ResourceHelper.getFileHash(paramFile2.getAbsolutePath()));
    paramResource2.setSize(paramFile2.length());
    resolveBuildInImages(paramFile1, paramResource2, this.context.getBuildInImagePrefixes());
    if (getRelatedResource("theme", paramResource2.getParentResources()) == null)
    {
      localObject = new RelatedResource();
      ((RelatedResource)localObject).setLocalPath(paramResource1.getLocalPath());
      ((RelatedResource)localObject).setResourceCode("theme");
      paramResource2.addParentResources((RelatedResource)localObject);
    }
    return (Resource)paramResource2;
  }

  private boolean importComponent(File paramFile1, File paramFile2, Resource paramResource1, Resource paramResource2, String paramString)
  {
    Resource localResource = buildComponent(paramFile1, paramFile2, paramResource1, paramResource2, paramString);
    if (getRelatedResource(paramString, paramResource1.getSubResources()) == null)
    {
      localObject = new RelatedResource();
      ((RelatedResource)localObject).setLocalPath(localResource.getLocalPath());
      ((RelatedResource)localObject).setFilePath(localResource.getFilePath());
      ((RelatedResource)localObject).setResourceCode(paramString);
      paramResource1.addSubResources((RelatedResource)localObject);
    }
    Object localObject = new File(localResource.getFilePath());
    ((File)localObject).getParentFile().mkdirs();
    ((File)localObject).delete();
    paramFile2.renameTo((File)localObject);
    this.controller.getLocalDataManager().addResource(localResource);
    return true;
  }

  private boolean importComponents(File paramFile, Resource paramResource1, Resource paramResource2)
  {
    boolean bool = false;
    Map localMap = getComponentsMap(paramFile);
    String str2 = this.context.getResourceCode();
    Iterator localIterator = localMap.keySet().iterator();
    while (localIterator.hasNext())
    {
      String str1 = (String)localIterator.next();
      File localFile = new File(paramFile, str1);
      if (!str2.equals(localMap.get(str1)))
        continue;
      bool = importComponent(paramFile, localFile, paramResource1, paramResource2, str2);
    }
    if ((!importComponents(paramFile, paramResource1)) || (!bool))
      bool = false;
    else
      bool = true;
    return bool;
  }

  protected Resource buildComponent(File paramFile1, File paramFile2, Resource paramResource, String paramString)
  {
    Resource localResource = super.buildComponent(paramFile1, paramFile2, paramResource, paramString);
    Long localLong = ConstantsHelper.getResourceTypeByCode(paramString);
    if (localLong != null)
      localResource.setPlatform(ConstantsHelper.getPlatform(localLong.longValue()));
    return localResource;
  }

  protected List<String> getComponentImagePrefixes(String paramString)
  {
    ArrayList localArrayList = new ArrayList();
    Long localLong = ConstantsHelper.getResourceTypeByCode(paramString);
    if (localLong == null)
      localArrayList.addAll(super.getComponentImagePrefixes(paramString));
    else
      localArrayList.add(ConstantsHelper.getPreviewPrefix(localLong.longValue()));
    return localArrayList;
  }

  protected Map<String, String> getComponentsMap(File paramFile)
  {
    HashMap localHashMap = new HashMap();
    String[] arrayOfString = COMPONENT_IDENTITIES;
    int j = arrayOfString.length;
    for (int i = 0; ; i++)
    {
      if (i >= j)
        return localHashMap;
      String str = arrayOfString[i];
      Long localLong = ConstantsHelper.getResourceTypeByIdentity(str);
      if (localLong == null)
        continue;
      localHashMap.put(str, ConstantsHelper.getResourceCode(localLong.longValue()));
    }
  }

  protected boolean importSingle(Resource paramResource)
  {
    int i = 0;
    Resource localResource1 = 1;
    Object localObject = unzipBundle(paramResource);
    Resource localResource2;
    if (localObject != null)
    {
      localObject = new File((String)localObject);
      if (((File)localObject).exists())
      {
        if (paramResource.getOnlineId() != null)
          paramResource.setStatus(3);
        else
          paramResource.setStatus(1);
        localResource2 = buildBundle((File)localObject, new Resource(), paramResource);
        paramResource.setHash(localResource2.getHash());
        if ((!importComponents((File)localObject, localResource2, paramResource)) || (localResource1 == 0))
          localResource1 = 0;
        else
          localResource1 = 1;
        ((File)localObject).delete();
        this.controller.getLocalDataManager().addResource(localResource2);
      }
      localResource2 = localResource1;
    }
    return localResource2;
  }

  protected void resolveBuildInImage(File paramFile, String paramString1, Resource paramResource, String paramString2, String paramString3)
  {
    int j = 0;
    int i = 1;
    String str1 = paramString2 + "small_";
    while (true)
    {
      Object localObject = new StringBuilder().append(str1);
      String str2 = j + 1;
      localObject = new File(paramFile, j + paramString3);
      if ((!((File)localObject).exists()) || (((File)localObject).isDirectory()))
      {
        if (i == 0)
          return;
        str1 = paramString2;
        j = 0;
        i = 0;
        continue;
      }
      String str4 = ((File)localObject).getName();
      String str3 = paramString1 + str4;
      paramResource.addBuildInThumbnail(str3);
      if ((!str4.startsWith("preview_cover_")) && (i == 0))
        paramResource.addBuildInPreview(str3);
      Shell.copy(((File)localObject).getAbsolutePath(), str3);
      str3 = str2;
    }
  }

  protected void resolveBuildInImages(File paramFile, Resource paramResource, List<String> paramList)
  {
    paramResource.clearBuildInThumbnails();
    paramResource.clearBuildInPreviews();
    File localFile = new File(paramFile, "preview");
    String str2;
    Iterator localIterator;
    if ((localFile.exists()) && (localFile.isDirectory()))
    {
      str2 = getBuildInImageFolderPath(paramResource.getLocalId());
      new File(str2).mkdirs();
      resolveBuildInImage(localFile, str2, paramResource, "preview_cover_", ".jpg");
      resolveBuildInImage(localFile, str2, paramResource, "preview_cover_", ".png");
      localIterator = paramList.iterator();
    }
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      String str1 = (String)localIterator.next();
      resolveBuildInImage(localFile, str2, paramResource, str1, ".jpg");
      resolveBuildInImage(localFile, str2, paramResource, str1, ".png");
    }
  }

  protected String unzipBundle(Resource paramResource)
  {
    String str = super.unzipBundle(paramResource);
    File localFile1 = new File(str + "fonts/Roboto-Regular.ttf");
    if (!localFile1.exists())
    {
      localFile2 = new File(str + "fonts/Arial.ttf");
      if (localFile2.exists())
        localFile2.renameTo(localFile1);
    }
    File localFile2 = new File(str + "fonts/DroidSansFallback.ttf");
    if ((!localFile1.exists()) && (localFile2.exists()));
    try
    {
      localFile1.createNewFile();
      ResourceHelper.setFileHash(localFile1.getAbsolutePath(), ResourceHelper.getFileHash(localFile2.getAbsolutePath()));
      return str;
    }
    catch (IOException localIOException)
    {
      while (true)
        localIOException.printStackTrace();
    }
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     com.android.thememanager.controller.ThemeImportManager
 * JD-Core Version:    0.6.0
 */