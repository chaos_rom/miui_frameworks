package com.android.thememanager.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.FileUtils;
import android.os.IBinder;
import android.os.RemoteException;
import android.text.TextUtils;
import com.android.thememanager.ThemeResourceConstants;
import com.android.thememanager.util.ThemeHelper;
import java.io.File;
import miui.app.resourcebrowser.service.IThemeManagerService.Stub;

public class ThemeManagerService extends Service
  implements ThemeResourceConstants
{
  private Binder mBinder = new IThemeManagerService.Stub()
  {
    public boolean saveLockWallpaper(String paramString)
      throws RemoteException
    {
      return ThemeManagerService.this.saveLockWallPaper_Impl(paramString);
    }
  };

  private boolean saveLockWallPaper_Impl(String paramString)
  {
    boolean bool = false;
    if (!TextUtils.isEmpty(paramString))
    {
      ThemeHelper.createRuntimeFolder(this);
      bool = FileUtils.copyFile(new File(paramString), new File("/data/system/theme/lock_wallpaper"));
      if (bool)
        ThemeHelper.updateLockWallpaperInfo(this, paramString);
    }
    return bool;
  }

  public IBinder onBind(Intent paramIntent)
  {
    return this.mBinder;
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     com.android.thememanager.service.ThemeManagerService
 * JD-Core Version:    0.6.0
 */