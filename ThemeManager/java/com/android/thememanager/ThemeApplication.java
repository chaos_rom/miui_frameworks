package com.android.thememanager;

import android.app.Application;
import android.os.Build;
import com.android.thememanager.util.DeprecationHelper;
import com.android.thememanager.util.ThemeHelper;
import dalvik.system.VMRuntime;
import java.io.File;

public class ThemeApplication extends Application
  implements ThemeResourceConstants
{
  public void onCreate()
  {
    super.onCreate();
    String str = Build.DEVICE.toLowerCase();
    if ((str.indexOf("mione") < 0) && (str.indexOf("aries") < 0))
      VMRuntime.getRuntime().clearGrowthLimit();
    ThemeHelper.createRuntimeFolder(this);
    ThemeHelper.initResource(this);
    File localFile = new File(getFilesDir().getParent(), "RestoreFromBackup");
    long l;
    if (localFile.exists())
      l = 1L;
    while (true)
    {
      if (l > 262144L)
      {
        localFile.delete();
        new Thread(new Runnable()
        {
          public void run()
          {
            DeprecationHelper.deleteDeprecatedPreviewCache();
          }
        }).start();
        return;
      }
      if ((0xFFFFFFFF & l) != 0L)
        ThemeHelper.saveUserPreferenceTime(l, localFile.lastModified());
      l <<= 1;
    }
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     com.android.thememanager.ThemeApplication
 * JD-Core Version:    0.6.0
 */