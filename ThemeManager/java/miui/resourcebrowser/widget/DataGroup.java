package miui.resourcebrowser.widget;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DataGroup<T> extends ArrayList<T>
{
  private static final long serialVersionUID = 1L;
  private Map<String, Object> extraMeta = new HashMap();
  private String id;
  private String title;

  public Object getExtraMeta(String paramString)
  {
    return this.extraMeta.get(paramString);
  }

  public String getId()
  {
    return this.id;
  }

  public String getTitle()
  {
    return this.title;
  }

  public void putExtraMeta(String paramString, Object paramObject)
  {
    this.extraMeta.put(paramString, paramObject);
  }

  public void setId(String paramString)
  {
    this.id = paramString;
  }

  public void setTitle(String paramString)
  {
    this.title = paramString;
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.widget.DataGroup
 * JD-Core Version:    0.6.0
 */