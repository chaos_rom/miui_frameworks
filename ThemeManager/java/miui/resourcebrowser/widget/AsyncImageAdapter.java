package miui.resourcebrowser.widget;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.util.Log;
import miui.util.ImageUtils;

public abstract class AsyncImageAdapter<T> extends AsyncAdapter<T>
{
  protected boolean useLowQualityDecoding()
  {
    return false;
  }

  public class AsyncLoadImageTask extends AsyncAdapter.AsyncLoadPartialDataTask
  {
    private boolean mScaled;
    private int mTargetHeight;
    private int mTargetWidth;

    public AsyncLoadImageTask()
    {
      super();
    }

    protected Object doJob(Object paramObject)
    {
      Object localObject2 = (String)paramObject;
      Object localObject1 = ImageUtils.getBitmapSize((String)localObject2);
      int j = ((BitmapFactory.Options)localObject1).outWidth;
      int i = ((BitmapFactory.Options)localObject1).outHeight;
      if ((this.mTargetWidth <= 0) || (this.mTargetHeight <= 0))
      {
        Log.i("ResourceBrowser", "AsyncImageAdapter does not set valid parameters for target size.");
        this.mTargetWidth = j;
        this.mTargetHeight = i;
      }
      BitmapFactory.Options localOptions = new BitmapFactory.Options();
      if (AsyncImageAdapter.this.useLowQualityDecoding())
        localOptions.inPreferredConfig = Bitmap.Config.RGB_565;
      Object localObject3 = null;
      localObject1 = null;
      try
      {
        if (!this.mScaled)
        {
          k = Math.min(j, this.mTargetWidth);
          m = Math.min(i, this.mTargetHeight);
          j = (j - k) / 2;
          i = (i - m) / 2;
          localObject1 = BitmapFactory.decodeFile((String)localObject2, localOptions);
          localObject2 = Bitmap.createBitmap((Bitmap)localObject1, j, i, k, m);
        }
        for (localObject3 = localObject2; ; localObject3 = localObject2)
        {
          if ((localObject3 != localObject1) && (localObject1 != null))
            ((Bitmap)localObject1).recycle();
          return localObject3;
          k = this.mTargetWidth;
          m = this.mTargetHeight;
          if ((this.mTargetWidth >= j) || (this.mTargetHeight >= i))
            break;
          localOptions.inSampleSize = Math.min(j / k, i / m);
          localObject1 = BitmapFactory.decodeFile((String)localObject2, localOptions);
          localObject2 = ImageUtils.scaleBitmapToDesire((Bitmap)localObject1, k, m, false);
        }
      }
      catch (OutOfMemoryError localOutOfMemoryError)
      {
        while (true)
          localOutOfMemoryError.printStackTrace();
      }
      catch (Exception localException)
      {
        while (true)
        {
          continue;
          int k = j;
          int m = i;
        }
      }
    }

    public void setScaled(boolean paramBoolean)
    {
      this.mScaled = paramBoolean;
    }

    public void setTargetSize(int paramInt1, int paramInt2)
    {
      this.mTargetWidth = paramInt1;
      this.mTargetHeight = paramInt2;
    }
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.widget.AsyncImageAdapter
 * JD-Core Version:    0.6.0
 */