package miui.resourcebrowser.widget;

import android.graphics.Bitmap;
import android.os.AsyncTask.Status;
import android.util.Pair;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import miui.cache.DataCache;
import miui.os.DaemonAsyncTask;
import miui.os.UniqueAsyncTask;

public abstract class AsyncAdapter<T> extends BaseAdapter
{
  private static DataCache<Object, Object> sPartialDataCache = new DataCache()
  {
    private static final long serialVersionUID = 1L;

    protected boolean removeEldestEntry(Map.Entry<Object, Object> paramEntry)
    {
      boolean bool = super.removeEldestEntry(paramEntry);
      if (bool);
      try
      {
        Bitmap localBitmap = (Bitmap)(Bitmap)paramEntry.getValue();
        if (localBitmap != null)
          localBitmap.recycle();
        label31: return bool;
      }
      catch (Exception localException)
      {
        break label31;
      }
    }
  };
  private boolean mAutoLoadDownwardsMore;
  private boolean mAutoLoadUpwardsMore;
  private boolean mBackgroundLoad = true;
  private int mDataPerLine = 1;
  private List<DataGroup<T>> mDataSet = new ArrayList();
  private AsyncAdapter<T>.AsyncLoadPartialDataTask mLoadPartialDataTask;
  private int mPreloadOffset;
  protected boolean mReachBottom;
  protected boolean mReachTop;
  private Set<UniqueAsyncTask<?, ?, ?>> mTaskSet = new HashSet();

  private DataGroup<T> getDataGroup(int paramInt)
  {
    if (this.mDataSet.size() <= paramInt)
      monitorenter;
    try
    {
      if (this.mDataSet.size() <= paramInt)
        for (int i = this.mDataSet.size(); i <= paramInt; i++)
          this.mDataSet.add(new DataGroup());
      return (DataGroup)this.mDataSet.get(paramInt);
    }
    finally
    {
      monitorexit;
    }
    throw localObject;
  }

  private void loadData(int paramInt, AsyncAdapter<T>.AsyncLoadDataTask paramAsyncAdapter)
  {
    if (paramAsyncAdapter == null);
    while (true)
    {
      return;
      paramAsyncAdapter.setId("loadData-" + paramInt);
      paramAsyncAdapter.setGroup(paramInt);
      try
      {
        paramAsyncAdapter.execute(new Void[0]);
      }
      catch (IllegalStateException localIllegalStateException)
      {
      }
    }
  }

  private void loadMoreData(boolean paramBoolean, int paramInt, AsyncAdapter<T>.AsyncLoadMoreDataTask paramAsyncAdapter)
  {
    if (paramAsyncAdapter == null)
      return;
    AsyncLoadMoreParams localAsyncLoadMoreParams = new AsyncLoadMoreParams();
    localAsyncLoadMoreParams.upwards = paramBoolean;
    if ((paramBoolean) || (getDataCount(paramInt) == 0));
    for (localAsyncLoadMoreParams.cursor = 0; ; localAsyncLoadMoreParams.cursor = getDataCount(paramInt))
    {
      while (true)
      {
        paramAsyncAdapter.setLoadParams(localAsyncLoadMoreParams);
        paramAsyncAdapter.setId("loadMoreData-" + paramInt);
        paramAsyncAdapter.setGroup(paramInt);
        try
        {
          paramAsyncAdapter.execute(new Void[0]);
        }
        catch (IllegalStateException localIllegalStateException)
        {
        }
      }
      break;
    }
  }

  protected abstract View bindContentView(View paramView, List<T> paramList, int paramInt1, int paramInt2, int paramInt3);

  protected abstract void bindPartialContentView(View paramView, T paramT, int paramInt, List<Object> paramList);

  public void clean()
  {
    if (this.mLoadPartialDataTask != null)
      this.mLoadPartialDataTask.stop();
  }

  public void clearDataSet()
  {
    this.mDataSet.clear();
    notifyDataSetInvalidated();
  }

  protected abstract List<Object> getCacheKeys(T paramT);

  public int getCount()
  {
    int i = 0;
    for (int j = 0; ; j++)
    {
      if (j >= this.mDataSet.size())
        return i;
      i += getCount(j);
    }
  }

  protected int getCount(int paramInt)
  {
    int i = getDataCount(paramInt);
    if (i != 0)
      i = 1 + (i - 1) / this.mDataPerLine;
    else
      i = 0;
    return i;
  }

  public int getDataCount(int paramInt)
  {
    return getDataGroup(paramInt).size();
  }

  public T getDataItem(int paramInt1, int paramInt2)
  {
    return getDataGroup(paramInt2).get(paramInt1);
  }

  public int getDataPerLine()
  {
    return this.mDataPerLine;
  }

  public List<DataGroup<T>> getDataSet()
  {
    return this.mDataSet;
  }

  public Pair<Integer, Integer> getGroupPosition(int paramInt)
  {
    int i = 0;
    int k = 0;
    Pair localPair;
    while (true)
    {
      Object localObject1;
      if (k >= this.mDataSet.size())
      {
        localObject1 = null;
      }
      else
      {
        Object localObject2 = getCount(k);
        int j;
        if (paramInt >= localObject1 + localObject2)
        {
          localObject1 += localObject2;
          k++;
          continue;
        }
        localPair = new Pair(Integer.valueOf(paramInt - j), Integer.valueOf(k));
      }
    }
    return localPair;
  }

  public List<T> getItem(int paramInt)
  {
    Pair localPair = getGroupPosition(paramInt);
    return getItem(((Integer)localPair.first).intValue(), ((Integer)localPair.second).intValue());
  }

  protected List<T> getItem(int paramInt1, int paramInt2)
  {
    int j = paramInt1 * this.mDataPerLine;
    int k = Math.min(this.mDataPerLine, getDataCount(paramInt2) - j);
    ArrayList localArrayList = new ArrayList();
    for (int i = 0; ; i++)
    {
      if (i >= k)
        return localArrayList;
      localArrayList.add(getDataItem(j + i, paramInt2));
    }
  }

  public long getItemId(int paramInt)
  {
    return paramInt;
  }

  protected List<AsyncAdapter<T>.AsyncLoadDataTask> getLoadDataTask()
  {
    return null;
  }

  protected List<AsyncAdapter<T>.AsyncLoadMoreDataTask> getLoadMoreDataTask()
  {
    return null;
  }

  protected AsyncAdapter<T>.AsyncLoadPartialDataTask getLoadPartialDataTask()
  {
    return null;
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    if (getCount() != 0)
    {
      Object localObject2 = getGroupPosition(paramInt);
      localObject1 = getItem(((Integer)((Pair)localObject2).first).intValue(), ((Integer)((Pair)localObject2).second).intValue());
      localObject2 = bindContentView(paramView, (List)localObject1, paramInt, ((Integer)((Pair)localObject2).first).intValue(), ((Integer)((Pair)localObject2).second).intValue());
      for (int i = -1 + ((List)localObject1).size(); ; i--)
      {
        if (i < 0)
        {
          if ((this.mReachTop) || (paramInt != this.mPreloadOffset) || (!this.mAutoLoadUpwardsMore))
          {
            if ((!this.mReachBottom) && (paramInt == -1 + getCount() - this.mPreloadOffset) && (this.mAutoLoadDownwardsMore))
              loadMoreData(false);
          }
          else
            loadMoreData(true);
          localObject1 = localObject2;
          break;
        }
        loadPartialData((View)localObject2, ((List)localObject1).get(i), i);
      }
    }
    Object localObject1 = null;
    return (View)(View)localObject1;
  }

  protected boolean isValidKey(Object paramObject, T paramT, int paramInt)
  {
    return sPartialDataCache.containsKey(paramObject);
  }

  public void loadData()
  {
    List localList = getLoadDataTask();
    if (localList != null);
    for (int i = 0; ; i++)
    {
      if (i >= localList.size())
        return;
      loadData(i, (AsyncLoadDataTask)localList.get(i));
    }
  }

  public void loadData(int paramInt)
  {
    List localList = getLoadDataTask();
    if ((localList != null) && (localList.size() > paramInt))
      loadData(paramInt, (AsyncLoadDataTask)localList.get(paramInt));
  }

  public void loadMoreData(boolean paramBoolean)
  {
    List localList = getLoadMoreDataTask();
    if (localList != null);
    for (int i = 0; ; i++)
    {
      if (i >= localList.size())
        return;
      loadMoreData(paramBoolean, i, (AsyncLoadMoreDataTask)localList.get(i));
    }
  }

  public void loadMoreData(boolean paramBoolean, int paramInt)
  {
    List localList = getLoadMoreDataTask();
    if ((localList != null) && (localList.size() > paramInt))
      loadMoreData(paramBoolean, paramInt, (AsyncLoadMoreDataTask)localList.get(paramInt));
  }

  protected void loadPartialData(View paramView, T paramT, int paramInt)
  {
    if ((this.mLoadPartialDataTask == null) || (this.mLoadPartialDataTask.getStatus() == AsyncTask.Status.FINISHED))
    {
      this.mLoadPartialDataTask = getLoadPartialDataTask();
      if (this.mLoadPartialDataTask != null);
    }
    while (true)
    {
      return;
      if (this.mLoadPartialDataTask.getStatus() == AsyncTask.Status.PENDING);
      try
      {
        this.mLoadPartialDataTask.execute(new Void[0]);
        label61: ArrayList localArrayList = new ArrayList();
        List localList = getCacheKeys(paramT);
        int i = 0;
        if (i < localList.size())
        {
          Object localObject = localList.get(i);
          if (isValidKey(localObject, paramT, i))
            localArrayList.add(sPartialDataCache.get(localObject));
          while (true)
          {
            i++;
            break;
            if (this.mLoadPartialDataTask.isJobDoing(localObject))
              continue;
            this.mLoadPartialDataTask.addJob(localObject);
          }
        }
        bindPartialContentView(paramView, paramT, paramInt, localArrayList);
      }
      catch (IllegalStateException localIllegalStateException)
      {
        break label61;
      }
    }
  }

  public boolean loadingData()
  {
    int i;
    if (this.mTaskSet.isEmpty())
      i = 0;
    else
      i = 1;
    return i;
  }

  protected void postLoadData(List<T> paramList)
  {
  }

  protected void postLoadMoreData(List<T> paramList)
  {
  }

  public void setAutoLoadMoreStyle(int paramInt)
  {
    boolean bool1 = true;
    boolean bool2;
    if ((paramInt & 0x1) == 0)
      bool2 = false;
    else
      bool2 = bool1;
    this.mAutoLoadUpwardsMore = bool2;
    if ((paramInt & 0x2) == 0)
      bool1 = false;
    this.mAutoLoadDownwardsMore = bool1;
  }

  public void setDataPerLine(int paramInt)
  {
    this.mDataPerLine = paramInt;
  }

  public void setPreloadOffset(int paramInt)
  {
    this.mPreloadOffset = paramInt;
  }

  public abstract class AsyncLoadPartialDataTask extends DaemonAsyncTask<Object, Object>
  {
    Set<Object> mDoingJobs = Collections.synchronizedSet(new HashSet());

    public AsyncLoadPartialDataTask()
    {
    }

    public boolean isJobDoing(Object paramObject)
    {
      return this.mDoingJobs.contains(paramObject);
    }

    protected boolean needsDoJob(Object paramObject)
    {
      int i;
      if ((AsyncAdapter.sPartialDataCache.containsKey(paramObject)) || (isJobDoing(paramObject)))
        i = 0;
      else
        i = 1;
      if (i != 0)
        this.mDoingJobs.add(paramObject);
      return i;
    }

    protected void onProgressUpdate(Pair<Object, Object>[] paramArrayOfPair)
    {
      if ((paramArrayOfPair != null) && (paramArrayOfPair.length != 0))
      {
        Object localObject2 = paramArrayOfPair[0].first;
        Object localObject1 = paramArrayOfPair[0].second;
        if (localObject1 != null)
        {
          AsyncAdapter.sPartialDataCache.put(localObject2, localObject1);
          AsyncAdapter.this.notifyDataSetChanged();
        }
        this.mDoingJobs.remove(localObject2);
        super.onProgressUpdate(paramArrayOfPair);
      }
    }
  }

  public abstract class AsyncLoadMoreDataTask extends UniqueAsyncTask<Void, T, List<T>>
  {
    private boolean mClearData;
    private int mGroup;
    private AsyncAdapter.AsyncLoadMoreParams mLoadParams;

    public AsyncLoadMoreDataTask()
    {
    }

    protected List<T> doInBackground(Void[] paramArrayOfVoid)
    {
      boolean bool = false;
      List localList;
      if (this.mLoadParams != null)
      {
        localList = loadMoreData(this.mLoadParams);
        AsyncAdapter localAsyncAdapter;
        if (!this.mLoadParams.upwards)
        {
          localAsyncAdapter = AsyncAdapter.this;
          if ((localList == null) || (localList.size() == 0))
            bool = true;
          localAsyncAdapter.mReachBottom = bool;
        }
        else
        {
          localAsyncAdapter = AsyncAdapter.this;
          if ((localList == null) || (localList.size() == 0))
            bool = true;
          localAsyncAdapter.mReachTop = bool;
        }
      }
      else
      {
        localList = null;
      }
      return localList;
    }

    protected boolean hasEquivalentRunningTasks()
    {
      return AsyncAdapter.this.mTaskSet.contains(this);
    }

    protected abstract List<T> loadMoreData(AsyncAdapter.AsyncLoadMoreParams paramAsyncLoadMoreParams);

    protected void onPostExecute(List<T> paramList)
    {
      if ((this.mClearData) && (paramList != null))
        AsyncAdapter.this.getDataGroup(this.mGroup).clear();
      if (paramList != null);
      for (int i = 0; ; i++)
      {
        if (i >= paramList.size())
        {
          AsyncAdapter.this.notifyDataSetChanged();
          super.onPostExecute(paramList);
          AsyncAdapter.this.mTaskSet.remove(this);
          AsyncAdapter.this.postLoadMoreData(paramList);
          return;
        }
        AsyncAdapter.this.getDataGroup(this.mGroup).add(paramList.get(i));
      }
    }

    protected void onPreExecute()
    {
      super.onPreExecute();
      AsyncAdapter.this.mTaskSet.add(this);
    }

    public void setGroup(int paramInt)
    {
      this.mGroup = paramInt;
    }

    public void setLoadParams(AsyncAdapter.AsyncLoadMoreParams paramAsyncLoadMoreParams)
    {
      this.mLoadParams = paramAsyncLoadMoreParams;
    }
  }

  public static class AsyncLoadMoreParams
  {
    public int cursor;
    public boolean upwards;
  }

  public abstract class AsyncLoadDataTask extends UniqueAsyncTask<Void, T, List<T>>
  {
    private boolean mFirstTimeLoad = true;
    private int mGroup;
    private List<T> mResultDataSet = new ArrayList();
    private List<T> mTempDataSet = new ArrayList();
    private List<AsyncAdapter.AsyncLoadDataVisitor<T>> mVisitors = new ArrayList();

    public AsyncLoadDataTask()
    {
    }

    private boolean needsExecuteTask()
    {
      int i;
      if (getMode() == 2)
      {
        for (i = 0; ; i++)
        {
          if (i >= this.mVisitors.size())
          {
            i = 0;
            break label62;
          }
          if (((AsyncAdapter.AsyncLoadDataVisitor)this.mVisitors.get(i)).dataChanged())
            break;
        }
        i = 1;
      }
      else
      {
        i = 1;
      }
      label62: return i;
    }

    protected List<T> doInBackground(Void[] paramArrayOfVoid)
    {
      int i = getMode();
      if (i != 2)
      {
        Object[] arrayOfObject;
        if (i != 1)
        {
          arrayOfObject = loadData();
          if (arrayOfObject == null)
            break label90;
          Collections.addAll(this.mResultDataSet, arrayOfObject);
          publishProgress(arrayOfObject);
          break label90;
        }
        int k = 0;
        while (true)
        {
          arrayOfObject = loadData(k);
          if (arrayOfObject == null)
            break;
          Collections.addAll(this.mResultDataSet, arrayOfObject);
          publishProgress(arrayOfObject);
          k += arrayOfObject.length;
        }
      }
      for (int j = 0; ; j++)
      {
        if (j >= this.mVisitors.size())
          label90: return this.mResultDataSet;
        ((AsyncAdapter.AsyncLoadDataVisitor)this.mVisitors.get(j)).loadData(this);
      }
    }

    protected abstract int getMode();

    protected boolean hasEquivalentRunningTasks()
    {
      return AsyncAdapter.this.mTaskSet.contains(this);
    }

    protected T[] loadData()
    {
      return null;
    }

    protected T[] loadData(int paramInt)
    {
      return null;
    }

    protected void onPostExecute(List<T> paramList)
    {
      if ((AsyncAdapter.this.mBackgroundLoad) && (!this.mFirstTimeLoad))
      {
        AsyncAdapter.this.getDataGroup(this.mGroup).clear();
        AsyncAdapter.this.getDataGroup(this.mGroup).addAll(this.mTempDataSet);
        AsyncAdapter.this.notifyDataSetChanged();
      }
      super.onPostExecute(paramList);
      AsyncAdapter.this.mTaskSet.remove(this);
      AsyncAdapter.this.postLoadData(paramList);
    }

    protected void onPreExecute()
    {
      boolean bool = false;
      super.onPreExecute();
      if (needsExecuteTask())
      {
        if (AsyncAdapter.this.getDataGroup(this.mGroup).size() == 0)
          bool = true;
        this.mFirstTimeLoad = bool;
        if ((AsyncAdapter.this.mBackgroundLoad) && (!this.mFirstTimeLoad))
          this.mTempDataSet.clear();
        else
          AsyncAdapter.this.getDataGroup(this.mGroup).clear();
        AsyncAdapter.this.mTaskSet.add(this);
      }
      else
      {
        cancel(false);
      }
    }

    protected void onProgressUpdate(T[] paramArrayOfT)
    {
      for (int i = 0; ; i++)
      {
        if (i >= paramArrayOfT.length)
        {
          if ((!AsyncAdapter.this.mBackgroundLoad) || (this.mFirstTimeLoad))
            AsyncAdapter.this.notifyDataSetChanged();
          super.onProgressUpdate(paramArrayOfT);
          return;
        }
        if ((AsyncAdapter.this.mBackgroundLoad) && (!this.mFirstTimeLoad))
          this.mTempDataSet.add(paramArrayOfT[i]);
        else
          AsyncAdapter.this.getDataGroup(this.mGroup).add(paramArrayOfT[i]);
      }
    }

    public void setGroup(int paramInt)
    {
      this.mGroup = paramInt;
    }
  }

  public static abstract interface AsyncLoadDataVisitor<T>
  {
    public abstract boolean dataChanged();

    public abstract void loadData(AsyncAdapter<T>.AsyncLoadDataTask paramAsyncAdapter);
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.widget.AsyncAdapter
 * JD-Core Version:    0.6.0
 */