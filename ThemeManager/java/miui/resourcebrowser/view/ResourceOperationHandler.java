package miui.resourcebrowser.view;

import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;
import java.io.File;
import miui.app.ActivityLifecycleObserver;
import miui.net.PaymentManager;
import miui.net.PaymentManager.PaymentListener;
import miui.resourcebrowser.ResourceContext;
import miui.resourcebrowser.controller.OnlineDataManager;
import miui.resourcebrowser.controller.ResourceController;
import miui.resourcebrowser.controller.online.DrmService;
import miui.resourcebrowser.controller.online.OnlineService;
import miui.resourcebrowser.model.Resource;
import miui.resourcebrowser.util.ResourceDownloadHandler;
import miui.resourcebrowser.util.ResourceDownloadHandler.ResourceDownloadListener;
import miui.resourcebrowser.util.ResourceHelper;

public class ResourceOperationHandler
  implements ActivityLifecycleObserver, ResourceDownloadHandler.ResourceDownloadListener, ResourceOperationView.ResourceOperationListener
{
  private int mCheckRightsCountDuringApplyEvent = 0;
  protected Context mContext;
  protected ResourceDownloadHandler mDownloadHandler;
  private boolean mIsLegal;
  protected LoadingStateInfo mLoadingInfo = new LoadingStateInfo();
  protected ResourceOperationView mOperationView;
  protected ResourceContext mResContext;
  protected ResourceController mResController;
  protected Resource mResource;
  protected DrmService mService;

  public ResourceOperationHandler(Context paramContext, ResourceContext paramResourceContext, ResourceOperationView paramResourceOperationView)
  {
    if (paramResourceOperationView != null)
    {
      this.mContext = paramContext;
      this.mResContext = paramResourceContext;
      this.mOperationView = paramResourceOperationView;
      this.mOperationView.setResourceOperationHandler(this);
      this.mDownloadHandler = new ResourceDownloadHandler(this.mContext, this.mResContext);
      this.mDownloadHandler.setResourceDownloadListener(this);
      this.mDownloadHandler.registerDownloadReceiver();
      this.mService = new DrmService(this.mResContext);
      return;
    }
    throw new RuntimeException("Operated view can not be null.");
  }

  private ProductState checkProductState()
  {
    Object localObject = this.mResController.getOnlineDataManager().getResource(this.mResource.getOnlineId(), true);
    if (localObject != null)
    {
      if (!((Resource)localObject).isProductBought())
      {
        localObject = ProductState.NOT_BOUGHT;
      }
      else
      {
        this.mResource.setProductBought(true);
        localObject = ProductState.HAS_BOUGHT;
      }
    }
    else
      localObject = ProductState.UNKOWN_PRODUCT;
    return (ProductState)localObject;
  }

  private void downloadResource()
  {
    new Thread(new Runnable()
    {
      public void run()
      {
        ResourceOperationHandler.this.mService.downloadRights(ResourceOperationHandler.this.mResource);
      }
    }).start();
    this.mDownloadHandler.downloadResource(this.mResource);
    this.mOperationView.updateStatus();
  }

  private void reset()
  {
    this.mIsLegal = false;
  }

  public LoadingStateInfo getLoadingStateInfo()
  {
    return this.mLoadingInfo;
  }

  public int getPrice()
  {
    return this.mResource.getProductPrice();
  }

  public boolean isAuthorizedResource()
  {
    return this.mResource.isProductBought();
  }

  public boolean isDeletable()
  {
    int i;
    if ((!isLocalResource()) || (ResourceHelper.isSystemResource(this.mResource.getLocalPath())))
      i = 0;
    else
      i = 1;
    return i;
  }

  public boolean isDownloading()
  {
    int i;
    if ((this.mResource.getDownloadPath() == null) || (!this.mDownloadHandler.isResourceDownloading(this.mResource.getOnlineId())))
      i = 0;
    else
      i = 1;
    return i;
  }

  protected boolean isLegal()
  {
    return this.mIsLegal;
  }

  public boolean isLocalResource()
  {
    int i;
    if ((this.mResource.getLocalPath() == null) || (!new File(this.mResource.getLocalPath()).exists()))
      i = 0;
    else
      i = 1;
    return i;
  }

  public boolean isOldResource()
  {
    int i;
    if ((this.mResource.getLocalPath() == null) || (!ResourceHelper.isOldResource(this.mResource.getStatus())))
      i = 0;
    else
      i = 1;
    return i;
  }

  public boolean isPicker()
  {
    return this.mResContext.isPicker();
  }

  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
  }

  public final void onApplyEventPerformed()
  {
    this.mCheckRightsCountDuringApplyEvent = 0;
    onCheckResourceRightEventBeforeRealApply();
  }

  public void onBuyEventPerformed()
  {
    if (!TextUtils.isEmpty(this.mResource.getProductId()))
    {
      new ProductBoughtTask().execute(new Void[0]);
    }
    else
    {
      if (!OnlineService.isNetworkAvailable())
        Toast.makeText(this.mContext, 101449760, 1).show();
      Log.i("Theme", "Fail to buy resource because of emtry product ID.");
    }
  }

  protected void onCheckResourceRightEventBeforeRealApply()
  {
    if (!this.mIsLegal)
    {
      int i = 1 + this.mCheckRightsCountDuringApplyEvent;
      this.mCheckRightsCountDuringApplyEvent = i;
      if (i <= 2)
      {
        new CheckRightsTask().execute(new Void[0]);
      }
      else
      {
        Log.i("Theme", "Fail to get theme auth because of exceeding max count of checking.");
        Toast.makeText(this.mContext, 101450317, 0).show();
      }
    }
    else
    {
      onRealApplyResourceEvent();
    }
  }

  public void onCreate(Bundle paramBundle)
  {
    this.mDownloadHandler.registerDownloadReceiver();
  }

  public void onDeleteEventPerformed()
  {
  }

  public void onDestroy()
  {
    this.mDownloadHandler.unregisterDownloadReceiver();
  }

  public void onDownloadEventPerformed()
  {
    downloadResource();
  }

  public void onDownloadFailed(String paramString1, String paramString2)
  {
    if (paramString1.equals(this.mResource.getDownloadPath()))
    {
      Toast.makeText(this.mContext, 101449763, 0).show();
      this.mOperationView.updateStatus();
    }
  }

  public void onDownloadProgressUpdated(String paramString1, String paramString2, int paramInt1, int paramInt2)
  {
    if (paramString1.equals(this.mResource.getDownloadPath()))
      this.mOperationView.updateDownloadProgressBar(paramInt1, paramInt2);
  }

  public void onDownloadSuccessful(String paramString1, String paramString2)
  {
    if (paramString1.equals(this.mResource.getDownloadPath()))
    {
      this.mResource.setStatus(3);
      this.mOperationView.updateStatus();
    }
  }

  public void onMagicEventPerformed()
  {
  }

  public void onPause()
  {
  }

  public void onPickEventPerformed()
  {
    Intent localIntent = new Intent();
    Object localObject = this.mResource.getFilePath();
    localIntent.putExtra("RESPONSE_PICKED_RESOURCE", (String)localObject);
    localIntent.putExtra("RESPONSE_TRACK_ID", this.mResContext.getTrackId());
    if (this.mResContext.getResourceFormat() == 3)
      localIntent.putExtra("android.intent.extra.ringtone.PICKED_URI", ResourceHelper.getUriByPath((String)localObject));
    localObject = (Activity)this.mContext;
    ((Activity)localObject).setResult(-1, localIntent);
    ((Activity)localObject).finish();
  }

  protected void onRealApplyResourceEvent()
  {
  }

  public void onResume()
  {
  }

  public void onStart()
  {
  }

  public void onStop()
  {
  }

  public void onUpdateEventPerformed()
  {
    downloadResource();
  }

  public void setResource(Resource paramResource)
  {
    this.mResource = paramResource;
    reset();
    this.mOperationView.updateStatus();
  }

  public void setResourceController(ResourceController paramResourceController)
  {
    this.mResController = paramResourceController;
  }

  public void updateLoadingState(int paramInt, String paramString)
  {
    this.mLoadingInfo.delayTime = paramInt;
    this.mLoadingInfo.title = paramString;
    this.mOperationView.updateStatus();
  }

  protected class ProductBoughtTask extends AsyncTask<Void, Void, ResourceOperationHandler.ProductState>
  {
    protected ProductBoughtTask()
    {
    }

    protected ResourceOperationHandler.ProductState doInBackground(Void[] paramArrayOfVoid)
    {
      return ResourceOperationHandler.this.checkProductState();
    }

    protected void onPostExecute(ResourceOperationHandler.ProductState paramProductState)
    {
      if (!((Activity)ResourceOperationHandler.this.mContext).isFinishing())
      {
        1 local1 = new PaymentManager.PaymentListener()
        {
          private void afterBoughtSuccess()
          {
            ResourceOperationHandler.this.mResource.setProductBought(true);
            if (!ResourceHelper.isLocalResource(ResourceOperationHandler.this.mResource.getStatus()))
              ResourceOperationHandler.this.onDownloadEventPerformed();
            else
              ResourceOperationHandler.this.onCheckResourceRightEventBeforeRealApply();
            ResourceOperationHandler.this.updateLoadingState(-1, null);
          }

          public void onFailed(String paramString1, int paramInt, String paramString2)
          {
            boolean bool;
            if (paramInt != 7)
              bool = false;
            else
              bool = true;
            if (!bool)
            {
              if (ResourceHelper.isLocalResource(ResourceOperationHandler.this.mResource.getStatus()))
                Toast.makeText(ResourceOperationHandler.this.mContext, 101450295, 1).show();
            }
            else
              afterBoughtSuccess();
            ResourceOperationHandler.this.updateLoadingState(-1, null);
            Log.i("Theme", "PaymentListener: purchase failed: duplicatePurchase=" + bool);
          }

          public void onSuccess(String paramString, Bundle paramBundle)
          {
            afterBoughtSuccess();
            Log.i("Theme", "PaymentListener: purchase success");
          }
        };
        if (paramProductState != ResourceOperationHandler.ProductState.HAS_BOUGHT)
        {
          if (paramProductState != ResourceOperationHandler.ProductState.UNKOWN_PRODUCT)
          {
            Bundle localBundle = new Bundle();
            boolean bool;
            if (ResourceOperationHandler.this.mResource.getProductPrice() != 0)
              bool = false;
            else
              bool = true;
            localBundle.putBoolean("payment_quick_payment", bool);
            PaymentManager.get(ResourceOperationHandler.this.mContext).pay((Activity)ResourceOperationHandler.this.mContext, "thm", ResourceOperationHandler.this.mResource.getProductId(), localBundle, local1);
          }
          else
          {
            local1.onFailed(ResourceOperationHandler.this.mResource.getProductId(), -1, "unkown product");
          }
        }
        else
          local1.onSuccess(ResourceOperationHandler.this.mResource.getProductId(), null);
        Log.i("Theme", "ProductBoughtTask return: " + paramProductState);
      }
    }

    protected void onPreExecute()
    {
      ResourceOperationHandler.this.updateLoadingState(0, null);
    }
  }

  protected class DownloadRightsTask extends AsyncTask<Void, Void, Integer>
  {
    protected DownloadRightsTask()
    {
    }

    protected Integer doInBackground(Void[] paramArrayOfVoid)
    {
      int i = 1;
      if (OnlineService.getAccount() != null)
        i = ResourceOperationHandler.this.mService.downloadRights(ResourceOperationHandler.this.mResource);
      else
        AccountManager.get(ResourceOperationHandler.this.mContext).addAccount("com.xiaomi", "thememarket", null, null, (Activity)ResourceOperationHandler.this.mContext, null, null);
      return Integer.valueOf(i);
    }

    protected void onPostExecute(Integer paramInteger)
    {
      if (!((Activity)ResourceOperationHandler.this.mContext).isFinishing())
      {
        ResourceOperationHandler.this.updateLoadingState(-1, null);
        switch (paramInteger.intValue())
        {
        case 0:
          ResourceOperationHandler.this.onCheckResourceRightEventBeforeRealApply();
          break;
        case 1:
          ResourceOperationHandler.this.onBuyEventPerformed();
          break;
        case 2:
          Toast.makeText(ResourceOperationHandler.this.mContext, 101449760, 0).show();
        }
        Log.i("Theme", "DownloadRightsTask return: " + paramInteger);
      }
    }

    protected void onPreExecute()
    {
      ResourceOperationHandler.this.updateLoadingState(0, ResourceOperationHandler.this.mContext.getString(101450294));
    }
  }

  protected class CheckRightsTask extends AsyncTask<Void, Void, Boolean>
  {
    protected CheckRightsTask()
    {
    }

    protected Boolean doInBackground(Void[] paramArrayOfVoid)
    {
      return Boolean.valueOf(ResourceOperationHandler.this.mService.isLegal(ResourceOperationHandler.this.mResource));
    }

    protected void onPostExecute(Boolean paramBoolean)
    {
      if (!((Activity)ResourceOperationHandler.this.mContext).isFinishing())
      {
        ResourceOperationHandler.this.updateLoadingState(-1, null);
        if (!paramBoolean.booleanValue())
        {
          if (!OnlineService.isNetworkAvailable())
            Toast.makeText(ResourceOperationHandler.this.mContext, 101449760, 0).show();
          else
            new ResourceOperationHandler.DownloadRightsTask(ResourceOperationHandler.this).execute(new Void[0]);
        }
        else
        {
          ResourceOperationHandler.access$002(ResourceOperationHandler.this, true);
          ResourceOperationHandler.this.onRealApplyResourceEvent();
        }
        Log.i("Theme", "CheckRightsTask return: " + paramBoolean);
      }
    }

    protected void onPreExecute()
    {
      ResourceOperationHandler.this.updateLoadingState(1000, ResourceOperationHandler.this.mContext.getString(101450293));
    }
  }

  protected static enum ProductState
  {
    static
    {
      ProductState[] arrayOfProductState = new ProductState[3];
      arrayOfProductState[0] = HAS_BOUGHT;
      arrayOfProductState[1] = NOT_BOUGHT;
      arrayOfProductState[2] = UNKOWN_PRODUCT;
      $VALUES = arrayOfProductState;
    }
  }

  public static class LoadingStateInfo
  {
    public int delayTime = -1;
    public String title = null;
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.view.ResourceOperationHandler
 * JD-Core Version:    0.6.0
 */