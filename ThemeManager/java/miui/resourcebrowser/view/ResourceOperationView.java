package miui.resourcebrowser.view;

import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

public class ResourceOperationView extends LinearLayout
{
  protected TextView mApplyBtn;
  protected View mControlBtns;
  protected ImageView mDeleteBtn;
  protected TextView mDownloadBtn;
  protected ProgressBar mDownloadProgress;
  protected Handler mHandler;
  protected ResourceOperationListener mListener;
  protected View mLoadingProgress;
  protected TextView mLoadingTextView;
  protected ImageView mMagicBtn;
  protected int mMagicBtnResId;
  protected ResourceOperationHandler mResourceHandler;

  public ResourceOperationView(Context paramContext)
  {
    super(paramContext);
  }

  public ResourceOperationView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  public ResourceOperationView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }

  private void disableAndDelayEnableView(View paramView)
  {
    paramView.setEnabled(false);
    getHandler().postDelayed(new Runnable(paramView)
    {
      public void run()
      {
        this.val$v.setEnabled(true);
      }
    }
    , 300L);
  }

  private String formatPrice(int paramInt)
  {
    Object localObject = null;
    if (paramInt != 0)
    {
      if (paramInt > 0)
      {
        localObject = new Object[1];
        localObject[0] = Float.valueOf(paramInt / 100.0F);
        for (localObject = String.format("%.2f", localObject); ; localObject = ((String)localObject).substring(0, -1 + ((String)localObject).length()))
        {
          if (((String)localObject).charAt(-1 + ((String)localObject).length()) == '0')
            continue;
          if (((String)localObject).charAt(-1 + ((String)localObject).length()) == '.')
            localObject = ((String)localObject).substring(0, -1 + ((String)localObject).length());
          localObject = (String)localObject + this.mContext.getString(101450291);
          break;
        }
      }
    }
    else
      localObject = this.mContext.getString(101450290);
    return (String)localObject;
  }

  private void notifyApplyEventPerformed()
  {
    if (this.mListener != null)
      this.mListener.onApplyEventPerformed();
    this.mResourceHandler.onApplyEventPerformed();
    updateStatus();
  }

  private void notifyBuyEventPerformed()
  {
    if (this.mListener != null)
      this.mListener.onBuyEventPerformed();
    this.mResourceHandler.onBuyEventPerformed();
    updateStatus();
  }

  private void notifyDeleteEventPerformed()
  {
    if (this.mListener != null)
      this.mListener.onDeleteEventPerformed();
    this.mResourceHandler.onDeleteEventPerformed();
    updateStatus();
  }

  private void notifyDownloadEventPerformed()
  {
    if (this.mListener != null)
      this.mListener.onDownloadEventPerformed();
    this.mResourceHandler.onDownloadEventPerformed();
    updateStatus();
  }

  private void notifyMagicEventPerformed()
  {
    if (this.mListener != null)
      this.mListener.onMagicEventPerformed();
    this.mResourceHandler.onMagicEventPerformed();
    updateStatus();
  }

  private void notifyPickEventPerformed()
  {
    if (this.mListener != null)
      this.mListener.onPickEventPerformed();
    this.mResourceHandler.onPickEventPerformed();
    updateStatus();
  }

  private void notifyUpdateEventPerformed()
  {
    if (this.mListener != null)
      this.mListener.onUpdateEventPerformed();
    this.mResourceHandler.onUpdateEventPerformed();
    updateStatus();
  }

  private void setApplyStatus()
  {
    String str = null;
    State localState = State.NONE;
    if ((this.mResourceHandler.isLocalResource()) && (!this.mResourceHandler.isDownloading()))
      if (!this.mResourceHandler.isPicker())
      {
        str = this.mContext.getString(101449753);
        localState = State.APPLY;
      }
      else
      {
        str = this.mContext.getString(101449755);
        localState = State.PICK;
      }
    if (str != null)
    {
      this.mApplyBtn.setVisibility(0);
      this.mApplyBtn.setText(str);
      this.mApplyBtn.setTag(localState);
    }
    else
    {
      this.mApplyBtn.setVisibility(8);
    }
  }

  private void setDeleteStatus()
  {
    ImageView localImageView = this.mDeleteBtn;
    int i;
    if ((!this.mResourceHandler.isDeletable()) || (this.mResourceHandler.isDownloading()))
      i = 4;
    else
      i = 0;
    localImageView.setVisibility(i);
  }

  private void setDownloadStatus()
  {
    boolean bool = true;
    Object localObject = null;
    State localState1 = State.NONE;
    State localState2;
    if (!this.mResourceHandler.isDownloading())
    {
      if (!this.mResourceHandler.isOldResource())
      {
        if (!this.mResourceHandler.isLocalResource())
          if (!this.mResourceHandler.isAuthorizedResource())
          {
            int i = this.mResourceHandler.getPrice();
            if (i != 0)
              localObject = this.mContext.getString(101450288);
            else
              localObject = this.mContext.getString(101449751);
            if (i > 0)
            {
              localObject = new StringBuilder().append((String)localObject);
              Context localContext = this.mContext;
              Object[] arrayOfObject = new Object[1];
              arrayOfObject[0] = formatPrice(i);
              localObject = localContext.getString(101450297, arrayOfObject);
            }
            localState2 = State.BUY;
          }
          else
          {
            localObject = this.mContext.getString(101449751);
            localState2 = State.DOWNLOAD;
          }
      }
      else
      {
        localObject = this.mContext.getString(101449754);
        localState2 = State.UPDATE;
      }
    }
    else
    {
      localObject = this.mContext.getString(101449752);
      bool = false;
      localState2 = State.DOWNLOADING;
    }
    if (localObject != null)
    {
      this.mDownloadBtn.setVisibility(0);
      this.mDownloadBtn.setEnabled(bool);
      this.mDownloadBtn.setText((CharSequence)localObject);
      this.mDownloadBtn.setTag(localState2);
    }
    else
    {
      this.mDownloadBtn.setVisibility(8);
    }
  }

  private boolean setLoadingStatus()
  {
    ResourceOperationHandler.LoadingStateInfo localLoadingStateInfo = this.mResourceHandler.getLoadingStateInfo();
    int i;
    if (localLoadingStateInfo.delayTime != 0)
    {
      updateLoadingUI(false, null);
      if ((localLoadingStateInfo.delayTime > 0) && (!this.mHandler.hasMessages(0)))
        this.mHandler.sendEmptyMessageDelayed(0, localLoadingStateInfo.delayTime);
      i = 0;
    }
    else
    {
      updateLoadingUI(true, localLoadingStateInfo.title);
      i = 1;
    }
    if (localLoadingStateInfo.delayTime <= 0)
      this.mHandler.removeMessages(0);
    return i;
  }

  private void setMagicStatus()
  {
    ImageView localImageView = this.mMagicBtn;
    int i;
    if (this.mMagicBtnResId == 0)
      i = 4;
    else
      i = 0;
    localImageView.setVisibility(i);
  }

  private void setupUI()
  {
    this.mLoadingProgress = findViewById(101384351);
    this.mControlBtns = findViewById(101384349);
    this.mLoadingTextView = ((TextView)findViewById(101384350));
    this.mDownloadBtn = ((TextView)findViewById(101384282));
    this.mDownloadBtn.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramView)
      {
        ResourceOperationView.State localState = (ResourceOperationView.State)paramView.getTag();
        if (localState != ResourceOperationView.State.DOWNLOAD)
        {
          if (localState != ResourceOperationView.State.BUY)
          {
            if (localState == ResourceOperationView.State.UPDATE)
              ResourceOperationView.this.notifyUpdateEventPerformed();
          }
          else
            ResourceOperationView.this.notifyBuyEventPerformed();
        }
        else
          ResourceOperationView.this.notifyDownloadEventPerformed();
      }
    });
    this.mApplyBtn = ((TextView)findViewById(101384283));
    this.mApplyBtn.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramView)
      {
        ResourceOperationView.this.disableAndDelayEnableView(paramView);
        ResourceOperationView.State localState = (ResourceOperationView.State)paramView.getTag();
        if (localState != ResourceOperationView.State.PICK)
        {
          if (localState == ResourceOperationView.State.APPLY)
            ResourceOperationView.this.notifyApplyEventPerformed();
        }
        else
          ResourceOperationView.this.notifyPickEventPerformed();
      }
    });
    this.mDeleteBtn = ((ImageView)findViewById(101384284));
    this.mDeleteBtn.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramView)
      {
        new AlertDialog.Builder(ResourceOperationView.this.mContext).setTitle(101450102).setIconAttribute(16843605).setMessage(101449766).setNegativeButton(17039360, null).setPositiveButton(17039370, new DialogInterface.OnClickListener()
        {
          public void onClick(DialogInterface paramDialogInterface, int paramInt)
          {
            ResourceOperationView.this.notifyDeleteEventPerformed();
          }
        }).show();
      }
    });
    this.mMagicBtn = ((ImageView)findViewById(101384285));
    this.mMagicBtn.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramView)
      {
        ResourceOperationView.this.disableAndDelayEnableView(paramView);
        ResourceOperationView.this.notifyMagicEventPerformed();
      }
    });
    this.mDownloadProgress = ((ProgressBar)findViewById(101384269));
    this.mDownloadProgress.setMax(100);
    this.mDownloadProgress.setVisibility(8);
  }

  private void updateLoadingUI(boolean paramBoolean, String paramString)
  {
    int i = 8;
    View localView2 = this.mLoadingProgress;
    int j;
    if (!paramBoolean)
      j = i;
    else
      j = 0;
    localView2.setVisibility(j);
    View localView1 = this.mControlBtns;
    if (!paramBoolean)
      i = 0;
    localView1.setVisibility(i);
    this.mLoadingTextView.setText(paramString);
  }

  protected Handler getMsgHandler()
  {
    return new Handler()
    {
      public void handleMessage(Message paramMessage)
      {
        if (paramMessage.what == 0)
        {
          ResourceOperationHandler.LoadingStateInfo localLoadingStateInfo = ResourceOperationView.this.mResourceHandler.getLoadingStateInfo();
          if (localLoadingStateInfo.delayTime > 0)
            ResourceOperationView.this.updateLoadingUI(true, localLoadingStateInfo.title);
        }
      }
    };
  }

  protected void onFinishInflate()
  {
    super.onFinishInflate();
    this.mHandler = getMsgHandler();
    setupUI();
  }

  public void setMagicButtonResource(int paramInt)
  {
    this.mMagicBtnResId = paramInt;
    this.mMagicBtn.setImageResource(paramInt);
  }

  public void setResourceOperationHandler(ResourceOperationHandler paramResourceOperationHandler)
  {
    this.mResourceHandler = paramResourceOperationHandler;
  }

  public void setResourceOperationListener(ResourceOperationListener paramResourceOperationListener)
  {
    this.mListener = paramResourceOperationListener;
  }

  public void updateDownloadProgressBar(int paramInt1, int paramInt2)
  {
    this.mDownloadProgress.setProgress((int)(100.0D * paramInt1 / paramInt2));
  }

  public void updateStatus()
  {
    if (!setLoadingStatus())
    {
      setApplyStatus();
      setDownloadStatus();
      setDeleteStatus();
      setMagicStatus();
    }
  }

  public static abstract interface ResourceOperationListener
  {
    public abstract void onApplyEventPerformed();

    public abstract void onBuyEventPerformed();

    public abstract void onDeleteEventPerformed();

    public abstract void onDownloadEventPerformed();

    public abstract void onMagicEventPerformed();

    public abstract void onPickEventPerformed();

    public abstract void onUpdateEventPerformed();
  }

  private static enum State
  {
    static
    {
      BUY = new State("BUY", 2);
      DOWNLOAD = new State("DOWNLOAD", 3);
      UPDATE = new State("UPDATE", 4);
      LOADING = new State("LOADING", 5);
      DOWNLOADING = new State("DOWNLOADING", 6);
      NONE = new State("NONE", 7);
      State[] arrayOfState = new State[8];
      arrayOfState[0] = APPLY;
      arrayOfState[1] = PICK;
      arrayOfState[2] = BUY;
      arrayOfState[3] = DOWNLOAD;
      arrayOfState[4] = UPDATE;
      arrayOfState[5] = LOADING;
      arrayOfState[6] = DOWNLOADING;
      arrayOfState[7] = NONE;
      $VALUES = arrayOfState;
    }
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.view.ResourceOperationView
 * JD-Core Version:    0.6.0
 */