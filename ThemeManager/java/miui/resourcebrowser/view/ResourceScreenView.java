package miui.resourcebrowser.view;

import android.content.Context;
import android.util.AttributeSet;
import miui.widget.ScreenView;

public class ResourceScreenView extends ScreenView
{
  private ScreenChangeListener mScreenChangeListener;

  public ResourceScreenView(Context paramContext)
  {
    super(paramContext);
  }

  public ResourceScreenView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  public ResourceScreenView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }

  public void setScreenChangeListener(ScreenChangeListener paramScreenChangeListener)
  {
    this.mScreenChangeListener = paramScreenChangeListener;
  }

  protected void snapToScreen(int paramInt1, int paramInt2, boolean paramBoolean)
  {
    if (this.mScreenChangeListener != null)
      this.mScreenChangeListener.snapToScreen(paramInt1);
    super.snapToScreen(paramInt1, paramInt2, paramBoolean);
  }

  public static abstract interface ScreenChangeListener
  {
    public abstract void snapToScreen(int paramInt);
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.view.ResourceScreenView
 * JD-Core Version:    0.6.0
 */