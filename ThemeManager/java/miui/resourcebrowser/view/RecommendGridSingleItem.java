package miui.resourcebrowser.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.View;
import android.view.View.OnClickListener;
import java.util.ArrayList;
import java.util.List;
import java.util.List<Lmiui.resourcebrowser.widget.DataGroup<Lmiui.resourcebrowser.model.Resource;>;>;
import miui.resourcebrowser.AppInnerContext;
import miui.resourcebrowser.ResourceContext;
import miui.resourcebrowser.controller.LocalDataManager;
import miui.resourcebrowser.controller.ResourceController;
import miui.resourcebrowser.controller.online.OnlineService;
import miui.resourcebrowser.controller.online.RequestUrl;
import miui.resourcebrowser.model.RecommendItemData;
import miui.resourcebrowser.model.Resource;
import miui.resourcebrowser.widget.DataGroup;

public class RecommendGridSingleItem extends RecommendGridBaseItem
{
  public RecommendGridSingleItem(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  protected List<DataGroup<Resource>> buildResourceSet(ResourceContext paramResourceContext)
  {
    Resource localResource1 = new Resource();
    Object localObject = this.mRecommendItemData.itemId;
    Resource localResource2 = AppInnerContext.getInstance().getResourceController().getLocalDataManager().getResourceByOnlineId((String)localObject);
    if (localResource2 != null)
      localResource1.updateFrom(localResource2);
    localResource1.setOnlineId((String)localObject);
    localResource1.setOnlinePath(new OnlineService(paramResourceContext).getDownloadUrl(this.mRecommendItemData.itemId).getUrl());
    localResource1.setTitle(this.mRecommendItemData.title);
    int i;
    if (localResource2 != null)
      i = 3;
    else
      i = 2;
    localResource1.setStatus(i);
    ArrayList localArrayList = new ArrayList();
    localObject = new DataGroup();
    ((DataGroup)localObject).add(localResource1);
    localArrayList.add(localObject);
    return (List<DataGroup<Resource>>)localArrayList;
  }

  protected View.OnClickListener getOnClickListener(ResourceContext paramResourceContext)
  {
    return new View.OnClickListener(paramResourceContext)
    {
      public void onClick(View paramView)
      {
        Intent localIntent = new Intent();
        Object localObject = RecommendGridSingleItem.this.getResourceDetailActivity(this.val$resContext);
        localIntent.setClassName((String)((Pair)localObject).first, (String)((Pair)localObject).second);
        localIntent.addFlags(67108864);
        localIntent.putExtra("REQUEST_RES_GROUP", 0);
        localIntent.putExtra("REQUEST_RES_INDEX", 0);
        localIntent.putExtra("REQUEST_SOURCE_TYPE", 2);
        localIntent.putExtra("REQUEST_RECOMMEND_ID", RecommendGridSingleItem.this.mRecommendItemData.itemId);
        localObject = AppInnerContext.getInstance();
        ((AppInnerContext)localObject).setWorkingDataSet(RecommendGridSingleItem.this.buildResourceSet(this.val$resContext));
        ((AppInnerContext)localObject).setResourceContext(this.val$resContext);
        ((AppInnerContext)localObject).setResourceController(null);
        ((Activity)RecommendGridSingleItem.this.mContext).startActivityForResult(localIntent, 1);
      }
    };
  }

  protected Pair<String, String> getResourceDetailActivity(ResourceContext paramResourceContext)
  {
    return new Pair(paramResourceContext.getDetailActivityPackage(), paramResourceContext.getDetailActivityClass());
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.view.RecommendGridSingleItem
 * JD-Core Version:    0.6.0
 */