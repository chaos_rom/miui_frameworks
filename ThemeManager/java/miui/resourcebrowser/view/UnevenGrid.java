package miui.resourcebrowser.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;

public class UnevenGrid extends ViewGroup
{
  private int mColumnCount;
  private TreeSet<Integer> mEmptyGrids = new TreeSet();
  private int mEmptyRow = 0;
  private int mGridHeight;
  private GridItemFactory mGridItemFactory;
  private int mGridItemGap;
  private HashMap<View, Integer> mGridItemPositions = new HashMap();
  private ArrayList<View> mGridItems = new ArrayList();
  private int mGridMaxHeight;
  private int mGridMaxWidth;
  private int mGridWidth;
  private int mHeightRatio;
  private int mWidthRatio;

  public UnevenGrid(Context paramContext)
  {
    super(paramContext);
    initialize();
  }

  private void addGridItem(View paramView, int paramInt1, int paramInt2)
  {
    if (paramView != null)
    {
      if (paramInt1 <= this.mGridMaxWidth)
      {
        if (paramInt1 < 1)
          paramInt1 = 1;
      }
      else
        paramInt1 = this.mGridMaxWidth;
      if (paramInt2 <= this.mGridMaxHeight)
      {
        if (paramInt2 < 1)
          paramInt2 = 1;
      }
      else
        paramInt2 = this.mGridMaxHeight;
      paramView.setLayoutParams(LayoutParams.create(paramInt1, paramInt2));
      this.mGridItems.add(paramView);
    }
  }

  private int computeAndPlace(int paramInt1, int paramInt2)
  {
    Iterator localIterator = this.mEmptyGrids.iterator();
    int i;
    while (true)
      if (!localIterator.hasNext())
      {
        i = this.mEmptyRow * this.mColumnCount;
        place(i, paramInt1, paramInt2);
      }
      else
      {
        Integer localInteger = (Integer)i.next();
        if (!isPlaceable(localInteger.intValue(), paramInt1, paramInt2))
          continue;
        place(localInteger.intValue(), paramInt1, paramInt2);
        i = localInteger.intValue();
      }
    return i;
  }

  private int getChildBottom(int paramInt1, int paramInt2)
  {
    return paramInt2 + getChildTop(paramInt1);
  }

  private int getChildLeft(int paramInt)
  {
    return paramInt % this.mColumnCount * (this.mGridWidth + this.mGridItemGap);
  }

  private int getChildRight(int paramInt1, int paramInt2)
  {
    return paramInt2 + getChildLeft(paramInt1);
  }

  private int getChildTop(int paramInt)
  {
    return paramInt / this.mColumnCount * (this.mGridHeight + this.mGridItemGap);
  }

  private void initialize()
  {
    this.mColumnCount = 2;
    this.mGridItemGap = 0;
    this.mGridMaxWidth = 2;
    this.mGridMaxHeight = 2;
    this.mWidthRatio = 5;
    this.mHeightRatio = 3;
  }

  private boolean isPlaceable(int paramInt1, int paramInt2, int paramInt3)
  {
    int k = 0;
    if (this.mColumnCount - paramInt1 % this.mColumnCount >= paramInt2)
    {
      int i = 0;
      if (i >= paramInt3)
      {
        k = 1;
      }
      else
      {
        for (int j = 0; ; j++)
        {
          if (j >= paramInt2)
          {
            i++;
            break;
          }
          int m = j + (paramInt1 + i * this.mColumnCount);
          if (m / this.mColumnCount >= this.mEmptyRow)
            break label97;
          if (!this.mEmptyGrids.contains(Integer.valueOf(m)))
            break label100;
        }
        label97: k = 1;
      }
    }
    label100: return k;
  }

  private void layoutItems()
  {
    removeAllViews();
    this.mGridItemPositions.clear();
    this.mEmptyGrids.clear();
    this.mEmptyRow = 0;
    Iterator localIterator = this.mGridItems.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      View localView = (View)localIterator.next();
      LayoutParams localLayoutParams = (LayoutParams)localView.getLayoutParams();
      int i = computeAndPlace(localLayoutParams.widthCount, localLayoutParams.heightCount);
      this.mGridItemPositions.put(localView, Integer.valueOf(i));
      addView(localView);
    }
  }

  private void place(int paramInt1, int paramInt2, int paramInt3)
  {
    int i = -1 + (paramInt3 + paramInt1 / this.mColumnCount);
    int j = this.mEmptyRow;
    int k;
    if (j > i)
    {
      if (this.mEmptyRow <= i)
        this.mEmptyRow = (i + 1);
      k = 0;
      if (k >= paramInt3)
        return;
      for (i = 0; ; i++)
      {
        if (i >= paramInt2)
        {
          k++;
          break;
        }
        j = i + (paramInt1 + k * this.mColumnCount);
        this.mEmptyGrids.remove(Integer.valueOf(j));
      }
    }
    for (int m = 0; ; m++)
    {
      if (m >= this.mColumnCount)
      {
        j++;
        break;
      }
      k = m + j * this.mColumnCount;
      this.mEmptyGrids.add(Integer.valueOf(k));
    }
  }

  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    for (int i = 0; ; i++)
    {
      if (i >= getChildCount())
        return;
      View localView = getChildAt(i);
      int m = localView.getMeasuredWidth();
      int j = localView.getMeasuredHeight();
      int k = ((Integer)this.mGridItemPositions.get(localView)).intValue();
      localView.layout(this.mPaddingLeft + getChildLeft(k), this.mPaddingTop + getChildTop(k), this.mPaddingLeft + getChildRight(k, m), this.mPaddingTop + getChildBottom(k, j));
    }
  }

  protected void onMeasure(int paramInt1, int paramInt2)
  {
    int k = View.MeasureSpec.getSize(paramInt1);
    this.mGridWidth = ((k - this.mPaddingLeft - this.mPaddingRight - (-1 + this.mColumnCount) * this.mGridItemGap) / this.mColumnCount);
    this.mGridHeight = (this.mGridWidth * this.mHeightRatio / this.mWidthRatio);
    int i = 0;
    for (int j = 0; ; j++)
    {
      if (j >= getChildCount())
      {
        setMeasuredDimension(k, i + this.mPaddingTop + this.mPaddingBottom);
        return;
      }
      View localView = getChildAt(j);
      LayoutParams localLayoutParams = (LayoutParams)localView.getLayoutParams();
      int m = localLayoutParams.widthCount;
      int n = localLayoutParams.heightCount;
      m = m * this.mGridWidth + (m - 1) * this.mGridItemGap;
      n = n * this.mGridHeight + (n - 1) * this.mGridItemGap;
      int i1 = ((Integer)this.mGridItemPositions.get(localView)).intValue();
      localView.measure(View.MeasureSpec.makeMeasureSpec(m, 1073741824), View.MeasureSpec.makeMeasureSpec(n, 1073741824));
      i = Math.max(i, getChildBottom(i1, n));
    }
  }

  public void setColumnCount(int paramInt)
  {
    if (paramInt > 0)
      this.mColumnCount = paramInt;
  }

  public void setGridItemFactory(GridItemFactory paramGridItemFactory)
  {
    this.mGridItemFactory = paramGridItemFactory;
  }

  public void setGridItemGap(int paramInt)
  {
    if (paramInt >= 0)
      this.mGridItemGap = paramInt;
  }

  public void setGridItemRatio(int paramInt1, int paramInt2)
  {
    if ((paramInt1 > 0) && (paramInt2 > 0))
    {
      this.mWidthRatio = paramInt1;
      this.mHeightRatio = paramInt2;
    }
  }

  public void updateData(List<? extends GridItemData> paramList)
  {
    Iterator localIterator;
    if ((paramList != null) && (!paramList.isEmpty()) && (this.mGridItemFactory != null))
    {
      this.mGridItems.clear();
      localIterator = paramList.iterator();
    }
    while (true)
    {
      if (!localIterator.hasNext())
      {
        layoutItems();
        return;
      }
      GridItemData localGridItemData = (GridItemData)localIterator.next();
      View localView = this.mGridItemFactory.createGridItem(localGridItemData);
      if (localView == null)
        continue;
      addGridItem(localView, localGridItemData.widthCount, localGridItemData.heightCount);
    }
  }

  public static abstract interface GridItemFactory
  {
    public abstract View createGridItem(UnevenGrid.GridItemData paramGridItemData);
  }

  public static class GridItemData
  {
    public int heightCount;
    public int widthCount;
  }

  public static class LayoutParams extends ViewGroup.MarginLayoutParams
  {
    int heightCount;
    int widthCount;

    public LayoutParams()
    {
      super(-1);
    }

    public LayoutParams(Context paramContext, AttributeSet paramAttributeSet)
    {
      super(paramAttributeSet);
    }

    public static LayoutParams create(int paramInt1, int paramInt2)
    {
      LayoutParams localLayoutParams = new LayoutParams();
      localLayoutParams.widthCount = paramInt1;
      localLayoutParams.heightCount = paramInt2;
      return localLayoutParams;
    }
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.view.UnevenGrid
 * JD-Core Version:    0.6.0
 */