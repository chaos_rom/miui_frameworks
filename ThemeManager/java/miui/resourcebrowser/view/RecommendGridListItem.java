package miui.resourcebrowser.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.View;
import android.view.View.OnClickListener;
import java.io.Serializable;
import miui.resourcebrowser.ResourceContext;
import miui.resourcebrowser.model.RecommendItemData;

public class RecommendGridListItem extends RecommendGridBaseItem
{
  public RecommendGridListItem(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  protected View.OnClickListener getOnClickListener(ResourceContext paramResourceContext)
  {
    return new View.OnClickListener(paramResourceContext)
    {
      public void onClick(View paramView)
      {
        Intent localIntent = new Intent();
        Pair localPair = RecommendGridListItem.this.getRecommendResourceListActivity(this.val$resContext);
        localIntent.setClassName((String)localPair.first, (String)localPair.second);
        localIntent.addFlags(67108864);
        localIntent.putExtra("REQUEST_RES_CONTEXT", this.val$resContext);
        localIntent.putExtra("REQUEST_RECOMMEND_ID", RecommendGridListItem.this.mRecommendItemData.itemId);
        localIntent.putExtra("REQUEST_IS_RECOMMEND_LIST", true);
        localIntent.putExtra("REQUEST_SUB_RECOMMENDS", (Serializable)RecommendGridListItem.this.mRecommendItemData.subItems);
        ((Activity)RecommendGridListItem.this.mContext).startActivityForResult(localIntent, 1);
      }
    };
  }

  protected Pair<String, String> getRecommendResourceListActivity(ResourceContext paramResourceContext)
  {
    return new Pair(paramResourceContext.getRecommendActivityPackage(), paramResourceContext.getRecommendActivityClass());
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.view.RecommendGridListItem
 * JD-Core Version:    0.6.0
 */