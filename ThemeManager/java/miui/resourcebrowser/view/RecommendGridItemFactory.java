package miui.resourcebrowser.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import java.util.HashMap;
import java.util.Map;
import miui.resourcebrowser.ResourceContext;
import miui.resourcebrowser.model.RecommendItemData;

public class RecommendGridItemFactory
  implements UnevenGrid.GridItemFactory
{
  private static Map<Integer, Integer> sGridTypeLayoutMap = new HashMap();
  private Context mContext;
  private ResourceContext mResContext;

  static
  {
    sGridTypeLayoutMap.put(Integer.valueOf(2), Integer.valueOf(100859906));
    sGridTypeLayoutMap.put(Integer.valueOf(1), Integer.valueOf(100859905));
  }

  public RecommendGridItemFactory(Context paramContext, ResourceContext paramResourceContext)
  {
    this.mContext = paramContext;
    this.mResContext = paramResourceContext;
  }

  public View createGridItem(UnevenGrid.GridItemData paramGridItemData)
  {
    RecommendGridBaseItem localRecommendGridBaseItem = null;
    if (paramGridItemData != null)
    {
      RecommendItemData localRecommendItemData = (RecommendItemData)paramGridItemData;
      int i = localRecommendItemData.itemType;
      if (i != 0)
      {
        localRecommendGridBaseItem = (RecommendGridBaseItem)LayoutInflater.from(this.mContext).inflate(((Integer)sGridTypeLayoutMap.get(Integer.valueOf(i))).intValue(), null, false);
        localRecommendGridBaseItem.bind(localRecommendItemData, this.mResContext);
      }
    }
    return localRecommendGridBaseItem;
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.view.RecommendGridItemFactory
 * JD-Core Version:    0.6.0
 */