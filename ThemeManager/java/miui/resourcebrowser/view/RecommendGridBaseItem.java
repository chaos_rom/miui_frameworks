package miui.resourcebrowser.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import java.io.File;
import java.util.HashMap;
import miui.resourcebrowser.ResourceContext;
import miui.resourcebrowser.controller.online.DownloadFileTask;
import miui.resourcebrowser.model.PathEntry;
import miui.resourcebrowser.model.RecommendItemData;
import miui.util.ImageUtils;
import miui.util.InputStreamLoader;

public abstract class RecommendGridBaseItem extends FrameLayout
{
  private static HashMap<PathEntry, Long> sDownloadingCache = new HashMap();
  private ImageView mImageView;
  protected RecommendItemData mRecommendItemData;

  public RecommendGridBaseItem(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  public void bind(RecommendItemData paramRecommendItemData, ResourceContext paramResourceContext)
  {
    this.mRecommendItemData = paramRecommendItemData;
    this.mImageView.setOnClickListener(getOnClickListener(paramResourceContext));
    if (!new File(paramRecommendItemData.localThumbnail).exists())
    {
      if (!TextUtils.isEmpty(paramRecommendItemData.onlineThumbnail))
        downloadThumbnail(new PathEntry(paramRecommendItemData.localThumbnail, paramRecommendItemData.onlineThumbnail));
    }
    else
    {
      Bitmap localBitmap = ImageUtils.getBitmap(new InputStreamLoader(paramRecommendItemData.localThumbnail), -1);
      this.mImageView.setImageBitmap(localBitmap);
    }
  }

  protected void downloadThumbnail(PathEntry paramPathEntry)
  {
    Object localObject = (Long)sDownloadingCache.get(paramPathEntry);
    if ((localObject == null) || (System.currentTimeMillis() - ((Long)localObject).longValue() >= 30000L))
    {
      sDownloadingCache.put(paramPathEntry, Long.valueOf(System.currentTimeMillis()));
      localObject = new AsyncTask()
      {
        protected Bitmap doInBackground(PathEntry[] paramArrayOfPathEntry)
        {
          PathEntry localPathEntry = paramArrayOfPathEntry[0];
          if (!new File(localPathEntry.getLocalPath()).exists())
          {
            DownloadFileTask localDownloadFileTask = new DownloadFileTask(localPathEntry.getOnlinePath());
            PathEntry[] arrayOfPathEntry = new PathEntry[1];
            arrayOfPathEntry[0] = localPathEntry;
            localDownloadFileTask.downloadFiles(arrayOfPathEntry);
          }
          return ImageUtils.getBitmap(new InputStreamLoader(localPathEntry.getLocalPath()), -1);
        }

        protected void onPostExecute(Bitmap paramBitmap)
        {
          if (paramBitmap != null)
            RecommendGridBaseItem.this.mImageView.setImageBitmap(paramBitmap);
        }
      };
      PathEntry[] arrayOfPathEntry = new PathEntry[1];
      arrayOfPathEntry[0] = paramPathEntry;
      ((1)localObject).execute(arrayOfPathEntry);
    }
  }

  protected abstract View.OnClickListener getOnClickListener(ResourceContext paramResourceContext);

  protected void onFinishInflate()
  {
    this.mImageView = ((ImageView)findViewById(101384298));
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.view.RecommendGridBaseItem
 * JD-Core Version:    0.6.0
 */