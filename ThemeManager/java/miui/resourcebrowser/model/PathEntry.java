package miui.resourcebrowser.model;

import java.io.Serializable;

public class PathEntry
  implements Serializable
{
  public static final String LOCAL_PATH = "localPath";
  public static final String ONLINE_PATH = "onlinePath";
  private static final long serialVersionUID = 1L;
  private String localPath;
  private String onlinePath;

  public PathEntry()
  {
  }

  public PathEntry(String paramString1, String paramString2)
  {
    this.localPath = paramString1;
    this.onlinePath = paramString2;
  }

  private boolean equals(Object paramObject1, Object paramObject2)
  {
    int i;
    if ((paramObject1 != paramObject2) && ((paramObject1 == null) || (paramObject2 == null) || (!paramObject1.equals(paramObject2))))
      i = 0;
    else
      i = 1;
    return i;
  }

  public boolean equals(Object paramObject)
  {
    int i = 1;
    if (this != paramObject)
      if (paramObject != null)
      {
        if (getClass() == paramObject.getClass())
        {
          PathEntry localPathEntry = (PathEntry)paramObject;
          if (equals(this.localPath, localPathEntry.localPath))
          {
            if (!equals(this.onlinePath, localPathEntry.onlinePath))
              i = 0;
          }
          else
            i = 0;
        }
        else
        {
          i = 0;
        }
      }
      else
        i = 0;
    return i;
  }

  public String getLocalPath()
  {
    return this.localPath;
  }

  public String getOnlinePath()
  {
    return this.onlinePath;
  }

  public int hashCode()
  {
    int i = 0;
    if (this.localPath != null)
      j = this.localPath.hashCode();
    else
      j = 0;
    int j = 31 * (j + 31);
    if (this.onlinePath != null)
      i = this.onlinePath.hashCode();
    return j + i;
  }

  public void setLocalPath(String paramString)
  {
    this.localPath = paramString;
  }

  public void setOnlinePath(String paramString)
  {
    this.onlinePath = paramString;
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.model.PathEntry
 * JD-Core Version:    0.6.0
 */