package miui.resourcebrowser.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class RelatedResource
  implements Serializable
{
  public static final String EXTRA_META = "extraMeta";
  public static final String FILE_PATH = "filePath";
  public static final String LOCAL_PATH = "localPath";
  public static final String RESOURCE_CODE = "resourceCode";
  private static final long serialVersionUID = 1L;
  private Map<String, String> extraMeta = new HashMap();
  private String filePath;
  private String localPath;
  private String resourceCode;

  private boolean equals(Object paramObject1, Object paramObject2)
  {
    int i;
    if ((paramObject1 != paramObject2) && ((paramObject1 == null) || (paramObject2 == null) || (!paramObject1.equals(paramObject2))))
      i = 0;
    else
      i = 1;
    return i;
  }

  public boolean equals(Object paramObject)
  {
    int i = 1;
    if (this != paramObject)
      if (paramObject != null)
      {
        if (getClass() == paramObject.getClass())
        {
          RelatedResource localRelatedResource = (RelatedResource)paramObject;
          if (equals(this.localPath, localRelatedResource.localPath))
          {
            if (equals(this.filePath, localRelatedResource.filePath))
            {
              if (equals(this.resourceCode, localRelatedResource.resourceCode))
              {
                if (!equals(this.extraMeta, localRelatedResource.extraMeta))
                  i = 0;
              }
              else
                i = 0;
            }
            else
              i = 0;
          }
          else
            i = 0;
        }
        else
        {
          i = 0;
        }
      }
      else
        i = 0;
    return i;
  }

  public String getExtraMeta(String paramString)
  {
    return (String)this.extraMeta.get(paramString);
  }

  public String getExtraMeta(String paramString1, String paramString2)
  {
    String str = (String)this.extraMeta.get(paramString1);
    if (str == null)
      str = paramString2;
    return str;
  }

  public Map<String, String> getExtraMeta()
  {
    return this.extraMeta;
  }

  public String getFilePath()
  {
    return this.filePath;
  }

  public String getLocalPath()
  {
    return this.localPath;
  }

  public String getResourceCode()
  {
    return this.resourceCode;
  }

  public int hashCode()
  {
    int i = 0;
    if (this.extraMeta != null)
      j = this.extraMeta.hashCode();
    else
      j = 0;
    int k = 31 * (j + 31);
    if (this.filePath != null)
      j = this.filePath.hashCode();
    else
      j = 0;
    k = 31 * (k + j);
    if (this.localPath != null)
      j = this.localPath.hashCode();
    else
      j = 0;
    int j = 31 * (k + j);
    if (this.resourceCode != null)
      i = this.resourceCode.hashCode();
    return j + i;
  }

  public void putExtraMeta(String paramString1, String paramString2)
  {
    this.extraMeta.put(paramString1, paramString2);
  }

  public void setExtraMeta(Map<String, String> paramMap)
  {
    this.extraMeta = paramMap;
  }

  public void setFilePath(String paramString)
  {
    this.filePath = paramString;
  }

  public void setLocalPath(String paramString)
  {
    this.localPath = paramString;
  }

  public void setResourceCode(String paramString)
  {
    this.resourceCode = paramString;
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.model.RelatedResource
 * JD-Core Version:    0.6.0
 */