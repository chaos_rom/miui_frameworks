package miui.resourcebrowser.model;

import java.io.Serializable;

public class ResourceCategory
  implements Serializable, Comparable<ResourceCategory>
{
  private static final long serialVersionUID = 1L;
  private String code;
  private String name;
  private long type;

  public int compareTo(ResourceCategory paramResourceCategory)
  {
    return this.code.compareTo(paramResourceCategory.getCode());
  }

  public String getCode()
  {
    return this.code;
  }

  public String getName()
  {
    return this.name;
  }

  public long getType()
  {
    return this.type;
  }

  public void setCode(String paramString)
  {
    this.code = paramString;
  }

  public void setName(String paramString)
  {
    this.name = paramString;
  }

  public void setType(long paramLong)
  {
    this.type = paramLong;
  }

  public String toString()
  {
    return this.name;
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.model.ResourceCategory
 * JD-Core Version:    0.6.0
 */