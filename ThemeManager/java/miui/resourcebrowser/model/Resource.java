package miui.resourcebrowser.model;

import android.text.TextUtils;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import miui.resourcebrowser.ResourceConstants;

public class Resource
  implements Serializable
{
  public static final String AUTHOR = "author";
  public static final String BUILD_IN_PREVIEWS = "buildInPreviews";
  public static final String BUILD_IN_THUMBNAILS = "buildInThumbnails";
  public static final String DESCRIPTION = "description";
  public static final String DESIGNER = "designer";
  public static final String DOWNLOAD_COUNT = "downloadCount";
  public static final String DOWNLOAD_PATH = "downloadPath";
  public static final String EXTRA_META = "extraMeta";
  public static final String FILE_PATH = "filePath";
  public static final String HASH = "hash";
  public static final String LOCAL_ID = "localId";
  public static final String LOCAL_PATH = "localPath";
  public static final String ONLINE_ID = "onlineId";
  public static final String ONLINE_PATH = "onlinePath";
  public static final String PARENT_RESOURCES = "parentResources";
  public static final String PLATFORM = "platform";
  public static final String PREVIEWS = "previews";
  public static final String PRODUCT_ID = "productId";
  public static final String RIGHTS_PATH = "rightsPath";
  public static final String SIZE = "size";
  public static final String STATUS = "status";
  public static final int STATUS_LOCAL = 1;
  public static final int STATUS_OLD = 4;
  public static final int STATUS_ONLINE = 2;
  public static final String SUB_RESOURCES = "subResources";
  public static final String THUMBNAILS = "thumbnails";
  public static final String TITLE = "title";
  public static final String UPDATED_TIME = "updatedTime";
  public static final String VERSION = "version";
  private static final long serialVersionUID = 1L;
  private String author;
  private List<String> buildInPreviews = new ArrayList();
  private List<String> buildInThumbnails = new ArrayList();
  private String description;
  private String designer;
  private String downloadCount;
  private String downloadPath;
  private Map<String, String> extraMeta = new HashMap();
  private String filePath;
  private String hash;
  private String localId;
  private String localPath;
  private String onlineId;
  private String onlinePath;
  private List<RelatedResource> parentResources = new ArrayList();
  private int platform;
  private List<PathEntry> previews = new ArrayList();
  private boolean productBought;
  private String productId;
  private int productPrice = -1;
  private String rightsPath;
  private long size;
  private int status;
  private List<RelatedResource> subResources = new ArrayList();
  private List<PathEntry> thumbnails = new ArrayList();
  private String title;
  private long updatedTime;
  private String version;

  private boolean equals(Object paramObject1, Object paramObject2)
  {
    int i;
    if ((paramObject1 != paramObject2) && ((paramObject1 == null) || (paramObject2 == null) || (!paramObject1.equals(paramObject2))))
      i = 0;
    else
      i = 1;
    return i;
  }

  private String getFakeRightsPath()
  {
    return ResourceConstants.RIGHTS_ROOT_PATH + "theme/.db/rights/theme/" + this.hash + ".mra";
  }

  public void addBuildInPreview(String paramString)
  {
    this.buildInPreviews.add(paramString);
  }

  public void addBuildInThumbnail(String paramString)
  {
    this.buildInThumbnails.add(paramString);
  }

  public void addParentResources(RelatedResource paramRelatedResource)
  {
    this.parentResources.add(paramRelatedResource);
  }

  public void addPreview(PathEntry paramPathEntry)
  {
    this.previews.add(paramPathEntry);
  }

  public void addSubResources(RelatedResource paramRelatedResource)
  {
    this.subResources.add(paramRelatedResource);
  }

  public void addThumbnail(PathEntry paramPathEntry)
  {
    this.thumbnails.add(paramPathEntry);
  }

  public void clearBuildInPreviews()
  {
    this.buildInPreviews.clear();
  }

  public void clearBuildInThumbnails()
  {
    this.buildInThumbnails.clear();
  }

  public void clearExtraMeta()
  {
    this.extraMeta.clear();
  }

  public void clearParentResources()
  {
    this.parentResources.clear();
  }

  public void clearPreviews()
  {
    this.previews.clear();
  }

  public void clearSubResources()
  {
    this.subResources.clear();
  }

  public void clearThumbnails()
  {
    this.thumbnails.clear();
  }

  public boolean equals(Object paramObject)
  {
    int i = 1;
    if (this != paramObject)
      if (paramObject != null)
      {
        if (getClass() == paramObject.getClass())
        {
          Resource localResource = (Resource)paramObject;
          if (equals(this.localId, localResource.localId))
          {
            if (equals(this.onlineId, localResource.onlineId))
            {
              if (equals(this.productId, localResource.productId))
              {
                if (equals(this.localPath, localResource.localPath))
                {
                  if (equals(this.onlinePath, localResource.onlinePath))
                  {
                    if (equals(this.filePath, localResource.filePath))
                    {
                      if (equals(this.downloadPath, localResource.downloadPath))
                      {
                        if (equals(this.rightsPath, localResource.rightsPath))
                        {
                          if (equals(Integer.valueOf(this.platform), Integer.valueOf(localResource.platform)))
                          {
                            if (equals(Integer.valueOf(this.status), Integer.valueOf(localResource.status)))
                            {
                              if (equals(this.hash, localResource.hash))
                              {
                                if (equals(Long.valueOf(this.size), Long.valueOf(localResource.size)))
                                {
                                  if (equals(Long.valueOf(this.updatedTime), Long.valueOf(localResource.updatedTime)))
                                  {
                                    if (equals(this.title, localResource.title))
                                    {
                                      if (equals(this.description, localResource.description))
                                      {
                                        if (equals(this.author, localResource.author))
                                        {
                                          if (equals(this.designer, localResource.designer))
                                          {
                                            if (equals(this.version, localResource.version))
                                            {
                                              if (equals(this.downloadCount, localResource.downloadCount))
                                              {
                                                if (equals(this.buildInThumbnails, localResource.buildInThumbnails))
                                                {
                                                  if (equals(this.buildInPreviews, localResource.buildInPreviews))
                                                  {
                                                    if (equals(this.thumbnails, localResource.thumbnails))
                                                    {
                                                      if (equals(this.previews, localResource.previews))
                                                      {
                                                        if (equals(this.parentResources, localResource.parentResources))
                                                        {
                                                          if (equals(this.subResources, localResource.subResources))
                                                          {
                                                            if (equals(this.extraMeta, localResource.extraMeta))
                                                            {
                                                              if (equals(Integer.valueOf(this.productPrice), Integer.valueOf(localResource.productPrice)))
                                                              {
                                                                if (!equals(Boolean.valueOf(this.productBought), Boolean.valueOf(localResource.productBought)))
                                                                  i = 0;
                                                              }
                                                              else
                                                                i = 0;
                                                            }
                                                            else
                                                              i = 0;
                                                          }
                                                          else
                                                            i = 0;
                                                        }
                                                        else
                                                          i = 0;
                                                      }
                                                      else
                                                        i = 0;
                                                    }
                                                    else
                                                      i = 0;
                                                  }
                                                  else
                                                    i = 0;
                                                }
                                                else
                                                  i = 0;
                                              }
                                              else
                                                i = 0;
                                            }
                                            else
                                              i = 0;
                                          }
                                          else
                                            i = 0;
                                        }
                                        else
                                          i = 0;
                                      }
                                      else
                                        i = 0;
                                    }
                                    else
                                      i = 0;
                                  }
                                  else
                                    i = 0;
                                }
                                else
                                  i = 0;
                              }
                              else
                                i = 0;
                            }
                            else
                              i = 0;
                          }
                          else
                            i = 0;
                        }
                        else
                          i = 0;
                      }
                      else
                        i = 0;
                    }
                    else
                      i = 0;
                  }
                  else
                    i = 0;
                }
                else
                  i = 0;
              }
              else
                i = 0;
            }
            else
              i = 0;
          }
          else
            i = 0;
        }
        else
        {
          i = 0;
        }
      }
      else
        i = 0;
    return i;
  }

  public String getAuthor()
  {
    return this.author;
  }

  public List<String> getBuildInPreviews()
  {
    return this.buildInPreviews;
  }

  public List<String> getBuildInThumbnails()
  {
    return this.buildInThumbnails;
  }

  public String getDescription()
  {
    return this.description;
  }

  public String getDesigner()
  {
    return this.designer;
  }

  public String getDownloadCount()
  {
    return this.downloadCount;
  }

  public String getDownloadPath()
  {
    return this.downloadPath;
  }

  public String getExtraMeta(String paramString)
  {
    return (String)this.extraMeta.get(paramString);
  }

  public String getExtraMeta(String paramString1, String paramString2)
  {
    String str = (String)this.extraMeta.get(paramString1);
    if (str == null)
      str = paramString2;
    return str;
  }

  public Map<String, String> getExtraMeta()
  {
    return this.extraMeta;
  }

  public String getFilePath()
  {
    return this.filePath;
  }

  public String getHash()
  {
    return this.hash;
  }

  public String getLocalId()
  {
    return this.localId;
  }

  public String getLocalPath()
  {
    return this.localPath;
  }

  public String getOnlineId()
  {
    return this.onlineId;
  }

  public String getOnlinePath()
  {
    return this.onlinePath;
  }

  public List<RelatedResource> getParentResources()
  {
    return this.parentResources;
  }

  public int getPlatform()
  {
    return this.platform;
  }

  public List<PathEntry> getPreviews()
  {
    return this.previews;
  }

  public String getProductId()
  {
    return this.productId;
  }

  public int getProductPrice()
  {
    return this.productPrice;
  }

  public String getRightsPath()
  {
    String str;
    if (!TextUtils.isEmpty(this.rightsPath))
      str = this.rightsPath;
    else
      str = getFakeRightsPath();
    return str;
  }

  public long getSize()
  {
    return this.size;
  }

  public int getStatus()
  {
    return this.status;
  }

  public List<RelatedResource> getSubResources()
  {
    return this.subResources;
  }

  public List<PathEntry> getThumbnails()
  {
    return this.thumbnails;
  }

  public String getTitle()
  {
    return this.title;
  }

  public long getUpdatedTime()
  {
    return this.updatedTime;
  }

  public String getVersion()
  {
    return this.version;
  }

  public int hashCode()
  {
    int i = 0;
    if (this.author != null)
      j = this.author.hashCode();
    else
      j = 0;
    int j = 31 * (j + 31);
    if (this.buildInPreviews != null)
      k = this.buildInPreviews.hashCode();
    else
      k = 0;
    int k = 31 * (j + k);
    if (this.buildInThumbnails != null)
      j = this.buildInThumbnails.hashCode();
    else
      j = 0;
    k = 31 * (k + j);
    if (this.description != null)
      j = this.description.hashCode();
    else
      j = 0;
    k = 31 * (k + j);
    if (this.designer != null)
      j = this.designer.hashCode();
    else
      j = 0;
    k = 31 * (k + j);
    if (this.downloadCount != null)
      j = this.downloadCount.hashCode();
    else
      j = 0;
    k = 31 * (k + j);
    if (this.downloadPath != null)
      j = this.downloadPath.hashCode();
    else
      j = 0;
    k = 31 * (k + j);
    if (this.extraMeta != null)
      j = this.extraMeta.hashCode();
    else
      j = 0;
    k = 31 * (k + j);
    if (this.filePath != null)
      j = this.filePath.hashCode();
    else
      j = 0;
    k = 31 * (k + j);
    if (this.hash != null)
      j = this.hash.hashCode();
    else
      j = 0;
    k = 31 * (k + j);
    if (this.localId != null)
      j = this.localId.hashCode();
    else
      j = 0;
    k = 31 * (k + j);
    if (this.localPath != null)
      j = this.localPath.hashCode();
    else
      j = 0;
    j = 31 * (k + j);
    if (this.onlineId != null)
      k = this.onlineId.hashCode();
    else
      k = 0;
    k = 31 * (j + k);
    if (this.onlinePath != null)
      j = this.onlinePath.hashCode();
    else
      j = 0;
    j = 31 * (k + j);
    if (this.parentResources != null)
      k = this.parentResources.hashCode();
    else
      k = 0;
    j = 31 * (31 * (j + k) + this.platform);
    if (this.previews != null)
      k = this.previews.hashCode();
    else
      k = 0;
    j = 31 * (j + k);
    if (!this.productBought)
      k = 1237;
    else
      k = 1231;
    j = 31 * (j + k);
    if (this.productId != null)
      k = this.productId.hashCode();
    else
      k = 0;
    j = 31 * (31 * (j + k) + this.productPrice);
    if (this.rightsPath != null)
      k = this.rightsPath.hashCode();
    else
      k = 0;
    k = 31 * (31 * (31 * (j + k) + (int)(this.size ^ this.size >>> 32)) + this.status);
    if (this.subResources != null)
      j = this.subResources.hashCode();
    else
      j = 0;
    j = 31 * (k + j);
    if (this.thumbnails != null)
      k = this.thumbnails.hashCode();
    else
      k = 0;
    j = 31 * (j + k);
    if (this.title != null)
      k = this.title.hashCode();
    else
      k = 0;
    j = 31 * (31 * (j + k) + (int)(this.updatedTime ^ this.updatedTime >>> 32));
    if (this.version != null)
      i = this.version.hashCode();
    return j + i;
  }

  public boolean isProductBought()
  {
    return this.productBought;
  }

  public void putExtraMeta(String paramString1, String paramString2)
  {
    this.extraMeta.put(paramString1, paramString2);
  }

  public void setAuthor(String paramString)
  {
    this.author = paramString;
  }

  public void setBuildInPreviews(List<String> paramList)
  {
    this.buildInPreviews = paramList;
  }

  public void setBuildInThumbnails(List<String> paramList)
  {
    this.buildInThumbnails = paramList;
  }

  public void setDescription(String paramString)
  {
    this.description = paramString;
  }

  public void setDesigner(String paramString)
  {
    this.designer = paramString;
  }

  public void setDownloadCount(String paramString)
  {
    this.downloadCount = paramString;
  }

  public void setDownloadPath(String paramString)
  {
    this.downloadPath = paramString;
  }

  public void setExtraMeta(Map<String, String> paramMap)
  {
    this.extraMeta = paramMap;
  }

  public void setFilePath(String paramString)
  {
    this.filePath = paramString;
  }

  public void setHash(String paramString)
  {
    this.hash = paramString;
  }

  public void setLocalId(String paramString)
  {
    this.localId = paramString;
  }

  public void setLocalPath(String paramString)
  {
    this.localPath = paramString;
  }

  public void setOnlineId(String paramString)
  {
    this.onlineId = paramString;
  }

  public void setOnlinePath(String paramString)
  {
    this.onlinePath = paramString;
  }

  public void setParentResources(List<RelatedResource> paramList)
  {
    this.parentResources = paramList;
  }

  public void setPlatform(int paramInt)
  {
    this.platform = paramInt;
  }

  public void setPreviews(List<PathEntry> paramList)
  {
    this.previews = paramList;
  }

  public void setProductBought(boolean paramBoolean)
  {
    this.productBought = paramBoolean;
  }

  public void setProductId(String paramString)
  {
    this.productId = paramString;
  }

  public void setProductPrice(int paramInt)
  {
    this.productPrice = paramInt;
  }

  public void setRightsPath(String paramString)
  {
    this.rightsPath = paramString;
  }

  public void setSize(long paramLong)
  {
    this.size = paramLong;
  }

  public void setStatus(int paramInt)
  {
    this.status = paramInt;
  }

  public void setSubResources(List<RelatedResource> paramList)
  {
    this.subResources = paramList;
  }

  public void setThumbnails(List<PathEntry> paramList)
  {
    this.thumbnails = paramList;
  }

  public void setTitle(String paramString)
  {
    this.title = paramString;
  }

  public void setUpdatedTime(long paramLong)
  {
    this.updatedTime = paramLong;
  }

  public void setVersion(String paramString)
  {
    this.version = paramString;
  }

  public void updateFrom(Resource paramResource)
  {
    if ((this != paramResource) && (paramResource != null))
    {
      this.localId = paramResource.localId;
      this.onlineId = paramResource.onlineId;
      this.productId = paramResource.productId;
      this.localPath = paramResource.localPath;
      this.onlinePath = paramResource.onlinePath;
      this.filePath = paramResource.filePath;
      this.downloadPath = paramResource.downloadPath;
      this.rightsPath = paramResource.rightsPath;
      this.platform = paramResource.platform;
      this.status = paramResource.status;
      this.hash = paramResource.hash;
      this.size = paramResource.size;
      this.updatedTime = paramResource.updatedTime;
      this.title = paramResource.title;
      this.description = paramResource.description;
      this.author = paramResource.author;
      this.designer = paramResource.designer;
      this.version = paramResource.version;
      this.downloadCount = paramResource.downloadCount;
      this.buildInThumbnails = paramResource.buildInThumbnails;
      this.buildInPreviews = paramResource.buildInPreviews;
      this.thumbnails = paramResource.thumbnails;
      this.previews = paramResource.previews;
      this.parentResources = paramResource.parentResources;
      this.subResources = paramResource.subResources;
      this.extraMeta = paramResource.extraMeta;
      this.productPrice = paramResource.productPrice;
      this.productBought = paramResource.productBought;
    }
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.model.Resource
 * JD-Core Version:    0.6.0
 */