package miui.resourcebrowser.model;

public class ResourceListMeta
{
  private String category;
  private String description;
  private String name;

  public String getDescription()
  {
    return this.description;
  }

  public void setCategory(String paramString)
  {
    this.category = paramString;
  }

  public void setDescription(String paramString)
  {
    this.description = paramString;
  }

  public void setName(String paramString)
  {
    this.name = paramString;
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.model.ResourceListMeta
 * JD-Core Version:    0.6.0
 */