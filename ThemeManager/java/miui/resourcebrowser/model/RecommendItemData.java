package miui.resourcebrowser.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import miui.resourcebrowser.view.UnevenGrid.GridItemData;

public class RecommendItemData extends UnevenGrid.GridItemData
  implements Serializable
{
  public static final int TYPE_LIST = 2;
  public static final int TYPE_SINGLE = 1;
  public static final int TYPE_UNKNOWN = 0;
  private static final long serialVersionUID = 1L;
  public String itemId;
  public int itemType;
  public String localThumbnail;
  public String onlineThumbnail;
  public List<RecommendItemData> subItems = new ArrayList();
  public String title;
  public String type;

  public String toString()
  {
    return this.title;
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.model.RecommendItemData
 * JD-Core Version:    0.6.0
 */