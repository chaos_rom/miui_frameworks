package miui.resourcebrowser.activity;

import android.app.Activity;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;
import miui.resourcebrowser.ResourceContext;
import miui.resourcebrowser.controller.LocalDataManager;
import miui.resourcebrowser.controller.OnlineDataManager;
import miui.resourcebrowser.controller.ResourceController;
import miui.resourcebrowser.controller.online.OnlineService;
import miui.resourcebrowser.model.Resource;
import miui.resourcebrowser.widget.AsyncAdapter;
import miui.resourcebrowser.widget.AsyncAdapter.AsyncLoadDataTask;
import miui.resourcebrowser.widget.AsyncAdapter.AsyncLoadMoreDataTask;
import miui.resourcebrowser.widget.AsyncAdapter.AsyncLoadMoreParams;

public class ResourceSearchAdapter extends ResourceAdapter
{
  private final int POST_LOAD_DATA_COUNT_MAX = 2;
  private String mKeyword;
  private OnlineService mService;
  private int postLoadDataCount;

  public ResourceSearchAdapter(BaseFragment paramBaseFragment, ResourceContext paramResourceContext)
  {
    super(paramBaseFragment, paramResourceContext);
    initParams(paramResourceContext);
  }

  private void initParams(ResourceContext paramResourceContext)
  {
    this.mService = new OnlineService(paramResourceContext);
  }

  protected int getDownloadableFlag(Resource paramResource, int paramInt)
  {
    int i = super.getDownloadableFlag(paramResource, paramInt);
    if ((i == 100794426) && (paramInt == 0))
      i = 0;
    return i;
  }

  protected String getGroupTitle(int paramInt)
  {
    Object localObject1;
    Object localObject2;
    switch (paramInt)
    {
    default:
      localObject1 = super.getGroupTitle(paramInt);
      break;
    case 0:
      localObject1 = this.mContext.getString(101449748);
      localObject2 = new Object[1];
      localObject2[0] = this.mResContext.getResourceTitle();
      localObject1 = String.format((String)localObject1, localObject2);
      break;
    case 1:
      localObject2 = this.mContext.getString(101449749);
      localObject1 = new Object[1];
      localObject1[0] = this.mResContext.getResourceTitle();
      localObject1 = String.format((String)localObject2, localObject1);
    }
    return (String)(String)localObject1;
  }

  protected List<AsyncAdapter<Resource>.AsyncLoadDataTask> getLoadDataTask()
  {
    ArrayList localArrayList = new ArrayList();
    AsyncLoadResourceTask localAsyncLoadResourceTask = new AsyncLoadResourceTask();
    localAsyncLoadResourceTask.addObserver(getRegisterAsyncTaskObserver());
    localArrayList.add(localAsyncLoadResourceTask);
    localArrayList.add(null);
    return localArrayList;
  }

  protected List<AsyncAdapter<Resource>.AsyncLoadMoreDataTask> getLoadMoreDataTask()
  {
    ArrayList localArrayList = new ArrayList();
    AsyncLoadMoreResourceTask localAsyncLoadMoreResourceTask = new AsyncLoadMoreResourceTask();
    localAsyncLoadMoreResourceTask.addObserver(getRegisterAsyncTaskObserver());
    localArrayList.add(null);
    localArrayList.add(localAsyncLoadMoreResourceTask);
    return localArrayList;
  }

  protected void postLoadData(List<Resource> paramList)
  {
    super.postLoadData(paramList);
    updateNoResultText();
  }

  protected void postLoadMoreData(List<Resource> paramList)
  {
    if ((paramList == null) && ((this.mFragment == null) || (this.mFragment.isVisibleForUser())))
      Toast.makeText(this.mContext, 101449760, 0).show();
    updateNoResultText();
  }

  public void setKeyword(String paramString)
  {
    this.mKeyword = paramString;
    this.mResContext.setListUrl(this.mService.getSearchListUrl(this.mKeyword));
  }

  protected void updateNoResultText()
  {
    monitorenter;
    try
    {
      this.postLoadDataCount = (1 + this.postLoadDataCount);
      if (this.postLoadDataCount == 2)
        super.updateNoResultText();
      monitorexit;
      return;
    }
    finally
    {
      localObject = finally;
      monitorexit;
    }
    throw localObject;
  }

  protected class AsyncLoadMoreResourceTask extends AsyncAdapter.AsyncLoadMoreDataTask
  {
    protected AsyncLoadMoreResourceTask()
    {
      super();
    }

    protected List<Resource> loadMoreData(AsyncAdapter.AsyncLoadMoreParams paramAsyncLoadMoreParams)
    {
      int i = ResourceSearchAdapter.this.mResContext.getPageItemCount();
      return ResourceSearchAdapter.this.mResController.getOnlineDataManager().getResources((-1 + (i + paramAsyncLoadMoreParams.cursor)) / i);
    }
  }

  protected class AsyncLoadResourceTask extends AsyncAdapter.AsyncLoadDataTask
  {
    protected AsyncLoadResourceTask()
    {
      super();
    }

    protected int getMode()
    {
      return 0;
    }

    protected Resource[] loadData()
    {
      return (Resource[])ResourceSearchAdapter.this.mResController.getLocalDataManager().findResources(ResourceSearchAdapter.this.mKeyword).toArray(new Resource[0]);
    }
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.activity.ResourceSearchAdapter
 * JD-Core Version:    0.6.0
 */