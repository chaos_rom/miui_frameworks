package miui.resourcebrowser.activity;

import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;
import miui.resourcebrowser.ResourceContext;
import miui.resourcebrowser.controller.OnlineDataManager;
import miui.resourcebrowser.controller.ResourceController;
import miui.resourcebrowser.model.Resource;
import miui.resourcebrowser.widget.AsyncAdapter;
import miui.resourcebrowser.widget.AsyncAdapter.AsyncLoadMoreDataTask;
import miui.resourcebrowser.widget.AsyncAdapter.AsyncLoadMoreParams;

public class OnlineResourceAdapter extends ResourceAdapter
{
  public OnlineResourceAdapter(BaseFragment paramBaseFragment, ResourceContext paramResourceContext)
  {
    super(paramBaseFragment, paramResourceContext);
  }

  protected List<AsyncAdapter<Resource>.AsyncLoadMoreDataTask> getLoadMoreDataTask()
  {
    ArrayList localArrayList = new ArrayList();
    AsyncLoadMoreResourceTask localAsyncLoadMoreResourceTask = new AsyncLoadMoreResourceTask();
    localAsyncLoadMoreResourceTask.addObserver(getRegisterAsyncTaskObserver());
    localArrayList.add(localAsyncLoadMoreResourceTask);
    return localArrayList;
  }

  protected void postLoadMoreData(List<Resource> paramList)
  {
    if ((paramList == null) && ((this.mFragment == null) || (this.mFragment.isVisibleForUser())))
      Toast.makeText(this.mContext, 101449760, 0).show();
    updateNoResultText();
  }

  protected class AsyncLoadMoreResourceTask extends AsyncAdapter.AsyncLoadMoreDataTask
  {
    protected AsyncLoadMoreResourceTask()
    {
      super();
    }

    protected List<Resource> loadMoreData(AsyncAdapter.AsyncLoadMoreParams paramAsyncLoadMoreParams)
    {
      int i = OnlineResourceAdapter.this.mResContext.getPageItemCount();
      return OnlineResourceAdapter.this.mResController.getOnlineDataManager().getResources((-1 + (i + paramAsyncLoadMoreParams.cursor)) / i);
    }
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.activity.OnlineResourceAdapter
 * JD-Core Version:    0.6.0
 */