//This did not convert in jd-gui.  Need to copy the raw smali here to be converted

 
//SMALI - LocalResourceAdapter.smali
//Wizard0f0s

.class public Lmiui/resourcebrowser/activity/LocalResourceAdapter;
.super Lmiui/resourcebrowser/activity/ResourceAdapter;
.source "LocalResourceAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/resourcebrowser/activity/LocalResourceAdapter$1;,
        Lmiui/resourcebrowser/activity/LocalResourceAdapter$DownloadVersionTask;,
        Lmiui/resourcebrowser/activity/LocalResourceAdapter$AsyncLoadResourceTask;
    }
.end annotation


# instance fields
.field private mFileHashMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lmiui/resourcebrowser/activity/BaseFragment;Lmiui/resourcebrowser/ResourceContext;)V
    .locals 1
    .parameter "fragment"
    .parameter "resContext"

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Lmiui/resourcebrowser/activity/ResourceAdapter;-><init>(Lmiui/resourcebrowser/activity/BaseFragment;Lmiui/resourcebrowser/ResourceContext;)V

    .line 21
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/activity/LocalResourceAdapter;->mFileHashMap:Ljava/util/Map;

    .line 25
    return-void
.end method

.method static synthetic access$100(Lmiui/resourcebrowser/activity/LocalResourceAdapter;Ljava/util/List;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lmiui/resourcebrowser/activity/LocalResourceAdapter;->setVersionStatus(Ljava/util/List;)V

    return-void
.end method

.method private requestVersionStatus(Ljava/util/List;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 84
    .local p1, hashs:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    new-instance v0, Lmiui/resourcebrowser/activity/LocalResourceAdapter$DownloadVersionTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lmiui/resourcebrowser/activity/LocalResourceAdapter$DownloadVersionTask;-><init>(Lmiui/resourcebrowser/activity/LocalResourceAdapter;Lmiui/resourcebrowser/activity/LocalResourceAdapter$1;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/util/List;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/activity/LocalResourceAdapter$DownloadVersionTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 85
    return-void
.end method

.method private setVersionStatus(Ljava/util/List;)V
    .locals 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 103
    .local p1, updatableResources:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/Resource;>;"
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 104
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/resourcebrowser/model/Resource;

    .line 105
    .local v2, updatableResource:Lmiui/resourcebrowser/model/Resource;
    iget-object v3, p0, Lmiui/resourcebrowser/activity/LocalResourceAdapter;->mFileHashMap:Ljava/util/Map;

    invoke-virtual {v2}, Lmiui/resourcebrowser/model/Resource;->getHash()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/resourcebrowser/model/Resource;

    .line 106
    .local v1, matchedResource:Lmiui/resourcebrowser/model/Resource;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lmiui/resourcebrowser/model/Resource;->getStatus()I

    move-result v3

    invoke-static {v3}, Lmiui/resourcebrowser/util/ResourceHelper;->isOldResource(I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 107
    invoke-virtual {v1}, Lmiui/resourcebrowser/model/Resource;->getStatus()I

    move-result v3

    or-int/lit8 v3, v3, 0x4

    invoke-virtual {v1, v3}, Lmiui/resourcebrowser/model/Resource;->setStatus(I)V

    .line 108
    iget-object v3, p0, Lmiui/resourcebrowser/activity/LocalResourceAdapter;->mResController:Lmiui/resourcebrowser/controller/ResourceController;

    invoke-virtual {v3}, Lmiui/resourcebrowser/controller/ResourceController;->getLocalDataManager()Lmiui/resourcebrowser/controller/LocalDataManager;

    move-result-object v3

    invoke-virtual {v3, v1}, Lmiui/resourcebrowser/controller/LocalDataManager;->updateResource(Lmiui/resourcebrowser/model/Resource;)Z

    .line 103
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 111
    .end local v1           #matchedResource:Lmiui/resourcebrowser/model/Resource;
    .end local v2           #updatableResource:Lmiui/resourcebrowser/model/Resource;
    :cond_1
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/LocalResourceAdapter;->notifyDataSetChanged()V

    .line 112
    return-void
.end method


# virtual methods
.method protected getDownloadableFlag(Lmiui/resourcebrowser/model/Resource;I)I
    .locals 2
    .parameter "resourceItem"
    .parameter "group"

    .prologue
    .line 33
    invoke-super {p0, p1, p2}, Lmiui/resourcebrowser/activity/ResourceAdapter;->getDownloadableFlag(Lmiui/resourcebrowser/model/Resource;I)I

    move-result v0

    .line 34
    .local v0, flag:I
    const v1, 0x602003a

    if-ne v0, v1, :cond_0

    .line 35
    const/4 v0, 0x0

    .line 37
    :cond_0
    return v0
.end method

.method protected getLoadDataTask()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/widget/AsyncAdapter",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">.Async",
            "LoadDataTask;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 43
    .local v1, tasks:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/widget/AsyncAdapter<Lmiui/resourcebrowser/model/Resource;>.AsyncLoadDataTask;>;"
    new-instance v0, Lmiui/resourcebrowser/activity/LocalResourceAdapter$AsyncLoadResourceTask;

    invoke-direct {v0, p0}, Lmiui/resourcebrowser/activity/LocalResourceAdapter$AsyncLoadResourceTask;-><init>(Lmiui/resourcebrowser/activity/LocalResourceAdapter;)V

    .line 44
    .local v0, task:Lmiui/resourcebrowser/activity/LocalResourceAdapter$AsyncLoadResourceTask;
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/LocalResourceAdapter;->getRegisterAsyncTaskObserver()Lmiui/os/AsyncTaskObserver;

    move-result-object v2

    invoke-virtual {v0, v2}, Lmiui/resourcebrowser/activity/LocalResourceAdapter$AsyncLoadResourceTask;->addObserver(Lmiui/os/AsyncTaskObserver;)V

    .line 45
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 46
    return-object v1
.end method

.method protected postLoadData(Ljava/util/List;)V
    .locals 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 64
    .local p1, result:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/Resource;>;"
    iget-object v4, p0, Lmiui/resourcebrowser/activity/LocalResourceAdapter;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v4}, Lmiui/resourcebrowser/ResourceContext;->isVersionSupported()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 65
    iget-object v4, p0, Lmiui/resourcebrowser/activity/LocalResourceAdapter;->mFileHashMap:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->clear()V

    .line 66
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 67
    .local v1, hashs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .local v2, i:I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v2, v4, :cond_1

    .line 68
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmiui/resourcebrowser/model/Resource;

    .line 69
    .local v3, resource:Lmiui/resourcebrowser/model/Resource;
    invoke-virtual {v3}, Lmiui/resourcebrowser/model/Resource;->getOnlinePath()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 67
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 72
    :cond_0
    invoke-virtual {v3}, Lmiui/resourcebrowser/model/Resource;->getHash()Ljava/lang/String;

    move-result-object v0

    .line 73
    .local v0, hash:Ljava/lang/String;
    iget-object v4, p0, Lmiui/resourcebrowser/activity/LocalResourceAdapter;->mFileHashMap:Ljava/util/Map;

    invoke-interface {v4, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 77
    .end local v0           #hash:Ljava/lang/String;
    .end local v3           #resource:Lmiui/resourcebrowser/model/Resource;
    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    .line 78
    invoke-direct {p0, v1}, Lmiui/resourcebrowser/activity/LocalResourceAdapter;->requestVersionStatus(Ljava/util/List;)V

    .line 81
    .end local v1           #hashs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v2           #i:I
    :cond_2
    return-void
.end method


//SMALI - LocalResourceAdapter$1.smali
//Wizard0f0s

.class synthetic Lmiui/resourcebrowser/activity/LocalResourceAdapter$1;
.super Ljava/lang/Object;
.source "LocalResourceAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/resourcebrowser/activity/LocalResourceAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


//SMALI - LocalResourceAdapter$AsyncLoadResourceTask.smali
//Wizard0f0s

.class public Lmiui/resourcebrowser/activity/LocalResourceAdapter$AsyncLoadResourceTask;
.super Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;
.source "LocalResourceAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/resourcebrowser/activity/LocalResourceAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "AsyncLoadResourceTask"
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/resourcebrowser/activity/LocalResourceAdapter;


# direct methods
.method protected constructor <init>(Lmiui/resourcebrowser/activity/LocalResourceAdapter;)V
    .locals 0
    .parameter

    .prologue
    .line 49
    iput-object p1, p0, Lmiui/resourcebrowser/activity/LocalResourceAdapter$AsyncLoadResourceTask;->this$0:Lmiui/resourcebrowser/activity/LocalResourceAdapter;

    invoke-direct {p0, p1}, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;-><init>(Lmiui/resourcebrowser/widget/AsyncAdapter;)V

    return-void
.end method


# virtual methods
.method protected getMode()I
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    return v0
.end method

.method protected bridge synthetic loadData()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 49
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/LocalResourceAdapter$AsyncLoadResourceTask;->loadData()[Lmiui/resourcebrowser/model/Resource;

    move-result-object v0

    return-object v0
.end method

.method protected loadData()[Lmiui/resourcebrowser/model/Resource;
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lmiui/resourcebrowser/activity/LocalResourceAdapter$AsyncLoadResourceTask;->this$0:Lmiui/resourcebrowser/activity/LocalResourceAdapter;

    iget-object v0, v0, Lmiui/resourcebrowser/activity/LocalResourceAdapter;->mResController:Lmiui/resourcebrowser/controller/ResourceController;

    invoke-virtual {v0}, Lmiui/resourcebrowser/controller/ResourceController;->getLocalDataManager()Lmiui/resourcebrowser/controller/LocalDataManager;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/resourcebrowser/controller/LocalDataManager;->getResources()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Lmiui/resourcebrowser/model/Resource;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmiui/resourcebrowser/model/Resource;

    return-object v0
.end method


//SMALI - LocalResourceAdapter$DownloadVersionTask.smali
//Wizard0f0s

.class Lmiui/resourcebrowser/activity/LocalResourceAdapter$DownloadVersionTask;
.super Landroid/os/AsyncTask;
.source "LocalResourceAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/resourcebrowser/activity/LocalResourceAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DownloadVersionTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/lang/String;",
        ">;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Lmiui/resourcebrowser/model/Resource;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/resourcebrowser/activity/LocalResourceAdapter;


# direct methods
.method private constructor <init>(Lmiui/resourcebrowser/activity/LocalResourceAdapter;)V
    .locals 0
    .parameter

    .prologue
    .line 87
    iput-object p1, p0, Lmiui/resourcebrowser/activity/LocalResourceAdapter$DownloadVersionTask;->this$0:Lmiui/resourcebrowser/activity/LocalResourceAdapter;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lmiui/resourcebrowser/activity/LocalResourceAdapter;Lmiui/resourcebrowser/activity/LocalResourceAdapter$1;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lmiui/resourcebrowser/activity/LocalResourceAdapter$DownloadVersionTask;-><init>(Lmiui/resourcebrowser/activity/LocalResourceAdapter;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"

    .prologue
    .line 87
    check-cast p1, [Ljava/util/List;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/activity/LocalResourceAdapter$DownloadVersionTask;->doInBackground([Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/util/List;)Ljava/util/List;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 91
    .local p1, params:[Ljava/util/List;,"[Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lmiui/resourcebrowser/activity/LocalResourceAdapter$DownloadVersionTask;->this$0:Lmiui/resourcebrowser/activity/LocalResourceAdapter;

    iget-object v0, v0, Lmiui/resourcebrowser/activity/ResourceAdapter;->mResController:Lmiui/resourcebrowser/controller/ResourceController;

    invoke-virtual {v0}, Lmiui/resourcebrowser/controller/ResourceController;->getOnlineDataManager()Lmiui/resourcebrowser/controller/OnlineDataManager;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/controller/OnlineDataManager;->getUpdatableResources(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 87
    check-cast p1, Ljava/util/List;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/activity/LocalResourceAdapter$DownloadVersionTask;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 96
    .local p1, updatableResources:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/Resource;>;"
    if-eqz p1, :cond_0

    .line 97
    iget-object v0, p0, Lmiui/resourcebrowser/activity/LocalResourceAdapter$DownloadVersionTask;->this$0:Lmiui/resourcebrowser/activity/LocalResourceAdapter;

    #calls: Lmiui/resourcebrowser/activity/LocalResourceAdapter;->setVersionStatus(Ljava/util/List;)V
    invoke-static {v0, p1}, Lmiui/resourcebrowser/activity/LocalResourceAdapter;->access$100(Lmiui/resourcebrowser/activity/LocalResourceAdapter;Ljava/util/List;)V

    .line 99
    :cond_0
    return-void
.end method

//End SMALI Code
