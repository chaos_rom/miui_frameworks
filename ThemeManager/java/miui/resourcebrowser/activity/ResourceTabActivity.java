package miui.resourcebrowser.activity;

import android.app.ActionBar;
import android.app.ActionBar.LayoutParams;
import android.app.ActionBar.Tab;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import java.util.ArrayList;
import java.util.List;
import java.util.List<Landroid.app.ActionBar.Tab;>;
import miui.resourcebrowser.AppInnerContext;
import miui.resourcebrowser.ResourceContext;
import miui.resourcebrowser.controller.ResourceController;
import miui.resourcebrowser.util.ResourceHelper;

public class ResourceTabActivity extends BaseTabActivity
{
  protected ResourceContext mResContext;

  protected ResourceContext buildResourceContext(ResourceContext paramResourceContext)
  {
    return ResourceHelper.buildResourceContext(paramResourceContext, getIntent(), this);
  }

  protected List<ActionBar.Tab> getActionBarTabs()
  {
    ArrayList localArrayList = new ArrayList();
    Object localObject1 = getActionBar();
    String str = this.mResContext.getResourceTitle();
    ActionBar.Tab localTab = ((ActionBar)localObject1).newTab();
    Object localObject2 = new Object[1];
    localObject2[0] = str;
    localArrayList.add(localTab.setText(getString(101449748, localObject2)));
    localObject2 = ((ActionBar)localObject1).newTab();
    localObject1 = new Object[1];
    localObject1[0] = str;
    localArrayList.add(((ActionBar.Tab)localObject2).setText(getString(101449749, localObject1)));
    return (List<ActionBar.Tab>)(List<ActionBar.Tab>)localArrayList;
  }

  protected BaseFragment getLocalResourceListFragment()
  {
    return new LocalResourceListFragment();
  }

  protected BaseFragment getOnlineResourceListFragment()
  {
    return new OnlineResourceListFragment();
  }

  protected ResourceController getResourceController(ResourceContext paramResourceContext)
  {
    return new ResourceController(paramResourceContext);
  }

  protected BaseFragment initTabFragment(int paramInt)
  {
    BaseFragment localBaseFragment;
    if (paramInt != 0)
    {
      if (paramInt != 1)
        localBaseFragment = null;
      else
        localBaseFragment = getOnlineResourceListFragment();
    }
    else
      localBaseFragment = getLocalResourceListFragment();
    return localBaseFragment;
  }

  protected void onCreate(Bundle paramBundle)
  {
    this.mResContext = ((ResourceContext)getIntent().getSerializableExtra("REQUEST_RES_CONTEXT"));
    if (this.mResContext == null)
      this.mResContext = buildResourceContext(new ResourceContext());
    Object localObject = AppInnerContext.getInstance();
    ((AppInnerContext)localObject).setApplicationContext(getApplicationContext());
    ((AppInnerContext)localObject).setResourceContext(this.mResContext);
    ((AppInnerContext)localObject).setResourceController(getResourceController(this.mResContext));
    super.onCreate(paramBundle);
    ActionBar localActionBar = getActionBar();
    localObject = new ImageView(this);
    ((ImageView)localObject).setImageResource(100794808);
    ((ImageView)localObject).setPadding(0, 0, 18, 0);
    ((ImageView)localObject).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramView)
      {
        ResourceTabActivity.this.onSearchRequested();
      }
    });
    localActionBar.setDisplayShowCustomEnabled(true);
    localActionBar.setCustomView((View)localObject, new ActionBar.LayoutParams(5));
    new Handler().postDelayed(new Runnable()
    {
      public void run()
      {
        int i = ResourceTabActivity.this.getIntent().getIntExtra("android.intent.extra.ringtone.TYPE", -1);
        if (i >= 0)
          ResourceHelper.setMusicVolumeType(ResourceTabActivity.this, i);
      }
    }
    , 800L);
  }

  public boolean onSearchRequested()
  {
    Bundle localBundle = new Bundle();
    localBundle.putSerializable("REQUEST_RES_CONTEXT", this.mResContext);
    startSearch(null, false, localBundle, false);
    return true;
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.activity.ResourceTabActivity
 * JD-Core Version:    0.6.0
 */