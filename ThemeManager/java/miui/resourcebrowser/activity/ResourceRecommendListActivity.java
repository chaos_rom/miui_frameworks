package miui.resourcebrowser.activity;

import android.app.ActionBar;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import miui.app.ObservableActivity;
import miui.resourcebrowser.AppInnerContext;
import miui.resourcebrowser.ResourceContext;
import miui.resourcebrowser.controller.ResourceController;

public class ResourceRecommendListActivity extends ObservableActivity
{
  protected BaseFragment mFragment;
  protected ResourceContext mResContext;

  protected BaseFragment getFragment()
  {
    return new OnlineResourceListFragment();
  }

  protected ResourceController getResourceController(ResourceContext paramResourceContext)
  {
    return new ResourceController(paramResourceContext);
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(100859929);
    this.mResContext = ((ResourceContext)getIntent().getSerializableExtra("REQUEST_RES_CONTEXT"));
    AppInnerContext localAppInnerContext = AppInnerContext.getInstance();
    localAppInnerContext.setResourceContext(this.mResContext);
    Object localObject = getResourceController(this.mResContext);
    ((ResourceController)localObject).setLocalDataManager(localAppInnerContext.getResourceController().getLocalDataManager());
    ((ResourceController)localObject).setImportManager(localAppInnerContext.getResourceController().getImportManager());
    localAppInnerContext.setResourceController((ResourceController)localObject);
    getActionBar().setHomeButtonEnabled(true);
    localObject = getFragmentManager().beginTransaction();
    this.mFragment = getFragment();
    ((FragmentTransaction)localObject).add(101384348, this.mFragment);
    ((FragmentTransaction)localObject).commit();
    this.mFragment.setVisibleForUser(true);
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    if (paramMenuItem.getItemId() == 16908332)
      finish();
    return super.onOptionsItemSelected(paramMenuItem);
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.activity.ResourceRecommendListActivity
 * JD-Core Version:    0.6.0
 */