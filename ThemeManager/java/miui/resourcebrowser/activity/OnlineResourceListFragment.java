package miui.resourcebrowser.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.os.AsyncTask;
import android.provider.Settings.System;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import java.util.List;
import miui.resourcebrowser.ResourceContext;
import miui.resourcebrowser.controller.OnlineDataManager;
import miui.resourcebrowser.controller.ResourceController;
import miui.resourcebrowser.controller.online.OnlineService;
import miui.resourcebrowser.controller.online.RequestUrl;
import miui.resourcebrowser.model.RecommendItemData;
import miui.resourcebrowser.model.ResourceCategory;
import miui.resourcebrowser.model.ResourceListMeta;
import miui.resourcebrowser.util.ResourceHelper;
import miui.resourcebrowser.view.RecommendGridItemFactory;
import miui.resourcebrowser.view.UnevenGrid;

public class OnlineResourceListFragment extends ResourceListFragment
{
  protected ArrayAdapter<ResourceCategory> mCategoryAdapter;
  protected Spinner mCategoryList;
  protected boolean mHasSubRecommends;
  protected boolean mIsRecommendList;
  protected String mRecommendId;
  protected OnlineService mService;
  protected ArrayAdapter<RecommendItemData> mSubRecommendAdapter;
  protected Spinner mSubRecommendList;
  protected List<RecommendItemData> mSubRecommends;
  protected TextView mTextView;
  protected UnevenGrid mUnevenGrid;

  private void showSeeMoreTextOnly(boolean paramBoolean)
  {
    int i = 0;
    View localView = getView().findViewById(101384272);
    int j;
    if (!paramBoolean)
      j = 8;
    else
      j = 0;
    localView.setVisibility(j);
    localView = getView().findViewById(101384270);
    if (paramBoolean)
      i = 4;
    localView.setVisibility(i);
  }

  private void updateContent(String paramString1, String paramString2)
  {
    RequestUrl localRequestUrl = this.mService.getRecommendListUrl(paramString1, paramString2);
    this.mResContext.setListUrl(localRequestUrl);
    this.mResController = getResourceController();
    restoreParams();
    this.mAdapter.clearDataSet();
    this.mAdapter.loadMoreData(false);
  }

  protected ResourceAdapter getAdapter()
  {
    return new OnlineResourceAdapter(this, this.mResContext);
  }

  protected int getContentView()
  {
    return 100859928;
  }

  protected View getHeaderView()
  {
    UnevenGrid localUnevenGrid;
    if (this.mIsRecommendList)
    {
      TextView localTextView;
      if (this.mHasSubRecommends)
      {
        localTextView = null;
      }
      else
      {
        this.mTextView = new TextView(getActivity());
        this.mTextView.setGravity(19);
        this.mTextView.setBackgroundResource(100794766);
        localTextView = this.mTextView;
      }
    }
    else
    {
      this.mUnevenGrid = new UnevenGrid(getActivity());
      this.mUnevenGrid.setGridItemFactory(new RecommendGridItemFactory(getActivity(), this.mResContext));
      this.mUnevenGrid.setBackgroundResource(100794766);
      int i = ResourceHelper.getThumbnailGap(this.mActivity);
      int j = i * 2;
      this.mUnevenGrid.setPadding(this.mUnevenGrid.getPaddingLeft(), i, this.mUnevenGrid.getPaddingRight(), j);
      this.mUnevenGrid.setGridItemRatio(218, 132);
      this.mUnevenGrid.setGridItemGap(i);
      this.mUnevenGrid.setColumnCount(2);
      Point localPoint = new Point();
      getActivity().getWindowManager().getDefaultDisplay().getSize(localPoint);
      i = (localPoint.x - this.mUnevenGrid.getPaddingLeft() - this.mUnevenGrid.getPaddingRight() - i * 1) / 2;
      this.mResContext.setRecommendImageWidth(i);
      localUnevenGrid = this.mUnevenGrid;
    }
    return localUnevenGrid;
  }

  protected int getSourceType()
  {
    return 2;
  }

  protected void initParams()
  {
    super.initParams();
    this.mService = new OnlineService(this.mResContext);
    this.mIsRecommendList = this.mActivity.getIntent().getBooleanExtra("REQUEST_IS_RECOMMEND_LIST", false);
    this.mSubRecommends = ((List)this.mActivity.getIntent().getSerializableExtra("REQUEST_SUB_RECOMMENDS"));
    boolean bool;
    if ((this.mSubRecommends == null) || (this.mSubRecommends.size() <= 1))
      bool = false;
    else
      bool = true;
    this.mHasSubRecommends = bool;
    if (!this.mIsRecommendList)
    {
      this.mResContext.setListUrl(this.mService.getCommonListUrl(null));
    }
    else
    {
      this.mRecommendId = this.mActivity.getIntent().getStringExtra("REQUEST_RECOMMEND_ID");
      this.mResContext.setListUrl(this.mService.getRecommendListUrl(this.mRecommendId, null));
    }
  }

  protected void initializeDataSet()
  {
    if (Settings.System.getInt(this.mActivity.getContentResolver(), "confirm_miui_disclaimer", 0) != 1)
      startActivityForResult(new Intent("android.intent.action.MIUI_DISCLAIMER"), 1000);
    showSeeMoreTextOnly(false);
    this.mAdapter.loadMoreData(false);
    if ((this.mIsRecommendList) || (!this.mResContext.isRecommendSupported()))
    {
      if (this.mHasSubRecommends)
      {
        if (this.mResContext.isCategorySupported())
          requestCategories();
      }
      else
        requestListMeta(this.mRecommendId);
    }
    else
      requestRecommends();
  }

  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if ((paramInt1 != 1000) || (paramInt2 != 0))
      super.onActivityResult(paramInt1, paramInt2, paramIntent);
    else
      this.mActivity.finish();
  }

  protected void refreshDataSet()
  {
  }

  protected void requestCategories()
  {
    new DownloadCategoryListTask().execute(new Void[0]);
  }

  protected void requestListMeta(String paramString)
  {
    DownloadListMetaDataTask localDownloadListMetaDataTask = new DownloadListMetaDataTask();
    String[] arrayOfString = new String[1];
    arrayOfString[0] = paramString;
    localDownloadListMetaDataTask.execute(arrayOfString);
  }

  protected void requestRecommends()
  {
    new DownloadRecommendListTask().execute(new Void[0]);
  }

  protected void setCategories(List<ResourceCategory> paramList)
  {
    if (paramList != null)
    {
      this.mCategoryAdapter.clear();
      ResourceCategory localResourceCategory = new ResourceCategory();
      localResourceCategory.setName(getString(101449767));
      this.mCategoryAdapter.add(localResourceCategory);
    }
    for (int i = 0; ; i++)
    {
      if (i >= paramList.size())
        return;
      this.mCategoryAdapter.add(paramList.get(i));
    }
  }

  protected void setListMetaData(ResourceListMeta paramResourceListMeta)
  {
    if (paramResourceListMeta == null)
    {
      this.mTextView.setVisibility(8);
    }
    else
    {
      this.mTextView.setText(paramResourceListMeta.getDescription());
      this.mTextView.setVisibility(0);
    }
  }

  protected void setRecommends(List<RecommendItemData> paramList)
  {
    if (paramList == null)
    {
      this.mUnevenGrid.setVisibility(8);
    }
    else
    {
      this.mUnevenGrid.updateData(paramList);
      this.mUnevenGrid.setVisibility(0);
    }
  }

  protected void setSubRecommends(List<RecommendItemData> paramList)
  {
    if (paramList != null)
      this.mSubRecommendAdapter.clear();
    for (int i = 0; ; i++)
    {
      if (i >= paramList.size())
        return;
      this.mSubRecommendAdapter.add(paramList.get(i));
    }
  }

  protected void setupUI()
  {
    super.setupUI();
    showSeeMoreTextOnly(true);
    if ((this.mIsRecommendList) && (this.mHasSubRecommends))
    {
      getView().findViewById(101384273).setVisibility(0);
      this.mSubRecommendList = ((Spinner)getView().findViewById(101384320));
      this.mSubRecommendAdapter = new ArrayAdapter(this.mActivity, 17367048);
      this.mSubRecommendAdapter.setDropDownViewResource(17367049);
      this.mSubRecommendList.setAdapter(this.mSubRecommendAdapter);
      this.mSubRecommendList.setVisibility(0);
      this.mSubRecommendList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
      {
        private boolean firstTime = true;

        public void onItemSelected(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
        {
          String str = ((RecommendItemData)OnlineResourceListFragment.this.mSubRecommendAdapter.getItem(paramInt)).itemId;
          Object localObject = null;
          if (OnlineResourceListFragment.this.mCategoryList != null)
          {
            localObject = (ResourceCategory)OnlineResourceListFragment.this.mCategoryList.getSelectedItem();
            if (localObject == null)
              localObject = null;
            else
              localObject = ((ResourceCategory)localObject).getCode();
          }
          if (!this.firstTime)
            OnlineResourceListFragment.this.updateContent(str, (String)localObject);
          else
            this.firstTime = false;
        }

        public void onNothingSelected(AdapterView<?> paramAdapterView)
        {
        }
      });
      setSubRecommends(this.mSubRecommends);
      if (this.mResContext.isCategorySupported())
      {
        Spinner localSpinner = (Spinner)getView().findViewById(101384297);
        this.mCategoryAdapter = new ArrayAdapter(this.mActivity, 17367048);
        this.mCategoryAdapter.setDropDownViewResource(17367049);
        localSpinner.setAdapter(this.mCategoryAdapter);
        localSpinner.setVisibility(0);
        localSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
          private boolean firstTime = true;

          public void onItemSelected(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
          {
            String str = ((ResourceCategory)OnlineResourceListFragment.this.mCategoryAdapter.getItem(paramInt)).getCode();
            Object localObject = null;
            if (OnlineResourceListFragment.this.mSubRecommendList != null)
            {
              localObject = (RecommendItemData)OnlineResourceListFragment.this.mSubRecommendList.getSelectedItem();
              if (localObject == null)
                localObject = null;
              else
                localObject = ((RecommendItemData)localObject).itemId;
            }
            if (!this.firstTime)
              OnlineResourceListFragment.this.updateContent((String)localObject, str);
            else
              this.firstTime = false;
          }

          public void onNothingSelected(AdapterView<?> paramAdapterView)
          {
          }
        });
      }
    }
  }

  protected class DownloadListMetaDataTask extends AsyncTask<String, Void, ResourceListMeta>
  {
    protected DownloadListMetaDataTask()
    {
    }

    protected ResourceListMeta doInBackground(String[] paramArrayOfString)
    {
      return OnlineResourceListFragment.this.mResController.getOnlineDataManager().getResourceListMeta(paramArrayOfString[0]);
    }

    protected void onPostExecute(ResourceListMeta paramResourceListMeta)
    {
      OnlineResourceListFragment.this.setListMetaData(paramResourceListMeta);
    }
  }

  protected class DownloadRecommendListTask extends AsyncTask<Void, Void, List<RecommendItemData>>
  {
    protected DownloadRecommendListTask()
    {
    }

    protected List<RecommendItemData> doInBackground(Void[] paramArrayOfVoid)
    {
      return OnlineResourceListFragment.this.mResController.getOnlineDataManager().getRecommends();
    }

    protected void onPostExecute(List<RecommendItemData> paramList)
    {
      OnlineResourceListFragment.this.setRecommends(paramList);
    }
  }

  protected class DownloadCategoryListTask extends AsyncTask<Void, Void, List<ResourceCategory>>
  {
    protected DownloadCategoryListTask()
    {
    }

    protected List<ResourceCategory> doInBackground(Void[] paramArrayOfVoid)
    {
      return OnlineResourceListFragment.this.mResController.getOnlineDataManager().getResourceCategories();
    }

    protected void onPostExecute(List<ResourceCategory> paramList)
    {
      OnlineResourceListFragment.this.setCategories(paramList);
    }
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.activity.OnlineResourceListFragment
 * JD-Core Version:    0.6.0
 */