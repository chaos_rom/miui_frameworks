package miui.resourcebrowser.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import miui.app.SDCardMonitor;
import miui.app.SDCardMonitor.SDCardStatusListener;
import miui.os.AsyncTaskObserver;
import miui.resourcebrowser.AppInnerContext;
import miui.resourcebrowser.ResourceConstants;
import miui.resourcebrowser.ResourceContext;
import miui.resourcebrowser.controller.DataSetObserver;
import miui.resourcebrowser.controller.LocalDataManager;
import miui.resourcebrowser.controller.OnlineDataManager;
import miui.resourcebrowser.controller.ResourceController;
import miui.resourcebrowser.controller.local.ImportResourceTask;
import miui.resourcebrowser.model.Resource;
import miui.resourcebrowser.util.AudioBatchResourceHandler;
import miui.resourcebrowser.util.BatchResourceHandler;
import miui.resourcebrowser.util.ResourceHelper;

public abstract class ResourceListFragment extends BaseFragment
  implements AsyncTaskObserver<Void, Resource, List<Resource>>, DataSetObserver, SDCardMonitor.SDCardStatusListener, ResourceConstants
{
  private static boolean mIsNeedCheckImportResources = false;
  protected Activity mActivity;
  protected ResourceAdapter mAdapter;
  protected AppInnerContext mAppContext;
  protected BatchResourceHandler mBatchHandler;
  private boolean mFirstTimeResume = true;
  private boolean mFirstTimeVisible = true;
  protected Handler mHandler;
  protected ListView mListView;
  protected View mProgressBar;
  protected ProgressDialog mProgressDialog;
  protected ResourceContext mResContext;
  protected ResourceController mResController;
  protected SDCardMonitor mSDCardMonitor;
  protected PowerManager.WakeLock mWakeLock;

  private int computeProgressPercentage(int paramInt1, int paramInt2)
  {
    int i;
    if (paramInt2 != 0)
      i = paramInt1 * 100 / paramInt2;
    else
      i = 0;
    return i;
  }

  private void createIsImportedFile(File paramFile)
  {
    paramFile.getParentFile().mkdirs();
    try
    {
      paramFile.createNewFile();
      return;
    }
    catch (IOException localIOException)
    {
      while (true)
        localIOException.printStackTrace();
    }
  }

  private void startImportResourceTask(File paramFile)
  {
    new Handler().postDelayed(new Runnable(paramFile)
    {
      public void run()
      {
        1 local1 = new ImportResourceTask(ResourceListFragment.this.mResContext, "recovery-" + ResourceListFragment.this.mResContext.getResourceCode())
        {
          protected void onPostExecute(Integer paramInteger)
          {
            if ((paramInteger != null) && ((paramInteger.intValue() > 0) || (paramInteger.intValue() == -1)))
            {
              ResourceListFragment.this.mAdapter.notifyDataSetChanged();
              ResourceListFragment.this.mAdapter.loadData();
            }
            if (ResourceListFragment.this.mWakeLock.isHeld())
              ResourceListFragment.this.mWakeLock.release();
            ResourceListFragment.this.mProgressBar.setVisibility(8);
            if (ResourceListFragment.this.mProgressDialog.isShowing())
              ResourceListFragment.this.mProgressDialog.dismiss();
            ResourceListFragment.this.createIsImportedFile(ResourceListFragment.1.this.val$isResImportedFile);
            int i = getImportTotalNum();
            if (i > 0)
            {
              AlertDialog.Builder localBuilder = new AlertDialog.Builder(ResourceListFragment.this.mActivity);
              localBuilder.setTitle(ResourceListFragment.this.mActivity.getString(101450314));
              Activity localActivity = ResourceListFragment.this.mActivity;
              Object localObject = new Object[1];
              localObject[0] = paramInteger;
              localObject = localActivity.getString(101450315, localObject);
              if (i - paramInteger.intValue() > 0)
              {
                StringBuilder localStringBuilder = new StringBuilder().append((String)localObject);
                localActivity = ResourceListFragment.this.mActivity;
                localObject = new Object[1];
                localObject[0] = Integer.valueOf(i - paramInteger.intValue());
                localObject = localActivity.getString(101450316, localObject);
              }
              localBuilder.setMessage((CharSequence)localObject);
              localBuilder.setNegativeButton(ResourceListFragment.this.mActivity.getString(17039379), new DialogInterface.OnClickListener()
              {
                public void onClick(DialogInterface paramDialogInterface, int paramInt)
                {
                  paramDialogInterface.dismiss();
                }
              });
              localBuilder.create().show();
            }
          }

          protected void onPreExecute()
          {
            ResourceListFragment.this.mWakeLock = ((PowerManager)ResourceListFragment.this.mActivity.getSystemService("power")).newWakeLock(268435462, "Resource Import Tag");
            ResourceListFragment.this.mWakeLock.acquire();
          }

          protected void onProgressUpdate(Integer[] paramArrayOfInteger)
          {
            ProgressDialog localProgressDialog = ResourceListFragment.this.mProgressDialog;
            Activity localActivity = ResourceListFragment.this.mActivity;
            Object[] arrayOfObject = new Object[2];
            arrayOfObject[0] = Integer.valueOf(paramArrayOfInteger[0].intValue());
            arrayOfObject[1] = Integer.valueOf(paramArrayOfInteger[1].intValue());
            localProgressDialog.setMessage(localActivity.getString(101450313, arrayOfObject));
            ResourceListFragment.this.mProgressDialog.setProgress(ResourceListFragment.this.computeProgressPercentage(paramArrayOfInteger[0].intValue(), paramArrayOfInteger[1].intValue()));
          }
        };
        String[] arrayOfString = new String[1];
        arrayOfString[0] = ImportResourceTask.IMPORT_BY_DOWNLOAD_FOLDER;
        local1.execute(arrayOfString);
      }
    }
    , 800L);
  }

  private void tryImportResources()
  {
    if (!mIsNeedCheckImportResources)
    {
      mIsNeedCheckImportResources = true;
      if (!this.mResContext.isSelfDescribing())
      {
        Object localObject2 = this.mResContext.getAsyncImportFolder();
        Object localObject1 = new File((String)localObject2 + ".import");
        if (!((File)localObject1).exists())
        {
          localObject2 = new File(this.mResContext.getDownloadFolder());
          if ((localObject2 != null) && (((File)localObject2).exists()) && (((File)localObject2).isDirectory()) && (((File)localObject2).list() != null) && (((File)localObject2).list().length != 0))
          {
            localObject2 = new AlertDialog.Builder(this.mActivity);
            ((AlertDialog.Builder)localObject2).setCancelable(false);
            ((AlertDialog.Builder)localObject2).setTitle(this.mActivity.getString(101450298));
            if (((File)localObject1).getParentFile().exists())
              ((AlertDialog.Builder)localObject2).setMessage(this.mActivity.getString(101450300));
            else
              ((AlertDialog.Builder)localObject2).setMessage(this.mActivity.getString(101450299));
            ((AlertDialog.Builder)localObject2).setPositiveButton(this.mActivity.getString(17039379), new DialogInterface.OnClickListener((File)localObject1)
            {
              public void onClick(DialogInterface paramDialogInterface, int paramInt)
              {
                paramDialogInterface.dismiss();
                ResourceListFragment.this.mProgressDialog = new ProgressDialog(ResourceListFragment.this.mActivity);
                ResourceListFragment.this.mProgressDialog.setProgressStyle(1);
                ResourceListFragment.this.mProgressDialog.setTitle(ResourceListFragment.this.mActivity.getString(101450301));
                ResourceListFragment.this.mProgressDialog.setMessage(ResourceListFragment.this.mActivity.getString(101450302));
                ResourceListFragment.this.mProgressDialog.setCancelable(false);
                ResourceListFragment.this.mProgressDialog.setProgressNumberFormat("");
                ResourceListFragment.this.mProgressDialog.setMax(100);
                ResourceListFragment.this.mProgressDialog.show();
                ResourceListFragment.this.mProgressBar.setVisibility(0);
                ResourceListFragment.this.startImportResourceTask(this.val$isResImportedFile);
              }
            });
            ((AlertDialog.Builder)localObject2).setNegativeButton(this.mActivity.getString(17039369), new DialogInterface.OnClickListener()
            {
              public void onClick(DialogInterface paramDialogInterface, int paramInt)
              {
                paramDialogInterface.dismiss();
                ResourceListFragment.access$402(false);
                ResourceListFragment.this.mActivity.finish();
              }
            });
            ((AlertDialog.Builder)localObject2).create().show();
          }
          else
          {
            createIsImportedFile((File)localObject1);
          }
        }
        else if (new File((String)localObject2).list().length > 1)
        {
          localObject1 = new ImportResourceTask(this.mResContext, "recovery-" + this.mResContext.getResourceCode())
          {
            protected void onPostExecute(Integer paramInteger)
            {
              if ((paramInteger != null) && ((paramInteger.intValue() > 0) || (paramInteger.intValue() == -1)))
              {
                ResourceListFragment.this.mAdapter.notifyDataSetChanged();
                ResourceListFragment.this.mAdapter.loadData();
              }
            }
          };
          localObject2 = new String[1];
          localObject2[0] = ImportResourceTask.IMPORT_BY_IMPORT_FOLDER;
          ((4)localObject1).execute(localObject2);
        }
      }
    }
  }

  protected abstract ResourceAdapter getAdapter();

  protected BatchResourceHandler getBatchOperationHandler()
  {
    Object localObject;
    if (this.mResContext.getResourceFormat() != 3)
      localObject = new BatchResourceHandler(this, this.mAdapter, this.mResContext);
    else
      localObject = new AudioBatchResourceHandler(this, this.mAdapter, this.mResContext);
    return (BatchResourceHandler)localObject;
  }

  protected abstract int getContentView();

  protected View getHeaderView()
  {
    return null;
  }

  protected ResourceContext getResourceContext()
  {
    return this.mAppContext.getResourceContext();
  }

  protected ResourceController getResourceController()
  {
    return this.mAppContext.getResourceController();
  }

  protected Pair<String, String> getResourceDetailActivity()
  {
    return new Pair(this.mResContext.getDetailActivityPackage(), this.mResContext.getDetailActivityClass());
  }

  protected abstract int getSourceType();

  protected int getSourceType(int paramInt)
  {
    return getSourceType();
  }

  protected void initParams()
  {
    this.mAdapter = getAdapter();
    this.mAdapter.setResourceController(this.mResController);
    this.mBatchHandler = getBatchOperationHandler();
    this.mBatchHandler.setResourceController(this.mResController);
    this.mBatchHandler.setSourceType(getSourceType());
    addObserver(this.mBatchHandler);
    this.mAdapter.setResourceBatchHandler(this.mBatchHandler);
    this.mHandler = new Handler();
    this.mSDCardMonitor = SDCardMonitor.getSDCardMonitor(this.mActivity);
    this.mSDCardMonitor.addListener(this);
  }

  protected void initializeDataSet()
  {
  }

  public final void onActivityCreated(Bundle paramBundle)
  {
    super.onActivityCreated(paramBundle);
    mIsNeedCheckImportResources = false;
    this.mActivity = getActivity();
    this.mAppContext = AppInnerContext.getInstance();
    this.mResContext = getResourceContext();
    this.mResController = getResourceController();
    this.mResController.getLocalDataManager().addObserver(this);
    this.mResController.getOnlineDataManager().addObserver(this);
    initParams();
    setupUI();
  }

  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if ((this.mResContext.isPicker()) && (paramIntent != null))
    {
      this.mActivity.setResult(paramInt2, paramIntent);
      this.mActivity.finish();
    }
  }

  public void onCancelled()
  {
    this.mProgressBar.setVisibility(8);
  }

  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    return paramLayoutInflater.inflate(getContentView(), null);
  }

  public void onDataSetUpdateFailed()
  {
  }

  public void onDataSetUpdateSuccessful()
  {
    this.mHandler.post(new Runnable()
    {
      public void run()
      {
        ResourceListFragment.this.mAdapter.notifyDataSetChanged();
        ResourceListFragment.this.tryImportResources();
      }
    });
  }

  public void onDestroy()
  {
    if (this.mSDCardMonitor != null)
      this.mSDCardMonitor.removeListener(this);
    this.mResController.getLocalDataManager().removeObserver(this);
    this.mResController.getOnlineDataManager().removeObserver(this);
    super.onDestroy();
  }

  protected void onInvisible()
  {
    this.mAdapter.clean();
  }

  public void onPause()
  {
    this.mAdapter.clean();
    super.onPause();
  }

  public void onPostExecute(List<Resource> paramList)
  {
    this.mProgressBar.setVisibility(8);
  }

  public void onPreExecute()
  {
    this.mProgressBar.setVisibility(0);
  }

  public void onProgressUpdate(Resource[] paramArrayOfResource)
  {
  }

  public void onResume()
  {
    super.onResume();
    if (!this.mFirstTimeResume)
    {
      if (isVisibleForUser())
        restoreParams();
    }
    else
    {
      if (isVisibleForUser())
        onVisibleChanged();
      this.mFirstTimeResume = false;
    }
  }

  public void onStatusChanged(boolean paramBoolean)
  {
    ResourceHelper.exit(this.mActivity);
  }

  protected void onVisible()
  {
    restoreParams();
    if (!this.mFirstTimeVisible)
    {
      refreshDataSet();
    }
    else
    {
      initializeDataSet();
      this.mFirstTimeVisible = false;
    }
  }

  public final void onVisibleChanged()
  {
    super.onVisibleChanged();
    if (!isVisibleForUser())
      onInvisible();
    else
      onVisible();
  }

  protected void refreshDataSet()
  {
  }

  protected void restoreParams()
  {
    this.mAppContext.setResourceContext(this.mResContext);
    this.mAppContext.setResourceController(this.mResController);
    this.mAppContext.setWorkingDataSet(this.mAdapter.getDataSet());
    this.mAdapter.notifyDataSetChanged();
  }

  protected void setupUI()
  {
    this.mListView = ((ListView)getView().findViewById(101384270));
    View localView = getHeaderView();
    if (localView != null)
      this.mListView.addHeaderView(localView);
    this.mListView.setAdapter(this.mAdapter);
    this.mListView.setFastScrollEnabled(true);
    this.mListView.setDividerHeight(0);
    this.mProgressBar = getView().findViewById(101384271);
  }

  public void startDetailActivityForResource(Pair<Integer, Integer> paramPair)
  {
    Intent localIntent = new Intent();
    Pair localPair = getResourceDetailActivity();
    localIntent.setClassName((String)localPair.first, (String)localPair.second);
    localIntent.addFlags(67108864);
    localIntent.putExtra("REQUEST_RES_INDEX", (Serializable)paramPair.first);
    localIntent.putExtra("REQUEST_RES_GROUP", (Serializable)paramPair.second);
    localIntent.putExtra("REQUEST_SOURCE_TYPE", getSourceType(((Integer)paramPair.second).intValue()));
    startActivityForResult(localIntent, 1);
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.activity.ResourceListFragment
 * JD-Core Version:    0.6.0
 */