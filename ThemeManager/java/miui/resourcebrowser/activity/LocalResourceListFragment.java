package miui.resourcebrowser.activity;

import miui.os.ExtraFileUtils;
import miui.resourcebrowser.ResourceContext;

public class LocalResourceListFragment extends ResourceListFragment
{
  protected ResourceAdapter getAdapter()
  {
    return new LocalResourceAdapter(this, this.mResContext);
  }

  protected int getContentView()
  {
    return 100859928;
  }

  protected int getSourceType()
  {
    return 1;
  }

  protected void initParams()
  {
    super.initParams();
    ExtraFileUtils.addNoMedia(this.mResContext.getBaseImageCacheFolder());
  }

  protected void initializeDataSet()
  {
    this.mAdapter.loadData();
  }

  protected void refreshDataSet()
  {
    this.mAdapter.loadData();
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.activity.LocalResourceListFragment
 * JD-Core Version:    0.6.0
 */