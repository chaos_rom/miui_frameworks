package miui.resourcebrowser.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.app.ActionBar;
import android.app.ActionBar.LayoutParams;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import miui.app.ObservableActivity;
import miui.app.SDCardMonitor;
import miui.app.SDCardMonitor.SDCardStatusListener;
import miui.os.UniqueAsyncTask;
import miui.resourcebrowser.AppInnerContext;
import miui.resourcebrowser.ResourceContext;
import miui.resourcebrowser.controller.LocalDataManager;
import miui.resourcebrowser.controller.OnlineDataManager;
import miui.resourcebrowser.controller.ResourceController;
import miui.resourcebrowser.model.PathEntry;
import miui.resourcebrowser.model.Resource;
import miui.resourcebrowser.util.BatchMediaPlayer;
import miui.resourcebrowser.util.BatchMediaPlayer.BatchPlayerListener;
import miui.resourcebrowser.util.ImageCacheDecoder;
import miui.resourcebrowser.util.ImageCacheDecoder.ImageDecodingListener;
import miui.resourcebrowser.util.ResourceHelper;
import miui.resourcebrowser.view.ResourceOperationHandler;
import miui.resourcebrowser.view.ResourceOperationView;
import miui.resourcebrowser.view.ResourceOperationView.ResourceOperationListener;
import miui.resourcebrowser.view.ResourceScreenView;
import miui.resourcebrowser.view.ResourceScreenView.ScreenChangeListener;
import miui.resourcebrowser.widget.DataGroup;

public class ResourceDetailActivity extends ObservableActivity
  implements SDCardMonitor.SDCardStatusListener, ResourceOperationView.ResourceOperationListener
{
  private final String PREVIEW_INDEX = "PREVIEW_INDEX";
  protected ActionBar mActionBar;
  protected AppInnerContext mAppContext;
  private BatchMediaPlayer mBatchPlayer;
  protected ImageView mCoverView;
  protected List<DataGroup<Resource>> mDataSet;
  private boolean mFullScreen;
  private ViewGroup.LayoutParams mFullScreenParams = new ViewGroup.LayoutParams(-1, -1);
  protected DataGroup<Resource> mGroupDataSet;
  protected boolean mHasInfoView;
  private ImageCacheDecoder mImageCacheDecoder;
  private ViewGroup.LayoutParams mImageParams = new ViewGroup.LayoutParams(-1, -1);
  protected FrameLayout mInfo;
  protected boolean mIsOnlineResourceSet;
  protected boolean mIsSingleResourceSet;
  protected View mNextItem;
  private ViewGroup.LayoutParams mNormalParams;
  protected ResourceOperationHandler mOperationHandler;
  protected ResourceOperationView mOperationView;
  private ImageView mPlayButton;
  private ResourceScreenView mPreview;
  private int mPreviewOffset;
  private int mPreviewWidth;
  private List<PathEntry> mPreviews = new ArrayList();
  protected View mPreviousItem;
  protected boolean mReachBottom;
  protected ResourceContext mResContext;
  protected ResourceController mResController;
  protected Resource mResource;
  protected int mResourceGroup;
  protected int mResourceIndex;
  private TextView mRingtoneName;
  private SDCardMonitor mSDCardMonitor;
  private int mScreenViewBackgroudId;
  private boolean mScreenViewNeedBackgroud;
  protected int mSourceType;
  private Set<UniqueAsyncTask<?, ?, ?>> mTaskSet = new HashSet();
  protected ObjectAnimator mToFullScreenModeAnimator;
  protected ObjectAnimator mToNormalModeAnimator;

  private void addImageView(ImageView paramImageView)
  {
    FrameLayout localFrameLayout = new FrameLayout(this);
    localFrameLayout.addView(paramImageView);
    enterNormalMode(localFrameLayout);
    this.mPreview.addView(localFrameLayout, this.mNormalParams);
  }

  private void bindScreenImageView()
  {
    initImageDecoder();
    this.mPreviews.clear();
    if (this.mHasInfoView)
    {
      this.mPreview.addView(this.mInfo, 0);
      this.mPreviews.add(0, null);
      if (this.mPreviewOffset == 0)
        this.mPreviewOffset = 1;
    }
    Object localObject1 = this.mResource.getPreviews();
    Object localObject2;
    if (((List)localObject1).size() != 0)
    {
      localObject1 = localObject1;
    }
    else
    {
      localObject2 = this.mResource.getBuildInPreviews();
      if (localObject2 == null)
        break label212;
      localObject1 = new ArrayList();
      localObject2 = ((List)localObject2).iterator();
    }
    while (true)
    {
      long l;
      if (!((Iterator)localObject2).hasNext())
      {
        l = this.mResource.getUpdatedTime();
        int j = Math.min(15, ((List)localObject1).size());
        for (int i = 0; ; i++)
        {
          if (i >= j)
          {
            this.mPreviews.addAll((Collection)localObject1);
            if (j != 0)
            {
              initImageForScreenView(this.mPreviewOffset);
            }
            else
            {
              localObject1 = new ImageView(this);
              ((ImageView)localObject1).setScaleType(ImageView.ScaleType.FIT_CENTER);
              ((ImageView)localObject1).setImageResource(100794810);
              ((ImageView)localObject1).setLayoutParams(this.mImageParams);
              addImageView((ImageView)localObject1);
              ((ImageView)localObject1).setClickable(false);
            }
            label212: return;
          }
          ImageView localImageView = new ImageView(this);
          localImageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
          localImageView.setImageResource(100794563);
          localImageView.setLayoutParams(this.mImageParams);
          addImageView(localImageView);
          if (this.mScreenViewNeedBackgroud)
          {
            localImageView.setBackgroundResource(this.mScreenViewBackgroudId);
            int k = (int)getResources().getDimension(101318697);
            localImageView.setPadding(k, k, k, k);
          }
          File localFile = new File(((PathEntry)((List)localObject1).get(i)).getLocalPath());
          if ((!localFile.exists()) || (localFile.lastModified() >= l))
            continue;
          localFile.delete();
        }
      }
      ((List)localObject1).add(new PathEntry((String)l.next(), null));
    }
  }

  private void buildModeChangeAnimator()
  {
    Object localObject1 = new float[2];
    localObject1[0] = 1.0F;
    localObject1[1] = 0.7F;
    localObject1 = PropertyValuesHolder.ofFloat("scaleX", localObject1);
    Object localObject2 = new float[2];
    localObject2[0] = 1.0F;
    localObject2[1] = 0.7F;
    localObject2 = PropertyValuesHolder.ofFloat("scaleY", localObject2);
    Object localObject3 = new float[2];
    localObject3[0] = 1.0F;
    localObject3[1] = 0.0F;
    Object localObject4 = PropertyValuesHolder.ofFloat("alpha", localObject3);
    Object localObject5 = this.mCoverView;
    localObject3 = new PropertyValuesHolder[3];
    localObject3[0] = localObject1;
    localObject3[1] = localObject2;
    localObject3[2] = localObject4;
    this.mToNormalModeAnimator = ObjectAnimator.ofPropertyValuesHolder(localObject5, localObject3).setDuration(200L);
    this.mToNormalModeAnimator.addListener(new AnimatorListenerAdapter()
    {
      public void onAnimationEnd(Animator paramAnimator)
      {
        ResourceDetailActivity.this.mCoverView.setVisibility(8);
        ResourceDetailActivity.this.onEndEnterNormalMode();
        super.onAnimationEnd(paramAnimator);
      }

      public void onAnimationStart(Animator paramAnimator)
      {
        Object localObject = ((PathEntry)ResourceDetailActivity.this.mPreviews.get(ResourceDetailActivity.this.mPreview.getCurrentScreenIndex())).getLocalPath();
        localObject = ResourceDetailActivity.this.mImageCacheDecoder.getBitmap((String)localObject);
        ResourceDetailActivity.this.mCoverView.setImageBitmap((Bitmap)localObject);
        ResourceDetailActivity.this.mCoverView.setVisibility(0);
        ResourceDetailActivity.this.onBeginEnterNormalMode();
        super.onAnimationStart(paramAnimator);
      }
    });
    localObject1 = new float[2];
    localObject1[0] = 0.7F;
    localObject1[1] = 1.0F;
    localObject1 = PropertyValuesHolder.ofFloat("scaleX", localObject1);
    localObject2 = new float[2];
    localObject2[0] = 0.7F;
    localObject2[1] = 1.0F;
    localObject2 = PropertyValuesHolder.ofFloat("scaleY", localObject2);
    localObject3 = new float[2];
    localObject3[0] = 0.0F;
    localObject3[1] = 1.0F;
    localObject5 = PropertyValuesHolder.ofFloat("alpha", localObject3);
    localObject4 = this.mCoverView;
    localObject3 = new PropertyValuesHolder[3];
    localObject3[0] = localObject1;
    localObject3[1] = localObject2;
    localObject3[2] = localObject5;
    this.mToFullScreenModeAnimator = ObjectAnimator.ofPropertyValuesHolder(localObject4, localObject3).setDuration(200L);
    this.mToFullScreenModeAnimator.addListener(new AnimatorListenerAdapter()
    {
      public void onAnimationEnd(Animator paramAnimator)
      {
        ResourceDetailActivity.this.mCoverView.setVisibility(8);
        ResourceDetailActivity.this.onEndEnterFullScreenMode();
        super.onAnimationEnd(paramAnimator);
      }

      public void onAnimationStart(Animator paramAnimator)
      {
        Object localObject = ((PathEntry)ResourceDetailActivity.this.mPreviews.get(ResourceDetailActivity.this.mPreview.getCurrentScreenIndex())).getLocalPath();
        localObject = ResourceDetailActivity.this.mImageCacheDecoder.getBitmap((String)localObject);
        ResourceDetailActivity.this.mCoverView.setImageBitmap((Bitmap)localObject);
        ResourceDetailActivity.this.mCoverView.setVisibility(0);
        ResourceDetailActivity.this.onBeginEnterFullScreenMode();
        super.onAnimationStart(paramAnimator);
      }
    });
  }

  private void decodeImageForScreenView(int paramInt)
  {
    if ((paramInt >= 0) && (paramInt < this.mPreviews.size()) && (this.mPreviews.get(paramInt) != null))
    {
      Object localObject = (PathEntry)this.mPreviews.get(paramInt);
      String str1 = ((PathEntry)localObject).getLocalPath();
      String str2 = ((PathEntry)localObject).getOnlinePath();
      localObject = this.mImageCacheDecoder.getBitmap(str1);
      if (localObject == null)
        this.mImageCacheDecoder.decodeImageAsync(str1, str2, paramInt);
      else
        ((ImageView)((ViewGroup)this.mPreview.getScreen(paramInt)).getChildAt(0)).setImageBitmap((Bitmap)localObject);
    }
  }

  private void enterFullScreenMode()
  {
    if ((this.mPreview.getCurrentScreenIndex() != 0) || (!this.mHasInfoView))
      this.mToFullScreenModeAnimator.start();
  }

  private void enterFullScreenMode(View paramView)
  {
    paramView.setLayoutParams(this.mFullScreenParams);
    paramView.setPadding(0, 0, 0, 0);
    paramView.setBackgroundColor(-16777216);
    ImageView localImageView = (ImageView)((ViewGroup)paramView).getChildAt(0);
    localImageView.setBackgroundColor(-16777216);
    localImageView.setAdjustViewBounds(false);
    localImageView.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramView)
      {
        ResourceDetailActivity.this.enterNormalMode();
      }
    });
  }

  private void enterNormalMode()
  {
    this.mToNormalModeAnimator.start();
  }

  private void enterNormalMode(View paramView)
  {
    paramView.setLayoutParams(this.mNormalParams);
    paramView.setPadding(6, 0, 6, 60);
    paramView.setBackgroundResource(0);
    ImageView localImageView = (ImageView)((ViewGroup)paramView).getChildAt(0);
    localImageView.setBackgroundResource(0);
    localImageView.setAdjustViewBounds(true);
    localImageView.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramView)
      {
        ResourceDetailActivity.this.enterFullScreenMode();
      }
    });
  }

  private int getScreenViewIndexForPreviewImage(String paramString)
  {
    for (int i = 0; ; i++)
    {
      if (i >= this.mPreviews.size())
      {
        i = -1;
        break;
      }
      PathEntry localPathEntry = (PathEntry)this.mPreviews.get(i);
      if ((localPathEntry != null) && (TextUtils.equals(localPathEntry.getLocalPath(), paramString)))
        break;
    }
    return i;
  }

  private void initImageDecoder()
  {
    if (this.mImageCacheDecoder != null)
      this.mImageCacheDecoder.clean(true);
    this.mImageCacheDecoder = new ImageCacheDecoder(3);
    this.mImageCacheDecoder.registerListener(new ImageCacheDecoder.ImageDecodingListener()
    {
      public void handleDecodingResult(boolean paramBoolean, String paramString1, String paramString2)
      {
        if (paramBoolean)
        {
          int i = ResourceDetailActivity.this.getScreenViewIndexForPreviewImage(paramString1);
          if ((i >= 0) && (ResourceDetailActivity.this.isVisibleScreen(i)))
            ResourceDetailActivity.this.decodeImageForScreenView(i);
        }
      }

      public void handleDownloadResult(boolean paramBoolean, String paramString1, String paramString2)
      {
        int i = ResourceDetailActivity.this.getScreenViewIndexForPreviewImage(paramString1);
        if ((i >= 0) && (ResourceDetailActivity.this.isVisibleScreen(i)))
          if (!paramBoolean)
            Toast.makeText(ResourceDetailActivity.this, 101449764, 0).show();
          else
            ResourceDetailActivity.this.mImageCacheDecoder.decodeImageAsync(paramString1, paramString2, i);
      }
    });
  }

  private void initImageForScreenView(int paramInt)
  {
    this.mImageCacheDecoder.setCurrentUseBitmapIndex(paramInt);
    decodeImageForScreenView(paramInt + 0);
    decodeImageForScreenView(paramInt + 1);
    decodeImageForScreenView(paramInt - 1);
  }

  private void initPlayer()
  {
    this.mBatchPlayer = new BatchMediaPlayer(this);
    this.mBatchPlayer.setListener(new BatchMediaPlayer.BatchPlayerListener()
    {
      public void finish(boolean paramBoolean)
      {
        ResourceDetailActivity.this.mPlayButton.setImageResource(100794581);
        if (ResourceDetailActivity.this.mRingtoneName != null)
          ResourceDetailActivity.this.mRingtoneName.setText(ResourceDetailActivity.this.getFormatTitleBeforePlayingRingtone());
        if (paramBoolean)
          Toast.makeText(ResourceDetailActivity.this, 101449760, 0).show();
      }

      public void play(String paramString, int paramInt1, int paramInt2)
      {
        if (ResourceDetailActivity.this.mRingtoneName != null)
          ResourceDetailActivity.this.mRingtoneName.setText(ResourceDetailActivity.this.getFormatPlayingRingtoneName(paramString, paramInt1, paramInt2));
      }
    });
    this.mBatchPlayer.setPlayList(getMusicPlayList(this.mResource));
  }

  private boolean isVisibleScreen(int paramInt)
  {
    int i;
    if (Math.abs(paramInt - this.mImageCacheDecoder.getCurrentUseBitmapIndex()) >= 2)
      i = 0;
    else
      i = 1;
    return i;
  }

  private void onBeginEnterFullScreenMode()
  {
    getWindow().addFlags(1024);
    getActionBar().hide();
    this.mPreview.setClickable(false);
  }

  private void onBeginEnterNormalMode()
  {
    getWindow().clearFlags(1024);
    getActionBar().show();
    for (int i = 0; ; i++)
    {
      if (i >= this.mPreview.getScreenCount())
      {
        i = this.mPreview.getCurrentScreenIndex();
        if (this.mHasInfoView)
        {
          this.mPreview.addView(this.mInfo, 0);
          this.mPreviews.add(0, null);
          this.mPreview.setCurrentScreen(i + 1);
        }
        if (this.mActionBar != null)
          ((LinearLayout)findViewById(101384318)).setPadding(0, getActionBar().getHeight(), 0, 0);
        this.mOperationView.setVisibility(0);
        ResourceScreenView localResourceScreenView = this.mPreview;
        if (this.mPreview.getScreenCount() <= 1)
          i = 8;
        else
          i = 0;
        localResourceScreenView.setSeekBarVisibility(i);
        this.mPreview.setBackgroundResource(0);
        this.mPreview.setClickable(false);
        this.mFullScreen = false;
        return;
      }
      enterNormalMode(this.mPreview.getScreen(i));
    }
  }

  private void onEndEnterFullScreenMode()
  {
    int i = this.mPreview.getCurrentScreenIndex();
    if (this.mHasInfoView)
    {
      this.mPreview.removeScreen(0);
      this.mPreviews.remove(0);
    }
    for (int j = 0; ; j++)
    {
      if (j >= this.mPreview.getScreenCount())
      {
        if (this.mHasInfoView)
          this.mPreview.setCurrentScreen(i - 1);
        if (this.mActionBar != null)
          ((LinearLayout)findViewById(101384318)).setPadding(0, 0, 0, 0);
        this.mOperationView.setVisibility(8);
        this.mPreview.setSeekBarVisibility(8);
        this.mPreview.setBackgroundColor(-16777216);
        this.mPreview.requestFocus();
        this.mPreview.setClickable(true);
        this.mFullScreen = true;
        return;
      }
      enterFullScreenMode(this.mPreview.getScreen(j));
    }
  }

  private void onEndEnterNormalMode()
  {
    this.mPreview.requestFocus();
    this.mPreview.setClickable(true);
  }

  private void stopMusic()
  {
    if (this.mBatchPlayer != null)
    {
      this.mBatchPlayer.stop();
      this.mBatchPlayer = null;
    }
  }

  protected void bindScreenRingtoneView()
  {
    View localView = getLayoutInflater().inflate(100859981, null);
    FrameLayout.LayoutParams localLayoutParams = new FrameLayout.LayoutParams(-2, -2, 17);
    this.mPreview.addView(localView, localLayoutParams);
    this.mPlayButton = ((ImageView)localView.findViewById(101384279));
    this.mPlayButton.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramView)
      {
        if (ResourceDetailActivity.this.mBatchPlayer != null)
        {
          if (!ResourceDetailActivity.this.mBatchPlayer.isPlaying())
          {
            ResourceDetailActivity.this.mPlayButton.setImageResource(100794578);
            ResourceDetailActivity.this.mBatchPlayer.start();
          }
          else
          {
            ResourceDetailActivity.this.mPlayButton.setImageResource(100794581);
            ResourceDetailActivity.this.mBatchPlayer.pause();
          }
        }
        else
        {
          ResourceDetailActivity.this.mPlayButton.setImageResource(100794578);
          ResourceDetailActivity.this.initPlayer();
          ResourceDetailActivity.this.mBatchPlayer.start();
        }
      }
    });
    if (TextUtils.isEmpty(this.mResource.getLocalPath()))
      this.mPlayButton.setVisibility(4);
    if (((Boolean)this.mResContext.getExtraMeta("EXTRA_CTX_SHOW_RINGTONE_NAME", Boolean.valueOf(false))).booleanValue())
    {
      this.mRingtoneName = ((TextView)localView.findViewById(101384280));
      this.mRingtoneName.setVisibility(0);
      this.mRingtoneName.setText(getFormatTitleBeforePlayingRingtone());
    }
  }

  protected void bindScreenView()
  {
    this.mPreview.removeAllScreens();
    if (this.mResContext.getResourceFormat() != 3)
    {
      bindScreenImageView();
    }
    else
    {
      bindScreenRingtoneView();
      stopMusic();
    }
    ResourceScreenView localResourceScreenView = this.mPreview;
    int i;
    if (this.mPreview.getScreenCount() <= 1)
      i = 8;
    else
      i = 0;
    localResourceScreenView.setSeekBarVisibility(i);
    this.mPreview.setCurrentScreen(this.mPreviewOffset);
  }

  protected ResourceContext buildResourceContext(ResourceContext paramResourceContext)
  {
    return ResourceHelper.buildResourceContext(paramResourceContext, getIntent(), this);
  }

  protected void changeCurrentResource()
  {
    this.mResource = ((Resource)this.mGroupDataSet.get(this.mResourceIndex));
    requestResourceDetail(this.mResourceIndex);
    setResourceStatus();
    setResourceInfo();
    bindScreenView();
  }

  protected int getContentView()
  {
    return 100859930;
  }

  protected Resource getExternalResource()
  {
    return null;
  }

  protected String getFormatPlayingRingtoneName(String paramString, int paramInt1, int paramInt2)
  {
    return ResourceHelper.getDefaultFormatPlayingRingtoneName(paramString, paramInt1, paramInt2);
  }

  protected String getFormatTitleBeforePlayingRingtone()
  {
    return "";
  }

  protected List<String> getMusicPlayList(Resource paramResource)
  {
    return ResourceHelper.getDefaultMusicPlayList(this, paramResource);
  }

  protected ResourceController getResourceController(ResourceContext paramResourceContext)
  {
    return new ResourceController(paramResourceContext);
  }

  protected ResourceOperationHandler getResourceOperationHandler(ResourceOperationView paramResourceOperationView)
  {
    return new ResourceOperationHandler(this, this.mResContext, paramResourceOperationView);
  }

  protected void initNavigationState()
  {
    this.mPreviousItem.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramView)
      {
        ResourceDetailActivity.this.navigateToPreviousResource();
      }
    });
    this.mNextItem.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramView)
      {
        ResourceDetailActivity.this.navigateToNextResource();
      }
    });
    updateNavigationState();
  }

  protected void initParams()
  {
    int i = 1;
    Intent localIntent = getIntent();
    if (!"android.intent.action.VIEW".equals(localIntent.getAction()))
    {
      this.mDataSet = this.mAppContext.getWorkingDataSet();
      this.mResourceGroup = localIntent.getIntExtra("REQUEST_RES_GROUP", 0);
      this.mResourceIndex = localIntent.getIntExtra("REQUEST_RES_INDEX", 0);
    }
    else
    {
      Resource localResource = getExternalResource();
      if (localResource == null)
        break label291;
      this.mDataSet = new ArrayList();
      DataGroup localDataGroup = new DataGroup();
      localDataGroup.add(localResource);
      this.mDataSet.add(localDataGroup);
      this.mResourceGroup = 0;
      this.mResourceIndex = 0;
    }
    if ((this.mDataSet != null) && (!this.mDataSet.isEmpty()))
    {
      this.mGroupDataSet = ((DataGroup)this.mDataSet.get(this.mResourceGroup));
      if ((this.mGroupDataSet != null) && (!this.mGroupDataSet.isEmpty()))
      {
        this.mSourceType = localIntent.getIntExtra("REQUEST_SOURCE_TYPE", i);
        boolean bool;
        if (this.mSourceType != 2)
          bool = false;
        else
          bool = i;
        this.mIsOnlineResourceSet = bool;
        if ((this.mDataSet.size() != i) || (this.mGroupDataSet.size() != i))
          i = 0;
        this.mIsSingleResourceSet = i;
        this.mSDCardMonitor = SDCardMonitor.getSDCardMonitor(this);
        this.mSDCardMonitor.addListener(this);
        new Handler().postDelayed(new Runnable(localIntent)
        {
          public void run()
          {
            int i = this.val$intent.getIntExtra("android.intent.extra.ringtone.TYPE", -1);
            if (i >= 0)
              ResourceHelper.setMusicVolumeType(ResourceDetailActivity.this, i);
          }
        }
        , 800L);
      }
      else
      {
        finish();
      }
    }
    else
    {
      finish();
      return;
      label291: finish();
    }
  }

  protected void navigateToNextResource()
  {
    int i = this.mGroupDataSet.size();
    if (this.mResourceIndex < i - 1)
    {
      this.mResourceIndex = (1 + this.mResourceIndex);
      changeCurrentResource();
      updateNavigationState();
    }
  }

  protected void navigateToPreviousResource()
  {
    if (this.mResourceIndex > 0)
    {
      this.mResourceIndex = (-1 + this.mResourceIndex);
      changeCurrentResource();
      updateNavigationState();
    }
  }

  public void onApplyEventPerformed()
  {
  }

  public void onBackPressed()
  {
    if (!this.mFullScreen)
      super.onBackPressed();
    else
      enterNormalMode();
  }

  public void onBuyEventPerformed()
  {
  }

  protected final void onCreate(Bundle paramBundle)
  {
    requestWindowFeature(9);
    requestWindowFeature(10);
    super.onCreate(paramBundle);
    setContentView(getContentView());
    this.mAppContext = AppInnerContext.getInstance();
    this.mResContext = this.mAppContext.getResourceContext();
    if (this.mResContext == null)
      this.mResContext = buildResourceContext(new ResourceContext());
    this.mResController = this.mAppContext.getResourceController();
    if (this.mResController == null)
    {
      this.mResController = getResourceController(this.mResContext);
      this.mAppContext.setResourceController(this.mResController);
    }
    if (paramBundle != null)
      this.mPreviewOffset = paramBundle.getInt("PREVIEW_INDEX");
    initParams();
    if (!isFinishing())
    {
      setupUI();
      changeCurrentResource();
    }
  }

  public void onDeleteEventPerformed()
  {
    if (!TextUtils.isEmpty(this.mResource.getLocalPath()))
    {
      String str = this.mResource.getDownloadPath();
      if (!TextUtils.isEmpty(str))
      {
        new File(this.mResContext.getIndexFolder() + this.mResource.getHash()).delete();
        new File(str).delete();
      }
      this.mResController.getLocalDataManager().removeResource(this.mResource);
      if (!this.mIsOnlineResourceSet)
      {
        this.mGroupDataSet.remove(this.mResource);
        finish();
      }
      else
      {
        this.mResource.setStatus(0xFFFFFFFE & this.mResource.getStatus());
      }
    }
  }

  protected void onDestroy()
  {
    if (this.mSDCardMonitor != null)
      this.mSDCardMonitor.removeListener(this);
    if (this.mImageCacheDecoder != null)
      this.mImageCacheDecoder.clean(true);
    Iterator localIterator = this.mTaskSet.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
      {
        this.mTaskSet.clear();
        super.onDestroy();
        return;
      }
      ((UniqueAsyncTask)localIterator.next()).cancel(true);
    }
  }

  public void onDownloadEventPerformed()
  {
  }

  public void onMagicEventPerformed()
  {
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    if (paramMenuItem.getItemId() == 16908332)
      finish();
    return super.onOptionsItemSelected(paramMenuItem);
  }

  protected void onPause()
  {
    stopMusic();
    if (this.mPreview != null)
      this.mPreview.onPause();
    super.onPause();
  }

  public void onPickEventPerformed()
  {
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    if (this.mPreview != null)
      paramBundle.putInt("PREVIEW_INDEX", this.mPreview.getCurrentScreenIndex());
    super.onSaveInstanceState(paramBundle);
  }

  public void onStatusChanged(boolean paramBoolean)
  {
    ResourceHelper.exit(this);
  }

  public void onUpdateEventPerformed()
  {
  }

  public void onWindowFocusChanged(boolean paramBoolean)
  {
    super.onWindowFocusChanged(paramBoolean);
    if ((paramBoolean) && (this.mActionBar != null) && (!this.mFullScreen))
      ((LinearLayout)findViewById(101384318)).setPadding(0, getActionBar().getHeight(), 0, 0);
  }

  protected void requestResourceDetail(int paramInt)
  {
    Resource localResource = (Resource)this.mGroupDataSet.get(paramInt);
    DownloadDetailTask localDownloadDetailTask = new DownloadDetailTask();
    localDownloadDetailTask.setValidIndex(paramInt);
    localDownloadDetailTask.setId("downloadDetail-" + paramInt);
    Resource[] arrayOfResource = new Resource[1];
    arrayOfResource[0] = localResource;
    localDownloadDetailTask.execute(arrayOfResource);
  }

  protected void requestResources(int paramInt)
  {
    DownloadListTask localDownloadListTask = new DownloadListTask();
    localDownloadListTask.setOffset(paramInt);
    localDownloadListTask.setId("downloadList-" + paramInt);
    Integer[] arrayOfInteger = new Integer[1];
    arrayOfInteger[0] = Integer.valueOf(paramInt);
    localDownloadListTask.execute(arrayOfInteger);
  }

  protected void setResourceInfo()
  {
    if (this.mActionBar != null)
      this.mActionBar.setTitle(this.mResource.getTitle());
    String str;
    if (this.mHasInfoView)
    {
      str = this.mResource.getAuthor();
      if (TextUtils.isEmpty(str))
        str = getString(101449740);
      ((TextView)this.mInfo.findViewById(101384275)).setText(str);
      str = this.mResource.getDesigner();
      if (TextUtils.isEmpty(str))
        str = getString(101449740);
      ((TextView)this.mInfo.findViewById(101384276)).setText(str);
      str = DateFormat.getDateInstance().format(new Date(this.mResource.getUpdatedTime()));
      ((TextView)this.mInfo.findViewById(101384277)).setText(str);
      str = this.mResource.getVersion();
      ((TextView)this.mInfo.findViewById(101384278)).setText(str);
      str = "";
    }
    try
    {
      str = ResourceHelper.formatFileSize(this.mResource.getSize());
      str = str;
      label188: ((TextView)this.mInfo.findViewById(101384293)).setText(str);
      str = String.valueOf(this.mResource.getDownloadCount());
      ((TextView)this.mInfo.findViewById(101384292)).setText(str);
      ((TextView)this.mInfo.findViewById(101384352)).setText(this.mResource.getDescription());
      return;
    }
    catch (Exception localException)
    {
      break label188;
    }
  }

  protected void setResourceStatus()
  {
    int i = 0;
    this.mOperationHandler.setResource(this.mResource);
    int j;
    if ((!this.mIsOnlineResourceSet) || (!TextUtils.isEmpty(this.mResource.getProductId())))
      j = 0;
    else
      j = 1;
    ResourceOperationHandler localResourceOperationHandler = this.mOperationHandler;
    if (j == 0)
      i = -1;
    localResourceOperationHandler.updateLoadingState(i, getString(101449762));
  }

  protected void setScreenViewBackground(int paramInt)
  {
    this.mScreenViewNeedBackgroud = true;
    this.mScreenViewBackgroudId = paramInt;
  }

  protected void setupNavigationButton()
  {
    LinearLayout localLinearLayout = new LinearLayout(this);
    localLinearLayout.setOrientation(0);
    localLinearLayout.setPadding(0, 0, (int)TypedValue.applyDimension(1, 10.0F, getResources().getDisplayMetrics()), 0);
    ImageView localImageView = new ImageView(this);
    localImageView.setImageResource(100794568);
    localLinearLayout.addView(localImageView, new ViewGroup.LayoutParams(-2, -2));
    this.mPreviousItem = localImageView;
    localImageView = new ImageView(this);
    localImageView.setImageResource(100794572);
    localLinearLayout.addView(localImageView, new ViewGroup.LayoutParams(-2, -2));
    this.mNextItem = localImageView;
    this.mActionBar.setDisplayShowCustomEnabled(true);
    this.mActionBar.setCustomView(localLinearLayout, new ActionBar.LayoutParams(-2, -2, 21));
    initNavigationState();
  }

  protected void setupUI()
  {
    this.mActionBar = getActionBar();
    if (this.mActionBar != null)
      this.mActionBar.setHomeButtonEnabled(true);
    this.mCoverView = ((ImageView)findViewById(101384319));
    buildModeChangeAnimator();
    this.mPreview = ((ResourceScreenView)findViewById(101384274));
    this.mPreview.setOverScrollRatio(0.2F);
    this.mPreview.setOvershootTension(0.0F);
    this.mPreview.setScreenAlignment(2);
    this.mPreview.setScreenChangeListener(new ResourceScreenView.ScreenChangeListener()
    {
      public void snapToScreen(int paramInt)
      {
        ResourceDetailActivity.this.initImageForScreenView(paramInt);
      }
    });
    Object localObject = new FrameLayout.LayoutParams(-2, -2, 81);
    ((FrameLayout.LayoutParams)localObject).setMargins(0, 0, 0, 30);
    this.mPreview.setSeekBarPosition((FrameLayout.LayoutParams)localObject);
    localObject = new Point();
    getWindowManager().getDefaultDisplay().getSize((Point)localObject);
    this.mPreviewWidth = ResourceHelper.getPreviewWidth(((Point)localObject).x);
    this.mResContext.setPreviewImageWidth(this.mPreviewWidth);
    this.mNormalParams = new ViewGroup.LayoutParams(this.mPreviewWidth, -1);
    boolean bool;
    if ((this.mResContext.getResourceFormat() != 1) && (this.mResContext.getResourceFormat() != 4))
      bool = false;
    else
      bool = true;
    this.mHasInfoView = bool;
    if (this.mHasInfoView)
    {
      View localView = getLayoutInflater().inflate(100859931, null);
      this.mInfo = new FrameLayout(this);
      this.mInfo.setPadding(6, 0, 6, 60);
      this.mInfo.addView(localView, this.mImageParams);
      this.mInfo.setLayoutParams(this.mNormalParams);
    }
    this.mOperationView = ((ResourceOperationView)findViewById(101384281));
    this.mOperationView.setResourceOperationListener(this);
    this.mOperationHandler = getResourceOperationHandler(this.mOperationView);
    this.mOperationHandler.setResourceController(this.mResController);
    addObserver(this.mOperationHandler);
    setupNavigationButton();
  }

  protected void updateNavigationState()
  {
    boolean bool1 = true;
    int i = this.mGroupDataSet.size();
    View localView = this.mPreviousItem;
    boolean bool2;
    if (this.mResourceIndex <= 0)
      bool2 = false;
    else
      bool2 = bool1;
    localView.setEnabled(bool2);
    localView = this.mNextItem;
    if (this.mResourceIndex >= i - 1)
      bool1 = false;
    localView.setEnabled(bool1);
    if ((this.mResourceIndex >= i - 5) && (!this.mReachBottom) && (this.mIsOnlineResourceSet) && (!this.mIsSingleResourceSet))
      requestResources(i);
  }

  protected class DownloadDetailTask extends UniqueAsyncTask<Resource, Void, Resource>
  {
    private int validIndex;

    protected DownloadDetailTask()
    {
    }

    protected Resource doInBackground(Resource[] paramArrayOfResource)
    {
      Resource localResource1 = null;
      Resource localResource2 = paramArrayOfResource[0];
      int i = localResource2.getStatus();
      Object localObject;
      if (localResource2.getOnlineId() == null)
      {
        localObject = new ArrayList();
        ((List)localObject).add(localResource2.getHash());
        localObject = ResourceDetailActivity.this.mResController.getOnlineDataManager().getAssociationResources((List)localObject);
        if ((localObject != null) && (((List)localObject).size() > 0))
        {
          localObject = ((Resource)((List)localObject).get(0)).getOnlineId();
          if ((localObject != null) && (!((String)localObject).equals("")))
          {
            localResource2.setOnlineId((String)localObject);
            localResource2.setStatus(i | 0x2);
          }
        }
      }
      if (!TextUtils.isEmpty(localResource2.getOnlineId()))
      {
        localObject = localResource2.getLocalId();
        String str3 = localResource2.getLocalPath();
        String str1 = localResource2.getFilePath();
        String str2 = localResource2.getHash();
        localResource1 = ResourceDetailActivity.this.mResController.getOnlineDataManager().getResource(localResource2.getOnlineId());
        if (localResource1 != null)
        {
          boolean bool = localResource2.equals(localResource1);
          if (!bool)
            localResource2.updateFrom(localResource1);
          localResource2.setStatus(i | 0x2);
          localResource2.setLocalId((String)localObject);
          localResource2.setLocalPath(str3);
          localResource2.setFilePath(str1);
          localResource2.setHash(str2);
          if ((!bool) && (ResourceHelper.isLocalResource(localResource2.getStatus())))
            ResourceDetailActivity.this.mResController.getLocalDataManager().updateResource(localResource2);
        }
      }
      return (Resource)localResource1;
    }

    protected boolean hasEquivalentRunningTasks()
    {
      return ResourceDetailActivity.this.mTaskSet.contains(this);
    }

    protected void onPostExecute(Resource paramResource)
    {
      if ((!ResourceDetailActivity.this.isFinishing()) && (paramResource != null))
      {
        if (this.validIndex == ResourceDetailActivity.this.mResourceIndex)
        {
          ResourceDetailActivity.this.setResourceStatus();
          ResourceDetailActivity.this.setResourceInfo();
          ResourceDetailActivity.this.bindScreenView();
        }
        ResourceDetailActivity.this.mTaskSet.remove(this);
      }
    }

    protected void onPreExecute()
    {
      super.onPreExecute();
      ResourceDetailActivity.this.mTaskSet.add(this);
    }

    public void setValidIndex(int paramInt)
    {
      this.validIndex = paramInt;
    }
  }

  protected class DownloadListTask extends UniqueAsyncTask<Integer, Void, List<Resource>>
  {
    private int offset = 0;

    protected DownloadListTask()
    {
    }

    protected List<Resource> doInBackground(Integer[] paramArrayOfInteger)
    {
      int i = ResourceDetailActivity.this.mResContext.getPageItemCount();
      return ResourceDetailActivity.this.mResController.getOnlineDataManager().getResources((-1 + (i + paramArrayOfInteger[0].intValue())) / i);
    }

    protected boolean hasEquivalentRunningTasks()
    {
      return ResourceDetailActivity.this.mTaskSet.contains(this);
    }

    protected void onPostExecute(List<Resource> paramList)
    {
      if (!ResourceDetailActivity.this.isFinishing())
      {
        if (paramList != null)
        {
          if (paramList.size() != 0)
          {
            if (this.offset != 0)
            {
              if (this.offset == ResourceDetailActivity.this.mGroupDataSet.size())
                ResourceDetailActivity.this.mGroupDataSet.addAll(paramList);
            }
            else
            {
              ResourceDetailActivity.this.mGroupDataSet.clear();
              ResourceDetailActivity.this.mGroupDataSet.addAll(paramList);
            }
            ResourceDetailActivity.this.updateNavigationState();
          }
          else
          {
            ResourceDetailActivity.this.mReachBottom = true;
          }
        }
        else
        {
          ResourceDetailActivity.this.mReachBottom = true;
          Toast.makeText(ResourceDetailActivity.this, 101449760, 0).show();
        }
        ResourceDetailActivity.this.mTaskSet.remove(this);
      }
    }

    protected void onPreExecute()
    {
      super.onPreExecute();
      ResourceDetailActivity.this.mTaskSet.add(this);
    }

    public void setOffset(int paramInt)
    {
      this.offset = paramInt;
    }
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.activity.ResourceDetailActivity
 * JD-Core Version:    0.6.0
 */