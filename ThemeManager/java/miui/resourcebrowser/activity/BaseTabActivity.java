package miui.resourcebrowser.activity;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.ActionBar.TabListener;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import miui.app.ObservableActivity;

public abstract class BaseTabActivity extends ObservableActivity
  implements ActionBar.TabListener, ViewPager.OnPageChangeListener
{
  protected ActionBar mActionBar;
  protected int mCurrentPagePosition;
  protected List<List<Integer>> mFragmentsMenuId = new ArrayList();
  protected ResourcePagerAdapter mPageAdapter;
  protected List<BaseFragment> mTabFragments = new ArrayList();
  protected ViewPager mViewPager;

  static
  {
    AsyncTask.setDefaultExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
  }

  private void changeFragmentVisibility(int paramInt, boolean paramBoolean)
  {
    BaseFragment localBaseFragment = (BaseFragment)this.mTabFragments.get(paramInt);
    localBaseFragment.setVisibleForUser(paramBoolean);
    localBaseFragment.onVisibleChanged();
  }

  private void createActionBar()
  {
    this.mActionBar = getActionBar();
    this.mActionBar.setNavigationMode(2);
    Iterator localIterator = getActionBarTabs().iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      ActionBar.Tab localTab = (ActionBar.Tab)localIterator.next();
      localTab.setTabListener(this);
      this.mActionBar.addTab(localTab);
    }
  }

  private void createPagerAdapter()
  {
    this.mPageAdapter = getResourcePagerAdapter();
    this.mViewPager.setAdapter(this.mPageAdapter);
    this.mViewPager.setOnPageChangeListener(this);
  }

  private void createTabFragments()
  {
    FragmentManager localFragmentManager = getFragmentManager();
    FragmentTransaction localFragmentTransaction = localFragmentManager.beginTransaction();
    this.mTabFragments.clear();
    for (int i = 0; ; i++)
    {
      if (i >= this.mActionBar.getTabCount())
      {
        localFragmentTransaction.commitAllowingStateLoss();
        localFragmentManager.executePendingTransactions();
        ((BaseFragment)this.mTabFragments.get(0)).setVisibleForUser(true);
        return;
      }
      String str = "tag-" + i;
      Object localObject = localFragmentManager.findFragmentByTag(str);
      if (localObject == null)
      {
        localObject = initTabFragment(i);
        localFragmentTransaction.add(1, (Fragment)localObject, str);
        localFragmentTransaction.hide((Fragment)localObject);
      }
      this.mTabFragments.add((BaseFragment)localObject);
    }
  }

  private void selectTab(int paramInt, boolean paramBoolean)
  {
    if (paramInt != this.mCurrentPagePosition)
    {
      changeFragmentVisibility(this.mCurrentPagePosition, false);
      this.mCurrentPagePosition = paramInt;
      this.mActionBar.setSelectedNavigationItem(paramInt);
      if (paramBoolean)
        this.mViewPager.setCurrentItem(paramInt, true);
      invalidateOptionsMenu();
      changeFragmentVisibility(this.mCurrentPagePosition, true);
    }
  }

  protected abstract List<ActionBar.Tab> getActionBarTabs();

  protected ResourcePagerAdapter getResourcePagerAdapter()
  {
    return new ResourcePagerAdapter();
  }

  protected abstract BaseFragment initTabFragment(int paramInt);

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.mViewPager = new ViewPager(this);
    this.mViewPager.setId(1);
    setContentView(this.mViewPager);
    createPagerAdapter();
    createActionBar();
    createTabFragments();
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    this.mFragmentsMenuId.clear();
    Iterator localIterator = this.mTabFragments.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return super.onCreateOptionsMenu(paramMenu);
      List localList = ((BaseFragment)localIterator.next()).onFragmentCreateOptionsMenu(paramMenu);
      this.mFragmentsMenuId.add(localList);
    }
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    for (int i = 0; ; i++)
    {
      if (i >= this.mFragmentsMenuId.size())
        return super.onOptionsItemSelected(paramMenuItem);
      if (!((List)this.mFragmentsMenuId.get(i)).contains(Integer.valueOf(paramMenuItem.getItemId())))
        continue;
      ((BaseFragment)this.mTabFragments.get(i)).onFragmentOptionsItemSelected(paramMenuItem);
    }
  }

  public void onPageScrollStateChanged(int paramInt)
  {
  }

  public void onPageScrolled(int paramInt1, float paramFloat, int paramInt2)
  {
  }

  public void onPageSelected(int paramInt)
  {
    selectTab(paramInt, false);
  }

  public boolean onPrepareOptionsMenu(Menu paramMenu)
  {
    int i = 0;
    if (i >= this.mTabFragments.size())
      return super.onPrepareOptionsMenu(paramMenu);
    BaseFragment localBaseFragment = (BaseFragment)this.mTabFragments.get(i);
    boolean bool;
    if (this.mCurrentPagePosition != i)
      bool = false;
    else
      bool = true;
    localBaseFragment.onFragmentPrepareOptionsMenu(paramMenu, bool);
    Iterator localIterator;
    if (!bool)
      localIterator = ((List)this.mFragmentsMenuId.get(i)).iterator();
    while (true)
    {
      if (!localIterator.hasNext())
      {
        i++;
        break;
      }
      paramMenu.findItem(((Integer)localIterator.next()).intValue()).setVisible(false);
    }
  }

  public void onTabReselected(ActionBar.Tab paramTab, FragmentTransaction paramFragmentTransaction)
  {
  }

  public void onTabSelected(ActionBar.Tab paramTab, FragmentTransaction paramFragmentTransaction)
  {
    selectTab(paramTab.getPosition(), true);
  }

  public void onTabUnselected(ActionBar.Tab paramTab, FragmentTransaction paramFragmentTransaction)
  {
  }

  protected class ResourcePagerAdapter extends PagerAdapter
  {
    private FragmentTransaction mCurTransaction;
    private FragmentManager mFragmentManager = BaseTabActivity.this.getFragmentManager();

    public ResourcePagerAdapter()
    {
    }

    public void destroyItem(ViewGroup paramViewGroup, int paramInt, Object paramObject)
    {
      if (this.mCurTransaction == null)
        this.mCurTransaction = this.mFragmentManager.beginTransaction();
      this.mCurTransaction.hide((Fragment)paramObject);
    }

    public void finishUpdate(ViewGroup paramViewGroup)
    {
      if (this.mCurTransaction != null)
      {
        this.mCurTransaction.commitAllowingStateLoss();
        this.mCurTransaction = null;
        this.mFragmentManager.executePendingTransactions();
      }
    }

    public int getCount()
    {
      return BaseTabActivity.this.mTabFragments.size();
    }

    public int getItemPosition(Object paramObject)
    {
      return BaseTabActivity.this.mTabFragments.indexOf(paramObject);
    }

    public Object instantiateItem(ViewGroup paramViewGroup, int paramInt)
    {
      if (this.mCurTransaction == null)
        this.mCurTransaction = this.mFragmentManager.beginTransaction();
      Fragment localFragment = (Fragment)BaseTabActivity.this.mTabFragments.get(paramInt);
      this.mCurTransaction.show(localFragment);
      return localFragment;
    }

    public boolean isViewFromObject(View paramView, Object paramObject)
    {
      int i;
      if (((Fragment)paramObject).getView() != paramView)
        i = 0;
      else
        i = 1;
      return i;
    }
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.activity.BaseTabActivity
 * JD-Core Version:    0.6.0
 */