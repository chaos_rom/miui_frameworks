package miui.resourcebrowser.activity;

import android.app.Activity;
import android.content.Intent;
import android.provider.Settings.System;

public class ResourceSearchListFragment extends ResourceListFragment
{
  protected ResourceAdapter getAdapter()
  {
    return new ResourceSearchAdapter(this, this.mResContext);
  }

  protected int getContentView()
  {
    return 100859928;
  }

  protected int getSourceType()
  {
    return 3;
  }

  protected int getSourceType(int paramInt)
  {
    int i;
    switch (paramInt)
    {
    default:
      i = super.getSourceType(paramInt);
      break;
    case 0:
      i = 1;
      break;
    case 1:
      i = 2;
    }
    return i;
  }

  protected void initParams()
  {
    super.initParams();
    String str = this.mActivity.getIntent().getStringExtra("query");
    ((ResourceSearchAdapter)this.mAdapter).setKeyword(str);
  }

  protected void initializeDataSet()
  {
    if (Settings.System.getInt(this.mActivity.getContentResolver(), "confirm_miui_disclaimer", 0) != 1)
      startActivityForResult(new Intent("android.intent.action.MIUI_DISCLAIMER"), 1000);
    this.mAdapter.loadData(0);
    this.mAdapter.loadMoreData(false, 1);
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.activity.ResourceSearchListFragment
 * JD-Core Version:    0.6.0
 */