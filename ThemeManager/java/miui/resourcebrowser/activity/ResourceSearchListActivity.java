package miui.resourcebrowser.activity;

import android.app.ActionBar;
import android.app.ActionBar.LayoutParams;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import miui.app.ObservableActivity;
import miui.resourcebrowser.AppInnerContext;
import miui.resourcebrowser.ResourceContext;
import miui.resourcebrowser.controller.ResourceController;

public class ResourceSearchListActivity extends ObservableActivity
{
  protected BaseFragment mFragment;
  protected ResourceContext mResContext;

  protected BaseFragment getFragment()
  {
    return new ResourceSearchListFragment();
  }

  protected ResourceController getResourceController(ResourceContext paramResourceContext)
  {
    return new ResourceController(paramResourceContext);
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(100859929);
    this.mResContext = ((ResourceContext)getIntent().getBundleExtra("app_data").getSerializable("REQUEST_RES_CONTEXT"));
    Object localObject1 = AppInnerContext.getInstance();
    ((AppInnerContext)localObject1).setResourceContext(this.mResContext);
    Object localObject2 = getResourceController(this.mResContext);
    ((ResourceController)localObject2).setLocalDataManager(((AppInnerContext)localObject1).getResourceController().getLocalDataManager());
    ((ResourceController)localObject2).setImportManager(((AppInnerContext)localObject1).getResourceController().getImportManager());
    ((AppInnerContext)localObject1).setResourceController((ResourceController)localObject2);
    localObject2 = getActionBar();
    localObject1 = new ImageView(this);
    ((ImageView)localObject1).setImageResource(100794808);
    ((ImageView)localObject1).setPadding(0, 0, 18, 0);
    ((ImageView)localObject1).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramView)
      {
        ResourceSearchListActivity.this.onSearchRequested();
      }
    });
    ((ActionBar)localObject2).setDisplayShowCustomEnabled(true);
    ((ActionBar)localObject2).setCustomView((View)localObject1, new ActionBar.LayoutParams(5));
    ((ActionBar)localObject2).setHomeButtonEnabled(true);
    localObject1 = getFragmentManager().beginTransaction();
    this.mFragment = getFragment();
    ((FragmentTransaction)localObject1).add(101384348, this.mFragment);
    ((FragmentTransaction)localObject1).commit();
    this.mFragment.setVisibleForUser(true);
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    if (paramMenuItem.getItemId() == 16908332)
      finish();
    return super.onOptionsItemSelected(paramMenuItem);
  }

  public boolean onSearchRequested()
  {
    Bundle localBundle = new Bundle();
    localBundle.putSerializable("REQUEST_RES_CONTEXT", this.mResContext);
    startSearch(null, false, localBundle, false);
    return true;
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.activity.ResourceSearchListActivity
 * JD-Core Version:    0.6.0
 */