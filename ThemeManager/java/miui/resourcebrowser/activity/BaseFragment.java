package miui.resourcebrowser.activity;

import android.view.Menu;
import android.view.MenuItem;
import java.util.ArrayList;
import java.util.List;
import miui.app.ObservableFragment;

public class BaseFragment extends ObservableFragment
{
  private boolean mVisibleForUser = false;

  public boolean isVisibleForUser()
  {
    return this.mVisibleForUser;
  }

  public List<Integer> onFragmentCreateOptionsMenu(Menu paramMenu)
  {
    return new ArrayList();
  }

  public void onFragmentOptionsItemSelected(MenuItem paramMenuItem)
  {
  }

  public void onFragmentPrepareOptionsMenu(Menu paramMenu, boolean paramBoolean)
  {
  }

  public void onVisibleChanged()
  {
  }

  public void setVisibleForUser(boolean paramBoolean)
  {
    this.mVisibleForUser = paramBoolean;
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.activity.BaseFragment
 * JD-Core Version:    0.6.0
 */