package miui.resourcebrowser;

import android.content.Context;
import java.util.List;
import miui.resourcebrowser.controller.ResourceController;
import miui.resourcebrowser.model.Resource;
import miui.resourcebrowser.widget.DataGroup;

public class AppInnerContext
{
  private static AppInnerContext instance = new AppInnerContext();
  private Context applicationContext;
  private ResourceContext resourceContext;
  private ResourceController resourceController;
  private List<DataGroup<Resource>> workingDataSet;

  public static AppInnerContext getInstance()
  {
    return instance;
  }

  public Context getApplicationContext()
  {
    return this.applicationContext;
  }

  public ResourceContext getResourceContext()
  {
    return this.resourceContext;
  }

  public ResourceController getResourceController()
  {
    return this.resourceController;
  }

  public List<DataGroup<Resource>> getWorkingDataSet()
  {
    return this.workingDataSet;
  }

  public void setApplicationContext(Context paramContext)
  {
    this.applicationContext = paramContext;
  }

  public void setResourceContext(ResourceContext paramResourceContext)
  {
    this.resourceContext = paramResourceContext;
  }

  public void setResourceController(ResourceController paramResourceController)
  {
    this.resourceController = paramResourceController;
  }

  public void setWorkingDataSet(List<DataGroup<Resource>> paramList)
  {
    this.workingDataSet = paramList;
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.AppInnerContext
 * JD-Core Version:    0.6.0
 */