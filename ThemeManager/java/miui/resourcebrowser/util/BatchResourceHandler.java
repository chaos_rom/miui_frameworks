package miui.resourcebrowser.util;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Pair;
import android.view.ActionMode;
import android.view.ActionMode.Callback;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.CheckBox;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import miui.app.FragmentLifecycleObserver;
import miui.resourcebrowser.ResourceContext;
import miui.resourcebrowser.activity.ResourceAdapter;
import miui.resourcebrowser.activity.ResourceListFragment;
import miui.resourcebrowser.controller.LocalDataManager;
import miui.resourcebrowser.controller.ResourceController;
import miui.resourcebrowser.model.Resource;
import miui.resourcebrowser.widget.DataGroup;

public class BatchResourceHandler
  implements FragmentLifecycleObserver, ResourceDownloadHandler.ResourceDownloadListener
{
  private ActionMode mActionMode;
  private ActionMode.Callback mActionModeCallback = new ActionMode.Callback()
  {
    public boolean onActionItemClicked(ActionMode paramActionMode, MenuItem paramMenuItem)
    {
      if (paramMenuItem.getItemId() == 101450102)
      {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(BatchResourceHandler.this.mActivity).setIconAttribute(16843605);
        Activity localActivity = BatchResourceHandler.this.mActivity;
        Object[] arrayOfObject = new Object[1];
        arrayOfObject[0] = Integer.valueOf(BatchResourceHandler.access$200(BatchResourceHandler.this).size());
        localBuilder.setMessage(localActivity.getString(101450167, arrayOfObject)).setNegativeButton(17039360, null).setPositiveButton(17039370, new DialogInterface.OnClickListener()
        {
          public void onClick(DialogInterface paramDialogInterface, int paramInt)
          {
            BatchResourceHandler.this.deleteOrDownloadResources();
          }
        }).show();
      }
      if (paramMenuItem.getItemId() == 101449751)
        BatchResourceHandler.this.deleteOrDownloadResources();
      return true;
    }

    public boolean onCreateActionMode(ActionMode paramActionMode, Menu paramMenu)
    {
      int i = 101449751;
      int j = 100794393;
      if (BatchResourceHandler.this.mIsLocalSource)
      {
        i = 101450102;
        j = 100794793;
      }
      paramMenu.add(0, i, 0, i).setIcon(j);
      View localView = LayoutInflater.from(BatchResourceHandler.this.mActivity).inflate(100859934, null);
      paramActionMode.setCustomView(localView);
      BatchResourceHandler.access$002(BatchResourceHandler.this, (TextView)localView.findViewById(101384194));
      BatchResourceHandler.this.updateSelectedTitle();
      return true;
    }

    public void onDestroyActionMode(ActionMode paramActionMode)
    {
      BatchResourceHandler.this.quitEditMode();
    }

    public boolean onPrepareActionMode(ActionMode paramActionMode, Menu paramMenu)
    {
      return false;
    }
  };
  protected Activity mActivity;
  protected ResourceAdapter mAdapter;
  private Set<Resource> mCheckedResource = new HashSet();
  private Map<String, Integer> mDownloadBytes = new HashMap();
  private ResourceDownloadHandler mDownloadHandler;
  private boolean mEditableMode;
  protected ResourceListFragment mFragment;
  protected boolean mIsLocalSource;
  private View.OnClickListener mItemClickListener = new View.OnClickListener()
  {
    public void onClick(View paramView)
    {
      BatchResourceHandler.this.onClickEventPerformed(paramView);
    }
  };
  private View.OnLongClickListener mItemLongClickListener = new View.OnLongClickListener()
  {
    public boolean onLongClick(View paramView)
    {
      return BatchResourceHandler.this.onLongClickEventPerformed(paramView);
    }
  };
  private long mLastNotifyProgressTime;
  protected boolean mListenDownloadProgress;
  protected ResourceContext mResContext;
  protected ResourceController mResController;
  private TextView mSelectTextView;

  public BatchResourceHandler(ResourceListFragment paramResourceListFragment, ResourceAdapter paramResourceAdapter, ResourceContext paramResourceContext)
  {
    if ((paramResourceListFragment != null) && (paramResourceAdapter != null))
    {
      this.mFragment = paramResourceListFragment;
      this.mActivity = paramResourceListFragment.getActivity();
      this.mAdapter = paramResourceAdapter;
      this.mResContext = paramResourceContext;
      this.mDownloadHandler = new ResourceDownloadHandler(this.mActivity, this.mResContext);
      this.mDownloadHandler.setResourceDownloadListener(this);
      return;
    }
    throw new IllegalArgumentException("BatchResourceOperationHandler() parameters can not be null!");
  }

  private void deleteOrDownloadResources()
  {
    new AsyncTask()
    {
      private ProgressDialog mProgress;

      protected Void doInBackground(Void[] paramArrayOfVoid)
      {
        Resource localResource;
        if (!BatchResourceHandler.this.mIsLocalSource)
        {
          localIterator = BatchResourceHandler.this.mCheckedResource.iterator();
          while (localIterator.hasNext())
          {
            localResource = (Resource)localIterator.next();
            BatchResourceHandler.this.doDownloadResource(localResource);
          }
        }
        Iterator localIterator = ((DataGroup)BatchResourceHandler.this.mAdapter.getDataSet().get(0)).iterator();
        while (true)
        {
          if (!localIterator.hasNext())
            return null;
          localResource = (Resource)localIterator.next();
          if (!BatchResourceHandler.this.mCheckedResource.contains(localResource))
            continue;
          BatchResourceHandler.this.mResController.getLocalDataManager().removeResource(localResource);
        }
      }

      protected void onPostExecute(Void paramVoid)
      {
        BatchResourceHandler.this.quitEditMode();
        this.mProgress.dismiss();
      }

      protected void onPreExecute()
      {
        this.mProgress = new ProgressDialog(BatchResourceHandler.this.mActivity);
        this.mProgress.setProgressStyle(0);
        this.mProgress.setMessage(BatchResourceHandler.this.mActivity.getString(101450168));
        this.mProgress.setCancelable(false);
        this.mProgress.show();
      }
    }
    .execute(new Void[0]);
  }

  private void setCheckBoxState(CheckBox paramCheckBox, boolean paramBoolean1, boolean paramBoolean2)
  {
    if (paramCheckBox != null)
    {
      int i;
      if (!paramBoolean1)
        i = 8;
      else
        i = 0;
      paramCheckBox.setVisibility(i);
      paramCheckBox.setChecked(paramBoolean2);
    }
  }

  private void setViewBatchSelectedState(View paramView)
  {
    int i = 0;
    Pair localPair = (Pair)paramView.getTag();
    if (localPair != null)
    {
      Resource localResource = getResource(localPair);
      boolean bool2;
      if ((!this.mEditableMode) || (!this.mCheckedResource.contains(localResource)))
        bool2 = false;
      else
        bool2 = true;
      boolean bool1;
      if ((!this.mEditableMode) || (!canSelecteResource(localResource)))
        bool1 = false;
      else
        bool1 = true;
      paramView.setSelected(bool2);
      View localView = paramView.findViewById(101384268);
      if (bool1)
        i = 4;
      localView.setVisibility(i);
      setCheckBoxState((CheckBox)paramView.findViewById(16908289), bool1, bool2);
      setDownloadProgress((ProgressBar)paramView.findViewById(101384269), localResource);
    }
  }

  protected boolean canDeleteResource(Resource paramResource)
  {
    String str = paramResource.getLocalPath();
    int i;
    if ((TextUtils.isEmpty(str)) || (ResourceHelper.isSystemResource(str)) || (!new File(str).exists()))
      i = 0;
    else
      i = 1;
    return i;
  }

  protected boolean canDownloadResource(Resource paramResource)
  {
    int i;
    if (((ResourceHelper.isLocalResource(paramResource.getStatus())) && (!ResourceHelper.isOldResource(paramResource.getStatus()))) || (this.mDownloadHandler.isResourceDownloading(paramResource.getOnlineId())))
      i = 0;
    else
      i = 1;
    return i;
  }

  protected boolean canSelecteResource(Resource paramResource)
  {
    int i;
    if (((!this.mIsLocalSource) || (!canDeleteResource(paramResource))) && ((this.mIsLocalSource) || (!canDownloadResource(paramResource))))
      i = 0;
    else
      i = 1;
    return i;
  }

  protected void doCancelDownload(Resource paramResource)
  {
    this.mDownloadHandler.cancelDownload(paramResource.getDownloadPath());
    this.mDownloadBytes.remove(paramResource.getDownloadPath());
    this.mAdapter.notifyDataSetChanged();
  }

  protected void doDownloadResource(Resource paramResource)
  {
    if (this.mDownloadHandler.downloadResource(paramResource, this.mListenDownloadProgress))
      this.mDownloadBytes.put(paramResource.getDownloadPath(), Integer.valueOf(0));
  }

  protected void enterEditMode(View paramView, Pair<Integer, Integer> paramPair)
  {
    this.mEditableMode = true;
    this.mCheckedResource.add(getResource(paramPair));
    setViewBatchSelectedState(paramView);
    this.mActionMode = this.mActivity.startActionMode(this.mActionModeCallback);
  }

  protected Resource getResource(Pair<Integer, Integer> paramPair)
  {
    Resource localResource;
    if (((Integer)paramPair.first).intValue() >= this.mAdapter.getCount())
      localResource = null;
    else
      localResource = (Resource)this.mAdapter.getDataItem(((Integer)paramPair.first).intValue(), ((Integer)paramPair.second).intValue());
    return localResource;
  }

  public View.OnClickListener getResourceClickListener()
  {
    return this.mItemClickListener;
  }

  public void initViewState(View paramView, Pair<Integer, Integer> paramPair)
  {
    paramView.setTag(paramPair);
    paramView.setOnClickListener(this.mItemClickListener);
    paramView.setOnLongClickListener(this.mItemLongClickListener);
    setViewBatchSelectedState(paramView);
  }

  protected boolean isDownloading(Resource paramResource)
  {
    return this.mDownloadBytes.containsKey(paramResource.getDownloadPath());
  }

  public boolean isEditMode()
  {
    return this.mEditableMode;
  }

  public void onActivityCreated(Bundle paramBundle)
  {
  }

  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
  }

  public void onAttach(Activity paramActivity)
  {
  }

  protected void onClickEventPerformed(View paramView)
  {
    int i = 1;
    Object localObject = (Pair)paramView.getTag();
    if (localObject != null)
      if (!this.mEditableMode)
      {
        this.mFragment.startDetailActivityForResource((Pair)localObject);
      }
      else
      {
        localObject = getResource((Pair)localObject);
        if (!canSelecteResource((Resource)localObject))
        {
          localObject = this.mActivity;
          Object[] arrayOfObject = new Object[i];
          String str;
          if (!this.mIsLocalSource)
            str = this.mActivity.getString(101450171);
          else
            str = this.mActivity.getString(101450170);
          arrayOfObject[0] = str;
          localObject = ((Activity)localObject).getString(101450169, arrayOfObject);
          Toast.makeText(this.mActivity, (CharSequence)localObject, 0).show();
        }
        else
        {
          boolean bool;
          if (paramView.isSelected())
            bool = false;
          paramView.setSelected(bool);
          if (!paramView.isSelected())
            this.mCheckedResource.remove(localObject);
          else
            this.mCheckedResource.add(localObject);
          if (!this.mCheckedResource.isEmpty())
          {
            setViewBatchSelectedState(paramView);
            updateSelectedTitle();
          }
          else
          {
            quitEditMode();
          }
        }
      }
  }

  public void onCreate(Bundle paramBundle)
  {
  }

  public void onDestroy()
  {
  }

  public void onDownloadFailed(String paramString1, String paramString2)
  {
    Toast.makeText(this.mActivity, 101449763, 0).show();
    this.mDownloadBytes.remove(paramString1);
  }

  public void onDownloadProgressUpdated(String paramString1, String paramString2, int paramInt1, int paramInt2)
  {
    if (paramInt1 != paramInt2)
    {
      this.mDownloadBytes.put(paramString1, Integer.valueOf(paramInt1));
      if (System.currentTimeMillis() - this.mLastNotifyProgressTime > 600L)
      {
        this.mLastNotifyProgressTime = System.currentTimeMillis();
        this.mAdapter.notifyDataSetChanged();
      }
    }
  }

  public void onDownloadSuccessful(String paramString1, String paramString2)
  {
    this.mAdapter.notifyDataSetChanged();
    this.mAdapter.loadData();
    this.mDownloadBytes.remove(paramString1);
  }

  protected boolean onLongClickEventPerformed(View paramView)
  {
    int i = 0;
    Pair localPair = (Pair)paramView.getTag();
    if (localPair != null)
    {
      int j;
      if (!this.mIsLocalSource)
        j = 1;
      else if (this.mAdapter.loadingData())
        j = 0;
      else
        j = 1;
      if ((j != 0) && (!this.mEditableMode) && (canSelecteResource(getResource(localPair))))
      {
        enterEditMode(paramView, localPair);
        i = 1;
      }
    }
    return i;
  }

  public void onPause()
  {
    this.mDownloadHandler.unregisterDownloadReceiver();
    quitEditMode();
  }

  public void onResume()
  {
    this.mDownloadHandler.registerDownloadReceiver(true);
  }

  public void onStart()
  {
  }

  public void onStop()
  {
  }

  public void quitEditMode()
  {
    if (this.mEditableMode)
    {
      this.mEditableMode = false;
      this.mActionMode.finish();
      this.mActionMode = null;
      this.mCheckedResource.clear();
      this.mAdapter.notifyDataSetChanged();
    }
  }

  protected void setDownloadProgress(ProgressBar paramProgressBar, Resource paramResource)
  {
    int i = 0;
    if (paramProgressBar != null)
    {
      Integer localInteger = (Integer)this.mDownloadBytes.get(paramResource.getDownloadPath());
      if (localInteger == null)
      {
        paramProgressBar.setVisibility(8);
      }
      else
      {
        paramProgressBar.setVisibility(0);
        paramProgressBar.setMax(100);
        if (paramResource.getSize() > 0L)
          i = (int)(100 * localInteger.intValue() / paramResource.getSize());
        paramProgressBar.setProgress(i);
      }
    }
  }

  public void setResourceController(ResourceController paramResourceController)
  {
    this.mResController = paramResourceController;
  }

  public void setSourceType(int paramInt)
  {
    int i = 1;
    if (paramInt != i)
      i = 0;
    this.mIsLocalSource = i;
  }

  protected void updateSelectedTitle()
  {
    Activity localActivity = this.mActivity;
    Object localObject = new Object[1];
    localObject[0] = Integer.valueOf(this.mCheckedResource.size());
    localObject = localActivity.getString(101450166, localObject);
    this.mSelectTextView.setText((CharSequence)localObject);
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.util.BatchResourceHandler
 * JD-Core Version:    0.6.0
 */