package miui.resourcebrowser.util;

import android.app.DownloadManager;
import android.app.DownloadManager.Query;
import android.app.DownloadManager.Request;
import android.app.MiuiDownloadManager;
import android.app.MiuiDownloadManager.Query;
import android.app.MiuiDownloadManager.Request;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.net.WebAddress;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;
import android.webkit.URLUtil;
import java.io.File;
import java.net.URI;
import miui.os.ExtraFileUtils;
import miui.resourcebrowser.ResourceConstants;
import miui.resourcebrowser.ResourceContext;
import miui.resourcebrowser.controller.local.ContextParser;
import miui.resourcebrowser.controller.local.ContextSerializableParser;
import miui.resourcebrowser.controller.local.ImportResourceTask;
import miui.resourcebrowser.controller.local.LocalDataParser;
import miui.resourcebrowser.controller.local.LocalSerializableDataParser;
import miui.resourcebrowser.controller.local.PersistenceException;
import miui.resourcebrowser.model.Resource;

public class ResourceDownloadHandler
  implements ResourceConstants
{
  private Context mContext;
  private ContextParser mContextParser;
  private LocalDataParser mDataParser;
  private DownloadManager mDownloadManager;
  private BroadcastReceiver mDownloadReceiver = new BroadcastReceiver()
  {
    public void onReceive(Context paramContext, Intent paramIntent)
    {
      Object localObject2 = new MiuiDownloadManager.Query();
      long l = paramIntent.getLongExtra("extra_download_id", -1L);
      if (l != -1L)
      {
        Object localObject1 = new long[1];
        localObject1[0] = l;
        ((MiuiDownloadManager.Query)localObject2).setFilterById(localObject1);
        localObject1 = ResourceDownloadHandler.this.mDownloadManager.query((DownloadManager.Query)localObject2);
        if ((localObject1 != null) && (((Cursor)localObject1).moveToFirst()))
        {
          String str2 = Uri.decode(Uri.parse(((Cursor)localObject1).getString(((Cursor)localObject1).getColumnIndex("local_uri"))).getEncodedPath());
          String str1 = ResourceHelper.getFileNameWithoutExtension(str2);
          localObject2 = paramIntent.getAction();
          if (!"android.intent.action.DOWNLOAD_COMPLETE".equals(localObject2))
          {
            if ("android.intent.action.DOWNLOAD_UPDATED".equals(localObject2))
            {
              int i = (int)paramIntent.getLongExtra("extra_download_current_bytes", 0L);
              int j = (int)paramIntent.getLongExtra("extra_download_total_bytes", 1L);
              ResourceDownloadHandler.this.handleDownloadProgressUpdated(str2, str1, i, j);
            }
          }
          else if (!MiuiDownloadManager.isDownloadSuccess((Cursor)localObject1))
          {
            if (paramIntent.getIntExtra("extra_download_status", 16) == 16)
              ResourceDownloadHandler.this.handleDownloadFailed(str2, str1);
          }
          else
            ResourceDownloadHandler.this.handleDownloadSuccessful(str2, str1);
        }
        if (localObject1 != null)
          ((Cursor)localObject1).close();
      }
    }
  };
  private boolean mHasRegistDownloadReceiver = false;
  private String mLastQueryOnlineId;
  private boolean mLastQueryResult;
  private Long mLastQueryTime;
  private ResourceDownloadListener mListener;
  private ResourceContext mResContext;

  public ResourceDownloadHandler(Context paramContext, ResourceContext paramResourceContext)
  {
    this.mContext = paramContext;
    this.mResContext = paramResourceContext;
    this.mDataParser = new LocalSerializableDataParser(paramResourceContext);
    this.mContextParser = new ContextSerializableParser();
    this.mDownloadManager = ((DownloadManager)this.mContext.getSystemService("download"));
  }

  private static String getMimeType(String paramString)
  {
    String str1 = paramString;
    String str2 = "";
    int i = paramString.lastIndexOf('/');
    if (-1 != i)
      str1 = paramString.substring(i + 1);
    i = str1.lastIndexOf('.');
    if (-1 != i)
      str2 = str1.substring(i + 1);
    return MimeTypeMap.getSingleton().getMimeTypeFromExtension(str2);
  }

  private static URI getUriFromUrl(String paramString)
  {
    try
    {
      WebAddress localWebAddress = new WebAddress(new String(URLUtil.decode(paramString.getBytes())));
      String str2 = null;
      String str1 = null;
      localObject = localWebAddress.getPath();
      if (((String)localObject).length() > 0)
      {
        int i = ((String)localObject).lastIndexOf('#');
        if (i != -1)
        {
          str2 = ((String)localObject).substring(i + 1);
          localObject = ((String)localObject).substring(0, i);
        }
        i = ((String)localObject).lastIndexOf('?');
        if (i != -1)
        {
          str1 = ((String)localObject).substring(i + 1);
          localObject = ((String)localObject).substring(0, i);
        }
      }
      localObject = new URI(localWebAddress.getScheme(), localWebAddress.getAuthInfo(), localWebAddress.getHost(), localWebAddress.getPort(), (String)localObject, str1, str2);
      return localObject;
    }
    catch (Exception localException)
    {
      while (true)
        Object localObject = null;
    }
  }

  public boolean cancelDownload(String paramString)
  {
    int i = 1;
    Object localObject = new MiuiDownloadManager.Query();
    ((MiuiDownloadManager.Query)localObject).setFilterByAppData(paramString);
    localObject = this.mDownloadManager.query((DownloadManager.Query)localObject);
    int j = 0;
    int k;
    if (localObject != null)
    {
      if (((Cursor)localObject).moveToFirst())
      {
        long l = ((Cursor)localObject).getLong(((Cursor)localObject).getColumnIndex("_id"));
        DownloadManager localDownloadManager = this.mDownloadManager;
        long[] arrayOfLong = new long[i];
        arrayOfLong[0] = l;
        k = localDownloadManager.remove(arrayOfLong);
      }
      ((Cursor)localObject).close();
    }
    if (k != i)
      i = 0;
    return i;
  }

  public long downloadResource(DownloadManager paramDownloadManager, Resource paramResource)
  {
    String str1 = paramResource.getOnlinePath();
    Object localObject1 = paramResource.getDownloadPath();
    String str2 = paramResource.getTitle();
    long l;
    if (getUriFromUrl(str1) == null)
      l = -1L;
    while (true)
    {
      return l;
      File localFile1;
      File localFile2;
      Object localObject2;
      if (!this.mResContext.isSelfDescribing())
      {
        localFile1 = new File(this.mResContext.getAsyncImportFolder());
        localFile2 = new File(localFile1, paramResource.getOnlineId() + ".mrm");
        localObject2 = new File(localFile1, paramResource.getOnlineId());
      }
      try
      {
        localFile1.mkdirs();
        this.mDataParser.storeResource(localFile2, paramResource);
        this.mContextParser.storeResourceContext((File)localObject2, this.mResContext);
        localFile1 = new File((String)localObject1);
        ExtraFileUtils.mkdirs(localFile1.getParentFile(), 511, -1, -1);
        localObject2 = (String)localObject1 + ".temp";
        new File((String)localObject1).delete();
        new File((String)localObject2).delete();
        localObject1 = new MiuiDownloadManager.Request(Uri.parse(l));
        ((MiuiDownloadManager.Request)localObject1).setNotificationVisibility(0);
        ((MiuiDownloadManager.Request)localObject1).setMimeType(getMimeType(l));
        ((MiuiDownloadManager.Request)localObject1).setTitle(str2);
        ((MiuiDownloadManager.Request)localObject1).setDestinationUri(Uri.fromFile(localFile1));
        ((MiuiDownloadManager.Request)localObject1).setAppointName((String)localObject2);
        ((MiuiDownloadManager.Request)localObject1).setVisibleInDownloadsUi(true);
        ((MiuiDownloadManager.Request)localObject1).setAppData(paramResource.getOnlineId());
        l = paramDownloadManager.enqueue((DownloadManager.Request)localObject1);
      }
      catch (PersistenceException localPersistenceException)
      {
        while (true)
          localPersistenceException.printStackTrace();
      }
    }
  }

  public boolean downloadResource(Resource paramResource)
  {
    return downloadResource(paramResource, false);
  }

  public boolean downloadResource(Resource paramResource, boolean paramBoolean)
  {
    int i = 1;
    long l = downloadResource(this.mDownloadManager, paramResource);
    if ((l >= 0L) && (paramBoolean))
    {
      Context localContext = this.mContext;
      long[] arrayOfLong = new long[i];
      arrayOfLong[0] = l;
      MiuiDownloadManager.operateDownloadsNeedToUpdateProgress(localContext, arrayOfLong, null);
    }
    if (l < 0L)
      i = 0;
    return i;
  }

  public void handleDownloadFailed(String paramString1, String paramString2)
  {
    if (this.mListener != null)
      this.mListener.onDownloadFailed(paramString1, paramString2);
  }

  public void handleDownloadProgressUpdated(String paramString1, String paramString2, int paramInt1, int paramInt2)
  {
    if (this.mListener != null)
      this.mListener.onDownloadProgressUpdated(paramString1, paramString2, paramInt1, paramInt2);
  }

  public void handleDownloadSuccessful(String paramString1, String paramString2)
  {
    2 local2 = new ImportResourceTask(this.mResContext, "import-" + paramString1, paramString1, paramString2)
    {
      protected void onPostExecute(Integer paramInteger)
      {
        if (ResourceDownloadHandler.this.mListener != null)
          if ((paramInteger == null) || ((paramInteger.intValue() <= 0) && (paramInteger.intValue() != -1)))
            ResourceDownloadHandler.this.mListener.onDownloadFailed(this.val$path, this.val$onlineId);
          else
            ResourceDownloadHandler.this.mListener.onDownloadSuccessful(this.val$path, this.val$onlineId);
      }
    };
    String[] arrayOfString = new String[2];
    arrayOfString[0] = ImportResourceTask.IMPORT_BY_ONLINE_ID;
    arrayOfString[1] = paramString2;
    local2.execute(arrayOfString);
  }

  public boolean isResourceDownloading(String paramString)
  {
    int i = 0;
    boolean bool;
    if (!TextUtils.isEmpty(paramString))
      if ((!paramString.equals(this.mLastQueryOnlineId)) || (System.currentTimeMillis() - this.mLastQueryTime.longValue() >= 500L))
      {
        Object localObject1 = new MiuiDownloadManager.Query();
        ((MiuiDownloadManager.Query)localObject1).setFilterByAppData(paramString);
        localObject1 = this.mDownloadManager.query((DownloadManager.Query)localObject1);
        Object localObject2 = 0;
        if (localObject1 != null)
        {
          if ((!((Cursor)localObject1).moveToFirst()) || (!MiuiDownloadManager.isDownloading((Cursor)localObject1)))
            localObject2 = 0;
          else
            localObject2 = 1;
          ((Cursor)localObject1).close();
        }
        this.mLastQueryResult = localObject2;
        this.mLastQueryOnlineId = paramString;
        this.mLastQueryTime = Long.valueOf(System.currentTimeMillis());
        localObject1 = localObject2;
      }
      else
      {
        bool = this.mLastQueryResult;
      }
    return bool;
  }

  public void registerDownloadReceiver()
  {
    registerDownloadReceiver(false);
  }

  public void registerDownloadReceiver(boolean paramBoolean)
  {
    if (!this.mHasRegistDownloadReceiver)
    {
      IntentFilter localIntentFilter = new IntentFilter();
      localIntentFilter.addAction("android.intent.action.DOWNLOAD_COMPLETE");
      if (paramBoolean)
        localIntentFilter.addAction("android.intent.action.DOWNLOAD_UPDATED");
      this.mContext.registerReceiver(this.mDownloadReceiver, localIntentFilter);
      this.mHasRegistDownloadReceiver = true;
    }
  }

  public void setResourceDownloadListener(ResourceDownloadListener paramResourceDownloadListener)
  {
    this.mListener = paramResourceDownloadListener;
  }

  public void unregisterDownloadReceiver()
  {
    if (this.mHasRegistDownloadReceiver)
    {
      this.mContext.unregisterReceiver(this.mDownloadReceiver);
      this.mHasRegistDownloadReceiver = false;
    }
  }

  public static abstract interface ResourceDownloadListener
  {
    public abstract void onDownloadFailed(String paramString1, String paramString2);

    public abstract void onDownloadProgressUpdated(String paramString1, String paramString2, int paramInt1, int paramInt2);

    public abstract void onDownloadSuccessful(String paramString1, String paramString2);
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.util.ResourceDownloadHandler
 * JD-Core Version:    0.6.0
 */