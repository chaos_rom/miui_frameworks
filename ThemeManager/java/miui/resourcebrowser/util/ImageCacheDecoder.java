package miui.resourcebrowser.util;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import miui.resourcebrowser.controller.online.DownloadFileTask;
import miui.resourcebrowser.model.PathEntry;
import miui.util.ImageUtils;
import miui.util.InputStreamLoader;

public class ImageCacheDecoder
{
  protected BitmapCache mBitmapCache;
  private ExecutorService mDecodeExecutorService = Executors.newFixedThreadPool(2);
  protected int mDecodedHeight;
  protected int mDecodedWidth;
  private HashSet<String> mDoingJob = new HashSet();
  private ExecutorService mDownloadExecutorService = Executors.newFixedThreadPool(4);
  private HashMap<String, Long> mFailToDownloadTime = new HashMap();
  private Handler mHandler = new Handler()
  {
    public void handleMessage(Message paramMessage)
    {
      Pair localPair = (Pair)paramMessage.obj;
      boolean bool;
      if (paramMessage.arg1 != 0)
        bool = false;
      else
        bool = true;
      if (paramMessage.what != 0)
      {
        if ((paramMessage.what == 1) && (ImageCacheDecoder.this.mListener != null))
          ImageCacheDecoder.this.mListener.handleDownloadResult(bool, (String)localPair.first, (String)localPair.second);
      }
      else if (ImageCacheDecoder.this.mListener != null)
        ImageCacheDecoder.this.mListener.handleDecodingResult(bool, (String)localPair.first, (String)localPair.second);
    }
  };
  private ImageDecodingListener mListener = null;

  public ImageCacheDecoder()
  {
    this(3);
  }

  public ImageCacheDecoder(int paramInt)
  {
    this.mBitmapCache = new BitmapCache(paramInt);
  }

  public void clean(boolean paramBoolean)
  {
    this.mBitmapCache.clean();
    this.mFailToDownloadTime.clear();
    if (paramBoolean)
    {
      this.mDecodeExecutorService.shutdown();
      this.mDownloadExecutorService.shutdown();
      this.mHandler.removeMessages(0);
      this.mHandler.removeMessages(1);
    }
  }

  public void decodeImageAsync(String paramString1, String paramString2, int paramInt)
  {
    if ((!TextUtils.isEmpty(paramString1)) && (this.mBitmapCache.getBitmap(paramString1) == null) && (!this.mDoingJob.contains(paramString1)) && (!this.mDecodeExecutorService.isShutdown()))
    {
      Long localLong = (Long)this.mFailToDownloadTime.get(paramString2);
      if ((localLong == null) || (System.currentTimeMillis() - localLong.longValue() >= 5000L))
      {
        this.mDoingJob.add(paramString1);
        new JobRun(paramString1, paramString2, paramInt).dispatchJob();
      }
    }
  }

  public Bitmap decodeLocalImage(String paramString, int paramInt, boolean paramBoolean)
  {
    Bitmap localBitmap = getBitmap(paramString);
    if (localBitmap == null)
    {
      localBitmap = ImageUtils.getBitmap(new InputStreamLoader(paramString), this.mDecodedWidth, this.mDecodedHeight, this.mBitmapCache.removeIdleCache(true));
      if ((localBitmap != null) && (paramBoolean))
        this.mBitmapCache.add(paramString, localBitmap, paramInt);
    }
    return localBitmap;
  }

  public Bitmap getBitmap(String paramString)
  {
    return this.mBitmapCache.getBitmap(paramString);
  }

  public int getCurrentUseBitmapIndex()
  {
    return this.mBitmapCache.getCurrentUseIndex();
  }

  public void registerListener(ImageDecodingListener paramImageDecodingListener)
  {
    this.mListener = paramImageDecodingListener;
  }

  public void setCurrentUseBitmapIndex(int paramInt)
  {
    this.mBitmapCache.setCurrentUseIndex(paramInt);
  }

  public void setScaledSize(int paramInt1, int paramInt2)
  {
    this.mDecodedWidth = paramInt1;
    this.mDecodedHeight = paramInt2;
  }

  protected class BitmapCache
  {
    private Map<String, Bitmap> mCache;
    private int mCacheCapacity;
    private int mCurrentIndex;
    ArrayList<String> mIdList;
    ArrayList<Integer> mIndexList;

    public BitmapCache(int arg2)
    {
      int i;
      setCacheSize(Math.max(3, i));
      this.mCache = Collections.synchronizedMap(new HashMap(this.mCacheCapacity));
      this.mIdList = new ArrayList(this.mCacheCapacity);
      this.mIndexList = new ArrayList(this.mCacheCapacity);
    }

    private int getNextRemoveCachePosition(int paramInt)
    {
      int j = this.mIndexList.indexOf(Integer.valueOf(paramInt));
      if ((j < 0) && ((paramInt == this.mCurrentIndex) || (isFull())))
        if (!this.mIndexList.isEmpty())
          j = 0;
        else
          j = -1;
      for (int i = 1; ; i++)
      {
        if (i >= this.mIndexList.size())
          return j;
        if (Math.abs(this.mCurrentIndex - ((Integer)this.mIndexList.get(i)).intValue()) <= Math.abs(this.mCurrentIndex - ((Integer)this.mIndexList.get(j)).intValue()))
          continue;
        j = i;
      }
    }

    public void add(String paramString, Bitmap paramBitmap, int paramInt)
    {
      if (!this.mCache.containsKey(paramString))
      {
        removeIdleCache(false);
        this.mCache.put(paramString, paramBitmap);
        this.mIdList.add(paramString);
        this.mIndexList.add(Integer.valueOf(paramInt));
      }
    }

    public void clean()
    {
      Iterator localIterator = this.mIdList.iterator();
      while (true)
      {
        if (!localIterator.hasNext())
        {
          this.mCache.clear();
          this.mIdList.clear();
          this.mIndexList.clear();
          return;
        }
        Object localObject = (String)localIterator.next();
        localObject = (Bitmap)this.mCache.get(localObject);
        if (localObject == null)
          continue;
        ((Bitmap)localObject).recycle();
      }
    }

    public Bitmap getBitmap(String paramString)
    {
      return (Bitmap)this.mCache.get(paramString);
    }

    public int getCurrentUseIndex()
    {
      return this.mCurrentIndex;
    }

    public boolean inCacheScope(int paramInt)
    {
      int i;
      if (Math.abs(paramInt - this.mCurrentIndex) > this.mCacheCapacity / 2)
        i = 0;
      else
        i = 1;
      return i;
    }

    public boolean isFull()
    {
      int i;
      if (this.mCache.size() < this.mCacheCapacity)
        i = 0;
      else
        i = 1;
      return i;
    }

    public Bitmap removeIdleCache(boolean paramBoolean)
    {
      return removeIdleCache(paramBoolean, -999);
    }

    public Bitmap removeIdleCache(boolean paramBoolean, int paramInt)
    {
      Object localObject = null;
      int i = getNextRemoveCachePosition(paramInt);
      if ((i >= 0) && (i < this.mIndexList.size()))
      {
        Bitmap localBitmap = (Bitmap)this.mCache.remove(this.mIdList.get(i));
        if (localBitmap != null)
          if (!paramBoolean)
            localBitmap.recycle();
          else
            localObject = localBitmap;
        this.mIdList.remove(i);
        this.mIndexList.remove(i);
      }
      return localObject;
    }

    public void setCacheSize(int paramInt)
    {
      if ((paramInt > 1) && (paramInt != this.mCacheCapacity))
      {
        if ((paramInt & 0x1) == 0)
          paramInt++;
        this.mCacheCapacity = paramInt;
      }
    }

    public void setCurrentUseIndex(int paramInt)
    {
      this.mCurrentIndex = paramInt;
    }
  }

  public static abstract interface ImageDecodingListener
  {
    public abstract void handleDecodingResult(boolean paramBoolean, String paramString1, String paramString2);

    public abstract void handleDownloadResult(boolean paramBoolean, String paramString1, String paramString2);
  }

  private class JobRun
    implements Runnable
  {
    private final boolean downloadingJob;
    private String imageLocalPath;
    private String imageOnlinePath;
    private long submitTimeForDebug;
    private int useIndex;

    public JobRun(String paramString1, String paramInt, int arg4)
    {
      this.imageLocalPath = paramString1;
      this.imageOnlinePath = paramInt;
      int i;
      this.useIndex = i;
      boolean bool;
      if (new File(paramString1).exists())
        bool = false;
      else
        bool = true;
      this.downloadingJob = bool;
      if (ResourceDebug.DEBUG)
      {
        this.submitTimeForDebug = System.currentTimeMillis();
        StringBuilder localStringBuilder = new StringBuilder().append("submit loading wallpaper: ").append(i);
        String str;
        if (!this.downloadingJob)
          str = " local " + paramString1;
        else
          str = " online " + paramInt;
        Log.d("Theme", str);
      }
    }

    public void dispatchJob()
    {
      if (!this.downloadingJob)
        ImageCacheDecoder.this.mDecodeExecutorService.submit(this);
      else
        ImageCacheDecoder.this.mDownloadExecutorService.submit(this);
    }

    public void run()
    {
      int j = 0;
      boolean bool;
      if (!ImageCacheDecoder.this.mBitmapCache.inCacheScope(this.useIndex))
      {
        if (ResourceDebug.DEBUG)
        {
          StringBuilder localStringBuilder = new StringBuilder().append("ingore loading wallpaper: ").append(this.useIndex);
          String str;
          if (!this.downloadingJob)
            str = " local";
          else
            str = " online ";
          Log.d("Theme", str);
        }
      }
      else
      {
        long l1;
        if (!ResourceDebug.DEBUG)
          l1 = 0L;
        else
          l1 = System.currentTimeMillis();
        if (!this.downloadingJob)
        {
          if (ImageCacheDecoder.this.decodeLocalImage(this.imageLocalPath, this.useIndex, true) == null)
            j = 0;
          else
            j = 1;
        }
        else if (!TextUtils.isEmpty(this.imageOnlinePath))
        {
          DownloadFileTask localDownloadFileTask = new DownloadFileTask(this.imageOnlinePath);
          Object localObject1 = new PathEntry(this.imageLocalPath, this.imageOnlinePath);
          PathEntry[] arrayOfPathEntry = new PathEntry[1];
          arrayOfPathEntry[0] = localObject1;
          localDownloadFileTask.downloadFiles(arrayOfPathEntry);
          localObject1 = new File(this.imageLocalPath);
          if ((((File)localObject1).exists()) && (((File)localObject1).length() < 1024L))
            ((File)localObject1).delete();
          bool = ((File)localObject1).exists();
          ImageCacheDecoder.this.mFailToDownloadTime.remove(this.imageOnlinePath);
          if (!bool)
            ImageCacheDecoder.this.mFailToDownloadTime.put(this.imageOnlinePath, Long.valueOf(System.currentTimeMillis()));
        }
        else
        {
          bool = false;
        }
        if (ResourceDebug.DEBUG)
        {
          long l2 = System.currentTimeMillis();
          long l3 = new File(this.imageLocalPath).length();
          Object localObject2 = "";
          if (this.downloadingJob)
          {
            localObject2 = new Object[1];
            localObject2[0] = Float.valueOf(0.976F * (float)(l3 / (l2 - l1)));
            localObject2 = String.format("%.1fKB/s", localObject2);
          }
          Object localObject3 = new StringBuilder().append("finish loading wallpaper: ").append(this.useIndex).append(" ");
          if (!bool)
            localObject4 = "fail   ";
          else
            localObject4 = "success";
          Object localObject4 = ((StringBuilder)localObject3).append((String)localObject4).append(" ");
          if (!this.downloadingJob)
            localObject3 = "local ";
          else
            localObject3 = "online";
          Log.d("Theme", (String)localObject3 + " " + ResourceHelper.formatFileSize(l3) + " " + (l2 - this.submitTimeForDebug) + " " + (l2 - l1) + " " + (String)localObject2);
        }
      }
      ImageCacheDecoder.this.mDoingJob.remove(this.imageLocalPath);
      if (ImageCacheDecoder.this.mBitmapCache.inCacheScope(this.useIndex))
      {
        Message localMessage = ImageCacheDecoder.this.mHandler.obtainMessage();
        int i;
        if (!this.downloadingJob)
          i = 0;
        else
          i = 1;
        localMessage.what = i;
        if (!bool)
          i = 1;
        else
          i = 0;
        localMessage.arg1 = i;
        localMessage.obj = new Pair(this.imageLocalPath, this.imageOnlinePath);
        ImageCacheDecoder.this.mHandler.sendMessage(localMessage);
      }
    }
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.util.ImageCacheDecoder
 * JD-Core Version:    0.6.0
 */