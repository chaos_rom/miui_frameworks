package miui.resourcebrowser.util;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Pair;
import android.view.Display;
import android.view.WindowManager;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import miui.os.Environment;
import miui.os.ExtraFileUtils;
import miui.resourcebrowser.ResourceConstants;
import miui.resourcebrowser.ResourceContext;
import miui.resourcebrowser.activity.ResourceDetailActivity;
import miui.resourcebrowser.activity.ResourceRecommendListActivity;
import miui.resourcebrowser.activity.ResourceSearchListActivity;
import miui.resourcebrowser.activity.ResourceTabActivity;
import miui.resourcebrowser.model.PathEntry;
import miui.resourcebrowser.model.Resource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class ResourceHelper
  implements ResourceConstants
{
  private static Map<String, String> mCacheFileNameMap = new LinkedHashMap()
  {
    private static final long serialVersionUID = 1L;

    protected boolean removeEldestEntry(Map.Entry<String, String> paramEntry)
    {
      int i;
      if (size() < 50)
        i = 0;
      else
        i = 1;
      return i;
    }
  };
  protected static HashMap<String, Pair<Long, String>> sFileHashCache = new HashMap();

  public static ResourceContext buildResourceContext(ResourceContext paramResourceContext, Intent paramIntent, Context paramContext)
  {
    Object localObject1 = null;
    int i = 1;
    String str2 = paramIntent.getAction();
    if (!"android.intent.action.RINGTONE_PICKER".equals(str2))
    {
      if (!"android.intent.action.SET_WALLPAPER".equals(str2))
      {
        if (!"android.intent.action.SET_LOCKSCREEN_WALLPAPER".equals(str2))
        {
          if ((!"android.intent.action.PICK_RESOURCE".equals(str2)) && (!"android.intent.action.PICK_GADGET".equals(str2)))
          {
            paramResourceContext.setPicker(false);
          }
          else
          {
            paramResourceContext.setPicker(i);
            paramResourceContext.setCurrentUsingPath(paramIntent.getStringExtra("REQUEST_CURRENT_USING_PATH"));
            paramResourceContext.setTrackId(paramIntent.getStringExtra("REQUEST_TRACK_ID"));
          }
        }
        else
        {
          paramResourceContext.setPicker(false);
          paramResourceContext.setDisplayType(9);
          paramResourceContext.setCategorySupported(i);
          if (paramResourceContext.getSourceFolders() == null)
          {
            localObject1 = new ArrayList();
            ((List)localObject1).add("/system/media/lockscreen/");
            ((List)localObject1).add("/data/media/lockscreen/");
            ((List)localObject1).add(USER_LOCKSCREEN_PATH);
            paramResourceContext.setSourceFolders((List)localObject1);
            paramResourceContext.setDownloadFolder(DOWNLOADED_LOCKSCREEN_PATH);
            paramResourceContext.setMetaFolder(META_LOCKSCREEN_PATH);
            paramResourceContext.setContentFolder(CONTENT_LOCKSCREEN_PATH);
            paramResourceContext.setRightsFolder(RIGHTS_LOCKSCREEN_PATH);
            paramResourceContext.setBuildInImageFolder(BUILDIN_IMAGE_LOCKSCREEN_PATH);
            paramResourceContext.setIndexFolder(INDEX_LOCKSCREEN_PATH);
            paramResourceContext.setAsyncImportFolder(ASYNC_IMPORT_LOCKSCREEN_PATH);
          }
        }
      }
      else
      {
        paramResourceContext.setPicker(false);
        paramResourceContext.setDisplayType(6);
        paramResourceContext.setCategorySupported(i);
        if (paramResourceContext.getSourceFolders() == null)
        {
          localObject1 = new ArrayList();
          ((List)localObject1).add("/system/media/wallpaper/");
          ((List)localObject1).add("/data/media/wallpaper/");
          ((List)localObject1).add(USER_WALLPAPER_PATH);
          paramResourceContext.setSourceFolders((List)localObject1);
          paramResourceContext.setDownloadFolder(DOWNLOADED_WALLPAPER_PATH);
          paramResourceContext.setMetaFolder(META_WALLPAPER_PATH);
          paramResourceContext.setContentFolder(CONTENT_WALLPAPER_PATH);
          paramResourceContext.setRightsFolder(RIGHTS_WALLPAPER_PATH);
          paramResourceContext.setBuildInImageFolder(BUILDIN_IMAGE_WALLPAPER_PATH);
          paramResourceContext.setIndexFolder(INDEX_WALLPAPER_PATH);
          paramResourceContext.setAsyncImportFolder(ASYNC_IMPORT_WALLPAPER_PATH);
        }
      }
    }
    else
    {
      paramResourceContext.setPicker(i);
      paramResourceContext.setDisplayType(4);
      paramResourceContext.setCategorySupported(i);
      paramResourceContext.setResourceTitle(paramIntent.getStringExtra("android.intent.extra.ringtone.TITLE"));
      boolean bool2 = paramIntent.getBooleanExtra("android.intent.extra.ringtone.SHOW_SILENT", i);
      boolean bool1 = paramIntent.getBooleanExtra("android.intent.extra.ringtone.SHOW_DEFAULT", i);
      paramResourceContext.putExtraMeta("android.intent.extra.ringtone.SHOW_SILENT", Boolean.valueOf(bool2));
      paramResourceContext.putExtraMeta("android.intent.extra.ringtone.SHOW_DEFAULT", Boolean.valueOf(bool1));
      Uri localUri = (Uri)paramIntent.getParcelableExtra("android.intent.extra.ringtone.EXISTING_URI");
      Object localObject3 = (Uri)paramIntent.getParcelableExtra("android.intent.extra.ringtone.DEFAULT_URI");
      if (localUri != null)
      {
        boolean bool3;
        if (localUri.equals(localObject3))
          bool3 = false;
        else
          bool3 = i;
        paramResourceContext.setCurrentUsingPath(getPathByUri(paramContext, localUri, bool3));
      }
      if ((bool1) && (localObject3 != null))
        paramResourceContext.putExtraMeta("android.intent.extra.ringtone.DEFAULT_URI", ((Uri)localObject3).toString());
      int k = paramIntent.getIntExtra("android.intent.extra.ringtone.TYPE", 7);
      paramResourceContext.putExtraMeta("android.intent.extra.ringtone.TYPE", Integer.valueOf(k));
      if (paramResourceContext.getSourceFolders() == null)
      {
        localObject2 = new ArrayList();
        String str5;
        String str6;
        String str7;
        String str4;
        switch (k)
        {
        case 3:
        case 5:
        case 6:
        default:
          str5 = null;
          str6 = null;
          str7 = null;
          str3 = null;
          str4 = null;
          localObject3 = null;
          break;
        case 1:
          ((List)localObject2).add(USER_RINGTONE_PATH);
          ((List)localObject2).add("/data/media/audio/ringtones/");
          ((List)localObject2).add("/system/media/audio/ringtones/");
          localObject3 = DOWNLOADED_RINGTONE_PATH;
          str4 = META_RINGTONE_PATH;
          str3 = CONTENT_RINGTONE_PATH;
          str7 = RIGHTS_RINGTONE_PATH;
          str6 = BUILDIN_IMAGE_RINGTONE_PATH;
          localObject1 = INDEX_RINGTONE_PATH;
          str5 = ASYNC_IMPORT_RINGTONE_PATH;
          break;
        case 2:
          ((List)localObject2).add(USER_NOTIFICATION_PATH);
          ((List)localObject2).add("/data/media/audio/notifications/");
          ((List)localObject2).add("/system/media/audio/notifications/");
          localObject3 = DOWNLOADED_NOTIFICATION_PATH;
          str4 = META_NOTIFICATION_PATH;
          str3 = CONTENT_NOTIFICATION_PATH;
          str7 = RIGHTS_NOTIFICATION_PATH;
          str6 = BUILDIN_IMAGE_NOTIFICATION_PATH;
          localObject1 = INDEX_NOTIFICATION_PATH;
          str5 = ASYNC_IMPORT_NOTIFICATION_PATH;
          break;
        case 4:
          ((List)localObject2).add(USER_ALARM_PATH);
          ((List)localObject2).add("/data/media/audio/alarms/");
          ((List)localObject2).add("/system/media/audio/alarms/");
          localObject3 = DOWNLOADED_ALARM_PATH;
          str4 = META_ALARM_PATH;
          str3 = CONTENT_ALARM_PATH;
          str7 = RIGHTS_ALARM_PATH;
          str6 = BUILDIN_IMAGE_ALARM_PATH;
          localObject1 = INDEX_ALARM_PATH;
          str5 = ASYNC_IMPORT_ALARM_PATH;
          break;
        case 7:
          ((List)localObject2).add(USER_RINGTONE_PATH);
          ((List)localObject2).add(USER_NOTIFICATION_PATH);
          ((List)localObject2).add(USER_ALARM_PATH);
          ((List)localObject2).add("/data/media/audio/ringtones/");
          ((List)localObject2).add("/data/media/audio/notifications/");
          ((List)localObject2).add("/data/media/audio/alarms/");
          ((List)localObject2).add("/system/media/audio/ringtones/");
          ((List)localObject2).add("/system/media/audio/notifications/");
          ((List)localObject2).add("/system/media/audio/alarms/");
          localObject3 = DOWNLOADED_RINGTONE_PATH;
          str4 = META_RINGTONE_PATH;
          str3 = CONTENT_RINGTONE_PATH;
          str7 = RIGHTS_RINGTONE_PATH;
          str6 = BUILDIN_IMAGE_RINGTONE_PATH;
          localObject1 = INDEX_RINGTONE_PATH;
          str5 = ASYNC_IMPORT_RINGTONE_PATH;
        }
        paramResourceContext.setSourceFolders((List)localObject2);
        paramResourceContext.setDownloadFolder((String)localObject3);
        paramResourceContext.setMetaFolder(str4);
        paramResourceContext.setContentFolder(str3);
        paramResourceContext.setRightsFolder(str7);
        paramResourceContext.setBuildInImageFolder(str6);
        paramResourceContext.setIndexFolder((String)localObject1);
        paramResourceContext.setAsyncImportFolder(str5);
      }
    }
    if (paramResourceContext.getPageItemCount() == 0)
      paramResourceContext.setPageItemCount(30);
    if (paramResourceContext.getDisplayType() == 0)
      paramResourceContext.setDisplayType(i);
    if (paramResourceContext.getResourceTitle() == null)
      paramResourceContext.setResourceTitle(paramContext.getString(101449768));
    if (paramResourceContext.getResourceFormat() == 0)
      paramResourceContext.setResourceFormat(4);
    if (paramResourceContext.getResourceStamp() == null)
      switch (paramResourceContext.getResourceFormat())
      {
      case 1:
        paramResourceContext.setResourceStamp("BundleUnion");
        break;
      case 2:
        paramResourceContext.setResourceStamp("WallpaperUnion");
        break;
      case 3:
        paramResourceContext.setResourceStamp("RingtoneUnion");
        break;
      case 4:
        paramResourceContext.setResourceStamp("ZipUnion");
        break;
      case 5:
        paramResourceContext.setResourceStamp("OtherUnion");
      }
    if (paramResourceContext.getResourceCode() == null)
      switch (paramResourceContext.getResourceFormat())
      {
      case 1:
        paramResourceContext.setResourceCode("bundle");
        break;
      case 2:
        paramResourceContext.setResourceCode("wallpaper");
        break;
      case 3:
        paramResourceContext.setResourceCode("ringtone");
        break;
      case 4:
        paramResourceContext.setResourceCode("zip");
        break;
      case 5:
        paramResourceContext.setResourceCode("other");
      }
    if (paramResourceContext.getResourceExtension() == null)
      switch (paramResourceContext.getResourceFormat())
      {
      case 1:
        paramResourceContext.setResourceExtension(".zip");
        break;
      case 2:
        paramResourceContext.setResourceExtension(".jpg");
        break;
      case 3:
        paramResourceContext.setResourceExtension(".mp3");
        break;
      case 4:
        paramResourceContext.setResourceExtension(".zip");
        break;
      case 5:
        paramResourceContext.setResourceExtension(".mrf");
      }
    if (paramResourceContext.getBuildInImagePrefixes() == null)
    {
      localObject1 = new String[i];
      localObject1[0] = ("preview_" + paramResourceContext.getResourceCode() + "_");
      paramResourceContext.setBuildInImagePrefixes(Arrays.asList(localObject1));
    }
    Object localObject2 = ExtraFileUtils.standardizeFolderPath(Environment.getMIUIStorageDirectory().getAbsolutePath());
    String str3 = ExtraFileUtils.standardizeFolderPath(paramContext.getCacheDir().getAbsolutePath());
    localObject1 = ExtraFileUtils.standardizeFolderPath(paramResourceContext.getResourceCode());
    int j;
    if (paramResourceContext.getResourceFormat() != i)
      j = 0;
    if (paramResourceContext.getDownloadFolder() == null)
      paramResourceContext.setDownloadFolder((String)localObject2 + (String)localObject1);
    if (paramResourceContext.getBaseDataFolder() == null)
      paramResourceContext.setBaseDataFolder((String)localObject2 + (String)localObject1 + ".db/");
    if (paramResourceContext.getBaseDataCacheFolder() == null)
      paramResourceContext.setBaseDataCacheFolder(str3 + (String)localObject1);
    if (paramResourceContext.getBaseImageCacheFolder() == null)
      paramResourceContext.setBaseImageCacheFolder((String)localObject2 + ".cache/resource/" + (String)localObject1);
    if (paramResourceContext.getSourceFolders() == null)
    {
      localObject2 = new ArrayList();
      if (j == 0)
        str3 = "meta/";
      else
        str3 = "meta/" + (String)localObject1;
      ((List)localObject2).add(paramResourceContext.getBaseDataFolder() + str3);
      paramResourceContext.setSourceFolders((List)localObject2);
    }
    if (paramResourceContext.getMetaFolder() == null)
    {
      if (j == 0)
        localObject2 = "meta/";
      else
        localObject2 = "meta/" + (String)localObject1;
      paramResourceContext.setMetaFolder(paramResourceContext.getBaseDataFolder() + (String)localObject2);
    }
    if (paramResourceContext.getContentFolder() == null)
    {
      if (j == 0)
        localObject2 = "content/";
      else
        localObject2 = "content/" + (String)localObject1;
      paramResourceContext.setContentFolder(paramResourceContext.getBaseDataFolder() + (String)localObject2);
    }
    if (paramResourceContext.getBuildInImageFolder() == null)
    {
      if (j == 0)
        localObject2 = "preview/";
      else
        localObject2 = "preview/" + (String)localObject1;
      paramResourceContext.setBuildInImageFolder(paramResourceContext.getBaseDataFolder() + (String)localObject2);
    }
    if (paramResourceContext.getIndexFolder() == null)
    {
      if (j == 0)
        localObject2 = "index/";
      else
        localObject2 = "index/" + (String)localObject1;
      paramResourceContext.setIndexFolder(paramResourceContext.getBaseDataFolder() + (String)localObject2);
    }
    if (paramResourceContext.getAsyncImportFolder() == null)
    {
      String str1;
      if (j == 0)
        str1 = "download/";
      else
        str1 = "download/" + (String)localObject1;
      paramResourceContext.setAsyncImportFolder(paramResourceContext.getBaseDataFolder() + str1);
    }
    if (paramResourceContext.getListCacheFolder() == null)
      paramResourceContext.setListCacheFolder(paramResourceContext.getBaseDataCacheFolder() + "list/");
    if (paramResourceContext.getDetailCacheFolder() == null)
      paramResourceContext.setDetailCacheFolder(paramResourceContext.getBaseDataCacheFolder() + "detail/");
    if (paramResourceContext.getCategoryCacheFolder() == null)
      paramResourceContext.setCategoryCacheFolder(paramResourceContext.getBaseDataCacheFolder() + "category/");
    if (paramResourceContext.getRecommendCacheFolder() == null)
      paramResourceContext.setRecommendCacheFolder(paramResourceContext.getBaseDataCacheFolder() + "recommend/");
    if (paramResourceContext.getVersionCacheFolder() == null)
      paramResourceContext.setVersionCacheFolder(paramResourceContext.getBaseDataCacheFolder() + "version/");
    if (paramResourceContext.getAssociationCacheFolder() == null)
      paramResourceContext.setAssociationCacheFolder(paramResourceContext.getBaseDataCacheFolder() + "association/");
    if (paramResourceContext.getThumbnailCacheFolder() == null)
      paramResourceContext.setThumbnailCacheFolder(paramResourceContext.getBaseImageCacheFolder() + "thumbnail/");
    if (paramResourceContext.getPreviewCacheFolder() == null)
      paramResourceContext.setPreviewCacheFolder(paramResourceContext.getBaseImageCacheFolder() + "preview/");
    if (paramResourceContext.getRecommendImageCacheFolder() == null)
      paramResourceContext.setRecommendImageCacheFolder(paramResourceContext.getBaseImageCacheFolder() + "recommend/");
    if (paramResourceContext.getTabActivityClass() != null)
    {
      if (paramResourceContext.getTabActivityPackage() == null)
        paramResourceContext.setTabActivityPackage(paramContext.getPackageName());
    }
    else
    {
      paramResourceContext.setTabActivityClass(ResourceTabActivity.class.getName());
      if (paramResourceContext.getTabActivityPackage() == null)
        paramResourceContext.setTabActivityPackage("miui");
    }
    if (paramResourceContext.getSearchActivityClass() != null)
    {
      if (paramResourceContext.getSearchActivityPackage() == null)
        paramResourceContext.setSearchActivityPackage(paramContext.getPackageName());
    }
    else
    {
      paramResourceContext.setSearchActivityClass(ResourceSearchListActivity.class.getName());
      if (paramResourceContext.getSearchActivityPackage() == null)
        paramResourceContext.setSearchActivityPackage("miui");
    }
    if (paramResourceContext.getRecommendActivityClass() != null)
    {
      if (paramResourceContext.getRecommendActivityPackage() == null)
        paramResourceContext.setRecommendActivityPackage(paramContext.getPackageName());
    }
    else
    {
      paramResourceContext.setRecommendActivityClass(ResourceRecommendListActivity.class.getName());
      if (paramResourceContext.getRecommendActivityPackage() == null)
        paramResourceContext.setRecommendActivityPackage("miui");
    }
    if (paramResourceContext.getDetailActivityClass() != null)
    {
      if (paramResourceContext.getDetailActivityPackage() == null)
        paramResourceContext.setDetailActivityPackage(paramContext.getPackageName());
    }
    else
    {
      paramResourceContext.setDetailActivityClass(ResourceDetailActivity.class.getName());
      if (paramResourceContext.getDetailActivityPackage() == null)
        paramResourceContext.setDetailActivityPackage("miui");
    }
    return (ResourceContext)(ResourceContext)(ResourceContext)paramResourceContext;
  }

  public static boolean deleteEmptyFolder(File paramFile)
  {
    boolean bool = false;
    File[] arrayOfFile;
    int j;
    if (!paramFile.isFile())
    {
      arrayOfFile = paramFile.listFiles();
      if (arrayOfFile != null)
        j = arrayOfFile.length;
    }
    for (int i = 0; ; i++)
    {
      if (i >= j)
        bool = paramFile.delete();
      else
        if (deleteEmptyFolder(arrayOfFile[i]))
          continue;
      return bool;
    }
  }

  public static void exit(Activity paramActivity)
  {
    AlertDialog localAlertDialog = new AlertDialog.Builder(paramActivity).setIconAttribute(16843605).setTitle(101449731).setMessage(101449732).setPositiveButton(17039370, null).create();
    localAlertDialog.setOnDismissListener(new DialogInterface.OnDismissListener(paramActivity)
    {
      public void onDismiss(DialogInterface paramDialogInterface)
      {
        ((ActivityManager)ResourceHelper.this.getSystemService("activity")).forceStopPackage(ResourceHelper.this.getPackageName());
      }
    });
    localAlertDialog.show();
  }

  public static String formatDuration(int paramInt)
  {
    int i = paramInt / 1000;
    Object[] arrayOfObject = new Object[2];
    arrayOfObject[0] = Integer.valueOf(i / 60);
    arrayOfObject[1] = Integer.valueOf(i % 60);
    return String.format("%02d:%02d", arrayOfObject);
  }

  public static String formatFileSize(long paramLong)
  {
    Object localObject;
    if (paramLong >= 1048576.0D)
    {
      localObject = new Object[1];
      localObject[0] = Double.valueOf(paramLong / 1048576.0D);
      localObject = String.format("%.1fM", localObject);
    }
    else
    {
      localObject = new Object[1];
      localObject[0] = Double.valueOf(paramLong / 1024.0D);
      localObject = String.format("%.0fK", localObject);
    }
    return (String)localObject;
  }

  public static String getContent(InputStream paramInputStream)
  {
    String str = null;
    ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
    byte[] arrayOfByte = new byte[1024];
    try
    {
      while (true)
      {
        int i = paramInputStream.read(arrayOfByte);
        if (i < 0)
          break;
        localByteArrayOutputStream.write(arrayOfByte, 0, i);
      }
    }
    catch (IOException localIOException)
    {
      localIOException.printStackTrace();
    }
    while (true)
    {
      return str;
      str = new String(localByteArrayOutputStream.toByteArray());
      str = str;
    }
  }

  private static String getContentColumnValue(Context paramContext, Uri paramUri, String paramString)
  {
    String str = null;
    try
    {
      localObject = paramContext.getContentResolver();
      String[] arrayOfString = new String[1];
      arrayOfString[0] = paramString;
      localObject = ((ContentResolver)localObject).query(paramUri, arrayOfString, null, null, null);
      if (localObject != null)
        if (((Cursor)localObject).moveToFirst())
        {
          str = ((Cursor)localObject).getString(0);
          str = str;
        }
    }
    catch (Exception localException3)
    {
      while (true)
      {
        try
        {
          Object localObject;
          ((Cursor)localObject).close();
          if (str != null)
          {
            return str;
            localException1 = localException1;
            localException1.printStackTrace();
            str = str;
            continue;
          }
          str = "";
          continue;
        }
        catch (Exception localException3)
        {
          str = str;
          Exception localException3 = localException2;
          continue;
        }
        str = null;
        continue;
        str = null;
      }
    }
  }

  public static int getDataPerLine(int paramInt)
  {
    int i = 1;
    switch (paramInt)
    {
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
      i = 1;
      break;
    case 6:
    case 7:
    case 8:
      i = 2;
      break;
    case 9:
    case 10:
    case 11:
      i = 3;
    }
    return i;
  }

  public static String getDefaultFormatPlayingRingtoneName(String paramString, int paramInt1, int paramInt2)
  {
    int i = paramString.lastIndexOf('.');
    if (i < 0)
      i = paramString.length();
    String str = paramString.substring(1 + paramString.lastIndexOf(File.separatorChar), i);
    Object[] arrayOfObject = new Object[3];
    arrayOfObject[0] = str;
    arrayOfObject[1] = Integer.valueOf(paramInt1 + 1);
    arrayOfObject[2] = Integer.valueOf(paramInt2);
    return String.format("%s (%d/%d)", arrayOfObject);
  }

  public static List<String> getDefaultMusicPlayList(Context paramContext, Resource paramResource)
  {
    ArrayList localArrayList = new ArrayList();
    String str = getPathByUri(paramContext, getUriByPath(paramResource.getLocalPath()));
    if (!new File(str).exists())
    {
      str = ((PathEntry)paramResource.getThumbnails().get(0)).getOnlinePath();
      if (str != null)
        localArrayList.add(str);
    }
    else
    {
      localArrayList.add(str);
    }
    return localArrayList;
  }

  public static Map<String, String> getDescription(File paramFile)
  {
    Map localMap = null;
    try
    {
      localMap = getDescription(new BufferedInputStream(new FileInputStream(paramFile)));
      localMap = localMap;
      return localMap;
    }
    catch (IOException localIOException)
    {
      while (true)
        localIOException.printStackTrace();
    }
  }

  // ERROR //
  public static Map<String, String> getDescription(InputStream paramInputStream)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_1
    //   2: aconst_null
    //   3: astore_2
    //   4: invokestatic 818	javax/xml/parsers/SAXParserFactory:newInstance	()Ljavax/xml/parsers/SAXParserFactory;
    //   7: invokevirtual 822	javax/xml/parsers/SAXParserFactory:newSAXParser	()Ljavax/xml/parsers/SAXParser;
    //   10: astore_3
    //   11: new 15	miui/resourcebrowser/util/ResourceHelper$DescriptionSAXHandler
    //   14: dup
    //   15: aconst_null
    //   16: invokespecial 825	miui/resourcebrowser/util/ResourceHelper$DescriptionSAXHandler:<init>	(Lmiui/resourcebrowser/util/ResourceHelper$1;)V
    //   19: astore_2
    //   20: aload_3
    //   21: aload_0
    //   22: aload_2
    //   23: invokevirtual 831	javax/xml/parsers/SAXParser:parse	(Ljava/io/InputStream;Lorg/xml/sax/helpers/DefaultHandler;)V
    //   26: aload_0
    //   27: ifnull +83 -> 110
    //   30: aload_0
    //   31: invokevirtual 832	java/io/InputStream:close	()V
    //   34: aload_2
    //   35: astore_2
    //   36: aload_2
    //   37: ifnull +8 -> 45
    //   40: aload_2
    //   41: invokevirtual 836	miui/resourcebrowser/util/ResourceHelper$DescriptionSAXHandler:getElementEntries	()Ljava/util/HashMap;
    //   44: astore_1
    //   45: aload_1
    //   46: areturn
    //   47: astore_3
    //   48: aload_3
    //   49: invokevirtual 713	java/io/IOException:printStackTrace	()V
    //   52: aload_2
    //   53: astore_2
    //   54: goto -18 -> 36
    //   57: astore_3
    //   58: aload_3
    //   59: invokevirtual 744	java/lang/Exception:printStackTrace	()V
    //   62: aload_0
    //   63: ifnull -27 -> 36
    //   66: aload_0
    //   67: invokevirtual 832	java/io/InputStream:close	()V
    //   70: goto -34 -> 36
    //   73: astore_3
    //   74: aload_3
    //   75: invokevirtual 713	java/io/IOException:printStackTrace	()V
    //   78: goto -42 -> 36
    //   81: astore_2
    //   82: aload_0
    //   83: ifnull +7 -> 90
    //   86: aload_0
    //   87: invokevirtual 832	java/io/InputStream:close	()V
    //   90: aload_2
    //   91: athrow
    //   92: astore_1
    //   93: aload_1
    //   94: invokevirtual 713	java/io/IOException:printStackTrace	()V
    //   97: goto -7 -> 90
    //   100: astore_2
    //   101: goto -19 -> 82
    //   104: astore_3
    //   105: aload_2
    //   106: astore_2
    //   107: goto -49 -> 58
    //   110: aload_2
    //   111: astore_2
    //   112: goto -76 -> 36
    //
    // Exception table:
    //   from	to	target	type
    //   30	34	47	java/io/IOException
    //   4	20	57	java/lang/Exception
    //   66	70	73	java/io/IOException
    //   4	20	81	finally
    //   58	62	81	finally
    //   86	90	92	java/io/IOException
    //   20	26	100	finally
    //   20	26	104	java/lang/Exception
  }

  public static Map<String, String> getDescription(ZipFile paramZipFile, String paramString)
  {
    Object localObject = null;
    try
    {
      Map localMap = getDescription(paramZipFile.getInputStream(paramZipFile.getEntry(paramString)));
      localObject = localMap;
      return localObject;
    }
    catch (IOException localIOException)
    {
      while (true)
        localIOException.printStackTrace();
    }
  }

  public static String getFileHash(String paramString)
  {
    long l = new File(paramString).lastModified();
    Pair localPair = (Pair)sFileHashCache.get(paramString);
    if ((localPair == null) || (l != ((Long)localPair.first).longValue()))
    {
      localPair = new Pair(Long.valueOf(l), getFileHashInner(paramString));
      sFileHashCache.put(paramString, localPair);
    }
    return (String)localPair.second;
  }

  // ERROR //
  private static String getFileHashInner(String paramString)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_3
    //   2: invokestatic 891	java/lang/System:currentTimeMillis	()J
    //   5: lstore_1
    //   6: ldc_w 893
    //   9: aload_0
    //   10: invokestatic 899	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
    //   13: pop
    //   14: new 801	java/io/BufferedInputStream
    //   17: dup
    //   18: new 803	java/io/FileInputStream
    //   21: dup
    //   22: aload_0
    //   23: invokespecial 900	java/io/FileInputStream:<init>	(Ljava/lang/String;)V
    //   26: invokespecial 809	java/io/BufferedInputStream:<init>	(Ljava/io/InputStream;)V
    //   29: astore 4
    //   31: ldc_w 902
    //   34: invokestatic 908	java/security/MessageDigest:getInstance	(Ljava/lang/String;)Ljava/security/MessageDigest;
    //   37: astore 7
    //   39: sipush 8192
    //   42: newarray byte
    //   44: astore 5
    //   46: aload 4
    //   48: aload 5
    //   50: invokevirtual 909	java/io/BufferedInputStream:read	([B)I
    //   53: istore 6
    //   55: iload 6
    //   57: ifle +64 -> 121
    //   60: aload 7
    //   62: aload 5
    //   64: iconst_0
    //   65: iload 6
    //   67: invokevirtual 912	java/security/MessageDigest:update	([BII)V
    //   70: goto -24 -> 46
    //   73: astore 5
    //   75: aload 5
    //   77: invokevirtual 913	java/security/NoSuchAlgorithmException:printStackTrace	()V
    //   80: aload 4
    //   82: ifnull +8 -> 90
    //   85: aload 4
    //   87: invokevirtual 914	java/io/BufferedInputStream:close	()V
    //   90: ldc_w 893
    //   93: new 394	java/lang/StringBuilder
    //   96: dup
    //   97: invokespecial 395	java/lang/StringBuilder:<init>	()V
    //   100: aload_0
    //   101: invokevirtual 401	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   104: invokestatic 891	java/lang/System:currentTimeMillis	()J
    //   107: lload_1
    //   108: lsub
    //   109: invokevirtual 917	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   112: invokevirtual 404	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   115: invokestatic 899	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
    //   118: pop
    //   119: aload_3
    //   120: areturn
    //   121: aload 7
    //   123: invokevirtual 920	java/security/MessageDigest:digest	()[B
    //   126: invokestatic 924	miui/resourcebrowser/util/ResourceHelper:toHexString	([B)Ljava/lang/String;
    //   129: astore_3
    //   130: aload_3
    //   131: astore_3
    //   132: aload 4
    //   134: ifnull -44 -> 90
    //   137: aload 4
    //   139: invokevirtual 914	java/io/BufferedInputStream:close	()V
    //   142: goto -52 -> 90
    //   145: astore 4
    //   147: aload 4
    //   149: invokevirtual 713	java/io/IOException:printStackTrace	()V
    //   152: goto -62 -> 90
    //   155: astore 4
    //   157: aload 4
    //   159: invokevirtual 713	java/io/IOException:printStackTrace	()V
    //   162: goto -72 -> 90
    //   165: astore 5
    //   167: aconst_null
    //   168: astore 4
    //   170: aload 5
    //   172: invokevirtual 925	java/io/FileNotFoundException:printStackTrace	()V
    //   175: aload 4
    //   177: ifnull -87 -> 90
    //   180: aload 4
    //   182: invokevirtual 914	java/io/BufferedInputStream:close	()V
    //   185: goto -95 -> 90
    //   188: astore 4
    //   190: aload 4
    //   192: invokevirtual 713	java/io/IOException:printStackTrace	()V
    //   195: goto -105 -> 90
    //   198: astore 5
    //   200: aconst_null
    //   201: astore 4
    //   203: aload 5
    //   205: invokevirtual 713	java/io/IOException:printStackTrace	()V
    //   208: aload 4
    //   210: ifnull -120 -> 90
    //   213: aload 4
    //   215: invokevirtual 914	java/io/BufferedInputStream:close	()V
    //   218: goto -128 -> 90
    //   221: astore 4
    //   223: aload 4
    //   225: invokevirtual 713	java/io/IOException:printStackTrace	()V
    //   228: goto -138 -> 90
    //   231: astore_1
    //   232: aconst_null
    //   233: astore 4
    //   235: aload_1
    //   236: astore_1
    //   237: aload 4
    //   239: ifnull +8 -> 247
    //   242: aload 4
    //   244: invokevirtual 914	java/io/BufferedInputStream:close	()V
    //   247: aload_1
    //   248: athrow
    //   249: astore_2
    //   250: aload_2
    //   251: invokevirtual 713	java/io/IOException:printStackTrace	()V
    //   254: goto -7 -> 247
    //   257: astore_1
    //   258: goto -21 -> 237
    //   261: astore 5
    //   263: goto -60 -> 203
    //   266: astore 5
    //   268: goto -98 -> 170
    //   271: astore 5
    //   273: aconst_null
    //   274: astore 4
    //   276: goto -201 -> 75
    //
    // Exception table:
    //   from	to	target	type
    //   31	70	73	java/security/NoSuchAlgorithmException
    //   121	130	73	java/security/NoSuchAlgorithmException
    //   137	142	145	java/io/IOException
    //   85	90	155	java/io/IOException
    //   14	31	165	java/io/FileNotFoundException
    //   180	185	188	java/io/IOException
    //   14	31	198	java/io/IOException
    //   213	218	221	java/io/IOException
    //   14	31	231	finally
    //   242	247	249	java/io/IOException
    //   31	70	257	finally
    //   75	80	257	finally
    //   121	130	257	finally
    //   170	175	257	finally
    //   203	208	257	finally
    //   31	70	261	java/io/IOException
    //   121	130	261	java/io/IOException
    //   31	70	266	java/io/FileNotFoundException
    //   121	130	266	java/io/FileNotFoundException
    //   14	31	271	java/security/NoSuchAlgorithmException
  }

  public static String getFileName(String paramString)
  {
    Object localObject = (String)mCacheFileNameMap.get(paramString);
    if (TextUtils.isEmpty((CharSequence)localObject))
      localObject = paramString.split("/");
    for (localObject = localObject[(-1 + localObject.length)]; ; localObject = ((String)localObject).substring(0, ((String)localObject).length() / 2) + ((String)localObject).hashCode())
    {
      if (((String)localObject).length() > 128)
        continue;
      mCacheFileNameMap.put(paramString, localObject);
      return localObject;
    }
  }

  public static String getFileNameWithoutExtension(String paramString)
  {
    String str = ExtraFileUtils.getFileName(paramString);
    int i = str.lastIndexOf(".");
    if (i != -1)
      str = str.substring(0, i);
    return str;
  }

  public static long getLocalRingtoneDuration(String paramString)
  {
    long l;
    try
    {
      if (!new File(paramString).exists())
      {
        l = -1L;
      }
      else
      {
        MediaPlayer localMediaPlayer = new MediaPlayer();
        localMediaPlayer.setDataSource(paramString);
        localMediaPlayer.prepare();
        l = localMediaPlayer.getDuration();
        localMediaPlayer.release();
      }
    }
    catch (Exception localException)
    {
      l = -1L;
    }
    return l;
  }

  public static String getPathByUri(Context paramContext, Uri paramUri)
  {
    return getPathByUri(paramContext, paramUri, true);
  }

  public static String getPathByUri(Context paramContext, Uri paramUri, boolean paramBoolean)
  {
    String str;
    if (paramUri != null)
    {
      str = paramUri.toString();
      Object localObject = paramUri.getScheme();
      if ((!"content".equals(localObject)) || (!paramBoolean))
      {
        if ("file".equals(localObject))
          str = paramUri.getPath();
      }
      else
      {
        localObject = paramUri.getAuthority();
        if (!"settings".equals(localObject))
        {
          if ("media".equals(localObject))
            str = getContentColumnValue(paramContext, paramUri, "_data");
        }
        else
          str = getContentColumnValue(paramContext, paramUri, "value");
        localObject = Uri.parse(str);
        if ((((Uri)localObject).getScheme() != null) && (!((Uri)localObject).equals(paramUri)))
          str = getPathByUri(paramContext, (Uri)localObject, true);
      }
    }
    else
    {
      str = "";
    }
    return (String)str;
  }

  public static int getPreviewWidth(int paramInt)
  {
    return paramInt * 3 / 4;
  }

  public static int getThumbnailGap(Context paramContext)
  {
    return paramContext.getResources().getDimensionPixelSize(101318667);
  }

  public static Pair<Integer, Integer> getThumbnailSize(Activity paramActivity, int paramInt1, int paramInt2)
  {
    int j = 0;
    int i = -1;
    switch (paramInt1)
    {
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
      i = -1;
      j = 1;
      break;
    case 6:
    case 8:
      i = 101777412;
      j = 2;
      break;
    case 7:
      i = 101777409;
      j = 2;
      break;
    case 9:
    case 11:
      i = 101777410;
      j = 3;
      break;
    case 10:
      i = 101777411;
      j = 3;
    }
    if (i <= 0)
    {
      if (i != -1)
      {
        j = -2;
        i = -2;
      }
      else
      {
        j = -1;
        i = -1;
      }
    }
    else
    {
      Point localPoint = new Point();
      paramActivity.getWindowManager().getDefaultDisplay().getSize(localPoint);
      int k = localPoint.x;
      int m = getThumbnailGap(paramActivity);
      j = (k - paramInt2 - m * (j - 1)) / j;
      i = (int)paramActivity.getResources().getFraction(i, j, j);
    }
    return new Pair(Integer.valueOf(j), Integer.valueOf(i));
  }

  public static int getThumbnailViewResource(int paramInt)
  {
    int i = 0;
    switch (paramInt)
    {
    case 1:
      i = 100859913;
      break;
    case 2:
      i = 100859914;
      break;
    case 3:
      i = 100859915;
      break;
    case 4:
      i = 100859917;
      break;
    case 5:
      i = 100859916;
      break;
    case 6:
      i = 100859908;
      break;
    case 7:
      i = 100859911;
      break;
    case 8:
      i = 100859910;
      break;
    case 9:
      i = 100859907;
      break;
    case 10:
      i = 100859908;
      break;
    case 11:
      i = 100859912;
    }
    return i;
  }

  public static Uri getUriByPath(String paramString)
  {
    Uri localUri;
    if (!TextUtils.isEmpty(paramString))
    {
      localUri = Uri.parse(paramString);
      if (localUri.getScheme() == null)
        localUri = Uri.fromFile(new File(paramString));
    }
    else
    {
      localUri = null;
    }
    return localUri;
  }

  public static boolean isBitToggled(int paramInt1, int paramInt2)
  {
    int i;
    if ((paramInt1 & paramInt2) == 0)
      i = 0;
    else
      i = 1;
    return i;
  }

  public static boolean isCombineView(int paramInt)
  {
    int i;
    if ((paramInt != 6) && (paramInt != 8) && (paramInt != 7) && (paramInt != 9) && (paramInt != 10) && (paramInt != 11))
      i = 0;
    else
      i = 1;
    return i;
  }

  public static boolean isLocalResource(int paramInt)
  {
    return isBitToggled(paramInt, 1);
  }

  public static boolean isMultipleView(int paramInt)
  {
    int i;
    if (paramInt != 5)
      i = 0;
    else
      i = 1;
    return i;
  }

  public static boolean isOldResource(int paramInt)
  {
    return isBitToggled(paramInt, 4);
  }

  public static boolean isSingleView(int paramInt)
  {
    int i = 1;
    if ((paramInt != i) && (paramInt != 2) && (paramInt != 3) && (paramInt != 4))
      i = 0;
    return i;
  }

  public static boolean isSystemResource(String paramString)
  {
    int i;
    if ((paramString == null) || (!paramString.startsWith("/system")))
      i = 0;
    else
      i = 1;
    return i;
  }

  public static void setFileHash(String paramString1, String paramString2)
  {
    sFileHashCache.put(paramString1, new Pair(Long.valueOf(new File(paramString1).lastModified()), paramString2));
  }

  public static void setMusicVolumeType(Activity paramActivity, int paramInt)
  {
    int i;
    if (paramInt != 1)
    {
      if (paramInt != 2)
      {
        if (paramInt != 4)
        {
          if (paramInt != 32)
            i = 2;
          else
            i = 3;
        }
        else
          i = 4;
      }
      else
        i = 5;
    }
    else
      i = 2;
    paramActivity.setVolumeControlStream(i);
  }

  private static String toHexString(byte[] paramArrayOfByte)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    for (int i = 0; ; i++)
    {
      if (i >= paramArrayOfByte.length)
        return localStringBuilder.toString();
      String str = Integer.toHexString(0xFF & paramArrayOfByte[i]);
      if (str.length() == 1)
        localStringBuilder.append("0");
      localStringBuilder.append(str);
    }
  }

  public static void unzip(String paramString1, String paramString2, UnzipProcessUpdateListener paramUnzipProcessUpdateListener)
    throws IOException
  {
    ZipFile localZipFile = null;
    try
    {
      localZipFile = new ZipFile(paramString1);
      while (true)
      {
        ZipEntry localZipEntry;
        try
        {
          Enumeration localEnumeration = localZipFile.entries();
          new File(paramString2).mkdirs();
          if (!localEnumeration.hasMoreElements())
            break;
          localZipEntry = (ZipEntry)localEnumeration.nextElement();
          String str1 = paramString2 + localZipEntry.getName();
          if (!localZipEntry.isDirectory())
            break label114;
          new File(str1).mkdirs();
          continue;
        }
        finally
        {
          localZipFile = localZipFile;
        }
        label103: if (localZipFile != null)
          localZipFile.close();
        throw str2;
        label114: String str3 = writeTo(localZipFile.getInputStream(localZipEntry), str2, true);
        if (paramUnzipProcessUpdateListener == null)
          continue;
        paramUnzipProcessUpdateListener.onUpdate(str2, localZipEntry.getCompressedSize(), str3);
      }
      if (localZipFile != null)
        localZipFile.close();
      return;
    }
    finally
    {
      break label103;
    }
  }

  public static String writeTo(InputStream paramInputStream, String paramString)
  {
    return writeTo(paramInputStream, paramString, false);
  }

  // ERROR //
  public static String writeTo(InputStream paramInputStream, String paramString, boolean paramBoolean)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 4
    //   3: new 421	java/io/File
    //   6: dup
    //   7: aload_1
    //   8: invokespecial 782	java/io/File:<init>	(Ljava/lang/String;)V
    //   11: astore 6
    //   13: aload 6
    //   15: invokevirtual 1135	java/io/File:getParentFile	()Ljava/io/File;
    //   18: sipush 509
    //   21: bipush 255
    //   23: bipush 255
    //   25: invokestatic 1138	miui/os/ExtraFileUtils:mkdirs	(Ljava/io/File;III)Z
    //   28: pop
    //   29: new 801	java/io/BufferedInputStream
    //   32: dup
    //   33: aload_0
    //   34: invokespecial 809	java/io/BufferedInputStream:<init>	(Ljava/io/InputStream;)V
    //   37: astore 5
    //   39: iload_2
    //   40: ifeq +373 -> 413
    //   43: ldc_w 902
    //   46: invokestatic 908	java/security/MessageDigest:getInstance	(Ljava/lang/String;)Ljava/security/MessageDigest;
    //   49: astore_3
    //   50: new 1140	java/security/DigestInputStream
    //   53: dup
    //   54: aload 5
    //   56: aload_3
    //   57: invokespecial 1143	java/security/DigestInputStream:<init>	(Ljava/io/InputStream;Ljava/security/MessageDigest;)V
    //   60: astore_0
    //   61: aload_3
    //   62: astore 5
    //   64: new 1145	org/apache/http/entity/InputStreamEntity
    //   67: dup
    //   68: aload_0
    //   69: ldc2_w 959
    //   72: invokespecial 1148	org/apache/http/entity/InputStreamEntity:<init>	(Ljava/io/InputStream;J)V
    //   75: astore 7
    //   77: new 1150	java/io/BufferedOutputStream
    //   80: dup
    //   81: new 1152	java/io/FileOutputStream
    //   84: dup
    //   85: aload_1
    //   86: invokespecial 1153	java/io/FileOutputStream:<init>	(Ljava/lang/String;)V
    //   89: invokespecial 1156	java/io/BufferedOutputStream:<init>	(Ljava/io/OutputStream;)V
    //   92: astore_3
    //   93: aload_1
    //   94: sipush 509
    //   97: bipush 255
    //   99: bipush 255
    //   101: invokestatic 1162	android/os/FileUtils:setPermissions	(Ljava/lang/String;III)I
    //   104: pop
    //   105: aload 7
    //   107: aload_3
    //   108: invokevirtual 1164	org/apache/http/entity/InputStreamEntity:writeTo	(Ljava/io/OutputStream;)V
    //   111: aload 6
    //   113: invokestatic 891	java/lang/System:currentTimeMillis	()J
    //   116: invokevirtual 1168	java/io/File:setLastModified	(J)Z
    //   119: pop
    //   120: aload_0
    //   121: ifnull +7 -> 128
    //   124: aload_0
    //   125: invokevirtual 832	java/io/InputStream:close	()V
    //   128: aload_3
    //   129: ifnull +7 -> 136
    //   132: aload_3
    //   133: invokevirtual 1171	java/io/OutputStream:close	()V
    //   136: aload 5
    //   138: ifnonnull +167 -> 305
    //   141: aload 4
    //   143: areturn
    //   144: astore 6
    //   146: aload 6
    //   148: invokevirtual 713	java/io/IOException:printStackTrace	()V
    //   151: goto -23 -> 128
    //   154: astore_3
    //   155: aload_3
    //   156: invokevirtual 713	java/io/IOException:printStackTrace	()V
    //   159: goto -23 -> 136
    //   162: astore 6
    //   164: aconst_null
    //   165: astore_3
    //   166: aconst_null
    //   167: astore 5
    //   169: aload 6
    //   171: invokevirtual 913	java/security/NoSuchAlgorithmException:printStackTrace	()V
    //   174: aload_0
    //   175: ifnull +7 -> 182
    //   178: aload_0
    //   179: invokevirtual 832	java/io/InputStream:close	()V
    //   182: aload_3
    //   183: ifnull -47 -> 136
    //   186: aload_3
    //   187: invokevirtual 1171	java/io/OutputStream:close	()V
    //   190: goto -54 -> 136
    //   193: astore_3
    //   194: aload_3
    //   195: invokevirtual 713	java/io/IOException:printStackTrace	()V
    //   198: goto -62 -> 136
    //   201: astore 6
    //   203: aload 6
    //   205: invokevirtual 713	java/io/IOException:printStackTrace	()V
    //   208: goto -26 -> 182
    //   211: astore 6
    //   213: aconst_null
    //   214: astore_3
    //   215: aconst_null
    //   216: astore 5
    //   218: aload 6
    //   220: invokevirtual 713	java/io/IOException:printStackTrace	()V
    //   223: aload_0
    //   224: ifnull +7 -> 231
    //   227: aload_0
    //   228: invokevirtual 832	java/io/InputStream:close	()V
    //   231: aload_3
    //   232: ifnull -96 -> 136
    //   235: aload_3
    //   236: invokevirtual 1171	java/io/OutputStream:close	()V
    //   239: goto -103 -> 136
    //   242: astore_3
    //   243: aload_3
    //   244: invokevirtual 713	java/io/IOException:printStackTrace	()V
    //   247: goto -111 -> 136
    //   250: astore 6
    //   252: aload 6
    //   254: invokevirtual 713	java/io/IOException:printStackTrace	()V
    //   257: goto -26 -> 231
    //   260: astore 4
    //   262: aconst_null
    //   263: astore_3
    //   264: aload 4
    //   266: astore 4
    //   268: aload_0
    //   269: ifnull +7 -> 276
    //   272: aload_0
    //   273: invokevirtual 832	java/io/InputStream:close	()V
    //   276: aload_3
    //   277: ifnull +7 -> 284
    //   280: aload_3
    //   281: invokevirtual 1171	java/io/OutputStream:close	()V
    //   284: aload 4
    //   286: athrow
    //   287: astore 5
    //   289: aload 5
    //   291: invokevirtual 713	java/io/IOException:printStackTrace	()V
    //   294: goto -18 -> 276
    //   297: astore_3
    //   298: aload_3
    //   299: invokevirtual 713	java/io/IOException:printStackTrace	()V
    //   302: goto -18 -> 284
    //   305: aload 5
    //   307: invokevirtual 920	java/security/MessageDigest:digest	()[B
    //   310: invokestatic 924	miui/resourcebrowser/util/ResourceHelper:toHexString	([B)Ljava/lang/String;
    //   313: astore 4
    //   315: goto -174 -> 141
    //   318: astore 4
    //   320: aconst_null
    //   321: astore_3
    //   322: aload 5
    //   324: astore_0
    //   325: aload 4
    //   327: astore 4
    //   329: goto -61 -> 268
    //   332: astore 4
    //   334: goto -66 -> 268
    //   337: astore 6
    //   339: aconst_null
    //   340: astore_3
    //   341: aload 5
    //   343: astore_0
    //   344: aconst_null
    //   345: astore 5
    //   347: goto -129 -> 218
    //   350: astore 6
    //   352: aload 5
    //   354: astore_0
    //   355: aload_3
    //   356: astore 5
    //   358: aconst_null
    //   359: astore_3
    //   360: goto -142 -> 218
    //   363: astore 6
    //   365: aconst_null
    //   366: astore_3
    //   367: goto -149 -> 218
    //   370: astore 6
    //   372: goto -154 -> 218
    //   375: astore 6
    //   377: aconst_null
    //   378: astore_3
    //   379: aload 5
    //   381: astore_0
    //   382: aconst_null
    //   383: astore 5
    //   385: goto -216 -> 169
    //   388: astore 6
    //   390: aload 5
    //   392: astore_0
    //   393: aload_3
    //   394: astore 5
    //   396: aconst_null
    //   397: astore_3
    //   398: goto -229 -> 169
    //   401: astore 6
    //   403: aconst_null
    //   404: astore_3
    //   405: goto -236 -> 169
    //   408: astore 6
    //   410: goto -241 -> 169
    //   413: aload 5
    //   415: astore_0
    //   416: aconst_null
    //   417: astore 5
    //   419: goto -355 -> 64
    //
    // Exception table:
    //   from	to	target	type
    //   124	128	144	java/io/IOException
    //   132	136	154	java/io/IOException
    //   3	39	162	java/security/NoSuchAlgorithmException
    //   186	190	193	java/io/IOException
    //   178	182	201	java/io/IOException
    //   3	39	211	java/io/IOException
    //   235	239	242	java/io/IOException
    //   227	231	250	java/io/IOException
    //   3	39	260	finally
    //   64	93	260	finally
    //   272	276	287	java/io/IOException
    //   280	284	297	java/io/IOException
    //   43	50	318	finally
    //   50	61	318	finally
    //   93	120	332	finally
    //   169	174	332	finally
    //   218	223	332	finally
    //   43	50	337	java/io/IOException
    //   50	61	350	java/io/IOException
    //   64	93	363	java/io/IOException
    //   93	120	370	java/io/IOException
    //   43	50	375	java/security/NoSuchAlgorithmException
    //   50	61	388	java/security/NoSuchAlgorithmException
    //   64	93	401	java/security/NoSuchAlgorithmException
    //   93	120	408	java/security/NoSuchAlgorithmException
  }

  // ERROR //
  public static boolean zip(java.util.zip.ZipOutputStream paramZipOutputStream, File paramFile, String paramString)
  {
    // Byte code:
    //   0: iconst_1
    //   1: istore_3
    //   2: aload_1
    //   3: invokevirtual 1173	java/io/File:isDirectory	()Z
    //   6: ifeq +91 -> 97
    //   9: aload_1
    //   10: invokevirtual 626	java/io/File:listFiles	()[Ljava/io/File;
    //   13: astore 5
    //   15: aload 5
    //   17: ifnull +257 -> 274
    //   20: aload_2
    //   21: invokevirtual 756	java/lang/String:length	()I
    //   24: ifle +8 -> 32
    //   27: aload_2
    //   28: invokestatic 429	miui/os/ExtraFileUtils:standardizeFolderPath	(Ljava/lang/String;)Ljava/lang/String;
    //   31: astore_2
    //   32: iconst_0
    //   33: istore 4
    //   35: iload 4
    //   37: aload 5
    //   39: arraylength
    //   40: if_icmpge +234 -> 274
    //   43: aload_0
    //   44: aload 5
    //   46: iload 4
    //   48: aaload
    //   49: new 394	java/lang/StringBuilder
    //   52: dup
    //   53: invokespecial 395	java/lang/StringBuilder:<init>	()V
    //   56: aload_2
    //   57: invokevirtual 401	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   60: aload 5
    //   62: iload 4
    //   64: aaload
    //   65: invokevirtual 1174	java/io/File:getName	()Ljava/lang/String;
    //   68: invokevirtual 401	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   71: invokevirtual 404	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   74: invokestatic 1176	miui/resourcebrowser/util/ResourceHelper:zip	(Ljava/util/zip/ZipOutputStream;Ljava/io/File;Ljava/lang/String;)Z
    //   77: ifeq +15 -> 92
    //   80: iload_3
    //   81: ifeq +11 -> 92
    //   84: iconst_1
    //   85: istore_3
    //   86: iinc 4 1
    //   89: goto -54 -> 35
    //   92: iconst_0
    //   93: istore_3
    //   94: goto -8 -> 86
    //   97: aconst_null
    //   98: astore 5
    //   100: aconst_null
    //   101: astore 4
    //   103: sipush 4096
    //   106: newarray byte
    //   108: astore 6
    //   110: new 1178	java/util/zip/CRC32
    //   113: dup
    //   114: invokespecial 1179	java/util/zip/CRC32:<init>	()V
    //   117: astore 7
    //   119: new 1181	java/util/zip/CheckedInputStream
    //   122: dup
    //   123: new 801	java/io/BufferedInputStream
    //   126: dup
    //   127: new 803	java/io/FileInputStream
    //   130: dup
    //   131: aload_1
    //   132: invokespecial 806	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   135: invokespecial 809	java/io/BufferedInputStream:<init>	(Ljava/io/InputStream;)V
    //   138: aload 7
    //   140: invokespecial 1184	java/util/zip/CheckedInputStream:<init>	(Ljava/io/InputStream;Ljava/util/zip/Checksum;)V
    //   143: astore 5
    //   145: aload 5
    //   147: aload 6
    //   149: invokevirtual 1185	java/util/zip/CheckedInputStream:read	([B)I
    //   152: bipush 255
    //   154: if_icmpne -9 -> 145
    //   157: new 1115	java/util/zip/ZipEntry
    //   160: dup
    //   161: aload_2
    //   162: invokespecial 1186	java/util/zip/ZipEntry:<init>	(Ljava/lang/String;)V
    //   165: astore 8
    //   167: aload 8
    //   169: iconst_0
    //   170: invokevirtual 1189	java/util/zip/ZipEntry:setMethod	(I)V
    //   173: aload 8
    //   175: aload_1
    //   176: invokevirtual 1191	java/io/File:length	()J
    //   179: invokevirtual 1195	java/util/zip/ZipEntry:setSize	(J)V
    //   182: aload 8
    //   184: aload 7
    //   186: invokevirtual 1198	java/util/zip/CRC32:getValue	()J
    //   189: invokevirtual 1201	java/util/zip/ZipEntry:setCrc	(J)V
    //   192: aload_0
    //   193: aload 8
    //   195: invokevirtual 1207	java/util/zip/ZipOutputStream:putNextEntry	(Ljava/util/zip/ZipEntry;)V
    //   198: new 801	java/io/BufferedInputStream
    //   201: dup
    //   202: new 803	java/io/FileInputStream
    //   205: dup
    //   206: aload_1
    //   207: invokespecial 806	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   210: invokespecial 809	java/io/BufferedInputStream:<init>	(Ljava/io/InputStream;)V
    //   213: astore 4
    //   215: aload 4
    //   217: aload 6
    //   219: invokevirtual 909	java/io/BufferedInputStream:read	([B)I
    //   222: istore 7
    //   224: iload 7
    //   226: bipush 255
    //   228: if_icmpeq +48 -> 276
    //   231: aload_0
    //   232: aload 6
    //   234: iconst_0
    //   235: iload 7
    //   237: invokevirtual 1208	java/util/zip/ZipOutputStream:write	([BII)V
    //   240: goto -25 -> 215
    //   243: pop
    //   244: aload 4
    //   246: astore 4
    //   248: aload 5
    //   250: astore 5
    //   252: iconst_0
    //   253: istore_3
    //   254: aload 5
    //   256: ifnull +8 -> 264
    //   259: aload 5
    //   261: invokevirtual 1209	java/util/zip/CheckedInputStream:close	()V
    //   264: aload 4
    //   266: ifnull +8 -> 274
    //   269: aload 4
    //   271: invokevirtual 914	java/io/BufferedInputStream:close	()V
    //   274: iload_3
    //   275: ireturn
    //   276: aload 5
    //   278: ifnull +8 -> 286
    //   281: aload 5
    //   283: invokevirtual 1209	java/util/zip/CheckedInputStream:close	()V
    //   286: aload 4
    //   288: ifnull -14 -> 274
    //   291: aload 4
    //   293: invokevirtual 914	java/io/BufferedInputStream:close	()V
    //   296: goto -22 -> 274
    //   299: astore 4
    //   301: aload 4
    //   303: invokevirtual 713	java/io/IOException:printStackTrace	()V
    //   306: goto -32 -> 274
    //   309: astore 5
    //   311: aload 5
    //   313: invokevirtual 713	java/io/IOException:printStackTrace	()V
    //   316: goto -30 -> 286
    //   319: astore 5
    //   321: aload 5
    //   323: invokevirtual 713	java/io/IOException:printStackTrace	()V
    //   326: goto -62 -> 264
    //   329: astore 4
    //   331: aload 4
    //   333: invokevirtual 713	java/io/IOException:printStackTrace	()V
    //   336: goto -62 -> 274
    //   339: astore_3
    //   340: aload 5
    //   342: ifnull +8 -> 350
    //   345: aload 5
    //   347: invokevirtual 1209	java/util/zip/CheckedInputStream:close	()V
    //   350: aload 4
    //   352: ifnull +8 -> 360
    //   355: aload 4
    //   357: invokevirtual 914	java/io/BufferedInputStream:close	()V
    //   360: aload_3
    //   361: athrow
    //   362: astore 5
    //   364: aload 5
    //   366: invokevirtual 713	java/io/IOException:printStackTrace	()V
    //   369: goto -19 -> 350
    //   372: astore 4
    //   374: aload 4
    //   376: invokevirtual 713	java/io/IOException:printStackTrace	()V
    //   379: goto -19 -> 360
    //   382: astore_3
    //   383: aload 5
    //   385: astore 5
    //   387: goto -47 -> 340
    //   390: astore_3
    //   391: aload 4
    //   393: astore 4
    //   395: aload 5
    //   397: astore 5
    //   399: goto -59 -> 340
    //   402: pop
    //   403: goto -151 -> 252
    //   406: pop
    //   407: aload 5
    //   409: astore 5
    //   411: goto -159 -> 252
    //
    // Exception table:
    //   from	to	target	type
    //   215	240	243	java/lang/Exception
    //   291	296	299	java/io/IOException
    //   281	286	309	java/io/IOException
    //   259	264	319	java/io/IOException
    //   269	274	329	java/io/IOException
    //   103	145	339	finally
    //   345	350	362	java/io/IOException
    //   355	360	372	java/io/IOException
    //   145	215	382	finally
    //   215	240	390	finally
    //   103	145	402	java/lang/Exception
    //   145	215	406	java/lang/Exception
  }

  public static abstract interface UnzipProcessUpdateListener
  {
    public abstract void onUpdate(String paramString1, long paramLong, String paramString2);
  }

  private static class DescriptionSAXHandler extends DefaultHandler
  {
    private HashMap<String, String> nvp = new HashMap();
    private String value;

    public void characters(char[] paramArrayOfChar, int paramInt1, int paramInt2)
      throws SAXException
    {
      this.value = new String(paramArrayOfChar, paramInt1, paramInt2);
    }

    public void endElement(String paramString1, String paramString2, String paramString3)
      throws SAXException
    {
      this.nvp.put(paramString2, this.value);
    }

    public HashMap<String, String> getElementEntries()
    {
      return this.nvp;
    }
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.util.ResourceHelper
 * JD-Core Version:    0.6.0
 */