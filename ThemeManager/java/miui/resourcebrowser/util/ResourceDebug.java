package miui.resourcebrowser.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;

public class ResourceDebug
{
  public static final boolean DEBUG = new File("/data/system/theme_debug").exists();
  public static final HashMap<Object, Object> sMap = new HashMap();
  private static int sMaxThumbnailDownloadTaskNumber = 3;

  static
  {
    if (DEBUG);
    try
    {
      BufferedReader localBufferedReader = new BufferedReader(new FileReader("/data/system/theme_debug"));
      int i = Integer.parseInt(localBufferedReader.readLine().trim());
      if ((i > 0) && (i < 10))
        sMaxThumbnailDownloadTaskNumber = i;
      localBufferedReader.close();
      label81: return;
    }
    catch (Exception localException)
    {
      break label81;
    }
  }

  public static int getMaxThumbnailDownloadTaskNumber()
  {
    return sMaxThumbnailDownloadTaskNumber;
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.util.ResourceDebug
 * JD-Core Version:    0.6.0
 */