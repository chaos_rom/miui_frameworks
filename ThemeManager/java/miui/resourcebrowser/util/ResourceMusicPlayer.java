package miui.resourcebrowser.util;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.Toast;
import java.util.List;
import miui.resourcebrowser.model.Resource;

public class ResourceMusicPlayer
{
  private Activity mActivity;
  private BatchMediaPlayer mBatchPlayer;
  protected ImageView mCurrentPlayingButton;
  private Resource mCurrentPlayingResource;
  private Handler mHandler = new Handler()
  {
    public void handleMessage(Message paramMessage)
    {
      if ((paramMessage.what == 0) && (ResourceMusicPlayer.this.mBatchPlayer != null) && (ResourceMusicPlayer.this.mProgressListener != null) && (ResourceMusicPlayer.this.mBatchPlayer.isPlaying()))
      {
        ResourceMusicPlayer.this.mProgressListener.onProgressUpdate(ResourceMusicPlayer.this.mBatchPlayer.getPlayedDuration(), ResourceMusicPlayer.this.mBatchPlayer.getTotalDuration());
        ResourceMusicPlayer.this.mHandler.sendEmptyMessageDelayed(0, 50L);
      }
    }
  };
  private PlayProgressListener mProgressListener;
  private boolean mShowPlayButton;
  private boolean mUserPlaying;

  public ResourceMusicPlayer(Activity paramActivity, boolean paramBoolean)
  {
    this.mActivity = paramActivity;
    this.mShowPlayButton = paramBoolean;
  }

  private void initPlayer()
  {
    this.mBatchPlayer = new BatchMediaPlayer(this.mActivity);
    this.mBatchPlayer.setListener(new BatchMediaPlayer.BatchPlayerListener()
    {
      public void finish(boolean paramBoolean)
      {
        ResourceMusicPlayer.access$402(ResourceMusicPlayer.this, false);
        ResourceMusicPlayer.access$302(ResourceMusicPlayer.this, null);
        if (ResourceMusicPlayer.this.mCurrentPlayingButton != null)
          ResourceMusicPlayer.this.mCurrentPlayingButton.setImageResource(100794585);
        ResourceMusicPlayer.this.mCurrentPlayingButton = null;
        if (ResourceMusicPlayer.this.mProgressListener != null)
          ResourceMusicPlayer.this.mProgressListener.onStopPlaying();
        if (paramBoolean)
          Toast.makeText(ResourceMusicPlayer.this.mActivity, 101449760, 0).show();
      }

      public void play(String paramString, int paramInt1, int paramInt2)
      {
        if (ResourceMusicPlayer.this.mProgressListener != null)
        {
          ResourceMusicPlayer.this.mProgressListener.onStartPlaying();
          ResourceMusicPlayer.this.mHandler.sendEmptyMessageDelayed(0, 50L);
        }
      }
    });
  }

  public boolean canPlay(Resource paramResource)
  {
    int i;
    if ((paramResource.equals(this.mCurrentPlayingResource)) || (TextUtils.isEmpty(paramResource.getLocalPath())))
      i = 0;
    else
      i = 1;
    return i;
  }

  protected List<String> getMusicPlayList(Resource paramResource)
  {
    return ResourceHelper.getDefaultMusicPlayList(this.mActivity, paramResource);
  }

  public void initPlayButtonIfNeed(ImageView paramImageView, Resource paramResource)
  {
    if ((this.mShowPlayButton) && (paramImageView != null))
    {
      if (!paramResource.equals(this.mCurrentPlayingResource))
        paramImageView.setImageResource(100794585);
      else
        paramImageView.setImageResource(100794588);
      paramImageView.setOnClickListener(new View.OnClickListener(paramResource, paramImageView)
      {
        public void onClick(View paramView)
        {
          int i;
          if (this.val$data.equals(ResourceMusicPlayer.this.mCurrentPlayingResource))
            i = 0;
          else
            i = 1;
          ResourceMusicPlayer.this.stopMusic();
          if ((i != 0) && (!TextUtils.isEmpty(this.val$data.getLocalPath())))
            ResourceMusicPlayer.this.playMusic(this.val$view, this.val$data);
        }
      });
    }
  }

  public boolean isPlaying()
  {
    return this.mUserPlaying;
  }

  public void playMusic(ImageView paramImageView, Resource paramResource)
  {
    if (this.mShowPlayButton)
    {
      paramImageView.setImageResource(100794588);
      this.mCurrentPlayingButton = paramImageView;
      playMusic(paramResource);
      return;
    }
    throw new IllegalArgumentException("ResourceMusicPlayer does not support playing button.");
  }

  public void playMusic(Resource paramResource)
  {
    this.mCurrentPlayingResource = paramResource;
    if (this.mBatchPlayer == null)
      initPlayer();
    this.mBatchPlayer.setPlayList(getMusicPlayList(paramResource));
    this.mBatchPlayer.start();
    this.mUserPlaying = true;
  }

  public void regeistePlayProgressListener(PlayProgressListener paramPlayProgressListener)
  {
    this.mProgressListener = paramPlayProgressListener;
  }

  public void stopMusic()
  {
    if (this.mBatchPlayer != null)
      this.mBatchPlayer.stop();
    this.mCurrentPlayingResource = null;
    this.mUserPlaying = false;
  }

  public static abstract interface PlayProgressListener
  {
    public abstract void onProgressUpdate(int paramInt1, int paramInt2);

    public abstract void onStartPlaying();

    public abstract void onStopPlaying();
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.util.ResourceMusicPlayer
 * JD-Core Version:    0.6.0
 */