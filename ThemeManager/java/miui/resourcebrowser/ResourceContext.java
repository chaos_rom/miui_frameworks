package miui.resourcebrowser;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import miui.resourcebrowser.controller.online.RequestUrl;

public class ResourceContext
  implements Serializable
{
  public static final int DISPLAY_TYPE_DOUBLE_FLAT = 6;
  public static final int DISPLAY_TYPE_DOUBLE_FLAT_FONT = 7;
  public static final int DISPLAY_TYPE_DOUBLE_FLAT_ICON = 8;
  public static final int DISPLAY_TYPE_SINGLE = 1;
  public static final int DISPLAY_TYPE_SINGLE_DETAIL = 3;
  public static final int DISPLAY_TYPE_SINGLE_GALLERY = 5;
  public static final int DISPLAY_TYPE_SINGLE_MUSIC = 4;
  public static final int DISPLAY_TYPE_SINGLE_SMALL = 2;
  public static final int DISPLAY_TYPE_TRIPLE = 9;
  public static final int DISPLAY_TYPE_TRIPLE_FLAT = 10;
  public static final int DISPLAY_TYPE_TRIPLE_TEXT = 11;
  public static final int RESOURCE_FORMAT_AUDIO = 3;
  public static final int RESOURCE_FORMAT_BUNDLE = 1;
  public static final int RESOURCE_FORMAT_IMAGE = 2;
  public static final int RESOURCE_FORMAT_OTHER = 5;
  public static final int RESOURCE_FORMAT_ZIP = 4;
  private static final long serialVersionUID = 1L;
  private String associationCacheFolder;
  private String asyncImportFolder;
  private String baseDataCacheFolder;
  private String baseDataFolder;
  private String baseImageCacheFolder;
  private String buildInImageFolder;
  private List<String> buildInImagePrefixes = new ArrayList();
  private String categoryCacheFolder;
  private boolean categorySupported;
  private String contentFolder;
  private int currentPlatform;
  private String currentUsingPath;
  private String detailActivityClass;
  private String detailActivityPackage;
  private String detailCacheFolder;
  private int displayType;
  private String downloadFolder;
  private Map<String, Serializable> extraMeta = new HashMap();
  private String indexFolder;
  private String listCacheFolder;
  private RequestUrl listUrl;
  private String metaFolder;
  private int pageItemCount;
  private boolean picker;
  private boolean platformSupported;
  private String previewCacheFolder;
  private int previewImageWidth;
  private String recommendActivityClass;
  private String recommendActivityPackage;
  private String recommendCacheFolder;
  private String recommendImageCacheFolder;
  private int recommendImageWidth;
  private boolean recommendSupported;
  private String resourceCode;
  private String resourceExtension;
  private int resourceFormat;
  private String resourceStamp;
  private String resourceTitle;
  private String rightsFolder;
  private String searchActivityClass;
  private String searchActivityPackage;
  private boolean selfDescribing;
  private List<String> sourceFolders = new ArrayList();
  private String tabActivityClass;
  private String tabActivityPackage;
  private String thumbnailCacheFolder;
  private int thumbnailImageWidth;
  private String trackId;
  private String versionCacheFolder;
  private boolean versionSupported;

  public void addBuildInImagePrefix(String paramString)
  {
    this.buildInImagePrefixes.add(paramString);
  }

  public void addSourceFolder(String paramString)
  {
    this.sourceFolders.add(paramString);
  }

  public String getAssociationCacheFolder()
  {
    return this.associationCacheFolder;
  }

  public String getAsyncImportFolder()
  {
    return this.asyncImportFolder;
  }

  public String getBaseDataCacheFolder()
  {
    return this.baseDataCacheFolder;
  }

  public String getBaseDataFolder()
  {
    return this.baseDataFolder;
  }

  public String getBaseImageCacheFolder()
  {
    return this.baseImageCacheFolder;
  }

  public String getBuildInImageFolder()
  {
    return this.buildInImageFolder;
  }

  public List<String> getBuildInImagePrefixes()
  {
    return this.buildInImagePrefixes;
  }

  public String getCategoryCacheFolder()
  {
    return this.categoryCacheFolder;
  }

  public String getContentFolder()
  {
    return this.contentFolder;
  }

  public int getCurrentPlatform()
  {
    return this.currentPlatform;
  }

  public String getCurrentUsingPath()
  {
    return this.currentUsingPath;
  }

  public String getDetailActivityClass()
  {
    return this.detailActivityClass;
  }

  public String getDetailActivityPackage()
  {
    return this.detailActivityPackage;
  }

  public String getDetailCacheFolder()
  {
    return this.detailCacheFolder;
  }

  public int getDisplayType()
  {
    return this.displayType;
  }

  public String getDownloadFolder()
  {
    return this.downloadFolder;
  }

  public Serializable getExtraMeta(String paramString)
  {
    return (Serializable)this.extraMeta.get(paramString);
  }

  public Serializable getExtraMeta(String paramString, Serializable paramSerializable)
  {
    Serializable localSerializable = (Serializable)this.extraMeta.get(paramString);
    if (localSerializable == null)
      localSerializable = paramSerializable;
    return localSerializable;
  }

  public Map<String, Serializable> getExtraMeta()
  {
    return this.extraMeta;
  }

  public String getIndexFolder()
  {
    return this.indexFolder;
  }

  public String getListCacheFolder()
  {
    return this.listCacheFolder;
  }

  public RequestUrl getListUrl()
  {
    return this.listUrl;
  }

  public String getMetaFolder()
  {
    return this.metaFolder;
  }

  public int getPageItemCount()
  {
    return this.pageItemCount;
  }

  public String getPreviewCacheFolder()
  {
    return this.previewCacheFolder;
  }

  public int getPreviewImageWidth()
  {
    return this.previewImageWidth;
  }

  public String getRecommendActivityClass()
  {
    return this.recommendActivityClass;
  }

  public String getRecommendActivityPackage()
  {
    return this.recommendActivityPackage;
  }

  public String getRecommendCacheFolder()
  {
    return this.recommendCacheFolder;
  }

  public String getRecommendImageCacheFolder()
  {
    return this.recommendImageCacheFolder;
  }

  public int getRecommendImageWidth()
  {
    return this.recommendImageWidth;
  }

  public String getResourceCode()
  {
    return this.resourceCode;
  }

  public String getResourceExtension()
  {
    return this.resourceExtension;
  }

  public int getResourceFormat()
  {
    return this.resourceFormat;
  }

  public String getResourceStamp()
  {
    return this.resourceStamp;
  }

  public String getResourceTitle()
  {
    return this.resourceTitle;
  }

  public String getRightsFolder()
  {
    return this.rightsFolder;
  }

  public String getSearchActivityClass()
  {
    return this.searchActivityClass;
  }

  public String getSearchActivityPackage()
  {
    return this.searchActivityPackage;
  }

  public List<String> getSourceFolders()
  {
    return this.sourceFolders;
  }

  public String getTabActivityClass()
  {
    return this.tabActivityClass;
  }

  public String getTabActivityPackage()
  {
    return this.tabActivityPackage;
  }

  public String getThumbnailCacheFolder()
  {
    return this.thumbnailCacheFolder;
  }

  public int getThumbnailImageWidth()
  {
    return this.thumbnailImageWidth;
  }

  public String getTrackId()
  {
    return this.trackId;
  }

  public String getVersionCacheFolder()
  {
    return this.versionCacheFolder;
  }

  public boolean isCategorySupported()
  {
    return this.categorySupported;
  }

  public boolean isPicker()
  {
    return this.picker;
  }

  public boolean isPlatformSupported()
  {
    return this.platformSupported;
  }

  public boolean isRecommendSupported()
  {
    return this.recommendSupported;
  }

  public boolean isSelfDescribing()
  {
    return this.selfDescribing;
  }

  public boolean isVersionSupported()
  {
    return this.versionSupported;
  }

  public void putExtraMeta(String paramString, Serializable paramSerializable)
  {
    this.extraMeta.put(paramString, paramSerializable);
  }

  public void setAssociationCacheFolder(String paramString)
  {
    this.associationCacheFolder = paramString;
  }

  public void setAsyncImportFolder(String paramString)
  {
    this.asyncImportFolder = paramString;
  }

  public void setBaseDataCacheFolder(String paramString)
  {
    this.baseDataCacheFolder = paramString;
  }

  public void setBaseDataFolder(String paramString)
  {
    this.baseDataFolder = paramString;
  }

  public void setBaseImageCacheFolder(String paramString)
  {
    this.baseImageCacheFolder = paramString;
  }

  public void setBuildInImageFolder(String paramString)
  {
    this.buildInImageFolder = paramString;
  }

  public void setBuildInImagePrefixes(List<String> paramList)
  {
    this.buildInImagePrefixes = paramList;
  }

  public void setCategoryCacheFolder(String paramString)
  {
    this.categoryCacheFolder = paramString;
  }

  public void setCategorySupported(boolean paramBoolean)
  {
    this.categorySupported = paramBoolean;
  }

  public void setContentFolder(String paramString)
  {
    this.contentFolder = paramString;
  }

  public void setCurrentPlatform(int paramInt)
  {
    this.currentPlatform = paramInt;
  }

  public void setCurrentUsingPath(String paramString)
  {
    this.currentUsingPath = paramString;
  }

  public void setDetailActivityClass(String paramString)
  {
    this.detailActivityClass = paramString;
  }

  public void setDetailActivityPackage(String paramString)
  {
    this.detailActivityPackage = paramString;
  }

  public void setDetailCacheFolder(String paramString)
  {
    this.detailCacheFolder = paramString;
  }

  public void setDisplayType(int paramInt)
  {
    this.displayType = paramInt;
  }

  public void setDownloadFolder(String paramString)
  {
    this.downloadFolder = paramString;
  }

  public void setExtraMeta(Map<String, Serializable> paramMap)
  {
    this.extraMeta = paramMap;
  }

  public void setIndexFolder(String paramString)
  {
    this.indexFolder = paramString;
  }

  public void setListCacheFolder(String paramString)
  {
    this.listCacheFolder = paramString;
  }

  public void setListUrl(RequestUrl paramRequestUrl)
  {
    this.listUrl = paramRequestUrl;
  }

  public void setMetaFolder(String paramString)
  {
    this.metaFolder = paramString;
  }

  public void setPageItemCount(int paramInt)
  {
    this.pageItemCount = paramInt;
  }

  public void setPicker(boolean paramBoolean)
  {
    this.picker = paramBoolean;
  }

  public void setPlatformSupported(boolean paramBoolean)
  {
    this.platformSupported = paramBoolean;
  }

  public void setPreviewCacheFolder(String paramString)
  {
    this.previewCacheFolder = paramString;
  }

  public void setPreviewImageWidth(int paramInt)
  {
    this.previewImageWidth = paramInt;
  }

  public void setRecommendActivityClass(String paramString)
  {
    this.recommendActivityClass = paramString;
  }

  public void setRecommendActivityPackage(String paramString)
  {
    this.recommendActivityPackage = paramString;
  }

  public void setRecommendCacheFolder(String paramString)
  {
    this.recommendCacheFolder = paramString;
  }

  public void setRecommendImageCacheFolder(String paramString)
  {
    this.recommendImageCacheFolder = paramString;
  }

  public void setRecommendImageWidth(int paramInt)
  {
    this.recommendImageWidth = paramInt;
  }

  public void setRecommendSupported(boolean paramBoolean)
  {
    this.recommendSupported = paramBoolean;
  }

  public void setResourceCode(String paramString)
  {
    this.resourceCode = paramString;
  }

  public void setResourceExtension(String paramString)
  {
    this.resourceExtension = paramString;
  }

  public void setResourceFormat(int paramInt)
  {
    this.resourceFormat = paramInt;
  }

  public void setResourceStamp(String paramString)
  {
    this.resourceStamp = paramString;
  }

  public void setResourceTitle(String paramString)
  {
    this.resourceTitle = paramString;
  }

  public void setRightsFolder(String paramString)
  {
    this.rightsFolder = paramString;
  }

  public void setSearchActivityClass(String paramString)
  {
    this.searchActivityClass = paramString;
  }

  public void setSearchActivityPackage(String paramString)
  {
    this.searchActivityPackage = paramString;
  }

  public void setSelfDescribing(boolean paramBoolean)
  {
    this.selfDescribing = paramBoolean;
  }

  public void setSourceFolders(List<String> paramList)
  {
    this.sourceFolders = paramList;
  }

  public void setTabActivityClass(String paramString)
  {
    this.tabActivityClass = paramString;
  }

  public void setTabActivityPackage(String paramString)
  {
    this.tabActivityPackage = paramString;
  }

  public void setThumbnailCacheFolder(String paramString)
  {
    this.thumbnailCacheFolder = paramString;
  }

  public void setThumbnailImageWidth(int paramInt)
  {
    this.thumbnailImageWidth = paramInt;
  }

  public void setTrackId(String paramString)
  {
    this.trackId = paramString;
  }

  public void setVersionCacheFolder(String paramString)
  {
    this.versionCacheFolder = paramString;
  }

  public void setVersionSupported(boolean paramBoolean)
  {
    this.versionSupported = paramBoolean;
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.ResourceContext
 * JD-Core Version:    0.6.0
 */