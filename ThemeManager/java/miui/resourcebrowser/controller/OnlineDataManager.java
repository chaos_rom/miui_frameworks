package miui.resourcebrowser.controller;

import java.util.List;
import miui.resourcebrowser.ResourceContext;
import miui.resourcebrowser.model.RecommendItemData;
import miui.resourcebrowser.model.Resource;
import miui.resourcebrowser.model.ResourceCategory;
import miui.resourcebrowser.model.ResourceListMeta;

public abstract class OnlineDataManager extends DataManager
{
  public OnlineDataManager(ResourceContext paramResourceContext)
  {
    super(paramResourceContext);
  }

  public abstract List<Resource> getAssociationResources(List<String> paramList);

  public abstract List<RecommendItemData> getRecommends();

  public abstract Resource getResource(String paramString);

  public abstract Resource getResource(String paramString, boolean paramBoolean);

  public abstract Resource getResourceByLocalId(String paramString);

  public abstract List<ResourceCategory> getResourceCategories();

  public abstract ResourceListMeta getResourceListMeta(String paramString);

  public abstract List<Resource> getResources(int paramInt);

  public abstract List<Resource> getUpdatableResources(List<String> paramList);
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.controller.OnlineDataManager
 * JD-Core Version:    0.6.0
 */