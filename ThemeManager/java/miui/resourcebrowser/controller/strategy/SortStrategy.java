package miui.resourcebrowser.controller.strategy;

import java.util.List;
import miui.resourcebrowser.model.Resource;

public abstract class SortStrategy
{
  public abstract List<Resource> sort(List<Resource> paramList);
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.controller.strategy.SortStrategy
 * JD-Core Version:    0.6.0
 */