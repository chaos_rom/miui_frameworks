package miui.resourcebrowser.controller.strategy;

import miui.resourcebrowser.model.Resource;

public class DefaultSearchStrategy extends SearchStrategy
{
  public boolean isHitted(String paramString, Resource paramResource)
  {
    int i = 1;
    if (paramResource != null)
    {
      String str = paramResource.getTitle();
      if ((str == null) || (!str.toLowerCase().contains(paramString.toLowerCase())))
      {
        str = paramResource.getDescription();
        if ((str != null) && (str.toLowerCase().contains(paramString.toLowerCase())));
      }
    }
    else
    {
      i = 0;
    }
    return i;
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.controller.strategy.DefaultSearchStrategy
 * JD-Core Version:    0.6.0
 */