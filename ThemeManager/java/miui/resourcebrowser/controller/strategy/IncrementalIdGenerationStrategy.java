package miui.resourcebrowser.controller.strategy;

import miui.resourcebrowser.ResourceConstants;

public class IncrementalIdGenerationStrategy extends IdGenerationStrategy
  implements ResourceConstants
{
  private static final String PATH = CONFIG_PATH + "id";
  private static long id = -1L;

  // ERROR //
  static
  {
    // Byte code:
    //   0: new 20	java/lang/StringBuilder
    //   3: dup
    //   4: invokespecial 23	java/lang/StringBuilder:<init>	()V
    //   7: getstatic 26	miui/resourcebrowser/controller/strategy/IncrementalIdGenerationStrategy:CONFIG_PATH	Ljava/lang/String;
    //   10: invokevirtual 30	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   13: ldc 31
    //   15: invokevirtual 30	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   18: invokevirtual 35	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   21: putstatic 37	miui/resourcebrowser/controller/strategy/IncrementalIdGenerationStrategy:PATH	Ljava/lang/String;
    //   24: ldc2_w 38
    //   27: putstatic 41	miui/resourcebrowser/controller/strategy/IncrementalIdGenerationStrategy:id	J
    //   30: new 43	java/io/File
    //   33: dup
    //   34: getstatic 37	miui/resourcebrowser/controller/strategy/IncrementalIdGenerationStrategy:PATH	Ljava/lang/String;
    //   37: invokespecial 46	java/io/File:<init>	(Ljava/lang/String;)V
    //   40: astore_0
    //   41: aload_0
    //   42: invokevirtual 50	java/io/File:exists	()Z
    //   45: ifeq +49 -> 94
    //   48: new 52	java/io/BufferedReader
    //   51: dup
    //   52: new 54	java/io/FileReader
    //   55: dup
    //   56: getstatic 37	miui/resourcebrowser/controller/strategy/IncrementalIdGenerationStrategy:PATH	Ljava/lang/String;
    //   59: invokespecial 55	java/io/FileReader:<init>	(Ljava/lang/String;)V
    //   62: invokespecial 58	java/io/BufferedReader:<init>	(Ljava/io/Reader;)V
    //   65: astore_1
    //   66: aload_1
    //   67: invokevirtual 61	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   70: invokestatic 67	java/lang/Long:parseLong	(Ljava/lang/String;)J
    //   73: putstatic 41	miui/resourcebrowser/controller/strategy/IncrementalIdGenerationStrategy:id	J
    //   76: getstatic 41	miui/resourcebrowser/controller/strategy/IncrementalIdGenerationStrategy:id	J
    //   79: ldc2_w 68
    //   82: lcmp
    //   83: ifge +41 -> 124
    //   86: ldc2_w 68
    //   89: lstore_1
    //   90: lload_1
    //   91: putstatic 41	miui/resourcebrowser/controller/strategy/IncrementalIdGenerationStrategy:id	J
    //   94: getstatic 41	miui/resourcebrowser/controller/strategy/IncrementalIdGenerationStrategy:id	J
    //   97: ldc2_w 38
    //   100: lcmp
    //   101: ifne +22 -> 123
    //   104: aload_0
    //   105: invokevirtual 73	java/io/File:getParentFile	()Ljava/io/File;
    //   108: invokevirtual 76	java/io/File:mkdirs	()Z
    //   111: pop
    //   112: aload_0
    //   113: invokevirtual 79	java/io/File:createNewFile	()Z
    //   116: pop
    //   117: ldc2_w 68
    //   120: putstatic 41	miui/resourcebrowser/controller/strategy/IncrementalIdGenerationStrategy:id	J
    //   123: return
    //   124: getstatic 41	miui/resourcebrowser/controller/strategy/IncrementalIdGenerationStrategy:id	J
    //   127: lstore_1
    //   128: goto -38 -> 90
    //   131: astore_1
    //   132: aload_1
    //   133: invokevirtual 82	java/io/FileNotFoundException:printStackTrace	()V
    //   136: goto -42 -> 94
    //   139: pop
    //   140: ldc2_w 68
    //   143: putstatic 41	miui/resourcebrowser/controller/strategy/IncrementalIdGenerationStrategy:id	J
    //   146: goto -52 -> 94
    //   149: pop
    //   150: ldc2_w 68
    //   153: putstatic 41	miui/resourcebrowser/controller/strategy/IncrementalIdGenerationStrategy:id	J
    //   156: goto -62 -> 94
    //   159: pop
    //   160: new 84	java/lang/RuntimeException
    //   163: dup
    //   164: ldc 86
    //   166: invokespecial 87	java/lang/RuntimeException:<init>	(Ljava/lang/String;)V
    //   169: athrow
    //   170: pop
    //   171: goto -21 -> 150
    //   174: pop
    //   175: goto -35 -> 140
    //   178: astore_1
    //   179: goto -47 -> 132
    //
    // Exception table:
    //   from	to	target	type
    //   48	66	131	java/io/FileNotFoundException
    //   48	66	139	java/io/IOException
    //   48	66	149	java/lang/NumberFormatException
    //   104	123	159	java/io/IOException
    //   66	94	170	java/lang/NumberFormatException
    //   124	128	170	java/lang/NumberFormatException
    //   66	94	174	java/io/IOException
    //   124	128	174	java/io/IOException
    //   66	94	178	java/io/FileNotFoundException
    //   124	128	178	java/io/FileNotFoundException
  }

  /** @deprecated */
  // ERROR //
  public String nextId()
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: getstatic 41	miui/resourcebrowser/controller/strategy/IncrementalIdGenerationStrategy:id	J
    //   5: lstore_1
    //   6: ldc2_w 90
    //   9: lload_1
    //   10: ladd
    //   11: putstatic 41	miui/resourcebrowser/controller/strategy/IncrementalIdGenerationStrategy:id	J
    //   14: lload_1
    //   15: invokestatic 97	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   18: astore_1
    //   19: aconst_null
    //   20: astore_2
    //   21: new 99	java/io/BufferedWriter
    //   24: dup
    //   25: new 101	java/io/FileWriter
    //   28: dup
    //   29: getstatic 37	miui/resourcebrowser/controller/strategy/IncrementalIdGenerationStrategy:PATH	Ljava/lang/String;
    //   32: invokespecial 102	java/io/FileWriter:<init>	(Ljava/lang/String;)V
    //   35: invokespecial 105	java/io/BufferedWriter:<init>	(Ljava/io/Writer;)V
    //   38: astore_2
    //   39: aload_2
    //   40: aload_1
    //   41: invokevirtual 108	java/io/BufferedWriter:write	(Ljava/lang/String;)V
    //   44: aload_2
    //   45: ifnull +79 -> 124
    //   48: aload_2
    //   49: invokevirtual 111	java/io/BufferedWriter:close	()V
    //   52: aload_0
    //   53: monitorexit
    //   54: aload_1
    //   55: areturn
    //   56: astore_2
    //   57: aload_2
    //   58: invokevirtual 112	java/io/IOException:printStackTrace	()V
    //   61: goto -9 -> 52
    //   64: astore_3
    //   65: aload_3
    //   66: invokevirtual 112	java/io/IOException:printStackTrace	()V
    //   69: aload_2
    //   70: ifnull -18 -> 52
    //   73: aload_2
    //   74: invokevirtual 111	java/io/BufferedWriter:close	()V
    //   77: goto -25 -> 52
    //   80: astore_2
    //   81: aload_2
    //   82: invokevirtual 112	java/io/IOException:printStackTrace	()V
    //   85: goto -33 -> 52
    //   88: astore_1
    //   89: aload_0
    //   90: monitorexit
    //   91: aload_1
    //   92: athrow
    //   93: astore_1
    //   94: aload_2
    //   95: ifnull +7 -> 102
    //   98: aload_2
    //   99: invokevirtual 111	java/io/BufferedWriter:close	()V
    //   102: aload_1
    //   103: athrow
    //   104: astore_2
    //   105: aload_2
    //   106: invokevirtual 112	java/io/IOException:printStackTrace	()V
    //   109: goto -7 -> 102
    //   112: astore_1
    //   113: aload_2
    //   114: astore_2
    //   115: goto -21 -> 94
    //   118: astore_3
    //   119: aload_2
    //   120: astore_2
    //   121: goto -56 -> 65
    //   124: goto -72 -> 52
    //
    // Exception table:
    //   from	to	target	type
    //   48	52	56	java/io/IOException
    //   21	39	64	java/io/IOException
    //   73	77	80	java/io/IOException
    //   2	19	88	finally
    //   48	52	88	finally
    //   57	61	88	finally
    //   73	77	88	finally
    //   81	85	88	finally
    //   98	102	88	finally
    //   102	109	88	finally
    //   21	39	93	finally
    //   65	69	93	finally
    //   98	102	104	java/io/IOException
    //   39	44	112	finally
    //   39	44	118	java/io/IOException
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.controller.strategy.IncrementalIdGenerationStrategy
 * JD-Core Version:    0.6.0
 */