package miui.resourcebrowser.controller.strategy;

import java.io.File;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import miui.resourcebrowser.model.Resource;

public class ModifiedTimeSortStrategy extends SortStrategy
{
  public List<Resource> sort(List<Resource> paramList)
  {
    Collections.sort(paramList, new Comparator()
    {
      public int compare(Resource paramResource1, Resource paramResource2)
      {
        return Long.signum(new File(paramResource2.getFilePath()).lastModified() - new File(paramResource1.getFilePath()).lastModified());
      }
    });
    return paramList;
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.controller.strategy.ModifiedTimeSortStrategy
 * JD-Core Version:    0.6.0
 */