package miui.resourcebrowser.controller;

import miui.resourcebrowser.ResourceContext;
import miui.resourcebrowser.controller.local.AudioDataManager;
import miui.resourcebrowser.controller.local.FileSystemDataManager;
import miui.resourcebrowser.controller.online.HttpServerDataManager;

public class ResourceController
{
  protected ResourceContext context;
  private ImportManager importManager;
  private LocalDataManager localDataManager;
  private OnlineDataManager onlineDataManager;

  public ResourceController(ResourceContext paramResourceContext)
  {
    this.context = paramResourceContext;
    this.localDataManager = newLocalDataManager();
    this.localDataManager.setResourceController(this);
    this.onlineDataManager = newOnlineDataManager();
    this.onlineDataManager.setResourceController(this);
    this.importManager = newImportManager();
    this.importManager.setResourceController(this);
  }

  public ImportManager getImportManager()
  {
    return this.importManager;
  }

  public LocalDataManager getLocalDataManager()
  {
    return this.localDataManager;
  }

  public OnlineDataManager getOnlineDataManager()
  {
    return this.onlineDataManager;
  }

  protected ImportManager newImportManager()
  {
    return new ImportManager(this.context);
  }

  protected LocalDataManager newLocalDataManager()
  {
    Object localObject;
    if (this.context.getResourceFormat() != 3)
      localObject = new FileSystemDataManager(this.context);
    else
      localObject = new AudioDataManager(this.context);
    return (LocalDataManager)localObject;
  }

  protected OnlineDataManager newOnlineDataManager()
  {
    return new HttpServerDataManager(this.context);
  }

  public void setImportManager(ImportManager paramImportManager)
  {
    this.importManager = paramImportManager;
  }

  public void setLocalDataManager(LocalDataManager paramLocalDataManager)
  {
    this.localDataManager = paramLocalDataManager;
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.controller.ResourceController
 * JD-Core Version:    0.6.0
 */