package miui.resourcebrowser.controller.local;

import android.content.Context;
import android.content.res.Resources;
import android.media.ExtraRingtone;
import android.net.Uri;
import android.provider.Settings.System;
import android.text.TextUtils;
import java.util.List;
import miui.resourcebrowser.AppInnerContext;
import miui.resourcebrowser.ResourceContext;
import miui.resourcebrowser.controller.DataManager;
import miui.resourcebrowser.model.Resource;
import miui.resourcebrowser.util.ResourceHelper;

public class AudioDataManager extends FileSystemDataManager
{
  private static final String[] COMMON_RINGTONE_SUFFIX;

  static
  {
    String[] arrayOfString = new String[2];
    arrayOfString[0] = ".mp3";
    arrayOfString[1] = ".ogg";
    COMMON_RINGTONE_SUFFIX = arrayOfString;
  }

  public AudioDataManager(ResourceContext paramResourceContext)
  {
    super(paramResourceContext);
  }

  private Resource getDefaultOptionResource()
  {
    Object localObject1 = AppInnerContext.getInstance().getApplicationContext();
    Resource localResource = new Resource();
    Object localObject2 = (String)this.context.getExtraMeta("android.intent.extra.ringtone.DEFAULT_URI");
    int i = ((Integer)this.context.getExtraMeta("android.intent.extra.ringtone.TYPE")).intValue();
    if (!TextUtils.isEmpty((CharSequence)localObject2))
      localObject2 = Uri.parse((String)localObject2);
    else
      localObject2 = getSystemDefaultRingtoneUri(i);
    String str = ResourceHelper.getPathByUri((Context)localObject1, (Uri)localObject2);
    localResource.setLocalPath(str);
    localResource.setFilePath(str);
    localResource.setDownloadPath(str);
    localObject2 = removeFileNameSuffix(ExtraRingtone.getRingtoneTitle((Context)localObject1, (Uri)localObject2, false));
    int j = getSystemDefaultRingtoneNameId(i);
    if (!TextUtils.isEmpty((CharSequence)localObject2))
    {
      Object[] arrayOfObject = new Object[2];
      arrayOfObject[0] = ((Context)localObject1).getResources().getString(j);
      arrayOfObject[1] = localObject2;
      localObject1 = String.format("%s (%s)", arrayOfObject);
    }
    else
    {
      localObject1 = ((Context)localObject1).getResources().getString(j);
    }
    localResource.setTitle((String)localObject1);
    return (Resource)(Resource)localResource;
  }

  private Resource getSilentOptionResource()
  {
    Resource localResource = new Resource();
    localResource.setLocalPath("");
    localResource.setFilePath("");
    localResource.setDownloadPath("");
    localResource.setTitle(AppInnerContext.getInstance().getApplicationContext().getString(101449734));
    return localResource;
  }

  private int getSystemDefaultRingtoneNameId(int paramInt)
  {
    int i = 101449735;
    switch (paramInt)
    {
    case 1:
      i = 101449736;
      break;
    case 2:
      i = 101449737;
      break;
    case 4:
      i = 101449738;
    case 3:
    }
    return i;
  }

  private Uri getSystemDefaultRingtoneUri(int paramInt)
  {
    Uri localUri = null;
    switch (paramInt)
    {
    case 1:
      localUri = Settings.System.DEFAULT_RINGTONE_URI;
      break;
    case 2:
      localUri = Settings.System.DEFAULT_NOTIFICATION_URI;
      break;
    case 4:
      localUri = Settings.System.DEFAULT_ALARM_ALERT_URI;
    case 3:
    }
    return localUri;
  }

  private String removeFileNameSuffix(String paramString)
  {
    if (!TextUtils.isEmpty(paramString))
    {
      String str2 = paramString.toLowerCase();
      String[] arrayOfString = COMMON_RINGTONE_SUFFIX;
      int i = arrayOfString.length;
      int j = 0;
      while (j < i)
      {
        String str1 = arrayOfString[j];
        if (!str2.endsWith(str1))
        {
          j++;
          continue;
        }
        paramString = paramString.substring(0, str2.lastIndexOf(str1));
      }
    }
    return paramString;
  }

  protected LocalDataParser getDataParser()
  {
    return new LocalAudioDataParser(this.context);
  }

  protected void refreshResources()
  {
    super.refreshResources();
    if ((this.context.isPicker()) && (((Boolean)this.context.getExtraMeta("android.intent.extra.ringtone.SHOW_DEFAULT")).booleanValue()))
      this.dataSet.add(0, getDefaultOptionResource());
    if ((!this.context.isPicker()) || (((Boolean)this.context.getExtraMeta("android.intent.extra.ringtone.SHOW_SILENT")).booleanValue()))
      this.dataSet.add(0, getSilentOptionResource());
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.controller.local.AudioDataManager
 * JD-Core Version:    0.6.0
 */