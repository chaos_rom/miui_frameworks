package miui.resourcebrowser.controller.local;

public class PersistenceException extends Exception
{
  private static final long serialVersionUID = 1L;

  public PersistenceException()
  {
  }

  public PersistenceException(String paramString)
  {
    super(paramString);
  }

  public PersistenceException(String paramString, Throwable paramThrowable)
  {
    super(paramString, paramThrowable);
  }

  public PersistenceException(Throwable paramThrowable)
  {
    super(paramThrowable);
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.controller.local.PersistenceException
 * JD-Core Version:    0.6.0
 */