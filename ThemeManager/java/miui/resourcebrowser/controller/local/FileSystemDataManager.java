package miui.resourcebrowser.controller.local;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import miui.cache.DataCache;
import miui.cache.FolderCache;
import miui.resourcebrowser.ResourceConstants;
import miui.resourcebrowser.ResourceContext;
import miui.resourcebrowser.controller.LocalDataManager;
import miui.resourcebrowser.controller.ResourceController;
import miui.resourcebrowser.controller.strategy.DefaultSearchStrategy;
import miui.resourcebrowser.controller.strategy.ModifiedTimeSortStrategy;
import miui.resourcebrowser.controller.strategy.SearchStrategy;
import miui.resourcebrowser.controller.strategy.SortStrategy;
import miui.resourcebrowser.model.RelatedResource;
import miui.resourcebrowser.model.Resource;

public class FileSystemDataManager extends LocalDataManager
  implements ResourceConstants
{
  private FolderCache dataFolderCache = new FolderCache();
  protected List<Resource> dataSet = new ArrayList();
  private Map<String, Resource> localIdIndex = new HashMap();
  private Map<String, Resource> onlineIdIndex = new HashMap();
  private LocalDataParser parser = getDataParser();
  private DataCache<String, List<Resource>> searchCache = new DataCache(5);
  private SearchStrategy searchStrategy = new DefaultSearchStrategy();
  private SortStrategy sortStrategy = new ModifiedTimeSortStrategy();
  private Set<String> visitedMap = new HashSet();

  public FileSystemDataManager(ResourceContext paramResourceContext)
  {
    super(paramResourceContext);
  }

  private boolean isRelatedResourceValid(List<RelatedResource> paramList)
  {
    int i = 0;
    Iterator localIterator = paramList.iterator();
    Object localObject;
    while (true)
      if (localIterator.hasNext())
      {
        localObject = ((RelatedResource)localIterator.next()).getLocalPath();
        if ((localObject == null) || (((String)localObject).trim().equals("")) || (this.visitedMap.contains(localObject)))
          continue;
        localObject = new File((String)localObject);
        if (!((File)localObject).exists())
          break;
      }
    while (true)
    {
      try
      {
        boolean bool = isResourceValid(loadResource((File)localObject));
        if (bool)
          break;
        return i;
      }
      catch (PersistenceException localPersistenceException)
      {
        continue;
      }
      i = 1;
    }
  }

  private boolean isResourceValid(Resource paramResource)
  {
    int i = 0;
    if (paramResource != null)
    {
      String str1 = paramResource.getLocalPath();
      if ((str1 != null) && (!str1.trim().equals("")) && (new File(str1).exists()))
      {
        String str2 = paramResource.getFilePath();
        if ((str2 == null) || (str2.trim().equals("")) || (new File(str2).exists()))
        {
          this.visitedMap.add(str1);
          if ((isRelatedResourceValid(paramResource.getSubResources())) && (isRelatedResourceValid(paramResource.getParentResources())))
            i = 1;
        }
      }
    }
    return i;
  }

  private Resource loadResource(File paramFile)
    throws PersistenceException
  {
    return this.parser.loadResource(paramFile);
  }

  private List<Resource> loadResources(String paramString)
  {
    ArrayList localArrayList = new ArrayList();
    Object localObject = new File(paramString);
    File[] arrayOfFile;
    int j;
    if ((((File)localObject).exists()) && (((File)localObject).isDirectory()))
    {
      this.parser.updateState();
      arrayOfFile = ((File)localObject).listFiles();
      j = arrayOfFile.length;
    }
    for (int i = 0; ; i++)
      if (i < j)
      {
        File localFile = arrayOfFile[i];
        try
        {
          localObject = loadResource(localFile);
          if (localObject == null)
            continue;
          ((Resource)localObject).setLocalPath(localFile.getAbsolutePath());
          if (isResourceValid((Resource)localObject))
          {
            localArrayList.add(localObject);
            continue;
          }
          removeResource((Resource)localObject);
        }
        catch (PersistenceException localPersistenceException)
        {
          localFile.delete();
        }
      }
      else
      {
        return this.sortStrategy.sort(localArrayList);
      }
  }

  private boolean removeRelatedResources(List<RelatedResource> paramList)
  {
    int i = 1;
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
    {
      RelatedResource localRelatedResource = (RelatedResource)localIterator.next();
      Object localObject2 = localRelatedResource.getLocalPath();
      if ((localObject2 == null) || (((String)localObject2).trim().equals("")))
        continue;
      localObject2 = new File((String)localObject2);
      boolean bool;
      if (((File)localObject2).exists())
      {
        try
        {
          bool = removeResource(loadResource((File)localObject2));
          if ((bool) && (i != 0));
          for (i = 1; ; i = 0)
            break;
        }
        catch (PersistenceException localPersistenceException)
        {
          if (!((File)localObject2).delete())
            break label122;
        }
        if (i != 0);
        label122: for (i = 1; ; i = 0)
          break;
      }
      Object localObject1 = bool.getFilePath();
      if ((localObject1 == null) || (((String)localObject1).trim().equals("")))
        continue;
      localObject1 = new File((String)localObject1);
      if (!((File)localObject1).exists())
        continue;
      if ((((File)localObject1).delete()) && (i != 0));
      for (i = 1; ; i = 0)
        break;
    }
    return i;
  }

  private void storeResource(File paramFile, Resource paramResource)
    throws PersistenceException
  {
    this.parser.storeResource(paramFile, paramResource);
  }

  public boolean addResource(Resource paramResource)
  {
    File localFile = new File(paramResource.getLocalPath());
    try
    {
      localFile.getParentFile().mkdirs();
      storeResource(localFile, paramResource);
      notifyDataSetUpdateSuccessful();
      i = 1;
      return i;
    }
    catch (PersistenceException localPersistenceException)
    {
      while (true)
      {
        notifyDataSetUpdateFailed();
        int i = 0;
      }
    }
  }

  public List<Resource> findResources(String paramString)
  {
    Object localObject = (List)this.searchCache.get(paramString);
    ArrayList localArrayList;
    Iterator localIterator;
    if (localObject == null)
    {
      localArrayList = new ArrayList();
      localIterator = this.dataSet.iterator();
    }
    while (true)
    {
      if (!localIterator.hasNext())
      {
        localObject = this.sortStrategy.sort(localArrayList);
        this.searchCache.put(paramString, localObject);
        return localObject;
      }
      localObject = (Resource)localIterator.next();
      if (!this.searchStrategy.isHitted(paramString, (Resource)localObject))
        continue;
      localArrayList.add(localObject);
    }
  }

  protected LocalDataParser getDataParser()
  {
    Object localObject;
    if (!this.context.isSelfDescribing())
      localObject = new LocalJSONDataParser(this.context);
    else
      localObject = new LocalBareDataParser(this.context);
    return (LocalDataParser)localObject;
  }

  public Resource getResourceByOnlineId(String paramString)
  {
    return (Resource)this.onlineIdIndex.get(paramString);
  }

  public List<Resource> getResources()
  {
    return getResources(false);
  }

  public List<Resource> getResources(boolean paramBoolean)
  {
    if ((paramBoolean) || (needRefresh()))
    {
      refreshResources();
      notifyDataSetUpdateSuccessful();
    }
    return this.dataSet;
  }

  protected boolean needRefresh()
  {
    int i = 0;
    Iterator localIterator = this.context.getSourceFolders().iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return i;
      String str = (String)localIterator.next();
      if ((!new File(str).exists()) || (!this.dataFolderCache.isCacheDirty(str)))
        continue;
      this.dataFolderCache.get(str);
      i = 1;
    }
  }

  protected void refreshResources()
  {
    this.dataSet.clear();
    Object localObject = this.context.getSourceFolders().iterator();
    while (true)
    {
      if (!((Iterator)localObject).hasNext())
      {
        this.localIdIndex.clear();
        this.onlineIdIndex.clear();
        this.searchCache.clear();
        Iterator localIterator = this.dataSet.iterator();
        while (true)
        {
          if (!localIterator.hasNext())
            return;
          Resource localResource = (Resource)localIterator.next();
          str = localResource.getLocalId();
          localObject = localResource.getOnlineId();
          if (str != null)
            this.localIdIndex.put(str, localResource);
          if (localObject == null)
            continue;
          this.onlineIdIndex.put(localObject, localResource);
        }
      }
      String str = (String)((Iterator)localObject).next();
      this.dataSet.addAll(loadResources(str));
    }
  }

  public boolean removeResource(Resource paramResource)
  {
    boolean bool = true;
    Object localObject = paramResource.getLocalPath();
    if ((localObject != null) && (!((String)localObject).trim().equals("")))
      if ((!new File((String)localObject).delete()) || (!bool))
        bool = false;
      else
        bool = true;
    localObject = paramResource.getFilePath();
    if ((localObject != null) && (!((String)localObject).trim().equals("")))
      if ((!new File((String)localObject).delete()) || (!bool))
        bool = false;
      else
        bool = true;
    localObject = paramResource.getBuildInThumbnails().iterator();
    while (true)
    {
      if (!((Iterator)localObject).hasNext())
      {
        localObject = paramResource.getBuildInPreviews().iterator();
        while (true)
        {
          if (!((Iterator)localObject).hasNext())
          {
            removeRelatedResources(paramResource.getSubResources());
            bool = removeRelatedResources(paramResource.getParentResources());
            if (!bool)
              notifyDataSetUpdateFailed();
            else
              notifyDataSetUpdateSuccessful();
            return bool;
          }
          if ((!new File((String)((Iterator)localObject).next()).delete()) || (!bool))
            bool = false;
          else
            bool = true;
        }
      }
      if ((!new File((String)((Iterator)localObject).next()).delete()) || (!bool))
        bool = false;
      else
        bool = true;
    }
  }

  public void setResourceController(ResourceController paramResourceController)
  {
    super.setResourceController(paramResourceController);
    this.parser.setResourceController(paramResourceController);
  }

  public boolean updateResource(Resource paramResource)
  {
    File localFile = new File(paramResource.getLocalPath());
    try
    {
      localFile.getParentFile().mkdirs();
      storeResource(localFile, paramResource);
      notifyDataSetUpdateSuccessful();
      i = 1;
      return i;
    }
    catch (PersistenceException localPersistenceException)
    {
      while (true)
      {
        notifyDataSetUpdateFailed();
        int i = 0;
      }
    }
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.controller.local.FileSystemDataManager
 * JD-Core Version:    0.6.0
 */