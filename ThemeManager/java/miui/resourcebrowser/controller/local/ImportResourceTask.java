package miui.resourcebrowser.controller.local;

import java.io.File;
import miui.os.UniqueAsyncTask;
import miui.resourcebrowser.AppInnerContext;
import miui.resourcebrowser.ResourceConstants;
import miui.resourcebrowser.ResourceContext;
import miui.resourcebrowser.controller.ResourceController;

public class ImportResourceTask extends UniqueAsyncTask<String, Integer, Integer>
  implements ResourceConstants
{
  public static String IMPORT_BY_DOWNLOAD_FOLDER;
  public static String IMPORT_BY_IMPORT_FOLDER;
  public static String IMPORT_BY_ONLINE_ID;
  public static String IMPORT_BY_PATH;
  private static Object sObject = new Object();
  private ContextParser mContextParser;
  private LocalDataParser mDataParser;
  private int mImportTotalNum;
  private ResourceContext mResContext = AppInnerContext.getInstance().getResourceContext();
  private ResourceController mResController = AppInnerContext.getInstance().getResourceController();

  static
  {
    IMPORT_BY_ONLINE_ID = "import_by_online_id";
    IMPORT_BY_PATH = "import_by_path";
    IMPORT_BY_IMPORT_FOLDER = "import_by_import_folder";
    IMPORT_BY_DOWNLOAD_FOLDER = "import_by_download_folder";
  }

  public ImportResourceTask(ResourceContext paramResourceContext, String paramString)
  {
    super(paramString);
    this.mDataParser = new LocalSerializableDataParser(paramResourceContext);
    this.mContextParser = new ContextSerializableParser();
  }

  private boolean checkResourceImported(String paramString)
  {
    int i = 0;
    Object localObject = new File(this.mResContext.getIndexFolder());
    if ((localObject != null) && (((File)localObject).exists()) && (((File)localObject).isDirectory()))
    {
      localObject = ((File)localObject).listFiles();
      int k = localObject.length;
      int j = 0;
      while (j < k)
      {
        if (!localObject[j].getName().equals(paramString))
        {
          j++;
          continue;
        }
        i = 1;
      }
    }
    return i;
  }

  // ERROR //
  protected Integer doInBackground(String[] paramArrayOfString)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 65	miui/resourcebrowser/controller/local/ImportResourceTask:mResContext	Lmiui/resourcebrowser/ResourceContext;
    //   4: invokevirtual 121	miui/resourcebrowser/ResourceContext:isSelfDescribing	()Z
    //   7: ifeq +11 -> 18
    //   10: bipush 255
    //   12: invokestatic 127	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   15: astore_3
    //   16: aload_3
    //   17: areturn
    //   18: new 129	java/util/HashMap
    //   21: dup
    //   22: invokespecial 130	java/util/HashMap:<init>	()V
    //   25: astore_3
    //   26: iconst_0
    //   27: istore 4
    //   29: getstatic 33	miui/resourcebrowser/controller/local/ImportResourceTask:sObject	Ljava/lang/Object;
    //   32: astore_2
    //   33: aload_2
    //   34: monitorenter
    //   35: aload_1
    //   36: ifnull +8 -> 44
    //   39: aload_1
    //   40: arraylength
    //   41: ifne +19 -> 60
    //   44: bipush 255
    //   46: invokestatic 127	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   49: astore_3
    //   50: aload_2
    //   51: monitorexit
    //   52: goto -36 -> 16
    //   55: astore_3
    //   56: aload_2
    //   57: monitorexit
    //   58: aload_3
    //   59: athrow
    //   60: new 87	java/io/File
    //   63: dup
    //   64: aload_0
    //   65: getfield 65	miui/resourcebrowser/controller/local/ImportResourceTask:mResContext	Lmiui/resourcebrowser/ResourceContext;
    //   68: invokevirtual 133	miui/resourcebrowser/ResourceContext:getAsyncImportFolder	()Ljava/lang/String;
    //   71: invokespecial 94	java/io/File:<init>	(Ljava/lang/String;)V
    //   74: astore 5
    //   76: new 87	java/io/File
    //   79: dup
    //   80: aload_0
    //   81: getfield 65	miui/resourcebrowser/controller/local/ImportResourceTask:mResContext	Lmiui/resourcebrowser/ResourceContext;
    //   84: invokevirtual 136	miui/resourcebrowser/ResourceContext:getDownloadFolder	()Ljava/lang/String;
    //   87: invokespecial 94	java/io/File:<init>	(Ljava/lang/String;)V
    //   90: astore 6
    //   92: new 87	java/io/File
    //   95: dup
    //   96: aload_0
    //   97: getfield 65	miui/resourcebrowser/controller/local/ImportResourceTask:mResContext	Lmiui/resourcebrowser/ResourceContext;
    //   100: invokevirtual 139	miui/resourcebrowser/ResourceContext:getContentFolder	()Ljava/lang/String;
    //   103: invokespecial 94	java/io/File:<init>	(Ljava/lang/String;)V
    //   106: astore 8
    //   108: new 87	java/io/File
    //   111: dup
    //   112: aload_0
    //   113: getfield 65	miui/resourcebrowser/controller/local/ImportResourceTask:mResContext	Lmiui/resourcebrowser/ResourceContext;
    //   116: invokevirtual 93	miui/resourcebrowser/ResourceContext:getIndexFolder	()Ljava/lang/String;
    //   119: invokespecial 94	java/io/File:<init>	(Ljava/lang/String;)V
    //   122: astore 7
    //   124: aload 6
    //   126: invokevirtual 142	java/io/File:mkdirs	()Z
    //   129: pop
    //   130: aload 8
    //   132: invokevirtual 142	java/io/File:mkdirs	()Z
    //   135: pop
    //   136: aload 7
    //   138: invokevirtual 142	java/io/File:mkdirs	()Z
    //   141: pop
    //   142: aload 5
    //   144: invokevirtual 142	java/io/File:mkdirs	()Z
    //   147: pop
    //   148: aload_1
    //   149: iconst_0
    //   150: aaload
    //   151: getstatic 45	miui/resourcebrowser/controller/local/ImportResourceTask:IMPORT_BY_IMPORT_FOLDER	Ljava/lang/String;
    //   154: invokevirtual 114	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   157: ifeq +387 -> 544
    //   160: new 144	miui/resourcebrowser/util/ResourceDownloadHandler
    //   163: dup
    //   164: invokestatic 59	miui/resourcebrowser/AppInnerContext:getInstance	()Lmiui/resourcebrowser/AppInnerContext;
    //   167: invokevirtual 148	miui/resourcebrowser/AppInnerContext:getApplicationContext	()Landroid/content/Context;
    //   170: aload_0
    //   171: getfield 65	miui/resourcebrowser/controller/local/ImportResourceTask:mResContext	Lmiui/resourcebrowser/ResourceContext;
    //   174: invokespecial 151	miui/resourcebrowser/util/ResourceDownloadHandler:<init>	(Landroid/content/Context;Lmiui/resourcebrowser/ResourceContext;)V
    //   177: astore 10
    //   179: aload 5
    //   181: invokevirtual 105	java/io/File:listFiles	()[Ljava/io/File;
    //   184: astore 6
    //   186: aload 6
    //   188: arraylength
    //   189: istore 11
    //   191: iconst_0
    //   192: istore 7
    //   194: iload 7
    //   196: iload 11
    //   198: if_icmpge +903 -> 1101
    //   201: aload 6
    //   203: iload 7
    //   205: aaload
    //   206: astore 15
    //   208: aconst_null
    //   209: astore 12
    //   211: aconst_null
    //   212: astore 13
    //   214: aconst_null
    //   215: astore 9
    //   217: aconst_null
    //   218: astore 8
    //   220: aload 15
    //   222: invokevirtual 108	java/io/File:getName	()Ljava/lang/String;
    //   225: astore 14
    //   227: aload 14
    //   229: ldc 153
    //   231: invokevirtual 114	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   234: ifeq +6 -> 240
    //   237: goto +1022 -> 1259
    //   240: aload 14
    //   242: ldc 155
    //   244: invokevirtual 158	java/lang/String:endsWith	(Ljava/lang/String;)Z
    //   247: ifeq +182 -> 429
    //   250: aload 15
    //   252: astore 9
    //   254: aload 5
    //   256: invokevirtual 105	java/io/File:listFiles	()[Ljava/io/File;
    //   259: astore 15
    //   261: aload 15
    //   263: arraylength
    //   264: istore 19
    //   266: iconst_0
    //   267: istore 17
    //   269: iload 17
    //   271: iload 19
    //   273: if_icmpge +49 -> 322
    //   276: aload 15
    //   278: iload 17
    //   280: aaload
    //   281: astore 16
    //   283: aload 14
    //   285: new 160	java/lang/StringBuilder
    //   288: dup
    //   289: invokespecial 161	java/lang/StringBuilder:<init>	()V
    //   292: aload 16
    //   294: invokevirtual 108	java/io/File:getName	()Ljava/lang/String;
    //   297: invokevirtual 165	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   300: ldc 155
    //   302: invokevirtual 165	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   305: invokevirtual 168	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   308: invokevirtual 114	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   311: istore 18
    //   313: iload 18
    //   315: ifeq +950 -> 1265
    //   318: aload 16
    //   320: astore 8
    //   322: aload 9
    //   324: ifnull +192 -> 516
    //   327: aload 8
    //   329: ifnull +187 -> 516
    //   332: aload_0
    //   333: getfield 78	miui/resourcebrowser/controller/local/ImportResourceTask:mDataParser	Lmiui/resourcebrowser/controller/local/LocalDataParser;
    //   336: aload 9
    //   338: invokevirtual 174	miui/resourcebrowser/controller/local/LocalDataParser:loadResource	(Ljava/io/File;)Lmiui/resourcebrowser/model/Resource;
    //   341: astore 12
    //   343: aload_0
    //   344: getfield 83	miui/resourcebrowser/controller/local/ImportResourceTask:mContextParser	Lmiui/resourcebrowser/controller/local/ContextParser;
    //   347: aload 8
    //   349: invokevirtual 180	miui/resourcebrowser/controller/local/ContextParser:loadResourceContext	(Ljava/io/File;)Lmiui/resourcebrowser/ResourceContext;
    //   352: astore 13
    //   354: aload 13
    //   356: astore 13
    //   358: aload 12
    //   360: ifnull +899 -> 1259
    //   363: aload 13
    //   365: ifnull +894 -> 1259
    //   368: aload_0
    //   369: getfield 71	miui/resourcebrowser/controller/local/ImportResourceTask:mResController	Lmiui/resourcebrowser/controller/ResourceController;
    //   372: invokevirtual 186	miui/resourcebrowser/controller/ResourceController:getOnlineDataManager	()Lmiui/resourcebrowser/controller/OnlineDataManager;
    //   375: aload 12
    //   377: invokevirtual 191	miui/resourcebrowser/model/Resource:getOnlineId	()Ljava/lang/String;
    //   380: invokevirtual 197	miui/resourcebrowser/controller/OnlineDataManager:getResource	(Ljava/lang/String;)Lmiui/resourcebrowser/model/Resource;
    //   383: astore 14
    //   385: aload 14
    //   387: ifnonnull +890 -> 1277
    //   390: aload 10
    //   392: aload 12
    //   394: invokevirtual 191	miui/resourcebrowser/model/Resource:getOnlineId	()Ljava/lang/String;
    //   397: invokevirtual 200	miui/resourcebrowser/util/ResourceDownloadHandler:isResourceDownloading	(Ljava/lang/String;)Z
    //   400: ifne +859 -> 1259
    //   403: aload_3
    //   404: aload 12
    //   406: aload 13
    //   408: invokeinterface 206 3 0
    //   413: pop
    //   414: aload 9
    //   416: invokevirtual 209	java/io/File:delete	()Z
    //   419: pop
    //   420: aload 8
    //   422: invokevirtual 209	java/io/File:delete	()Z
    //   425: pop
    //   426: goto +833 -> 1259
    //   429: aload 15
    //   431: astore 8
    //   433: aload 5
    //   435: invokevirtual 105	java/io/File:listFiles	()[Ljava/io/File;
    //   438: astore 18
    //   440: aload 18
    //   442: arraylength
    //   443: istore 17
    //   445: iconst_0
    //   446: istore 15
    //   448: iload 15
    //   450: iload 17
    //   452: if_icmpge -130 -> 322
    //   455: aload 18
    //   457: iload 15
    //   459: aaload
    //   460: astore 16
    //   462: aload 16
    //   464: invokevirtual 108	java/io/File:getName	()Ljava/lang/String;
    //   467: new 160	java/lang/StringBuilder
    //   470: dup
    //   471: invokespecial 161	java/lang/StringBuilder:<init>	()V
    //   474: aload 14
    //   476: invokevirtual 165	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   479: ldc 155
    //   481: invokevirtual 165	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   484: invokevirtual 168	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   487: invokevirtual 114	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   490: ifeq +781 -> 1271
    //   493: aload 16
    //   495: astore 9
    //   497: goto -175 -> 322
    //   500: pop
    //   501: aload 9
    //   503: invokevirtual 209	java/io/File:delete	()Z
    //   506: pop
    //   507: aload 8
    //   509: invokevirtual 209	java/io/File:delete	()Z
    //   512: pop
    //   513: goto -155 -> 358
    //   516: aload 9
    //   518: ifnull +12 -> 530
    //   521: aload 9
    //   523: invokevirtual 209	java/io/File:delete	()Z
    //   526: pop
    //   527: goto -169 -> 358
    //   530: aload 8
    //   532: ifnull -174 -> 358
    //   535: aload 8
    //   537: invokevirtual 209	java/io/File:delete	()Z
    //   540: pop
    //   541: goto -183 -> 358
    //   544: aload_1
    //   545: iconst_0
    //   546: aaload
    //   547: getstatic 49	miui/resourcebrowser/controller/local/ImportResourceTask:IMPORT_BY_DOWNLOAD_FOLDER	Ljava/lang/String;
    //   550: invokevirtual 114	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   553: ifeq +90 -> 643
    //   556: aload 6
    //   558: invokevirtual 105	java/io/File:listFiles	()[Ljava/io/File;
    //   561: astore 9
    //   563: aload 9
    //   565: arraylength
    //   566: istore 7
    //   568: iconst_0
    //   569: istore 8
    //   571: iload 8
    //   573: iload 7
    //   575: if_icmpge +526 -> 1101
    //   578: aload 9
    //   580: iload 8
    //   582: aaload
    //   583: astore 6
    //   585: aload 6
    //   587: invokevirtual 101	java/io/File:isDirectory	()Z
    //   590: ifeq +6 -> 596
    //   593: goto +691 -> 1284
    //   596: aload_0
    //   597: aload 6
    //   599: invokevirtual 212	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   602: invokestatic 218	miui/resourcebrowser/util/ResourceHelper:getFileHash	(Ljava/lang/String;)Ljava/lang/String;
    //   605: invokespecial 220	miui/resourcebrowser/controller/local/ImportResourceTask:checkResourceImported	(Ljava/lang/String;)Z
    //   608: ifne +676 -> 1284
    //   611: new 188	miui/resourcebrowser/model/Resource
    //   614: dup
    //   615: invokespecial 221	miui/resourcebrowser/model/Resource:<init>	()V
    //   618: astore 5
    //   620: aload 5
    //   622: aload 6
    //   624: invokevirtual 212	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   627: invokevirtual 224	miui/resourcebrowser/model/Resource:setDownloadPath	(Ljava/lang/String;)V
    //   630: aload_3
    //   631: aload 5
    //   633: aconst_null
    //   634: invokeinterface 206 3 0
    //   639: pop
    //   640: goto +644 -> 1284
    //   643: aload_1
    //   644: iconst_0
    //   645: aaload
    //   646: getstatic 37	miui/resourcebrowser/controller/local/ImportResourceTask:IMPORT_BY_ONLINE_ID	Ljava/lang/String;
    //   649: invokevirtual 114	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   652: ifeq +333 -> 985
    //   655: aload_0
    //   656: bipush 255
    //   658: aload_1
    //   659: arraylength
    //   660: iadd
    //   661: putfield 226	miui/resourcebrowser/controller/local/ImportResourceTask:mImportTotalNum	I
    //   664: iconst_1
    //   665: istore 9
    //   667: iload 9
    //   669: aload_1
    //   670: arraylength
    //   671: if_icmpge +430 -> 1101
    //   674: aload_1
    //   675: iload 9
    //   677: aaload
    //   678: astore 10
    //   680: aload_0
    //   681: getfield 71	miui/resourcebrowser/controller/local/ImportResourceTask:mResController	Lmiui/resourcebrowser/controller/ResourceController;
    //   684: invokevirtual 186	miui/resourcebrowser/controller/ResourceController:getOnlineDataManager	()Lmiui/resourcebrowser/controller/OnlineDataManager;
    //   687: aload 10
    //   689: invokevirtual 197	miui/resourcebrowser/controller/OnlineDataManager:getResource	(Ljava/lang/String;)Lmiui/resourcebrowser/model/Resource;
    //   692: astore 11
    //   694: aconst_null
    //   695: astore 12
    //   697: aconst_null
    //   698: astore 8
    //   700: aconst_null
    //   701: astore 7
    //   703: new 160	java/lang/StringBuilder
    //   706: dup
    //   707: invokespecial 161	java/lang/StringBuilder:<init>	()V
    //   710: aload 10
    //   712: invokevirtual 165	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   715: ldc 155
    //   717: invokevirtual 165	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   720: invokevirtual 168	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   723: astore 6
    //   725: aload 11
    //   727: ifnonnull +183 -> 910
    //   730: aload 5
    //   732: invokevirtual 105	java/io/File:listFiles	()[Ljava/io/File;
    //   735: astore 13
    //   737: aload 13
    //   739: arraylength
    //   740: istore 15
    //   742: iconst_0
    //   743: istore 14
    //   745: iload 14
    //   747: iload 15
    //   749: if_icmpge +52 -> 801
    //   752: aload 13
    //   754: iload 14
    //   756: aaload
    //   757: astore 16
    //   759: aload 16
    //   761: invokevirtual 108	java/io/File:getName	()Ljava/lang/String;
    //   764: aload 6
    //   766: invokevirtual 114	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   769: ifeq +96 -> 865
    //   772: aload 16
    //   774: astore 8
    //   776: aload_0
    //   777: getfield 78	miui/resourcebrowser/controller/local/ImportResourceTask:mDataParser	Lmiui/resourcebrowser/controller/local/LocalDataParser;
    //   780: aload 16
    //   782: invokevirtual 174	miui/resourcebrowser/controller/local/LocalDataParser:loadResource	(Ljava/io/File;)Lmiui/resourcebrowser/model/Resource;
    //   785: astore 11
    //   787: aload 11
    //   789: astore 11
    //   791: aload 8
    //   793: ifnull +497 -> 1290
    //   796: aload 7
    //   798: ifnull +492 -> 1290
    //   801: aload 11
    //   803: ifnull +56 -> 859
    //   806: aload 12
    //   808: ifnull +51 -> 859
    //   811: aload_0
    //   812: aload 11
    //   814: invokevirtual 229	miui/resourcebrowser/model/Resource:getDownloadPath	()Ljava/lang/String;
    //   817: invokestatic 218	miui/resourcebrowser/util/ResourceHelper:getFileHash	(Ljava/lang/String;)Ljava/lang/String;
    //   820: invokespecial 220	miui/resourcebrowser/controller/local/ImportResourceTask:checkResourceImported	(Ljava/lang/String;)Z
    //   823: ifne +489 -> 1312
    //   826: aload_3
    //   827: aload 11
    //   829: aload 12
    //   831: invokeinterface 206 3 0
    //   836: pop
    //   837: aload 8
    //   839: ifnull +9 -> 848
    //   842: aload 8
    //   844: invokevirtual 209	java/io/File:delete	()Z
    //   847: pop
    //   848: aload 7
    //   850: ifnull +9 -> 859
    //   853: aload 7
    //   855: invokevirtual 209	java/io/File:delete	()Z
    //   858: pop
    //   859: iinc 9 1
    //   862: goto -195 -> 667
    //   865: aload 16
    //   867: invokevirtual 108	java/io/File:getName	()Ljava/lang/String;
    //   870: aload 10
    //   872: invokevirtual 114	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   875: ifeq -84 -> 791
    //   878: aload 16
    //   880: astore 7
    //   882: aload_0
    //   883: getfield 83	miui/resourcebrowser/controller/local/ImportResourceTask:mContextParser	Lmiui/resourcebrowser/controller/local/ContextParser;
    //   886: aload 16
    //   888: invokevirtual 180	miui/resourcebrowser/controller/local/ContextParser:loadResourceContext	(Ljava/io/File;)Lmiui/resourcebrowser/ResourceContext;
    //   891: astore 12
    //   893: aload 12
    //   895: astore 12
    //   897: goto -106 -> 791
    //   900: pop
    //   901: aload 16
    //   903: invokevirtual 209	java/io/File:delete	()Z
    //   906: pop
    //   907: goto -106 -> 801
    //   910: aload_0
    //   911: getfield 65	miui/resourcebrowser/controller/local/ImportResourceTask:mResContext	Lmiui/resourcebrowser/ResourceContext;
    //   914: astore 12
    //   916: aload 5
    //   918: invokevirtual 105	java/io/File:listFiles	()[Ljava/io/File;
    //   921: astore 16
    //   923: aload 16
    //   925: arraylength
    //   926: istore 13
    //   928: iconst_0
    //   929: istore 15
    //   931: iload 15
    //   933: iload 13
    //   935: if_icmpge -134 -> 801
    //   938: aload 16
    //   940: iload 15
    //   942: aaload
    //   943: astore 14
    //   945: aload 14
    //   947: invokevirtual 108	java/io/File:getName	()Ljava/lang/String;
    //   950: aload 6
    //   952: invokevirtual 114	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   955: ifeq +10 -> 965
    //   958: aload 14
    //   960: astore 8
    //   962: goto +334 -> 1296
    //   965: aload 14
    //   967: invokevirtual 108	java/io/File:getName	()Ljava/lang/String;
    //   970: aload 10
    //   972: invokevirtual 114	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   975: ifeq +321 -> 1296
    //   978: aload 14
    //   980: astore 7
    //   982: goto +314 -> 1296
    //   985: aload_1
    //   986: iconst_0
    //   987: aaload
    //   988: getstatic 41	miui/resourcebrowser/controller/local/ImportResourceTask:IMPORT_BY_PATH	Ljava/lang/String;
    //   991: invokevirtual 114	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   994: ifeq +107 -> 1101
    //   997: aload_0
    //   998: bipush 255
    //   1000: aload_1
    //   1001: arraylength
    //   1002: iadd
    //   1003: putfield 226	miui/resourcebrowser/controller/local/ImportResourceTask:mImportTotalNum	I
    //   1006: iconst_1
    //   1007: istore 5
    //   1009: iload 5
    //   1011: aload_1
    //   1012: arraylength
    //   1013: if_icmpge +88 -> 1101
    //   1016: aload_1
    //   1017: iload 5
    //   1019: aaload
    //   1020: astore 7
    //   1022: aload_0
    //   1023: aload 7
    //   1025: invokestatic 218	miui/resourcebrowser/util/ResourceHelper:getFileHash	(Ljava/lang/String;)Ljava/lang/String;
    //   1028: invokespecial 220	miui/resourcebrowser/controller/local/ImportResourceTask:checkResourceImported	(Ljava/lang/String;)Z
    //   1031: ifne +287 -> 1318
    //   1034: new 160	java/lang/StringBuilder
    //   1037: dup
    //   1038: invokespecial 161	java/lang/StringBuilder:<init>	()V
    //   1041: aload_0
    //   1042: getfield 65	miui/resourcebrowser/controller/local/ImportResourceTask:mResContext	Lmiui/resourcebrowser/ResourceContext;
    //   1045: invokevirtual 136	miui/resourcebrowser/ResourceContext:getDownloadFolder	()Ljava/lang/String;
    //   1048: invokevirtual 165	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1051: aload 7
    //   1053: invokestatic 234	miui/os/ExtraFileUtils:getFileName	(Ljava/lang/String;)Ljava/lang/String;
    //   1056: invokevirtual 165	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1059: invokevirtual 168	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1062: astore 6
    //   1064: aload 7
    //   1066: aload 6
    //   1068: invokestatic 240	miui/os/Shell:copy	(Ljava/lang/String;Ljava/lang/String;)Z
    //   1071: pop
    //   1072: new 188	miui/resourcebrowser/model/Resource
    //   1075: dup
    //   1076: invokespecial 221	miui/resourcebrowser/model/Resource:<init>	()V
    //   1079: astore 7
    //   1081: aload 7
    //   1083: aload 6
    //   1085: invokevirtual 224	miui/resourcebrowser/model/Resource:setDownloadPath	(Ljava/lang/String;)V
    //   1088: aload_3
    //   1089: aload 7
    //   1091: aconst_null
    //   1092: invokeinterface 206 3 0
    //   1097: pop
    //   1098: goto +220 -> 1318
    //   1101: aload_3
    //   1102: invokeinterface 244 1 0
    //   1107: istore 8
    //   1109: aload_0
    //   1110: iload 8
    //   1112: putfield 226	miui/resourcebrowser/controller/local/ImportResourceTask:mImportTotalNum	I
    //   1115: iconst_1
    //   1116: istore 7
    //   1118: aload_3
    //   1119: invokeinterface 248 1 0
    //   1124: invokeinterface 254 1 0
    //   1129: astore 5
    //   1131: aload 5
    //   1133: invokeinterface 259 1 0
    //   1138: ifeq +110 -> 1248
    //   1141: aload 5
    //   1143: invokeinterface 263 1 0
    //   1148: checkcast 188	miui/resourcebrowser/model/Resource
    //   1151: astore 6
    //   1153: iconst_2
    //   1154: anewarray 123	java/lang/Integer
    //   1157: astore 9
    //   1159: aload 9
    //   1161: iconst_0
    //   1162: iload 7
    //   1164: invokestatic 127	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   1167: aastore
    //   1168: aload 9
    //   1170: iconst_1
    //   1171: iload 8
    //   1173: invokestatic 127	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   1176: aastore
    //   1177: aload_0
    //   1178: aload 9
    //   1180: invokevirtual 267	miui/resourcebrowser/controller/local/ImportResourceTask:publishProgress	([Ljava/lang/Object;)V
    //   1183: iinc 7 1
    //   1186: aload_3
    //   1187: aload 6
    //   1189: invokeinterface 271 2 0
    //   1194: checkcast 89	miui/resourcebrowser/ResourceContext
    //   1197: astore 9
    //   1199: aload 9
    //   1201: ifnull +26 -> 1227
    //   1204: aload_0
    //   1205: getfield 71	miui/resourcebrowser/controller/local/ImportResourceTask:mResController	Lmiui/resourcebrowser/controller/ResourceController;
    //   1208: invokevirtual 275	miui/resourcebrowser/controller/ResourceController:getImportManager	()Lmiui/resourcebrowser/controller/ImportManager;
    //   1211: aload 6
    //   1213: aload 9
    //   1215: invokevirtual 281	miui/resourcebrowser/controller/ImportManager:importResource	(Lmiui/resourcebrowser/model/Resource;Lmiui/resourcebrowser/ResourceContext;)Z
    //   1218: ifeq -87 -> 1131
    //   1221: iinc 4 1
    //   1224: goto -93 -> 1131
    //   1227: aload_0
    //   1228: getfield 71	miui/resourcebrowser/controller/local/ImportResourceTask:mResController	Lmiui/resourcebrowser/controller/ResourceController;
    //   1231: invokevirtual 275	miui/resourcebrowser/controller/ResourceController:getImportManager	()Lmiui/resourcebrowser/controller/ImportManager;
    //   1234: aload 6
    //   1236: invokevirtual 284	miui/resourcebrowser/controller/ImportManager:importResource	(Lmiui/resourcebrowser/model/Resource;)Z
    //   1239: ifeq -108 -> 1131
    //   1242: iinc 4 1
    //   1245: goto -114 -> 1131
    //   1248: aload_2
    //   1249: monitorexit
    //   1250: iload 4
    //   1252: invokestatic 127	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   1255: astore_3
    //   1256: goto -1240 -> 16
    //   1259: iinc 7 1
    //   1262: goto -1068 -> 194
    //   1265: iinc 17 1
    //   1268: goto -999 -> 269
    //   1271: iinc 15 1
    //   1274: goto -826 -> 448
    //   1277: aload 14
    //   1279: astore 12
    //   1281: goto -891 -> 390
    //   1284: iinc 8 1
    //   1287: goto -716 -> 571
    //   1290: iinc 14 1
    //   1293: goto -548 -> 745
    //   1296: aload 8
    //   1298: ifnull +8 -> 1306
    //   1301: aload 7
    //   1303: ifnonnull -502 -> 801
    //   1306: iinc 15 1
    //   1309: goto -378 -> 931
    //   1312: iinc 4 1
    //   1315: goto -478 -> 837
    //   1318: iinc 5 1
    //   1321: goto -312 -> 1009
    //
    // Exception table:
    //   from	to	target	type
    //   39	58	55	finally
    //   60	313	55	finally
    //   332	354	55	finally
    //   368	759	55	finally
    //   759	787	55	finally
    //   811	859	55	finally
    //   865	893	55	finally
    //   901	1250	55	finally
    //   332	354	500	miui/resourcebrowser/controller/local/PersistenceException
    //   759	787	900	miui/resourcebrowser/controller/local/PersistenceException
    //   865	893	900	miui/resourcebrowser/controller/local/PersistenceException
  }

  public int getImportTotalNum()
  {
    return this.mImportTotalNum;
  }

  protected boolean hasEquivalentRunningTasks()
  {
    return false;
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.controller.local.ImportResourceTask
 * JD-Core Version:    0.6.0
 */