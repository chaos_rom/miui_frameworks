package miui.resourcebrowser.controller.local;

import java.io.File;
import miui.resourcebrowser.ResourceContext;

public abstract class ContextParser
{
  public abstract ResourceContext loadResourceContext(File paramFile)
    throws PersistenceException;

  public abstract void storeResourceContext(File paramFile, ResourceContext paramResourceContext)
    throws PersistenceException;
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.controller.local.ContextParser
 * JD-Core Version:    0.6.0
 */