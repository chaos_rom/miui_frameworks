package miui.resourcebrowser.controller.local;

import android.util.JsonReader;
import android.util.JsonToken;
import android.util.JsonWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import miui.resourcebrowser.ResourceContext;
import miui.resourcebrowser.model.PathEntry;
import miui.resourcebrowser.model.RelatedResource;

public class LocalJSONDataParser extends LocalDataParser
{
  public LocalJSONDataParser(ResourceContext paramResourceContext)
  {
    super(paramResourceContext);
  }

  private List<String> readBuildInImagePaths(JsonReader paramJsonReader)
    throws IOException
  {
    ArrayList localArrayList = new ArrayList();
    paramJsonReader.beginArray();
    while (true)
    {
      if (!paramJsonReader.hasNext())
      {
        paramJsonReader.endArray();
        return localArrayList;
      }
      localArrayList.add(paramJsonReader.nextString());
    }
  }

  private Map<String, String> readExtraMeta(JsonReader paramJsonReader)
    throws IOException
  {
    HashMap localHashMap = new HashMap();
    paramJsonReader.beginObject();
    while (true)
    {
      if (!paramJsonReader.hasNext())
      {
        paramJsonReader.endObject();
        return localHashMap;
      }
      String str = paramJsonReader.nextName();
      if (paramJsonReader.peek() == JsonToken.NULL)
      {
        paramJsonReader.skipValue();
        continue;
      }
      localHashMap.put(str, paramJsonReader.nextString());
    }
  }

  private List<PathEntry> readImagePaths(JsonReader paramJsonReader)
    throws IOException
  {
    ArrayList localArrayList = new ArrayList();
    paramJsonReader.beginArray();
    if (!paramJsonReader.hasNext())
    {
      paramJsonReader.endArray();
      return localArrayList;
    }
    paramJsonReader.beginObject();
    PathEntry localPathEntry = new PathEntry();
    while (true)
    {
      if (!paramJsonReader.hasNext())
      {
        localArrayList.add(localPathEntry);
        paramJsonReader.endObject();
        break;
      }
      String str = paramJsonReader.nextName();
      if ((!"localPath".equals(str)) || (paramJsonReader.peek() == JsonToken.NULL))
      {
        if ((!"onlinePath".equals(str)) || (paramJsonReader.peek() == JsonToken.NULL))
        {
          paramJsonReader.skipValue();
          continue;
        }
        localPathEntry.setOnlinePath(paramJsonReader.nextString());
        continue;
      }
      localPathEntry.setLocalPath(paramJsonReader.nextString());
    }
  }

  private List<RelatedResource> readRelatedResource(JsonReader paramJsonReader)
    throws IOException
  {
    ArrayList localArrayList = new ArrayList();
    paramJsonReader.beginArray();
    if (!paramJsonReader.hasNext())
    {
      paramJsonReader.endArray();
      return localArrayList;
    }
    paramJsonReader.beginObject();
    RelatedResource localRelatedResource = new RelatedResource();
    while (true)
    {
      if (!paramJsonReader.hasNext())
      {
        localArrayList.add(localRelatedResource);
        paramJsonReader.endObject();
        break;
      }
      String str = paramJsonReader.nextName();
      if ((!"localPath".equals(str)) || (paramJsonReader.peek() == JsonToken.NULL))
      {
        if ((!"filePath".equals(str)) || (paramJsonReader.peek() == JsonToken.NULL))
        {
          if ((!"resourceCode".equals(str)) || (paramJsonReader.peek() == JsonToken.NULL))
          {
            if ((!"extraMeta".equals(str)) || (paramJsonReader.peek() == JsonToken.NULL))
            {
              paramJsonReader.skipValue();
              continue;
            }
            localRelatedResource.setExtraMeta(readExtraMeta(paramJsonReader));
            continue;
          }
          localRelatedResource.setResourceCode(paramJsonReader.nextString());
          continue;
        }
        localRelatedResource.setFilePath(paramJsonReader.nextString());
        continue;
      }
      localRelatedResource.setLocalPath(paramJsonReader.nextString());
    }
  }

  private void writeBuildInImagePaths(JsonWriter paramJsonWriter, List<String> paramList)
    throws IOException
  {
    paramJsonWriter.beginArray();
    Iterator localIterator = paramList.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
      {
        paramJsonWriter.endArray();
        return;
      }
      paramJsonWriter.value((String)localIterator.next());
    }
  }

  private void writeExtraMeta(JsonWriter paramJsonWriter, Map<String, String> paramMap)
    throws IOException
  {
    paramJsonWriter.beginObject();
    Iterator localIterator = paramMap.keySet().iterator();
    while (true)
    {
      if (!localIterator.hasNext())
      {
        paramJsonWriter.endObject();
        return;
      }
      String str = (String)localIterator.next();
      paramJsonWriter.name(str).value((String)paramMap.get(str));
    }
  }

  private void writeImagePaths(JsonWriter paramJsonWriter, List<PathEntry> paramList)
    throws IOException
  {
    paramJsonWriter.beginArray();
    Iterator localIterator = paramList.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
      {
        paramJsonWriter.endArray();
        return;
      }
      PathEntry localPathEntry = (PathEntry)localIterator.next();
      paramJsonWriter.beginObject();
      paramJsonWriter.name("localPath").value(localPathEntry.getLocalPath());
      paramJsonWriter.name("onlinePath").value(localPathEntry.getOnlinePath());
      paramJsonWriter.endObject();
    }
  }

  private void writeRelatedResources(JsonWriter paramJsonWriter, List<RelatedResource> paramList)
    throws IOException
  {
    paramJsonWriter.beginArray();
    Iterator localIterator = paramList.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
      {
        paramJsonWriter.endArray();
        return;
      }
      RelatedResource localRelatedResource = (RelatedResource)localIterator.next();
      paramJsonWriter.beginObject();
      paramJsonWriter.name("localPath").value(localRelatedResource.getLocalPath());
      paramJsonWriter.name("filePath").value(localRelatedResource.getFilePath());
      paramJsonWriter.name("resourceCode").value(localRelatedResource.getResourceCode());
      paramJsonWriter.name("extraMeta");
      writeExtraMeta(paramJsonWriter, localRelatedResource.getExtraMeta());
      paramJsonWriter.endObject();
    }
  }

  // ERROR //
  public miui.resourcebrowser.model.Resource loadResource(java.io.File paramFile)
    throws PersistenceException
  {
    // Byte code:
    //   0: new 186	miui/resourcebrowser/model/Resource
    //   3: dup
    //   4: invokespecial 187	miui/resourcebrowser/model/Resource:<init>	()V
    //   7: astore_2
    //   8: aconst_null
    //   9: astore_3
    //   10: new 19	android/util/JsonReader
    //   13: dup
    //   14: new 189	java/io/BufferedReader
    //   17: dup
    //   18: new 191	java/io/FileReader
    //   21: dup
    //   22: aload_1
    //   23: invokespecial 194	java/io/FileReader:<init>	(Ljava/io/File;)V
    //   26: invokespecial 197	java/io/BufferedReader:<init>	(Ljava/io/Reader;)V
    //   29: invokespecial 198	android/util/JsonReader:<init>	(Ljava/io/Reader;)V
    //   32: astore_3
    //   33: aload_3
    //   34: invokevirtual 47	android/util/JsonReader:beginObject	()V
    //   37: aload_3
    //   38: invokevirtual 26	android/util/JsonReader:hasNext	()Z
    //   41: ifeq +869 -> 910
    //   44: aload_3
    //   45: invokevirtual 53	android/util/JsonReader:nextName	()Ljava/lang/String;
    //   48: astore 4
    //   50: ldc 200
    //   52: aload 4
    //   54: invokevirtual 83	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   57: ifeq +50 -> 107
    //   60: aload_3
    //   61: invokevirtual 57	android/util/JsonReader:peek	()Landroid/util/JsonToken;
    //   64: getstatic 63	android/util/JsonToken:NULL	Landroid/util/JsonToken;
    //   67: if_acmpeq +40 -> 107
    //   70: aload_2
    //   71: aload_3
    //   72: invokevirtual 33	android/util/JsonReader:nextString	()Ljava/lang/String;
    //   75: invokevirtual 203	miui/resourcebrowser/model/Resource:setLocalId	(Ljava/lang/String;)V
    //   78: goto -41 -> 37
    //   81: astore_2
    //   82: aload_3
    //   83: astore_3
    //   84: new 184	miui/resourcebrowser/controller/local/PersistenceException
    //   87: dup
    //   88: aload_2
    //   89: invokevirtual 206	java/io/IOException:getMessage	()Ljava/lang/String;
    //   92: invokespecial 208	miui/resourcebrowser/controller/local/PersistenceException:<init>	(Ljava/lang/String;)V
    //   95: athrow
    //   96: astore_2
    //   97: aload_3
    //   98: ifnull +7 -> 105
    //   101: aload_3
    //   102: invokevirtual 211	android/util/JsonReader:close	()V
    //   105: aload_2
    //   106: athrow
    //   107: ldc 213
    //   109: aload 4
    //   111: invokevirtual 83	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   114: ifeq +24 -> 138
    //   117: aload_3
    //   118: invokevirtual 57	android/util/JsonReader:peek	()Landroid/util/JsonToken;
    //   121: getstatic 63	android/util/JsonToken:NULL	Landroid/util/JsonToken;
    //   124: if_acmpeq +14 -> 138
    //   127: aload_2
    //   128: aload_3
    //   129: invokevirtual 33	android/util/JsonReader:nextString	()Ljava/lang/String;
    //   132: invokevirtual 216	miui/resourcebrowser/model/Resource:setOnlineId	(Ljava/lang/String;)V
    //   135: goto -98 -> 37
    //   138: ldc 218
    //   140: aload 4
    //   142: invokevirtual 83	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   145: ifeq +24 -> 169
    //   148: aload_3
    //   149: invokevirtual 57	android/util/JsonReader:peek	()Landroid/util/JsonToken;
    //   152: getstatic 63	android/util/JsonToken:NULL	Landroid/util/JsonToken;
    //   155: if_acmpeq +14 -> 169
    //   158: aload_2
    //   159: aload_3
    //   160: invokevirtual 33	android/util/JsonReader:nextString	()Ljava/lang/String;
    //   163: invokevirtual 221	miui/resourcebrowser/model/Resource:setProductId	(Ljava/lang/String;)V
    //   166: goto -129 -> 37
    //   169: ldc 78
    //   171: aload 4
    //   173: invokevirtual 83	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   176: ifeq +24 -> 200
    //   179: aload_3
    //   180: invokevirtual 57	android/util/JsonReader:peek	()Landroid/util/JsonToken;
    //   183: getstatic 63	android/util/JsonToken:NULL	Landroid/util/JsonToken;
    //   186: if_acmpeq +14 -> 200
    //   189: aload_2
    //   190: aload_3
    //   191: invokevirtual 33	android/util/JsonReader:nextString	()Ljava/lang/String;
    //   194: invokevirtual 222	miui/resourcebrowser/model/Resource:setLocalPath	(Ljava/lang/String;)V
    //   197: goto -160 -> 37
    //   200: ldc 85
    //   202: aload 4
    //   204: invokevirtual 83	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   207: ifeq +24 -> 231
    //   210: aload_3
    //   211: invokevirtual 57	android/util/JsonReader:peek	()Landroid/util/JsonToken;
    //   214: getstatic 63	android/util/JsonToken:NULL	Landroid/util/JsonToken;
    //   217: if_acmpeq +14 -> 231
    //   220: aload_2
    //   221: aload_3
    //   222: invokevirtual 33	android/util/JsonReader:nextString	()Ljava/lang/String;
    //   225: invokevirtual 223	miui/resourcebrowser/model/Resource:setOnlinePath	(Ljava/lang/String;)V
    //   228: goto -191 -> 37
    //   231: ldc 98
    //   233: aload 4
    //   235: invokevirtual 83	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   238: ifeq +24 -> 262
    //   241: aload_3
    //   242: invokevirtual 57	android/util/JsonReader:peek	()Landroid/util/JsonToken;
    //   245: getstatic 63	android/util/JsonToken:NULL	Landroid/util/JsonToken;
    //   248: if_acmpeq +14 -> 262
    //   251: aload_2
    //   252: aload_3
    //   253: invokevirtual 33	android/util/JsonReader:nextString	()Ljava/lang/String;
    //   256: invokevirtual 224	miui/resourcebrowser/model/Resource:setFilePath	(Ljava/lang/String;)V
    //   259: goto -222 -> 37
    //   262: ldc 226
    //   264: aload 4
    //   266: invokevirtual 83	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   269: ifeq +24 -> 293
    //   272: aload_3
    //   273: invokevirtual 57	android/util/JsonReader:peek	()Landroid/util/JsonToken;
    //   276: getstatic 63	android/util/JsonToken:NULL	Landroid/util/JsonToken;
    //   279: if_acmpeq +14 -> 293
    //   282: aload_2
    //   283: aload_3
    //   284: invokevirtual 33	android/util/JsonReader:nextString	()Ljava/lang/String;
    //   287: invokevirtual 229	miui/resourcebrowser/model/Resource:setDownloadPath	(Ljava/lang/String;)V
    //   290: goto -253 -> 37
    //   293: ldc 231
    //   295: aload 4
    //   297: invokevirtual 83	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   300: ifeq +24 -> 324
    //   303: aload_3
    //   304: invokevirtual 57	android/util/JsonReader:peek	()Landroid/util/JsonToken;
    //   307: getstatic 63	android/util/JsonToken:NULL	Landroid/util/JsonToken;
    //   310: if_acmpeq +14 -> 324
    //   313: aload_2
    //   314: aload_3
    //   315: invokevirtual 33	android/util/JsonReader:nextString	()Ljava/lang/String;
    //   318: invokevirtual 234	miui/resourcebrowser/model/Resource:setRightsPath	(Ljava/lang/String;)V
    //   321: goto -284 -> 37
    //   324: ldc 236
    //   326: aload 4
    //   328: invokevirtual 83	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   331: ifeq +24 -> 355
    //   334: aload_3
    //   335: invokevirtual 57	android/util/JsonReader:peek	()Landroid/util/JsonToken;
    //   338: getstatic 63	android/util/JsonToken:NULL	Landroid/util/JsonToken;
    //   341: if_acmpeq +14 -> 355
    //   344: aload_2
    //   345: aload_3
    //   346: invokevirtual 240	android/util/JsonReader:nextInt	()I
    //   349: invokevirtual 244	miui/resourcebrowser/model/Resource:setPlatform	(I)V
    //   352: goto -315 -> 37
    //   355: ldc 246
    //   357: aload 4
    //   359: invokevirtual 83	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   362: ifeq +24 -> 386
    //   365: aload_3
    //   366: invokevirtual 57	android/util/JsonReader:peek	()Landroid/util/JsonToken;
    //   369: getstatic 63	android/util/JsonToken:NULL	Landroid/util/JsonToken;
    //   372: if_acmpeq +14 -> 386
    //   375: aload_2
    //   376: aload_3
    //   377: invokevirtual 240	android/util/JsonReader:nextInt	()I
    //   380: invokevirtual 249	miui/resourcebrowser/model/Resource:setStatus	(I)V
    //   383: goto -346 -> 37
    //   386: ldc 251
    //   388: aload 4
    //   390: invokevirtual 83	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   393: ifeq +24 -> 417
    //   396: aload_3
    //   397: invokevirtual 57	android/util/JsonReader:peek	()Landroid/util/JsonToken;
    //   400: getstatic 63	android/util/JsonToken:NULL	Landroid/util/JsonToken;
    //   403: if_acmpeq +14 -> 417
    //   406: aload_2
    //   407: aload_3
    //   408: invokevirtual 33	android/util/JsonReader:nextString	()Ljava/lang/String;
    //   411: invokevirtual 254	miui/resourcebrowser/model/Resource:setHash	(Ljava/lang/String;)V
    //   414: goto -377 -> 37
    //   417: ldc_w 256
    //   420: aload 4
    //   422: invokevirtual 83	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   425: ifeq +24 -> 449
    //   428: aload_3
    //   429: invokevirtual 57	android/util/JsonReader:peek	()Landroid/util/JsonToken;
    //   432: getstatic 63	android/util/JsonToken:NULL	Landroid/util/JsonToken;
    //   435: if_acmpeq +14 -> 449
    //   438: aload_2
    //   439: aload_3
    //   440: invokevirtual 260	android/util/JsonReader:nextLong	()J
    //   443: invokevirtual 264	miui/resourcebrowser/model/Resource:setSize	(J)V
    //   446: goto -409 -> 37
    //   449: ldc_w 266
    //   452: aload 4
    //   454: invokevirtual 83	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   457: ifeq +24 -> 481
    //   460: aload_3
    //   461: invokevirtual 57	android/util/JsonReader:peek	()Landroid/util/JsonToken;
    //   464: getstatic 63	android/util/JsonToken:NULL	Landroid/util/JsonToken;
    //   467: if_acmpeq +14 -> 481
    //   470: aload_2
    //   471: aload_3
    //   472: invokevirtual 260	android/util/JsonReader:nextLong	()J
    //   475: invokevirtual 269	miui/resourcebrowser/model/Resource:setUpdatedTime	(J)V
    //   478: goto -441 -> 37
    //   481: ldc_w 271
    //   484: aload 4
    //   486: invokevirtual 83	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   489: ifeq +24 -> 513
    //   492: aload_3
    //   493: invokevirtual 57	android/util/JsonReader:peek	()Landroid/util/JsonToken;
    //   496: getstatic 63	android/util/JsonToken:NULL	Landroid/util/JsonToken;
    //   499: if_acmpeq +14 -> 513
    //   502: aload_2
    //   503: aload_3
    //   504: invokevirtual 33	android/util/JsonReader:nextString	()Ljava/lang/String;
    //   507: invokevirtual 274	miui/resourcebrowser/model/Resource:setTitle	(Ljava/lang/String;)V
    //   510: goto -473 -> 37
    //   513: ldc_w 276
    //   516: aload 4
    //   518: invokevirtual 83	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   521: ifeq +24 -> 545
    //   524: aload_3
    //   525: invokevirtual 57	android/util/JsonReader:peek	()Landroid/util/JsonToken;
    //   528: getstatic 63	android/util/JsonToken:NULL	Landroid/util/JsonToken;
    //   531: if_acmpeq +14 -> 545
    //   534: aload_2
    //   535: aload_3
    //   536: invokevirtual 33	android/util/JsonReader:nextString	()Ljava/lang/String;
    //   539: invokevirtual 279	miui/resourcebrowser/model/Resource:setDescription	(Ljava/lang/String;)V
    //   542: goto -505 -> 37
    //   545: ldc_w 281
    //   548: aload 4
    //   550: invokevirtual 83	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   553: ifeq +24 -> 577
    //   556: aload_3
    //   557: invokevirtual 57	android/util/JsonReader:peek	()Landroid/util/JsonToken;
    //   560: getstatic 63	android/util/JsonToken:NULL	Landroid/util/JsonToken;
    //   563: if_acmpeq +14 -> 577
    //   566: aload_2
    //   567: aload_3
    //   568: invokevirtual 33	android/util/JsonReader:nextString	()Ljava/lang/String;
    //   571: invokevirtual 284	miui/resourcebrowser/model/Resource:setAuthor	(Ljava/lang/String;)V
    //   574: goto -537 -> 37
    //   577: ldc_w 286
    //   580: aload 4
    //   582: invokevirtual 83	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   585: ifeq +24 -> 609
    //   588: aload_3
    //   589: invokevirtual 57	android/util/JsonReader:peek	()Landroid/util/JsonToken;
    //   592: getstatic 63	android/util/JsonToken:NULL	Landroid/util/JsonToken;
    //   595: if_acmpeq +14 -> 609
    //   598: aload_2
    //   599: aload_3
    //   600: invokevirtual 33	android/util/JsonReader:nextString	()Ljava/lang/String;
    //   603: invokevirtual 289	miui/resourcebrowser/model/Resource:setDesigner	(Ljava/lang/String;)V
    //   606: goto -569 -> 37
    //   609: ldc_w 291
    //   612: aload 4
    //   614: invokevirtual 83	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   617: ifeq +24 -> 641
    //   620: aload_3
    //   621: invokevirtual 57	android/util/JsonReader:peek	()Landroid/util/JsonToken;
    //   624: getstatic 63	android/util/JsonToken:NULL	Landroid/util/JsonToken;
    //   627: if_acmpeq +14 -> 641
    //   630: aload_2
    //   631: aload_3
    //   632: invokevirtual 33	android/util/JsonReader:nextString	()Ljava/lang/String;
    //   635: invokevirtual 294	miui/resourcebrowser/model/Resource:setVersion	(Ljava/lang/String;)V
    //   638: goto -601 -> 37
    //   641: ldc_w 296
    //   644: aload 4
    //   646: invokevirtual 83	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   649: ifeq +24 -> 673
    //   652: aload_3
    //   653: invokevirtual 57	android/util/JsonReader:peek	()Landroid/util/JsonToken;
    //   656: getstatic 63	android/util/JsonToken:NULL	Landroid/util/JsonToken;
    //   659: if_acmpeq +14 -> 673
    //   662: aload_2
    //   663: aload_3
    //   664: invokevirtual 33	android/util/JsonReader:nextString	()Ljava/lang/String;
    //   667: invokevirtual 299	miui/resourcebrowser/model/Resource:setDownloadCount	(Ljava/lang/String;)V
    //   670: goto -633 -> 37
    //   673: ldc_w 301
    //   676: aload 4
    //   678: invokevirtual 83	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   681: ifeq +25 -> 706
    //   684: aload_3
    //   685: invokevirtual 57	android/util/JsonReader:peek	()Landroid/util/JsonToken;
    //   688: getstatic 63	android/util/JsonToken:NULL	Landroid/util/JsonToken;
    //   691: if_acmpeq +15 -> 706
    //   694: aload_2
    //   695: aload_0
    //   696: aload_3
    //   697: invokespecial 303	miui/resourcebrowser/controller/local/LocalJSONDataParser:readBuildInImagePaths	(Landroid/util/JsonReader;)Ljava/util/List;
    //   700: invokevirtual 307	miui/resourcebrowser/model/Resource:setBuildInThumbnails	(Ljava/util/List;)V
    //   703: goto -666 -> 37
    //   706: ldc_w 309
    //   709: aload 4
    //   711: invokevirtual 83	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   714: ifeq +25 -> 739
    //   717: aload_3
    //   718: invokevirtual 57	android/util/JsonReader:peek	()Landroid/util/JsonToken;
    //   721: getstatic 63	android/util/JsonToken:NULL	Landroid/util/JsonToken;
    //   724: if_acmpeq +15 -> 739
    //   727: aload_2
    //   728: aload_0
    //   729: aload_3
    //   730: invokespecial 303	miui/resourcebrowser/controller/local/LocalJSONDataParser:readBuildInImagePaths	(Landroid/util/JsonReader;)Ljava/util/List;
    //   733: invokevirtual 312	miui/resourcebrowser/model/Resource:setBuildInPreviews	(Ljava/util/List;)V
    //   736: goto -699 -> 37
    //   739: ldc_w 314
    //   742: aload 4
    //   744: invokevirtual 83	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   747: ifeq +25 -> 772
    //   750: aload_3
    //   751: invokevirtual 57	android/util/JsonReader:peek	()Landroid/util/JsonToken;
    //   754: getstatic 63	android/util/JsonToken:NULL	Landroid/util/JsonToken;
    //   757: if_acmpeq +15 -> 772
    //   760: aload_2
    //   761: aload_0
    //   762: aload_3
    //   763: invokespecial 316	miui/resourcebrowser/controller/local/LocalJSONDataParser:readImagePaths	(Landroid/util/JsonReader;)Ljava/util/List;
    //   766: invokevirtual 319	miui/resourcebrowser/model/Resource:setThumbnails	(Ljava/util/List;)V
    //   769: goto -732 -> 37
    //   772: ldc_w 321
    //   775: aload 4
    //   777: invokevirtual 83	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   780: ifeq +25 -> 805
    //   783: aload_3
    //   784: invokevirtual 57	android/util/JsonReader:peek	()Landroid/util/JsonToken;
    //   787: getstatic 63	android/util/JsonToken:NULL	Landroid/util/JsonToken;
    //   790: if_acmpeq +15 -> 805
    //   793: aload_2
    //   794: aload_0
    //   795: aload_3
    //   796: invokespecial 316	miui/resourcebrowser/controller/local/LocalJSONDataParser:readImagePaths	(Landroid/util/JsonReader;)Ljava/util/List;
    //   799: invokevirtual 324	miui/resourcebrowser/model/Resource:setPreviews	(Ljava/util/List;)V
    //   802: goto -765 -> 37
    //   805: ldc_w 326
    //   808: aload 4
    //   810: invokevirtual 83	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   813: ifeq +25 -> 838
    //   816: aload_3
    //   817: invokevirtual 57	android/util/JsonReader:peek	()Landroid/util/JsonToken;
    //   820: getstatic 63	android/util/JsonToken:NULL	Landroid/util/JsonToken;
    //   823: if_acmpeq +15 -> 838
    //   826: aload_2
    //   827: aload_0
    //   828: aload_3
    //   829: invokespecial 328	miui/resourcebrowser/controller/local/LocalJSONDataParser:readRelatedResource	(Landroid/util/JsonReader;)Ljava/util/List;
    //   832: invokevirtual 331	miui/resourcebrowser/model/Resource:setParentResources	(Ljava/util/List;)V
    //   835: goto -798 -> 37
    //   838: ldc_w 333
    //   841: aload 4
    //   843: invokevirtual 83	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   846: ifeq +25 -> 871
    //   849: aload_3
    //   850: invokevirtual 57	android/util/JsonReader:peek	()Landroid/util/JsonToken;
    //   853: getstatic 63	android/util/JsonToken:NULL	Landroid/util/JsonToken;
    //   856: if_acmpeq +15 -> 871
    //   859: aload_2
    //   860: aload_0
    //   861: aload_3
    //   862: invokespecial 328	miui/resourcebrowser/controller/local/LocalJSONDataParser:readRelatedResource	(Landroid/util/JsonReader;)Ljava/util/List;
    //   865: invokevirtual 336	miui/resourcebrowser/model/Resource:setSubResources	(Ljava/util/List;)V
    //   868: goto -831 -> 37
    //   871: ldc 102
    //   873: aload 4
    //   875: invokevirtual 83	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   878: ifeq +25 -> 903
    //   881: aload_3
    //   882: invokevirtual 57	android/util/JsonReader:peek	()Landroid/util/JsonToken;
    //   885: getstatic 63	android/util/JsonToken:NULL	Landroid/util/JsonToken;
    //   888: if_acmpeq +15 -> 903
    //   891: aload_2
    //   892: aload_0
    //   893: aload_3
    //   894: invokespecial 104	miui/resourcebrowser/controller/local/LocalJSONDataParser:readExtraMeta	(Landroid/util/JsonReader;)Ljava/util/Map;
    //   897: invokevirtual 337	miui/resourcebrowser/model/Resource:setExtraMeta	(Ljava/util/Map;)V
    //   900: goto -863 -> 37
    //   903: aload_3
    //   904: invokevirtual 66	android/util/JsonReader:skipValue	()V
    //   907: goto -870 -> 37
    //   910: aload_3
    //   911: invokevirtual 50	android/util/JsonReader:endObject	()V
    //   914: aload_3
    //   915: ifnull +7 -> 922
    //   918: aload_3
    //   919: invokevirtual 211	android/util/JsonReader:close	()V
    //   922: aload_2
    //   923: invokevirtual 338	miui/resourcebrowser/model/Resource:getOnlinePath	()Ljava/lang/String;
    //   926: invokestatic 344	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   929: ifeq +38 -> 967
    //   932: aload_2
    //   933: invokevirtual 347	miui/resourcebrowser/model/Resource:getOnlineId	()Ljava/lang/String;
    //   936: invokestatic 344	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   939: ifne +28 -> 967
    //   942: aload_2
    //   943: new 349	miui/resourcebrowser/controller/online/OnlineService
    //   946: dup
    //   947: aload_0
    //   948: getfield 353	miui/resourcebrowser/controller/local/LocalDataParser:context	Lmiui/resourcebrowser/ResourceContext;
    //   951: invokespecial 354	miui/resourcebrowser/controller/online/OnlineService:<init>	(Lmiui/resourcebrowser/ResourceContext;)V
    //   954: aload_2
    //   955: invokevirtual 347	miui/resourcebrowser/model/Resource:getOnlineId	()Ljava/lang/String;
    //   958: invokevirtual 358	miui/resourcebrowser/controller/online/OnlineService:getDownloadUrl	(Ljava/lang/String;)Lmiui/resourcebrowser/controller/online/RequestUrl;
    //   961: invokevirtual 363	miui/resourcebrowser/controller/online/RequestUrl:getUrl	()Ljava/lang/String;
    //   964: invokevirtual 223	miui/resourcebrowser/model/Resource:setOnlinePath	(Ljava/lang/String;)V
    //   967: aload_2
    //   968: areturn
    //   969: astore_3
    //   970: aload_3
    //   971: invokevirtual 366	java/io/IOException:printStackTrace	()V
    //   974: goto -52 -> 922
    //   977: astore_3
    //   978: aload_3
    //   979: invokevirtual 366	java/io/IOException:printStackTrace	()V
    //   982: goto -877 -> 105
    //   985: astore_2
    //   986: goto -902 -> 84
    //   989: astore_2
    //   990: aload_3
    //   991: astore_3
    //   992: goto -895 -> 97
    //
    // Exception table:
    //   from	to	target	type
    //   33	78	81	java/io/IOException
    //   107	914	81	java/io/IOException
    //   10	33	96	finally
    //   84	96	96	finally
    //   918	922	969	java/io/IOException
    //   101	105	977	java/io/IOException
    //   10	33	985	java/io/IOException
    //   33	78	989	finally
    //   107	914	989	finally
  }

  // ERROR //
  public void storeResource(java.io.File paramFile, miui.resourcebrowser.model.Resource paramResource)
    throws PersistenceException
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 4
    //   3: new 119	android/util/JsonWriter
    //   6: dup
    //   7: new 370	java/io/BufferedWriter
    //   10: dup
    //   11: new 372	java/io/FileWriter
    //   14: dup
    //   15: aload_1
    //   16: invokespecial 373	java/io/FileWriter:<init>	(Ljava/io/File;)V
    //   19: invokespecial 376	java/io/BufferedWriter:<init>	(Ljava/io/Writer;)V
    //   22: invokespecial 377	android/util/JsonWriter:<init>	(Ljava/io/Writer;)V
    //   25: astore 4
    //   27: aload 4
    //   29: ldc_w 379
    //   32: invokevirtual 382	android/util/JsonWriter:setIndent	(Ljava/lang/String;)V
    //   35: aload 4
    //   37: invokevirtual 143	android/util/JsonWriter:beginObject	()Landroid/util/JsonWriter;
    //   40: pop
    //   41: aload 4
    //   43: ldc 200
    //   45: invokevirtual 155	android/util/JsonWriter:name	(Ljava/lang/String;)Landroid/util/JsonWriter;
    //   48: aload_2
    //   49: invokevirtual 385	miui/resourcebrowser/model/Resource:getLocalId	()Ljava/lang/String;
    //   52: invokevirtual 139	android/util/JsonWriter:value	(Ljava/lang/String;)Landroid/util/JsonWriter;
    //   55: pop
    //   56: aload 4
    //   58: ldc 213
    //   60: invokevirtual 155	android/util/JsonWriter:name	(Ljava/lang/String;)Landroid/util/JsonWriter;
    //   63: aload_2
    //   64: invokevirtual 347	miui/resourcebrowser/model/Resource:getOnlineId	()Ljava/lang/String;
    //   67: invokevirtual 139	android/util/JsonWriter:value	(Ljava/lang/String;)Landroid/util/JsonWriter;
    //   70: pop
    //   71: aload 4
    //   73: ldc 218
    //   75: invokevirtual 155	android/util/JsonWriter:name	(Ljava/lang/String;)Landroid/util/JsonWriter;
    //   78: aload_2
    //   79: invokevirtual 388	miui/resourcebrowser/model/Resource:getProductId	()Ljava/lang/String;
    //   82: invokevirtual 139	android/util/JsonWriter:value	(Ljava/lang/String;)Landroid/util/JsonWriter;
    //   85: pop
    //   86: aload 4
    //   88: ldc 78
    //   90: invokevirtual 155	android/util/JsonWriter:name	(Ljava/lang/String;)Landroid/util/JsonWriter;
    //   93: aload_2
    //   94: invokevirtual 389	miui/resourcebrowser/model/Resource:getLocalPath	()Ljava/lang/String;
    //   97: invokevirtual 139	android/util/JsonWriter:value	(Ljava/lang/String;)Landroid/util/JsonWriter;
    //   100: pop
    //   101: aload 4
    //   103: ldc 85
    //   105: invokevirtual 155	android/util/JsonWriter:name	(Ljava/lang/String;)Landroid/util/JsonWriter;
    //   108: aload_2
    //   109: invokevirtual 338	miui/resourcebrowser/model/Resource:getOnlinePath	()Ljava/lang/String;
    //   112: invokevirtual 139	android/util/JsonWriter:value	(Ljava/lang/String;)Landroid/util/JsonWriter;
    //   115: pop
    //   116: aload 4
    //   118: ldc 98
    //   120: invokevirtual 155	android/util/JsonWriter:name	(Ljava/lang/String;)Landroid/util/JsonWriter;
    //   123: aload_2
    //   124: invokevirtual 390	miui/resourcebrowser/model/Resource:getFilePath	()Ljava/lang/String;
    //   127: invokevirtual 139	android/util/JsonWriter:value	(Ljava/lang/String;)Landroid/util/JsonWriter;
    //   130: pop
    //   131: aload 4
    //   133: ldc 226
    //   135: invokevirtual 155	android/util/JsonWriter:name	(Ljava/lang/String;)Landroid/util/JsonWriter;
    //   138: aload_2
    //   139: invokevirtual 393	miui/resourcebrowser/model/Resource:getDownloadPath	()Ljava/lang/String;
    //   142: invokevirtual 139	android/util/JsonWriter:value	(Ljava/lang/String;)Landroid/util/JsonWriter;
    //   145: pop
    //   146: aload 4
    //   148: ldc 231
    //   150: invokevirtual 155	android/util/JsonWriter:name	(Ljava/lang/String;)Landroid/util/JsonWriter;
    //   153: aload_2
    //   154: invokevirtual 396	miui/resourcebrowser/model/Resource:getRightsPath	()Ljava/lang/String;
    //   157: invokevirtual 139	android/util/JsonWriter:value	(Ljava/lang/String;)Landroid/util/JsonWriter;
    //   160: pop
    //   161: aload 4
    //   163: ldc 236
    //   165: invokevirtual 155	android/util/JsonWriter:name	(Ljava/lang/String;)Landroid/util/JsonWriter;
    //   168: aload_2
    //   169: invokevirtual 399	miui/resourcebrowser/model/Resource:getPlatform	()I
    //   172: i2l
    //   173: invokevirtual 402	android/util/JsonWriter:value	(J)Landroid/util/JsonWriter;
    //   176: pop
    //   177: aload 4
    //   179: ldc 246
    //   181: invokevirtual 155	android/util/JsonWriter:name	(Ljava/lang/String;)Landroid/util/JsonWriter;
    //   184: aload_2
    //   185: invokevirtual 405	miui/resourcebrowser/model/Resource:getStatus	()I
    //   188: i2l
    //   189: invokevirtual 402	android/util/JsonWriter:value	(J)Landroid/util/JsonWriter;
    //   192: pop
    //   193: aload 4
    //   195: ldc 251
    //   197: invokevirtual 155	android/util/JsonWriter:name	(Ljava/lang/String;)Landroid/util/JsonWriter;
    //   200: aload_2
    //   201: invokevirtual 408	miui/resourcebrowser/model/Resource:getHash	()Ljava/lang/String;
    //   204: invokevirtual 139	android/util/JsonWriter:value	(Ljava/lang/String;)Landroid/util/JsonWriter;
    //   207: pop
    //   208: aload 4
    //   210: ldc_w 256
    //   213: invokevirtual 155	android/util/JsonWriter:name	(Ljava/lang/String;)Landroid/util/JsonWriter;
    //   216: aload_2
    //   217: invokevirtual 411	miui/resourcebrowser/model/Resource:getSize	()J
    //   220: invokevirtual 402	android/util/JsonWriter:value	(J)Landroid/util/JsonWriter;
    //   223: pop
    //   224: aload 4
    //   226: ldc_w 266
    //   229: invokevirtual 155	android/util/JsonWriter:name	(Ljava/lang/String;)Landroid/util/JsonWriter;
    //   232: aload_2
    //   233: invokevirtual 414	miui/resourcebrowser/model/Resource:getUpdatedTime	()J
    //   236: invokevirtual 402	android/util/JsonWriter:value	(J)Landroid/util/JsonWriter;
    //   239: pop
    //   240: aload 4
    //   242: ldc_w 271
    //   245: invokevirtual 155	android/util/JsonWriter:name	(Ljava/lang/String;)Landroid/util/JsonWriter;
    //   248: aload_2
    //   249: invokevirtual 417	miui/resourcebrowser/model/Resource:getTitle	()Ljava/lang/String;
    //   252: invokevirtual 139	android/util/JsonWriter:value	(Ljava/lang/String;)Landroid/util/JsonWriter;
    //   255: pop
    //   256: aload 4
    //   258: ldc_w 276
    //   261: invokevirtual 155	android/util/JsonWriter:name	(Ljava/lang/String;)Landroid/util/JsonWriter;
    //   264: aload_2
    //   265: invokevirtual 420	miui/resourcebrowser/model/Resource:getDescription	()Ljava/lang/String;
    //   268: invokevirtual 139	android/util/JsonWriter:value	(Ljava/lang/String;)Landroid/util/JsonWriter;
    //   271: pop
    //   272: aload 4
    //   274: ldc_w 281
    //   277: invokevirtual 155	android/util/JsonWriter:name	(Ljava/lang/String;)Landroid/util/JsonWriter;
    //   280: aload_2
    //   281: invokevirtual 423	miui/resourcebrowser/model/Resource:getAuthor	()Ljava/lang/String;
    //   284: invokevirtual 139	android/util/JsonWriter:value	(Ljava/lang/String;)Landroid/util/JsonWriter;
    //   287: pop
    //   288: aload 4
    //   290: ldc_w 286
    //   293: invokevirtual 155	android/util/JsonWriter:name	(Ljava/lang/String;)Landroid/util/JsonWriter;
    //   296: aload_2
    //   297: invokevirtual 426	miui/resourcebrowser/model/Resource:getDesigner	()Ljava/lang/String;
    //   300: invokevirtual 139	android/util/JsonWriter:value	(Ljava/lang/String;)Landroid/util/JsonWriter;
    //   303: pop
    //   304: aload 4
    //   306: ldc_w 291
    //   309: invokevirtual 155	android/util/JsonWriter:name	(Ljava/lang/String;)Landroid/util/JsonWriter;
    //   312: aload_2
    //   313: invokevirtual 429	miui/resourcebrowser/model/Resource:getVersion	()Ljava/lang/String;
    //   316: invokevirtual 139	android/util/JsonWriter:value	(Ljava/lang/String;)Landroid/util/JsonWriter;
    //   319: pop
    //   320: aload 4
    //   322: ldc_w 296
    //   325: invokevirtual 155	android/util/JsonWriter:name	(Ljava/lang/String;)Landroid/util/JsonWriter;
    //   328: aload_2
    //   329: invokevirtual 432	miui/resourcebrowser/model/Resource:getDownloadCount	()Ljava/lang/String;
    //   332: invokevirtual 139	android/util/JsonWriter:value	(Ljava/lang/String;)Landroid/util/JsonWriter;
    //   335: pop
    //   336: aload 4
    //   338: ldc_w 301
    //   341: invokevirtual 155	android/util/JsonWriter:name	(Ljava/lang/String;)Landroid/util/JsonWriter;
    //   344: pop
    //   345: aload_0
    //   346: aload 4
    //   348: aload_2
    //   349: invokevirtual 436	miui/resourcebrowser/model/Resource:getBuildInThumbnails	()Ljava/util/List;
    //   352: invokespecial 438	miui/resourcebrowser/controller/local/LocalJSONDataParser:writeBuildInImagePaths	(Landroid/util/JsonWriter;Ljava/util/List;)V
    //   355: aload 4
    //   357: ldc_w 309
    //   360: invokevirtual 155	android/util/JsonWriter:name	(Ljava/lang/String;)Landroid/util/JsonWriter;
    //   363: pop
    //   364: aload_0
    //   365: aload 4
    //   367: aload_2
    //   368: invokevirtual 441	miui/resourcebrowser/model/Resource:getBuildInPreviews	()Ljava/util/List;
    //   371: invokespecial 438	miui/resourcebrowser/controller/local/LocalJSONDataParser:writeBuildInImagePaths	(Landroid/util/JsonWriter;Ljava/util/List;)V
    //   374: aload 4
    //   376: ldc_w 314
    //   379: invokevirtual 155	android/util/JsonWriter:name	(Ljava/lang/String;)Landroid/util/JsonWriter;
    //   382: pop
    //   383: aload_0
    //   384: aload 4
    //   386: aload_2
    //   387: invokevirtual 444	miui/resourcebrowser/model/Resource:getThumbnails	()Ljava/util/List;
    //   390: invokespecial 446	miui/resourcebrowser/controller/local/LocalJSONDataParser:writeImagePaths	(Landroid/util/JsonWriter;Ljava/util/List;)V
    //   393: aload 4
    //   395: ldc_w 321
    //   398: invokevirtual 155	android/util/JsonWriter:name	(Ljava/lang/String;)Landroid/util/JsonWriter;
    //   401: pop
    //   402: aload_0
    //   403: aload 4
    //   405: aload_2
    //   406: invokevirtual 449	miui/resourcebrowser/model/Resource:getPreviews	()Ljava/util/List;
    //   409: invokespecial 446	miui/resourcebrowser/controller/local/LocalJSONDataParser:writeImagePaths	(Landroid/util/JsonWriter;Ljava/util/List;)V
    //   412: aload 4
    //   414: ldc_w 326
    //   417: invokevirtual 155	android/util/JsonWriter:name	(Ljava/lang/String;)Landroid/util/JsonWriter;
    //   420: pop
    //   421: aload_0
    //   422: aload 4
    //   424: aload_2
    //   425: invokevirtual 452	miui/resourcebrowser/model/Resource:getParentResources	()Ljava/util/List;
    //   428: invokespecial 454	miui/resourcebrowser/controller/local/LocalJSONDataParser:writeRelatedResources	(Landroid/util/JsonWriter;Ljava/util/List;)V
    //   431: aload 4
    //   433: ldc_w 333
    //   436: invokevirtual 155	android/util/JsonWriter:name	(Ljava/lang/String;)Landroid/util/JsonWriter;
    //   439: pop
    //   440: aload_0
    //   441: aload 4
    //   443: aload_2
    //   444: invokevirtual 457	miui/resourcebrowser/model/Resource:getSubResources	()Ljava/util/List;
    //   447: invokespecial 454	miui/resourcebrowser/controller/local/LocalJSONDataParser:writeRelatedResources	(Landroid/util/JsonWriter;Ljava/util/List;)V
    //   450: aload 4
    //   452: ldc 102
    //   454: invokevirtual 155	android/util/JsonWriter:name	(Ljava/lang/String;)Landroid/util/JsonWriter;
    //   457: pop
    //   458: aload_0
    //   459: aload 4
    //   461: aload_2
    //   462: invokevirtual 458	miui/resourcebrowser/model/Resource:getExtraMeta	()Ljava/util/Map;
    //   465: invokespecial 180	miui/resourcebrowser/controller/local/LocalJSONDataParser:writeExtraMeta	(Landroid/util/JsonWriter;Ljava/util/Map;)V
    //   468: aload 4
    //   470: invokevirtual 152	android/util/JsonWriter:endObject	()Landroid/util/JsonWriter;
    //   473: pop
    //   474: aload 4
    //   476: ifnull +8 -> 484
    //   479: aload 4
    //   481: invokevirtual 459	android/util/JsonWriter:close	()V
    //   484: return
    //   485: astore_3
    //   486: aload_3
    //   487: invokevirtual 366	java/io/IOException:printStackTrace	()V
    //   490: goto -6 -> 484
    //   493: astore_3
    //   494: new 184	miui/resourcebrowser/controller/local/PersistenceException
    //   497: dup
    //   498: aload_3
    //   499: invokevirtual 206	java/io/IOException:getMessage	()Ljava/lang/String;
    //   502: invokespecial 208	miui/resourcebrowser/controller/local/PersistenceException:<init>	(Ljava/lang/String;)V
    //   505: athrow
    //   506: astore_3
    //   507: aload 4
    //   509: ifnull +8 -> 517
    //   512: aload 4
    //   514: invokevirtual 459	android/util/JsonWriter:close	()V
    //   517: aload_3
    //   518: athrow
    //   519: astore 4
    //   521: aload 4
    //   523: invokevirtual 366	java/io/IOException:printStackTrace	()V
    //   526: goto -9 -> 517
    //   529: astore_3
    //   530: aload 4
    //   532: astore 4
    //   534: goto -27 -> 507
    //   537: astore_3
    //   538: aload 4
    //   540: astore 4
    //   542: goto -48 -> 494
    //
    // Exception table:
    //   from	to	target	type
    //   479	484	485	java/io/IOException
    //   3	27	493	java/io/IOException
    //   3	27	506	finally
    //   494	506	506	finally
    //   512	517	519	java/io/IOException
    //   27	474	529	finally
    //   27	474	537	java/io/IOException
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.controller.local.LocalJSONDataParser
 * JD-Core Version:    0.6.0
 */