package miui.resourcebrowser.controller.local;

import java.io.File;
import miui.resourcebrowser.ResourceContext;
import miui.resourcebrowser.controller.ResourceController;
import miui.resourcebrowser.model.Resource;

public abstract class LocalDataParser
{
  protected ResourceContext context;
  protected ResourceController controller;

  public LocalDataParser(ResourceContext paramResourceContext)
  {
    this.context = paramResourceContext;
  }

  public abstract Resource loadResource(File paramFile)
    throws PersistenceException;

  public void setResourceController(ResourceController paramResourceController)
  {
    this.controller = paramResourceController;
  }

  public abstract void storeResource(File paramFile, Resource paramResource)
    throws PersistenceException;

  public void updateState()
  {
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.controller.local.LocalDataParser
 * JD-Core Version:    0.6.0
 */