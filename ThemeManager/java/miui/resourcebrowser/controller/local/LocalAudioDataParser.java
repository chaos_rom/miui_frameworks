package miui.resourcebrowser.controller.local;

import java.io.File;
import miui.resourcebrowser.ResourceContext;
import miui.resourcebrowser.model.Resource;
import miui.resourcebrowser.util.ResourceHelper;

public class LocalAudioDataParser extends LocalBareDataParser
{
  private int mMaxDurationLimit = 2147483647;
  private int mMinDurationLimit = 0;
  private int mRingtoneType;

  public LocalAudioDataParser(ResourceContext paramResourceContext)
  {
    super(paramResourceContext);
  }

  private boolean matchLimitation(long paramLong)
  {
    int i;
    if ((this.mMinDurationLimit > paramLong) || (paramLong > this.mMaxDurationLimit))
      i = 0;
    else
      i = 1;
    return i;
  }

  public Resource loadResource(File paramFile)
    throws PersistenceException
  {
    long l = ResourceHelper.getLocalRingtoneDuration(paramFile.getAbsolutePath());
    Resource localResource;
    if ((l >= 0L) && (matchLimitation(l)))
    {
      localResource = super.loadResource(paramFile);
      localResource.putExtraMeta("duration", String.valueOf(l));
    }
    else
    {
      localResource = null;
    }
    return localResource;
  }

  public void setDurationLimitation(int paramInt1, int paramInt2)
  {
    this.mMinDurationLimit = paramInt1;
    this.mMaxDurationLimit = paramInt2;
  }

  public void updateState()
  {
    int i = 5000;
    this.mRingtoneType = ((Integer)this.context.getExtraMeta("android.intent.extra.ringtone.TYPE")).intValue();
    ResourceContext localResourceContext1 = this.context;
    int k;
    if (this.mRingtoneType != 2)
      k = i;
    else
      k = 0;
    int j = ((Integer)localResourceContext1.getExtraMeta("resourcebrowser.RINGTONE_MIN_DURATION_LIMIT", Integer.valueOf(k))).intValue();
    ResourceContext localResourceContext2 = this.context;
    if (this.mRingtoneType != 2)
      i = 2147483647;
    setDurationLimitation(j, ((Integer)localResourceContext2.getExtraMeta("resourcebrowser.RINGTONE_MAX_DURATION_LIMIT", Integer.valueOf(i))).intValue());
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.controller.local.LocalAudioDataParser
 * JD-Core Version:    0.6.0
 */