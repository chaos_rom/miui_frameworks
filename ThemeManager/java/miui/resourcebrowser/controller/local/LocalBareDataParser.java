package miui.resourcebrowser.controller.local;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import miui.resourcebrowser.ResourceContext;
import miui.resourcebrowser.model.PathEntry;
import miui.resourcebrowser.model.Resource;
import miui.resourcebrowser.util.ResourceHelper;

public class LocalBareDataParser extends LocalDataParser
{
  public LocalBareDataParser(ResourceContext paramResourceContext)
  {
    super(paramResourceContext);
  }

  public Resource loadResource(File paramFile)
    throws PersistenceException
  {
    Resource localResource = new Resource();
    String str = paramFile.getAbsolutePath();
    localResource.setLocalPath(str);
    localResource.setFilePath(str);
    localResource.setDownloadPath(str);
    localResource.setHash(ResourceHelper.getFileHash(str));
    localResource.setSize(paramFile.length());
    localResource.setUpdatedTime(paramFile.lastModified());
    localResource.setStatus(1);
    localResource.setTitle(ResourceHelper.getFileNameWithoutExtension(str));
    ArrayList localArrayList = new ArrayList();
    localArrayList.add(new PathEntry(str, ""));
    localResource.setThumbnails(localArrayList);
    localResource.setPreviews(localArrayList);
    return localResource;
  }

  public void storeResource(File paramFile, Resource paramResource)
    throws PersistenceException
  {
    String str2 = paramResource.getDownloadPath();
    String str1 = paramResource.getLocalPath();
    if ((str2 != null) && (str1 != null) && (!str2.equals(str1)))
      new File(str2).renameTo(paramFile);
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.controller.local.LocalBareDataParser
 * JD-Core Version:    0.6.0
 */