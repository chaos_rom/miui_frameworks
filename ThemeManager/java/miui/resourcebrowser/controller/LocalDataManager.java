package miui.resourcebrowser.controller;

import java.util.List;
import miui.resourcebrowser.ResourceContext;
import miui.resourcebrowser.model.Resource;

public abstract class LocalDataManager extends DataManager
{
  public LocalDataManager(ResourceContext paramResourceContext)
  {
    super(paramResourceContext);
  }

  public abstract boolean addResource(Resource paramResource);

  public abstract List<Resource> findResources(String paramString);

  public abstract Resource getResourceByOnlineId(String paramString);

  public abstract List<Resource> getResources();

  public abstract boolean removeResource(Resource paramResource);

  public abstract boolean updateResource(Resource paramResource);
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.controller.LocalDataManager
 * JD-Core Version:    0.6.0
 */