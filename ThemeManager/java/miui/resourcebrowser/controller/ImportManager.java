package miui.resourcebrowser.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import miui.os.ExtraFileUtils;
import miui.os.Shell;
import miui.resourcebrowser.ResourceConstants;
import miui.resourcebrowser.ResourceContext;
import miui.resourcebrowser.controller.local.LocalJSONDataParser;
import miui.resourcebrowser.controller.local.PersistenceException;
import miui.resourcebrowser.controller.strategy.IdGenerationStrategy;
import miui.resourcebrowser.controller.strategy.IncrementalIdGenerationStrategy;
import miui.resourcebrowser.model.RelatedResource;
import miui.resourcebrowser.model.Resource;
import miui.resourcebrowser.util.ResourceHelper;
import miui.resourcebrowser.util.ResourceHelper.UnzipProcessUpdateListener;

public class ImportManager
  implements ResourceConstants
{
  protected ResourceContext baseContext;
  protected ResourceContext context;
  protected ResourceController controller;
  protected IdGenerationStrategy idGenerationStrategy = new IncrementalIdGenerationStrategy();

  public ImportManager(ResourceContext paramResourceContext)
  {
    this.baseContext = paramResourceContext;
  }

  protected Resource buildBundle(File paramFile, Resource paramResource)
  {
    File localFile = new File(paramResource.getDownloadPath());
    String str = paramResource.getLocalId();
    if (str == null)
      str = this.idGenerationStrategy.nextId();
    paramResource.setLocalId(str);
    paramResource.setLocalPath(getBundleLocalPath(localFile, str));
    str = getBundleFilePath(localFile, str);
    createBundleContentFile(str);
    paramResource.setFilePath(str);
    paramResource.setHash(ResourceHelper.getFileHash(localFile.getAbsolutePath()));
    paramResource.setSize(localFile.length());
    if (paramResource.getTitle() == null)
    {
      localFile = new File(paramFile, "description.xml");
      if (localFile.exists())
      {
        populateDataByDescription(paramResource, ResourceHelper.getDescription(localFile));
        localFile.delete();
      }
    }
    resolveBuildInImages(paramFile, paramResource, this.context.getBuildInImagePrefixes());
    return paramResource;
  }

  protected Resource buildComponent(File paramFile1, File paramFile2, Resource paramResource, String paramString)
  {
    Resource localResource = new Resource();
    Object localObject = getRelatedResource(paramString, paramResource.getSubResources());
    LocalJSONDataParser localLocalJSONDataParser;
    if (localObject != null)
      localLocalJSONDataParser = new LocalJSONDataParser(this.context);
    try
    {
      localObject = localLocalJSONDataParser.loadResource(new File(((RelatedResource)localObject).getLocalPath()));
      if (localObject != null)
        localResource.updateFrom((Resource)localObject);
      copyInheritedProperties(localResource, paramResource);
      localObject = localResource.getLocalId();
      if (localObject == null)
        localObject = this.idGenerationStrategy.nextId();
      localResource.setLocalId((String)localObject);
      localResource.setLocalPath(getComponentLocalPath(paramFile2, (String)localObject, paramString));
      localResource.setFilePath(getComponentFilePath(paramFile2, (String)localObject, paramString));
      localResource.setHash(ResourceHelper.getFileHash(paramFile2.getAbsolutePath()));
      localResource.setSize(paramFile2.length());
      resolveBuildInImages(paramFile1, localResource, getComponentImagePrefixes(paramString));
      if (getRelatedResource(this.context.getResourceCode(), localResource.getParentResources()) == null)
      {
        localObject = new RelatedResource();
        ((RelatedResource)localObject).setLocalPath(paramResource.getLocalPath());
        ((RelatedResource)localObject).setResourceCode(this.context.getResourceCode());
        localResource.addParentResources((RelatedResource)localObject);
      }
      return localResource;
    }
    catch (PersistenceException localPersistenceException)
    {
      while (true)
        localPersistenceException.printStackTrace();
    }
  }

  protected Resource buildSingle(File paramFile, Resource paramResource)
  {
    String str = paramResource.getLocalId();
    if (str == null)
      str = this.idGenerationStrategy.nextId();
    paramResource.setLocalId(str);
    paramResource.setLocalPath(getSingleLocalPath(paramFile, str));
    paramResource.setFilePath(getSingleFilePath(paramFile, str));
    paramResource.setHash(ResourceHelper.getFileHash(paramFile.getAbsolutePath()));
    paramResource.setSize(paramFile.length());
    return paramResource;
  }

  protected void copyInheritedProperties(Resource paramResource1, Resource paramResource2)
  {
    paramResource1.setProductId(paramResource2.getProductId());
    paramResource1.setOnlinePath(paramResource2.getOnlinePath());
    paramResource1.setDownloadPath(paramResource2.getDownloadPath());
    paramResource1.setRightsPath(paramResource2.getRightsPath());
    paramResource1.setPlatform(paramResource2.getPlatform());
    paramResource1.setStatus(paramResource2.getStatus());
    paramResource1.setUpdatedTime(paramResource2.getUpdatedTime());
    paramResource1.setTitle(paramResource2.getTitle());
    paramResource1.setDescription(paramResource2.getDescription());
    paramResource1.setAuthor(paramResource2.getAuthor());
    paramResource1.setDesigner(paramResource2.getDesigner());
    paramResource1.setVersion(paramResource2.getVersion());
    paramResource1.setDownloadCount(paramResource2.getDownloadCount());
    paramResource1.setExtraMeta(paramResource2.getExtraMeta());
  }

  protected boolean createBundleContentFile(String paramString)
  {
    try
    {
      File localFile = new File(paramString);
      localFile.getParentFile().mkdirs();
      localFile.createNewFile();
      int i = 1;
      return i;
    }
    catch (IOException j)
    {
      while (true)
      {
        localIOException.printStackTrace();
        int j = 0;
      }
    }
  }

  protected boolean createBundleIndexFile(String paramString)
  {
    try
    {
      File localFile = new File(paramString);
      localFile.getParentFile().mkdirs();
      localFile.createNewFile();
      int i = 1;
      return i;
    }
    catch (IOException j)
    {
      while (true)
      {
        localIOException.printStackTrace();
        int j = 0;
      }
    }
  }

  protected String getBuildInImageFolderPath(String paramString)
  {
    return ExtraFileUtils.standardizeFolderPath(this.context.getBuildInImageFolder() + paramString);
  }

  protected String getBundleFilePath(File paramFile, String paramString)
  {
    return this.context.getContentFolder() + paramFile.getName();
  }

  protected String getBundleLocalPath(File paramFile, String paramString)
  {
    return this.context.getMetaFolder() + paramString + ".mrm";
  }

  protected String getComponentFilePath(File paramFile, String paramString1, String paramString2)
  {
    String str2 = ExtraFileUtils.standardizeFolderPath(paramString2);
    String str1 = ExtraFileUtils.standardizeFolderPath(new File(this.context.getContentFolder()).getParent());
    return str1 + str2 + paramString1 + ".mrc";
  }

  protected List<String> getComponentImagePrefixes(String paramString)
  {
    ArrayList localArrayList = new ArrayList();
    localArrayList.add("preview_" + paramString + "_");
    return localArrayList;
  }

  protected String getComponentLocalPath(File paramFile, String paramString1, String paramString2)
  {
    String str1 = ExtraFileUtils.standardizeFolderPath(paramString2);
    String str2 = ExtraFileUtils.standardizeFolderPath(new File(this.context.getMetaFolder()).getParent());
    return str2 + str1 + paramString1 + ".mrm";
  }

  protected Map<String, String> getComponentsMap(File paramFile)
  {
    HashMap localHashMap = new HashMap();
    File[] arrayOfFile = paramFile.listFiles();
    int j = arrayOfFile.length;
    for (int i = 0; ; i++)
    {
      if (i >= j)
        return localHashMap;
      File localFile = arrayOfFile[i];
      if ("preview".equals(localFile.getName()))
        continue;
      localHashMap.put(localFile.getName(), localFile.getName());
    }
  }

  protected RelatedResource getRelatedResource(String paramString, List<RelatedResource> paramList)
  {
    Iterator localIterator = paramList.iterator();
    RelatedResource localRelatedResource;
    do
    {
      if (!localIterator.hasNext())
      {
        localRelatedResource = null;
        break;
      }
      localRelatedResource = (RelatedResource)localIterator.next();
    }
    while (!paramString.equals(localRelatedResource.getResourceCode()));
    return localRelatedResource;
  }

  protected String getSingleFilePath(File paramFile, String paramString)
  {
    return this.context.getContentFolder() + paramString + ".mrc";
  }

  protected String getSingleLocalPath(File paramFile, String paramString)
  {
    return this.context.getMetaFolder() + paramString + ".mrm";
  }

  protected boolean importAudio(Resource paramResource)
  {
    return true;
  }

  protected boolean importBundle(Resource paramResource)
  {
    int i = 0;
    File localFile1 = 1;
    Object localObject = unzipBundle(paramResource);
    File localFile2;
    if (localObject != null)
    {
      localFile2 = new File((String)localObject);
      if (localFile2.exists())
      {
        localObject = buildBundle(localFile2, paramResource);
        if ((!importComponents(localFile2, (Resource)localObject)) || (localFile1 == 0))
          localFile1 = 0;
        else
          localFile1 = 1;
        Shell.remove(localFile2.getAbsolutePath());
        this.controller.getLocalDataManager().addResource((Resource)localObject);
      }
      localFile2 = localFile1;
    }
    return localFile2;
  }

  protected boolean importComponent(File paramFile1, File paramFile2, Resource paramResource, String paramString)
  {
    Resource localResource = buildComponent(paramFile1, paramFile2, paramResource, paramString);
    if (getRelatedResource(paramString, paramResource.getSubResources()) == null)
    {
      localObject = new RelatedResource();
      ((RelatedResource)localObject).setLocalPath(localResource.getLocalPath());
      ((RelatedResource)localObject).setFilePath(localResource.getFilePath());
      ((RelatedResource)localObject).setResourceCode(paramString);
      paramResource.addSubResources((RelatedResource)localObject);
    }
    Object localObject = new File(localResource.getFilePath());
    ((File)localObject).getParentFile().mkdirs();
    ((File)localObject).delete();
    paramFile2.renameTo((File)localObject);
    this.controller.getLocalDataManager().addResource(localResource);
    return true;
  }

  protected boolean importComponents(File paramFile, Resource paramResource)
  {
    int i = 1;
    Map localMap = getComponentsMap(paramFile);
    Iterator localIterator = localMap.keySet().iterator();
    while (true)
    {
      int k;
      if (!localIterator.hasNext())
      {
        localObject = paramFile.listFiles();
        int j = localObject.length;
        for (k = 0; ; k++)
        {
          if (k >= j)
            return i;
          localIterator = localObject[k];
          if (ResourceHelper.deleteEmptyFolder(localIterator))
            continue;
          String str = localIterator.getName();
          if (("preview".equals(str)) || (localIterator.isDirectory()))
            continue;
          if ((!importComponent(paramFile, localIterator, paramResource, str)) || (i == 0))
            i = 0;
          else
            i = 1;
        }
      }
      Object localObject = (String)localIterator.next();
      File localFile = new File(paramFile, (String)localObject);
      if (!localFile.exists())
        continue;
      if ((!importComponent(paramFile, localFile, paramResource, (String)k.get(localObject))) || (i == 0))
        i = 0;
      else
        i = 1;
    }
  }

  protected boolean importImage(Resource paramResource)
  {
    return true;
  }

  protected boolean importOther(Resource paramResource)
  {
    return importSingle(paramResource);
  }

  public boolean importResource(Resource paramResource)
  {
    return importResource(paramResource, this.baseContext);
  }

  public boolean importResource(Resource paramResource, ResourceContext paramResourceContext)
  {
    boolean bool = false;
    this.context = paramResourceContext;
    resolveOnlineId(paramResource);
    switch (paramResourceContext.getResourceFormat())
    {
    case 1:
      bool = importBundle(paramResource);
      break;
    case 2:
      bool = importImage(paramResource);
      break;
    case 3:
      bool = importAudio(paramResource);
      break;
    case 4:
      bool = importZip(paramResource);
      break;
    case 5:
      bool = importOther(paramResource);
    }
    if (bool)
      createBundleIndexFile(paramResourceContext.getIndexFolder() + paramResource.getHash());
    return bool;
  }

  protected boolean importSingle(Resource paramResource)
  {
    File localFile1 = new File(paramResource.getDownloadPath());
    Resource localResource = buildSingle(localFile1, paramResource);
    File localFile2 = new File(localResource.getFilePath());
    localFile2.getParentFile().mkdirs();
    localFile2.delete();
    localFile1.renameTo(localFile2);
    this.controller.getLocalDataManager().addResource(localResource);
    return true;
  }

  protected boolean importZip(Resource paramResource)
  {
    return importSingle(paramResource);
  }

  protected void populateDataByDescription(Resource paramResource, Map<String, String> paramMap)
  {
    paramResource.setTitle((String)paramMap.get("title"));
    paramResource.setDescription((String)paramMap.get("description"));
    paramResource.setAuthor((String)paramMap.get("author"));
    paramResource.setDesigner((String)paramMap.get("designer"));
    paramResource.setVersion((String)paramMap.get("version"));
    int j = 0;
    String str = (String)paramMap.get("platform");
    if (str == null)
      str = (String)paramMap.get("uiVersion");
    if (str != null);
    try
    {
      int i = Integer.parseInt(str);
      j = i;
      label125: paramResource.setPlatform(j);
      return;
    }
    catch (NumberFormatException localNumberFormatException)
    {
      break label125;
    }
  }

  protected void resolveBuildInImage(File paramFile, String paramString1, Resource paramResource, String paramString2, String paramString3)
  {
    File localFile1;
    File localFile2;
    for (int i = 0; ; localFile2 = localFile1)
    {
      Object localObject = new StringBuilder().append(paramString2);
      localFile1 = i + 1;
      localFile2 = new File(paramFile, i + paramString3);
      if ((!localFile2.exists()) || (localFile2.isDirectory()))
        return;
      localObject = localFile2.getName();
      localObject = paramString1 + (String)localObject;
      paramResource.addBuildInThumbnail((String)localObject);
      paramResource.addBuildInPreview((String)localObject);
      Shell.copy(localFile2.getAbsolutePath(), (String)localObject);
    }
  }

  protected void resolveBuildInImages(File paramFile, Resource paramResource, List<String> paramList)
  {
    paramResource.clearBuildInThumbnails();
    paramResource.clearBuildInPreviews();
    File localFile = new File(paramFile, "preview");
    String str2;
    Iterator localIterator;
    if ((localFile.exists()) && (localFile.isDirectory()))
    {
      str2 = getBuildInImageFolderPath(paramResource.getLocalId());
      new File(str2).mkdirs();
      localIterator = paramList.iterator();
    }
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      String str1 = (String)localIterator.next();
      resolveBuildInImage(localFile, str2, paramResource, str1, ".jpg");
      resolveBuildInImage(localFile, str2, paramResource, str1, ".png");
    }
  }

  protected void resolveOnlineId(Resource paramResource)
  {
    if (paramResource.getOnlineId() != null)
    {
      paramResource.setPlatform(this.context.getCurrentPlatform());
      paramResource.setStatus(3);
    }
    else
    {
      File localFile = new File(paramResource.getDownloadPath());
      Object localObject = ResourceHelper.getFileHash(localFile.getAbsolutePath());
      ArrayList localArrayList = new ArrayList();
      localArrayList.add(localObject);
      localObject = this.controller.getOnlineDataManager().getAssociationResources(localArrayList);
      if ((localObject != null) && (((List)localObject).size() > 0))
      {
        localObject = ((Resource)((List)localObject).get(0)).getOnlineId();
        if ((localObject != null) && (!((String)localObject).equals("")))
        {
          localObject = this.controller.getOnlineDataManager().getResource((String)localObject);
          if (localObject != null)
          {
            ((Resource)localObject).setDownloadPath(paramResource.getDownloadPath());
            ((Resource)localObject).setStatus(3);
            paramResource.updateFrom((Resource)localObject);
          }
        }
      }
      if ((paramResource.getOnlineId() == null) && (!ResourceHelper.getFileNameWithoutExtension(localFile.getAbsolutePath()).matches("[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}")))
        paramResource.setStatus(1);
    }
  }

  public void setResourceController(ResourceController paramResourceController)
  {
    this.controller = paramResourceController;
  }

  protected String unzipBundle(Resource paramResource)
  {
    Object localObject = this.context.getDownloadFolder() + "temp/";
    if (paramResource.getOnlineId() != null)
      localObject = (String)localObject + paramResource.getOnlineId();
    while (true)
    {
      String str = ExtraFileUtils.standardizeFolderPath((String)localObject);
      try
      {
        localObject = new ResourceHelper.UnzipProcessUpdateListener()
        {
          public void onUpdate(String paramString1, long paramLong, String paramString2)
          {
            ResourceHelper.setFileHash(paramString1, paramString2);
          }
        };
        ResourceHelper.unzip(paramResource.getDownloadPath(), str, (ResourceHelper.UnzipProcessUpdateListener)localObject);
        return str;
        localObject = (String)localObject + ResourceHelper.getFileNameWithoutExtension(paramResource.getDownloadPath());
      }
      catch (IOException localIOException)
      {
        while (true)
        {
          localIOException.printStackTrace();
          str = null;
        }
      }
    }
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.controller.ImportManager
 * JD-Core Version:    0.6.0
 */