package miui.resourcebrowser.controller.online;

import android.text.TextUtils;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import miui.resourcebrowser.ResourceConstants;
import miui.resourcebrowser.ResourceContext;
import miui.resourcebrowser.controller.LocalDataManager;
import miui.resourcebrowser.controller.ResourceController;
import miui.resourcebrowser.model.PathEntry;
import miui.resourcebrowser.model.RecommendItemData;
import miui.resourcebrowser.model.Resource;
import miui.resourcebrowser.model.ResourceCategory;
import miui.resourcebrowser.model.ResourceListMeta;
import miui.resourcebrowser.util.ResourceHelper;
import miui.resourcebrowser.view.UnevenGrid.GridItemData;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class OnlineJSONDataParser extends OnlineDataParser
  implements ResourceConstants
{
  public OnlineJSONDataParser(ResourceContext paramResourceContext)
  {
    super(paramResourceContext);
  }

  private String getJSONInformation(File paramFile)
    throws IOException, JSONException
  {
    BufferedReader localBufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(paramFile)), 1024);
    StringBuffer localStringBuffer = new StringBuffer();
    for (String str = localBufferedReader.readLine(); ; str = localBufferedReader.readLine())
    {
      if (str == null)
        return localStringBuffer.toString();
      localStringBuffer.append(str);
    }
  }

  public List<Resource> buildAssociationResources(File paramFile)
    throws IOException, JSONException
  {
    ArrayList localArrayList = new ArrayList();
    JSONObject localJSONObject = new JSONObject(getJSONInformation(paramFile));
    Iterator localIterator = localJSONObject.keys();
    while (true)
    {
      if (!localIterator.hasNext())
        return localArrayList;
      String str1 = (String)localIterator.next();
      String str2 = localJSONObject.getString(str1);
      Resource localResource = new Resource();
      localResource.setHash(str1);
      localResource.setOnlineId(str2);
      localArrayList.add(localResource);
    }
  }

  public List<ResourceCategory> buildCategories(File paramFile)
    throws IOException, JSONException
  {
    ArrayList localArrayList = new ArrayList();
    Object localObject = new JSONObject(getJSONInformation(paramFile));
    JSONObject localJSONObject = ((JSONObject)localObject).getJSONObject("clazzNameMap");
    JSONArray localJSONArray = ((JSONObject)localObject).getJSONArray("sortList");
    for (int i = 0; ; i++)
    {
      if (i >= localJSONArray.length())
        return localArrayList;
      ResourceCategory localResourceCategory = new ResourceCategory();
      localObject = localJSONArray.getString(i);
      localResourceCategory.setCode((String)localObject);
      localResourceCategory.setName(localJSONObject.getString((String)localObject));
      localArrayList.add(localResourceCategory);
    }
  }

  public List<RecommendItemData> buildRecommends(File paramFile)
    throws IOException, JSONException
  {
    ArrayList localArrayList = new ArrayList();
    JSONObject localJSONObject = new JSONObject(getJSONInformation(paramFile));
    String str1 = this.context.getRecommendImageCacheFolder();
    Object localObject1 = new Object[1];
    localObject1[0] = Integer.valueOf(this.context.getRecommendImageWidth());
    String str2 = String.format("jpeg/w%s/", localObject1);
    JSONArray localJSONArray1 = localJSONObject.getJSONArray("recommendations");
    int i = 0;
    if (i >= localJSONArray1.length())
      return localArrayList;
    localObject1 = new RecommendItemData();
    Object localObject3 = localJSONArray1.getJSONObject(i);
    Object localObject2 = ((JSONObject)localObject3).getString("picUrlRoot");
    ((RecommendItemData)localObject1).onlineThumbnail = ((String)localObject2 + str2 + ((JSONObject)localObject3).getString("picUrl"));
    ((RecommendItemData)localObject1).localThumbnail = (str1 + ResourceHelper.getFileName(((RecommendItemData)localObject1).onlineThumbnail));
    ((RecommendItemData)localObject1).itemType = ((JSONObject)localObject3).getInt("type");
    ((RecommendItemData)localObject1).itemId = ((JSONObject)localObject3).getString("relatedId");
    ((RecommendItemData)localObject1).title = ((JSONObject)localObject3).getString("title");
    ((UnevenGrid.GridItemData)localObject1).widthCount = ((JSONObject)localObject3).getInt("layoutCol");
    ((UnevenGrid.GridItemData)localObject1).heightCount = ((JSONObject)localObject3).getInt("layoutRow");
    ((RecommendItemData)localObject1).type = localJSONObject.getString("category");
    JSONArray localJSONArray2 = ((JSONObject)localObject3).getJSONArray("subjects");
    for (int j = 0; ; j++)
    {
      if (j >= localJSONArray2.length())
      {
        localArrayList.add(localObject1);
        i++;
        break;
      }
      localObject3 = new RecommendItemData();
      localObject2 = localJSONArray2.getJSONObject(j);
      ((RecommendItemData)localObject3).itemId = ((JSONObject)localObject2).getString("id");
      ((RecommendItemData)localObject3).title = ((JSONObject)localObject2).getString("name");
      ((RecommendItemData)localObject1).subItems.add(localObject3);
    }
  }

  public Resource buildResource(File paramFile)
    throws IOException, JSONException
  {
    return buildResource(new JSONObject(getJSONInformation(paramFile)));
  }

  protected Resource buildResource(JSONObject paramJSONObject)
    throws JSONException
  {
    Resource localResource = new Resource();
    Object localObject1 = new Object[1];
    localObject1[0] = Integer.valueOf(this.context.getThumbnailImageWidth());
    String str2 = String.format("jpeg/w%s/", localObject1);
    localObject1 = new Object[1];
    localObject1[0] = Integer.valueOf(this.context.getPreviewImageWidth());
    localObject1 = String.format("jpeg/w%s/", localObject1);
    if (this.context.getResourceFormat() == 3)
      str2 = "";
    Object localObject5 = paramJSONObject.getString("moduleId");
    Object localObject4 = paramJSONObject.optString("productId");
    Object localObject3 = this.controller.getLocalDataManager().getResourceByOnlineId((String)localObject5);
    if (localObject3 != null)
      localResource.updateFrom((Resource)localObject3);
    localResource.setOnlineId((String)localObject5);
    localResource.setProductId((String)localObject4);
    Object localObject2 = paramJSONObject.getString("downloadUrlRoot");
    localResource.setOnlinePath(new OnlineService(this.context).getDownloadUrl((String)localObject5).getUrl());
    localObject5 = this.context.getDownloadFolder() + (String)localObject5 + this.context.getResourceExtension();
    if (this.context.isSelfDescribing())
    {
      localObject6 = paramJSONObject.optString("name");
      if (!((String)localObject6).equals(""))
        localObject5 = this.context.getDownloadFolder() + (String)localObject6 + this.context.getResourceExtension();
      localResource.setLocalPath((String)localObject5);
      localResource.setFilePath((String)localObject5);
    }
    localResource.setDownloadPath((String)localObject5);
    localResource.setRightsPath(this.context.getRightsFolder() + (String)localObject4 + ".mra");
    Object localObject6 = new ArrayList();
    localObject5 = paramJSONObject.optString("frontCover");
    localObject4 = new PathEntry();
    ((PathEntry)localObject4).setLocalPath(this.context.getThumbnailCacheFolder() + ResourceHelper.getFileName((String)localObject5));
    ((PathEntry)localObject4).setOnlinePath((String)localObject2 + str2 + (String)localObject5);
    ((List)localObject6).add(localObject4);
    localResource.setThumbnails((List)localObject6);
    localObject4 = new ArrayList();
    JSONArray localJSONArray = paramJSONObject.optJSONArray("snapshotsUrl");
    if (localJSONArray != null);
    for (int j = 0; ; j++)
    {
      String str1;
      if (j >= localJSONArray.length())
      {
        localResource.setPreviews((List)localObject4);
        localResource.setPlatform(this.context.getCurrentPlatform());
        int i;
        if (localObject3 == null)
          i = 2;
        else
          i = 0x2 | ((Resource)localObject3).getStatus();
        localResource.setStatus(i);
        localResource.setSize(paramJSONObject.optLong("fileSize"));
        localResource.setUpdatedTime(paramJSONObject.optLong("modifyTime"));
        localResource.setTitle(paramJSONObject.optString("name"));
        localResource.setDescription(paramJSONObject.optString("description"));
        localResource.setAuthor(paramJSONObject.optString("author"));
        localResource.setDesigner(paramJSONObject.optString("designer"));
        localResource.setVersion(paramJSONObject.optString("version"));
        str1 = paramJSONObject.optString("purchaseCount");
        if (TextUtils.isEmpty(str1))
          str1 = String.valueOf(paramJSONObject.optLong("downloads"));
        localResource.setDownloadCount(str1);
        localResource.setProductPrice(paramJSONObject.optInt("productPrice", -1));
        localResource.setProductBought(paramJSONObject.optBoolean("productBought", false));
        localObject3 = new HashSet();
        ((Set)localObject3).add("moduleId");
        ((Set)localObject3).add("downloadUrlRoot");
        ((Set)localObject3).add("frontCover");
        ((Set)localObject3).add("snapshotsUrl");
        ((Set)localObject3).add("fileSize");
        ((Set)localObject3).add("modifyTime");
        ((Set)localObject3).add("name");
        ((Set)localObject3).add("brief");
        ((Set)localObject3).add("author");
        ((Set)localObject3).add("designer");
        ((Set)localObject3).add("version");
        ((Set)localObject3).add("downloads");
        ((Set)localObject3).add("createTime");
        ((Set)localObject3).add("description");
        ((Set)localObject3).add("tags");
        ((Set)localObject3).add("hot");
        ((Set)localObject3).add("star");
        ((Set)localObject3).add("starCount");
        ((Set)localObject3).add("starCountSize");
        ((Set)localObject3).add("snapshotsUrlSize");
        ((Set)localObject3).add("productId");
        ((Set)localObject3).add("productPrice");
        ((Set)localObject3).add("productBought");
        localObject2 = paramJSONObject.keys();
        while (true)
        {
          if (!((Iterator)localObject2).hasNext())
            return localResource;
          str1 = (String)((Iterator)localObject2).next();
          if (((Set)localObject3).contains(str1))
            continue;
          localResource.putExtraMeta(str1, paramJSONObject.get(str1).toString());
        }
      }
      localObject6 = localJSONArray.getString(j);
      localObject5 = new PathEntry();
      ((PathEntry)localObject5).setLocalPath(this.context.getPreviewCacheFolder() + ResourceHelper.getFileName((String)localObject6));
      ((PathEntry)localObject5).setOnlinePath((String)localObject2 + str1 + (String)localObject6);
      ((List)localObject4).add(localObject5);
    }
  }

  public ResourceListMeta buildResourceListMeta(File paramFile)
    throws IOException, JSONException
  {
    ResourceListMeta localResourceListMeta = new ResourceListMeta();
    JSONObject localJSONObject = new JSONObject(getJSONInformation(paramFile));
    localResourceListMeta.setName(localJSONObject.optString("name"));
    localResourceListMeta.setDescription(localJSONObject.optString("description"));
    localResourceListMeta.setCategory(localJSONObject.optString("category"));
    return localResourceListMeta;
  }

  public List<Resource> buildResources(File paramFile)
    throws IOException, JSONException
  {
    ArrayList localArrayList = new ArrayList();
    Object localObject = new JSONObject(getJSONInformation(paramFile));
    localObject = ((JSONObject)localObject).getJSONArray((String)((JSONObject)localObject).keys().next());
    for (int i = 0; ; i++)
    {
      if (i >= ((JSONArray)localObject).length())
        return localArrayList;
      localArrayList.add(buildResource(((JSONArray)localObject).getJSONObject(i)));
    }
  }

  public List<Resource> buildUpdatableResources(File paramFile)
    throws IOException, JSONException
  {
    ArrayList localArrayList = new ArrayList();
    JSONObject localJSONObject = new JSONObject(getJSONInformation(paramFile));
    Iterator localIterator = localJSONObject.keys();
    while (true)
    {
      if (!localIterator.hasNext())
        return localArrayList;
      String str2 = (String)localIterator.next();
      String str1 = localJSONObject.getString(str2);
      Resource localResource = new Resource();
      localResource.setHash(str2);
      localResource.setOnlineId(str1);
      localArrayList.add(localResource);
    }
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.controller.online.OnlineJSONDataParser
 * JD-Core Version:    0.6.0
 */