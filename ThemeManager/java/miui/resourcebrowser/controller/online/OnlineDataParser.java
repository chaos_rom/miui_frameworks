package miui.resourcebrowser.controller.online;

import java.io.File;
import java.util.List;
import miui.resourcebrowser.ResourceContext;
import miui.resourcebrowser.controller.ResourceController;
import miui.resourcebrowser.model.RecommendItemData;
import miui.resourcebrowser.model.Resource;
import miui.resourcebrowser.model.ResourceCategory;
import miui.resourcebrowser.model.ResourceListMeta;

public abstract class OnlineDataParser
{
  protected ResourceContext context;
  protected ResourceController controller;

  public OnlineDataParser(ResourceContext paramResourceContext)
  {
    this.context = paramResourceContext;
  }

  public abstract List<Resource> buildAssociationResources(File paramFile)
    throws Exception;

  public abstract List<ResourceCategory> buildCategories(File paramFile)
    throws Exception;

  public abstract List<RecommendItemData> buildRecommends(File paramFile)
    throws Exception;

  public abstract Resource buildResource(File paramFile)
    throws Exception;

  public abstract ResourceListMeta buildResourceListMeta(File paramFile)
    throws Exception;

  public abstract List<Resource> buildResources(File paramFile)
    throws Exception;

  public abstract List<Resource> buildUpdatableResources(File paramFile)
    throws Exception;

  public List<Resource> readAssociationResources(String paramString)
  {
    Object localObject = new File(paramString);
    if (((File)localObject).exists());
    while (true)
    {
      try
      {
        localObject = buildAssociationResources((File)localObject);
        localObject = localObject;
        return localObject;
      }
      catch (Exception localException)
      {
        ((File)localObject).delete();
      }
      localObject = null;
    }
  }

  public List<ResourceCategory> readCategories(String paramString)
  {
    Object localObject = new File(paramString);
    if (((File)localObject).exists());
    while (true)
    {
      try
      {
        localObject = buildCategories((File)localObject);
        localObject = localObject;
        return localObject;
      }
      catch (Exception localException)
      {
        ((File)localObject).delete();
      }
      localObject = null;
    }
  }

  public List<RecommendItemData> readRecommends(String paramString)
  {
    Object localObject = new File(paramString);
    if (((File)localObject).exists());
    while (true)
    {
      try
      {
        localObject = buildRecommends((File)localObject);
        localObject = localObject;
        return localObject;
      }
      catch (Exception localException)
      {
        ((File)localObject).delete();
      }
      localObject = null;
    }
  }

  public Resource readResource(String paramString)
  {
    Object localObject = new File(paramString);
    if (((File)localObject).exists());
    while (true)
    {
      try
      {
        localObject = buildResource((File)localObject);
        localObject = localObject;
        return localObject;
      }
      catch (Exception localException)
      {
        ((File)localObject).delete();
      }
      localObject = null;
    }
  }

  public ResourceListMeta readResourceListMeta(String paramString)
  {
    Object localObject = new File(paramString);
    if (((File)localObject).exists());
    while (true)
    {
      try
      {
        localObject = buildResourceListMeta((File)localObject);
        localObject = localObject;
        return localObject;
      }
      catch (Exception localException)
      {
        ((File)localObject).delete();
      }
      localObject = null;
    }
  }

  public List<Resource> readResources(String paramString)
  {
    Object localObject = new File(paramString);
    if (((File)localObject).exists());
    while (true)
    {
      try
      {
        localObject = buildResources((File)localObject);
        localObject = localObject;
        return localObject;
      }
      catch (Exception localException)
      {
        ((File)localObject).delete();
      }
      localObject = null;
    }
  }

  public List<Resource> readUpdatableResources(String paramString)
  {
    Object localObject = new File(paramString);
    if (((File)localObject).exists());
    while (true)
    {
      try
      {
        localObject = buildUpdatableResources((File)localObject);
        localObject = localObject;
        return localObject;
      }
      catch (Exception localException)
      {
        ((File)localObject).delete();
      }
      localObject = null;
    }
  }

  public void setResourceController(ResourceController paramResourceController)
  {
    this.controller = paramResourceController;
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.controller.online.OnlineDataParser
 * JD-Core Version:    0.6.0
 */