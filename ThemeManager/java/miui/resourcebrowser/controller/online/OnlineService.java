package miui.resourcebrowser.controller.online;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerFuture;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.SystemProperties;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Pair;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import javax.crypto.Cipher;
import miui.net.CloudCoder;
import miui.net.ExtendedAuthToken;
import miui.resourcebrowser.AppInnerContext;
import miui.resourcebrowser.ResourceContext;

public class OnlineService
{
  private static Pair<Account, ExtendedAuthToken> sAccount;
  private static long sLastGetAccountTime;
  private static Map<String, String> sVersionSuffixParams;
  private ResourceContext context;

  public OnlineService(ResourceContext paramResourceContext)
  {
    this.context = paramResourceContext;
  }

  public static InputStream decryptStream(InputStream paramInputStream)
  {
    String str1 = getSecurityKey();
    Object localObject = paramInputStream;
    if ((!TextUtils.isEmpty(str1)) && (paramInputStream != null));
    try
    {
      String str2 = getContent(paramInputStream);
      localObject = new ByteArrayInputStream(CloudCoder.newAESCipher(str1, 2).doFinal(Base64.decode(str2, 2)));
      localObject = localObject;
      return localObject;
    }
    catch (Exception localException)
    {
      while (true)
        localException.printStackTrace();
    }
  }

  private static void encryptParameters(String paramString, Map<String, String> paramMap)
  {
    Cipher localCipher = CloudCoder.newAESCipher(paramString, 1);
    try
    {
      Iterator localIterator = paramMap.entrySet().iterator();
      while (localIterator.hasNext())
      {
        Map.Entry localEntry = (Map.Entry)localIterator.next();
        paramMap.put(localEntry.getKey(), Base64.encodeToString(localCipher.doFinal(((String)localEntry.getValue()).getBytes("UTF-8")), 2));
      }
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
  }

  private static String generateSignature(String paramString1, RequestUrl paramRequestUrl, String paramString2)
  {
    String str = null;
    try
    {
      TreeMap localTreeMap = new TreeMap(paramRequestUrl.getParameters());
      str = CloudCoder.generateSignature(paramString1, paramRequestUrl.getBaseUrl(), localTreeMap, paramString2);
      str = str;
      return str;
    }
    catch (Exception localException)
    {
      while (true)
        localException.printStackTrace();
    }
  }

  public static Pair<Account, ExtendedAuthToken> getAccount()
  {
    if (!isAccountCacheValid())
      monitorenter;
    try
    {
      if (!isAccountCacheValid())
      {
        sAccount = retrieveAccount();
        sLastGetAccountTime = System.currentTimeMillis();
      }
      return sAccount;
    }
    finally
    {
      monitorexit;
    }
    throw localObject;
  }

  private static String getApkVersion()
  {
    int i = 0;
    Context localContext = AppInnerContext.getInstance().getApplicationContext();
    try
    {
      i = localContext.getPackageManager().getPackageInfo(localContext.getPackageName(), 16384).versionCode;
      return Integer.toString(i);
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      while (true)
        localNameNotFoundException.printStackTrace();
    }
  }

  private static String getContent(InputStream paramInputStream)
    throws IOException
  {
    StringBuilder localStringBuilder = new StringBuilder();
    BufferedReader localBufferedReader = new BufferedReader(new InputStreamReader(paramInputStream));
    char[] arrayOfChar = new char[1024];
    while (true)
    {
      int i = localBufferedReader.read(arrayOfChar);
      if (i == -1)
        return localStringBuilder.toString();
      localStringBuilder.append(arrayOfChar, 0, i);
    }
  }

  private static String getDevice()
  {
    String str = SystemProperties.get("ro.product.mod_device", null);
    if (TextUtils.isEmpty(str))
      str = Build.DEVICE;
    return str;
  }

  public static String getImei()
  {
    String str = TelephonyManager.getDefault().getDeviceId();
    if (TextUtils.isEmpty(str))
      str = "";
    return str;
  }

  private static String getLanguage()
  {
    return Locale.getDefault().getLanguage() + "_" + Locale.getDefault().getCountry();
  }

  public static RequestUrl getSecureUrl(RequestUrl paramRequestUrl)
  {
    RequestUrl localRequestUrl = paramRequestUrl;
    Pair localPair = getAccount();
    if (localPair != null)
    {
      localRequestUrl = (RequestUrl)paramRequestUrl.clone();
      if (!paramRequestUrl.getParameters().isEmpty())
      {
        HashMap localHashMap = new HashMap();
        localRequestUrl.setParameters(localHashMap);
        localHashMap.putAll(paramRequestUrl.getParameters());
        localHashMap.put("userId", ((Account)localPair.first).name);
        if (paramRequestUrl.getHttpMethod() == 1)
          localHashMap.put("serviceToken", ((ExtendedAuthToken)localPair.second).authToken);
        encryptParameters(((ExtendedAuthToken)localPair.second).security, localHashMap);
        localHashMap.put("signature", generateSignature(localRequestUrl.getHttpMethodName(), localRequestUrl, ((ExtendedAuthToken)localPair.second).security));
      }
    }
    return localRequestUrl;
  }

  private static String getSecurityKey()
  {
    String str = null;
    Pair localPair = getAccount();
    if (localPair != null)
      str = ((ExtendedAuthToken)localPair.second).security;
    return str;
  }

  private static String getSystemType()
  {
    return "miui";
  }

  private static String getSystemVersion()
  {
    return Build.VERSION.RELEASE + "_" + Build.VERSION.INCREMENTAL;
  }

  public static String getUser()
  {
    Object localObject = getAccount();
    if (localObject == null)
      localObject = null;
    else
      localObject = ((Account)((Pair)localObject).first).name;
    return (String)localObject;
  }

  public static String getUserCookies()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    Pair localPair = getAccount();
    if (localPair != null)
    {
      localStringBuilder.append("userId");
      localStringBuilder.append("=");
      localStringBuilder.append(((Account)localPair.first).name);
      localStringBuilder.append("; ");
      localStringBuilder.append("serviceToken");
      localStringBuilder.append("=");
      localStringBuilder.append(((ExtendedAuthToken)localPair.second).authToken);
    }
    return localStringBuilder.toString();
  }

  public static Map<String, String> getVersionParameters()
  {
    if (sVersionSuffixParams == null)
    {
      sVersionSuffixParams = new HashMap();
      sVersionSuffixParams.put("device", getDevice());
      sVersionSuffixParams.put("system", getSystemType());
      sVersionSuffixParams.put("version", getSystemVersion());
      sVersionSuffixParams.put("apk", getApkVersion());
      sVersionSuffixParams.put("imei", getImei());
      sVersionSuffixParams.put("language", getLanguage());
    }
    return sVersionSuffixParams;
  }

  private static boolean isAccountCacheValid()
  {
    int i;
    if (System.currentTimeMillis() - sLastGetAccountTime >= 30000L)
      i = 0;
    else
      i = 1;
    return i;
  }

  public static boolean isNetworkAvailable()
  {
    Object localObject = (ConnectivityManager)AppInnerContext.getInstance().getApplicationContext().getSystemService("connectivity");
    if (localObject != null)
    {
      localObject = ((ConnectivityManager)localObject).getActiveNetworkInfo();
      if ((localObject != null) && (((NetworkInfo)localObject).getState() == NetworkInfo.State.CONNECTED));
    }
    else
    {
      i = 0;
      break label46;
    }
    int i = 1;
    label46: return i;
  }

  private static Pair<Account, ExtendedAuthToken> retrieveAccount()
  {
    Object localObject3 = AccountManager.get(AppInnerContext.getInstance().getApplicationContext());
    Object localObject1 = ((AccountManager)localObject3).getAccountsByType("com.xiaomi");
    if ((localObject1 != null) && (localObject1.length > 0));
    while (true)
    {
      try
      {
        localObject1 = localObject1[0];
        localObject3 = ExtendedAuthToken.parse(((Bundle)((AccountManager)localObject3).getAuthToken((Account)localObject1, "thememarket", null, false, null, null).getResult()).getString("authtoken"));
        if ((localObject3 != null) && (((ExtendedAuthToken)localObject3).authToken != null) && (((ExtendedAuthToken)localObject3).security != null))
        {
          localObject1 = new Pair(localObject1, localObject3);
          return localObject1;
        }
      }
      catch (Exception localException)
      {
        localException.printStackTrace();
      }
      Object localObject2 = null;
    }
  }

  public RequestUrl getAssociationUrl(List<String> paramList)
  {
    HashMap localHashMap = new HashMap();
    StringBuilder localStringBuilder = new StringBuilder();
    Iterator localIterator = paramList.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
      {
        localHashMap.put("fileshash", localStringBuilder.substring(0, -1 + localStringBuilder.length()));
        return new RequestUrl("http://market.xiaomi.com/thm/relatemodule", localHashMap);
      }
      localStringBuilder.append((String)localIterator.next());
      localStringBuilder.append(",");
    }
  }

  public RequestUrl getCategoryUrl()
  {
    String str = this.context.getResourceStamp();
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = str;
    return new RequestUrl(String.format("http://market.xiaomi.com/thm/config/clazz/%s/zh-cn", arrayOfObject));
  }

  public RequestUrl getCommonListUrl(String paramString)
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("category", this.context.getResourceStamp());
    if (!TextUtils.isEmpty(paramString))
      localHashMap.put("clazz", paramString);
    return new RequestUrl("http://market.xiaomi.com/thm/subject/index", localHashMap);
  }

  public RequestUrl getDetailUrl(String paramString)
  {
    Object localObject;
    boolean bool;
    if (getAccount() == null)
    {
      localObject = new Object[1];
      localObject[0] = paramString;
      localObject = String.format("http://market.xiaomi.com/thm/details/%s", localObject);
      bool = false;
    }
    else
    {
      localObject = new Object[1];
      localObject[0] = paramString;
      localObject = String.format("http://market.xiaomi.com/thm/native/details/%s", localObject);
      bool = true;
    }
    return (RequestUrl)new RequestUrl((String)localObject, bool, bool);
  }

  public RequestUrl getDownloadUrl(String paramString)
  {
    Object localObject = new Object[1];
    localObject[0] = paramString;
    RequestUrl localRequestUrl = new RequestUrl(String.format("http://market.xiaomi.com/thm/download/%s", localObject));
    localObject = getVersionParameters();
    localRequestUrl.putParameter("device", (String)((Map)localObject).get("device"));
    localRequestUrl.putParameter("system", (String)((Map)localObject).get("system"));
    localRequestUrl.putParameter("version", (String)((Map)localObject).get("version"));
    return (RequestUrl)localRequestUrl;
  }

  public RequestUrl getListMetaUrl(String paramString)
  {
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = paramString;
    return new RequestUrl(String.format("http://market.xiaomi.com/thm/subject/metadata/%s", arrayOfObject));
  }

  public RequestUrl getRecommendListUrl(String paramString1, String paramString2)
  {
    Object localObject = new Object[1];
    localObject[0] = paramString1;
    localObject = String.format("http://market.xiaomi.com/thm/subject/%s", localObject);
    HashMap localHashMap = new HashMap();
    localHashMap.put("category", this.context.getResourceStamp());
    if (!TextUtils.isEmpty(paramString2))
      localHashMap.put("clazz", paramString2);
    return (RequestUrl)new RequestUrl((String)localObject, localHashMap);
  }

  public RequestUrl getRecommendUrl()
  {
    String str = this.context.getResourceStamp();
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = str;
    return new RequestUrl(String.format("http://market.xiaomi.com/thm/recommendation/list/%s", arrayOfObject));
  }

  public RequestUrl getRightsDownloadUrl(String paramString1, String paramString2, String paramString3)
  {
    HashMap localHashMap = new HashMap();
    if (!TextUtils.isEmpty(paramString1))
      localHashMap.put("productId", paramString1);
    if (!TextUtils.isEmpty(paramString2))
      localHashMap.put("hash", paramString2);
    if (!TextUtils.isEmpty(paramString3))
      localHashMap.put("subhash", paramString3);
    return new RequestUrl("http://market.xiaomi.com/thm/native/down", localHashMap, true, true);
  }

  public RequestUrl getRightsIssueUrl(String paramString)
  {
    return new RequestUrl("http://drm.market.xiaomi.com/issue", 1, paramString);
  }

  public RequestUrl getSearchListUrl(String paramString)
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("apiversion", "1");
    localHashMap.put("keywords", paramString);
    localHashMap.put("category", this.context.getResourceStamp());
    return new RequestUrl("http://market.xiaomi.com/thm/search", localHashMap);
  }

  public RequestUrl getVersionUrl(List<String> paramList)
  {
    HashMap localHashMap = new HashMap();
    StringBuilder localStringBuilder = new StringBuilder();
    Iterator localIterator = paramList.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
      {
        localHashMap.put("fileshash", localStringBuilder.substring(0, -1 + localStringBuilder.length()));
        return new RequestUrl("http://market.xiaomi.com/thm/checkupdate", localHashMap);
      }
      localStringBuilder.append((String)localIterator.next());
      localStringBuilder.append(",");
    }
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.controller.online.OnlineService
 * JD-Core Version:    0.6.0
 */