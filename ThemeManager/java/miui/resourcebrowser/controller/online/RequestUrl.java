package miui.resourcebrowser.controller.online;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class RequestUrl
  implements Serializable, Cloneable
{
  public static final int METHOD_GET = 0;
  public static final int METHOD_POST = 1;
  private static final long serialVersionUID = 1L;
  private String baseUrl;
  private String body;
  private boolean decryptionNeeded;
  private boolean encryptionNeeded;
  private int httpMethod;
  private Map<String, String> parameters = new HashMap();

  public RequestUrl()
  {
  }

  public RequestUrl(String paramString)
  {
    this.baseUrl = paramString;
  }

  public RequestUrl(String paramString1, int paramInt, String paramString2)
  {
    this.baseUrl = paramString1;
    this.httpMethod = paramInt;
    this.body = paramString2;
  }

  public RequestUrl(String paramString, Map<String, String> paramMap)
  {
    this.baseUrl = paramString;
    this.parameters = paramMap;
  }

  public RequestUrl(String paramString, Map<String, String> paramMap, boolean paramBoolean1, boolean paramBoolean2)
  {
    this.baseUrl = paramString;
    this.parameters = paramMap;
    this.encryptionNeeded = paramBoolean1;
    this.decryptionNeeded = paramBoolean2;
  }

  public RequestUrl(String paramString, boolean paramBoolean1, boolean paramBoolean2)
  {
    this.baseUrl = paramString;
    this.encryptionNeeded = paramBoolean1;
    this.decryptionNeeded = paramBoolean2;
  }

  public Object clone()
  {
    try
    {
      Object localObject1 = super.clone();
      localObject1 = localObject1;
      return localObject1;
    }
    catch (CloneNotSupportedException localObject2)
    {
      while (true)
      {
        localCloneNotSupportedException.printStackTrace();
        Object localObject2 = null;
      }
    }
  }

  public String getBaseUrl()
  {
    return this.baseUrl;
  }

  public String getBody()
  {
    return this.body;
  }

  public String getBodyPart()
  {
    String str = null;
    if (this.httpMethod == 1)
      if (this.body == null)
        str = getParameterPart();
      else
        str = this.body;
    return str;
  }

  public int getHttpMethod()
  {
    return this.httpMethod;
  }

  public String getHttpMethodName()
  {
    String str;
    if (this.httpMethod != 0)
      str = "POST";
    else
      str = "GET";
    return str;
  }

  public String getParameter(String paramString)
  {
    return (String)this.parameters.get(paramString);
  }

  public String getParameter(String paramString1, String paramString2)
  {
    String str = (String)this.parameters.get(paramString1);
    if (str == null)
      str = paramString2;
    return str;
  }

  public String getParameterPart()
  {
    StringBuffer localStringBuffer = new StringBuffer();
    if ((this.parameters != null) && (!this.parameters.isEmpty()))
    {
      if (getHttpMethod() == 0)
        localStringBuffer.append("?");
      try
      {
        Iterator localIterator = this.parameters.entrySet().iterator();
        while (localIterator.hasNext())
        {
          Map.Entry localEntry = (Map.Entry)localIterator.next();
          localStringBuffer.append((String)localEntry.getKey());
          localStringBuffer.append("=");
          localStringBuffer.append(URLEncoder.encode((String)localEntry.getValue(), "UTF-8"));
          localStringBuffer.append("&");
        }
      }
      catch (UnsupportedEncodingException localUnsupportedEncodingException)
      {
        localUnsupportedEncodingException.printStackTrace();
      }
    }
    while (true)
    {
      return localStringBuffer.toString();
      localStringBuffer.deleteCharAt(-1 + localStringBuffer.length());
    }
  }

  public Map<String, String> getParameters()
  {
    return this.parameters;
  }

  public String getUrl()
  {
    String str;
    if (this.httpMethod == 1)
      str = this.baseUrl;
    else
      str = this.baseUrl + getParameterPart();
    return str;
  }

  public boolean isDecryptionNeeded()
  {
    return this.decryptionNeeded;
  }

  public boolean isEncryptionNeeded()
  {
    return this.encryptionNeeded;
  }

  public void putParameter(String paramString1, String paramString2)
  {
    this.parameters.put(paramString1, paramString2);
  }

  public void setBaseUrl(String paramString)
  {
    this.baseUrl = paramString;
  }

  public void setBody(String paramString)
  {
    this.body = paramString;
  }

  public void setDecryptionNeeded(boolean paramBoolean)
  {
    this.decryptionNeeded = paramBoolean;
  }

  public void setEncryptionNeeded(boolean paramBoolean)
  {
    this.encryptionNeeded = paramBoolean;
  }

  public void setHttpMethod(int paramInt)
  {
    this.httpMethod = paramInt;
  }

  public void setParameters(Map<String, String> paramMap)
  {
    this.parameters = paramMap;
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.controller.online.RequestUrl
 * JD-Core Version:    0.6.0
 */