package miui.resourcebrowser.controller.online;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import miui.resourcebrowser.ResourceConstants;
import miui.resourcebrowser.ResourceContext;
import miui.resourcebrowser.controller.DataManager;
import miui.resourcebrowser.controller.OnlineDataManager;
import miui.resourcebrowser.controller.ResourceController;
import miui.resourcebrowser.model.RecommendItemData;
import miui.resourcebrowser.model.Resource;
import miui.resourcebrowser.model.ResourceCategory;
import miui.resourcebrowser.model.ResourceListMeta;
import miui.resourcebrowser.util.ResourceHelper;

public class HttpServerDataManager extends OnlineDataManager
  implements ResourceConstants
{
  private List<Resource> associationResources;
  private List<ResourceCategory> categories;
  private List<List<Resource>> dataSet = new ArrayList();
  private ResourceListMeta listMeta;
  private Map<String, Resource> localIdIndex = new HashMap();
  private Map<String, Resource> onlineIdIndex = new HashMap();
  private OnlineDataParser parser = getDataParser();
  private List<RecommendItemData> recommends;
  private OnlineService service = getService();
  private List<Resource> updatableResources;

  public HttpServerDataManager(ResourceContext paramResourceContext)
  {
    super(paramResourceContext);
  }

  private String composeAssociationPath(List<String> paramList)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    Iterator localIterator = paramList.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return this.context.getAssociationCacheFolder() + localStringBuilder.substring(0, -1 + localStringBuilder.length());
      localStringBuilder.append((String)localIterator.next());
      localStringBuilder.append(",");
    }
  }

  private RequestUrl composeAssociationUrl(List<String> paramList)
  {
    return this.service.getAssociationUrl(paramList);
  }

  private String composeCategoryPath()
  {
    return this.context.getCategoryCacheFolder() + "category";
  }

  private RequestUrl composeCategoryUrl()
  {
    return this.service.getCategoryUrl();
  }

  private String composeDetailPath(String paramString)
  {
    return this.context.getDetailCacheFolder() + paramString;
  }

  private RequestUrl composeDetailUrl(String paramString)
  {
    return this.service.getDetailUrl(paramString);
  }

  private String composeListMetaPath(String paramString)
  {
    return this.context.getListCacheFolder() + paramString;
  }

  private RequestUrl composeListMetaUrl(String paramString)
  {
    return this.service.getListMetaUrl(paramString);
  }

  private String composeListPath(int paramInt)
  {
    return this.context.getListCacheFolder() + ResourceHelper.getFileName(composeListUrl(paramInt).getUrl());
  }

  private RequestUrl composeListUrl(int paramInt)
  {
    RequestUrl localRequestUrl = (RequestUrl)this.context.getListUrl().clone();
    localRequestUrl.setParameters(new HashMap(this.context.getListUrl().getParameters()));
    int i = this.context.getPageItemCount();
    localRequestUrl.putParameter("start", String.valueOf(paramInt * i));
    localRequestUrl.putParameter("count", String.valueOf(i));
    return localRequestUrl;
  }

  private String composeRecommendPath()
  {
    return this.context.getRecommendCacheFolder() + "recommend";
  }

  private RequestUrl composeRecommendUrl()
  {
    return this.service.getRecommendUrl();
  }

  private String composeVersionPath(List<String> paramList)
  {
    return this.context.getVersionCacheFolder() + "version";
  }

  private RequestUrl composeVersionUrl(List<String> paramList)
  {
    return this.service.getVersionUrl(paramList);
  }

  private OnlineDataParser getDataParser()
  {
    return new OnlineJSONDataParser(this.context);
  }

  private OnlineService getService()
  {
    return new OnlineService(this.context);
  }

  private boolean needRefresh(String paramString, long paramLong)
  {
    int i;
    if (System.currentTimeMillis() - new File(paramString).lastModified() <= paramLong)
      i = 0;
    else
      i = 1;
    return i;
  }

  private boolean refreshFileCache(RequestUrl paramRequestUrl, String paramString)
  {
    DownloadFileTask localDownloadFileTask = new DownloadFileTask(paramRequestUrl.getUrl());
    int i = 1;
    if (paramRequestUrl.isEncryptionNeeded())
      i |= 2;
    return localDownloadFileTask.downloadFile(paramRequestUrl, paramString, i);
  }

  private void updateAssociationCache(String paramString, List<String> paramList)
  {
    this.associationResources = this.parser.readAssociationResources(paramString);
  }

  private void updateCategoryCache(String paramString)
  {
    this.categories = this.parser.readCategories(paramString);
  }

  private void updateDetailCache(String paramString1, String paramString2)
  {
    Resource localResource = this.parser.readResource(paramString1);
    if (localResource != null)
    {
      Object localObject = localResource.getOnlineId();
      if ((localObject != null) && (!((String)localObject).trim().equals("")))
      {
        localObject = (Resource)this.onlineIdIndex.get(paramString2);
        if (localObject != null)
          ((Resource)localObject).updateFrom(localResource);
        else
          this.onlineIdIndex.put(paramString2, localResource);
      }
    }
  }

  private void updateListCache(String paramString, int paramInt)
  {
    if (this.dataSet.size() <= paramInt);
    Object localObject1;
    for (int i = this.dataSet.size(); ; localObject1++)
    {
      if (i > paramInt)
      {
        localObject1 = ((List)this.dataSet.get(paramInt)).iterator();
        while (true)
        {
          if (!((Iterator)localObject1).hasNext())
          {
            localObject1 = this.parser.readResources(paramString);
            this.dataSet.set(paramInt, localObject1);
            if (localObject1 != null)
              localObject1 = ((List)localObject1).iterator();
            while (true)
            {
              if (!((Iterator)localObject1).hasNext())
                return;
              localObject2 = (Resource)((Iterator)localObject1).next();
              str1 = ((Resource)localObject2).getLocalId();
              String str2 = ((Resource)localObject2).getOnlineId();
              if (str1 != null)
                this.localIdIndex.put(str1, localObject2);
              if (str2 == null)
                continue;
              this.onlineIdIndex.put(str2, localObject2);
            }
          }
          Object localObject2 = (Resource)((Iterator)localObject1).next();
          String str1 = ((Resource)localObject2).getLocalId();
          localObject2 = ((Resource)localObject2).getOnlineId();
          if (str1 != null)
            this.localIdIndex.remove(str1);
          if (localObject2 == null)
            continue;
          this.onlineIdIndex.remove(localObject2);
        }
      }
      this.dataSet.add(new ArrayList());
    }
  }

  private void updateListMetaCache(String paramString)
  {
    this.listMeta = this.parser.readResourceListMeta(paramString);
  }

  private void updateRecommendCache(String paramString)
  {
    this.recommends = this.parser.readRecommends(paramString);
  }

  private void updateVersionCache(String paramString, List<String> paramList)
  {
    this.updatableResources = this.parser.readUpdatableResources(paramString);
  }

  public List<Resource> getAssociationResources(List<String> paramList)
  {
    return getAssociationResources(paramList, false);
  }

  public List<Resource> getAssociationResources(List<String> paramList, boolean paramBoolean)
  {
    RequestUrl localRequestUrl = composeAssociationUrl(paramList);
    String str = composeAssociationPath(paramList);
    if ((!paramBoolean) && (!needRefresh(str, 86400000L)))
    {
      updateAssociationCache(str, paramList);
    }
    else
    {
      refreshFileCache(localRequestUrl, str);
      updateAssociationCache(str, paramList);
    }
    return this.associationResources;
  }

  public List<RecommendItemData> getRecommends()
  {
    return getRecommends(false);
  }

  public List<RecommendItemData> getRecommends(boolean paramBoolean)
  {
    RequestUrl localRequestUrl = composeRecommendUrl();
    String str = composeRecommendPath();
    if ((!paramBoolean) && (!needRefresh(str, 3600000L)))
    {
      if (this.recommends == null)
        updateRecommendCache(str);
    }
    else
    {
      refreshFileCache(localRequestUrl, str);
      updateRecommendCache(str);
    }
    return this.recommends;
  }

  public Resource getResource(String paramString)
  {
    return getResource(paramString, false);
  }

  public Resource getResource(String paramString, boolean paramBoolean)
  {
    RequestUrl localRequestUrl = composeDetailUrl(paramString);
    String str = composeDetailPath(paramString);
    if ((!paramBoolean) && (!needRefresh(str, 3600000L)))
    {
      updateDetailCache(str, paramString);
    }
    else
    {
      refreshFileCache(localRequestUrl, str);
      updateDetailCache(str, paramString);
    }
    return (Resource)this.onlineIdIndex.get(paramString);
  }

  public Resource getResourceByLocalId(String paramString)
  {
    return (Resource)this.localIdIndex.get(paramString);
  }

  public List<ResourceCategory> getResourceCategories()
  {
    return getResourceCategories(false);
  }

  public List<ResourceCategory> getResourceCategories(boolean paramBoolean)
  {
    RequestUrl localRequestUrl = composeCategoryUrl();
    String str = composeCategoryPath();
    if ((!paramBoolean) && (!needRefresh(str, 86400000L)))
    {
      if (this.categories == null)
        updateCategoryCache(str);
    }
    else
    {
      refreshFileCache(localRequestUrl, str);
      updateCategoryCache(str);
    }
    return this.categories;
  }

  public ResourceListMeta getResourceListMeta(String paramString)
  {
    return getResourceListMeta(paramString, false);
  }

  public ResourceListMeta getResourceListMeta(String paramString, boolean paramBoolean)
  {
    RequestUrl localRequestUrl = composeListMetaUrl(paramString);
    String str = composeListMetaPath(paramString);
    if ((!paramBoolean) && (!needRefresh(str, 3600000L)))
    {
      if (this.listMeta == null)
        updateListMetaCache(str);
    }
    else
    {
      refreshFileCache(localRequestUrl, str);
      updateListMetaCache(str);
    }
    return this.listMeta;
  }

  public List<Resource> getResources(int paramInt)
  {
    return getResources(paramInt, false);
  }

  public List<Resource> getResources(int paramInt, boolean paramBoolean)
  {
    RequestUrl localRequestUrl = composeListUrl(paramInt);
    String str = composeListPath(paramInt);
    if ((paramInt == 0) && (!paramBoolean) && (!needRefresh(str, 300000L)))
    {
      if ((this.dataSet.size() <= paramInt) || (this.dataSet.get(paramInt) == null))
        updateListCache(str, paramInt);
    }
    else
    {
      refreshFileCache(localRequestUrl, str);
      updateListCache(str, paramInt);
    }
    return (List)this.dataSet.get(paramInt);
  }

  public List<Resource> getUpdatableResources(List<String> paramList)
  {
    return getUpdatableResources(paramList, false);
  }

  public List<Resource> getUpdatableResources(List<String> paramList, boolean paramBoolean)
  {
    RequestUrl localRequestUrl = composeVersionUrl(paramList);
    String str = composeVersionPath(paramList);
    if ((!paramBoolean) && (!needRefresh(str, 3600000L)))
    {
      if (this.updatableResources == null)
        updateVersionCache(str, paramList);
    }
    else
    {
      refreshFileCache(localRequestUrl, str);
      updateVersionCache(str, paramList);
    }
    return this.updatableResources;
  }

  public void setResourceController(ResourceController paramResourceController)
  {
    super.setResourceController(paramResourceController);
    this.parser.setResourceController(paramResourceController);
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.controller.online.HttpServerDataManager
 * JD-Core Version:    0.6.0
 */