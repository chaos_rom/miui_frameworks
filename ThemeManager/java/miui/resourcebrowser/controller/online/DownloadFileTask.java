package miui.resourcebrowser.controller.online;

import android.os.FileUtils;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import miui.resourcebrowser.model.PathEntry;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;

public class DownloadFileTask
{
  private String id;
  private List<DownloadFileObserver> observers = new ArrayList();

  public DownloadFileTask(String paramString)
  {
    this.id = paramString;
  }

  private boolean equals(Object paramObject1, Object paramObject2)
  {
    int i;
    if ((paramObject1 != paramObject2) && ((paramObject1 == null) || (paramObject2 == null) || (!paramObject1.equals(paramObject2))))
      i = 0;
    else
      i = 1;
    return i;
  }

  public static InputStream getUrlInputStream(RequestUrl paramRequestUrl, Map<String, String> paramMap)
    throws IOException, HttpStatusException
  {
    Object localObject;
    if (paramRequestUrl.getHttpMethod() != 0)
    {
      localObject = new HttpPost(paramRequestUrl.getBaseUrl());
      ((HttpPost)localObject).setEntity(new StringEntity(paramRequestUrl.getBodyPart()));
      localObject = localObject;
    }
    else
    {
      localObject = new HttpGet(paramRequestUrl.getUrl());
    }
    Iterator localIterator;
    if ((paramMap != null) && (!paramMap.isEmpty()))
      localIterator = paramMap.entrySet().iterator();
    while (true)
    {
      if (!localIterator.hasNext())
      {
        BasicHttpParams localBasicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(localBasicHttpParams, 20000);
        HttpConnectionParams.setSoTimeout(localBasicHttpParams, 20000);
        localObject = new DefaultHttpClient(localBasicHttpParams).execute((HttpUriRequest)localObject);
        int i = ((HttpResponse)localObject).getStatusLine().getStatusCode();
        if (i < 400)
          return new BufferedHttpEntity(((HttpResponse)localObject).getEntity()).getContent();
        throw new HttpStatusException("http response code is " + i);
      }
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      ((HttpUriRequest)localObject).addHeader(new BasicHeader((String)localEntry.getKey(), (String)localEntry.getValue()));
    }
  }

  private void notifyAllDownloadsCompleted(PathEntry[] paramArrayOfPathEntry)
  {
    Iterator localIterator = this.observers.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      ((DownloadFileObserver)localIterator.next()).onAllDownloadsCompleted(paramArrayOfPathEntry);
    }
  }

  private void notifyDownloadFailed(PathEntry paramPathEntry)
  {
    Iterator localIterator = this.observers.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      ((DownloadFileObserver)localIterator.next()).onDownloadFailed(paramPathEntry);
    }
  }

  private void notifyDownloadSuccessful(PathEntry paramPathEntry)
  {
    Iterator localIterator = this.observers.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      ((DownloadFileObserver)localIterator.next()).onDownloadSuccessful(paramPathEntry);
    }
  }

  private void writeToFile(RequestUrl paramRequestUrl, File paramFile, int paramInt)
    throws IOException, HttpStatusException
  {
    if (!paramFile.exists())
    {
      paramFile.getParentFile().mkdirs();
      paramFile.createNewFile();
    }
    InputStream localInputStream = getInputStream(paramRequestUrl, paramInt);
    FileOutputStream localFileOutputStream = new FileOutputStream(paramFile);
    byte[] arrayOfByte = new byte[1024];
    while (true)
    {
      int i = localInputStream.read(arrayOfByte);
      if (i < 0)
      {
        FileUtils.setPermissions(paramFile.getAbsolutePath(), 511, -1, -1);
        return;
      }
      localFileOutputStream.write(arrayOfByte, 0, i);
    }
  }

  public boolean downloadFile(RequestUrl paramRequestUrl, String paramString, int paramInt)
  {
    PathEntry localPathEntry = new PathEntry(paramString, paramRequestUrl.getUrl());
    File localFile2 = new File(paramString);
    File localFile1 = new File(paramString + ".temp");
    try
    {
      writeToFile(paramRequestUrl, localFile1, paramInt);
      localFile1.renameTo(localFile2);
      notifyDownloadSuccessful(localPathEntry);
      i = 1;
      return i;
    }
    catch (Exception localException)
    {
      while (true)
      {
        localException.printStackTrace();
        if (i.exists())
          i.delete();
        notifyDownloadFailed(localPathEntry);
        int i = 0;
      }
    }
  }

  public boolean downloadFile(PathEntry paramPathEntry, int paramInt)
  {
    RequestUrl localRequestUrl = new RequestUrl(paramPathEntry.getOnlinePath());
    if ((paramInt & 0x2) != 0)
      localRequestUrl.setEncryptionNeeded(true);
    return downloadFile(localRequestUrl, paramPathEntry.getLocalPath(), paramInt);
  }

  public boolean downloadFiles(PathEntry[] paramArrayOfPathEntry)
  {
    int i = 0;
    if (paramArrayOfPathEntry != null)
      i = 1;
    for (int j = 0; ; j++)
    {
      if (j >= paramArrayOfPathEntry.length)
      {
        notifyAllDownloadsCompleted(paramArrayOfPathEntry);
        i = i;
        return i;
      }
      if ((i == 0) || (!downloadFile(paramArrayOfPathEntry[j], 0)))
        i = 0;
      else
        i = 1;
    }
  }

  public boolean equals(Object paramObject)
  {
    int i = 1;
    if (this != paramObject)
      if (paramObject != null)
      {
        if (getClass() == paramObject.getClass())
        {
          DownloadFileTask localDownloadFileTask = (DownloadFileTask)paramObject;
          if (!equals(this.id, localDownloadFileTask.id))
            i = 0;
        }
        else
        {
          i = 0;
        }
      }
      else
        i = 0;
    return i;
  }

  public InputStream getInputStream(RequestUrl paramRequestUrl, int paramInt)
    throws IOException, HttpStatusException
  {
    Object localObject = null;
    if ((paramInt & 0x1) != 0)
    {
      RequestUrl localRequestUrl = (RequestUrl)paramRequestUrl.clone();
      HashMap localHashMap = new HashMap(paramRequestUrl.getParameters());
      localHashMap.putAll(OnlineService.getVersionParameters());
      localRequestUrl.setParameters(localHashMap);
      paramRequestUrl = localRequestUrl;
    }
    if ((paramInt & 0x2) != 0)
    {
      localObject = new HashMap();
      ((Map)localObject).put("Cookie", OnlineService.getUserCookies());
    }
    if (paramRequestUrl.isEncryptionNeeded())
      paramRequestUrl = OnlineService.getSecureUrl(paramRequestUrl);
    localObject = getUrlInputStream(paramRequestUrl, (Map)localObject);
    if (paramRequestUrl.isDecryptionNeeded())
      localObject = OnlineService.decryptStream((InputStream)localObject);
    return (InputStream)localObject;
  }

  public int hashCode()
  {
    int i;
    if (this.id != null)
      i = this.id.hashCode();
    else
      i = 0;
    return i + 31;
  }

  public static abstract interface DownloadFileObserver
  {
    public abstract void onAllDownloadsCompleted(PathEntry[] paramArrayOfPathEntry);

    public abstract void onDownloadFailed(PathEntry paramPathEntry);

    public abstract void onDownloadSuccessful(PathEntry paramPathEntry);
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.controller.online.DownloadFileTask
 * JD-Core Version:    0.6.0
 */