package miui.resourcebrowser.controller.online;

public class HttpStatusException extends Exception
{
  private static final long serialVersionUID = 1L;

  public HttpStatusException()
  {
  }

  public HttpStatusException(String paramString)
  {
    super(paramString);
  }

  public HttpStatusException(String paramString, Throwable paramThrowable)
  {
    super(paramString, paramThrowable);
  }

  public HttpStatusException(Throwable paramThrowable)
  {
    super(paramThrowable);
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.controller.online.HttpStatusException
 * JD-Core Version:    0.6.0
 */