package miui.resourcebrowser.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import miui.resourcebrowser.ResourceContext;

public abstract class DataManager
{
  protected ResourceContext context;
  protected ResourceController controller;
  protected List<DataSetObserver> observers = new ArrayList();

  public DataManager(ResourceContext paramResourceContext)
  {
    this.context = paramResourceContext;
  }

  public void addObserver(DataSetObserver paramDataSetObserver)
  {
    if (paramDataSetObserver != null)
      this.observers.add(paramDataSetObserver);
  }

  protected void notifyDataSetUpdateFailed()
  {
    Iterator localIterator = this.observers.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      ((DataSetObserver)localIterator.next()).onDataSetUpdateFailed();
    }
  }

  protected void notifyDataSetUpdateSuccessful()
  {
    Iterator localIterator = this.observers.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      ((DataSetObserver)localIterator.next()).onDataSetUpdateSuccessful();
    }
  }

  public void removeObserver(DataSetObserver paramDataSetObserver)
  {
    if (paramDataSetObserver != null)
      this.observers.remove(paramDataSetObserver);
  }

  public void setResourceController(ResourceController paramResourceController)
  {
    this.controller = paramResourceController;
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.controller.DataManager
 * JD-Core Version:    0.6.0
 */