package miui.resourcebrowser.controller;

public abstract interface DataSetObserver
{
  public abstract void onDataSetUpdateFailed();

  public abstract void onDataSetUpdateSuccessful();
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.controller.DataSetObserver
 * JD-Core Version:    0.6.0
 */