package miui.resourcebrowser;

import java.io.File;
import miui.os.Environment;
import miui.os.ExtraFileUtils;

public abstract interface ResourceConstants
{
  public static final String ASYNC_IMPORT_ALARM_PATH;
  public static final String ASYNC_IMPORT_LOCKSCREEN_PATH;
  public static final String ASYNC_IMPORT_NOTIFICATION_PATH;
  public static final String ASYNC_IMPORT_RINGTONE_PATH;
  public static final String ASYNC_IMPORT_ROOT_PATH;
  public static final String ASYNC_IMPORT_WALLPAPER_PATH;
  public static final String BUILDIN_IMAGE_ALARM_PATH;
  public static final String BUILDIN_IMAGE_LOCKSCREEN_PATH;
  public static final String BUILDIN_IMAGE_NOTIFICATION_PATH;
  public static final String BUILDIN_IMAGE_RINGTONE_PATH;
  public static final String BUILDIN_IMAGE_ROOT_PATH;
  public static final String BUILDIN_IMAGE_WALLPAPER_PATH;
  public static final String CONFIG_PATH;
  public static final String CONTENT_ALARM_PATH;
  public static final String CONTENT_LOCKSCREEN_PATH;
  public static final String CONTENT_NOTIFICATION_PATH;
  public static final String CONTENT_RINGTONE_PATH;
  public static final String CONTENT_ROOT_PATH;
  public static final String CONTENT_WALLPAPER_PATH;
  public static final String DOWNLOADED_ALARM_PATH;
  public static final String DOWNLOADED_LOCKSCREEN_PATH;
  public static final String DOWNLOADED_NOTIFICATION_PATH;
  public static final String DOWNLOADED_RINGTONE_PATH;
  public static final String DOWNLOADED_ROOT_PATH;
  public static final String DOWNLOADED_WALLPAPER_PATH;
  public static final String INDEX_ALARM_PATH;
  public static final String INDEX_LOCKSCREEN_PATH;
  public static final String INDEX_NOTIFICATION_PATH;
  public static final String INDEX_RINGTONE_PATH;
  public static final String INDEX_ROOT_PATH;
  public static final String INDEX_WALLPAPER_PATH;
  public static final String META_ALARM_PATH;
  public static final String META_LOCKSCREEN_PATH;
  public static final String META_NOTIFICATION_PATH;
  public static final String META_RINGTONE_PATH;
  public static final String META_ROOT_PATH;
  public static final String META_WALLPAPER_PATH;
  public static final String MIUI_EXTERNAL_PATH;
  public static final String MIUI_INTERNAL_PATH;
  public static final String MIUI_PATH = ExtraFileUtils.standardizeFolderPath(Environment.getMIUIStorageDirectory().getAbsolutePath());
  public static final String RIGHTS_ALARM_PATH;
  public static final String RIGHTS_LOCKSCREEN_PATH;
  public static final String RIGHTS_NOTIFICATION_PATH;
  public static final String RIGHTS_RINGTONE_PATH;
  public static final String RIGHTS_ROOT_PATH;
  public static final String RIGHTS_WALLPAPER_PATH;
  public static final String USER_ALARM_PATH;
  public static final String USER_LOCKSCREEN_PATH;
  public static final String USER_NOTIFICATION_PATH;
  public static final String USER_RINGTONE_PATH;
  public static final String USER_ROOT_PATH;
  public static final String USER_WALLPAPER_PATH;

  static
  {
    MIUI_INTERNAL_PATH = ExtraFileUtils.standardizeFolderPath(Environment.getMIUIInternalStorageDirectory().getAbsolutePath());
    MIUI_EXTERNAL_PATH = ExtraFileUtils.standardizeFolderPath(Environment.getMIUIExternalStorageDirectory().getAbsolutePath());
    USER_ROOT_PATH = MIUI_PATH;
    USER_WALLPAPER_PATH = USER_ROOT_PATH + "wallpaper/";
    USER_LOCKSCREEN_PATH = USER_ROOT_PATH + "wallpaper/";
    USER_RINGTONE_PATH = USER_ROOT_PATH + "ringtone/";
    USER_NOTIFICATION_PATH = USER_ROOT_PATH + "ringtone/";
    USER_ALARM_PATH = USER_ROOT_PATH + "ringtone/";
    DOWNLOADED_ROOT_PATH = MIUI_PATH;
    DOWNLOADED_WALLPAPER_PATH = DOWNLOADED_ROOT_PATH + "wallpaper/";
    DOWNLOADED_LOCKSCREEN_PATH = DOWNLOADED_ROOT_PATH + "wallpaper/";
    DOWNLOADED_RINGTONE_PATH = DOWNLOADED_ROOT_PATH + "ringtone/";
    DOWNLOADED_NOTIFICATION_PATH = DOWNLOADED_ROOT_PATH + "ringtone/";
    DOWNLOADED_ALARM_PATH = DOWNLOADED_ROOT_PATH + "ringtone/";
    META_ROOT_PATH = MIUI_PATH;
    META_WALLPAPER_PATH = META_ROOT_PATH + "wallpaper/";
    META_LOCKSCREEN_PATH = META_ROOT_PATH + "wallpaper/";
    META_RINGTONE_PATH = META_ROOT_PATH + "ringtone/";
    META_NOTIFICATION_PATH = META_ROOT_PATH + "ringtone/";
    META_ALARM_PATH = META_ROOT_PATH + "ringtone/";
    CONTENT_ROOT_PATH = MIUI_PATH;
    CONTENT_WALLPAPER_PATH = CONTENT_ROOT_PATH + "wallpaper/";
    CONTENT_LOCKSCREEN_PATH = CONTENT_ROOT_PATH + "wallpaper/";
    CONTENT_RINGTONE_PATH = CONTENT_ROOT_PATH + "ringtone/";
    CONTENT_NOTIFICATION_PATH = CONTENT_ROOT_PATH + "ringtone/";
    CONTENT_ALARM_PATH = CONTENT_ROOT_PATH + "ringtone/";
    RIGHTS_ROOT_PATH = MIUI_PATH;
    RIGHTS_WALLPAPER_PATH = RIGHTS_ROOT_PATH + "wallpaper/";
    RIGHTS_LOCKSCREEN_PATH = RIGHTS_ROOT_PATH + "wallpaper/";
    RIGHTS_RINGTONE_PATH = RIGHTS_ROOT_PATH + "ringtone/";
    RIGHTS_NOTIFICATION_PATH = RIGHTS_ROOT_PATH + "ringtone/";
    RIGHTS_ALARM_PATH = RIGHTS_ROOT_PATH + "ringtone/";
    BUILDIN_IMAGE_ROOT_PATH = MIUI_PATH;
    BUILDIN_IMAGE_WALLPAPER_PATH = BUILDIN_IMAGE_ROOT_PATH + "wallpaper/";
    BUILDIN_IMAGE_LOCKSCREEN_PATH = BUILDIN_IMAGE_ROOT_PATH + "wallpaper/";
    BUILDIN_IMAGE_RINGTONE_PATH = BUILDIN_IMAGE_ROOT_PATH + "ringtone/";
    BUILDIN_IMAGE_NOTIFICATION_PATH = BUILDIN_IMAGE_ROOT_PATH + "ringtone/";
    BUILDIN_IMAGE_ALARM_PATH = BUILDIN_IMAGE_ROOT_PATH + "ringtone/";
    INDEX_ROOT_PATH = MIUI_PATH;
    INDEX_WALLPAPER_PATH = INDEX_ROOT_PATH + "wallpaper/";
    INDEX_LOCKSCREEN_PATH = INDEX_ROOT_PATH + "wallpaper/";
    INDEX_RINGTONE_PATH = INDEX_ROOT_PATH + "ringtone/";
    INDEX_NOTIFICATION_PATH = INDEX_ROOT_PATH + "ringtone/";
    INDEX_ALARM_PATH = INDEX_ROOT_PATH + "ringtone/";
    ASYNC_IMPORT_ROOT_PATH = MIUI_PATH;
    ASYNC_IMPORT_WALLPAPER_PATH = ASYNC_IMPORT_ROOT_PATH + "wallpaper/";
    ASYNC_IMPORT_LOCKSCREEN_PATH = ASYNC_IMPORT_ROOT_PATH + "wallpaper/";
    ASYNC_IMPORT_RINGTONE_PATH = ASYNC_IMPORT_ROOT_PATH + "ringtone/";
    ASYNC_IMPORT_NOTIFICATION_PATH = ASYNC_IMPORT_ROOT_PATH + "ringtone/";
    ASYNC_IMPORT_ALARM_PATH = ASYNC_IMPORT_ROOT_PATH + "ringtone/";
    CONFIG_PATH = MIUI_PATH + ".config/";
  }
}

/* Location:           /media/Android_3T/Android/Roms/ChameleonOS/MIUI.us_crespo_v4_2.11.16_Eng_Deo_ZipA_Signed/system/app/ThemeManager_dex2jar.jar
 * Qualified Name:     miui.resourcebrowser.ResourceConstants
 * JD-Core Version:    0.6.0
 */