.class Lmiui/resourcebrowser/activity/ResourceDetailActivity$5;
.super Ljava/lang/Object;
.source "ResourceDetailActivity.java"

# interfaces
.implements Lmiui/resourcebrowser/util/ImageCacheDecoder$ImageDecodingListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiui/resourcebrowser/activity/ResourceDetailActivity;->initImageDecoder()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;


# direct methods
.method constructor <init>(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 674
    iput-object p1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$5;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleDecodingResult(ZLjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .parameter "result"
    .parameter "localPath"
    .parameter "onlinePath"

    .prologue
    .line 691
    if-eqz p1, :cond_0

    .line 692
    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$5;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    #calls: Lmiui/resourcebrowser/activity/ResourceDetailActivity;->getScreenViewIndexForPreviewImage(Ljava/lang/String;)I
    invoke-static {v1, p2}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->access$200(Lmiui/resourcebrowser/activity/ResourceDetailActivity;Ljava/lang/String;)I

    move-result v0

    .line 693
    .local v0, screenIndex:I
    if-ltz v0, :cond_0

    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$5;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    #calls: Lmiui/resourcebrowser/activity/ResourceDetailActivity;->isVisibleScreen(I)Z
    invoke-static {v1, v0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->access$300(Lmiui/resourcebrowser/activity/ResourceDetailActivity;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 694
    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$5;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    #calls: Lmiui/resourcebrowser/activity/ResourceDetailActivity;->decodeImageForScreenView(I)V
    invoke-static {v1, v0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->access$500(Lmiui/resourcebrowser/activity/ResourceDetailActivity;I)V

    .line 697
    .end local v0           #screenIndex:I
    :cond_0
    return-void
.end method

.method public handleDownloadResult(ZLjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .parameter "result"
    .parameter "localPath"
    .parameter "onlinePath"

    .prologue
    .line 678
    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$5;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    #calls: Lmiui/resourcebrowser/activity/ResourceDetailActivity;->getScreenViewIndexForPreviewImage(Ljava/lang/String;)I
    invoke-static {v1, p2}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->access$200(Lmiui/resourcebrowser/activity/ResourceDetailActivity;Ljava/lang/String;)I

    move-result v0

    .line 679
    .local v0, screenIndex:I
    if-ltz v0, :cond_0

    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$5;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    #calls: Lmiui/resourcebrowser/activity/ResourceDetailActivity;->isVisibleScreen(I)Z
    invoke-static {v1, v0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->access$300(Lmiui/resourcebrowser/activity/ResourceDetailActivity;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 680
    if-eqz p1, :cond_1

    .line 681
    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$5;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    #getter for: Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mImageCacheDecoder:Lmiui/resourcebrowser/util/ImageCacheDecoder;
    invoke-static {v1}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->access$400(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)Lmiui/resourcebrowser/util/ImageCacheDecoder;

    move-result-object v1

    invoke-virtual {v1, p2, p3, v0}, Lmiui/resourcebrowser/util/ImageCacheDecoder;->decodeImageAsync(Ljava/lang/String;Ljava/lang/String;I)V

    .line 687
    :cond_0
    :goto_0
    return-void

    .line 683
    :cond_1
    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$5;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    const v2, 0x60c0024

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
