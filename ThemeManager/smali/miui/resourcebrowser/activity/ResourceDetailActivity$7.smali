.class Lmiui/resourcebrowser/activity/ResourceDetailActivity$7;
.super Landroid/animation/AnimatorListenerAdapter;
.source "ResourceDetailActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiui/resourcebrowser/activity/ResourceDetailActivity;->buildModeChangeAnimator()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;


# direct methods
.method constructor <init>(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 784
    iput-object p1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$7;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .parameter "animation"

    .prologue
    .line 800
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$7;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    iget-object v0, v0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mCoverView:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 802
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$7;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    #calls: Lmiui/resourcebrowser/activity/ResourceDetailActivity;->onEndEnterFullScreenMode()V
    invoke-static {v0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->access$1100(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)V

    .line 804
    invoke-super {p0, p1}, Landroid/animation/AnimatorListenerAdapter;->onAnimationEnd(Landroid/animation/Animator;)V

    .line 805
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 5
    .parameter "animation"

    .prologue
    .line 787
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$7;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    #getter for: Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreviews:Ljava/util/List;
    invoke-static {v3}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->access$700(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$7;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    #getter for: Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreview:Lmiui/resourcebrowser/view/ResourceScreenView;
    invoke-static {v4}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->access$600(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)Lmiui/resourcebrowser/view/ResourceScreenView;

    move-result-object v4

    invoke-virtual {v4}, Lmiui/resourcebrowser/view/ResourceScreenView;->getCurrentScreenIndex()I

    move-result v4

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/resourcebrowser/model/PathEntry;

    .line 788
    .local v2, pInfo:Lmiui/resourcebrowser/model/PathEntry;
    invoke-virtual {v2}, Lmiui/resourcebrowser/model/PathEntry;->getLocalPath()Ljava/lang/String;

    move-result-object v1

    .line 789
    .local v1, localPath:Ljava/lang/String;
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$7;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    #getter for: Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mImageCacheDecoder:Lmiui/resourcebrowser/util/ImageCacheDecoder;
    invoke-static {v3}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->access$400(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)Lmiui/resourcebrowser/util/ImageCacheDecoder;

    move-result-object v3

    invoke-virtual {v3, v1}, Lmiui/resourcebrowser/util/ImageCacheDecoder;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 790
    .local v0, bitmap:Landroid/graphics/Bitmap;
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$7;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    iget-object v3, v3, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mCoverView:Landroid/widget/ImageView;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 791
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$7;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    iget-object v3, v3, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mCoverView:Landroid/widget/ImageView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 793
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$7;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    #calls: Lmiui/resourcebrowser/activity/ResourceDetailActivity;->onBeginEnterFullScreenMode()V
    invoke-static {v3}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->access$1000(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)V

    .line 795
    invoke-super {p0, p1}, Landroid/animation/AnimatorListenerAdapter;->onAnimationStart(Landroid/animation/Animator;)V

    .line 796
    return-void
.end method
