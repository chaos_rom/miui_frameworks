.class public Lmiui/resourcebrowser/activity/OnlineResourceListFragment;
.super Lmiui/resourcebrowser/activity/ResourceListFragment;
.source "OnlineResourceListFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/resourcebrowser/activity/OnlineResourceListFragment$DownloadListMetaDataTask;,
        Lmiui/resourcebrowser/activity/OnlineResourceListFragment$DownloadRecommendListTask;,
        Lmiui/resourcebrowser/activity/OnlineResourceListFragment$DownloadCategoryListTask;
    }
.end annotation


# instance fields
.field protected mCategoryAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Lmiui/resourcebrowser/model/ResourceCategory;",
            ">;"
        }
    .end annotation
.end field

.field protected mCategoryList:Landroid/widget/Spinner;

.field protected mHasSubRecommends:Z

.field protected mIsRecommendList:Z

.field protected mRecommendId:Ljava/lang/String;

.field protected mService:Lmiui/resourcebrowser/controller/online/OnlineService;

.field protected mSubRecommendAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Lmiui/resourcebrowser/model/RecommendItemData;",
            ">;"
        }
    .end annotation
.end field

.field protected mSubRecommendList:Landroid/widget/Spinner;

.field protected mSubRecommends:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/RecommendItemData;",
            ">;"
        }
    .end annotation
.end field

.field protected mTextView:Landroid/widget/TextView;

.field protected mUnevenGrid:Lmiui/resourcebrowser/view/UnevenGrid;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lmiui/resourcebrowser/activity/ResourceListFragment;-><init>()V

    .line 287
    return-void
.end method

.method static synthetic access$000(Lmiui/resourcebrowser/activity/OnlineResourceListFragment;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->updateContent(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private showSeeMoreTextOnly(Z)V
    .locals 3
    .parameter "needsShow"

    .prologue
    const/4 v1, 0x0

    .line 194
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v2, 0x60b0050

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz p1, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 195
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v2, 0x60b004e

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz p1, :cond_0

    const/4 v1, 0x4

    :cond_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 196
    return-void

    .line 194
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private updateContent(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .parameter "subRecommendId"
    .parameter "categoryCode"

    .prologue
    .line 185
    iget-object v1, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mService:Lmiui/resourcebrowser/controller/online/OnlineService;

    invoke-virtual {v1, p1, p2}, Lmiui/resourcebrowser/controller/online/OnlineService;->getRecommendListUrl(Ljava/lang/String;Ljava/lang/String;)Lmiui/resourcebrowser/controller/online/RequestUrl;

    move-result-object v0

    .line 186
    .local v0, url:Lmiui/resourcebrowser/controller/online/RequestUrl;
    iget-object v1, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v1, v0}, Lmiui/resourcebrowser/ResourceContext;->setListUrl(Lmiui/resourcebrowser/controller/online/RequestUrl;)V

    .line 187
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->getResourceController()Lmiui/resourcebrowser/controller/ResourceController;

    move-result-object v1

    iput-object v1, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mResController:Lmiui/resourcebrowser/controller/ResourceController;

    .line 188
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->restoreParams()V

    .line 189
    iget-object v1, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;

    invoke-virtual {v1}, Lmiui/resourcebrowser/activity/ResourceAdapter;->clearDataSet()V

    .line 190
    iget-object v1, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lmiui/resourcebrowser/activity/ResourceAdapter;->loadMoreData(Z)V

    .line 191
    return-void
.end method


# virtual methods
.method protected getAdapter()Lmiui/resourcebrowser/activity/ResourceAdapter;
    .locals 2

    .prologue
    .line 55
    new-instance v0, Lmiui/resourcebrowser/activity/OnlineResourceAdapter;

    iget-object v1, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-direct {v0, p0, v1}, Lmiui/resourcebrowser/activity/OnlineResourceAdapter;-><init>(Lmiui/resourcebrowser/activity/BaseFragment;Lmiui/resourcebrowser/ResourceContext;)V

    return-object v0
.end method

.method protected getContentView()I
    .locals 1

    .prologue
    .line 50
    const v0, 0x6030018

    return v0
.end method

.method protected getHeaderView()Landroid/view/View;
    .locals 10

    .prologue
    const v9, 0x602018e

    .line 200
    iget-boolean v5, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mIsRecommendList:Z

    if-nez v5, :cond_0

    .line 201
    new-instance v5, Lmiui/resourcebrowser/view/UnevenGrid;

    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-direct {v5, v6}, Lmiui/resourcebrowser/view/UnevenGrid;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mUnevenGrid:Lmiui/resourcebrowser/view/UnevenGrid;

    .line 202
    iget-object v5, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mUnevenGrid:Lmiui/resourcebrowser/view/UnevenGrid;

    new-instance v6, Lmiui/resourcebrowser/view/RecommendGridItemFactory;

    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    iget-object v8, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-direct {v6, v7, v8}, Lmiui/resourcebrowser/view/RecommendGridItemFactory;-><init>(Landroid/content/Context;Lmiui/resourcebrowser/ResourceContext;)V

    invoke-virtual {v5, v6}, Lmiui/resourcebrowser/view/UnevenGrid;->setGridItemFactory(Lmiui/resourcebrowser/view/UnevenGrid$GridItemFactory;)V

    .line 203
    iget-object v5, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mUnevenGrid:Lmiui/resourcebrowser/view/UnevenGrid;

    invoke-virtual {v5, v9}, Lmiui/resourcebrowser/view/UnevenGrid;->setBackgroundResource(I)V

    .line 205
    const/4 v0, 0x2

    .line 206
    .local v0, columnCount:I
    iget-object v5, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v5}, Lmiui/resourcebrowser/util/ResourceHelper;->getThumbnailGap(Landroid/content/Context;)I

    move-result v2

    .line 207
    .local v2, gridGap:I
    mul-int/lit8 v1, v2, 0x2

    .line 208
    .local v1, gridBottomMargin:I
    iget-object v5, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mUnevenGrid:Lmiui/resourcebrowser/view/UnevenGrid;

    iget-object v6, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mUnevenGrid:Lmiui/resourcebrowser/view/UnevenGrid;

    invoke-virtual {v6}, Lmiui/resourcebrowser/view/UnevenGrid;->getPaddingLeft()I

    move-result v6

    iget-object v7, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mUnevenGrid:Lmiui/resourcebrowser/view/UnevenGrid;

    invoke-virtual {v7}, Lmiui/resourcebrowser/view/UnevenGrid;->getPaddingRight()I

    move-result v7

    invoke-virtual {v5, v6, v2, v7, v1}, Lmiui/resourcebrowser/view/UnevenGrid;->setPadding(IIII)V

    .line 210
    iget-object v5, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mUnevenGrid:Lmiui/resourcebrowser/view/UnevenGrid;

    const/16 v6, 0xda

    const/16 v7, 0x84

    invoke-virtual {v5, v6, v7}, Lmiui/resourcebrowser/view/UnevenGrid;->setGridItemRatio(II)V

    .line 211
    iget-object v5, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mUnevenGrid:Lmiui/resourcebrowser/view/UnevenGrid;

    invoke-virtual {v5, v2}, Lmiui/resourcebrowser/view/UnevenGrid;->setGridItemGap(I)V

    .line 212
    iget-object v5, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mUnevenGrid:Lmiui/resourcebrowser/view/UnevenGrid;

    invoke-virtual {v5, v0}, Lmiui/resourcebrowser/view/UnevenGrid;->setColumnCount(I)V

    .line 213
    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3}, Landroid/graphics/Point;-><init>()V

    .line 214
    .local v3, p:Landroid/graphics/Point;
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v5

    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 215
    iget v5, v3, Landroid/graphics/Point;->x:I

    iget-object v6, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mUnevenGrid:Lmiui/resourcebrowser/view/UnevenGrid;

    invoke-virtual {v6}, Lmiui/resourcebrowser/view/UnevenGrid;->getPaddingLeft()I

    move-result v6

    sub-int/2addr v5, v6

    iget-object v6, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mUnevenGrid:Lmiui/resourcebrowser/view/UnevenGrid;

    invoke-virtual {v6}, Lmiui/resourcebrowser/view/UnevenGrid;->getPaddingRight()I

    move-result v6

    sub-int/2addr v5, v6

    mul-int/lit8 v6, v2, 0x1

    sub-int/2addr v5, v6

    div-int v4, v5, v0

    .line 217
    .local v4, recommendationWidth:I
    iget-object v5, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v5, v4}, Lmiui/resourcebrowser/ResourceContext;->setRecommendImageWidth(I)V

    .line 218
    iget-object v5, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mUnevenGrid:Lmiui/resourcebrowser/view/UnevenGrid;

    .line 225
    .end local v0           #columnCount:I
    .end local v1           #gridBottomMargin:I
    .end local v2           #gridGap:I
    .end local v3           #p:Landroid/graphics/Point;
    .end local v4           #recommendationWidth:I
    :goto_0
    return-object v5

    .line 219
    :cond_0
    iget-boolean v5, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mHasSubRecommends:Z

    if-nez v5, :cond_1

    .line 220
    new-instance v5, Landroid/widget/TextView;

    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mTextView:Landroid/widget/TextView;

    .line 221
    iget-object v5, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mTextView:Landroid/widget/TextView;

    const/16 v6, 0x13

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setGravity(I)V

    .line 222
    iget-object v5, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 223
    iget-object v5, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mTextView:Landroid/widget/TextView;

    goto :goto_0

    .line 225
    :cond_1
    const/4 v5, 0x0

    goto :goto_0
.end method

.method protected getSourceType()I
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x2

    return v0
.end method

.method protected initParams()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 66
    invoke-super {p0}, Lmiui/resourcebrowser/activity/ResourceListFragment;->initParams()V

    .line 67
    new-instance v0, Lmiui/resourcebrowser/controller/online/OnlineService;

    iget-object v3, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-direct {v0, v3}, Lmiui/resourcebrowser/controller/online/OnlineService;-><init>(Lmiui/resourcebrowser/ResourceContext;)V

    iput-object v0, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mService:Lmiui/resourcebrowser/controller/online/OnlineService;

    .line 68
    iget-object v0, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "REQUEST_IS_RECOMMEND_LIST"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mIsRecommendList:Z

    .line 69
    iget-object v0, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "REQUEST_SUB_RECOMMENDS"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mSubRecommends:Ljava/util/List;

    .line 70
    iget-object v0, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mSubRecommends:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mSubRecommends:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mHasSubRecommends:Z

    .line 71
    iget-boolean v0, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mIsRecommendList:Z

    if-eqz v0, :cond_1

    .line 72
    iget-object v0, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "REQUEST_RECOMMEND_ID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mRecommendId:Ljava/lang/String;

    .line 73
    iget-object v0, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    iget-object v1, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mService:Lmiui/resourcebrowser/controller/online/OnlineService;

    iget-object v2, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mRecommendId:Ljava/lang/String;

    invoke-virtual {v1, v2, v4}, Lmiui/resourcebrowser/controller/online/OnlineService;->getRecommendListUrl(Ljava/lang/String;Ljava/lang/String;)Lmiui/resourcebrowser/controller/online/RequestUrl;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/ResourceContext;->setListUrl(Lmiui/resourcebrowser/controller/online/RequestUrl;)V

    .line 77
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 70
    goto :goto_0

    .line 75
    :cond_1
    iget-object v0, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    iget-object v1, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mService:Lmiui/resourcebrowser/controller/online/OnlineService;

    invoke-virtual {v1, v4}, Lmiui/resourcebrowser/controller/online/OnlineService;->getCommonListUrl(Ljava/lang/String;)Lmiui/resourcebrowser/controller/online/RequestUrl;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/ResourceContext;->setListUrl(Lmiui/resourcebrowser/controller/online/RequestUrl;)V

    goto :goto_1
.end method

.method protected initializeDataSet()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 81
    iget-object v1, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "confirm_miui_disclaimer"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    .line 83
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MIUI_DISCLAIMER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 84
    .local v0, i:Landroid/content/Intent;
    const/16 v1, 0x3e8

    invoke-virtual {p0, v0, v1}, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 87
    .end local v0           #i:Landroid/content/Intent;
    :cond_0
    invoke-direct {p0, v3}, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->showSeeMoreTextOnly(Z)V

    .line 88
    iget-object v1, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;

    invoke-virtual {v1, v3}, Lmiui/resourcebrowser/activity/ResourceAdapter;->loadMoreData(Z)V

    .line 89
    iget-boolean v1, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mIsRecommendList:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v1}, Lmiui/resourcebrowser/ResourceContext;->isRecommendSupported()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 90
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->requestRecommends()V

    .line 96
    :cond_1
    :goto_0
    return-void

    .line 91
    :cond_2
    iget-boolean v1, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mHasSubRecommends:Z

    if-nez v1, :cond_3

    .line 92
    iget-object v1, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mRecommendId:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->requestListMeta(Ljava/lang/String;)V

    goto :goto_0

    .line 93
    :cond_3
    iget-object v1, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v1}, Lmiui/resourcebrowser/ResourceContext;->isCategorySupported()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 94
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->requestCategories()V

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 104
    const/16 v0, 0x3e8

    if-ne p1, v0, :cond_0

    if-nez p2, :cond_0

    .line 106
    iget-object v0, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 110
    :goto_0
    return-void

    .line 108
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lmiui/resourcebrowser/activity/ResourceListFragment;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method protected refreshDataSet()V
    .locals 0

    .prologue
    .line 100
    return-void
.end method

.method protected requestCategories()V
    .locals 2

    .prologue
    .line 229
    new-instance v0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment$DownloadCategoryListTask;

    invoke-direct {v0, p0}, Lmiui/resourcebrowser/activity/OnlineResourceListFragment$DownloadCategoryListTask;-><init>(Lmiui/resourcebrowser/activity/OnlineResourceListFragment;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/activity/OnlineResourceListFragment$DownloadCategoryListTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 230
    return-void
.end method

.method protected requestListMeta(Ljava/lang/String;)V
    .locals 3
    .parameter "id"

    .prologue
    .line 284
    new-instance v0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment$DownloadListMetaDataTask;

    invoke-direct {v0, p0}, Lmiui/resourcebrowser/activity/OnlineResourceListFragment$DownloadListMetaDataTask;-><init>(Lmiui/resourcebrowser/activity/OnlineResourceListFragment;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/activity/OnlineResourceListFragment$DownloadListMetaDataTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 285
    return-void
.end method

.method protected requestRecommends()V
    .locals 2

    .prologue
    .line 258
    new-instance v0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment$DownloadRecommendListTask;

    invoke-direct {v0, p0}, Lmiui/resourcebrowser/activity/OnlineResourceListFragment$DownloadRecommendListTask;-><init>(Lmiui/resourcebrowser/activity/OnlineResourceListFragment;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/activity/OnlineResourceListFragment$DownloadRecommendListTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 259
    return-void
.end method

.method protected setCategories(Ljava/util/List;)V
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/ResourceCategory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 246
    .local p1, categories:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/ResourceCategory;>;"
    if-eqz p1, :cond_0

    .line 247
    iget-object v2, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mCategoryAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v2}, Landroid/widget/ArrayAdapter;->clear()V

    .line 248
    new-instance v0, Lmiui/resourcebrowser/model/ResourceCategory;

    invoke-direct {v0}, Lmiui/resourcebrowser/model/ResourceCategory;-><init>()V

    .line 249
    .local v0, category:Lmiui/resourcebrowser/model/ResourceCategory;
    const v2, 0x60c0027

    invoke-virtual {p0, v2}, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lmiui/resourcebrowser/model/ResourceCategory;->setName(Ljava/lang/String;)V

    .line 250
    iget-object v2, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mCategoryAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v2, v0}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 251
    const/4 v1, 0x0

    .local v1, i:I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 252
    iget-object v2, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mCategoryAdapter:Landroid/widget/ArrayAdapter;

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 251
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 255
    .end local v0           #category:Lmiui/resourcebrowser/model/ResourceCategory;
    .end local v1           #i:I
    :cond_0
    return-void
.end method

.method protected setListMetaData(Lmiui/resourcebrowser/model/ResourceListMeta;)V
    .locals 2
    .parameter "listMetaData"

    .prologue
    .line 301
    if-eqz p1, :cond_0

    .line 302
    iget-object v0, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mTextView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lmiui/resourcebrowser/model/ResourceListMeta;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 303
    iget-object v0, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mTextView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 307
    :goto_0
    return-void

    .line 305
    :cond_0
    iget-object v0, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mTextView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method protected setRecommends(Ljava/util/List;)V
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/RecommendItemData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 275
    .local p1, recommendations:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/RecommendItemData;>;"
    if-eqz p1, :cond_0

    .line 276
    iget-object v0, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mUnevenGrid:Lmiui/resourcebrowser/view/UnevenGrid;

    invoke-virtual {v0, p1}, Lmiui/resourcebrowser/view/UnevenGrid;->updateData(Ljava/util/List;)V

    .line 277
    iget-object v0, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mUnevenGrid:Lmiui/resourcebrowser/view/UnevenGrid;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/view/UnevenGrid;->setVisibility(I)V

    .line 281
    :goto_0
    return-void

    .line 279
    :cond_0
    iget-object v0, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mUnevenGrid:Lmiui/resourcebrowser/view/UnevenGrid;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/view/UnevenGrid;->setVisibility(I)V

    goto :goto_0
.end method

.method protected setSubRecommends(Ljava/util/List;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/RecommendItemData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 310
    .local p1, recommends:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/RecommendItemData;>;"
    if-eqz p1, :cond_0

    .line 311
    iget-object v1, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mSubRecommendAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1}, Landroid/widget/ArrayAdapter;->clear()V

    .line 312
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 313
    iget-object v1, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mSubRecommendAdapter:Landroid/widget/ArrayAdapter;

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 312
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 316
    .end local v0           #i:I
    :cond_0
    return-void
.end method

.method protected setupUI()V
    .locals 7

    .prologue
    const v6, 0x1090009

    const v5, 0x1090008

    const/4 v4, 0x0

    .line 114
    invoke-super {p0}, Lmiui/resourcebrowser/activity/ResourceListFragment;->setupUI()V

    .line 115
    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->showSeeMoreTextOnly(Z)V

    .line 117
    iget-boolean v2, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mIsRecommendList:Z

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mHasSubRecommends:Z

    if-eqz v2, :cond_0

    .line 118
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->getView()Landroid/view/View;

    move-result-object v2

    const v3, 0x60b0051

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 119
    .local v1, filters:Landroid/view/View;
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 120
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->getView()Landroid/view/View;

    move-result-object v2

    const v3, 0x60b0080

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Spinner;

    iput-object v2, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mSubRecommendList:Landroid/widget/Spinner;

    .line 121
    new-instance v2, Landroid/widget/ArrayAdapter;

    iget-object v3, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mActivity:Landroid/app/Activity;

    invoke-direct {v2, v3, v5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mSubRecommendAdapter:Landroid/widget/ArrayAdapter;

    .line 122
    iget-object v2, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mSubRecommendAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v2, v6}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 123
    iget-object v2, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mSubRecommendList:Landroid/widget/Spinner;

    iget-object v3, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mSubRecommendAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 124
    iget-object v2, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mSubRecommendList:Landroid/widget/Spinner;

    invoke-virtual {v2, v4}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 125
    iget-object v2, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mSubRecommendList:Landroid/widget/Spinner;

    new-instance v3, Lmiui/resourcebrowser/activity/OnlineResourceListFragment$1;

    invoke-direct {v3, p0}, Lmiui/resourcebrowser/activity/OnlineResourceListFragment$1;-><init>(Lmiui/resourcebrowser/activity/OnlineResourceListFragment;)V

    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 149
    iget-object v2, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mSubRecommends:Ljava/util/List;

    invoke-virtual {p0, v2}, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->setSubRecommends(Ljava/util/List;)V

    .line 150
    iget-object v2, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v2}, Lmiui/resourcebrowser/ResourceContext;->isCategorySupported()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 151
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->getView()Landroid/view/View;

    move-result-object v2

    const v3, 0x60b0069

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    .line 152
    .local v0, categoryList:Landroid/widget/Spinner;
    new-instance v2, Landroid/widget/ArrayAdapter;

    iget-object v3, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mActivity:Landroid/app/Activity;

    invoke-direct {v2, v3, v5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mCategoryAdapter:Landroid/widget/ArrayAdapter;

    .line 153
    iget-object v2, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mCategoryAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v2, v6}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 154
    iget-object v2, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mCategoryAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 155
    invoke-virtual {v0, v4}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 156
    new-instance v2, Lmiui/resourcebrowser/activity/OnlineResourceListFragment$2;

    invoke-direct {v2, p0}, Lmiui/resourcebrowser/activity/OnlineResourceListFragment$2;-><init>(Lmiui/resourcebrowser/activity/OnlineResourceListFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 182
    .end local v0           #categoryList:Landroid/widget/Spinner;
    .end local v1           #filters:Landroid/view/View;
    :cond_0
    return-void
.end method
