.class Lmiui/resourcebrowser/activity/ResourceListFragment$1;
.super Ljava/lang/Object;
.source "ResourceListFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiui/resourcebrowser/activity/ResourceListFragment;->startImportResourceTask(Ljava/io/File;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/resourcebrowser/activity/ResourceListFragment;

.field final synthetic val$isResImportedFile:Ljava/io/File;


# direct methods
.method constructor <init>(Lmiui/resourcebrowser/activity/ResourceListFragment;Ljava/io/File;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 107
    iput-object p1, p0, Lmiui/resourcebrowser/activity/ResourceListFragment$1;->this$0:Lmiui/resourcebrowser/activity/ResourceListFragment;

    iput-object p2, p0, Lmiui/resourcebrowser/activity/ResourceListFragment$1;->val$isResImportedFile:Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 110
    new-instance v0, Lmiui/resourcebrowser/activity/ResourceListFragment$1$1;

    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceListFragment$1;->this$0:Lmiui/resourcebrowser/activity/ResourceListFragment;

    iget-object v1, v1, Lmiui/resourcebrowser/activity/ResourceListFragment;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "recovery-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceListFragment$1;->this$0:Lmiui/resourcebrowser/activity/ResourceListFragment;

    iget-object v3, v3, Lmiui/resourcebrowser/activity/ResourceListFragment;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v3}, Lmiui/resourcebrowser/ResourceContext;->getResourceCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lmiui/resourcebrowser/activity/ResourceListFragment$1$1;-><init>(Lmiui/resourcebrowser/activity/ResourceListFragment$1;Lmiui/resourcebrowser/ResourceContext;Ljava/lang/String;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    sget-object v3, Lmiui/resourcebrowser/controller/local/ImportResourceTask;->IMPORT_BY_DOWNLOAD_FOLDER:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/activity/ResourceListFragment$1$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 170
    return-void
.end method
