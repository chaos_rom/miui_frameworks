.class Lmiui/resourcebrowser/activity/ResourceListFragment$1$1;
.super Lmiui/resourcebrowser/controller/local/ImportResourceTask;
.source "ResourceListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiui/resourcebrowser/activity/ResourceListFragment$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lmiui/resourcebrowser/activity/ResourceListFragment$1;


# direct methods
.method constructor <init>(Lmiui/resourcebrowser/activity/ResourceListFragment$1;Lmiui/resourcebrowser/ResourceContext;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 110
    iput-object p1, p0, Lmiui/resourcebrowser/activity/ResourceListFragment$1$1;->this$1:Lmiui/resourcebrowser/activity/ResourceListFragment$1;

    invoke-direct {p0, p2, p3}, Lmiui/resourcebrowser/controller/local/ImportResourceTask;-><init>(Lmiui/resourcebrowser/ResourceContext;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 9
    .parameter "successNum"

    .prologue
    const/4 v6, 0x1

    const/4 v8, 0x0

    .line 122
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-gtz v3, :cond_0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1

    .line 123
    :cond_0
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceListFragment$1$1;->this$1:Lmiui/resourcebrowser/activity/ResourceListFragment$1;

    iget-object v3, v3, Lmiui/resourcebrowser/activity/ResourceListFragment$1;->this$0:Lmiui/resourcebrowser/activity/ResourceListFragment;

    iget-object v3, v3, Lmiui/resourcebrowser/activity/ResourceListFragment;->mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;

    invoke-virtual {v3}, Lmiui/resourcebrowser/activity/ResourceAdapter;->notifyDataSetChanged()V

    .line 124
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceListFragment$1$1;->this$1:Lmiui/resourcebrowser/activity/ResourceListFragment$1;

    iget-object v3, v3, Lmiui/resourcebrowser/activity/ResourceListFragment$1;->this$0:Lmiui/resourcebrowser/activity/ResourceListFragment;

    iget-object v3, v3, Lmiui/resourcebrowser/activity/ResourceListFragment;->mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;

    invoke-virtual {v3}, Lmiui/resourcebrowser/activity/ResourceAdapter;->loadData()V

    .line 127
    :cond_1
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceListFragment$1$1;->this$1:Lmiui/resourcebrowser/activity/ResourceListFragment$1;

    iget-object v3, v3, Lmiui/resourcebrowser/activity/ResourceListFragment$1;->this$0:Lmiui/resourcebrowser/activity/ResourceListFragment;

    iget-object v3, v3, Lmiui/resourcebrowser/activity/ResourceListFragment;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 128
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceListFragment$1$1;->this$1:Lmiui/resourcebrowser/activity/ResourceListFragment$1;

    iget-object v3, v3, Lmiui/resourcebrowser/activity/ResourceListFragment$1;->this$0:Lmiui/resourcebrowser/activity/ResourceListFragment;

    iget-object v3, v3, Lmiui/resourcebrowser/activity/ResourceListFragment;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 131
    :cond_2
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceListFragment$1$1;->this$1:Lmiui/resourcebrowser/activity/ResourceListFragment$1;

    iget-object v3, v3, Lmiui/resourcebrowser/activity/ResourceListFragment$1;->this$0:Lmiui/resourcebrowser/activity/ResourceListFragment;

    iget-object v3, v3, Lmiui/resourcebrowser/activity/ResourceListFragment;->mProgressBar:Landroid/view/View;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 133
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceListFragment$1$1;->this$1:Lmiui/resourcebrowser/activity/ResourceListFragment$1;

    iget-object v3, v3, Lmiui/resourcebrowser/activity/ResourceListFragment$1;->this$0:Lmiui/resourcebrowser/activity/ResourceListFragment;

    iget-object v3, v3, Lmiui/resourcebrowser/activity/ResourceListFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v3}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 134
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceListFragment$1$1;->this$1:Lmiui/resourcebrowser/activity/ResourceListFragment$1;

    iget-object v3, v3, Lmiui/resourcebrowser/activity/ResourceListFragment$1;->this$0:Lmiui/resourcebrowser/activity/ResourceListFragment;

    iget-object v3, v3, Lmiui/resourcebrowser/activity/ResourceListFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v3}, Landroid/app/ProgressDialog;->dismiss()V

    .line 137
    :cond_3
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceListFragment$1$1;->this$1:Lmiui/resourcebrowser/activity/ResourceListFragment$1;

    iget-object v3, v3, Lmiui/resourcebrowser/activity/ResourceListFragment$1;->this$0:Lmiui/resourcebrowser/activity/ResourceListFragment;

    iget-object v4, p0, Lmiui/resourcebrowser/activity/ResourceListFragment$1$1;->this$1:Lmiui/resourcebrowser/activity/ResourceListFragment$1;

    iget-object v4, v4, Lmiui/resourcebrowser/activity/ResourceListFragment$1;->val$isResImportedFile:Ljava/io/File;

    #calls: Lmiui/resourcebrowser/activity/ResourceListFragment;->createIsImportedFile(Ljava/io/File;)V
    invoke-static {v3, v4}, Lmiui/resourcebrowser/activity/ResourceListFragment;->access$000(Lmiui/resourcebrowser/activity/ResourceListFragment;Ljava/io/File;)V

    .line 139
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceListFragment$1$1;->getImportTotalNum()I

    move-result v1

    .line 140
    .local v1, importNum:I
    if-lez v1, :cond_5

    .line 142
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceListFragment$1$1;->this$1:Lmiui/resourcebrowser/activity/ResourceListFragment$1;

    iget-object v3, v3, Lmiui/resourcebrowser/activity/ResourceListFragment$1;->this$0:Lmiui/resourcebrowser/activity/ResourceListFragment;

    iget-object v3, v3, Lmiui/resourcebrowser/activity/ResourceListFragment;->mActivity:Landroid/app/Activity;

    invoke-direct {v0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 143
    .local v0, alertDialogBuilder:Landroid/app/AlertDialog$Builder;
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceListFragment$1$1;->this$1:Lmiui/resourcebrowser/activity/ResourceListFragment$1;

    iget-object v3, v3, Lmiui/resourcebrowser/activity/ResourceListFragment$1;->this$0:Lmiui/resourcebrowser/activity/ResourceListFragment;

    iget-object v3, v3, Lmiui/resourcebrowser/activity/ResourceListFragment;->mActivity:Landroid/app/Activity;

    const v4, 0x60c024a

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 144
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceListFragment$1$1;->this$1:Lmiui/resourcebrowser/activity/ResourceListFragment$1;

    iget-object v3, v3, Lmiui/resourcebrowser/activity/ResourceListFragment$1;->this$0:Lmiui/resourcebrowser/activity/ResourceListFragment;

    iget-object v3, v3, Lmiui/resourcebrowser/activity/ResourceListFragment;->mActivity:Landroid/app/Activity;

    const v4, 0x60c024b

    new-array v5, v6, [Ljava/lang/Object;

    aput-object p1, v5, v8

    invoke-virtual {v3, v4, v5}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 146
    .local v2, resultMessage:Ljava/lang/String;
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    sub-int v3, v1, v3

    if-lez v3, :cond_4

    .line 147
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lmiui/resourcebrowser/activity/ResourceListFragment$1$1;->this$1:Lmiui/resourcebrowser/activity/ResourceListFragment$1;

    iget-object v4, v4, Lmiui/resourcebrowser/activity/ResourceListFragment$1;->this$0:Lmiui/resourcebrowser/activity/ResourceListFragment;

    iget-object v4, v4, Lmiui/resourcebrowser/activity/ResourceListFragment;->mActivity:Landroid/app/Activity;

    const v5, 0x60c024c

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v7

    sub-int v7, v1, v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v4, v5, v6}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 150
    :cond_4
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 151
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceListFragment$1$1;->this$1:Lmiui/resourcebrowser/activity/ResourceListFragment$1;

    iget-object v3, v3, Lmiui/resourcebrowser/activity/ResourceListFragment$1;->this$0:Lmiui/resourcebrowser/activity/ResourceListFragment;

    iget-object v3, v3, Lmiui/resourcebrowser/activity/ResourceListFragment;->mActivity:Landroid/app/Activity;

    const v4, 0x1040013

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lmiui/resourcebrowser/activity/ResourceListFragment$1$1$1;

    invoke-direct {v4, p0}, Lmiui/resourcebrowser/activity/ResourceListFragment$1$1$1;-><init>(Lmiui/resourcebrowser/activity/ResourceListFragment$1$1;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 158
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    .line 160
    .end local v0           #alertDialogBuilder:Landroid/app/AlertDialog$Builder;
    .end local v2           #resultMessage:Ljava/lang/String;
    :cond_5
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 110
    check-cast p1, Ljava/lang/Integer;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/activity/ResourceListFragment$1$1;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 5

    .prologue
    .line 114
    const-string v0, "Resource Import Tag"

    .line 115
    .local v0, TAG:Ljava/lang/String;
    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceListFragment$1$1;->this$1:Lmiui/resourcebrowser/activity/ResourceListFragment$1;

    iget-object v2, v1, Lmiui/resourcebrowser/activity/ResourceListFragment$1;->this$0:Lmiui/resourcebrowser/activity/ResourceListFragment;

    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceListFragment$1$1;->this$1:Lmiui/resourcebrowser/activity/ResourceListFragment$1;

    iget-object v1, v1, Lmiui/resourcebrowser/activity/ResourceListFragment$1;->this$0:Lmiui/resourcebrowser/activity/ResourceListFragment;

    iget-object v1, v1, Lmiui/resourcebrowser/activity/ResourceListFragment;->mActivity:Landroid/app/Activity;

    const-string v3, "power"

    invoke-virtual {v1, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    const v3, 0x10000006

    const-string v4, "Resource Import Tag"

    invoke-virtual {v1, v3, v4}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, v2, Lmiui/resourcebrowser/activity/ResourceListFragment;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 117
    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceListFragment$1$1;->this$1:Lmiui/resourcebrowser/activity/ResourceListFragment$1;

    iget-object v1, v1, Lmiui/resourcebrowser/activity/ResourceListFragment$1;->this$0:Lmiui/resourcebrowser/activity/ResourceListFragment;

    iget-object v1, v1, Lmiui/resourcebrowser/activity/ResourceListFragment;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 118
    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 7
    .parameter "values"

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 164
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment$1$1;->this$1:Lmiui/resourcebrowser/activity/ResourceListFragment$1;

    iget-object v0, v0, Lmiui/resourcebrowser/activity/ResourceListFragment$1;->this$0:Lmiui/resourcebrowser/activity/ResourceListFragment;

    iget-object v0, v0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceListFragment$1$1;->this$1:Lmiui/resourcebrowser/activity/ResourceListFragment$1;

    iget-object v1, v1, Lmiui/resourcebrowser/activity/ResourceListFragment$1;->this$0:Lmiui/resourcebrowser/activity/ResourceListFragment;

    iget-object v1, v1, Lmiui/resourcebrowser/activity/ResourceListFragment;->mActivity:Landroid/app/Activity;

    const v2, 0x60c0249

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aget-object v4, p1, v5

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    aget-object v4, p1, v6

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 166
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment$1$1;->this$1:Lmiui/resourcebrowser/activity/ResourceListFragment$1;

    iget-object v0, v0, Lmiui/resourcebrowser/activity/ResourceListFragment$1;->this$0:Lmiui/resourcebrowser/activity/ResourceListFragment;

    iget-object v0, v0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceListFragment$1$1;->this$1:Lmiui/resourcebrowser/activity/ResourceListFragment$1;

    iget-object v1, v1, Lmiui/resourcebrowser/activity/ResourceListFragment$1;->this$0:Lmiui/resourcebrowser/activity/ResourceListFragment;

    aget-object v2, p1, v5

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    aget-object v3, p1, v6

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    #calls: Lmiui/resourcebrowser/activity/ResourceListFragment;->computeProgressPercentage(II)I
    invoke-static {v1, v2, v3}, Lmiui/resourcebrowser/activity/ResourceListFragment;->access$100(Lmiui/resourcebrowser/activity/ResourceListFragment;II)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 168
    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 110
    check-cast p1, [Ljava/lang/Integer;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/activity/ResourceListFragment$1$1;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method
