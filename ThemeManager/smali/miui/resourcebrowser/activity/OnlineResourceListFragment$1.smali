.class Lmiui/resourcebrowser/activity/OnlineResourceListFragment$1;
.super Ljava/lang/Object;
.source "OnlineResourceListFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->setupUI()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private firstTime:Z

.field final synthetic this$0:Lmiui/resourcebrowser/activity/OnlineResourceListFragment;


# direct methods
.method constructor <init>(Lmiui/resourcebrowser/activity/OnlineResourceListFragment;)V
    .locals 1
    .parameter

    .prologue
    .line 125
    iput-object p1, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment$1;->this$0:Lmiui/resourcebrowser/activity/OnlineResourceListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 127
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment$1;->firstTime:Z

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .parameter
    .parameter "view"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 131
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    iget-object v3, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment$1;->this$0:Lmiui/resourcebrowser/activity/OnlineResourceListFragment;

    iget-object v3, v3, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mSubRecommendAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v3, p3}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmiui/resourcebrowser/model/RecommendItemData;

    iget-object v2, v3, Lmiui/resourcebrowser/model/RecommendItemData;->itemId:Ljava/lang/String;

    .line 132
    .local v2, subRecommendId:Ljava/lang/String;
    const/4 v1, 0x0

    .line 133
    .local v1, categoryCode:Ljava/lang/String;
    iget-object v3, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment$1;->this$0:Lmiui/resourcebrowser/activity/OnlineResourceListFragment;

    iget-object v3, v3, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mCategoryList:Landroid/widget/Spinner;

    if-eqz v3, :cond_0

    .line 134
    iget-object v3, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment$1;->this$0:Lmiui/resourcebrowser/activity/OnlineResourceListFragment;

    iget-object v3, v3, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mCategoryList:Landroid/widget/Spinner;

    invoke-virtual {v3}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/resourcebrowser/model/ResourceCategory;

    .line 135
    .local v0, category:Lmiui/resourcebrowser/model/ResourceCategory;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lmiui/resourcebrowser/model/ResourceCategory;->getCode()Ljava/lang/String;

    move-result-object v1

    .line 137
    .end local v0           #category:Lmiui/resourcebrowser/model/ResourceCategory;
    :cond_0
    :goto_0
    iget-boolean v3, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment$1;->firstTime:Z

    if-eqz v3, :cond_2

    .line 138
    const/4 v3, 0x0

    iput-boolean v3, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment$1;->firstTime:Z

    .line 142
    :goto_1
    return-void

    .line 135
    .restart local v0       #category:Lmiui/resourcebrowser/model/ResourceCategory;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 140
    .end local v0           #category:Lmiui/resourcebrowser/model/ResourceCategory;
    :cond_2
    iget-object v3, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment$1;->this$0:Lmiui/resourcebrowser/activity/OnlineResourceListFragment;

    #calls: Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->updateContent(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v3, v2, v1}, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->access$000(Lmiui/resourcebrowser/activity/OnlineResourceListFragment;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 147
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    return-void
.end method
