.class public Lmiui/resourcebrowser/activity/ResourceSearchAdapter;
.super Lmiui/resourcebrowser/activity/ResourceAdapter;
.source "ResourceSearchAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/resourcebrowser/activity/ResourceSearchAdapter$AsyncLoadMoreResourceTask;,
        Lmiui/resourcebrowser/activity/ResourceSearchAdapter$AsyncLoadResourceTask;
    }
.end annotation


# instance fields
.field private final POST_LOAD_DATA_COUNT_MAX:I

.field private mKeyword:Ljava/lang/String;

.field private mService:Lmiui/resourcebrowser/controller/online/OnlineService;

.field private postLoadDataCount:I


# direct methods
.method public constructor <init>(Lmiui/resourcebrowser/activity/BaseFragment;Lmiui/resourcebrowser/ResourceContext;)V
    .locals 1
    .parameter "fragment"
    .parameter "resContext"

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Lmiui/resourcebrowser/activity/ResourceAdapter;-><init>(Lmiui/resourcebrowser/activity/BaseFragment;Lmiui/resourcebrowser/ResourceContext;)V

    .line 27
    const/4 v0, 0x2

    iput v0, p0, Lmiui/resourcebrowser/activity/ResourceSearchAdapter;->POST_LOAD_DATA_COUNT_MAX:I

    .line 31
    invoke-direct {p0, p2}, Lmiui/resourcebrowser/activity/ResourceSearchAdapter;->initParams(Lmiui/resourcebrowser/ResourceContext;)V

    .line 32
    return-void
.end method

.method static synthetic access$000(Lmiui/resourcebrowser/activity/ResourceSearchAdapter;)Ljava/lang/String;
    .locals 1
    .parameter "x0"

    .prologue
    .line 19
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceSearchAdapter;->mKeyword:Ljava/lang/String;

    return-object v0
.end method

.method private initParams(Lmiui/resourcebrowser/ResourceContext;)V
    .locals 1
    .parameter "resContext"

    .prologue
    .line 66
    new-instance v0, Lmiui/resourcebrowser/controller/online/OnlineService;

    invoke-direct {v0, p1}, Lmiui/resourcebrowser/controller/online/OnlineService;-><init>(Lmiui/resourcebrowser/ResourceContext;)V

    iput-object v0, p0, Lmiui/resourcebrowser/activity/ResourceSearchAdapter;->mService:Lmiui/resourcebrowser/controller/online/OnlineService;

    .line 67
    return-void
.end method


# virtual methods
.method protected getDownloadableFlag(Lmiui/resourcebrowser/model/Resource;I)I
    .locals 2
    .parameter "resourceItem"
    .parameter "group"

    .prologue
    .line 58
    invoke-super {p0, p1, p2}, Lmiui/resourcebrowser/activity/ResourceAdapter;->getDownloadableFlag(Lmiui/resourcebrowser/model/Resource;I)I

    move-result v0

    .line 59
    .local v0, flag:I
    const v1, 0x602003a

    if-ne v0, v1, :cond_0

    if-nez p2, :cond_0

    .line 60
    const/4 v0, 0x0

    .line 62
    :cond_0
    return v0
.end method

.method protected getGroupTitle(I)Ljava/lang/String;
    .locals 5
    .parameter "group"

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 41
    const/4 v0, 0x0

    .line 42
    .local v0, text:Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    .line 50
    invoke-super {p0, p1}, Lmiui/resourcebrowser/activity/ResourceAdapter;->getGroupTitle(I)Ljava/lang/String;

    move-result-object v0

    .line 53
    :goto_0
    return-object v0

    .line 44
    :pswitch_0
    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceSearchAdapter;->mContext:Landroid/app/Activity;

    const v2, 0x60c0014

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceSearchAdapter;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v3}, Lmiui/resourcebrowser/ResourceContext;->getResourceTitle()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 45
    goto :goto_0

    .line 47
    :pswitch_1
    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceSearchAdapter;->mContext:Landroid/app/Activity;

    const v2, 0x60c0015

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceSearchAdapter;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v3}, Lmiui/resourcebrowser/ResourceContext;->getResourceTitle()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 48
    goto :goto_0

    .line 42
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected getLoadDataTask()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/widget/AsyncAdapter",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">.Async",
            "LoadDataTask;",
            ">;"
        }
    .end annotation

    .prologue
    .line 76
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 77
    .local v1, tasks:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/widget/AsyncAdapter<Lmiui/resourcebrowser/model/Resource;>.AsyncLoadDataTask;>;"
    new-instance v0, Lmiui/resourcebrowser/activity/ResourceSearchAdapter$AsyncLoadResourceTask;

    invoke-direct {v0, p0}, Lmiui/resourcebrowser/activity/ResourceSearchAdapter$AsyncLoadResourceTask;-><init>(Lmiui/resourcebrowser/activity/ResourceSearchAdapter;)V

    .line 78
    .local v0, task:Lmiui/resourcebrowser/activity/ResourceSearchAdapter$AsyncLoadResourceTask;
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceSearchAdapter;->getRegisterAsyncTaskObserver()Lmiui/os/AsyncTaskObserver;

    move-result-object v2

    invoke-virtual {v0, v2}, Lmiui/resourcebrowser/activity/ResourceSearchAdapter$AsyncLoadResourceTask;->addObserver(Lmiui/os/AsyncTaskObserver;)V

    .line 79
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 80
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    return-object v1
.end method

.method protected getLoadMoreDataTask()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/widget/AsyncAdapter",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">.Async",
            "LoadMoreDataTask;",
            ">;"
        }
    .end annotation

    .prologue
    .line 99
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 100
    .local v1, tasks:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/widget/AsyncAdapter<Lmiui/resourcebrowser/model/Resource;>.AsyncLoadMoreDataTask;>;"
    new-instance v0, Lmiui/resourcebrowser/activity/ResourceSearchAdapter$AsyncLoadMoreResourceTask;

    invoke-direct {v0, p0}, Lmiui/resourcebrowser/activity/ResourceSearchAdapter$AsyncLoadMoreResourceTask;-><init>(Lmiui/resourcebrowser/activity/ResourceSearchAdapter;)V

    .line 101
    .local v0, task:Lmiui/resourcebrowser/activity/ResourceSearchAdapter$AsyncLoadMoreResourceTask;
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceSearchAdapter;->getRegisterAsyncTaskObserver()Lmiui/os/AsyncTaskObserver;

    move-result-object v2

    invoke-virtual {v0, v2}, Lmiui/resourcebrowser/activity/ResourceSearchAdapter$AsyncLoadMoreResourceTask;->addObserver(Lmiui/os/AsyncTaskObserver;)V

    .line 102
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 103
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 104
    return-object v1
.end method

.method protected postLoadData(Ljava/util/List;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 127
    .local p1, result:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/Resource;>;"
    invoke-super {p0, p1}, Lmiui/resourcebrowser/activity/ResourceAdapter;->postLoadData(Ljava/util/List;)V

    .line 128
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceSearchAdapter;->updateNoResultText()V

    .line 129
    return-void
.end method

.method protected postLoadMoreData(Ljava/util/List;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 119
    .local p1, result:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/Resource;>;"
    if-nez p1, :cond_1

    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceSearchAdapter;->mFragment:Lmiui/resourcebrowser/activity/BaseFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceSearchAdapter;->mFragment:Lmiui/resourcebrowser/activity/BaseFragment;

    invoke-virtual {v0}, Lmiui/resourcebrowser/activity/BaseFragment;->isVisibleForUser()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 120
    :cond_0
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceSearchAdapter;->mContext:Landroid/app/Activity;

    const v1, 0x60c0020

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 122
    :cond_1
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceSearchAdapter;->updateNoResultText()V

    .line 123
    return-void
.end method

.method public setKeyword(Ljava/lang/String;)V
    .locals 3
    .parameter "keyword"

    .prologue
    .line 70
    iput-object p1, p0, Lmiui/resourcebrowser/activity/ResourceSearchAdapter;->mKeyword:Ljava/lang/String;

    .line 71
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceSearchAdapter;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceSearchAdapter;->mService:Lmiui/resourcebrowser/controller/online/OnlineService;

    iget-object v2, p0, Lmiui/resourcebrowser/activity/ResourceSearchAdapter;->mKeyword:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lmiui/resourcebrowser/controller/online/OnlineService;->getSearchListUrl(Ljava/lang/String;)Lmiui/resourcebrowser/controller/online/RequestUrl;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/ResourceContext;->setListUrl(Lmiui/resourcebrowser/controller/online/RequestUrl;)V

    .line 72
    return-void
.end method

.method protected updateNoResultText()V
    .locals 2

    .prologue
    .line 133
    monitor-enter p0

    .line 134
    :try_start_0
    iget v0, p0, Lmiui/resourcebrowser/activity/ResourceSearchAdapter;->postLoadDataCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmiui/resourcebrowser/activity/ResourceSearchAdapter;->postLoadDataCount:I

    .line 135
    iget v0, p0, Lmiui/resourcebrowser/activity/ResourceSearchAdapter;->postLoadDataCount:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 136
    invoke-super {p0}, Lmiui/resourcebrowser/activity/ResourceAdapter;->updateNoResultText()V

    .line 138
    :cond_0
    monitor-exit p0

    .line 139
    return-void

    .line 138
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
