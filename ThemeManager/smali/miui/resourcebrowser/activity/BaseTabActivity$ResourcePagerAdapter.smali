.class public Lmiui/resourcebrowser/activity/BaseTabActivity$ResourcePagerAdapter;
.super Landroid/support/v4/view/PagerAdapter;
.source "BaseTabActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/resourcebrowser/activity/BaseTabActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "ResourcePagerAdapter"
.end annotation


# instance fields
.field private mCurTransaction:Landroid/app/FragmentTransaction;

.field private mFragmentManager:Landroid/app/FragmentManager;

.field final synthetic this$0:Lmiui/resourcebrowser/activity/BaseTabActivity;


# direct methods
.method public constructor <init>(Lmiui/resourcebrowser/activity/BaseTabActivity;)V
    .locals 1
    .parameter

    .prologue
    .line 159
    iput-object p1, p0, Lmiui/resourcebrowser/activity/BaseTabActivity$ResourcePagerAdapter;->this$0:Lmiui/resourcebrowser/activity/BaseTabActivity;

    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 160
    invoke-virtual {p1}, Lmiui/resourcebrowser/activity/BaseTabActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    iput-object v0, p0, Lmiui/resourcebrowser/activity/BaseTabActivity$ResourcePagerAdapter;->mFragmentManager:Landroid/app/FragmentManager;

    .line 161
    return-void
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 1
    .parameter "container"
    .parameter "position"
    .parameter "object"

    .prologue
    .line 185
    iget-object v0, p0, Lmiui/resourcebrowser/activity/BaseTabActivity$ResourcePagerAdapter;->mCurTransaction:Landroid/app/FragmentTransaction;

    if-nez v0, :cond_0

    .line 186
    iget-object v0, p0, Lmiui/resourcebrowser/activity/BaseTabActivity$ResourcePagerAdapter;->mFragmentManager:Landroid/app/FragmentManager;

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iput-object v0, p0, Lmiui/resourcebrowser/activity/BaseTabActivity$ResourcePagerAdapter;->mCurTransaction:Landroid/app/FragmentTransaction;

    .line 188
    :cond_0
    iget-object v0, p0, Lmiui/resourcebrowser/activity/BaseTabActivity$ResourcePagerAdapter;->mCurTransaction:Landroid/app/FragmentTransaction;

    check-cast p3, Landroid/app/Fragment;

    .end local p3
    invoke-virtual {v0, p3}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 189
    return-void
.end method

.method public finishUpdate(Landroid/view/ViewGroup;)V
    .locals 1
    .parameter "container"

    .prologue
    .line 193
    iget-object v0, p0, Lmiui/resourcebrowser/activity/BaseTabActivity$ResourcePagerAdapter;->mCurTransaction:Landroid/app/FragmentTransaction;

    if-eqz v0, :cond_0

    .line 194
    iget-object v0, p0, Lmiui/resourcebrowser/activity/BaseTabActivity$ResourcePagerAdapter;->mCurTransaction:Landroid/app/FragmentTransaction;

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 195
    const/4 v0, 0x0

    iput-object v0, p0, Lmiui/resourcebrowser/activity/BaseTabActivity$ResourcePagerAdapter;->mCurTransaction:Landroid/app/FragmentTransaction;

    .line 196
    iget-object v0, p0, Lmiui/resourcebrowser/activity/BaseTabActivity$ResourcePagerAdapter;->mFragmentManager:Landroid/app/FragmentManager;

    invoke-virtual {v0}, Landroid/app/FragmentManager;->executePendingTransactions()Z

    .line 198
    :cond_0
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lmiui/resourcebrowser/activity/BaseTabActivity$ResourcePagerAdapter;->this$0:Lmiui/resourcebrowser/activity/BaseTabActivity;

    iget-object v0, v0, Lmiui/resourcebrowser/activity/BaseTabActivity;->mTabFragments:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 1
    .parameter "object"

    .prologue
    .line 170
    iget-object v0, p0, Lmiui/resourcebrowser/activity/BaseTabActivity$ResourcePagerAdapter;->this$0:Lmiui/resourcebrowser/activity/BaseTabActivity;

    iget-object v0, v0, Lmiui/resourcebrowser/activity/BaseTabActivity;->mTabFragments:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 2
    .parameter "container"
    .parameter "position"

    .prologue
    .line 175
    iget-object v1, p0, Lmiui/resourcebrowser/activity/BaseTabActivity$ResourcePagerAdapter;->mCurTransaction:Landroid/app/FragmentTransaction;

    if-nez v1, :cond_0

    .line 176
    iget-object v1, p0, Lmiui/resourcebrowser/activity/BaseTabActivity$ResourcePagerAdapter;->mFragmentManager:Landroid/app/FragmentManager;

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    iput-object v1, p0, Lmiui/resourcebrowser/activity/BaseTabActivity$ResourcePagerAdapter;->mCurTransaction:Landroid/app/FragmentTransaction;

    .line 178
    :cond_0
    iget-object v1, p0, Lmiui/resourcebrowser/activity/BaseTabActivity$ResourcePagerAdapter;->this$0:Lmiui/resourcebrowser/activity/BaseTabActivity;

    iget-object v1, v1, Lmiui/resourcebrowser/activity/BaseTabActivity;->mTabFragments:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Fragment;

    .line 179
    .local v0, f:Landroid/app/Fragment;
    iget-object v1, p0, Lmiui/resourcebrowser/activity/BaseTabActivity$ResourcePagerAdapter;->mCurTransaction:Landroid/app/FragmentTransaction;

    invoke-virtual {v1, v0}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 180
    return-object v0
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .parameter "view"
    .parameter "object"

    .prologue
    .line 202
    check-cast p2, Landroid/app/Fragment;

    .end local p2
    invoke-virtual {p2}, Landroid/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
