.class public Lmiui/resourcebrowser/activity/ResourceRecommendListActivity;
.super Lmiui/app/ObservableActivity;
.source "ResourceRecommendListActivity.java"


# instance fields
.field protected mFragment:Lmiui/resourcebrowser/activity/BaseFragment;

.field protected mResContext:Lmiui/resourcebrowser/ResourceContext;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lmiui/app/ObservableActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected getFragment()Lmiui/resourcebrowser/activity/BaseFragment;
    .locals 1

    .prologue
    .line 58
    new-instance v0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;

    invoke-direct {v0}, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;-><init>()V

    return-object v0
.end method

.method protected getResourceController(Lmiui/resourcebrowser/ResourceContext;)Lmiui/resourcebrowser/controller/ResourceController;
    .locals 1
    .parameter "resContext"

    .prologue
    .line 54
    new-instance v0, Lmiui/resourcebrowser/controller/ResourceController;

    invoke-direct {v0, p1}, Lmiui/resourcebrowser/controller/ResourceController;-><init>(Lmiui/resourcebrowser/ResourceContext;)V

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .parameter "savedInstanceState"

    .prologue
    const/4 v6, 0x1

    .line 24
    invoke-super {p0, p1}, Lmiui/app/ObservableActivity;->onCreate(Landroid/os/Bundle;)V

    .line 25
    const v4, 0x6030019

    invoke-virtual {p0, v4}, Lmiui/resourcebrowser/activity/ResourceRecommendListActivity;->setContentView(I)V

    .line 27
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceRecommendListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "REQUEST_RES_CONTEXT"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v4

    check-cast v4, Lmiui/resourcebrowser/ResourceContext;

    iput-object v4, p0, Lmiui/resourcebrowser/activity/ResourceRecommendListActivity;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    .line 28
    invoke-static {}, Lmiui/resourcebrowser/AppInnerContext;->getInstance()Lmiui/resourcebrowser/AppInnerContext;

    move-result-object v0

    .line 29
    .local v0, appContext:Lmiui/resourcebrowser/AppInnerContext;
    iget-object v4, p0, Lmiui/resourcebrowser/activity/ResourceRecommendListActivity;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v0, v4}, Lmiui/resourcebrowser/AppInnerContext;->setResourceContext(Lmiui/resourcebrowser/ResourceContext;)V

    .line 30
    iget-object v4, p0, Lmiui/resourcebrowser/activity/ResourceRecommendListActivity;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {p0, v4}, Lmiui/resourcebrowser/activity/ResourceRecommendListActivity;->getResourceController(Lmiui/resourcebrowser/ResourceContext;)Lmiui/resourcebrowser/controller/ResourceController;

    move-result-object v1

    .line 31
    .local v1, controller:Lmiui/resourcebrowser/controller/ResourceController;
    invoke-virtual {v0}, Lmiui/resourcebrowser/AppInnerContext;->getResourceController()Lmiui/resourcebrowser/controller/ResourceController;

    move-result-object v4

    invoke-virtual {v4}, Lmiui/resourcebrowser/controller/ResourceController;->getLocalDataManager()Lmiui/resourcebrowser/controller/LocalDataManager;

    move-result-object v4

    invoke-virtual {v1, v4}, Lmiui/resourcebrowser/controller/ResourceController;->setLocalDataManager(Lmiui/resourcebrowser/controller/LocalDataManager;)V

    .line 32
    invoke-virtual {v0}, Lmiui/resourcebrowser/AppInnerContext;->getResourceController()Lmiui/resourcebrowser/controller/ResourceController;

    move-result-object v4

    invoke-virtual {v4}, Lmiui/resourcebrowser/controller/ResourceController;->getImportManager()Lmiui/resourcebrowser/controller/ImportManager;

    move-result-object v4

    invoke-virtual {v1, v4}, Lmiui/resourcebrowser/controller/ResourceController;->setImportManager(Lmiui/resourcebrowser/controller/ImportManager;)V

    .line 33
    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/AppInnerContext;->setResourceController(Lmiui/resourcebrowser/controller/ResourceController;)V

    .line 35
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceRecommendListActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 37
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceRecommendListActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    .line 38
    .local v2, fragmentManager:Landroid/app/FragmentManager;
    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v3

    .line 39
    .local v3, fragmentTransaction:Landroid/app/FragmentTransaction;
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceRecommendListActivity;->getFragment()Lmiui/resourcebrowser/activity/BaseFragment;

    move-result-object v4

    iput-object v4, p0, Lmiui/resourcebrowser/activity/ResourceRecommendListActivity;->mFragment:Lmiui/resourcebrowser/activity/BaseFragment;

    .line 40
    const v4, 0x60b009c

    iget-object v5, p0, Lmiui/resourcebrowser/activity/ResourceRecommendListActivity;->mFragment:Lmiui/resourcebrowser/activity/BaseFragment;

    invoke-virtual {v3, v4, v5}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 41
    invoke-virtual {v3}, Landroid/app/FragmentTransaction;->commit()I

    .line 42
    iget-object v4, p0, Lmiui/resourcebrowser/activity/ResourceRecommendListActivity;->mFragment:Lmiui/resourcebrowser/activity/BaseFragment;

    invoke-virtual {v4, v6}, Lmiui/resourcebrowser/activity/BaseFragment;->setVisibleForUser(Z)V

    .line 43
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .parameter "item"

    .prologue
    .line 47
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 48
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceRecommendListActivity;->finish()V

    .line 50
    :cond_0
    invoke-super {p0, p1}, Lmiui/app/ObservableActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method
