.class Lmiui/resourcebrowser/activity/LocalResourceAdapter$DownloadVersionTask;
.super Landroid/os/AsyncTask;
.source "LocalResourceAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/resourcebrowser/activity/LocalResourceAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DownloadVersionTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/lang/String;",
        ">;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Lmiui/resourcebrowser/model/Resource;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/resourcebrowser/activity/LocalResourceAdapter;


# direct methods
.method private constructor <init>(Lmiui/resourcebrowser/activity/LocalResourceAdapter;)V
    .locals 0
    .parameter

    .prologue
    .line 87
    iput-object p1, p0, Lmiui/resourcebrowser/activity/LocalResourceAdapter$DownloadVersionTask;->this$0:Lmiui/resourcebrowser/activity/LocalResourceAdapter;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lmiui/resourcebrowser/activity/LocalResourceAdapter;Lmiui/resourcebrowser/activity/LocalResourceAdapter$1;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lmiui/resourcebrowser/activity/LocalResourceAdapter$DownloadVersionTask;-><init>(Lmiui/resourcebrowser/activity/LocalResourceAdapter;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"

    .prologue
    .line 87
    check-cast p1, [Ljava/util/List;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/activity/LocalResourceAdapter$DownloadVersionTask;->doInBackground([Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/util/List;)Ljava/util/List;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 91
    .local p1, params:[Ljava/util/List;,"[Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lmiui/resourcebrowser/activity/LocalResourceAdapter$DownloadVersionTask;->this$0:Lmiui/resourcebrowser/activity/LocalResourceAdapter;

    iget-object v0, v0, Lmiui/resourcebrowser/activity/ResourceAdapter;->mResController:Lmiui/resourcebrowser/controller/ResourceController;

    invoke-virtual {v0}, Lmiui/resourcebrowser/controller/ResourceController;->getOnlineDataManager()Lmiui/resourcebrowser/controller/OnlineDataManager;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/controller/OnlineDataManager;->getUpdatableResources(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 87
    check-cast p1, Ljava/util/List;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/activity/LocalResourceAdapter$DownloadVersionTask;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 96
    .local p1, updatableResources:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/Resource;>;"
    if-eqz p1, :cond_0

    .line 97
    iget-object v0, p0, Lmiui/resourcebrowser/activity/LocalResourceAdapter$DownloadVersionTask;->this$0:Lmiui/resourcebrowser/activity/LocalResourceAdapter;

    #calls: Lmiui/resourcebrowser/activity/LocalResourceAdapter;->setVersionStatus(Ljava/util/List;)V
    invoke-static {v0, p1}, Lmiui/resourcebrowser/activity/LocalResourceAdapter;->access$100(Lmiui/resourcebrowser/activity/LocalResourceAdapter;Ljava/util/List;)V

    .line 99
    :cond_0
    return-void
.end method
