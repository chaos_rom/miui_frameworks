.class public Lmiui/resourcebrowser/activity/ResourceSearchAdapter$AsyncLoadMoreResourceTask;
.super Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreDataTask;
.source "ResourceSearchAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/resourcebrowser/activity/ResourceSearchAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "AsyncLoadMoreResourceTask"
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/resourcebrowser/activity/ResourceSearchAdapter;


# direct methods
.method protected constructor <init>(Lmiui/resourcebrowser/activity/ResourceSearchAdapter;)V
    .locals 0
    .parameter

    .prologue
    .line 107
    iput-object p1, p0, Lmiui/resourcebrowser/activity/ResourceSearchAdapter$AsyncLoadMoreResourceTask;->this$0:Lmiui/resourcebrowser/activity/ResourceSearchAdapter;

    invoke-direct {p0, p1}, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreDataTask;-><init>(Lmiui/resourcebrowser/widget/AsyncAdapter;)V

    return-void
.end method


# virtual methods
.method protected loadMoreData(Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreParams;)Ljava/util/List;
    .locals 3
    .parameter "params"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreParams;",
            ")",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 112
    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceSearchAdapter$AsyncLoadMoreResourceTask;->this$0:Lmiui/resourcebrowser/activity/ResourceSearchAdapter;

    iget-object v1, v1, Lmiui/resourcebrowser/activity/ResourceSearchAdapter;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v1}, Lmiui/resourcebrowser/ResourceContext;->getPageItemCount()I

    move-result v0

    .line 113
    .local v0, count:I
    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceSearchAdapter$AsyncLoadMoreResourceTask;->this$0:Lmiui/resourcebrowser/activity/ResourceSearchAdapter;

    iget-object v1, v1, Lmiui/resourcebrowser/activity/ResourceSearchAdapter;->mResController:Lmiui/resourcebrowser/controller/ResourceController;

    invoke-virtual {v1}, Lmiui/resourcebrowser/controller/ResourceController;->getOnlineDataManager()Lmiui/resourcebrowser/controller/OnlineDataManager;

    move-result-object v1

    iget v2, p1, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreParams;->cursor:I

    add-int/2addr v2, v0

    add-int/lit8 v2, v2, -0x1

    div-int/2addr v2, v0

    invoke-virtual {v1, v2}, Lmiui/resourcebrowser/controller/OnlineDataManager;->getResources(I)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method
