.class public Lmiui/resourcebrowser/activity/ResourceSearchListFragment;
.super Lmiui/resourcebrowser/activity/ResourceListFragment;
.source "ResourceSearchListFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lmiui/resourcebrowser/activity/ResourceListFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected getAdapter()Lmiui/resourcebrowser/activity/ResourceAdapter;
    .locals 2

    .prologue
    .line 21
    new-instance v0, Lmiui/resourcebrowser/activity/ResourceSearchAdapter;

    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceSearchListFragment;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-direct {v0, p0, v1}, Lmiui/resourcebrowser/activity/ResourceSearchAdapter;-><init>(Lmiui/resourcebrowser/activity/BaseFragment;Lmiui/resourcebrowser/ResourceContext;)V

    return-object v0
.end method

.method protected getContentView()I
    .locals 1

    .prologue
    .line 16
    const v0, 0x6030018

    return v0
.end method

.method protected getSourceType()I
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x3

    return v0
.end method

.method protected getSourceType(I)I
    .locals 1
    .parameter "group"

    .prologue
    .line 31
    packed-switch p1, :pswitch_data_0

    .line 37
    invoke-super {p0, p1}, Lmiui/resourcebrowser/activity/ResourceListFragment;->getSourceType(I)I

    move-result v0

    :goto_0
    return v0

    .line 33
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 35
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 31
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected initParams()V
    .locals 3

    .prologue
    .line 43
    invoke-super {p0}, Lmiui/resourcebrowser/activity/ResourceListFragment;->initParams()V

    .line 44
    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceSearchListFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "query"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 45
    .local v0, keyword:Ljava/lang/String;
    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceSearchListFragment;->mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;

    check-cast v1, Lmiui/resourcebrowser/activity/ResourceSearchAdapter;

    invoke-virtual {v1, v0}, Lmiui/resourcebrowser/activity/ResourceSearchAdapter;->setKeyword(Ljava/lang/String;)V

    .line 46
    return-void
.end method

.method protected initializeDataSet()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 50
    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceSearchListFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "confirm_miui_disclaimer"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eq v1, v4, :cond_0

    .line 52
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MIUI_DISCLAIMER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 53
    .local v0, i:Landroid/content/Intent;
    const/16 v1, 0x3e8

    invoke-virtual {p0, v0, v1}, Lmiui/resourcebrowser/activity/ResourceSearchListFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 55
    .end local v0           #i:Landroid/content/Intent;
    :cond_0
    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceSearchListFragment;->mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;

    invoke-virtual {v1, v3}, Lmiui/resourcebrowser/activity/ResourceAdapter;->loadData(I)V

    .line 56
    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceSearchListFragment;->mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;

    invoke-virtual {v1, v3, v4}, Lmiui/resourcebrowser/activity/ResourceAdapter;->loadMoreData(ZI)V

    .line 57
    return-void
.end method
