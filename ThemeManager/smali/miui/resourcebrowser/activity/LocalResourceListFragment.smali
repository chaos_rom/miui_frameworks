.class public Lmiui/resourcebrowser/activity/LocalResourceListFragment;
.super Lmiui/resourcebrowser/activity/ResourceListFragment;
.source "LocalResourceListFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lmiui/resourcebrowser/activity/ResourceListFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected getAdapter()Lmiui/resourcebrowser/activity/ResourceAdapter;
    .locals 2

    .prologue
    .line 16
    new-instance v0, Lmiui/resourcebrowser/activity/LocalResourceAdapter;

    iget-object v1, p0, Lmiui/resourcebrowser/activity/LocalResourceListFragment;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-direct {v0, p0, v1}, Lmiui/resourcebrowser/activity/LocalResourceAdapter;-><init>(Lmiui/resourcebrowser/activity/BaseFragment;Lmiui/resourcebrowser/ResourceContext;)V

    return-object v0
.end method

.method protected getContentView()I
    .locals 1

    .prologue
    .line 11
    const v0, 0x6030018

    return v0
.end method

.method protected getSourceType()I
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x1

    return v0
.end method

.method protected initParams()V
    .locals 1

    .prologue
    .line 26
    invoke-super {p0}, Lmiui/resourcebrowser/activity/ResourceListFragment;->initParams()V

    .line 27
    iget-object v0, p0, Lmiui/resourcebrowser/activity/LocalResourceListFragment;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v0}, Lmiui/resourcebrowser/ResourceContext;->getBaseImageCacheFolder()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lmiui/os/ExtraFileUtils;->addNoMedia(Ljava/lang/String;)V

    .line 28
    return-void
.end method

.method protected initializeDataSet()V
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lmiui/resourcebrowser/activity/LocalResourceListFragment;->mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;

    invoke-virtual {v0}, Lmiui/resourcebrowser/activity/ResourceAdapter;->loadData()V

    .line 33
    return-void
.end method

.method protected refreshDataSet()V
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lmiui/resourcebrowser/activity/LocalResourceListFragment;->mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;

    invoke-virtual {v0}, Lmiui/resourcebrowser/activity/ResourceAdapter;->loadData()V

    .line 38
    return-void
.end method
