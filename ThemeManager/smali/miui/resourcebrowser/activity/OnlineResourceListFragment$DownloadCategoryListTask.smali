.class public Lmiui/resourcebrowser/activity/OnlineResourceListFragment$DownloadCategoryListTask;
.super Landroid/os/AsyncTask;
.source "OnlineResourceListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/resourcebrowser/activity/OnlineResourceListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "DownloadCategoryListTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Lmiui/resourcebrowser/model/ResourceCategory;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/resourcebrowser/activity/OnlineResourceListFragment;


# direct methods
.method protected constructor <init>(Lmiui/resourcebrowser/activity/OnlineResourceListFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 232
    iput-object p1, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment$DownloadCategoryListTask;->this$0:Lmiui/resourcebrowser/activity/OnlineResourceListFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"

    .prologue
    .line 232
    check-cast p1, [Ljava/lang/Void;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/activity/OnlineResourceListFragment$DownloadCategoryListTask;->doInBackground([Ljava/lang/Void;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/util/List;
    .locals 1
    .parameter "params"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/ResourceCategory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 236
    iget-object v0, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment$DownloadCategoryListTask;->this$0:Lmiui/resourcebrowser/activity/OnlineResourceListFragment;

    iget-object v0, v0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mResController:Lmiui/resourcebrowser/controller/ResourceController;

    invoke-virtual {v0}, Lmiui/resourcebrowser/controller/ResourceController;->getOnlineDataManager()Lmiui/resourcebrowser/controller/OnlineDataManager;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/resourcebrowser/controller/OnlineDataManager;->getResourceCategories()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 232
    check-cast p1, Ljava/util/List;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/activity/OnlineResourceListFragment$DownloadCategoryListTask;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/ResourceCategory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 241
    .local p1, result:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/ResourceCategory;>;"
    iget-object v0, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment$DownloadCategoryListTask;->this$0:Lmiui/resourcebrowser/activity/OnlineResourceListFragment;

    invoke-virtual {v0, p1}, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->setCategories(Ljava/util/List;)V

    .line 242
    return-void
.end method
