.class public Lmiui/resourcebrowser/activity/LocalResourceAdapter$AsyncLoadResourceTask;
.super Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;
.source "LocalResourceAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/resourcebrowser/activity/LocalResourceAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "AsyncLoadResourceTask"
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/resourcebrowser/activity/LocalResourceAdapter;


# direct methods
.method protected constructor <init>(Lmiui/resourcebrowser/activity/LocalResourceAdapter;)V
    .locals 0
    .parameter

    .prologue
    .line 49
    iput-object p1, p0, Lmiui/resourcebrowser/activity/LocalResourceAdapter$AsyncLoadResourceTask;->this$0:Lmiui/resourcebrowser/activity/LocalResourceAdapter;

    invoke-direct {p0, p1}, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;-><init>(Lmiui/resourcebrowser/widget/AsyncAdapter;)V

    return-void
.end method


# virtual methods
.method protected getMode()I
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    return v0
.end method

.method protected bridge synthetic loadData()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 49
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/LocalResourceAdapter$AsyncLoadResourceTask;->loadData()[Lmiui/resourcebrowser/model/Resource;

    move-result-object v0

    return-object v0
.end method

.method protected loadData()[Lmiui/resourcebrowser/model/Resource;
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lmiui/resourcebrowser/activity/LocalResourceAdapter$AsyncLoadResourceTask;->this$0:Lmiui/resourcebrowser/activity/LocalResourceAdapter;

    iget-object v0, v0, Lmiui/resourcebrowser/activity/LocalResourceAdapter;->mResController:Lmiui/resourcebrowser/controller/ResourceController;

    invoke-virtual {v0}, Lmiui/resourcebrowser/controller/ResourceController;->getLocalDataManager()Lmiui/resourcebrowser/controller/LocalDataManager;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/resourcebrowser/controller/LocalDataManager;->getResources()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Lmiui/resourcebrowser/model/Resource;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmiui/resourcebrowser/model/Resource;

    return-object v0
.end method
