.class public abstract Lmiui/resourcebrowser/activity/ResourceListFragment;
.super Lmiui/resourcebrowser/activity/BaseFragment;
.source "ResourceListFragment.java"

# interfaces
.implements Lmiui/app/SDCardMonitor$SDCardStatusListener;
.implements Lmiui/os/AsyncTaskObserver;
.implements Lmiui/resourcebrowser/ResourceConstants;
.implements Lmiui/resourcebrowser/controller/DataSetObserver;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmiui/resourcebrowser/activity/BaseFragment;",
        "Lmiui/os/AsyncTaskObserver",
        "<",
        "Ljava/lang/Void;",
        "Lmiui/resourcebrowser/model/Resource;",
        "Ljava/util/List",
        "<",
        "Lmiui/resourcebrowser/model/Resource;",
        ">;>;",
        "Lmiui/resourcebrowser/controller/DataSetObserver;",
        "Lmiui/app/SDCardMonitor$SDCardStatusListener;",
        "Lmiui/resourcebrowser/ResourceConstants;"
    }
.end annotation


# static fields
.field private static mIsNeedCheckImportResources:Z


# instance fields
.field protected mActivity:Landroid/app/Activity;

.field protected mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;

.field protected mAppContext:Lmiui/resourcebrowser/AppInnerContext;

.field protected mBatchHandler:Lmiui/resourcebrowser/util/BatchResourceHandler;

.field private mFirstTimeResume:Z

.field private mFirstTimeVisible:Z

.field protected mHandler:Landroid/os/Handler;

.field protected mListView:Landroid/widget/ListView;

.field protected mProgressBar:Landroid/view/View;

.field protected mProgressDialog:Landroid/app/ProgressDialog;

.field protected mResContext:Lmiui/resourcebrowser/ResourceContext;

.field protected mResController:Lmiui/resourcebrowser/controller/ResourceController;

.field protected mSDCardMonitor:Lmiui/app/SDCardMonitor;

.field protected mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    sput-boolean v0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mIsNeedCheckImportResources:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 42
    invoke-direct {p0}, Lmiui/resourcebrowser/activity/BaseFragment;-><init>()V

    .line 62
    iput-boolean v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mFirstTimeVisible:Z

    .line 63
    iput-boolean v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mFirstTimeResume:Z

    return-void
.end method

.method static synthetic access$000(Lmiui/resourcebrowser/activity/ResourceListFragment;Ljava/io/File;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lmiui/resourcebrowser/activity/ResourceListFragment;->createIsImportedFile(Ljava/io/File;)V

    return-void
.end method

.method static synthetic access$100(Lmiui/resourcebrowser/activity/ResourceListFragment;II)I
    .locals 1
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Lmiui/resourcebrowser/activity/ResourceListFragment;->computeProgressPercentage(II)I

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lmiui/resourcebrowser/activity/ResourceListFragment;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 42
    invoke-direct {p0}, Lmiui/resourcebrowser/activity/ResourceListFragment;->tryImportResources()V

    return-void
.end method

.method static synthetic access$300(Lmiui/resourcebrowser/activity/ResourceListFragment;Ljava/io/File;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lmiui/resourcebrowser/activity/ResourceListFragment;->startImportResourceTask(Ljava/io/File;)V

    return-void
.end method

.method static synthetic access$402(Z)Z
    .locals 0
    .parameter "x0"

    .prologue
    .line 42
    sput-boolean p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mIsNeedCheckImportResources:Z

    return p0
.end method

.method private computeProgressPercentage(II)I
    .locals 1
    .parameter "processCursor"
    .parameter "processMax"

    .prologue
    .line 363
    if-nez p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    mul-int/lit8 v0, p1, 0x64

    div-int/2addr v0, p2

    goto :goto_0
.end method

.method private createIsImportedFile(Ljava/io/File;)V
    .locals 2
    .parameter "isResImportedFile"

    .prologue
    .line 367
    invoke-virtual {p1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 369
    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 373
    :goto_0
    return-void

    .line 370
    :catch_0
    move-exception v0

    .line 371
    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private startImportResourceTask(Ljava/io/File;)V
    .locals 4
    .parameter "isResImportedFile"

    .prologue
    .line 107
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lmiui/resourcebrowser/activity/ResourceListFragment$1;

    invoke-direct {v1, p0, p1}, Lmiui/resourcebrowser/activity/ResourceListFragment$1;-><init>(Lmiui/resourcebrowser/activity/ResourceListFragment;Ljava/io/File;)V

    const-wide/16 v2, 0x320

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 172
    return-void
.end method

.method private tryImportResources()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 376
    sget-boolean v5, Lmiui/resourcebrowser/activity/ResourceListFragment;->mIsNeedCheckImportResources:Z

    if-eqz v5, :cond_1

    .line 448
    :cond_0
    :goto_0
    return-void

    .line 379
    :cond_1
    sput-boolean v9, Lmiui/resourcebrowser/activity/ResourceListFragment;->mIsNeedCheckImportResources:Z

    .line 381
    iget-object v5, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v5}, Lmiui/resourcebrowser/ResourceContext;->isSelfDescribing()Z

    move-result v5

    if-nez v5, :cond_0

    .line 385
    iget-object v5, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v5}, Lmiui/resourcebrowser/ResourceContext;->getAsyncImportFolder()Ljava/lang/String;

    move-result-object v2

    .line 386
    .local v2, asyncImportFolderPath:Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".import"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 387
    .local v3, isResImportedFile:Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 388
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 389
    .local v1, asyncImportFolder:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v5

    array-length v5, v5

    if-le v5, v9, :cond_0

    .line 390
    new-instance v5, Lmiui/resourcebrowser/activity/ResourceListFragment$4;

    iget-object v6, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "recovery-"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v8}, Lmiui/resourcebrowser/ResourceContext;->getResourceCode()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, p0, v6, v7}, Lmiui/resourcebrowser/activity/ResourceListFragment$4;-><init>(Lmiui/resourcebrowser/activity/ResourceListFragment;Lmiui/resourcebrowser/ResourceContext;Ljava/lang/String;)V

    new-array v6, v9, [Ljava/lang/String;

    sget-object v7, Lmiui/resourcebrowser/controller/local/ImportResourceTask;->IMPORT_BY_IMPORT_FOLDER:Ljava/lang/String;

    aput-object v7, v6, v10

    invoke-virtual {v5, v6}, Lmiui/resourcebrowser/activity/ResourceListFragment$4;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 403
    .end local v1           #asyncImportFolder:Ljava/io/File;
    :cond_2
    new-instance v4, Ljava/io/File;

    iget-object v5, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v5}, Lmiui/resourcebrowser/ResourceContext;->getDownloadFolder()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 404
    .local v4, resourceFolder:Ljava/io/File;
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {v4}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {v4}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v5

    array-length v5, v5

    if-nez v5, :cond_4

    .line 406
    :cond_3
    invoke-direct {p0, v3}, Lmiui/resourcebrowser/activity/ResourceListFragment;->createIsImportedFile(Ljava/io/File;)V

    goto/16 :goto_0

    .line 410
    :cond_4
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v5, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mActivity:Landroid/app/Activity;

    invoke-direct {v0, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 411
    .local v0, alertDialogBuilder:Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v10}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 412
    iget-object v5, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mActivity:Landroid/app/Activity;

    const v6, 0x60c023a

    invoke-virtual {v5, v6}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 413
    invoke-virtual {v3}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_5

    .line 414
    iget-object v5, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mActivity:Landroid/app/Activity;

    const v6, 0x60c023b

    invoke-virtual {v5, v6}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 419
    :goto_1
    iget-object v5, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mActivity:Landroid/app/Activity;

    const v6, 0x1040013

    invoke-virtual {v5, v6}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lmiui/resourcebrowser/activity/ResourceListFragment$5;

    invoke-direct {v6, p0, v3}, Lmiui/resourcebrowser/activity/ResourceListFragment$5;-><init>(Lmiui/resourcebrowser/activity/ResourceListFragment;Ljava/io/File;)V

    invoke-virtual {v0, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 438
    iget-object v5, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mActivity:Landroid/app/Activity;

    const v6, 0x1040009

    invoke-virtual {v5, v6}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lmiui/resourcebrowser/activity/ResourceListFragment$6;

    invoke-direct {v6, p0}, Lmiui/resourcebrowser/activity/ResourceListFragment$6;-><init>(Lmiui/resourcebrowser/activity/ResourceListFragment;)V

    invoke-virtual {v0, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 447
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_0

    .line 417
    :cond_5
    iget-object v5, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mActivity:Landroid/app/Activity;

    const v6, 0x60c023c

    invoke-virtual {v5, v6}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_1
.end method


# virtual methods
.method protected abstract getAdapter()Lmiui/resourcebrowser/activity/ResourceAdapter;
.end method

.method protected getBatchOperationHandler()Lmiui/resourcebrowser/util/BatchResourceHandler;
    .locals 3

    .prologue
    .line 266
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v0}, Lmiui/resourcebrowser/ResourceContext;->getResourceFormat()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 267
    new-instance v0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;

    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;

    iget-object v2, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-direct {v0, p0, v1, v2}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;-><init>(Lmiui/resourcebrowser/activity/ResourceListFragment;Lmiui/resourcebrowser/activity/ResourceAdapter;Lmiui/resourcebrowser/ResourceContext;)V

    .line 269
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lmiui/resourcebrowser/util/BatchResourceHandler;

    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;

    iget-object v2, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-direct {v0, p0, v1, v2}, Lmiui/resourcebrowser/util/BatchResourceHandler;-><init>(Lmiui/resourcebrowser/activity/ResourceListFragment;Lmiui/resourcebrowser/activity/ResourceAdapter;Lmiui/resourcebrowser/ResourceContext;)V

    goto :goto_0
.end method

.method protected abstract getContentView()I
.end method

.method protected getHeaderView()Landroid/view/View;
    .locals 1

    .prologue
    .line 273
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getResourceContext()Lmiui/resourcebrowser/ResourceContext;
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mAppContext:Lmiui/resourcebrowser/AppInnerContext;

    invoke-virtual {v0}, Lmiui/resourcebrowser/AppInnerContext;->getResourceContext()Lmiui/resourcebrowser/ResourceContext;

    move-result-object v0

    return-object v0
.end method

.method protected getResourceController()Lmiui/resourcebrowser/controller/ResourceController;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mAppContext:Lmiui/resourcebrowser/AppInnerContext;

    invoke-virtual {v0}, Lmiui/resourcebrowser/AppInnerContext;->getResourceController()Lmiui/resourcebrowser/controller/ResourceController;

    move-result-object v0

    return-object v0
.end method

.method protected getResourceDetailActivity()Landroid/util/Pair;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 288
    new-instance v0, Landroid/util/Pair;

    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v1}, Lmiui/resourcebrowser/ResourceContext;->getDetailActivityPackage()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v2}, Lmiui/resourcebrowser/ResourceContext;->getDetailActivityClass()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method protected abstract getSourceType()I
.end method

.method protected getSourceType(I)I
    .locals 1
    .parameter "group"

    .prologue
    .line 298
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceListFragment;->getSourceType()I

    move-result v0

    return v0
.end method

.method protected initParams()V
    .locals 2

    .prologue
    .line 238
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceListFragment;->getAdapter()Lmiui/resourcebrowser/activity/ResourceAdapter;

    move-result-object v0

    iput-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;

    .line 239
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;

    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mResController:Lmiui/resourcebrowser/controller/ResourceController;

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/activity/ResourceAdapter;->setResourceController(Lmiui/resourcebrowser/controller/ResourceController;)V

    .line 241
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceListFragment;->getBatchOperationHandler()Lmiui/resourcebrowser/util/BatchResourceHandler;

    move-result-object v0

    iput-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mBatchHandler:Lmiui/resourcebrowser/util/BatchResourceHandler;

    .line 242
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mBatchHandler:Lmiui/resourcebrowser/util/BatchResourceHandler;

    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mResController:Lmiui/resourcebrowser/controller/ResourceController;

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/util/BatchResourceHandler;->setResourceController(Lmiui/resourcebrowser/controller/ResourceController;)V

    .line 243
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mBatchHandler:Lmiui/resourcebrowser/util/BatchResourceHandler;

    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceListFragment;->getSourceType()I

    move-result v1

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/util/BatchResourceHandler;->setSourceType(I)V

    .line 244
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mBatchHandler:Lmiui/resourcebrowser/util/BatchResourceHandler;

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/activity/ResourceListFragment;->addObserver(Lmiui/app/FragmentLifecycleObserver;)V

    .line 245
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;

    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mBatchHandler:Lmiui/resourcebrowser/util/BatchResourceHandler;

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/activity/ResourceAdapter;->setResourceBatchHandler(Lmiui/resourcebrowser/util/BatchResourceHandler;)V

    .line 247
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mHandler:Landroid/os/Handler;

    .line 249
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lmiui/app/SDCardMonitor;->getSDCardMonitor(Landroid/content/Context;)Lmiui/app/SDCardMonitor;

    move-result-object v0

    iput-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mSDCardMonitor:Lmiui/app/SDCardMonitor;

    .line 250
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mSDCardMonitor:Lmiui/app/SDCardMonitor;

    invoke-virtual {v0, p0}, Lmiui/app/SDCardMonitor;->addListener(Lmiui/app/SDCardMonitor$SDCardStatusListener;)V

    .line 251
    return-void
.end method

.method protected initializeDataSet()V
    .locals 0

    .prologue
    .line 179
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 1
    .parameter "savedInstanceState"

    .prologue
    .line 69
    invoke-super {p0, p1}, Lmiui/resourcebrowser/activity/BaseFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 70
    const/4 v0, 0x0

    sput-boolean v0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mIsNeedCheckImportResources:Z

    .line 71
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mActivity:Landroid/app/Activity;

    .line 72
    invoke-static {}, Lmiui/resourcebrowser/AppInnerContext;->getInstance()Lmiui/resourcebrowser/AppInnerContext;

    move-result-object v0

    iput-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mAppContext:Lmiui/resourcebrowser/AppInnerContext;

    .line 73
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceListFragment;->getResourceContext()Lmiui/resourcebrowser/ResourceContext;

    move-result-object v0

    iput-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    .line 74
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceListFragment;->getResourceController()Lmiui/resourcebrowser/controller/ResourceController;

    move-result-object v0

    iput-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mResController:Lmiui/resourcebrowser/controller/ResourceController;

    .line 75
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mResController:Lmiui/resourcebrowser/controller/ResourceController;

    invoke-virtual {v0}, Lmiui/resourcebrowser/controller/ResourceController;->getLocalDataManager()Lmiui/resourcebrowser/controller/LocalDataManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lmiui/resourcebrowser/controller/LocalDataManager;->addObserver(Lmiui/resourcebrowser/controller/DataSetObserver;)V

    .line 76
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mResController:Lmiui/resourcebrowser/controller/ResourceController;

    invoke-virtual {v0}, Lmiui/resourcebrowser/controller/ResourceController;->getOnlineDataManager()Lmiui/resourcebrowser/controller/OnlineDataManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lmiui/resourcebrowser/controller/OnlineDataManager;->addObserver(Lmiui/resourcebrowser/controller/DataSetObserver;)V

    .line 77
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceListFragment;->initParams()V

    .line 78
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceListFragment;->setupUI()V

    .line 79
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 223
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v0}, Lmiui/resourcebrowser/ResourceContext;->isPicker()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p3, :cond_0

    .line 224
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0, p2, p3}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 225
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 227
    :cond_0
    return-void
.end method

.method public onCancelled()V
    .locals 2

    .prologue
    .line 354
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mProgressBar:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 355
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    .line 83
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceListFragment;->getContentView()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDataSetUpdateFailed()V
    .locals 0

    .prologue
    .line 336
    return-void
.end method

.method public onDataSetUpdateSuccessful()V
    .locals 2

    .prologue
    .line 325
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mHandler:Landroid/os/Handler;

    new-instance v1, Lmiui/resourcebrowser/activity/ResourceListFragment$3;

    invoke-direct {v1, p0}, Lmiui/resourcebrowser/activity/ResourceListFragment$3;-><init>(Lmiui/resourcebrowser/activity/ResourceListFragment;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 332
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mSDCardMonitor:Lmiui/app/SDCardMonitor;

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mSDCardMonitor:Lmiui/app/SDCardMonitor;

    invoke-virtual {v0, p0}, Lmiui/app/SDCardMonitor;->removeListener(Lmiui/app/SDCardMonitor$SDCardStatusListener;)V

    .line 215
    :cond_0
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mResController:Lmiui/resourcebrowser/controller/ResourceController;

    invoke-virtual {v0}, Lmiui/resourcebrowser/controller/ResourceController;->getLocalDataManager()Lmiui/resourcebrowser/controller/LocalDataManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lmiui/resourcebrowser/controller/LocalDataManager;->removeObserver(Lmiui/resourcebrowser/controller/DataSetObserver;)V

    .line 216
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mResController:Lmiui/resourcebrowser/controller/ResourceController;

    invoke-virtual {v0}, Lmiui/resourcebrowser/controller/ResourceController;->getOnlineDataManager()Lmiui/resourcebrowser/controller/OnlineDataManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lmiui/resourcebrowser/controller/OnlineDataManager;->removeObserver(Lmiui/resourcebrowser/controller/DataSetObserver;)V

    .line 218
    invoke-super {p0}, Lmiui/resourcebrowser/activity/BaseFragment;->onDestroy()V

    .line 219
    return-void
.end method

.method protected onInvisible()V
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;

    invoke-virtual {v0}, Lmiui/resourcebrowser/activity/ResourceAdapter;->clean()V

    .line 176
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;

    invoke-virtual {v0}, Lmiui/resourcebrowser/activity/ResourceAdapter;->clean()V

    .line 207
    invoke-super {p0}, Lmiui/resourcebrowser/activity/BaseFragment;->onPause()V

    .line 208
    return-void
.end method

.method public bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 42
    check-cast p1, Ljava/util/List;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/activity/ResourceListFragment;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method public onPostExecute(Ljava/util/List;)V
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 345
    .local p1, result:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/Resource;>;"
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mProgressBar:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 346
    return-void
.end method

.method public onPreExecute()V
    .locals 2

    .prologue
    .line 340
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mProgressBar:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 341
    return-void
.end method

.method public bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 42
    check-cast p1, [Lmiui/resourcebrowser/model/Resource;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/activity/ResourceListFragment;->onProgressUpdate([Lmiui/resourcebrowser/model/Resource;)V

    return-void
.end method

.method public varargs onProgressUpdate([Lmiui/resourcebrowser/model/Resource;)V
    .locals 0
    .parameter "values"

    .prologue
    .line 350
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 193
    invoke-super {p0}, Lmiui/resourcebrowser/activity/BaseFragment;->onResume()V

    .line 194
    iget-boolean v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mFirstTimeResume:Z

    if-eqz v0, :cond_2

    .line 195
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceListFragment;->isVisibleForUser()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 196
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceListFragment;->onVisibleChanged()V

    .line 198
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mFirstTimeResume:Z

    .line 202
    :cond_1
    :goto_0
    return-void

    .line 199
    :cond_2
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceListFragment;->isVisibleForUser()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 200
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceListFragment;->restoreParams()V

    goto :goto_0
.end method

.method public onStatusChanged(Z)V
    .locals 1
    .parameter "mount"

    .prologue
    .line 359
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lmiui/resourcebrowser/util/ResourceHelper;->exit(Landroid/app/Activity;)V

    .line 360
    return-void
.end method

.method protected onVisible()V
    .locals 1

    .prologue
    .line 97
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceListFragment;->restoreParams()V

    .line 98
    iget-boolean v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mFirstTimeVisible:Z

    if-eqz v0, :cond_0

    .line 99
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceListFragment;->initializeDataSet()V

    .line 100
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mFirstTimeVisible:Z

    .line 104
    :goto_0
    return-void

    .line 102
    :cond_0
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceListFragment;->refreshDataSet()V

    goto :goto_0
.end method

.method public final onVisibleChanged()V
    .locals 1

    .prologue
    .line 88
    invoke-super {p0}, Lmiui/resourcebrowser/activity/BaseFragment;->onVisibleChanged()V

    .line 89
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceListFragment;->isVisibleForUser()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceListFragment;->onVisible()V

    .line 94
    :goto_0
    return-void

    .line 92
    :cond_0
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceListFragment;->onInvisible()V

    goto :goto_0
.end method

.method protected refreshDataSet()V
    .locals 0

    .prologue
    .line 182
    return-void
.end method

.method protected restoreParams()V
    .locals 2

    .prologue
    .line 185
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mAppContext:Lmiui/resourcebrowser/AppInnerContext;

    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/AppInnerContext;->setResourceContext(Lmiui/resourcebrowser/ResourceContext;)V

    .line 186
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mAppContext:Lmiui/resourcebrowser/AppInnerContext;

    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mResController:Lmiui/resourcebrowser/controller/ResourceController;

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/AppInnerContext;->setResourceController(Lmiui/resourcebrowser/controller/ResourceController;)V

    .line 187
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mAppContext:Lmiui/resourcebrowser/AppInnerContext;

    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;

    invoke-virtual {v1}, Lmiui/resourcebrowser/activity/ResourceAdapter;->getDataSet()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/AppInnerContext;->setWorkingDataSet(Ljava/util/List;)V

    .line 188
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;

    invoke-virtual {v0}, Lmiui/resourcebrowser/activity/ResourceAdapter;->notifyDataSetChanged()V

    .line 189
    return-void
.end method

.method protected setupUI()V
    .locals 3

    .prologue
    .line 254
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceListFragment;->getView()Landroid/view/View;

    move-result-object v1

    const v2, 0x60b004e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mListView:Landroid/widget/ListView;

    .line 255
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceListFragment;->getHeaderView()Landroid/view/View;

    move-result-object v0

    .line 256
    .local v0, header:Landroid/view/View;
    if-eqz v0, :cond_0

    .line 257
    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 259
    :cond_0
    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mListView:Landroid/widget/ListView;

    iget-object v2, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 260
    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mListView:Landroid/widget/ListView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    .line 261
    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mListView:Landroid/widget/ListView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 262
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceListFragment;->getView()Landroid/view/View;

    move-result-object v1

    const v2, 0x60b004f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mProgressBar:Landroid/view/View;

    .line 263
    return-void
.end method

.method public startDetailActivityForResource(Landroid/util/Pair;)V
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 277
    .local p1, position:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 278
    .local v1, intent:Landroid/content/Intent;
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceListFragment;->getResourceDetailActivity()Landroid/util/Pair;

    move-result-object v0

    .line 279
    .local v0, activityClass:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v2, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    iget-object v3, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 280
    const/high16 v2, 0x400

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 281
    const-string v3, "REQUEST_RES_INDEX"

    iget-object v2, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/io/Serializable;

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 282
    const-string v3, "REQUEST_RES_GROUP"

    iget-object v2, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/io/Serializable;

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 283
    const-string v3, "REQUEST_SOURCE_TYPE"

    iget-object v2, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v2}, Lmiui/resourcebrowser/activity/ResourceListFragment;->getSourceType(I)I

    move-result v2

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 284
    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, Lmiui/resourcebrowser/activity/ResourceListFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 285
    return-void
.end method
