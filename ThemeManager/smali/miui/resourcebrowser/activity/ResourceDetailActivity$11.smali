.class Lmiui/resourcebrowser/activity/ResourceDetailActivity$11;
.super Ljava/lang/Object;
.source "ResourceDetailActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiui/resourcebrowser/activity/ResourceDetailActivity;->bindScreenRingtoneView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;


# direct methods
.method constructor <init>(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 967
    iput-object p1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$11;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .parameter "v"

    .prologue
    const v1, 0x60200d2

    .line 970
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$11;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    #getter for: Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mBatchPlayer:Lmiui/resourcebrowser/util/BatchMediaPlayer;
    invoke-static {v0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->access$1600(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)Lmiui/resourcebrowser/util/BatchMediaPlayer;

    move-result-object v0

    if-nez v0, :cond_0

    .line 971
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$11;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    #getter for: Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPlayButton:Landroid/widget/ImageView;
    invoke-static {v0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->access$1500(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 972
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$11;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    #calls: Lmiui/resourcebrowser/activity/ResourceDetailActivity;->initPlayer()V
    invoke-static {v0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->access$1700(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)V

    .line 973
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$11;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    #getter for: Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mBatchPlayer:Lmiui/resourcebrowser/util/BatchMediaPlayer;
    invoke-static {v0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->access$1600(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)Lmiui/resourcebrowser/util/BatchMediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/resourcebrowser/util/BatchMediaPlayer;->start()V

    .line 981
    :goto_0
    return-void

    .line 974
    :cond_0
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$11;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    #getter for: Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mBatchPlayer:Lmiui/resourcebrowser/util/BatchMediaPlayer;
    invoke-static {v0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->access$1600(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)Lmiui/resourcebrowser/util/BatchMediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/resourcebrowser/util/BatchMediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 975
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$11;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    #getter for: Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPlayButton:Landroid/widget/ImageView;
    invoke-static {v0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->access$1500(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x60200d5

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 976
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$11;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    #getter for: Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mBatchPlayer:Lmiui/resourcebrowser/util/BatchMediaPlayer;
    invoke-static {v0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->access$1600(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)Lmiui/resourcebrowser/util/BatchMediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/resourcebrowser/util/BatchMediaPlayer;->pause()V

    goto :goto_0

    .line 978
    :cond_1
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$11;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    #getter for: Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPlayButton:Landroid/widget/ImageView;
    invoke-static {v0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->access$1500(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 979
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$11;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    #getter for: Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mBatchPlayer:Lmiui/resourcebrowser/util/BatchMediaPlayer;
    invoke-static {v0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->access$1600(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)Lmiui/resourcebrowser/util/BatchMediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/resourcebrowser/util/BatchMediaPlayer;->start()V

    goto :goto_0
.end method
