.class public Lmiui/resourcebrowser/activity/OnlineResourceAdapter$AsyncLoadMoreResourceTask;
.super Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreDataTask;
.source "OnlineResourceAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/resourcebrowser/activity/OnlineResourceAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "AsyncLoadMoreResourceTask"
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/resourcebrowser/activity/OnlineResourceAdapter;


# direct methods
.method protected constructor <init>(Lmiui/resourcebrowser/activity/OnlineResourceAdapter;)V
    .locals 0
    .parameter

    .prologue
    .line 36
    iput-object p1, p0, Lmiui/resourcebrowser/activity/OnlineResourceAdapter$AsyncLoadMoreResourceTask;->this$0:Lmiui/resourcebrowser/activity/OnlineResourceAdapter;

    invoke-direct {p0, p1}, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreDataTask;-><init>(Lmiui/resourcebrowser/widget/AsyncAdapter;)V

    return-void
.end method


# virtual methods
.method protected loadMoreData(Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreParams;)Ljava/util/List;
    .locals 3
    .parameter "params"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreParams;",
            ")",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    iget-object v1, p0, Lmiui/resourcebrowser/activity/OnlineResourceAdapter$AsyncLoadMoreResourceTask;->this$0:Lmiui/resourcebrowser/activity/OnlineResourceAdapter;

    iget-object v1, v1, Lmiui/resourcebrowser/activity/OnlineResourceAdapter;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v1}, Lmiui/resourcebrowser/ResourceContext;->getPageItemCount()I

    move-result v0

    .line 42
    .local v0, count:I
    iget-object v1, p0, Lmiui/resourcebrowser/activity/OnlineResourceAdapter$AsyncLoadMoreResourceTask;->this$0:Lmiui/resourcebrowser/activity/OnlineResourceAdapter;

    iget-object v1, v1, Lmiui/resourcebrowser/activity/OnlineResourceAdapter;->mResController:Lmiui/resourcebrowser/controller/ResourceController;

    invoke-virtual {v1}, Lmiui/resourcebrowser/controller/ResourceController;->getOnlineDataManager()Lmiui/resourcebrowser/controller/OnlineDataManager;

    move-result-object v1

    iget v2, p1, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreParams;->cursor:I

    add-int/2addr v2, v0

    add-int/lit8 v2, v2, -0x1

    div-int/2addr v2, v0

    invoke-virtual {v1, v2}, Lmiui/resourcebrowser/controller/OnlineDataManager;->getResources(I)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method
