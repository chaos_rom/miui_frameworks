.class Lmiui/resourcebrowser/activity/ResourceListFragment$4;
.super Lmiui/resourcebrowser/controller/local/ImportResourceTask;
.source "ResourceListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiui/resourcebrowser/activity/ResourceListFragment;->tryImportResources()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/resourcebrowser/activity/ResourceListFragment;


# direct methods
.method constructor <init>(Lmiui/resourcebrowser/activity/ResourceListFragment;Lmiui/resourcebrowser/ResourceContext;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 390
    iput-object p1, p0, Lmiui/resourcebrowser/activity/ResourceListFragment$4;->this$0:Lmiui/resourcebrowser/activity/ResourceListFragment;

    invoke-direct {p0, p2, p3}, Lmiui/resourcebrowser/controller/local/ImportResourceTask;-><init>(Lmiui/resourcebrowser/ResourceContext;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 2
    .parameter "successNum"

    .prologue
    .line 393
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-gtz v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 394
    :cond_0
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment$4;->this$0:Lmiui/resourcebrowser/activity/ResourceListFragment;

    iget-object v0, v0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;

    invoke-virtual {v0}, Lmiui/resourcebrowser/activity/ResourceAdapter;->notifyDataSetChanged()V

    .line 395
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment$4;->this$0:Lmiui/resourcebrowser/activity/ResourceListFragment;

    iget-object v0, v0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;

    invoke-virtual {v0}, Lmiui/resourcebrowser/activity/ResourceAdapter;->loadData()V

    .line 397
    :cond_1
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 390
    check-cast p1, Ljava/lang/Integer;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/activity/ResourceListFragment$4;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method
