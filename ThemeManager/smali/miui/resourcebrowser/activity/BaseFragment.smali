.class public Lmiui/resourcebrowser/activity/BaseFragment;
.super Lmiui/app/ObservableFragment;
.source "BaseFragment.java"


# instance fields
.field private mVisibleForUser:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Lmiui/app/ObservableFragment;-><init>()V

    .line 13
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiui/resourcebrowser/activity/BaseFragment;->mVisibleForUser:Z

    return-void
.end method


# virtual methods
.method public isVisibleForUser()Z
    .locals 1

    .prologue
    .line 23
    iget-boolean v0, p0, Lmiui/resourcebrowser/activity/BaseFragment;->mVisibleForUser:Z

    return v0
.end method

.method public onFragmentCreateOptionsMenu(Landroid/view/Menu;)Ljava/util/List;
    .locals 1
    .parameter "menu"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Menu;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method public onFragmentOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 0
    .parameter "item"

    .prologue
    .line 39
    return-void
.end method

.method public onFragmentPrepareOptionsMenu(Landroid/view/Menu;Z)V
    .locals 0
    .parameter "menu"
    .parameter "goingVisiable"

    .prologue
    .line 36
    return-void
.end method

.method public onVisibleChanged()V
    .locals 0

    .prologue
    .line 16
    return-void
.end method

.method public setVisibleForUser(Z)V
    .locals 0
    .parameter "visibleForUser"

    .prologue
    .line 19
    iput-boolean p1, p0, Lmiui/resourcebrowser/activity/BaseFragment;->mVisibleForUser:Z

    .line 20
    return-void
.end method
