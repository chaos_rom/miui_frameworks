.class Lmiui/resourcebrowser/activity/ResourceListFragment$5;
.super Ljava/lang/Object;
.source "ResourceListFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiui/resourcebrowser/activity/ResourceListFragment;->tryImportResources()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/resourcebrowser/activity/ResourceListFragment;

.field final synthetic val$isResImportedFile:Ljava/io/File;


# direct methods
.method constructor <init>(Lmiui/resourcebrowser/activity/ResourceListFragment;Ljava/io/File;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 419
    iput-object p1, p0, Lmiui/resourcebrowser/activity/ResourceListFragment$5;->this$0:Lmiui/resourcebrowser/activity/ResourceListFragment;

    iput-object p2, p0, Lmiui/resourcebrowser/activity/ResourceListFragment$5;->val$isResImportedFile:Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .parameter "dialog"
    .parameter "which"

    .prologue
    const/4 v3, 0x0

    .line 422
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 423
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment$5;->this$0:Lmiui/resourcebrowser/activity/ResourceListFragment;

    new-instance v1, Landroid/app/ProgressDialog;

    iget-object v2, p0, Lmiui/resourcebrowser/activity/ResourceListFragment$5;->this$0:Lmiui/resourcebrowser/activity/ResourceListFragment;

    iget-object v2, v2, Lmiui/resourcebrowser/activity/ResourceListFragment;->mActivity:Landroid/app/Activity;

    invoke-direct {v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, v0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 424
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment$5;->this$0:Lmiui/resourcebrowser/activity/ResourceListFragment;

    iget-object v0, v0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 425
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment$5;->this$0:Lmiui/resourcebrowser/activity/ResourceListFragment;

    iget-object v0, v0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceListFragment$5;->this$0:Lmiui/resourcebrowser/activity/ResourceListFragment;

    iget-object v1, v1, Lmiui/resourcebrowser/activity/ResourceListFragment;->mActivity:Landroid/app/Activity;

    const v2, 0x60c023d

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 426
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment$5;->this$0:Lmiui/resourcebrowser/activity/ResourceListFragment;

    iget-object v0, v0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceListFragment$5;->this$0:Lmiui/resourcebrowser/activity/ResourceListFragment;

    iget-object v1, v1, Lmiui/resourcebrowser/activity/ResourceListFragment;->mActivity:Landroid/app/Activity;

    const v2, 0x60c023e

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 427
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment$5;->this$0:Lmiui/resourcebrowser/activity/ResourceListFragment;

    iget-object v0, v0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 428
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment$5;->this$0:Lmiui/resourcebrowser/activity/ResourceListFragment;

    iget-object v0, v0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressNumberFormat(Ljava/lang/String;)V

    .line 429
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment$5;->this$0:Lmiui/resourcebrowser/activity/ResourceListFragment;

    iget-object v0, v0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMax(I)V

    .line 430
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment$5;->this$0:Lmiui/resourcebrowser/activity/ResourceListFragment;

    iget-object v0, v0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 432
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment$5;->this$0:Lmiui/resourcebrowser/activity/ResourceListFragment;

    iget-object v0, v0, Lmiui/resourcebrowser/activity/ResourceListFragment;->mProgressBar:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 434
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceListFragment$5;->this$0:Lmiui/resourcebrowser/activity/ResourceListFragment;

    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceListFragment$5;->val$isResImportedFile:Ljava/io/File;

    #calls: Lmiui/resourcebrowser/activity/ResourceListFragment;->startImportResourceTask(Ljava/io/File;)V
    invoke-static {v0, v1}, Lmiui/resourcebrowser/activity/ResourceListFragment;->access$300(Lmiui/resourcebrowser/activity/ResourceListFragment;Ljava/io/File;)V

    .line 435
    return-void
.end method
