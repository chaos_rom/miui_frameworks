.class Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload$1;
.super Landroid/os/AsyncTask;
.source "ResourceAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;->startOneDownloadTask()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;

.field final synthetic val$downloadEntry:Lmiui/resourcebrowser/model/PathEntry;


# direct methods
.method constructor <init>(Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;Lmiui/resourcebrowser/model/PathEntry;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 540
    iput-object p1, p0, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload$1;->this$1:Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;

    iput-object p2, p0, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload$1;->val$downloadEntry:Lmiui/resourcebrowser/model/PathEntry;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"

    .prologue
    .line 540
    check-cast p1, [Ljava/lang/Void;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 4
    .parameter "params"

    .prologue
    .line 543
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload$1;->val$downloadEntry:Lmiui/resourcebrowser/model/PathEntry;

    invoke-virtual {v1}, Lmiui/resourcebrowser/model/PathEntry;->getLocalPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 544
    new-instance v0, Lmiui/resourcebrowser/controller/online/DownloadFileTask;

    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload$1;->val$downloadEntry:Lmiui/resourcebrowser/model/PathEntry;

    invoke-virtual {v1}, Lmiui/resourcebrowser/model/PathEntry;->getOnlinePath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lmiui/resourcebrowser/controller/online/DownloadFileTask;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x1

    new-array v1, v1, [Lmiui/resourcebrowser/model/PathEntry;

    const/4 v2, 0x0

    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload$1;->val$downloadEntry:Lmiui/resourcebrowser/model/PathEntry;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/controller/online/DownloadFileTask;->downloadFiles([Lmiui/resourcebrowser/model/PathEntry;)Z

    .line 546
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onCancelled()V
    .locals 2

    .prologue
    .line 550
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Thumbnail downloading task can not be cancelled!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 540
    check-cast p1, Ljava/lang/Void;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload$1;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 4
    .parameter "result"

    .prologue
    .line 555
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload$1;->this$1:Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;

    iget-object v0, v0, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;->this$0:Lmiui/resourcebrowser/activity/ResourceAdapter;

    invoke-virtual {v0}, Lmiui/resourcebrowser/activity/ResourceAdapter;->notifyDataSetChanged()V

    .line 556
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload$1;->this$1:Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;

    #getter for: Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;->mDownloadingJobsSet:Ljava/util/Set;
    invoke-static {v0}, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;->access$100(Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;)Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload$1;->val$downloadEntry:Lmiui/resourcebrowser/model/PathEntry;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 557
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload$1;->this$1:Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;

    #getter for: Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;->mFinishJobsQueue:Ljava/util/Map;
    invoke-static {v0}, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;->access$200(Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    const/16 v1, 0x32

    if-le v0, v1, :cond_0

    .line 558
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload$1;->this$1:Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;

    #getter for: Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;->mFinishJobsQueue:Ljava/util/Map;
    invoke-static {v0}, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;->access$200(Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 560
    :cond_0
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload$1;->this$1:Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;

    #getter for: Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;->mFinishJobsQueue:Ljava/util/Map;
    invoke-static {v0}, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;->access$200(Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload$1;->val$downloadEntry:Lmiui/resourcebrowser/model/PathEntry;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 561
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload$1;->this$1:Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;

    invoke-static {v0}, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;->access$306(Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;)I

    .line 563
    sget-boolean v0, Lmiui/resourcebrowser/util/ResourceDebug;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 564
    const-string v0, "Theme"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Finish one thumbnail downloading task: RemainTaskNumber="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload$1;->this$1:Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;

    #getter for: Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;->mWaitingJobsQueue:Ljava/util/Map;
    invoke-static {v2}, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;->access$400(Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;)Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ExecutingThreadNumber="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload$1;->this$1:Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;

    #getter for: Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;->mCurrentParallelTaskNumber:I
    invoke-static {v2}, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;->access$300(Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 568
    :cond_1
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload$1;->this$1:Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;

    #calls: Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;->startOneDownloadTask()V
    invoke-static {v0}, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;->access$500(Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;)V

    .line 569
    return-void
.end method
