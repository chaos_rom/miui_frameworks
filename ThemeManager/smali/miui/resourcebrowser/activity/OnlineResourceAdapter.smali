.class public Lmiui/resourcebrowser/activity/OnlineResourceAdapter;
.super Lmiui/resourcebrowser/activity/ResourceAdapter;
.source "OnlineResourceAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/resourcebrowser/activity/OnlineResourceAdapter$AsyncLoadMoreResourceTask;
    }
.end annotation


# direct methods
.method public constructor <init>(Lmiui/resourcebrowser/activity/BaseFragment;Lmiui/resourcebrowser/ResourceContext;)V
    .locals 0
    .parameter "fragment"
    .parameter "resContext"

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Lmiui/resourcebrowser/activity/ResourceAdapter;-><init>(Lmiui/resourcebrowser/activity/BaseFragment;Lmiui/resourcebrowser/ResourceContext;)V

    .line 21
    return-void
.end method


# virtual methods
.method protected getLoadMoreDataTask()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/widget/AsyncAdapter",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">.Async",
            "LoadMoreDataTask;",
            ">;"
        }
    .end annotation

    .prologue
    .line 29
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 30
    .local v1, tasks:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/widget/AsyncAdapter<Lmiui/resourcebrowser/model/Resource;>.AsyncLoadMoreDataTask;>;"
    new-instance v0, Lmiui/resourcebrowser/activity/OnlineResourceAdapter$AsyncLoadMoreResourceTask;

    invoke-direct {v0, p0}, Lmiui/resourcebrowser/activity/OnlineResourceAdapter$AsyncLoadMoreResourceTask;-><init>(Lmiui/resourcebrowser/activity/OnlineResourceAdapter;)V

    .line 31
    .local v0, task:Lmiui/resourcebrowser/activity/OnlineResourceAdapter$AsyncLoadMoreResourceTask;
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/OnlineResourceAdapter;->getRegisterAsyncTaskObserver()Lmiui/os/AsyncTaskObserver;

    move-result-object v2

    invoke-virtual {v0, v2}, Lmiui/resourcebrowser/activity/OnlineResourceAdapter$AsyncLoadMoreResourceTask;->addObserver(Lmiui/os/AsyncTaskObserver;)V

    .line 32
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 33
    return-object v1
.end method

.method protected postLoadMoreData(Ljava/util/List;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 48
    .local p1, result:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/Resource;>;"
    if-nez p1, :cond_1

    iget-object v0, p0, Lmiui/resourcebrowser/activity/OnlineResourceAdapter;->mFragment:Lmiui/resourcebrowser/activity/BaseFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiui/resourcebrowser/activity/OnlineResourceAdapter;->mFragment:Lmiui/resourcebrowser/activity/BaseFragment;

    invoke-virtual {v0}, Lmiui/resourcebrowser/activity/BaseFragment;->isVisibleForUser()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 49
    :cond_0
    iget-object v0, p0, Lmiui/resourcebrowser/activity/OnlineResourceAdapter;->mContext:Landroid/app/Activity;

    const v1, 0x60c0020

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 51
    :cond_1
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/OnlineResourceAdapter;->updateNoResultText()V

    .line 52
    return-void
.end method
