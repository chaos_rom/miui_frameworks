.class public Lmiui/resourcebrowser/activity/ResourceTabActivity;
.super Lmiui/resourcebrowser/activity/BaseTabActivity;
.source "ResourceTabActivity.java"


# instance fields
.field protected mResContext:Lmiui/resourcebrowser/ResourceContext;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lmiui/resourcebrowser/activity/BaseTabActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected buildResourceContext(Lmiui/resourcebrowser/ResourceContext;)Lmiui/resourcebrowser/ResourceContext;
    .locals 1
    .parameter "resContext"

    .prologue
    .line 103
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceTabActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p1, v0, p0}, Lmiui/resourcebrowser/util/ResourceHelper;->buildResourceContext(Lmiui/resourcebrowser/ResourceContext;Landroid/content/Intent;Landroid/content/Context;)Lmiui/resourcebrowser/ResourceContext;

    move-result-object v0

    return-object v0
.end method

.method protected getActionBarTabs()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/app/ActionBar$Tab;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 68
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 69
    .local v2, ret:Ljava/util/List;,"Ljava/util/List<Landroid/app/ActionBar$Tab;>;"
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceTabActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 70
    .local v0, bar:Landroid/app/ActionBar;
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceTabActivity;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v3}, Lmiui/resourcebrowser/ResourceContext;->getResourceTitle()Ljava/lang/String;

    move-result-object v1

    .line 71
    .local v1, resourceSetName:Ljava/lang/String;
    invoke-virtual {v0}, Landroid/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v3

    const v4, 0x60c0014

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v1, v5, v6

    invoke-virtual {p0, v4, v5}, Lmiui/resourcebrowser/activity/ResourceTabActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/ActionBar$Tab;->setText(Ljava/lang/CharSequence;)Landroid/app/ActionBar$Tab;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    invoke-virtual {v0}, Landroid/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v3

    const v4, 0x60c0015

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v1, v5, v6

    invoke-virtual {p0, v4, v5}, Lmiui/resourcebrowser/activity/ResourceTabActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/ActionBar$Tab;->setText(Ljava/lang/CharSequence;)Landroid/app/ActionBar$Tab;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 73
    return-object v2
.end method

.method protected getLocalResourceListFragment()Lmiui/resourcebrowser/activity/BaseFragment;
    .locals 1

    .prologue
    .line 95
    new-instance v0, Lmiui/resourcebrowser/activity/LocalResourceListFragment;

    invoke-direct {v0}, Lmiui/resourcebrowser/activity/LocalResourceListFragment;-><init>()V

    return-object v0
.end method

.method protected getOnlineResourceListFragment()Lmiui/resourcebrowser/activity/BaseFragment;
    .locals 1

    .prologue
    .line 99
    new-instance v0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;

    invoke-direct {v0}, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;-><init>()V

    return-object v0
.end method

.method protected getResourceController(Lmiui/resourcebrowser/ResourceContext;)Lmiui/resourcebrowser/controller/ResourceController;
    .locals 1
    .parameter "resContext"

    .prologue
    .line 107
    new-instance v0, Lmiui/resourcebrowser/controller/ResourceController;

    invoke-direct {v0, p1}, Lmiui/resourcebrowser/controller/ResourceController;-><init>(Lmiui/resourcebrowser/ResourceContext;)V

    return-object v0
.end method

.method protected initTabFragment(I)Lmiui/resourcebrowser/activity/BaseFragment;
    .locals 1
    .parameter "tabPosition"

    .prologue
    .line 78
    if-nez p1, :cond_0

    .line 79
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceTabActivity;->getLocalResourceListFragment()Lmiui/resourcebrowser/activity/BaseFragment;

    move-result-object v0

    .line 83
    :goto_0
    return-object v0

    .line 80
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 81
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceTabActivity;->getOnlineResourceListFragment()Lmiui/resourcebrowser/activity/BaseFragment;

    move-result-object v0

    goto :goto_0

    .line 83
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .parameter "savedInstanceState"

    .prologue
    const/4 v6, 0x0

    .line 30
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceTabActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "REQUEST_RES_CONTEXT"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v4

    check-cast v4, Lmiui/resourcebrowser/ResourceContext;

    iput-object v4, p0, Lmiui/resourcebrowser/activity/ResourceTabActivity;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    .line 31
    iget-object v4, p0, Lmiui/resourcebrowser/activity/ResourceTabActivity;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    if-nez v4, :cond_0

    .line 32
    new-instance v4, Lmiui/resourcebrowser/ResourceContext;

    invoke-direct {v4}, Lmiui/resourcebrowser/ResourceContext;-><init>()V

    invoke-virtual {p0, v4}, Lmiui/resourcebrowser/activity/ResourceTabActivity;->buildResourceContext(Lmiui/resourcebrowser/ResourceContext;)Lmiui/resourcebrowser/ResourceContext;

    move-result-object v4

    iput-object v4, p0, Lmiui/resourcebrowser/activity/ResourceTabActivity;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    .line 34
    :cond_0
    invoke-static {}, Lmiui/resourcebrowser/AppInnerContext;->getInstance()Lmiui/resourcebrowser/AppInnerContext;

    move-result-object v1

    .line 35
    .local v1, appContext:Lmiui/resourcebrowser/AppInnerContext;
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceTabActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v1, v4}, Lmiui/resourcebrowser/AppInnerContext;->setApplicationContext(Landroid/content/Context;)V

    .line 36
    iget-object v4, p0, Lmiui/resourcebrowser/activity/ResourceTabActivity;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v1, v4}, Lmiui/resourcebrowser/AppInnerContext;->setResourceContext(Lmiui/resourcebrowser/ResourceContext;)V

    .line 37
    iget-object v4, p0, Lmiui/resourcebrowser/activity/ResourceTabActivity;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {p0, v4}, Lmiui/resourcebrowser/activity/ResourceTabActivity;->getResourceController(Lmiui/resourcebrowser/ResourceContext;)Lmiui/resourcebrowser/controller/ResourceController;

    move-result-object v4

    invoke-virtual {v1, v4}, Lmiui/resourcebrowser/AppInnerContext;->setResourceController(Lmiui/resourcebrowser/controller/ResourceController;)V

    .line 39
    invoke-super {p0, p1}, Lmiui/resourcebrowser/activity/BaseTabActivity;->onCreate(Landroid/os/Bundle;)V

    .line 41
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceTabActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 42
    .local v0, actionBar:Landroid/app/ActionBar;
    new-instance v3, Landroid/widget/ImageView;

    invoke-direct {v3, p0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 43
    .local v3, searchView:Landroid/widget/ImageView;
    const v4, 0x60201b8

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 44
    const/16 v4, 0x12

    invoke-virtual {v3, v6, v6, v4, v6}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 45
    new-instance v4, Lmiui/resourcebrowser/activity/ResourceTabActivity$1;

    invoke-direct {v4, p0}, Lmiui/resourcebrowser/activity/ResourceTabActivity$1;-><init>(Lmiui/resourcebrowser/activity/ResourceTabActivity;)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 51
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 52
    new-instance v2, Landroid/app/ActionBar$LayoutParams;

    const/4 v4, 0x5

    invoke-direct {v2, v4}, Landroid/app/ActionBar$LayoutParams;-><init>(I)V

    .line 53
    .local v2, params:Landroid/app/ActionBar$LayoutParams;
    invoke-virtual {v0, v3, v2}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    .line 55
    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    new-instance v5, Lmiui/resourcebrowser/activity/ResourceTabActivity$2;

    invoke-direct {v5, p0}, Lmiui/resourcebrowser/activity/ResourceTabActivity$2;-><init>(Lmiui/resourcebrowser/activity/ResourceTabActivity;)V

    const-wide/16 v6, 0x320

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 64
    return-void
.end method

.method public onSearchRequested()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 88
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 89
    .local v0, appData:Landroid/os/Bundle;
    const-string v1, "REQUEST_RES_CONTEXT"

    iget-object v2, p0, Lmiui/resourcebrowser/activity/ResourceTabActivity;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 90
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v3, v0, v3}, Lmiui/resourcebrowser/activity/ResourceTabActivity;->startSearch(Ljava/lang/String;ZLandroid/os/Bundle;Z)V

    .line 91
    const/4 v1, 0x1

    return v1
.end method
