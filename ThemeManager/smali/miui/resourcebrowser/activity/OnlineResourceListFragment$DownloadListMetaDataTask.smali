.class public Lmiui/resourcebrowser/activity/OnlineResourceListFragment$DownloadListMetaDataTask;
.super Landroid/os/AsyncTask;
.source "OnlineResourceListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/resourcebrowser/activity/OnlineResourceListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "DownloadListMetaDataTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Lmiui/resourcebrowser/model/ResourceListMeta;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/resourcebrowser/activity/OnlineResourceListFragment;


# direct methods
.method protected constructor <init>(Lmiui/resourcebrowser/activity/OnlineResourceListFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 287
    iput-object p1, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment$DownloadListMetaDataTask;->this$0:Lmiui/resourcebrowser/activity/OnlineResourceListFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"

    .prologue
    .line 287
    check-cast p1, [Ljava/lang/String;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/activity/OnlineResourceListFragment$DownloadListMetaDataTask;->doInBackground([Ljava/lang/String;)Lmiui/resourcebrowser/model/ResourceListMeta;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Lmiui/resourcebrowser/model/ResourceListMeta;
    .locals 2
    .parameter "params"

    .prologue
    .line 291
    iget-object v0, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment$DownloadListMetaDataTask;->this$0:Lmiui/resourcebrowser/activity/OnlineResourceListFragment;

    iget-object v0, v0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->mResController:Lmiui/resourcebrowser/controller/ResourceController;

    invoke-virtual {v0}, Lmiui/resourcebrowser/controller/ResourceController;->getOnlineDataManager()Lmiui/resourcebrowser/controller/OnlineDataManager;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/controller/OnlineDataManager;->getResourceListMeta(Ljava/lang/String;)Lmiui/resourcebrowser/model/ResourceListMeta;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 287
    check-cast p1, Lmiui/resourcebrowser/model/ResourceListMeta;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/activity/OnlineResourceListFragment$DownloadListMetaDataTask;->onPostExecute(Lmiui/resourcebrowser/model/ResourceListMeta;)V

    return-void
.end method

.method protected onPostExecute(Lmiui/resourcebrowser/model/ResourceListMeta;)V
    .locals 1
    .parameter "result"

    .prologue
    .line 296
    iget-object v0, p0, Lmiui/resourcebrowser/activity/OnlineResourceListFragment$DownloadListMetaDataTask;->this$0:Lmiui/resourcebrowser/activity/OnlineResourceListFragment;

    invoke-virtual {v0, p1}, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->setListMetaData(Lmiui/resourcebrowser/model/ResourceListMeta;)V

    .line 297
    return-void
.end method
