.class public abstract Lmiui/resourcebrowser/activity/BaseTabActivity;
.super Lmiui/app/ObservableActivity;
.source "BaseTabActivity.java"

# interfaces
.implements Landroid/app/ActionBar$TabListener;
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/resourcebrowser/activity/BaseTabActivity$ResourcePagerAdapter;
    }
.end annotation


# instance fields
.field protected mActionBar:Landroid/app/ActionBar;

.field protected mCurrentPagePosition:I

.field protected mFragmentsMenuId:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field protected mPageAdapter:Lmiui/resourcebrowser/activity/BaseTabActivity$ResourcePagerAdapter;

.field protected mTabFragments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/activity/BaseFragment;",
            ">;"
        }
    .end annotation
.end field

.field protected mViewPager:Landroid/support/v4/view/ViewPager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    invoke-static {v0}, Landroid/os/AsyncTask;->setDefaultExecutor(Ljava/util/concurrent/Executor;)V

    .line 27
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Lmiui/app/ObservableActivity;-><init>()V

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/activity/BaseTabActivity;->mTabFragments:Ljava/util/List;

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/activity/BaseTabActivity;->mFragmentsMenuId:Ljava/util/List;

    .line 154
    return-void
.end method

.method private changeFragmentVisibility(IZ)V
    .locals 2
    .parameter "position"
    .parameter "visible"

    .prologue
    .line 149
    iget-object v1, p0, Lmiui/resourcebrowser/activity/BaseTabActivity;->mTabFragments:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/resourcebrowser/activity/BaseFragment;

    .line 150
    .local v0, frag:Lmiui/resourcebrowser/activity/BaseFragment;
    invoke-virtual {v0, p2}, Lmiui/resourcebrowser/activity/BaseFragment;->setVisibleForUser(Z)V

    .line 151
    invoke-virtual {v0}, Lmiui/resourcebrowser/activity/BaseFragment;->onVisibleChanged()V

    .line 152
    return-void
.end method

.method private createActionBar()V
    .locals 4

    .prologue
    .line 63
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/BaseTabActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    iput-object v2, p0, Lmiui/resourcebrowser/activity/BaseTabActivity;->mActionBar:Landroid/app/ActionBar;

    .line 64
    iget-object v2, p0, Lmiui/resourcebrowser/activity/BaseTabActivity;->mActionBar:Landroid/app/ActionBar;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 66
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/BaseTabActivity;->getActionBarTabs()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActionBar$Tab;

    .line 67
    .local v1, tab:Landroid/app/ActionBar$Tab;
    invoke-virtual {v1, p0}, Landroid/app/ActionBar$Tab;->setTabListener(Landroid/app/ActionBar$TabListener;)Landroid/app/ActionBar$Tab;

    .line 68
    iget-object v2, p0, Lmiui/resourcebrowser/activity/BaseTabActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v2, v1}, Landroid/app/ActionBar;->addTab(Landroid/app/ActionBar$Tab;)V

    goto :goto_0

    .line 70
    .end local v1           #tab:Landroid/app/ActionBar$Tab;
    :cond_0
    return-void
.end method

.method private createPagerAdapter()V
    .locals 2

    .prologue
    .line 53
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/BaseTabActivity;->getResourcePagerAdapter()Lmiui/resourcebrowser/activity/BaseTabActivity$ResourcePagerAdapter;

    move-result-object v0

    iput-object v0, p0, Lmiui/resourcebrowser/activity/BaseTabActivity;->mPageAdapter:Lmiui/resourcebrowser/activity/BaseTabActivity$ResourcePagerAdapter;

    .line 54
    iget-object v0, p0, Lmiui/resourcebrowser/activity/BaseTabActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lmiui/resourcebrowser/activity/BaseTabActivity;->mPageAdapter:Lmiui/resourcebrowser/activity/BaseTabActivity$ResourcePagerAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 55
    iget-object v0, p0, Lmiui/resourcebrowser/activity/BaseTabActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 56
    return-void
.end method

.method private createTabFragments()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 73
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/BaseTabActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 74
    .local v1, fragmentManager:Landroid/app/FragmentManager;
    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v4

    .line 75
    .local v4, transaction:Landroid/app/FragmentTransaction;
    iget-object v5, p0, Lmiui/resourcebrowser/activity/BaseTabActivity;->mTabFragments:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->clear()V

    .line 76
    const/4 v2, 0x0

    .local v2, i:I
    :goto_0
    iget-object v5, p0, Lmiui/resourcebrowser/activity/BaseTabActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v5}, Landroid/app/ActionBar;->getTabCount()I

    move-result v5

    if-ge v2, v5, :cond_1

    .line 77
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "tag-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 78
    .local v3, tag:Ljava/lang/String;
    invoke-virtual {v1, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 79
    .local v0, frag:Landroid/app/Fragment;
    if-nez v0, :cond_0

    .line 80
    invoke-virtual {p0, v2}, Lmiui/resourcebrowser/activity/BaseTabActivity;->initTabFragment(I)Lmiui/resourcebrowser/activity/BaseFragment;

    move-result-object v0

    .line 81
    invoke-virtual {v4, v7, v0, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 82
    invoke-virtual {v4, v0}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 84
    :cond_0
    iget-object v5, p0, Lmiui/resourcebrowser/activity/BaseTabActivity;->mTabFragments:Ljava/util/List;

    check-cast v0, Lmiui/resourcebrowser/activity/BaseFragment;

    .end local v0           #frag:Landroid/app/Fragment;
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 76
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 86
    .end local v3           #tag:Ljava/lang/String;
    :cond_1
    invoke-virtual {v4}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 87
    invoke-virtual {v1}, Landroid/app/FragmentManager;->executePendingTransactions()Z

    .line 88
    iget-object v5, p0, Lmiui/resourcebrowser/activity/BaseTabActivity;->mTabFragments:Ljava/util/List;

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lmiui/resourcebrowser/activity/BaseFragment;

    invoke-virtual {v5, v7}, Lmiui/resourcebrowser/activity/BaseFragment;->setVisibleForUser(Z)V

    .line 89
    return-void
.end method

.method private selectTab(IZ)V
    .locals 3
    .parameter "position"
    .parameter "updateFragment"

    .prologue
    const/4 v2, 0x1

    .line 132
    iget v0, p0, Lmiui/resourcebrowser/activity/BaseTabActivity;->mCurrentPagePosition:I

    if-ne p1, v0, :cond_0

    .line 146
    :goto_0
    return-void

    .line 135
    :cond_0
    iget v0, p0, Lmiui/resourcebrowser/activity/BaseTabActivity;->mCurrentPagePosition:I

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lmiui/resourcebrowser/activity/BaseTabActivity;->changeFragmentVisibility(IZ)V

    .line 137
    iput p1, p0, Lmiui/resourcebrowser/activity/BaseTabActivity;->mCurrentPagePosition:I

    .line 139
    iget-object v0, p0, Lmiui/resourcebrowser/activity/BaseTabActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    .line 140
    if-eqz p2, :cond_1

    .line 141
    iget-object v0, p0, Lmiui/resourcebrowser/activity/BaseTabActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p1, v2}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    .line 143
    :cond_1
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/BaseTabActivity;->invalidateOptionsMenu()V

    .line 145
    iget v0, p0, Lmiui/resourcebrowser/activity/BaseTabActivity;->mCurrentPagePosition:I

    invoke-direct {p0, v0, v2}, Lmiui/resourcebrowser/activity/BaseTabActivity;->changeFragmentVisibility(IZ)V

    goto :goto_0
.end method


# virtual methods
.method protected abstract getActionBarTabs()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/app/ActionBar$Tab;",
            ">;"
        }
    .end annotation
.end method

.method protected getResourcePagerAdapter()Lmiui/resourcebrowser/activity/BaseTabActivity$ResourcePagerAdapter;
    .locals 1

    .prologue
    .line 59
    new-instance v0, Lmiui/resourcebrowser/activity/BaseTabActivity$ResourcePagerAdapter;

    invoke-direct {v0, p0}, Lmiui/resourcebrowser/activity/BaseTabActivity$ResourcePagerAdapter;-><init>(Lmiui/resourcebrowser/activity/BaseTabActivity;)V

    return-object v0
.end method

.method protected abstract initTabFragment(I)Lmiui/resourcebrowser/activity/BaseFragment;
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter "savedInstanceState"

    .prologue
    .line 42
    invoke-super {p0, p1}, Lmiui/app/ObservableActivity;->onCreate(Landroid/os/Bundle;)V

    .line 43
    new-instance v0, Landroid/support/v4/view/ViewPager;

    invoke-direct {v0, p0}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lmiui/resourcebrowser/activity/BaseTabActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 44
    iget-object v0, p0, Lmiui/resourcebrowser/activity/BaseTabActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setId(I)V

    .line 45
    iget-object v0, p0, Lmiui/resourcebrowser/activity/BaseTabActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/activity/BaseTabActivity;->setContentView(Landroid/view/View;)V

    .line 47
    invoke-direct {p0}, Lmiui/resourcebrowser/activity/BaseTabActivity;->createPagerAdapter()V

    .line 48
    invoke-direct {p0}, Lmiui/resourcebrowser/activity/BaseTabActivity;->createActionBar()V

    .line 49
    invoke-direct {p0}, Lmiui/resourcebrowser/activity/BaseTabActivity;->createTabFragments()V

    .line 50
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .parameter "menu"

    .prologue
    .line 97
    iget-object v3, p0, Lmiui/resourcebrowser/activity/BaseTabActivity;->mFragmentsMenuId:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 98
    iget-object v3, p0, Lmiui/resourcebrowser/activity/BaseTabActivity;->mTabFragments:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/resourcebrowser/activity/BaseFragment;

    .line 99
    .local v0, f:Lmiui/resourcebrowser/activity/BaseFragment;
    invoke-virtual {v0, p1}, Lmiui/resourcebrowser/activity/BaseFragment;->onFragmentCreateOptionsMenu(Landroid/view/Menu;)Ljava/util/List;

    move-result-object v2

    .line 100
    .local v2, menuId:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Integer;>;"
    iget-object v3, p0, Lmiui/resourcebrowser/activity/BaseTabActivity;->mFragmentsMenuId:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 102
    .end local v0           #f:Lmiui/resourcebrowser/activity/BaseFragment;
    .end local v2           #menuId:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Integer;>;"
    :cond_0
    invoke-super {p0, p1}, Lmiui/app/ObservableActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v3

    return v3
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .parameter "item"

    .prologue
    .line 123
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    iget-object v1, p0, Lmiui/resourcebrowser/activity/BaseTabActivity;->mFragmentsMenuId:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 124
    iget-object v1, p0, Lmiui/resourcebrowser/activity/BaseTabActivity;->mFragmentsMenuId:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 125
    iget-object v1, p0, Lmiui/resourcebrowser/activity/BaseTabActivity;->mTabFragments:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/resourcebrowser/activity/BaseFragment;

    invoke-virtual {v1, p1}, Lmiui/resourcebrowser/activity/BaseFragment;->onFragmentOptionsItemSelected(Landroid/view/MenuItem;)V

    .line 123
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 128
    :cond_1
    invoke-super {p0, p1}, Lmiui/app/ObservableActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    return v1
.end method

.method public onPageScrollStateChanged(I)V
    .locals 0
    .parameter "state"

    .prologue
    .line 221
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 0
    .parameter "position"
    .parameter "positionOffset"
    .parameter "positionOffsetPixels"

    .prologue
    .line 225
    return-void
.end method

.method public onPageSelected(I)V
    .locals 1
    .parameter "position"

    .prologue
    .line 229
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lmiui/resourcebrowser/activity/BaseTabActivity;->selectTab(IZ)V

    .line 230
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 7
    .parameter "menu"

    .prologue
    const/4 v6, 0x0

    .line 107
    const/4 v2, 0x0

    .local v2, i:I
    :goto_0
    iget-object v5, p0, Lmiui/resourcebrowser/activity/BaseTabActivity;->mTabFragments:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v2, v5, :cond_2

    .line 108
    iget-object v5, p0, Lmiui/resourcebrowser/activity/BaseTabActivity;->mTabFragments:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/resourcebrowser/activity/BaseFragment;

    .line 109
    .local v0, f:Lmiui/resourcebrowser/activity/BaseFragment;
    iget v5, p0, Lmiui/resourcebrowser/activity/BaseTabActivity;->mCurrentPagePosition:I

    if-ne v5, v2, :cond_0

    const/4 v1, 0x1

    .line 110
    .local v1, goingVisiable:Z
    :goto_1
    invoke-virtual {v0, p1, v1}, Lmiui/resourcebrowser/activity/BaseFragment;->onFragmentPrepareOptionsMenu(Landroid/view/Menu;Z)V

    .line 112
    if-nez v1, :cond_1

    .line 113
    iget-object v5, p0, Lmiui/resourcebrowser/activity/BaseTabActivity;->mFragmentsMenuId:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, i$:Ljava/util/Iterator;
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 114
    .local v4, menuId:I
    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    invoke-interface {v5, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_2

    .end local v1           #goingVisiable:Z
    .end local v3           #i$:Ljava/util/Iterator;
    .end local v4           #menuId:I
    :cond_0
    move v1, v6

    .line 109
    goto :goto_1

    .line 107
    .restart local v1       #goingVisiable:Z
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 118
    .end local v0           #f:Lmiui/resourcebrowser/activity/BaseFragment;
    .end local v1           #goingVisiable:Z
    :cond_2
    invoke-super {p0, p1}, Lmiui/app/ObservableActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v5

    return v5
.end method

.method public onTabReselected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .locals 0
    .parameter "tab"
    .parameter "ft"

    .prologue
    .line 208
    return-void
.end method

.method public onTabSelected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .locals 2
    .parameter "tab"
    .parameter "ft"

    .prologue
    .line 212
    invoke-virtual {p1}, Landroid/app/ActionBar$Tab;->getPosition()I

    move-result v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lmiui/resourcebrowser/activity/BaseTabActivity;->selectTab(IZ)V

    .line 213
    return-void
.end method

.method public onTabUnselected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .locals 0
    .parameter "tab"
    .parameter "ft"

    .prologue
    .line 217
    return-void
.end method
