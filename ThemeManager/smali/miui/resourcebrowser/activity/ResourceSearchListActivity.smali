.class public Lmiui/resourcebrowser/activity/ResourceSearchListActivity;
.super Lmiui/app/ObservableActivity;
.source "ResourceSearchListActivity.java"


# instance fields
.field protected mFragment:Lmiui/resourcebrowser/activity/BaseFragment;

.field protected mResContext:Lmiui/resourcebrowser/ResourceContext;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lmiui/app/ObservableActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected getFragment()Lmiui/resourcebrowser/activity/BaseFragment;
    .locals 1

    .prologue
    .line 90
    new-instance v0, Lmiui/resourcebrowser/activity/ResourceSearchListFragment;

    invoke-direct {v0}, Lmiui/resourcebrowser/activity/ResourceSearchListFragment;-><init>()V

    return-object v0
.end method

.method protected getResourceController(Lmiui/resourcebrowser/ResourceContext;)Lmiui/resourcebrowser/controller/ResourceController;
    .locals 1
    .parameter "resContext"

    .prologue
    .line 86
    new-instance v0, Lmiui/resourcebrowser/controller/ResourceController;

    invoke-direct {v0, p1}, Lmiui/resourcebrowser/controller/ResourceController;-><init>(Lmiui/resourcebrowser/ResourceContext;)V

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12
    .parameter "savedInstanceState"

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 31
    invoke-super {p0, p1}, Lmiui/app/ObservableActivity;->onCreate(Landroid/os/Bundle;)V

    .line 32
    const v9, 0x6030019

    invoke-virtual {p0, v9}, Lmiui/resourcebrowser/activity/ResourceSearchListActivity;->setContentView(I)V

    .line 34
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceSearchListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    .line 36
    .local v6, intent:Landroid/content/Intent;
    const-string v9, "app_data"

    invoke-virtual {v6, v9}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    .line 37
    .local v2, appData:Landroid/os/Bundle;
    const-string v9, "REQUEST_RES_CONTEXT"

    invoke-virtual {v2, v9}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v9

    check-cast v9, Lmiui/resourcebrowser/ResourceContext;

    iput-object v9, p0, Lmiui/resourcebrowser/activity/ResourceSearchListActivity;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    .line 38
    invoke-static {}, Lmiui/resourcebrowser/AppInnerContext;->getInstance()Lmiui/resourcebrowser/AppInnerContext;

    move-result-object v1

    .line 39
    .local v1, appContext:Lmiui/resourcebrowser/AppInnerContext;
    iget-object v9, p0, Lmiui/resourcebrowser/activity/ResourceSearchListActivity;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v1, v9}, Lmiui/resourcebrowser/AppInnerContext;->setResourceContext(Lmiui/resourcebrowser/ResourceContext;)V

    .line 40
    iget-object v9, p0, Lmiui/resourcebrowser/activity/ResourceSearchListActivity;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {p0, v9}, Lmiui/resourcebrowser/activity/ResourceSearchListActivity;->getResourceController(Lmiui/resourcebrowser/ResourceContext;)Lmiui/resourcebrowser/controller/ResourceController;

    move-result-object v3

    .line 41
    .local v3, controller:Lmiui/resourcebrowser/controller/ResourceController;
    invoke-virtual {v1}, Lmiui/resourcebrowser/AppInnerContext;->getResourceController()Lmiui/resourcebrowser/controller/ResourceController;

    move-result-object v9

    invoke-virtual {v9}, Lmiui/resourcebrowser/controller/ResourceController;->getLocalDataManager()Lmiui/resourcebrowser/controller/LocalDataManager;

    move-result-object v9

    invoke-virtual {v3, v9}, Lmiui/resourcebrowser/controller/ResourceController;->setLocalDataManager(Lmiui/resourcebrowser/controller/LocalDataManager;)V

    .line 42
    invoke-virtual {v1}, Lmiui/resourcebrowser/AppInnerContext;->getResourceController()Lmiui/resourcebrowser/controller/ResourceController;

    move-result-object v9

    invoke-virtual {v9}, Lmiui/resourcebrowser/controller/ResourceController;->getImportManager()Lmiui/resourcebrowser/controller/ImportManager;

    move-result-object v9

    invoke-virtual {v3, v9}, Lmiui/resourcebrowser/controller/ResourceController;->setImportManager(Lmiui/resourcebrowser/controller/ImportManager;)V

    .line 43
    invoke-virtual {v1, v3}, Lmiui/resourcebrowser/AppInnerContext;->setResourceController(Lmiui/resourcebrowser/controller/ResourceController;)V

    .line 45
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceSearchListActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 46
    .local v0, actionBar:Landroid/app/ActionBar;
    new-instance v8, Landroid/widget/ImageView;

    invoke-direct {v8, p0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 47
    .local v8, searchView:Landroid/widget/ImageView;
    const v9, 0x60201b8

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 48
    const/16 v9, 0x12

    invoke-virtual {v8, v10, v10, v9, v10}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 49
    new-instance v9, Lmiui/resourcebrowser/activity/ResourceSearchListActivity$1;

    invoke-direct {v9, p0}, Lmiui/resourcebrowser/activity/ResourceSearchListActivity$1;-><init>(Lmiui/resourcebrowser/activity/ResourceSearchListActivity;)V

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    invoke-virtual {v0, v11}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 56
    new-instance v7, Landroid/app/ActionBar$LayoutParams;

    const/4 v9, 0x5

    invoke-direct {v7, v9}, Landroid/app/ActionBar$LayoutParams;-><init>(I)V

    .line 57
    .local v7, params:Landroid/app/ActionBar$LayoutParams;
    invoke-virtual {v0, v8, v7}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    .line 59
    invoke-virtual {v0, v11}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 61
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceSearchListActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    .line 62
    .local v4, fragmentManager:Landroid/app/FragmentManager;
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v5

    .line 63
    .local v5, fragmentTransaction:Landroid/app/FragmentTransaction;
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceSearchListActivity;->getFragment()Lmiui/resourcebrowser/activity/BaseFragment;

    move-result-object v9

    iput-object v9, p0, Lmiui/resourcebrowser/activity/ResourceSearchListActivity;->mFragment:Lmiui/resourcebrowser/activity/BaseFragment;

    .line 64
    const v9, 0x60b009c

    iget-object v10, p0, Lmiui/resourcebrowser/activity/ResourceSearchListActivity;->mFragment:Lmiui/resourcebrowser/activity/BaseFragment;

    invoke-virtual {v5, v9, v10}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 65
    invoke-virtual {v5}, Landroid/app/FragmentTransaction;->commit()I

    .line 66
    iget-object v9, p0, Lmiui/resourcebrowser/activity/ResourceSearchListActivity;->mFragment:Lmiui/resourcebrowser/activity/BaseFragment;

    invoke-virtual {v9, v11}, Lmiui/resourcebrowser/activity/BaseFragment;->setVisibleForUser(Z)V

    .line 67
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .parameter "item"

    .prologue
    .line 71
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 72
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceSearchListActivity;->finish()V

    .line 74
    :cond_0
    invoke-super {p0, p1}, Lmiui/app/ObservableActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onSearchRequested()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 79
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 80
    .local v0, appData:Landroid/os/Bundle;
    const-string v1, "REQUEST_RES_CONTEXT"

    iget-object v2, p0, Lmiui/resourcebrowser/activity/ResourceSearchListActivity;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 81
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v3, v0, v3}, Lmiui/resourcebrowser/activity/ResourceSearchListActivity;->startSearch(Ljava/lang/String;ZLandroid/os/Bundle;Z)V

    .line 82
    const/4 v1, 0x1

    return v1
.end method
