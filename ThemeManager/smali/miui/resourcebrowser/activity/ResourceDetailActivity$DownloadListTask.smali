.class public Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadListTask;
.super Lmiui/os/UniqueAsyncTask;
.source "ResourceDetailActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/resourcebrowser/activity/ResourceDetailActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "DownloadListTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmiui/os/UniqueAsyncTask",
        "<",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Lmiui/resourcebrowser/model/Resource;",
        ">;>;"
    }
.end annotation


# instance fields
.field private offset:I

.field final synthetic this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;


# direct methods
.method protected constructor <init>(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)V
    .locals 1
    .parameter

    .prologue
    .line 393
    iput-object p1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadListTask;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    invoke-direct {p0}, Lmiui/os/UniqueAsyncTask;-><init>()V

    .line 395
    const/4 v0, 0x0

    iput v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadListTask;->offset:I

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"

    .prologue
    .line 393
    check-cast p1, [Ljava/lang/Integer;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadListTask;->doInBackground([Ljava/lang/Integer;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Integer;)Ljava/util/List;
    .locals 3
    .parameter "params"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Integer;",
            ")",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 445
    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadListTask;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    iget-object v1, v1, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v1}, Lmiui/resourcebrowser/ResourceContext;->getPageItemCount()I

    move-result v0

    .line 446
    .local v0, count:I
    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadListTask;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    iget-object v1, v1, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResController:Lmiui/resourcebrowser/controller/ResourceController;

    invoke-virtual {v1}, Lmiui/resourcebrowser/controller/ResourceController;->getOnlineDataManager()Lmiui/resourcebrowser/controller/OnlineDataManager;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v2, p1, v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/2addr v2, v0

    add-int/lit8 v2, v2, -0x1

    div-int/2addr v2, v0

    invoke-virtual {v1, v2}, Lmiui/resourcebrowser/controller/OnlineDataManager;->getResources(I)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method protected hasEquivalentRunningTasks()Z
    .locals 1

    .prologue
    .line 413
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadListTask;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    #getter for: Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mTaskSet:Ljava/util/Set;
    invoke-static {v0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->access$100(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 393
    check-cast p1, Ljava/util/List;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadListTask;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, result:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/Resource;>;"
    const/4 v1, 0x1

    .line 418
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadListTask;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    invoke-virtual {v0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 441
    :goto_0
    return-void

    .line 422
    :cond_0
    if-nez p1, :cond_1

    .line 423
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadListTask;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    iput-boolean v1, v0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mReachBottom:Z

    .line 424
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadListTask;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    const v1, 0x60c0020

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 440
    :goto_1
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadListTask;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    #getter for: Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mTaskSet:Ljava/util/Set;
    invoke-static {v0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->access$100(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 426
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 427
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadListTask;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    iput-boolean v1, v0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mReachBottom:Z

    goto :goto_1

    .line 429
    :cond_2
    iget v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadListTask;->offset:I

    if-nez v0, :cond_4

    .line 430
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadListTask;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    iget-object v0, v0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mGroupDataSet:Lmiui/resourcebrowser/widget/DataGroup;

    invoke-virtual {v0}, Lmiui/resourcebrowser/widget/DataGroup;->clear()V

    .line 431
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadListTask;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    iget-object v0, v0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mGroupDataSet:Lmiui/resourcebrowser/widget/DataGroup;

    invoke-virtual {v0, p1}, Lmiui/resourcebrowser/widget/DataGroup;->addAll(Ljava/util/Collection;)Z

    .line 437
    :cond_3
    :goto_2
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadListTask;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    invoke-virtual {v0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->updateNavigationState()V

    goto :goto_1

    .line 433
    :cond_4
    iget v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadListTask;->offset:I

    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadListTask;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    iget-object v1, v1, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mGroupDataSet:Lmiui/resourcebrowser/widget/DataGroup;

    invoke-virtual {v1}, Lmiui/resourcebrowser/widget/DataGroup;->size()I

    move-result v1

    if-ne v0, v1, :cond_3

    .line 434
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadListTask;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    iget-object v0, v0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mGroupDataSet:Lmiui/resourcebrowser/widget/DataGroup;

    invoke-virtual {v0, p1}, Lmiui/resourcebrowser/widget/DataGroup;->addAll(Ljava/util/Collection;)Z

    goto :goto_2
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 407
    invoke-super {p0}, Lmiui/os/UniqueAsyncTask;->onPreExecute()V

    .line 408
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadListTask;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    #getter for: Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mTaskSet:Ljava/util/Set;
    invoke-static {v0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->access$100(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 409
    return-void
.end method

.method public setOffset(I)V
    .locals 0
    .parameter "offset"

    .prologue
    .line 402
    iput p1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadListTask;->offset:I

    .line 403
    return-void
.end method
