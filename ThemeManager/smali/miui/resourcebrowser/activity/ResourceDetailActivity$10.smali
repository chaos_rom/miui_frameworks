.class Lmiui/resourcebrowser/activity/ResourceDetailActivity$10;
.super Ljava/lang/Object;
.source "ResourceDetailActivity.java"

# interfaces
.implements Lmiui/resourcebrowser/util/BatchMediaPlayer$BatchPlayerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiui/resourcebrowser/activity/ResourceDetailActivity;->initPlayer()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;


# direct methods
.method constructor <init>(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 919
    iput-object p1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$10;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public finish(Z)V
    .locals 3
    .parameter "hasError"

    .prologue
    .line 929
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$10;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    #getter for: Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPlayButton:Landroid/widget/ImageView;
    invoke-static {v0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->access$1500(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x60200d5

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 930
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$10;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    #getter for: Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mRingtoneName:Landroid/widget/TextView;
    invoke-static {v0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->access$1400(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 931
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$10;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    #getter for: Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mRingtoneName:Landroid/widget/TextView;
    invoke-static {v0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->access$1400(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$10;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    invoke-virtual {v1}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->getFormatTitleBeforePlayingRingtone()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 933
    :cond_0
    if-eqz p1, :cond_1

    .line 934
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$10;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    const v1, 0x60c0020

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 936
    :cond_1
    return-void
.end method

.method public play(Ljava/lang/String;II)V
    .locals 2
    .parameter "ringtonePath"
    .parameter "current"
    .parameter "total"

    .prologue
    .line 922
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$10;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    #getter for: Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mRingtoneName:Landroid/widget/TextView;
    invoke-static {v0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->access$1400(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 923
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$10;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    #getter for: Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mRingtoneName:Landroid/widget/TextView;
    invoke-static {v0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->access$1400(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$10;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    invoke-virtual {v1, p1, p2, p3}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->getFormatPlayingRingtoneName(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 925
    :cond_0
    return-void
.end method
