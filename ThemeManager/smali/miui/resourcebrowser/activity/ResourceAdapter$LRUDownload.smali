.class Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;
.super Ljava/lang/Object;
.source "ResourceAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/resourcebrowser/activity/ResourceAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LRUDownload"
.end annotation


# instance fields
.field private final MAX_NUMBER_OF_DOWNLOAD_TASK:I

.field private mCurrentParallelTaskNumber:I

.field private mDownloadingJobsSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lmiui/resourcebrowser/model/PathEntry;",
            ">;"
        }
    .end annotation
.end field

.field private mFinishJobsQueue:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lmiui/resourcebrowser/model/PathEntry;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mWaitingJobsQueue:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lmiui/resourcebrowser/model/PathEntry;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lmiui/resourcebrowser/activity/ResourceAdapter;


# direct methods
.method private constructor <init>(Lmiui/resourcebrowser/activity/ResourceAdapter;)V
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 489
    iput-object p1, p0, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;->this$0:Lmiui/resourcebrowser/activity/ResourceAdapter;

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 491
    sget-boolean v0, Lmiui/resourcebrowser/util/ResourceDebug;->DEBUG:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lmiui/resourcebrowser/util/ResourceDebug;->getMaxThumbnailDownloadTaskNumber()I

    move-result v0

    :goto_0
    iput v0, p0, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;->MAX_NUMBER_OF_DOWNLOAD_TASK:I

    .line 494
    new-instance v0, Ljava/util/LinkedHashMap;

    const/high16 v1, 0x3f40

    const/4 v2, 0x1

    invoke-direct {v0, v3, v1, v2}, Ljava/util/LinkedHashMap;-><init>(IFZ)V

    iput-object v0, p0, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;->mWaitingJobsQueue:Ljava/util/Map;

    .line 495
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;->mDownloadingJobsSet:Ljava/util/Set;

    .line 496
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;->mFinishJobsQueue:Ljava/util/Map;

    .line 498
    iput v3, p0, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;->mCurrentParallelTaskNumber:I

    return-void

    .line 491
    :cond_0
    const/4 v0, 0x3

    goto :goto_0
.end method

.method synthetic constructor <init>(Lmiui/resourcebrowser/activity/ResourceAdapter;Lmiui/resourcebrowser/activity/ResourceAdapter$1;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 489
    invoke-direct {p0, p1}, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;-><init>(Lmiui/resourcebrowser/activity/ResourceAdapter;)V

    return-void
.end method

.method static synthetic access$100(Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;)Ljava/util/Set;
    .locals 1
    .parameter "x0"

    .prologue
    .line 489
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;->mDownloadingJobsSet:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$200(Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;)Ljava/util/Map;
    .locals 1
    .parameter "x0"

    .prologue
    .line 489
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;->mFinishJobsQueue:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$300(Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;)I
    .locals 1
    .parameter "x0"

    .prologue
    .line 489
    iget v0, p0, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;->mCurrentParallelTaskNumber:I

    return v0
.end method

.method static synthetic access$306(Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;)I
    .locals 1
    .parameter "x0"

    .prologue
    .line 489
    iget v0, p0, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;->mCurrentParallelTaskNumber:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;->mCurrentParallelTaskNumber:I

    return v0
.end method

.method static synthetic access$400(Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;)Ljava/util/Map;
    .locals 1
    .parameter "x0"

    .prologue
    .line 489
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;->mWaitingJobsQueue:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$500(Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 489
    invoke-direct {p0}, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;->startOneDownloadTask()V

    return-void
.end method

.method private getNextDownloadEntry()Lmiui/resourcebrowser/model/PathEntry;
    .locals 3

    .prologue
    .line 512
    const/4 v0, 0x0

    .line 513
    .local v0, entry:Lmiui/resourcebrowser/model/PathEntry;
    iget-object v2, p0, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;->mWaitingJobsQueue:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 514
    .local v1, iterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Lmiui/resourcebrowser/model/PathEntry;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 515
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .end local v0           #entry:Lmiui/resourcebrowser/model/PathEntry;
    check-cast v0, Lmiui/resourcebrowser/model/PathEntry;

    .restart local v0       #entry:Lmiui/resourcebrowser/model/PathEntry;
    goto :goto_0

    .line 517
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 518
    return-object v0
.end method

.method private startOneDownloadTask()V
    .locals 6

    .prologue
    .line 522
    iget-object v2, p0, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;->mWaitingJobsQueue:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    iget v2, p0, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;->mCurrentParallelTaskNumber:I

    iget v3, p0, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;->MAX_NUMBER_OF_DOWNLOAD_TASK:I

    if-ge v2, v3, :cond_0

    .line 524
    invoke-direct {p0}, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;->getNextDownloadEntry()Lmiui/resourcebrowser/model/PathEntry;

    move-result-object v0

    .line 525
    .local v0, downloadEntry:Lmiui/resourcebrowser/model/PathEntry;
    if-nez v0, :cond_1

    .line 578
    .end local v0           #downloadEntry:Lmiui/resourcebrowser/model/PathEntry;
    :cond_0
    :goto_0
    return-void

    .line 528
    .restart local v0       #downloadEntry:Lmiui/resourcebrowser/model/PathEntry;
    :cond_1
    iget-object v2, p0, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;->mFinishJobsQueue:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 529
    .local v1, lastDownloadFinishTime:Ljava/lang/Long;
    if-eqz v1, :cond_2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x7530

    cmp-long v2, v2, v4

    if-gez v2, :cond_2

    .line 531
    sget-boolean v2, Lmiui/resourcebrowser/util/ResourceDebug;->DEBUG:Z

    if-eqz v2, :cond_0

    .line 532
    const-string v2, "Theme"

    const-string v3, "Interval of thumbnail downloading is too short: cancelled"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 537
    :cond_2
    iget-object v2, p0, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;->mDownloadingJobsSet:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 538
    iget v2, p0, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;->mCurrentParallelTaskNumber:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;->mCurrentParallelTaskNumber:I

    .line 540
    new-instance v2, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload$1;

    invoke-direct {v2, p0, v0}, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload$1;-><init>(Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;Lmiui/resourcebrowser/model/PathEntry;)V

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-virtual {v2, v3}, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 572
    sget-boolean v2, Lmiui/resourcebrowser/util/ResourceDebug;->DEBUG:Z

    if-eqz v2, :cond_0

    .line 573
    const-string v2, "Theme"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Start one thumbnail downloading task: RemainTaskNumber="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;->mWaitingJobsQueue:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ExecutingThreadNumber="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;->mCurrentParallelTaskNumber:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public clean()V
    .locals 1

    .prologue
    .line 508
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;->mWaitingJobsQueue:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 509
    return-void
.end method

.method public submitDownloadJob(Lmiui/resourcebrowser/model/PathEntry;)V
    .locals 2
    .parameter "downloadEntry"

    .prologue
    .line 501
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;->mDownloadingJobsSet:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 502
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;->mWaitingJobsQueue:Ljava/util/Map;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 503
    invoke-direct {p0}, Lmiui/resourcebrowser/activity/ResourceAdapter$LRUDownload;->startOneDownloadTask()V

    .line 505
    :cond_0
    return-void
.end method
