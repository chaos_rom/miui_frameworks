.class public Lmiui/resourcebrowser/activity/ResourceDetailActivity;
.super Lmiui/app/ObservableActivity;
.source "ResourceDetailActivity.java"

# interfaces
.implements Lmiui/app/SDCardMonitor$SDCardStatusListener;
.implements Lmiui/resourcebrowser/view/ResourceOperationView$ResourceOperationListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadDetailTask;,
        Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadListTask;
    }
.end annotation


# instance fields
.field private final PREVIEW_INDEX:Ljava/lang/String;

.field protected mActionBar:Landroid/app/ActionBar;

.field protected mAppContext:Lmiui/resourcebrowser/AppInnerContext;

.field private mBatchPlayer:Lmiui/resourcebrowser/util/BatchMediaPlayer;

.field protected mCoverView:Landroid/widget/ImageView;

.field protected mDataSet:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/widget/DataGroup",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;>;"
        }
    .end annotation
.end field

.field private mFullScreen:Z

.field private mFullScreenParams:Landroid/view/ViewGroup$LayoutParams;

.field protected mGroupDataSet:Lmiui/resourcebrowser/widget/DataGroup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lmiui/resourcebrowser/widget/DataGroup",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;"
        }
    .end annotation
.end field

.field protected mHasInfoView:Z

.field private mImageCacheDecoder:Lmiui/resourcebrowser/util/ImageCacheDecoder;

.field private mImageParams:Landroid/view/ViewGroup$LayoutParams;

.field protected mInfo:Landroid/widget/FrameLayout;

.field protected mIsOnlineResourceSet:Z

.field protected mIsSingleResourceSet:Z

.field protected mNextItem:Landroid/view/View;

.field private mNormalParams:Landroid/view/ViewGroup$LayoutParams;

.field protected mOperationHandler:Lmiui/resourcebrowser/view/ResourceOperationHandler;

.field protected mOperationView:Lmiui/resourcebrowser/view/ResourceOperationView;

.field private mPlayButton:Landroid/widget/ImageView;

.field private mPreview:Lmiui/resourcebrowser/view/ResourceScreenView;

.field private mPreviewOffset:I

.field private mPreviewWidth:I

.field private mPreviews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/PathEntry;",
            ">;"
        }
    .end annotation
.end field

.field protected mPreviousItem:Landroid/view/View;

.field protected mReachBottom:Z

.field protected mResContext:Lmiui/resourcebrowser/ResourceContext;

.field protected mResController:Lmiui/resourcebrowser/controller/ResourceController;

.field protected mResource:Lmiui/resourcebrowser/model/Resource;

.field protected mResourceGroup:I

.field protected mResourceIndex:I

.field private mRingtoneName:Landroid/widget/TextView;

.field private mSDCardMonitor:Lmiui/app/SDCardMonitor;

.field private mScreenViewBackgroudId:I

.field private mScreenViewNeedBackgroud:Z

.field protected mSourceType:I

.field private mTaskSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lmiui/os/UniqueAsyncTask",
            "<***>;>;"
        }
    .end annotation
.end field

.field protected mToFullScreenModeAnimator:Landroid/animation/ObjectAnimator;

.field protected mToNormalModeAnimator:Landroid/animation/ObjectAnimator;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 66
    invoke-direct {p0}, Lmiui/app/ObservableActivity;-><init>()V

    .line 69
    const-string v0, "PREVIEW_INDEX"

    iput-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->PREVIEW_INDEX:Ljava/lang/String;

    .line 98
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreviews:Ljava/util/List;

    .line 113
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mFullScreenParams:Landroid/view/ViewGroup$LayoutParams;

    .line 114
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mImageParams:Landroid/view/ViewGroup$LayoutParams;

    .line 121
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mTaskSet:Ljava/util/Set;

    .line 458
    return-void
.end method

.method static synthetic access$000(Lmiui/resourcebrowser/activity/ResourceDetailActivity;I)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->initImageForScreenView(I)V

    return-void
.end method

.method static synthetic access$100(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)Ljava/util/Set;
    .locals 1
    .parameter "x0"

    .prologue
    .line 66
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mTaskSet:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$1000(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 66
    invoke-direct {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->onBeginEnterFullScreenMode()V

    return-void
.end method

.method static synthetic access$1100(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 66
    invoke-direct {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->onEndEnterFullScreenMode()V

    return-void
.end method

.method static synthetic access$1200(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 66
    invoke-direct {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->enterFullScreenMode()V

    return-void
.end method

.method static synthetic access$1300(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 66
    invoke-direct {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->enterNormalMode()V

    return-void
.end method

.method static synthetic access$1400(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)Landroid/widget/TextView;
    .locals 1
    .parameter "x0"

    .prologue
    .line 66
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mRingtoneName:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1500(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)Landroid/widget/ImageView;
    .locals 1
    .parameter "x0"

    .prologue
    .line 66
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPlayButton:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1600(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)Lmiui/resourcebrowser/util/BatchMediaPlayer;
    .locals 1
    .parameter "x0"

    .prologue
    .line 66
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mBatchPlayer:Lmiui/resourcebrowser/util/BatchMediaPlayer;

    return-object v0
.end method

.method static synthetic access$1700(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 66
    invoke-direct {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->initPlayer()V

    return-void
.end method

.method static synthetic access$200(Lmiui/resourcebrowser/activity/ResourceDetailActivity;Ljava/lang/String;)I
    .locals 1
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->getScreenViewIndexForPreviewImage(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lmiui/resourcebrowser/activity/ResourceDetailActivity;I)Z
    .locals 1
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->isVisibleScreen(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)Lmiui/resourcebrowser/util/ImageCacheDecoder;
    .locals 1
    .parameter "x0"

    .prologue
    .line 66
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mImageCacheDecoder:Lmiui/resourcebrowser/util/ImageCacheDecoder;

    return-object v0
.end method

.method static synthetic access$500(Lmiui/resourcebrowser/activity/ResourceDetailActivity;I)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->decodeImageForScreenView(I)V

    return-void
.end method

.method static synthetic access$600(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)Lmiui/resourcebrowser/view/ResourceScreenView;
    .locals 1
    .parameter "x0"

    .prologue
    .line 66
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreview:Lmiui/resourcebrowser/view/ResourceScreenView;

    return-object v0
.end method

.method static synthetic access$700(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)Ljava/util/List;
    .locals 1
    .parameter "x0"

    .prologue
    .line 66
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreviews:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$800(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 66
    invoke-direct {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->onBeginEnterNormalMode()V

    return-void
.end method

.method static synthetic access$900(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 66
    invoke-direct {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->onEndEnterNormalMode()V

    return-void
.end method

.method private addImageView(Landroid/widget/ImageView;)V
    .locals 3
    .parameter "imageView"

    .prologue
    .line 663
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 664
    .local v0, imageContainer:Landroid/widget/FrameLayout;
    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 665
    invoke-direct {p0, v0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->enterNormalMode(Landroid/view/View;)V

    .line 666
    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreview:Lmiui/resourcebrowser/view/ResourceScreenView;

    iget-object v2, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mNormalParams:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v1, v0, v2}, Lmiui/resourcebrowser/view/ResourceScreenView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 667
    return-void
.end method

.method private bindScreenImageView()V
    .locals 10

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 601
    invoke-direct {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->initImageDecoder()V

    .line 602
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreviews:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 604
    iget-boolean v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mHasInfoView:Z

    if-eqz v0, :cond_0

    .line 605
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreview:Lmiui/resourcebrowser/view/ResourceScreenView;

    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mInfo:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1, v3}, Lmiui/resourcebrowser/view/ResourceScreenView;->addView(Landroid/view/View;I)V

    .line 606
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreviews:Ljava/util/List;

    invoke-interface {v0, v3, v5}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 608
    iget v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreviewOffset:I

    if-nez v0, :cond_0

    .line 609
    const/4 v0, 0x1

    iput v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreviewOffset:I

    .line 613
    :cond_0
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v0}, Lmiui/resourcebrowser/model/Resource;->getPreviews()Ljava/util/List;

    move-result-object v0

    .line 614
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_2

    .line 615
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v0}, Lmiui/resourcebrowser/model/Resource;->getBuildInPreviews()Ljava/util/List;

    move-result-object v0

    .line 616
    if-nez v0, :cond_1

    .line 660
    :goto_0
    return-void

    .line 619
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 620
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 621
    new-instance v4, Lmiui/resourcebrowser/model/PathEntry;

    invoke-direct {v4, v0, v5}, Lmiui/resourcebrowser/model/PathEntry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    move-object v1, v0

    .line 625
    :cond_3
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v0}, Lmiui/resourcebrowser/model/Resource;->getUpdatedTime()J

    move-result-wide v4

    .line 626
    const/16 v0, 0xf

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v6

    move v2, v3

    .line 627
    :goto_2
    if-ge v2, v6, :cond_6

    .line 628
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 629
    sget-object v7, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 630
    const v7, 0x60200c3

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 631
    iget-object v7, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mImageParams:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 632
    invoke-direct {p0, v0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->addImageView(Landroid/widget/ImageView;)V

    .line 635
    iget-boolean v7, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mScreenViewNeedBackgroud:Z

    if-eqz v7, :cond_4

    .line 636
    iget v7, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mScreenViewBackgroudId:I

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 637
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x60a0029

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v7, v7

    .line 638
    invoke-virtual {v0, v7, v7, v7, v7}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 641
    :cond_4
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/resourcebrowser/model/PathEntry;

    .line 642
    new-instance v7, Ljava/io/File;

    invoke-virtual {v0}, Lmiui/resourcebrowser/model/PathEntry;->getLocalPath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 643
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {v7}, Ljava/io/File;->lastModified()J

    move-result-wide v8

    cmp-long v0, v8, v4

    if-gez v0, :cond_5

    .line 644
    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    .line 627
    :cond_5
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 647
    :cond_6
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreviews:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 649
    if-nez v6, :cond_7

    .line 650
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 651
    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 652
    const v1, 0x60201ba

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 653
    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mImageParams:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 654
    invoke-direct {p0, v0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->addImageView(Landroid/widget/ImageView;)V

    .line 656
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setClickable(Z)V

    goto/16 :goto_0

    .line 658
    :cond_7
    iget v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreviewOffset:I

    invoke-direct {p0, v0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->initImageForScreenView(I)V

    goto/16 :goto_0
.end method

.method private buildModeChangeAnimator()V
    .locals 11

    .prologue
    const-wide/16 v9, 0xc8

    const/4 v8, 0x3

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x2

    .line 750
    const-string v0, "scaleX"

    new-array v1, v5, [F

    fill-array-data v1, :array_0

    invoke-static {v0, v1}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    .line 751
    const-string v1, "scaleY"

    new-array v2, v5, [F

    fill-array-data v2, :array_1

    invoke-static {v1, v2}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    .line 752
    const-string v2, "alpha"

    new-array v3, v5, [F

    fill-array-data v3, :array_2

    invoke-static {v2, v3}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v2

    .line 753
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mCoverView:Landroid/widget/ImageView;

    new-array v4, v8, [Landroid/animation/PropertyValuesHolder;

    aput-object v0, v4, v6

    aput-object v1, v4, v7

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0, v9, v10}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mToNormalModeAnimator:Landroid/animation/ObjectAnimator;

    .line 755
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mToNormalModeAnimator:Landroid/animation/ObjectAnimator;

    new-instance v1, Lmiui/resourcebrowser/activity/ResourceDetailActivity$6;

    invoke-direct {v1, p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity$6;-><init>(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 779
    const-string v0, "scaleX"

    new-array v1, v5, [F

    fill-array-data v1, :array_3

    invoke-static {v0, v1}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    .line 780
    const-string v1, "scaleY"

    new-array v2, v5, [F

    fill-array-data v2, :array_4

    invoke-static {v1, v2}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    .line 781
    const-string v2, "alpha"

    new-array v3, v5, [F

    fill-array-data v3, :array_5

    invoke-static {v2, v3}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v2

    .line 782
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mCoverView:Landroid/widget/ImageView;

    new-array v4, v8, [Landroid/animation/PropertyValuesHolder;

    aput-object v0, v4, v6

    aput-object v1, v4, v7

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0, v9, v10}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mToFullScreenModeAnimator:Landroid/animation/ObjectAnimator;

    .line 784
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mToFullScreenModeAnimator:Landroid/animation/ObjectAnimator;

    new-instance v1, Lmiui/resourcebrowser/activity/ResourceDetailActivity$7;

    invoke-direct {v1, p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity$7;-><init>(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 807
    return-void

    .line 750
    nop

    :array_0
    .array-data 0x4
        0x0t 0x0t 0x80t 0x3ft
        0x33t 0x33t 0x33t 0x3ft
    .end array-data

    .line 751
    :array_1
    .array-data 0x4
        0x0t 0x0t 0x80t 0x3ft
        0x33t 0x33t 0x33t 0x3ft
    .end array-data

    .line 752
    :array_2
    .array-data 0x4
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
    .end array-data

    .line 779
    :array_3
    .array-data 0x4
        0x33t 0x33t 0x33t 0x3ft
        0x0t 0x0t 0x80t 0x3ft
    .end array-data

    .line 780
    :array_4
    .array-data 0x4
        0x33t 0x33t 0x33t 0x3ft
        0x0t 0x0t 0x80t 0x3ft
    .end array-data

    .line 781
    :array_5
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
    .end array-data
.end method

.method private decodeImageForScreenView(I)V
    .locals 7
    .parameter "screenViewIndex"

    .prologue
    .line 727
    if-ltz p1, :cond_0

    iget-object v5, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreviews:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge p1, v5, :cond_0

    iget-object v5, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreviews:Ljava/util/List;

    invoke-interface {v5, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    if-nez v5, :cond_1

    .line 744
    :cond_0
    :goto_0
    return-void

    .line 733
    :cond_1
    iget-object v5, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreviews:Ljava/util/List;

    invoke-interface {v5, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lmiui/resourcebrowser/model/PathEntry;

    .line 734
    .local v4, pInfo:Lmiui/resourcebrowser/model/PathEntry;
    invoke-virtual {v4}, Lmiui/resourcebrowser/model/PathEntry;->getLocalPath()Ljava/lang/String;

    move-result-object v2

    .line 735
    .local v2, localPath:Ljava/lang/String;
    invoke-virtual {v4}, Lmiui/resourcebrowser/model/PathEntry;->getOnlinePath()Ljava/lang/String;

    move-result-object v3

    .line 737
    .local v3, onlinePath:Ljava/lang/String;
    iget-object v5, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mImageCacheDecoder:Lmiui/resourcebrowser/util/ImageCacheDecoder;

    invoke-virtual {v5, v2}, Lmiui/resourcebrowser/util/ImageCacheDecoder;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 738
    .local v0, bitmap:Landroid/graphics/Bitmap;
    if-eqz v0, :cond_2

    .line 739
    iget-object v5, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreview:Lmiui/resourcebrowser/view/ResourceScreenView;

    invoke-virtual {v5, p1}, Lmiui/resourcebrowser/view/ResourceScreenView;->getScreen(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 740
    .local v1, imageView:Landroid/widget/ImageView;
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 742
    .end local v1           #imageView:Landroid/widget/ImageView;
    :cond_2
    iget-object v5, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mImageCacheDecoder:Lmiui/resourcebrowser/util/ImageCacheDecoder;

    invoke-virtual {v5, v2, v3, p1}, Lmiui/resourcebrowser/util/ImageCacheDecoder;->decodeImageAsync(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method private enterFullScreenMode()V
    .locals 1

    .prologue
    .line 897
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreview:Lmiui/resourcebrowser/view/ResourceScreenView;

    invoke-virtual {v0}, Lmiui/resourcebrowser/view/ResourceScreenView;->getCurrentScreenIndex()I

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mHasInfoView:Z

    if-nez v0, :cond_1

    .line 898
    :cond_0
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mToFullScreenModeAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 900
    :cond_1
    return-void
.end method

.method private enterFullScreenMode(Landroid/view/View;)V
    .locals 3
    .parameter

    .prologue
    const/high16 v2, -0x100

    const/4 v1, 0x0

    .line 903
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mFullScreenParams:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 904
    invoke-virtual {p1, v1, v1, v1, v1}, Landroid/view/View;->setPadding(IIII)V

    .line 905
    invoke-virtual {p1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 906
    check-cast p1, Landroid/view/ViewGroup;

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 907
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 908
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAdjustViewBounds(Z)V

    .line 909
    new-instance v1, Lmiui/resourcebrowser/activity/ResourceDetailActivity$9;

    invoke-direct {v1, p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity$9;-><init>(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 915
    return-void
.end method

.method private enterNormalMode()V
    .locals 1

    .prologue
    .line 844
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mToNormalModeAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 845
    return-void
.end method

.method private enterNormalMode(Landroid/view/View;)V
    .locals 4
    .parameter "imageContainer"

    .prologue
    const/4 v3, 0x6

    const/4 v2, 0x0

    .line 848
    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mNormalParams:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p1, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 849
    const/16 v1, 0x3c

    invoke-virtual {p1, v3, v2, v3, v1}, Landroid/view/View;->setPadding(IIII)V

    .line 850
    invoke-virtual {p1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 851
    check-cast p1, Landroid/view/ViewGroup;

    .end local p1
    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 852
    .local v0, imageView:Landroid/widget/ImageView;
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 853
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAdjustViewBounds(Z)V

    .line 854
    new-instance v1, Lmiui/resourcebrowser/activity/ResourceDetailActivity$8;

    invoke-direct {v1, p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity$8;-><init>(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 860
    return-void
.end method

.method private getScreenViewIndexForPreviewImage(Ljava/lang/String;)I
    .locals 3
    .parameter "imageLocalPath"

    .prologue
    .line 710
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    iget-object v2, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreviews:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 711
    iget-object v2, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreviews:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/resourcebrowser/model/PathEntry;

    .line 712
    .local v1, pInfo:Lmiui/resourcebrowser/model/PathEntry;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lmiui/resourcebrowser/model/PathEntry;->getLocalPath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 716
    .end local v0           #i:I
    .end local v1           #pInfo:Lmiui/resourcebrowser/model/PathEntry;
    :goto_1
    return v0

    .line 710
    .restart local v0       #i:I
    .restart local v1       #pInfo:Lmiui/resourcebrowser/model/PathEntry;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 716
    .end local v1           #pInfo:Lmiui/resourcebrowser/model/PathEntry;
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private initImageDecoder()V
    .locals 2

    .prologue
    .line 670
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mImageCacheDecoder:Lmiui/resourcebrowser/util/ImageCacheDecoder;

    if-eqz v0, :cond_0

    .line 671
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mImageCacheDecoder:Lmiui/resourcebrowser/util/ImageCacheDecoder;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/util/ImageCacheDecoder;->clean(Z)V

    .line 673
    :cond_0
    new-instance v0, Lmiui/resourcebrowser/util/ImageCacheDecoder;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Lmiui/resourcebrowser/util/ImageCacheDecoder;-><init>(I)V

    iput-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mImageCacheDecoder:Lmiui/resourcebrowser/util/ImageCacheDecoder;

    .line 674
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mImageCacheDecoder:Lmiui/resourcebrowser/util/ImageCacheDecoder;

    new-instance v1, Lmiui/resourcebrowser/activity/ResourceDetailActivity$5;

    invoke-direct {v1, p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity$5;-><init>(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)V

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/util/ImageCacheDecoder;->registerListener(Lmiui/resourcebrowser/util/ImageCacheDecoder$ImageDecodingListener;)V

    .line 699
    return-void
.end method

.method private initImageForScreenView(I)V
    .locals 1
    .parameter "currentScreen"

    .prologue
    .line 720
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mImageCacheDecoder:Lmiui/resourcebrowser/util/ImageCacheDecoder;

    invoke-virtual {v0, p1}, Lmiui/resourcebrowser/util/ImageCacheDecoder;->setCurrentUseBitmapIndex(I)V

    .line 721
    add-int/lit8 v0, p1, 0x0

    invoke-direct {p0, v0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->decodeImageForScreenView(I)V

    .line 722
    add-int/lit8 v0, p1, 0x1

    invoke-direct {p0, v0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->decodeImageForScreenView(I)V

    .line 723
    add-int/lit8 v0, p1, -0x1

    invoke-direct {p0, v0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->decodeImageForScreenView(I)V

    .line 724
    return-void
.end method

.method private initPlayer()V
    .locals 2

    .prologue
    .line 918
    new-instance v0, Lmiui/resourcebrowser/util/BatchMediaPlayer;

    invoke-direct {v0, p0}, Lmiui/resourcebrowser/util/BatchMediaPlayer;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mBatchPlayer:Lmiui/resourcebrowser/util/BatchMediaPlayer;

    .line 919
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mBatchPlayer:Lmiui/resourcebrowser/util/BatchMediaPlayer;

    new-instance v1, Lmiui/resourcebrowser/activity/ResourceDetailActivity$10;

    invoke-direct {v1, p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity$10;-><init>(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)V

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/util/BatchMediaPlayer;->setListener(Lmiui/resourcebrowser/util/BatchMediaPlayer$BatchPlayerListener;)V

    .line 938
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mBatchPlayer:Lmiui/resourcebrowser/util/BatchMediaPlayer;

    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {p0, v1}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->getMusicPlayList(Lmiui/resourcebrowser/model/Resource;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/util/BatchMediaPlayer;->setPlayList(Ljava/util/List;)V

    .line 939
    return-void
.end method

.method private isVisibleScreen(I)Z
    .locals 2
    .parameter "screen"

    .prologue
    .line 706
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mImageCacheDecoder:Lmiui/resourcebrowser/util/ImageCacheDecoder;

    invoke-virtual {v0}, Lmiui/resourcebrowser/util/ImageCacheDecoder;->getCurrentUseBitmapIndex()I

    move-result v0

    sub-int v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onBeginEnterFullScreenMode()V
    .locals 2

    .prologue
    .line 863
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 864
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    .line 865
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreview:Lmiui/resourcebrowser/view/ResourceScreenView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/view/ResourceScreenView;->setClickable(Z)V

    .line 866
    return-void
.end method

.method private onBeginEnterNormalMode()V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 810
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    const/16 v5, 0x400

    invoke-virtual {v3, v5}, Landroid/view/Window;->clearFlags(I)V

    .line 811
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/ActionBar;->show()V

    .line 813
    const/4 v2, 0x0

    .local v2, i:I
    :goto_0
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreview:Lmiui/resourcebrowser/view/ResourceScreenView;

    invoke-virtual {v3}, Lmiui/resourcebrowser/view/ResourceScreenView;->getScreenCount()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 814
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreview:Lmiui/resourcebrowser/view/ResourceScreenView;

    invoke-virtual {v3, v2}, Lmiui/resourcebrowser/view/ResourceScreenView;->getScreen(I)Landroid/view/View;

    move-result-object v3

    invoke-direct {p0, v3}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->enterNormalMode(Landroid/view/View;)V

    .line 813
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 817
    :cond_0
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreview:Lmiui/resourcebrowser/view/ResourceScreenView;

    invoke-virtual {v3}, Lmiui/resourcebrowser/view/ResourceScreenView;->getCurrentScreenIndex()I

    move-result v1

    .line 818
    .local v1, current:I
    iget-boolean v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mHasInfoView:Z

    if-eqz v3, :cond_1

    .line 819
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreview:Lmiui/resourcebrowser/view/ResourceScreenView;

    iget-object v5, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mInfo:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v5, v4}, Lmiui/resourcebrowser/view/ResourceScreenView;->addView(Landroid/view/View;I)V

    .line 820
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreviews:Ljava/util/List;

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 821
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreview:Lmiui/resourcebrowser/view/ResourceScreenView;

    add-int/lit8 v5, v1, 0x1

    invoke-virtual {v3, v5}, Lmiui/resourcebrowser/view/ResourceScreenView;->setCurrentScreen(I)V

    .line 824
    :cond_1
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mActionBar:Landroid/app/ActionBar;

    if-eqz v3, :cond_2

    .line 825
    const v3, 0x60b007e

    invoke-virtual {p0, v3}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 826
    .local v0, childRoot:Landroid/widget/LinearLayout;
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/ActionBar;->getHeight()I

    move-result v3

    invoke-virtual {v0, v4, v3, v4, v4}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 829
    .end local v0           #childRoot:Landroid/widget/LinearLayout;
    :cond_2
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mOperationView:Lmiui/resourcebrowser/view/ResourceOperationView;

    invoke-virtual {v3, v4}, Lmiui/resourcebrowser/view/ResourceOperationView;->setVisibility(I)V

    .line 830
    iget-object v5, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreview:Lmiui/resourcebrowser/view/ResourceScreenView;

    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreview:Lmiui/resourcebrowser/view/ResourceScreenView;

    invoke-virtual {v3}, Lmiui/resourcebrowser/view/ResourceScreenView;->getScreenCount()I

    move-result v3

    const/4 v6, 0x1

    if-le v3, v6, :cond_3

    move v3, v4

    :goto_1
    invoke-virtual {v5, v3}, Lmiui/resourcebrowser/view/ResourceScreenView;->setSeekBarVisibility(I)V

    .line 831
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreview:Lmiui/resourcebrowser/view/ResourceScreenView;

    invoke-virtual {v3, v4}, Lmiui/resourcebrowser/view/ResourceScreenView;->setBackgroundResource(I)V

    .line 832
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreview:Lmiui/resourcebrowser/view/ResourceScreenView;

    invoke-virtual {v3, v4}, Lmiui/resourcebrowser/view/ResourceScreenView;->setClickable(Z)V

    .line 834
    iput-boolean v4, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mFullScreen:Z

    .line 835
    return-void

    .line 830
    :cond_3
    const/16 v3, 0x8

    goto :goto_1
.end method

.method private onEndEnterFullScreenMode()V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 869
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreview:Lmiui/resourcebrowser/view/ResourceScreenView;

    invoke-virtual {v0}, Lmiui/resourcebrowser/view/ResourceScreenView;->getCurrentScreenIndex()I

    move-result v2

    .line 870
    iget-boolean v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mHasInfoView:Z

    if-eqz v0, :cond_0

    .line 871
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreview:Lmiui/resourcebrowser/view/ResourceScreenView;

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/view/ResourceScreenView;->removeScreen(I)V

    .line 872
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreviews:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_0
    move v0, v1

    .line 875
    :goto_0
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreview:Lmiui/resourcebrowser/view/ResourceScreenView;

    invoke-virtual {v3}, Lmiui/resourcebrowser/view/ResourceScreenView;->getScreenCount()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 876
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreview:Lmiui/resourcebrowser/view/ResourceScreenView;

    invoke-virtual {v3, v0}, Lmiui/resourcebrowser/view/ResourceScreenView;->getScreen(I)Landroid/view/View;

    move-result-object v3

    invoke-direct {p0, v3}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->enterFullScreenMode(Landroid/view/View;)V

    .line 875
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 879
    :cond_1
    iget-boolean v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mHasInfoView:Z

    if-eqz v0, :cond_2

    .line 880
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreview:Lmiui/resourcebrowser/view/ResourceScreenView;

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Lmiui/resourcebrowser/view/ResourceScreenView;->setCurrentScreen(I)V

    .line 883
    :cond_2
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mActionBar:Landroid/app/ActionBar;

    if-eqz v0, :cond_3

    .line 884
    const v0, 0x60b007e

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 885
    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 888
    :cond_3
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mOperationView:Lmiui/resourcebrowser/view/ResourceOperationView;

    invoke-virtual {v0, v5}, Lmiui/resourcebrowser/view/ResourceOperationView;->setVisibility(I)V

    .line 889
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreview:Lmiui/resourcebrowser/view/ResourceScreenView;

    invoke-virtual {v0, v5}, Lmiui/resourcebrowser/view/ResourceScreenView;->setSeekBarVisibility(I)V

    .line 890
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreview:Lmiui/resourcebrowser/view/ResourceScreenView;

    const/high16 v1, -0x100

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/view/ResourceScreenView;->setBackgroundColor(I)V

    .line 891
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreview:Lmiui/resourcebrowser/view/ResourceScreenView;

    invoke-virtual {v0}, Lmiui/resourcebrowser/view/ResourceScreenView;->requestFocus()Z

    .line 892
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreview:Lmiui/resourcebrowser/view/ResourceScreenView;

    invoke-virtual {v0, v4}, Lmiui/resourcebrowser/view/ResourceScreenView;->setClickable(Z)V

    .line 893
    iput-boolean v4, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mFullScreen:Z

    .line 894
    return-void
.end method

.method private onEndEnterNormalMode()V
    .locals 2

    .prologue
    .line 838
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreview:Lmiui/resourcebrowser/view/ResourceScreenView;

    invoke-virtual {v0}, Lmiui/resourcebrowser/view/ResourceScreenView;->requestFocus()Z

    .line 840
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreview:Lmiui/resourcebrowser/view/ResourceScreenView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/view/ResourceScreenView;->setClickable(Z)V

    .line 841
    return-void
.end method

.method private stopMusic()V
    .locals 1

    .prologue
    .line 954
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mBatchPlayer:Lmiui/resourcebrowser/util/BatchMediaPlayer;

    if-eqz v0, :cond_0

    .line 955
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mBatchPlayer:Lmiui/resourcebrowser/util/BatchMediaPlayer;

    invoke-virtual {v0}, Lmiui/resourcebrowser/util/BatchMediaPlayer;->stop()V

    .line 956
    const/4 v0, 0x0

    iput-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mBatchPlayer:Lmiui/resourcebrowser/util/BatchMediaPlayer;

    .line 958
    :cond_0
    return-void
.end method


# virtual methods
.method protected bindScreenRingtoneView()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x2

    .line 961
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x603004d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 962
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v2, 0x11

    invoke-direct {v0, v3, v3, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 964
    iget-object v2, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreview:Lmiui/resourcebrowser/view/ResourceScreenView;

    invoke-virtual {v2, v1, v0}, Lmiui/resourcebrowser/view/ResourceScreenView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 966
    const v0, 0x60b0057

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPlayButton:Landroid/widget/ImageView;

    .line 967
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPlayButton:Landroid/widget/ImageView;

    new-instance v2, Lmiui/resourcebrowser/activity/ResourceDetailActivity$11;

    invoke-direct {v2, p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity$11;-><init>(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 983
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v0}, Lmiui/resourcebrowser/model/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 984
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPlayButton:Landroid/widget/ImageView;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 987
    :cond_0
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    const-string v2, "EXTRA_CTX_SHOW_RINGTONE_NAME"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lmiui/resourcebrowser/ResourceContext;->getExtraMeta(Ljava/lang/String;Ljava/io/Serializable;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 988
    const v0, 0x60b0058

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mRingtoneName:Landroid/widget/TextView;

    .line 989
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mRingtoneName:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 990
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mRingtoneName:Landroid/widget/TextView;

    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->getFormatTitleBeforePlayingRingtone()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 992
    :cond_1
    return-void
.end method

.method protected bindScreenView()V
    .locals 3

    .prologue
    .line 587
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreview:Lmiui/resourcebrowser/view/ResourceScreenView;

    invoke-virtual {v0}, Lmiui/resourcebrowser/view/ResourceScreenView;->removeAllScreens()V

    .line 589
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v0}, Lmiui/resourcebrowser/ResourceContext;->getResourceFormat()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 590
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->bindScreenRingtoneView()V

    .line 591
    invoke-direct {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->stopMusic()V

    .line 596
    :goto_0
    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreview:Lmiui/resourcebrowser/view/ResourceScreenView;

    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreview:Lmiui/resourcebrowser/view/ResourceScreenView;

    invoke-virtual {v0}, Lmiui/resourcebrowser/view/ResourceScreenView;->getScreenCount()I

    move-result v0

    const/4 v2, 0x1

    if-le v0, v2, :cond_1

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Lmiui/resourcebrowser/view/ResourceScreenView;->setSeekBarVisibility(I)V

    .line 597
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreview:Lmiui/resourcebrowser/view/ResourceScreenView;

    iget v1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreviewOffset:I

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/view/ResourceScreenView;->setCurrentScreen(I)V

    .line 598
    return-void

    .line 593
    :cond_0
    invoke-direct {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->bindScreenImageView()V

    goto :goto_0

    .line 596
    :cond_1
    const/16 v0, 0x8

    goto :goto_1
.end method

.method protected buildResourceContext(Lmiui/resourcebrowser/ResourceContext;)Lmiui/resourcebrowser/ResourceContext;
    .locals 1
    .parameter "resContext"

    .prologue
    .line 160
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p1, v0, p0}, Lmiui/resourcebrowser/util/ResourceHelper;->buildResourceContext(Lmiui/resourcebrowser/ResourceContext;Landroid/content/Intent;Landroid/content/Context;)Lmiui/resourcebrowser/ResourceContext;

    move-result-object v0

    return-object v0
.end method

.method protected changeCurrentResource()V
    .locals 2

    .prologue
    .line 379
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mGroupDataSet:Lmiui/resourcebrowser/widget/DataGroup;

    iget v1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResourceIndex:I

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/widget/DataGroup;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/resourcebrowser/model/Resource;

    iput-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    .line 380
    iget v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResourceIndex:I

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->requestResourceDetail(I)V

    .line 381
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->setResourceStatus()V

    .line 382
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->setResourceInfo()V

    .line 383
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->bindScreenView()V

    .line 384
    return-void
.end method

.method protected getContentView()I
    .locals 1

    .prologue
    .line 156
    const v0, 0x603001a

    return v0
.end method

.method protected getExternalResource()Lmiui/resourcebrowser/model/Resource;
    .locals 1

    .prologue
    .line 296
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getFormatPlayingRingtoneName(Ljava/lang/String;II)Ljava/lang/String;
    .locals 1
    .parameter "ringtonePath"
    .parameter "current"
    .parameter "total"

    .prologue
    .line 946
    invoke-static {p1, p2, p3}, Lmiui/resourcebrowser/util/ResourceHelper;->getDefaultFormatPlayingRingtoneName(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getFormatTitleBeforePlayingRingtone()Ljava/lang/String;
    .locals 1

    .prologue
    .line 942
    const-string v0, ""

    return-object v0
.end method

.method protected getMusicPlayList(Lmiui/resourcebrowser/model/Resource;)Ljava/util/List;
    .locals 1
    .parameter "resourceItem"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiui/resourcebrowser/model/Resource;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 950
    invoke-static {p0, p1}, Lmiui/resourcebrowser/util/ResourceHelper;->getDefaultMusicPlayList(Landroid/content/Context;Lmiui/resourcebrowser/model/Resource;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected getResourceController(Lmiui/resourcebrowser/ResourceContext;)Lmiui/resourcebrowser/controller/ResourceController;
    .locals 1
    .parameter "resContext"

    .prologue
    .line 164
    new-instance v0, Lmiui/resourcebrowser/controller/ResourceController;

    invoke-direct {v0, p1}, Lmiui/resourcebrowser/controller/ResourceController;-><init>(Lmiui/resourcebrowser/ResourceContext;)V

    return-object v0
.end method

.method protected getResourceOperationHandler(Lmiui/resourcebrowser/view/ResourceOperationView;)Lmiui/resourcebrowser/view/ResourceOperationHandler;
    .locals 2
    .parameter "view"

    .prologue
    .line 300
    new-instance v0, Lmiui/resourcebrowser/view/ResourceOperationHandler;

    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-direct {v0, p0, v1, p1}, Lmiui/resourcebrowser/view/ResourceOperationHandler;-><init>(Landroid/content/Context;Lmiui/resourcebrowser/ResourceContext;Lmiui/resourcebrowser/view/ResourceOperationView;)V

    return-object v0
.end method

.method protected initNavigationState()V
    .locals 2

    .prologue
    .line 326
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreviousItem:Landroid/view/View;

    new-instance v1, Lmiui/resourcebrowser/activity/ResourceDetailActivity$3;

    invoke-direct {v1, p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity$3;-><init>(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 332
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mNextItem:Landroid/view/View;

    new-instance v1, Lmiui/resourcebrowser/activity/ResourceDetailActivity$4;

    invoke-direct {v1, p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity$4;-><init>(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 338
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->updateNavigationState()V

    .line 339
    return-void
.end method

.method protected initParams()V
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 168
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 170
    .local v1, intent:Landroid/content/Intent;
    const-string v3, "android.intent.action.VIEW"

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 171
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->getExternalResource()Lmiui/resourcebrowser/model/Resource;

    move-result-object v2

    .line 172
    .local v2, resource:Lmiui/resourcebrowser/model/Resource;
    if-nez v2, :cond_0

    .line 173
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->finish()V

    .line 214
    .end local v2           #resource:Lmiui/resourcebrowser/model/Resource;
    :goto_0
    return-void

    .line 176
    .restart local v2       #resource:Lmiui/resourcebrowser/model/Resource;
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mDataSet:Ljava/util/List;

    .line 177
    new-instance v0, Lmiui/resourcebrowser/widget/DataGroup;

    invoke-direct {v0}, Lmiui/resourcebrowser/widget/DataGroup;-><init>()V

    .line 178
    .local v0, group:Lmiui/resourcebrowser/widget/DataGroup;,"Lmiui/resourcebrowser/widget/DataGroup<Lmiui/resourcebrowser/model/Resource;>;"
    invoke-virtual {v0, v2}, Lmiui/resourcebrowser/widget/DataGroup;->add(Ljava/lang/Object;)Z

    .line 179
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mDataSet:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 180
    iput v5, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResourceGroup:I

    .line 181
    iput v5, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResourceIndex:I

    .line 188
    .end local v0           #group:Lmiui/resourcebrowser/widget/DataGroup;,"Lmiui/resourcebrowser/widget/DataGroup<Lmiui/resourcebrowser/model/Resource;>;"
    .end local v2           #resource:Lmiui/resourcebrowser/model/Resource;
    :goto_1
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mDataSet:Ljava/util/List;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mDataSet:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 189
    :cond_1
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->finish()V

    goto :goto_0

    .line 183
    :cond_2
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mAppContext:Lmiui/resourcebrowser/AppInnerContext;

    invoke-virtual {v3}, Lmiui/resourcebrowser/AppInnerContext;->getWorkingDataSet()Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mDataSet:Ljava/util/List;

    .line 184
    const-string v3, "REQUEST_RES_GROUP"

    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResourceGroup:I

    .line 185
    const-string v3, "REQUEST_RES_INDEX"

    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResourceIndex:I

    goto :goto_1

    .line 192
    :cond_3
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mDataSet:Ljava/util/List;

    iget v6, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResourceGroup:I

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmiui/resourcebrowser/widget/DataGroup;

    iput-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mGroupDataSet:Lmiui/resourcebrowser/widget/DataGroup;

    .line 193
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mGroupDataSet:Lmiui/resourcebrowser/widget/DataGroup;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mGroupDataSet:Lmiui/resourcebrowser/widget/DataGroup;

    invoke-virtual {v3}, Lmiui/resourcebrowser/widget/DataGroup;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 194
    :cond_4
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->finish()V

    goto :goto_0

    .line 198
    :cond_5
    const-string v3, "REQUEST_SOURCE_TYPE"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mSourceType:I

    .line 199
    iget v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mSourceType:I

    const/4 v6, 0x2

    if-ne v3, v6, :cond_6

    move v3, v4

    :goto_2
    iput-boolean v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mIsOnlineResourceSet:Z

    .line 200
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mDataSet:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ne v3, v4, :cond_7

    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mGroupDataSet:Lmiui/resourcebrowser/widget/DataGroup;

    invoke-virtual {v3}, Lmiui/resourcebrowser/widget/DataGroup;->size()I

    move-result v3

    if-ne v3, v4, :cond_7

    :goto_3
    iput-boolean v4, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mIsSingleResourceSet:Z

    .line 202
    invoke-static {p0}, Lmiui/app/SDCardMonitor;->getSDCardMonitor(Landroid/content/Context;)Lmiui/app/SDCardMonitor;

    move-result-object v3

    iput-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mSDCardMonitor:Lmiui/app/SDCardMonitor;

    .line 203
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mSDCardMonitor:Lmiui/app/SDCardMonitor;

    invoke-virtual {v3, p0}, Lmiui/app/SDCardMonitor;->addListener(Lmiui/app/SDCardMonitor$SDCardStatusListener;)V

    .line 205
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    new-instance v4, Lmiui/resourcebrowser/activity/ResourceDetailActivity$1;

    invoke-direct {v4, p0, v1}, Lmiui/resourcebrowser/activity/ResourceDetailActivity$1;-><init>(Lmiui/resourcebrowser/activity/ResourceDetailActivity;Landroid/content/Intent;)V

    const-wide/16 v5, 0x320

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    :cond_6
    move v3, v5

    .line 199
    goto :goto_2

    :cond_7
    move v4, v5

    .line 200
    goto :goto_3
.end method

.method protected navigateToNextResource()V
    .locals 3

    .prologue
    .line 362
    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mGroupDataSet:Lmiui/resourcebrowser/widget/DataGroup;

    invoke-virtual {v1}, Lmiui/resourcebrowser/widget/DataGroup;->size()I

    move-result v0

    .line 363
    .local v0, resourceTotal:I
    iget v1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResourceIndex:I

    add-int/lit8 v2, v0, -0x1

    if-ge v1, v2, :cond_0

    .line 364
    iget v1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResourceIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResourceIndex:I

    .line 365
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->changeCurrentResource()V

    .line 366
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->updateNavigationState()V

    .line 368
    :cond_0
    return-void
.end method

.method protected navigateToPreviousResource()V
    .locals 1

    .prologue
    .line 354
    iget v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResourceIndex:I

    if-lez v0, :cond_0

    .line 355
    iget v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResourceIndex:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResourceIndex:I

    .line 356
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->changeCurrentResource()V

    .line 357
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->updateNavigationState()V

    .line 359
    :cond_0
    return-void
.end method

.method public onApplyEventPerformed()V
    .locals 0

    .prologue
    .line 1031
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 1022
    iget-boolean v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mFullScreen:Z

    if-eqz v0, :cond_0

    .line 1023
    invoke-direct {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->enterNormalMode()V

    .line 1027
    :goto_0
    return-void

    .line 1025
    :cond_0
    invoke-super {p0}, Lmiui/app/ObservableActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onBuyEventPerformed()V
    .locals 0

    .prologue
    .line 1039
    return-void
.end method

.method protected final onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter "savedInstanceState"

    .prologue
    .line 128
    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->requestWindowFeature(I)Z

    .line 129
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->requestWindowFeature(I)Z

    .line 131
    invoke-super {p0, p1}, Lmiui/app/ObservableActivity;->onCreate(Landroid/os/Bundle;)V

    .line 132
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->getContentView()I

    move-result v0

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->setContentView(I)V

    .line 134
    invoke-static {}, Lmiui/resourcebrowser/AppInnerContext;->getInstance()Lmiui/resourcebrowser/AppInnerContext;

    move-result-object v0

    iput-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mAppContext:Lmiui/resourcebrowser/AppInnerContext;

    .line 135
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mAppContext:Lmiui/resourcebrowser/AppInnerContext;

    invoke-virtual {v0}, Lmiui/resourcebrowser/AppInnerContext;->getResourceContext()Lmiui/resourcebrowser/ResourceContext;

    move-result-object v0

    iput-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    .line 136
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    if-nez v0, :cond_0

    .line 137
    new-instance v0, Lmiui/resourcebrowser/ResourceContext;

    invoke-direct {v0}, Lmiui/resourcebrowser/ResourceContext;-><init>()V

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->buildResourceContext(Lmiui/resourcebrowser/ResourceContext;)Lmiui/resourcebrowser/ResourceContext;

    move-result-object v0

    iput-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    .line 139
    :cond_0
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mAppContext:Lmiui/resourcebrowser/AppInnerContext;

    invoke-virtual {v0}, Lmiui/resourcebrowser/AppInnerContext;->getResourceController()Lmiui/resourcebrowser/controller/ResourceController;

    move-result-object v0

    iput-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResController:Lmiui/resourcebrowser/controller/ResourceController;

    .line 140
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResController:Lmiui/resourcebrowser/controller/ResourceController;

    if-nez v0, :cond_1

    .line 141
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->getResourceController(Lmiui/resourcebrowser/ResourceContext;)Lmiui/resourcebrowser/controller/ResourceController;

    move-result-object v0

    iput-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResController:Lmiui/resourcebrowser/controller/ResourceController;

    .line 142
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mAppContext:Lmiui/resourcebrowser/AppInnerContext;

    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResController:Lmiui/resourcebrowser/controller/ResourceController;

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/AppInnerContext;->setResourceController(Lmiui/resourcebrowser/controller/ResourceController;)V

    .line 145
    :cond_1
    if-eqz p1, :cond_2

    .line 146
    const-string v0, "PREVIEW_INDEX"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreviewOffset:I

    .line 149
    :cond_2
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->initParams()V

    .line 150
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 153
    :goto_0
    return-void

    .line 151
    :cond_3
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->setupUI()V

    .line 152
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->changeCurrentResource()V

    goto :goto_0
.end method

.method public onDeleteEventPerformed()V
    .locals 4

    .prologue
    .line 1051
    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v1}, Lmiui/resourcebrowser/model/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1052
    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v1}, Lmiui/resourcebrowser/model/Resource;->getDownloadPath()Ljava/lang/String;

    move-result-object v0

    .line 1053
    .local v0, downloadPath:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1054
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v3}, Lmiui/resourcebrowser/ResourceContext;->getIndexFolder()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v3}, Lmiui/resourcebrowser/model/Resource;->getHash()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 1055
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 1057
    :cond_0
    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResController:Lmiui/resourcebrowser/controller/ResourceController;

    invoke-virtual {v1}, Lmiui/resourcebrowser/controller/ResourceController;->getLocalDataManager()Lmiui/resourcebrowser/controller/LocalDataManager;

    move-result-object v1

    iget-object v2, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v1, v2}, Lmiui/resourcebrowser/controller/LocalDataManager;->removeResource(Lmiui/resourcebrowser/model/Resource;)Z

    .line 1058
    iget-boolean v1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mIsOnlineResourceSet:Z

    if-eqz v1, :cond_2

    .line 1059
    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    iget-object v2, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v2}, Lmiui/resourcebrowser/model/Resource;->getStatus()I

    move-result v2

    and-int/lit8 v2, v2, -0x2

    invoke-virtual {v1, v2}, Lmiui/resourcebrowser/model/Resource;->setStatus(I)V

    .line 1065
    .end local v0           #downloadPath:Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 1061
    .restart local v0       #downloadPath:Ljava/lang/String;
    :cond_2
    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mGroupDataSet:Lmiui/resourcebrowser/widget/DataGroup;

    iget-object v2, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v1, v2}, Lmiui/resourcebrowser/widget/DataGroup;->remove(Ljava/lang/Object;)Z

    .line 1062
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->finish()V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1005
    iget-object v2, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mSDCardMonitor:Lmiui/app/SDCardMonitor;

    if-eqz v2, :cond_0

    .line 1006
    iget-object v2, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mSDCardMonitor:Lmiui/app/SDCardMonitor;

    invoke-virtual {v2, p0}, Lmiui/app/SDCardMonitor;->removeListener(Lmiui/app/SDCardMonitor$SDCardStatusListener;)V

    .line 1008
    :cond_0
    iget-object v2, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mImageCacheDecoder:Lmiui/resourcebrowser/util/ImageCacheDecoder;

    if-eqz v2, :cond_1

    .line 1009
    iget-object v2, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mImageCacheDecoder:Lmiui/resourcebrowser/util/ImageCacheDecoder;

    invoke-virtual {v2, v3}, Lmiui/resourcebrowser/util/ImageCacheDecoder;->clean(Z)V

    .line 1012
    :cond_1
    iget-object v2, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mTaskSet:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/os/UniqueAsyncTask;

    .line 1013
    .local v1, t:Lmiui/os/UniqueAsyncTask;,"Lmiui/os/UniqueAsyncTask<***>;"
    invoke-virtual {v1, v3}, Lmiui/os/UniqueAsyncTask;->cancel(Z)Z

    goto :goto_0

    .line 1015
    .end local v1           #t:Lmiui/os/UniqueAsyncTask;,"Lmiui/os/UniqueAsyncTask<***>;"
    :cond_2
    iget-object v2, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mTaskSet:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    .line 1017
    invoke-super {p0}, Lmiui/app/ObservableActivity;->onDestroy()V

    .line 1018
    return-void
.end method

.method public onDownloadEventPerformed()V
    .locals 0

    .prologue
    .line 1043
    return-void
.end method

.method public onMagicEventPerformed()V
    .locals 0

    .prologue
    .line 1069
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .parameter "item"

    .prologue
    .line 372
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 373
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->finish()V

    .line 375
    :cond_0
    invoke-super {p0, p1}, Lmiui/app/ObservableActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 996
    invoke-direct {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->stopMusic()V

    .line 997
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreview:Lmiui/resourcebrowser/view/ResourceScreenView;

    if-eqz v0, :cond_0

    .line 998
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreview:Lmiui/resourcebrowser/view/ResourceScreenView;

    invoke-virtual {v0}, Lmiui/resourcebrowser/view/ResourceScreenView;->onPause()V

    .line 1000
    :cond_0
    invoke-super {p0}, Lmiui/app/ObservableActivity;->onPause()V

    .line 1001
    return-void
.end method

.method public onPickEventPerformed()V
    .locals 0

    .prologue
    .line 1035
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter "outState"

    .prologue
    .line 288
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreview:Lmiui/resourcebrowser/view/ResourceScreenView;

    if-eqz v0, :cond_0

    .line 289
    const-string v0, "PREVIEW_INDEX"

    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreview:Lmiui/resourcebrowser/view/ResourceScreenView;

    invoke-virtual {v1}, Lmiui/resourcebrowser/view/ResourceScreenView;->getCurrentScreenIndex()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 291
    :cond_0
    invoke-super {p0, p1}, Lmiui/app/ObservableActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 292
    return-void
.end method

.method public onStatusChanged(Z)V
    .locals 0
    .parameter "mount"

    .prologue
    .line 1073
    invoke-static {p0}, Lmiui/resourcebrowser/util/ResourceHelper;->exit(Landroid/app/Activity;)V

    .line 1074
    return-void
.end method

.method public onUpdateEventPerformed()V
    .locals 0

    .prologue
    .line 1047
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 3
    .parameter "hasFocus"

    .prologue
    const/4 v2, 0x0

    .line 277
    invoke-super {p0, p1}, Lmiui/app/ObservableActivity;->onWindowFocusChanged(Z)V

    .line 280
    if-eqz p1, :cond_0

    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mActionBar:Landroid/app/ActionBar;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mFullScreen:Z

    if-nez v1, :cond_0

    .line 281
    const v1, 0x60b007e

    invoke-virtual {p0, v1}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 282
    .local v0, childRoot:Landroid/widget/LinearLayout;
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ActionBar;->getHeight()I

    move-result v1

    invoke-virtual {v0, v2, v1, v2, v2}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 284
    .end local v0           #childRoot:Landroid/widget/LinearLayout;
    :cond_0
    return-void
.end method

.method protected requestResourceDetail(I)V
    .locals 4
    .parameter "index"

    .prologue
    .line 451
    iget-object v2, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mGroupDataSet:Lmiui/resourcebrowser/widget/DataGroup;

    invoke-virtual {v2, p1}, Lmiui/resourcebrowser/widget/DataGroup;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/resourcebrowser/model/Resource;

    .line 452
    .local v0, resource:Lmiui/resourcebrowser/model/Resource;
    new-instance v1, Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadDetailTask;

    invoke-direct {v1, p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadDetailTask;-><init>(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)V

    .line 453
    .local v1, task:Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadDetailTask;
    invoke-virtual {v1, p1}, Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadDetailTask;->setValidIndex(I)V

    .line 454
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "downloadDetail-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadDetailTask;->setId(Ljava/lang/String;)V

    .line 455
    const/4 v2, 0x1

    new-array v2, v2, [Lmiui/resourcebrowser/model/Resource;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadDetailTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 456
    return-void
.end method

.method protected requestResources(I)V
    .locals 4
    .parameter "start"

    .prologue
    .line 387
    new-instance v0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadListTask;

    invoke-direct {v0, p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadListTask;-><init>(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)V

    .line 388
    .local v0, task:Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadListTask;
    invoke-virtual {v0, p1}, Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadListTask;->setOffset(I)V

    .line 389
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "downloadList-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadListTask;->setId(Ljava/lang/String;)V

    .line 390
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Integer;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadListTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 391
    return-void
.end method

.method protected setResourceInfo()V
    .locals 18

    .prologue
    .line 542
    move-object/from16 v0, p0

    iget-object v14, v0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mActionBar:Landroid/app/ActionBar;

    if-eqz v14, :cond_0

    .line 543
    move-object/from16 v0, p0

    iget-object v14, v0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mActionBar:Landroid/app/ActionBar;

    move-object/from16 v0, p0

    iget-object v15, v0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v15}, Lmiui/resourcebrowser/model/Resource;->getTitle()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 546
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mHasInfoView:Z

    if-eqz v14, :cond_3

    .line 547
    move-object/from16 v0, p0

    iget-object v14, v0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v14}, Lmiui/resourcebrowser/model/Resource;->getAuthor()Ljava/lang/String;

    move-result-object v1

    .line 548
    .local v1, author:Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-eqz v14, :cond_1

    .line 549
    const v14, 0x60c000c

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 551
    :cond_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mInfo:Landroid/widget/FrameLayout;

    const v15, 0x60b0053

    invoke-virtual {v14, v15}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 552
    .local v2, authorView:Landroid/widget/TextView;
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 554
    move-object/from16 v0, p0

    iget-object v14, v0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v14}, Lmiui/resourcebrowser/model/Resource;->getDesigner()Ljava/lang/String;

    move-result-object v3

    .line 555
    .local v3, designer:Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 556
    const v14, 0x60c000c

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 558
    :cond_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mInfo:Landroid/widget/FrameLayout;

    const v15, 0x60b0054

    invoke-virtual {v14, v15}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 559
    .local v4, designerView:Landroid/widget/TextView;
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 561
    invoke-static {}, Ljava/text/DateFormat;->getDateInstance()Ljava/text/DateFormat;

    move-result-object v14

    new-instance v15, Ljava/util/Date;

    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lmiui/resourcebrowser/model/Resource;->getUpdatedTime()J

    move-result-wide v16

    invoke-direct/range {v15 .. v17}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v14, v15}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    .line 562
    .local v7, modifiedTime:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mInfo:Landroid/widget/FrameLayout;

    const v15, 0x60b0055

    invoke-virtual {v14, v15}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 563
    .local v8, modifiedTimeView:Landroid/widget/TextView;
    invoke-virtual {v8, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 565
    move-object/from16 v0, p0

    iget-object v14, v0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v14}, Lmiui/resourcebrowser/model/Resource;->getVersion()Ljava/lang/String;

    move-result-object v12

    .line 566
    .local v12, version:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mInfo:Landroid/widget/FrameLayout;

    const v15, 0x60b0056

    invoke-virtual {v14, v15}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    .line 567
    .local v13, versionView:Landroid/widget/TextView;
    invoke-virtual {v13, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 569
    const-string v9, ""

    .line 571
    .local v9, size:Ljava/lang/String;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v14}, Lmiui/resourcebrowser/model/Resource;->getSize()J

    move-result-wide v14

    invoke-static {v14, v15}, Lmiui/resourcebrowser/util/ResourceHelper;->formatFileSize(J)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v9

    .line 574
    :goto_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mInfo:Landroid/widget/FrameLayout;

    const v15, 0x60b0065

    invoke-virtual {v14, v15}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 575
    .local v10, sizeView:Landroid/widget/TextView;
    invoke-virtual {v10, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 577
    move-object/from16 v0, p0

    iget-object v14, v0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v14}, Lmiui/resourcebrowser/model/Resource;->getDownloadCount()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 578
    .local v5, downloadTimes:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mInfo:Landroid/widget/FrameLayout;

    const v15, 0x60b0064

    invoke-virtual {v14, v15}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 579
    .local v6, downloadTimesView:Landroid/widget/TextView;
    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 581
    move-object/from16 v0, p0

    iget-object v14, v0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mInfo:Landroid/widget/FrameLayout;

    const v15, 0x60b00a0

    invoke-virtual {v14, v15}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    .line 582
    .local v11, summaryView:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v14, v0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v14}, Lmiui/resourcebrowser/model/Resource;->getDescription()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v11, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 584
    .end local v1           #author:Ljava/lang/String;
    .end local v2           #authorView:Landroid/widget/TextView;
    .end local v3           #designer:Ljava/lang/String;
    .end local v4           #designerView:Landroid/widget/TextView;
    .end local v5           #downloadTimes:Ljava/lang/String;
    .end local v6           #downloadTimesView:Landroid/widget/TextView;
    .end local v7           #modifiedTime:Ljava/lang/String;
    .end local v8           #modifiedTimeView:Landroid/widget/TextView;
    .end local v9           #size:Ljava/lang/String;
    .end local v10           #sizeView:Landroid/widget/TextView;
    .end local v11           #summaryView:Landroid/widget/TextView;
    .end local v12           #version:Ljava/lang/String;
    .end local v13           #versionView:Landroid/widget/TextView;
    :cond_3
    return-void

    .line 572
    .restart local v1       #author:Ljava/lang/String;
    .restart local v2       #authorView:Landroid/widget/TextView;
    .restart local v3       #designer:Ljava/lang/String;
    .restart local v4       #designerView:Landroid/widget/TextView;
    .restart local v7       #modifiedTime:Ljava/lang/String;
    .restart local v8       #modifiedTimeView:Landroid/widget/TextView;
    .restart local v9       #size:Ljava/lang/String;
    .restart local v12       #version:Ljava/lang/String;
    .restart local v13       #versionView:Landroid/widget/TextView;
    :catch_0
    move-exception v14

    goto :goto_0
.end method

.method protected setResourceStatus()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 536
    iget-object v2, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mOperationHandler:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v2, v3}, Lmiui/resourcebrowser/view/ResourceOperationHandler;->setResource(Lmiui/resourcebrowser/model/Resource;)V

    .line 537
    iget-boolean v2, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mIsOnlineResourceSet:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v2}, Lmiui/resourcebrowser/model/Resource;->getProductId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    .line 538
    .local v0, showLoadingState:Z
    :goto_0
    iget-object v2, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mOperationHandler:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    if-eqz v0, :cond_1

    :goto_1
    const v3, 0x60c0022

    invoke-virtual {p0, v3}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lmiui/resourcebrowser/view/ResourceOperationHandler;->updateLoadingState(ILjava/lang/String;)V

    .line 539
    return-void

    .end local v0           #showLoadingState:Z
    :cond_0
    move v0, v1

    .line 537
    goto :goto_0

    .line 538
    .restart local v0       #showLoadingState:Z
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method protected setScreenViewBackground(I)V
    .locals 1
    .parameter "resId"

    .prologue
    .line 271
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mScreenViewNeedBackgroud:Z

    .line 272
    iput p1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mScreenViewBackgroudId:I

    .line 273
    return-void
.end method

.method protected setupNavigationButton()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v5, 0x0

    const/4 v6, -0x2

    .line 304
    new-instance v1, Landroid/widget/LinearLayout;

    invoke-direct {v1, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 305
    .local v1, custom:Landroid/widget/LinearLayout;
    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 306
    const/high16 v3, 0x4120

    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    invoke-static {v7, v3, v4}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    float-to-int v2, v3

    .line 308
    .local v2, rightPadding:I
    invoke-virtual {v1, v5, v5, v2, v5}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 309
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 310
    .local v0, child:Landroid/widget/ImageView;
    const v3, 0x60200c8

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 311
    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v3, v6, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v0, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 312
    iput-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreviousItem:Landroid/view/View;

    .line 313
    new-instance v0, Landroid/widget/ImageView;

    .end local v0           #child:Landroid/widget/ImageView;
    invoke-direct {v0, p0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 314
    .restart local v0       #child:Landroid/widget/ImageView;
    const v3, 0x60200cc

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 315
    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v3, v6, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v0, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 316
    iput-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mNextItem:Landroid/view/View;

    .line 318
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v3, v7}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 319
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mActionBar:Landroid/app/ActionBar;

    new-instance v4, Landroid/app/ActionBar$LayoutParams;

    const/16 v5, 0x15

    invoke-direct {v4, v6, v6, v5}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    invoke-virtual {v3, v1, v4}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    .line 322
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->initNavigationState()V

    .line 323
    return-void
.end method

.method protected setupUI()V
    .locals 9

    .prologue
    const/4 v8, 0x6

    const/4 v7, -0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 217
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    iput-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mActionBar:Landroid/app/ActionBar;

    .line 218
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mActionBar:Landroid/app/ActionBar;

    if-eqz v3, :cond_0

    .line 219
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v3, v5}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 222
    :cond_0
    const v3, 0x60b007f

    invoke-virtual {p0, v3}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mCoverView:Landroid/widget/ImageView;

    .line 223
    invoke-direct {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->buildModeChangeAnimator()V

    .line 225
    const v3, 0x60b0052

    invoke-virtual {p0, v3}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lmiui/resourcebrowser/view/ResourceScreenView;

    iput-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreview:Lmiui/resourcebrowser/view/ResourceScreenView;

    .line 226
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreview:Lmiui/resourcebrowser/view/ResourceScreenView;

    const v6, 0x3e4ccccd

    invoke-virtual {v3, v6}, Lmiui/resourcebrowser/view/ResourceScreenView;->setOverScrollRatio(F)V

    .line 227
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreview:Lmiui/resourcebrowser/view/ResourceScreenView;

    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Lmiui/resourcebrowser/view/ResourceScreenView;->setOvershootTension(F)V

    .line 228
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreview:Lmiui/resourcebrowser/view/ResourceScreenView;

    const/4 v6, 0x2

    invoke-virtual {v3, v6}, Lmiui/resourcebrowser/view/ResourceScreenView;->setScreenAlignment(I)V

    .line 229
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreview:Lmiui/resourcebrowser/view/ResourceScreenView;

    new-instance v6, Lmiui/resourcebrowser/activity/ResourceDetailActivity$2;

    invoke-direct {v6, p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity$2;-><init>(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)V

    invoke-virtual {v3, v6}, Lmiui/resourcebrowser/view/ResourceScreenView;->setScreenChangeListener(Lmiui/resourcebrowser/view/ResourceScreenView$ScreenChangeListener;)V

    .line 237
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v3, 0x51

    invoke-direct {v2, v7, v7, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 239
    .local v2, params:Landroid/widget/FrameLayout$LayoutParams;
    const/16 v3, 0x1e

    invoke-virtual {v2, v4, v4, v4, v3}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 240
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreview:Lmiui/resourcebrowser/view/ResourceScreenView;

    invoke-virtual {v3, v2}, Lmiui/resourcebrowser/view/ResourceScreenView;->setSeekBarPosition(Landroid/widget/FrameLayout$LayoutParams;)V

    .line 242
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 243
    .local v1, p:Landroid/graphics/Point;
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 244
    iget v3, v1, Landroid/graphics/Point;->x:I

    invoke-static {v3}, Lmiui/resourcebrowser/util/ResourceHelper;->getPreviewWidth(I)I

    move-result v3

    iput v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreviewWidth:I

    .line 245
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    iget v6, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreviewWidth:I

    invoke-virtual {v3, v6}, Lmiui/resourcebrowser/ResourceContext;->setPreviewImageWidth(I)V

    .line 246
    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    iget v6, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreviewWidth:I

    const/4 v7, -0x1

    invoke-direct {v3, v6, v7}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    iput-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mNormalParams:Landroid/view/ViewGroup$LayoutParams;

    .line 248
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v3}, Lmiui/resourcebrowser/ResourceContext;->getResourceFormat()I

    move-result v3

    if-eq v3, v5, :cond_1

    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v3}, Lmiui/resourcebrowser/ResourceContext;->getResourceFormat()I

    move-result v3

    const/4 v6, 0x4

    if-ne v3, v6, :cond_3

    :cond_1
    move v3, v5

    :goto_0
    iput-boolean v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mHasInfoView:Z

    .line 250
    iget-boolean v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mHasInfoView:Z

    if-eqz v3, :cond_2

    .line 251
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    const v5, 0x603001b

    const/4 v6, 0x0

    invoke-virtual {v3, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 252
    .local v0, infoView:Landroid/view/View;
    new-instance v3, Landroid/widget/FrameLayout;

    invoke-direct {v3, p0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mInfo:Landroid/widget/FrameLayout;

    .line 253
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mInfo:Landroid/widget/FrameLayout;

    const/16 v5, 0x3c

    invoke-virtual {v3, v8, v4, v8, v5}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    .line 254
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mInfo:Landroid/widget/FrameLayout;

    iget-object v4, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mImageParams:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v3, v0, v4}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 255
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mInfo:Landroid/widget/FrameLayout;

    iget-object v4, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mNormalParams:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v3, v4}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 258
    .end local v0           #infoView:Landroid/view/View;
    :cond_2
    const v3, 0x60b0059

    invoke-virtual {p0, v3}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lmiui/resourcebrowser/view/ResourceOperationView;

    iput-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mOperationView:Lmiui/resourcebrowser/view/ResourceOperationView;

    .line 259
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mOperationView:Lmiui/resourcebrowser/view/ResourceOperationView;

    invoke-virtual {v3, p0}, Lmiui/resourcebrowser/view/ResourceOperationView;->setResourceOperationListener(Lmiui/resourcebrowser/view/ResourceOperationView$ResourceOperationListener;)V

    .line 260
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mOperationView:Lmiui/resourcebrowser/view/ResourceOperationView;

    invoke-virtual {p0, v3}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->getResourceOperationHandler(Lmiui/resourcebrowser/view/ResourceOperationView;)Lmiui/resourcebrowser/view/ResourceOperationHandler;

    move-result-object v3

    iput-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mOperationHandler:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    .line 261
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mOperationHandler:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    iget-object v4, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResController:Lmiui/resourcebrowser/controller/ResourceController;

    invoke-virtual {v3, v4}, Lmiui/resourcebrowser/view/ResourceOperationHandler;->setResourceController(Lmiui/resourcebrowser/controller/ResourceController;)V

    .line 262
    iget-object v3, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mOperationHandler:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    invoke-virtual {p0, v3}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->addObserver(Lmiui/app/ActivityLifecycleObserver;)V

    .line 264
    invoke-virtual {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->setupNavigationButton()V

    .line 265
    return-void

    :cond_3
    move v3, v4

    .line 248
    goto :goto_0
.end method

.method protected updateNavigationState()V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 342
    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mGroupDataSet:Lmiui/resourcebrowser/widget/DataGroup;

    invoke-virtual {v1}, Lmiui/resourcebrowser/widget/DataGroup;->size()I

    move-result v0

    .line 343
    .local v0, resourceTotal:I
    iget-object v4, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mPreviousItem:Landroid/view/View;

    iget v1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResourceIndex:I

    if-lez v1, :cond_1

    move v1, v2

    :goto_0
    invoke-virtual {v4, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 344
    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mNextItem:Landroid/view/View;

    iget v4, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResourceIndex:I

    add-int/lit8 v5, v0, -0x1

    if-ge v4, v5, :cond_2

    :goto_1
    invoke-virtual {v1, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 346
    iget v1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResourceIndex:I

    add-int/lit8 v2, v0, -0x5

    if-lt v1, v2, :cond_0

    .line 347
    iget-boolean v1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mReachBottom:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mIsOnlineResourceSet:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mIsSingleResourceSet:Z

    if-nez v1, :cond_0

    .line 348
    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->requestResources(I)V

    .line 351
    :cond_0
    return-void

    :cond_1
    move v1, v3

    .line 343
    goto :goto_0

    :cond_2
    move v2, v3

    .line 344
    goto :goto_1
.end method
