.class public Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadDetailTask;
.super Lmiui/os/UniqueAsyncTask;
.source "ResourceDetailActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/resourcebrowser/activity/ResourceDetailActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "DownloadDetailTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmiui/os/UniqueAsyncTask",
        "<",
        "Lmiui/resourcebrowser/model/Resource;",
        "Ljava/lang/Void;",
        "Lmiui/resourcebrowser/model/Resource;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

.field private validIndex:I


# direct methods
.method protected constructor <init>(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 458
    iput-object p1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadDetailTask;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    invoke-direct {p0}, Lmiui/os/UniqueAsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"

    .prologue
    .line 458
    check-cast p1, [Lmiui/resourcebrowser/model/Resource;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadDetailTask;->doInBackground([Lmiui/resourcebrowser/model/Resource;)Lmiui/resourcebrowser/model/Resource;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Lmiui/resourcebrowser/model/Resource;)Lmiui/resourcebrowser/model/Resource;
    .locals 13
    .parameter "params"

    .prologue
    const/4 v12, 0x0

    .line 493
    const/4 v9, 0x0

    .line 494
    .local v9, result:Lmiui/resourcebrowser/model/Resource;
    aget-object v7, p1, v12

    .line 495
    .local v7, resource:Lmiui/resourcebrowser/model/Resource;
    invoke-virtual {v7}, Lmiui/resourcebrowser/model/Resource;->getStatus()I

    move-result v10

    .line 496
    .local v10, status:I
    invoke-virtual {v7}, Lmiui/resourcebrowser/model/Resource;->getOnlineId()Ljava/lang/String;

    move-result-object v11

    if-nez v11, :cond_0

    .line 497
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 498
    .local v3, hashs:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {v7}, Lmiui/resourcebrowser/model/Resource;->getHash()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v3, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 499
    iget-object v11, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadDetailTask;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    iget-object v11, v11, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResController:Lmiui/resourcebrowser/controller/ResourceController;

    invoke-virtual {v11}, Lmiui/resourcebrowser/controller/ResourceController;->getOnlineDataManager()Lmiui/resourcebrowser/controller/OnlineDataManager;

    move-result-object v11

    invoke-virtual {v11, v3}, Lmiui/resourcebrowser/controller/OnlineDataManager;->getAssociationResources(Ljava/util/List;)Ljava/util/List;

    move-result-object v8

    .line 500
    .local v8, resources:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/Resource;>;"
    if-eqz v8, :cond_0

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v11

    if-lez v11, :cond_0

    .line 501
    invoke-interface {v8, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v11}, Lmiui/resourcebrowser/model/Resource;->getOnlineId()Ljava/lang/String;

    move-result-object v6

    .line 502
    .local v6, onlineId:Ljava/lang/String;
    if-eqz v6, :cond_0

    const-string v11, ""

    invoke-virtual {v6, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_0

    .line 503
    invoke-virtual {v7, v6}, Lmiui/resourcebrowser/model/Resource;->setOnlineId(Ljava/lang/String;)V

    .line 504
    or-int/lit8 v11, v10, 0x2

    invoke-virtual {v7, v11}, Lmiui/resourcebrowser/model/Resource;->setStatus(I)V

    .line 508
    .end local v3           #hashs:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .end local v6           #onlineId:Ljava/lang/String;
    .end local v8           #resources:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/Resource;>;"
    :cond_0
    invoke-virtual {v7}, Lmiui/resourcebrowser/model/Resource;->getOnlineId()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_2

    .line 509
    invoke-virtual {v7}, Lmiui/resourcebrowser/model/Resource;->getLocalId()Ljava/lang/String;

    move-result-object v4

    .line 510
    .local v4, localId:Ljava/lang/String;
    invoke-virtual {v7}, Lmiui/resourcebrowser/model/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v5

    .line 511
    .local v5, localPath:Ljava/lang/String;
    invoke-virtual {v7}, Lmiui/resourcebrowser/model/Resource;->getFilePath()Ljava/lang/String;

    move-result-object v1

    .line 512
    .local v1, filePath:Ljava/lang/String;
    invoke-virtual {v7}, Lmiui/resourcebrowser/model/Resource;->getHash()Ljava/lang/String;

    move-result-object v2

    .line 513
    .local v2, hash:Ljava/lang/String;
    iget-object v11, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadDetailTask;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    iget-object v11, v11, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResController:Lmiui/resourcebrowser/controller/ResourceController;

    invoke-virtual {v11}, Lmiui/resourcebrowser/controller/ResourceController;->getOnlineDataManager()Lmiui/resourcebrowser/controller/OnlineDataManager;

    move-result-object v11

    invoke-virtual {v7}, Lmiui/resourcebrowser/model/Resource;->getOnlineId()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lmiui/resourcebrowser/controller/OnlineDataManager;->getResource(Ljava/lang/String;)Lmiui/resourcebrowser/model/Resource;

    move-result-object v9

    .line 514
    if-eqz v9, :cond_2

    .line 515
    invoke-virtual {v7, v9}, Lmiui/resourcebrowser/model/Resource;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 516
    .local v0, equals:Z
    if-nez v0, :cond_1

    .line 517
    invoke-virtual {v7, v9}, Lmiui/resourcebrowser/model/Resource;->updateFrom(Lmiui/resourcebrowser/model/Resource;)V

    .line 519
    :cond_1
    or-int/lit8 v11, v10, 0x2

    invoke-virtual {v7, v11}, Lmiui/resourcebrowser/model/Resource;->setStatus(I)V

    .line 520
    invoke-virtual {v7, v4}, Lmiui/resourcebrowser/model/Resource;->setLocalId(Ljava/lang/String;)V

    .line 521
    invoke-virtual {v7, v5}, Lmiui/resourcebrowser/model/Resource;->setLocalPath(Ljava/lang/String;)V

    .line 522
    invoke-virtual {v7, v1}, Lmiui/resourcebrowser/model/Resource;->setFilePath(Ljava/lang/String;)V

    .line 523
    invoke-virtual {v7, v2}, Lmiui/resourcebrowser/model/Resource;->setHash(Ljava/lang/String;)V

    .line 524
    if-nez v0, :cond_2

    .line 525
    invoke-virtual {v7}, Lmiui/resourcebrowser/model/Resource;->getStatus()I

    move-result v11

    invoke-static {v11}, Lmiui/resourcebrowser/util/ResourceHelper;->isLocalResource(I)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 526
    iget-object v11, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadDetailTask;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    iget-object v11, v11, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResController:Lmiui/resourcebrowser/controller/ResourceController;

    invoke-virtual {v11}, Lmiui/resourcebrowser/controller/ResourceController;->getLocalDataManager()Lmiui/resourcebrowser/controller/LocalDataManager;

    move-result-object v11

    invoke-virtual {v11, v7}, Lmiui/resourcebrowser/controller/LocalDataManager;->updateResource(Lmiui/resourcebrowser/model/Resource;)Z

    .line 531
    .end local v0           #equals:Z
    .end local v1           #filePath:Ljava/lang/String;
    .end local v2           #hash:Ljava/lang/String;
    .end local v4           #localId:Ljava/lang/String;
    .end local v5           #localPath:Ljava/lang/String;
    :cond_2
    return-object v9
.end method

.method protected hasEquivalentRunningTasks()Z
    .locals 1

    .prologue
    .line 474
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadDetailTask;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    #getter for: Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mTaskSet:Ljava/util/Set;
    invoke-static {v0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->access$100(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 458
    check-cast p1, Lmiui/resourcebrowser/model/Resource;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadDetailTask;->onPostExecute(Lmiui/resourcebrowser/model/Resource;)V

    return-void
.end method

.method protected onPostExecute(Lmiui/resourcebrowser/model/Resource;)V
    .locals 2
    .parameter "result"

    .prologue
    .line 479
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadDetailTask;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    invoke-virtual {v0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p1, :cond_1

    .line 489
    :cond_0
    :goto_0
    return-void

    .line 483
    :cond_1
    iget v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadDetailTask;->validIndex:I

    iget-object v1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadDetailTask;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    iget v1, v1, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mResourceIndex:I

    if-ne v0, v1, :cond_2

    .line 484
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadDetailTask;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    invoke-virtual {v0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->setResourceStatus()V

    .line 485
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadDetailTask;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    invoke-virtual {v0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->setResourceInfo()V

    .line 486
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadDetailTask;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    invoke-virtual {v0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->bindScreenView()V

    .line 488
    :cond_2
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadDetailTask;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    #getter for: Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mTaskSet:Ljava/util/Set;
    invoke-static {v0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->access$100(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 468
    invoke-super {p0}, Lmiui/os/UniqueAsyncTask;->onPreExecute()V

    .line 469
    iget-object v0, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadDetailTask;->this$0:Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    #getter for: Lmiui/resourcebrowser/activity/ResourceDetailActivity;->mTaskSet:Ljava/util/Set;
    invoke-static {v0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->access$100(Lmiui/resourcebrowser/activity/ResourceDetailActivity;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 470
    return-void
.end method

.method public setValidIndex(I)V
    .locals 0
    .parameter "validIndex"

    .prologue
    .line 463
    iput p1, p0, Lmiui/resourcebrowser/activity/ResourceDetailActivity$DownloadDetailTask;->validIndex:I

    .line 464
    return-void
.end method
