.class Lmiui/resourcebrowser/util/ResourceDownloadHandler$2;
.super Lmiui/resourcebrowser/controller/local/ImportResourceTask;
.source "ResourceDownloadHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiui/resourcebrowser/util/ResourceDownloadHandler;->handleDownloadSuccessful(Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/resourcebrowser/util/ResourceDownloadHandler;

.field final synthetic val$onlineId:Ljava/lang/String;

.field final synthetic val$path:Ljava/lang/String;


# direct methods
.method constructor <init>(Lmiui/resourcebrowser/util/ResourceDownloadHandler;Lmiui/resourcebrowser/ResourceContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter "x0"
    .parameter "x1"
    .parameter
    .parameter

    .prologue
    .line 229
    iput-object p1, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler$2;->this$0:Lmiui/resourcebrowser/util/ResourceDownloadHandler;

    iput-object p4, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler$2;->val$path:Ljava/lang/String;

    iput-object p5, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler$2;->val$onlineId:Ljava/lang/String;

    invoke-direct {p0, p2, p3}, Lmiui/resourcebrowser/controller/local/ImportResourceTask;-><init>(Lmiui/resourcebrowser/ResourceContext;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 3
    .parameter "successNum"

    .prologue
    .line 232
    iget-object v0, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler$2;->this$0:Lmiui/resourcebrowser/util/ResourceDownloadHandler;

    #getter for: Lmiui/resourcebrowser/util/ResourceDownloadHandler;->mListener:Lmiui/resourcebrowser/util/ResourceDownloadHandler$ResourceDownloadListener;
    invoke-static {v0}, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->access$100(Lmiui/resourcebrowser/util/ResourceDownloadHandler;)Lmiui/resourcebrowser/util/ResourceDownloadHandler$ResourceDownloadListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 233
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-gtz v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    .line 234
    :cond_0
    iget-object v0, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler$2;->this$0:Lmiui/resourcebrowser/util/ResourceDownloadHandler;

    #getter for: Lmiui/resourcebrowser/util/ResourceDownloadHandler;->mListener:Lmiui/resourcebrowser/util/ResourceDownloadHandler$ResourceDownloadListener;
    invoke-static {v0}, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->access$100(Lmiui/resourcebrowser/util/ResourceDownloadHandler;)Lmiui/resourcebrowser/util/ResourceDownloadHandler$ResourceDownloadListener;

    move-result-object v0

    iget-object v1, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler$2;->val$path:Ljava/lang/String;

    iget-object v2, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler$2;->val$onlineId:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lmiui/resourcebrowser/util/ResourceDownloadHandler$ResourceDownloadListener;->onDownloadSuccessful(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    :cond_1
    :goto_0
    return-void

    .line 236
    :cond_2
    iget-object v0, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler$2;->this$0:Lmiui/resourcebrowser/util/ResourceDownloadHandler;

    #getter for: Lmiui/resourcebrowser/util/ResourceDownloadHandler;->mListener:Lmiui/resourcebrowser/util/ResourceDownloadHandler$ResourceDownloadListener;
    invoke-static {v0}, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->access$100(Lmiui/resourcebrowser/util/ResourceDownloadHandler;)Lmiui/resourcebrowser/util/ResourceDownloadHandler$ResourceDownloadListener;

    move-result-object v0

    iget-object v1, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler$2;->val$path:Ljava/lang/String;

    iget-object v2, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler$2;->val$onlineId:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lmiui/resourcebrowser/util/ResourceDownloadHandler$ResourceDownloadListener;->onDownloadFailed(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 229
    check-cast p1, Ljava/lang/Integer;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/util/ResourceDownloadHandler$2;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method
