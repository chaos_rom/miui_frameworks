.class Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI$1;
.super Ljava/lang/Object;
.source "AudioBatchResourceHandler.java"

# interfaces
.implements Lmiui/resourcebrowser/util/ResourceMusicPlayer$PlayProgressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->getMusicPlayListener()Lmiui/resourcebrowser/util/ResourceMusicPlayer$PlayProgressListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;


# direct methods
.method constructor <init>(Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;)V
    .locals 0
    .parameter

    .prologue
    .line 126
    iput-object p1, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI$1;->this$1:Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressUpdate(II)V
    .locals 7
    .parameter "playDuration"
    .parameter "totalDuration"

    .prologue
    .line 138
    mul-int/lit8 v3, p1, 0x64

    int-to-double v3, v3

    const-wide/high16 v5, 0x3ff0

    mul-double/2addr v3, v5

    int-to-double v5, p2

    div-double/2addr v3, v5

    double-to-int v1, v3

    .line 139
    .local v1, progress:I
    iget-object v3, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI$1;->this$1:Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;

    #setter for: Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->mLastPlayProgress:I
    invoke-static {v3, v1}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->access$102(Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;I)I

    .line 140
    iget-object v3, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI$1;->this$1:Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;

    #getter for: Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->mGoneViewWhenClick:Ljava/util/List;
    invoke-static {v3}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->access$200(Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 141
    .local v2, v:Landroid/view/View;
    iget-object v4, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI$1;->this$1:Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;

    const v3, 0x60b005e

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ProgressBar;

    const/4 v5, 0x0

    iget-object v6, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI$1;->this$1:Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;

    #getter for: Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->mLastPlayProgress:I
    invoke-static {v6}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->access$100(Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;)I

    move-result v6

    #calls: Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->setProgressBarState(Landroid/widget/ProgressBar;ZI)V
    invoke-static {v4, v3, v5, v6}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->access$300(Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;Landroid/widget/ProgressBar;ZI)V

    goto :goto_0

    .line 143
    .end local v2           #v:Landroid/view/View;
    :cond_0
    return-void
.end method

.method public onStartPlaying()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 130
    iget-object v2, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI$1;->this$1:Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;

    #setter for: Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->mLastPlayProgress:I
    invoke-static {v2, v5}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->access$102(Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;I)I

    .line 131
    iget-object v2, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI$1;->this$1:Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;

    #getter for: Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->mGoneViewWhenClick:Ljava/util/List;
    invoke-static {v2}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->access$200(Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 132
    .local v1, v:Landroid/view/View;
    iget-object v3, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI$1;->this$1:Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;

    const v2, 0x60b005e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    iget-object v4, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI$1;->this$1:Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;

    #getter for: Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->mLastPlayProgress:I
    invoke-static {v4}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->access$100(Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;)I

    move-result v4

    #calls: Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->setProgressBarState(Landroid/widget/ProgressBar;ZI)V
    invoke-static {v3, v2, v5, v4}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->access$300(Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;Landroid/widget/ProgressBar;ZI)V

    goto :goto_0

    .line 134
    .end local v1           #v:Landroid/view/View;
    :cond_0
    return-void
.end method

.method public onStopPlaying()V
    .locals 5

    .prologue
    .line 147
    iget-object v3, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI$1;->this$1:Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;

    #getter for: Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->mGoneViewWhenClick:Ljava/util/List;
    invoke-static {v3}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->access$200(Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 149
    .local v2, v:Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    .line 150
    .local v1, position:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    iget-object v3, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI$1;->this$1:Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;

    #getter for: Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->mSelectPosition:Landroid/util/Pair;
    invoke-static {v3}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->access$400(Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;)Landroid/util/Pair;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/util/Pair;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI$1;->this$1:Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;

    #getter for: Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->mLastSelectPosition:Landroid/util/Pair;
    invoke-static {v3}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->access$500(Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;)Landroid/util/Pair;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/util/Pair;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 153
    :cond_1
    iget-object v3, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI$1;->this$1:Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;

    const/4 v4, 0x1

    #calls: Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->startProgressBarAnimation(Landroid/view/View;Z)V
    invoke-static {v3, v2, v4}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->access$600(Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;Landroid/view/View;Z)V

    goto :goto_0

    .line 155
    .end local v1           #position:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    .end local v2           #v:Landroid/view/View;
    :cond_2
    return-void
.end method
