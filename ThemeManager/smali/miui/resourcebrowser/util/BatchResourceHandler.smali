.class public Lmiui/resourcebrowser/util/BatchResourceHandler;
.super Ljava/lang/Object;
.source "BatchResourceHandler.java"

# interfaces
.implements Lmiui/app/FragmentLifecycleObserver;
.implements Lmiui/resourcebrowser/util/ResourceDownloadHandler$ResourceDownloadListener;


# instance fields
.field private mActionMode:Landroid/view/ActionMode;

.field private mActionModeCallback:Landroid/view/ActionMode$Callback;

.field protected mActivity:Landroid/app/Activity;

.field protected mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;

.field private mCheckedResource:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;"
        }
    .end annotation
.end field

.field private mDownloadBytes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mDownloadHandler:Lmiui/resourcebrowser/util/ResourceDownloadHandler;

.field private mEditableMode:Z

.field protected mFragment:Lmiui/resourcebrowser/activity/ResourceListFragment;

.field protected mIsLocalSource:Z

.field private mItemClickListener:Landroid/view/View$OnClickListener;

.field private mItemLongClickListener:Landroid/view/View$OnLongClickListener;

.field private mLastNotifyProgressTime:J

.field protected mListenDownloadProgress:Z

.field protected mResContext:Lmiui/resourcebrowser/ResourceContext;

.field protected mResController:Lmiui/resourcebrowser/controller/ResourceController;

.field private mSelectTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lmiui/resourcebrowser/activity/ResourceListFragment;Lmiui/resourcebrowser/activity/ResourceAdapter;Lmiui/resourcebrowser/ResourceContext;)V
    .locals 3
    .parameter "fragment"
    .parameter "adapter"
    .parameter "resContext"

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mCheckedResource:Ljava/util/Set;

    .line 53
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mDownloadBytes:Ljava/util/Map;

    .line 82
    new-instance v0, Lmiui/resourcebrowser/util/BatchResourceHandler$1;

    invoke-direct {v0, p0}, Lmiui/resourcebrowser/util/BatchResourceHandler$1;-><init>(Lmiui/resourcebrowser/util/BatchResourceHandler;)V

    iput-object v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mItemClickListener:Landroid/view/View$OnClickListener;

    .line 89
    new-instance v0, Lmiui/resourcebrowser/util/BatchResourceHandler$2;

    invoke-direct {v0, p0}, Lmiui/resourcebrowser/util/BatchResourceHandler$2;-><init>(Lmiui/resourcebrowser/util/BatchResourceHandler;)V

    iput-object v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mItemLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 96
    new-instance v0, Lmiui/resourcebrowser/util/BatchResourceHandler$3;

    invoke-direct {v0, p0}, Lmiui/resourcebrowser/util/BatchResourceHandler$3;-><init>(Lmiui/resourcebrowser/util/BatchResourceHandler;)V

    iput-object v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mActionModeCallback:Landroid/view/ActionMode$Callback;

    .line 62
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 63
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "BatchResourceOperationHandler() parameters can not be null!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 65
    :cond_1
    iput-object p1, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mFragment:Lmiui/resourcebrowser/activity/ResourceListFragment;

    .line 66
    invoke-virtual {p1}, Lmiui/resourcebrowser/activity/ResourceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mActivity:Landroid/app/Activity;

    .line 67
    iput-object p2, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;

    .line 68
    iput-object p3, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    .line 70
    new-instance v0, Lmiui/resourcebrowser/util/ResourceDownloadHandler;

    iget-object v1, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mActivity:Landroid/app/Activity;

    iget-object v2, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-direct {v0, v1, v2}, Lmiui/resourcebrowser/util/ResourceDownloadHandler;-><init>(Landroid/content/Context;Lmiui/resourcebrowser/ResourceContext;)V

    iput-object v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mDownloadHandler:Lmiui/resourcebrowser/util/ResourceDownloadHandler;

    .line 71
    iget-object v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mDownloadHandler:Lmiui/resourcebrowser/util/ResourceDownloadHandler;

    invoke-virtual {v0, p0}, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->setResourceDownloadListener(Lmiui/resourcebrowser/util/ResourceDownloadHandler$ResourceDownloadListener;)V

    .line 72
    return-void
.end method

.method static synthetic access$002(Lmiui/resourcebrowser/util/BatchResourceHandler;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 40
    iput-object p1, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mSelectTextView:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$100(Lmiui/resourcebrowser/util/BatchResourceHandler;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 40
    invoke-direct {p0}, Lmiui/resourcebrowser/util/BatchResourceHandler;->deleteOrDownloadResources()V

    return-void
.end method

.method static synthetic access$200(Lmiui/resourcebrowser/util/BatchResourceHandler;)Ljava/util/Set;
    .locals 1
    .parameter "x0"

    .prologue
    .line 40
    iget-object v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mCheckedResource:Ljava/util/Set;

    return-object v0
.end method

.method private deleteOrDownloadResources()V
    .locals 2

    .prologue
    .line 176
    new-instance v0, Lmiui/resourcebrowser/util/BatchResourceHandler$4;

    invoke-direct {v0, p0}, Lmiui/resourcebrowser/util/BatchResourceHandler$4;-><init>(Lmiui/resourcebrowser/util/BatchResourceHandler;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/util/BatchResourceHandler$4;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 215
    return-void
.end method

.method private setCheckBoxState(Landroid/widget/CheckBox;ZZ)V
    .locals 1
    .parameter "checkBox"
    .parameter "visiable"
    .parameter "checked"

    .prologue
    .line 348
    if-eqz p1, :cond_0

    .line 349
    if-eqz p2, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 350
    invoke-virtual {p1, p3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 352
    :cond_0
    return-void

    .line 349
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private setViewBatchSelectedState(Landroid/view/View;)V
    .locals 7
    .parameter "v"

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 332
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    .line 333
    .local v1, position:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    if-nez v1, :cond_0

    .line 345
    :goto_0
    return-void

    .line 336
    :cond_0
    invoke-virtual {p0, v1}, Lmiui/resourcebrowser/util/BatchResourceHandler;->getResource(Landroid/util/Pair;)Lmiui/resourcebrowser/model/Resource;

    move-result-object v2

    .line 337
    .local v2, r:Lmiui/resourcebrowser/model/Resource;
    iget-boolean v6, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mEditableMode:Z

    if-eqz v6, :cond_2

    iget-object v6, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mCheckedResource:Ljava/util/Set;

    invoke-interface {v6, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    move v3, v4

    .line 338
    .local v3, selected:Z
    :goto_1
    iget-boolean v6, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mEditableMode:Z

    if-eqz v6, :cond_3

    invoke-virtual {p0, v2}, Lmiui/resourcebrowser/util/BatchResourceHandler;->canSelecteResource(Lmiui/resourcebrowser/model/Resource;)Z

    move-result v6

    if-eqz v6, :cond_3

    move v0, v4

    .line 340
    .local v0, checkBoxVisiable:Z
    :goto_2
    invoke-virtual {p1, v3}, Landroid/view/View;->setSelected(Z)V

    .line 341
    const v4, 0x60b004c

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz v0, :cond_1

    const/4 v5, 0x4

    :cond_1
    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 343
    const v4, 0x1020001

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    invoke-direct {p0, v4, v0, v3}, Lmiui/resourcebrowser/util/BatchResourceHandler;->setCheckBoxState(Landroid/widget/CheckBox;ZZ)V

    .line 344
    const v4, 0x60b004d

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ProgressBar;

    invoke-virtual {p0, v4, v2}, Lmiui/resourcebrowser/util/BatchResourceHandler;->setDownloadProgress(Landroid/widget/ProgressBar;Lmiui/resourcebrowser/model/Resource;)V

    goto :goto_0

    .end local v0           #checkBoxVisiable:Z
    .end local v3           #selected:Z
    :cond_2
    move v3, v5

    .line 337
    goto :goto_1

    .restart local v3       #selected:Z
    :cond_3
    move v0, v5

    .line 338
    goto :goto_2
.end method


# virtual methods
.method protected canDeleteResource(Lmiui/resourcebrowser/model/Resource;)Z
    .locals 2
    .parameter "r"

    .prologue
    .line 165
    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v0

    .line 166
    .local v0, localPath:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lmiui/resourcebrowser/util/ResourceHelper;->isSystemResource(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected canDownloadResource(Lmiui/resourcebrowser/model/Resource;)Z
    .locals 2
    .parameter "r"

    .prologue
    .line 171
    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getStatus()I

    move-result v0

    invoke-static {v0}, Lmiui/resourcebrowser/util/ResourceHelper;->isLocalResource(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getStatus()I

    move-result v0

    invoke-static {v0}, Lmiui/resourcebrowser/util/ResourceHelper;->isOldResource(I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mDownloadHandler:Lmiui/resourcebrowser/util/ResourceDownloadHandler;

    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getOnlineId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->isResourceDownloading(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected canSelecteResource(Lmiui/resourcebrowser/model/Resource;)Z
    .locals 1
    .parameter "r"

    .prologue
    .line 161
    iget-boolean v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mIsLocalSource:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/util/BatchResourceHandler;->canDeleteResource(Lmiui/resourcebrowser/model/Resource;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mIsLocalSource:Z

    if-nez v0, :cond_2

    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/util/BatchResourceHandler;->canDownloadResource(Lmiui/resourcebrowser/model/Resource;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected doCancelDownload(Lmiui/resourcebrowser/model/Resource;)V
    .locals 2
    .parameter "r"

    .prologue
    .line 229
    iget-object v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mDownloadHandler:Lmiui/resourcebrowser/util/ResourceDownloadHandler;

    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getDownloadPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->cancelDownload(Ljava/lang/String;)Z

    .line 230
    iget-object v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mDownloadBytes:Ljava/util/Map;

    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getDownloadPath()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 231
    iget-object v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;

    invoke-virtual {v0}, Lmiui/resourcebrowser/activity/ResourceAdapter;->notifyDataSetChanged()V

    .line 232
    return-void
.end method

.method protected doDownloadResource(Lmiui/resourcebrowser/model/Resource;)V
    .locals 4
    .parameter "r"

    .prologue
    .line 222
    iget-object v1, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mDownloadHandler:Lmiui/resourcebrowser/util/ResourceDownloadHandler;

    iget-boolean v2, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mListenDownloadProgress:Z

    invoke-virtual {v1, p1, v2}, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->downloadResource(Lmiui/resourcebrowser/model/Resource;Z)Z

    move-result v0

    .line 223
    .local v0, ret:Z
    if-eqz v0, :cond_0

    .line 224
    iget-object v1, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mDownloadBytes:Ljava/util/Map;

    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getDownloadPath()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    :cond_0
    return-void
.end method

.method protected enterEditMode(Landroid/view/View;Landroid/util/Pair;)V
    .locals 2
    .parameter "v"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 244
    .local p2, position:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mEditableMode:Z

    .line 245
    iget-object v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mCheckedResource:Ljava/util/Set;

    invoke-virtual {p0, p2}, Lmiui/resourcebrowser/util/BatchResourceHandler;->getResource(Landroid/util/Pair;)Lmiui/resourcebrowser/model/Resource;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 246
    invoke-direct {p0, p1}, Lmiui/resourcebrowser/util/BatchResourceHandler;->setViewBatchSelectedState(Landroid/view/View;)V

    .line 247
    iget-object v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mActionModeCallback:Landroid/view/ActionMode$Callback;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v0

    iput-object v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mActionMode:Landroid/view/ActionMode;

    .line 248
    return-void
.end method

.method protected getResource(Landroid/util/Pair;)Lmiui/resourcebrowser/model/Resource;
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)",
            "Lmiui/resourcebrowser/model/Resource;"
        }
    .end annotation

    .prologue
    .line 154
    .local p1, position:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;

    invoke-virtual {v1}, Lmiui/resourcebrowser/activity/ResourceAdapter;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 155
    iget-object v1, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;

    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v0, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v2, v0}, Lmiui/resourcebrowser/activity/ResourceAdapter;->getDataItem(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/resourcebrowser/model/Resource;

    .line 157
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getResourceClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 316
    iget-object v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mItemClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public initViewState(Landroid/view/View;Landroid/util/Pair;)V
    .locals 1
    .parameter "v"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 324
    .local p2, position:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    invoke-virtual {p1, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 325
    iget-object v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mItemClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 326
    iget-object v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mItemLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 327
    invoke-direct {p0, p1}, Lmiui/resourcebrowser/util/BatchResourceHandler;->setViewBatchSelectedState(Landroid/view/View;)V

    .line 328
    return-void
.end method

.method protected isDownloading(Lmiui/resourcebrowser/model/Resource;)Z
    .locals 2
    .parameter "r"

    .prologue
    .line 235
    iget-object v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mDownloadBytes:Ljava/util/Map;

    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getDownloadPath()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isEditMode()Z
    .locals 1

    .prologue
    .line 312
    iget-boolean v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mEditableMode:Z

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0
    .parameter "savedInstanceState"

    .prologue
    .line 403
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 0
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 430
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0
    .parameter "activity"

    .prologue
    .line 399
    return-void
.end method

.method protected onClickEventPerformed(Landroid/view/View;)V
    .locals 9
    .parameter "v"

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 262
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 263
    .local v0, position:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    if-nez v0, :cond_0

    .line 292
    :goto_0
    return-void

    .line 267
    :cond_0
    iget-boolean v5, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mEditableMode:Z

    if-eqz v5, :cond_6

    .line 268
    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/util/BatchResourceHandler;->getResource(Landroid/util/Pair;)Lmiui/resourcebrowser/model/Resource;

    move-result-object v1

    .line 269
    .local v1, r:Lmiui/resourcebrowser/model/Resource;
    invoke-virtual {p0, v1}, Lmiui/resourcebrowser/util/BatchResourceHandler;->canSelecteResource(Lmiui/resourcebrowser/model/Resource;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 270
    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v5

    if-nez v5, :cond_1

    :goto_1
    invoke-virtual {p1, v3}, Landroid/view/View;->setSelected(Z)V

    .line 272
    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 273
    iget-object v3, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mCheckedResource:Ljava/util/Set;

    invoke-interface {v3, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 277
    :goto_2
    iget-object v3, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mCheckedResource:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 278
    invoke-virtual {p0}, Lmiui/resourcebrowser/util/BatchResourceHandler;->quitEditMode()V

    goto :goto_0

    :cond_1
    move v3, v4

    .line 270
    goto :goto_1

    .line 275
    :cond_2
    iget-object v3, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mCheckedResource:Ljava/util/Set;

    invoke-interface {v3, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_2

    .line 280
    :cond_3
    invoke-direct {p0, p1}, Lmiui/resourcebrowser/util/BatchResourceHandler;->setViewBatchSelectedState(Landroid/view/View;)V

    .line 281
    invoke-virtual {p0}, Lmiui/resourcebrowser/util/BatchResourceHandler;->updateSelectedTitle()V

    goto :goto_0

    .line 284
    :cond_4
    iget-object v5, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mActivity:Landroid/app/Activity;

    const v6, 0x60c01b9

    new-array v7, v3, [Ljava/lang/Object;

    iget-boolean v3, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mIsLocalSource:Z

    if-eqz v3, :cond_5

    iget-object v3, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mActivity:Landroid/app/Activity;

    const v8, 0x60c01ba

    invoke-virtual {v3, v8}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_3
    aput-object v3, v7, v4

    invoke-virtual {v5, v6, v7}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 287
    .local v2, tips:Ljava/lang/String;
    iget-object v3, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mActivity:Landroid/app/Activity;

    invoke-static {v3, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 284
    .end local v2           #tips:Ljava/lang/String;
    :cond_5
    iget-object v3, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mActivity:Landroid/app/Activity;

    const v8, 0x60c01bb

    invoke-virtual {v3, v8}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    .line 290
    .end local v1           #r:Lmiui/resourcebrowser/model/Resource;
    :cond_6
    iget-object v3, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mFragment:Lmiui/resourcebrowser/activity/ResourceListFragment;

    invoke-virtual {v3, v0}, Lmiui/resourcebrowser/activity/ResourceListFragment;->startDetailActivityForResource(Landroid/util/Pair;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .parameter "savedInstanceState"

    .prologue
    .line 395
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 426
    return-void
.end method

.method public onDownloadFailed(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .parameter "path"
    .parameter "onlineId"

    .prologue
    .line 378
    iget-object v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mActivity:Landroid/app/Activity;

    const v1, 0x60c0023

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 379
    iget-object v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mDownloadBytes:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 380
    return-void
.end method

.method public onDownloadProgressUpdated(Ljava/lang/String;Ljava/lang/String;II)V
    .locals 4
    .parameter "path"
    .parameter "onlineId"
    .parameter "downloadBytes"
    .parameter "totalBytes"

    .prologue
    .line 384
    if-eq p3, p4, :cond_0

    .line 385
    iget-object v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mDownloadBytes:Ljava/util/Map;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 386
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mLastNotifyProgressTime:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x258

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 387
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mLastNotifyProgressTime:J

    .line 388
    iget-object v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;

    invoke-virtual {v0}, Lmiui/resourcebrowser/activity/ResourceAdapter;->notifyDataSetChanged()V

    .line 391
    :cond_0
    return-void
.end method

.method public onDownloadSuccessful(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter "path"
    .parameter "onlineId"

    .prologue
    .line 371
    iget-object v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;

    invoke-virtual {v0}, Lmiui/resourcebrowser/activity/ResourceAdapter;->notifyDataSetChanged()V

    .line 372
    iget-object v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;

    invoke-virtual {v0}, Lmiui/resourcebrowser/activity/ResourceAdapter;->loadData()V

    .line 373
    iget-object v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mDownloadBytes:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 374
    return-void
.end method

.method protected onLongClickEventPerformed(Landroid/view/View;)Z
    .locals 5
    .parameter "v"

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 296
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    .line 297
    .local v1, position:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    if-nez v1, :cond_1

    .line 308
    :cond_0
    :goto_0
    return v2

    .line 301
    :cond_1
    iget-boolean v4, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mIsLocalSource:Z

    if-eqz v4, :cond_3

    iget-object v4, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;

    invoke-virtual {v4}, Lmiui/resourcebrowser/activity/ResourceAdapter;->loadingData()Z

    move-result v4

    if-nez v4, :cond_2

    move v0, v3

    .line 303
    .local v0, allowLongClick:Z
    :goto_1
    if-eqz v0, :cond_0

    iget-boolean v4, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mEditableMode:Z

    if-nez v4, :cond_0

    invoke-virtual {p0, v1}, Lmiui/resourcebrowser/util/BatchResourceHandler;->getResource(Landroid/util/Pair;)Lmiui/resourcebrowser/model/Resource;

    move-result-object v4

    invoke-virtual {p0, v4}, Lmiui/resourcebrowser/util/BatchResourceHandler;->canSelecteResource(Lmiui/resourcebrowser/model/Resource;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 304
    invoke-virtual {p0, p1, v1}, Lmiui/resourcebrowser/util/BatchResourceHandler;->enterEditMode(Landroid/view/View;Landroid/util/Pair;)V

    move v2, v3

    .line 305
    goto :goto_0

    .end local v0           #allowLongClick:Z
    :cond_2
    move v0, v2

    .line 301
    goto :goto_1

    :cond_3
    move v0, v3

    goto :goto_1
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 416
    iget-object v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mDownloadHandler:Lmiui/resourcebrowser/util/ResourceDownloadHandler;

    invoke-virtual {v0}, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->unregisterDownloadReceiver()V

    .line 417
    invoke-virtual {p0}, Lmiui/resourcebrowser/util/BatchResourceHandler;->quitEditMode()V

    .line 418
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 411
    iget-object v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mDownloadHandler:Lmiui/resourcebrowser/util/ResourceDownloadHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->registerDownloadReceiver(Z)V

    .line 412
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 407
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 422
    return-void
.end method

.method public quitEditMode()V
    .locals 1

    .prologue
    .line 251
    iget-boolean v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mEditableMode:Z

    if-eqz v0, :cond_0

    .line 252
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mEditableMode:Z

    .line 253
    iget-object v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mActionMode:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    .line 254
    const/4 v0, 0x0

    iput-object v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mActionMode:Landroid/view/ActionMode;

    .line 255
    iget-object v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mCheckedResource:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 256
    iget-object v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;

    invoke-virtual {v0}, Lmiui/resourcebrowser/activity/ResourceAdapter;->notifyDataSetChanged()V

    .line 258
    :cond_0
    return-void
.end method

.method protected setDownloadProgress(Landroid/widget/ProgressBar;Lmiui/resourcebrowser/model/Resource;)V
    .locals 7
    .parameter "progress"
    .parameter "r"

    .prologue
    const/4 v2, 0x0

    .line 355
    if-eqz p1, :cond_1

    .line 356
    iget-object v3, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mDownloadBytes:Ljava/util/Map;

    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getDownloadPath()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 357
    .local v0, downloadBytes:Ljava/lang/Integer;
    if-eqz v0, :cond_2

    .line 358
    invoke-virtual {p1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 359
    const/16 v1, 0x64

    .line 360
    .local v1, max:I
    const/16 v3, 0x64

    invoke-virtual {p1, v3}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 361
    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getSize()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-lez v3, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    mul-int/lit8 v3, v3, 0x64

    int-to-long v3, v3

    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getSize()J

    move-result-wide v5

    div-long/2addr v3, v5

    long-to-int v2, v3

    .line 362
    .local v2, step:I
    :cond_0
    invoke-virtual {p1, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 367
    .end local v0           #downloadBytes:Ljava/lang/Integer;
    .end local v1           #max:I
    .end local v2           #step:I
    :cond_1
    :goto_0
    return-void

    .line 364
    .restart local v0       #downloadBytes:Ljava/lang/Integer;
    :cond_2
    const/16 v3, 0x8

    invoke-virtual {p1, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0
.end method

.method public setResourceController(Lmiui/resourcebrowser/controller/ResourceController;)V
    .locals 0
    .parameter "resController"

    .prologue
    .line 75
    iput-object p1, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mResController:Lmiui/resourcebrowser/controller/ResourceController;

    .line 76
    return-void
.end method

.method public setSourceType(I)V
    .locals 1
    .parameter "sourceType"

    .prologue
    const/4 v0, 0x1

    .line 79
    if-ne p1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mIsLocalSource:Z

    .line 80
    return-void

    .line 79
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected updateSelectedTitle()V
    .locals 6

    .prologue
    .line 149
    iget-object v1, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mActivity:Landroid/app/Activity;

    const v2, 0x60c01b6

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mCheckedResource:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 150
    .local v0, title:Ljava/lang/String;
    iget-object v1, p0, Lmiui/resourcebrowser/util/BatchResourceHandler;->mSelectTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 151
    return-void
.end method
