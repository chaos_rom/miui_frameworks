.class public Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;
.super Ljava/lang/Object;
.source "ImageCacheDecoder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/resourcebrowser/util/ImageCacheDecoder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "BitmapCache"
.end annotation


# instance fields
.field private mCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mCacheCapacity:I

.field private mCurrentIndex:I

.field mIdList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mIndexList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lmiui/resourcebrowser/util/ImageCacheDecoder;


# direct methods
.method public constructor <init>(Lmiui/resourcebrowser/util/ImageCacheDecoder;I)V
    .locals 2
    .parameter
    .parameter "cacheCapacity"

    .prologue
    .line 264
    iput-object p1, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->this$0:Lmiui/resourcebrowser/util/ImageCacheDecoder;

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 265
    const/4 v0, 0x3

    invoke-static {v0, p2}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->setCacheSize(I)V

    .line 266
    new-instance v0, Ljava/util/HashMap;

    iget v1, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mCacheCapacity:I

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mCache:Ljava/util/Map;

    .line 267
    new-instance v0, Ljava/util/ArrayList;

    iget v1, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mCacheCapacity:I

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mIdList:Ljava/util/ArrayList;

    .line 268
    new-instance v0, Ljava/util/ArrayList;

    iget v1, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mCacheCapacity:I

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mIndexList:Ljava/util/ArrayList;

    .line 269
    return-void
.end method

.method private getNextRemoveCachePosition(I)I
    .locals 5
    .parameter "forIndex"

    .prologue
    .line 323
    iget-object v2, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mIndexList:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 324
    .local v1, selectIndex:I
    if-gez v1, :cond_3

    .line 325
    iget v2, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mCurrentIndex:I

    if-eq p1, v2, :cond_0

    invoke-virtual {p0}, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->isFull()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 326
    :cond_0
    iget-object v2, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mIndexList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v1, -0x1

    .line 327
    :goto_0
    const/4 v0, 0x1

    .local v0, i:I
    :goto_1
    iget-object v2, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mIndexList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 328
    iget v3, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mCurrentIndex:I

    iget-object v2, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mIndexList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    sub-int v2, v3, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v3

    iget v4, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mCurrentIndex:I

    iget-object v2, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mIndexList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    sub-int v2, v4, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    if-le v3, v2, :cond_1

    .line 329
    move v1, v0

    .line 327
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 326
    .end local v0           #i:I
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 334
    :cond_3
    return v1
.end method


# virtual methods
.method public add(Ljava/lang/String;Landroid/graphics/Bitmap;I)V
    .locals 2
    .parameter "id"
    .parameter "b"
    .parameter "useIndex"

    .prologue
    .line 285
    iget-object v0, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mCache:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 292
    :goto_0
    return-void

    .line 288
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->removeIdleCache(Z)Landroid/graphics/Bitmap;

    .line 289
    iget-object v0, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mCache:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 290
    iget-object v0, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mIdList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 291
    iget-object v0, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mIndexList:Ljava/util/ArrayList;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public clean()V
    .locals 4

    .prologue
    .line 307
    iget-object v3, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mIdList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 308
    .local v2, id:Ljava/lang/String;
    iget-object v3, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mCache:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 309
    .local v0, b:Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 310
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 313
    .end local v0           #b:Landroid/graphics/Bitmap;
    .end local v2           #id:Ljava/lang/String;
    :cond_1
    iget-object v3, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mCache:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->clear()V

    .line 314
    iget-object v3, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mIdList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 315
    iget-object v3, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mIndexList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 316
    return-void
.end method

.method public getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 1
    .parameter "id"

    .prologue
    .line 281
    iget-object v0, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mCache:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getCurrentUseIndex()I
    .locals 1

    .prologue
    .line 299
    iget v0, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mCurrentIndex:I

    return v0
.end method

.method public inCacheScope(I)Z
    .locals 2
    .parameter "index"

    .prologue
    .line 303
    iget v0, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mCurrentIndex:I

    sub-int v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v1, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mCacheCapacity:I

    div-int/lit8 v1, v1, 0x2

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFull()Z
    .locals 2

    .prologue
    .line 319
    iget-object v0, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mCache:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    iget v1, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mCacheCapacity:I

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeIdleCache(Z)Landroid/graphics/Bitmap;
    .locals 1
    .parameter "returnBitmap"

    .prologue
    .line 338
    const/16 v0, -0x3e7

    invoke-virtual {p0, p1, v0}, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->removeIdleCache(ZI)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public removeIdleCache(ZI)Landroid/graphics/Bitmap;
    .locals 5
    .parameter "returnBitmap"
    .parameter "forIndex"

    .prologue
    .line 342
    const/4 v1, 0x0

    .line 343
    .local v1, ret:Landroid/graphics/Bitmap;
    invoke-direct {p0, p2}, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->getNextRemoveCachePosition(I)I

    move-result v2

    .line 344
    .local v2, selectIndex:I
    if-ltz v2, :cond_1

    iget-object v3, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mIndexList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 345
    iget-object v3, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mCache:Ljava/util/Map;

    iget-object v4, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mIdList:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 346
    .local v0, b:Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 347
    if-eqz p1, :cond_2

    .line 348
    move-object v1, v0

    .line 353
    :cond_0
    :goto_0
    iget-object v3, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mIdList:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 354
    iget-object v3, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mIndexList:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 356
    .end local v0           #b:Landroid/graphics/Bitmap;
    :cond_1
    return-object v1

    .line 350
    .restart local v0       #b:Landroid/graphics/Bitmap;
    :cond_2
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0
.end method

.method public setCacheSize(I)V
    .locals 1
    .parameter "newSize"

    .prologue
    .line 272
    const/4 v0, 0x1

    if-le p1, v0, :cond_1

    iget v0, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mCacheCapacity:I

    if-eq p1, v0, :cond_1

    .line 273
    and-int/lit8 v0, p1, 0x1

    if-nez v0, :cond_0

    .line 274
    add-int/lit8 p1, p1, 0x1

    .line 276
    :cond_0
    iput p1, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mCacheCapacity:I

    .line 278
    :cond_1
    return-void
.end method

.method public setCurrentUseIndex(I)V
    .locals 0
    .parameter "index"

    .prologue
    .line 295
    iput p1, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mCurrentIndex:I

    .line 296
    return-void
.end method
