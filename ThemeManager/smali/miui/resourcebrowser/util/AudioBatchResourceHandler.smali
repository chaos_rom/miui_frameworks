.class public Lmiui/resourcebrowser/util/AudioBatchResourceHandler;
.super Lmiui/resourcebrowser/util/BatchResourceHandler;
.source "AudioBatchResourceHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/resourcebrowser/util/AudioBatchResourceHandler$1;,
        Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;
    }
.end annotation


# instance fields
.field private mMusicPlayer:Lmiui/resourcebrowser/util/ResourceMusicPlayer;

.field private mMusicUI:Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;


# direct methods
.method public constructor <init>(Lmiui/resourcebrowser/activity/ResourceListFragment;Lmiui/resourcebrowser/activity/ResourceAdapter;Lmiui/resourcebrowser/ResourceContext;)V
    .locals 3
    .parameter "fragment"
    .parameter "adapter"
    .parameter "resContext"

    .prologue
    .line 33
    invoke-direct {p0, p1, p2, p3}, Lmiui/resourcebrowser/util/BatchResourceHandler;-><init>(Lmiui/resourcebrowser/activity/ResourceListFragment;Lmiui/resourcebrowser/activity/ResourceAdapter;Lmiui/resourcebrowser/ResourceContext;)V

    .line 34
    new-instance v0, Lmiui/resourcebrowser/util/ResourceMusicPlayer;

    iget-object v1, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->mActivity:Landroid/app/Activity;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lmiui/resourcebrowser/util/ResourceMusicPlayer;-><init>(Landroid/app/Activity;Z)V

    iput-object v0, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->mMusicPlayer:Lmiui/resourcebrowser/util/ResourceMusicPlayer;

    .line 35
    new-instance v0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;-><init>(Lmiui/resourcebrowser/util/AudioBatchResourceHandler;Lmiui/resourcebrowser/util/AudioBatchResourceHandler$1;)V

    iput-object v0, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->mMusicUI:Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;

    .line 36
    iget-object v0, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->mMusicPlayer:Lmiui/resourcebrowser/util/ResourceMusicPlayer;

    iget-object v1, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->mMusicUI:Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;

    invoke-virtual {v1}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->getMusicPlayListener()Lmiui/resourcebrowser/util/ResourceMusicPlayer$PlayProgressListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->regeistePlayProgressListener(Lmiui/resourcebrowser/util/ResourceMusicPlayer$PlayProgressListener;)V

    .line 37
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->mListenDownloadProgress:Z

    .line 38
    return-void
.end method

.method static synthetic access$700(Lmiui/resourcebrowser/util/AudioBatchResourceHandler;)Lmiui/resourcebrowser/util/ResourceMusicPlayer;
    .locals 1
    .parameter "x0"

    .prologue
    .line 27
    iget-object v0, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->mMusicPlayer:Lmiui/resourcebrowser/util/ResourceMusicPlayer;

    return-object v0
.end method

.method static synthetic access$800(Lmiui/resourcebrowser/util/AudioBatchResourceHandler;)Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;
    .locals 1
    .parameter "x0"

    .prologue
    .line 27
    iget-object v0, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->mMusicUI:Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;

    return-object v0
.end method

.method private clickMusicView(Landroid/view/View;)V
    .locals 4
    .parameter "view"

    .prologue
    .line 71
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    .line 72
    .local v1, position:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    invoke-virtual {p0, v1}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->getResource(Landroid/util/Pair;)Lmiui/resourcebrowser/model/Resource;

    move-result-object v2

    .line 74
    .local v2, r:Lmiui/resourcebrowser/model/Resource;
    iget-object v3, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->mMusicPlayer:Lmiui/resourcebrowser/util/ResourceMusicPlayer;

    invoke-virtual {v3, v2}, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->canPlay(Lmiui/resourcebrowser/model/Resource;)Z

    move-result v0

    .line 75
    .local v0, canPlay:Z
    iget-object v3, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->mMusicPlayer:Lmiui/resourcebrowser/util/ResourceMusicPlayer;

    invoke-virtual {v3}, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->stopMusic()V

    .line 76
    if-eqz v0, :cond_0

    .line 77
    iget-object v3, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->mMusicPlayer:Lmiui/resourcebrowser/util/ResourceMusicPlayer;

    invoke-virtual {v3, v2}, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->playMusic(Lmiui/resourcebrowser/model/Resource;)V

    .line 80
    :cond_0
    iget-object v3, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->mMusicUI:Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;

    invoke-virtual {v3, p1}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->clickMusicUI(Landroid/view/View;)V

    .line 81
    return-void
.end method


# virtual methods
.method protected enterEditMode(Landroid/view/View;Landroid/util/Pair;)V
    .locals 1
    .parameter "v"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 48
    .local p2, position:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    invoke-super {p0, p1, p2}, Lmiui/resourcebrowser/util/BatchResourceHandler;->enterEditMode(Landroid/view/View;Landroid/util/Pair;)V

    .line 49
    iget-object v0, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->mMusicPlayer:Lmiui/resourcebrowser/util/ResourceMusicPlayer;

    invoke-virtual {v0}, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->stopMusic()V

    .line 50
    iget-object v0, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->mMusicUI:Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;

    invoke-virtual {v0}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->reset()V

    .line 51
    return-void
.end method

.method protected handleApplyEvent(Lmiui/resourcebrowser/model/Resource;)V
    .locals 0
    .parameter "r"

    .prologue
    .line 90
    return-void
.end method

.method protected handleCancelDownloadEvent(Lmiui/resourcebrowser/model/Resource;)V
    .locals 0
    .parameter "r"

    .prologue
    .line 105
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->doCancelDownload(Lmiui/resourcebrowser/model/Resource;)V

    .line 106
    return-void
.end method

.method protected handleDownloadEvent(Lmiui/resourcebrowser/model/Resource;)V
    .locals 0
    .parameter "r"

    .prologue
    .line 101
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->doDownloadResource(Lmiui/resourcebrowser/model/Resource;)V

    .line 102
    return-void
.end method

.method protected handlePickEvent(Lmiui/resourcebrowser/model/Resource;)V
    .locals 3
    .parameter "r"

    .prologue
    .line 93
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 94
    .local v0, resultIntent:Landroid/content/Intent;
    const-string v1, "RESPONSE_PICKED_RESOURCE"

    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 95
    const-string v1, "android.intent.extra.ringtone.PICKED_URI"

    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lmiui/resourcebrowser/util/ResourceHelper;->getUriByPath(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 96
    iget-object v1, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->mActivity:Landroid/app/Activity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 97
    iget-object v1, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    .line 98
    return-void
.end method

.method public initViewState(Landroid/view/View;Landroid/util/Pair;)V
    .locals 1
    .parameter "v"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 42
    .local p2, position:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    invoke-super {p0, p1, p2}, Lmiui/resourcebrowser/util/BatchResourceHandler;->initViewState(Landroid/view/View;Landroid/util/Pair;)V

    .line 43
    iget-object v0, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->mMusicUI:Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;

    invoke-virtual {v0, p1}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->initMusicUI(Landroid/view/View;)V

    .line 44
    return-void
.end method

.method protected onClickEventPerformed(Landroid/view/View;)V
    .locals 2
    .parameter "v"

    .prologue
    .line 62
    invoke-virtual {p0}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->isEditMode()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v0}, Lmiui/resourcebrowser/ResourceContext;->getDisplayType()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 63
    invoke-direct {p0, p1}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->clickMusicView(Landroid/view/View;)V

    .line 67
    :goto_0
    return-void

    .line 65
    :cond_0
    invoke-super {p0, p1}, Lmiui/resourcebrowser/util/BatchResourceHandler;->onClickEventPerformed(Landroid/view/View;)V

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 85
    invoke-super {p0}, Lmiui/resourcebrowser/util/BatchResourceHandler;->onPause()V

    .line 86
    iget-object v0, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->mMusicPlayer:Lmiui/resourcebrowser/util/ResourceMusicPlayer;

    invoke-virtual {v0}, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->stopMusic()V

    .line 87
    return-void
.end method

.method public quitEditMode()V
    .locals 1

    .prologue
    .line 55
    invoke-super {p0}, Lmiui/resourcebrowser/util/BatchResourceHandler;->quitEditMode()V

    .line 56
    iget-object v0, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->mMusicPlayer:Lmiui/resourcebrowser/util/ResourceMusicPlayer;

    invoke-virtual {v0}, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->stopMusic()V

    .line 57
    iget-object v0, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->mMusicUI:Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;

    invoke-virtual {v0}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->reset()V

    .line 58
    return-void
.end method
