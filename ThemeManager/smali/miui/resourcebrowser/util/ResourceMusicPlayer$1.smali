.class Lmiui/resourcebrowser/util/ResourceMusicPlayer$1;
.super Landroid/os/Handler;
.source "ResourceMusicPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/resourcebrowser/util/ResourceMusicPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/resourcebrowser/util/ResourceMusicPlayer;


# direct methods
.method constructor <init>(Lmiui/resourcebrowser/util/ResourceMusicPlayer;)V
    .locals 0
    .parameter

    .prologue
    .line 39
    iput-object p1, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer$1;->this$0:Lmiui/resourcebrowser/util/ResourceMusicPlayer;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .parameter "msg"

    .prologue
    .line 41
    iget v0, p1, Landroid/os/Message;->what:I

    if-nez v0, :cond_0

    .line 42
    iget-object v0, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer$1;->this$0:Lmiui/resourcebrowser/util/ResourceMusicPlayer;

    #getter for: Lmiui/resourcebrowser/util/ResourceMusicPlayer;->mBatchPlayer:Lmiui/resourcebrowser/util/BatchMediaPlayer;
    invoke-static {v0}, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->access$000(Lmiui/resourcebrowser/util/ResourceMusicPlayer;)Lmiui/resourcebrowser/util/BatchMediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer$1;->this$0:Lmiui/resourcebrowser/util/ResourceMusicPlayer;

    #getter for: Lmiui/resourcebrowser/util/ResourceMusicPlayer;->mProgressListener:Lmiui/resourcebrowser/util/ResourceMusicPlayer$PlayProgressListener;
    invoke-static {v0}, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->access$100(Lmiui/resourcebrowser/util/ResourceMusicPlayer;)Lmiui/resourcebrowser/util/ResourceMusicPlayer$PlayProgressListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer$1;->this$0:Lmiui/resourcebrowser/util/ResourceMusicPlayer;

    #getter for: Lmiui/resourcebrowser/util/ResourceMusicPlayer;->mBatchPlayer:Lmiui/resourcebrowser/util/BatchMediaPlayer;
    invoke-static {v0}, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->access$000(Lmiui/resourcebrowser/util/ResourceMusicPlayer;)Lmiui/resourcebrowser/util/BatchMediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/resourcebrowser/util/BatchMediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer$1;->this$0:Lmiui/resourcebrowser/util/ResourceMusicPlayer;

    #getter for: Lmiui/resourcebrowser/util/ResourceMusicPlayer;->mProgressListener:Lmiui/resourcebrowser/util/ResourceMusicPlayer$PlayProgressListener;
    invoke-static {v0}, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->access$100(Lmiui/resourcebrowser/util/ResourceMusicPlayer;)Lmiui/resourcebrowser/util/ResourceMusicPlayer$PlayProgressListener;

    move-result-object v0

    iget-object v1, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer$1;->this$0:Lmiui/resourcebrowser/util/ResourceMusicPlayer;

    #getter for: Lmiui/resourcebrowser/util/ResourceMusicPlayer;->mBatchPlayer:Lmiui/resourcebrowser/util/BatchMediaPlayer;
    invoke-static {v1}, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->access$000(Lmiui/resourcebrowser/util/ResourceMusicPlayer;)Lmiui/resourcebrowser/util/BatchMediaPlayer;

    move-result-object v1

    invoke-virtual {v1}, Lmiui/resourcebrowser/util/BatchMediaPlayer;->getPlayedDuration()I

    move-result v1

    iget-object v2, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer$1;->this$0:Lmiui/resourcebrowser/util/ResourceMusicPlayer;

    #getter for: Lmiui/resourcebrowser/util/ResourceMusicPlayer;->mBatchPlayer:Lmiui/resourcebrowser/util/BatchMediaPlayer;
    invoke-static {v2}, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->access$000(Lmiui/resourcebrowser/util/ResourceMusicPlayer;)Lmiui/resourcebrowser/util/BatchMediaPlayer;

    move-result-object v2

    invoke-virtual {v2}, Lmiui/resourcebrowser/util/BatchMediaPlayer;->getTotalDuration()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lmiui/resourcebrowser/util/ResourceMusicPlayer$PlayProgressListener;->onProgressUpdate(II)V

    .line 45
    iget-object v0, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer$1;->this$0:Lmiui/resourcebrowser/util/ResourceMusicPlayer;

    #getter for: Lmiui/resourcebrowser/util/ResourceMusicPlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->access$200(Lmiui/resourcebrowser/util/ResourceMusicPlayer;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x0

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 48
    :cond_0
    return-void
.end method
