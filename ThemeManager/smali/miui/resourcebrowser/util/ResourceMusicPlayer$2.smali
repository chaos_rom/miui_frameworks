.class Lmiui/resourcebrowser/util/ResourceMusicPlayer$2;
.super Ljava/lang/Object;
.source "ResourceMusicPlayer.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiui/resourcebrowser/util/ResourceMusicPlayer;->initPlayButtonIfNeed(Landroid/widget/ImageView;Lmiui/resourcebrowser/model/Resource;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/resourcebrowser/util/ResourceMusicPlayer;

.field final synthetic val$data:Lmiui/resourcebrowser/model/Resource;

.field final synthetic val$view:Landroid/widget/ImageView;


# direct methods
.method constructor <init>(Lmiui/resourcebrowser/util/ResourceMusicPlayer;Lmiui/resourcebrowser/model/Resource;Landroid/widget/ImageView;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 65
    iput-object p1, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer$2;->this$0:Lmiui/resourcebrowser/util/ResourceMusicPlayer;

    iput-object p2, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer$2;->val$data:Lmiui/resourcebrowser/model/Resource;

    iput-object p3, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer$2;->val$view:Landroid/widget/ImageView;

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .parameter "v"

    .prologue
    .line 68
    iget-object v1, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer$2;->val$data:Lmiui/resourcebrowser/model/Resource;

    iget-object v2, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer$2;->this$0:Lmiui/resourcebrowser/util/ResourceMusicPlayer;

    #getter for: Lmiui/resourcebrowser/util/ResourceMusicPlayer;->mCurrentPlayingResource:Lmiui/resourcebrowser/model/Resource;
    invoke-static {v2}, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->access$300(Lmiui/resourcebrowser/util/ResourceMusicPlayer;)Lmiui/resourcebrowser/model/Resource;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmiui/resourcebrowser/model/Resource;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, 0x1

    .line 69
    .local v0, isGoingToPlay:Z
    :goto_0
    iget-object v1, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer$2;->this$0:Lmiui/resourcebrowser/util/ResourceMusicPlayer;

    invoke-virtual {v1}, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->stopMusic()V

    .line 70
    if-eqz v0, :cond_0

    iget-object v1, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer$2;->val$data:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v1}, Lmiui/resourcebrowser/model/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 71
    iget-object v1, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer$2;->this$0:Lmiui/resourcebrowser/util/ResourceMusicPlayer;

    iget-object v2, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer$2;->val$view:Landroid/widget/ImageView;

    iget-object v3, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer$2;->val$data:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v1, v2, v3}, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->playMusic(Landroid/widget/ImageView;Lmiui/resourcebrowser/model/Resource;)V

    .line 73
    :cond_0
    return-void

    .line 68
    .end local v0           #isGoingToPlay:Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
