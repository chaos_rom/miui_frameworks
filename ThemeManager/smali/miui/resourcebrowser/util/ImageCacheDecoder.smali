.class public Lmiui/resourcebrowser/util/ImageCacheDecoder;
.super Ljava/lang/Object;
.source "ImageCacheDecoder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;,
        Lmiui/resourcebrowser/util/ImageCacheDecoder$ImageDecodingListener;,
        Lmiui/resourcebrowser/util/ImageCacheDecoder$JobRun;
    }
.end annotation


# instance fields
.field protected mBitmapCache:Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;

.field private mDecodeExecutorService:Ljava/util/concurrent/ExecutorService;

.field protected mDecodedHeight:I

.field protected mDecodedWidth:I

.field private mDoingJob:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mDownloadExecutorService:Ljava/util/concurrent/ExecutorService;

.field private mFailToDownloadTime:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mHandler:Landroid/os/Handler;

.field private mListener:Lmiui/resourcebrowser/util/ImageCacheDecoder$ImageDecodingListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lmiui/resourcebrowser/util/ImageCacheDecoder;-><init>(I)V

    .line 78
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .parameter "cacheSize"

    .prologue
    .line 80
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder;->mDoingJob:Ljava/util/HashSet;

    .line 40
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder;->mDecodeExecutorService:Ljava/util/concurrent/ExecutorService;

    .line 41
    const/4 v0, 0x4

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder;->mDownloadExecutorService:Ljava/util/concurrent/ExecutorService;

    .line 43
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder;->mFailToDownloadTime:Ljava/util/HashMap;

    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder;->mListener:Lmiui/resourcebrowser/util/ImageCacheDecoder$ImageDecodingListener;

    .line 50
    new-instance v0, Lmiui/resourcebrowser/util/ImageCacheDecoder$1;

    invoke-direct {v0, p0}, Lmiui/resourcebrowser/util/ImageCacheDecoder$1;-><init>(Lmiui/resourcebrowser/util/ImageCacheDecoder;)V

    iput-object v0, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder;->mHandler:Landroid/os/Handler;

    .line 81
    new-instance v0, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;

    invoke-direct {v0, p0, p1}, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;-><init>(Lmiui/resourcebrowser/util/ImageCacheDecoder;I)V

    iput-object v0, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder;->mBitmapCache:Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;

    .line 82
    return-void
.end method

.method static synthetic access$000(Lmiui/resourcebrowser/util/ImageCacheDecoder;)Lmiui/resourcebrowser/util/ImageCacheDecoder$ImageDecodingListener;
    .locals 1
    .parameter "x0"

    .prologue
    .line 31
    iget-object v0, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder;->mListener:Lmiui/resourcebrowser/util/ImageCacheDecoder$ImageDecodingListener;

    return-object v0
.end method

.method static synthetic access$100(Lmiui/resourcebrowser/util/ImageCacheDecoder;)Ljava/util/concurrent/ExecutorService;
    .locals 1
    .parameter "x0"

    .prologue
    .line 31
    iget-object v0, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder;->mDownloadExecutorService:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method static synthetic access$200(Lmiui/resourcebrowser/util/ImageCacheDecoder;)Ljava/util/concurrent/ExecutorService;
    .locals 1
    .parameter "x0"

    .prologue
    .line 31
    iget-object v0, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder;->mDecodeExecutorService:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method static synthetic access$300(Lmiui/resourcebrowser/util/ImageCacheDecoder;)Ljava/util/HashMap;
    .locals 1
    .parameter "x0"

    .prologue
    .line 31
    iget-object v0, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder;->mFailToDownloadTime:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$400(Lmiui/resourcebrowser/util/ImageCacheDecoder;)Ljava/util/HashSet;
    .locals 1
    .parameter "x0"

    .prologue
    .line 31
    iget-object v0, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder;->mDoingJob:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic access$500(Lmiui/resourcebrowser/util/ImageCacheDecoder;)Landroid/os/Handler;
    .locals 1
    .parameter "x0"

    .prologue
    .line 31
    iget-object v0, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder;->mHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public clean(Z)V
    .locals 2
    .parameter "stopBgThread"

    .prologue
    .line 141
    iget-object v0, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder;->mBitmapCache:Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;

    invoke-virtual {v0}, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->clean()V

    .line 142
    iget-object v0, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder;->mFailToDownloadTime:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 143
    if-eqz p1, :cond_0

    .line 144
    iget-object v0, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder;->mDecodeExecutorService:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 145
    iget-object v0, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder;->mDownloadExecutorService:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 146
    iget-object v0, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 147
    iget-object v0, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 149
    :cond_0
    return-void
.end method

.method public decodeImageAsync(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 5
    .parameter "imageLocalPath"
    .parameter "imageOnlinePath"
    .parameter "useIndex"

    .prologue
    .line 124
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder;->mBitmapCache:Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;

    invoke-virtual {v1, p1}, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder;->mDoingJob:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder;->mDecodeExecutorService:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1}, Ljava/util/concurrent/ExecutorService;->isShutdown()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 138
    :cond_0
    :goto_0
    return-void

    .line 130
    :cond_1
    iget-object v1, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder;->mFailToDownloadTime:Ljava/util/HashMap;

    invoke-virtual {v1, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 131
    .local v0, lastFailToDownloadTime:Ljava/lang/Long;
    if-eqz v0, :cond_2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    sub-long/2addr v1, v3

    const-wide/16 v3, 0x1388

    cmp-long v1, v1, v3

    if-ltz v1, :cond_0

    .line 136
    :cond_2
    iget-object v1, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder;->mDoingJob:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 137
    new-instance v1, Lmiui/resourcebrowser/util/ImageCacheDecoder$JobRun;

    invoke-direct {v1, p0, p1, p2, p3}, Lmiui/resourcebrowser/util/ImageCacheDecoder$JobRun;-><init>(Lmiui/resourcebrowser/util/ImageCacheDecoder;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v1}, Lmiui/resourcebrowser/util/ImageCacheDecoder$JobRun;->dispatchJob()V

    goto :goto_0
.end method

.method public decodeLocalImage(Ljava/lang/String;IZ)Landroid/graphics/Bitmap;
    .locals 6
    .parameter "imagePath"
    .parameter "useIndex"
    .parameter "addIntoCache"

    .prologue
    .line 106
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/util/ImageCacheDecoder;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 107
    .local v0, b:Landroid/graphics/Bitmap;
    if-nez v0, :cond_0

    .line 108
    new-instance v1, Lmiui/util/InputStreamLoader;

    invoke-direct {v1, p1}, Lmiui/util/InputStreamLoader;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder;->mDecodedWidth:I

    iget v3, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder;->mDecodedHeight:I

    iget-object v4, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder;->mBitmapCache:Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->removeIdleCache(Z)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lmiui/util/ImageUtils;->getBitmap(Lmiui/util/InputStreamLoader;IILandroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 109
    if-eqz v0, :cond_0

    if-eqz p3, :cond_0

    .line 110
    iget-object v1, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder;->mBitmapCache:Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;

    invoke-virtual {v1, p1, v0, p2}, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->add(Ljava/lang/String;Landroid/graphics/Bitmap;I)V

    .line 113
    :cond_0
    return-object v0
.end method

.method public getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 1
    .parameter "imagePath"

    .prologue
    .line 102
    iget-object v0, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder;->mBitmapCache:Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;

    invoke-virtual {v0, p1}, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentUseBitmapIndex()I
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder;->mBitmapCache:Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;

    invoke-virtual {v0}, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->getCurrentUseIndex()I

    move-result v0

    return v0
.end method

.method public registerListener(Lmiui/resourcebrowser/util/ImageCacheDecoder$ImageDecodingListener;)V
    .locals 0
    .parameter "l"

    .prologue
    .line 70
    iput-object p1, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder;->mListener:Lmiui/resourcebrowser/util/ImageCacheDecoder$ImageDecodingListener;

    .line 71
    return-void
.end method

.method public setCurrentUseBitmapIndex(I)V
    .locals 1
    .parameter "index"

    .prologue
    .line 94
    iget-object v0, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder;->mBitmapCache:Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;

    invoke-virtual {v0, p1}, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->setCurrentUseIndex(I)V

    .line 95
    return-void
.end method

.method public setScaledSize(II)V
    .locals 0
    .parameter "scaledWidth"
    .parameter "scaledHeight"

    .prologue
    .line 85
    iput p1, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder;->mDecodedWidth:I

    .line 86
    iput p2, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder;->mDecodedHeight:I

    .line 87
    return-void
.end method
