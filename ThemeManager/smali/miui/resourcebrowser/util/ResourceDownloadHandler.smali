.class public Lmiui/resourcebrowser/util/ResourceDownloadHandler;
.super Ljava/lang/Object;
.source "ResourceDownloadHandler.java"

# interfaces
.implements Lmiui/resourcebrowser/ResourceConstants;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/resourcebrowser/util/ResourceDownloadHandler$ResourceDownloadListener;
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mContextParser:Lmiui/resourcebrowser/controller/local/ContextParser;

.field private mDataParser:Lmiui/resourcebrowser/controller/local/LocalDataParser;

.field private mDownloadManager:Landroid/app/DownloadManager;

.field private mDownloadReceiver:Landroid/content/BroadcastReceiver;

.field private mHasRegistDownloadReceiver:Z

.field private mLastQueryOnlineId:Ljava/lang/String;

.field private mLastQueryResult:Z

.field private mLastQueryTime:Ljava/lang/Long;

.field private mListener:Lmiui/resourcebrowser/util/ResourceDownloadHandler$ResourceDownloadListener;

.field private mResContext:Lmiui/resourcebrowser/ResourceContext;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lmiui/resourcebrowser/ResourceContext;)V
    .locals 2
    .parameter "context"
    .parameter "resContext"

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->mHasRegistDownloadReceiver:Z

    .line 40
    new-instance v0, Lmiui/resourcebrowser/util/ResourceDownloadHandler$1;

    invoke-direct {v0, p0}, Lmiui/resourcebrowser/util/ResourceDownloadHandler$1;-><init>(Lmiui/resourcebrowser/util/ResourceDownloadHandler;)V

    iput-object v0, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->mDownloadReceiver:Landroid/content/BroadcastReceiver;

    .line 90
    iput-object p1, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->mContext:Landroid/content/Context;

    .line 91
    iput-object p2, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    .line 92
    new-instance v0, Lmiui/resourcebrowser/controller/local/LocalSerializableDataParser;

    invoke-direct {v0, p2}, Lmiui/resourcebrowser/controller/local/LocalSerializableDataParser;-><init>(Lmiui/resourcebrowser/ResourceContext;)V

    iput-object v0, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->mDataParser:Lmiui/resourcebrowser/controller/local/LocalDataParser;

    .line 93
    new-instance v0, Lmiui/resourcebrowser/controller/local/ContextSerializableParser;

    invoke-direct {v0}, Lmiui/resourcebrowser/controller/local/ContextSerializableParser;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->mContextParser:Lmiui/resourcebrowser/controller/local/ContextParser;

    .line 94
    iget-object v0, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->mContext:Landroid/content/Context;

    const-string v1, "download"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/DownloadManager;

    iput-object v0, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->mDownloadManager:Landroid/app/DownloadManager;

    .line 95
    return-void
.end method

.method static synthetic access$000(Lmiui/resourcebrowser/util/ResourceDownloadHandler;)Landroid/app/DownloadManager;
    .locals 1
    .parameter "x0"

    .prologue
    .line 30
    iget-object v0, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->mDownloadManager:Landroid/app/DownloadManager;

    return-object v0
.end method

.method static synthetic access$100(Lmiui/resourcebrowser/util/ResourceDownloadHandler;)Lmiui/resourcebrowser/util/ResourceDownloadHandler$ResourceDownloadListener;
    .locals 1
    .parameter "x0"

    .prologue
    .line 30
    iget-object v0, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->mListener:Lmiui/resourcebrowser/util/ResourceDownloadHandler$ResourceDownloadListener;

    return-object v0
.end method

.method private static getMimeType(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .parameter "url"

    .prologue
    const/4 v4, -0x1

    .line 288
    move-object v2, p0

    .line 289
    .local v2, name:Ljava/lang/String;
    const-string v0, ""

    .line 290
    .local v0, extension:Ljava/lang/String;
    const/16 v3, 0x2f

    invoke-virtual {p0, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 291
    .local v1, index:I
    if-eq v4, v1, :cond_0

    .line 292
    add-int/lit8 v3, v1, 0x1

    invoke-virtual {p0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 294
    :cond_0
    const/16 v3, 0x2e

    invoke-virtual {v2, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 295
    if-eq v4, v1, :cond_1

    .line 296
    add-int/lit8 v3, v1, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 298
    :cond_1
    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private static getUriFromUrl(Ljava/lang/String;)Ljava/net/URI;
    .locals 12
    .parameter "url"

    .prologue
    const/4 v2, -0x1

    .line 258
    const/4 v10, 0x0

    .line 261
    .local v10, uri:Ljava/net/URI;
    :try_start_0
    new-instance v9, Ljava/lang/String;

    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-static {v1}, Landroid/webkit/URLUtil;->decode([B)[B

    move-result-object v1

    invoke-direct {v9, v1}, Ljava/lang/String;-><init>([B)V

    .line 263
    .local v9, newUrl:Ljava/lang/String;
    new-instance v11, Landroid/net/WebAddress;

    invoke-direct {v11, v9}, Landroid/net/WebAddress;-><init>(Ljava/lang/String;)V

    .line 264
    .local v11, w:Landroid/net/WebAddress;
    const/4 v7, 0x0

    .line 265
    .local v7, frag:Ljava/lang/String;
    const/4 v6, 0x0

    .line 266
    .local v6, query:Ljava/lang/String;
    invoke-virtual {v11}, Landroid/net/WebAddress;->getPath()Ljava/lang/String;

    move-result-object v5

    .line 268
    .local v5, path:Ljava/lang/String;
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 270
    const/16 v1, 0x23

    invoke-virtual {v5, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v8

    .line 271
    .local v8, idx:I
    if-eq v8, v2, :cond_0

    .line 272
    add-int/lit8 v1, v8, 0x1

    invoke-virtual {v5, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    .line 273
    const/4 v1, 0x0

    invoke-virtual {v5, v1, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 275
    :cond_0
    const/16 v1, 0x3f

    invoke-virtual {v5, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v8

    .line 276
    if-eq v8, v2, :cond_1

    .line 277
    add-int/lit8 v1, v8, 0x1

    invoke-virtual {v5, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    .line 278
    const/4 v1, 0x0

    invoke-virtual {v5, v1, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 281
    .end local v8           #idx:I
    :cond_1
    new-instance v0, Ljava/net/URI;

    invoke-virtual {v11}, Landroid/net/WebAddress;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11}, Landroid/net/WebAddress;->getAuthInfo()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v11}, Landroid/net/WebAddress;->getHost()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v11}, Landroid/net/WebAddress;->getPort()I

    move-result v4

    invoke-direct/range {v0 .. v7}, Ljava/net/URI;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 284
    .end local v5           #path:Ljava/lang/String;
    .end local v6           #query:Ljava/lang/String;
    .end local v7           #frag:Ljava/lang/String;
    .end local v9           #newUrl:Ljava/lang/String;
    .end local v10           #uri:Ljava/net/URI;
    .end local v11           #w:Landroid/net/WebAddress;
    .local v0, uri:Ljava/net/URI;
    :goto_0
    return-object v0

    .line 282
    .end local v0           #uri:Ljava/net/URI;
    .restart local v10       #uri:Ljava/net/URI;
    :catch_0
    move-exception v1

    move-object v0, v10

    .end local v10           #uri:Ljava/net/URI;
    .restart local v0       #uri:Ljava/net/URI;
    goto :goto_0
.end method


# virtual methods
.method public cancelDownload(Ljava/lang/String;)Z
    .locals 9
    .parameter "onlineId"

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 147
    new-instance v3, Landroid/app/MiuiDownloadManager$Query;

    invoke-direct {v3}, Landroid/app/MiuiDownloadManager$Query;-><init>()V

    .line 148
    .local v3, query:Landroid/app/MiuiDownloadManager$Query;
    invoke-virtual {v3, p1}, Landroid/app/MiuiDownloadManager$Query;->setFilterByAppData(Ljava/lang/String;)Landroid/app/MiuiDownloadManager$Query;

    .line 149
    iget-object v7, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->mDownloadManager:Landroid/app/DownloadManager;

    invoke-virtual {v7, v3}, Landroid/app/DownloadManager;->query(Landroid/app/DownloadManager$Query;)Landroid/database/Cursor;

    move-result-object v0

    .line 150
    .local v0, cursor:Landroid/database/Cursor;
    const/4 v4, 0x0

    .line 151
    .local v4, ret:I
    if-eqz v0, :cond_1

    .line 152
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 153
    const-string v7, "_id"

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    .line 154
    .local v1, downloadId:J
    iget-object v7, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->mDownloadManager:Landroid/app/DownloadManager;

    new-array v8, v5, [J

    aput-wide v1, v8, v6

    invoke-virtual {v7, v8}, Landroid/app/DownloadManager;->remove([J)I

    move-result v4

    .line 156
    .end local v1           #downloadId:J
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 158
    :cond_1
    if-ne v4, v5, :cond_2

    :goto_0
    return v5

    :cond_2
    move v5, v6

    goto :goto_0
.end method

.method public downloadResource(Landroid/app/DownloadManager;Lmiui/resourcebrowser/model/Resource;)J
    .locals 16
    .parameter "dmgr"
    .parameter "resource"

    .prologue
    .line 177
    invoke-virtual/range {p2 .. p2}, Lmiui/resourcebrowser/model/Resource;->getOnlinePath()Ljava/lang/String;

    move-result-object v2

    .line 178
    .local v2, downloadUrl:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Lmiui/resourcebrowser/model/Resource;->getDownloadPath()Ljava/lang/String;

    move-result-object v8

    .line 179
    .local v8, savePath:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Lmiui/resourcebrowser/model/Resource;->getTitle()Ljava/lang/String;

    move-result-object v5

    .line 180
    .local v5, notificationTitle:Ljava/lang/String;
    invoke-static {v2}, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->getUriFromUrl(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v11

    .line 181
    .local v11, uri:Ljava/net/URI;
    if-nez v11, :cond_0

    .line 182
    const-wide/16 v12, -0x1

    .line 212
    :goto_0
    return-wide v12

    .line 185
    :cond_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v12}, Lmiui/resourcebrowser/ResourceContext;->isSelfDescribing()Z

    move-result v12

    if-nez v12, :cond_1

    .line 186
    new-instance v4, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v12, v0, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v12}, Lmiui/resourcebrowser/ResourceContext;->getAsyncImportFolder()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v4, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 187
    .local v4, importFolder:Ljava/io/File;
    new-instance v7, Ljava/io/File;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p2 .. p2}, Lmiui/resourcebrowser/model/Resource;->getOnlineId()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ".mrm"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v7, v4, v12}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 188
    .local v7, resourceFile:Ljava/io/File;
    new-instance v1, Ljava/io/File;

    invoke-virtual/range {p2 .. p2}, Lmiui/resourcebrowser/model/Resource;->getOnlineId()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v1, v4, v12}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 190
    .local v1, contextFile:Ljava/io/File;
    :try_start_0
    invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z

    .line 191
    move-object/from16 v0, p0

    iget-object v12, v0, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->mDataParser:Lmiui/resourcebrowser/controller/local/LocalDataParser;

    move-object/from16 v0, p2

    invoke-virtual {v12, v7, v0}, Lmiui/resourcebrowser/controller/local/LocalDataParser;->storeResource(Ljava/io/File;Lmiui/resourcebrowser/model/Resource;)V

    .line 192
    move-object/from16 v0, p0

    iget-object v12, v0, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->mContextParser:Lmiui/resourcebrowser/controller/local/ContextParser;

    move-object/from16 v0, p0

    iget-object v13, v0, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v12, v1, v13}, Lmiui/resourcebrowser/controller/local/ContextParser;->storeResourceContext(Ljava/io/File;Lmiui/resourcebrowser/ResourceContext;)V
    :try_end_0
    .catch Lmiui/resourcebrowser/controller/local/PersistenceException; {:try_start_0 .. :try_end_0} :catch_0

    .line 198
    .end local v1           #contextFile:Ljava/io/File;
    .end local v4           #importFolder:Ljava/io/File;
    .end local v7           #resourceFile:Ljava/io/File;
    :cond_1
    :goto_1
    new-instance v9, Ljava/io/File;

    invoke-direct {v9, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 199
    .local v9, targetFile:Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v12

    const/16 v13, 0x1ff

    const/4 v14, -0x1

    const/4 v15, -0x1

    invoke-static {v12, v13, v14, v15}, Lmiui/os/ExtraFileUtils;->mkdirs(Ljava/io/File;III)Z

    .line 200
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ".temp"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 201
    .local v10, tmpDownloadPath:Ljava/lang/String;
    new-instance v12, Ljava/io/File;

    invoke-direct {v12, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12}, Ljava/io/File;->delete()Z

    .line 202
    new-instance v12, Ljava/io/File;

    invoke-direct {v12, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12}, Ljava/io/File;->delete()Z

    .line 204
    new-instance v6, Landroid/app/MiuiDownloadManager$Request;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v12

    invoke-direct {v6, v12}, Landroid/app/MiuiDownloadManager$Request;-><init>(Landroid/net/Uri;)V

    .line 205
    .local v6, request:Landroid/app/MiuiDownloadManager$Request;
    const/4 v12, 0x0

    invoke-virtual {v6, v12}, Landroid/app/MiuiDownloadManager$Request;->setNotificationVisibility(I)Landroid/app/DownloadManager$Request;

    .line 206
    invoke-static {v2}, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->getMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v6, v12}, Landroid/app/MiuiDownloadManager$Request;->setMimeType(Ljava/lang/String;)Landroid/app/DownloadManager$Request;

    .line 207
    invoke-virtual {v6, v5}, Landroid/app/MiuiDownloadManager$Request;->setTitle(Ljava/lang/CharSequence;)Landroid/app/DownloadManager$Request;

    .line 208
    invoke-static {v9}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v12

    invoke-virtual {v6, v12}, Landroid/app/MiuiDownloadManager$Request;->setDestinationUri(Landroid/net/Uri;)Landroid/app/DownloadManager$Request;

    .line 209
    invoke-virtual {v6, v10}, Landroid/app/MiuiDownloadManager$Request;->setAppointName(Ljava/lang/String;)Landroid/app/MiuiDownloadManager$Request;

    .line 210
    const/4 v12, 0x1

    invoke-virtual {v6, v12}, Landroid/app/MiuiDownloadManager$Request;->setVisibleInDownloadsUi(Z)Landroid/app/DownloadManager$Request;

    .line 211
    invoke-virtual/range {p2 .. p2}, Lmiui/resourcebrowser/model/Resource;->getOnlineId()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v6, v12}, Landroid/app/MiuiDownloadManager$Request;->setAppData(Ljava/lang/String;)Landroid/app/MiuiDownloadManager$Request;

    .line 212
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/app/DownloadManager;->enqueue(Landroid/app/DownloadManager$Request;)J

    move-result-wide v12

    goto/16 :goto_0

    .line 193
    .end local v6           #request:Landroid/app/MiuiDownloadManager$Request;
    .end local v9           #targetFile:Ljava/io/File;
    .end local v10           #tmpDownloadPath:Ljava/lang/String;
    .restart local v1       #contextFile:Ljava/io/File;
    .restart local v4       #importFolder:Ljava/io/File;
    .restart local v7       #resourceFile:Ljava/io/File;
    :catch_0
    move-exception v3

    .line 194
    .local v3, e:Lmiui/resourcebrowser/controller/local/PersistenceException;
    invoke-virtual {v3}, Lmiui/resourcebrowser/controller/local/PersistenceException;->printStackTrace()V

    goto :goto_1
.end method

.method public downloadResource(Lmiui/resourcebrowser/model/Resource;)Z
    .locals 1
    .parameter "resource"

    .prologue
    .line 162
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->downloadResource(Lmiui/resourcebrowser/model/Resource;Z)Z

    move-result v0

    return v0
.end method

.method public downloadResource(Lmiui/resourcebrowser/model/Resource;Z)Z
    .locals 9
    .parameter "resource"
    .parameter "listenDownloadProgress"

    .prologue
    const-wide/16 v7, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 166
    iget-object v4, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->mDownloadManager:Landroid/app/DownloadManager;

    invoke-virtual {p0, v4, p1}, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->downloadResource(Landroid/app/DownloadManager;Lmiui/resourcebrowser/model/Resource;)J

    move-result-wide v0

    .line 167
    .local v0, id:J
    cmp-long v4, v0, v7

    if-ltz v4, :cond_0

    if-eqz p2, :cond_0

    .line 168
    iget-object v4, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->mContext:Landroid/content/Context;

    new-array v5, v2, [J

    aput-wide v0, v5, v3

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Landroid/app/MiuiDownloadManager;->operateDownloadsNeedToUpdateProgress(Landroid/content/Context;[J[J)V

    .line 173
    :cond_0
    cmp-long v4, v0, v7

    if-ltz v4, :cond_1

    :goto_0
    return v2

    :cond_1
    move v2, v3

    goto :goto_0
.end method

.method public handleDownloadFailed(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter "path"
    .parameter "onlineId"

    .prologue
    .line 244
    iget-object v0, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->mListener:Lmiui/resourcebrowser/util/ResourceDownloadHandler$ResourceDownloadListener;

    if-eqz v0, :cond_0

    .line 245
    iget-object v0, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->mListener:Lmiui/resourcebrowser/util/ResourceDownloadHandler$ResourceDownloadListener;

    invoke-interface {v0, p1, p2}, Lmiui/resourcebrowser/util/ResourceDownloadHandler$ResourceDownloadListener;->onDownloadFailed(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    :cond_0
    return-void
.end method

.method public handleDownloadProgressUpdated(Ljava/lang/String;Ljava/lang/String;II)V
    .locals 1
    .parameter "path"
    .parameter "onlineId"
    .parameter "downloadBytes"
    .parameter "totalBytes"

    .prologue
    .line 250
    iget-object v0, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->mListener:Lmiui/resourcebrowser/util/ResourceDownloadHandler$ResourceDownloadListener;

    if-eqz v0, :cond_0

    .line 251
    iget-object v0, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->mListener:Lmiui/resourcebrowser/util/ResourceDownloadHandler$ResourceDownloadListener;

    invoke-interface {v0, p1, p2, p3, p4}, Lmiui/resourcebrowser/util/ResourceDownloadHandler$ResourceDownloadListener;->onDownloadProgressUpdated(Ljava/lang/String;Ljava/lang/String;II)V

    .line 253
    :cond_0
    return-void
.end method

.method public handleDownloadSuccessful(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .parameter "path"
    .parameter "onlineId"

    .prologue
    .line 229
    new-instance v0, Lmiui/resourcebrowser/util/ResourceDownloadHandler$2;

    iget-object v2, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "import-"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lmiui/resourcebrowser/util/ResourceDownloadHandler$2;-><init>(Lmiui/resourcebrowser/util/ResourceDownloadHandler;Lmiui/resourcebrowser/ResourceContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    sget-object v3, Lmiui/resourcebrowser/controller/local/ImportResourceTask;->IMPORT_BY_ONLINE_ID:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/util/ResourceDownloadHandler$2;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 241
    return-void
.end method

.method public isResourceDownloading(Ljava/lang/String;)Z
    .locals 8
    .parameter "onlineId"

    .prologue
    const/4 v3, 0x0

    .line 122
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 143
    :goto_0
    return v3

    .line 125
    :cond_0
    iget-object v4, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->mLastQueryOnlineId:Ljava/lang/String;

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-object v6, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->mLastQueryTime:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sub-long/2addr v4, v6

    const-wide/16 v6, 0x1f4

    cmp-long v4, v4, v6

    if-gez v4, :cond_1

    .line 126
    iget-boolean v3, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->mLastQueryResult:Z

    goto :goto_0

    .line 129
    :cond_1
    new-instance v1, Landroid/app/MiuiDownloadManager$Query;

    invoke-direct {v1}, Landroid/app/MiuiDownloadManager$Query;-><init>()V

    .line 130
    .local v1, query:Landroid/app/MiuiDownloadManager$Query;
    invoke-virtual {v1, p1}, Landroid/app/MiuiDownloadManager$Query;->setFilterByAppData(Ljava/lang/String;)Landroid/app/MiuiDownloadManager$Query;

    .line 131
    iget-object v4, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->mDownloadManager:Landroid/app/DownloadManager;

    invoke-virtual {v4, v1}, Landroid/app/DownloadManager;->query(Landroid/app/DownloadManager$Query;)Landroid/database/Cursor;

    move-result-object v0

    .line 133
    .local v0, cursor:Landroid/database/Cursor;
    const/4 v2, 0x0

    .line 134
    .local v2, result:Z
    if-eqz v0, :cond_2

    .line 135
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-static {v0}, Landroid/app/MiuiDownloadManager;->isDownloading(Landroid/database/Cursor;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v2, 0x1

    .line 136
    :goto_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 139
    :cond_2
    iput-boolean v2, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->mLastQueryResult:Z

    .line 140
    iput-object p1, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->mLastQueryOnlineId:Ljava/lang/String;

    .line 141
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->mLastQueryTime:Ljava/lang/Long;

    move v3, v2

    .line 143
    goto :goto_0

    :cond_3
    move v2, v3

    .line 135
    goto :goto_1
.end method

.method public registerDownloadReceiver()V
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->registerDownloadReceiver(Z)V

    .line 99
    return-void
.end method

.method public registerDownloadReceiver(Z)V
    .locals 3
    .parameter "listenDownloadProgress"

    .prologue
    .line 102
    iget-boolean v1, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->mHasRegistDownloadReceiver:Z

    if-nez v1, :cond_1

    .line 103
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 104
    .local v0, f:Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.DOWNLOAD_COMPLETE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 105
    if-eqz p1, :cond_0

    .line 106
    const-string v1, "android.intent.action.DOWNLOAD_UPDATED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 108
    :cond_0
    iget-object v1, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->mDownloadReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 110
    const/4 v1, 0x1

    iput-boolean v1, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->mHasRegistDownloadReceiver:Z

    .line 112
    .end local v0           #f:Landroid/content/IntentFilter;
    :cond_1
    return-void
.end method

.method public setResourceDownloadListener(Lmiui/resourcebrowser/util/ResourceDownloadHandler$ResourceDownloadListener;)V
    .locals 0
    .parameter "listener"

    .prologue
    .line 225
    iput-object p1, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->mListener:Lmiui/resourcebrowser/util/ResourceDownloadHandler$ResourceDownloadListener;

    .line 226
    return-void
.end method

.method public unregisterDownloadReceiver()V
    .locals 2

    .prologue
    .line 115
    iget-boolean v0, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->mHasRegistDownloadReceiver:Z

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->mDownloadReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 117
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->mHasRegistDownloadReceiver:Z

    .line 119
    :cond_0
    return-void
.end method
