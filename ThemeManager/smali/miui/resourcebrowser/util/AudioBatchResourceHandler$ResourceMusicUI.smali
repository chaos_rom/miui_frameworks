.class Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;
.super Ljava/lang/Object;
.source "AudioBatchResourceHandler.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/resourcebrowser/util/AudioBatchResourceHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ResourceMusicUI"
.end annotation


# instance fields
.field private mGoneViewWhenClick:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mLastPlayProgress:I

.field private mLastSelectPosition:Landroid/util/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectPosition:Landroid/util/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lmiui/resourcebrowser/util/AudioBatchResourceHandler;


# direct methods
.method private constructor <init>(Lmiui/resourcebrowser/util/AudioBatchResourceHandler;)V
    .locals 1
    .parameter

    .prologue
    .line 108
    iput-object p1, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/resourcebrowser/util/AudioBatchResourceHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 119
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->mGoneViewWhenClick:Ljava/util/List;

    return-void
.end method

.method synthetic constructor <init>(Lmiui/resourcebrowser/util/AudioBatchResourceHandler;Lmiui/resourcebrowser/util/AudioBatchResourceHandler$1;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 108
    invoke-direct {p0, p1}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;-><init>(Lmiui/resourcebrowser/util/AudioBatchResourceHandler;)V

    return-void
.end method

.method static synthetic access$100(Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;)I
    .locals 1
    .parameter "x0"

    .prologue
    .line 108
    iget v0, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->mLastPlayProgress:I

    return v0
.end method

.method static synthetic access$102(Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;I)I
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 108
    iput p1, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->mLastPlayProgress:I

    return p1
.end method

.method static synthetic access$200(Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;)Ljava/util/List;
    .locals 1
    .parameter "x0"

    .prologue
    .line 108
    iget-object v0, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->mGoneViewWhenClick:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$300(Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;Landroid/widget/ProgressBar;ZI)V
    .locals 0
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    .prologue
    .line 108
    invoke-direct {p0, p1, p2, p3}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->setProgressBarState(Landroid/widget/ProgressBar;ZI)V

    return-void
.end method

.method static synthetic access$400(Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;)Landroid/util/Pair;
    .locals 1
    .parameter "x0"

    .prologue
    .line 108
    iget-object v0, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->mSelectPosition:Landroid/util/Pair;

    return-object v0
.end method

.method static synthetic access$500(Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;)Landroid/util/Pair;
    .locals 1
    .parameter "x0"

    .prologue
    .line 108
    iget-object v0, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->mLastSelectPosition:Landroid/util/Pair;

    return-object v0
.end method

.method static synthetic access$600(Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;Landroid/view/View;Z)V
    .locals 0
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 108
    invoke-direct {p0, p1, p2}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->startProgressBarAnimation(Landroid/view/View;Z)V

    return-void
.end method

.method private clearGoneViews(Landroid/view/View;)V
    .locals 4
    .parameter "remainState"

    .prologue
    .line 165
    iget-object v2, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->mGoneViewWhenClick:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 166
    .local v0, gv:Landroid/view/View;
    if-eq v0, p1, :cond_0

    .line 167
    iget-object v2, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/resourcebrowser/util/AudioBatchResourceHandler;

    iget-object v3, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->mSelectPosition:Landroid/util/Pair;

    invoke-virtual {v2, v3}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->getResource(Landroid/util/Pair;)Lmiui/resourcebrowser/model/Resource;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {p0, v0, v2, v3}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->setOperatingViewsState(Landroid/view/View;Lmiui/resourcebrowser/model/Resource;Z)V

    goto :goto_0

    .line 170
    .end local v0           #gv:Landroid/view/View;
    :cond_1
    iget-object v2, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->mGoneViewWhenClick:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 171
    return-void
.end method

.method private realSetOperateButtonState(Landroid/view/View;Ljava/lang/String;I)V
    .locals 4
    .parameter "v"
    .parameter "text"
    .parameter "btnState"

    .prologue
    .line 297
    const v2, 0x60b006b

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 298
    .local v0, opBtn:Landroid/widget/Button;
    if-nez p3, :cond_0

    .line 299
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 308
    :goto_0
    return-void

    .line 301
    :cond_0
    invoke-virtual {v0, p2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 302
    iget-object v2, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/resourcebrowser/util/AudioBatchResourceHandler;

    #getter for: Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->mMusicUI:Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;
    invoke-static {v2}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->access$800(Lmiui/resourcebrowser/util/AudioBatchResourceHandler;)Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 303
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 305
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    .line 306
    .local v1, position:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    new-instance v2, Landroid/util/Pair;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v2, v1, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private refreshPlayProgressBarState(Landroid/view/View;Lmiui/resourcebrowser/model/Resource;Z)V
    .locals 3
    .parameter "v"
    .parameter "r"
    .parameter "visiable"

    .prologue
    .line 231
    const v1, 0x60b005e

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    move-object v0, v1

    check-cast v0, Landroid/widget/ProgressBar;

    .line 232
    .local v0, p:Landroid/widget/ProgressBar;
    if-eqz p3, :cond_0

    iget-object v1, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/resourcebrowser/util/AudioBatchResourceHandler;

    #getter for: Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->mMusicPlayer:Lmiui/resourcebrowser/util/ResourceMusicPlayer;
    invoke-static {v1}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->access$700(Lmiui/resourcebrowser/util/AudioBatchResourceHandler;)Lmiui/resourcebrowser/util/ResourceMusicPlayer;

    move-result-object v1

    invoke-virtual {v1}, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 233
    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 238
    :goto_0
    return-void

    .line 235
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 236
    const/4 v1, 0x1

    const/4 v2, -0x1

    invoke-direct {p0, v0, v1, v2}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->setProgressBarState(Landroid/widget/ProgressBar;ZI)V

    goto :goto_0
.end method

.method private setApplyOrPickState(Landroid/view/View;Z)V
    .locals 3
    .parameter "v"
    .parameter "pick"

    .prologue
    .line 271
    if-eqz p2, :cond_0

    .line 272
    const v1, 0x60c001b

    .line 273
    .local v1, textId:I
    const/4 v0, 0x2

    .line 278
    .local v0, btnState:I
    :goto_0
    iget-object v2, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/resourcebrowser/util/AudioBatchResourceHandler;

    iget-object v2, v2, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, p1, v2, v0}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->realSetOperateButtonState(Landroid/view/View;Ljava/lang/String;I)V

    .line 279
    return-void

    .line 275
    .end local v0           #btnState:I
    .end local v1           #textId:I
    :cond_0
    const v1, 0x60c0019

    .line 276
    .restart local v1       #textId:I
    const/4 v0, 0x1

    .restart local v0       #btnState:I
    goto :goto_0
.end method

.method private setDownloadOrCancelState(Landroid/view/View;Lmiui/resourcebrowser/model/Resource;Z)V
    .locals 6
    .parameter "v"
    .parameter "r"
    .parameter "downloading"

    .prologue
    const v5, 0x60c0017

    .line 284
    if-eqz p3, :cond_0

    .line 285
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/resourcebrowser/util/AudioBatchResourceHandler;

    iget-object v3, v3, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->mActivity:Landroid/app/Activity;

    const/high16 v4, 0x104

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/resourcebrowser/util/AudioBatchResourceHandler;

    iget-object v3, v3, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 286
    .local v1, text:Ljava/lang/String;
    iget-object v3, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/resourcebrowser/util/AudioBatchResourceHandler;

    const v2, 0x60b004d

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    invoke-virtual {v3, v2, p2}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->setDownloadProgress(Landroid/widget/ProgressBar;Lmiui/resourcebrowser/model/Resource;)V

    .line 287
    const/4 v0, 0x4

    .line 293
    .local v0, btnState:I
    :goto_0
    invoke-direct {p0, p1, v1, v0}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->realSetOperateButtonState(Landroid/view/View;Ljava/lang/String;I)V

    .line 294
    return-void

    .line 289
    .end local v0           #btnState:I
    .end local v1           #text:Ljava/lang/String;
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/resourcebrowser/util/AudioBatchResourceHandler;

    iget-object v3, v3, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getSize()J

    move-result-wide v3

    invoke-static {v3, v4}, Lmiui/resourcebrowser/util/ResourceHelper;->formatFileSize(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 291
    .restart local v1       #text:Ljava/lang/String;
    const/4 v0, 0x3

    .restart local v0       #btnState:I
    goto :goto_0
.end method

.method private setOperatingViewsState(Landroid/view/View;Lmiui/resourcebrowser/model/Resource;Z)V
    .locals 4
    .parameter "v"
    .parameter "r"
    .parameter "visible"

    .prologue
    const v3, 0x60b004c

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 241
    if-nez p3, :cond_0

    iget-object v2, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/resourcebrowser/util/AudioBatchResourceHandler;

    invoke-virtual {v2}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->isEditMode()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 242
    :cond_0
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 246
    :goto_0
    invoke-direct {p0, p1, p2, p3}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->refreshPlayProgressBarState(Landroid/view/View;Lmiui/resourcebrowser/model/Resource;Z)V

    .line 248
    if-nez p3, :cond_3

    .line 254
    iget-object v2, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/resourcebrowser/util/AudioBatchResourceHandler;

    iget-boolean v2, v2, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->mIsLocalSource:Z

    if-nez v2, :cond_2

    if-eqz p2, :cond_2

    iget-object v2, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/resourcebrowser/util/AudioBatchResourceHandler;

    invoke-virtual {v2, p2}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->isDownloading(Lmiui/resourcebrowser/model/Resource;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 255
    invoke-direct {p0, p1, p2, v1}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->setDownloadOrCancelState(Landroid/view/View;Lmiui/resourcebrowser/model/Resource;Z)V

    .line 267
    :goto_1
    return-void

    .line 244
    :cond_1
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 257
    :cond_2
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1, v0}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->realSetOperateButtonState(Landroid/view/View;Ljava/lang/String;I)V

    goto :goto_1

    .line 260
    :cond_3
    iget-object v2, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/resourcebrowser/util/AudioBatchResourceHandler;

    iget-boolean v2, v2, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->mIsLocalSource:Z

    if-nez v2, :cond_4

    iget-object v2, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/resourcebrowser/util/AudioBatchResourceHandler;

    invoke-virtual {v2, p2}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->canDeleteResource(Lmiui/resourcebrowser/model/Resource;)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_4
    move v0, v1

    .line 261
    .local v0, isLocalResource:Z
    :cond_5
    if-eqz v0, :cond_6

    .line 262
    iget-object v1, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/resourcebrowser/util/AudioBatchResourceHandler;

    iget-object v1, v1, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v1}, Lmiui/resourcebrowser/ResourceContext;->isPicker()Z

    move-result v1

    invoke-direct {p0, p1, v1}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->setApplyOrPickState(Landroid/view/View;Z)V

    goto :goto_1

    .line 264
    :cond_6
    iget-object v1, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/resourcebrowser/util/AudioBatchResourceHandler;

    invoke-virtual {v1, p2}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->isDownloading(Lmiui/resourcebrowser/model/Resource;)Z

    move-result v1

    invoke-direct {p0, p1, p2, v1}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->setDownloadOrCancelState(Landroid/view/View;Lmiui/resourcebrowser/model/Resource;Z)V

    goto :goto_1
.end method

.method private setProgressBarState(Landroid/widget/ProgressBar;ZI)V
    .locals 1
    .parameter "p"
    .parameter "indeterminate"
    .parameter "progress"

    .prologue
    .line 222
    invoke-virtual {p1}, Landroid/widget/ProgressBar;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 228
    .end local p3
    :goto_0
    return-void

    .line 225
    .restart local p3
    :cond_0
    invoke-virtual {p1, p2}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 226
    const/16 v0, 0x64

    invoke-virtual {p1, v0}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 227
    if-ltz p3, :cond_1

    .end local p3
    :goto_1
    invoke-virtual {p1, p3}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto :goto_0

    .restart local p3
    :cond_1
    iget p3, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->mLastPlayProgress:I

    goto :goto_1
.end method

.method private startProgressBarAnimation(Landroid/view/View;Z)V
    .locals 12
    .parameter "v"
    .parameter "progressBarQuit"

    .prologue
    const-wide/16 v10, 0xfa

    const/high16 v9, 0x3f80

    const/4 v8, 0x0

    .line 196
    const v5, 0x60b005e

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    .line 197
    .local v0, p:Landroid/widget/ProgressBar;
    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getVisibility()I

    move-result v5

    if-eqz v5, :cond_0

    .line 219
    :goto_0
    return-void

    .line 200
    :cond_0
    const v5, 0x60b005f

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 202
    .local v3, tv:Landroid/view/View;
    if-eqz p2, :cond_1

    .line 203
    const/16 v5, 0x8

    invoke-virtual {v0, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 204
    new-instance v1, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v1, v9, v8}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 205
    .local v1, pAnim:Landroid/view/animation/Animation;
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v5

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getLeft()I

    move-result v6

    sub-int/2addr v5, v6

    int-to-float v5, v5

    invoke-direct {v2, v5, v8, v8, v8}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 215
    .local v2, tAnim:Landroid/view/animation/Animation;
    :goto_1
    invoke-virtual {v1, v10, v11}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 216
    invoke-virtual {v2, v10, v11}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 217
    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->startAnimation(Landroid/view/animation/Animation;)V

    .line 218
    invoke-virtual {v3, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 210
    .end local v1           #pAnim:Landroid/view/animation/Animation;
    .end local v2           #tAnim:Landroid/view/animation/Animation;
    :cond_1
    const/4 v5, 0x1

    const v6, 0x424551ec

    iget-object v7, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/resourcebrowser/util/AudioBatchResourceHandler;

    iget-object v7, v7, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->mActivity:Landroid/app/Activity;

    invoke-virtual {v7}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v7

    invoke-static {v5, v6, v7}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v4

    .line 212
    .local v4, xmove:F
    new-instance v1, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v1, v8, v9}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 213
    .restart local v1       #pAnim:Landroid/view/animation/Animation;
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    neg-float v5, v4

    invoke-direct {v2, v5, v8, v8, v8}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .restart local v2       #tAnim:Landroid/view/animation/Animation;
    goto :goto_1
.end method


# virtual methods
.method public clickMusicUI(Landroid/view/View;)V
    .locals 3
    .parameter "v"

    .prologue
    .line 184
    invoke-direct {p0, p1}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->clearGoneViews(Landroid/view/View;)V

    .line 187
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 188
    .local v0, position:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    iget-object v1, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/resourcebrowser/util/AudioBatchResourceHandler;

    invoke-virtual {v1, v0}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->getResource(Landroid/util/Pair;)Lmiui/resourcebrowser/model/Resource;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {p0, p1, v1, v2}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->setOperatingViewsState(Landroid/view/View;Lmiui/resourcebrowser/model/Resource;Z)V

    .line 189
    iget-object v1, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->mSelectPosition:Landroid/util/Pair;

    iput-object v1, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->mLastSelectPosition:Landroid/util/Pair;

    .line 190
    iput-object v0, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->mSelectPosition:Landroid/util/Pair;

    .line 191
    iget-object v1, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->mGoneViewWhenClick:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 192
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->startProgressBarAnimation(Landroid/view/View;Z)V

    .line 193
    return-void
.end method

.method public getMusicPlayListener()Lmiui/resourcebrowser/util/ResourceMusicPlayer$PlayProgressListener;
    .locals 1

    .prologue
    .line 126
    new-instance v0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI$1;

    invoke-direct {v0, p0}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI$1;-><init>(Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;)V

    return-object v0
.end method

.method public initMusicUI(Landroid/view/View;)V
    .locals 3
    .parameter "v"

    .prologue
    .line 175
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 176
    .local v0, position:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    iget-object v2, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/resourcebrowser/util/AudioBatchResourceHandler;

    invoke-virtual {v2, v0}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->getResource(Landroid/util/Pair;)Lmiui/resourcebrowser/model/Resource;

    move-result-object v1

    .line 177
    .local v1, r:Lmiui/resourcebrowser/model/Resource;
    iget-object v2, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->mSelectPosition:Landroid/util/Pair;

    if-ne v0, v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    invoke-direct {p0, p1, v1, v2}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->setOperatingViewsState(Landroid/view/View;Lmiui/resourcebrowser/model/Resource;Z)V

    .line 178
    iget-object v2, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->mSelectPosition:Landroid/util/Pair;

    if-ne v0, v2, :cond_0

    .line 179
    iget-object v2, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->mGoneViewWhenClick:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 181
    :cond_0
    return-void

    .line 177
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 9
    .parameter "v"

    .prologue
    .line 312
    move-object v0, p1

    check-cast v0, Landroid/widget/Button;

    .line 314
    .local v0, btn:Landroid/widget/Button;
    invoke-virtual {v0}, Landroid/widget/Button;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/util/Pair;

    .line 315
    .local v5, tag:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;Ljava/lang/Integer;>;"
    iget-object v3, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Landroid/util/Pair;

    .line 316
    .local v3, position:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    iget-object v6, v5, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 317
    .local v1, btnState:I
    iget-object v6, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/resourcebrowser/util/AudioBatchResourceHandler;

    invoke-virtual {v6, v3}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->getResource(Landroid/util/Pair;)Lmiui/resourcebrowser/model/Resource;

    move-result-object v4

    .line 318
    .local v4, r:Lmiui/resourcebrowser/model/Resource;
    packed-switch v1, :pswitch_data_0

    .line 339
    :goto_0
    return-void

    .line 320
    :pswitch_0
    iget-object v6, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/resourcebrowser/util/AudioBatchResourceHandler;

    invoke-virtual {v6, v4}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->handleApplyEvent(Lmiui/resourcebrowser/model/Resource;)V

    goto :goto_0

    .line 323
    :pswitch_1
    iget-object v6, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/resourcebrowser/util/AudioBatchResourceHandler;

    invoke-virtual {v6, v4}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->handlePickEvent(Lmiui/resourcebrowser/model/Resource;)V

    goto :goto_0

    .line 326
    :pswitch_2
    iget-object v6, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/resourcebrowser/util/AudioBatchResourceHandler;

    invoke-virtual {v6, v4}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->handleDownloadEvent(Lmiui/resourcebrowser/model/Resource;)V

    .line 327
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/resourcebrowser/util/AudioBatchResourceHandler;

    iget-object v7, v7, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->mActivity:Landroid/app/Activity;

    const/high16 v8, 0x104

    invoke-virtual {v7, v8}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/resourcebrowser/util/AudioBatchResourceHandler;

    iget-object v7, v7, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->mActivity:Landroid/app/Activity;

    const v8, 0x60c0017

    invoke-virtual {v7, v8}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 329
    .local v2, cancelTip:Ljava/lang/String;
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 330
    new-instance v6, Landroid/util/Pair;

    const/4 v7, 0x4

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-direct {v6, v3, v7}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    .line 331
    invoke-virtual {v0}, Landroid/widget/Button;->getParent()Landroid/view/ViewParent;

    move-result-object v6

    check-cast v6, Landroid/view/View;

    const v7, 0x60b004d

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 334
    .end local v2           #cancelTip:Ljava/lang/String;
    :pswitch_3
    iget-object v6, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/resourcebrowser/util/AudioBatchResourceHandler;

    invoke-virtual {v6, v4}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;->handleCancelDownloadEvent(Lmiui/resourcebrowser/model/Resource;)V

    goto :goto_0

    .line 318
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public reset()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 160
    invoke-direct {p0, v0}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->clearGoneViews(Landroid/view/View;)V

    .line 161
    iput-object v0, p0, Lmiui/resourcebrowser/util/AudioBatchResourceHandler$ResourceMusicUI;->mSelectPosition:Landroid/util/Pair;

    .line 162
    return-void
.end method
