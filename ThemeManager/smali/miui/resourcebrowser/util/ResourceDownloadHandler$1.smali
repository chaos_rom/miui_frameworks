.class Lmiui/resourcebrowser/util/ResourceDownloadHandler$1;
.super Landroid/content/BroadcastReceiver;
.source "ResourceDownloadHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/resourcebrowser/util/ResourceDownloadHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/resourcebrowser/util/ResourceDownloadHandler;


# direct methods
.method constructor <init>(Lmiui/resourcebrowser/util/ResourceDownloadHandler;)V
    .locals 0
    .parameter

    .prologue
    .line 40
    iput-object p1, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler$1;->this$0:Lmiui/resourcebrowser/util/ResourceDownloadHandler;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 13
    .parameter "context"
    .parameter "intent"

    .prologue
    .line 44
    new-instance v6, Landroid/app/MiuiDownloadManager$Query;

    invoke-direct {v6}, Landroid/app/MiuiDownloadManager$Query;-><init>()V

    .line 45
    .local v6, query:Landroid/app/MiuiDownloadManager$Query;
    const-string v10, "extra_download_id"

    const-wide/16 v11, -0x1

    invoke-virtual {p2, v10, v11, v12}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    .line 46
    .local v3, id:J
    const-wide/16 v10, -0x1

    cmp-long v10, v3, v10

    if-eqz v10, :cond_1

    .line 47
    const/4 v10, 0x1

    new-array v10, v10, [J

    const/4 v11, 0x0

    aput-wide v3, v10, v11

    invoke-virtual {v6, v10}, Landroid/app/MiuiDownloadManager$Query;->setFilterById([J)Landroid/app/DownloadManager$Query;

    .line 48
    iget-object v10, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler$1;->this$0:Lmiui/resourcebrowser/util/ResourceDownloadHandler;

    #getter for: Lmiui/resourcebrowser/util/ResourceDownloadHandler;->mDownloadManager:Landroid/app/DownloadManager;
    invoke-static {v10}, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->access$000(Lmiui/resourcebrowser/util/ResourceDownloadHandler;)Landroid/app/DownloadManager;

    move-result-object v10

    invoke-virtual {v10, v6}, Landroid/app/DownloadManager;->query(Landroid/app/DownloadManager$Query;)Landroid/database/Cursor;

    move-result-object v1

    .line 50
    .local v1, cursor:Landroid/database/Cursor;
    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 51
    const-string v10, "local_uri"

    invoke-interface {v1, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    invoke-interface {v1, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 52
    .local v8, storedPath:Ljava/lang/String;
    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    invoke-virtual {v10}, Landroid/net/Uri;->getEncodedPath()Ljava/lang/String;

    move-result-object v8

    .line 53
    invoke-static {v8}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 54
    invoke-static {v8}, Lmiui/resourcebrowser/util/ResourceHelper;->getFileNameWithoutExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 56
    .local v5, onlineId:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 57
    .local v0, action:Ljava/lang/String;
    const-string v10, "android.intent.action.DOWNLOAD_COMPLETE"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 58
    invoke-static {v1}, Landroid/app/MiuiDownloadManager;->isDownloadSuccess(Landroid/database/Cursor;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 59
    iget-object v10, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler$1;->this$0:Lmiui/resourcebrowser/util/ResourceDownloadHandler;

    invoke-virtual {v10, v8, v5}, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->handleDownloadSuccessful(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    .end local v0           #action:Ljava/lang/String;
    .end local v5           #onlineId:Ljava/lang/String;
    .end local v8           #storedPath:Ljava/lang/String;
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 74
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 77
    .end local v1           #cursor:Landroid/database/Cursor;
    :cond_1
    return-void

    .line 61
    .restart local v0       #action:Ljava/lang/String;
    .restart local v1       #cursor:Landroid/database/Cursor;
    .restart local v5       #onlineId:Ljava/lang/String;
    .restart local v8       #storedPath:Ljava/lang/String;
    :cond_2
    const-string v10, "extra_download_status"

    const/16 v11, 0x10

    invoke-virtual {p2, v10, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    .line 63
    .local v7, status:I
    const/16 v10, 0x10

    if-ne v7, v10, :cond_0

    .line 64
    iget-object v10, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler$1;->this$0:Lmiui/resourcebrowser/util/ResourceDownloadHandler;

    invoke-virtual {v10, v8, v5}, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->handleDownloadFailed(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 67
    .end local v7           #status:I
    :cond_3
    const-string v10, "android.intent.action.DOWNLOAD_UPDATED"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 68
    const-string v10, "extra_download_current_bytes"

    const-wide/16 v11, 0x0

    invoke-virtual {p2, v10, v11, v12}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v10

    long-to-int v2, v10

    .line 69
    .local v2, downloaded:I
    const-string v10, "extra_download_total_bytes"

    const-wide/16 v11, 0x1

    invoke-virtual {p2, v10, v11, v12}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v10

    long-to-int v9, v10

    .line 70
    .local v9, total:I
    iget-object v10, p0, Lmiui/resourcebrowser/util/ResourceDownloadHandler$1;->this$0:Lmiui/resourcebrowser/util/ResourceDownloadHandler;

    invoke-virtual {v10, v8, v5, v2, v9}, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->handleDownloadProgressUpdated(Ljava/lang/String;Ljava/lang/String;II)V

    goto :goto_0
.end method
