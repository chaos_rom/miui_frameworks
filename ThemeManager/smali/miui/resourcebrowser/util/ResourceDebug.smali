.class public Lmiui/resourcebrowser/util/ResourceDebug;
.super Ljava/lang/Object;
.source "ResourceDebug.java"


# static fields
.field public static final DEBUG:Z

.field public static final sMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static sMaxThumbnailDownloadTaskNumber:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 11
    new-instance v3, Ljava/io/File;

    const-string v4, "/data/system/theme_debug"

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    sput-boolean v3, Lmiui/resourcebrowser/util/ResourceDebug;->DEBUG:Z

    .line 15
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    sput-object v3, Lmiui/resourcebrowser/util/ResourceDebug;->sMap:Ljava/util/HashMap;

    .line 17
    const/4 v3, 0x3

    sput v3, Lmiui/resourcebrowser/util/ResourceDebug;->sMaxThumbnailDownloadTaskNumber:I

    .line 20
    sget-boolean v3, Lmiui/resourcebrowser/util/ResourceDebug;->DEBUG:Z

    if-eqz v3, :cond_1

    .line 22
    :try_start_0
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/FileReader;

    const-string v4, "/data/system/theme_debug"

    invoke-direct {v3, v4}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 23
    .local v0, is:Ljava/io/BufferedReader;
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 24
    .local v1, line:Ljava/lang/String;
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 25
    .local v2, number:I
    if-lez v2, :cond_0

    const/16 v3, 0xa

    if-ge v2, v3, :cond_0

    .line 26
    sput v2, Lmiui/resourcebrowser/util/ResourceDebug;->sMaxThumbnailDownloadTaskNumber:I

    .line 28
    :cond_0
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 32
    .end local v1           #line:Ljava/lang/String;
    .end local v2           #number:I
    :cond_1
    :goto_0
    return-void

    .line 29
    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getMaxThumbnailDownloadTaskNumber()I
    .locals 1

    .prologue
    .line 35
    sget v0, Lmiui/resourcebrowser/util/ResourceDebug;->sMaxThumbnailDownloadTaskNumber:I

    return v0
.end method
