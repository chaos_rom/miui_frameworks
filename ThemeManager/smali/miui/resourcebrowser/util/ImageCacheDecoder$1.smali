.class Lmiui/resourcebrowser/util/ImageCacheDecoder$1;
.super Landroid/os/Handler;
.source "ImageCacheDecoder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/resourcebrowser/util/ImageCacheDecoder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/resourcebrowser/util/ImageCacheDecoder;


# direct methods
.method constructor <init>(Lmiui/resourcebrowser/util/ImageCacheDecoder;)V
    .locals 0
    .parameter

    .prologue
    .line 50
    iput-object p1, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$1;->this$0:Lmiui/resourcebrowser/util/ImageCacheDecoder;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .parameter "msg"

    .prologue
    const/4 v2, 0x1

    .line 54
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/util/Pair;

    .line 55
    .local v0, imagePath:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    iget v3, p1, Landroid/os/Message;->arg1:I

    if-nez v3, :cond_1

    move v1, v2

    .line 57
    .local v1, result:Z
    :goto_0
    iget v3, p1, Landroid/os/Message;->what:I

    if-nez v3, :cond_2

    .line 58
    iget-object v2, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$1;->this$0:Lmiui/resourcebrowser/util/ImageCacheDecoder;

    #getter for: Lmiui/resourcebrowser/util/ImageCacheDecoder;->mListener:Lmiui/resourcebrowser/util/ImageCacheDecoder$ImageDecodingListener;
    invoke-static {v2}, Lmiui/resourcebrowser/util/ImageCacheDecoder;->access$000(Lmiui/resourcebrowser/util/ImageCacheDecoder;)Lmiui/resourcebrowser/util/ImageCacheDecoder$ImageDecodingListener;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 59
    iget-object v2, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$1;->this$0:Lmiui/resourcebrowser/util/ImageCacheDecoder;

    #getter for: Lmiui/resourcebrowser/util/ImageCacheDecoder;->mListener:Lmiui/resourcebrowser/util/ImageCacheDecoder$ImageDecodingListener;
    invoke-static {v2}, Lmiui/resourcebrowser/util/ImageCacheDecoder;->access$000(Lmiui/resourcebrowser/util/ImageCacheDecoder;)Lmiui/resourcebrowser/util/ImageCacheDecoder$ImageDecodingListener;

    move-result-object v4

    iget-object v2, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    iget-object v3, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    invoke-interface {v4, v1, v2, v3}, Lmiui/resourcebrowser/util/ImageCacheDecoder$ImageDecodingListener;->handleDecodingResult(ZLjava/lang/String;Ljava/lang/String;)V

    .line 66
    :cond_0
    :goto_1
    return-void

    .line 55
    .end local v1           #result:Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 61
    .restart local v1       #result:Z
    :cond_2
    iget v3, p1, Landroid/os/Message;->what:I

    if-ne v3, v2, :cond_0

    .line 62
    iget-object v2, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$1;->this$0:Lmiui/resourcebrowser/util/ImageCacheDecoder;

    #getter for: Lmiui/resourcebrowser/util/ImageCacheDecoder;->mListener:Lmiui/resourcebrowser/util/ImageCacheDecoder$ImageDecodingListener;
    invoke-static {v2}, Lmiui/resourcebrowser/util/ImageCacheDecoder;->access$000(Lmiui/resourcebrowser/util/ImageCacheDecoder;)Lmiui/resourcebrowser/util/ImageCacheDecoder$ImageDecodingListener;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 63
    iget-object v2, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$1;->this$0:Lmiui/resourcebrowser/util/ImageCacheDecoder;

    #getter for: Lmiui/resourcebrowser/util/ImageCacheDecoder;->mListener:Lmiui/resourcebrowser/util/ImageCacheDecoder$ImageDecodingListener;
    invoke-static {v2}, Lmiui/resourcebrowser/util/ImageCacheDecoder;->access$000(Lmiui/resourcebrowser/util/ImageCacheDecoder;)Lmiui/resourcebrowser/util/ImageCacheDecoder$ImageDecodingListener;

    move-result-object v4

    iget-object v2, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    iget-object v3, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    invoke-interface {v4, v1, v2, v3}, Lmiui/resourcebrowser/util/ImageCacheDecoder$ImageDecodingListener;->handleDownloadResult(ZLjava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
