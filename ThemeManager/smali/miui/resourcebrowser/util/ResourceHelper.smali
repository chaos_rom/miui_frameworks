.class public Lmiui/resourcebrowser/util/ResourceHelper;
.super Ljava/lang/Object;
.source "ResourceHelper.java"

# interfaces
.implements Lmiui/resourcebrowser/ResourceConstants;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/resourcebrowser/util/ResourceHelper$UnzipProcessUpdateListener;,
        Lmiui/resourcebrowser/util/ResourceHelper$DescriptionSAXHandler;
    }
.end annotation


# static fields
.field private static mCacheFileNameMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected static sFileHashCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 77
    new-instance v0, Lmiui/resourcebrowser/util/ResourceHelper$1;

    invoke-direct {v0}, Lmiui/resourcebrowser/util/ResourceHelper$1;-><init>()V

    sput-object v0, Lmiui/resourcebrowser/util/ResourceHelper;->mCacheFileNameMap:Ljava/util/Map;

    .line 852
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lmiui/resourcebrowser/util/ResourceHelper;->sFileHashCache:Ljava/util/HashMap;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    return-void
.end method

.method public static buildResourceContext(Lmiui/resourcebrowser/ResourceContext;Landroid/content/Intent;Landroid/content/Context;)Lmiui/resourcebrowser/ResourceContext;
    .locals 11
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v10, 0x4

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v3, 0x1

    .line 91
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 92
    const-string v1, "android.intent.action.RINGTONE_PICKER"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_23

    .line 93
    invoke-virtual {p0, v3}, Lmiui/resourcebrowser/ResourceContext;->setPicker(Z)V

    .line 94
    invoke-virtual {p0, v10}, Lmiui/resourcebrowser/ResourceContext;->setDisplayType(I)V

    .line 95
    invoke-virtual {p0, v3}, Lmiui/resourcebrowser/ResourceContext;->setCategorySupported(Z)V

    .line 96
    const-string v0, "android.intent.extra.ringtone.TITLE"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setResourceTitle(Ljava/lang/String;)V

    .line 98
    const-string v0, "android.intent.extra.ringtone.SHOW_SILENT"

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 99
    const-string v1, "android.intent.extra.ringtone.SHOW_DEFAULT"

    invoke-virtual {p1, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    .line 100
    const-string v1, "android.intent.extra.ringtone.SHOW_SILENT"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lmiui/resourcebrowser/ResourceContext;->putExtraMeta(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 101
    const-string v0, "android.intent.extra.ringtone.SHOW_DEFAULT"

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lmiui/resourcebrowser/ResourceContext;->putExtraMeta(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 103
    const-string v0, "android.intent.extra.ringtone.EXISTING_URI"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 104
    const-string v1, "android.intent.extra.ringtone.DEFAULT_URI"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    .line 105
    if-eqz v0, :cond_0

    .line 106
    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_22

    move v2, v3

    :goto_0
    invoke-static {p2, v0, v2}, Lmiui/resourcebrowser/util/ResourceHelper;->getPathByUri(Landroid/content/Context;Landroid/net/Uri;Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setCurrentUsingPath(Ljava/lang/String;)V

    .line 108
    :cond_0
    if-eqz v6, :cond_1

    if-eqz v1, :cond_1

    .line 109
    const-string v0, "android.intent.extra.ringtone.DEFAULT_URI"

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lmiui/resourcebrowser/ResourceContext;->putExtraMeta(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 112
    :cond_1
    const-string v0, "android.intent.extra.ringtone.TYPE"

    const/4 v1, 0x7

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 113
    const-string v1, "android.intent.extra.ringtone.TYPE"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lmiui/resourcebrowser/ResourceContext;->putExtraMeta(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 115
    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getSourceFolders()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_2

    .line 116
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 124
    packed-switch v0, :pswitch_data_0

    :pswitch_0
    move-object v0, v5

    move-object v1, v5

    move-object v2, v5

    move-object v6, v5

    move-object v7, v5

    move-object v8, v5

    .line 179
    :goto_1
    invoke-virtual {p0, v9}, Lmiui/resourcebrowser/ResourceContext;->setSourceFolders(Ljava/util/List;)V

    .line 180
    invoke-virtual {p0, v8}, Lmiui/resourcebrowser/ResourceContext;->setDownloadFolder(Ljava/lang/String;)V

    .line 181
    invoke-virtual {p0, v7}, Lmiui/resourcebrowser/ResourceContext;->setMetaFolder(Ljava/lang/String;)V

    .line 182
    invoke-virtual {p0, v6}, Lmiui/resourcebrowser/ResourceContext;->setContentFolder(Ljava/lang/String;)V

    .line 183
    invoke-virtual {p0, v2}, Lmiui/resourcebrowser/ResourceContext;->setRightsFolder(Ljava/lang/String;)V

    .line 184
    invoke-virtual {p0, v1}, Lmiui/resourcebrowser/ResourceContext;->setBuildInImageFolder(Ljava/lang/String;)V

    .line 185
    invoke-virtual {p0, v5}, Lmiui/resourcebrowser/ResourceContext;->setIndexFolder(Ljava/lang/String;)V

    .line 186
    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setAsyncImportFolder(Ljava/lang/String;)V

    .line 233
    :cond_2
    :goto_2
    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getPageItemCount()I

    move-result v0

    if-nez v0, :cond_3

    .line 234
    const/16 v0, 0x1e

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setPageItemCount(I)V

    .line 237
    :cond_3
    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getDisplayType()I

    move-result v0

    if-nez v0, :cond_4

    .line 238
    invoke-virtual {p0, v3}, Lmiui/resourcebrowser/ResourceContext;->setDisplayType(I)V

    .line 241
    :cond_4
    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getResourceTitle()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_5

    .line 242
    const v0, 0x60c0028

    invoke-virtual {p2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setResourceTitle(Ljava/lang/String;)V

    .line 245
    :cond_5
    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getResourceFormat()I

    move-result v0

    if-nez v0, :cond_6

    .line 246
    invoke-virtual {p0, v10}, Lmiui/resourcebrowser/ResourceContext;->setResourceFormat(I)V

    .line 249
    :cond_6
    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getResourceStamp()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_7

    .line 250
    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getResourceFormat()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    .line 269
    :cond_7
    :goto_3
    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getResourceCode()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_8

    .line 270
    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getResourceFormat()I

    move-result v0

    packed-switch v0, :pswitch_data_2

    .line 289
    :cond_8
    :goto_4
    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getResourceExtension()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_9

    .line 290
    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getResourceFormat()I

    move-result v0

    packed-switch v0, :pswitch_data_3

    .line 309
    :cond_9
    :goto_5
    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getBuildInImagePrefixes()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_a

    .line 310
    new-array v0, v3, [Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "preview_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getResourceCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setBuildInImagePrefixes(Ljava/util/List;)V

    .line 315
    :cond_a
    invoke-static {}, Lmiui/os/Environment;->getMIUIStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lmiui/os/ExtraFileUtils;->standardizeFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 316
    invoke-virtual {p2}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lmiui/os/ExtraFileUtils;->standardizeFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 317
    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getResourceCode()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lmiui/os/ExtraFileUtils;->standardizeFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 318
    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getResourceFormat()I

    move-result v5

    if-ne v5, v3, :cond_28

    .line 320
    :goto_6
    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getDownloadFolder()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_b

    .line 321
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lmiui/resourcebrowser/ResourceContext;->setDownloadFolder(Ljava/lang/String;)V

    .line 323
    :cond_b
    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getBaseDataFolder()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_c

    .line 324
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".db/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lmiui/resourcebrowser/ResourceContext;->setBaseDataFolder(Ljava/lang/String;)V

    .line 326
    :cond_c
    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getBaseDataCacheFolder()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_d

    .line 327
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lmiui/resourcebrowser/ResourceContext;->setBaseDataCacheFolder(Ljava/lang/String;)V

    .line 329
    :cond_d
    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getBaseImageCacheFolder()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_e

    .line 330
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".cache/resource/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setBaseImageCacheFolder(Ljava/lang/String;)V

    .line 333
    :cond_e
    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getSourceFolders()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_f

    .line 334
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 335
    if-eqz v3, :cond_29

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "meta/"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 336
    :goto_7
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getBaseDataFolder()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 337
    invoke-virtual {p0, v1}, Lmiui/resourcebrowser/ResourceContext;->setSourceFolders(Ljava/util/List;)V

    .line 339
    :cond_f
    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getMetaFolder()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_10

    .line 340
    if-eqz v3, :cond_2a

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "meta/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 341
    :goto_8
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getBaseDataFolder()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setMetaFolder(Ljava/lang/String;)V

    .line 343
    :cond_10
    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getContentFolder()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_11

    .line 344
    if-eqz v3, :cond_2b

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "content/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 345
    :goto_9
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getBaseDataFolder()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setContentFolder(Ljava/lang/String;)V

    .line 347
    :cond_11
    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getBuildInImageFolder()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_12

    .line 348
    if-eqz v3, :cond_2c

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "preview/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 349
    :goto_a
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getBaseDataFolder()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setBuildInImageFolder(Ljava/lang/String;)V

    .line 351
    :cond_12
    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getIndexFolder()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_13

    .line 352
    if-eqz v3, :cond_2d

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "index/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 353
    :goto_b
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getBaseDataFolder()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setIndexFolder(Ljava/lang/String;)V

    .line 355
    :cond_13
    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getAsyncImportFolder()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_14

    .line 356
    if-eqz v3, :cond_2e

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "download/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 357
    :goto_c
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getBaseDataFolder()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setAsyncImportFolder(Ljava/lang/String;)V

    .line 360
    :cond_14
    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getListCacheFolder()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_15

    .line 361
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getBaseDataCacheFolder()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "list/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setListCacheFolder(Ljava/lang/String;)V

    .line 363
    :cond_15
    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getDetailCacheFolder()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_16

    .line 364
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getBaseDataCacheFolder()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "detail/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setDetailCacheFolder(Ljava/lang/String;)V

    .line 366
    :cond_16
    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getCategoryCacheFolder()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_17

    .line 367
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getBaseDataCacheFolder()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "category/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setCategoryCacheFolder(Ljava/lang/String;)V

    .line 369
    :cond_17
    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getRecommendCacheFolder()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_18

    .line 370
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getBaseDataCacheFolder()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "recommend/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setRecommendCacheFolder(Ljava/lang/String;)V

    .line 372
    :cond_18
    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getVersionCacheFolder()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_19

    .line 373
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getBaseDataCacheFolder()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "version/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setVersionCacheFolder(Ljava/lang/String;)V

    .line 375
    :cond_19
    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getAssociationCacheFolder()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1a

    .line 376
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getBaseDataCacheFolder()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "association/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setAssociationCacheFolder(Ljava/lang/String;)V

    .line 379
    :cond_1a
    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getThumbnailCacheFolder()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1b

    .line 380
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getBaseImageCacheFolder()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "thumbnail/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setThumbnailCacheFolder(Ljava/lang/String;)V

    .line 382
    :cond_1b
    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getPreviewCacheFolder()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1c

    .line 383
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getBaseImageCacheFolder()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "preview/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setPreviewCacheFolder(Ljava/lang/String;)V

    .line 385
    :cond_1c
    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getRecommendImageCacheFolder()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1d

    .line 386
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getBaseImageCacheFolder()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "recommend/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setRecommendImageCacheFolder(Ljava/lang/String;)V

    .line 390
    :cond_1d
    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getTabActivityClass()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2f

    .line 391
    const-class v0, Lmiui/resourcebrowser/activity/ResourceTabActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setTabActivityClass(Ljava/lang/String;)V

    .line 392
    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getTabActivityPackage()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1e

    .line 393
    const-string v0, "miui"

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setTabActivityPackage(Ljava/lang/String;)V

    .line 399
    :cond_1e
    :goto_d
    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getSearchActivityClass()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_30

    .line 400
    const-class v0, Lmiui/resourcebrowser/activity/ResourceSearchListActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setSearchActivityClass(Ljava/lang/String;)V

    .line 401
    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getSearchActivityPackage()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1f

    .line 402
    const-string v0, "miui"

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setSearchActivityPackage(Ljava/lang/String;)V

    .line 408
    :cond_1f
    :goto_e
    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getRecommendActivityClass()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_31

    .line 409
    const-class v0, Lmiui/resourcebrowser/activity/ResourceRecommendListActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setRecommendActivityClass(Ljava/lang/String;)V

    .line 410
    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getRecommendActivityPackage()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_20

    .line 411
    const-string v0, "miui"

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setRecommendActivityPackage(Ljava/lang/String;)V

    .line 417
    :cond_20
    :goto_f
    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getDetailActivityClass()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_32

    .line 418
    const-class v0, Lmiui/resourcebrowser/activity/ResourceDetailActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setDetailActivityClass(Ljava/lang/String;)V

    .line 419
    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getDetailActivityPackage()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_21

    .line 420
    const-string v0, "miui"

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setDetailActivityPackage(Ljava/lang/String;)V

    .line 426
    :cond_21
    :goto_10
    return-object p0

    :cond_22
    move v2, v4

    .line 106
    goto/16 :goto_0

    .line 126
    :pswitch_1
    sget-object v0, Lmiui/resourcebrowser/util/ResourceHelper;->USER_RINGTONE_PATH:Ljava/lang/String;

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 127
    const-string v0, "/data/media/audio/ringtones/"

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 128
    const-string v0, "/system/media/audio/ringtones/"

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 129
    sget-object v8, Lmiui/resourcebrowser/util/ResourceHelper;->DOWNLOADED_RINGTONE_PATH:Ljava/lang/String;

    .line 130
    sget-object v7, Lmiui/resourcebrowser/util/ResourceHelper;->META_RINGTONE_PATH:Ljava/lang/String;

    .line 131
    sget-object v6, Lmiui/resourcebrowser/util/ResourceHelper;->CONTENT_RINGTONE_PATH:Ljava/lang/String;

    .line 132
    sget-object v2, Lmiui/resourcebrowser/util/ResourceHelper;->RIGHTS_RINGTONE_PATH:Ljava/lang/String;

    .line 133
    sget-object v1, Lmiui/resourcebrowser/util/ResourceHelper;->BUILDIN_IMAGE_RINGTONE_PATH:Ljava/lang/String;

    .line 134
    sget-object v5, Lmiui/resourcebrowser/util/ResourceHelper;->INDEX_RINGTONE_PATH:Ljava/lang/String;

    .line 135
    sget-object v0, Lmiui/resourcebrowser/util/ResourceHelper;->ASYNC_IMPORT_RINGTONE_PATH:Ljava/lang/String;

    goto/16 :goto_1

    .line 138
    :pswitch_2
    sget-object v0, Lmiui/resourcebrowser/util/ResourceHelper;->USER_NOTIFICATION_PATH:Ljava/lang/String;

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 139
    const-string v0, "/data/media/audio/notifications/"

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 140
    const-string v0, "/system/media/audio/notifications/"

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 141
    sget-object v8, Lmiui/resourcebrowser/util/ResourceHelper;->DOWNLOADED_NOTIFICATION_PATH:Ljava/lang/String;

    .line 142
    sget-object v7, Lmiui/resourcebrowser/util/ResourceHelper;->META_NOTIFICATION_PATH:Ljava/lang/String;

    .line 143
    sget-object v6, Lmiui/resourcebrowser/util/ResourceHelper;->CONTENT_NOTIFICATION_PATH:Ljava/lang/String;

    .line 144
    sget-object v2, Lmiui/resourcebrowser/util/ResourceHelper;->RIGHTS_NOTIFICATION_PATH:Ljava/lang/String;

    .line 145
    sget-object v1, Lmiui/resourcebrowser/util/ResourceHelper;->BUILDIN_IMAGE_NOTIFICATION_PATH:Ljava/lang/String;

    .line 146
    sget-object v5, Lmiui/resourcebrowser/util/ResourceHelper;->INDEX_NOTIFICATION_PATH:Ljava/lang/String;

    .line 147
    sget-object v0, Lmiui/resourcebrowser/util/ResourceHelper;->ASYNC_IMPORT_NOTIFICATION_PATH:Ljava/lang/String;

    goto/16 :goto_1

    .line 150
    :pswitch_3
    sget-object v0, Lmiui/resourcebrowser/util/ResourceHelper;->USER_ALARM_PATH:Ljava/lang/String;

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 151
    const-string v0, "/data/media/audio/alarms/"

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 152
    const-string v0, "/system/media/audio/alarms/"

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 153
    sget-object v8, Lmiui/resourcebrowser/util/ResourceHelper;->DOWNLOADED_ALARM_PATH:Ljava/lang/String;

    .line 154
    sget-object v7, Lmiui/resourcebrowser/util/ResourceHelper;->META_ALARM_PATH:Ljava/lang/String;

    .line 155
    sget-object v6, Lmiui/resourcebrowser/util/ResourceHelper;->CONTENT_ALARM_PATH:Ljava/lang/String;

    .line 156
    sget-object v2, Lmiui/resourcebrowser/util/ResourceHelper;->RIGHTS_ALARM_PATH:Ljava/lang/String;

    .line 157
    sget-object v1, Lmiui/resourcebrowser/util/ResourceHelper;->BUILDIN_IMAGE_ALARM_PATH:Ljava/lang/String;

    .line 158
    sget-object v5, Lmiui/resourcebrowser/util/ResourceHelper;->INDEX_ALARM_PATH:Ljava/lang/String;

    .line 159
    sget-object v0, Lmiui/resourcebrowser/util/ResourceHelper;->ASYNC_IMPORT_ALARM_PATH:Ljava/lang/String;

    goto/16 :goto_1

    .line 162
    :pswitch_4
    sget-object v0, Lmiui/resourcebrowser/util/ResourceHelper;->USER_RINGTONE_PATH:Ljava/lang/String;

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 163
    sget-object v0, Lmiui/resourcebrowser/util/ResourceHelper;->USER_NOTIFICATION_PATH:Ljava/lang/String;

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 164
    sget-object v0, Lmiui/resourcebrowser/util/ResourceHelper;->USER_ALARM_PATH:Ljava/lang/String;

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 165
    const-string v0, "/data/media/audio/ringtones/"

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 166
    const-string v0, "/data/media/audio/notifications/"

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 167
    const-string v0, "/data/media/audio/alarms/"

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 168
    const-string v0, "/system/media/audio/ringtones/"

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 169
    const-string v0, "/system/media/audio/notifications/"

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 170
    const-string v0, "/system/media/audio/alarms/"

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 171
    sget-object v8, Lmiui/resourcebrowser/util/ResourceHelper;->DOWNLOADED_RINGTONE_PATH:Ljava/lang/String;

    .line 172
    sget-object v7, Lmiui/resourcebrowser/util/ResourceHelper;->META_RINGTONE_PATH:Ljava/lang/String;

    .line 173
    sget-object v6, Lmiui/resourcebrowser/util/ResourceHelper;->CONTENT_RINGTONE_PATH:Ljava/lang/String;

    .line 174
    sget-object v2, Lmiui/resourcebrowser/util/ResourceHelper;->RIGHTS_RINGTONE_PATH:Ljava/lang/String;

    .line 175
    sget-object v1, Lmiui/resourcebrowser/util/ResourceHelper;->BUILDIN_IMAGE_RINGTONE_PATH:Ljava/lang/String;

    .line 176
    sget-object v5, Lmiui/resourcebrowser/util/ResourceHelper;->INDEX_RINGTONE_PATH:Ljava/lang/String;

    .line 177
    sget-object v0, Lmiui/resourcebrowser/util/ResourceHelper;->ASYNC_IMPORT_RINGTONE_PATH:Ljava/lang/String;

    goto/16 :goto_1

    .line 188
    :cond_23
    const-string v1, "android.intent.action.SET_WALLPAPER"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_24

    .line 189
    invoke-virtual {p0, v4}, Lmiui/resourcebrowser/ResourceContext;->setPicker(Z)V

    .line 190
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setDisplayType(I)V

    .line 191
    invoke-virtual {p0, v3}, Lmiui/resourcebrowser/ResourceContext;->setCategorySupported(Z)V

    .line 192
    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getSourceFolders()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_2

    .line 193
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 194
    const-string v1, "/system/media/wallpaper/"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 195
    const-string v1, "/data/media/wallpaper/"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 196
    sget-object v1, Lmiui/resourcebrowser/util/ResourceHelper;->USER_WALLPAPER_PATH:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 197
    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setSourceFolders(Ljava/util/List;)V

    .line 198
    sget-object v0, Lmiui/resourcebrowser/util/ResourceHelper;->DOWNLOADED_WALLPAPER_PATH:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setDownloadFolder(Ljava/lang/String;)V

    .line 199
    sget-object v0, Lmiui/resourcebrowser/util/ResourceHelper;->META_WALLPAPER_PATH:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setMetaFolder(Ljava/lang/String;)V

    .line 200
    sget-object v0, Lmiui/resourcebrowser/util/ResourceHelper;->CONTENT_WALLPAPER_PATH:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setContentFolder(Ljava/lang/String;)V

    .line 201
    sget-object v0, Lmiui/resourcebrowser/util/ResourceHelper;->RIGHTS_WALLPAPER_PATH:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setRightsFolder(Ljava/lang/String;)V

    .line 202
    sget-object v0, Lmiui/resourcebrowser/util/ResourceHelper;->BUILDIN_IMAGE_WALLPAPER_PATH:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setBuildInImageFolder(Ljava/lang/String;)V

    .line 203
    sget-object v0, Lmiui/resourcebrowser/util/ResourceHelper;->INDEX_WALLPAPER_PATH:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setIndexFolder(Ljava/lang/String;)V

    .line 204
    sget-object v0, Lmiui/resourcebrowser/util/ResourceHelper;->ASYNC_IMPORT_WALLPAPER_PATH:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setAsyncImportFolder(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 206
    :cond_24
    const-string v1, "android.intent.action.SET_LOCKSCREEN_WALLPAPER"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_25

    .line 207
    invoke-virtual {p0, v4}, Lmiui/resourcebrowser/ResourceContext;->setPicker(Z)V

    .line 208
    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setDisplayType(I)V

    .line 209
    invoke-virtual {p0, v3}, Lmiui/resourcebrowser/ResourceContext;->setCategorySupported(Z)V

    .line 210
    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getSourceFolders()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_2

    .line 211
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 212
    const-string v1, "/system/media/lockscreen/"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 213
    const-string v1, "/data/media/lockscreen/"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 214
    sget-object v1, Lmiui/resourcebrowser/util/ResourceHelper;->USER_LOCKSCREEN_PATH:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 215
    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setSourceFolders(Ljava/util/List;)V

    .line 216
    sget-object v0, Lmiui/resourcebrowser/util/ResourceHelper;->DOWNLOADED_LOCKSCREEN_PATH:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setDownloadFolder(Ljava/lang/String;)V

    .line 217
    sget-object v0, Lmiui/resourcebrowser/util/ResourceHelper;->META_LOCKSCREEN_PATH:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setMetaFolder(Ljava/lang/String;)V

    .line 218
    sget-object v0, Lmiui/resourcebrowser/util/ResourceHelper;->CONTENT_LOCKSCREEN_PATH:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setContentFolder(Ljava/lang/String;)V

    .line 219
    sget-object v0, Lmiui/resourcebrowser/util/ResourceHelper;->RIGHTS_LOCKSCREEN_PATH:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setRightsFolder(Ljava/lang/String;)V

    .line 220
    sget-object v0, Lmiui/resourcebrowser/util/ResourceHelper;->BUILDIN_IMAGE_LOCKSCREEN_PATH:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setBuildInImageFolder(Ljava/lang/String;)V

    .line 221
    sget-object v0, Lmiui/resourcebrowser/util/ResourceHelper;->INDEX_LOCKSCREEN_PATH:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setIndexFolder(Ljava/lang/String;)V

    .line 222
    sget-object v0, Lmiui/resourcebrowser/util/ResourceHelper;->ASYNC_IMPORT_LOCKSCREEN_PATH:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setAsyncImportFolder(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 224
    :cond_25
    const-string v1, "android.intent.action.PICK_RESOURCE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_26

    const-string v1, "android.intent.action.PICK_GADGET"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 226
    :cond_26
    invoke-virtual {p0, v3}, Lmiui/resourcebrowser/ResourceContext;->setPicker(Z)V

    .line 227
    const-string v0, "REQUEST_CURRENT_USING_PATH"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setCurrentUsingPath(Ljava/lang/String;)V

    .line 228
    const-string v0, "REQUEST_TRACK_ID"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setTrackId(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 230
    :cond_27
    invoke-virtual {p0, v4}, Lmiui/resourcebrowser/ResourceContext;->setPicker(Z)V

    goto/16 :goto_2

    .line 252
    :pswitch_5
    const-string v0, "BundleUnion"

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setResourceStamp(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 255
    :pswitch_6
    const-string v0, "WallpaperUnion"

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setResourceStamp(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 258
    :pswitch_7
    const-string v0, "RingtoneUnion"

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setResourceStamp(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 261
    :pswitch_8
    const-string v0, "ZipUnion"

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setResourceStamp(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 264
    :pswitch_9
    const-string v0, "OtherUnion"

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setResourceStamp(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 272
    :pswitch_a
    const-string v0, "bundle"

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setResourceCode(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 275
    :pswitch_b
    const-string v0, "wallpaper"

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setResourceCode(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 278
    :pswitch_c
    const-string v0, "ringtone"

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setResourceCode(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 281
    :pswitch_d
    const-string v0, "zip"

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setResourceCode(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 284
    :pswitch_e
    const-string v0, "other"

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setResourceCode(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 292
    :pswitch_f
    const-string v0, ".zip"

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setResourceExtension(Ljava/lang/String;)V

    goto/16 :goto_5

    .line 295
    :pswitch_10
    const-string v0, ".jpg"

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setResourceExtension(Ljava/lang/String;)V

    goto/16 :goto_5

    .line 298
    :pswitch_11
    const-string v0, ".mp3"

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setResourceExtension(Ljava/lang/String;)V

    goto/16 :goto_5

    .line 301
    :pswitch_12
    const-string v0, ".zip"

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setResourceExtension(Ljava/lang/String;)V

    goto/16 :goto_5

    .line 304
    :pswitch_13
    const-string v0, ".mrf"

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setResourceExtension(Ljava/lang/String;)V

    goto/16 :goto_5

    :cond_28
    move v3, v4

    .line 318
    goto/16 :goto_6

    .line 335
    :cond_29
    const-string v0, "meta/"

    goto/16 :goto_7

    .line 340
    :cond_2a
    const-string v0, "meta/"

    goto/16 :goto_8

    .line 344
    :cond_2b
    const-string v0, "content/"

    goto/16 :goto_9

    .line 348
    :cond_2c
    const-string v0, "preview/"

    goto/16 :goto_a

    .line 352
    :cond_2d
    const-string v0, "index/"

    goto/16 :goto_b

    .line 356
    :cond_2e
    const-string v0, "download/"

    goto/16 :goto_c

    .line 395
    :cond_2f
    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getTabActivityPackage()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1e

    .line 396
    invoke-virtual {p2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setTabActivityPackage(Ljava/lang/String;)V

    goto/16 :goto_d

    .line 404
    :cond_30
    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getSearchActivityPackage()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1f

    .line 405
    invoke-virtual {p2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setSearchActivityPackage(Ljava/lang/String;)V

    goto/16 :goto_e

    .line 413
    :cond_31
    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getRecommendActivityPackage()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_20

    .line 414
    invoke-virtual {p2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setRecommendActivityPackage(Ljava/lang/String;)V

    goto/16 :goto_f

    .line 422
    :cond_32
    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->getDetailActivityPackage()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_21

    .line 423
    invoke-virtual {p2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setDetailActivityPackage(Ljava/lang/String;)V

    goto/16 :goto_10

    .line 124
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch

    .line 250
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch

    .line 270
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch

    .line 290
    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
    .end packed-switch
.end method

.method public static deleteEmptyFolder(Ljava/io/File;)Z
    .locals 7
    .parameter "folder"

    .prologue
    const/4 v5, 0x0

    .line 822
    invoke-virtual {p0}, Ljava/io/File;->isFile()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 830
    :cond_0
    :goto_0
    return v5

    .line 824
    :cond_1
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 825
    .local v2, files:[Ljava/io/File;
    if-eqz v2, :cond_0

    .line 827
    move-object v0, v2

    .local v0, arr$:[Ljava/io/File;
    array-length v4, v0

    .local v4, len$:I
    const/4 v3, 0x0

    .local v3, i$:I
    :goto_1
    if-ge v3, v4, :cond_2

    aget-object v1, v0, v3

    .line 828
    .local v1, file:Ljava/io/File;
    invoke-static {v1}, Lmiui/resourcebrowser/util/ResourceHelper;->deleteEmptyFolder(Ljava/io/File;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 827
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 830
    .end local v1           #file:Ljava/io/File;
    :cond_2
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    move-result v5

    goto :goto_0
.end method

.method public static exit(Landroid/app/Activity;)V
    .locals 3
    .parameter

    .prologue
    .line 1084
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x1010355

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x60c0003

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x60c0004

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 1088
    new-instance v1, Lmiui/resourcebrowser/util/ResourceHelper$2;

    invoke-direct {v1, p0}, Lmiui/resourcebrowser/util/ResourceHelper$2;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 1095
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 1096
    return-void
.end method

.method public static formatDuration(I)Ljava/lang/String;
    .locals 5
    .parameter

    .prologue
    .line 924
    div-int/lit16 v0, p0, 0x3e8

    .line 925
    const-string v1, "%02d:%02d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    div-int/lit8 v4, v0, 0x3c

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    rem-int/lit8 v0, v0, 0x3c

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static formatFileSize(J)Ljava/lang/String;
    .locals 7
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v6, 0x0

    const-wide/high16 v4, 0x4130

    .line 914
    .line 916
    long-to-double v0, p0

    cmpg-double v0, v0, v4

    if-gez v0, :cond_0

    .line 917
    const-string v0, "%.0fK"

    new-array v1, v2, [Ljava/lang/Object;

    long-to-double v2, p0

    const-wide/high16 v4, 0x4090

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 919
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "%.1fM"

    new-array v1, v2, [Ljava/lang/Object;

    long-to-double v2, p0

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getContent(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 7
    .parameter "is"

    .prologue
    .line 806
    const/4 v2, 0x0

    .line 807
    .local v2, content:Ljava/lang/String;
    new-instance v5, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v5}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 808
    .local v5, os:Ljava/io/ByteArrayOutputStream;
    const/16 v6, 0x400

    new-array v0, v6, [B

    .line 809
    .local v0, buffer:[B
    const/4 v1, 0x0

    .line 811
    .local v1, bytesRead:I
    :goto_0
    :try_start_0
    invoke-virtual {p0, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    if-ltz v1, :cond_0

    .line 812
    const/4 v6, 0x0

    invoke-virtual {v5, v0, v6, v1}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 815
    :catch_0
    move-exception v4

    .line 816
    .local v4, e:Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    .line 818
    .end local v4           #e:Ljava/io/IOException;
    :goto_1
    return-object v2

    .line 814
    :cond_0
    :try_start_1
    new-instance v3, Ljava/lang/String;

    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    invoke-direct {v3, v6}, Ljava/lang/String;-><init>([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .end local v2           #content:Ljava/lang/String;
    .local v3, content:Ljava/lang/String;
    move-object v2, v3

    .line 817
    .end local v3           #content:Ljava/lang/String;
    .restart local v2       #content:Ljava/lang/String;
    goto :goto_1
.end method

.method private static getContentColumnValue(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 985
    .line 987
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p2, v2, v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 991
    if-eqz v1, :cond_2

    .line 992
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 993
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 995
    :goto_0
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1000
    :goto_1
    if-eqz v0, :cond_0

    :goto_2
    return-object v0

    .line 997
    :catch_0
    move-exception v0

    .line 998
    :goto_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v6

    goto :goto_1

    .line 1000
    :cond_0
    const-string v0, ""

    goto :goto_2

    .line 997
    :catch_1
    move-exception v1

    move-object v6, v0

    move-object v0, v1

    goto :goto_3

    :cond_1
    move-object v0, v6

    goto :goto_0

    :cond_2
    move-object v0, v6

    goto :goto_1
.end method

.method public static getDataPerLine(I)I
    .locals 1
    .parameter "displayType"

    .prologue
    .line 430
    const/4 v0, 0x1

    .line 431
    .local v0, dataPerLine:I
    packed-switch p0, :pswitch_data_0

    .line 452
    :goto_0
    return v0

    .line 437
    :pswitch_0
    const/4 v0, 0x1

    .line 438
    goto :goto_0

    .line 442
    :pswitch_1
    const/4 v0, 0x2

    .line 443
    goto :goto_0

    .line 447
    :pswitch_2
    const/4 v0, 0x3

    .line 448
    goto :goto_0

    .line 431
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public static getDefaultFormatPlayingRingtoneName(Ljava/lang/String;II)Ljava/lang/String;
    .locals 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1040
    const/16 v0, 0x2e

    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 1041
    if-gez v0, :cond_0

    .line 1042
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    .line 1044
    :cond_0
    sget-char v1, Ljava/io/File;->separatorChar:C

    invoke-virtual {p0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 1046
    const-string v1, "%s (%d/%d)"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    add-int/lit8 v3, p1, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getDefaultMusicPlayList(Landroid/content/Context;Lmiui/resourcebrowser/model/Resource;)Ljava/util/List;
    .locals 7
    .parameter "context"
    .parameter "resourceItem"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lmiui/resourcebrowser/model/Resource;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1050
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1051
    .local v3, ret:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v0

    .line 1052
    .local v0, localPath:Ljava/lang/String;
    invoke-static {v0}, Lmiui/resourcebrowser/util/ResourceHelper;->getUriByPath(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 1053
    .local v4, uri:Landroid/net/Uri;
    invoke-static {p0, v4}, Lmiui/resourcebrowser/util/ResourceHelper;->getPathByUri(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    .line 1054
    .local v2, path:Ljava/lang/String;
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1055
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1062
    :cond_0
    :goto_0
    return-object v3

    .line 1057
    :cond_1
    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getThumbnails()Ljava/util/List;

    move-result-object v5

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lmiui/resourcebrowser/model/PathEntry;

    invoke-virtual {v5}, Lmiui/resourcebrowser/model/PathEntry;->getOnlinePath()Ljava/lang/String;

    move-result-object v1

    .line 1058
    .local v1, onlinePath:Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 1059
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static getDescription(Ljava/io/File;)Ljava/util/Map;
    .locals 4
    .parameter "file"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 643
    const/4 v1, 0x0

    .line 645
    .local v1, map:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :try_start_0
    new-instance v2, Ljava/io/BufferedInputStream;

    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-static {v2}, Lmiui/resourcebrowser/util/ResourceHelper;->getDescription(Ljava/io/InputStream;)Ljava/util/Map;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 649
    :goto_0
    return-object v1

    .line 646
    :catch_0
    move-exception v0

    .line 647
    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getDescription(Ljava/io/InputStream;)Ljava/util/Map;
    .locals 7
    .parameter "is"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 653
    const/4 v1, 0x0

    .line 655
    .local v1, handler:Lmiui/resourcebrowser/util/ResourceHelper$DescriptionSAXHandler;
    :try_start_0
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v4

    .line 656
    .local v4, spf:Ljavax/xml/parsers/SAXParserFactory;
    invoke-virtual {v4}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v3

    .line 657
    .local v3, sp:Ljavax/xml/parsers/SAXParser;
    new-instance v2, Lmiui/resourcebrowser/util/ResourceHelper$DescriptionSAXHandler;

    const/4 v6, 0x0

    invoke-direct {v2, v6}, Lmiui/resourcebrowser/util/ResourceHelper$DescriptionSAXHandler;-><init>(Lmiui/resourcebrowser/util/ResourceHelper$1;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 658
    .end local v1           #handler:Lmiui/resourcebrowser/util/ResourceHelper$DescriptionSAXHandler;
    .local v2, handler:Lmiui/resourcebrowser/util/ResourceHelper$DescriptionSAXHandler;
    :try_start_1
    invoke-virtual {v3, p0, v2}, Ljavax/xml/parsers/SAXParser;->parse(Ljava/io/InputStream;Lorg/xml/sax/helpers/DefaultHandler;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    .line 662
    if-eqz p0, :cond_3

    .line 664
    :try_start_2
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v1, v2

    .line 670
    .end local v2           #handler:Lmiui/resourcebrowser/util/ResourceHelper$DescriptionSAXHandler;
    .end local v3           #sp:Ljavax/xml/parsers/SAXParser;
    .end local v4           #spf:Ljavax/xml/parsers/SAXParserFactory;
    .restart local v1       #handler:Lmiui/resourcebrowser/util/ResourceHelper$DescriptionSAXHandler;
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lmiui/resourcebrowser/util/ResourceHelper$DescriptionSAXHandler;->getElementEntries()Ljava/util/HashMap;

    move-result-object v5

    :cond_1
    return-object v5

    .line 665
    .end local v1           #handler:Lmiui/resourcebrowser/util/ResourceHelper$DescriptionSAXHandler;
    .restart local v2       #handler:Lmiui/resourcebrowser/util/ResourceHelper$DescriptionSAXHandler;
    .restart local v3       #sp:Ljavax/xml/parsers/SAXParser;
    .restart local v4       #spf:Ljavax/xml/parsers/SAXParserFactory;
    :catch_0
    move-exception v0

    .line 666
    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move-object v1, v2

    .line 667
    .end local v2           #handler:Lmiui/resourcebrowser/util/ResourceHelper$DescriptionSAXHandler;
    .restart local v1       #handler:Lmiui/resourcebrowser/util/ResourceHelper$DescriptionSAXHandler;
    goto :goto_0

    .line 659
    .end local v0           #e:Ljava/io/IOException;
    .end local v3           #sp:Ljavax/xml/parsers/SAXParser;
    .end local v4           #spf:Ljavax/xml/parsers/SAXParserFactory;
    :catch_1
    move-exception v0

    .line 660
    .local v0, e:Ljava/lang/Exception;
    :goto_1
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 662
    if-eqz p0, :cond_0

    .line 664
    :try_start_4
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 665
    :catch_2
    move-exception v0

    .line 666
    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 662
    .end local v0           #e:Ljava/io/IOException;
    :catchall_0
    move-exception v5

    :goto_2
    if-eqz p0, :cond_2

    .line 664
    :try_start_5
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 667
    :cond_2
    :goto_3
    throw v5

    .line 665
    :catch_3
    move-exception v0

    .line 666
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 662
    .end local v0           #e:Ljava/io/IOException;
    .end local v1           #handler:Lmiui/resourcebrowser/util/ResourceHelper$DescriptionSAXHandler;
    .restart local v2       #handler:Lmiui/resourcebrowser/util/ResourceHelper$DescriptionSAXHandler;
    .restart local v3       #sp:Ljavax/xml/parsers/SAXParser;
    .restart local v4       #spf:Ljavax/xml/parsers/SAXParserFactory;
    :catchall_1
    move-exception v5

    move-object v1, v2

    .end local v2           #handler:Lmiui/resourcebrowser/util/ResourceHelper$DescriptionSAXHandler;
    .restart local v1       #handler:Lmiui/resourcebrowser/util/ResourceHelper$DescriptionSAXHandler;
    goto :goto_2

    .line 659
    .end local v1           #handler:Lmiui/resourcebrowser/util/ResourceHelper$DescriptionSAXHandler;
    .restart local v2       #handler:Lmiui/resourcebrowser/util/ResourceHelper$DescriptionSAXHandler;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2           #handler:Lmiui/resourcebrowser/util/ResourceHelper$DescriptionSAXHandler;
    .restart local v1       #handler:Lmiui/resourcebrowser/util/ResourceHelper$DescriptionSAXHandler;
    goto :goto_1

    .end local v1           #handler:Lmiui/resourcebrowser/util/ResourceHelper$DescriptionSAXHandler;
    .restart local v2       #handler:Lmiui/resourcebrowser/util/ResourceHelper$DescriptionSAXHandler;
    :cond_3
    move-object v1, v2

    .end local v2           #handler:Lmiui/resourcebrowser/util/ResourceHelper$DescriptionSAXHandler;
    .restart local v1       #handler:Lmiui/resourcebrowser/util/ResourceHelper$DescriptionSAXHandler;
    goto :goto_0
.end method

.method public static getDescription(Ljava/util/zip/ZipFile;Ljava/lang/String;)Ljava/util/Map;
    .locals 3
    .parameter "zipfile"
    .parameter "xmlEntryName"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/zip/ZipFile;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 633
    const/4 v1, 0x0

    .line 635
    .local v1, map:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :try_start_0
    invoke-virtual {p0, p1}, Ljava/util/zip/ZipFile;->getEntry(Ljava/lang/String;)Ljava/util/zip/ZipEntry;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/util/zip/ZipFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;

    move-result-object v2

    invoke-static {v2}, Lmiui/resourcebrowser/util/ResourceHelper;->getDescription(Ljava/io/InputStream;)Ljava/util/Map;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 639
    :goto_0
    return-object v1

    .line 636
    :catch_0
    move-exception v0

    .line 637
    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getFileHash(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .parameter "path"

    .prologue
    .line 858
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->lastModified()J

    move-result-wide v1

    .line 859
    .local v1, modified:J
    sget-object v3, Lmiui/resourcebrowser/util/ResourceHelper;->sFileHashCache:Ljava/util/HashMap;

    invoke-virtual {v3, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 860
    .local v0, cache:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/String;>;"
    if-eqz v0, :cond_0

    iget-object v3, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v3, v1, v3

    if-eqz v3, :cond_1

    .line 861
    :cond_0
    new-instance v0, Landroid/util/Pair;

    .end local v0           #cache:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/String;>;"
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {p0}, Lmiui/resourcebrowser/util/ResourceHelper;->getFileHashInner(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 862
    .restart local v0       #cache:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/String;>;"
    sget-object v3, Lmiui/resourcebrowser/util/ResourceHelper;->sFileHashCache:Ljava/util/HashMap;

    invoke-virtual {v3, p0, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 864
    :cond_1
    iget-object v3, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    return-object v3
.end method

.method private static getFileHashInner(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 868
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 869
    const-string v1, "Calculating Hash"

    invoke-static {v1, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 873
    :try_start_0
    new-instance v2, Ljava/io/BufferedInputStream;

    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_a
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5

    .line 874
    :try_start_1
    const-string v1, "SHA1"

    invoke-static {v1}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    .line 875
    const/16 v5, 0x2000

    new-array v5, v5, [B

    .line 877
    :goto_0
    invoke-virtual {v2, v5}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v6

    if-lez v6, :cond_1

    .line 878
    const/4 v7, 0x0

    invoke-virtual {v1, v5, v7, v6}, Ljava/security/MessageDigest;->update([BII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_9
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_8

    goto :goto_0

    .line 882
    :catch_0
    move-exception v1

    .line 883
    :goto_1
    :try_start_2
    invoke-virtual {v1}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 889
    if-eqz v2, :cond_0

    .line 891
    :try_start_3
    invoke-virtual {v2}, Ljava/io/BufferedInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 897
    :cond_0
    :goto_2
    const-string v1, "Calculating Hash"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long v3, v5, v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 898
    return-object v0

    .line 880
    :cond_1
    :try_start_4
    invoke-virtual {v1}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v1

    .line 881
    invoke-static {v1}, Lmiui/resourcebrowser/util/ResourceHelper;->toHexString([B)Ljava/lang/String;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_9
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_8

    move-result-object v0

    .line 889
    if-eqz v2, :cond_0

    .line 891
    :try_start_5
    invoke-virtual {v2}, Ljava/io/BufferedInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_2

    .line 892
    :catch_1
    move-exception v1

    .line 893
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 892
    :catch_2
    move-exception v1

    .line 893
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 884
    :catch_3
    move-exception v1

    move-object v2, v0

    .line 885
    :goto_3
    :try_start_6
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 889
    if-eqz v2, :cond_0

    .line 891
    :try_start_7
    invoke-virtual {v2}, Ljava/io/BufferedInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto :goto_2

    .line 892
    :catch_4
    move-exception v1

    .line 893
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 886
    :catch_5
    move-exception v1

    move-object v2, v0

    .line 887
    :goto_4
    :try_start_8
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 889
    if-eqz v2, :cond_0

    .line 891
    :try_start_9
    invoke-virtual {v2}, Ljava/io/BufferedInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    goto :goto_2

    .line 892
    :catch_6
    move-exception v1

    .line 893
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 889
    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_5
    if-eqz v2, :cond_2

    .line 891
    :try_start_a
    invoke-virtual {v2}, Ljava/io/BufferedInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_7

    .line 894
    :cond_2
    :goto_6
    throw v0

    .line 892
    :catch_7
    move-exception v1

    .line 893
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 889
    :catchall_1
    move-exception v0

    goto :goto_5

    .line 886
    :catch_8
    move-exception v1

    goto :goto_4

    .line 884
    :catch_9
    move-exception v1

    goto :goto_3

    .line 882
    :catch_a
    move-exception v1

    move-object v2, v0

    goto :goto_1
.end method

.method public static getFileName(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .parameter

    .prologue
    .line 834
    sget-object v0, Lmiui/resourcebrowser/util/ResourceHelper;->mCacheFileNameMap:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 835
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 836
    const-string v0, "/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 837
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    .line 838
    :goto_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x80

    if-le v1, v2, :cond_0

    .line 839
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 841
    :cond_0
    sget-object v1, Lmiui/resourcebrowser/util/ResourceHelper;->mCacheFileNameMap:Ljava/util/Map;

    invoke-interface {v1, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 843
    :cond_1
    return-object v0
.end method

.method public static getFileNameWithoutExtension(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .parameter

    .prologue
    .line 847
    invoke-static {p0}, Lmiui/os/ExtraFileUtils;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 848
    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 849
    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getLocalRingtoneDuration(Ljava/lang/String;)J
    .locals 5
    .parameter "filePath"

    .prologue
    .line 1066
    const-wide/16 v0, 0x0

    .line 1068
    .local v0, duration:J
    :try_start_0
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1069
    const-wide/16 v0, -0x1

    .line 1080
    :goto_0
    return-wide v0

    .line 1071
    :cond_0
    new-instance v3, Landroid/media/MediaPlayer;

    invoke-direct {v3}, Landroid/media/MediaPlayer;-><init>()V

    .line 1072
    .local v3, mediaPlayer:Landroid/media/MediaPlayer;
    invoke-virtual {v3, p0}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    .line 1073
    invoke-virtual {v3}, Landroid/media/MediaPlayer;->prepare()V

    .line 1074
    invoke-virtual {v3}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v4

    int-to-long v0, v4

    .line 1075
    invoke-virtual {v3}, Landroid/media/MediaPlayer;->release()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1077
    .end local v3           #mediaPlayer:Landroid/media/MediaPlayer;
    :catch_0
    move-exception v2

    .line 1078
    .local v2, e:Ljava/lang/Exception;
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public static getPathByUri(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .parameter "context"
    .parameter "uri"

    .prologue
    .line 945
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lmiui/resourcebrowser/util/ResourceHelper;->getPathByUri(Landroid/content/Context;Landroid/net/Uri;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getPathByUri(Landroid/content/Context;Landroid/net/Uri;Z)Ljava/lang/String;
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 949
    if-nez p1, :cond_1

    .line 950
    const-string v0, ""

    .line 969
    :cond_0
    :goto_0
    return-object v0

    .line 952
    :cond_1
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 953
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    .line 954
    const-string v2, "content"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    if-eqz p2, :cond_4

    .line 955
    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    .line 956
    const-string v2, "settings"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 957
    const-string v0, "value"

    invoke-static {p0, p1, v0}, Lmiui/resourcebrowser/util/ResourceHelper;->getContentColumnValue(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 961
    :cond_2
    :goto_1
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 962
    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    .line 963
    if-eqz v2, :cond_0

    invoke-virtual {v1, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 964
    const/4 v0, 0x1

    invoke-static {p0, v1, v0}, Lmiui/resourcebrowser/util/ResourceHelper;->getPathByUri(Landroid/content/Context;Landroid/net/Uri;Z)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 958
    :cond_3
    const-string v2, "media"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 959
    const-string v0, "_data"

    invoke-static {p0, p1, v0}, Lmiui/resourcebrowser/util/ResourceHelper;->getContentColumnValue(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 966
    :cond_4
    const-string v2, "file"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 967
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getPreviewWidth(I)I
    .locals 1
    .parameter "total"

    .prologue
    .line 557
    mul-int/lit8 v0, p0, 0x3

    div-int/lit8 v0, v0, 0x4

    return v0
.end method

.method public static getThumbnailGap(Landroid/content/Context;)I
    .locals 2
    .parameter "context"

    .prologue
    .line 498
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x60a000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method public static getThumbnailSize(Landroid/app/Activity;II)Landroid/util/Pair;
    .locals 10
    .parameter "activity"
    .parameter "displayType"
    .parameter "horizontalPadding"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "II)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 502
    const/4 v2, 0x0

    .line 503
    .local v2, itemCntPerLine:I
    const/4 v1, -0x1

    .line 504
    .local v1, heightVsWidthRatioId:I
    packed-switch p1, :pswitch_data_0

    .line 536
    :goto_0
    const/4 v6, 0x0

    .local v6, width:I
    const/4 v0, 0x0

    .line 537
    .local v0, height:I
    if-lez v1, :cond_0

    .line 538
    new-instance v4, Landroid/graphics/Point;

    invoke-direct {v4}, Landroid/graphics/Point;-><init>()V

    .line 539
    .local v4, p:Landroid/graphics/Point;
    invoke-virtual {p0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v7

    invoke-interface {v7}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v7

    invoke-virtual {v7, v4}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 540
    iget v5, v4, Landroid/graphics/Point;->x:I

    .line 541
    .local v5, screenWidth:I
    invoke-static {p0}, Lmiui/resourcebrowser/util/ResourceHelper;->getThumbnailGap(Landroid/content/Context;)I

    move-result v3

    .line 543
    .local v3, itemGap:I
    sub-int v7, v5, p2

    add-int/lit8 v8, v2, -0x1

    mul-int/2addr v8, v3

    sub-int/2addr v7, v8

    div-int v6, v7, v2

    .line 544
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v1, v6, v6}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v7

    float-to-int v0, v7

    .line 553
    .end local v3           #itemGap:I
    .end local v4           #p:Landroid/graphics/Point;
    .end local v5           #screenWidth:I
    :goto_1
    new-instance v7, Landroid/util/Pair;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v7

    .line 512
    .end local v0           #height:I
    .end local v6           #width:I
    :pswitch_0
    const/4 v1, -0x1

    .line 513
    const/4 v2, 0x1

    .line 514
    goto :goto_0

    .line 517
    :pswitch_1
    const v1, 0x6110004

    .line 518
    const/4 v2, 0x2

    .line 519
    goto :goto_0

    .line 521
    :pswitch_2
    const v1, 0x6110003

    .line 522
    const/4 v2, 0x3

    .line 523
    goto :goto_0

    .line 526
    :pswitch_3
    const v1, 0x6110002

    .line 527
    const/4 v2, 0x3

    .line 528
    goto :goto_0

    .line 530
    :pswitch_4
    const v1, 0x6110001

    .line 531
    const/4 v2, 0x2

    goto :goto_0

    .line 545
    .restart local v0       #height:I
    .restart local v6       #width:I
    :cond_0
    const/4 v7, -0x1

    if-ne v1, v7, :cond_1

    .line 546
    const/4 v6, -0x1

    .line 547
    const/4 v0, -0x1

    goto :goto_1

    .line 549
    :cond_1
    const/4 v6, -0x2

    .line 550
    const/4 v0, -0x2

    goto :goto_1

    .line 504
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static getThumbnailViewResource(I)I
    .locals 1
    .parameter "displayType"

    .prologue
    .line 456
    const/4 v0, 0x0

    .line 457
    .local v0, id:I
    packed-switch p0, :pswitch_data_0

    .line 494
    :goto_0
    return v0

    .line 459
    :pswitch_0
    const v0, 0x6030009

    .line 460
    goto :goto_0

    .line 462
    :pswitch_1
    const v0, 0x603000a

    .line 463
    goto :goto_0

    .line 465
    :pswitch_2
    const v0, 0x603000b

    .line 466
    goto :goto_0

    .line 468
    :pswitch_3
    const v0, 0x603000d

    .line 469
    goto :goto_0

    .line 471
    :pswitch_4
    const v0, 0x603000c

    .line 472
    goto :goto_0

    .line 474
    :pswitch_5
    const v0, 0x6030004

    .line 475
    goto :goto_0

    .line 477
    :pswitch_6
    const v0, 0x6030003

    .line 478
    goto :goto_0

    .line 480
    :pswitch_7
    const v0, 0x6030004

    .line 481
    goto :goto_0

    .line 483
    :pswitch_8
    const v0, 0x6030008

    .line 484
    goto :goto_0

    .line 486
    :pswitch_9
    const v0, 0x6030006

    .line 487
    goto :goto_0

    .line 489
    :pswitch_a
    const v0, 0x6030007

    .line 490
    goto :goto_0

    .line 457
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_a
        :pswitch_9
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public static getUriByPath(Ljava/lang/String;)Landroid/net/Uri;
    .locals 3
    .parameter "path"

    .prologue
    .line 973
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 974
    const/4 v1, 0x0

    .line 981
    :cond_0
    :goto_0
    return-object v1

    .line 976
    :cond_1
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 977
    .local v1, uri:Landroid/net/Uri;
    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    .line 978
    .local v0, scheme:Ljava/lang/String;
    if-nez v0, :cond_0

    .line 979
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_0
.end method

.method public static isBitToggled(II)Z
    .locals 1
    .parameter "content"
    .parameter "bit"

    .prologue
    .line 609
    and-int v0, p0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isCombineView(I)Z
    .locals 1
    .parameter "displayType"

    .prologue
    .line 565
    const/4 v0, 0x6

    if-eq p0, v0, :cond_0

    const/16 v0, 0x8

    if-eq p0, v0, :cond_0

    const/4 v0, 0x7

    if-eq p0, v0, :cond_0

    const/16 v0, 0x9

    if-eq p0, v0, :cond_0

    const/16 v0, 0xa

    if-eq p0, v0, :cond_0

    const/16 v0, 0xb

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isLocalResource(I)Z
    .locals 1
    .parameter "status"

    .prologue
    .line 597
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lmiui/resourcebrowser/util/ResourceHelper;->isBitToggled(II)Z

    move-result v0

    return v0
.end method

.method public static isMultipleView(I)Z
    .locals 1
    .parameter "displayType"

    .prologue
    .line 581
    const/4 v0, 0x5

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isOldResource(I)Z
    .locals 1
    .parameter "status"

    .prologue
    .line 605
    const/4 v0, 0x4

    invoke-static {p0, v0}, Lmiui/resourcebrowser/util/ResourceHelper;->isBitToggled(II)Z

    move-result v0

    return v0
.end method

.method public static isSingleView(I)Z
    .locals 2
    .parameter "displayType"

    .prologue
    const/4 v0, 0x1

    .line 574
    if-eq p0, v0, :cond_0

    const/4 v1, 0x2

    if-eq p0, v1, :cond_0

    const/4 v1, 0x3

    if-eq p0, v1, :cond_0

    const/4 v1, 0x4

    if-ne p0, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSystemResource(Ljava/lang/String;)Z
    .locals 1
    .parameter "path"

    .prologue
    .line 585
    if-eqz p0, :cond_0

    const-string v0, "/system"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static setFileHash(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .parameter "path"
    .parameter "hash"

    .prologue
    .line 854
    sget-object v0, Lmiui/resourcebrowser/util/ResourceHelper;->sFileHashCache:Ljava/util/HashMap;

    new-instance v1, Landroid/util/Pair;

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 855
    return-void
.end method

.method public static setMusicVolumeType(Landroid/app/Activity;I)V
    .locals 2
    .parameter "context"
    .parameter "ringtoneType"

    .prologue
    .line 929
    const/4 v0, -0x1

    .line 930
    .local v0, streamType:I
    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    .line 931
    const/4 v0, 0x2

    .line 941
    :goto_0
    invoke-virtual {p0, v0}, Landroid/app/Activity;->setVolumeControlStream(I)V

    .line 942
    return-void

    .line 932
    :cond_0
    const/4 v1, 0x2

    if-ne p1, v1, :cond_1

    .line 933
    const/4 v0, 0x5

    goto :goto_0

    .line 934
    :cond_1
    const/4 v1, 0x4

    if-ne p1, v1, :cond_2

    .line 935
    const/4 v0, 0x4

    goto :goto_0

    .line 936
    :cond_2
    const/16 v1, 0x20

    if-ne p1, v1, :cond_3

    .line 937
    const/4 v0, 0x3

    goto :goto_0

    .line 939
    :cond_3
    const/4 v0, 0x2

    goto :goto_0
.end method

.method private static toHexString([B)Ljava/lang/String;
    .locals 5
    .parameter

    .prologue
    .line 902
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 903
    const/4 v0, 0x0

    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_1

    .line 904
    aget-byte v2, p0, v0

    and-int/lit16 v2, v2, 0xff

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    .line 905
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 906
    const-string v3, "0"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 908
    :cond_0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 903
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 910
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static unzip(Ljava/lang/String;Ljava/lang/String;Lmiui/resourcebrowser/util/ResourceHelper$UnzipProcessUpdateListener;)V
    .locals 9
    .parameter "zipPath"
    .parameter "targetFolder"
    .parameter "listner"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 734
    const/4 v5, 0x0

    .line 736
    .local v5, zipFile:Ljava/util/zip/ZipFile;
    :try_start_0
    new-instance v6, Ljava/util/zip/ZipFile;

    invoke-direct {v6, p0}, Ljava/util/zip/ZipFile;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 738
    .end local v5           #zipFile:Ljava/util/zip/ZipFile;
    .local v6, zipFile:Ljava/util/zip/ZipFile;
    :try_start_1
    invoke-virtual {v6}, Ljava/util/zip/ZipFile;->entries()Ljava/util/Enumeration;

    move-result-object v0

    .line 739
    .local v0, entries:Ljava/util/Enumeration;,"Ljava/util/Enumeration<Ljava/util/zip/ZipEntry;>;"
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 740
    .local v4, root:Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z

    .line 741
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 742
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/zip/ZipEntry;

    .line 743
    .local v1, entry:Ljava/util/zip/ZipEntry;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 744
    .local v3, path:Ljava/lang/String;
    invoke-virtual {v1}, Ljava/util/zip/ZipEntry;->isDirectory()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 745
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->mkdirs()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 754
    .end local v0           #entries:Ljava/util/Enumeration;,"Ljava/util/Enumeration<Ljava/util/zip/ZipEntry;>;"
    .end local v1           #entry:Ljava/util/zip/ZipEntry;
    .end local v3           #path:Ljava/lang/String;
    .end local v4           #root:Ljava/io/File;
    :catchall_0
    move-exception v7

    move-object v5, v6

    .end local v6           #zipFile:Ljava/util/zip/ZipFile;
    .restart local v5       #zipFile:Ljava/util/zip/ZipFile;
    :goto_1
    if-eqz v5, :cond_1

    .line 755
    invoke-virtual {v5}, Ljava/util/zip/ZipFile;->close()V

    :cond_1
    throw v7

    .line 747
    .end local v5           #zipFile:Ljava/util/zip/ZipFile;
    .restart local v0       #entries:Ljava/util/Enumeration;,"Ljava/util/Enumeration<Ljava/util/zip/ZipEntry;>;"
    .restart local v1       #entry:Ljava/util/zip/ZipEntry;
    .restart local v3       #path:Ljava/lang/String;
    .restart local v4       #root:Ljava/io/File;
    .restart local v6       #zipFile:Ljava/util/zip/ZipFile;
    :cond_2
    :try_start_2
    invoke-virtual {v6, v1}, Ljava/util/zip/ZipFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;

    move-result-object v7

    const/4 v8, 0x1

    invoke-static {v7, v3, v8}, Lmiui/resourcebrowser/util/ResourceHelper;->writeTo(Ljava/io/InputStream;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    .line 748
    .local v2, hash:Ljava/lang/String;
    if-eqz p2, :cond_0

    .line 749
    invoke-virtual {v1}, Ljava/util/zip/ZipEntry;->getCompressedSize()J

    move-result-wide v7

    invoke-interface {p2, v3, v7, v8, v2}, Lmiui/resourcebrowser/util/ResourceHelper$UnzipProcessUpdateListener;->onUpdate(Ljava/lang/String;JLjava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 754
    .end local v1           #entry:Ljava/util/zip/ZipEntry;
    .end local v2           #hash:Ljava/lang/String;
    .end local v3           #path:Ljava/lang/String;
    :cond_3
    if-eqz v6, :cond_4

    .line 755
    invoke-virtual {v6}, Ljava/util/zip/ZipFile;->close()V

    .line 758
    :cond_4
    return-void

    .line 754
    .end local v0           #entries:Ljava/util/Enumeration;,"Ljava/util/Enumeration<Ljava/util/zip/ZipEntry;>;"
    .end local v4           #root:Ljava/io/File;
    .end local v6           #zipFile:Ljava/util/zip/ZipFile;
    .restart local v5       #zipFile:Ljava/util/zip/ZipFile;
    :catchall_1
    move-exception v7

    goto :goto_1
.end method

.method public static writeTo(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .parameter "is"
    .parameter "filename"

    .prologue
    .line 761
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lmiui/resourcebrowser/util/ResourceHelper;->writeTo(Ljava/io/InputStream;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static writeTo(Ljava/io/InputStream;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 765
    .line 768
    :try_start_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 769
    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    const/16 v3, 0x1fd

    const/4 v4, -0x1

    const/4 v5, -0x1

    invoke-static {v2, v3, v4, v5}, Lmiui/os/ExtraFileUtils;->mkdirs(Ljava/io/File;III)Z

    .line 771
    new-instance v3, Ljava/io/BufferedInputStream;

    invoke-direct {v3, p0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5

    .line 772
    if-eqz p2, :cond_7

    .line 773
    :try_start_1
    const-string v2, "SHA1"

    invoke-static {v2}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_e
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_a

    move-result-object v2

    .line 774
    :try_start_2
    new-instance p0, Ljava/security/DigestInputStream;

    invoke-direct {p0, v3, v2}, Ljava/security/DigestInputStream;-><init>(Ljava/io/InputStream;Ljava/security/MessageDigest;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_2} :catch_f
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_b

    move-object v3, v2

    .line 777
    :goto_0
    :try_start_3
    new-instance v4, Lorg/apache/http/entity/InputStreamEntity;

    const-wide/16 v5, -0x1

    invoke-direct {v4, p0, v5, v6}, Lorg/apache/http/entity/InputStreamEntity;-><init>(Ljava/io/InputStream;J)V

    .line 778
    new-instance v2, Ljava/io/BufferedOutputStream;

    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v5}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_3 .. :try_end_3} :catch_10
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_c

    .line 779
    const/16 v5, 0x1fd

    const/4 v6, -0x1

    const/4 v7, -0x1

    :try_start_4
    invoke-static {p1, v5, v6, v7}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    .line 780
    invoke-virtual {v4, v2}, Lorg/apache/http/entity/InputStreamEntity;->writeTo(Ljava/io/OutputStream;)V

    .line 781
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Ljava/io/File;->setLastModified(J)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_4 .. :try_end_4} :catch_11
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_d

    .line 787
    if-eqz p0, :cond_0

    .line 789
    :try_start_5
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    .line 794
    :cond_0
    :goto_1
    if-eqz v2, :cond_1

    .line 796
    :try_start_6
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    .line 802
    :cond_1
    :goto_2
    if-nez v3, :cond_6

    :goto_3
    return-object v0

    .line 790
    :catch_0
    move-exception v1

    .line 791
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 797
    :catch_1
    move-exception v1

    .line 798
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 782
    :catch_2
    move-exception v1

    move-object v2, v0

    move-object v3, v0

    .line 783
    :goto_4
    :try_start_7
    invoke-virtual {v1}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 787
    if-eqz p0, :cond_2

    .line 789
    :try_start_8
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    .line 794
    :cond_2
    :goto_5
    if-eqz v2, :cond_1

    .line 796
    :try_start_9
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3

    goto :goto_2

    .line 797
    :catch_3
    move-exception v1

    .line 798
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 790
    :catch_4
    move-exception v1

    .line 791
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 784
    :catch_5
    move-exception v1

    move-object v2, v0

    move-object v3, v0

    .line 785
    :goto_6
    :try_start_a
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 787
    if-eqz p0, :cond_3

    .line 789
    :try_start_b
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_7

    .line 794
    :cond_3
    :goto_7
    if-eqz v2, :cond_1

    .line 796
    :try_start_c
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_6

    goto :goto_2

    .line 797
    :catch_6
    move-exception v1

    .line 798
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 790
    :catch_7
    move-exception v1

    .line 791
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 787
    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_8
    if-eqz p0, :cond_4

    .line 789
    :try_start_d
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_8

    .line 794
    :cond_4
    :goto_9
    if-eqz v2, :cond_5

    .line 796
    :try_start_e
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_9

    .line 799
    :cond_5
    :goto_a
    throw v0

    .line 790
    :catch_8
    move-exception v1

    .line 791
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_9

    .line 797
    :catch_9
    move-exception v1

    .line 798
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_a

    .line 802
    :cond_6
    invoke-virtual {v3}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v0

    invoke-static {v0}, Lmiui/resourcebrowser/util/ResourceHelper;->toHexString([B)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 787
    :catchall_1
    move-exception v1

    move-object v2, v0

    move-object p0, v3

    move-object v0, v1

    goto :goto_8

    :catchall_2
    move-exception v0

    goto :goto_8

    .line 784
    :catch_a
    move-exception v1

    move-object v2, v0

    move-object p0, v3

    move-object v3, v0

    goto :goto_6

    :catch_b
    move-exception v1

    move-object p0, v3

    move-object v3, v2

    move-object v2, v0

    goto :goto_6

    :catch_c
    move-exception v1

    move-object v2, v0

    goto :goto_6

    :catch_d
    move-exception v1

    goto :goto_6

    .line 782
    :catch_e
    move-exception v1

    move-object v2, v0

    move-object p0, v3

    move-object v3, v0

    goto :goto_4

    :catch_f
    move-exception v1

    move-object p0, v3

    move-object v3, v2

    move-object v2, v0

    goto :goto_4

    :catch_10
    move-exception v1

    move-object v2, v0

    goto :goto_4

    :catch_11
    move-exception v1

    goto :goto_4

    :cond_7
    move-object p0, v3

    move-object v3, v0

    goto/16 :goto_0
.end method

.method public static zip(Ljava/util/zip/ZipOutputStream;Ljava/io/File;Ljava/lang/String;)Z
    .locals 16
    .parameter "out"
    .parameter "sourceFolder"
    .parameter "targetFile"

    .prologue
    .line 674
    const/4 v12, 0x1

    .line 675
    .local v12, result:Z
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->isDirectory()Z

    move-result v13

    if-eqz v13, :cond_2

    .line 676
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v8

    .line 677
    .local v8, files:[Ljava/io/File;
    if-eqz v8, :cond_5

    .line 678
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v13

    if-lez v13, :cond_0

    .line 679
    invoke-static/range {p2 .. p2}, Lmiui/os/ExtraFileUtils;->standardizeFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 681
    :cond_0
    const/4 v9, 0x0

    .local v9, i:I
    :goto_0
    array-length v13, v8

    if-ge v9, v13, :cond_5

    .line 682
    aget-object v13, v8, v9

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    aget-object v15, v8, v9

    invoke-virtual {v15}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-static {v0, v13, v14}, Lmiui/resourcebrowser/util/ResourceHelper;->zip(Ljava/util/zip/ZipOutputStream;Ljava/io/File;Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_1

    if-eqz v12, :cond_1

    const/4 v12, 0x1

    .line 681
    :goto_1
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 682
    :cond_1
    const/4 v12, 0x0

    goto :goto_1

    .line 686
    .end local v8           #files:[Ljava/io/File;
    .end local v9           #i:I
    :cond_2
    const/4 v2, 0x0

    .line 687
    .local v2, ch:Ljava/util/zip/CheckedInputStream;
    const/4 v10, 0x0

    .line 689
    .local v10, in:Ljava/io/BufferedInputStream;
    const/16 v13, 0x1000

    :try_start_0
    new-array v1, v13, [B

    .line 690
    .local v1, buffer:[B
    const/4 v4, -0x1

    .line 692
    .local v4, count:I
    new-instance v5, Ljava/util/zip/CRC32;

    invoke-direct {v5}, Ljava/util/zip/CRC32;-><init>()V

    .line 693
    .local v5, crc32:Ljava/util/zip/CRC32;
    new-instance v3, Ljava/util/zip/CheckedInputStream;

    new-instance v13, Ljava/io/BufferedInputStream;

    new-instance v14, Ljava/io/FileInputStream;

    move-object/from16 v0, p1

    invoke-direct {v14, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v13, v14}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v3, v13, v5}, Ljava/util/zip/CheckedInputStream;-><init>(Ljava/io/InputStream;Ljava/util/zip/Checksum;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_7

    .line 694
    .end local v2           #ch:Ljava/util/zip/CheckedInputStream;
    .local v3, ch:Ljava/util/zip/CheckedInputStream;
    :cond_3
    :try_start_1
    invoke-virtual {v3, v1}, Ljava/util/zip/CheckedInputStream;->read([B)I

    move-result v4

    const/4 v13, -0x1

    if-ne v4, v13, :cond_3

    .line 697
    new-instance v7, Ljava/util/zip/ZipEntry;

    move-object/from16 v0, p2

    invoke-direct {v7, v0}, Ljava/util/zip/ZipEntry;-><init>(Ljava/lang/String;)V

    .line 698
    .local v7, entry:Ljava/util/zip/ZipEntry;
    const/4 v13, 0x0

    invoke-virtual {v7, v13}, Ljava/util/zip/ZipEntry;->setMethod(I)V

    .line 699
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->length()J

    move-result-wide v13

    invoke-virtual {v7, v13, v14}, Ljava/util/zip/ZipEntry;->setSize(J)V

    .line 700
    invoke-virtual {v5}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v13

    invoke-virtual {v7, v13, v14}, Ljava/util/zip/ZipEntry;->setCrc(J)V

    .line 701
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Ljava/util/zip/ZipOutputStream;->putNextEntry(Ljava/util/zip/ZipEntry;)V

    .line 703
    new-instance v11, Ljava/io/BufferedInputStream;

    new-instance v13, Ljava/io/FileInputStream;

    move-object/from16 v0, p1

    invoke-direct {v13, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v11, v13}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_8

    .line 704
    .end local v10           #in:Ljava/io/BufferedInputStream;
    .local v11, in:Ljava/io/BufferedInputStream;
    :goto_2
    :try_start_2
    invoke-virtual {v11, v1}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v4

    const/4 v13, -0x1

    if-eq v4, v13, :cond_6

    .line 705
    const/4 v13, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v13, v4}, Ljava/util/zip/ZipOutputStream;->write([BII)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    .line 707
    :catch_0
    move-exception v6

    move-object v10, v11

    .end local v11           #in:Ljava/io/BufferedInputStream;
    .restart local v10       #in:Ljava/io/BufferedInputStream;
    move-object v2, v3

    .line 708
    .end local v1           #buffer:[B
    .end local v3           #ch:Ljava/util/zip/CheckedInputStream;
    .end local v4           #count:I
    .end local v5           #crc32:Ljava/util/zip/CRC32;
    .end local v7           #entry:Ljava/util/zip/ZipEntry;
    .restart local v2       #ch:Ljava/util/zip/CheckedInputStream;
    .local v6, e:Ljava/lang/Exception;
    :goto_3
    const/4 v12, 0x0

    .line 710
    if-eqz v2, :cond_4

    .line 712
    :try_start_3
    invoke-virtual {v2}, Ljava/util/zip/CheckedInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 717
    .end local v6           #e:Ljava/lang/Exception;
    :cond_4
    :goto_4
    if-eqz v10, :cond_5

    .line 719
    :try_start_4
    invoke-virtual {v10}, Ljava/io/BufferedInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    .line 726
    .end local v2           #ch:Ljava/util/zip/CheckedInputStream;
    .end local v10           #in:Ljava/io/BufferedInputStream;
    :cond_5
    :goto_5
    return v12

    .line 710
    .restart local v1       #buffer:[B
    .restart local v3       #ch:Ljava/util/zip/CheckedInputStream;
    .restart local v4       #count:I
    .restart local v5       #crc32:Ljava/util/zip/CRC32;
    .restart local v7       #entry:Ljava/util/zip/ZipEntry;
    .restart local v11       #in:Ljava/io/BufferedInputStream;
    :cond_6
    if-eqz v3, :cond_7

    .line 712
    :try_start_5
    invoke-virtual {v3}, Ljava/util/zip/CheckedInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 717
    :cond_7
    :goto_6
    if-eqz v11, :cond_5

    .line 719
    :try_start_6
    invoke-virtual {v11}, Ljava/io/BufferedInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_5

    .line 720
    :catch_1
    move-exception v6

    .line 721
    .local v6, e:Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 713
    .end local v6           #e:Ljava/io/IOException;
    :catch_2
    move-exception v6

    .line 714
    .restart local v6       #e:Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 713
    .end local v1           #buffer:[B
    .end local v3           #ch:Ljava/util/zip/CheckedInputStream;
    .end local v4           #count:I
    .end local v5           #crc32:Ljava/util/zip/CRC32;
    .end local v7           #entry:Ljava/util/zip/ZipEntry;
    .end local v11           #in:Ljava/io/BufferedInputStream;
    .restart local v2       #ch:Ljava/util/zip/CheckedInputStream;
    .local v6, e:Ljava/lang/Exception;
    .restart local v10       #in:Ljava/io/BufferedInputStream;
    :catch_3
    move-exception v6

    .line 714
    .local v6, e:Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 720
    .end local v6           #e:Ljava/io/IOException;
    :catch_4
    move-exception v6

    .line 721
    .restart local v6       #e:Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 710
    .end local v6           #e:Ljava/io/IOException;
    :catchall_0
    move-exception v13

    :goto_7
    if-eqz v2, :cond_8

    .line 712
    :try_start_7
    invoke-virtual {v2}, Ljava/util/zip/CheckedInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 717
    :cond_8
    :goto_8
    if-eqz v10, :cond_9

    .line 719
    :try_start_8
    invoke-virtual {v10}, Ljava/io/BufferedInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6

    .line 722
    :cond_9
    :goto_9
    throw v13

    .line 713
    :catch_5
    move-exception v6

    .line 714
    .restart local v6       #e:Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8

    .line 720
    .end local v6           #e:Ljava/io/IOException;
    :catch_6
    move-exception v6

    .line 721
    .restart local v6       #e:Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_9

    .line 710
    .end local v2           #ch:Ljava/util/zip/CheckedInputStream;
    .end local v6           #e:Ljava/io/IOException;
    .restart local v1       #buffer:[B
    .restart local v3       #ch:Ljava/util/zip/CheckedInputStream;
    .restart local v4       #count:I
    .restart local v5       #crc32:Ljava/util/zip/CRC32;
    :catchall_1
    move-exception v13

    move-object v2, v3

    .end local v3           #ch:Ljava/util/zip/CheckedInputStream;
    .restart local v2       #ch:Ljava/util/zip/CheckedInputStream;
    goto :goto_7

    .end local v2           #ch:Ljava/util/zip/CheckedInputStream;
    .end local v10           #in:Ljava/io/BufferedInputStream;
    .restart local v3       #ch:Ljava/util/zip/CheckedInputStream;
    .restart local v7       #entry:Ljava/util/zip/ZipEntry;
    .restart local v11       #in:Ljava/io/BufferedInputStream;
    :catchall_2
    move-exception v13

    move-object v10, v11

    .end local v11           #in:Ljava/io/BufferedInputStream;
    .restart local v10       #in:Ljava/io/BufferedInputStream;
    move-object v2, v3

    .end local v3           #ch:Ljava/util/zip/CheckedInputStream;
    .restart local v2       #ch:Ljava/util/zip/CheckedInputStream;
    goto :goto_7

    .line 707
    .end local v1           #buffer:[B
    .end local v4           #count:I
    .end local v5           #crc32:Ljava/util/zip/CRC32;
    .end local v7           #entry:Ljava/util/zip/ZipEntry;
    :catch_7
    move-exception v6

    goto :goto_3

    .end local v2           #ch:Ljava/util/zip/CheckedInputStream;
    .restart local v1       #buffer:[B
    .restart local v3       #ch:Ljava/util/zip/CheckedInputStream;
    .restart local v4       #count:I
    .restart local v5       #crc32:Ljava/util/zip/CRC32;
    :catch_8
    move-exception v6

    move-object v2, v3

    .end local v3           #ch:Ljava/util/zip/CheckedInputStream;
    .restart local v2       #ch:Ljava/util/zip/CheckedInputStream;
    goto :goto_3
.end method
