.class public Lmiui/resourcebrowser/util/ResourceMusicPlayer;
.super Ljava/lang/Object;
.source "ResourceMusicPlayer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/resourcebrowser/util/ResourceMusicPlayer$PlayProgressListener;
    }
.end annotation


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mBatchPlayer:Lmiui/resourcebrowser/util/BatchMediaPlayer;

.field protected mCurrentPlayingButton:Landroid/widget/ImageView;

.field private mCurrentPlayingResource:Lmiui/resourcebrowser/model/Resource;

.field private mHandler:Landroid/os/Handler;

.field private mProgressListener:Lmiui/resourcebrowser/util/ResourceMusicPlayer$PlayProgressListener;

.field private mShowPlayButton:Z

.field private mUserPlaying:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;Z)V
    .locals 1
    .parameter "activity"
    .parameter "showPlayButton"

    .prologue
    .line 51
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Lmiui/resourcebrowser/util/ResourceMusicPlayer$1;

    invoke-direct {v0, p0}, Lmiui/resourcebrowser/util/ResourceMusicPlayer$1;-><init>(Lmiui/resourcebrowser/util/ResourceMusicPlayer;)V

    iput-object v0, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->mHandler:Landroid/os/Handler;

    .line 52
    iput-object p1, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->mActivity:Landroid/app/Activity;

    .line 53
    iput-boolean p2, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->mShowPlayButton:Z

    .line 54
    return-void
.end method

.method static synthetic access$000(Lmiui/resourcebrowser/util/ResourceMusicPlayer;)Lmiui/resourcebrowser/util/BatchMediaPlayer;
    .locals 1
    .parameter "x0"

    .prologue
    .line 18
    iget-object v0, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->mBatchPlayer:Lmiui/resourcebrowser/util/BatchMediaPlayer;

    return-object v0
.end method

.method static synthetic access$100(Lmiui/resourcebrowser/util/ResourceMusicPlayer;)Lmiui/resourcebrowser/util/ResourceMusicPlayer$PlayProgressListener;
    .locals 1
    .parameter "x0"

    .prologue
    .line 18
    iget-object v0, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->mProgressListener:Lmiui/resourcebrowser/util/ResourceMusicPlayer$PlayProgressListener;

    return-object v0
.end method

.method static synthetic access$200(Lmiui/resourcebrowser/util/ResourceMusicPlayer;)Landroid/os/Handler;
    .locals 1
    .parameter "x0"

    .prologue
    .line 18
    iget-object v0, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lmiui/resourcebrowser/util/ResourceMusicPlayer;)Lmiui/resourcebrowser/model/Resource;
    .locals 1
    .parameter "x0"

    .prologue
    .line 18
    iget-object v0, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->mCurrentPlayingResource:Lmiui/resourcebrowser/model/Resource;

    return-object v0
.end method

.method static synthetic access$302(Lmiui/resourcebrowser/util/ResourceMusicPlayer;Lmiui/resourcebrowser/model/Resource;)Lmiui/resourcebrowser/model/Resource;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 18
    iput-object p1, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->mCurrentPlayingResource:Lmiui/resourcebrowser/model/Resource;

    return-object p1
.end method

.method static synthetic access$402(Lmiui/resourcebrowser/util/ResourceMusicPlayer;Z)Z
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 18
    iput-boolean p1, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->mUserPlaying:Z

    return p1
.end method

.method static synthetic access$500(Lmiui/resourcebrowser/util/ResourceMusicPlayer;)Landroid/app/Activity;
    .locals 1
    .parameter "x0"

    .prologue
    .line 18
    iget-object v0, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method private initPlayer()V
    .locals 2

    .prologue
    .line 125
    new-instance v0, Lmiui/resourcebrowser/util/BatchMediaPlayer;

    iget-object v1, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->mActivity:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lmiui/resourcebrowser/util/BatchMediaPlayer;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->mBatchPlayer:Lmiui/resourcebrowser/util/BatchMediaPlayer;

    .line 126
    iget-object v0, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->mBatchPlayer:Lmiui/resourcebrowser/util/BatchMediaPlayer;

    new-instance v1, Lmiui/resourcebrowser/util/ResourceMusicPlayer$3;

    invoke-direct {v1, p0}, Lmiui/resourcebrowser/util/ResourceMusicPlayer$3;-><init>(Lmiui/resourcebrowser/util/ResourceMusicPlayer;)V

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/util/BatchMediaPlayer;->setListener(Lmiui/resourcebrowser/util/BatchMediaPlayer$BatchPlayerListener;)V

    .line 155
    return-void
.end method


# virtual methods
.method public canPlay(Lmiui/resourcebrowser/model/Resource;)Z
    .locals 1
    .parameter "r"

    .prologue
    .line 82
    iget-object v0, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->mCurrentPlayingResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {p1, v0}, Lmiui/resourcebrowser/model/Resource;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getMusicPlayList(Lmiui/resourcebrowser/model/Resource;)Ljava/util/List;
    .locals 1
    .parameter "resourceItem"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiui/resourcebrowser/model/Resource;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 121
    iget-object v0, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->mActivity:Landroid/app/Activity;

    invoke-static {v0, p1}, Lmiui/resourcebrowser/util/ResourceHelper;->getDefaultMusicPlayList(Landroid/content/Context;Lmiui/resourcebrowser/model/Resource;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public initPlayButtonIfNeed(Landroid/widget/ImageView;Lmiui/resourcebrowser/model/Resource;)V
    .locals 1
    .parameter "view"
    .parameter "data"

    .prologue
    .line 57
    iget-boolean v0, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->mShowPlayButton:Z

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 75
    :cond_0
    :goto_0
    return-void

    .line 60
    :cond_1
    iget-object v0, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->mCurrentPlayingResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {p2, v0}, Lmiui/resourcebrowser/model/Resource;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 61
    const v0, 0x60200dc

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 65
    :goto_1
    new-instance v0, Lmiui/resourcebrowser/util/ResourceMusicPlayer$2;

    invoke-direct {v0, p0, p2, p1}, Lmiui/resourcebrowser/util/ResourceMusicPlayer$2;-><init>(Lmiui/resourcebrowser/util/ResourceMusicPlayer;Lmiui/resourcebrowser/model/Resource;Landroid/widget/ImageView;)V

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 63
    :cond_2
    const v0, 0x60200d9

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1
.end method

.method public isPlaying()Z
    .locals 1

    .prologue
    .line 117
    iget-boolean v0, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->mUserPlaying:Z

    return v0
.end method

.method public playMusic(Landroid/widget/ImageView;Lmiui/resourcebrowser/model/Resource;)V
    .locals 2
    .parameter "v"
    .parameter "r"

    .prologue
    .line 86
    iget-boolean v0, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->mShowPlayButton:Z

    if-nez v0, :cond_0

    .line 87
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "ResourceMusicPlayer does not support playing button."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 89
    :cond_0
    const v0, 0x60200dc

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 90
    iput-object p1, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->mCurrentPlayingButton:Landroid/widget/ImageView;

    .line 91
    invoke-virtual {p0, p2}, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->playMusic(Lmiui/resourcebrowser/model/Resource;)V

    .line 92
    return-void
.end method

.method public playMusic(Lmiui/resourcebrowser/model/Resource;)V
    .locals 2
    .parameter "r"

    .prologue
    .line 95
    iput-object p1, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->mCurrentPlayingResource:Lmiui/resourcebrowser/model/Resource;

    .line 96
    iget-object v0, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->mBatchPlayer:Lmiui/resourcebrowser/util/BatchMediaPlayer;

    if-nez v0, :cond_0

    .line 97
    invoke-direct {p0}, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->initPlayer()V

    .line 99
    :cond_0
    iget-object v0, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->mBatchPlayer:Lmiui/resourcebrowser/util/BatchMediaPlayer;

    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->getMusicPlayList(Lmiui/resourcebrowser/model/Resource;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/util/BatchMediaPlayer;->setPlayList(Ljava/util/List;)V

    .line 100
    iget-object v0, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->mBatchPlayer:Lmiui/resourcebrowser/util/BatchMediaPlayer;

    invoke-virtual {v0}, Lmiui/resourcebrowser/util/BatchMediaPlayer;->start()V

    .line 101
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->mUserPlaying:Z

    .line 102
    return-void
.end method

.method public regeistePlayProgressListener(Lmiui/resourcebrowser/util/ResourceMusicPlayer$PlayProgressListener;)V
    .locals 0
    .parameter "l"

    .prologue
    .line 78
    iput-object p1, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->mProgressListener:Lmiui/resourcebrowser/util/ResourceMusicPlayer$PlayProgressListener;

    .line 79
    return-void
.end method

.method public stopMusic()V
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->mBatchPlayer:Lmiui/resourcebrowser/util/BatchMediaPlayer;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->mBatchPlayer:Lmiui/resourcebrowser/util/BatchMediaPlayer;

    invoke-virtual {v0}, Lmiui/resourcebrowser/util/BatchMediaPlayer;->stop()V

    .line 108
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->mCurrentPlayingResource:Lmiui/resourcebrowser/model/Resource;

    .line 109
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->mUserPlaying:Z

    .line 110
    return-void
.end method
