.class Lmiui/resourcebrowser/util/ImageCacheDecoder$JobRun;
.super Ljava/lang/Object;
.source "ImageCacheDecoder.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/resourcebrowser/util/ImageCacheDecoder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "JobRun"
.end annotation


# instance fields
.field private final downloadingJob:Z

.field private imageLocalPath:Ljava/lang/String;

.field private imageOnlinePath:Ljava/lang/String;

.field private submitTimeForDebug:J

.field final synthetic this$0:Lmiui/resourcebrowser/util/ImageCacheDecoder;

.field private useIndex:I


# direct methods
.method public constructor <init>(Lmiui/resourcebrowser/util/ImageCacheDecoder;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4
    .parameter
    .parameter "imageLocalPath"
    .parameter "imageOnlinePath"
    .parameter "useIndex"

    .prologue
    .line 160
    iput-object p1, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$JobRun;->this$0:Lmiui/resourcebrowser/util/ImageCacheDecoder;

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 161
    iput-object p2, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$JobRun;->imageLocalPath:Ljava/lang/String;

    .line 162
    iput-object p3, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$JobRun;->imageOnlinePath:Ljava/lang/String;

    .line 163
    iput p4, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$JobRun;->useIndex:I

    .line 164
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$JobRun;->downloadingJob:Z

    .line 165
    sget-boolean v0, Lmiui/resourcebrowser/util/ResourceDebug;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 166
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$JobRun;->submitTimeForDebug:J

    .line 167
    const-string v1, "Theme"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "submit loading wallpaper: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v0, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$JobRun;->downloadingJob:Z

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " online "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    :cond_0
    return-void

    .line 164
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 167
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " local "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public dispatchJob()V
    .locals 1

    .prologue
    .line 174
    iget-boolean v0, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$JobRun;->downloadingJob:Z

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$JobRun;->this$0:Lmiui/resourcebrowser/util/ImageCacheDecoder;

    #getter for: Lmiui/resourcebrowser/util/ImageCacheDecoder;->mDownloadExecutorService:Ljava/util/concurrent/ExecutorService;
    invoke-static {v0}, Lmiui/resourcebrowser/util/ImageCacheDecoder;->access$100(Lmiui/resourcebrowser/util/ImageCacheDecoder;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 179
    :goto_0
    return-void

    .line 177
    :cond_0
    iget-object v0, p0, Lmiui/resourcebrowser/util/ImageCacheDecoder$JobRun;->this$0:Lmiui/resourcebrowser/util/ImageCacheDecoder;

    #getter for: Lmiui/resourcebrowser/util/ImageCacheDecoder;->mDecodeExecutorService:Ljava/util/concurrent/ExecutorService;
    invoke-static {v0}, Lmiui/resourcebrowser/util/ImageCacheDecoder;->access$200(Lmiui/resourcebrowser/util/ImageCacheDecoder;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method public run()V
    .locals 20

    .prologue
    .line 183
    const/4 v12, 0x0

    .line 185
    .local v12, result:Z
    move-object/from16 v0, p0

    iget-object v15, v0, Lmiui/resourcebrowser/util/ImageCacheDecoder$JobRun;->this$0:Lmiui/resourcebrowser/util/ImageCacheDecoder;

    iget-object v15, v15, Lmiui/resourcebrowser/util/ImageCacheDecoder;->mBitmapCache:Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;

    move-object/from16 v0, p0

    iget v0, v0, Lmiui/resourcebrowser/util/ImageCacheDecoder$JobRun;->useIndex:I

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->inCacheScope(I)Z

    move-result v15

    if-eqz v15, :cond_b

    .line 186
    sget-boolean v15, Lmiui/resourcebrowser/util/ResourceDebug;->DEBUG:Z

    if-eqz v15, :cond_4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 188
    .local v8, jobStartTime:J
    :goto_0
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lmiui/resourcebrowser/util/ImageCacheDecoder$JobRun;->downloadingJob:Z

    if-eqz v15, :cond_7

    .line 190
    move-object/from16 v0, p0

    iget-object v15, v0, Lmiui/resourcebrowser/util/ImageCacheDecoder$JobRun;->imageOnlinePath:Ljava/lang/String;

    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_5

    .line 191
    const/4 v12, 0x0

    .line 214
    :cond_0
    :goto_1
    sget-boolean v15, Lmiui/resourcebrowser/util/ResourceDebug;->DEBUG:Z

    if-eqz v15, :cond_2

    .line 215
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 216
    .local v6, jobEndTime:J
    new-instance v15, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/resourcebrowser/util/ImageCacheDecoder$JobRun;->imageLocalPath:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-direct/range {v15 .. v16}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15}, Ljava/io/File;->length()J

    move-result-wide v4

    .line 217
    .local v4, fileSize:J
    const-string v13, ""

    .line 218
    .local v13, speed:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lmiui/resourcebrowser/util/ImageCacheDecoder$JobRun;->downloadingJob:Z

    if-eqz v15, :cond_1

    .line 219
    const-string v15, "%.1fKB/s"

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    sub-long v18, v6, v8

    div-long v18, v4, v18

    move-wide/from16 v0, v18

    long-to-float v0, v0

    move/from16 v18, v0

    const v19, 0x3f79db23

    mul-float v18, v18, v19

    invoke-static/range {v18 .. v18}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-static/range {v15 .. v16}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    .line 221
    :cond_1
    const-string v16, "Theme"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "finish loading wallpaper: "

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget v0, v0, Lmiui/resourcebrowser/util/ImageCacheDecoder$JobRun;->useIndex:I

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v17, " "

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    if-eqz v12, :cond_9

    const-string v15, "success"

    :goto_2
    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v17, " "

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lmiui/resourcebrowser/util/ImageCacheDecoder$JobRun;->downloadingJob:Z

    if-eqz v15, :cond_a

    const-string v15, "online"

    :goto_3
    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v17, " "

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-static {v4, v5}, Lmiui/resourcebrowser/util/ResourceHelper;->formatFileSize(J)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v17, " "

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-wide v0, v0, Lmiui/resourcebrowser/util/ImageCacheDecoder$JobRun;->submitTimeForDebug:J

    move-wide/from16 v17, v0

    sub-long v17, v6, v17

    move-wide/from16 v0, v17

    invoke-virtual {v15, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v17, " "

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sub-long v17, v6, v8

    move-wide/from16 v0, v17

    invoke-virtual {v15, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v17, " "

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, v16

    invoke-static {v0, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    .end local v4           #fileSize:J
    .end local v6           #jobEndTime:J
    .end local v8           #jobStartTime:J
    .end local v13           #speed:Ljava/lang/String;
    :cond_2
    :goto_4
    move-object/from16 v0, p0

    iget-object v15, v0, Lmiui/resourcebrowser/util/ImageCacheDecoder$JobRun;->this$0:Lmiui/resourcebrowser/util/ImageCacheDecoder;

    #getter for: Lmiui/resourcebrowser/util/ImageCacheDecoder;->mDoingJob:Ljava/util/HashSet;
    invoke-static {v15}, Lmiui/resourcebrowser/util/ImageCacheDecoder;->access$400(Lmiui/resourcebrowser/util/ImageCacheDecoder;)Ljava/util/HashSet;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/resourcebrowser/util/ImageCacheDecoder$JobRun;->imageLocalPath:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 239
    move-object/from16 v0, p0

    iget-object v15, v0, Lmiui/resourcebrowser/util/ImageCacheDecoder$JobRun;->this$0:Lmiui/resourcebrowser/util/ImageCacheDecoder;

    iget-object v15, v15, Lmiui/resourcebrowser/util/ImageCacheDecoder;->mBitmapCache:Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;

    move-object/from16 v0, p0

    iget v0, v0, Lmiui/resourcebrowser/util/ImageCacheDecoder$JobRun;->useIndex:I

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Lmiui/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->inCacheScope(I)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 240
    move-object/from16 v0, p0

    iget-object v15, v0, Lmiui/resourcebrowser/util/ImageCacheDecoder$JobRun;->this$0:Lmiui/resourcebrowser/util/ImageCacheDecoder;

    #getter for: Lmiui/resourcebrowser/util/ImageCacheDecoder;->mHandler:Landroid/os/Handler;
    invoke-static {v15}, Lmiui/resourcebrowser/util/ImageCacheDecoder;->access$500(Lmiui/resourcebrowser/util/ImageCacheDecoder;)Landroid/os/Handler;

    move-result-object v15

    invoke-virtual {v15}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v11

    .line 241
    .local v11, msg:Landroid/os/Message;
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lmiui/resourcebrowser/util/ImageCacheDecoder$JobRun;->downloadingJob:Z

    if-eqz v15, :cond_d

    const/4 v15, 0x1

    :goto_5
    iput v15, v11, Landroid/os/Message;->what:I

    .line 242
    if-eqz v12, :cond_e

    const/4 v15, 0x0

    :goto_6
    iput v15, v11, Landroid/os/Message;->arg1:I

    .line 243
    new-instance v15, Landroid/util/Pair;

    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/resourcebrowser/util/ImageCacheDecoder$JobRun;->imageLocalPath:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/resourcebrowser/util/ImageCacheDecoder$JobRun;->imageOnlinePath:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-direct/range {v15 .. v17}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v15, v11, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 244
    move-object/from16 v0, p0

    iget-object v15, v0, Lmiui/resourcebrowser/util/ImageCacheDecoder$JobRun;->this$0:Lmiui/resourcebrowser/util/ImageCacheDecoder;

    #getter for: Lmiui/resourcebrowser/util/ImageCacheDecoder;->mHandler:Landroid/os/Handler;
    invoke-static {v15}, Lmiui/resourcebrowser/util/ImageCacheDecoder;->access$500(Lmiui/resourcebrowser/util/ImageCacheDecoder;)Landroid/os/Handler;

    move-result-object v15

    invoke-virtual {v15, v11}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 246
    .end local v11           #msg:Landroid/os/Message;
    :cond_3
    return-void

    .line 186
    :cond_4
    const-wide/16 v8, 0x0

    goto/16 :goto_0

    .line 193
    .restart local v8       #jobStartTime:J
    :cond_5
    new-instance v14, Lmiui/resourcebrowser/controller/online/DownloadFileTask;

    move-object/from16 v0, p0

    iget-object v15, v0, Lmiui/resourcebrowser/util/ImageCacheDecoder$JobRun;->imageOnlinePath:Ljava/lang/String;

    invoke-direct {v14, v15}, Lmiui/resourcebrowser/controller/online/DownloadFileTask;-><init>(Ljava/lang/String;)V

    .line 194
    .local v14, task:Lmiui/resourcebrowser/controller/online/DownloadFileTask;
    new-instance v3, Lmiui/resourcebrowser/model/PathEntry;

    move-object/from16 v0, p0

    iget-object v15, v0, Lmiui/resourcebrowser/util/ImageCacheDecoder$JobRun;->imageLocalPath:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/resourcebrowser/util/ImageCacheDecoder$JobRun;->imageOnlinePath:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-direct {v3, v15, v0}, Lmiui/resourcebrowser/model/PathEntry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    .local v3, entry:Lmiui/resourcebrowser/model/PathEntry;
    const/4 v15, 0x1

    new-array v15, v15, [Lmiui/resourcebrowser/model/PathEntry;

    const/16 v16, 0x0

    aput-object v3, v15, v16

    invoke-virtual {v14, v15}, Lmiui/resourcebrowser/controller/online/DownloadFileTask;->downloadFiles([Lmiui/resourcebrowser/model/PathEntry;)Z

    .line 197
    new-instance v10, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v15, v0, Lmiui/resourcebrowser/util/ImageCacheDecoder$JobRun;->imageLocalPath:Ljava/lang/String;

    invoke-direct {v10, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 198
    .local v10, localFile:Ljava/io/File;
    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v15

    if-eqz v15, :cond_6

    invoke-virtual {v10}, Ljava/io/File;->length()J

    move-result-wide v15

    const-wide/16 v17, 0x400

    cmp-long v15, v15, v17

    if-gez v15, :cond_6

    .line 199
    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    .line 201
    :cond_6
    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v12

    .line 203
    move-object/from16 v0, p0

    iget-object v15, v0, Lmiui/resourcebrowser/util/ImageCacheDecoder$JobRun;->this$0:Lmiui/resourcebrowser/util/ImageCacheDecoder;

    #getter for: Lmiui/resourcebrowser/util/ImageCacheDecoder;->mFailToDownloadTime:Ljava/util/HashMap;
    invoke-static {v15}, Lmiui/resourcebrowser/util/ImageCacheDecoder;->access$300(Lmiui/resourcebrowser/util/ImageCacheDecoder;)Ljava/util/HashMap;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/resourcebrowser/util/ImageCacheDecoder$JobRun;->imageOnlinePath:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    if-nez v12, :cond_0

    .line 205
    move-object/from16 v0, p0

    iget-object v15, v0, Lmiui/resourcebrowser/util/ImageCacheDecoder$JobRun;->this$0:Lmiui/resourcebrowser/util/ImageCacheDecoder;

    #getter for: Lmiui/resourcebrowser/util/ImageCacheDecoder;->mFailToDownloadTime:Ljava/util/HashMap;
    invoke-static {v15}, Lmiui/resourcebrowser/util/ImageCacheDecoder;->access$300(Lmiui/resourcebrowser/util/ImageCacheDecoder;)Ljava/util/HashMap;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/resourcebrowser/util/ImageCacheDecoder$JobRun;->imageOnlinePath:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v17

    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    invoke-virtual/range {v15 .. v17}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 210
    .end local v3           #entry:Lmiui/resourcebrowser/model/PathEntry;
    .end local v10           #localFile:Ljava/io/File;
    .end local v14           #task:Lmiui/resourcebrowser/controller/online/DownloadFileTask;
    :cond_7
    move-object/from16 v0, p0

    iget-object v15, v0, Lmiui/resourcebrowser/util/ImageCacheDecoder$JobRun;->this$0:Lmiui/resourcebrowser/util/ImageCacheDecoder;

    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/resourcebrowser/util/ImageCacheDecoder$JobRun;->imageLocalPath:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lmiui/resourcebrowser/util/ImageCacheDecoder$JobRun;->useIndex:I

    move/from16 v17, v0

    const/16 v18, 0x1

    invoke-virtual/range {v15 .. v18}, Lmiui/resourcebrowser/util/ImageCacheDecoder;->decodeLocalImage(Ljava/lang/String;IZ)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 211
    .local v2, b:Landroid/graphics/Bitmap;
    if-eqz v2, :cond_8

    const/4 v12, 0x1

    :goto_7
    goto/16 :goto_1

    :cond_8
    const/4 v12, 0x0

    goto :goto_7

    .line 221
    .end local v2           #b:Landroid/graphics/Bitmap;
    .restart local v4       #fileSize:J
    .restart local v6       #jobEndTime:J
    .restart local v13       #speed:Ljava/lang/String;
    :cond_9
    const-string v15, "fail   "

    goto/16 :goto_2

    :cond_a
    const-string v15, "local "

    goto/16 :goto_3

    .line 231
    .end local v4           #fileSize:J
    .end local v6           #jobEndTime:J
    .end local v8           #jobStartTime:J
    .end local v13           #speed:Ljava/lang/String;
    :cond_b
    sget-boolean v15, Lmiui/resourcebrowser/util/ResourceDebug;->DEBUG:Z

    if-eqz v15, :cond_2

    .line 232
    const-string v16, "Theme"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "ingore loading wallpaper: "

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget v0, v0, Lmiui/resourcebrowser/util/ImageCacheDecoder$JobRun;->useIndex:I

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lmiui/resourcebrowser/util/ImageCacheDecoder$JobRun;->downloadingJob:Z

    if-eqz v15, :cond_c

    const-string v15, " online "

    :goto_8
    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, v16

    invoke-static {v0, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    :cond_c
    const-string v15, " local"

    goto :goto_8

    .line 241
    .restart local v11       #msg:Landroid/os/Message;
    :cond_d
    const/4 v15, 0x0

    goto/16 :goto_5

    .line 242
    :cond_e
    const/4 v15, 0x1

    goto/16 :goto_6
.end method
