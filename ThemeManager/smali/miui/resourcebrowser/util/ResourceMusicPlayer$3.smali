.class Lmiui/resourcebrowser/util/ResourceMusicPlayer$3;
.super Ljava/lang/Object;
.source "ResourceMusicPlayer.java"

# interfaces
.implements Lmiui/resourcebrowser/util/BatchMediaPlayer$BatchPlayerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiui/resourcebrowser/util/ResourceMusicPlayer;->initPlayer()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/resourcebrowser/util/ResourceMusicPlayer;


# direct methods
.method constructor <init>(Lmiui/resourcebrowser/util/ResourceMusicPlayer;)V
    .locals 0
    .parameter

    .prologue
    .line 126
    iput-object p1, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer$3;->this$0:Lmiui/resourcebrowser/util/ResourceMusicPlayer;

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public finish(Z)V
    .locals 4
    .parameter "hasError"

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 138
    iget-object v0, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer$3;->this$0:Lmiui/resourcebrowser/util/ResourceMusicPlayer;

    #setter for: Lmiui/resourcebrowser/util/ResourceMusicPlayer;->mUserPlaying:Z
    invoke-static {v0, v2}, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->access$402(Lmiui/resourcebrowser/util/ResourceMusicPlayer;Z)Z

    .line 139
    iget-object v0, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer$3;->this$0:Lmiui/resourcebrowser/util/ResourceMusicPlayer;

    #setter for: Lmiui/resourcebrowser/util/ResourceMusicPlayer;->mCurrentPlayingResource:Lmiui/resourcebrowser/model/Resource;
    invoke-static {v0, v3}, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->access$302(Lmiui/resourcebrowser/util/ResourceMusicPlayer;Lmiui/resourcebrowser/model/Resource;)Lmiui/resourcebrowser/model/Resource;

    .line 141
    iget-object v0, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer$3;->this$0:Lmiui/resourcebrowser/util/ResourceMusicPlayer;

    iget-object v0, v0, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->mCurrentPlayingButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer$3;->this$0:Lmiui/resourcebrowser/util/ResourceMusicPlayer;

    iget-object v0, v0, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->mCurrentPlayingButton:Landroid/widget/ImageView;

    const v1, 0x60200d9

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 144
    :cond_0
    iget-object v0, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer$3;->this$0:Lmiui/resourcebrowser/util/ResourceMusicPlayer;

    iput-object v3, v0, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->mCurrentPlayingButton:Landroid/widget/ImageView;

    .line 146
    iget-object v0, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer$3;->this$0:Lmiui/resourcebrowser/util/ResourceMusicPlayer;

    #getter for: Lmiui/resourcebrowser/util/ResourceMusicPlayer;->mProgressListener:Lmiui/resourcebrowser/util/ResourceMusicPlayer$PlayProgressListener;
    invoke-static {v0}, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->access$100(Lmiui/resourcebrowser/util/ResourceMusicPlayer;)Lmiui/resourcebrowser/util/ResourceMusicPlayer$PlayProgressListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 147
    iget-object v0, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer$3;->this$0:Lmiui/resourcebrowser/util/ResourceMusicPlayer;

    #getter for: Lmiui/resourcebrowser/util/ResourceMusicPlayer;->mProgressListener:Lmiui/resourcebrowser/util/ResourceMusicPlayer$PlayProgressListener;
    invoke-static {v0}, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->access$100(Lmiui/resourcebrowser/util/ResourceMusicPlayer;)Lmiui/resourcebrowser/util/ResourceMusicPlayer$PlayProgressListener;

    move-result-object v0

    invoke-interface {v0}, Lmiui/resourcebrowser/util/ResourceMusicPlayer$PlayProgressListener;->onStopPlaying()V

    .line 150
    :cond_1
    if-eqz p1, :cond_2

    .line 151
    iget-object v0, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer$3;->this$0:Lmiui/resourcebrowser/util/ResourceMusicPlayer;

    #getter for: Lmiui/resourcebrowser/util/ResourceMusicPlayer;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->access$500(Lmiui/resourcebrowser/util/ResourceMusicPlayer;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x60c0020

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 153
    :cond_2
    return-void
.end method

.method public play(Ljava/lang/String;II)V
    .locals 4
    .parameter "ringtonePath"
    .parameter "current"
    .parameter "total"

    .prologue
    .line 130
    iget-object v0, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer$3;->this$0:Lmiui/resourcebrowser/util/ResourceMusicPlayer;

    #getter for: Lmiui/resourcebrowser/util/ResourceMusicPlayer;->mProgressListener:Lmiui/resourcebrowser/util/ResourceMusicPlayer$PlayProgressListener;
    invoke-static {v0}, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->access$100(Lmiui/resourcebrowser/util/ResourceMusicPlayer;)Lmiui/resourcebrowser/util/ResourceMusicPlayer$PlayProgressListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer$3;->this$0:Lmiui/resourcebrowser/util/ResourceMusicPlayer;

    #getter for: Lmiui/resourcebrowser/util/ResourceMusicPlayer;->mProgressListener:Lmiui/resourcebrowser/util/ResourceMusicPlayer$PlayProgressListener;
    invoke-static {v0}, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->access$100(Lmiui/resourcebrowser/util/ResourceMusicPlayer;)Lmiui/resourcebrowser/util/ResourceMusicPlayer$PlayProgressListener;

    move-result-object v0

    invoke-interface {v0}, Lmiui/resourcebrowser/util/ResourceMusicPlayer$PlayProgressListener;->onStartPlaying()V

    .line 132
    iget-object v0, p0, Lmiui/resourcebrowser/util/ResourceMusicPlayer$3;->this$0:Lmiui/resourcebrowser/util/ResourceMusicPlayer;

    #getter for: Lmiui/resourcebrowser/util/ResourceMusicPlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lmiui/resourcebrowser/util/ResourceMusicPlayer;->access$200(Lmiui/resourcebrowser/util/ResourceMusicPlayer;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x0

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 134
    :cond_0
    return-void
.end method
