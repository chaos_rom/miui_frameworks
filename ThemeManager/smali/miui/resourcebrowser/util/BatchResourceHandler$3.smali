.class Lmiui/resourcebrowser/util/BatchResourceHandler$3;
.super Ljava/lang/Object;
.source "BatchResourceHandler.java"

# interfaces
.implements Landroid/view/ActionMode$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/resourcebrowser/util/BatchResourceHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/resourcebrowser/util/BatchResourceHandler;


# direct methods
.method constructor <init>(Lmiui/resourcebrowser/util/BatchResourceHandler;)V
    .locals 0
    .parameter

    .prologue
    .line 96
    iput-object p1, p0, Lmiui/resourcebrowser/util/BatchResourceHandler$3;->this$0:Lmiui/resourcebrowser/util/BatchResourceHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 7
    .parameter "mode"
    .parameter "item"

    .prologue
    const/4 v6, 0x1

    .line 130
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x60c0176

    if-ne v0, v1, :cond_0

    .line 131
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lmiui/resourcebrowser/util/BatchResourceHandler$3;->this$0:Lmiui/resourcebrowser/util/BatchResourceHandler;

    iget-object v1, v1, Lmiui/resourcebrowser/util/BatchResourceHandler;->mActivity:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x1010355

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lmiui/resourcebrowser/util/BatchResourceHandler$3;->this$0:Lmiui/resourcebrowser/util/BatchResourceHandler;

    iget-object v1, v1, Lmiui/resourcebrowser/util/BatchResourceHandler;->mActivity:Landroid/app/Activity;

    const v2, 0x60c01b7

    new-array v3, v6, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lmiui/resourcebrowser/util/BatchResourceHandler$3;->this$0:Lmiui/resourcebrowser/util/BatchResourceHandler;

    #getter for: Lmiui/resourcebrowser/util/BatchResourceHandler;->mCheckedResource:Ljava/util/Set;
    invoke-static {v5}, Lmiui/resourcebrowser/util/BatchResourceHandler;->access$200(Lmiui/resourcebrowser/util/BatchResourceHandler;)Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x104

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    new-instance v2, Lmiui/resourcebrowser/util/BatchResourceHandler$3$1;

    invoke-direct {v2, p0}, Lmiui/resourcebrowser/util/BatchResourceHandler$3$1;-><init>(Lmiui/resourcebrowser/util/BatchResourceHandler$3;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 141
    :cond_0
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x60c0017

    if-ne v0, v1, :cond_1

    .line 142
    iget-object v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler$3;->this$0:Lmiui/resourcebrowser/util/BatchResourceHandler;

    #calls: Lmiui/resourcebrowser/util/BatchResourceHandler;->deleteOrDownloadResources()V
    invoke-static {v0}, Lmiui/resourcebrowser/util/BatchResourceHandler;->access$100(Lmiui/resourcebrowser/util/BatchResourceHandler;)V

    .line 144
    :cond_1
    return v6
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 7
    .parameter "mode"
    .parameter "menu"

    .prologue
    const/4 v5, 0x0

    .line 110
    const v3, 0x60c0017

    .line 111
    .local v3, titleResId:I
    const v2, 0x6020019

    .line 112
    .local v2, iconResId:I
    iget-object v4, p0, Lmiui/resourcebrowser/util/BatchResourceHandler$3;->this$0:Lmiui/resourcebrowser/util/BatchResourceHandler;

    iget-boolean v4, v4, Lmiui/resourcebrowser/util/BatchResourceHandler;->mIsLocalSource:Z

    if-eqz v4, :cond_0

    .line 113
    const v3, 0x60c0176

    .line 114
    const v2, 0x60201a9

    .line 117
    :cond_0
    invoke-interface {p2, v5, v3, v5, v3}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v1

    .line 118
    .local v1, deleteItem:Landroid/view/MenuItem;
    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 120
    iget-object v4, p0, Lmiui/resourcebrowser/util/BatchResourceHandler$3;->this$0:Lmiui/resourcebrowser/util/BatchResourceHandler;

    iget-object v4, v4, Lmiui/resourcebrowser/util/BatchResourceHandler;->mActivity:Landroid/app/Activity;

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x603001e

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 121
    .local v0, customView:Landroid/view/View;
    invoke-virtual {p1, v0}, Landroid/view/ActionMode;->setCustomView(Landroid/view/View;)V

    .line 122
    iget-object v5, p0, Lmiui/resourcebrowser/util/BatchResourceHandler$3;->this$0:Lmiui/resourcebrowser/util/BatchResourceHandler;

    const v4, 0x60b0002

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    #setter for: Lmiui/resourcebrowser/util/BatchResourceHandler;->mSelectTextView:Landroid/widget/TextView;
    invoke-static {v5, v4}, Lmiui/resourcebrowser/util/BatchResourceHandler;->access$002(Lmiui/resourcebrowser/util/BatchResourceHandler;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 123
    iget-object v4, p0, Lmiui/resourcebrowser/util/BatchResourceHandler$3;->this$0:Lmiui/resourcebrowser/util/BatchResourceHandler;

    invoke-virtual {v4}, Lmiui/resourcebrowser/util/BatchResourceHandler;->updateSelectedTitle()V

    .line 125
    const/4 v4, 0x1

    return v4
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 1
    .parameter "mode"

    .prologue
    .line 105
    iget-object v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler$3;->this$0:Lmiui/resourcebrowser/util/BatchResourceHandler;

    invoke-virtual {v0}, Lmiui/resourcebrowser/util/BatchResourceHandler;->quitEditMode()V

    .line 106
    return-void
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 1
    .parameter "mode"
    .parameter "menu"

    .prologue
    .line 100
    const/4 v0, 0x0

    return v0
.end method
