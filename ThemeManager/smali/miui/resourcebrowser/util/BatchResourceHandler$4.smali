.class Lmiui/resourcebrowser/util/BatchResourceHandler$4;
.super Landroid/os/AsyncTask;
.source "BatchResourceHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiui/resourcebrowser/util/BatchResourceHandler;->deleteOrDownloadResources()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private mProgress:Landroid/app/ProgressDialog;

.field final synthetic this$0:Lmiui/resourcebrowser/util/BatchResourceHandler;


# direct methods
.method constructor <init>(Lmiui/resourcebrowser/util/BatchResourceHandler;)V
    .locals 0
    .parameter

    .prologue
    .line 176
    iput-object p1, p0, Lmiui/resourcebrowser/util/BatchResourceHandler$4;->this$0:Lmiui/resourcebrowser/util/BatchResourceHandler;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"

    .prologue
    .line 176
    check-cast p1, [Ljava/lang/Void;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/util/BatchResourceHandler$4;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 5
    .parameter "params"

    .prologue
    .line 189
    iget-object v3, p0, Lmiui/resourcebrowser/util/BatchResourceHandler$4;->this$0:Lmiui/resourcebrowser/util/BatchResourceHandler;

    iget-boolean v3, v3, Lmiui/resourcebrowser/util/BatchResourceHandler;->mIsLocalSource:Z

    if-eqz v3, :cond_1

    .line 193
    iget-object v3, p0, Lmiui/resourcebrowser/util/BatchResourceHandler$4;->this$0:Lmiui/resourcebrowser/util/BatchResourceHandler;

    iget-object v3, v3, Lmiui/resourcebrowser/util/BatchResourceHandler;->mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;

    invoke-virtual {v3}, Lmiui/resourcebrowser/activity/ResourceAdapter;->getDataSet()Ljava/util/List;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/resourcebrowser/widget/DataGroup;

    .line 194
    .local v1, originDataSet:Lmiui/resourcebrowser/widget/DataGroup;,"Lmiui/resourcebrowser/widget/DataGroup<Lmiui/resourcebrowser/model/Resource;>;"
    invoke-virtual {v1}, Lmiui/resourcebrowser/widget/DataGroup;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/resourcebrowser/model/Resource;

    .line 195
    .local v2, r:Lmiui/resourcebrowser/model/Resource;
    iget-object v3, p0, Lmiui/resourcebrowser/util/BatchResourceHandler$4;->this$0:Lmiui/resourcebrowser/util/BatchResourceHandler;

    #getter for: Lmiui/resourcebrowser/util/BatchResourceHandler;->mCheckedResource:Ljava/util/Set;
    invoke-static {v3}, Lmiui/resourcebrowser/util/BatchResourceHandler;->access$200(Lmiui/resourcebrowser/util/BatchResourceHandler;)Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 196
    iget-object v3, p0, Lmiui/resourcebrowser/util/BatchResourceHandler$4;->this$0:Lmiui/resourcebrowser/util/BatchResourceHandler;

    iget-object v3, v3, Lmiui/resourcebrowser/util/BatchResourceHandler;->mResController:Lmiui/resourcebrowser/controller/ResourceController;

    invoke-virtual {v3}, Lmiui/resourcebrowser/controller/ResourceController;->getLocalDataManager()Lmiui/resourcebrowser/controller/LocalDataManager;

    move-result-object v3

    invoke-virtual {v3, v2}, Lmiui/resourcebrowser/controller/LocalDataManager;->removeResource(Lmiui/resourcebrowser/model/Resource;)Z

    goto :goto_0

    .line 203
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #originDataSet:Lmiui/resourcebrowser/widget/DataGroup;,"Lmiui/resourcebrowser/widget/DataGroup<Lmiui/resourcebrowser/model/Resource;>;"
    .end local v2           #r:Lmiui/resourcebrowser/model/Resource;
    :cond_1
    iget-object v3, p0, Lmiui/resourcebrowser/util/BatchResourceHandler$4;->this$0:Lmiui/resourcebrowser/util/BatchResourceHandler;

    #getter for: Lmiui/resourcebrowser/util/BatchResourceHandler;->mCheckedResource:Ljava/util/Set;
    invoke-static {v3}, Lmiui/resourcebrowser/util/BatchResourceHandler;->access$200(Lmiui/resourcebrowser/util/BatchResourceHandler;)Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .restart local v0       #i$:Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/resourcebrowser/model/Resource;

    .line 204
    .restart local v2       #r:Lmiui/resourcebrowser/model/Resource;
    iget-object v3, p0, Lmiui/resourcebrowser/util/BatchResourceHandler$4;->this$0:Lmiui/resourcebrowser/util/BatchResourceHandler;

    invoke-virtual {v3, v2}, Lmiui/resourcebrowser/util/BatchResourceHandler;->doDownloadResource(Lmiui/resourcebrowser/model/Resource;)V

    goto :goto_1

    .line 207
    .end local v2           #r:Lmiui/resourcebrowser/model/Resource;
    :cond_2
    const/4 v3, 0x0

    return-object v3
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 176
    check-cast p1, Ljava/lang/Void;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/util/BatchResourceHandler$4;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 1
    .parameter "result"

    .prologue
    .line 211
    iget-object v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler$4;->this$0:Lmiui/resourcebrowser/util/BatchResourceHandler;

    invoke-virtual {v0}, Lmiui/resourcebrowser/util/BatchResourceHandler;->quitEditMode()V

    .line 212
    iget-object v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler$4;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 213
    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 180
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lmiui/resourcebrowser/util/BatchResourceHandler$4;->this$0:Lmiui/resourcebrowser/util/BatchResourceHandler;

    iget-object v1, v1, Lmiui/resourcebrowser/util/BatchResourceHandler;->mActivity:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler$4;->mProgress:Landroid/app/ProgressDialog;

    .line 181
    iget-object v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler$4;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 182
    iget-object v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler$4;->mProgress:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lmiui/resourcebrowser/util/BatchResourceHandler$4;->this$0:Lmiui/resourcebrowser/util/BatchResourceHandler;

    iget-object v1, v1, Lmiui/resourcebrowser/util/BatchResourceHandler;->mActivity:Landroid/app/Activity;

    const v2, 0x60c01b8

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 183
    iget-object v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler$4;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 184
    iget-object v0, p0, Lmiui/resourcebrowser/util/BatchResourceHandler$4;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 185
    return-void
.end method
