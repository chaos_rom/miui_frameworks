.class Lmiui/resourcebrowser/util/BatchMediaPlayer$3;
.super Ljava/lang/Object;
.source "BatchMediaPlayer.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiui/resourcebrowser/util/BatchMediaPlayer;->start()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/resourcebrowser/util/BatchMediaPlayer;


# direct methods
.method constructor <init>(Lmiui/resourcebrowser/util/BatchMediaPlayer;)V
    .locals 0
    .parameter

    .prologue
    .line 77
    iput-object p1, p0, Lmiui/resourcebrowser/util/BatchMediaPlayer$3;->this$0:Lmiui/resourcebrowser/util/BatchMediaPlayer;

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 4
    .parameter "mp"

    .prologue
    .line 80
    const/16 v2, 0x3e8

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v3

    rsub-int v3, v3, 0x7d0

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    int-to-long v0, v2

    .line 81
    .local v0, delay:J
    const-wide/16 v2, 0x1f4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 82
    iget-object v2, p0, Lmiui/resourcebrowser/util/BatchMediaPlayer$3;->this$0:Lmiui/resourcebrowser/util/BatchMediaPlayer;

    #getter for: Lmiui/resourcebrowser/util/BatchMediaPlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lmiui/resourcebrowser/util/BatchMediaPlayer;->access$200(Lmiui/resourcebrowser/util/BatchMediaPlayer;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lmiui/resourcebrowser/util/BatchMediaPlayer$3;->this$0:Lmiui/resourcebrowser/util/BatchMediaPlayer;

    #getter for: Lmiui/resourcebrowser/util/BatchMediaPlayer;->mBatchPlayRun:Ljava/lang/Runnable;
    invoke-static {v3}, Lmiui/resourcebrowser/util/BatchMediaPlayer;->access$100(Lmiui/resourcebrowser/util/BatchMediaPlayer;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 83
    iget-object v2, p0, Lmiui/resourcebrowser/util/BatchMediaPlayer$3;->this$0:Lmiui/resourcebrowser/util/BatchMediaPlayer;

    #getter for: Lmiui/resourcebrowser/util/BatchMediaPlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lmiui/resourcebrowser/util/BatchMediaPlayer;->access$200(Lmiui/resourcebrowser/util/BatchMediaPlayer;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lmiui/resourcebrowser/util/BatchMediaPlayer$3;->this$0:Lmiui/resourcebrowser/util/BatchMediaPlayer;

    #getter for: Lmiui/resourcebrowser/util/BatchMediaPlayer;->mBatchPlayRun:Ljava/lang/Runnable;
    invoke-static {v3}, Lmiui/resourcebrowser/util/BatchMediaPlayer;->access$100(Lmiui/resourcebrowser/util/BatchMediaPlayer;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 84
    return-void
.end method
