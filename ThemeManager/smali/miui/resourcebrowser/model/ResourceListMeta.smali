.class public Lmiui/resourcebrowser/model/ResourceListMeta;
.super Ljava/lang/Object;
.source "ResourceListMeta.java"


# instance fields
.field private category:Ljava/lang/String;

.field private description:Ljava/lang/String;

.field private name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lmiui/resourcebrowser/model/ResourceListMeta;->description:Ljava/lang/String;

    return-object v0
.end method

.method public setCategory(Ljava/lang/String;)V
    .locals 0
    .parameter "category"

    .prologue
    .line 32
    iput-object p1, p0, Lmiui/resourcebrowser/model/ResourceListMeta;->category:Ljava/lang/String;

    .line 33
    return-void
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 0
    .parameter "description"

    .prologue
    .line 24
    iput-object p1, p0, Lmiui/resourcebrowser/model/ResourceListMeta;->description:Ljava/lang/String;

    .line 25
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .parameter "name"

    .prologue
    .line 16
    iput-object p1, p0, Lmiui/resourcebrowser/model/ResourceListMeta;->name:Ljava/lang/String;

    .line 17
    return-void
.end method
