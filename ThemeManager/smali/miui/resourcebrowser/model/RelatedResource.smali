.class public Lmiui/resourcebrowser/model/RelatedResource;
.super Ljava/lang/Object;
.source "RelatedResource.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final EXTRA_META:Ljava/lang/String; = "extraMeta"

.field public static final FILE_PATH:Ljava/lang/String; = "filePath"

.field public static final LOCAL_PATH:Ljava/lang/String; = "localPath"

.field public static final RESOURCE_CODE:Ljava/lang/String; = "resourceCode"

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private extraMeta:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private filePath:Ljava/lang/String;

.field private localPath:Ljava/lang/String;

.field private resourceCode:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/model/RelatedResource;->extraMeta:Ljava/util/Map;

    return-void
.end method

.method private equals(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .parameter "obj1"
    .parameter "obj2"

    .prologue
    .line 108
    if-eq p1, p2, :cond_0

    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .parameter "obj"

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 82
    if-ne p0, p1, :cond_1

    .line 104
    :cond_0
    :goto_0
    return v1

    .line 85
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 86
    goto :goto_0

    .line 88
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 89
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 91
    check-cast v0, Lmiui/resourcebrowser/model/RelatedResource;

    .line 92
    .local v0, other:Lmiui/resourcebrowser/model/RelatedResource;
    iget-object v3, p0, Lmiui/resourcebrowser/model/RelatedResource;->localPath:Ljava/lang/String;

    iget-object v4, v0, Lmiui/resourcebrowser/model/RelatedResource;->localPath:Ljava/lang/String;

    invoke-direct {p0, v3, v4}, Lmiui/resourcebrowser/model/RelatedResource;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 93
    goto :goto_0

    .line 95
    :cond_4
    iget-object v3, p0, Lmiui/resourcebrowser/model/RelatedResource;->filePath:Ljava/lang/String;

    iget-object v4, v0, Lmiui/resourcebrowser/model/RelatedResource;->filePath:Ljava/lang/String;

    invoke-direct {p0, v3, v4}, Lmiui/resourcebrowser/model/RelatedResource;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    .line 96
    goto :goto_0

    .line 98
    :cond_5
    iget-object v3, p0, Lmiui/resourcebrowser/model/RelatedResource;->resourceCode:Ljava/lang/String;

    iget-object v4, v0, Lmiui/resourcebrowser/model/RelatedResource;->resourceCode:Ljava/lang/String;

    invoke-direct {p0, v3, v4}, Lmiui/resourcebrowser/model/RelatedResource;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    move v1, v2

    .line 99
    goto :goto_0

    .line 101
    :cond_6
    iget-object v3, p0, Lmiui/resourcebrowser/model/RelatedResource;->extraMeta:Ljava/util/Map;

    iget-object v4, v0, Lmiui/resourcebrowser/model/RelatedResource;->extraMeta:Ljava/util/Map;

    invoke-direct {p0, v3, v4}, Lmiui/resourcebrowser/model/RelatedResource;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 102
    goto :goto_0
.end method

.method public getExtraMeta(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .parameter "key"

    .prologue
    .line 57
    iget-object v0, p0, Lmiui/resourcebrowser/model/RelatedResource;->extraMeta:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getExtraMeta(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter "key"
    .parameter "defaultValue"

    .prologue
    .line 61
    iget-object v1, p0, Lmiui/resourcebrowser/model/RelatedResource;->extraMeta:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 62
    .local v0, result:Ljava/lang/String;
    if-eqz v0, :cond_0

    .end local v0           #result:Ljava/lang/String;
    :goto_0
    return-object v0

    .restart local v0       #result:Ljava/lang/String;
    :cond_0
    move-object v0, p2

    goto :goto_0
.end method

.method public getExtraMeta()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Lmiui/resourcebrowser/model/RelatedResource;->extraMeta:Ljava/util/Map;

    return-object v0
.end method

.method public getFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lmiui/resourcebrowser/model/RelatedResource;->filePath:Ljava/lang/String;

    return-object v0
.end method

.method public getLocalPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lmiui/resourcebrowser/model/RelatedResource;->localPath:Ljava/lang/String;

    return-object v0
.end method

.method public getResourceCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lmiui/resourcebrowser/model/RelatedResource;->resourceCode:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 71
    const/16 v0, 0x1f

    .line 72
    .local v0, prime:I
    const/4 v1, 0x1

    .line 73
    .local v1, result:I
    iget-object v2, p0, Lmiui/resourcebrowser/model/RelatedResource;->extraMeta:Ljava/util/Map;

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int/lit8 v1, v2, 0x1f

    .line 74
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lmiui/resourcebrowser/model/RelatedResource;->filePath:Ljava/lang/String;

    if-nez v2, :cond_1

    move v2, v3

    :goto_1
    add-int v1, v4, v2

    .line 75
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lmiui/resourcebrowser/model/RelatedResource;->localPath:Ljava/lang/String;

    if-nez v2, :cond_2

    move v2, v3

    :goto_2
    add-int v1, v4, v2

    .line 76
    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lmiui/resourcebrowser/model/RelatedResource;->resourceCode:Ljava/lang/String;

    if-nez v4, :cond_3

    :goto_3
    add-int v1, v2, v3

    .line 77
    return v1

    .line 73
    :cond_0
    iget-object v2, p0, Lmiui/resourcebrowser/model/RelatedResource;->extraMeta:Ljava/util/Map;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_0

    .line 74
    :cond_1
    iget-object v2, p0, Lmiui/resourcebrowser/model/RelatedResource;->filePath:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    .line 75
    :cond_2
    iget-object v2, p0, Lmiui/resourcebrowser/model/RelatedResource;->localPath:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2

    .line 76
    :cond_3
    iget-object v3, p0, Lmiui/resourcebrowser/model/RelatedResource;->resourceCode:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    goto :goto_3
.end method

.method public putExtraMeta(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter "key"
    .parameter "value"

    .prologue
    .line 66
    iget-object v0, p0, Lmiui/resourcebrowser/model/RelatedResource;->extraMeta:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    return-void
.end method

.method public setExtraMeta(Ljava/util/Map;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 53
    .local p1, extraMeta:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iput-object p1, p0, Lmiui/resourcebrowser/model/RelatedResource;->extraMeta:Ljava/util/Map;

    .line 54
    return-void
.end method

.method public setFilePath(Ljava/lang/String;)V
    .locals 0
    .parameter "filePath"

    .prologue
    .line 37
    iput-object p1, p0, Lmiui/resourcebrowser/model/RelatedResource;->filePath:Ljava/lang/String;

    .line 38
    return-void
.end method

.method public setLocalPath(Ljava/lang/String;)V
    .locals 0
    .parameter "localPath"

    .prologue
    .line 29
    iput-object p1, p0, Lmiui/resourcebrowser/model/RelatedResource;->localPath:Ljava/lang/String;

    .line 30
    return-void
.end method

.method public setResourceCode(Ljava/lang/String;)V
    .locals 0
    .parameter "resourceCode"

    .prologue
    .line 45
    iput-object p1, p0, Lmiui/resourcebrowser/model/RelatedResource;->resourceCode:Ljava/lang/String;

    .line 46
    return-void
.end method
