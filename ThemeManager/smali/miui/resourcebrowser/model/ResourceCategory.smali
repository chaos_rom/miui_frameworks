.class public Lmiui/resourcebrowser/model/ResourceCategory;
.super Ljava/lang/Object;
.source "ResourceCategory.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Comparable",
        "<",
        "Lmiui/resourcebrowser/model/ResourceCategory;",
        ">;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private code:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private type:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .parameter "x0"

    .prologue
    .line 5
    check-cast p1, Lmiui/resourcebrowser/model/ResourceCategory;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/model/ResourceCategory;->compareTo(Lmiui/resourcebrowser/model/ResourceCategory;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lmiui/resourcebrowser/model/ResourceCategory;)I
    .locals 2
    .parameter "another"

    .prologue
    .line 41
    iget-object v0, p0, Lmiui/resourcebrowser/model/ResourceCategory;->code:Ljava/lang/String;

    invoke-virtual {p1}, Lmiui/resourcebrowser/model/ResourceCategory;->getCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lmiui/resourcebrowser/model/ResourceCategory;->code:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lmiui/resourcebrowser/model/ResourceCategory;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getType()J
    .locals 2

    .prologue
    .line 16
    iget-wide v0, p0, Lmiui/resourcebrowser/model/ResourceCategory;->type:J

    return-wide v0
.end method

.method public setCode(Ljava/lang/String;)V
    .locals 0
    .parameter "code"

    .prologue
    .line 28
    iput-object p1, p0, Lmiui/resourcebrowser/model/ResourceCategory;->code:Ljava/lang/String;

    .line 29
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .parameter "name"

    .prologue
    .line 36
    iput-object p1, p0, Lmiui/resourcebrowser/model/ResourceCategory;->name:Ljava/lang/String;

    .line 37
    return-void
.end method

.method public setType(J)V
    .locals 0
    .parameter "type"

    .prologue
    .line 20
    iput-wide p1, p0, Lmiui/resourcebrowser/model/ResourceCategory;->type:J

    .line 21
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lmiui/resourcebrowser/model/ResourceCategory;->name:Ljava/lang/String;

    return-object v0
.end method
