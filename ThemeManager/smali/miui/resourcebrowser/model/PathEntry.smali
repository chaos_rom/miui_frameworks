.class public Lmiui/resourcebrowser/model/PathEntry;
.super Ljava/lang/Object;
.source "PathEntry.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final LOCAL_PATH:Ljava/lang/String; = "localPath"

.field public static final ONLINE_PATH:Ljava/lang/String; = "onlinePath"

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private localPath:Ljava/lang/String;

.field private onlinePath:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 16
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .parameter "localPath"
    .parameter "onlinePath"

    .prologue
    .line 18
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lmiui/resourcebrowser/model/PathEntry;->localPath:Ljava/lang/String;

    .line 20
    iput-object p2, p0, Lmiui/resourcebrowser/model/PathEntry;->onlinePath:Ljava/lang/String;

    .line 21
    return-void
.end method

.method private equals(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .parameter "obj1"
    .parameter "obj2"

    .prologue
    .line 70
    if-eq p1, p2, :cond_0

    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .parameter "obj"

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 50
    if-ne p0, p1, :cond_1

    .line 66
    :cond_0
    :goto_0
    return v1

    .line 53
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 54
    goto :goto_0

    .line 56
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 57
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 59
    check-cast v0, Lmiui/resourcebrowser/model/PathEntry;

    .line 60
    .local v0, other:Lmiui/resourcebrowser/model/PathEntry;
    iget-object v3, p0, Lmiui/resourcebrowser/model/PathEntry;->localPath:Ljava/lang/String;

    iget-object v4, v0, Lmiui/resourcebrowser/model/PathEntry;->localPath:Ljava/lang/String;

    invoke-direct {p0, v3, v4}, Lmiui/resourcebrowser/model/PathEntry;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 61
    goto :goto_0

    .line 63
    :cond_4
    iget-object v3, p0, Lmiui/resourcebrowser/model/PathEntry;->onlinePath:Ljava/lang/String;

    iget-object v4, v0, Lmiui/resourcebrowser/model/PathEntry;->onlinePath:Ljava/lang/String;

    invoke-direct {p0, v3, v4}, Lmiui/resourcebrowser/model/PathEntry;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 64
    goto :goto_0
.end method

.method public getLocalPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lmiui/resourcebrowser/model/PathEntry;->localPath:Ljava/lang/String;

    return-object v0
.end method

.method public getOnlinePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lmiui/resourcebrowser/model/PathEntry;->onlinePath:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 41
    const/16 v0, 0x1f

    .line 42
    .local v0, prime:I
    const/4 v1, 0x1

    .line 43
    .local v1, result:I
    iget-object v2, p0, Lmiui/resourcebrowser/model/PathEntry;->localPath:Ljava/lang/String;

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int/lit8 v1, v2, 0x1f

    .line 44
    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lmiui/resourcebrowser/model/PathEntry;->onlinePath:Ljava/lang/String;

    if-nez v4, :cond_1

    :goto_1
    add-int v1, v2, v3

    .line 45
    return v1

    .line 43
    :cond_0
    iget-object v2, p0, Lmiui/resourcebrowser/model/PathEntry;->localPath:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    .line 44
    :cond_1
    iget-object v3, p0, Lmiui/resourcebrowser/model/PathEntry;->onlinePath:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    goto :goto_1
.end method

.method public setLocalPath(Ljava/lang/String;)V
    .locals 0
    .parameter "localPath"

    .prologue
    .line 28
    iput-object p1, p0, Lmiui/resourcebrowser/model/PathEntry;->localPath:Ljava/lang/String;

    .line 29
    return-void
.end method

.method public setOnlinePath(Ljava/lang/String;)V
    .locals 0
    .parameter "onlinePath"

    .prologue
    .line 36
    iput-object p1, p0, Lmiui/resourcebrowser/model/PathEntry;->onlinePath:Ljava/lang/String;

    .line 37
    return-void
.end method
