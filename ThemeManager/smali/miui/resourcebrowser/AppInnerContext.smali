.class public Lmiui/resourcebrowser/AppInnerContext;
.super Ljava/lang/Object;
.source "AppInnerContext.java"


# static fields
.field private static instance:Lmiui/resourcebrowser/AppInnerContext;


# instance fields
.field private applicationContext:Landroid/content/Context;

.field private resourceContext:Lmiui/resourcebrowser/ResourceContext;

.field private resourceController:Lmiui/resourcebrowser/controller/ResourceController;

.field private workingDataSet:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/widget/DataGroup",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    new-instance v0, Lmiui/resourcebrowser/AppInnerContext;

    invoke-direct {v0}, Lmiui/resourcebrowser/AppInnerContext;-><init>()V

    sput-object v0, Lmiui/resourcebrowser/AppInnerContext;->instance:Lmiui/resourcebrowser/AppInnerContext;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 20
    return-void
.end method

.method public static getInstance()Lmiui/resourcebrowser/AppInnerContext;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lmiui/resourcebrowser/AppInnerContext;->instance:Lmiui/resourcebrowser/AppInnerContext;

    return-object v0
.end method


# virtual methods
.method public getApplicationContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lmiui/resourcebrowser/AppInnerContext;->applicationContext:Landroid/content/Context;

    return-object v0
.end method

.method public getResourceContext()Lmiui/resourcebrowser/ResourceContext;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lmiui/resourcebrowser/AppInnerContext;->resourceContext:Lmiui/resourcebrowser/ResourceContext;

    return-object v0
.end method

.method public getResourceController()Lmiui/resourcebrowser/controller/ResourceController;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lmiui/resourcebrowser/AppInnerContext;->resourceController:Lmiui/resourcebrowser/controller/ResourceController;

    return-object v0
.end method

.method public getWorkingDataSet()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/widget/DataGroup",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lmiui/resourcebrowser/AppInnerContext;->workingDataSet:Ljava/util/List;

    return-object v0
.end method

.method public setApplicationContext(Landroid/content/Context;)V
    .locals 0
    .parameter "applicationContext"

    .prologue
    .line 35
    iput-object p1, p0, Lmiui/resourcebrowser/AppInnerContext;->applicationContext:Landroid/content/Context;

    .line 36
    return-void
.end method

.method public setResourceContext(Lmiui/resourcebrowser/ResourceContext;)V
    .locals 0
    .parameter "resouceContext"

    .prologue
    .line 43
    iput-object p1, p0, Lmiui/resourcebrowser/AppInnerContext;->resourceContext:Lmiui/resourcebrowser/ResourceContext;

    .line 44
    return-void
.end method

.method public setResourceController(Lmiui/resourcebrowser/controller/ResourceController;)V
    .locals 0
    .parameter "resourceController"

    .prologue
    .line 51
    iput-object p1, p0, Lmiui/resourcebrowser/AppInnerContext;->resourceController:Lmiui/resourcebrowser/controller/ResourceController;

    .line 52
    return-void
.end method

.method public setWorkingDataSet(Ljava/util/List;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/widget/DataGroup",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 59
    .local p1, workingDataSet:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/widget/DataGroup<Lmiui/resourcebrowser/model/Resource;>;>;"
    iput-object p1, p0, Lmiui/resourcebrowser/AppInnerContext;->workingDataSet:Ljava/util/List;

    .line 60
    return-void
.end method
