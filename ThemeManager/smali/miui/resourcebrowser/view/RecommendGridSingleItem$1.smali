.class Lmiui/resourcebrowser/view/RecommendGridSingleItem$1;
.super Ljava/lang/Object;
.source "RecommendGridSingleItem.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiui/resourcebrowser/view/RecommendGridSingleItem;->getOnClickListener(Lmiui/resourcebrowser/ResourceContext;)Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/resourcebrowser/view/RecommendGridSingleItem;

.field final synthetic val$resContext:Lmiui/resourcebrowser/ResourceContext;


# direct methods
.method constructor <init>(Lmiui/resourcebrowser/view/RecommendGridSingleItem;Lmiui/resourcebrowser/ResourceContext;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 27
    iput-object p1, p0, Lmiui/resourcebrowser/view/RecommendGridSingleItem$1;->this$0:Lmiui/resourcebrowser/view/RecommendGridSingleItem;

    iput-object p2, p0, Lmiui/resourcebrowser/view/RecommendGridSingleItem$1;->val$resContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .parameter "v"

    .prologue
    const/4 v5, 0x0

    .line 30
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 31
    .local v2, intent:Landroid/content/Intent;
    iget-object v3, p0, Lmiui/resourcebrowser/view/RecommendGridSingleItem$1;->this$0:Lmiui/resourcebrowser/view/RecommendGridSingleItem;

    iget-object v4, p0, Lmiui/resourcebrowser/view/RecommendGridSingleItem$1;->val$resContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v3, v4}, Lmiui/resourcebrowser/view/RecommendGridSingleItem;->getResourceDetailActivity(Lmiui/resourcebrowser/ResourceContext;)Landroid/util/Pair;

    move-result-object v0

    .line 32
    .local v0, activityClass:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v3, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    iget-object v4, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 33
    const/high16 v3, 0x400

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 34
    const-string v3, "REQUEST_RES_GROUP"

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 35
    const-string v3, "REQUEST_RES_INDEX"

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 36
    const-string v3, "REQUEST_SOURCE_TYPE"

    const/4 v4, 0x2

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 37
    const-string v3, "REQUEST_RECOMMEND_ID"

    iget-object v4, p0, Lmiui/resourcebrowser/view/RecommendGridSingleItem$1;->this$0:Lmiui/resourcebrowser/view/RecommendGridSingleItem;

    iget-object v4, v4, Lmiui/resourcebrowser/view/RecommendGridBaseItem;->mRecommendItemData:Lmiui/resourcebrowser/model/RecommendItemData;

    iget-object v4, v4, Lmiui/resourcebrowser/model/RecommendItemData;->itemId:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 38
    invoke-static {}, Lmiui/resourcebrowser/AppInnerContext;->getInstance()Lmiui/resourcebrowser/AppInnerContext;

    move-result-object v1

    .line 39
    .local v1, appContext:Lmiui/resourcebrowser/AppInnerContext;
    iget-object v3, p0, Lmiui/resourcebrowser/view/RecommendGridSingleItem$1;->this$0:Lmiui/resourcebrowser/view/RecommendGridSingleItem;

    iget-object v4, p0, Lmiui/resourcebrowser/view/RecommendGridSingleItem$1;->val$resContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v3, v4}, Lmiui/resourcebrowser/view/RecommendGridSingleItem;->buildResourceSet(Lmiui/resourcebrowser/ResourceContext;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v1, v3}, Lmiui/resourcebrowser/AppInnerContext;->setWorkingDataSet(Ljava/util/List;)V

    .line 40
    iget-object v3, p0, Lmiui/resourcebrowser/view/RecommendGridSingleItem$1;->val$resContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v1, v3}, Lmiui/resourcebrowser/AppInnerContext;->setResourceContext(Lmiui/resourcebrowser/ResourceContext;)V

    .line 41
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lmiui/resourcebrowser/AppInnerContext;->setResourceController(Lmiui/resourcebrowser/controller/ResourceController;)V

    .line 42
    iget-object v3, p0, Lmiui/resourcebrowser/view/RecommendGridSingleItem$1;->this$0:Lmiui/resourcebrowser/view/RecommendGridSingleItem;

    #getter for: Lmiui/resourcebrowser/view/RecommendGridSingleItem;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lmiui/resourcebrowser/view/RecommendGridSingleItem;->access$000(Lmiui/resourcebrowser/view/RecommendGridSingleItem;)Landroid/content/Context;

    move-result-object v3

    check-cast v3, Landroid/app/Activity;

    const/4 v4, 0x1

    invoke-virtual {v3, v2, v4}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 43
    return-void
.end method
