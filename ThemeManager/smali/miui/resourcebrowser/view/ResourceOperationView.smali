.class public Lmiui/resourcebrowser/view/ResourceOperationView;
.super Landroid/widget/LinearLayout;
.source "ResourceOperationView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/resourcebrowser/view/ResourceOperationView$ResourceOperationListener;,
        Lmiui/resourcebrowser/view/ResourceOperationView$State;
    }
.end annotation


# instance fields
.field protected mApplyBtn:Landroid/widget/TextView;

.field protected mControlBtns:Landroid/view/View;

.field protected mDeleteBtn:Landroid/widget/ImageView;

.field protected mDownloadBtn:Landroid/widget/TextView;

.field protected mDownloadProgress:Landroid/widget/ProgressBar;

.field protected mHandler:Landroid/os/Handler;

.field protected mListener:Lmiui/resourcebrowser/view/ResourceOperationView$ResourceOperationListener;

.field protected mLoadingProgress:Landroid/view/View;

.field protected mLoadingTextView:Landroid/widget/TextView;

.field protected mMagicBtn:Landroid/widget/ImageView;

.field protected mMagicBtnResId:I

.field protected mResourceHandler:Lmiui/resourcebrowser/view/ResourceOperationHandler;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .parameter "context"

    .prologue
    .line 57
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 65
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 66
    return-void
.end method

.method static synthetic access$000(Lmiui/resourcebrowser/view/ResourceOperationView;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 23
    invoke-direct {p0}, Lmiui/resourcebrowser/view/ResourceOperationView;->notifyDownloadEventPerformed()V

    return-void
.end method

.method static synthetic access$100(Lmiui/resourcebrowser/view/ResourceOperationView;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 23
    invoke-direct {p0}, Lmiui/resourcebrowser/view/ResourceOperationView;->notifyBuyEventPerformed()V

    return-void
.end method

.method static synthetic access$200(Lmiui/resourcebrowser/view/ResourceOperationView;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 23
    invoke-direct {p0}, Lmiui/resourcebrowser/view/ResourceOperationView;->notifyUpdateEventPerformed()V

    return-void
.end method

.method static synthetic access$300(Lmiui/resourcebrowser/view/ResourceOperationView;Landroid/view/View;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lmiui/resourcebrowser/view/ResourceOperationView;->disableAndDelayEnableView(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$400(Lmiui/resourcebrowser/view/ResourceOperationView;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 23
    invoke-direct {p0}, Lmiui/resourcebrowser/view/ResourceOperationView;->notifyPickEventPerformed()V

    return-void
.end method

.method static synthetic access$500(Lmiui/resourcebrowser/view/ResourceOperationView;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 23
    invoke-direct {p0}, Lmiui/resourcebrowser/view/ResourceOperationView;->notifyApplyEventPerformed()V

    return-void
.end method

.method static synthetic access$600(Lmiui/resourcebrowser/view/ResourceOperationView;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 23
    invoke-direct {p0}, Lmiui/resourcebrowser/view/ResourceOperationView;->notifyDeleteEventPerformed()V

    return-void
.end method

.method static synthetic access$700(Lmiui/resourcebrowser/view/ResourceOperationView;)Landroid/content/Context;
    .locals 1
    .parameter "x0"

    .prologue
    .line 23
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$800(Lmiui/resourcebrowser/view/ResourceOperationView;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 23
    invoke-direct {p0}, Lmiui/resourcebrowser/view/ResourceOperationView;->notifyMagicEventPerformed()V

    return-void
.end method

.method static synthetic access$900(Lmiui/resourcebrowser/view/ResourceOperationView;ZLjava/lang/String;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Lmiui/resourcebrowser/view/ResourceOperationView;->updateLoadingUI(ZLjava/lang/String;)V

    return-void
.end method

.method private disableAndDelayEnableView(Landroid/view/View;)V
    .locals 4
    .parameter "v"

    .prologue
    .line 163
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 164
    invoke-virtual {p0}, Lmiui/resourcebrowser/view/ResourceOperationView;->getHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lmiui/resourcebrowser/view/ResourceOperationView$1;

    invoke-direct {v1, p0, p1}, Lmiui/resourcebrowser/view/ResourceOperationView$1;-><init>(Lmiui/resourcebrowser/view/ResourceOperationView;Landroid/view/View;)V

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 170
    return-void
.end method

.method private formatPrice(I)Ljava/lang/String;
    .locals 6
    .parameter "price"

    .prologue
    const/4 v5, 0x0

    .line 356
    const/4 v0, 0x0

    .line 357
    .local v0, ret:Ljava/lang/String;
    if-nez p1, :cond_1

    .line 358
    iget-object v1, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mContext:Landroid/content/Context;

    const v2, 0x60c0232

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 369
    :cond_0
    :goto_0
    return-object v0

    .line 359
    :cond_1
    if-lez p1, :cond_0

    .line 360
    const-string v1, "%.2f"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    int-to-float v3, p1

    const/high16 v4, 0x42c8

    div-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 361
    :goto_1
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x30

    if-ne v1, v2, :cond_2

    .line 362
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 364
    :cond_2
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x2e

    if-ne v1, v2, :cond_3

    .line 365
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 367
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mContext:Landroid/content/Context;

    const v3, 0x60c0233

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private notifyApplyEventPerformed()V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mListener:Lmiui/resourcebrowser/view/ResourceOperationView$ResourceOperationListener;

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mListener:Lmiui/resourcebrowser/view/ResourceOperationView$ResourceOperationListener;

    invoke-interface {v0}, Lmiui/resourcebrowser/view/ResourceOperationView$ResourceOperationListener;->onApplyEventPerformed()V

    .line 97
    :cond_0
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mResourceHandler:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    invoke-virtual {v0}, Lmiui/resourcebrowser/view/ResourceOperationHandler;->onApplyEventPerformed()V

    .line 98
    invoke-virtual {p0}, Lmiui/resourcebrowser/view/ResourceOperationView;->updateStatus()V

    .line 99
    return-void
.end method

.method private notifyBuyEventPerformed()V
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mListener:Lmiui/resourcebrowser/view/ResourceOperationView$ResourceOperationListener;

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mListener:Lmiui/resourcebrowser/view/ResourceOperationView$ResourceOperationListener;

    invoke-interface {v0}, Lmiui/resourcebrowser/view/ResourceOperationView$ResourceOperationListener;->onBuyEventPerformed()V

    .line 105
    :cond_0
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mResourceHandler:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    invoke-virtual {v0}, Lmiui/resourcebrowser/view/ResourceOperationHandler;->onBuyEventPerformed()V

    .line 106
    invoke-virtual {p0}, Lmiui/resourcebrowser/view/ResourceOperationView;->updateStatus()V

    .line 107
    return-void
.end method

.method private notifyDeleteEventPerformed()V
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mListener:Lmiui/resourcebrowser/view/ResourceOperationView$ResourceOperationListener;

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mListener:Lmiui/resourcebrowser/view/ResourceOperationView$ResourceOperationListener;

    invoke-interface {v0}, Lmiui/resourcebrowser/view/ResourceOperationView$ResourceOperationListener;->onDeleteEventPerformed()V

    .line 137
    :cond_0
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mResourceHandler:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    invoke-virtual {v0}, Lmiui/resourcebrowser/view/ResourceOperationHandler;->onDeleteEventPerformed()V

    .line 138
    invoke-virtual {p0}, Lmiui/resourcebrowser/view/ResourceOperationView;->updateStatus()V

    .line 139
    return-void
.end method

.method private notifyDownloadEventPerformed()V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mListener:Lmiui/resourcebrowser/view/ResourceOperationView$ResourceOperationListener;

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mListener:Lmiui/resourcebrowser/view/ResourceOperationView$ResourceOperationListener;

    invoke-interface {v0}, Lmiui/resourcebrowser/view/ResourceOperationView$ResourceOperationListener;->onDownloadEventPerformed()V

    .line 121
    :cond_0
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mResourceHandler:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    invoke-virtual {v0}, Lmiui/resourcebrowser/view/ResourceOperationHandler;->onDownloadEventPerformed()V

    .line 122
    invoke-virtual {p0}, Lmiui/resourcebrowser/view/ResourceOperationView;->updateStatus()V

    .line 123
    return-void
.end method

.method private notifyMagicEventPerformed()V
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mListener:Lmiui/resourcebrowser/view/ResourceOperationView$ResourceOperationListener;

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mListener:Lmiui/resourcebrowser/view/ResourceOperationView$ResourceOperationListener;

    invoke-interface {v0}, Lmiui/resourcebrowser/view/ResourceOperationView$ResourceOperationListener;->onMagicEventPerformed()V

    .line 145
    :cond_0
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mResourceHandler:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    invoke-virtual {v0}, Lmiui/resourcebrowser/view/ResourceOperationHandler;->onMagicEventPerformed()V

    .line 146
    invoke-virtual {p0}, Lmiui/resourcebrowser/view/ResourceOperationView;->updateStatus()V

    .line 147
    return-void
.end method

.method private notifyPickEventPerformed()V
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mListener:Lmiui/resourcebrowser/view/ResourceOperationView$ResourceOperationListener;

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mListener:Lmiui/resourcebrowser/view/ResourceOperationView$ResourceOperationListener;

    invoke-interface {v0}, Lmiui/resourcebrowser/view/ResourceOperationView$ResourceOperationListener;->onPickEventPerformed()V

    .line 113
    :cond_0
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mResourceHandler:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    invoke-virtual {v0}, Lmiui/resourcebrowser/view/ResourceOperationHandler;->onPickEventPerformed()V

    .line 114
    invoke-virtual {p0}, Lmiui/resourcebrowser/view/ResourceOperationView;->updateStatus()V

    .line 115
    return-void
.end method

.method private notifyUpdateEventPerformed()V
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mListener:Lmiui/resourcebrowser/view/ResourceOperationView$ResourceOperationListener;

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mListener:Lmiui/resourcebrowser/view/ResourceOperationView$ResourceOperationListener;

    invoke-interface {v0}, Lmiui/resourcebrowser/view/ResourceOperationView$ResourceOperationListener;->onUpdateEventPerformed()V

    .line 129
    :cond_0
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mResourceHandler:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    invoke-virtual {v0}, Lmiui/resourcebrowser/view/ResourceOperationHandler;->onUpdateEventPerformed()V

    .line 130
    invoke-virtual {p0}, Lmiui/resourcebrowser/view/ResourceOperationView;->updateStatus()V

    .line 131
    return-void
.end method

.method private setApplyStatus()V
    .locals 4

    .prologue
    .line 295
    const/4 v1, 0x0

    .line 296
    .local v1, title:Ljava/lang/String;
    sget-object v0, Lmiui/resourcebrowser/view/ResourceOperationView$State;->NONE:Lmiui/resourcebrowser/view/ResourceOperationView$State;

    .line 297
    .local v0, state:Lmiui/resourcebrowser/view/ResourceOperationView$State;
    iget-object v2, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mResourceHandler:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    invoke-virtual {v2}, Lmiui/resourcebrowser/view/ResourceOperationHandler;->isLocalResource()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mResourceHandler:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    invoke-virtual {v2}, Lmiui/resourcebrowser/view/ResourceOperationHandler;->isDownloading()Z

    move-result v2

    if-nez v2, :cond_0

    .line 298
    iget-object v2, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mResourceHandler:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    invoke-virtual {v2}, Lmiui/resourcebrowser/view/ResourceOperationHandler;->isPicker()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 299
    iget-object v2, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mContext:Landroid/content/Context;

    const v3, 0x60c001b

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 300
    sget-object v0, Lmiui/resourcebrowser/view/ResourceOperationView$State;->PICK:Lmiui/resourcebrowser/view/ResourceOperationView$State;

    .line 307
    :cond_0
    :goto_0
    if-nez v1, :cond_2

    .line 308
    iget-object v2, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mApplyBtn:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 314
    :goto_1
    return-void

    .line 302
    :cond_1
    iget-object v2, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mContext:Landroid/content/Context;

    const v3, 0x60c0019

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 303
    sget-object v0, Lmiui/resourcebrowser/view/ResourceOperationView$State;->APPLY:Lmiui/resourcebrowser/view/ResourceOperationView$State;

    goto :goto_0

    .line 310
    :cond_2
    iget-object v2, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mApplyBtn:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 311
    iget-object v2, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mApplyBtn:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 312
    iget-object v2, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mApplyBtn:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method private setDeleteStatus()V
    .locals 2

    .prologue
    .line 381
    iget-object v1, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mDeleteBtn:Landroid/widget/ImageView;

    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mResourceHandler:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    invoke-virtual {v0}, Lmiui/resourcebrowser/view/ResourceOperationHandler;->isDeletable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mResourceHandler:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    invoke-virtual {v0}, Lmiui/resourcebrowser/view/ResourceOperationHandler;->isDownloading()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 383
    return-void

    .line 381
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method private setDownloadStatus()V
    .locals 10

    .prologue
    const v5, 0x60c0017

    const/4 v9, 0x0

    .line 317
    const/4 v0, 0x1

    .line 318
    .local v0, enabled:Z
    const/4 v3, 0x0

    .line 319
    .local v3, title:Ljava/lang/String;
    sget-object v2, Lmiui/resourcebrowser/view/ResourceOperationView$State;->NONE:Lmiui/resourcebrowser/view/ResourceOperationView$State;

    .line 320
    .local v2, state:Lmiui/resourcebrowser/view/ResourceOperationView$State;
    iget-object v4, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mResourceHandler:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    invoke-virtual {v4}, Lmiui/resourcebrowser/view/ResourceOperationHandler;->isDownloading()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 321
    iget-object v4, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mContext:Landroid/content/Context;

    const v5, 0x60c0018

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 322
    const/4 v0, 0x0

    .line 323
    sget-object v2, Lmiui/resourcebrowser/view/ResourceOperationView$State;->DOWNLOADING:Lmiui/resourcebrowser/view/ResourceOperationView$State;

    .line 345
    :cond_0
    :goto_0
    if-nez v3, :cond_6

    .line 346
    iget-object v4, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mDownloadBtn:Landroid/widget/TextView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 353
    :goto_1
    return-void

    .line 324
    :cond_1
    iget-object v4, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mResourceHandler:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    invoke-virtual {v4}, Lmiui/resourcebrowser/view/ResourceOperationHandler;->isOldResource()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 325
    iget-object v4, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mContext:Landroid/content/Context;

    const v5, 0x60c001a

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 326
    sget-object v2, Lmiui/resourcebrowser/view/ResourceOperationView$State;->UPDATE:Lmiui/resourcebrowser/view/ResourceOperationView$State;

    goto :goto_0

    .line 327
    :cond_2
    iget-object v4, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mResourceHandler:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    invoke-virtual {v4}, Lmiui/resourcebrowser/view/ResourceOperationHandler;->isLocalResource()Z

    move-result v4

    if-nez v4, :cond_0

    .line 328
    iget-object v4, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mResourceHandler:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    invoke-virtual {v4}, Lmiui/resourcebrowser/view/ResourceOperationHandler;->isAuthorizedResource()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 329
    iget-object v4, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 330
    sget-object v2, Lmiui/resourcebrowser/view/ResourceOperationView$State;->DOWNLOAD:Lmiui/resourcebrowser/view/ResourceOperationView$State;

    goto :goto_0

    .line 332
    :cond_3
    iget-object v4, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mResourceHandler:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    invoke-virtual {v4}, Lmiui/resourcebrowser/view/ResourceOperationHandler;->getPrice()I

    move-result v1

    .line 333
    .local v1, price:I
    if-nez v1, :cond_5

    .line 334
    iget-object v4, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 338
    :goto_2
    if-lez v1, :cond_4

    .line 339
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mContext:Landroid/content/Context;

    const v6, 0x60c0239

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    invoke-direct {p0, v1}, Lmiui/resourcebrowser/view/ResourceOperationView;->formatPrice(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 341
    :cond_4
    sget-object v2, Lmiui/resourcebrowser/view/ResourceOperationView$State;->BUY:Lmiui/resourcebrowser/view/ResourceOperationView$State;

    goto :goto_0

    .line 336
    :cond_5
    iget-object v4, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mContext:Landroid/content/Context;

    const v5, 0x60c0230

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    .line 348
    .end local v1           #price:I
    :cond_6
    iget-object v4, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mDownloadBtn:Landroid/widget/TextView;

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 349
    iget-object v4, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mDownloadBtn:Landroid/widget/TextView;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 350
    iget-object v4, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mDownloadBtn:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 351
    iget-object v4, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mDownloadBtn:Landroid/widget/TextView;

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_1
.end method

.method private setLoadingStatus()Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 275
    iget-object v2, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mResourceHandler:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    invoke-virtual {v2}, Lmiui/resourcebrowser/view/ResourceOperationHandler;->getLoadingStateInfo()Lmiui/resourcebrowser/view/ResourceOperationHandler$LoadingStateInfo;

    move-result-object v0

    .line 277
    .local v0, loadState:Lmiui/resourcebrowser/view/ResourceOperationHandler$LoadingStateInfo;
    const/4 v1, 0x0

    .line 278
    .local v1, ret:Z
    iget v2, v0, Lmiui/resourcebrowser/view/ResourceOperationHandler$LoadingStateInfo;->delayTime:I

    if-nez v2, :cond_1

    .line 279
    const/4 v2, 0x1

    iget-object v3, v0, Lmiui/resourcebrowser/view/ResourceOperationHandler$LoadingStateInfo;->title:Ljava/lang/String;

    invoke-direct {p0, v2, v3}, Lmiui/resourcebrowser/view/ResourceOperationView;->updateLoadingUI(ZLjava/lang/String;)V

    .line 280
    const/4 v1, 0x1

    .line 288
    :goto_0
    iget v2, v0, Lmiui/resourcebrowser/view/ResourceOperationHandler$LoadingStateInfo;->delayTime:I

    if-gtz v2, :cond_0

    .line 289
    iget-object v2, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 291
    :cond_0
    return v1

    .line 282
    :cond_1
    const/4 v2, 0x0

    invoke-direct {p0, v5, v2}, Lmiui/resourcebrowser/view/ResourceOperationView;->updateLoadingUI(ZLjava/lang/String;)V

    .line 283
    iget v2, v0, Lmiui/resourcebrowser/view/ResourceOperationHandler$LoadingStateInfo;->delayTime:I

    if-lez v2, :cond_2

    iget-object v2, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v5}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v2

    if-nez v2, :cond_2

    .line 284
    iget-object v2, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mHandler:Landroid/os/Handler;

    iget v3, v0, Lmiui/resourcebrowser/view/ResourceOperationHandler$LoadingStateInfo;->delayTime:I

    int-to-long v3, v3

    invoke-virtual {v2, v5, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 286
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private setMagicStatus()V
    .locals 2

    .prologue
    .line 386
    iget-object v1, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mMagicBtn:Landroid/widget/ImageView;

    iget v0, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mMagicBtnResId:I

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 387
    return-void

    .line 386
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method private setupUI()V
    .locals 2

    .prologue
    .line 173
    const v0, 0x60b009f

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/view/ResourceOperationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mLoadingProgress:Landroid/view/View;

    .line 174
    const v0, 0x60b009d

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/view/ResourceOperationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mControlBtns:Landroid/view/View;

    .line 176
    const v0, 0x60b009e

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/view/ResourceOperationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mLoadingTextView:Landroid/widget/TextView;

    .line 178
    const v0, 0x60b005a

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/view/ResourceOperationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mDownloadBtn:Landroid/widget/TextView;

    .line 179
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mDownloadBtn:Landroid/widget/TextView;

    new-instance v1, Lmiui/resourcebrowser/view/ResourceOperationView$2;

    invoke-direct {v1, p0}, Lmiui/resourcebrowser/view/ResourceOperationView$2;-><init>(Lmiui/resourcebrowser/view/ResourceOperationView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 193
    const v0, 0x60b005b

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/view/ResourceOperationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mApplyBtn:Landroid/widget/TextView;

    .line 194
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mApplyBtn:Landroid/widget/TextView;

    new-instance v1, Lmiui/resourcebrowser/view/ResourceOperationView$3;

    invoke-direct {v1, p0}, Lmiui/resourcebrowser/view/ResourceOperationView$3;-><init>(Lmiui/resourcebrowser/view/ResourceOperationView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 207
    const v0, 0x60b005c

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/view/ResourceOperationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mDeleteBtn:Landroid/widget/ImageView;

    .line 208
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mDeleteBtn:Landroid/widget/ImageView;

    new-instance v1, Lmiui/resourcebrowser/view/ResourceOperationView$4;

    invoke-direct {v1, p0}, Lmiui/resourcebrowser/view/ResourceOperationView$4;-><init>(Lmiui/resourcebrowser/view/ResourceOperationView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 224
    const v0, 0x60b005d

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/view/ResourceOperationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mMagicBtn:Landroid/widget/ImageView;

    .line 225
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mMagicBtn:Landroid/widget/ImageView;

    new-instance v1, Lmiui/resourcebrowser/view/ResourceOperationView$5;

    invoke-direct {v1, p0}, Lmiui/resourcebrowser/view/ResourceOperationView$5;-><init>(Lmiui/resourcebrowser/view/ResourceOperationView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 233
    const v0, 0x60b004d

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/view/ResourceOperationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mDownloadProgress:Landroid/widget/ProgressBar;

    .line 234
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mDownloadProgress:Landroid/widget/ProgressBar;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 235
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mDownloadProgress:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 236
    return-void
.end method

.method private updateLoadingUI(ZLjava/lang/String;)V
    .locals 4
    .parameter "showLoading"
    .parameter "text"

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 269
    iget-object v3, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mLoadingProgress:Landroid/view/View;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 270
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mControlBtns:Landroid/view/View;

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 271
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mLoadingTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 272
    return-void

    :cond_0
    move v0, v2

    .line 269
    goto :goto_0

    :cond_1
    move v2, v1

    .line 270
    goto :goto_1
.end method


# virtual methods
.method protected getMsgHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 255
    new-instance v0, Lmiui/resourcebrowser/view/ResourceOperationView$6;

    invoke-direct {v0, p0}, Lmiui/resourcebrowser/view/ResourceOperationView$6;-><init>(Lmiui/resourcebrowser/view/ResourceOperationView;)V

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 70
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 72
    invoke-virtual {p0}, Lmiui/resourcebrowser/view/ResourceOperationView;->getMsgHandler()Landroid/os/Handler;

    move-result-object v0

    iput-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mHandler:Landroid/os/Handler;

    .line 73
    invoke-direct {p0}, Lmiui/resourcebrowser/view/ResourceOperationView;->setupUI()V

    .line 74
    return-void
.end method

.method public setMagicButtonResource(I)V
    .locals 1
    .parameter "resourceId"

    .prologue
    .line 158
    iput p1, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mMagicBtnResId:I

    .line 159
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mMagicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 160
    return-void
.end method

.method public setResourceOperationHandler(Lmiui/resourcebrowser/view/ResourceOperationHandler;)V
    .locals 0
    .parameter "handler"

    .prologue
    .line 150
    iput-object p1, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mResourceHandler:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    .line 151
    return-void
.end method

.method public setResourceOperationListener(Lmiui/resourcebrowser/view/ResourceOperationView$ResourceOperationListener;)V
    .locals 0
    .parameter "listener"

    .prologue
    .line 154
    iput-object p1, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mListener:Lmiui/resourcebrowser/view/ResourceOperationView$ResourceOperationListener;

    .line 155
    return-void
.end method

.method public updateDownloadProgressBar(II)V
    .locals 5
    .parameter "downloadBytes"
    .parameter "totalBytes"

    .prologue
    .line 239
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationView;->mDownloadProgress:Landroid/widget/ProgressBar;

    int-to-double v1, p1

    const-wide/high16 v3, 0x4059

    mul-double/2addr v1, v3

    int-to-double v3, p2

    div-double/2addr v1, v3

    double-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 240
    return-void
.end method

.method public updateStatus()V
    .locals 1

    .prologue
    .line 243
    invoke-direct {p0}, Lmiui/resourcebrowser/view/ResourceOperationView;->setLoadingStatus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 251
    :goto_0
    return-void

    .line 246
    :cond_0
    invoke-direct {p0}, Lmiui/resourcebrowser/view/ResourceOperationView;->setApplyStatus()V

    .line 247
    invoke-direct {p0}, Lmiui/resourcebrowser/view/ResourceOperationView;->setDownloadStatus()V

    .line 249
    invoke-direct {p0}, Lmiui/resourcebrowser/view/ResourceOperationView;->setDeleteStatus()V

    .line 250
    invoke-direct {p0}, Lmiui/resourcebrowser/view/ResourceOperationView;->setMagicStatus()V

    goto :goto_0
.end method
