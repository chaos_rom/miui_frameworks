.class Lmiui/resourcebrowser/view/RecommendGridBaseItem$1;
.super Landroid/os/AsyncTask;
.source "RecommendGridBaseItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiui/resourcebrowser/view/RecommendGridBaseItem;->downloadThumbnail(Lmiui/resourcebrowser/model/PathEntry;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lmiui/resourcebrowser/model/PathEntry;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/resourcebrowser/view/RecommendGridBaseItem;


# direct methods
.method constructor <init>(Lmiui/resourcebrowser/view/RecommendGridBaseItem;)V
    .locals 0
    .parameter

    .prologue
    .line 66
    iput-object p1, p0, Lmiui/resourcebrowser/view/RecommendGridBaseItem$1;->this$0:Lmiui/resourcebrowser/view/RecommendGridBaseItem;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Lmiui/resourcebrowser/model/PathEntry;)Landroid/graphics/Bitmap;
    .locals 4
    .parameter "params"

    .prologue
    const/4 v3, 0x0

    .line 70
    aget-object v0, p1, v3

    .line 71
    .local v0, entry:Lmiui/resourcebrowser/model/PathEntry;
    new-instance v1, Ljava/io/File;

    invoke-virtual {v0}, Lmiui/resourcebrowser/model/PathEntry;->getLocalPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 72
    new-instance v1, Lmiui/resourcebrowser/controller/online/DownloadFileTask;

    invoke-virtual {v0}, Lmiui/resourcebrowser/model/PathEntry;->getOnlinePath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lmiui/resourcebrowser/controller/online/DownloadFileTask;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x1

    new-array v2, v2, [Lmiui/resourcebrowser/model/PathEntry;

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lmiui/resourcebrowser/controller/online/DownloadFileTask;->downloadFiles([Lmiui/resourcebrowser/model/PathEntry;)Z

    .line 74
    :cond_0
    new-instance v1, Lmiui/util/InputStreamLoader;

    invoke-virtual {v0}, Lmiui/resourcebrowser/model/PathEntry;->getLocalPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lmiui/util/InputStreamLoader;-><init>(Ljava/lang/String;)V

    const/4 v2, -0x1

    invoke-static {v1, v2}, Lmiui/util/ImageUtils;->getBitmap(Lmiui/util/InputStreamLoader;I)Landroid/graphics/Bitmap;

    move-result-object v1

    return-object v1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"

    .prologue
    .line 66
    check-cast p1, [Lmiui/resourcebrowser/model/PathEntry;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/view/RecommendGridBaseItem$1;->doInBackground([Lmiui/resourcebrowser/model/PathEntry;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/graphics/Bitmap;)V
    .locals 1
    .parameter "bm"

    .prologue
    .line 79
    if-eqz p1, :cond_0

    .line 80
    iget-object v0, p0, Lmiui/resourcebrowser/view/RecommendGridBaseItem$1;->this$0:Lmiui/resourcebrowser/view/RecommendGridBaseItem;

    #getter for: Lmiui/resourcebrowser/view/RecommendGridBaseItem;->mImageView:Landroid/widget/ImageView;
    invoke-static {v0}, Lmiui/resourcebrowser/view/RecommendGridBaseItem;->access$000(Lmiui/resourcebrowser/view/RecommendGridBaseItem;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 82
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 66
    check-cast p1, Landroid/graphics/Bitmap;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/view/RecommendGridBaseItem$1;->onPostExecute(Landroid/graphics/Bitmap;)V

    return-void
.end method
