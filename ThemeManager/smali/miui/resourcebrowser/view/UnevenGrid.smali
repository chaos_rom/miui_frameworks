.class public Lmiui/resourcebrowser/view/UnevenGrid;
.super Landroid/view/ViewGroup;
.source "UnevenGrid.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/resourcebrowser/view/UnevenGrid$GridItemFactory;,
        Lmiui/resourcebrowser/view/UnevenGrid$GridItemData;,
        Lmiui/resourcebrowser/view/UnevenGrid$LayoutParams;
    }
.end annotation


# instance fields
.field private mColumnCount:I

.field private mEmptyGrids:Ljava/util/TreeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mEmptyRow:I

.field private mGridHeight:I

.field private mGridItemFactory:Lmiui/resourcebrowser/view/UnevenGrid$GridItemFactory;

.field private mGridItemGap:I

.field private mGridItemPositions:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mGridItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mGridMaxHeight:I

.field private mGridMaxWidth:I

.field private mGridWidth:I

.field private mHeightRatio:I

.field private mWidthRatio:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter "context"

    .prologue
    .line 35
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mGridItems:Ljava/util/ArrayList;

    .line 38
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mGridItemPositions:Ljava/util/HashMap;

    .line 39
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mEmptyGrids:Ljava/util/TreeSet;

    .line 40
    const/4 v0, 0x0

    iput v0, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mEmptyRow:I

    .line 42
    invoke-direct {p0}, Lmiui/resourcebrowser/view/UnevenGrid;->initialize()V

    .line 43
    return-void
.end method

.method private addGridItem(Landroid/view/View;II)V
    .locals 2
    .parameter "view"
    .parameter "widthCount"
    .parameter "heightCount"

    .prologue
    const/4 v1, 0x1

    .line 107
    if-nez p1, :cond_0

    .line 122
    :goto_0
    return-void

    .line 110
    :cond_0
    iget v0, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mGridMaxWidth:I

    if-le p2, v0, :cond_3

    .line 111
    iget p2, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mGridMaxWidth:I

    .line 115
    :cond_1
    :goto_1
    iget v0, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mGridMaxHeight:I

    if-le p3, v0, :cond_4

    .line 116
    iget p3, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mGridMaxHeight:I

    .line 120
    :cond_2
    :goto_2
    invoke-static {p2, p3}, Lmiui/resourcebrowser/view/UnevenGrid$LayoutParams;->create(II)Lmiui/resourcebrowser/view/UnevenGrid$LayoutParams;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 121
    iget-object v0, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mGridItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 112
    :cond_3
    if-ge p2, v1, :cond_1

    .line 113
    const/4 p2, 0x1

    goto :goto_1

    .line 117
    :cond_4
    if-ge p3, v1, :cond_2

    .line 118
    const/4 p3, 0x1

    goto :goto_2
.end method

.method private computeAndPlace(II)I
    .locals 4
    .parameter "widthCount"
    .parameter "heightCount"

    .prologue
    .line 142
    iget-object v2, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mEmptyGrids:Ljava/util/TreeSet;

    invoke-virtual {v2}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 143
    .local v0, emptyPos:Ljava/lang/Integer;
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {p0, v2, p1, p2}, Lmiui/resourcebrowser/view/UnevenGrid;->isPlaceable(III)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 145
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {p0, v2, p1, p2}, Lmiui/resourcebrowser/view/UnevenGrid;->place(III)V

    .line 146
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 152
    .end local v0           #emptyPos:Ljava/lang/Integer;
    :goto_0
    return v0

    .line 150
    :cond_1
    iget v2, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mEmptyRow:I

    iget v3, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mColumnCount:I

    mul-int v0, v2, v3

    .line 151
    .local v0, emptyPos:I
    invoke-direct {p0, v0, p1, p2}, Lmiui/resourcebrowser/view/UnevenGrid;->place(III)V

    goto :goto_0
.end method

.method private getChildBottom(II)I
    .locals 1
    .parameter "position"
    .parameter "height"

    .prologue
    .line 210
    invoke-direct {p0, p1}, Lmiui/resourcebrowser/view/UnevenGrid;->getChildTop(I)I

    move-result v0

    add-int/2addr v0, p2

    return v0
.end method

.method private getChildLeft(I)I
    .locals 3
    .parameter "position"

    .prologue
    .line 198
    iget v0, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mColumnCount:I

    rem-int v0, p1, v0

    iget v1, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mGridWidth:I

    iget v2, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mGridItemGap:I

    add-int/2addr v1, v2

    mul-int/2addr v0, v1

    return v0
.end method

.method private getChildRight(II)I
    .locals 1
    .parameter "position"
    .parameter "width"

    .prologue
    .line 206
    invoke-direct {p0, p1}, Lmiui/resourcebrowser/view/UnevenGrid;->getChildLeft(I)I

    move-result v0

    add-int/2addr v0, p2

    return v0
.end method

.method private getChildTop(I)I
    .locals 3
    .parameter "position"

    .prologue
    .line 202
    iget v0, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mColumnCount:I

    div-int v0, p1, v0

    iget v1, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mGridHeight:I

    iget v2, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mGridItemGap:I

    add-int/2addr v1, v2

    mul-int/2addr v0, v1

    return v0
.end method

.method private initialize()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 47
    iput v1, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mColumnCount:I

    .line 49
    const/4 v0, 0x0

    iput v0, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mGridItemGap:I

    .line 51
    iput v1, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mGridMaxWidth:I

    .line 52
    iput v1, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mGridMaxHeight:I

    .line 54
    const/4 v0, 0x5

    iput v0, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mWidthRatio:I

    .line 55
    const/4 v0, 0x3

    iput v0, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mHeightRatio:I

    .line 56
    return-void
.end method

.method private isPlaceable(III)Z
    .locals 7
    .parameter "position"
    .parameter "widthCount"
    .parameter "heightCount"

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 156
    iget v5, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mColumnCount:I

    iget v6, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mColumnCount:I

    rem-int v6, p1, v6

    sub-int/2addr v5, v6

    if-ge v5, p2, :cond_1

    .line 174
    :cond_0
    :goto_0
    return v3

    .line 160
    :cond_1
    const/4 v0, 0x0

    .local v0, i:I
    :goto_1
    if-ge v0, p3, :cond_4

    .line 161
    const/4 v1, 0x0

    .local v1, j:I
    :goto_2
    if-ge v1, p2, :cond_3

    .line 162
    iget v5, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mColumnCount:I

    mul-int/2addr v5, v0

    add-int/2addr v5, p1

    add-int v2, v5, v1

    .line 163
    .local v2, needPos:I
    iget v5, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mColumnCount:I

    div-int v5, v2, v5

    iget v6, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mEmptyRow:I

    if-lt v5, v6, :cond_2

    move v3, v4

    .line 165
    goto :goto_0

    .line 167
    :cond_2
    iget-object v5, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mEmptyGrids:Ljava/util/TreeSet;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/TreeSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 161
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 160
    .end local v2           #needPos:I
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .end local v1           #j:I
    :cond_4
    move v3, v4

    .line 174
    goto :goto_0
.end method

.method private layoutItems()V
    .locals 8

    .prologue
    .line 125
    invoke-virtual {p0}, Lmiui/resourcebrowser/view/UnevenGrid;->removeAllViews()V

    .line 126
    iget-object v6, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mGridItemPositions:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->clear()V

    .line 127
    iget-object v6, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mEmptyGrids:Ljava/util/TreeSet;

    invoke-virtual {v6}, Ljava/util/TreeSet;->clear()V

    .line 128
    const/4 v6, 0x0

    iput v6, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mEmptyRow:I

    .line 129
    iget-object v6, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mGridItems:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    .line 130
    .local v4, view:Landroid/view/View;
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Lmiui/resourcebrowser/view/UnevenGrid$LayoutParams;

    .line 132
    .local v2, params:Lmiui/resourcebrowser/view/UnevenGrid$LayoutParams;
    iget v5, v2, Lmiui/resourcebrowser/view/UnevenGrid$LayoutParams;->widthCount:I

    .line 133
    .local v5, widthCount:I
    iget v0, v2, Lmiui/resourcebrowser/view/UnevenGrid$LayoutParams;->heightCount:I

    .line 135
    .local v0, heightCount:I
    invoke-direct {p0, v5, v0}, Lmiui/resourcebrowser/view/UnevenGrid;->computeAndPlace(II)I

    move-result v3

    .line 136
    .local v3, pos:I
    iget-object v6, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mGridItemPositions:Ljava/util/HashMap;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v4, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    invoke-virtual {p0, v4}, Lmiui/resourcebrowser/view/UnevenGrid;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 139
    .end local v0           #heightCount:I
    .end local v2           #params:Lmiui/resourcebrowser/view/UnevenGrid$LayoutParams;
    .end local v3           #pos:I
    .end local v4           #view:Landroid/view/View;
    .end local v5           #widthCount:I
    :cond_0
    return-void
.end method

.method private place(III)V
    .locals 7
    .parameter "position"
    .parameter "widthCount"
    .parameter "heightCount"

    .prologue
    .line 178
    iget v5, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mColumnCount:I

    div-int v5, p1, v5

    add-int/2addr v5, p3

    add-int/lit8 v2, v5, -0x1

    .line 179
    .local v2, maxRow:I
    iget v0, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mEmptyRow:I

    .local v0, i:I
    :goto_0
    if-gt v0, v2, :cond_1

    .line 180
    const/4 v1, 0x0

    .local v1, j:I
    :goto_1
    iget v5, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mColumnCount:I

    if-ge v1, v5, :cond_0

    .line 181
    iget v5, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mColumnCount:I

    mul-int/2addr v5, v0

    add-int v4, v5, v1

    .line 182
    .local v4, pos:I
    iget-object v5, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mEmptyGrids:Ljava/util/TreeSet;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 180
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 179
    .end local v4           #pos:I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 185
    .end local v1           #j:I
    :cond_1
    iget v5, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mEmptyRow:I

    if-gt v5, v2, :cond_2

    .line 186
    add-int/lit8 v5, v2, 0x1

    iput v5, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mEmptyRow:I

    .line 189
    :cond_2
    const/4 v0, 0x0

    :goto_2
    if-ge v0, p3, :cond_4

    .line 190
    const/4 v1, 0x0

    .restart local v1       #j:I
    :goto_3
    if-ge v1, p2, :cond_3

    .line 191
    iget v5, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mColumnCount:I

    mul-int/2addr v5, v0

    add-int/2addr v5, p1

    add-int v3, v5, v1

    .line 192
    .local v3, needPos:I
    iget-object v5, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mEmptyGrids:Ljava/util/TreeSet;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    .line 190
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 189
    .end local v3           #needPos:I
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 195
    .end local v1           #j:I
    :cond_4
    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 10
    .parameter "changed"
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    .prologue
    .line 240
    const/4 v4, 0x0

    .local v4, i:I
    :goto_0
    invoke-virtual {p0}, Lmiui/resourcebrowser/view/UnevenGrid;->getChildCount()I

    move-result v5

    if-ge v4, v5, :cond_0

    .line 241
    invoke-virtual {p0, v4}, Lmiui/resourcebrowser/view/UnevenGrid;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 242
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    .line 243
    .local v3, childWidth:I
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    .line 244
    .local v1, childHeight:I
    iget-object v5, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mGridItemPositions:Ljava/util/HashMap;

    invoke-virtual {v5, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 246
    .local v2, childPos:I
    iget v5, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mPaddingLeft:I

    invoke-direct {p0, v2}, Lmiui/resourcebrowser/view/UnevenGrid;->getChildLeft(I)I

    move-result v6

    add-int/2addr v5, v6

    iget v6, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mPaddingTop:I

    invoke-direct {p0, v2}, Lmiui/resourcebrowser/view/UnevenGrid;->getChildTop(I)I

    move-result v7

    add-int/2addr v6, v7

    iget v7, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mPaddingLeft:I

    invoke-direct {p0, v2, v3}, Lmiui/resourcebrowser/view/UnevenGrid;->getChildRight(II)I

    move-result v8

    add-int/2addr v7, v8

    iget v8, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mPaddingTop:I

    invoke-direct {p0, v2, v1}, Lmiui/resourcebrowser/view/UnevenGrid;->getChildBottom(II)I

    move-result v9

    add-int/2addr v8, v9

    invoke-virtual {v0, v5, v6, v7, v8}, Landroid/view/View;->layout(IIII)V

    .line 240
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 251
    .end local v0           #child:Landroid/view/View;
    .end local v1           #childHeight:I
    .end local v2           #childPos:I
    .end local v3           #childWidth:I
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 13
    .parameter "widthSpec"
    .parameter "heightSpec"

    .prologue
    .line 215
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v8

    .line 216
    .local v8, width:I
    iget v10, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mPaddingLeft:I

    sub-int v10, v8, v10

    iget v11, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mPaddingRight:I

    sub-int/2addr v10, v11

    iget v11, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mColumnCount:I

    add-int/lit8 v11, v11, -0x1

    iget v12, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mGridItemGap:I

    mul-int/2addr v11, v12

    sub-int/2addr v10, v11

    iget v11, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mColumnCount:I

    div-int/2addr v10, v11

    iput v10, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mGridWidth:I

    .line 217
    iget v10, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mGridWidth:I

    iget v11, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mHeightRatio:I

    mul-int/2addr v10, v11

    iget v11, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mWidthRatio:I

    div-int/2addr v10, v11

    iput v10, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mGridHeight:I

    .line 219
    const/4 v0, 0x0

    .line 220
    .local v0, bottom:I
    const/4 v6, 0x0

    .local v6, i:I
    :goto_0
    invoke-virtual {p0}, Lmiui/resourcebrowser/view/UnevenGrid;->getChildCount()I

    move-result v10

    if-ge v6, v10, :cond_0

    .line 221
    invoke-virtual {p0, v6}, Lmiui/resourcebrowser/view/UnevenGrid;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 222
    .local v1, child:Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Lmiui/resourcebrowser/view/UnevenGrid$LayoutParams;

    .line 223
    .local v7, param:Lmiui/resourcebrowser/view/UnevenGrid$LayoutParams;
    iget v9, v7, Lmiui/resourcebrowser/view/UnevenGrid$LayoutParams;->widthCount:I

    .line 224
    .local v9, widthCount:I
    iget v5, v7, Lmiui/resourcebrowser/view/UnevenGrid$LayoutParams;->heightCount:I

    .line 226
    .local v5, heightCount:I
    iget v10, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mGridWidth:I

    mul-int/2addr v10, v9

    add-int/lit8 v11, v9, -0x1

    iget v12, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mGridItemGap:I

    mul-int/2addr v11, v12

    add-int v4, v10, v11

    .line 227
    .local v4, childWidth:I
    iget v10, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mGridHeight:I

    mul-int/2addr v10, v5

    add-int/lit8 v11, v5, -0x1

    iget v12, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mGridItemGap:I

    mul-int/2addr v11, v12

    add-int v2, v10, v11

    .line 228
    .local v2, childHeight:I
    iget-object v10, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mGridItemPositions:Ljava/util/HashMap;

    invoke-virtual {v10, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 230
    .local v3, childPos:I
    const/high16 v10, 0x4000

    invoke-static {v4, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    const/high16 v11, 0x4000

    invoke-static {v2, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v11

    invoke-virtual {v1, v10, v11}, Landroid/view/View;->measure(II)V

    .line 233
    invoke-direct {p0, v3, v2}, Lmiui/resourcebrowser/view/UnevenGrid;->getChildBottom(II)I

    move-result v10

    invoke-static {v0, v10}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 220
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 235
    .end local v1           #child:Landroid/view/View;
    .end local v2           #childHeight:I
    .end local v3           #childPos:I
    .end local v4           #childWidth:I
    .end local v5           #heightCount:I
    .end local v7           #param:Lmiui/resourcebrowser/view/UnevenGrid$LayoutParams;
    .end local v9           #widthCount:I
    :cond_0
    iget v10, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mPaddingTop:I

    add-int/2addr v10, v0

    iget v11, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mPaddingBottom:I

    add-int/2addr v10, v11

    invoke-virtual {p0, v8, v10}, Lmiui/resourcebrowser/view/UnevenGrid;->setMeasuredDimension(II)V

    .line 236
    return-void
.end method

.method public setColumnCount(I)V
    .locals 0
    .parameter "columnCount"

    .prologue
    .line 76
    if-lez p1, :cond_0

    .line 77
    iput p1, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mColumnCount:I

    .line 79
    :cond_0
    return-void
.end method

.method public setGridItemFactory(Lmiui/resourcebrowser/view/UnevenGrid$GridItemFactory;)V
    .locals 0
    .parameter "gridItemFactory"

    .prologue
    .line 59
    iput-object p1, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mGridItemFactory:Lmiui/resourcebrowser/view/UnevenGrid$GridItemFactory;

    .line 60
    return-void
.end method

.method public setGridItemGap(I)V
    .locals 0
    .parameter "gridItemGap"

    .prologue
    .line 63
    if-ltz p1, :cond_0

    .line 64
    iput p1, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mGridItemGap:I

    .line 66
    :cond_0
    return-void
.end method

.method public setGridItemRatio(II)V
    .locals 0
    .parameter "width"
    .parameter "height"

    .prologue
    .line 69
    if-lez p1, :cond_0

    if-lez p2, :cond_0

    .line 70
    iput p1, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mWidthRatio:I

    .line 71
    iput p2, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mHeightRatio:I

    .line 73
    :cond_0
    return-void
.end method

.method public updateData(Ljava/util/List;)V
    .locals 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lmiui/resourcebrowser/view/UnevenGrid$GridItemData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 93
    .local p1, datas:Ljava/util/List;,"Ljava/util/List<+Lmiui/resourcebrowser/view/UnevenGrid$GridItemData;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mGridItemFactory:Lmiui/resourcebrowser/view/UnevenGrid$GridItemFactory;

    if-nez v3, :cond_1

    .line 104
    :cond_0
    :goto_0
    return-void

    .line 96
    :cond_1
    iget-object v3, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mGridItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 97
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/resourcebrowser/view/UnevenGrid$GridItemData;

    .line 98
    .local v0, data:Lmiui/resourcebrowser/view/UnevenGrid$GridItemData;
    iget-object v3, p0, Lmiui/resourcebrowser/view/UnevenGrid;->mGridItemFactory:Lmiui/resourcebrowser/view/UnevenGrid$GridItemFactory;

    invoke-interface {v3, v0}, Lmiui/resourcebrowser/view/UnevenGrid$GridItemFactory;->createGridItem(Lmiui/resourcebrowser/view/UnevenGrid$GridItemData;)Landroid/view/View;

    move-result-object v2

    .line 99
    .local v2, view:Landroid/view/View;
    if-eqz v2, :cond_2

    .line 100
    iget v3, v0, Lmiui/resourcebrowser/view/UnevenGrid$GridItemData;->widthCount:I

    iget v4, v0, Lmiui/resourcebrowser/view/UnevenGrid$GridItemData;->heightCount:I

    invoke-direct {p0, v2, v3, v4}, Lmiui/resourcebrowser/view/UnevenGrid;->addGridItem(Landroid/view/View;II)V

    goto :goto_1

    .line 103
    .end local v0           #data:Lmiui/resourcebrowser/view/UnevenGrid$GridItemData;
    .end local v2           #view:Landroid/view/View;
    :cond_3
    invoke-direct {p0}, Lmiui/resourcebrowser/view/UnevenGrid;->layoutItems()V

    goto :goto_0
.end method
