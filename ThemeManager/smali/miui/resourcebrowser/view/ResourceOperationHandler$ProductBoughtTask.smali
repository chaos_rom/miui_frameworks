.class public Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask;
.super Landroid/os/AsyncTask;
.source "ResourceOperationHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/resourcebrowser/view/ResourceOperationHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "ProductBoughtTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/resourcebrowser/view/ResourceOperationHandler;


# direct methods
.method protected constructor <init>(Lmiui/resourcebrowser/view/ResourceOperationHandler;)V
    .locals 0
    .parameter

    .prologue
    .line 243
    iput-object p1, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask;->this$0:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"

    .prologue
    .line 243
    check-cast p1, [Ljava/lang/Void;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask;->doInBackground([Ljava/lang/Void;)Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;
    .locals 1
    .parameter "params"

    .prologue
    .line 252
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask;->this$0:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    #calls: Lmiui/resourcebrowser/view/ResourceOperationHandler;->checkProductState()Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;
    invoke-static {v0}, Lmiui/resourcebrowser/view/ResourceOperationHandler;->access$100(Lmiui/resourcebrowser/view/ResourceOperationHandler;)Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 243
    check-cast p1, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask;->onPostExecute(Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;)V

    return-void
.end method

.method protected onPostExecute(Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;)V
    .locals 6
    .parameter "result"

    .prologue
    .line 257
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask;->this$0:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    iget-object v0, v0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 303
    :goto_0
    return-void

    .line 261
    :cond_0
    new-instance v5, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask$1;

    invoke-direct {v5, p0}, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask$1;-><init>(Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask;)V

    .line 292
    .local v5, paymentListener:Lmiui/net/PaymentManager$PaymentListener;
    sget-object v0, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;->HAS_BOUGHT:Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;

    if-ne p1, v0, :cond_1

    .line 293
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask;->this$0:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    iget-object v0, v0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v0}, Lmiui/resourcebrowser/model/Resource;->getProductId()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v5, v0, v1}, Lmiui/net/PaymentManager$PaymentListener;->onSuccess(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 302
    :goto_1
    const-string v0, "Theme"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ProductBoughtTask return: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 294
    :cond_1
    sget-object v0, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;->UNKOWN_PRODUCT:Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;

    if-ne p1, v0, :cond_2

    .line 295
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask;->this$0:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    iget-object v0, v0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v0}, Lmiui/resourcebrowser/model/Resource;->getProductId()Ljava/lang/String;

    move-result-object v0

    const/4 v1, -0x1

    const-string v2, "unkown product"

    invoke-interface {v5, v0, v1, v2}, Lmiui/net/PaymentManager$PaymentListener;->onFailed(Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_1

    .line 297
    :cond_2
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 298
    .local v4, bundle:Landroid/os/Bundle;
    const-string v1, "payment_quick_payment"

    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask;->this$0:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    iget-object v0, v0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v0}, Lmiui/resourcebrowser/model/Resource;->getProductPrice()I

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {v4, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 299
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask;->this$0:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    iget-object v0, v0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lmiui/net/PaymentManager;->get(Landroid/content/Context;)Lmiui/net/PaymentManager;

    move-result-object v0

    iget-object v1, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask;->this$0:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    iget-object v1, v1, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    const-string v2, "thm"

    iget-object v3, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask;->this$0:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    iget-object v3, v3, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v3}, Lmiui/resourcebrowser/model/Resource;->getProductId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {v0 .. v5}, Lmiui/net/PaymentManager;->pay(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lmiui/net/PaymentManager$PaymentListener;)V

    goto :goto_1

    .line 298
    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    .line 247
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask;->this$0:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lmiui/resourcebrowser/view/ResourceOperationHandler;->updateLoadingState(ILjava/lang/String;)V

    .line 248
    return-void
.end method
