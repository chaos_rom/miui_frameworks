.class public Lmiui/resourcebrowser/view/RecommendGridItemFactory;
.super Ljava/lang/Object;
.source "RecommendGridItemFactory.java"

# interfaces
.implements Lmiui/resourcebrowser/view/UnevenGrid$GridItemFactory;


# static fields
.field private static sGridTypeLayoutMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mContext:Landroid/content/Context;

.field private mResContext:Lmiui/resourcebrowser/ResourceContext;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 19
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lmiui/resourcebrowser/view/RecommendGridItemFactory;->sGridTypeLayoutMap:Ljava/util/Map;

    .line 25
    sget-object v0, Lmiui/resourcebrowser/view/RecommendGridItemFactory;->sGridTypeLayoutMap:Ljava/util/Map;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x6030002

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    sget-object v0, Lmiui/resourcebrowser/view/RecommendGridItemFactory;->sGridTypeLayoutMap:Ljava/util/Map;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x6030001

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lmiui/resourcebrowser/ResourceContext;)V
    .locals 0
    .parameter "context"
    .parameter "resContext"

    .prologue
    .line 29
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lmiui/resourcebrowser/view/RecommendGridItemFactory;->mContext:Landroid/content/Context;

    .line 31
    iput-object p2, p0, Lmiui/resourcebrowser/view/RecommendGridItemFactory;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    .line 32
    return-void
.end method


# virtual methods
.method public createGridItem(Lmiui/resourcebrowser/view/UnevenGrid$GridItemData;)Landroid/view/View;
    .locals 6
    .parameter "gridItemData"

    .prologue
    const/4 v1, 0x0

    .line 36
    if-nez p1, :cond_1

    .line 47
    :cond_0
    :goto_0
    return-object v1

    :cond_1
    move-object v0, p1

    .line 39
    check-cast v0, Lmiui/resourcebrowser/model/RecommendItemData;

    .line 40
    .local v0, data:Lmiui/resourcebrowser/model/RecommendItemData;
    iget v2, v0, Lmiui/resourcebrowser/model/RecommendItemData;->itemType:I

    .line 41
    .local v2, type:I
    if-eqz v2, :cond_0

    .line 44
    iget-object v3, p0, Lmiui/resourcebrowser/view/RecommendGridItemFactory;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    sget-object v3, Lmiui/resourcebrowser/view/RecommendGridItemFactory;->sGridTypeLayoutMap:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v5, 0x0

    invoke-virtual {v4, v3, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lmiui/resourcebrowser/view/RecommendGridBaseItem;

    .line 46
    .local v1, item:Lmiui/resourcebrowser/view/RecommendGridBaseItem;
    iget-object v3, p0, Lmiui/resourcebrowser/view/RecommendGridItemFactory;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v1, v0, v3}, Lmiui/resourcebrowser/view/RecommendGridBaseItem;->bind(Lmiui/resourcebrowser/model/RecommendItemData;Lmiui/resourcebrowser/ResourceContext;)V

    goto :goto_0
.end method
