.class Lmiui/resourcebrowser/view/ResourceOperationView$2;
.super Ljava/lang/Object;
.source "ResourceOperationView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiui/resourcebrowser/view/ResourceOperationView;->setupUI()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/resourcebrowser/view/ResourceOperationView;


# direct methods
.method constructor <init>(Lmiui/resourcebrowser/view/ResourceOperationView;)V
    .locals 0
    .parameter

    .prologue
    .line 179
    iput-object p1, p0, Lmiui/resourcebrowser/view/ResourceOperationView$2;->this$0:Lmiui/resourcebrowser/view/ResourceOperationView;

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .parameter "v"

    .prologue
    .line 182
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/resourcebrowser/view/ResourceOperationView$State;

    .line 183
    .local v0, state:Lmiui/resourcebrowser/view/ResourceOperationView$State;
    sget-object v1, Lmiui/resourcebrowser/view/ResourceOperationView$State;->DOWNLOAD:Lmiui/resourcebrowser/view/ResourceOperationView$State;

    if-ne v0, v1, :cond_1

    .line 184
    iget-object v1, p0, Lmiui/resourcebrowser/view/ResourceOperationView$2;->this$0:Lmiui/resourcebrowser/view/ResourceOperationView;

    #calls: Lmiui/resourcebrowser/view/ResourceOperationView;->notifyDownloadEventPerformed()V
    invoke-static {v1}, Lmiui/resourcebrowser/view/ResourceOperationView;->access$000(Lmiui/resourcebrowser/view/ResourceOperationView;)V

    .line 190
    :cond_0
    :goto_0
    return-void

    .line 185
    :cond_1
    sget-object v1, Lmiui/resourcebrowser/view/ResourceOperationView$State;->BUY:Lmiui/resourcebrowser/view/ResourceOperationView$State;

    if-ne v0, v1, :cond_2

    .line 186
    iget-object v1, p0, Lmiui/resourcebrowser/view/ResourceOperationView$2;->this$0:Lmiui/resourcebrowser/view/ResourceOperationView;

    #calls: Lmiui/resourcebrowser/view/ResourceOperationView;->notifyBuyEventPerformed()V
    invoke-static {v1}, Lmiui/resourcebrowser/view/ResourceOperationView;->access$100(Lmiui/resourcebrowser/view/ResourceOperationView;)V

    goto :goto_0

    .line 187
    :cond_2
    sget-object v1, Lmiui/resourcebrowser/view/ResourceOperationView$State;->UPDATE:Lmiui/resourcebrowser/view/ResourceOperationView$State;

    if-ne v0, v1, :cond_0

    .line 188
    iget-object v1, p0, Lmiui/resourcebrowser/view/ResourceOperationView$2;->this$0:Lmiui/resourcebrowser/view/ResourceOperationView;

    #calls: Lmiui/resourcebrowser/view/ResourceOperationView;->notifyUpdateEventPerformed()V
    invoke-static {v1}, Lmiui/resourcebrowser/view/ResourceOperationView;->access$200(Lmiui/resourcebrowser/view/ResourceOperationView;)V

    goto :goto_0
.end method
