.class public Lmiui/resourcebrowser/view/ResourceScreenView;
.super Lmiui/widget/ScreenView;
.source "ResourceScreenView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/resourcebrowser/view/ResourceScreenView$ScreenChangeListener;
    }
.end annotation


# instance fields
.field private mScreenChangeListener:Lmiui/resourcebrowser/view/ResourceScreenView$ScreenChangeListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .parameter "context"

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lmiui/widget/ScreenView;-><init>(Landroid/content/Context;)V

    .line 14
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Lmiui/widget/ScreenView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 21
    invoke-direct {p0, p1, p2, p3}, Lmiui/widget/ScreenView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 22
    return-void
.end method


# virtual methods
.method public setScreenChangeListener(Lmiui/resourcebrowser/view/ResourceScreenView$ScreenChangeListener;)V
    .locals 0
    .parameter "l"

    .prologue
    .line 33
    iput-object p1, p0, Lmiui/resourcebrowser/view/ResourceScreenView;->mScreenChangeListener:Lmiui/resourcebrowser/view/ResourceScreenView$ScreenChangeListener;

    .line 34
    return-void
.end method

.method protected snapToScreen(IIZ)V
    .locals 1
    .parameter "whichScreen"
    .parameter "velocity"
    .parameter "settle"

    .prologue
    .line 26
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceScreenView;->mScreenChangeListener:Lmiui/resourcebrowser/view/ResourceScreenView$ScreenChangeListener;

    if-eqz v0, :cond_0

    .line 27
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceScreenView;->mScreenChangeListener:Lmiui/resourcebrowser/view/ResourceScreenView$ScreenChangeListener;

    invoke-interface {v0, p1}, Lmiui/resourcebrowser/view/ResourceScreenView$ScreenChangeListener;->snapToScreen(I)V

    .line 29
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lmiui/widget/ScreenView;->snapToScreen(IIZ)V

    .line 30
    return-void
.end method
