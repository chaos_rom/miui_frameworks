.class Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask$1;
.super Ljava/lang/Object;
.source "ResourceOperationHandler.java"

# interfaces
.implements Lmiui/net/PaymentManager$PaymentListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask;->onPostExecute(Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask;


# direct methods
.method constructor <init>(Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask;)V
    .locals 0
    .parameter

    .prologue
    .line 261
    iput-object p1, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask$1;->this$1:Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private afterBoughtSuccess()V
    .locals 3

    .prologue
    .line 264
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask$1;->this$1:Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask;

    iget-object v0, v0, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask;->this$0:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    iget-object v0, v0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/resourcebrowser/model/Resource;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/model/Resource;->setProductBought(Z)V

    .line 265
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask$1;->this$1:Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask;

    iget-object v0, v0, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask;->this$0:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    iget-object v0, v0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v0}, Lmiui/resourcebrowser/model/Resource;->getStatus()I

    move-result v0

    invoke-static {v0}, Lmiui/resourcebrowser/util/ResourceHelper;->isLocalResource(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 266
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask$1;->this$1:Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask;

    iget-object v0, v0, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask;->this$0:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    invoke-virtual {v0}, Lmiui/resourcebrowser/view/ResourceOperationHandler;->onCheckResourceRightEventBeforeRealApply()V

    .line 270
    :goto_0
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask$1;->this$1:Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask;

    iget-object v0, v0, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask;->this$0:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lmiui/resourcebrowser/view/ResourceOperationHandler;->updateLoadingState(ILjava/lang/String;)V

    .line 271
    return-void

    .line 268
    :cond_0
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask$1;->this$1:Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask;

    iget-object v0, v0, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask;->this$0:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    invoke-virtual {v0}, Lmiui/resourcebrowser/view/ResourceOperationHandler;->onDownloadEventPerformed()V

    goto :goto_0
.end method


# virtual methods
.method public onFailed(Ljava/lang/String;ILjava/lang/String;)V
    .locals 4
    .parameter "productId"
    .parameter "code"
    .parameter "message"

    .prologue
    const/4 v1, 0x1

    .line 281
    const/4 v2, 0x7

    if-ne p2, v2, :cond_1

    move v0, v1

    .line 282
    .local v0, duplicate:Z
    :goto_0
    if-eqz v0, :cond_2

    .line 283
    invoke-direct {p0}, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask$1;->afterBoughtSuccess()V

    .line 287
    :cond_0
    :goto_1
    iget-object v1, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask$1;->this$1:Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask;

    iget-object v1, v1, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask;->this$0:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lmiui/resourcebrowser/view/ResourceOperationHandler;->updateLoadingState(ILjava/lang/String;)V

    .line 288
    const-string v1, "Theme"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "PaymentListener: purchase failed: duplicatePurchase="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    return-void

    .line 281
    .end local v0           #duplicate:Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 284
    .restart local v0       #duplicate:Z
    :cond_2
    iget-object v2, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask$1;->this$1:Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask;

    iget-object v2, v2, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask;->this$0:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    iget-object v2, v2, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v2}, Lmiui/resourcebrowser/model/Resource;->getStatus()I

    move-result v2

    invoke-static {v2}, Lmiui/resourcebrowser/util/ResourceHelper;->isLocalResource(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 285
    iget-object v2, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask$1;->this$1:Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask;

    iget-object v2, v2, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask;->this$0:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    iget-object v2, v2, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mContext:Landroid/content/Context;

    const v3, 0x60c0237

    invoke-static {v2, v3, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method

.method public onSuccess(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 2
    .parameter "productId"
    .parameter "result"

    .prologue
    .line 275
    invoke-direct {p0}, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask$1;->afterBoughtSuccess()V

    .line 276
    const-string v0, "Theme"

    const-string v1, "PaymentListener: purchase success"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    return-void
.end method
