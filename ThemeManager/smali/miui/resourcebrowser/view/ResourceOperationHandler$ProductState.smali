.class public final enum Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;
.super Ljava/lang/Enum;
.source "ResourceOperationHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/resourcebrowser/view/ResourceOperationHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401c
    name = "ProductState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;

.field public static final enum HAS_BOUGHT:Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;

.field public static final enum NOT_BOUGHT:Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;

.field public static final enum UNKOWN_PRODUCT:Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 66
    new-instance v0, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;

    const-string v1, "HAS_BOUGHT"

    invoke-direct {v0, v1, v2}, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;->HAS_BOUGHT:Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;

    new-instance v0, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;

    const-string v1, "NOT_BOUGHT"

    invoke-direct {v0, v1, v3}, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;->NOT_BOUGHT:Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;

    new-instance v0, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;

    const-string v1, "UNKOWN_PRODUCT"

    invoke-direct {v0, v1, v4}, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;->UNKOWN_PRODUCT:Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;

    .line 65
    const/4 v0, 0x3

    new-array v0, v0, [Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;

    sget-object v1, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;->HAS_BOUGHT:Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;

    aput-object v1, v0, v2

    sget-object v1, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;->NOT_BOUGHT:Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;

    aput-object v1, v0, v3

    sget-object v1, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;->UNKOWN_PRODUCT:Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;

    aput-object v1, v0, v4

    sput-object v0, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;->$VALUES:[Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;
    .locals 1
    .parameter

    .prologue
    .line 65
    const-class v0, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;

    return-object v0
.end method

.method public static values()[Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;->$VALUES:[Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;

    invoke-virtual {v0}, [Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;

    return-object v0
.end method
