.class public Lmiui/resourcebrowser/view/RecommendGridListItem;
.super Lmiui/resourcebrowser/view/RecommendGridBaseItem;
.source "RecommendGridListItem.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Lmiui/resourcebrowser/view/RecommendGridBaseItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    return-void
.end method

.method static synthetic access$000(Lmiui/resourcebrowser/view/RecommendGridListItem;)Landroid/content/Context;
    .locals 1
    .parameter "x0"

    .prologue
    .line 14
    iget-object v0, p0, Lmiui/resourcebrowser/view/RecommendGridListItem;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method protected getOnClickListener(Lmiui/resourcebrowser/ResourceContext;)Landroid/view/View$OnClickListener;
    .locals 1
    .parameter "resContext"

    .prologue
    .line 22
    new-instance v0, Lmiui/resourcebrowser/view/RecommendGridListItem$1;

    invoke-direct {v0, p0, p1}, Lmiui/resourcebrowser/view/RecommendGridListItem$1;-><init>(Lmiui/resourcebrowser/view/RecommendGridListItem;Lmiui/resourcebrowser/ResourceContext;)V

    return-object v0
.end method

.method protected getRecommendResourceListActivity(Lmiui/resourcebrowser/ResourceContext;)Landroid/util/Pair;
    .locals 3
    .parameter "resContext"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiui/resourcebrowser/ResourceContext;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    new-instance v0, Landroid/util/Pair;

    invoke-virtual {p1}, Lmiui/resourcebrowser/ResourceContext;->getRecommendActivityPackage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lmiui/resourcebrowser/ResourceContext;->getRecommendActivityClass()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method
