.class public Lmiui/resourcebrowser/view/ResourceOperationHandler$CheckRightsTask;
.super Landroid/os/AsyncTask;
.source "ResourceOperationHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/resourcebrowser/view/ResourceOperationHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "CheckRightsTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/resourcebrowser/view/ResourceOperationHandler;


# direct methods
.method protected constructor <init>(Lmiui/resourcebrowser/view/ResourceOperationHandler;)V
    .locals 0
    .parameter

    .prologue
    .line 157
    iput-object p1, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler$CheckRightsTask;->this$0:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 2
    .parameter "params"

    .prologue
    .line 166
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler$CheckRightsTask;->this$0:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    iget-object v0, v0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mService:Lmiui/resourcebrowser/controller/online/DrmService;

    iget-object v1, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler$CheckRightsTask;->this$0:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    iget-object v1, v1, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/controller/online/DrmService;->isLegal(Lmiui/resourcebrowser/model/Resource;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"

    .prologue
    .line 157
    check-cast p1, [Ljava/lang/Void;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/view/ResourceOperationHandler$CheckRightsTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 4
    .parameter "result"

    .prologue
    const/4 v3, 0x0

    .line 171
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler$CheckRightsTask;->this$0:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    iget-object v0, v0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187
    :goto_0
    return-void

    .line 174
    :cond_0
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler$CheckRightsTask;->this$0:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lmiui/resourcebrowser/view/ResourceOperationHandler;->updateLoadingState(ILjava/lang/String;)V

    .line 176
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 177
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler$CheckRightsTask;->this$0:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    const/4 v1, 0x1

    #setter for: Lmiui/resourcebrowser/view/ResourceOperationHandler;->mIsLegal:Z
    invoke-static {v0, v1}, Lmiui/resourcebrowser/view/ResourceOperationHandler;->access$002(Lmiui/resourcebrowser/view/ResourceOperationHandler;Z)Z

    .line 178
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler$CheckRightsTask;->this$0:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    invoke-virtual {v0}, Lmiui/resourcebrowser/view/ResourceOperationHandler;->onRealApplyResourceEvent()V

    .line 186
    :goto_1
    const-string v0, "Theme"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CheckRightsTask return: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 180
    :cond_1
    invoke-static {}, Lmiui/resourcebrowser/controller/online/OnlineService;->isNetworkAvailable()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 181
    new-instance v0, Lmiui/resourcebrowser/view/ResourceOperationHandler$DownloadRightsTask;

    iget-object v1, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler$CheckRightsTask;->this$0:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    invoke-direct {v0, v1}, Lmiui/resourcebrowser/view/ResourceOperationHandler$DownloadRightsTask;-><init>(Lmiui/resourcebrowser/view/ResourceOperationHandler;)V

    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/view/ResourceOperationHandler$DownloadRightsTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_1

    .line 183
    :cond_2
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler$CheckRightsTask;->this$0:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    iget-object v0, v0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mContext:Landroid/content/Context;

    const v1, 0x60c0020

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 157
    check-cast p1, Ljava/lang/Boolean;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/view/ResourceOperationHandler$CheckRightsTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .prologue
    .line 161
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler$CheckRightsTask;->this$0:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    const/16 v1, 0x3e8

    iget-object v2, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler$CheckRightsTask;->this$0:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    iget-object v2, v2, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mContext:Landroid/content/Context;

    const v3, 0x60c0235

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmiui/resourcebrowser/view/ResourceOperationHandler;->updateLoadingState(ILjava/lang/String;)V

    .line 162
    return-void
.end method
