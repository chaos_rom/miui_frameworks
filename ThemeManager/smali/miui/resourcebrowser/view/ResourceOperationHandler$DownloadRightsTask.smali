.class public Lmiui/resourcebrowser/view/ResourceOperationHandler$DownloadRightsTask;
.super Landroid/os/AsyncTask;
.source "ResourceOperationHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/resourcebrowser/view/ResourceOperationHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "DownloadRightsTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/resourcebrowser/view/ResourceOperationHandler;


# direct methods
.method protected constructor <init>(Lmiui/resourcebrowser/view/ResourceOperationHandler;)V
    .locals 0
    .parameter

    .prologue
    .line 190
    iput-object p1, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler$DownloadRightsTask;->this$0:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 10
    .parameter "params"

    .prologue
    const/4 v3, 0x0

    .line 199
    const/4 v9, 0x1

    .line 200
    .local v9, result:I
    invoke-static {}, Lmiui/resourcebrowser/controller/online/OnlineService;->getAccount()Landroid/util/Pair;

    move-result-object v8

    .line 201
    .local v8, auth:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/accounts/Account;Lmiui/net/ExtendedAuthToken;>;"
    if-nez v8, :cond_0

    .line 202
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler$DownloadRightsTask;->this$0:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    iget-object v0, v0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.xiaomi"

    const-string v2, "thememarket"

    iget-object v4, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler$DownloadRightsTask;->this$0:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    iget-object v5, v4, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mContext:Landroid/content/Context;

    check-cast v5, Landroid/app/Activity;

    move-object v4, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/accounts/AccountManager;->addAccount(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 207
    :goto_0
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    .line 205
    :cond_0
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler$DownloadRightsTask;->this$0:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    iget-object v0, v0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mService:Lmiui/resourcebrowser/controller/online/DrmService;

    iget-object v1, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler$DownloadRightsTask;->this$0:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    iget-object v1, v1, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/controller/online/DrmService;->downloadRights(Lmiui/resourcebrowser/model/Resource;)I

    move-result v9

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"

    .prologue
    .line 190
    check-cast p1, [Ljava/lang/Void;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/view/ResourceOperationHandler$DownloadRightsTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 3
    .parameter "result"

    .prologue
    .line 212
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler$DownloadRightsTask;->this$0:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    iget-object v0, v0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 228
    :goto_0
    return-void

    .line 215
    :cond_0
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler$DownloadRightsTask;->this$0:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lmiui/resourcebrowser/view/ResourceOperationHandler;->updateLoadingState(ILjava/lang/String;)V

    .line 216
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 227
    :goto_1
    const-string v0, "Theme"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DownloadRightsTask return: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 218
    :pswitch_0
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler$DownloadRightsTask;->this$0:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    invoke-virtual {v0}, Lmiui/resourcebrowser/view/ResourceOperationHandler;->onCheckResourceRightEventBeforeRealApply()V

    goto :goto_1

    .line 221
    :pswitch_1
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler$DownloadRightsTask;->this$0:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    invoke-virtual {v0}, Lmiui/resourcebrowser/view/ResourceOperationHandler;->onBuyEventPerformed()V

    goto :goto_1

    .line 224
    :pswitch_2
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler$DownloadRightsTask;->this$0:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    iget-object v0, v0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mContext:Landroid/content/Context;

    const v1, 0x60c0020

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 216
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 190
    check-cast p1, Ljava/lang/Integer;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/view/ResourceOperationHandler$DownloadRightsTask;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .prologue
    .line 194
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler$DownloadRightsTask;->this$0:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    const/4 v1, 0x0

    iget-object v2, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler$DownloadRightsTask;->this$0:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    iget-object v2, v2, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mContext:Landroid/content/Context;

    const v3, 0x60c0236

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmiui/resourcebrowser/view/ResourceOperationHandler;->updateLoadingState(ILjava/lang/String;)V

    .line 195
    return-void
.end method
