.class public Lmiui/resourcebrowser/view/UnevenGrid$LayoutParams;
.super Landroid/view/ViewGroup$MarginLayoutParams;
.source "UnevenGrid.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/resourcebrowser/view/UnevenGrid;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LayoutParams"
.end annotation


# instance fields
.field heightCount:I

.field widthCount:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 258
    invoke-direct {p0, v0, v0}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 259
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .parameter "c"
    .parameter "attrs"

    .prologue
    .line 262
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 263
    return-void
.end method

.method public static create(II)Lmiui/resourcebrowser/view/UnevenGrid$LayoutParams;
    .locals 1
    .parameter "widthCount"
    .parameter "heightCount"

    .prologue
    .line 266
    new-instance v0, Lmiui/resourcebrowser/view/UnevenGrid$LayoutParams;

    invoke-direct {v0}, Lmiui/resourcebrowser/view/UnevenGrid$LayoutParams;-><init>()V

    .line 267
    .local v0, param:Lmiui/resourcebrowser/view/UnevenGrid$LayoutParams;
    iput p0, v0, Lmiui/resourcebrowser/view/UnevenGrid$LayoutParams;->widthCount:I

    .line 268
    iput p1, v0, Lmiui/resourcebrowser/view/UnevenGrid$LayoutParams;->heightCount:I

    .line 269
    return-object v0
.end method
