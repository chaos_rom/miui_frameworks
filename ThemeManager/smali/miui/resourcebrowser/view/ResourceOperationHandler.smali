.class public Lmiui/resourcebrowser/view/ResourceOperationHandler;
.super Ljava/lang/Object;
.source "ResourceOperationHandler.java"

# interfaces
.implements Lmiui/app/ActivityLifecycleObserver;
.implements Lmiui/resourcebrowser/util/ResourceDownloadHandler$ResourceDownloadListener;
.implements Lmiui/resourcebrowser/view/ResourceOperationView$ResourceOperationListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask;,
        Lmiui/resourcebrowser/view/ResourceOperationHandler$DownloadRightsTask;,
        Lmiui/resourcebrowser/view/ResourceOperationHandler$CheckRightsTask;,
        Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;,
        Lmiui/resourcebrowser/view/ResourceOperationHandler$LoadingStateInfo;
    }
.end annotation


# instance fields
.field private mCheckRightsCountDuringApplyEvent:I

.field protected mContext:Landroid/content/Context;

.field protected mDownloadHandler:Lmiui/resourcebrowser/util/ResourceDownloadHandler;

.field private mIsLegal:Z

.field protected mLoadingInfo:Lmiui/resourcebrowser/view/ResourceOperationHandler$LoadingStateInfo;

.field protected mOperationView:Lmiui/resourcebrowser/view/ResourceOperationView;

.field protected mResContext:Lmiui/resourcebrowser/ResourceContext;

.field protected mResController:Lmiui/resourcebrowser/controller/ResourceController;

.field protected mResource:Lmiui/resourcebrowser/model/Resource;

.field protected mService:Lmiui/resourcebrowser/controller/online/DrmService;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lmiui/resourcebrowser/ResourceContext;Lmiui/resourcebrowser/view/ResourceOperationView;)V
    .locals 3
    .parameter "context"
    .parameter "resContext"
    .parameter "view"

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    new-instance v0, Lmiui/resourcebrowser/view/ResourceOperationHandler$LoadingStateInfo;

    invoke-direct {v0}, Lmiui/resourcebrowser/view/ResourceOperationHandler$LoadingStateInfo;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mLoadingInfo:Lmiui/resourcebrowser/view/ResourceOperationHandler$LoadingStateInfo;

    .line 69
    const/4 v0, 0x0

    iput v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mCheckRightsCountDuringApplyEvent:I

    .line 72
    if-nez p3, :cond_0

    .line 73
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Operated view can not be null."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 76
    :cond_0
    iput-object p1, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mContext:Landroid/content/Context;

    .line 77
    iput-object p2, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    .line 78
    iput-object p3, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mOperationView:Lmiui/resourcebrowser/view/ResourceOperationView;

    .line 79
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mOperationView:Lmiui/resourcebrowser/view/ResourceOperationView;

    invoke-virtual {v0, p0}, Lmiui/resourcebrowser/view/ResourceOperationView;->setResourceOperationHandler(Lmiui/resourcebrowser/view/ResourceOperationHandler;)V

    .line 80
    new-instance v0, Lmiui/resourcebrowser/util/ResourceDownloadHandler;

    iget-object v1, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-direct {v0, v1, v2}, Lmiui/resourcebrowser/util/ResourceDownloadHandler;-><init>(Landroid/content/Context;Lmiui/resourcebrowser/ResourceContext;)V

    iput-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mDownloadHandler:Lmiui/resourcebrowser/util/ResourceDownloadHandler;

    .line 81
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mDownloadHandler:Lmiui/resourcebrowser/util/ResourceDownloadHandler;

    invoke-virtual {v0, p0}, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->setResourceDownloadListener(Lmiui/resourcebrowser/util/ResourceDownloadHandler$ResourceDownloadListener;)V

    .line 82
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mDownloadHandler:Lmiui/resourcebrowser/util/ResourceDownloadHandler;

    invoke-virtual {v0}, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->registerDownloadReceiver()V

    .line 83
    new-instance v0, Lmiui/resourcebrowser/controller/online/DrmService;

    iget-object v1, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-direct {v0, v1}, Lmiui/resourcebrowser/controller/online/DrmService;-><init>(Lmiui/resourcebrowser/ResourceContext;)V

    iput-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mService:Lmiui/resourcebrowser/controller/online/DrmService;

    .line 84
    return-void
.end method

.method static synthetic access$002(Lmiui/resourcebrowser/view/ResourceOperationHandler;Z)Z
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 37
    iput-boolean p1, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mIsLegal:Z

    return p1
.end method

.method static synthetic access$100(Lmiui/resourcebrowser/view/ResourceOperationHandler;)Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;
    .locals 1
    .parameter "x0"

    .prologue
    .line 37
    invoke-direct {p0}, Lmiui/resourcebrowser/view/ResourceOperationHandler;->checkProductState()Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;

    move-result-object v0

    return-object v0
.end method

.method private checkProductState()Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 232
    iget-object v1, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mResController:Lmiui/resourcebrowser/controller/ResourceController;

    invoke-virtual {v1}, Lmiui/resourcebrowser/controller/ResourceController;->getOnlineDataManager()Lmiui/resourcebrowser/controller/OnlineDataManager;

    move-result-object v1

    iget-object v2, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v2}, Lmiui/resourcebrowser/model/Resource;->getOnlineId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v3}, Lmiui/resourcebrowser/controller/OnlineDataManager;->getResource(Ljava/lang/String;Z)Lmiui/resourcebrowser/model/Resource;

    move-result-object v0

    .line 233
    .local v0, res:Lmiui/resourcebrowser/model/Resource;
    if-nez v0, :cond_0

    .line 234
    sget-object v1, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;->UNKOWN_PRODUCT:Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;

    .line 239
    :goto_0
    return-object v1

    .line 235
    :cond_0
    invoke-virtual {v0}, Lmiui/resourcebrowser/model/Resource;->isProductBought()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 236
    iget-object v1, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v1, v3}, Lmiui/resourcebrowser/model/Resource;->setProductBought(Z)V

    .line 237
    sget-object v1, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;->HAS_BOUGHT:Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;

    goto :goto_0

    .line 239
    :cond_1
    sget-object v1, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;->NOT_BOUGHT:Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductState;

    goto :goto_0
.end method

.method private downloadResource()V
    .locals 2

    .prologue
    .line 143
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lmiui/resourcebrowser/view/ResourceOperationHandler$1;

    invoke-direct {v1, p0}, Lmiui/resourcebrowser/view/ResourceOperationHandler$1;-><init>(Lmiui/resourcebrowser/view/ResourceOperationHandler;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 149
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mDownloadHandler:Lmiui/resourcebrowser/util/ResourceDownloadHandler;

    iget-object v1, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->downloadResource(Lmiui/resourcebrowser/model/Resource;)Z

    .line 150
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mOperationView:Lmiui/resourcebrowser/view/ResourceOperationView;

    invoke-virtual {v0}, Lmiui/resourcebrowser/view/ResourceOperationView;->updateStatus()V

    .line 151
    return-void
.end method

.method private reset()V
    .locals 1

    .prologue
    .line 101
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mIsLegal:Z

    .line 102
    return-void
.end method


# virtual methods
.method public getLoadingStateInfo()Lmiui/resourcebrowser/view/ResourceOperationHandler$LoadingStateInfo;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mLoadingInfo:Lmiui/resourcebrowser/view/ResourceOperationHandler$LoadingStateInfo;

    return-object v0
.end method

.method public getPrice()I
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v0}, Lmiui/resourcebrowser/model/Resource;->getProductPrice()I

    move-result v0

    return v0
.end method

.method public isAuthorizedResource()Z
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v0}, Lmiui/resourcebrowser/model/Resource;->isProductBought()Z

    move-result v0

    return v0
.end method

.method public isDeletable()Z
    .locals 1

    .prologue
    .line 129
    invoke-virtual {p0}, Lmiui/resourcebrowser/view/ResourceOperationHandler;->isLocalResource()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v0}, Lmiui/resourcebrowser/model/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lmiui/resourcebrowser/util/ResourceHelper;->isSystemResource(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDownloading()Z
    .locals 2

    .prologue
    .line 121
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v0}, Lmiui/resourcebrowser/model/Resource;->getDownloadPath()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mDownloadHandler:Lmiui/resourcebrowser/util/ResourceDownloadHandler;

    iget-object v1, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v1}, Lmiui/resourcebrowser/model/Resource;->getOnlineId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->isResourceDownloading(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isLegal()Z
    .locals 1

    .prologue
    .line 154
    iget-boolean v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mIsLegal:Z

    return v0
.end method

.method public isLocalResource()Z
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v0}, Lmiui/resourcebrowser/model/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v1}, Lmiui/resourcebrowser/model/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isOldResource()Z
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v0}, Lmiui/resourcebrowser/model/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v0}, Lmiui/resourcebrowser/model/Resource;->getStatus()I

    move-result v0

    invoke-static {v0}, Lmiui/resourcebrowser/util/ResourceHelper;->isOldResource(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPicker()Z
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v0}, Lmiui/resourcebrowser/ResourceContext;->isPicker()Z

    move-result v0

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 0
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 432
    return-void
.end method

.method public final onApplyEventPerformed()V
    .locals 1

    .prologue
    .line 315
    const/4 v0, 0x0

    iput v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mCheckRightsCountDuringApplyEvent:I

    .line 316
    invoke-virtual {p0}, Lmiui/resourcebrowser/view/ResourceOperationHandler;->onCheckResourceRightEventBeforeRealApply()V

    .line 317
    return-void
.end method

.method public onBuyEventPerformed()V
    .locals 4

    .prologue
    .line 338
    iget-object v1, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v1}, Lmiui/resourcebrowser/model/Resource;->getProductId()Ljava/lang/String;

    move-result-object v0

    .line 339
    .local v0, productId:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 340
    invoke-static {}, Lmiui/resourcebrowser/controller/online/OnlineService;->isNetworkAvailable()Z

    move-result v1

    if-nez v1, :cond_0

    .line 341
    iget-object v1, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mContext:Landroid/content/Context;

    const v2, 0x60c0020

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 343
    :cond_0
    const-string v1, "Theme"

    const-string v2, "Fail to buy resource because of emtry product ID."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 347
    :goto_0
    return-void

    .line 346
    :cond_1
    new-instance v1, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask;

    invoke-direct {v1, p0}, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask;-><init>(Lmiui/resourcebrowser/view/ResourceOperationHandler;)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lmiui/resourcebrowser/view/ResourceOperationHandler$ProductBoughtTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method protected onCheckResourceRightEventBeforeRealApply()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 320
    iget-boolean v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mIsLegal:Z

    if-eqz v0, :cond_0

    .line 321
    invoke-virtual {p0}, Lmiui/resourcebrowser/view/ResourceOperationHandler;->onRealApplyResourceEvent()V

    .line 331
    :goto_0
    return-void

    .line 323
    :cond_0
    iget v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mCheckRightsCountDuringApplyEvent:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mCheckRightsCountDuringApplyEvent:I

    const/4 v1, 0x2

    if-le v0, v1, :cond_1

    .line 325
    const-string v0, "Theme"

    const-string v1, "Fail to get theme auth because of exceeding max count of checking."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mContext:Landroid/content/Context;

    const v1, 0x60c024d

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 328
    :cond_1
    new-instance v0, Lmiui/resourcebrowser/view/ResourceOperationHandler$CheckRightsTask;

    invoke-direct {v0, p0}, Lmiui/resourcebrowser/view/ResourceOperationHandler$CheckRightsTask;-><init>(Lmiui/resourcebrowser/view/ResourceOperationHandler;)V

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/view/ResourceOperationHandler$CheckRightsTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .parameter "savedInstanceState"

    .prologue
    .line 406
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mDownloadHandler:Lmiui/resourcebrowser/util/ResourceDownloadHandler;

    invoke-virtual {v0}, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->registerDownloadReceiver()V

    .line 407
    return-void
.end method

.method public onDeleteEventPerformed()V
    .locals 0

    .prologue
    .line 375
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 427
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mDownloadHandler:Lmiui/resourcebrowser/util/ResourceDownloadHandler;

    invoke-virtual {v0}, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->unregisterDownloadReceiver()V

    .line 428
    return-void
.end method

.method public onDownloadEventPerformed()V
    .locals 0

    .prologue
    .line 365
    invoke-direct {p0}, Lmiui/resourcebrowser/view/ResourceOperationHandler;->downloadResource()V

    .line 366
    return-void
.end method

.method public onDownloadFailed(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .parameter "path"
    .parameter "onlineId"

    .prologue
    .line 391
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v0}, Lmiui/resourcebrowser/model/Resource;->getDownloadPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 392
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mContext:Landroid/content/Context;

    const v1, 0x60c0023

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 393
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mOperationView:Lmiui/resourcebrowser/view/ResourceOperationView;

    invoke-virtual {v0}, Lmiui/resourcebrowser/view/ResourceOperationView;->updateStatus()V

    .line 395
    :cond_0
    return-void
.end method

.method public onDownloadProgressUpdated(Ljava/lang/String;Ljava/lang/String;II)V
    .locals 1
    .parameter "path"
    .parameter "onlineId"
    .parameter "downloadBytes"
    .parameter "totalBytes"

    .prologue
    .line 399
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v0}, Lmiui/resourcebrowser/model/Resource;->getDownloadPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 400
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mOperationView:Lmiui/resourcebrowser/view/ResourceOperationView;

    invoke-virtual {v0, p3, p4}, Lmiui/resourcebrowser/view/ResourceOperationView;->updateDownloadProgressBar(II)V

    .line 402
    :cond_0
    return-void
.end method

.method public onDownloadSuccessful(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .parameter "path"
    .parameter "onlineId"

    .prologue
    .line 383
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v0}, Lmiui/resourcebrowser/model/Resource;->getDownloadPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 384
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/resourcebrowser/model/Resource;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/model/Resource;->setStatus(I)V

    .line 385
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mOperationView:Lmiui/resourcebrowser/view/ResourceOperationView;

    invoke-virtual {v0}, Lmiui/resourcebrowser/view/ResourceOperationView;->updateStatus()V

    .line 387
    :cond_0
    return-void
.end method

.method public onMagicEventPerformed()V
    .locals 0

    .prologue
    .line 379
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 419
    return-void
.end method

.method public onPickEventPerformed()V
    .locals 5

    .prologue
    .line 351
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 352
    .local v2, resultIntent:Landroid/content/Intent;
    iget-object v3, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v3}, Lmiui/resourcebrowser/model/Resource;->getFilePath()Ljava/lang/String;

    move-result-object v1

    .line 353
    .local v1, localPath:Ljava/lang/String;
    const-string v3, "RESPONSE_PICKED_RESOURCE"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 354
    const-string v3, "RESPONSE_TRACK_ID"

    iget-object v4, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v4}, Lmiui/resourcebrowser/ResourceContext;->getTrackId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 355
    iget-object v3, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v3}, Lmiui/resourcebrowser/ResourceContext;->getResourceFormat()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_0

    .line 356
    const-string v3, "android.intent.extra.ringtone.PICKED_URI"

    invoke-static {v1}, Lmiui/resourcebrowser/util/ResourceHelper;->getUriByPath(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 358
    :cond_0
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 359
    .local v0, activity:Landroid/app/Activity;
    const/4 v3, -0x1

    invoke-virtual {v0, v3, v2}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 360
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 361
    return-void
.end method

.method protected onRealApplyResourceEvent()V
    .locals 0

    .prologue
    .line 334
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 415
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 411
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 423
    return-void
.end method

.method public onUpdateEventPerformed()V
    .locals 0

    .prologue
    .line 370
    invoke-direct {p0}, Lmiui/resourcebrowser/view/ResourceOperationHandler;->downloadResource()V

    .line 371
    return-void
.end method

.method public setResource(Lmiui/resourcebrowser/model/Resource;)V
    .locals 1
    .parameter "resource"

    .prologue
    .line 95
    iput-object p1, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/resourcebrowser/model/Resource;

    .line 96
    invoke-direct {p0}, Lmiui/resourcebrowser/view/ResourceOperationHandler;->reset()V

    .line 97
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mOperationView:Lmiui/resourcebrowser/view/ResourceOperationView;

    invoke-virtual {v0}, Lmiui/resourcebrowser/view/ResourceOperationView;->updateStatus()V

    .line 98
    return-void
.end method

.method public setResourceController(Lmiui/resourcebrowser/controller/ResourceController;)V
    .locals 0
    .parameter "resController"

    .prologue
    .line 87
    iput-object p1, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mResController:Lmiui/resourcebrowser/controller/ResourceController;

    .line 88
    return-void
.end method

.method public updateLoadingState(ILjava/lang/String;)V
    .locals 1
    .parameter "delay"
    .parameter "text"

    .prologue
    .line 137
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mLoadingInfo:Lmiui/resourcebrowser/view/ResourceOperationHandler$LoadingStateInfo;

    iput p1, v0, Lmiui/resourcebrowser/view/ResourceOperationHandler$LoadingStateInfo;->delayTime:I

    .line 138
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mLoadingInfo:Lmiui/resourcebrowser/view/ResourceOperationHandler$LoadingStateInfo;

    iput-object p2, v0, Lmiui/resourcebrowser/view/ResourceOperationHandler$LoadingStateInfo;->title:Ljava/lang/String;

    .line 139
    iget-object v0, p0, Lmiui/resourcebrowser/view/ResourceOperationHandler;->mOperationView:Lmiui/resourcebrowser/view/ResourceOperationView;

    invoke-virtual {v0}, Lmiui/resourcebrowser/view/ResourceOperationView;->updateStatus()V

    .line 140
    return-void
.end method
