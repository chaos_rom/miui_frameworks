.class public abstract Lmiui/resourcebrowser/view/RecommendGridBaseItem;
.super Landroid/widget/FrameLayout;
.source "RecommendGridBaseItem.java"


# static fields
.field private static sDownloadingCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lmiui/resourcebrowser/model/PathEntry;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mImageView:Landroid/widget/ImageView;

.field protected mRecommendItemData:Lmiui/resourcebrowser/model/RecommendItemData;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 86
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lmiui/resourcebrowser/view/RecommendGridBaseItem;->sDownloadingCache:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    return-void
.end method

.method static synthetic access$000(Lmiui/resourcebrowser/view/RecommendGridBaseItem;)Landroid/widget/ImageView;
    .locals 1
    .parameter "x0"

    .prologue
    .line 24
    iget-object v0, p0, Lmiui/resourcebrowser/view/RecommendGridBaseItem;->mImageView:Landroid/widget/ImageView;

    return-object v0
.end method


# virtual methods
.method public bind(Lmiui/resourcebrowser/model/RecommendItemData;Lmiui/resourcebrowser/ResourceContext;)V
    .locals 5
    .parameter "gridItemData"
    .parameter "resContext"

    .prologue
    .line 44
    iput-object p1, p0, Lmiui/resourcebrowser/view/RecommendGridBaseItem;->mRecommendItemData:Lmiui/resourcebrowser/model/RecommendItemData;

    .line 45
    iget-object v3, p0, Lmiui/resourcebrowser/view/RecommendGridBaseItem;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {p0, p2}, Lmiui/resourcebrowser/view/RecommendGridBaseItem;->getOnClickListener(Lmiui/resourcebrowser/ResourceContext;)Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 46
    new-instance v2, Ljava/io/File;

    iget-object v3, p1, Lmiui/resourcebrowser/model/RecommendItemData;->localThumbnail:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 47
    .local v2, file:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 48
    new-instance v3, Lmiui/util/InputStreamLoader;

    iget-object v4, p1, Lmiui/resourcebrowser/model/RecommendItemData;->localThumbnail:Ljava/lang/String;

    invoke-direct {v3, v4}, Lmiui/util/InputStreamLoader;-><init>(Ljava/lang/String;)V

    const/4 v4, -0x1

    invoke-static {v3, v4}, Lmiui/util/ImageUtils;->getBitmap(Lmiui/util/InputStreamLoader;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 49
    .local v0, bitmap:Landroid/graphics/Bitmap;
    iget-object v3, p0, Lmiui/resourcebrowser/view/RecommendGridBaseItem;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 54
    .end local v0           #bitmap:Landroid/graphics/Bitmap;
    :cond_0
    :goto_0
    return-void

    .line 50
    :cond_1
    iget-object v3, p1, Lmiui/resourcebrowser/model/RecommendItemData;->onlineThumbnail:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 51
    new-instance v1, Lmiui/resourcebrowser/model/PathEntry;

    iget-object v3, p1, Lmiui/resourcebrowser/model/RecommendItemData;->localThumbnail:Ljava/lang/String;

    iget-object v4, p1, Lmiui/resourcebrowser/model/RecommendItemData;->onlineThumbnail:Ljava/lang/String;

    invoke-direct {v1, v3, v4}, Lmiui/resourcebrowser/model/PathEntry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    .local v1, entry:Lmiui/resourcebrowser/model/PathEntry;
    invoke-virtual {p0, v1}, Lmiui/resourcebrowser/view/RecommendGridBaseItem;->downloadThumbnail(Lmiui/resourcebrowser/model/PathEntry;)V

    goto :goto_0
.end method

.method protected downloadThumbnail(Lmiui/resourcebrowser/model/PathEntry;)V
    .locals 5
    .parameter "path"

    .prologue
    .line 59
    sget-object v1, Lmiui/resourcebrowser/view/RecommendGridBaseItem;->sDownloadingCache:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 60
    .local v0, lastDownloadFinishTime:Ljava/lang/Long;
    if-eqz v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    sub-long/2addr v1, v3

    const-wide/16 v3, 0x7530

    cmp-long v1, v1, v3

    if-gez v1, :cond_0

    .line 84
    :goto_0
    return-void

    .line 64
    :cond_0
    sget-object v1, Lmiui/resourcebrowser/view/RecommendGridBaseItem;->sDownloadingCache:Ljava/util/HashMap;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    new-instance v1, Lmiui/resourcebrowser/view/RecommendGridBaseItem$1;

    invoke-direct {v1, p0}, Lmiui/resourcebrowser/view/RecommendGridBaseItem$1;-><init>(Lmiui/resourcebrowser/view/RecommendGridBaseItem;)V

    const/4 v2, 0x1

    new-array v2, v2, [Lmiui/resourcebrowser/model/PathEntry;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v1, v2}, Lmiui/resourcebrowser/view/RecommendGridBaseItem$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method protected abstract getOnClickListener(Lmiui/resourcebrowser/ResourceContext;)Landroid/view/View$OnClickListener;
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 36
    const v0, 0x60b006a

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/view/RecommendGridBaseItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lmiui/resourcebrowser/view/RecommendGridBaseItem;->mImageView:Landroid/widget/ImageView;

    .line 37
    return-void
.end method
