.class public Lmiui/resourcebrowser/view/RecommendGridSingleItem;
.super Lmiui/resourcebrowser/view/RecommendGridBaseItem;
.source "RecommendGridSingleItem.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Lmiui/resourcebrowser/view/RecommendGridBaseItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    return-void
.end method

.method static synthetic access$000(Lmiui/resourcebrowser/view/RecommendGridSingleItem;)Landroid/content/Context;
    .locals 1
    .parameter "x0"

    .prologue
    .line 19
    iget-object v0, p0, Lmiui/resourcebrowser/view/RecommendGridSingleItem;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method protected buildResourceSet(Lmiui/resourcebrowser/ResourceContext;)Ljava/util/List;
    .locals 7
    .parameter "resContext"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiui/resourcebrowser/ResourceContext;",
            ")",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/widget/DataGroup",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 53
    new-instance v4, Lmiui/resourcebrowser/model/Resource;

    invoke-direct {v4}, Lmiui/resourcebrowser/model/Resource;-><init>()V

    .line 54
    .local v4, resource:Lmiui/resourcebrowser/model/Resource;
    iget-object v5, p0, Lmiui/resourcebrowser/view/RecommendGridBaseItem;->mRecommendItemData:Lmiui/resourcebrowser/model/RecommendItemData;

    iget-object v2, v5, Lmiui/resourcebrowser/model/RecommendItemData;->itemId:Ljava/lang/String;

    .line 55
    .local v2, onlineId:Ljava/lang/String;
    invoke-static {}, Lmiui/resourcebrowser/AppInnerContext;->getInstance()Lmiui/resourcebrowser/AppInnerContext;

    move-result-object v5

    invoke-virtual {v5}, Lmiui/resourcebrowser/AppInnerContext;->getResourceController()Lmiui/resourcebrowser/controller/ResourceController;

    move-result-object v5

    invoke-virtual {v5}, Lmiui/resourcebrowser/controller/ResourceController;->getLocalDataManager()Lmiui/resourcebrowser/controller/LocalDataManager;

    move-result-object v5

    invoke-virtual {v5, v2}, Lmiui/resourcebrowser/controller/LocalDataManager;->getResourceByOnlineId(Ljava/lang/String;)Lmiui/resourcebrowser/model/Resource;

    move-result-object v3

    .line 57
    .local v3, r:Lmiui/resourcebrowser/model/Resource;
    if-eqz v3, :cond_0

    .line 58
    invoke-virtual {v4, v3}, Lmiui/resourcebrowser/model/Resource;->updateFrom(Lmiui/resourcebrowser/model/Resource;)V

    .line 60
    :cond_0
    invoke-virtual {v4, v2}, Lmiui/resourcebrowser/model/Resource;->setOnlineId(Ljava/lang/String;)V

    .line 61
    new-instance v5, Lmiui/resourcebrowser/controller/online/OnlineService;

    invoke-direct {v5, p1}, Lmiui/resourcebrowser/controller/online/OnlineService;-><init>(Lmiui/resourcebrowser/ResourceContext;)V

    iget-object v6, p0, Lmiui/resourcebrowser/view/RecommendGridBaseItem;->mRecommendItemData:Lmiui/resourcebrowser/model/RecommendItemData;

    iget-object v6, v6, Lmiui/resourcebrowser/model/RecommendItemData;->itemId:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lmiui/resourcebrowser/controller/online/OnlineService;->getDownloadUrl(Ljava/lang/String;)Lmiui/resourcebrowser/controller/online/RequestUrl;

    move-result-object v5

    invoke-virtual {v5}, Lmiui/resourcebrowser/controller/online/RequestUrl;->getUrl()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lmiui/resourcebrowser/model/Resource;->setOnlinePath(Ljava/lang/String;)V

    .line 62
    iget-object v5, p0, Lmiui/resourcebrowser/view/RecommendGridBaseItem;->mRecommendItemData:Lmiui/resourcebrowser/model/RecommendItemData;

    iget-object v5, v5, Lmiui/resourcebrowser/model/RecommendItemData;->title:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lmiui/resourcebrowser/model/Resource;->setTitle(Ljava/lang/String;)V

    .line 63
    if-nez v3, :cond_1

    const/4 v5, 0x2

    :goto_0
    invoke-virtual {v4, v5}, Lmiui/resourcebrowser/model/Resource;->setStatus(I)V

    .line 65
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 66
    .local v0, dataSet:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/widget/DataGroup<Lmiui/resourcebrowser/model/Resource;>;>;"
    new-instance v1, Lmiui/resourcebrowser/widget/DataGroup;

    invoke-direct {v1}, Lmiui/resourcebrowser/widget/DataGroup;-><init>()V

    .line 67
    .local v1, group:Lmiui/resourcebrowser/widget/DataGroup;,"Lmiui/resourcebrowser/widget/DataGroup<Lmiui/resourcebrowser/model/Resource;>;"
    invoke-virtual {v1, v4}, Lmiui/resourcebrowser/widget/DataGroup;->add(Ljava/lang/Object;)Z

    .line 68
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 69
    return-object v0

    .line 63
    .end local v0           #dataSet:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/widget/DataGroup<Lmiui/resourcebrowser/model/Resource;>;>;"
    .end local v1           #group:Lmiui/resourcebrowser/widget/DataGroup;,"Lmiui/resourcebrowser/widget/DataGroup<Lmiui/resourcebrowser/model/Resource;>;"
    :cond_1
    const/4 v5, 0x3

    goto :goto_0
.end method

.method protected getOnClickListener(Lmiui/resourcebrowser/ResourceContext;)Landroid/view/View$OnClickListener;
    .locals 1
    .parameter "resContext"

    .prologue
    .line 27
    new-instance v0, Lmiui/resourcebrowser/view/RecommendGridSingleItem$1;

    invoke-direct {v0, p0, p1}, Lmiui/resourcebrowser/view/RecommendGridSingleItem$1;-><init>(Lmiui/resourcebrowser/view/RecommendGridSingleItem;Lmiui/resourcebrowser/ResourceContext;)V

    return-object v0
.end method

.method protected getResourceDetailActivity(Lmiui/resourcebrowser/ResourceContext;)Landroid/util/Pair;
    .locals 3
    .parameter "resContext"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiui/resourcebrowser/ResourceContext;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 48
    new-instance v0, Landroid/util/Pair;

    invoke-virtual {p1}, Lmiui/resourcebrowser/ResourceContext;->getDetailActivityPackage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lmiui/resourcebrowser/ResourceContext;->getDetailActivityClass()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method
