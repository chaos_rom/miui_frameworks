.class Lmiui/resourcebrowser/view/RecommendGridListItem$1;
.super Ljava/lang/Object;
.source "RecommendGridListItem.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiui/resourcebrowser/view/RecommendGridListItem;->getOnClickListener(Lmiui/resourcebrowser/ResourceContext;)Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/resourcebrowser/view/RecommendGridListItem;

.field final synthetic val$resContext:Lmiui/resourcebrowser/ResourceContext;


# direct methods
.method constructor <init>(Lmiui/resourcebrowser/view/RecommendGridListItem;Lmiui/resourcebrowser/ResourceContext;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 22
    iput-object p1, p0, Lmiui/resourcebrowser/view/RecommendGridListItem$1;->this$0:Lmiui/resourcebrowser/view/RecommendGridListItem;

    iput-object p2, p0, Lmiui/resourcebrowser/view/RecommendGridListItem$1;->val$resContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .parameter "v"

    .prologue
    const/4 v4, 0x1

    .line 25
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 26
    .local v1, intent:Landroid/content/Intent;
    iget-object v2, p0, Lmiui/resourcebrowser/view/RecommendGridListItem$1;->this$0:Lmiui/resourcebrowser/view/RecommendGridListItem;

    iget-object v3, p0, Lmiui/resourcebrowser/view/RecommendGridListItem$1;->val$resContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v2, v3}, Lmiui/resourcebrowser/view/RecommendGridListItem;->getRecommendResourceListActivity(Lmiui/resourcebrowser/ResourceContext;)Landroid/util/Pair;

    move-result-object v0

    .line 27
    .local v0, activityClass:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v2, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    iget-object v3, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 28
    const/high16 v2, 0x400

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 29
    const-string v2, "REQUEST_RES_CONTEXT"

    iget-object v3, p0, Lmiui/resourcebrowser/view/RecommendGridListItem$1;->val$resContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 30
    const-string v2, "REQUEST_RECOMMEND_ID"

    iget-object v3, p0, Lmiui/resourcebrowser/view/RecommendGridListItem$1;->this$0:Lmiui/resourcebrowser/view/RecommendGridListItem;

    iget-object v3, v3, Lmiui/resourcebrowser/view/RecommendGridBaseItem;->mRecommendItemData:Lmiui/resourcebrowser/model/RecommendItemData;

    iget-object v3, v3, Lmiui/resourcebrowser/model/RecommendItemData;->itemId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 31
    const-string v2, "REQUEST_IS_RECOMMEND_LIST"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 32
    const-string v3, "REQUEST_SUB_RECOMMENDS"

    iget-object v2, p0, Lmiui/resourcebrowser/view/RecommendGridListItem$1;->this$0:Lmiui/resourcebrowser/view/RecommendGridListItem;

    iget-object v2, v2, Lmiui/resourcebrowser/view/RecommendGridBaseItem;->mRecommendItemData:Lmiui/resourcebrowser/model/RecommendItemData;

    iget-object v2, v2, Lmiui/resourcebrowser/model/RecommendItemData;->subItems:Ljava/util/List;

    check-cast v2, Ljava/io/Serializable;

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 33
    iget-object v2, p0, Lmiui/resourcebrowser/view/RecommendGridListItem$1;->this$0:Lmiui/resourcebrowser/view/RecommendGridListItem;

    #getter for: Lmiui/resourcebrowser/view/RecommendGridListItem;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lmiui/resourcebrowser/view/RecommendGridListItem;->access$000(Lmiui/resourcebrowser/view/RecommendGridListItem;)Landroid/content/Context;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2, v1, v4}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 34
    return-void
.end method
