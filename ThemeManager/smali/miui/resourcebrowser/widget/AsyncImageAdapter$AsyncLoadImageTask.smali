.class public Lmiui/resourcebrowser/widget/AsyncImageAdapter$AsyncLoadImageTask;
.super Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadPartialDataTask;
.source "AsyncImageAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/resourcebrowser/widget/AsyncImageAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "AsyncLoadImageTask"
.end annotation


# instance fields
.field private mScaled:Z

.field private mTargetHeight:I

.field private mTargetWidth:I

.field final synthetic this$0:Lmiui/resourcebrowser/widget/AsyncImageAdapter;


# direct methods
.method public constructor <init>(Lmiui/resourcebrowser/widget/AsyncImageAdapter;)V
    .locals 0
    .parameter

    .prologue
    .line 18
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncImageAdapter$AsyncLoadImageTask;,"Lmiui/resourcebrowser/widget/AsyncImageAdapter<TT;>.AsyncLoadImageTask;"
    iput-object p1, p0, Lmiui/resourcebrowser/widget/AsyncImageAdapter$AsyncLoadImageTask;->this$0:Lmiui/resourcebrowser/widget/AsyncImageAdapter;

    invoke-direct {p0, p1}, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadPartialDataTask;-><init>(Lmiui/resourcebrowser/widget/AsyncAdapter;)V

    return-void
.end method


# virtual methods
.method protected doJob(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 17
    .parameter "key"

    .prologue
    .line 35
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncImageAdapter$AsyncLoadImageTask;,"Lmiui/resourcebrowser/widget/AsyncImageAdapter<TT;>.AsyncLoadImageTask;"
    move-object/from16 v5, p1

    check-cast v5, Ljava/lang/String;

    .line 37
    .local v5, imagePath:Ljava/lang/String;
    invoke-static {v5}, Lmiui/util/ImageUtils;->getBitmapSize(Ljava/lang/String;)Landroid/graphics/BitmapFactory$Options;

    move-result-object v1

    .line 38
    .local v1, boundsOptions:Landroid/graphics/BitmapFactory$Options;
    iget v12, v1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 39
    .local v12, width:I
    iget v4, v1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 41
    .local v4, height:I
    move-object/from16 v0, p0

    iget v15, v0, Lmiui/resourcebrowser/widget/AsyncImageAdapter$AsyncLoadImageTask;->mTargetWidth:I

    if-lez v15, :cond_0

    move-object/from16 v0, p0

    iget v15, v0, Lmiui/resourcebrowser/widget/AsyncImageAdapter$AsyncLoadImageTask;->mTargetHeight:I

    if-gtz v15, :cond_1

    .line 42
    :cond_0
    const-string v15, "ResourceBrowser"

    const-string v16, "AsyncImageAdapter does not set valid parameters for target size."

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    move-object/from16 v0, p0

    iput v12, v0, Lmiui/resourcebrowser/widget/AsyncImageAdapter$AsyncLoadImageTask;->mTargetWidth:I

    .line 44
    move-object/from16 v0, p0

    iput v4, v0, Lmiui/resourcebrowser/widget/AsyncImageAdapter$AsyncLoadImageTask;->mTargetHeight:I

    .line 47
    :cond_1
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 48
    .local v2, decodeOptions:Landroid/graphics/BitmapFactory$Options;
    move-object/from16 v0, p0

    iget-object v15, v0, Lmiui/resourcebrowser/widget/AsyncImageAdapter$AsyncLoadImageTask;->this$0:Lmiui/resourcebrowser/widget/AsyncImageAdapter;

    invoke-virtual {v15}, Lmiui/resourcebrowser/widget/AsyncImageAdapter;->useLowQualityDecoding()Z

    move-result v15

    if-eqz v15, :cond_2

    .line 49
    sget-object v15, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v15, v2, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 52
    :cond_2
    const/4 v9, 0x0

    .line 53
    .local v9, scaledBitmap:Landroid/graphics/Bitmap;
    const/4 v8, 0x0

    .line 55
    .local v8, originalBitmap:Landroid/graphics/Bitmap;
    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lmiui/resourcebrowser/widget/AsyncImageAdapter$AsyncLoadImageTask;->mScaled:Z

    if-nez v15, :cond_4

    .line 56
    move-object/from16 v0, p0

    iget v15, v0, Lmiui/resourcebrowser/widget/AsyncImageAdapter$AsyncLoadImageTask;->mTargetWidth:I

    invoke-static {v12, v15}, Ljava/lang/Math;->min(II)I

    move-result v7

    .line 57
    .local v7, minWidth:I
    move-object/from16 v0, p0

    iget v15, v0, Lmiui/resourcebrowser/widget/AsyncImageAdapter$AsyncLoadImageTask;->mTargetHeight:I

    invoke-static {v4, v15}, Ljava/lang/Math;->min(II)I

    move-result v6

    .line 58
    .local v6, minHeight:I
    sub-int v15, v12, v7

    div-int/lit8 v13, v15, 0x2

    .line 59
    .local v13, x:I
    sub-int v15, v4, v6

    div-int/lit8 v14, v15, 0x2

    .line 61
    .local v14, y:I
    invoke-static {v5, v2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 62
    invoke-static {v8, v13, v14, v7, v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v9

    .line 80
    .end local v6           #minHeight:I
    .end local v7           #minWidth:I
    .end local v13           #x:I
    .end local v14           #y:I
    :goto_0
    if-eq v9, v8, :cond_3

    if-eqz v8, :cond_3

    .line 81
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->recycle()V

    .line 84
    :cond_3
    return-object v9

    .line 64
    :cond_4
    :try_start_1
    move-object/from16 v0, p0

    iget v11, v0, Lmiui/resourcebrowser/widget/AsyncImageAdapter$AsyncLoadImageTask;->mTargetWidth:I

    .line 65
    .local v11, scaledWidth:I
    move-object/from16 v0, p0

    iget v10, v0, Lmiui/resourcebrowser/widget/AsyncImageAdapter$AsyncLoadImageTask;->mTargetHeight:I

    .line 66
    .local v10, scaledHeight:I
    move-object/from16 v0, p0

    iget v15, v0, Lmiui/resourcebrowser/widget/AsyncImageAdapter$AsyncLoadImageTask;->mTargetWidth:I

    if-ge v15, v12, :cond_5

    move-object/from16 v0, p0

    iget v15, v0, Lmiui/resourcebrowser/widget/AsyncImageAdapter$AsyncLoadImageTask;->mTargetHeight:I

    if-lt v15, v4, :cond_6

    .line 67
    :cond_5
    move v11, v12

    .line 68
    move v10, v4

    .line 71
    :cond_6
    div-int v15, v12, v11

    div-int v16, v4, v10

    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->min(II)I

    move-result v15

    iput v15, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 72
    invoke-static {v5, v2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 73
    const/4 v15, 0x0

    invoke-static {v8, v11, v10, v15}, Lmiui/util/ImageUtils;->scaleBitmapToDesire(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v9

    goto :goto_0

    .line 76
    .end local v10           #scaledHeight:I
    .end local v11           #scaledWidth:I
    :catch_0
    move-exception v3

    .line 77
    .local v3, e:Ljava/lang/OutOfMemoryError;
    invoke-virtual {v3}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto :goto_0

    .line 75
    .end local v3           #e:Ljava/lang/OutOfMemoryError;
    :catch_1
    move-exception v15

    goto :goto_0
.end method

.method public setScaled(Z)V
    .locals 0
    .parameter "scaled"

    .prologue
    .line 30
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncImageAdapter$AsyncLoadImageTask;,"Lmiui/resourcebrowser/widget/AsyncImageAdapter<TT;>.AsyncLoadImageTask;"
    iput-boolean p1, p0, Lmiui/resourcebrowser/widget/AsyncImageAdapter$AsyncLoadImageTask;->mScaled:Z

    .line 31
    return-void
.end method

.method public setTargetSize(II)V
    .locals 0
    .parameter "targetWidth"
    .parameter "targetHeight"

    .prologue
    .line 25
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncImageAdapter$AsyncLoadImageTask;,"Lmiui/resourcebrowser/widget/AsyncImageAdapter<TT;>.AsyncLoadImageTask;"
    iput p1, p0, Lmiui/resourcebrowser/widget/AsyncImageAdapter$AsyncLoadImageTask;->mTargetWidth:I

    .line 26
    iput p2, p0, Lmiui/resourcebrowser/widget/AsyncImageAdapter$AsyncLoadImageTask;->mTargetHeight:I

    .line 27
    return-void
.end method
