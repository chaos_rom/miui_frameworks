.class public abstract Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadPartialDataTask;
.super Lmiui/os/DaemonAsyncTask;
.source "AsyncAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/resourcebrowser/widget/AsyncAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401
    name = "AsyncLoadPartialDataTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmiui/os/DaemonAsyncTask",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field mDoingJobs:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lmiui/resourcebrowser/widget/AsyncAdapter;


# direct methods
.method public constructor <init>(Lmiui/resourcebrowser/widget/AsyncAdapter;)V
    .locals 1
    .parameter

    .prologue
    .line 541
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadPartialDataTask;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>.AsyncLoadPartialDataTask;"
    iput-object p1, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadPartialDataTask;->this$0:Lmiui/resourcebrowser/widget/AsyncAdapter;

    invoke-direct {p0}, Lmiui/os/DaemonAsyncTask;-><init>()V

    .line 543
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadPartialDataTask;->mDoingJobs:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public isJobDoing(Ljava/lang/Object;)Z
    .locals 1
    .parameter "job"

    .prologue
    .line 546
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadPartialDataTask;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>.AsyncLoadPartialDataTask;"
    iget-object v0, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadPartialDataTask;->mDoingJobs:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected needsDoJob(Ljava/lang/Object;)Z
    .locals 2
    .parameter "job"

    .prologue
    .line 551
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadPartialDataTask;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>.AsyncLoadPartialDataTask;"
    invoke-static {}, Lmiui/resourcebrowser/widget/AsyncAdapter;->access$300()Lmiui/cache/DataCache;

    move-result-object v1

    invoke-virtual {v1, p1}, Lmiui/cache/DataCache;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadPartialDataTask;->isJobDoing(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, 0x1

    .line 552
    .local v0, needed:Z
    :goto_0
    if-eqz v0, :cond_0

    .line 553
    iget-object v1, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadPartialDataTask;->mDoingJobs:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 555
    :cond_0
    return v0

    .line 551
    .end local v0           #needed:Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected varargs onProgressUpdate([Landroid/util/Pair;)V
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadPartialDataTask;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>.AsyncLoadPartialDataTask;"
    .local p1, values:[Landroid/util/Pair;,"[Landroid/util/Pair<Ljava/lang/Object;Ljava/lang/Object;>;"
    const/4 v3, 0x0

    .line 560
    if-eqz p1, :cond_0

    array-length v2, p1

    if-nez v2, :cond_1

    .line 571
    :cond_0
    :goto_0
    return-void

    .line 563
    :cond_1
    aget-object v2, p1, v3

    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    .line 564
    .local v0, key:Ljava/lang/Object;
    aget-object v2, p1, v3

    iget-object v1, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    .line 565
    .local v1, value:Ljava/lang/Object;
    if-eqz v1, :cond_2

    .line 566
    invoke-static {}, Lmiui/resourcebrowser/widget/AsyncAdapter;->access$300()Lmiui/cache/DataCache;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lmiui/cache/DataCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 567
    iget-object v2, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadPartialDataTask;->this$0:Lmiui/resourcebrowser/widget/AsyncAdapter;

    invoke-virtual {v2}, Lmiui/resourcebrowser/widget/AsyncAdapter;->notifyDataSetChanged()V

    .line 569
    :cond_2
    iget-object v2, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadPartialDataTask;->mDoingJobs:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 570
    invoke-super {p0, p1}, Lmiui/os/DaemonAsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 541
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadPartialDataTask;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>.AsyncLoadPartialDataTask;"
    check-cast p1, [Landroid/util/Pair;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadPartialDataTask;->onProgressUpdate([Landroid/util/Pair;)V

    return-void
.end method
