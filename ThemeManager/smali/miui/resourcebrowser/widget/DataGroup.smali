.class public Lmiui/resourcebrowser/widget/DataGroup;
.super Ljava/util/ArrayList;
.source "DataGroup.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/ArrayList",
        "<TT;>;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private extraMeta:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private id:Ljava/lang/String;

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7
    .local p0, this:Lmiui/resourcebrowser/widget/DataGroup;,"Lmiui/resourcebrowser/widget/DataGroup<TT;>;"
    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    .line 15
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/widget/DataGroup;->extraMeta:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public getExtraMeta(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .parameter "key"

    .prologue
    .line 38
    .local p0, this:Lmiui/resourcebrowser/widget/DataGroup;,"Lmiui/resourcebrowser/widget/DataGroup<TT;>;"
    iget-object v0, p0, Lmiui/resourcebrowser/widget/DataGroup;->extraMeta:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18
    .local p0, this:Lmiui/resourcebrowser/widget/DataGroup;,"Lmiui/resourcebrowser/widget/DataGroup<TT;>;"
    iget-object v0, p0, Lmiui/resourcebrowser/widget/DataGroup;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    .local p0, this:Lmiui/resourcebrowser/widget/DataGroup;,"Lmiui/resourcebrowser/widget/DataGroup<TT;>;"
    iget-object v0, p0, Lmiui/resourcebrowser/widget/DataGroup;->title:Ljava/lang/String;

    return-object v0
.end method

.method public putExtraMeta(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1
    .parameter "key"
    .parameter "value"

    .prologue
    .line 34
    .local p0, this:Lmiui/resourcebrowser/widget/DataGroup;,"Lmiui/resourcebrowser/widget/DataGroup<TT;>;"
    iget-object v0, p0, Lmiui/resourcebrowser/widget/DataGroup;->extraMeta:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    return-void
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0
    .parameter "id"

    .prologue
    .line 22
    .local p0, this:Lmiui/resourcebrowser/widget/DataGroup;,"Lmiui/resourcebrowser/widget/DataGroup<TT;>;"
    iput-object p1, p0, Lmiui/resourcebrowser/widget/DataGroup;->id:Ljava/lang/String;

    .line 23
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .parameter "title"

    .prologue
    .line 30
    .local p0, this:Lmiui/resourcebrowser/widget/DataGroup;,"Lmiui/resourcebrowser/widget/DataGroup<TT;>;"
    iput-object p1, p0, Lmiui/resourcebrowser/widget/DataGroup;->title:Ljava/lang/String;

    .line 31
    return-void
.end method
