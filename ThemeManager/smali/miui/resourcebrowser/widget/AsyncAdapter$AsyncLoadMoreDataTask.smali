.class public abstract Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreDataTask;
.super Lmiui/os/UniqueAsyncTask;
.source "AsyncAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/resourcebrowser/widget/AsyncAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401
    name = "AsyncLoadMoreDataTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmiui/os/UniqueAsyncTask",
        "<",
        "Ljava/lang/Void;",
        "TT;",
        "Ljava/util/List",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field private mClearData:Z

.field private mGroup:I

.field private mLoadParams:Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreParams;

.field final synthetic this$0:Lmiui/resourcebrowser/widget/AsyncAdapter;


# direct methods
.method public constructor <init>(Lmiui/resourcebrowser/widget/AsyncAdapter;)V
    .locals 0
    .parameter

    .prologue
    .line 433
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreDataTask;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>.AsyncLoadMoreDataTask;"
    iput-object p1, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreDataTask;->this$0:Lmiui/resourcebrowser/widget/AsyncAdapter;

    invoke-direct {p0}, Lmiui/os/UniqueAsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"

    .prologue
    .line 433
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreDataTask;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>.AsyncLoadMoreDataTask;"
    check-cast p1, [Ljava/lang/Void;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreDataTask;->doInBackground([Ljava/lang/Void;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/util/List;
    .locals 5
    .parameter "params"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreDataTask;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>.AsyncLoadMoreDataTask;"
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 482
    iget-object v3, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreDataTask;->mLoadParams:Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreParams;

    if-nez v3, :cond_0

    .line 483
    const/4 v0, 0x0

    .line 492
    :goto_0
    return-object v0

    .line 485
    :cond_0
    const/4 v0, 0x0

    .line 486
    .local v0, dataSet:Ljava/util/List;,"Ljava/util/List<TT;>;"
    iget-object v3, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreDataTask;->mLoadParams:Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreParams;

    invoke-virtual {p0, v3}, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreDataTask;->loadMoreData(Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreParams;)Ljava/util/List;

    move-result-object v0

    .line 487
    iget-object v3, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreDataTask;->mLoadParams:Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreParams;

    iget-boolean v3, v3, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreParams;->upwards:Z

    if-eqz v3, :cond_3

    .line 488
    iget-object v3, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreDataTask;->this$0:Lmiui/resourcebrowser/widget/AsyncAdapter;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_2

    :cond_1
    move v1, v2

    :cond_2
    iput-boolean v1, v3, Lmiui/resourcebrowser/widget/AsyncAdapter;->mReachTop:Z

    goto :goto_0

    .line 490
    :cond_3
    iget-object v3, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreDataTask;->this$0:Lmiui/resourcebrowser/widget/AsyncAdapter;

    if-eqz v0, :cond_4

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_5

    :cond_4
    move v1, v2

    :cond_5
    iput-boolean v1, v3, Lmiui/resourcebrowser/widget/AsyncAdapter;->mReachBottom:Z

    goto :goto_0
.end method

.method protected hasEquivalentRunningTasks()Z
    .locals 1

    .prologue
    .line 461
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreDataTask;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>.AsyncLoadMoreDataTask;"
    iget-object v0, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreDataTask;->this$0:Lmiui/resourcebrowser/widget/AsyncAdapter;

    #getter for: Lmiui/resourcebrowser/widget/AsyncAdapter;->mTaskSet:Ljava/util/Set;
    invoke-static {v0}, Lmiui/resourcebrowser/widget/AsyncAdapter;->access$200(Lmiui/resourcebrowser/widget/AsyncAdapter;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected abstract loadMoreData(Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreParams;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreParams;",
            ")",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 433
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreDataTask;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>.AsyncLoadMoreDataTask;"
    check-cast p1, Ljava/util/List;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreDataTask;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 466
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreDataTask;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>.AsyncLoadMoreDataTask;"
    .local p1, result:Ljava/util/List;,"Ljava/util/List<TT;>;"
    iget-boolean v1, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreDataTask;->mClearData:Z

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    .line 467
    iget-object v1, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreDataTask;->this$0:Lmiui/resourcebrowser/widget/AsyncAdapter;

    iget v2, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreDataTask;->mGroup:I

    #calls: Lmiui/resourcebrowser/widget/AsyncAdapter;->getDataGroup(I)Lmiui/resourcebrowser/widget/DataGroup;
    invoke-static {v1, v2}, Lmiui/resourcebrowser/widget/AsyncAdapter;->access$000(Lmiui/resourcebrowser/widget/AsyncAdapter;I)Lmiui/resourcebrowser/widget/DataGroup;

    move-result-object v1

    invoke-virtual {v1}, Lmiui/resourcebrowser/widget/DataGroup;->clear()V

    .line 469
    :cond_0
    if-eqz p1, :cond_1

    .line 470
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 471
    iget-object v1, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreDataTask;->this$0:Lmiui/resourcebrowser/widget/AsyncAdapter;

    iget v2, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreDataTask;->mGroup:I

    #calls: Lmiui/resourcebrowser/widget/AsyncAdapter;->getDataGroup(I)Lmiui/resourcebrowser/widget/DataGroup;
    invoke-static {v1, v2}, Lmiui/resourcebrowser/widget/AsyncAdapter;->access$000(Lmiui/resourcebrowser/widget/AsyncAdapter;I)Lmiui/resourcebrowser/widget/DataGroup;

    move-result-object v1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmiui/resourcebrowser/widget/DataGroup;->add(Ljava/lang/Object;)Z

    .line 470
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 474
    .end local v0           #i:I
    :cond_1
    iget-object v1, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreDataTask;->this$0:Lmiui/resourcebrowser/widget/AsyncAdapter;

    invoke-virtual {v1}, Lmiui/resourcebrowser/widget/AsyncAdapter;->notifyDataSetChanged()V

    .line 475
    invoke-super {p0, p1}, Lmiui/os/UniqueAsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 476
    iget-object v1, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreDataTask;->this$0:Lmiui/resourcebrowser/widget/AsyncAdapter;

    #getter for: Lmiui/resourcebrowser/widget/AsyncAdapter;->mTaskSet:Ljava/util/Set;
    invoke-static {v1}, Lmiui/resourcebrowser/widget/AsyncAdapter;->access$200(Lmiui/resourcebrowser/widget/AsyncAdapter;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 477
    iget-object v1, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreDataTask;->this$0:Lmiui/resourcebrowser/widget/AsyncAdapter;

    invoke-virtual {v1, p1}, Lmiui/resourcebrowser/widget/AsyncAdapter;->postLoadMoreData(Ljava/util/List;)V

    .line 478
    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 455
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreDataTask;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>.AsyncLoadMoreDataTask;"
    invoke-super {p0}, Lmiui/os/UniqueAsyncTask;->onPreExecute()V

    .line 456
    iget-object v0, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreDataTask;->this$0:Lmiui/resourcebrowser/widget/AsyncAdapter;

    #getter for: Lmiui/resourcebrowser/widget/AsyncAdapter;->mTaskSet:Ljava/util/Set;
    invoke-static {v0}, Lmiui/resourcebrowser/widget/AsyncAdapter;->access$200(Lmiui/resourcebrowser/widget/AsyncAdapter;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 457
    return-void
.end method

.method public setGroup(I)V
    .locals 0
    .parameter "group"

    .prologue
    .line 442
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreDataTask;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>.AsyncLoadMoreDataTask;"
    iput p1, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreDataTask;->mGroup:I

    .line 443
    return-void
.end method

.method public setLoadParams(Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreParams;)V
    .locals 0
    .parameter "loadParams"

    .prologue
    .line 446
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreDataTask;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>.AsyncLoadMoreDataTask;"
    iput-object p1, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreDataTask;->mLoadParams:Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreParams;

    .line 447
    return-void
.end method
