.class public abstract Lmiui/resourcebrowser/widget/AsyncAdapter;
.super Landroid/widget/BaseAdapter;
.source "AsyncAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadPartialDataTask;,
        Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreDataTask;,
        Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreParams;,
        Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;,
        Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataVisitor;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/widget/BaseAdapter;"
    }
.end annotation


# static fields
.field private static sPartialDataCache:Lmiui/cache/DataCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lmiui/cache/DataCache",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAutoLoadDownwardsMore:Z

.field private mAutoLoadUpwardsMore:Z

.field private mBackgroundLoad:Z

.field private mDataPerLine:I

.field private mDataSet:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/widget/DataGroup",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field private mLoadPartialDataTask:Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadPartialDataTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lmiui/resourcebrowser/widget/AsyncAdapter",
            "<TT;>.Async",
            "LoadPartialDataTask;"
        }
    .end annotation
.end field

.field private mPreloadOffset:I

.field protected mReachBottom:Z

.field protected mReachTop:Z

.field private mTaskSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lmiui/os/UniqueAsyncTask",
            "<***>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 40
    new-instance v0, Lmiui/resourcebrowser/widget/AsyncAdapter$1;

    const/16 v1, 0x64

    invoke-direct {v0, v1}, Lmiui/resourcebrowser/widget/AsyncAdapter$1;-><init>(I)V

    sput-object v0, Lmiui/resourcebrowser/widget/AsyncAdapter;->sPartialDataCache:Lmiui/cache/DataCache;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>;"
    const/4 v1, 0x1

    .line 21
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/widget/AsyncAdapter;->mDataSet:Ljava/util/List;

    .line 30
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/widget/AsyncAdapter;->mTaskSet:Ljava/util/Set;

    .line 33
    iput-boolean v1, p0, Lmiui/resourcebrowser/widget/AsyncAdapter;->mBackgroundLoad:Z

    .line 61
    iput v1, p0, Lmiui/resourcebrowser/widget/AsyncAdapter;->mDataPerLine:I

    .line 541
    return-void
.end method

.method static synthetic access$000(Lmiui/resourcebrowser/widget/AsyncAdapter;I)Lmiui/resourcebrowser/widget/DataGroup;
    .locals 1
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lmiui/resourcebrowser/widget/AsyncAdapter;->getDataGroup(I)Lmiui/resourcebrowser/widget/DataGroup;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lmiui/resourcebrowser/widget/AsyncAdapter;)Z
    .locals 1
    .parameter "x0"

    .prologue
    .line 21
    iget-boolean v0, p0, Lmiui/resourcebrowser/widget/AsyncAdapter;->mBackgroundLoad:Z

    return v0
.end method

.method static synthetic access$200(Lmiui/resourcebrowser/widget/AsyncAdapter;)Ljava/util/Set;
    .locals 1
    .parameter "x0"

    .prologue
    .line 21
    iget-object v0, p0, Lmiui/resourcebrowser/widget/AsyncAdapter;->mTaskSet:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$300()Lmiui/cache/DataCache;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lmiui/resourcebrowser/widget/AsyncAdapter;->sPartialDataCache:Lmiui/cache/DataCache;

    return-object v0
.end method

.method private getDataGroup(I)Lmiui/resourcebrowser/widget/DataGroup;
    .locals 3
    .parameter "group"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lmiui/resourcebrowser/widget/DataGroup",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 102
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>;"
    iget-object v1, p0, Lmiui/resourcebrowser/widget/AsyncAdapter;->mDataSet:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-gt v1, p1, :cond_1

    .line 103
    monitor-enter p0

    .line 104
    :try_start_0
    iget-object v1, p0, Lmiui/resourcebrowser/widget/AsyncAdapter;->mDataSet:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-gt v1, p1, :cond_0

    .line 105
    iget-object v1, p0, Lmiui/resourcebrowser/widget/AsyncAdapter;->mDataSet:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    .local v0, i:I
    :goto_0
    if-gt v0, p1, :cond_0

    .line 106
    iget-object v1, p0, Lmiui/resourcebrowser/widget/AsyncAdapter;->mDataSet:Ljava/util/List;

    new-instance v2, Lmiui/resourcebrowser/widget/DataGroup;

    invoke-direct {v2}, Lmiui/resourcebrowser/widget/DataGroup;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 105
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 109
    .end local v0           #i:I
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 111
    :cond_1
    iget-object v1, p0, Lmiui/resourcebrowser/widget/AsyncAdapter;->mDataSet:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/resourcebrowser/widget/DataGroup;

    return-object v1

    .line 109
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private loadData(ILmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;)V
    .locals 2
    .parameter "group"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lmiui/resourcebrowser/widget/AsyncAdapter",
            "<TT;>.Async",
            "LoadDataTask;",
            ")V"
        }
    .end annotation

    .prologue
    .line 213
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>;"
    .local p2, task:Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>.AsyncLoadDataTask;"
    if-nez p2, :cond_0

    .line 223
    :goto_0
    return-void

    .line 216
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "loadData-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->setId(Ljava/lang/String;)V

    .line 217
    invoke-virtual {p2, p1}, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->setGroup(I)V

    .line 219
    const/4 v0, 0x0

    :try_start_0
    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {p2, v0}, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 220
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private loadMoreData(ZILmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreDataTask;)V
    .locals 3
    .parameter "upwards"
    .parameter "group"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZI",
            "Lmiui/resourcebrowser/widget/AsyncAdapter",
            "<TT;>.Async",
            "LoadMoreDataTask;",
            ")V"
        }
    .end annotation

    .prologue
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>;"
    .local p3, task:Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreDataTask;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>.AsyncLoadMoreDataTask;"
    const/4 v2, 0x0

    .line 401
    if-nez p3, :cond_0

    .line 419
    :goto_0
    return-void

    .line 404
    :cond_0
    new-instance v0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreParams;

    invoke-direct {v0}, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreParams;-><init>()V

    .line 405
    .local v0, params:Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreParams;
    iput-boolean p1, v0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreParams;->upwards:Z

    .line 406
    if-nez p1, :cond_1

    invoke-virtual {p0, p2}, Lmiui/resourcebrowser/widget/AsyncAdapter;->getDataCount(I)I

    move-result v1

    if-nez v1, :cond_2

    .line 407
    :cond_1
    iput v2, v0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreParams;->cursor:I

    .line 411
    :goto_1
    invoke-virtual {p3, v0}, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreDataTask;->setLoadParams(Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreParams;)V

    .line 412
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "loadMoreData-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreDataTask;->setId(Ljava/lang/String;)V

    .line 413
    invoke-virtual {p3, p2}, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreDataTask;->setGroup(I)V

    .line 415
    const/4 v1, 0x0

    :try_start_0
    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {p3, v1}, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreDataTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 416
    :catch_0
    move-exception v1

    goto :goto_0

    .line 409
    :cond_2
    invoke-virtual {p0, p2}, Lmiui/resourcebrowser/widget/AsyncAdapter;->getDataCount(I)I

    move-result v1

    iput v1, v0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreParams;->cursor:I

    goto :goto_1
.end method


# virtual methods
.method protected abstract bindContentView(Landroid/view/View;Ljava/util/List;III)Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/List",
            "<TT;>;III)",
            "Landroid/view/View;"
        }
    .end annotation
.end method

.method protected abstract bindPartialContentView(Landroid/view/View;Ljava/lang/Object;ILjava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "TT;I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation
.end method

.method public clean()V
    .locals 1

    .prologue
    .line 576
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>;"
    iget-object v0, p0, Lmiui/resourcebrowser/widget/AsyncAdapter;->mLoadPartialDataTask:Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadPartialDataTask;

    if-eqz v0, :cond_0

    .line 577
    iget-object v0, p0, Lmiui/resourcebrowser/widget/AsyncAdapter;->mLoadPartialDataTask:Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadPartialDataTask;

    invoke-virtual {v0}, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadPartialDataTask;->stop()V

    .line 579
    :cond_0
    return-void
.end method

.method public clearDataSet()V
    .locals 1

    .prologue
    .line 177
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>;"
    iget-object v0, p0, Lmiui/resourcebrowser/widget/AsyncAdapter;->mDataSet:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 178
    invoke-virtual {p0}, Lmiui/resourcebrowser/widget/AsyncAdapter;->notifyDataSetInvalidated()V

    .line 179
    return-void
.end method

.method protected abstract getCacheKeys(Ljava/lang/Object;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method public getCount()I
    .locals 3

    .prologue
    .line 65
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>;"
    const/4 v1, 0x0

    .line 66
    .local v1, total:I
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    iget-object v2, p0, Lmiui/resourcebrowser/widget/AsyncAdapter;->mDataSet:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 67
    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/widget/AsyncAdapter;->getCount(I)I

    move-result v2

    add-int/2addr v1, v2

    .line 66
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 69
    :cond_0
    return v1
.end method

.method protected getCount(I)I
    .locals 3
    .parameter "group"

    .prologue
    .line 84
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>;"
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/widget/AsyncAdapter;->getDataCount(I)I

    move-result v0

    .line 85
    .local v0, total:I
    if-nez v0, :cond_0

    .line 86
    const/4 v1, 0x0

    .line 88
    :goto_0
    return v1

    :cond_0
    add-int/lit8 v1, v0, -0x1

    iget v2, p0, Lmiui/resourcebrowser/widget/AsyncAdapter;->mDataPerLine:I

    div-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getDataCount(I)I
    .locals 1
    .parameter "group"

    .prologue
    .line 128
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>;"
    invoke-direct {p0, p1}, Lmiui/resourcebrowser/widget/AsyncAdapter;->getDataGroup(I)Lmiui/resourcebrowser/widget/DataGroup;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/resourcebrowser/widget/DataGroup;->size()I

    move-result v0

    return v0
.end method

.method public getDataItem(II)Ljava/lang/Object;
    .locals 1
    .parameter "index"
    .parameter "group"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)TT;"
        }
    .end annotation

    .prologue
    .line 132
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>;"
    invoke-direct {p0, p2}, Lmiui/resourcebrowser/widget/AsyncAdapter;->getDataGroup(I)Lmiui/resourcebrowser/widget/DataGroup;

    move-result-object v0

    invoke-virtual {v0, p1}, Lmiui/resourcebrowser/widget/DataGroup;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getDataPerLine()I
    .locals 1

    .prologue
    .line 140
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>;"
    iget v0, p0, Lmiui/resourcebrowser/widget/AsyncAdapter;->mDataPerLine:I

    return v0
.end method

.method public getDataSet()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/widget/DataGroup",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 173
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>;"
    iget-object v0, p0, Lmiui/resourcebrowser/widget/AsyncAdapter;->mDataSet:Ljava/util/List;

    return-object v0
.end method

.method public getGroupPosition(I)Landroid/util/Pair;
    .locals 7
    .parameter "position"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 115
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>;"
    const/4 v3, 0x0

    .line 116
    .local v3, total:I
    const/4 v2, 0x0

    .local v2, i:I
    :goto_0
    iget-object v4, p0, Lmiui/resourcebrowser/widget/AsyncAdapter;->mDataSet:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v2, v4, :cond_1

    .line 117
    invoke-virtual {p0, v2}, Lmiui/resourcebrowser/widget/AsyncAdapter;->getCount(I)I

    move-result v0

    .line 118
    .local v0, count:I
    add-int v4, v3, v0

    if-ge p1, v4, :cond_0

    .line 119
    sub-int v1, p1, v3

    .line 120
    .local v1, groupPos:I
    new-instance v4, Landroid/util/Pair;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 124
    .end local v0           #count:I
    .end local v1           #groupPos:I
    :goto_1
    return-object v4

    .line 122
    .restart local v0       #count:I
    :cond_0
    add-int/2addr v3, v0

    .line 116
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 124
    .end local v0           #count:I
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .parameter "x0"

    .prologue
    .line 21
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>;"
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/widget/AsyncAdapter;->getItem(I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getItem(I)Ljava/util/List;
    .locals 3
    .parameter "position"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 74
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>;"
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/widget/AsyncAdapter;->getGroupPosition(I)Landroid/util/Pair;

    move-result-object v0

    .line 75
    .local v0, groupPosition:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v2, v1}, Lmiui/resourcebrowser/widget/AsyncAdapter;->getItem(II)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method protected getItem(II)Ljava/util/List;
    .locals 6
    .parameter "groupPos"
    .parameter "group"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 92
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>;"
    iget v4, p0, Lmiui/resourcebrowser/widget/AsyncAdapter;->mDataPerLine:I

    mul-int v2, p1, v4

    .line 93
    .local v2, index:I
    iget v4, p0, Lmiui/resourcebrowser/widget/AsyncAdapter;->mDataPerLine:I

    invoke-virtual {p0, p2}, Lmiui/resourcebrowser/widget/AsyncAdapter;->getDataCount(I)I

    move-result v5

    sub-int/2addr v5, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 94
    .local v3, length:I
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 95
    .local v0, data:Ljava/util/List;,"Ljava/util/List<TT;>;"
    const/4 v1, 0x0

    .local v1, i:I
    :goto_0
    if-ge v1, v3, :cond_0

    .line 96
    add-int v4, v2, v1

    invoke-virtual {p0, v4, p2}, Lmiui/resourcebrowser/widget/AsyncAdapter;->getDataItem(II)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 95
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 98
    :cond_0
    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .parameter "position"

    .prologue
    .line 80
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>;"
    int-to-long v0, p1

    return-wide v0
.end method

.method protected getLoadDataTask()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/widget/AsyncAdapter",
            "<TT;>.Async",
            "LoadDataTask;",
            ">;"
        }
    .end annotation

    .prologue
    .line 244
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>;"
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getLoadMoreDataTask()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/widget/AsyncAdapter",
            "<TT;>.Async",
            "LoadMoreDataTask;",
            ">;"
        }
    .end annotation

    .prologue
    .line 430
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>;"
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getLoadPartialDataTask()Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadPartialDataTask;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lmiui/resourcebrowser/widget/AsyncAdapter",
            "<TT;>.Async",
            "LoadPartialDataTask;"
        }
    .end annotation

    .prologue
    .line 538
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>;"
    const/4 v0, 0x0

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    .line 145
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>;"
    invoke-virtual {p0}, Lmiui/resourcebrowser/widget/AsyncAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 146
    const/4 v0, 0x0

    .line 164
    :goto_0
    return-object v0

    .line 149
    :cond_0
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/widget/AsyncAdapter;->getGroupPosition(I)Landroid/util/Pair;

    move-result-object v6

    .line 150
    .local v6, groupPosition:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    iget-object v0, v6, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v0, v6, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lmiui/resourcebrowser/widget/AsyncAdapter;->getItem(II)Ljava/util/List;

    move-result-object v2

    .line 152
    .local v2, bindData:Ljava/util/List;,"Ljava/util/List<TT;>;"
    iget-object v0, v6, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget-object v0, v6, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    move-object v0, p0

    move-object v1, p2

    move v3, p1

    invoke-virtual/range {v0 .. v5}, Lmiui/resourcebrowser/widget/AsyncAdapter;->bindContentView(Landroid/view/View;Ljava/util/List;III)Landroid/view/View;

    move-result-object p2

    .line 154
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v7, v0, -0x1

    .local v7, i:I
    :goto_1
    if-ltz v7, :cond_1

    .line 155
    invoke-interface {v2, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, p2, v0, v7}, Lmiui/resourcebrowser/widget/AsyncAdapter;->loadPartialData(Landroid/view/View;Ljava/lang/Object;I)V

    .line 154
    add-int/lit8 v7, v7, -0x1

    goto :goto_1

    .line 158
    :cond_1
    iget-boolean v0, p0, Lmiui/resourcebrowser/widget/AsyncAdapter;->mReachTop:Z

    if-nez v0, :cond_3

    iget v0, p0, Lmiui/resourcebrowser/widget/AsyncAdapter;->mPreloadOffset:I

    if-ne p1, v0, :cond_3

    iget-boolean v0, p0, Lmiui/resourcebrowser/widget/AsyncAdapter;->mAutoLoadUpwardsMore:Z

    if-eqz v0, :cond_3

    .line 159
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/widget/AsyncAdapter;->loadMoreData(Z)V

    :cond_2
    :goto_2
    move-object v0, p2

    .line 164
    goto :goto_0

    .line 160
    :cond_3
    iget-boolean v0, p0, Lmiui/resourcebrowser/widget/AsyncAdapter;->mReachBottom:Z

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lmiui/resourcebrowser/widget/AsyncAdapter;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Lmiui/resourcebrowser/widget/AsyncAdapter;->mPreloadOffset:I

    sub-int/2addr v0, v1

    if-ne p1, v0, :cond_2

    iget-boolean v0, p0, Lmiui/resourcebrowser/widget/AsyncAdapter;->mAutoLoadDownwardsMore:Z

    if-eqz v0, :cond_2

    .line 161
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/widget/AsyncAdapter;->loadMoreData(Z)V

    goto :goto_2
.end method

.method protected isValidKey(Ljava/lang/Object;Ljava/lang/Object;I)Z
    .locals 1
    .parameter "key"
    .parameter
    .parameter "index"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "TT;I)Z"
        }
    .end annotation

    .prologue
    .line 532
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>;"
    .local p2, data:Ljava/lang/Object;,"TT;"
    sget-object v0, Lmiui/resourcebrowser/widget/AsyncAdapter;->sPartialDataCache:Lmiui/cache/DataCache;

    invoke-virtual {v0, p1}, Lmiui/cache/DataCache;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public loadData()V
    .locals 3

    .prologue
    .line 195
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>;"
    invoke-virtual {p0}, Lmiui/resourcebrowser/widget/AsyncAdapter;->getLoadDataTask()Ljava/util/List;

    move-result-object v1

    .line 196
    .local v1, tasks:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>.AsyncLoadDataTask;>;"
    if-nez v1, :cond_1

    .line 202
    :cond_0
    return-void

    .line 199
    :cond_1
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 200
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;

    invoke-direct {p0, v0, v2}, Lmiui/resourcebrowser/widget/AsyncAdapter;->loadData(ILmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;)V

    .line 199
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public loadData(I)V
    .locals 2
    .parameter "group"

    .prologue
    .line 205
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>;"
    invoke-virtual {p0}, Lmiui/resourcebrowser/widget/AsyncAdapter;->getLoadDataTask()Ljava/util/List;

    move-result-object v0

    .line 206
    .local v0, tasks:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>.AsyncLoadDataTask;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-gt v1, p1, :cond_1

    .line 210
    :cond_0
    :goto_0
    return-void

    .line 209
    :cond_1
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;

    invoke-direct {p0, p1, v1}, Lmiui/resourcebrowser/widget/AsyncAdapter;->loadData(ILmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;)V

    goto :goto_0
.end method

.method public loadMoreData(Z)V
    .locals 3
    .parameter "upwards"

    .prologue
    .line 383
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>;"
    invoke-virtual {p0}, Lmiui/resourcebrowser/widget/AsyncAdapter;->getLoadMoreDataTask()Ljava/util/List;

    move-result-object v1

    .line 384
    .local v1, tasks:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>.AsyncLoadMoreDataTask;>;"
    if-nez v1, :cond_1

    .line 390
    :cond_0
    return-void

    .line 387
    :cond_1
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 388
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreDataTask;

    invoke-direct {p0, p1, v0, v2}, Lmiui/resourcebrowser/widget/AsyncAdapter;->loadMoreData(ZILmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreDataTask;)V

    .line 387
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public loadMoreData(ZI)V
    .locals 2
    .parameter "upwards"
    .parameter "group"

    .prologue
    .line 393
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>;"
    invoke-virtual {p0}, Lmiui/resourcebrowser/widget/AsyncAdapter;->getLoadMoreDataTask()Ljava/util/List;

    move-result-object v0

    .line 394
    .local v0, tasks:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>.AsyncLoadMoreDataTask;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-gt v1, p2, :cond_1

    .line 398
    :cond_0
    :goto_0
    return-void

    .line 397
    :cond_1
    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreDataTask;

    invoke-direct {p0, p1, p2, v1}, Lmiui/resourcebrowser/widget/AsyncAdapter;->loadMoreData(ZILmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadMoreDataTask;)V

    goto :goto_0
.end method

.method protected loadPartialData(Landroid/view/View;Ljava/lang/Object;I)V
    .locals 6
    .parameter "view"
    .parameter
    .parameter "offset"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "TT;I)V"
        }
    .end annotation

    .prologue
    .line 500
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>;"
    .local p2, data:Ljava/lang/Object;,"TT;"
    iget-object v4, p0, Lmiui/resourcebrowser/widget/AsyncAdapter;->mLoadPartialDataTask:Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadPartialDataTask;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lmiui/resourcebrowser/widget/AsyncAdapter;->mLoadPartialDataTask:Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadPartialDataTask;

    invoke-virtual {v4}, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadPartialDataTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v4

    sget-object v5, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-ne v4, v5, :cond_1

    .line 501
    :cond_0
    invoke-virtual {p0}, Lmiui/resourcebrowser/widget/AsyncAdapter;->getLoadPartialDataTask()Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadPartialDataTask;

    move-result-object v4

    iput-object v4, p0, Lmiui/resourcebrowser/widget/AsyncAdapter;->mLoadPartialDataTask:Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadPartialDataTask;

    .line 502
    iget-object v4, p0, Lmiui/resourcebrowser/widget/AsyncAdapter;->mLoadPartialDataTask:Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadPartialDataTask;

    if-nez v4, :cond_1

    .line 524
    :goto_0
    return-void

    .line 506
    :cond_1
    iget-object v4, p0, Lmiui/resourcebrowser/widget/AsyncAdapter;->mLoadPartialDataTask:Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadPartialDataTask;

    invoke-virtual {v4}, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadPartialDataTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v4

    sget-object v5, Landroid/os/AsyncTask$Status;->PENDING:Landroid/os/AsyncTask$Status;

    if-ne v4, v5, :cond_2

    .line 508
    :try_start_0
    iget-object v4, p0, Lmiui/resourcebrowser/widget/AsyncAdapter;->mLoadPartialDataTask:Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadPartialDataTask;

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Void;

    invoke-virtual {v4, v5}, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadPartialDataTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 513
    :cond_2
    :goto_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 514
    .local v3, partialData:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Object;>;"
    invoke-virtual {p0, p2}, Lmiui/resourcebrowser/widget/AsyncAdapter;->getCacheKeys(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 515
    .local v2, keys:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Object;>;"
    const/4 v0, 0x0

    .local v0, i:I
    :goto_2
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_5

    .line 516
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 517
    .local v1, key:Ljava/lang/Object;
    invoke-virtual {p0, v1, p2, v0}, Lmiui/resourcebrowser/widget/AsyncAdapter;->isValidKey(Ljava/lang/Object;Ljava/lang/Object;I)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 518
    sget-object v4, Lmiui/resourcebrowser/widget/AsyncAdapter;->sPartialDataCache:Lmiui/cache/DataCache;

    invoke-virtual {v4, v1}, Lmiui/cache/DataCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 515
    :cond_3
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 519
    :cond_4
    iget-object v4, p0, Lmiui/resourcebrowser/widget/AsyncAdapter;->mLoadPartialDataTask:Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadPartialDataTask;

    invoke-virtual {v4, v1}, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadPartialDataTask;->isJobDoing(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 520
    iget-object v4, p0, Lmiui/resourcebrowser/widget/AsyncAdapter;->mLoadPartialDataTask:Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadPartialDataTask;

    invoke-virtual {v4, v1}, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadPartialDataTask;->addJob(Ljava/lang/Object;)V

    goto :goto_3

    .line 523
    .end local v1           #key:Ljava/lang/Object;
    :cond_5
    invoke-virtual {p0, p1, p2, p3, v3}, Lmiui/resourcebrowser/widget/AsyncAdapter;->bindPartialContentView(Landroid/view/View;Ljava/lang/Object;ILjava/util/List;)V

    goto :goto_0

    .line 509
    .end local v0           #i:I
    .end local v2           #keys:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Object;>;"
    .end local v3           #partialData:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Object;>;"
    :catch_0
    move-exception v4

    goto :goto_1
.end method

.method public loadingData()Z
    .locals 1

    .prologue
    .line 226
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>;"
    iget-object v0, p0, Lmiui/resourcebrowser/widget/AsyncAdapter;->mTaskSet:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected postLoadData(Ljava/util/List;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 230
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>;"
    .local p1, result:Ljava/util/List;,"Ljava/util/List<TT;>;"
    return-void
.end method

.method protected postLoadMoreData(Ljava/util/List;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 422
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>;"
    .local p1, result:Ljava/util/List;,"Ljava/util/List<TT;>;"
    return-void
.end method

.method public setAutoLoadMoreStyle(I)V
    .locals 3
    .parameter "autoLoadMoreStyle"

    .prologue
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>;"
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 378
    and-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lmiui/resourcebrowser/widget/AsyncAdapter;->mAutoLoadUpwardsMore:Z

    .line 379
    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lmiui/resourcebrowser/widget/AsyncAdapter;->mAutoLoadDownwardsMore:Z

    .line 380
    return-void

    :cond_0
    move v0, v2

    .line 378
    goto :goto_0

    :cond_1
    move v1, v2

    .line 379
    goto :goto_1
.end method

.method public setDataPerLine(I)V
    .locals 0
    .parameter "dataPerLine"

    .prologue
    .line 136
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>;"
    iput p1, p0, Lmiui/resourcebrowser/widget/AsyncAdapter;->mDataPerLine:I

    .line 137
    return-void
.end method

.method public setPreloadOffset(I)V
    .locals 0
    .parameter "preloadOffset"

    .prologue
    .line 187
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>;"
    iput p1, p0, Lmiui/resourcebrowser/widget/AsyncAdapter;->mPreloadOffset:I

    .line 188
    return-void
.end method
