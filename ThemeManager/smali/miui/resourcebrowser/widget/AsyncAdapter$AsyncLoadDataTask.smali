.class public abstract Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;
.super Lmiui/os/UniqueAsyncTask;
.source "AsyncAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/resourcebrowser/widget/AsyncAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401
    name = "AsyncLoadDataTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmiui/os/UniqueAsyncTask",
        "<",
        "Ljava/lang/Void;",
        "TT;",
        "Ljava/util/List",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field private mFirstTimeLoad:Z

.field private mGroup:I

.field private mResultDataSet:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end field

.field private mTempDataSet:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end field

.field private mVisitors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataVisitor",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lmiui/resourcebrowser/widget/AsyncAdapter;


# direct methods
.method public constructor <init>(Lmiui/resourcebrowser/widget/AsyncAdapter;)V
    .locals 1
    .parameter

    .prologue
    .line 247
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>.AsyncLoadDataTask;"
    iput-object p1, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->this$0:Lmiui/resourcebrowser/widget/AsyncAdapter;

    invoke-direct {p0}, Lmiui/os/UniqueAsyncTask;-><init>()V

    .line 255
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->mFirstTimeLoad:Z

    .line 257
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->mResultDataSet:Ljava/util/List;

    .line 259
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->mTempDataSet:Ljava/util/List;

    .line 261
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->mVisitors:Ljava/util/List;

    return-void
.end method

.method private needsExecuteTask()Z
    .locals 4

    .prologue
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>.AsyncLoadDataTask;"
    const/4 v2, 0x1

    .line 272
    invoke-virtual {p0}, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->getMode()I

    move-result v1

    const/4 v3, 0x2

    if-eq v1, v3, :cond_0

    move v1, v2

    .line 280
    :goto_0
    return v1

    .line 275
    :cond_0
    const/4 v0, 0x0

    .local v0, i:I
    :goto_1
    iget-object v1, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->mVisitors:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 276
    iget-object v1, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->mVisitors:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataVisitor;

    invoke-interface {v1}, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataVisitor;->dataChanged()Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v2

    .line 277
    goto :goto_0

    .line 275
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 280
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"

    .prologue
    .line 247
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>.AsyncLoadDataTask;"
    check-cast p1, [Ljava/lang/Void;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->doInBackground([Ljava/lang/Void;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/util/List;
    .locals 5
    .parameter "params"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 333
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>.AsyncLoadDataTask;"
    invoke-virtual {p0}, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->getMode()I

    move-result v2

    .line 334
    .local v2, mode:I
    const/4 v4, 0x2

    if-ne v2, v4, :cond_0

    .line 335
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    iget-object v4, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->mVisitors:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_2

    .line 336
    iget-object v4, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->mVisitors:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataVisitor;

    invoke-interface {v4, p0}, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataVisitor;->loadData(Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;)V

    .line 335
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 338
    .end local v0           #i:I
    :cond_0
    const/4 v4, 0x1

    if-ne v2, v4, :cond_1

    .line 339
    const/4 v1, 0x0

    .line 341
    .local v1, index:I
    :goto_1
    invoke-virtual {p0, v1}, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->loadData(I)[Ljava/lang/Object;

    move-result-object v3

    .line 342
    .local v3, partialDataSet:[Ljava/lang/Object;,"[TT;"
    if-eqz v3, :cond_2

    .line 343
    iget-object v4, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->mResultDataSet:Ljava/util/List;

    invoke-static {v4, v3}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 344
    invoke-virtual {p0, v3}, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->publishProgress([Ljava/lang/Object;)V

    .line 345
    array-length v4, v3

    add-int/2addr v1, v4

    .line 349
    goto :goto_1

    .line 351
    .end local v1           #index:I
    .end local v3           #partialDataSet:[Ljava/lang/Object;,"[TT;"
    :cond_1
    invoke-virtual {p0}, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->loadData()[Ljava/lang/Object;

    move-result-object v3

    .line 352
    .restart local v3       #partialDataSet:[Ljava/lang/Object;,"[TT;"
    if-eqz v3, :cond_2

    .line 353
    iget-object v4, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->mResultDataSet:Ljava/util/List;

    invoke-static {v4, v3}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 354
    invoke-virtual {p0, v3}, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->publishProgress([Ljava/lang/Object;)V

    .line 357
    .end local v3           #partialDataSet:[Ljava/lang/Object;,"[TT;"
    :cond_2
    iget-object v4, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->mResultDataSet:Ljava/util/List;

    return-object v4
.end method

.method protected abstract getMode()I
.end method

.method protected hasEquivalentRunningTasks()Z
    .locals 1

    .prologue
    .line 301
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>.AsyncLoadDataTask;"
    iget-object v0, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->this$0:Lmiui/resourcebrowser/widget/AsyncAdapter;

    #getter for: Lmiui/resourcebrowser/widget/AsyncAdapter;->mTaskSet:Ljava/util/Set;
    invoke-static {v0}, Lmiui/resourcebrowser/widget/AsyncAdapter;->access$200(Lmiui/resourcebrowser/widget/AsyncAdapter;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected loadData()[Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()[TT;"
        }
    .end annotation

    .prologue
    .line 363
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>.AsyncLoadDataTask;"
    const/4 v0, 0x0

    return-object v0
.end method

.method protected loadData(I)[Ljava/lang/Object;
    .locals 1
    .parameter "index"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)[TT;"
        }
    .end annotation

    .prologue
    .line 367
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>.AsyncLoadDataTask;"
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 247
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>.AsyncLoadDataTask;"
    check-cast p1, Ljava/util/List;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 321
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>.AsyncLoadDataTask;"
    .local p1, result:Ljava/util/List;,"Ljava/util/List<TT;>;"
    iget-object v0, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->this$0:Lmiui/resourcebrowser/widget/AsyncAdapter;

    #getter for: Lmiui/resourcebrowser/widget/AsyncAdapter;->mBackgroundLoad:Z
    invoke-static {v0}, Lmiui/resourcebrowser/widget/AsyncAdapter;->access$100(Lmiui/resourcebrowser/widget/AsyncAdapter;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->mFirstTimeLoad:Z

    if-nez v0, :cond_0

    .line 322
    iget-object v0, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->this$0:Lmiui/resourcebrowser/widget/AsyncAdapter;

    iget v1, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->mGroup:I

    #calls: Lmiui/resourcebrowser/widget/AsyncAdapter;->getDataGroup(I)Lmiui/resourcebrowser/widget/DataGroup;
    invoke-static {v0, v1}, Lmiui/resourcebrowser/widget/AsyncAdapter;->access$000(Lmiui/resourcebrowser/widget/AsyncAdapter;I)Lmiui/resourcebrowser/widget/DataGroup;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/resourcebrowser/widget/DataGroup;->clear()V

    .line 323
    iget-object v0, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->this$0:Lmiui/resourcebrowser/widget/AsyncAdapter;

    iget v1, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->mGroup:I

    #calls: Lmiui/resourcebrowser/widget/AsyncAdapter;->getDataGroup(I)Lmiui/resourcebrowser/widget/DataGroup;
    invoke-static {v0, v1}, Lmiui/resourcebrowser/widget/AsyncAdapter;->access$000(Lmiui/resourcebrowser/widget/AsyncAdapter;I)Lmiui/resourcebrowser/widget/DataGroup;

    move-result-object v0

    iget-object v1, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->mTempDataSet:Ljava/util/List;

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/widget/DataGroup;->addAll(Ljava/util/Collection;)Z

    .line 324
    iget-object v0, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->this$0:Lmiui/resourcebrowser/widget/AsyncAdapter;

    invoke-virtual {v0}, Lmiui/resourcebrowser/widget/AsyncAdapter;->notifyDataSetChanged()V

    .line 326
    :cond_0
    invoke-super {p0, p1}, Lmiui/os/UniqueAsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 327
    iget-object v0, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->this$0:Lmiui/resourcebrowser/widget/AsyncAdapter;

    #getter for: Lmiui/resourcebrowser/widget/AsyncAdapter;->mTaskSet:Ljava/util/Set;
    invoke-static {v0}, Lmiui/resourcebrowser/widget/AsyncAdapter;->access$200(Lmiui/resourcebrowser/widget/AsyncAdapter;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 328
    iget-object v0, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->this$0:Lmiui/resourcebrowser/widget/AsyncAdapter;

    invoke-virtual {v0, p1}, Lmiui/resourcebrowser/widget/AsyncAdapter;->postLoadData(Ljava/util/List;)V

    .line 329
    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>.AsyncLoadDataTask;"
    const/4 v0, 0x0

    .line 285
    invoke-super {p0}, Lmiui/os/UniqueAsyncTask;->onPreExecute()V

    .line 286
    invoke-direct {p0}, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->needsExecuteTask()Z

    move-result v1

    if-nez v1, :cond_0

    .line 287
    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->cancel(Z)Z

    .line 297
    :goto_0
    return-void

    .line 290
    :cond_0
    iget-object v1, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->this$0:Lmiui/resourcebrowser/widget/AsyncAdapter;

    iget v2, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->mGroup:I

    #calls: Lmiui/resourcebrowser/widget/AsyncAdapter;->getDataGroup(I)Lmiui/resourcebrowser/widget/DataGroup;
    invoke-static {v1, v2}, Lmiui/resourcebrowser/widget/AsyncAdapter;->access$000(Lmiui/resourcebrowser/widget/AsyncAdapter;I)Lmiui/resourcebrowser/widget/DataGroup;

    move-result-object v1

    invoke-virtual {v1}, Lmiui/resourcebrowser/widget/DataGroup;->size()I

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    iput-boolean v0, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->mFirstTimeLoad:Z

    .line 291
    iget-object v0, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->this$0:Lmiui/resourcebrowser/widget/AsyncAdapter;

    #getter for: Lmiui/resourcebrowser/widget/AsyncAdapter;->mBackgroundLoad:Z
    invoke-static {v0}, Lmiui/resourcebrowser/widget/AsyncAdapter;->access$100(Lmiui/resourcebrowser/widget/AsyncAdapter;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->mFirstTimeLoad:Z

    if-eqz v0, :cond_3

    .line 292
    :cond_2
    iget-object v0, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->this$0:Lmiui/resourcebrowser/widget/AsyncAdapter;

    iget v1, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->mGroup:I

    #calls: Lmiui/resourcebrowser/widget/AsyncAdapter;->getDataGroup(I)Lmiui/resourcebrowser/widget/DataGroup;
    invoke-static {v0, v1}, Lmiui/resourcebrowser/widget/AsyncAdapter;->access$000(Lmiui/resourcebrowser/widget/AsyncAdapter;I)Lmiui/resourcebrowser/widget/DataGroup;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/resourcebrowser/widget/DataGroup;->clear()V

    .line 296
    :goto_1
    iget-object v0, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->this$0:Lmiui/resourcebrowser/widget/AsyncAdapter;

    #getter for: Lmiui/resourcebrowser/widget/AsyncAdapter;->mTaskSet:Ljava/util/Set;
    invoke-static {v0}, Lmiui/resourcebrowser/widget/AsyncAdapter;->access$200(Lmiui/resourcebrowser/widget/AsyncAdapter;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 294
    :cond_3
    iget-object v0, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->mTempDataSet:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto :goto_1
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Object;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TT;)V"
        }
    .end annotation

    .prologue
    .line 306
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>.AsyncLoadDataTask;"
    .local p1, values:[Ljava/lang/Object;,"[TT;"
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_2

    .line 307
    iget-object v1, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->this$0:Lmiui/resourcebrowser/widget/AsyncAdapter;

    #getter for: Lmiui/resourcebrowser/widget/AsyncAdapter;->mBackgroundLoad:Z
    invoke-static {v1}, Lmiui/resourcebrowser/widget/AsyncAdapter;->access$100(Lmiui/resourcebrowser/widget/AsyncAdapter;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->mFirstTimeLoad:Z

    if-eqz v1, :cond_1

    .line 308
    :cond_0
    iget-object v1, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->this$0:Lmiui/resourcebrowser/widget/AsyncAdapter;

    iget v2, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->mGroup:I

    #calls: Lmiui/resourcebrowser/widget/AsyncAdapter;->getDataGroup(I)Lmiui/resourcebrowser/widget/DataGroup;
    invoke-static {v1, v2}, Lmiui/resourcebrowser/widget/AsyncAdapter;->access$000(Lmiui/resourcebrowser/widget/AsyncAdapter;I)Lmiui/resourcebrowser/widget/DataGroup;

    move-result-object v1

    aget-object v2, p1, v0

    invoke-virtual {v1, v2}, Lmiui/resourcebrowser/widget/DataGroup;->add(Ljava/lang/Object;)Z

    .line 306
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 310
    :cond_1
    iget-object v1, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->mTempDataSet:Ljava/util/List;

    aget-object v2, p1, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 313
    :cond_2
    iget-object v1, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->this$0:Lmiui/resourcebrowser/widget/AsyncAdapter;

    #getter for: Lmiui/resourcebrowser/widget/AsyncAdapter;->mBackgroundLoad:Z
    invoke-static {v1}, Lmiui/resourcebrowser/widget/AsyncAdapter;->access$100(Lmiui/resourcebrowser/widget/AsyncAdapter;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->mFirstTimeLoad:Z

    if-eqz v1, :cond_4

    .line 314
    :cond_3
    iget-object v1, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->this$0:Lmiui/resourcebrowser/widget/AsyncAdapter;

    invoke-virtual {v1}, Lmiui/resourcebrowser/widget/AsyncAdapter;->notifyDataSetChanged()V

    .line 316
    :cond_4
    invoke-super {p0, p1}, Lmiui/os/UniqueAsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 317
    return-void
.end method

.method public setGroup(I)V
    .locals 0
    .parameter "group"

    .prologue
    .line 264
    .local p0, this:Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;,"Lmiui/resourcebrowser/widget/AsyncAdapter<TT;>.AsyncLoadDataTask;"
    iput p1, p0, Lmiui/resourcebrowser/widget/AsyncAdapter$AsyncLoadDataTask;->mGroup:I

    .line 265
    return-void
.end method
