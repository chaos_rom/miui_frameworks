.class public abstract Lmiui/resourcebrowser/controller/OnlineDataManager;
.super Lmiui/resourcebrowser/controller/DataManager;
.source "OnlineDataManager.java"


# direct methods
.method public constructor <init>(Lmiui/resourcebrowser/ResourceContext;)V
    .locals 0
    .parameter "context"

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lmiui/resourcebrowser/controller/DataManager;-><init>(Lmiui/resourcebrowser/ResourceContext;)V

    .line 15
    return-void
.end method


# virtual methods
.method public abstract getAssociationResources(Ljava/util/List;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getRecommends()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/RecommendItemData;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getResource(Ljava/lang/String;)Lmiui/resourcebrowser/model/Resource;
.end method

.method public abstract getResource(Ljava/lang/String;Z)Lmiui/resourcebrowser/model/Resource;
.end method

.method public abstract getResourceByLocalId(Ljava/lang/String;)Lmiui/resourcebrowser/model/Resource;
.end method

.method public abstract getResourceCategories()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/ResourceCategory;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getResourceListMeta(Ljava/lang/String;)Lmiui/resourcebrowser/model/ResourceListMeta;
.end method

.method public abstract getResources(I)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getUpdatableResources(Ljava/util/List;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;"
        }
    .end annotation
.end method
