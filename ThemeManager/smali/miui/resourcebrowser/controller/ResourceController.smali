.class public Lmiui/resourcebrowser/controller/ResourceController;
.super Ljava/lang/Object;
.source "ResourceController.java"


# instance fields
.field protected context:Lmiui/resourcebrowser/ResourceContext;

.field private importManager:Lmiui/resourcebrowser/controller/ImportManager;

.field private localDataManager:Lmiui/resourcebrowser/controller/LocalDataManager;

.field private onlineDataManager:Lmiui/resourcebrowser/controller/OnlineDataManager;


# direct methods
.method public constructor <init>(Lmiui/resourcebrowser/ResourceContext;)V
    .locals 1
    .parameter "context"

    .prologue
    .line 17
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lmiui/resourcebrowser/controller/ResourceController;->context:Lmiui/resourcebrowser/ResourceContext;

    .line 19
    invoke-virtual {p0}, Lmiui/resourcebrowser/controller/ResourceController;->newLocalDataManager()Lmiui/resourcebrowser/controller/LocalDataManager;

    move-result-object v0

    iput-object v0, p0, Lmiui/resourcebrowser/controller/ResourceController;->localDataManager:Lmiui/resourcebrowser/controller/LocalDataManager;

    .line 20
    iget-object v0, p0, Lmiui/resourcebrowser/controller/ResourceController;->localDataManager:Lmiui/resourcebrowser/controller/LocalDataManager;

    invoke-virtual {v0, p0}, Lmiui/resourcebrowser/controller/LocalDataManager;->setResourceController(Lmiui/resourcebrowser/controller/ResourceController;)V

    .line 21
    invoke-virtual {p0}, Lmiui/resourcebrowser/controller/ResourceController;->newOnlineDataManager()Lmiui/resourcebrowser/controller/OnlineDataManager;

    move-result-object v0

    iput-object v0, p0, Lmiui/resourcebrowser/controller/ResourceController;->onlineDataManager:Lmiui/resourcebrowser/controller/OnlineDataManager;

    .line 22
    iget-object v0, p0, Lmiui/resourcebrowser/controller/ResourceController;->onlineDataManager:Lmiui/resourcebrowser/controller/OnlineDataManager;

    invoke-virtual {v0, p0}, Lmiui/resourcebrowser/controller/OnlineDataManager;->setResourceController(Lmiui/resourcebrowser/controller/ResourceController;)V

    .line 23
    invoke-virtual {p0}, Lmiui/resourcebrowser/controller/ResourceController;->newImportManager()Lmiui/resourcebrowser/controller/ImportManager;

    move-result-object v0

    iput-object v0, p0, Lmiui/resourcebrowser/controller/ResourceController;->importManager:Lmiui/resourcebrowser/controller/ImportManager;

    .line 24
    iget-object v0, p0, Lmiui/resourcebrowser/controller/ResourceController;->importManager:Lmiui/resourcebrowser/controller/ImportManager;

    invoke-virtual {v0, p0}, Lmiui/resourcebrowser/controller/ImportManager;->setResourceController(Lmiui/resourcebrowser/controller/ResourceController;)V

    .line 25
    return-void
.end method


# virtual methods
.method public getImportManager()Lmiui/resourcebrowser/controller/ImportManager;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lmiui/resourcebrowser/controller/ResourceController;->importManager:Lmiui/resourcebrowser/controller/ImportManager;

    return-object v0
.end method

.method public getLocalDataManager()Lmiui/resourcebrowser/controller/LocalDataManager;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lmiui/resourcebrowser/controller/ResourceController;->localDataManager:Lmiui/resourcebrowser/controller/LocalDataManager;

    return-object v0
.end method

.method public getOnlineDataManager()Lmiui/resourcebrowser/controller/OnlineDataManager;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lmiui/resourcebrowser/controller/ResourceController;->onlineDataManager:Lmiui/resourcebrowser/controller/OnlineDataManager;

    return-object v0
.end method

.method protected newImportManager()Lmiui/resourcebrowser/controller/ImportManager;
    .locals 2

    .prologue
    .line 39
    new-instance v0, Lmiui/resourcebrowser/controller/ImportManager;

    iget-object v1, p0, Lmiui/resourcebrowser/controller/ResourceController;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-direct {v0, v1}, Lmiui/resourcebrowser/controller/ImportManager;-><init>(Lmiui/resourcebrowser/ResourceContext;)V

    return-object v0
.end method

.method protected newLocalDataManager()Lmiui/resourcebrowser/controller/LocalDataManager;
    .locals 2

    .prologue
    .line 28
    iget-object v0, p0, Lmiui/resourcebrowser/controller/ResourceController;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v0}, Lmiui/resourcebrowser/ResourceContext;->getResourceFormat()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 29
    new-instance v0, Lmiui/resourcebrowser/controller/local/AudioDataManager;

    iget-object v1, p0, Lmiui/resourcebrowser/controller/ResourceController;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-direct {v0, v1}, Lmiui/resourcebrowser/controller/local/AudioDataManager;-><init>(Lmiui/resourcebrowser/ResourceContext;)V

    .line 31
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;

    iget-object v1, p0, Lmiui/resourcebrowser/controller/ResourceController;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-direct {v0, v1}, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;-><init>(Lmiui/resourcebrowser/ResourceContext;)V

    goto :goto_0
.end method

.method protected newOnlineDataManager()Lmiui/resourcebrowser/controller/OnlineDataManager;
    .locals 2

    .prologue
    .line 35
    new-instance v0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;

    iget-object v1, p0, Lmiui/resourcebrowser/controller/ResourceController;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-direct {v0, v1}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;-><init>(Lmiui/resourcebrowser/ResourceContext;)V

    return-object v0
.end method

.method public setImportManager(Lmiui/resourcebrowser/controller/ImportManager;)V
    .locals 0
    .parameter "importManager"

    .prologue
    .line 63
    iput-object p1, p0, Lmiui/resourcebrowser/controller/ResourceController;->importManager:Lmiui/resourcebrowser/controller/ImportManager;

    .line 64
    return-void
.end method

.method public setLocalDataManager(Lmiui/resourcebrowser/controller/LocalDataManager;)V
    .locals 0
    .parameter "localDataManager"

    .prologue
    .line 47
    iput-object p1, p0, Lmiui/resourcebrowser/controller/ResourceController;->localDataManager:Lmiui/resourcebrowser/controller/LocalDataManager;

    .line 48
    return-void
.end method
