.class public Lmiui/resourcebrowser/controller/ImportManager;
.super Ljava/lang/Object;
.source "ImportManager.java"

# interfaces
.implements Lmiui/resourcebrowser/ResourceConstants;


# instance fields
.field protected baseContext:Lmiui/resourcebrowser/ResourceContext;

.field protected context:Lmiui/resourcebrowser/ResourceContext;

.field protected controller:Lmiui/resourcebrowser/controller/ResourceController;

.field protected idGenerationStrategy:Lmiui/resourcebrowser/controller/strategy/IdGenerationStrategy;


# direct methods
.method public constructor <init>(Lmiui/resourcebrowser/ResourceContext;)V
    .locals 1
    .parameter "context"

    .prologue
    .line 32
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Lmiui/resourcebrowser/controller/strategy/IncrementalIdGenerationStrategy;

    invoke-direct {v0}, Lmiui/resourcebrowser/controller/strategy/IncrementalIdGenerationStrategy;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/controller/ImportManager;->idGenerationStrategy:Lmiui/resourcebrowser/controller/strategy/IdGenerationStrategy;

    .line 33
    iput-object p1, p0, Lmiui/resourcebrowser/controller/ImportManager;->baseContext:Lmiui/resourcebrowser/ResourceContext;

    .line 34
    return-void
.end method


# virtual methods
.method protected buildBundle(Ljava/io/File;Lmiui/resourcebrowser/model/Resource;)Lmiui/resourcebrowser/model/Resource;
    .locals 6
    .parameter "folder"
    .parameter "resource"

    .prologue
    .line 149
    new-instance v1, Ljava/io/File;

    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getDownloadPath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 150
    .local v1, downloadFile:Ljava/io/File;
    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getLocalId()Ljava/lang/String;

    move-result-object v3

    .line 151
    .local v3, id:Ljava/lang/String;
    if-nez v3, :cond_0

    .line 152
    iget-object v4, p0, Lmiui/resourcebrowser/controller/ImportManager;->idGenerationStrategy:Lmiui/resourcebrowser/controller/strategy/IdGenerationStrategy;

    invoke-virtual {v4}, Lmiui/resourcebrowser/controller/strategy/IdGenerationStrategy;->nextId()Ljava/lang/String;

    move-result-object v3

    .line 155
    :cond_0
    invoke-virtual {p2, v3}, Lmiui/resourcebrowser/model/Resource;->setLocalId(Ljava/lang/String;)V

    .line 156
    invoke-virtual {p0, v1, v3}, Lmiui/resourcebrowser/controller/ImportManager;->getBundleLocalPath(Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Lmiui/resourcebrowser/model/Resource;->setLocalPath(Ljava/lang/String;)V

    .line 157
    invoke-virtual {p0, v1, v3}, Lmiui/resourcebrowser/controller/ImportManager;->getBundleFilePath(Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 158
    .local v2, filePath:Ljava/lang/String;
    invoke-virtual {p0, v2}, Lmiui/resourcebrowser/controller/ImportManager;->createBundleContentFile(Ljava/lang/String;)Z

    .line 159
    invoke-virtual {p2, v2}, Lmiui/resourcebrowser/model/Resource;->setFilePath(Ljava/lang/String;)V

    .line 160
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lmiui/resourcebrowser/util/ResourceHelper;->getFileHash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Lmiui/resourcebrowser/model/Resource;->setHash(Ljava/lang/String;)V

    .line 161
    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-virtual {p2, v4, v5}, Lmiui/resourcebrowser/model/Resource;->setSize(J)V

    .line 163
    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getTitle()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_1

    .line 164
    new-instance v0, Ljava/io/File;

    const-string v4, "description.xml"

    invoke-direct {v0, p1, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 165
    .local v0, descFile:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 166
    invoke-static {v0}, Lmiui/resourcebrowser/util/ResourceHelper;->getDescription(Ljava/io/File;)Ljava/util/Map;

    move-result-object v4

    invoke-virtual {p0, p2, v4}, Lmiui/resourcebrowser/controller/ImportManager;->populateDataByDescription(Lmiui/resourcebrowser/model/Resource;Ljava/util/Map;)V

    .line 167
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 170
    .end local v0           #descFile:Ljava/io/File;
    :cond_1
    iget-object v4, p0, Lmiui/resourcebrowser/controller/ImportManager;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v4}, Lmiui/resourcebrowser/ResourceContext;->getBuildInImagePrefixes()Ljava/util/List;

    move-result-object v4

    invoke-virtual {p0, p1, p2, v4}, Lmiui/resourcebrowser/controller/ImportManager;->resolveBuildInImages(Ljava/io/File;Lmiui/resourcebrowser/model/Resource;Ljava/util/List;)V

    .line 172
    return-object p2
.end method

.method protected buildComponent(Ljava/io/File;Ljava/io/File;Lmiui/resourcebrowser/model/Resource;Ljava/lang/String;)Lmiui/resourcebrowser/model/Resource;
    .locals 9
    .parameter "folder"
    .parameter "file"
    .parameter "parent"
    .parameter "code"

    .prologue
    .line 283
    new-instance v5, Lmiui/resourcebrowser/model/Resource;

    invoke-direct {v5}, Lmiui/resourcebrowser/model/Resource;-><init>()V

    .line 284
    .local v5, resource:Lmiui/resourcebrowser/model/Resource;
    invoke-virtual {p3}, Lmiui/resourcebrowser/model/Resource;->getSubResources()Ljava/util/List;

    move-result-object v7

    invoke-virtual {p0, p4, v7}, Lmiui/resourcebrowser/controller/ImportManager;->getRelatedResource(Ljava/lang/String;Ljava/util/List;)Lmiui/resourcebrowser/model/RelatedResource;

    move-result-object v6

    .line 285
    .local v6, subResource:Lmiui/resourcebrowser/model/RelatedResource;
    if-eqz v6, :cond_0

    .line 286
    new-instance v2, Lmiui/resourcebrowser/controller/local/LocalJSONDataParser;

    iget-object v7, p0, Lmiui/resourcebrowser/controller/ImportManager;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-direct {v2, v7}, Lmiui/resourcebrowser/controller/local/LocalJSONDataParser;-><init>(Lmiui/resourcebrowser/ResourceContext;)V

    .line 288
    .local v2, parser:Lmiui/resourcebrowser/controller/local/LocalDataParser;
    :try_start_0
    new-instance v7, Ljava/io/File;

    invoke-virtual {v6}, Lmiui/resourcebrowser/model/RelatedResource;->getLocalPath()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v7}, Lmiui/resourcebrowser/controller/local/LocalJSONDataParser;->loadResource(Ljava/io/File;)Lmiui/resourcebrowser/model/Resource;

    move-result-object v3

    .line 289
    .local v3, r:Lmiui/resourcebrowser/model/Resource;
    if-eqz v3, :cond_0

    .line 290
    invoke-virtual {v5, v3}, Lmiui/resourcebrowser/model/Resource;->updateFrom(Lmiui/resourcebrowser/model/Resource;)V
    :try_end_0
    .catch Lmiui/resourcebrowser/controller/local/PersistenceException; {:try_start_0 .. :try_end_0} :catch_0

    .line 296
    .end local v2           #parser:Lmiui/resourcebrowser/controller/local/LocalDataParser;
    .end local v3           #r:Lmiui/resourcebrowser/model/Resource;
    :cond_0
    :goto_0
    invoke-virtual {p0, v5, p3}, Lmiui/resourcebrowser/controller/ImportManager;->copyInheritedProperties(Lmiui/resourcebrowser/model/Resource;Lmiui/resourcebrowser/model/Resource;)V

    .line 297
    invoke-virtual {v5}, Lmiui/resourcebrowser/model/Resource;->getLocalId()Ljava/lang/String;

    move-result-object v1

    .line 298
    .local v1, id:Ljava/lang/String;
    if-nez v1, :cond_1

    .line 299
    iget-object v7, p0, Lmiui/resourcebrowser/controller/ImportManager;->idGenerationStrategy:Lmiui/resourcebrowser/controller/strategy/IdGenerationStrategy;

    invoke-virtual {v7}, Lmiui/resourcebrowser/controller/strategy/IdGenerationStrategy;->nextId()Ljava/lang/String;

    move-result-object v1

    .line 302
    :cond_1
    invoke-virtual {v5, v1}, Lmiui/resourcebrowser/model/Resource;->setLocalId(Ljava/lang/String;)V

    .line 303
    invoke-virtual {p0, p2, v1, p4}, Lmiui/resourcebrowser/controller/ImportManager;->getComponentLocalPath(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lmiui/resourcebrowser/model/Resource;->setLocalPath(Ljava/lang/String;)V

    .line 304
    invoke-virtual {p0, p2, v1, p4}, Lmiui/resourcebrowser/controller/ImportManager;->getComponentFilePath(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lmiui/resourcebrowser/model/Resource;->setFilePath(Ljava/lang/String;)V

    .line 305
    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lmiui/resourcebrowser/util/ResourceHelper;->getFileHash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lmiui/resourcebrowser/model/Resource;->setHash(Ljava/lang/String;)V

    .line 306
    invoke-virtual {p2}, Ljava/io/File;->length()J

    move-result-wide v7

    invoke-virtual {v5, v7, v8}, Lmiui/resourcebrowser/model/Resource;->setSize(J)V

    .line 308
    invoke-virtual {p0, p4}, Lmiui/resourcebrowser/controller/ImportManager;->getComponentImagePrefixes(Ljava/lang/String;)Ljava/util/List;

    move-result-object v7

    invoke-virtual {p0, p1, v5, v7}, Lmiui/resourcebrowser/controller/ImportManager;->resolveBuildInImages(Ljava/io/File;Lmiui/resourcebrowser/model/Resource;Ljava/util/List;)V

    .line 310
    iget-object v7, p0, Lmiui/resourcebrowser/controller/ImportManager;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v7}, Lmiui/resourcebrowser/ResourceContext;->getResourceCode()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5}, Lmiui/resourcebrowser/model/Resource;->getParentResources()Ljava/util/List;

    move-result-object v8

    invoke-virtual {p0, v7, v8}, Lmiui/resourcebrowser/controller/ImportManager;->getRelatedResource(Ljava/lang/String;Ljava/util/List;)Lmiui/resourcebrowser/model/RelatedResource;

    move-result-object v4

    .line 311
    .local v4, relatedParent:Lmiui/resourcebrowser/model/RelatedResource;
    if-nez v4, :cond_2

    .line 312
    new-instance v4, Lmiui/resourcebrowser/model/RelatedResource;

    .end local v4           #relatedParent:Lmiui/resourcebrowser/model/RelatedResource;
    invoke-direct {v4}, Lmiui/resourcebrowser/model/RelatedResource;-><init>()V

    .line 313
    .restart local v4       #relatedParent:Lmiui/resourcebrowser/model/RelatedResource;
    invoke-virtual {p3}, Lmiui/resourcebrowser/model/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lmiui/resourcebrowser/model/RelatedResource;->setLocalPath(Ljava/lang/String;)V

    .line 314
    iget-object v7, p0, Lmiui/resourcebrowser/controller/ImportManager;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v7}, Lmiui/resourcebrowser/ResourceContext;->getResourceCode()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lmiui/resourcebrowser/model/RelatedResource;->setResourceCode(Ljava/lang/String;)V

    .line 315
    invoke-virtual {v5, v4}, Lmiui/resourcebrowser/model/Resource;->addParentResources(Lmiui/resourcebrowser/model/RelatedResource;)V

    .line 318
    :cond_2
    return-object v5

    .line 292
    .end local v1           #id:Ljava/lang/String;
    .end local v4           #relatedParent:Lmiui/resourcebrowser/model/RelatedResource;
    .restart local v2       #parser:Lmiui/resourcebrowser/controller/local/LocalDataParser;
    :catch_0
    move-exception v0

    .line 293
    .local v0, e:Lmiui/resourcebrowser/controller/local/PersistenceException;
    invoke-virtual {v0}, Lmiui/resourcebrowser/controller/local/PersistenceException;->printStackTrace()V

    goto :goto_0
.end method

.method protected buildSingle(Ljava/io/File;Lmiui/resourcebrowser/model/Resource;)Lmiui/resourcebrowser/model/Resource;
    .locals 3
    .parameter "file"
    .parameter "resource"

    .prologue
    .line 435
    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getLocalId()Ljava/lang/String;

    move-result-object v0

    .line 436
    .local v0, id:Ljava/lang/String;
    if-nez v0, :cond_0

    .line 437
    iget-object v1, p0, Lmiui/resourcebrowser/controller/ImportManager;->idGenerationStrategy:Lmiui/resourcebrowser/controller/strategy/IdGenerationStrategy;

    invoke-virtual {v1}, Lmiui/resourcebrowser/controller/strategy/IdGenerationStrategy;->nextId()Ljava/lang/String;

    move-result-object v0

    .line 440
    :cond_0
    invoke-virtual {p2, v0}, Lmiui/resourcebrowser/model/Resource;->setLocalId(Ljava/lang/String;)V

    .line 441
    invoke-virtual {p0, p1, v0}, Lmiui/resourcebrowser/controller/ImportManager;->getSingleLocalPath(Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Lmiui/resourcebrowser/model/Resource;->setLocalPath(Ljava/lang/String;)V

    .line 442
    invoke-virtual {p0, p1, v0}, Lmiui/resourcebrowser/controller/ImportManager;->getSingleFilePath(Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Lmiui/resourcebrowser/model/Resource;->setFilePath(Ljava/lang/String;)V

    .line 443
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lmiui/resourcebrowser/util/ResourceHelper;->getFileHash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Lmiui/resourcebrowser/model/Resource;->setHash(Ljava/lang/String;)V

    .line 444
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v1

    invoke-virtual {p2, v1, v2}, Lmiui/resourcebrowser/model/Resource;->setSize(J)V

    .line 446
    return-object p2
.end method

.method protected copyInheritedProperties(Lmiui/resourcebrowser/model/Resource;Lmiui/resourcebrowser/model/Resource;)V
    .locals 2
    .parameter "target"
    .parameter "source"

    .prologue
    .line 331
    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getProductId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmiui/resourcebrowser/model/Resource;->setProductId(Ljava/lang/String;)V

    .line 332
    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getOnlinePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmiui/resourcebrowser/model/Resource;->setOnlinePath(Ljava/lang/String;)V

    .line 333
    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getDownloadPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmiui/resourcebrowser/model/Resource;->setDownloadPath(Ljava/lang/String;)V

    .line 334
    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getRightsPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmiui/resourcebrowser/model/Resource;->setRightsPath(Ljava/lang/String;)V

    .line 335
    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getPlatform()I

    move-result v0

    invoke-virtual {p1, v0}, Lmiui/resourcebrowser/model/Resource;->setPlatform(I)V

    .line 336
    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getStatus()I

    move-result v0

    invoke-virtual {p1, v0}, Lmiui/resourcebrowser/model/Resource;->setStatus(I)V

    .line 337
    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getUpdatedTime()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lmiui/resourcebrowser/model/Resource;->setUpdatedTime(J)V

    .line 338
    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmiui/resourcebrowser/model/Resource;->setTitle(Ljava/lang/String;)V

    .line 339
    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getDescription()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmiui/resourcebrowser/model/Resource;->setDescription(Ljava/lang/String;)V

    .line 340
    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getAuthor()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmiui/resourcebrowser/model/Resource;->setAuthor(Ljava/lang/String;)V

    .line 341
    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getDesigner()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmiui/resourcebrowser/model/Resource;->setDesigner(Ljava/lang/String;)V

    .line 342
    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getVersion()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmiui/resourcebrowser/model/Resource;->setVersion(Ljava/lang/String;)V

    .line 343
    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getDownloadCount()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmiui/resourcebrowser/model/Resource;->setDownloadCount(Ljava/lang/String;)V

    .line 344
    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getExtraMeta()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmiui/resourcebrowser/model/Resource;->setExtraMeta(Ljava/util/Map;)V

    .line 345
    return-void
.end method

.method protected createBundleContentFile(Ljava/lang/String;)Z
    .locals 3
    .parameter "filePath"

    .prologue
    .line 185
    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 186
    .local v0, contentFile:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    .line 187
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 192
    const/4 v2, 0x1

    .end local v0           #contentFile:Ljava/io/File;
    :goto_0
    return v2

    .line 188
    :catch_0
    move-exception v1

    .line 189
    .local v1, e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 190
    const/4 v2, 0x0

    goto :goto_0
.end method

.method protected createBundleIndexFile(Ljava/lang/String;)Z
    .locals 3
    .parameter "filePath"

    .prologue
    .line 197
    :try_start_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 198
    .local v1, indexFile:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    .line 199
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 204
    const/4 v2, 0x1

    .end local v1           #indexFile:Ljava/io/File;
    :goto_0
    return v2

    .line 200
    :catch_0
    move-exception v0

    .line 201
    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 202
    const/4 v2, 0x0

    goto :goto_0
.end method

.method protected getBuildInImageFolderPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter "id"

    .prologue
    .line 396
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lmiui/resourcebrowser/controller/ImportManager;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v1}, Lmiui/resourcebrowser/ResourceContext;->getBuildInImageFolder()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lmiui/os/ExtraFileUtils;->standardizeFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getBundleFilePath(Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter "file"
    .parameter "id"

    .prologue
    .line 180
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lmiui/resourcebrowser/controller/ImportManager;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v1}, Lmiui/resourcebrowser/ResourceContext;->getContentFolder()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getBundleLocalPath(Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter "file"
    .parameter "id"

    .prologue
    .line 176
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lmiui/resourcebrowser/controller/ImportManager;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v1}, Lmiui/resourcebrowser/ResourceContext;->getMetaFolder()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".mrm"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getComponentFilePath(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .parameter "file"
    .parameter "id"
    .parameter "code"

    .prologue
    .line 354
    invoke-static {p3}, Lmiui/os/ExtraFileUtils;->standardizeFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 355
    .local v0, codeFolder:Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lmiui/resourcebrowser/controller/ImportManager;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v3}, Lmiui/resourcebrowser/ResourceContext;->getContentFolder()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lmiui/os/ExtraFileUtils;->standardizeFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 356
    .local v1, folder:Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".mrc"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method protected getComponentImagePrefixes(Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .parameter "code"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 360
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 361
    .local v0, prefixes:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "preview_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 362
    return-object v0
.end method

.method protected getComponentLocalPath(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .parameter "file"
    .parameter "id"
    .parameter "code"

    .prologue
    .line 348
    invoke-static {p3}, Lmiui/os/ExtraFileUtils;->standardizeFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 349
    .local v0, codeFolder:Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lmiui/resourcebrowser/controller/ImportManager;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v3}, Lmiui/resourcebrowser/ResourceContext;->getMetaFolder()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lmiui/os/ExtraFileUtils;->standardizeFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 350
    .local v1, folder:Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".mrm"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method protected getComponentsMap(Ljava/io/File;)Ljava/util/Map;
    .locals 8
    .parameter "folder"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 230
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 231
    .local v4, map:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .local v0, arr$:[Ljava/io/File;
    array-length v3, v0

    .local v3, len$:I
    const/4 v2, 0x0

    .local v2, i$:I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 232
    .local v1, file:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    .line 233
    .local v5, name:Ljava/lang/String;
    const-string v6, "preview"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 234
    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v4, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 231
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 237
    .end local v1           #file:Ljava/io/File;
    .end local v5           #name:Ljava/lang/String;
    :cond_1
    return-object v4
.end method

.method protected getRelatedResource(Ljava/lang/String;Ljava/util/List;)Lmiui/resourcebrowser/model/RelatedResource;
    .locals 3
    .parameter "code"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/RelatedResource;",
            ">;)",
            "Lmiui/resourcebrowser/model/RelatedResource;"
        }
    .end annotation

    .prologue
    .line 322
    .local p2, relatedResources:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/RelatedResource;>;"
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/resourcebrowser/model/RelatedResource;

    .line 323
    .local v1, relatedResource:Lmiui/resourcebrowser/model/RelatedResource;
    invoke-virtual {v1}, Lmiui/resourcebrowser/model/RelatedResource;->getResourceCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 327
    .end local v1           #relatedResource:Lmiui/resourcebrowser/model/RelatedResource;
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected getSingleFilePath(Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter "file"
    .parameter "id"

    .prologue
    .line 454
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lmiui/resourcebrowser/controller/ImportManager;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v1}, Lmiui/resourcebrowser/ResourceContext;->getContentFolder()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".mrc"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getSingleLocalPath(Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter "file"
    .parameter "id"

    .prologue
    .line 450
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lmiui/resourcebrowser/controller/ImportManager;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v1}, Lmiui/resourcebrowser/ResourceContext;->getMetaFolder()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".mrm"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected importAudio(Lmiui/resourcebrowser/model/Resource;)Z
    .locals 1
    .parameter "resource"

    .prologue
    .line 404
    const/4 v0, 0x1

    return v0
.end method

.method protected importBundle(Lmiui/resourcebrowser/model/Resource;)Z
    .locals 5
    .parameter "resource"

    .prologue
    const/4 v3, 0x0

    .line 108
    const/4 v1, 0x1

    .line 109
    .local v1, imported:Z
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/controller/ImportManager;->unzipBundle(Lmiui/resourcebrowser/model/Resource;)Ljava/lang/String;

    move-result-object v2

    .line 110
    .local v2, unzipPath:Ljava/lang/String;
    if-nez v2, :cond_0

    .line 121
    :goto_0
    return v3

    .line 113
    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 114
    .local v0, folder:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 115
    invoke-virtual {p0, v0, p1}, Lmiui/resourcebrowser/controller/ImportManager;->buildBundle(Ljava/io/File;Lmiui/resourcebrowser/model/Resource;)Lmiui/resourcebrowser/model/Resource;

    move-result-object p1

    .line 116
    invoke-virtual {p0, v0, p1}, Lmiui/resourcebrowser/controller/ImportManager;->importComponents(Ljava/io/File;Lmiui/resourcebrowser/model/Resource;)Z

    move-result v4

    if-eqz v4, :cond_2

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    .line 118
    :goto_1
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lmiui/os/Shell;->remove(Ljava/lang/String;)Z

    .line 119
    iget-object v3, p0, Lmiui/resourcebrowser/controller/ImportManager;->controller:Lmiui/resourcebrowser/controller/ResourceController;

    invoke-virtual {v3}, Lmiui/resourcebrowser/controller/ResourceController;->getLocalDataManager()Lmiui/resourcebrowser/controller/LocalDataManager;

    move-result-object v3

    invoke-virtual {v3, p1}, Lmiui/resourcebrowser/controller/LocalDataManager;->addResource(Lmiui/resourcebrowser/model/Resource;)Z

    :cond_1
    move v3, v1

    .line 121
    goto :goto_0

    :cond_2
    move v1, v3

    .line 116
    goto :goto_1
.end method

.method protected importComponent(Ljava/io/File;Ljava/io/File;Lmiui/resourcebrowser/model/Resource;Ljava/lang/String;)Z
    .locals 4
    .parameter "folder"
    .parameter "file"
    .parameter "parent"
    .parameter "code"

    .prologue
    .line 262
    invoke-virtual {p0, p1, p2, p3, p4}, Lmiui/resourcebrowser/controller/ImportManager;->buildComponent(Ljava/io/File;Ljava/io/File;Lmiui/resourcebrowser/model/Resource;Ljava/lang/String;)Lmiui/resourcebrowser/model/Resource;

    move-result-object v2

    .line 264
    .local v2, resource:Lmiui/resourcebrowser/model/Resource;
    invoke-virtual {p3}, Lmiui/resourcebrowser/model/Resource;->getSubResources()Ljava/util/List;

    move-result-object v3

    invoke-virtual {p0, p4, v3}, Lmiui/resourcebrowser/controller/ImportManager;->getRelatedResource(Ljava/lang/String;Ljava/util/List;)Lmiui/resourcebrowser/model/RelatedResource;

    move-result-object v1

    .line 265
    .local v1, relatedSub:Lmiui/resourcebrowser/model/RelatedResource;
    if-nez v1, :cond_0

    .line 266
    new-instance v1, Lmiui/resourcebrowser/model/RelatedResource;

    .end local v1           #relatedSub:Lmiui/resourcebrowser/model/RelatedResource;
    invoke-direct {v1}, Lmiui/resourcebrowser/model/RelatedResource;-><init>()V

    .line 267
    .restart local v1       #relatedSub:Lmiui/resourcebrowser/model/RelatedResource;
    invoke-virtual {v2}, Lmiui/resourcebrowser/model/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lmiui/resourcebrowser/model/RelatedResource;->setLocalPath(Ljava/lang/String;)V

    .line 268
    invoke-virtual {v2}, Lmiui/resourcebrowser/model/Resource;->getFilePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lmiui/resourcebrowser/model/RelatedResource;->setFilePath(Ljava/lang/String;)V

    .line 269
    invoke-virtual {v1, p4}, Lmiui/resourcebrowser/model/RelatedResource;->setResourceCode(Ljava/lang/String;)V

    .line 270
    invoke-virtual {p3, v1}, Lmiui/resourcebrowser/model/Resource;->addSubResources(Lmiui/resourcebrowser/model/RelatedResource;)V

    .line 273
    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-virtual {v2}, Lmiui/resourcebrowser/model/Resource;->getFilePath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 274
    .local v0, componentFile:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    .line 275
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 276
    invoke-virtual {p2, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 278
    iget-object v3, p0, Lmiui/resourcebrowser/controller/ImportManager;->controller:Lmiui/resourcebrowser/controller/ResourceController;

    invoke-virtual {v3}, Lmiui/resourcebrowser/controller/ResourceController;->getLocalDataManager()Lmiui/resourcebrowser/controller/LocalDataManager;

    move-result-object v3

    invoke-virtual {v3, v2}, Lmiui/resourcebrowser/controller/LocalDataManager;->addResource(Lmiui/resourcebrowser/model/Resource;)Z

    .line 279
    const/4 v3, 0x1

    return v3
.end method

.method protected importComponents(Ljava/io/File;Lmiui/resourcebrowser/model/Resource;)Z
    .locals 10
    .parameter "folder"
    .parameter "parent"

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 208
    const/4 v3, 0x1

    .line 209
    .local v3, imported:Z
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/controller/ImportManager;->getComponentsMap(Ljava/io/File;)Ljava/util/Map;

    move-result-object v5

    .line 211
    .local v5, map:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, i$:Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 212
    .local v6, name:Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 213
    .local v1, file:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 214
    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {p0, p1, v1, p2, v7}, Lmiui/resourcebrowser/controller/ImportManager;->importComponent(Ljava/io/File;Ljava/io/File;Lmiui/resourcebrowser/model/Resource;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    if-eqz v3, :cond_1

    move v3, v8

    :goto_1
    goto :goto_0

    :cond_1
    move v3, v9

    goto :goto_1

    .line 218
    .end local v1           #file:Ljava/io/File;
    .end local v6           #name:Ljava/lang/String;
    :cond_2
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .local v0, arr$:[Ljava/io/File;
    array-length v4, v0

    .local v4, len$:I
    const/4 v2, 0x0

    .local v2, i$:I
    :goto_2
    if-ge v2, v4, :cond_5

    aget-object v1, v0, v2

    .line 219
    .restart local v1       #file:Ljava/io/File;
    invoke-static {v1}, Lmiui/resourcebrowser/util/ResourceHelper;->deleteEmptyFolder(Ljava/io/File;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 220
    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    .line 221
    .restart local v6       #name:Ljava/lang/String;
    const-string v7, "preview"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_3

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v7

    if-nez v7, :cond_3

    .line 222
    invoke-virtual {p0, p1, v1, p2, v6}, Lmiui/resourcebrowser/controller/ImportManager;->importComponent(Ljava/io/File;Ljava/io/File;Lmiui/resourcebrowser/model/Resource;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    if-eqz v3, :cond_4

    move v3, v8

    .line 218
    .end local v6           #name:Ljava/lang/String;
    :cond_3
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .restart local v6       #name:Ljava/lang/String;
    :cond_4
    move v3, v9

    .line 222
    goto :goto_3

    .line 226
    .end local v1           #file:Ljava/io/File;
    .end local v6           #name:Ljava/lang/String;
    :cond_5
    return v3
.end method

.method protected importImage(Lmiui/resourcebrowser/model/Resource;)Z
    .locals 1
    .parameter "resource"

    .prologue
    .line 400
    const/4 v0, 0x1

    return v0
.end method

.method protected importOther(Lmiui/resourcebrowser/model/Resource;)Z
    .locals 1
    .parameter "resource"

    .prologue
    .line 416
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/controller/ImportManager;->importSingle(Lmiui/resourcebrowser/model/Resource;)Z

    move-result v0

    return v0
.end method

.method public importResource(Lmiui/resourcebrowser/model/Resource;)Z
    .locals 1
    .parameter "resource"

    .prologue
    .line 41
    iget-object v0, p0, Lmiui/resourcebrowser/controller/ImportManager;->baseContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {p0, p1, v0}, Lmiui/resourcebrowser/controller/ImportManager;->importResource(Lmiui/resourcebrowser/model/Resource;Lmiui/resourcebrowser/ResourceContext;)Z

    move-result v0

    return v0
.end method

.method public importResource(Lmiui/resourcebrowser/model/Resource;Lmiui/resourcebrowser/ResourceContext;)Z
    .locals 3
    .parameter "resource"
    .parameter "context"

    .prologue
    .line 45
    const/4 v0, 0x0

    .line 46
    .local v0, imported:Z
    iput-object p2, p0, Lmiui/resourcebrowser/controller/ImportManager;->context:Lmiui/resourcebrowser/ResourceContext;

    .line 47
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/controller/ImportManager;->resolveOnlineId(Lmiui/resourcebrowser/model/Resource;)V

    .line 49
    invoke-virtual {p2}, Lmiui/resourcebrowser/ResourceContext;->getResourceFormat()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 66
    :goto_0
    if-eqz v0, :cond_0

    .line 67
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Lmiui/resourcebrowser/ResourceContext;->getIndexFolder()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getHash()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lmiui/resourcebrowser/controller/ImportManager;->createBundleIndexFile(Ljava/lang/String;)Z

    .line 69
    :cond_0
    return v0

    .line 51
    :pswitch_0
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/controller/ImportManager;->importBundle(Lmiui/resourcebrowser/model/Resource;)Z

    move-result v0

    .line 52
    goto :goto_0

    .line 54
    :pswitch_1
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/controller/ImportManager;->importImage(Lmiui/resourcebrowser/model/Resource;)Z

    move-result v0

    .line 55
    goto :goto_0

    .line 57
    :pswitch_2
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/controller/ImportManager;->importAudio(Lmiui/resourcebrowser/model/Resource;)Z

    move-result v0

    .line 58
    goto :goto_0

    .line 60
    :pswitch_3
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/controller/ImportManager;->importZip(Lmiui/resourcebrowser/model/Resource;)Z

    move-result v0

    .line 61
    goto :goto_0

    .line 63
    :pswitch_4
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/controller/ImportManager;->importOther(Lmiui/resourcebrowser/model/Resource;)Z

    move-result v0

    goto :goto_0

    .line 49
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected importSingle(Lmiui/resourcebrowser/model/Resource;)Z
    .locals 4
    .parameter "resource"

    .prologue
    .line 420
    const/4 v2, 0x1

    .line 422
    .local v2, imported:Z
    new-instance v1, Ljava/io/File;

    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getDownloadPath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 423
    .local v1, downloadFile:Ljava/io/File;
    invoke-virtual {p0, v1, p1}, Lmiui/resourcebrowser/controller/ImportManager;->buildSingle(Ljava/io/File;Lmiui/resourcebrowser/model/Resource;)Lmiui/resourcebrowser/model/Resource;

    move-result-object p1

    .line 425
    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getFilePath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 426
    .local v0, contentFile:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    .line 427
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 428
    invoke-virtual {v1, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 430
    iget-object v3, p0, Lmiui/resourcebrowser/controller/ImportManager;->controller:Lmiui/resourcebrowser/controller/ResourceController;

    invoke-virtual {v3}, Lmiui/resourcebrowser/controller/ResourceController;->getLocalDataManager()Lmiui/resourcebrowser/controller/LocalDataManager;

    move-result-object v3

    invoke-virtual {v3, p1}, Lmiui/resourcebrowser/controller/LocalDataManager;->addResource(Lmiui/resourcebrowser/model/Resource;)Z

    .line 431
    return v2
.end method

.method protected importZip(Lmiui/resourcebrowser/model/Resource;)Z
    .locals 1
    .parameter "resource"

    .prologue
    .line 408
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/controller/ImportManager;->importSingle(Lmiui/resourcebrowser/model/Resource;)Z

    move-result v0

    return v0
.end method

.method protected populateDataByDescription(Lmiui/resourcebrowser/model/Resource;Ljava/util/Map;)V
    .locals 3
    .parameter "resource"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiui/resourcebrowser/model/Resource;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 241
    .local p2, map:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v2, "title"

    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {p1, v2}, Lmiui/resourcebrowser/model/Resource;->setTitle(Ljava/lang/String;)V

    .line 242
    const-string v2, "description"

    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {p1, v2}, Lmiui/resourcebrowser/model/Resource;->setDescription(Ljava/lang/String;)V

    .line 243
    const-string v2, "author"

    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {p1, v2}, Lmiui/resourcebrowser/model/Resource;->setAuthor(Ljava/lang/String;)V

    .line 244
    const-string v2, "designer"

    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {p1, v2}, Lmiui/resourcebrowser/model/Resource;->setDesigner(Ljava/lang/String;)V

    .line 245
    const-string v2, "version"

    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {p1, v2}, Lmiui/resourcebrowser/model/Resource;->setVersion(Ljava/lang/String;)V

    .line 246
    const/4 v0, 0x0

    .line 247
    .local v0, platform:I
    const-string v2, "platform"

    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 248
    .local v1, str:Ljava/lang/String;
    if-nez v1, :cond_0

    .line 249
    const-string v2, "uiVersion"

    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1           #str:Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 251
    .restart local v1       #str:Ljava/lang/String;
    :cond_0
    if-eqz v1, :cond_1

    .line 253
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 258
    :cond_1
    :goto_0
    invoke-virtual {p1, v0}, Lmiui/resourcebrowser/model/Resource;->setPlatform(I)V

    .line 259
    return-void

    .line 254
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method protected resolveBuildInImage(Ljava/io/File;Ljava/lang/String;Lmiui/resourcebrowser/model/Resource;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .parameter "folder"
    .parameter "targetFolder"
    .parameter "resource"
    .parameter "prefix"
    .parameter "ext"

    .prologue
    .line 380
    const/4 v0, 0x0

    .line 382
    .local v0, i:I
    :goto_0
    new-instance v2, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    add-int/lit8 v1, v0, 0x1

    .end local v0           #i:I
    .local v1, i:I
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, p1, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 383
    .local v2, imageFile:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-nez v5, :cond_0

    .line 384
    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    .line 385
    .local v3, name:Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 386
    .local v4, path:Ljava/lang/String;
    invoke-virtual {p3, v4}, Lmiui/resourcebrowser/model/Resource;->addBuildInThumbnail(Ljava/lang/String;)V

    .line 387
    invoke-virtual {p3, v4}, Lmiui/resourcebrowser/model/Resource;->addBuildInPreview(Ljava/lang/String;)V

    .line 388
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v4}, Lmiui/os/Shell;->copy(Ljava/lang/String;Ljava/lang/String;)Z

    move v0, v1

    .line 392
    .end local v1           #i:I
    .restart local v0       #i:I
    goto :goto_0

    .line 393
    .end local v0           #i:I
    .end local v3           #name:Ljava/lang/String;
    .end local v4           #path:Ljava/lang/String;
    .restart local v1       #i:I
    :cond_0
    return-void
.end method

.method protected resolveBuildInImages(Ljava/io/File;Lmiui/resourcebrowser/model/Resource;Ljava/util/List;)V
    .locals 7
    .parameter "folder"
    .parameter "resource"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Lmiui/resourcebrowser/model/Resource;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 366
    .local p3, prefixes:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->clearBuildInThumbnails()V

    .line 367
    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->clearBuildInPreviews()V

    .line 368
    new-instance v1, Ljava/io/File;

    const-string v0, "preview"

    invoke-direct {v1, p1, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 369
    .end local p1
    .local v1, folder:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 370
    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getLocalId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/controller/ImportManager;->getBuildInImageFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 371
    .local v2, targetFolder:Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 372
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 373
    .local v4, prefix:Ljava/lang/String;
    const-string v5, ".jpg"

    move-object v0, p0

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lmiui/resourcebrowser/controller/ImportManager;->resolveBuildInImage(Ljava/io/File;Ljava/lang/String;Lmiui/resourcebrowser/model/Resource;Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    const-string v5, ".png"

    move-object v0, p0

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lmiui/resourcebrowser/controller/ImportManager;->resolveBuildInImage(Ljava/io/File;Ljava/lang/String;Lmiui/resourcebrowser/model/Resource;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 377
    .end local v2           #targetFolder:Ljava/lang/String;
    .end local v4           #prefix:Ljava/lang/String;
    .end local v6           #i$:Ljava/util/Iterator;
    :cond_0
    return-void
.end method

.method protected resolveOnlineId(Lmiui/resourcebrowser/model/Resource;)V
    .locals 9
    .parameter "resource"

    .prologue
    const/4 v8, 0x3

    .line 73
    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getOnlineId()Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_3

    .line 74
    new-instance v1, Ljava/io/File;

    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getDownloadPath()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v1, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 75
    .local v1, downloadFile:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lmiui/resourcebrowser/util/ResourceHelper;->getFileHash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 76
    .local v2, hash:Ljava/lang/String;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 77
    .local v3, hashs:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    iget-object v7, p0, Lmiui/resourcebrowser/controller/ImportManager;->controller:Lmiui/resourcebrowser/controller/ResourceController;

    invoke-virtual {v7}, Lmiui/resourcebrowser/controller/ResourceController;->getOnlineDataManager()Lmiui/resourcebrowser/controller/OnlineDataManager;

    move-result-object v7

    invoke-virtual {v7, v3}, Lmiui/resourcebrowser/controller/OnlineDataManager;->getAssociationResources(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    .line 79
    .local v6, resources:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/Resource;>;"
    if-eqz v6, :cond_0

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    if-lez v7, :cond_0

    .line 80
    const/4 v7, 0x0

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v7}, Lmiui/resourcebrowser/model/Resource;->getOnlineId()Ljava/lang/String;

    move-result-object v5

    .line 81
    .local v5, onlineId:Ljava/lang/String;
    if-eqz v5, :cond_0

    const-string v7, ""

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 82
    iget-object v7, p0, Lmiui/resourcebrowser/controller/ImportManager;->controller:Lmiui/resourcebrowser/controller/ResourceController;

    invoke-virtual {v7}, Lmiui/resourcebrowser/controller/ResourceController;->getOnlineDataManager()Lmiui/resourcebrowser/controller/OnlineDataManager;

    move-result-object v7

    invoke-virtual {v7, v5}, Lmiui/resourcebrowser/controller/OnlineDataManager;->getResource(Ljava/lang/String;)Lmiui/resourcebrowser/model/Resource;

    move-result-object v0

    .line 83
    .local v0, associatedResource:Lmiui/resourcebrowser/model/Resource;
    if-eqz v0, :cond_0

    .line 84
    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getDownloadPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Lmiui/resourcebrowser/model/Resource;->setDownloadPath(Ljava/lang/String;)V

    .line 85
    invoke-virtual {v0, v8}, Lmiui/resourcebrowser/model/Resource;->setStatus(I)V

    .line 86
    invoke-virtual {p1, v0}, Lmiui/resourcebrowser/model/Resource;->updateFrom(Lmiui/resourcebrowser/model/Resource;)V

    .line 90
    .end local v0           #associatedResource:Lmiui/resourcebrowser/model/Resource;
    .end local v5           #onlineId:Ljava/lang/String;
    :cond_0
    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getOnlineId()Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_1

    .line 91
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lmiui/resourcebrowser/util/ResourceHelper;->getFileNameWithoutExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 92
    .local v4, name:Ljava/lang/String;
    const-string v7, "[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}"

    invoke-virtual {v4, v7}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 105
    .end local v1           #downloadFile:Ljava/io/File;
    .end local v2           #hash:Ljava/lang/String;
    .end local v3           #hashs:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .end local v4           #name:Ljava/lang/String;
    .end local v6           #resources:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/Resource;>;"
    :cond_1
    :goto_0
    return-void

    .line 98
    .restart local v1       #downloadFile:Ljava/io/File;
    .restart local v2       #hash:Ljava/lang/String;
    .restart local v3       #hashs:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .restart local v4       #name:Ljava/lang/String;
    .restart local v6       #resources:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/Resource;>;"
    :cond_2
    const/4 v7, 0x1

    invoke-virtual {p1, v7}, Lmiui/resourcebrowser/model/Resource;->setStatus(I)V

    goto :goto_0

    .line 102
    .end local v1           #downloadFile:Ljava/io/File;
    .end local v2           #hash:Ljava/lang/String;
    .end local v3           #hashs:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .end local v4           #name:Ljava/lang/String;
    .end local v6           #resources:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/Resource;>;"
    :cond_3
    iget-object v7, p0, Lmiui/resourcebrowser/controller/ImportManager;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v7}, Lmiui/resourcebrowser/ResourceContext;->getCurrentPlatform()I

    move-result v7

    invoke-virtual {p1, v7}, Lmiui/resourcebrowser/model/Resource;->setPlatform(I)V

    .line 103
    invoke-virtual {p1, v8}, Lmiui/resourcebrowser/model/Resource;->setStatus(I)V

    goto :goto_0
.end method

.method public setResourceController(Lmiui/resourcebrowser/controller/ResourceController;)V
    .locals 0
    .parameter "controller"

    .prologue
    .line 37
    iput-object p1, p0, Lmiui/resourcebrowser/controller/ImportManager;->controller:Lmiui/resourcebrowser/controller/ResourceController;

    .line 38
    return-void
.end method

.method protected unzipBundle(Lmiui/resourcebrowser/model/Resource;)Ljava/lang/String;
    .locals 5
    .parameter "resource"

    .prologue
    .line 125
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lmiui/resourcebrowser/controller/ImportManager;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v4}, Lmiui/resourcebrowser/ResourceContext;->getDownloadFolder()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "temp/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 126
    .local v2, unzipPath:Ljava/lang/String;
    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getOnlineId()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 127
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getOnlineId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 131
    :goto_0
    invoke-static {v2}, Lmiui/os/ExtraFileUtils;->standardizeFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 133
    :try_start_0
    new-instance v1, Lmiui/resourcebrowser/controller/ImportManager$1;

    invoke-direct {v1, p0}, Lmiui/resourcebrowser/controller/ImportManager$1;-><init>(Lmiui/resourcebrowser/controller/ImportManager;)V

    .line 139
    .local v1, listener:Lmiui/resourcebrowser/util/ResourceHelper$UnzipProcessUpdateListener;
    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getDownloadPath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v2, v1}, Lmiui/resourcebrowser/util/ResourceHelper;->unzip(Ljava/lang/String;Ljava/lang/String;Lmiui/resourcebrowser/util/ResourceHelper$UnzipProcessUpdateListener;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 145
    .end local v1           #listener:Lmiui/resourcebrowser/util/ResourceHelper$UnzipProcessUpdateListener;
    .end local v2           #unzipPath:Ljava/lang/String;
    :goto_1
    return-object v2

    .line 129
    .restart local v2       #unzipPath:Ljava/lang/String;
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getDownloadPath()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lmiui/resourcebrowser/util/ResourceHelper;->getFileNameWithoutExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 140
    :catch_0
    move-exception v0

    .line 141
    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 142
    const/4 v2, 0x0

    goto :goto_1
.end method
