.class public Lmiui/resourcebrowser/controller/online/OnlineService;
.super Ljava/lang/Object;
.source "OnlineService.java"


# static fields
.field private static sAccount:Landroid/util/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Pair",
            "<",
            "Landroid/accounts/Account;",
            "Lmiui/net/ExtendedAuthToken;",
            ">;"
        }
    .end annotation
.end field

.field private static sLastGetAccountTime:J

.field private static sVersionSuffixParams:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private context:Lmiui/resourcebrowser/ResourceContext;


# direct methods
.method public constructor <init>(Lmiui/resourcebrowser/ResourceContext;)V
    .locals 0
    .parameter "context"

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lmiui/resourcebrowser/controller/online/OnlineService;->context:Lmiui/resourcebrowser/ResourceContext;

    .line 51
    return-void
.end method

.method public static decryptStream(Ljava/io/InputStream;)Ljava/io/InputStream;
    .locals 8
    .parameter "is"

    .prologue
    .line 169
    invoke-static {}, Lmiui/resourcebrowser/controller/online/OnlineService;->getSecurityKey()Ljava/lang/String;

    move-result-object v3

    .line 170
    .local v3, key:Ljava/lang/String;
    move-object v5, p0

    .line 171
    .local v5, ret:Ljava/io/InputStream;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    if-eqz p0, :cond_0

    .line 173
    :try_start_0
    invoke-static {p0}, Lmiui/resourcebrowser/controller/online/OnlineService;->getContent(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v0

    .line 174
    .local v0, data:Ljava/lang/String;
    const/4 v7, 0x2

    invoke-static {v3, v7}, Lmiui/net/CloudCoder;->newAESCipher(Ljava/lang/String;I)Ljavax/crypto/Cipher;

    move-result-object v1

    .line 175
    .local v1, decoder:Ljavax/crypto/Cipher;
    const/4 v7, 0x2

    invoke-static {v0, v7}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v7

    invoke-virtual {v1, v7}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v4

    .line 176
    .local v4, result:[B
    new-instance v6, Ljava/io/ByteArrayInputStream;

    invoke-direct {v6, v4}, Ljava/io/ByteArrayInputStream;-><init>([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v5           #ret:Ljava/io/InputStream;
    .local v6, ret:Ljava/io/InputStream;
    move-object v5, v6

    .line 181
    .end local v0           #data:Ljava/lang/String;
    .end local v1           #decoder:Ljavax/crypto/Cipher;
    .end local v4           #result:[B
    .end local v6           #ret:Ljava/io/InputStream;
    .restart local v5       #ret:Ljava/io/InputStream;
    :cond_0
    :goto_0
    return-object v5

    .line 177
    :catch_0
    move-exception v2

    .line 178
    .local v2, e:Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private static encryptParameters(Ljava/lang/String;Ljava/util/Map;)V
    .locals 7
    .parameter "security"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 123
    .local p1, params:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v4, 0x1

    invoke-static {p0, v4}, Lmiui/net/CloudCoder;->newAESCipher(Ljava/lang/String;I)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 125
    .local v0, coder:Ljavax/crypto/Cipher;
    :try_start_0
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 126
    .local v3, param:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const-string v6, "UTF-8"

    invoke-virtual {v4, v6}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v4

    invoke-virtual {v0, v4}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v4

    const/4 v6, 0x2

    invoke-static {v4, v6}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 129
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v3           #param:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :catch_0
    move-exception v1

    .line 130
    .local v1, e:Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 132
    .end local v1           #e:Ljava/lang/Exception;
    :cond_0
    return-void
.end method

.method private static generateSignature(Ljava/lang/String;Lmiui/resourcebrowser/controller/online/RequestUrl;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .parameter "httpMethod"
    .parameter "url"
    .parameter "security"

    .prologue
    .line 135
    const/4 v2, 0x0

    .line 137
    .local v2, signature:Ljava/lang/String;
    :try_start_0
    new-instance v1, Ljava/util/TreeMap;

    invoke-virtual {p1}, Lmiui/resourcebrowser/controller/online/RequestUrl;->getParameters()Ljava/util/Map;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/util/TreeMap;-><init>(Ljava/util/Map;)V

    .line 138
    .local v1, params:Ljava/util/TreeMap;,"Ljava/util/TreeMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {p1}, Lmiui/resourcebrowser/controller/online/RequestUrl;->getBaseUrl()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3, v1, p2}, Lmiui/net/CloudCoder;->generateSignature(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 142
    .end local v1           #params:Ljava/util/TreeMap;,"Ljava/util/TreeMap<Ljava/lang/String;Ljava/lang/String;>;"
    :goto_0
    return-object v2

    .line 139
    :catch_0
    move-exception v0

    .line 140
    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static getAccount()Landroid/util/Pair;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Pair",
            "<",
            "Landroid/accounts/Account;",
            "Lmiui/net/ExtendedAuthToken;",
            ">;"
        }
    .end annotation

    .prologue
    .line 70
    invoke-static {}, Lmiui/resourcebrowser/controller/online/OnlineService;->isAccountCacheValid()Z

    move-result v0

    if-nez v0, :cond_1

    .line 71
    const-class v1, Lmiui/resourcebrowser/controller/online/OnlineService;

    monitor-enter v1

    .line 72
    :try_start_0
    invoke-static {}, Lmiui/resourcebrowser/controller/online/OnlineService;->isAccountCacheValid()Z

    move-result v0

    if-nez v0, :cond_0

    .line 73
    invoke-static {}, Lmiui/resourcebrowser/controller/online/OnlineService;->retrieveAccount()Landroid/util/Pair;

    move-result-object v0

    sput-object v0, Lmiui/resourcebrowser/controller/online/OnlineService;->sAccount:Landroid/util/Pair;

    .line 74
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sput-wide v2, Lmiui/resourcebrowser/controller/online/OnlineService;->sLastGetAccountTime:J

    .line 76
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 78
    :cond_1
    sget-object v0, Lmiui/resourcebrowser/controller/online/OnlineService;->sAccount:Landroid/util/Pair;

    return-object v0

    .line 76
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private static getApkVersion()Ljava/lang/String;
    .locals 7

    .prologue
    .line 235
    const/4 v3, 0x0

    .line 236
    .local v3, version:I
    invoke-static {}, Lmiui/resourcebrowser/AppInnerContext;->getInstance()Lmiui/resourcebrowser/AppInnerContext;

    move-result-object v4

    invoke-virtual {v4}, Lmiui/resourcebrowser/AppInnerContext;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 238
    .local v0, context:Landroid/content/Context;
    :try_start_0
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x4000

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 240
    .local v2, pinfo:Landroid/content/pm/PackageInfo;
    iget v3, v2, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 244
    .end local v2           #pinfo:Landroid/content/pm/PackageInfo;
    :goto_0
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 241
    :catch_0
    move-exception v1

    .line 242
    .local v1, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private static getContent(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 5
    .parameter "is"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 194
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 195
    .local v3, sb:Ljava/lang/StringBuilder;
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/InputStreamReader;

    invoke-direct {v4, p0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 196
    .local v1, bufferedReader:Ljava/io/BufferedReader;
    const/16 v4, 0x400

    new-array v0, v4, [C

    .line 197
    .local v0, buf:[C
    const/4 v2, -0x1

    .line 198
    .local v2, i:I
    :goto_0
    invoke-virtual {v1, v0}, Ljava/io/BufferedReader;->read([C)I

    move-result v2

    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    .line 199
    const/4 v4, 0x0

    invoke-virtual {v3, v0, v4, v2}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 201
    :cond_0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method private static getDevice()Ljava/lang/String;
    .locals 3

    .prologue
    .line 222
    const-string v1, "ro.product.mod_device"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 223
    .local v0, modDevice:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v0, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    .end local v0           #modDevice:Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public static getImei()Ljava/lang/String;
    .locals 2

    .prologue
    .line 248
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    .line 249
    .local v0, imei:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 250
    const-string v0, ""

    .line 252
    .end local v0           #imei:Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method private static getLanguage()Ljava/lang/String;
    .locals 2

    .prologue
    .line 218
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getSecureUrl(Lmiui/resourcebrowser/controller/online/RequestUrl;)Lmiui/resourcebrowser/controller/online/RequestUrl;
    .locals 6
    .parameter "url"

    .prologue
    .line 103
    move-object v2, p0

    .line 104
    .local v2, secureUrl:Lmiui/resourcebrowser/controller/online/RequestUrl;
    invoke-static {}, Lmiui/resourcebrowser/controller/online/OnlineService;->getAccount()Landroid/util/Pair;

    move-result-object v0

    .line 105
    .local v0, auth:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/accounts/Account;Lmiui/net/ExtendedAuthToken;>;"
    if-eqz v0, :cond_1

    .line 106
    invoke-virtual {p0}, Lmiui/resourcebrowser/controller/online/RequestUrl;->clone()Ljava/lang/Object;

    move-result-object v2

    .end local v2           #secureUrl:Lmiui/resourcebrowser/controller/online/RequestUrl;
    check-cast v2, Lmiui/resourcebrowser/controller/online/RequestUrl;

    .line 107
    .restart local v2       #secureUrl:Lmiui/resourcebrowser/controller/online/RequestUrl;
    invoke-virtual {p0}, Lmiui/resourcebrowser/controller/online/RequestUrl;->getParameters()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 108
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 109
    .local v1, params:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {v2, v1}, Lmiui/resourcebrowser/controller/online/RequestUrl;->setParameters(Ljava/util/Map;)V

    .line 110
    invoke-virtual {p0}, Lmiui/resourcebrowser/controller/online/RequestUrl;->getParameters()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 111
    const-string v4, "userId"

    iget-object v3, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v1, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    invoke-virtual {p0}, Lmiui/resourcebrowser/controller/online/RequestUrl;->getHttpMethod()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 113
    const-string v4, "serviceToken"

    iget-object v3, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Lmiui/net/ExtendedAuthToken;

    iget-object v3, v3, Lmiui/net/ExtendedAuthToken;->authToken:Ljava/lang/String;

    invoke-interface {v1, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    :cond_0
    iget-object v3, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Lmiui/net/ExtendedAuthToken;

    iget-object v3, v3, Lmiui/net/ExtendedAuthToken;->security:Ljava/lang/String;

    invoke-static {v3, v1}, Lmiui/resourcebrowser/controller/online/OnlineService;->encryptParameters(Ljava/lang/String;Ljava/util/Map;)V

    .line 116
    const-string v4, "signature"

    invoke-virtual {v2}, Lmiui/resourcebrowser/controller/online/RequestUrl;->getHttpMethodName()Ljava/lang/String;

    move-result-object v5

    iget-object v3, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Lmiui/net/ExtendedAuthToken;

    iget-object v3, v3, Lmiui/net/ExtendedAuthToken;->security:Ljava/lang/String;

    invoke-static {v5, v2, v3}, Lmiui/resourcebrowser/controller/online/OnlineService;->generateSignature(Ljava/lang/String;Lmiui/resourcebrowser/controller/online/RequestUrl;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    .end local v1           #params:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_1
    return-object v2
.end method

.method private static getSecurityKey()Ljava/lang/String;
    .locals 3

    .prologue
    .line 185
    const/4 v1, 0x0

    .line 186
    .local v1, key:Ljava/lang/String;
    invoke-static {}, Lmiui/resourcebrowser/controller/online/OnlineService;->getAccount()Landroid/util/Pair;

    move-result-object v0

    .line 187
    .local v0, auth:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/accounts/Account;Lmiui/net/ExtendedAuthToken;>;"
    if-eqz v0, :cond_0

    .line 188
    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Lmiui/net/ExtendedAuthToken;

    iget-object v1, v2, Lmiui/net/ExtendedAuthToken;->security:Ljava/lang/String;

    .line 190
    :cond_0
    return-object v1
.end method

.method private static getSystemType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 227
    const-string v0, "miui"

    return-object v0
.end method

.method private static getSystemVersion()Ljava/lang/String;
    .locals 2

    .prologue
    .line 231
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getUser()Ljava/lang/String;
    .locals 2

    .prologue
    .line 146
    invoke-static {}, Lmiui/resourcebrowser/controller/online/OnlineService;->getAccount()Landroid/util/Pair;

    move-result-object v0

    .line 147
    .local v0, auth:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/accounts/Account;Lmiui/net/ExtendedAuthToken;>;"
    if-eqz v0, :cond_0

    .line 148
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 150
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getUserCookies()Ljava/lang/String;
    .locals 3

    .prologue
    .line 154
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 155
    .local v1, sb:Ljava/lang/StringBuilder;
    invoke-static {}, Lmiui/resourcebrowser/controller/online/OnlineService;->getAccount()Landroid/util/Pair;

    move-result-object v0

    .line 156
    .local v0, auth:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/accounts/Account;Lmiui/net/ExtendedAuthToken;>;"
    if-eqz v0, :cond_0

    .line 157
    const-string v2, "userId"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 158
    const-string v2, "="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 159
    iget-object v2, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 160
    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 161
    const-string v2, "serviceToken"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 162
    const-string v2, "="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 163
    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Lmiui/net/ExtendedAuthToken;

    iget-object v2, v2, Lmiui/net/ExtendedAuthToken;->authToken:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 165
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static getVersionParameters()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 205
    sget-object v0, Lmiui/resourcebrowser/controller/online/OnlineService;->sVersionSuffixParams:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 206
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lmiui/resourcebrowser/controller/online/OnlineService;->sVersionSuffixParams:Ljava/util/Map;

    .line 207
    sget-object v0, Lmiui/resourcebrowser/controller/online/OnlineService;->sVersionSuffixParams:Ljava/util/Map;

    const-string v1, "device"

    invoke-static {}, Lmiui/resourcebrowser/controller/online/OnlineService;->getDevice()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    sget-object v0, Lmiui/resourcebrowser/controller/online/OnlineService;->sVersionSuffixParams:Ljava/util/Map;

    const-string v1, "system"

    invoke-static {}, Lmiui/resourcebrowser/controller/online/OnlineService;->getSystemType()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 209
    sget-object v0, Lmiui/resourcebrowser/controller/online/OnlineService;->sVersionSuffixParams:Ljava/util/Map;

    const-string v1, "version"

    invoke-static {}, Lmiui/resourcebrowser/controller/online/OnlineService;->getSystemVersion()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 210
    sget-object v0, Lmiui/resourcebrowser/controller/online/OnlineService;->sVersionSuffixParams:Ljava/util/Map;

    const-string v1, "apk"

    invoke-static {}, Lmiui/resourcebrowser/controller/online/OnlineService;->getApkVersion()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 211
    sget-object v0, Lmiui/resourcebrowser/controller/online/OnlineService;->sVersionSuffixParams:Ljava/util/Map;

    const-string v1, "imei"

    invoke-static {}, Lmiui/resourcebrowser/controller/online/OnlineService;->getImei()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 212
    sget-object v0, Lmiui/resourcebrowser/controller/online/OnlineService;->sVersionSuffixParams:Ljava/util/Map;

    const-string v1, "language"

    invoke-static {}, Lmiui/resourcebrowser/controller/online/OnlineService;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 214
    :cond_0
    sget-object v0, Lmiui/resourcebrowser/controller/online/OnlineService;->sVersionSuffixParams:Ljava/util/Map;

    return-object v0
.end method

.method private static isAccountCacheValid()Z
    .locals 4

    .prologue
    .line 66
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sget-wide v2, Lmiui/resourcebrowser/controller/online/OnlineService;->sLastGetAccountTime:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x7530

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isNetworkAvailable()Z
    .locals 5

    .prologue
    .line 54
    invoke-static {}, Lmiui/resourcebrowser/AppInnerContext;->getInstance()Lmiui/resourcebrowser/AppInnerContext;

    move-result-object v3

    invoke-virtual {v3}, Lmiui/resourcebrowser/AppInnerContext;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 55
    .local v1, context:Landroid/content/Context;
    const-string v3, "connectivity"

    invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 56
    .local v0, cm:Landroid/net/ConnectivityManager;
    if-eqz v0, :cond_0

    .line 57
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    .line 58
    .local v2, info:Landroid/net/NetworkInfo;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v3

    sget-object v4, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    if-ne v3, v4, :cond_0

    .line 59
    const/4 v3, 0x1

    .line 62
    .end local v2           #info:Landroid/net/NetworkInfo;
    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private static retrieveAccount()Landroid/util/Pair;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Pair",
            "<",
            "Landroid/accounts/Account;",
            "Lmiui/net/ExtendedAuthToken;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v13, 0x0

    .line 82
    invoke-static {}, Lmiui/resourcebrowser/AppInnerContext;->getInstance()Lmiui/resourcebrowser/AppInnerContext;

    move-result-object v2

    invoke-virtual {v2}, Lmiui/resourcebrowser/AppInnerContext;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    .line 83
    .local v8, context:Landroid/content/Context;
    invoke-static {v8}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 84
    .local v0, accountManager:Landroid/accounts/AccountManager;
    const-string v2, "com.xiaomi"

    invoke-virtual {v0, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v7

    .line 85
    .local v7, accounts:[Landroid/accounts/Account;
    if-eqz v7, :cond_0

    array-length v2, v7

    if-lez v2, :cond_0

    .line 87
    const/4 v2, 0x0

    :try_start_0
    aget-object v1, v7, v2

    .line 88
    .local v1, account:Landroid/accounts/Account;
    const-string v2, "thememarket"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Landroid/accounts/AccountManager;->getAuthToken(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;ZLandroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    move-result-object v11

    .line 90
    .local v11, future:Landroid/accounts/AccountManagerFuture;,"Landroid/accounts/AccountManagerFuture<Landroid/os/Bundle;>;"
    invoke-interface {v11}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;

    const-string v3, "authtoken"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 91
    .local v10, extTokenStr:Ljava/lang/String;
    invoke-static {v10}, Lmiui/net/ExtendedAuthToken;->parse(Ljava/lang/String;)Lmiui/net/ExtendedAuthToken;

    move-result-object v12

    .line 92
    .local v12, token:Lmiui/net/ExtendedAuthToken;
    if-eqz v12, :cond_0

    iget-object v2, v12, Lmiui/net/ExtendedAuthToken;->authToken:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, v12, Lmiui/net/ExtendedAuthToken;->security:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 93
    new-instance v2, Landroid/util/Pair;

    invoke-direct {v2, v1, v12}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 99
    .end local v1           #account:Landroid/accounts/Account;
    .end local v10           #extTokenStr:Ljava/lang/String;
    .end local v11           #future:Landroid/accounts/AccountManagerFuture;,"Landroid/accounts/AccountManagerFuture<Landroid/os/Bundle;>;"
    .end local v12           #token:Lmiui/net/ExtendedAuthToken;
    :goto_0
    return-object v2

    .line 95
    :catch_0
    move-exception v9

    .line 96
    .local v9, e:Ljava/lang/Exception;
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    .end local v9           #e:Ljava/lang/Exception;
    :cond_0
    move-object v2, v13

    .line 99
    goto :goto_0
.end method


# virtual methods
.method public getAssociationUrl(Ljava/util/List;)Lmiui/resourcebrowser/controller/online/RequestUrl;
    .locals 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lmiui/resourcebrowser/controller/online/RequestUrl;"
        }
    .end annotation

    .prologue
    .line 328
    .local p1, hashs:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const-string v4, "http://market.xiaomi.com/thm/relatemodule"

    .line 329
    .local v4, url:Ljava/lang/String;
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 330
    .local v2, params:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 331
    .local v3, sb:Ljava/lang/StringBuilder;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 332
    .local v0, hash:Ljava/lang/String;
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 333
    const-string v5, ","

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 335
    .end local v0           #hash:Ljava/lang/String;
    :cond_0
    const-string v5, "fileshash"

    const/4 v6, 0x0

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v2, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 336
    new-instance v5, Lmiui/resourcebrowser/controller/online/RequestUrl;

    invoke-direct {v5, v4, v2}, Lmiui/resourcebrowser/controller/online/RequestUrl;-><init>(Ljava/lang/String;Ljava/util/Map;)V

    return-object v5
.end method

.method public getCategoryUrl()Lmiui/resourcebrowser/controller/online/RequestUrl;
    .locals 5

    .prologue
    .line 304
    iget-object v2, p0, Lmiui/resourcebrowser/controller/online/OnlineService;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v2}, Lmiui/resourcebrowser/ResourceContext;->getResourceStamp()Ljava/lang/String;

    move-result-object v0

    .line 305
    .local v0, stamp:Ljava/lang/String;
    const-string v2, "http://market.xiaomi.com/thm/config/clazz/%s/zh-cn"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 306
    .local v1, url:Ljava/lang/String;
    new-instance v2, Lmiui/resourcebrowser/controller/online/RequestUrl;

    invoke-direct {v2, v1}, Lmiui/resourcebrowser/controller/online/RequestUrl;-><init>(Ljava/lang/String;)V

    return-object v2
.end method

.method public getCommonListUrl(Ljava/lang/String;)Lmiui/resourcebrowser/controller/online/RequestUrl;
    .locals 4
    .parameter "categoryCode"

    .prologue
    .line 256
    const-string v1, "http://market.xiaomi.com/thm/subject/index"

    .line 257
    .local v1, url:Ljava/lang/String;
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 258
    .local v0, params:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v2, "category"

    iget-object v3, p0, Lmiui/resourcebrowser/controller/online/OnlineService;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v3}, Lmiui/resourcebrowser/ResourceContext;->getResourceStamp()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 259
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 260
    const-string v2, "clazz"

    invoke-interface {v0, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 262
    :cond_0
    new-instance v2, Lmiui/resourcebrowser/controller/online/RequestUrl;

    invoke-direct {v2, v1, v0}, Lmiui/resourcebrowser/controller/online/RequestUrl;-><init>(Ljava/lang/String;Ljava/util/Map;)V

    return-object v2
.end method

.method public getDetailUrl(Ljava/lang/String;)Lmiui/resourcebrowser/controller/online/RequestUrl;
    .locals 6
    .parameter "id"

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 290
    invoke-static {}, Lmiui/resourcebrowser/controller/online/OnlineService;->getAccount()Landroid/util/Pair;

    move-result-object v0

    .line 291
    .local v0, auth:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/accounts/Account;Lmiui/net/ExtendedAuthToken;>;"
    const/4 v2, 0x0

    .line 292
    .local v2, url:Ljava/lang/String;
    const/4 v1, 0x0

    .line 293
    .local v1, encryptionNeeded:Z
    if-eqz v0, :cond_0

    .line 294
    const-string v3, "http://market.xiaomi.com/thm/native/details/%s"

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 295
    const/4 v1, 0x1

    .line 300
    :goto_0
    new-instance v3, Lmiui/resourcebrowser/controller/online/RequestUrl;

    invoke-direct {v3, v2, v1, v1}, Lmiui/resourcebrowser/controller/online/RequestUrl;-><init>(Ljava/lang/String;ZZ)V

    return-object v3

    .line 297
    :cond_0
    const-string v3, "http://market.xiaomi.com/thm/details/%s"

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 298
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDownloadUrl(Ljava/lang/String;)Lmiui/resourcebrowser/controller/online/RequestUrl;
    .locals 6
    .parameter "id"

    .prologue
    .line 366
    const-string v3, "http://market.xiaomi.com/thm/download/%s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 367
    .local v2, url:Ljava/lang/String;
    new-instance v1, Lmiui/resourcebrowser/controller/online/RequestUrl;

    invoke-direct {v1, v2}, Lmiui/resourcebrowser/controller/online/RequestUrl;-><init>(Ljava/lang/String;)V

    .line 368
    .local v1, ret:Lmiui/resourcebrowser/controller/online/RequestUrl;
    invoke-static {}, Lmiui/resourcebrowser/controller/online/OnlineService;->getVersionParameters()Ljava/util/Map;

    move-result-object v0

    .line 369
    .local v0, map:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v4, "device"

    const-string v3, "device"

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v1, v4, v3}, Lmiui/resourcebrowser/controller/online/RequestUrl;->putParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    const-string v4, "system"

    const-string v3, "system"

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v1, v4, v3}, Lmiui/resourcebrowser/controller/online/RequestUrl;->putParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    const-string v4, "version"

    const-string v3, "version"

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v1, v4, v3}, Lmiui/resourcebrowser/controller/online/RequestUrl;->putParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 372
    return-object v1
.end method

.method public getListMetaUrl(Ljava/lang/String;)Lmiui/resourcebrowser/controller/online/RequestUrl;
    .locals 4
    .parameter "id"

    .prologue
    .line 285
    const-string v1, "http://market.xiaomi.com/thm/subject/metadata/%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 286
    .local v0, url:Ljava/lang/String;
    new-instance v1, Lmiui/resourcebrowser/controller/online/RequestUrl;

    invoke-direct {v1, v0}, Lmiui/resourcebrowser/controller/online/RequestUrl;-><init>(Ljava/lang/String;)V

    return-object v1
.end method

.method public getRecommendListUrl(Ljava/lang/String;Ljava/lang/String;)Lmiui/resourcebrowser/controller/online/RequestUrl;
    .locals 5
    .parameter "id"
    .parameter "categoryCode"

    .prologue
    .line 275
    const-string v2, "http://market.xiaomi.com/thm/subject/%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 276
    .local v1, url:Ljava/lang/String;
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 277
    .local v0, params:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v2, "category"

    iget-object v3, p0, Lmiui/resourcebrowser/controller/online/OnlineService;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v3}, Lmiui/resourcebrowser/ResourceContext;->getResourceStamp()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 278
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 279
    const-string v2, "clazz"

    invoke-interface {v0, v2, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 281
    :cond_0
    new-instance v2, Lmiui/resourcebrowser/controller/online/RequestUrl;

    invoke-direct {v2, v1, v0}, Lmiui/resourcebrowser/controller/online/RequestUrl;-><init>(Ljava/lang/String;Ljava/util/Map;)V

    return-object v2
.end method

.method public getRecommendUrl()Lmiui/resourcebrowser/controller/online/RequestUrl;
    .locals 5

    .prologue
    .line 310
    iget-object v2, p0, Lmiui/resourcebrowser/controller/online/OnlineService;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v2}, Lmiui/resourcebrowser/ResourceContext;->getResourceStamp()Ljava/lang/String;

    move-result-object v0

    .line 311
    .local v0, stamp:Ljava/lang/String;
    const-string v2, "http://market.xiaomi.com/thm/recommendation/list/%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 312
    .local v1, url:Ljava/lang/String;
    new-instance v2, Lmiui/resourcebrowser/controller/online/RequestUrl;

    invoke-direct {v2, v1}, Lmiui/resourcebrowser/controller/online/RequestUrl;-><init>(Ljava/lang/String;)V

    return-object v2
.end method

.method public getRightsDownloadUrl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lmiui/resourcebrowser/controller/online/RequestUrl;
    .locals 5
    .parameter "id"
    .parameter "hash"
    .parameter "subhash"

    .prologue
    const/4 v4, 0x1

    .line 344
    const-string v2, "http://market.xiaomi.com/thm/native/down"

    .line 345
    .local v2, url:Ljava/lang/String;
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 346
    .local v0, params:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 347
    const-string v3, "productId"

    invoke-interface {v0, v3, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 349
    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 350
    const-string v3, "hash"

    invoke-interface {v0, v3, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 352
    :cond_1
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 353
    const-string v3, "subhash"

    invoke-interface {v0, v3, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 355
    :cond_2
    new-instance v1, Lmiui/resourcebrowser/controller/online/RequestUrl;

    invoke-direct {v1, v2, v0, v4, v4}, Lmiui/resourcebrowser/controller/online/RequestUrl;-><init>(Ljava/lang/String;Ljava/util/Map;ZZ)V

    .line 357
    .local v1, ret:Lmiui/resourcebrowser/controller/online/RequestUrl;
    return-object v1
.end method

.method public getRightsIssueUrl(Ljava/lang/String;)Lmiui/resourcebrowser/controller/online/RequestUrl;
    .locals 3
    .parameter "body"

    .prologue
    .line 361
    const-string v0, "http://drm.market.xiaomi.com/issue"

    .line 362
    .local v0, url:Ljava/lang/String;
    new-instance v1, Lmiui/resourcebrowser/controller/online/RequestUrl;

    const/4 v2, 0x1

    invoke-direct {v1, v0, v2, p1}, Lmiui/resourcebrowser/controller/online/RequestUrl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    return-object v1
.end method

.method public getSearchListUrl(Ljava/lang/String;)Lmiui/resourcebrowser/controller/online/RequestUrl;
    .locals 4
    .parameter "keyword"

    .prologue
    .line 266
    const-string v1, "http://market.xiaomi.com/thm/search"

    .line 267
    .local v1, url:Ljava/lang/String;
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 268
    .local v0, params:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v2, "apiversion"

    const-string v3, "1"

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 269
    const-string v2, "keywords"

    invoke-interface {v0, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 270
    const-string v2, "category"

    iget-object v3, p0, Lmiui/resourcebrowser/controller/online/OnlineService;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v3}, Lmiui/resourcebrowser/ResourceContext;->getResourceStamp()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 271
    new-instance v2, Lmiui/resourcebrowser/controller/online/RequestUrl;

    invoke-direct {v2, v1, v0}, Lmiui/resourcebrowser/controller/online/RequestUrl;-><init>(Ljava/lang/String;Ljava/util/Map;)V

    return-object v2
.end method

.method public getVersionUrl(Ljava/util/List;)Lmiui/resourcebrowser/controller/online/RequestUrl;
    .locals 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lmiui/resourcebrowser/controller/online/RequestUrl;"
        }
    .end annotation

    .prologue
    .line 316
    .local p1, hashs:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const-string v4, "http://market.xiaomi.com/thm/checkupdate"

    .line 317
    .local v4, url:Ljava/lang/String;
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 318
    .local v2, params:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 319
    .local v3, sb:Ljava/lang/StringBuilder;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 320
    .local v0, hash:Ljava/lang/String;
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 321
    const-string v5, ","

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 323
    .end local v0           #hash:Ljava/lang/String;
    :cond_0
    const-string v5, "fileshash"

    const/4 v6, 0x0

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v2, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 324
    new-instance v5, Lmiui/resourcebrowser/controller/online/RequestUrl;

    invoke-direct {v5, v4, v2}, Lmiui/resourcebrowser/controller/online/RequestUrl;-><init>(Ljava/lang/String;Ljava/util/Map;)V

    return-object v5
.end method
