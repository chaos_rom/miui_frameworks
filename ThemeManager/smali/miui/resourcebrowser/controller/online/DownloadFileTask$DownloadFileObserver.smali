.class public interface abstract Lmiui/resourcebrowser/controller/online/DownloadFileTask$DownloadFileObserver;
.super Ljava/lang/Object;
.source "DownloadFileTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/resourcebrowser/controller/online/DownloadFileTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "DownloadFileObserver"
.end annotation


# virtual methods
.method public varargs abstract onAllDownloadsCompleted([Lmiui/resourcebrowser/model/PathEntry;)V
.end method

.method public abstract onDownloadFailed(Lmiui/resourcebrowser/model/PathEntry;)V
.end method

.method public abstract onDownloadSuccessful(Lmiui/resourcebrowser/model/PathEntry;)V
.end method
