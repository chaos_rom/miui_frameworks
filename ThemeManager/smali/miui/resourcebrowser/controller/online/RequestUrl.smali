.class public Lmiui/resourcebrowser/controller/online/RequestUrl;
.super Ljava/lang/Object;
.source "RequestUrl.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static final METHOD_GET:I = 0x0

.field public static final METHOD_POST:I = 0x1

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private baseUrl:Ljava/lang/String;

.field private body:Ljava/lang/String;

.field private decryptionNeeded:Z

.field private encryptionNeeded:Z

.field private httpMethod:I

.field private parameters:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/controller/online/RequestUrl;->parameters:Ljava/util/Map;

    .line 30
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .parameter "baseUrl"

    .prologue
    .line 32
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/controller/online/RequestUrl;->parameters:Ljava/util/Map;

    .line 33
    iput-object p1, p0, Lmiui/resourcebrowser/controller/online/RequestUrl;->baseUrl:Ljava/lang/String;

    .line 34
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .parameter "baseUrl"
    .parameter "httpMethod"
    .parameter "body"

    .prologue
    .line 41
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/controller/online/RequestUrl;->parameters:Ljava/util/Map;

    .line 42
    iput-object p1, p0, Lmiui/resourcebrowser/controller/online/RequestUrl;->baseUrl:Ljava/lang/String;

    .line 43
    iput p2, p0, Lmiui/resourcebrowser/controller/online/RequestUrl;->httpMethod:I

    .line 44
    iput-object p3, p0, Lmiui/resourcebrowser/controller/online/RequestUrl;->body:Ljava/lang/String;

    .line 45
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/Map;)V
    .locals 1
    .parameter "baseUrl"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 36
    .local p2, parameters:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/controller/online/RequestUrl;->parameters:Ljava/util/Map;

    .line 37
    iput-object p1, p0, Lmiui/resourcebrowser/controller/online/RequestUrl;->baseUrl:Ljava/lang/String;

    .line 38
    iput-object p2, p0, Lmiui/resourcebrowser/controller/online/RequestUrl;->parameters:Ljava/util/Map;

    .line 39
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/Map;ZZ)V
    .locals 1
    .parameter "baseUrl"
    .parameter
    .parameter "encryptionNeeded"
    .parameter "decryptionNeeded"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;ZZ)V"
        }
    .end annotation

    .prologue
    .line 53
    .local p2, parameters:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/controller/online/RequestUrl;->parameters:Ljava/util/Map;

    .line 54
    iput-object p1, p0, Lmiui/resourcebrowser/controller/online/RequestUrl;->baseUrl:Ljava/lang/String;

    .line 55
    iput-object p2, p0, Lmiui/resourcebrowser/controller/online/RequestUrl;->parameters:Ljava/util/Map;

    .line 56
    iput-boolean p3, p0, Lmiui/resourcebrowser/controller/online/RequestUrl;->encryptionNeeded:Z

    .line 57
    iput-boolean p4, p0, Lmiui/resourcebrowser/controller/online/RequestUrl;->decryptionNeeded:Z

    .line 58
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ZZ)V
    .locals 1
    .parameter "baseUrl"
    .parameter "encryptionNeeded"
    .parameter "decryptionNeeded"

    .prologue
    .line 47
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/controller/online/RequestUrl;->parameters:Ljava/util/Map;

    .line 48
    iput-object p1, p0, Lmiui/resourcebrowser/controller/online/RequestUrl;->baseUrl:Ljava/lang/String;

    .line 49
    iput-boolean p2, p0, Lmiui/resourcebrowser/controller/online/RequestUrl;->encryptionNeeded:Z

    .line 50
    iput-boolean p3, p0, Lmiui/resourcebrowser/controller/online/RequestUrl;->decryptionNeeded:Z

    .line 51
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 165
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 169
    :goto_0
    return-object v1

    .line 166
    :catch_0
    move-exception v0

    .line 167
    .local v0, e:Ljava/lang/CloneNotSupportedException;
    invoke-virtual {v0}, Ljava/lang/CloneNotSupportedException;->printStackTrace()V

    .line 169
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getBaseUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lmiui/resourcebrowser/controller/online/RequestUrl;->baseUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getBody()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lmiui/resourcebrowser/controller/online/RequestUrl;->body:Ljava/lang/String;

    return-object v0
.end method

.method public getBodyPart()Ljava/lang/String;
    .locals 3

    .prologue
    .line 151
    const/4 v0, 0x0

    .line 152
    .local v0, bodyPart:Ljava/lang/String;
    iget v1, p0, Lmiui/resourcebrowser/controller/online/RequestUrl;->httpMethod:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 153
    iget-object v1, p0, Lmiui/resourcebrowser/controller/online/RequestUrl;->body:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 154
    iget-object v0, p0, Lmiui/resourcebrowser/controller/online/RequestUrl;->body:Ljava/lang/String;

    .line 159
    :cond_0
    :goto_0
    return-object v0

    .line 156
    :cond_1
    invoke-virtual {p0}, Lmiui/resourcebrowser/controller/online/RequestUrl;->getParameterPart()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getHttpMethod()I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lmiui/resourcebrowser/controller/online/RequestUrl;->httpMethod:I

    return v0
.end method

.method public getHttpMethodName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lmiui/resourcebrowser/controller/online/RequestUrl;->httpMethod:I

    if-nez v0, :cond_0

    const-string v0, "GET"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "POST"

    goto :goto_0
.end method

.method public getParameter(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .parameter "key"

    .prologue
    .line 89
    iget-object v0, p0, Lmiui/resourcebrowser/controller/online/RequestUrl;->parameters:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getParameter(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter "key"
    .parameter "defaultValue"

    .prologue
    .line 93
    iget-object v1, p0, Lmiui/resourcebrowser/controller/online/RequestUrl;->parameters:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 94
    .local v0, result:Ljava/lang/String;
    if-eqz v0, :cond_0

    .end local v0           #result:Ljava/lang/String;
    :goto_0
    return-object v0

    .restart local v0       #result:Ljava/lang/String;
    :cond_0
    move-object v0, p2

    goto :goto_0
.end method

.method public getParameterPart()Ljava/lang/String;
    .locals 6

    .prologue
    .line 130
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 131
    .local v3, sb:Ljava/lang/StringBuffer;
    iget-object v4, p0, Lmiui/resourcebrowser/controller/online/RequestUrl;->parameters:Ljava/util/Map;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lmiui/resourcebrowser/controller/online/RequestUrl;->parameters:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    .line 132
    invoke-virtual {p0}, Lmiui/resourcebrowser/controller/online/RequestUrl;->getHttpMethod()I

    move-result v4

    if-nez v4, :cond_0

    .line 133
    const-string v4, "?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 136
    :cond_0
    :try_start_0
    iget-object v4, p0, Lmiui/resourcebrowser/controller/online/RequestUrl;->parameters:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 137
    .local v2, param:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 138
    const-string v4, "="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 139
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const-string v5, "UTF-8"

    invoke-static {v4, v5}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 140
    const-string v4, "&"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 143
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #param:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :catch_0
    move-exception v0

    .line 144
    .local v0, e:Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 147
    .end local v0           #e:Ljava/io/UnsupportedEncodingException;
    :cond_1
    :goto_1
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 142
    .restart local v1       #i$:Ljava/util/Iterator;
    :cond_2
    :try_start_1
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public getParameters()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 81
    iget-object v0, p0, Lmiui/resourcebrowser/controller/online/RequestUrl;->parameters:Ljava/util/Map;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 126
    iget v0, p0, Lmiui/resourcebrowser/controller/online/RequestUrl;->httpMethod:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lmiui/resourcebrowser/controller/online/RequestUrl;->baseUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lmiui/resourcebrowser/controller/online/RequestUrl;->getParameterPart()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lmiui/resourcebrowser/controller/online/RequestUrl;->baseUrl:Ljava/lang/String;

    goto :goto_0
.end method

.method public isDecryptionNeeded()Z
    .locals 1

    .prologue
    .line 118
    iget-boolean v0, p0, Lmiui/resourcebrowser/controller/online/RequestUrl;->decryptionNeeded:Z

    return v0
.end method

.method public isEncryptionNeeded()Z
    .locals 1

    .prologue
    .line 110
    iget-boolean v0, p0, Lmiui/resourcebrowser/controller/online/RequestUrl;->encryptionNeeded:Z

    return v0
.end method

.method public putParameter(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter "key"
    .parameter "value"

    .prologue
    .line 98
    iget-object v0, p0, Lmiui/resourcebrowser/controller/online/RequestUrl;->parameters:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    return-void
.end method

.method public setBaseUrl(Ljava/lang/String;)V
    .locals 0
    .parameter "baseUrl"

    .prologue
    .line 77
    iput-object p1, p0, Lmiui/resourcebrowser/controller/online/RequestUrl;->baseUrl:Ljava/lang/String;

    .line 78
    return-void
.end method

.method public setBody(Ljava/lang/String;)V
    .locals 0
    .parameter "body"

    .prologue
    .line 106
    iput-object p1, p0, Lmiui/resourcebrowser/controller/online/RequestUrl;->body:Ljava/lang/String;

    .line 107
    return-void
.end method

.method public setDecryptionNeeded(Z)V
    .locals 0
    .parameter "decryptionNeeded"

    .prologue
    .line 122
    iput-boolean p1, p0, Lmiui/resourcebrowser/controller/online/RequestUrl;->decryptionNeeded:Z

    .line 123
    return-void
.end method

.method public setEncryptionNeeded(Z)V
    .locals 0
    .parameter "encryptionNeeded"

    .prologue
    .line 114
    iput-boolean p1, p0, Lmiui/resourcebrowser/controller/online/RequestUrl;->encryptionNeeded:Z

    .line 115
    return-void
.end method

.method public setHttpMethod(I)V
    .locals 0
    .parameter "httpMethod"

    .prologue
    .line 69
    iput p1, p0, Lmiui/resourcebrowser/controller/online/RequestUrl;->httpMethod:I

    .line 70
    return-void
.end method

.method public setParameters(Ljava/util/Map;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 85
    .local p1, parameters:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iput-object p1, p0, Lmiui/resourcebrowser/controller/online/RequestUrl;->parameters:Ljava/util/Map;

    .line 86
    return-void
.end method
