.class public Lmiui/resourcebrowser/controller/online/DrmService;
.super Ljava/lang/Object;
.source "DrmService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/resourcebrowser/controller/online/DrmService$1;,
        Lmiui/resourcebrowser/controller/online/DrmService$DrmNSContext;,
        Lmiui/resourcebrowser/controller/online/DrmService$RightObject;
    }
.end annotation


# instance fields
.field private agent:Lmiui/resourcebrowser/jni/DrmAgent;

.field private context:Lmiui/resourcebrowser/ResourceContext;

.field private mCacheHash:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private service:Lmiui/resourcebrowser/controller/online/OnlineService;


# direct methods
.method public constructor <init>(Lmiui/resourcebrowser/ResourceContext;)V
    .locals 2
    .parameter "context"

    .prologue
    .line 62
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 105
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/controller/online/DrmService;->mCacheHash:Ljava/util/HashMap;

    .line 63
    iput-object p1, p0, Lmiui/resourcebrowser/controller/online/DrmService;->context:Lmiui/resourcebrowser/ResourceContext;

    .line 64
    new-instance v0, Lmiui/resourcebrowser/controller/online/OnlineService;

    iget-object v1, p0, Lmiui/resourcebrowser/controller/online/DrmService;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-direct {v0, v1}, Lmiui/resourcebrowser/controller/online/OnlineService;-><init>(Lmiui/resourcebrowser/ResourceContext;)V

    iput-object v0, p0, Lmiui/resourcebrowser/controller/online/DrmService;->service:Lmiui/resourcebrowser/controller/online/OnlineService;

    .line 65
    new-instance v0, Lmiui/resourcebrowser/jni/DrmAgent;

    invoke-direct {v0}, Lmiui/resourcebrowser/jni/DrmAgent;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/controller/online/DrmService;->agent:Lmiui/resourcebrowser/jni/DrmAgent;

    .line 66
    return-void
.end method

.method private getSubResourcesHash(Lmiui/resourcebrowser/model/Resource;)Ljava/lang/String;
    .locals 6
    .parameter "resource"

    .prologue
    .line 107
    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getSubResources()Ljava/util/List;

    move-result-object v4

    .line 108
    .local v4, subResources:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/RelatedResource;>;"
    if-eqz v4, :cond_3

    .line 109
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 110
    .local v0, builder:Ljava/lang/StringBuilder;
    const/4 v3, 0x0

    .local v3, i:I
    :goto_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    if-ge v3, v5, :cond_2

    .line 111
    if-lez v3, :cond_0

    .line 112
    const/16 v5, 0x2c

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 115
    :cond_0
    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lmiui/resourcebrowser/model/RelatedResource;

    invoke-virtual {v5}, Lmiui/resourcebrowser/model/RelatedResource;->getFilePath()Ljava/lang/String;

    move-result-object v1

    .line 116
    .local v1, filePath:Ljava/lang/String;
    iget-object v5, p0, Lmiui/resourcebrowser/controller/online/DrmService;->mCacheHash:Ljava/util/HashMap;

    invoke-virtual {v5, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 120
    .local v2, hash:Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 121
    invoke-static {v1}, Lmiui/resourcebrowser/util/ResourceHelper;->getFileHash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 122
    iget-object v5, p0, Lmiui/resourcebrowser/controller/online/DrmService;->mCacheHash:Ljava/util/HashMap;

    invoke-virtual {v5, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    :cond_1
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 126
    .end local v1           #filePath:Ljava/lang/String;
    .end local v2           #hash:Ljava/lang/String;
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 129
    .end local v0           #builder:Ljava/lang/StringBuilder;
    .end local v3           #i:I
    :goto_1
    return-object v5

    :cond_3
    const/4 v5, 0x0

    goto :goto_1
.end method


# virtual methods
.method public downloadRights(Lmiui/resourcebrowser/model/Resource;)I
    .locals 13
    .parameter "resource"

    .prologue
    const/4 v10, 0x0

    .line 69
    const/4 v5, 0x0

    .line 72
    .local v5, status:I
    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getParentResources()Ljava/util/List;

    move-result-object v3

    .line 73
    .local v3, parents:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/RelatedResource;>;"
    if-eqz v3, :cond_0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v9

    if-lez v9, :cond_0

    .line 74
    invoke-interface {v3, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/resourcebrowser/model/RelatedResource;

    .line 75
    .local v2, parent:Lmiui/resourcebrowser/model/RelatedResource;
    new-instance v4, Lmiui/resourcebrowser/controller/local/LocalJSONDataParser;

    iget-object v9, p0, Lmiui/resourcebrowser/controller/online/DrmService;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-direct {v4, v9}, Lmiui/resourcebrowser/controller/local/LocalJSONDataParser;-><init>(Lmiui/resourcebrowser/ResourceContext;)V

    .line 77
    .local v4, parser:Lmiui/resourcebrowser/controller/local/LocalDataParser;
    :try_start_0
    new-instance v9, Ljava/io/File;

    invoke-virtual {v2}, Lmiui/resourcebrowser/model/RelatedResource;->getLocalPath()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v9}, Lmiui/resourcebrowser/controller/local/LocalJSONDataParser;->loadResource(Ljava/io/File;)Lmiui/resourcebrowser/model/Resource;
    :try_end_0
    .catch Lmiui/resourcebrowser/controller/local/PersistenceException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    .line 83
    .end local v2           #parent:Lmiui/resourcebrowser/model/RelatedResource;
    .end local v4           #parser:Lmiui/resourcebrowser/controller/local/LocalDataParser;
    :cond_0
    :goto_0
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getRightsPath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ".temp"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 84
    .local v7, tempPath:Ljava/lang/String;
    new-instance v6, Lmiui/resourcebrowser/controller/online/DownloadFileTask;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "rights-"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getOnlineId()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, v9}, Lmiui/resourcebrowser/controller/online/DownloadFileTask;-><init>(Ljava/lang/String;)V

    .line 86
    .local v6, task:Lmiui/resourcebrowser/controller/online/DownloadFileTask;
    :try_start_1
    iget-object v9, p0, Lmiui/resourcebrowser/controller/online/DrmService;->service:Lmiui/resourcebrowser/controller/online/OnlineService;

    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getProductId()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getHash()Ljava/lang/String;

    move-result-object v11

    invoke-direct {p0, p1}, Lmiui/resourcebrowser/controller/online/DrmService;->getSubResourcesHash(Lmiui/resourcebrowser/model/Resource;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v10, v11, v12}, Lmiui/resourcebrowser/controller/online/OnlineService;->getRightsDownloadUrl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lmiui/resourcebrowser/controller/online/RequestUrl;

    move-result-object v8

    .line 87
    .local v8, url:Lmiui/resourcebrowser/controller/online/RequestUrl;
    const/4 v9, 0x3

    invoke-virtual {v6, v8, v9}, Lmiui/resourcebrowser/controller/online/DownloadFileTask;->getInputStream(Lmiui/resourcebrowser/controller/online/RequestUrl;I)Ljava/io/InputStream;

    move-result-object v1

    .line 88
    .local v1, is:Ljava/io/InputStream;
    iget-object v9, p0, Lmiui/resourcebrowser/controller/online/DrmService;->service:Lmiui/resourcebrowser/controller/online/OnlineService;

    invoke-static {v1}, Lmiui/resourcebrowser/util/ResourceHelper;->getContent(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lmiui/resourcebrowser/controller/online/OnlineService;->getRightsIssueUrl(Ljava/lang/String;)Lmiui/resourcebrowser/controller/online/RequestUrl;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v6, v9, v7, v10}, Lmiui/resourcebrowser/controller/online/DownloadFileTask;->downloadFile(Lmiui/resourcebrowser/controller/online/RequestUrl;Ljava/lang/String;I)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lmiui/resourcebrowser/controller/online/HttpStatusException; {:try_start_1 .. :try_end_1} :catch_2

    move-result v9

    if-nez v9, :cond_1

    .line 89
    const/4 v5, 0x2

    .line 97
    .end local v1           #is:Ljava/io/InputStream;
    .end local v8           #url:Lmiui/resourcebrowser/controller/online/RequestUrl;
    :cond_1
    :goto_1
    if-nez v5, :cond_2

    .line 98
    iget-object v9, p0, Lmiui/resourcebrowser/controller/online/DrmService;->agent:Lmiui/resourcebrowser/jni/DrmAgent;

    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getRightsPath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v7, v10}, Lmiui/resourcebrowser/jni/DrmAgent;->saveRightObject(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    new-instance v9, Ljava/io/File;

    invoke-direct {v9, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 102
    :cond_2
    return v5

    .line 78
    .end local v6           #task:Lmiui/resourcebrowser/controller/online/DownloadFileTask;
    .end local v7           #tempPath:Ljava/lang/String;
    .restart local v2       #parent:Lmiui/resourcebrowser/model/RelatedResource;
    .restart local v4       #parser:Lmiui/resourcebrowser/controller/local/LocalDataParser;
    :catch_0
    move-exception v0

    .line 79
    .local v0, e:Lmiui/resourcebrowser/controller/local/PersistenceException;
    invoke-virtual {v0}, Lmiui/resourcebrowser/controller/local/PersistenceException;->printStackTrace()V

    goto :goto_0

    .line 91
    .end local v0           #e:Lmiui/resourcebrowser/controller/local/PersistenceException;
    .end local v2           #parent:Lmiui/resourcebrowser/model/RelatedResource;
    .end local v4           #parser:Lmiui/resourcebrowser/controller/local/LocalDataParser;
    .restart local v6       #task:Lmiui/resourcebrowser/controller/online/DownloadFileTask;
    .restart local v7       #tempPath:Ljava/lang/String;
    :catch_1
    move-exception v0

    .line 92
    .local v0, e:Ljava/io/IOException;
    const/4 v5, 0x2

    .line 96
    goto :goto_1

    .line 93
    .end local v0           #e:Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 94
    .local v0, e:Lmiui/resourcebrowser/controller/online/HttpStatusException;
    const/4 v5, 0x1

    .line 95
    invoke-virtual {v0}, Lmiui/resourcebrowser/controller/online/HttpStatusException;->printStackTrace()V

    goto :goto_1
.end method

.method public isLegal(Lmiui/resourcebrowser/model/Resource;)Z
    .locals 14
    .parameter "resource"

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 133
    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getRightsPath()Ljava/lang/String;

    move-result-object v5

    .line 134
    .local v5, rightsPath:Ljava/lang/String;
    if-nez v5, :cond_1

    .line 171
    :cond_0
    :goto_0
    return v11

    .line 137
    :cond_1
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 138
    .local v4, rightsFile:Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v13

    if-eqz v13, :cond_0

    .line 141
    invoke-virtual {p0, v4}, Lmiui/resourcebrowser/controller/online/DrmService;->parseRightsFile(Ljava/io/File;)Lmiui/resourcebrowser/controller/online/DrmService$RightObject;

    move-result-object v6

    .line 143
    .local v6, ro:Lmiui/resourcebrowser/controller/online/DrmService$RightObject;
    const/4 v3, 0x0

    .line 144
    .local v3, match:Z
    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getHash()Ljava/lang/String;

    move-result-object v7

    .line 145
    .local v7, targetAsset:Ljava/lang/String;
    iget-object v13, v6, Lmiui/resourcebrowser/controller/online/DrmService$RightObject;->assets:Ljava/util/List;

    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 146
    .local v0, asset:Ljava/lang/String;
    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 147
    const/4 v3, 0x1

    .line 151
    .end local v0           #asset:Ljava/lang/String;
    :cond_3
    if-eqz v3, :cond_0

    .line 155
    invoke-static {}, Lmiui/resourcebrowser/controller/online/OnlineService;->getImei()Ljava/lang/String;

    move-result-object v8

    .line 156
    .local v8, targetImei:Ljava/lang/String;
    invoke-static {}, Lmiui/resourcebrowser/controller/online/OnlineService;->getUser()Ljava/lang/String;

    move-result-object v9

    .line 157
    .local v9, targetUser:Ljava/lang/String;
    iget-object v13, v6, Lmiui/resourcebrowser/controller/online/DrmService$RightObject;->imeis:Ljava/util/List;

    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 158
    .local v2, imei:Ljava/lang/String;
    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_5

    move v11, v12

    .line 159
    goto :goto_0

    .line 160
    :cond_5
    const-string v13, "-1"

    invoke-virtual {v2, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_4

    move v11, v12

    .line 161
    goto :goto_0

    .line 164
    .end local v2           #imei:Ljava/lang/String;
    :cond_6
    iget-object v13, v6, Lmiui/resourcebrowser/controller/online/DrmService$RightObject;->users:Ljava/util/List;

    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 165
    .local v10, user:Ljava/lang/String;
    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_8

    move v11, v12

    .line 166
    goto :goto_0

    .line 167
    :cond_8
    const-string v13, "-1"

    invoke-virtual {v10, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_7

    move v11, v12

    .line 168
    goto :goto_0
.end method

.method public parseRightsFile(Ljava/io/File;)Lmiui/resourcebrowser/controller/online/DrmService$RightObject;
    .locals 16
    .parameter "file"

    .prologue
    .line 208
    new-instance v10, Lmiui/resourcebrowser/controller/online/DrmService$RightObject;

    const/4 v13, 0x0

    invoke-direct {v10, v13}, Lmiui/resourcebrowser/controller/online/DrmService$RightObject;-><init>(Lmiui/resourcebrowser/controller/online/DrmService$1;)V

    .line 210
    .local v10, ro:Lmiui/resourcebrowser/controller/online/DrmService$RightObject;
    :try_start_0
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v6

    .line 211
    .local v6, factory:Ljavax/xml/parsers/DocumentBuilderFactory;
    const/4 v13, 0x1

    invoke-virtual {v6, v13}, Ljavax/xml/parsers/DocumentBuilderFactory;->setNamespaceAware(Z)V

    .line 212
    invoke-virtual {v6}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v2

    .line 213
    .local v2, builder:Ljavax/xml/parsers/DocumentBuilder;
    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/File;)Lorg/w3c/dom/Document;

    move-result-object v4

    .line 214
    .local v4, document:Lorg/w3c/dom/Document;
    invoke-static {}, Ljavax/xml/xpath/XPathFactory;->newInstance()Ljavax/xml/xpath/XPathFactory;

    move-result-object v12

    .line 215
    .local v12, xpathFactory:Ljavax/xml/xpath/XPathFactory;
    invoke-virtual {v12}, Ljavax/xml/xpath/XPathFactory;->newXPath()Ljavax/xml/xpath/XPath;

    move-result-object v11

    .line 216
    .local v11, xPath:Ljavax/xml/xpath/XPath;
    new-instance v13, Lmiui/resourcebrowser/controller/online/DrmService$DrmNSContext;

    const/4 v14, 0x0

    invoke-direct {v13, v14}, Lmiui/resourcebrowser/controller/online/DrmService$DrmNSContext;-><init>(Lmiui/resourcebrowser/controller/online/DrmService$1;)V

    invoke-interface {v11, v13}, Ljavax/xml/xpath/XPath;->setNamespaceContext(Ljavax/xml/namespace/NamespaceContext;)V

    .line 217
    const-string v13, "/o-ex:rights/o-ex:agreement/o-ex:asset/o-ex:context/o-dd:uid"

    sget-object v14, Ljavax/xml/xpath/XPathConstants;->NODESET:Ljavax/xml/namespace/QName;

    invoke-interface {v11, v13, v4, v14}, Ljavax/xml/xpath/XPath;->evaluate(Ljava/lang/String;Ljava/lang/Object;Ljavax/xml/namespace/QName;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/w3c/dom/NodeList;

    .line 218
    .local v1, assetList:Lorg/w3c/dom/NodeList;
    if-eqz v1, :cond_3

    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v13

    if-lez v13, :cond_3

    .line 219
    const/4 v7, 0x0

    .local v7, i:I
    :goto_0
    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v13

    if-ge v7, v13, :cond_3

    .line 220
    invoke-interface {v1, v7}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v13

    check-cast v13, Lorg/w3c/dom/Element;

    invoke-interface {v13}, Lorg/w3c/dom/Element;->getTextContent()Ljava/lang/String;

    move-result-object v3

    .line 221
    .local v3, content:Ljava/lang/String;
    const-string v13, ":"

    invoke-virtual {v3, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 222
    .local v8, pair:[Ljava/lang/String;
    array-length v13, v8

    const/4 v14, 0x1

    if-ne v13, v14, :cond_1

    .line 223
    iget-object v13, v10, Lmiui/resourcebrowser/controller/online/DrmService$RightObject;->assets:Ljava/util/List;

    const/4 v14, 0x0

    aget-object v14, v8, v14

    const-string v15, ","

    invoke-virtual {v14, v15}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v14

    invoke-interface {v13, v14}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 219
    :cond_0
    :goto_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 224
    :cond_1
    array-length v13, v8

    const/4 v14, 0x2

    if-ne v13, v14, :cond_0

    .line 225
    iget-object v13, v10, Lmiui/resourcebrowser/controller/online/DrmService$RightObject;->assets:Ljava/util/List;

    const/4 v14, 0x0

    aget-object v14, v8, v14

    invoke-interface {v13, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 226
    iget-object v13, v10, Lmiui/resourcebrowser/controller/online/DrmService$RightObject;->assets:Ljava/util/List;

    const/4 v14, 0x1

    aget-object v14, v8, v14

    const-string v15, ","

    invoke-virtual {v14, v15}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v14

    invoke-interface {v13, v14}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xml/sax/SAXException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljavax/xml/xpath/XPathExpressionException; {:try_start_0 .. :try_end_0} :catch_3

    goto :goto_1

    .line 241
    .end local v1           #assetList:Lorg/w3c/dom/NodeList;
    .end local v2           #builder:Ljavax/xml/parsers/DocumentBuilder;
    .end local v3           #content:Ljava/lang/String;
    .end local v4           #document:Lorg/w3c/dom/Document;
    .end local v6           #factory:Ljavax/xml/parsers/DocumentBuilderFactory;
    .end local v7           #i:I
    .end local v8           #pair:[Ljava/lang/String;
    .end local v11           #xPath:Ljavax/xml/xpath/XPath;
    .end local v12           #xpathFactory:Ljavax/xml/xpath/XPathFactory;
    :catch_0
    move-exception v5

    .line 242
    .local v5, e:Ljavax/xml/parsers/ParserConfigurationException;
    invoke-virtual {v5}, Ljavax/xml/parsers/ParserConfigurationException;->printStackTrace()V

    .line 250
    .end local v5           #e:Ljavax/xml/parsers/ParserConfigurationException;
    :cond_2
    :goto_2
    return-object v10

    .line 230
    .restart local v1       #assetList:Lorg/w3c/dom/NodeList;
    .restart local v2       #builder:Ljavax/xml/parsers/DocumentBuilder;
    .restart local v4       #document:Lorg/w3c/dom/Document;
    .restart local v6       #factory:Ljavax/xml/parsers/DocumentBuilderFactory;
    .restart local v11       #xPath:Ljavax/xml/xpath/XPath;
    .restart local v12       #xpathFactory:Ljavax/xml/xpath/XPathFactory;
    :cond_3
    :try_start_1
    const-string v13, "/o-ex:rights/o-ex:agreement/o-ex:permission/o-dd:execute/o-ex:constraint/oma-dd:individual/o-ex:context/o-dd:uid"

    sget-object v14, Ljavax/xml/xpath/XPathConstants;->NODESET:Ljavax/xml/namespace/QName;

    invoke-interface {v11, v13, v4, v14}, Ljavax/xml/xpath/XPath;->evaluate(Ljava/lang/String;Ljava/lang/Object;Ljavax/xml/namespace/QName;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/w3c/dom/NodeList;

    .line 231
    .local v9, permissonList:Lorg/w3c/dom/NodeList;
    if-eqz v9, :cond_2

    invoke-interface {v9}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v13

    if-lez v13, :cond_2

    .line 232
    const/4 v7, 0x0

    .restart local v7       #i:I
    :goto_3
    invoke-interface {v9}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v13

    if-ge v7, v13, :cond_2

    .line 233
    invoke-interface {v9, v7}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v13

    check-cast v13, Lorg/w3c/dom/Element;

    invoke-interface {v13}, Lorg/w3c/dom/Element;->getTextContent()Ljava/lang/String;

    move-result-object v3

    .line 234
    .restart local v3       #content:Ljava/lang/String;
    const-string v13, "d"

    invoke-virtual {v3, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 235
    iget-object v13, v10, Lmiui/resourcebrowser/controller/online/DrmService$RightObject;->imeis:Ljava/util/List;

    const-string v14, "d"

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v14

    invoke-virtual {v3, v14}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v14

    invoke-interface {v13, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 232
    :cond_4
    :goto_4
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 236
    :cond_5
    const-string v13, "m"

    invoke-virtual {v3, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 237
    iget-object v13, v10, Lmiui/resourcebrowser/controller/online/DrmService$RightObject;->users:Ljava/util/List;

    const-string v14, "m"

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v14

    invoke-virtual {v3, v14}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v14

    invoke-interface {v13, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/xml/sax/SAXException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljavax/xml/xpath/XPathExpressionException; {:try_start_1 .. :try_end_1} :catch_3

    goto :goto_4

    .line 243
    .end local v1           #assetList:Lorg/w3c/dom/NodeList;
    .end local v2           #builder:Ljavax/xml/parsers/DocumentBuilder;
    .end local v3           #content:Ljava/lang/String;
    .end local v4           #document:Lorg/w3c/dom/Document;
    .end local v6           #factory:Ljavax/xml/parsers/DocumentBuilderFactory;
    .end local v7           #i:I
    .end local v9           #permissonList:Lorg/w3c/dom/NodeList;
    .end local v11           #xPath:Ljavax/xml/xpath/XPath;
    .end local v12           #xpathFactory:Ljavax/xml/xpath/XPathFactory;
    :catch_1
    move-exception v5

    .line 244
    .local v5, e:Lorg/xml/sax/SAXException;
    invoke-virtual {v5}, Lorg/xml/sax/SAXException;->printStackTrace()V

    goto :goto_2

    .line 245
    .end local v5           #e:Lorg/xml/sax/SAXException;
    :catch_2
    move-exception v5

    .line 246
    .local v5, e:Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 247
    .end local v5           #e:Ljava/io/IOException;
    :catch_3
    move-exception v5

    .line 248
    .local v5, e:Ljavax/xml/xpath/XPathExpressionException;
    invoke-virtual {v5}, Ljavax/xml/xpath/XPathExpressionException;->printStackTrace()V

    goto :goto_2
.end method
