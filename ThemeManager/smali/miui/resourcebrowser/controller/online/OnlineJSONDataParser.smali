.class public Lmiui/resourcebrowser/controller/online/OnlineJSONDataParser;
.super Lmiui/resourcebrowser/controller/online/OnlineDataParser;
.source "OnlineJSONDataParser.java"

# interfaces
.implements Lmiui/resourcebrowser/ResourceConstants;


# direct methods
.method public constructor <init>(Lmiui/resourcebrowser/ResourceContext;)V
    .locals 0
    .parameter "context"

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lmiui/resourcebrowser/controller/online/OnlineDataParser;-><init>(Lmiui/resourcebrowser/ResourceContext;)V

    .line 33
    return-void
.end method

.method private getJSONInformation(Ljava/io/File;)Ljava/lang/String;
    .locals 5
    .parameter "file"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 36
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/InputStreamReader;

    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v3, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    const/16 v4, 0x400

    invoke-direct {v0, v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V

    .line 37
    .local v0, br:Ljava/io/BufferedReader;
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 38
    .local v2, sb:Ljava/lang/StringBuffer;
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    .line 39
    .local v1, line:Ljava/lang/String;
    :goto_0
    if-eqz v1, :cond_0

    .line 40
    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 41
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 43
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method


# virtual methods
.method public buildAssociationResources(Ljava/io/File;)Ljava/util/List;
    .locals 7
    .parameter "file"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 151
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 152
    .local v4, resources:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/Resource;>;"
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {p0, p1}, Lmiui/resourcebrowser/controller/online/OnlineJSONDataParser;->getJSONInformation(Ljava/io/File;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v6}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 155
    .local v1, jsonRoot:Lorg/json/JSONObject;
    invoke-virtual {v1}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v0

    .line 156
    .local v0, iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 157
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 158
    .local v2, key:Ljava/lang/String;
    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 159
    .local v5, value:Ljava/lang/String;
    new-instance v3, Lmiui/resourcebrowser/model/Resource;

    invoke-direct {v3}, Lmiui/resourcebrowser/model/Resource;-><init>()V

    .line 160
    .local v3, resource:Lmiui/resourcebrowser/model/Resource;
    invoke-virtual {v3, v2}, Lmiui/resourcebrowser/model/Resource;->setHash(Ljava/lang/String;)V

    .line 161
    invoke-virtual {v3, v5}, Lmiui/resourcebrowser/model/Resource;->setOnlineId(Ljava/lang/String;)V

    .line 162
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 164
    .end local v2           #key:Ljava/lang/String;
    .end local v3           #resource:Lmiui/resourcebrowser/model/Resource;
    .end local v5           #value:Ljava/lang/String;
    :cond_0
    return-object v4
.end method

.method public buildCategories(Ljava/io/File;)Ljava/util/List;
    .locals 8
    .parameter "file"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/ResourceCategory;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 49
    .local v0, categories:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/ResourceCategory;>;"
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {p0, p1}, Lmiui/resourcebrowser/controller/online/OnlineJSONDataParser;->getJSONInformation(Ljava/io/File;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v7}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 51
    .local v5, jsonRoot:Lorg/json/JSONObject;
    const-string v7, "clazzNameMap"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 52
    .local v3, jsonCategories:Lorg/json/JSONObject;
    const-string v7, "sortList"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    .line 53
    .local v4, jsonOrder:Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, i:I
    :goto_0
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v2, v7, :cond_0

    .line 54
    new-instance v1, Lmiui/resourcebrowser/model/ResourceCategory;

    invoke-direct {v1}, Lmiui/resourcebrowser/model/ResourceCategory;-><init>()V

    .line 55
    .local v1, category:Lmiui/resourcebrowser/model/ResourceCategory;
    invoke-virtual {v4, v2}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 56
    .local v6, order:Ljava/lang/String;
    invoke-virtual {v1, v6}, Lmiui/resourcebrowser/model/ResourceCategory;->setCode(Ljava/lang/String;)V

    .line 57
    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Lmiui/resourcebrowser/model/ResourceCategory;->setName(Ljava/lang/String;)V

    .line 58
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 53
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 60
    .end local v1           #category:Lmiui/resourcebrowser/model/ResourceCategory;
    .end local v6           #order:Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public buildRecommends(Ljava/io/File;)Ljava/util/List;
    .locals 18
    .parameter "file"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/RecommendItemData;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 65
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 66
    .local v11, recommends:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/RecommendItemData;>;"
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct/range {p0 .. p1}, Lmiui/resourcebrowser/controller/online/OnlineJSONDataParser;->getJSONInformation(Ljava/io/File;)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v7, v14}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 68
    .local v7, jsonRoot:Lorg/json/JSONObject;
    move-object/from16 v0, p0

    iget-object v14, v0, Lmiui/resourcebrowser/controller/online/OnlineDataParser;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v14}, Lmiui/resourcebrowser/ResourceContext;->getRecommendImageCacheFolder()Ljava/lang/String;

    move-result-object v1

    .line 69
    .local v1, cacheFolder:Ljava/lang/String;
    const-string v14, "jpeg/w%s/"

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/resourcebrowser/controller/online/OnlineDataParser;->context:Lmiui/resourcebrowser/ResourceContext;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lmiui/resourcebrowser/ResourceContext;->getRecommendImageWidth()I

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 71
    .local v3, imagePrefix:Ljava/lang/String;
    const-string v14, "recommendations"

    invoke-virtual {v7, v14}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    .line 72
    .local v6, jsonRecommends:Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, i:I
    :goto_0
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v14

    if-ge v2, v14, :cond_1

    .line 73
    new-instance v10, Lmiui/resourcebrowser/model/RecommendItemData;

    invoke-direct {v10}, Lmiui/resourcebrowser/model/RecommendItemData;-><init>()V

    .line 74
    .local v10, recommend:Lmiui/resourcebrowser/model/RecommendItemData;
    invoke-virtual {v6, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 75
    .local v5, jsonRecommend:Lorg/json/JSONObject;
    const-string v14, "picUrlRoot"

    invoke-virtual {v5, v14}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 76
    .local v13, urlRoot:Ljava/lang/String;
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "picUrl"

    invoke-virtual {v5, v15}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    iput-object v14, v10, Lmiui/resourcebrowser/model/RecommendItemData;->onlineThumbnail:Ljava/lang/String;

    .line 77
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget-object v15, v10, Lmiui/resourcebrowser/model/RecommendItemData;->onlineThumbnail:Ljava/lang/String;

    invoke-static {v15}, Lmiui/resourcebrowser/util/ResourceHelper;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    iput-object v14, v10, Lmiui/resourcebrowser/model/RecommendItemData;->localThumbnail:Ljava/lang/String;

    .line 78
    const-string v14, "type"

    invoke-virtual {v5, v14}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v14

    iput v14, v10, Lmiui/resourcebrowser/model/RecommendItemData;->itemType:I

    .line 79
    const-string v14, "relatedId"

    invoke-virtual {v5, v14}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v10, Lmiui/resourcebrowser/model/RecommendItemData;->itemId:Ljava/lang/String;

    .line 80
    const-string v14, "title"

    invoke-virtual {v5, v14}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v10, Lmiui/resourcebrowser/model/RecommendItemData;->title:Ljava/lang/String;

    .line 81
    const-string v14, "layoutCol"

    invoke-virtual {v5, v14}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v14

    iput v14, v10, Lmiui/resourcebrowser/view/UnevenGrid$GridItemData;->widthCount:I

    .line 82
    const-string v14, "layoutRow"

    invoke-virtual {v5, v14}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v14

    iput v14, v10, Lmiui/resourcebrowser/view/UnevenGrid$GridItemData;->heightCount:I

    .line 83
    const-string v14, "category"

    invoke-virtual {v7, v14}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v10, Lmiui/resourcebrowser/model/RecommendItemData;->type:Ljava/lang/String;

    .line 84
    const-string v14, "subjects"

    invoke-virtual {v5, v14}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v9

    .line 85
    .local v9, jsonSubs:Lorg/json/JSONArray;
    const/4 v4, 0x0

    .local v4, j:I
    :goto_1
    invoke-virtual {v9}, Lorg/json/JSONArray;->length()I

    move-result v14

    if-ge v4, v14, :cond_0

    .line 86
    new-instance v12, Lmiui/resourcebrowser/model/RecommendItemData;

    invoke-direct {v12}, Lmiui/resourcebrowser/model/RecommendItemData;-><init>()V

    .line 87
    .local v12, sub:Lmiui/resourcebrowser/model/RecommendItemData;
    invoke-virtual {v9, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v8

    .line 88
    .local v8, jsonSub:Lorg/json/JSONObject;
    const-string v14, "id"

    invoke-virtual {v8, v14}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v12, Lmiui/resourcebrowser/model/RecommendItemData;->itemId:Ljava/lang/String;

    .line 89
    const-string v14, "name"

    invoke-virtual {v8, v14}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v12, Lmiui/resourcebrowser/model/RecommendItemData;->title:Ljava/lang/String;

    .line 90
    iget-object v14, v10, Lmiui/resourcebrowser/model/RecommendItemData;->subItems:Ljava/util/List;

    invoke-interface {v14, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 85
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 92
    .end local v8           #jsonSub:Lorg/json/JSONObject;
    .end local v12           #sub:Lmiui/resourcebrowser/model/RecommendItemData;
    :cond_0
    invoke-interface {v11, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 94
    .end local v4           #j:I
    .end local v5           #jsonRecommend:Lorg/json/JSONObject;
    .end local v9           #jsonSubs:Lorg/json/JSONArray;
    .end local v10           #recommend:Lmiui/resourcebrowser/model/RecommendItemData;
    .end local v13           #urlRoot:Ljava/lang/String;
    :cond_1
    return-object v11
.end method

.method public buildResource(Ljava/io/File;)Lmiui/resourcebrowser/model/Resource;
    .locals 3
    .parameter "file"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 124
    const/4 v1, 0x0

    .line 125
    .local v1, resource:Lmiui/resourcebrowser/model/Resource;
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {p0, p1}, Lmiui/resourcebrowser/controller/online/OnlineJSONDataParser;->getJSONInformation(Ljava/io/File;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 127
    .local v0, jsonRoot:Lorg/json/JSONObject;
    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/controller/online/OnlineJSONDataParser;->buildResource(Lorg/json/JSONObject;)Lmiui/resourcebrowser/model/Resource;

    move-result-object v1

    .line 128
    return-object v1
.end method

.method protected buildResource(Lorg/json/JSONObject;)Lmiui/resourcebrowser/model/Resource;
    .locals 30
    .parameter "json"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 168
    new-instance v18, Lmiui/resourcebrowser/model/Resource;

    invoke-direct/range {v18 .. v18}, Lmiui/resourcebrowser/model/Resource;-><init>()V

    .line 170
    .local v18, resource:Lmiui/resourcebrowser/model/Resource;
    const-string v26, "jpeg/w%s/"

    const/16 v27, 0x1

    move/from16 v0, v27

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/resourcebrowser/controller/online/OnlineDataParser;->context:Lmiui/resourcebrowser/ResourceContext;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Lmiui/resourcebrowser/ResourceContext;->getThumbnailImageWidth()I

    move-result v29

    invoke-static/range {v29 .. v29}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v29

    aput-object v29, v27, v28

    invoke-static/range {v26 .. v27}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    .line 171
    .local v22, thumbnailPrefix:Ljava/lang/String;
    const-string v26, "jpeg/w%s/"

    const/16 v27, 0x1

    move/from16 v0, v27

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/resourcebrowser/controller/online/OnlineDataParser;->context:Lmiui/resourcebrowser/ResourceContext;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Lmiui/resourcebrowser/ResourceContext;->getPreviewImageWidth()I

    move-result v29

    invoke-static/range {v29 .. v29}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v29

    aput-object v29, v27, v28

    invoke-static/range {v26 .. v27}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    .line 173
    .local v14, previewPrefix:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/resourcebrowser/controller/online/OnlineDataParser;->context:Lmiui/resourcebrowser/ResourceContext;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lmiui/resourcebrowser/ResourceContext;->getResourceFormat()I

    move-result v26

    const/16 v27, 0x3

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_0

    .line 174
    const-string v22, ""

    .line 177
    :cond_0
    const-string v26, "moduleId"

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 178
    .local v7, id:Ljava/lang/String;
    const-string v26, "productId"

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 180
    .local v16, productId:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/resourcebrowser/controller/online/OnlineDataParser;->controller:Lmiui/resourcebrowser/controller/ResourceController;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lmiui/resourcebrowser/controller/ResourceController;->getLocalDataManager()Lmiui/resourcebrowser/controller/LocalDataManager;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v7}, Lmiui/resourcebrowser/controller/LocalDataManager;->getResourceByOnlineId(Ljava/lang/String;)Lmiui/resourcebrowser/model/Resource;

    move-result-object v17

    .line 181
    .local v17, r:Lmiui/resourcebrowser/model/Resource;
    if-eqz v17, :cond_1

    .line 182
    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/model/Resource;->updateFrom(Lmiui/resourcebrowser/model/Resource;)V

    .line 186
    :cond_1
    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Lmiui/resourcebrowser/model/Resource;->setOnlineId(Ljava/lang/String;)V

    .line 189
    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/model/Resource;->setProductId(Ljava/lang/String;)V

    .line 191
    const-string v26, "downloadUrlRoot"

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 194
    .local v25, urlRoot:Ljava/lang/String;
    new-instance v26, Lmiui/resourcebrowser/controller/online/OnlineService;

    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/resourcebrowser/controller/online/OnlineDataParser;->context:Lmiui/resourcebrowser/ResourceContext;

    move-object/from16 v27, v0

    invoke-direct/range {v26 .. v27}, Lmiui/resourcebrowser/controller/online/OnlineService;-><init>(Lmiui/resourcebrowser/ResourceContext;)V

    move-object/from16 v0, v26

    invoke-virtual {v0, v7}, Lmiui/resourcebrowser/controller/online/OnlineService;->getDownloadUrl(Ljava/lang/String;)Lmiui/resourcebrowser/controller/online/RequestUrl;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lmiui/resourcebrowser/controller/online/RequestUrl;->getUrl()Ljava/lang/String;

    move-result-object v11

    .line 195
    .local v11, onlinePath:Ljava/lang/String;
    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Lmiui/resourcebrowser/model/Resource;->setOnlinePath(Ljava/lang/String;)V

    .line 198
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/resourcebrowser/controller/online/OnlineDataParser;->context:Lmiui/resourcebrowser/ResourceContext;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lmiui/resourcebrowser/ResourceContext;->getDownloadFolder()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/resourcebrowser/controller/online/OnlineDataParser;->context:Lmiui/resourcebrowser/ResourceContext;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lmiui/resourcebrowser/ResourceContext;->getResourceExtension()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 199
    .local v4, downloadPath:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/resourcebrowser/controller/online/OnlineDataParser;->context:Lmiui/resourcebrowser/ResourceContext;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lmiui/resourcebrowser/ResourceContext;->isSelfDescribing()Z

    move-result v26

    if-eqz v26, :cond_3

    .line 200
    const-string v26, "name"

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 201
    .local v24, title:Ljava/lang/String;
    const-string v26, ""

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-nez v26, :cond_2

    .line 202
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/resourcebrowser/controller/online/OnlineDataParser;->context:Lmiui/resourcebrowser/ResourceContext;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lmiui/resourcebrowser/ResourceContext;->getDownloadFolder()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/resourcebrowser/controller/online/OnlineDataParser;->context:Lmiui/resourcebrowser/ResourceContext;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lmiui/resourcebrowser/ResourceContext;->getResourceExtension()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 204
    :cond_2
    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Lmiui/resourcebrowser/model/Resource;->setLocalPath(Ljava/lang/String;)V

    .line 205
    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Lmiui/resourcebrowser/model/Resource;->setFilePath(Ljava/lang/String;)V

    .line 207
    .end local v24           #title:Ljava/lang/String;
    :cond_3
    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Lmiui/resourcebrowser/model/Resource;->setDownloadPath(Ljava/lang/String;)V

    .line 210
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/resourcebrowser/controller/online/OnlineDataParser;->context:Lmiui/resourcebrowser/ResourceContext;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lmiui/resourcebrowser/ResourceContext;->getRightsFolder()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, ".mra"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 211
    .local v19, rightsPath:Ljava/lang/String;
    invoke-virtual/range {v18 .. v19}, Lmiui/resourcebrowser/model/Resource;->setRightsPath(Ljava/lang/String;)V

    .line 214
    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    .line 215
    .local v23, thumbnails:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/PathEntry;>;"
    const-string v26, "frontCover"

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 216
    .local v21, thumbnailName:Ljava/lang/String;
    new-instance v20, Lmiui/resourcebrowser/model/PathEntry;

    invoke-direct/range {v20 .. v20}, Lmiui/resourcebrowser/model/PathEntry;-><init>()V

    .line 217
    .local v20, thumbnail:Lmiui/resourcebrowser/model/PathEntry;
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/resourcebrowser/controller/online/OnlineDataParser;->context:Lmiui/resourcebrowser/ResourceContext;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lmiui/resourcebrowser/ResourceContext;->getThumbnailCacheFolder()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-static/range {v21 .. v21}, Lmiui/resourcebrowser/util/ResourceHelper;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v20

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/model/PathEntry;->setLocalPath(Ljava/lang/String;)V

    .line 218
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v20

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/model/PathEntry;->setOnlinePath(Ljava/lang/String;)V

    .line 219
    move-object/from16 v0, v23

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 220
    move-object/from16 v0, v18

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/model/Resource;->setThumbnails(Ljava/util/List;)V

    .line 223
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 224
    .local v15, previews:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/PathEntry;>;"
    const-string v26, "snapshotsUrl"

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v9

    .line 225
    .local v9, jsonPreviews:Lorg/json/JSONArray;
    if-eqz v9, :cond_4

    .line 226
    const/4 v6, 0x0

    .local v6, i:I
    :goto_0
    invoke-virtual {v9}, Lorg/json/JSONArray;->length()I

    move-result v26

    move/from16 v0, v26

    if-ge v6, v0, :cond_4

    .line 227
    invoke-virtual {v9, v6}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 228
    .local v13, previewName:Ljava/lang/String;
    new-instance v12, Lmiui/resourcebrowser/model/PathEntry;

    invoke-direct {v12}, Lmiui/resourcebrowser/model/PathEntry;-><init>()V

    .line 229
    .local v12, preview:Lmiui/resourcebrowser/model/PathEntry;
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/resourcebrowser/controller/online/OnlineDataParser;->context:Lmiui/resourcebrowser/ResourceContext;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lmiui/resourcebrowser/ResourceContext;->getPreviewCacheFolder()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-static {v13}, Lmiui/resourcebrowser/util/ResourceHelper;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v12, v0}, Lmiui/resourcebrowser/model/PathEntry;->setLocalPath(Ljava/lang/String;)V

    .line 230
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v12, v0}, Lmiui/resourcebrowser/model/PathEntry;->setOnlinePath(Ljava/lang/String;)V

    .line 231
    invoke-interface {v15, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 226
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 234
    .end local v6           #i:I
    .end local v12           #preview:Lmiui/resourcebrowser/model/PathEntry;
    .end local v13           #previewName:Ljava/lang/String;
    :cond_4
    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Lmiui/resourcebrowser/model/Resource;->setPreviews(Ljava/util/List;)V

    .line 237
    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/resourcebrowser/controller/online/OnlineDataParser;->context:Lmiui/resourcebrowser/ResourceContext;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lmiui/resourcebrowser/ResourceContext;->getCurrentPlatform()I

    move-result v26

    move-object/from16 v0, v18

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/model/Resource;->setPlatform(I)V

    .line 238
    if-eqz v17, :cond_7

    invoke-virtual/range {v17 .. v17}, Lmiui/resourcebrowser/model/Resource;->getStatus()I

    move-result v26

    or-int/lit8 v26, v26, 0x2

    :goto_1
    move-object/from16 v0, v18

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/model/Resource;->setStatus(I)V

    .line 240
    const-string v26, "fileSize"

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v26

    move-object/from16 v0, v18

    move-wide/from16 v1, v26

    invoke-virtual {v0, v1, v2}, Lmiui/resourcebrowser/model/Resource;->setSize(J)V

    .line 241
    const-string v26, "modifyTime"

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v26

    move-object/from16 v0, v18

    move-wide/from16 v1, v26

    invoke-virtual {v0, v1, v2}, Lmiui/resourcebrowser/model/Resource;->setUpdatedTime(J)V

    .line 243
    const-string v26, "name"

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v18

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/model/Resource;->setTitle(Ljava/lang/String;)V

    .line 244
    const-string v26, "description"

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v18

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/model/Resource;->setDescription(Ljava/lang/String;)V

    .line 245
    const-string v26, "author"

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v18

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/model/Resource;->setAuthor(Ljava/lang/String;)V

    .line 246
    const-string v26, "designer"

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v18

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/model/Resource;->setDesigner(Ljava/lang/String;)V

    .line 247
    const-string v26, "version"

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v18

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/model/Resource;->setVersion(Ljava/lang/String;)V

    .line 248
    const-string v26, "purchaseCount"

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 249
    .local v3, downCnt:Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v26

    if-eqz v26, :cond_5

    .line 250
    const-string v26, "downloads"

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v26

    invoke-static/range {v26 .. v27}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    .line 252
    :cond_5
    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Lmiui/resourcebrowser/model/Resource;->setDownloadCount(Ljava/lang/String;)V

    .line 254
    const-string v26, "productPrice"

    const/16 v27, -0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v26

    move-object/from16 v0, v18

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/model/Resource;->setProductPrice(I)V

    .line 255
    const-string v26, "productBought"

    const/16 v27, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v26

    move-object/from16 v0, v18

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/model/Resource;->setProductBought(Z)V

    .line 257
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 258
    .local v5, handledKeys:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    const-string v26, "moduleId"

    move-object/from16 v0, v26

    invoke-interface {v5, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 259
    const-string v26, "downloadUrlRoot"

    move-object/from16 v0, v26

    invoke-interface {v5, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 260
    const-string v26, "frontCover"

    move-object/from16 v0, v26

    invoke-interface {v5, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 261
    const-string v26, "snapshotsUrl"

    move-object/from16 v0, v26

    invoke-interface {v5, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 262
    const-string v26, "fileSize"

    move-object/from16 v0, v26

    invoke-interface {v5, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 263
    const-string v26, "modifyTime"

    move-object/from16 v0, v26

    invoke-interface {v5, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 264
    const-string v26, "name"

    move-object/from16 v0, v26

    invoke-interface {v5, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 265
    const-string v26, "brief"

    move-object/from16 v0, v26

    invoke-interface {v5, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 266
    const-string v26, "author"

    move-object/from16 v0, v26

    invoke-interface {v5, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 267
    const-string v26, "designer"

    move-object/from16 v0, v26

    invoke-interface {v5, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 268
    const-string v26, "version"

    move-object/from16 v0, v26

    invoke-interface {v5, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 269
    const-string v26, "downloads"

    move-object/from16 v0, v26

    invoke-interface {v5, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 271
    const-string v26, "createTime"

    move-object/from16 v0, v26

    invoke-interface {v5, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 272
    const-string v26, "description"

    move-object/from16 v0, v26

    invoke-interface {v5, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 273
    const-string v26, "tags"

    move-object/from16 v0, v26

    invoke-interface {v5, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 274
    const-string v26, "hot"

    move-object/from16 v0, v26

    invoke-interface {v5, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 275
    const-string v26, "star"

    move-object/from16 v0, v26

    invoke-interface {v5, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 276
    const-string v26, "starCount"

    move-object/from16 v0, v26

    invoke-interface {v5, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 277
    const-string v26, "starCountSize"

    move-object/from16 v0, v26

    invoke-interface {v5, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 278
    const-string v26, "snapshotsUrlSize"

    move-object/from16 v0, v26

    invoke-interface {v5, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 280
    const-string v26, "productId"

    move-object/from16 v0, v26

    invoke-interface {v5, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 281
    const-string v26, "productPrice"

    move-object/from16 v0, v26

    invoke-interface {v5, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 282
    const-string v26, "productBought"

    move-object/from16 v0, v26

    invoke-interface {v5, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 285
    invoke-virtual/range {p1 .. p1}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v8

    .line 286
    .local v8, iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_6
    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v26

    if-eqz v26, :cond_8

    .line 287
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 288
    .local v10, key:Ljava/lang/String;
    invoke-interface {v5, v10}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v26

    if-nez v26, :cond_6

    .line 289
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v18

    move-object/from16 v1, v26

    invoke-virtual {v0, v10, v1}, Lmiui/resourcebrowser/model/Resource;->putExtraMeta(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 238
    .end local v3           #downCnt:Ljava/lang/String;
    .end local v5           #handledKeys:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    .end local v8           #iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v10           #key:Ljava/lang/String;
    :cond_7
    const/16 v26, 0x2

    goto/16 :goto_1

    .line 293
    .restart local v3       #downCnt:Ljava/lang/String;
    .restart local v5       #handledKeys:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    .restart local v8       #iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_8
    return-object v18
.end method

.method public buildResourceListMeta(Ljava/io/File;)Lmiui/resourcebrowser/model/ResourceListMeta;
    .locals 3
    .parameter "file"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 99
    new-instance v1, Lmiui/resourcebrowser/model/ResourceListMeta;

    invoke-direct {v1}, Lmiui/resourcebrowser/model/ResourceListMeta;-><init>()V

    .line 100
    .local v1, resourceListMeta:Lmiui/resourcebrowser/model/ResourceListMeta;
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {p0, p1}, Lmiui/resourcebrowser/controller/online/OnlineJSONDataParser;->getJSONInformation(Ljava/io/File;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 102
    .local v0, jsonRoot:Lorg/json/JSONObject;
    const-string v2, "name"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmiui/resourcebrowser/model/ResourceListMeta;->setName(Ljava/lang/String;)V

    .line 103
    const-string v2, "description"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmiui/resourcebrowser/model/ResourceListMeta;->setDescription(Ljava/lang/String;)V

    .line 104
    const-string v2, "category"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmiui/resourcebrowser/model/ResourceListMeta;->setCategory(Ljava/lang/String;)V

    .line 105
    return-object v1
.end method

.method public buildResources(Ljava/io/File;)Ljava/util/List;
    .locals 7
    .parameter "file"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 110
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 111
    .local v5, resources:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/Resource;>;"
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {p0, p1}, Lmiui/resourcebrowser/controller/online/OnlineJSONDataParser;->getJSONInformation(Ljava/io/File;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v6}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 113
    .local v2, jsonRoot:Lorg/json/JSONObject;
    invoke-virtual {v2}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 114
    .local v3, key:Ljava/lang/String;
    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 115
    .local v1, jsonResources:Lorg/json/JSONArray;
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v0, v6, :cond_0

    .line 116
    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    invoke-virtual {p0, v6}, Lmiui/resourcebrowser/controller/online/OnlineJSONDataParser;->buildResource(Lorg/json/JSONObject;)Lmiui/resourcebrowser/model/Resource;

    move-result-object v4

    .line 117
    .local v4, resource:Lmiui/resourcebrowser/model/Resource;
    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 115
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 119
    .end local v4           #resource:Lmiui/resourcebrowser/model/Resource;
    :cond_0
    return-object v5
.end method

.method public buildUpdatableResources(Ljava/io/File;)Ljava/util/List;
    .locals 7
    .parameter "file"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 133
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 134
    .local v4, resources:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/Resource;>;"
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {p0, p1}, Lmiui/resourcebrowser/controller/online/OnlineJSONDataParser;->getJSONInformation(Ljava/io/File;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v6}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 137
    .local v1, jsonRoot:Lorg/json/JSONObject;
    invoke-virtual {v1}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v0

    .line 138
    .local v0, iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 139
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 140
    .local v2, key:Ljava/lang/String;
    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 141
    .local v5, value:Ljava/lang/String;
    new-instance v3, Lmiui/resourcebrowser/model/Resource;

    invoke-direct {v3}, Lmiui/resourcebrowser/model/Resource;-><init>()V

    .line 142
    .local v3, resource:Lmiui/resourcebrowser/model/Resource;
    invoke-virtual {v3, v2}, Lmiui/resourcebrowser/model/Resource;->setHash(Ljava/lang/String;)V

    .line 143
    invoke-virtual {v3, v5}, Lmiui/resourcebrowser/model/Resource;->setOnlineId(Ljava/lang/String;)V

    .line 144
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 146
    .end local v2           #key:Ljava/lang/String;
    .end local v3           #resource:Lmiui/resourcebrowser/model/Resource;
    .end local v5           #value:Ljava/lang/String;
    :cond_0
    return-object v4
.end method
