.class public Lmiui/resourcebrowser/controller/online/DownloadFileTask;
.super Ljava/lang/Object;
.source "DownloadFileTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/resourcebrowser/controller/online/DownloadFileTask$DownloadFileObserver;
    }
.end annotation


# instance fields
.field private id:Ljava/lang/String;

.field private observers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/controller/online/DownloadFileTask$DownloadFileObserver;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .parameter "id"

    .prologue
    .line 109
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/controller/online/DownloadFileTask;->observers:Ljava/util/List;

    .line 110
    iput-object p1, p0, Lmiui/resourcebrowser/controller/online/DownloadFileTask;->id:Ljava/lang/String;

    .line 111
    return-void
.end method

.method private equals(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .parameter "obj1"
    .parameter "obj2"

    .prologue
    .line 261
    if-eq p1, p2, :cond_0

    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getUrlInputStream(Lmiui/resourcebrowser/controller/online/RequestUrl;Ljava/util/Map;)Ljava/io/InputStream;
    .locals 14
    .parameter "url"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiui/resourcebrowser/controller/online/RequestUrl;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/io/InputStream;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lmiui/resourcebrowser/controller/online/HttpStatusException;
        }
    .end annotation

    .prologue
    .local p1, headers:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/16 v13, 0x4e20

    .line 53
    const/4 v8, 0x0

    .line 54
    .local v8, httpRequest:Lorg/apache/http/client/methods/HttpUriRequest;
    invoke-virtual {p0}, Lmiui/resourcebrowser/controller/online/RequestUrl;->getHttpMethod()I

    move-result v11

    if-nez v11, :cond_0

    .line 55
    new-instance v4, Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {p0}, Lmiui/resourcebrowser/controller/online/RequestUrl;->getUrl()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v4, v11}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 56
    .local v4, httpGet:Lorg/apache/http/client/methods/HttpGet;
    move-object v8, v4

    .line 63
    .end local v4           #httpGet:Lorg/apache/http/client/methods/HttpGet;
    :goto_0
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_1

    .line 64
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, i$:Ljava/util/Iterator;
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 65
    .local v3, header:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    invoke-direct {v5, v11, v12}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    .local v5, httpHeader:Lorg/apache/http/Header;
    invoke-interface {v8, v5}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Lorg/apache/http/Header;)V

    goto :goto_1

    .line 58
    .end local v3           #header:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v5           #httpHeader:Lorg/apache/http/Header;
    .end local v9           #i$:Ljava/util/Iterator;
    :cond_0
    new-instance v7, Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {p0}, Lmiui/resourcebrowser/controller/online/RequestUrl;->getBaseUrl()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v7, v11}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 59
    .local v7, httpPost:Lorg/apache/http/client/methods/HttpPost;
    new-instance v2, Lorg/apache/http/entity/StringEntity;

    invoke-virtual {p0}, Lmiui/resourcebrowser/controller/online/RequestUrl;->getBodyPart()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v2, v11}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;)V

    .line 60
    .local v2, entity:Lorg/apache/http/HttpEntity;
    invoke-virtual {v7, v2}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 61
    move-object v8, v7

    goto :goto_0

    .line 69
    .end local v2           #entity:Lorg/apache/http/HttpEntity;
    .end local v7           #httpPost:Lorg/apache/http/client/methods/HttpPost;
    :cond_1
    new-instance v6, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v6}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 70
    .local v6, httpParams:Lorg/apache/http/params/HttpParams;
    invoke-static {v6, v13}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 71
    invoke-static {v6, v13}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 72
    new-instance v0, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v0, v6}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/params/HttpParams;)V

    .line 73
    .local v0, client:Lorg/apache/http/client/HttpClient;
    invoke-interface {v0, v8}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v10

    .line 74
    .local v10, response:Lorg/apache/http/HttpResponse;
    invoke-interface {v10}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v11

    invoke-interface {v11}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v1

    .line 75
    .local v1, code:I
    const/16 v11, 0x190

    if-lt v1, v11, :cond_2

    .line 76
    new-instance v11, Lmiui/resourcebrowser/controller/online/HttpStatusException;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "http response code is "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Lmiui/resourcebrowser/controller/online/HttpStatusException;-><init>(Ljava/lang/String;)V

    throw v11

    .line 78
    :cond_2
    new-instance v2, Lorg/apache/http/entity/BufferedHttpEntity;

    invoke-interface {v10}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v11

    invoke-direct {v2, v11}, Lorg/apache/http/entity/BufferedHttpEntity;-><init>(Lorg/apache/http/HttpEntity;)V

    .line 79
    .local v2, entity:Lorg/apache/http/entity/BufferedHttpEntity;
    invoke-virtual {v2}, Lorg/apache/http/entity/BufferedHttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v11

    return-object v11
.end method

.method private varargs notifyAllDownloadsCompleted([Lmiui/resourcebrowser/model/PathEntry;)V
    .locals 3
    .parameter "entries"

    .prologue
    .line 138
    iget-object v2, p0, Lmiui/resourcebrowser/controller/online/DownloadFileTask;->observers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/resourcebrowser/controller/online/DownloadFileTask$DownloadFileObserver;

    .line 139
    .local v1, observer:Lmiui/resourcebrowser/controller/online/DownloadFileTask$DownloadFileObserver;
    invoke-interface {v1, p1}, Lmiui/resourcebrowser/controller/online/DownloadFileTask$DownloadFileObserver;->onAllDownloadsCompleted([Lmiui/resourcebrowser/model/PathEntry;)V

    goto :goto_0

    .line 141
    .end local v1           #observer:Lmiui/resourcebrowser/controller/online/DownloadFileTask$DownloadFileObserver;
    :cond_0
    return-void
.end method

.method private notifyDownloadFailed(Lmiui/resourcebrowser/model/PathEntry;)V
    .locals 3
    .parameter "entry"

    .prologue
    .line 132
    iget-object v2, p0, Lmiui/resourcebrowser/controller/online/DownloadFileTask;->observers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/resourcebrowser/controller/online/DownloadFileTask$DownloadFileObserver;

    .line 133
    .local v1, observer:Lmiui/resourcebrowser/controller/online/DownloadFileTask$DownloadFileObserver;
    invoke-interface {v1, p1}, Lmiui/resourcebrowser/controller/online/DownloadFileTask$DownloadFileObserver;->onDownloadFailed(Lmiui/resourcebrowser/model/PathEntry;)V

    goto :goto_0

    .line 135
    .end local v1           #observer:Lmiui/resourcebrowser/controller/online/DownloadFileTask$DownloadFileObserver;
    :cond_0
    return-void
.end method

.method private notifyDownloadSuccessful(Lmiui/resourcebrowser/model/PathEntry;)V
    .locals 3
    .parameter "entry"

    .prologue
    .line 126
    iget-object v2, p0, Lmiui/resourcebrowser/controller/online/DownloadFileTask;->observers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/resourcebrowser/controller/online/DownloadFileTask$DownloadFileObserver;

    .line 127
    .local v1, observer:Lmiui/resourcebrowser/controller/online/DownloadFileTask$DownloadFileObserver;
    invoke-interface {v1, p1}, Lmiui/resourcebrowser/controller/online/DownloadFileTask$DownloadFileObserver;->onDownloadSuccessful(Lmiui/resourcebrowser/model/PathEntry;)V

    goto :goto_0

    .line 129
    .end local v1           #observer:Lmiui/resourcebrowser/controller/online/DownloadFileTask$DownloadFileObserver;
    :cond_0
    return-void
.end method

.method private writeToFile(Lmiui/resourcebrowser/controller/online/RequestUrl;Ljava/io/File;I)V
    .locals 7
    .parameter "url"
    .parameter "file"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lmiui/resourcebrowser/controller/online/HttpStatusException;
        }
    .end annotation

    .prologue
    const/4 v6, -0x1

    .line 191
    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_0

    .line 192
    invoke-virtual {p2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z

    .line 193
    invoke-virtual {p2}, Ljava/io/File;->createNewFile()Z

    .line 196
    :cond_0
    invoke-virtual {p0, p1, p3}, Lmiui/resourcebrowser/controller/online/DownloadFileTask;->getInputStream(Lmiui/resourcebrowser/controller/online/RequestUrl;I)Ljava/io/InputStream;

    move-result-object v2

    .line 197
    .local v2, is:Ljava/io/InputStream;
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 198
    .local v3, os:Ljava/io/OutputStream;
    const/16 v4, 0x400

    new-array v0, v4, [B

    .line 199
    .local v0, buffer:[B
    const/4 v1, 0x0

    .line 200
    .local v1, bytesRead:I
    :goto_0
    invoke-virtual {v2, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    if-ltz v1, :cond_1

    .line 201
    const/4 v4, 0x0

    invoke-virtual {v3, v0, v4, v1}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_0

    .line 203
    :cond_1
    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x1ff

    invoke-static {v4, v5, v6, v6}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    .line 204
    return-void
.end method


# virtual methods
.method public downloadFile(Lmiui/resourcebrowser/controller/online/RequestUrl;Ljava/lang/String;I)Z
    .locals 6
    .parameter "url"
    .parameter "path"
    .parameter "flags"

    .prologue
    .line 172
    new-instance v1, Lmiui/resourcebrowser/model/PathEntry;

    invoke-virtual {p1}, Lmiui/resourcebrowser/controller/online/RequestUrl;->getUrl()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, p2, v4}, Lmiui/resourcebrowser/model/PathEntry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    .local v1, entry:Lmiui/resourcebrowser/model/PathEntry;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 174
    .local v2, file:Ljava/io/File;
    new-instance v3, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".temp"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 176
    .local v3, tempFile:Ljava/io/File;
    :try_start_0
    invoke-direct {p0, p1, v3, p3}, Lmiui/resourcebrowser/controller/online/DownloadFileTask;->writeToFile(Lmiui/resourcebrowser/controller/online/RequestUrl;Ljava/io/File;I)V

    .line 177
    invoke-virtual {v3, v2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 178
    invoke-direct {p0, v1}, Lmiui/resourcebrowser/controller/online/DownloadFileTask;->notifyDownloadSuccessful(Lmiui/resourcebrowser/model/PathEntry;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 187
    const/4 v4, 0x1

    :goto_0
    return v4

    .line 179
    :catch_0
    move-exception v0

    .line 180
    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 181
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 182
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 184
    :cond_0
    invoke-direct {p0, v1}, Lmiui/resourcebrowser/controller/online/DownloadFileTask;->notifyDownloadFailed(Lmiui/resourcebrowser/model/PathEntry;)V

    .line 185
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public downloadFile(Lmiui/resourcebrowser/model/PathEntry;I)Z
    .locals 2
    .parameter "entry"
    .parameter "flags"

    .prologue
    .line 164
    new-instance v0, Lmiui/resourcebrowser/controller/online/RequestUrl;

    invoke-virtual {p1}, Lmiui/resourcebrowser/model/PathEntry;->getOnlinePath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lmiui/resourcebrowser/controller/online/RequestUrl;-><init>(Ljava/lang/String;)V

    .line 165
    .local v0, requestUrl:Lmiui/resourcebrowser/controller/online/RequestUrl;
    and-int/lit8 v1, p2, 0x2

    if-eqz v1, :cond_0

    .line 166
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/controller/online/RequestUrl;->setEncryptionNeeded(Z)V

    .line 168
    :cond_0
    invoke-virtual {p1}, Lmiui/resourcebrowser/model/PathEntry;->getLocalPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, p2}, Lmiui/resourcebrowser/controller/online/DownloadFileTask;->downloadFile(Lmiui/resourcebrowser/controller/online/RequestUrl;Ljava/lang/String;I)Z

    move-result v1

    return v1
.end method

.method public varargs downloadFiles([Lmiui/resourcebrowser/model/PathEntry;)Z
    .locals 4
    .parameter "entries"

    .prologue
    const/4 v2, 0x0

    .line 152
    if-nez p1, :cond_0

    .line 160
    :goto_0
    return v2

    .line 155
    :cond_0
    const/4 v1, 0x1

    .line 156
    .local v1, result:Z
    const/4 v0, 0x0

    .local v0, i:I
    :goto_1
    array-length v3, p1

    if-ge v0, v3, :cond_2

    .line 157
    if-eqz v1, :cond_1

    aget-object v3, p1, v0

    invoke-virtual {p0, v3, v2}, Lmiui/resourcebrowser/controller/online/DownloadFileTask;->downloadFile(Lmiui/resourcebrowser/model/PathEntry;I)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v1, 0x1

    .line 156
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move v1, v2

    .line 157
    goto :goto_2

    .line 159
    :cond_2
    invoke-direct {p0, p1}, Lmiui/resourcebrowser/controller/online/DownloadFileTask;->notifyAllDownloadsCompleted([Lmiui/resourcebrowser/model/PathEntry;)V

    move v2, v1

    .line 160
    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .parameter "obj"

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 244
    if-ne p0, p1, :cond_1

    .line 257
    :cond_0
    :goto_0
    return v1

    .line 247
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 248
    goto :goto_0

    .line 250
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 251
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 253
    check-cast v0, Lmiui/resourcebrowser/controller/online/DownloadFileTask;

    .line 254
    .local v0, other:Lmiui/resourcebrowser/controller/online/DownloadFileTask;
    iget-object v3, p0, Lmiui/resourcebrowser/controller/online/DownloadFileTask;->id:Ljava/lang/String;

    iget-object v4, v0, Lmiui/resourcebrowser/controller/online/DownloadFileTask;->id:Ljava/lang/String;

    invoke-direct {p0, v3, v4}, Lmiui/resourcebrowser/controller/online/DownloadFileTask;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 255
    goto :goto_0
.end method

.method public getInputStream(Lmiui/resourcebrowser/controller/online/RequestUrl;I)Ljava/io/InputStream;
    .locals 6
    .parameter "url"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lmiui/resourcebrowser/controller/online/HttpStatusException;
        }
    .end annotation

    .prologue
    .line 207
    const/4 v0, 0x0

    .line 209
    .local v0, headers:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    and-int/lit8 v4, p2, 0x1

    if-eqz v4, :cond_0

    .line 210
    invoke-virtual {p1}, Lmiui/resourcebrowser/controller/online/RequestUrl;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/resourcebrowser/controller/online/RequestUrl;

    .line 211
    .local v2, newUrl:Lmiui/resourcebrowser/controller/online/RequestUrl;
    new-instance v3, Ljava/util/HashMap;

    invoke-virtual {p1}, Lmiui/resourcebrowser/controller/online/RequestUrl;->getParameters()Ljava/util/Map;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 212
    .local v3, params:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-static {}, Lmiui/resourcebrowser/controller/online/OnlineService;->getVersionParameters()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 213
    invoke-virtual {v2, v3}, Lmiui/resourcebrowser/controller/online/RequestUrl;->setParameters(Ljava/util/Map;)V

    .line 214
    move-object p1, v2

    .line 217
    .end local v2           #newUrl:Lmiui/resourcebrowser/controller/online/RequestUrl;
    .end local v3           #params:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_0
    and-int/lit8 v4, p2, 0x2

    if-eqz v4, :cond_1

    .line 218
    new-instance v0, Ljava/util/HashMap;

    .end local v0           #headers:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 219
    .restart local v0       #headers:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v4, "Cookie"

    invoke-static {}, Lmiui/resourcebrowser/controller/online/OnlineService;->getUserCookies()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 222
    :cond_1
    invoke-virtual {p1}, Lmiui/resourcebrowser/controller/online/RequestUrl;->isEncryptionNeeded()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 223
    invoke-static {p1}, Lmiui/resourcebrowser/controller/online/OnlineService;->getSecureUrl(Lmiui/resourcebrowser/controller/online/RequestUrl;)Lmiui/resourcebrowser/controller/online/RequestUrl;

    move-result-object p1

    .line 226
    :cond_2
    invoke-static {p1, v0}, Lmiui/resourcebrowser/controller/online/DownloadFileTask;->getUrlInputStream(Lmiui/resourcebrowser/controller/online/RequestUrl;Ljava/util/Map;)Ljava/io/InputStream;

    move-result-object v1

    .line 227
    .local v1, is:Ljava/io/InputStream;
    invoke-virtual {p1}, Lmiui/resourcebrowser/controller/online/RequestUrl;->isDecryptionNeeded()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 228
    invoke-static {v1}, Lmiui/resourcebrowser/controller/online/OnlineService;->decryptStream(Ljava/io/InputStream;)Ljava/io/InputStream;

    move-result-object v1

    .line 231
    :cond_3
    return-object v1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 236
    const/16 v0, 0x1f

    .line 237
    .local v0, prime:I
    const/4 v1, 0x1

    .line 238
    .local v1, result:I
    iget-object v2, p0, Lmiui/resourcebrowser/controller/online/DownloadFileTask;->id:Ljava/lang/String;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    add-int/lit8 v1, v2, 0x1f

    .line 239
    return v1

    .line 238
    :cond_0
    iget-object v2, p0, Lmiui/resourcebrowser/controller/online/DownloadFileTask;->id:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0
.end method
