.class public Lmiui/resourcebrowser/controller/online/HttpServerDataManager;
.super Lmiui/resourcebrowser/controller/OnlineDataManager;
.source "HttpServerDataManager.java"

# interfaces
.implements Lmiui/resourcebrowser/ResourceConstants;


# instance fields
.field private associationResources:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;"
        }
    .end annotation
.end field

.field private categories:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/ResourceCategory;",
            ">;"
        }
    .end annotation
.end field

.field private dataSet:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;>;"
        }
    .end annotation
.end field

.field private listMeta:Lmiui/resourcebrowser/model/ResourceListMeta;

.field private localIdIndex:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;"
        }
    .end annotation
.end field

.field private onlineIdIndex:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;"
        }
    .end annotation
.end field

.field private parser:Lmiui/resourcebrowser/controller/online/OnlineDataParser;

.field private recommends:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/RecommendItemData;",
            ">;"
        }
    .end annotation
.end field

.field private service:Lmiui/resourcebrowser/controller/online/OnlineService;

.field private updatableResources:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lmiui/resourcebrowser/ResourceContext;)V
    .locals 1
    .parameter "context"

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lmiui/resourcebrowser/controller/OnlineDataManager;-><init>(Lmiui/resourcebrowser/ResourceContext;)V

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->dataSet:Ljava/util/List;

    .line 35
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->localIdIndex:Ljava/util/Map;

    .line 36
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->onlineIdIndex:Ljava/util/Map;

    .line 49
    invoke-direct {p0}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->getDataParser()Lmiui/resourcebrowser/controller/online/OnlineDataParser;

    move-result-object v0

    iput-object v0, p0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->parser:Lmiui/resourcebrowser/controller/online/OnlineDataParser;

    .line 50
    invoke-direct {p0}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->getService()Lmiui/resourcebrowser/controller/online/OnlineService;

    move-result-object v0

    iput-object v0, p0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->service:Lmiui/resourcebrowser/controller/online/OnlineService;

    .line 51
    return-void
.end method

.method private composeAssociationPath(Ljava/util/List;)Ljava/lang/String;
    .locals 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 351
    .local p1, hashs:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 352
    .local v2, sb:Ljava/lang/StringBuilder;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 353
    .local v0, hash:Ljava/lang/String;
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 354
    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 356
    .end local v0           #hash:Ljava/lang/String;
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lmiui/resourcebrowser/controller/DataManager;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v4}, Lmiui/resourcebrowser/ResourceContext;->getAssociationCacheFolder()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private composeAssociationUrl(Ljava/util/List;)Lmiui/resourcebrowser/controller/online/RequestUrl;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lmiui/resourcebrowser/controller/online/RequestUrl;"
        }
    .end annotation

    .prologue
    .line 347
    .local p1, hashs:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->service:Lmiui/resourcebrowser/controller/online/OnlineService;

    invoke-virtual {v0, p1}, Lmiui/resourcebrowser/controller/online/OnlineService;->getAssociationUrl(Ljava/util/List;)Lmiui/resourcebrowser/controller/online/RequestUrl;

    move-result-object v0

    return-object v0
.end method

.method private composeCategoryPath()Ljava/lang/String;
    .locals 2

    .prologue
    .line 257
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lmiui/resourcebrowser/controller/DataManager;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v1}, Lmiui/resourcebrowser/ResourceContext;->getCategoryCacheFolder()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "category"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private composeCategoryUrl()Lmiui/resourcebrowser/controller/online/RequestUrl;
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->service:Lmiui/resourcebrowser/controller/online/OnlineService;

    invoke-virtual {v0}, Lmiui/resourcebrowser/controller/online/OnlineService;->getCategoryUrl()Lmiui/resourcebrowser/controller/online/RequestUrl;

    move-result-object v0

    return-object v0
.end method

.method private composeDetailPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter "id"

    .prologue
    .line 188
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lmiui/resourcebrowser/controller/DataManager;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v1}, Lmiui/resourcebrowser/ResourceContext;->getDetailCacheFolder()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private composeDetailUrl(Ljava/lang/String;)Lmiui/resourcebrowser/controller/online/RequestUrl;
    .locals 1
    .parameter "id"

    .prologue
    .line 184
    iget-object v0, p0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->service:Lmiui/resourcebrowser/controller/online/OnlineService;

    invoke-virtual {v0, p1}, Lmiui/resourcebrowser/controller/online/OnlineService;->getDetailUrl(Ljava/lang/String;)Lmiui/resourcebrowser/controller/online/RequestUrl;

    move-result-object v0

    return-object v0
.end method

.method private composeListMetaPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter "id"

    .prologue
    .line 225
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lmiui/resourcebrowser/controller/DataManager;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v1}, Lmiui/resourcebrowser/ResourceContext;->getListCacheFolder()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private composeListMetaUrl(Ljava/lang/String;)Lmiui/resourcebrowser/controller/online/RequestUrl;
    .locals 1
    .parameter "id"

    .prologue
    .line 221
    iget-object v0, p0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->service:Lmiui/resourcebrowser/controller/online/OnlineService;

    invoke-virtual {v0, p1}, Lmiui/resourcebrowser/controller/online/OnlineService;->getListMetaUrl(Ljava/lang/String;)Lmiui/resourcebrowser/controller/online/RequestUrl;

    move-result-object v0

    return-object v0
.end method

.method private composeListPath(I)Ljava/lang/String;
    .locals 2
    .parameter "page"

    .prologue
    .line 145
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lmiui/resourcebrowser/controller/DataManager;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v1}, Lmiui/resourcebrowser/ResourceContext;->getListCacheFolder()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0, p1}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->composeListUrl(I)Lmiui/resourcebrowser/controller/online/RequestUrl;

    move-result-object v1

    invoke-virtual {v1}, Lmiui/resourcebrowser/controller/online/RequestUrl;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lmiui/resourcebrowser/util/ResourceHelper;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private composeListUrl(I)Lmiui/resourcebrowser/controller/online/RequestUrl;
    .locals 4
    .parameter "page"

    .prologue
    .line 134
    iget-object v2, p0, Lmiui/resourcebrowser/controller/DataManager;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v2}, Lmiui/resourcebrowser/ResourceContext;->getListUrl()Lmiui/resourcebrowser/controller/online/RequestUrl;

    move-result-object v2

    invoke-virtual {v2}, Lmiui/resourcebrowser/controller/online/RequestUrl;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/resourcebrowser/controller/online/RequestUrl;

    .line 135
    .local v1, url:Lmiui/resourcebrowser/controller/online/RequestUrl;
    new-instance v2, Ljava/util/HashMap;

    iget-object v3, p0, Lmiui/resourcebrowser/controller/DataManager;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v3}, Lmiui/resourcebrowser/ResourceContext;->getListUrl()Lmiui/resourcebrowser/controller/online/RequestUrl;

    move-result-object v3

    invoke-virtual {v3}, Lmiui/resourcebrowser/controller/online/RequestUrl;->getParameters()Ljava/util/Map;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    invoke-virtual {v1, v2}, Lmiui/resourcebrowser/controller/online/RequestUrl;->setParameters(Ljava/util/Map;)V

    .line 137
    iget-object v2, p0, Lmiui/resourcebrowser/controller/DataManager;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v2}, Lmiui/resourcebrowser/ResourceContext;->getPageItemCount()I

    move-result v0

    .line 138
    .local v0, pageItemCount:I
    const-string v2, "start"

    mul-int v3, p1, v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lmiui/resourcebrowser/controller/online/RequestUrl;->putParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    const-string v2, "count"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lmiui/resourcebrowser/controller/online/RequestUrl;->putParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    return-object v1
.end method

.method private composeRecommendPath()Ljava/lang/String;
    .locals 2

    .prologue
    .line 289
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lmiui/resourcebrowser/controller/DataManager;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v1}, Lmiui/resourcebrowser/ResourceContext;->getRecommendCacheFolder()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "recommend"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private composeRecommendUrl()Lmiui/resourcebrowser/controller/online/RequestUrl;
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->service:Lmiui/resourcebrowser/controller/online/OnlineService;

    invoke-virtual {v0}, Lmiui/resourcebrowser/controller/online/OnlineService;->getRecommendUrl()Lmiui/resourcebrowser/controller/online/RequestUrl;

    move-result-object v0

    return-object v0
.end method

.method private composeVersionPath(Ljava/util/List;)Ljava/lang/String;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 321
    .local p1, hashs:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lmiui/resourcebrowser/controller/DataManager;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v1}, Lmiui/resourcebrowser/ResourceContext;->getVersionCacheFolder()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "version"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private composeVersionUrl(Ljava/util/List;)Lmiui/resourcebrowser/controller/online/RequestUrl;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lmiui/resourcebrowser/controller/online/RequestUrl;"
        }
    .end annotation

    .prologue
    .line 317
    .local p1, hashs:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->service:Lmiui/resourcebrowser/controller/online/OnlineService;

    invoke-virtual {v0, p1}, Lmiui/resourcebrowser/controller/online/OnlineService;->getVersionUrl(Ljava/util/List;)Lmiui/resourcebrowser/controller/online/RequestUrl;

    move-result-object v0

    return-object v0
.end method

.method private getDataParser()Lmiui/resourcebrowser/controller/online/OnlineDataParser;
    .locals 2

    .prologue
    .line 60
    new-instance v0, Lmiui/resourcebrowser/controller/online/OnlineJSONDataParser;

    iget-object v1, p0, Lmiui/resourcebrowser/controller/DataManager;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-direct {v0, v1}, Lmiui/resourcebrowser/controller/online/OnlineJSONDataParser;-><init>(Lmiui/resourcebrowser/ResourceContext;)V

    return-object v0
.end method

.method private getService()Lmiui/resourcebrowser/controller/online/OnlineService;
    .locals 2

    .prologue
    .line 64
    new-instance v0, Lmiui/resourcebrowser/controller/online/OnlineService;

    iget-object v1, p0, Lmiui/resourcebrowser/controller/DataManager;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-direct {v0, v1}, Lmiui/resourcebrowser/controller/online/OnlineService;-><init>(Lmiui/resourcebrowser/ResourceContext;)V

    return-object v0
.end method

.method private needRefresh(Ljava/lang/String;J)Z
    .locals 4
    .parameter "path"
    .parameter "invalidTime"

    .prologue
    .line 68
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    sub-long/2addr v0, v2

    cmp-long v0, v0, p2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private refreshFileCache(Lmiui/resourcebrowser/controller/online/RequestUrl;Ljava/lang/String;)Z
    .locals 3
    .parameter "url"
    .parameter "path"

    .prologue
    .line 72
    new-instance v1, Lmiui/resourcebrowser/controller/online/DownloadFileTask;

    invoke-virtual {p1}, Lmiui/resourcebrowser/controller/online/RequestUrl;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lmiui/resourcebrowser/controller/online/DownloadFileTask;-><init>(Ljava/lang/String;)V

    .line 73
    .local v1, task:Lmiui/resourcebrowser/controller/online/DownloadFileTask;
    const/4 v0, 0x1

    .line 74
    .local v0, flag:I
    invoke-virtual {p1}, Lmiui/resourcebrowser/controller/online/RequestUrl;->isEncryptionNeeded()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 75
    or-int/lit8 v0, v0, 0x2

    .line 77
    :cond_0
    invoke-virtual {v1, p1, p2, v0}, Lmiui/resourcebrowser/controller/online/DownloadFileTask;->downloadFile(Lmiui/resourcebrowser/controller/online/RequestUrl;Ljava/lang/String;I)Z

    move-result v2

    return v2
.end method

.method private updateAssociationCache(Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .parameter "path"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 343
    .local p2, hashs:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->parser:Lmiui/resourcebrowser/controller/online/OnlineDataParser;

    invoke-virtual {v0, p1}, Lmiui/resourcebrowser/controller/online/OnlineDataParser;->readAssociationResources(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->associationResources:Ljava/util/List;

    .line 344
    return-void
.end method

.method private updateCategoryCache(Ljava/lang/String;)V
    .locals 1
    .parameter "path"

    .prologue
    .line 249
    iget-object v0, p0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->parser:Lmiui/resourcebrowser/controller/online/OnlineDataParser;

    invoke-virtual {v0, p1}, Lmiui/resourcebrowser/controller/online/OnlineDataParser;->readCategories(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->categories:Ljava/util/List;

    .line 250
    return-void
.end method

.method private updateDetailCache(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .parameter "path"
    .parameter "id"

    .prologue
    .line 167
    iget-object v3, p0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->parser:Lmiui/resourcebrowser/controller/online/OnlineDataParser;

    invoke-virtual {v3, p1}, Lmiui/resourcebrowser/controller/online/OnlineDataParser;->readResource(Ljava/lang/String;)Lmiui/resourcebrowser/model/Resource;

    move-result-object v0

    .line 168
    .local v0, newResource:Lmiui/resourcebrowser/model/Resource;
    if-eqz v0, :cond_0

    .line 169
    invoke-virtual {v0}, Lmiui/resourcebrowser/model/Resource;->getOnlineId()Ljava/lang/String;

    move-result-object v1

    .line 171
    .local v1, onlineId:Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 172
    iget-object v3, p0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->onlineIdIndex:Ljava/util/Map;

    invoke-interface {v3, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/resourcebrowser/model/Resource;

    .line 173
    .local v2, resource:Lmiui/resourcebrowser/model/Resource;
    if-nez v2, :cond_1

    .line 174
    move-object v2, v0

    .line 175
    iget-object v3, p0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->onlineIdIndex:Ljava/util/Map;

    invoke-interface {v3, p2, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 181
    .end local v1           #onlineId:Ljava/lang/String;
    .end local v2           #resource:Lmiui/resourcebrowser/model/Resource;
    :cond_0
    :goto_0
    return-void

    .line 177
    .restart local v1       #onlineId:Ljava/lang/String;
    .restart local v2       #resource:Lmiui/resourcebrowser/model/Resource;
    :cond_1
    invoke-virtual {v2, v0}, Lmiui/resourcebrowser/model/Resource;->updateFrom(Lmiui/resourcebrowser/model/Resource;)V

    goto :goto_0
.end method

.method private updateListCache(Ljava/lang/String;I)V
    .locals 8
    .parameter "path"
    .parameter "page"

    .prologue
    .line 101
    iget-object v6, p0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->dataSet:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-gt v6, p2, :cond_0

    .line 102
    iget-object v6, p0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->dataSet:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    .local v0, i:I
    :goto_0
    if-gt v0, p2, :cond_0

    .line 103
    iget-object v6, p0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->dataSet:Ljava/util/List;

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 102
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 106
    .end local v0           #i:I
    :cond_0
    iget-object v6, p0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->dataSet:Ljava/util/List;

    invoke-interface {v6, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    .line 107
    .local v5, resources:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/Resource;>;"
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lmiui/resourcebrowser/model/Resource;

    .line 108
    .local v4, resource:Lmiui/resourcebrowser/model/Resource;
    invoke-virtual {v4}, Lmiui/resourcebrowser/model/Resource;->getLocalId()Ljava/lang/String;

    move-result-object v2

    .line 109
    .local v2, localId:Ljava/lang/String;
    invoke-virtual {v4}, Lmiui/resourcebrowser/model/Resource;->getOnlineId()Ljava/lang/String;

    move-result-object v3

    .line 110
    .local v3, onlineId:Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 111
    iget-object v6, p0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->localIdIndex:Ljava/util/Map;

    invoke-interface {v6, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    :cond_2
    if-eqz v3, :cond_1

    .line 114
    iget-object v6, p0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->onlineIdIndex:Ljava/util/Map;

    invoke-interface {v6, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 117
    .end local v2           #localId:Ljava/lang/String;
    .end local v3           #onlineId:Ljava/lang/String;
    .end local v4           #resource:Lmiui/resourcebrowser/model/Resource;
    :cond_3
    iget-object v6, p0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->parser:Lmiui/resourcebrowser/controller/online/OnlineDataParser;

    invoke-virtual {v6, p1}, Lmiui/resourcebrowser/controller/online/OnlineDataParser;->readResources(Ljava/lang/String;)Ljava/util/List;

    move-result-object v5

    .line 118
    iget-object v6, p0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->dataSet:Ljava/util/List;

    invoke-interface {v6, p2, v5}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 119
    if-eqz v5, :cond_6

    .line 120
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lmiui/resourcebrowser/model/Resource;

    .line 121
    .restart local v4       #resource:Lmiui/resourcebrowser/model/Resource;
    invoke-virtual {v4}, Lmiui/resourcebrowser/model/Resource;->getLocalId()Ljava/lang/String;

    move-result-object v2

    .line 122
    .restart local v2       #localId:Ljava/lang/String;
    invoke-virtual {v4}, Lmiui/resourcebrowser/model/Resource;->getOnlineId()Ljava/lang/String;

    move-result-object v3

    .line 123
    .restart local v3       #onlineId:Ljava/lang/String;
    if-eqz v2, :cond_5

    .line 124
    iget-object v6, p0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->localIdIndex:Ljava/util/Map;

    invoke-interface {v6, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    :cond_5
    if-eqz v3, :cond_4

    .line 127
    iget-object v6, p0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->onlineIdIndex:Ljava/util/Map;

    invoke-interface {v6, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 131
    .end local v2           #localId:Ljava/lang/String;
    .end local v3           #onlineId:Ljava/lang/String;
    .end local v4           #resource:Lmiui/resourcebrowser/model/Resource;
    :cond_6
    return-void
.end method

.method private updateListMetaCache(Ljava/lang/String;)V
    .locals 1
    .parameter "path"

    .prologue
    .line 217
    iget-object v0, p0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->parser:Lmiui/resourcebrowser/controller/online/OnlineDataParser;

    invoke-virtual {v0, p1}, Lmiui/resourcebrowser/controller/online/OnlineDataParser;->readResourceListMeta(Ljava/lang/String;)Lmiui/resourcebrowser/model/ResourceListMeta;

    move-result-object v0

    iput-object v0, p0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->listMeta:Lmiui/resourcebrowser/model/ResourceListMeta;

    .line 218
    return-void
.end method

.method private updateRecommendCache(Ljava/lang/String;)V
    .locals 1
    .parameter "path"

    .prologue
    .line 281
    iget-object v0, p0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->parser:Lmiui/resourcebrowser/controller/online/OnlineDataParser;

    invoke-virtual {v0, p1}, Lmiui/resourcebrowser/controller/online/OnlineDataParser;->readRecommends(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->recommends:Ljava/util/List;

    .line 282
    return-void
.end method

.method private updateVersionCache(Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .parameter "path"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 313
    .local p2, hashs:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->parser:Lmiui/resourcebrowser/controller/online/OnlineDataParser;

    invoke-virtual {v0, p1}, Lmiui/resourcebrowser/controller/online/OnlineDataParser;->readUpdatableResources(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->updatableResources:Ljava/util/List;

    .line 314
    return-void
.end method


# virtual methods
.method public getAssociationResources(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 326
    .local p1, hashs:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->getAssociationResources(Ljava/util/List;Z)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getAssociationResources(Ljava/util/List;Z)Ljava/util/List;
    .locals 4
    .parameter
    .parameter "forceRefresh"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z)",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 331
    .local p1, hashs:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->composeAssociationUrl(Ljava/util/List;)Lmiui/resourcebrowser/controller/online/RequestUrl;

    move-result-object v1

    .line 332
    .local v1, url:Lmiui/resourcebrowser/controller/online/RequestUrl;
    invoke-direct {p0, p1}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->composeAssociationPath(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    .line 333
    .local v0, path:Ljava/lang/String;
    if-nez p2, :cond_0

    const-wide/32 v2, 0x5265c00

    invoke-direct {p0, v0, v2, v3}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->needRefresh(Ljava/lang/String;J)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 334
    :cond_0
    invoke-direct {p0, v1, v0}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->refreshFileCache(Lmiui/resourcebrowser/controller/online/RequestUrl;Ljava/lang/String;)Z

    .line 335
    invoke-direct {p0, v0, p1}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->updateAssociationCache(Ljava/lang/String;Ljava/util/List;)V

    .line 339
    :goto_0
    iget-object v2, p0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->associationResources:Ljava/util/List;

    return-object v2

    .line 337
    :cond_1
    invoke-direct {p0, v0, p1}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->updateAssociationCache(Ljava/lang/String;Ljava/util/List;)V

    goto :goto_0
.end method

.method public getRecommends()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/RecommendItemData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 262
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->getRecommends(Z)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getRecommends(Z)Ljava/util/List;
    .locals 4
    .parameter "forceRefresh"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/RecommendItemData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 267
    invoke-direct {p0}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->composeRecommendUrl()Lmiui/resourcebrowser/controller/online/RequestUrl;

    move-result-object v1

    .line 268
    .local v1, url:Lmiui/resourcebrowser/controller/online/RequestUrl;
    invoke-direct {p0}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->composeRecommendPath()Ljava/lang/String;

    move-result-object v0

    .line 269
    .local v0, path:Ljava/lang/String;
    if-nez p1, :cond_0

    const-wide/32 v2, 0x36ee80

    invoke-direct {p0, v0, v2, v3}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->needRefresh(Ljava/lang/String;J)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 270
    :cond_0
    invoke-direct {p0, v1, v0}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->refreshFileCache(Lmiui/resourcebrowser/controller/online/RequestUrl;Ljava/lang/String;)Z

    .line 271
    invoke-direct {p0, v0}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->updateRecommendCache(Ljava/lang/String;)V

    .line 277
    :cond_1
    :goto_0
    iget-object v2, p0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->recommends:Ljava/util/List;

    return-object v2

    .line 273
    :cond_2
    iget-object v2, p0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->recommends:Ljava/util/List;

    if-nez v2, :cond_1

    .line 274
    invoke-direct {p0, v0}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->updateRecommendCache(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getResource(Ljava/lang/String;)Lmiui/resourcebrowser/model/Resource;
    .locals 1
    .parameter "id"

    .prologue
    .line 150
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->getResource(Ljava/lang/String;Z)Lmiui/resourcebrowser/model/Resource;

    move-result-object v0

    return-object v0
.end method

.method public getResource(Ljava/lang/String;Z)Lmiui/resourcebrowser/model/Resource;
    .locals 4
    .parameter "id"
    .parameter "forceRefresh"

    .prologue
    .line 155
    invoke-direct {p0, p1}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->composeDetailUrl(Ljava/lang/String;)Lmiui/resourcebrowser/controller/online/RequestUrl;

    move-result-object v1

    .line 156
    .local v1, url:Lmiui/resourcebrowser/controller/online/RequestUrl;
    invoke-direct {p0, p1}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->composeDetailPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 157
    .local v0, path:Ljava/lang/String;
    if-nez p2, :cond_0

    const-wide/32 v2, 0x36ee80

    invoke-direct {p0, v0, v2, v3}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->needRefresh(Ljava/lang/String;J)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 158
    :cond_0
    invoke-direct {p0, v1, v0}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->refreshFileCache(Lmiui/resourcebrowser/controller/online/RequestUrl;Ljava/lang/String;)Z

    .line 159
    invoke-direct {p0, v0, p1}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->updateDetailCache(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    :goto_0
    iget-object v2, p0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->onlineIdIndex:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/resourcebrowser/model/Resource;

    return-object v2

    .line 161
    :cond_1
    invoke-direct {p0, v0, p1}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->updateDetailCache(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getResourceByLocalId(Ljava/lang/String;)Lmiui/resourcebrowser/model/Resource;
    .locals 1
    .parameter "id"

    .prologue
    .line 193
    iget-object v0, p0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->localIdIndex:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/resourcebrowser/model/Resource;

    return-object v0
.end method

.method public getResourceCategories()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/ResourceCategory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 230
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->getResourceCategories(Z)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getResourceCategories(Z)Ljava/util/List;
    .locals 4
    .parameter "forceRefresh"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/ResourceCategory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 235
    invoke-direct {p0}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->composeCategoryUrl()Lmiui/resourcebrowser/controller/online/RequestUrl;

    move-result-object v1

    .line 236
    .local v1, url:Lmiui/resourcebrowser/controller/online/RequestUrl;
    invoke-direct {p0}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->composeCategoryPath()Ljava/lang/String;

    move-result-object v0

    .line 237
    .local v0, path:Ljava/lang/String;
    if-nez p1, :cond_0

    const-wide/32 v2, 0x5265c00

    invoke-direct {p0, v0, v2, v3}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->needRefresh(Ljava/lang/String;J)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 238
    :cond_0
    invoke-direct {p0, v1, v0}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->refreshFileCache(Lmiui/resourcebrowser/controller/online/RequestUrl;Ljava/lang/String;)Z

    .line 239
    invoke-direct {p0, v0}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->updateCategoryCache(Ljava/lang/String;)V

    .line 245
    :cond_1
    :goto_0
    iget-object v2, p0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->categories:Ljava/util/List;

    return-object v2

    .line 241
    :cond_2
    iget-object v2, p0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->categories:Ljava/util/List;

    if-nez v2, :cond_1

    .line 242
    invoke-direct {p0, v0}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->updateCategoryCache(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getResourceListMeta(Ljava/lang/String;)Lmiui/resourcebrowser/model/ResourceListMeta;
    .locals 1
    .parameter "id"

    .prologue
    .line 198
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->getResourceListMeta(Ljava/lang/String;Z)Lmiui/resourcebrowser/model/ResourceListMeta;

    move-result-object v0

    return-object v0
.end method

.method public getResourceListMeta(Ljava/lang/String;Z)Lmiui/resourcebrowser/model/ResourceListMeta;
    .locals 4
    .parameter "id"
    .parameter "forceRefresh"

    .prologue
    .line 203
    invoke-direct {p0, p1}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->composeListMetaUrl(Ljava/lang/String;)Lmiui/resourcebrowser/controller/online/RequestUrl;

    move-result-object v1

    .line 204
    .local v1, url:Lmiui/resourcebrowser/controller/online/RequestUrl;
    invoke-direct {p0, p1}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->composeListMetaPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 205
    .local v0, path:Ljava/lang/String;
    if-nez p2, :cond_0

    const-wide/32 v2, 0x36ee80

    invoke-direct {p0, v0, v2, v3}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->needRefresh(Ljava/lang/String;J)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 206
    :cond_0
    invoke-direct {p0, v1, v0}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->refreshFileCache(Lmiui/resourcebrowser/controller/online/RequestUrl;Ljava/lang/String;)Z

    .line 207
    invoke-direct {p0, v0}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->updateListMetaCache(Ljava/lang/String;)V

    .line 213
    :cond_1
    :goto_0
    iget-object v2, p0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->listMeta:Lmiui/resourcebrowser/model/ResourceListMeta;

    return-object v2

    .line 209
    :cond_2
    iget-object v2, p0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->listMeta:Lmiui/resourcebrowser/model/ResourceListMeta;

    if-nez v2, :cond_1

    .line 210
    invoke-direct {p0, v0}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->updateListMetaCache(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getResources(I)Ljava/util/List;
    .locals 1
    .parameter "page"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 82
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->getResources(IZ)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getResources(IZ)Ljava/util/List;
    .locals 4
    .parameter "page"
    .parameter "forceRefresh"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ)",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->composeListUrl(I)Lmiui/resourcebrowser/controller/online/RequestUrl;

    move-result-object v1

    .line 88
    .local v1, url:Lmiui/resourcebrowser/controller/online/RequestUrl;
    invoke-direct {p0, p1}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->composeListPath(I)Ljava/lang/String;

    move-result-object v0

    .line 89
    .local v0, path:Ljava/lang/String;
    if-nez p1, :cond_0

    if-nez p2, :cond_0

    const-wide/32 v2, 0x493e0

    invoke-direct {p0, v0, v2, v3}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->needRefresh(Ljava/lang/String;J)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 90
    :cond_0
    invoke-direct {p0, v1, v0}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->refreshFileCache(Lmiui/resourcebrowser/controller/online/RequestUrl;Ljava/lang/String;)Z

    .line 91
    invoke-direct {p0, v0, p1}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->updateListCache(Ljava/lang/String;I)V

    .line 97
    :cond_1
    :goto_0
    iget-object v2, p0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->dataSet:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    return-object v2

    .line 93
    :cond_2
    iget-object v2, p0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->dataSet:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-le v2, p1, :cond_3

    iget-object v2, p0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->dataSet:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_1

    .line 94
    :cond_3
    invoke-direct {p0, v0, p1}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->updateListCache(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public getUpdatableResources(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 294
    .local p1, hashs:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->getUpdatableResources(Ljava/util/List;Z)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getUpdatableResources(Ljava/util/List;Z)Ljava/util/List;
    .locals 4
    .parameter
    .parameter "forceRefresh"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z)",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 299
    .local p1, hashs:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->composeVersionUrl(Ljava/util/List;)Lmiui/resourcebrowser/controller/online/RequestUrl;

    move-result-object v1

    .line 300
    .local v1, url:Lmiui/resourcebrowser/controller/online/RequestUrl;
    invoke-direct {p0, p1}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->composeVersionPath(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    .line 301
    .local v0, path:Ljava/lang/String;
    if-nez p2, :cond_0

    const-wide/32 v2, 0x36ee80

    invoke-direct {p0, v0, v2, v3}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->needRefresh(Ljava/lang/String;J)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 302
    :cond_0
    invoke-direct {p0, v1, v0}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->refreshFileCache(Lmiui/resourcebrowser/controller/online/RequestUrl;Ljava/lang/String;)Z

    .line 303
    invoke-direct {p0, v0, p1}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->updateVersionCache(Ljava/lang/String;Ljava/util/List;)V

    .line 309
    :cond_1
    :goto_0
    iget-object v2, p0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->updatableResources:Ljava/util/List;

    return-object v2

    .line 305
    :cond_2
    iget-object v2, p0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->updatableResources:Ljava/util/List;

    if-nez v2, :cond_1

    .line 306
    invoke-direct {p0, v0, p1}, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->updateVersionCache(Ljava/lang/String;Ljava/util/List;)V

    goto :goto_0
.end method

.method public setResourceController(Lmiui/resourcebrowser/controller/ResourceController;)V
    .locals 1
    .parameter "controller"

    .prologue
    .line 55
    invoke-super {p0, p1}, Lmiui/resourcebrowser/controller/OnlineDataManager;->setResourceController(Lmiui/resourcebrowser/controller/ResourceController;)V

    .line 56
    iget-object v0, p0, Lmiui/resourcebrowser/controller/online/HttpServerDataManager;->parser:Lmiui/resourcebrowser/controller/online/OnlineDataParser;

    invoke-virtual {v0, p1}, Lmiui/resourcebrowser/controller/online/OnlineDataParser;->setResourceController(Lmiui/resourcebrowser/controller/ResourceController;)V

    .line 57
    return-void
.end method
