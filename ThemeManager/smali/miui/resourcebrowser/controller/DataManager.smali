.class public abstract Lmiui/resourcebrowser/controller/DataManager;
.super Ljava/lang/Object;
.source "DataManager.java"


# instance fields
.field protected context:Lmiui/resourcebrowser/ResourceContext;

.field protected controller:Lmiui/resourcebrowser/controller/ResourceController;

.field protected observers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/controller/DataSetObserver;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lmiui/resourcebrowser/ResourceContext;)V
    .locals 1
    .parameter "context"

    .prologue
    .line 16
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 14
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/controller/DataManager;->observers:Ljava/util/List;

    .line 17
    iput-object p1, p0, Lmiui/resourcebrowser/controller/DataManager;->context:Lmiui/resourcebrowser/ResourceContext;

    .line 18
    return-void
.end method


# virtual methods
.method public addObserver(Lmiui/resourcebrowser/controller/DataSetObserver;)V
    .locals 1
    .parameter "observer"

    .prologue
    .line 25
    if-eqz p1, :cond_0

    .line 26
    iget-object v0, p0, Lmiui/resourcebrowser/controller/DataManager;->observers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 28
    :cond_0
    return-void
.end method

.method protected notifyDataSetUpdateFailed()V
    .locals 3

    .prologue
    .line 67
    iget-object v2, p0, Lmiui/resourcebrowser/controller/DataManager;->observers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/resourcebrowser/controller/DataSetObserver;

    .line 68
    .local v1, observer:Lmiui/resourcebrowser/controller/DataSetObserver;
    invoke-interface {v1}, Lmiui/resourcebrowser/controller/DataSetObserver;->onDataSetUpdateFailed()V

    goto :goto_0

    .line 70
    .end local v1           #observer:Lmiui/resourcebrowser/controller/DataSetObserver;
    :cond_0
    return-void
.end method

.method protected notifyDataSetUpdateSuccessful()V
    .locals 3

    .prologue
    .line 61
    iget-object v2, p0, Lmiui/resourcebrowser/controller/DataManager;->observers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/resourcebrowser/controller/DataSetObserver;

    .line 62
    .local v1, observer:Lmiui/resourcebrowser/controller/DataSetObserver;
    invoke-interface {v1}, Lmiui/resourcebrowser/controller/DataSetObserver;->onDataSetUpdateSuccessful()V

    goto :goto_0

    .line 64
    .end local v1           #observer:Lmiui/resourcebrowser/controller/DataSetObserver;
    :cond_0
    return-void
.end method

.method public removeObserver(Lmiui/resourcebrowser/controller/DataSetObserver;)V
    .locals 1
    .parameter "observer"

    .prologue
    .line 31
    if-eqz p1, :cond_0

    .line 32
    iget-object v0, p0, Lmiui/resourcebrowser/controller/DataManager;->observers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 34
    :cond_0
    return-void
.end method

.method public setResourceController(Lmiui/resourcebrowser/controller/ResourceController;)V
    .locals 0
    .parameter "controller"

    .prologue
    .line 21
    iput-object p1, p0, Lmiui/resourcebrowser/controller/DataManager;->controller:Lmiui/resourcebrowser/controller/ResourceController;

    .line 22
    return-void
.end method
