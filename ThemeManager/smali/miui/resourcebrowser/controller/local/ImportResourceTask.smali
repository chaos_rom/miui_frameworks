.class public Lmiui/resourcebrowser/controller/local/ImportResourceTask;
.super Lmiui/os/UniqueAsyncTask;
.source "ImportResourceTask.java"

# interfaces
.implements Lmiui/resourcebrowser/ResourceConstants;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmiui/os/UniqueAsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        ">;",
        "Lmiui/resourcebrowser/ResourceConstants;"
    }
.end annotation


# static fields
.field public static IMPORT_BY_DOWNLOAD_FOLDER:Ljava/lang/String;

.field public static IMPORT_BY_IMPORT_FOLDER:Ljava/lang/String;

.field public static IMPORT_BY_ONLINE_ID:Ljava/lang/String;

.field public static IMPORT_BY_PATH:Ljava/lang/String;

.field private static sObject:Ljava/lang/Object;


# instance fields
.field private mContextParser:Lmiui/resourcebrowser/controller/local/ContextParser;

.field private mDataParser:Lmiui/resourcebrowser/controller/local/LocalDataParser;

.field private mImportTotalNum:I

.field private mResContext:Lmiui/resourcebrowser/ResourceContext;

.field private mResController:Lmiui/resourcebrowser/controller/ResourceController;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lmiui/resourcebrowser/controller/local/ImportResourceTask;->sObject:Ljava/lang/Object;

    .line 28
    const-string v0, "import_by_online_id"

    sput-object v0, Lmiui/resourcebrowser/controller/local/ImportResourceTask;->IMPORT_BY_ONLINE_ID:Ljava/lang/String;

    .line 29
    const-string v0, "import_by_path"

    sput-object v0, Lmiui/resourcebrowser/controller/local/ImportResourceTask;->IMPORT_BY_PATH:Ljava/lang/String;

    .line 30
    const-string v0, "import_by_import_folder"

    sput-object v0, Lmiui/resourcebrowser/controller/local/ImportResourceTask;->IMPORT_BY_IMPORT_FOLDER:Ljava/lang/String;

    .line 31
    const-string v0, "import_by_download_folder"

    sput-object v0, Lmiui/resourcebrowser/controller/local/ImportResourceTask;->IMPORT_BY_DOWNLOAD_FOLDER:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lmiui/resourcebrowser/ResourceContext;Ljava/lang/String;)V
    .locals 1
    .parameter "resContext"
    .parameter "id"

    .prologue
    .line 34
    invoke-direct {p0, p2}, Lmiui/os/UniqueAsyncTask;-><init>(Ljava/lang/String;)V

    .line 35
    invoke-static {}, Lmiui/resourcebrowser/AppInnerContext;->getInstance()Lmiui/resourcebrowser/AppInnerContext;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/resourcebrowser/AppInnerContext;->getResourceContext()Lmiui/resourcebrowser/ResourceContext;

    move-result-object v0

    iput-object v0, p0, Lmiui/resourcebrowser/controller/local/ImportResourceTask;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    .line 36
    invoke-static {}, Lmiui/resourcebrowser/AppInnerContext;->getInstance()Lmiui/resourcebrowser/AppInnerContext;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/resourcebrowser/AppInnerContext;->getResourceController()Lmiui/resourcebrowser/controller/ResourceController;

    move-result-object v0

    iput-object v0, p0, Lmiui/resourcebrowser/controller/local/ImportResourceTask;->mResController:Lmiui/resourcebrowser/controller/ResourceController;

    .line 37
    new-instance v0, Lmiui/resourcebrowser/controller/local/LocalSerializableDataParser;

    invoke-direct {v0, p1}, Lmiui/resourcebrowser/controller/local/LocalSerializableDataParser;-><init>(Lmiui/resourcebrowser/ResourceContext;)V

    iput-object v0, p0, Lmiui/resourcebrowser/controller/local/ImportResourceTask;->mDataParser:Lmiui/resourcebrowser/controller/local/LocalDataParser;

    .line 38
    new-instance v0, Lmiui/resourcebrowser/controller/local/ContextSerializableParser;

    invoke-direct {v0}, Lmiui/resourcebrowser/controller/local/ContextSerializableParser;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/controller/local/ImportResourceTask;->mContextParser:Lmiui/resourcebrowser/controller/local/ContextParser;

    .line 39
    return-void
.end method

.method private checkResourceImported(Ljava/lang/String;)Z
    .locals 7
    .parameter "hash"

    .prologue
    .line 255
    const/4 v2, 0x0

    .line 256
    .local v2, imported:Z
    new-instance v4, Ljava/io/File;

    iget-object v6, p0, Lmiui/resourcebrowser/controller/local/ImportResourceTask;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v6}, Lmiui/resourcebrowser/ResourceContext;->getIndexFolder()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 257
    .local v4, indexFolder:Ljava/io/File;
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 258
    invoke-virtual {v4}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .local v0, arr$:[Ljava/io/File;
    array-length v5, v0

    .local v5, len$:I
    const/4 v1, 0x0

    .local v1, i$:I
    :goto_0
    if-ge v1, v5, :cond_0

    aget-object v3, v0, v1

    .line 259
    .local v3, indexFile:Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 260
    const/4 v2, 0x1

    .line 265
    .end local v0           #arr$:[Ljava/io/File;
    .end local v1           #i$:I
    .end local v3           #indexFile:Ljava/io/File;
    .end local v5           #len$:I
    :cond_0
    return v2

    .line 258
    .restart local v0       #arr$:[Ljava/io/File;
    .restart local v1       #i$:I
    .restart local v3       #indexFile:Ljava/io/File;
    .restart local v5       #len$:I
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Integer;
    .locals 41
    .parameter "params"

    .prologue
    .line 52
    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/resourcebrowser/controller/local/ImportResourceTask;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Lmiui/resourcebrowser/ResourceContext;->isSelfDescribing()Z

    move-result v37

    if-eqz v37, :cond_0

    .line 53
    const/16 v37, -0x1

    invoke-static/range {v37 .. v37}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v37

    .line 251
    :goto_0
    return-object v37

    .line 55
    :cond_0
    new-instance v32, Ljava/util/HashMap;

    invoke-direct/range {v32 .. v32}, Ljava/util/HashMap;-><init>()V

    .line 56
    .local v32, resourceMap:Ljava/util/Map;,"Ljava/util/Map<Lmiui/resourcebrowser/model/Resource;Lmiui/resourcebrowser/ResourceContext;>;"
    const/16 v33, 0x0

    .line 57
    .local v33, successNum:I
    sget-object v38, Lmiui/resourcebrowser/controller/local/ImportResourceTask;->sObject:Ljava/lang/Object;

    monitor-enter v38

    .line 58
    if-eqz p1, :cond_1

    :try_start_0
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v37, v0

    if-nez v37, :cond_2

    .line 59
    :cond_1
    const/16 v37, -0x1

    invoke-static/range {v37 .. v37}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v37

    monitor-exit v38

    goto :goto_0

    .line 250
    :catchall_0
    move-exception v37

    monitor-exit v38
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v37

    .line 61
    :cond_2
    :try_start_1
    new-instance v18, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/resourcebrowser/controller/local/ImportResourceTask;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Lmiui/resourcebrowser/ResourceContext;->getAsyncImportFolder()Ljava/lang/String;

    move-result-object v37

    move-object/from16 v0, v18

    move-object/from16 v1, v37

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 62
    .local v18, importFolder:Ljava/io/File;
    new-instance v10, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/resourcebrowser/controller/local/ImportResourceTask;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Lmiui/resourcebrowser/ResourceContext;->getDownloadFolder()Ljava/lang/String;

    move-result-object v37

    move-object/from16 v0, v37

    invoke-direct {v10, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 63
    .local v10, downloadFolder:Ljava/io/File;
    new-instance v5, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/resourcebrowser/controller/local/ImportResourceTask;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Lmiui/resourcebrowser/ResourceContext;->getContentFolder()Ljava/lang/String;

    move-result-object v37

    move-object/from16 v0, v37

    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 64
    .local v5, contentFolder:Ljava/io/File;
    new-instance v19, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/resourcebrowser/controller/local/ImportResourceTask;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Lmiui/resourcebrowser/ResourceContext;->getIndexFolder()Ljava/lang/String;

    move-result-object v37

    move-object/from16 v0, v19

    move-object/from16 v1, v37

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 65
    .local v19, indexFolder:Ljava/io/File;
    invoke-virtual {v10}, Ljava/io/File;->mkdirs()Z

    .line 66
    invoke-virtual {v5}, Ljava/io/File;->mkdirs()Z

    .line 67
    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->mkdirs()Z

    .line 68
    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->mkdirs()Z

    .line 70
    const/16 v37, 0x0

    aget-object v37, p1, v37

    sget-object v39, Lmiui/resourcebrowser/controller/local/ImportResourceTask;->IMPORT_BY_IMPORT_FOLDER:Ljava/lang/String;

    move-object/from16 v0, v37

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v37

    if-eqz v37, :cond_d

    .line 71
    new-instance v29, Lmiui/resourcebrowser/util/ResourceDownloadHandler;

    invoke-static {}, Lmiui/resourcebrowser/AppInnerContext;->getInstance()Lmiui/resourcebrowser/AppInnerContext;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Lmiui/resourcebrowser/AppInnerContext;->getApplicationContext()Landroid/content/Context;

    move-result-object v37

    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/resourcebrowser/controller/local/ImportResourceTask;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    move-object/from16 v39, v0

    move-object/from16 v0, v29

    move-object/from16 v1, v37

    move-object/from16 v2, v39

    invoke-direct {v0, v1, v2}, Lmiui/resourcebrowser/util/ResourceDownloadHandler;-><init>(Landroid/content/Context;Lmiui/resourcebrowser/ResourceContext;)V

    .line 74
    .local v29, resourceDownloadHandler:Lmiui/resourcebrowser/util/ResourceDownloadHandler;
    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .local v3, arr$:[Ljava/io/File;
    array-length v0, v3

    move/from16 v20, v0

    .local v20, len$:I
    const/4 v15, 0x0

    .local v15, i$:I
    move/from16 v16, v15

    .end local v3           #arr$:[Ljava/io/File;
    .end local v15           #i$:I
    .end local v20           #len$:I
    .local v16, i$:I
    :goto_1
    move/from16 v0, v16

    move/from16 v1, v20

    if-ge v0, v1, :cond_1e

    aget-object v17, v3, v16

    .line 75
    .local v17, importFile:Ljava/io/File;
    const/16 v28, 0x0

    .line 76
    .local v28, resource:Lmiui/resourcebrowser/model/Resource;
    const/4 v6, 0x0

    .line 78
    .local v6, context:Lmiui/resourcebrowser/ResourceContext;
    const/16 v30, 0x0

    .line 79
    .local v30, resourceFile:Ljava/io/File;
    const/4 v7, 0x0

    .line 80
    .local v7, contextFile:Ljava/io/File;
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v22

    .line 82
    .local v22, name:Ljava/lang/String;
    const-string v37, ".import"

    move-object/from16 v0, v22

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v37

    if-eqz v37, :cond_4

    .line 74
    .end local v16           #i$:I
    :cond_3
    :goto_2
    add-int/lit8 v15, v16, 0x1

    .restart local v15       #i$:I
    move/from16 v16, v15

    .end local v15           #i$:I
    .restart local v16       #i$:I
    goto :goto_1

    .line 86
    :cond_4
    const-string v37, ".mrm"

    move-object/from16 v0, v22

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v37

    if-eqz v37, :cond_8

    .line 87
    move-object/from16 v30, v17

    .line 88
    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v4

    .local v4, arr$:[Ljava/io/File;
    array-length v0, v4

    move/from16 v21, v0

    .local v21, len$:I
    const/4 v15, 0x0

    .end local v16           #i$:I
    .restart local v15       #i$:I
    :goto_3
    move/from16 v0, v21

    if-ge v15, v0, :cond_5

    aget-object v12, v4, v15

    .line 89
    .local v12, file:Ljava/io/File;
    new-instance v37, Ljava/lang/StringBuilder;

    invoke-direct/range {v37 .. v37}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v37

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    const-string v39, ".mrm"

    move-object/from16 v0, v37

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v37

    move-object/from16 v0, v22

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v37

    if-eqz v37, :cond_7

    .line 90
    move-object v7, v12

    .line 103
    .end local v12           #file:Ljava/io/File;
    :cond_5
    :goto_4
    if-eqz v30, :cond_a

    if-eqz v7, :cond_a

    .line 105
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/resourcebrowser/controller/local/ImportResourceTask;->mDataParser:Lmiui/resourcebrowser/controller/local/LocalDataParser;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/controller/local/LocalDataParser;->loadResource(Ljava/io/File;)Lmiui/resourcebrowser/model/Resource;

    move-result-object v28

    .line 106
    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/resourcebrowser/controller/local/ImportResourceTask;->mContextParser:Lmiui/resourcebrowser/controller/local/ContextParser;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    invoke-virtual {v0, v7}, Lmiui/resourcebrowser/controller/local/ContextParser;->loadResourceContext(Ljava/io/File;)Lmiui/resourcebrowser/ResourceContext;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
    .catch Lmiui/resourcebrowser/controller/local/PersistenceException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v6

    .line 116
    :cond_6
    :goto_5
    if-eqz v28, :cond_3

    if-eqz v6, :cond_3

    .line 117
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/resourcebrowser/controller/local/ImportResourceTask;->mResController:Lmiui/resourcebrowser/controller/ResourceController;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Lmiui/resourcebrowser/controller/ResourceController;->getOnlineDataManager()Lmiui/resourcebrowser/controller/OnlineDataManager;

    move-result-object v37

    invoke-virtual/range {v28 .. v28}, Lmiui/resourcebrowser/model/Resource;->getOnlineId()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v37

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/controller/OnlineDataManager;->getResource(Ljava/lang/String;)Lmiui/resourcebrowser/model/Resource;

    move-result-object v27

    .line 118
    .local v27, r:Lmiui/resourcebrowser/model/Resource;
    if-nez v27, :cond_c

    .line 119
    :goto_6
    invoke-virtual/range {v28 .. v28}, Lmiui/resourcebrowser/model/Resource;->getOnlineId()Ljava/lang/String;

    move-result-object v37

    move-object/from16 v0, v29

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/util/ResourceDownloadHandler;->isResourceDownloading(Ljava/lang/String;)Z

    move-result v37

    if-nez v37, :cond_3

    .line 120
    move-object/from16 v0, v32

    move-object/from16 v1, v28

    invoke-interface {v0, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    invoke-virtual/range {v30 .. v30}, Ljava/io/File;->delete()Z

    .line 122
    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    goto/16 :goto_2

    .line 88
    .end local v27           #r:Lmiui/resourcebrowser/model/Resource;
    .restart local v12       #file:Ljava/io/File;
    :cond_7
    add-int/lit8 v15, v15, 0x1

    goto/16 :goto_3

    .line 95
    .end local v4           #arr$:[Ljava/io/File;
    .end local v12           #file:Ljava/io/File;
    .end local v15           #i$:I
    .end local v21           #len$:I
    .restart local v16       #i$:I
    :cond_8
    move-object/from16 v7, v17

    .line 96
    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v4

    .restart local v4       #arr$:[Ljava/io/File;
    array-length v0, v4

    move/from16 v21, v0

    .restart local v21       #len$:I
    const/4 v15, 0x0

    .end local v16           #i$:I
    .restart local v15       #i$:I
    :goto_7
    move/from16 v0, v21

    if-ge v15, v0, :cond_5

    aget-object v12, v4, v15

    .line 97
    .restart local v12       #file:Ljava/io/File;
    invoke-virtual {v12}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v37

    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v39

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v40, ".mrm"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v37

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v37

    if-eqz v37, :cond_9

    .line 98
    move-object/from16 v30, v12

    .line 99
    goto/16 :goto_4

    .line 96
    :cond_9
    add-int/lit8 v15, v15, 0x1

    goto :goto_7

    .line 107
    .end local v12           #file:Ljava/io/File;
    :catch_0
    move-exception v11

    .line 108
    .local v11, e:Lmiui/resourcebrowser/controller/local/PersistenceException;
    invoke-virtual/range {v30 .. v30}, Ljava/io/File;->delete()Z

    .line 109
    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    goto :goto_5

    .line 111
    .end local v11           #e:Lmiui/resourcebrowser/controller/local/PersistenceException;
    :cond_a
    if-eqz v30, :cond_b

    .line 112
    invoke-virtual/range {v30 .. v30}, Ljava/io/File;->delete()Z

    goto/16 :goto_5

    .line 113
    :cond_b
    if-eqz v7, :cond_6

    .line 114
    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    goto/16 :goto_5

    .restart local v27       #r:Lmiui/resourcebrowser/model/Resource;
    :cond_c
    move-object/from16 v28, v27

    .line 118
    goto :goto_6

    .line 126
    .end local v4           #arr$:[Ljava/io/File;
    .end local v6           #context:Lmiui/resourcebrowser/ResourceContext;
    .end local v7           #contextFile:Ljava/io/File;
    .end local v15           #i$:I
    .end local v17           #importFile:Ljava/io/File;
    .end local v21           #len$:I
    .end local v22           #name:Ljava/lang/String;
    .end local v27           #r:Lmiui/resourcebrowser/model/Resource;
    .end local v28           #resource:Lmiui/resourcebrowser/model/Resource;
    .end local v29           #resourceDownloadHandler:Lmiui/resourcebrowser/util/ResourceDownloadHandler;
    .end local v30           #resourceFile:Ljava/io/File;
    :cond_d
    const/16 v37, 0x0

    aget-object v37, p1, v37

    sget-object v39, Lmiui/resourcebrowser/controller/local/ImportResourceTask;->IMPORT_BY_DOWNLOAD_FOLDER:Ljava/lang/String;

    move-object/from16 v0, v37

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v37

    if-eqz v37, :cond_10

    .line 128
    invoke-virtual {v10}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .restart local v3       #arr$:[Ljava/io/File;
    array-length v0, v3

    move/from16 v20, v0

    .restart local v20       #len$:I
    const/4 v15, 0x0

    .restart local v15       #i$:I
    :goto_8
    move/from16 v0, v20

    if-ge v15, v0, :cond_1e

    aget-object v9, v3, v15

    .line 129
    .local v9, downloadFile:Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->isDirectory()Z

    move-result v37

    if-eqz v37, :cond_f

    .line 128
    :cond_e
    :goto_9
    add-int/lit8 v15, v15, 0x1

    goto :goto_8

    .line 145
    :cond_f
    invoke-virtual {v9}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v24

    .line 146
    .local v24, path:Ljava/lang/String;
    invoke-static/range {v24 .. v24}, Lmiui/resourcebrowser/util/ResourceHelper;->getFileHash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 147
    .local v13, hash:Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lmiui/resourcebrowser/controller/local/ImportResourceTask;->checkResourceImported(Ljava/lang/String;)Z

    move-result v37

    if-nez v37, :cond_e

    .line 148
    new-instance v28, Lmiui/resourcebrowser/model/Resource;

    invoke-direct/range {v28 .. v28}, Lmiui/resourcebrowser/model/Resource;-><init>()V

    .line 149
    .restart local v28       #resource:Lmiui/resourcebrowser/model/Resource;
    invoke-virtual {v9}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v37

    move-object/from16 v0, v28

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/model/Resource;->setDownloadPath(Ljava/lang/String;)V

    .line 150
    const/16 v37, 0x0

    move-object/from16 v0, v32

    move-object/from16 v1, v28

    move-object/from16 v2, v37

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_9

    .line 153
    .end local v3           #arr$:[Ljava/io/File;
    .end local v9           #downloadFile:Ljava/io/File;
    .end local v13           #hash:Ljava/lang/String;
    .end local v15           #i$:I
    .end local v20           #len$:I
    .end local v24           #path:Ljava/lang/String;
    .end local v28           #resource:Lmiui/resourcebrowser/model/Resource;
    :cond_10
    const/16 v37, 0x0

    aget-object v37, p1, v37

    sget-object v39, Lmiui/resourcebrowser/controller/local/ImportResourceTask;->IMPORT_BY_ONLINE_ID:Ljava/lang/String;

    move-object/from16 v0, v37

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v37

    if-eqz v37, :cond_1c

    .line 155
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v37, v0

    add-int/lit8 v37, v37, -0x1

    move/from16 v0, v37

    move-object/from16 v1, p0

    iput v0, v1, Lmiui/resourcebrowser/controller/local/ImportResourceTask;->mImportTotalNum:I

    .line 156
    const/4 v14, 0x1

    .local v14, i:I
    :goto_a
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v37, v0

    move/from16 v0, v37

    if-ge v14, v0, :cond_1e

    .line 157
    aget-object v23, p1, v14

    .line 158
    .local v23, onlineId:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/resourcebrowser/controller/local/ImportResourceTask;->mResController:Lmiui/resourcebrowser/controller/ResourceController;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Lmiui/resourcebrowser/controller/ResourceController;->getOnlineDataManager()Lmiui/resourcebrowser/controller/OnlineDataManager;

    move-result-object v37

    move-object/from16 v0, v37

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/controller/OnlineDataManager;->getResource(Ljava/lang/String;)Lmiui/resourcebrowser/model/Resource;

    move-result-object v28

    .line 159
    .restart local v28       #resource:Lmiui/resourcebrowser/model/Resource;
    const/4 v6, 0x0

    .line 161
    .restart local v6       #context:Lmiui/resourcebrowser/ResourceContext;
    const/16 v30, 0x0

    .line 162
    .restart local v30       #resourceFile:Ljava/io/File;
    const/4 v7, 0x0

    .line 163
    .restart local v7       #contextFile:Ljava/io/File;
    new-instance v37, Ljava/lang/StringBuilder;

    invoke-direct/range {v37 .. v37}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v37

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    const-string v39, ".mrm"

    move-object/from16 v0, v37

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    .line 164
    .local v31, resourceFileName:Ljava/lang/String;
    move-object/from16 v8, v23

    .line 166
    .local v8, contextFileName:Ljava/lang/String;
    if-nez v28, :cond_17

    .line 167
    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .restart local v3       #arr$:[Ljava/io/File;
    array-length v0, v3

    move/from16 v20, v0

    .restart local v20       #len$:I
    const/4 v15, 0x0

    .restart local v15       #i$:I
    :goto_b
    move/from16 v0, v20

    if-ge v15, v0, :cond_12

    aget-object v12, v3, v15
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 169
    .restart local v12       #file:Ljava/io/File;
    :try_start_4
    invoke-virtual {v12}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v37

    move-object/from16 v0, v37

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v37

    if-eqz v37, :cond_15

    .line 170
    move-object/from16 v30, v12

    .line 171
    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/resourcebrowser/controller/local/ImportResourceTask;->mDataParser:Lmiui/resourcebrowser/controller/local/LocalDataParser;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    invoke-virtual {v0, v12}, Lmiui/resourcebrowser/controller/local/LocalDataParser;->loadResource(Ljava/io/File;)Lmiui/resourcebrowser/model/Resource;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
    .catch Lmiui/resourcebrowser/controller/local/PersistenceException; {:try_start_4 .. :try_end_4} :catch_1

    move-result-object v28

    .line 180
    :cond_11
    :goto_c
    if-eqz v30, :cond_16

    if-eqz v7, :cond_16

    .line 197
    .end local v12           #file:Ljava/io/File;
    :cond_12
    :goto_d
    if-eqz v28, :cond_14

    if-eqz v6, :cond_14

    .line 198
    :try_start_5
    invoke-virtual/range {v28 .. v28}, Lmiui/resourcebrowser/model/Resource;->getDownloadPath()Ljava/lang/String;

    move-result-object v24

    .line 199
    .restart local v24       #path:Ljava/lang/String;
    invoke-static/range {v24 .. v24}, Lmiui/resourcebrowser/util/ResourceHelper;->getFileHash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 200
    .restart local v13       #hash:Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lmiui/resourcebrowser/controller/local/ImportResourceTask;->checkResourceImported(Ljava/lang/String;)Z

    move-result v37

    if-nez v37, :cond_1b

    .line 201
    move-object/from16 v0, v32

    move-object/from16 v1, v28

    invoke-interface {v0, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 207
    :goto_e
    if-eqz v30, :cond_13

    .line 208
    invoke-virtual/range {v30 .. v30}, Ljava/io/File;->delete()Z

    .line 210
    :cond_13
    if-eqz v7, :cond_14

    .line 211
    invoke-virtual {v7}, Ljava/io/File;->delete()Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 156
    .end local v13           #hash:Ljava/lang/String;
    .end local v24           #path:Ljava/lang/String;
    :cond_14
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_a

    .line 172
    .restart local v12       #file:Ljava/io/File;
    :cond_15
    :try_start_6
    invoke-virtual {v12}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v37

    move-object/from16 v0, v37

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v37

    if-eqz v37, :cond_11

    .line 173
    move-object v7, v12

    .line 174
    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/resourcebrowser/controller/local/ImportResourceTask;->mContextParser:Lmiui/resourcebrowser/controller/local/ContextParser;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    invoke-virtual {v0, v12}, Lmiui/resourcebrowser/controller/local/ContextParser;->loadResourceContext(Ljava/io/File;)Lmiui/resourcebrowser/ResourceContext;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
    .catch Lmiui/resourcebrowser/controller/local/PersistenceException; {:try_start_6 .. :try_end_6} :catch_1

    move-result-object v6

    goto :goto_c

    .line 176
    :catch_1
    move-exception v11

    .line 177
    .restart local v11       #e:Lmiui/resourcebrowser/controller/local/PersistenceException;
    :try_start_7
    invoke-virtual {v12}, Ljava/io/File;->delete()Z

    goto :goto_d

    .line 167
    .end local v11           #e:Lmiui/resourcebrowser/controller/local/PersistenceException;
    :cond_16
    add-int/lit8 v15, v15, 0x1

    goto :goto_b

    .line 185
    .end local v3           #arr$:[Ljava/io/File;
    .end local v12           #file:Ljava/io/File;
    .end local v15           #i$:I
    .end local v20           #len$:I
    :cond_17
    move-object/from16 v0, p0

    iget-object v6, v0, Lmiui/resourcebrowser/controller/local/ImportResourceTask;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    .line 186
    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .restart local v3       #arr$:[Ljava/io/File;
    array-length v0, v3

    move/from16 v20, v0

    .restart local v20       #len$:I
    const/4 v15, 0x0

    .restart local v15       #i$:I
    :goto_f
    move/from16 v0, v20

    if-ge v15, v0, :cond_12

    aget-object v12, v3, v15

    .line 187
    .restart local v12       #file:Ljava/io/File;
    invoke-virtual {v12}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v37

    move-object/from16 v0, v37

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v37

    if-eqz v37, :cond_1a

    .line 188
    move-object/from16 v30, v12

    .line 192
    :cond_18
    :goto_10
    if-eqz v30, :cond_19

    if-nez v7, :cond_12

    .line 186
    :cond_19
    add-int/lit8 v15, v15, 0x1

    goto :goto_f

    .line 189
    :cond_1a
    invoke-virtual {v12}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v37

    move-object/from16 v0, v37

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v37

    if-eqz v37, :cond_18

    .line 190
    move-object v7, v12

    goto :goto_10

    .line 204
    .end local v12           #file:Ljava/io/File;
    .restart local v13       #hash:Ljava/lang/String;
    .restart local v24       #path:Ljava/lang/String;
    :cond_1b
    add-int/lit8 v33, v33, 0x1

    goto :goto_e

    .line 215
    .end local v3           #arr$:[Ljava/io/File;
    .end local v6           #context:Lmiui/resourcebrowser/ResourceContext;
    .end local v7           #contextFile:Ljava/io/File;
    .end local v8           #contextFileName:Ljava/lang/String;
    .end local v13           #hash:Ljava/lang/String;
    .end local v14           #i:I
    .end local v15           #i$:I
    .end local v20           #len$:I
    .end local v23           #onlineId:Ljava/lang/String;
    .end local v24           #path:Ljava/lang/String;
    .end local v28           #resource:Lmiui/resourcebrowser/model/Resource;
    .end local v30           #resourceFile:Ljava/io/File;
    .end local v31           #resourceFileName:Ljava/lang/String;
    :cond_1c
    const/16 v37, 0x0

    aget-object v37, p1, v37

    sget-object v39, Lmiui/resourcebrowser/controller/local/ImportResourceTask;->IMPORT_BY_PATH:Ljava/lang/String;

    move-object/from16 v0, v37

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v37

    if-eqz v37, :cond_1e

    .line 217
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v37, v0

    add-int/lit8 v37, v37, -0x1

    move/from16 v0, v37

    move-object/from16 v1, p0

    iput v0, v1, Lmiui/resourcebrowser/controller/local/ImportResourceTask;->mImportTotalNum:I

    .line 218
    const/4 v14, 0x1

    .restart local v14       #i:I
    :goto_11
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v37, v0

    move/from16 v0, v37

    if-ge v14, v0, :cond_1e

    .line 219
    aget-object v24, p1, v14

    .line 220
    .restart local v24       #path:Ljava/lang/String;
    invoke-static/range {v24 .. v24}, Lmiui/resourcebrowser/util/ResourceHelper;->getFileHash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 221
    .restart local v13       #hash:Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lmiui/resourcebrowser/controller/local/ImportResourceTask;->checkResourceImported(Ljava/lang/String;)Z

    move-result v37

    if-nez v37, :cond_1d

    .line 222
    new-instance v37, Ljava/lang/StringBuilder;

    invoke-direct/range {v37 .. v37}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/resourcebrowser/controller/local/ImportResourceTask;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lmiui/resourcebrowser/ResourceContext;->getDownloadFolder()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v37

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-static/range {v24 .. v24}, Lmiui/os/ExtraFileUtils;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v37

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v34

    .line 224
    .local v34, targetPath:Ljava/lang/String;
    move-object/from16 v0, v24

    move-object/from16 v1, v34

    invoke-static {v0, v1}, Lmiui/os/Shell;->copy(Ljava/lang/String;Ljava/lang/String;)Z

    .line 225
    new-instance v28, Lmiui/resourcebrowser/model/Resource;

    invoke-direct/range {v28 .. v28}, Lmiui/resourcebrowser/model/Resource;-><init>()V

    .line 226
    .restart local v28       #resource:Lmiui/resourcebrowser/model/Resource;
    move-object/from16 v0, v28

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/model/Resource;->setDownloadPath(Ljava/lang/String;)V

    .line 227
    const/16 v37, 0x0

    move-object/from16 v0, v32

    move-object/from16 v1, v28

    move-object/from16 v2, v37

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    .end local v28           #resource:Lmiui/resourcebrowser/model/Resource;
    .end local v34           #targetPath:Ljava/lang/String;
    :cond_1d
    add-int/lit8 v14, v14, 0x1

    goto :goto_11

    .line 233
    .end local v13           #hash:Ljava/lang/String;
    .end local v14           #i:I
    .end local v24           #path:Ljava/lang/String;
    :cond_1e
    invoke-interface/range {v32 .. v32}, Ljava/util/Map;->size()I

    move-result v26

    .line 234
    .local v26, processMax:I
    move/from16 v0, v26

    move-object/from16 v1, p0

    iput v0, v1, Lmiui/resourcebrowser/controller/local/ImportResourceTask;->mImportTotalNum:I

    .line 235
    const/16 v25, 0x1

    .line 236
    .local v25, processCursor:I
    invoke-interface/range {v32 .. v32}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v37

    invoke-interface/range {v37 .. v37}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .local v15, i$:Ljava/util/Iterator;
    :cond_1f
    :goto_12
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v37

    if-eqz v37, :cond_21

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v36

    check-cast v36, Lmiui/resourcebrowser/model/Resource;

    .line 237
    .local v36, tempResource:Lmiui/resourcebrowser/model/Resource;
    const/16 v37, 0x2

    move/from16 v0, v37

    new-array v0, v0, [Ljava/lang/Integer;

    move-object/from16 v37, v0

    const/16 v39, 0x0

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v40

    aput-object v40, v37, v39

    const/16 v39, 0x1

    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v40

    aput-object v40, v37, v39

    move-object/from16 v0, p0

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/controller/local/ImportResourceTask;->publishProgress([Ljava/lang/Object;)V

    .line 238
    add-int/lit8 v25, v25, 0x1

    .line 239
    move-object/from16 v0, v32

    move-object/from16 v1, v36

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v35

    check-cast v35, Lmiui/resourcebrowser/ResourceContext;

    .line 240
    .local v35, tempContext:Lmiui/resourcebrowser/ResourceContext;
    if-eqz v35, :cond_20

    .line 241
    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/resourcebrowser/controller/local/ImportResourceTask;->mResController:Lmiui/resourcebrowser/controller/ResourceController;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Lmiui/resourcebrowser/controller/ResourceController;->getImportManager()Lmiui/resourcebrowser/controller/ImportManager;

    move-result-object v37

    move-object/from16 v0, v37

    move-object/from16 v1, v36

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Lmiui/resourcebrowser/controller/ImportManager;->importResource(Lmiui/resourcebrowser/model/Resource;Lmiui/resourcebrowser/ResourceContext;)Z

    move-result v37

    if-eqz v37, :cond_1f

    .line 242
    add-int/lit8 v33, v33, 0x1

    goto :goto_12

    .line 245
    :cond_20
    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/resourcebrowser/controller/local/ImportResourceTask;->mResController:Lmiui/resourcebrowser/controller/ResourceController;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Lmiui/resourcebrowser/controller/ResourceController;->getImportManager()Lmiui/resourcebrowser/controller/ImportManager;

    move-result-object v37

    move-object/from16 v0, v37

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/controller/ImportManager;->importResource(Lmiui/resourcebrowser/model/Resource;)Z

    move-result v37

    if-eqz v37, :cond_1f

    .line 246
    add-int/lit8 v33, v33, 0x1

    goto :goto_12

    .line 250
    .end local v35           #tempContext:Lmiui/resourcebrowser/ResourceContext;
    .end local v36           #tempResource:Lmiui/resourcebrowser/model/Resource;
    :cond_21
    monitor-exit v38
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 251
    invoke-static/range {v33 .. v33}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v37

    goto/16 :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"

    .prologue
    .line 18
    check-cast p1, [Ljava/lang/String;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/resourcebrowser/controller/local/ImportResourceTask;->doInBackground([Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getImportTotalNum()I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lmiui/resourcebrowser/controller/local/ImportResourceTask;->mImportTotalNum:I

    return v0
.end method

.method protected hasEquivalentRunningTasks()Z
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    return v0
.end method
