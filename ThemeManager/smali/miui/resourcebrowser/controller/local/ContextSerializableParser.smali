.class public Lmiui/resourcebrowser/controller/local/ContextSerializableParser;
.super Lmiui/resourcebrowser/controller/local/ContextParser;
.source "ContextSerializableParser.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lmiui/resourcebrowser/controller/local/ContextParser;-><init>()V

    return-void
.end method


# virtual methods
.method public loadResourceContext(Ljava/io/File;)Lmiui/resourcebrowser/ResourceContext;
    .locals 6
    .parameter "file"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lmiui/resourcebrowser/controller/local/PersistenceException;
        }
    .end annotation

    .prologue
    .line 18
    const/4 v0, 0x0

    .line 19
    .local v0, context:Lmiui/resourcebrowser/ResourceContext;
    const/4 v2, 0x0

    .line 21
    .local v2, is:Ljava/io/ObjectInputStream;
    :try_start_0
    new-instance v3, Ljava/io/ObjectInputStream;

    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v3, v4}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/io/StreamCorruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_4

    .line 22
    .end local v2           #is:Ljava/io/ObjectInputStream;
    .local v3, is:Ljava/io/ObjectInputStream;
    :try_start_1
    invoke-virtual {v3}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    .end local v0           #context:Lmiui/resourcebrowser/ResourceContext;
    check-cast v0, Lmiui/resourcebrowser/ResourceContext;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1
    .catch Ljava/io/StreamCorruptedException; {:try_start_1 .. :try_end_1} :catch_9
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_8
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_6

    .line 32
    .restart local v0       #context:Lmiui/resourcebrowser/ResourceContext;
    if-eqz v3, :cond_0

    .line 34
    :try_start_2
    invoke-virtual {v3}, Ljava/io/ObjectInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 40
    :cond_0
    :goto_0
    return-object v0

    .line 35
    :catch_0
    move-exception v1

    .line 36
    .local v1, e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 23
    .end local v1           #e:Ljava/io/IOException;
    .end local v3           #is:Ljava/io/ObjectInputStream;
    .restart local v2       #is:Ljava/io/ObjectInputStream;
    :catch_1
    move-exception v1

    .line 24
    .end local v0           #context:Lmiui/resourcebrowser/ResourceContext;
    .local v1, e:Ljava/io/StreamCorruptedException;
    :goto_1
    :try_start_3
    new-instance v4, Lmiui/resourcebrowser/controller/local/PersistenceException;

    invoke-virtual {v1}, Ljava/io/StreamCorruptedException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lmiui/resourcebrowser/controller/local/PersistenceException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 32
    .end local v1           #e:Ljava/io/StreamCorruptedException;
    :catchall_0
    move-exception v4

    :goto_2
    if-eqz v2, :cond_1

    .line 34
    :try_start_4
    invoke-virtual {v2}, Ljava/io/ObjectInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_5

    .line 37
    :cond_1
    :goto_3
    throw v4

    .line 25
    .restart local v0       #context:Lmiui/resourcebrowser/ResourceContext;
    :catch_2
    move-exception v1

    .line 26
    .end local v0           #context:Lmiui/resourcebrowser/ResourceContext;
    .local v1, e:Ljava/io/FileNotFoundException;
    :goto_4
    :try_start_5
    new-instance v4, Lmiui/resourcebrowser/controller/local/PersistenceException;

    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lmiui/resourcebrowser/controller/local/PersistenceException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 27
    .end local v1           #e:Ljava/io/FileNotFoundException;
    .restart local v0       #context:Lmiui/resourcebrowser/ResourceContext;
    :catch_3
    move-exception v1

    .line 28
    .end local v0           #context:Lmiui/resourcebrowser/ResourceContext;
    .local v1, e:Ljava/io/IOException;
    :goto_5
    new-instance v4, Lmiui/resourcebrowser/controller/local/PersistenceException;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lmiui/resourcebrowser/controller/local/PersistenceException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 29
    .end local v1           #e:Ljava/io/IOException;
    .restart local v0       #context:Lmiui/resourcebrowser/ResourceContext;
    :catch_4
    move-exception v1

    .line 30
    .end local v0           #context:Lmiui/resourcebrowser/ResourceContext;
    .local v1, e:Ljava/lang/ClassNotFoundException;
    :goto_6
    new-instance v4, Lmiui/resourcebrowser/controller/local/PersistenceException;

    invoke-virtual {v1}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lmiui/resourcebrowser/controller/local/PersistenceException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 35
    .end local v1           #e:Ljava/lang/ClassNotFoundException;
    :catch_5
    move-exception v1

    .line 36
    .local v1, e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 32
    .end local v1           #e:Ljava/io/IOException;
    .end local v2           #is:Ljava/io/ObjectInputStream;
    .restart local v3       #is:Ljava/io/ObjectInputStream;
    :catchall_1
    move-exception v4

    move-object v2, v3

    .end local v3           #is:Ljava/io/ObjectInputStream;
    .restart local v2       #is:Ljava/io/ObjectInputStream;
    goto :goto_2

    .line 29
    .end local v2           #is:Ljava/io/ObjectInputStream;
    .restart local v3       #is:Ljava/io/ObjectInputStream;
    :catch_6
    move-exception v1

    move-object v2, v3

    .end local v3           #is:Ljava/io/ObjectInputStream;
    .restart local v2       #is:Ljava/io/ObjectInputStream;
    goto :goto_6

    .line 27
    .end local v2           #is:Ljava/io/ObjectInputStream;
    .restart local v3       #is:Ljava/io/ObjectInputStream;
    :catch_7
    move-exception v1

    move-object v2, v3

    .end local v3           #is:Ljava/io/ObjectInputStream;
    .restart local v2       #is:Ljava/io/ObjectInputStream;
    goto :goto_5

    .line 25
    .end local v2           #is:Ljava/io/ObjectInputStream;
    .restart local v3       #is:Ljava/io/ObjectInputStream;
    :catch_8
    move-exception v1

    move-object v2, v3

    .end local v3           #is:Ljava/io/ObjectInputStream;
    .restart local v2       #is:Ljava/io/ObjectInputStream;
    goto :goto_4

    .line 23
    .end local v2           #is:Ljava/io/ObjectInputStream;
    .restart local v3       #is:Ljava/io/ObjectInputStream;
    :catch_9
    move-exception v1

    move-object v2, v3

    .end local v3           #is:Ljava/io/ObjectInputStream;
    .restart local v2       #is:Ljava/io/ObjectInputStream;
    goto :goto_1
.end method

.method public storeResourceContext(Ljava/io/File;Lmiui/resourcebrowser/ResourceContext;)V
    .locals 5
    .parameter "file"
    .parameter "resource"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lmiui/resourcebrowser/controller/local/PersistenceException;
        }
    .end annotation

    .prologue
    .line 45
    const/4 v1, 0x0

    .line 47
    .local v1, os:Ljava/io/ObjectOutputStream;
    :try_start_0
    new-instance v2, Ljava/io/ObjectOutputStream;

    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v3}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 48
    .end local v1           #os:Ljava/io/ObjectOutputStream;
    .local v2, os:Ljava/io/ObjectOutputStream;
    :try_start_1
    invoke-virtual {v2, p2}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4

    .line 54
    if-eqz v2, :cond_0

    .line 56
    :try_start_2
    invoke-virtual {v2}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 62
    :cond_0
    :goto_0
    return-void

    .line 57
    :catch_0
    move-exception v0

    .line 58
    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 49
    .end local v0           #e:Ljava/io/IOException;
    .end local v2           #os:Ljava/io/ObjectOutputStream;
    .restart local v1       #os:Ljava/io/ObjectOutputStream;
    :catch_1
    move-exception v0

    .line 50
    .local v0, e:Ljava/io/FileNotFoundException;
    :goto_1
    :try_start_3
    new-instance v3, Lmiui/resourcebrowser/controller/local/PersistenceException;

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lmiui/resourcebrowser/controller/local/PersistenceException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 54
    .end local v0           #e:Ljava/io/FileNotFoundException;
    :catchall_0
    move-exception v3

    :goto_2
    if-eqz v1, :cond_1

    .line 56
    :try_start_4
    invoke-virtual {v1}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 59
    :cond_1
    :goto_3
    throw v3

    .line 51
    :catch_2
    move-exception v0

    .line 52
    .local v0, e:Ljava/io/IOException;
    :goto_4
    :try_start_5
    new-instance v3, Lmiui/resourcebrowser/controller/local/PersistenceException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lmiui/resourcebrowser/controller/local/PersistenceException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 57
    .end local v0           #e:Ljava/io/IOException;
    :catch_3
    move-exception v0

    .line 58
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 54
    .end local v0           #e:Ljava/io/IOException;
    .end local v1           #os:Ljava/io/ObjectOutputStream;
    .restart local v2       #os:Ljava/io/ObjectOutputStream;
    :catchall_1
    move-exception v3

    move-object v1, v2

    .end local v2           #os:Ljava/io/ObjectOutputStream;
    .restart local v1       #os:Ljava/io/ObjectOutputStream;
    goto :goto_2

    .line 51
    .end local v1           #os:Ljava/io/ObjectOutputStream;
    .restart local v2       #os:Ljava/io/ObjectOutputStream;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2           #os:Ljava/io/ObjectOutputStream;
    .restart local v1       #os:Ljava/io/ObjectOutputStream;
    goto :goto_4

    .line 49
    .end local v1           #os:Ljava/io/ObjectOutputStream;
    .restart local v2       #os:Ljava/io/ObjectOutputStream;
    :catch_5
    move-exception v0

    move-object v1, v2

    .end local v2           #os:Ljava/io/ObjectOutputStream;
    .restart local v1       #os:Ljava/io/ObjectOutputStream;
    goto :goto_1
.end method
