.class public Lmiui/resourcebrowser/controller/local/LocalBareDataParser;
.super Lmiui/resourcebrowser/controller/local/LocalDataParser;
.source "LocalBareDataParser.java"


# direct methods
.method public constructor <init>(Lmiui/resourcebrowser/ResourceContext;)V
    .locals 0
    .parameter "context"

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lmiui/resourcebrowser/controller/local/LocalDataParser;-><init>(Lmiui/resourcebrowser/ResourceContext;)V

    .line 16
    return-void
.end method


# virtual methods
.method public loadResource(Ljava/io/File;)Lmiui/resourcebrowser/model/Resource;
    .locals 6
    .parameter "file"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lmiui/resourcebrowser/controller/local/PersistenceException;
        }
    .end annotation

    .prologue
    .line 20
    new-instance v3, Lmiui/resourcebrowser/model/Resource;

    invoke-direct {v3}, Lmiui/resourcebrowser/model/Resource;-><init>()V

    .line 21
    .local v3, resource:Lmiui/resourcebrowser/model/Resource;
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    .line 22
    .local v2, path:Ljava/lang/String;
    invoke-virtual {v3, v2}, Lmiui/resourcebrowser/model/Resource;->setLocalPath(Ljava/lang/String;)V

    .line 23
    invoke-virtual {v3, v2}, Lmiui/resourcebrowser/model/Resource;->setFilePath(Ljava/lang/String;)V

    .line 24
    invoke-virtual {v3, v2}, Lmiui/resourcebrowser/model/Resource;->setDownloadPath(Ljava/lang/String;)V

    .line 25
    invoke-static {v2}, Lmiui/resourcebrowser/util/ResourceHelper;->getFileHash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lmiui/resourcebrowser/model/Resource;->setHash(Ljava/lang/String;)V

    .line 26
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lmiui/resourcebrowser/model/Resource;->setSize(J)V

    .line 27
    invoke-virtual {p1}, Ljava/io/File;->lastModified()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lmiui/resourcebrowser/model/Resource;->setUpdatedTime(J)V

    .line 28
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lmiui/resourcebrowser/model/Resource;->setStatus(I)V

    .line 29
    invoke-static {v2}, Lmiui/resourcebrowser/util/ResourceHelper;->getFileNameWithoutExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lmiui/resourcebrowser/model/Resource;->setTitle(Ljava/lang/String;)V

    .line 30
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 31
    .local v1, images:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/PathEntry;>;"
    new-instance v0, Lmiui/resourcebrowser/model/PathEntry;

    const-string v4, ""

    invoke-direct {v0, v2, v4}, Lmiui/resourcebrowser/model/PathEntry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    .local v0, image:Lmiui/resourcebrowser/model/PathEntry;
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 33
    invoke-virtual {v3, v1}, Lmiui/resourcebrowser/model/Resource;->setThumbnails(Ljava/util/List;)V

    .line 34
    invoke-virtual {v3, v1}, Lmiui/resourcebrowser/model/Resource;->setPreviews(Ljava/util/List;)V

    .line 35
    return-object v3
.end method

.method public storeResource(Ljava/io/File;Lmiui/resourcebrowser/model/Resource;)V
    .locals 3
    .parameter "file"
    .parameter "resource"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lmiui/resourcebrowser/controller/local/PersistenceException;
        }
    .end annotation

    .prologue
    .line 40
    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getDownloadPath()Ljava/lang/String;

    move-result-object v0

    .line 41
    .local v0, downloadPath:Ljava/lang/String;
    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v1

    .line 42
    .local v1, localPath:Ljava/lang/String;
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 43
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 45
    :cond_0
    return-void
.end method
