.class public abstract Lmiui/resourcebrowser/controller/local/LocalDataParser;
.super Ljava/lang/Object;
.source "LocalDataParser.java"


# instance fields
.field protected context:Lmiui/resourcebrowser/ResourceContext;

.field protected controller:Lmiui/resourcebrowser/controller/ResourceController;


# direct methods
.method public constructor <init>(Lmiui/resourcebrowser/ResourceContext;)V
    .locals 0
    .parameter "context"

    .prologue
    .line 14
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lmiui/resourcebrowser/controller/local/LocalDataParser;->context:Lmiui/resourcebrowser/ResourceContext;

    .line 16
    return-void
.end method


# virtual methods
.method public abstract loadResource(Ljava/io/File;)Lmiui/resourcebrowser/model/Resource;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lmiui/resourcebrowser/controller/local/PersistenceException;
        }
    .end annotation
.end method

.method public setResourceController(Lmiui/resourcebrowser/controller/ResourceController;)V
    .locals 0
    .parameter "controller"

    .prologue
    .line 19
    iput-object p1, p0, Lmiui/resourcebrowser/controller/local/LocalDataParser;->controller:Lmiui/resourcebrowser/controller/ResourceController;

    .line 20
    return-void
.end method

.method public abstract storeResource(Ljava/io/File;Lmiui/resourcebrowser/model/Resource;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lmiui/resourcebrowser/controller/local/PersistenceException;
        }
    .end annotation
.end method

.method public updateState()V
    .locals 0

    .prologue
    .line 23
    return-void
.end method
