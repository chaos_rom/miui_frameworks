.class public Lmiui/resourcebrowser/controller/local/LocalJSONDataParser;
.super Lmiui/resourcebrowser/controller/local/LocalDataParser;
.source "LocalJSONDataParser.java"


# direct methods
.method public constructor <init>(Lmiui/resourcebrowser/ResourceContext;)V
    .locals 0
    .parameter "context"

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lmiui/resourcebrowser/controller/local/LocalDataParser;-><init>(Lmiui/resourcebrowser/ResourceContext;)V

    .line 29
    return-void
.end method

.method private readBuildInImagePaths(Landroid/util/JsonReader;)Ljava/util/List;
    .locals 2
    .parameter "reader"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/JsonReader;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 123
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 124
    .local v0, buildInImagePaths:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p1}, Landroid/util/JsonReader;->beginArray()V

    .line 125
    :goto_0
    invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 126
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 128
    :cond_0
    invoke-virtual {p1}, Landroid/util/JsonReader;->endArray()V

    .line 129
    return-object v0
.end method

.method private readExtraMeta(Landroid/util/JsonReader;)Ljava/util/Map;
    .locals 4
    .parameter "reader"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/JsonReader;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 183
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 184
    .local v0, extraMeta:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {p1}, Landroid/util/JsonReader;->beginObject()V

    .line 185
    :goto_0
    invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 186
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v1

    .line 187
    .local v1, name:Ljava/lang/String;
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v2

    sget-object v3, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-eq v2, v3, :cond_0

    .line 188
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 190
    :cond_0
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    goto :goto_0

    .line 193
    .end local v1           #name:Ljava/lang/String;
    :cond_1
    invoke-virtual {p1}, Landroid/util/JsonReader;->endObject()V

    .line 194
    return-object v0
.end method

.method private readImagePaths(Landroid/util/JsonReader;)Ljava/util/List;
    .locals 5
    .parameter "reader"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/JsonReader;",
            ")",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/PathEntry;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 133
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 134
    .local v1, imagePaths:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/PathEntry;>;"
    invoke-virtual {p1}, Landroid/util/JsonReader;->beginArray()V

    .line 135
    :goto_0
    invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 136
    invoke-virtual {p1}, Landroid/util/JsonReader;->beginObject()V

    .line 137
    new-instance v0, Lmiui/resourcebrowser/model/PathEntry;

    invoke-direct {v0}, Lmiui/resourcebrowser/model/PathEntry;-><init>()V

    .line 138
    .local v0, entry:Lmiui/resourcebrowser/model/PathEntry;
    :goto_1
    invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 139
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v2

    .line 140
    .local v2, name:Ljava/lang/String;
    const-string v3, "localPath"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v3

    sget-object v4, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-eq v3, v4, :cond_0

    .line 141
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lmiui/resourcebrowser/model/PathEntry;->setLocalPath(Ljava/lang/String;)V

    goto :goto_1

    .line 142
    :cond_0
    const-string v3, "onlinePath"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v3

    sget-object v4, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-eq v3, v4, :cond_1

    .line 143
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lmiui/resourcebrowser/model/PathEntry;->setOnlinePath(Ljava/lang/String;)V

    goto :goto_1

    .line 145
    :cond_1
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    goto :goto_1

    .line 148
    .end local v2           #name:Ljava/lang/String;
    :cond_2
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 149
    invoke-virtual {p1}, Landroid/util/JsonReader;->endObject()V

    goto :goto_0

    .line 151
    .end local v0           #entry:Lmiui/resourcebrowser/model/PathEntry;
    :cond_3
    invoke-virtual {p1}, Landroid/util/JsonReader;->endArray()V

    .line 152
    return-object v1
.end method

.method private readRelatedResource(Landroid/util/JsonReader;)Ljava/util/List;
    .locals 5
    .parameter "reader"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/JsonReader;",
            ")",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/RelatedResource;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 156
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 157
    .local v2, relatedResources:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/RelatedResource;>;"
    invoke-virtual {p1}, Landroid/util/JsonReader;->beginArray()V

    .line 158
    :goto_0
    invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 159
    invoke-virtual {p1}, Landroid/util/JsonReader;->beginObject()V

    .line 160
    new-instance v1, Lmiui/resourcebrowser/model/RelatedResource;

    invoke-direct {v1}, Lmiui/resourcebrowser/model/RelatedResource;-><init>()V

    .line 161
    .local v1, relatedResource:Lmiui/resourcebrowser/model/RelatedResource;
    :goto_1
    invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 162
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v0

    .line 163
    .local v0, name:Ljava/lang/String;
    const-string v3, "localPath"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v3

    sget-object v4, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-eq v3, v4, :cond_0

    .line 164
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lmiui/resourcebrowser/model/RelatedResource;->setLocalPath(Ljava/lang/String;)V

    goto :goto_1

    .line 165
    :cond_0
    const-string v3, "filePath"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v3

    sget-object v4, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-eq v3, v4, :cond_1

    .line 166
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lmiui/resourcebrowser/model/RelatedResource;->setFilePath(Ljava/lang/String;)V

    goto :goto_1

    .line 167
    :cond_1
    const-string v3, "resourceCode"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v3

    sget-object v4, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-eq v3, v4, :cond_2

    .line 168
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lmiui/resourcebrowser/model/RelatedResource;->setResourceCode(Ljava/lang/String;)V

    goto :goto_1

    .line 169
    :cond_2
    const-string v3, "extraMeta"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v3

    sget-object v4, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-eq v3, v4, :cond_3

    .line 170
    invoke-direct {p0, p1}, Lmiui/resourcebrowser/controller/local/LocalJSONDataParser;->readExtraMeta(Landroid/util/JsonReader;)Ljava/util/Map;

    move-result-object v3

    invoke-virtual {v1, v3}, Lmiui/resourcebrowser/model/RelatedResource;->setExtraMeta(Ljava/util/Map;)V

    goto :goto_1

    .line 172
    :cond_3
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    goto :goto_1

    .line 175
    .end local v0           #name:Ljava/lang/String;
    :cond_4
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 176
    invoke-virtual {p1}, Landroid/util/JsonReader;->endObject()V

    goto/16 :goto_0

    .line 178
    .end local v1           #relatedResource:Lmiui/resourcebrowser/model/RelatedResource;
    :cond_5
    invoke-virtual {p1}, Landroid/util/JsonReader;->endArray()V

    .line 179
    return-object v2
.end method

.method private writeBuildInImagePaths(Landroid/util/JsonWriter;Ljava/util/List;)V
    .locals 3
    .parameter "writer"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/JsonWriter;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 261
    .local p2, buildInImagePaths:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p1}, Landroid/util/JsonWriter;->beginArray()Landroid/util/JsonWriter;

    .line 262
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 263
    .local v1, value:Ljava/lang/String;
    invoke-virtual {p1, v1}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    goto :goto_0

    .line 265
    .end local v1           #value:Ljava/lang/String;
    :cond_0
    invoke-virtual {p1}, Landroid/util/JsonWriter;->endArray()Landroid/util/JsonWriter;

    .line 266
    return-void
.end method

.method private writeExtraMeta(Landroid/util/JsonWriter;Ljava/util/Map;)V
    .locals 4
    .parameter "writer"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/JsonWriter;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 294
    .local p2, extraMeta:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {p1}, Landroid/util/JsonWriter;->beginObject()Landroid/util/JsonWriter;

    .line 295
    invoke-interface {p2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 296
    .local v1, key:Ljava/lang/String;
    invoke-virtual {p1, v1}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v3

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v2}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    goto :goto_0

    .line 298
    .end local v1           #key:Ljava/lang/String;
    :cond_0
    invoke-virtual {p1}, Landroid/util/JsonWriter;->endObject()Landroid/util/JsonWriter;

    .line 299
    return-void
.end method

.method private writeImagePaths(Landroid/util/JsonWriter;Ljava/util/List;)V
    .locals 4
    .parameter "writer"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/JsonWriter;",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/PathEntry;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 269
    .local p2, imagePaths:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/PathEntry;>;"
    invoke-virtual {p1}, Landroid/util/JsonWriter;->beginArray()Landroid/util/JsonWriter;

    .line 270
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/resourcebrowser/model/PathEntry;

    .line 271
    .local v0, entry:Lmiui/resourcebrowser/model/PathEntry;
    invoke-virtual {p1}, Landroid/util/JsonWriter;->beginObject()Landroid/util/JsonWriter;

    .line 272
    const-string v2, "localPath"

    invoke-virtual {p1, v2}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v2

    invoke-virtual {v0}, Lmiui/resourcebrowser/model/PathEntry;->getLocalPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 273
    const-string v2, "onlinePath"

    invoke-virtual {p1, v2}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v2

    invoke-virtual {v0}, Lmiui/resourcebrowser/model/PathEntry;->getOnlinePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 274
    invoke-virtual {p1}, Landroid/util/JsonWriter;->endObject()Landroid/util/JsonWriter;

    goto :goto_0

    .line 276
    .end local v0           #entry:Lmiui/resourcebrowser/model/PathEntry;
    :cond_0
    invoke-virtual {p1}, Landroid/util/JsonWriter;->endArray()Landroid/util/JsonWriter;

    .line 277
    return-void
.end method

.method private writeRelatedResources(Landroid/util/JsonWriter;Ljava/util/List;)V
    .locals 4
    .parameter "writer"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/JsonWriter;",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/RelatedResource;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 280
    .local p2, relatedResources:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/RelatedResource;>;"
    invoke-virtual {p1}, Landroid/util/JsonWriter;->beginArray()Landroid/util/JsonWriter;

    .line 281
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/resourcebrowser/model/RelatedResource;

    .line 282
    .local v1, relatedResource:Lmiui/resourcebrowser/model/RelatedResource;
    invoke-virtual {p1}, Landroid/util/JsonWriter;->beginObject()Landroid/util/JsonWriter;

    .line 283
    const-string v2, "localPath"

    invoke-virtual {p1, v2}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v2

    invoke-virtual {v1}, Lmiui/resourcebrowser/model/RelatedResource;->getLocalPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 284
    const-string v2, "filePath"

    invoke-virtual {p1, v2}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v2

    invoke-virtual {v1}, Lmiui/resourcebrowser/model/RelatedResource;->getFilePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 285
    const-string v2, "resourceCode"

    invoke-virtual {p1, v2}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v2

    invoke-virtual {v1}, Lmiui/resourcebrowser/model/RelatedResource;->getResourceCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 286
    const-string v2, "extraMeta"

    invoke-virtual {p1, v2}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 287
    invoke-virtual {v1}, Lmiui/resourcebrowser/model/RelatedResource;->getExtraMeta()Ljava/util/Map;

    move-result-object v2

    invoke-direct {p0, p1, v2}, Lmiui/resourcebrowser/controller/local/LocalJSONDataParser;->writeExtraMeta(Landroid/util/JsonWriter;Ljava/util/Map;)V

    .line 288
    invoke-virtual {p1}, Landroid/util/JsonWriter;->endObject()Landroid/util/JsonWriter;

    goto :goto_0

    .line 290
    .end local v1           #relatedResource:Lmiui/resourcebrowser/model/RelatedResource;
    :cond_0
    invoke-virtual {p1}, Landroid/util/JsonWriter;->endArray()Landroid/util/JsonWriter;

    .line 291
    return-void
.end method


# virtual methods
.method public loadResource(Ljava/io/File;)Lmiui/resourcebrowser/model/Resource;
    .locals 8
    .parameter "file"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lmiui/resourcebrowser/controller/local/PersistenceException;
        }
    .end annotation

    .prologue
    .line 33
    new-instance v5, Lmiui/resourcebrowser/model/Resource;

    invoke-direct {v5}, Lmiui/resourcebrowser/model/Resource;-><init>()V

    .line 34
    .local v5, resource:Lmiui/resourcebrowser/model/Resource;
    const/4 v3, 0x0

    .line 36
    .local v3, reader:Landroid/util/JsonReader;
    :try_start_0
    new-instance v4, Landroid/util/JsonReader;

    new-instance v6, Ljava/io/BufferedReader;

    new-instance v7, Ljava/io/FileReader;

    invoke-direct {v7, p1}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v6, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    invoke-direct {v4, v6}, Landroid/util/JsonReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    .line 37
    .end local v3           #reader:Landroid/util/JsonReader;
    .local v4, reader:Landroid/util/JsonReader;
    :try_start_1
    invoke-virtual {v4}, Landroid/util/JsonReader;->beginObject()V

    .line 39
    :goto_0
    invoke-virtual {v4}, Landroid/util/JsonReader;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1b

    .line 40
    invoke-virtual {v4}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v1

    .line 41
    .local v1, name:Ljava/lang/String;
    const-string v6, "localId"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v4}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v6

    sget-object v7, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-eq v6, v7, :cond_1

    .line 42
    invoke-virtual {v4}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lmiui/resourcebrowser/model/Resource;->setLocalId(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 99
    .end local v1           #name:Ljava/lang/String;
    :catch_0
    move-exception v0

    move-object v3, v4

    .line 100
    .end local v4           #reader:Landroid/util/JsonReader;
    .local v0, e:Ljava/io/IOException;
    .restart local v3       #reader:Landroid/util/JsonReader;
    :goto_1
    :try_start_2
    new-instance v6, Lmiui/resourcebrowser/controller/local/PersistenceException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lmiui/resourcebrowser/controller/local/PersistenceException;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 102
    .end local v0           #e:Ljava/io/IOException;
    :catchall_0
    move-exception v6

    :goto_2
    if-eqz v3, :cond_0

    .line 104
    :try_start_3
    invoke-virtual {v3}, Landroid/util/JsonReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 107
    :cond_0
    :goto_3
    throw v6

    .line 43
    .end local v3           #reader:Landroid/util/JsonReader;
    .restart local v1       #name:Ljava/lang/String;
    .restart local v4       #reader:Landroid/util/JsonReader;
    :cond_1
    :try_start_4
    const-string v6, "onlineId"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v4}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v6

    sget-object v7, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-eq v6, v7, :cond_2

    .line 44
    invoke-virtual {v4}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lmiui/resourcebrowser/model/Resource;->setOnlineId(Ljava/lang/String;)V

    goto :goto_0

    .line 102
    .end local v1           #name:Ljava/lang/String;
    :catchall_1
    move-exception v6

    move-object v3, v4

    .end local v4           #reader:Landroid/util/JsonReader;
    .restart local v3       #reader:Landroid/util/JsonReader;
    goto :goto_2

    .line 45
    .end local v3           #reader:Landroid/util/JsonReader;
    .restart local v1       #name:Ljava/lang/String;
    .restart local v4       #reader:Landroid/util/JsonReader;
    :cond_2
    const-string v6, "productId"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {v4}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v6

    sget-object v7, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-eq v6, v7, :cond_3

    .line 46
    invoke-virtual {v4}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lmiui/resourcebrowser/model/Resource;->setProductId(Ljava/lang/String;)V

    goto :goto_0

    .line 47
    :cond_3
    const-string v6, "localPath"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-virtual {v4}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v6

    sget-object v7, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-eq v6, v7, :cond_4

    .line 48
    invoke-virtual {v4}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lmiui/resourcebrowser/model/Resource;->setLocalPath(Ljava/lang/String;)V

    goto :goto_0

    .line 49
    :cond_4
    const-string v6, "onlinePath"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-virtual {v4}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v6

    sget-object v7, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-eq v6, v7, :cond_5

    .line 50
    invoke-virtual {v4}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lmiui/resourcebrowser/model/Resource;->setOnlinePath(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 51
    :cond_5
    const-string v6, "filePath"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-virtual {v4}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v6

    sget-object v7, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-eq v6, v7, :cond_6

    .line 52
    invoke-virtual {v4}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lmiui/resourcebrowser/model/Resource;->setFilePath(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 53
    :cond_6
    const-string v6, "downloadPath"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-virtual {v4}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v6

    sget-object v7, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-eq v6, v7, :cond_7

    .line 54
    invoke-virtual {v4}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lmiui/resourcebrowser/model/Resource;->setDownloadPath(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 55
    :cond_7
    const-string v6, "rightsPath"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    invoke-virtual {v4}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v6

    sget-object v7, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-eq v6, v7, :cond_8

    .line 56
    invoke-virtual {v4}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lmiui/resourcebrowser/model/Resource;->setRightsPath(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 57
    :cond_8
    const-string v6, "platform"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    invoke-virtual {v4}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v6

    sget-object v7, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-eq v6, v7, :cond_9

    .line 58
    invoke-virtual {v4}, Landroid/util/JsonReader;->nextInt()I

    move-result v6

    invoke-virtual {v5, v6}, Lmiui/resourcebrowser/model/Resource;->setPlatform(I)V

    goto/16 :goto_0

    .line 59
    :cond_9
    const-string v6, "status"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    invoke-virtual {v4}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v6

    sget-object v7, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-eq v6, v7, :cond_a

    .line 60
    invoke-virtual {v4}, Landroid/util/JsonReader;->nextInt()I

    move-result v6

    invoke-virtual {v5, v6}, Lmiui/resourcebrowser/model/Resource;->setStatus(I)V

    goto/16 :goto_0

    .line 61
    :cond_a
    const-string v6, "hash"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    invoke-virtual {v4}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v6

    sget-object v7, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-eq v6, v7, :cond_b

    .line 62
    invoke-virtual {v4}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lmiui/resourcebrowser/model/Resource;->setHash(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 63
    :cond_b
    const-string v6, "size"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    invoke-virtual {v4}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v6

    sget-object v7, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-eq v6, v7, :cond_c

    .line 64
    invoke-virtual {v4}, Landroid/util/JsonReader;->nextLong()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lmiui/resourcebrowser/model/Resource;->setSize(J)V

    goto/16 :goto_0

    .line 65
    :cond_c
    const-string v6, "updatedTime"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    invoke-virtual {v4}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v6

    sget-object v7, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-eq v6, v7, :cond_d

    .line 66
    invoke-virtual {v4}, Landroid/util/JsonReader;->nextLong()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lmiui/resourcebrowser/model/Resource;->setUpdatedTime(J)V

    goto/16 :goto_0

    .line 67
    :cond_d
    const-string v6, "title"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_e

    invoke-virtual {v4}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v6

    sget-object v7, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-eq v6, v7, :cond_e

    .line 68
    invoke-virtual {v4}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lmiui/resourcebrowser/model/Resource;->setTitle(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 69
    :cond_e
    const-string v6, "description"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_f

    invoke-virtual {v4}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v6

    sget-object v7, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-eq v6, v7, :cond_f

    .line 70
    invoke-virtual {v4}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lmiui/resourcebrowser/model/Resource;->setDescription(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 71
    :cond_f
    const-string v6, "author"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_10

    invoke-virtual {v4}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v6

    sget-object v7, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-eq v6, v7, :cond_10

    .line 72
    invoke-virtual {v4}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lmiui/resourcebrowser/model/Resource;->setAuthor(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 73
    :cond_10
    const-string v6, "designer"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_11

    invoke-virtual {v4}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v6

    sget-object v7, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-eq v6, v7, :cond_11

    .line 74
    invoke-virtual {v4}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lmiui/resourcebrowser/model/Resource;->setDesigner(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 75
    :cond_11
    const-string v6, "version"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_12

    invoke-virtual {v4}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v6

    sget-object v7, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-eq v6, v7, :cond_12

    .line 76
    invoke-virtual {v4}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lmiui/resourcebrowser/model/Resource;->setVersion(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 77
    :cond_12
    const-string v6, "downloadCount"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_13

    invoke-virtual {v4}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v6

    sget-object v7, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-eq v6, v7, :cond_13

    .line 78
    invoke-virtual {v4}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lmiui/resourcebrowser/model/Resource;->setDownloadCount(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 79
    :cond_13
    const-string v6, "buildInThumbnails"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_14

    invoke-virtual {v4}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v6

    sget-object v7, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-eq v6, v7, :cond_14

    .line 80
    invoke-direct {p0, v4}, Lmiui/resourcebrowser/controller/local/LocalJSONDataParser;->readBuildInImagePaths(Landroid/util/JsonReader;)Ljava/util/List;

    move-result-object v6

    invoke-virtual {v5, v6}, Lmiui/resourcebrowser/model/Resource;->setBuildInThumbnails(Ljava/util/List;)V

    goto/16 :goto_0

    .line 81
    :cond_14
    const-string v6, "buildInPreviews"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_15

    invoke-virtual {v4}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v6

    sget-object v7, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-eq v6, v7, :cond_15

    .line 82
    invoke-direct {p0, v4}, Lmiui/resourcebrowser/controller/local/LocalJSONDataParser;->readBuildInImagePaths(Landroid/util/JsonReader;)Ljava/util/List;

    move-result-object v6

    invoke-virtual {v5, v6}, Lmiui/resourcebrowser/model/Resource;->setBuildInPreviews(Ljava/util/List;)V

    goto/16 :goto_0

    .line 83
    :cond_15
    const-string v6, "thumbnails"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_16

    invoke-virtual {v4}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v6

    sget-object v7, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-eq v6, v7, :cond_16

    .line 84
    invoke-direct {p0, v4}, Lmiui/resourcebrowser/controller/local/LocalJSONDataParser;->readImagePaths(Landroid/util/JsonReader;)Ljava/util/List;

    move-result-object v6

    invoke-virtual {v5, v6}, Lmiui/resourcebrowser/model/Resource;->setThumbnails(Ljava/util/List;)V

    goto/16 :goto_0

    .line 85
    :cond_16
    const-string v6, "previews"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_17

    invoke-virtual {v4}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v6

    sget-object v7, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-eq v6, v7, :cond_17

    .line 86
    invoke-direct {p0, v4}, Lmiui/resourcebrowser/controller/local/LocalJSONDataParser;->readImagePaths(Landroid/util/JsonReader;)Ljava/util/List;

    move-result-object v6

    invoke-virtual {v5, v6}, Lmiui/resourcebrowser/model/Resource;->setPreviews(Ljava/util/List;)V

    goto/16 :goto_0

    .line 87
    :cond_17
    const-string v6, "parentResources"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_18

    invoke-virtual {v4}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v6

    sget-object v7, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-eq v6, v7, :cond_18

    .line 88
    invoke-direct {p0, v4}, Lmiui/resourcebrowser/controller/local/LocalJSONDataParser;->readRelatedResource(Landroid/util/JsonReader;)Ljava/util/List;

    move-result-object v6

    invoke-virtual {v5, v6}, Lmiui/resourcebrowser/model/Resource;->setParentResources(Ljava/util/List;)V

    goto/16 :goto_0

    .line 89
    :cond_18
    const-string v6, "subResources"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_19

    invoke-virtual {v4}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v6

    sget-object v7, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-eq v6, v7, :cond_19

    .line 90
    invoke-direct {p0, v4}, Lmiui/resourcebrowser/controller/local/LocalJSONDataParser;->readRelatedResource(Landroid/util/JsonReader;)Ljava/util/List;

    move-result-object v6

    invoke-virtual {v5, v6}, Lmiui/resourcebrowser/model/Resource;->setSubResources(Ljava/util/List;)V

    goto/16 :goto_0

    .line 91
    :cond_19
    const-string v6, "extraMeta"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1a

    invoke-virtual {v4}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v6

    sget-object v7, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-eq v6, v7, :cond_1a

    .line 92
    invoke-direct {p0, v4}, Lmiui/resourcebrowser/controller/local/LocalJSONDataParser;->readExtraMeta(Landroid/util/JsonReader;)Ljava/util/Map;

    move-result-object v6

    invoke-virtual {v5, v6}, Lmiui/resourcebrowser/model/Resource;->setExtraMeta(Ljava/util/Map;)V

    goto/16 :goto_0

    .line 94
    :cond_1a
    invoke-virtual {v4}, Landroid/util/JsonReader;->skipValue()V

    goto/16 :goto_0

    .line 98
    .end local v1           #name:Ljava/lang/String;
    :cond_1b
    invoke-virtual {v4}, Landroid/util/JsonReader;->endObject()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 102
    if-eqz v4, :cond_1c

    .line 104
    :try_start_5
    invoke-virtual {v4}, Landroid/util/JsonReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    .line 114
    :cond_1c
    :goto_4
    invoke-virtual {v5}, Lmiui/resourcebrowser/model/Resource;->getOnlinePath()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1d

    invoke-virtual {v5}, Lmiui/resourcebrowser/model/Resource;->getOnlineId()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1d

    .line 115
    new-instance v6, Lmiui/resourcebrowser/controller/online/OnlineService;

    iget-object v7, p0, Lmiui/resourcebrowser/controller/local/LocalDataParser;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-direct {v6, v7}, Lmiui/resourcebrowser/controller/online/OnlineService;-><init>(Lmiui/resourcebrowser/ResourceContext;)V

    invoke-virtual {v5}, Lmiui/resourcebrowser/model/Resource;->getOnlineId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lmiui/resourcebrowser/controller/online/OnlineService;->getDownloadUrl(Ljava/lang/String;)Lmiui/resourcebrowser/controller/online/RequestUrl;

    move-result-object v6

    invoke-virtual {v6}, Lmiui/resourcebrowser/controller/online/RequestUrl;->getUrl()Ljava/lang/String;

    move-result-object v2

    .line 116
    .local v2, onlinePath:Ljava/lang/String;
    invoke-virtual {v5, v2}, Lmiui/resourcebrowser/model/Resource;->setOnlinePath(Ljava/lang/String;)V

    .line 119
    .end local v2           #onlinePath:Ljava/lang/String;
    :cond_1d
    return-object v5

    .line 105
    :catch_1
    move-exception v0

    .line 106
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 105
    .end local v0           #e:Ljava/io/IOException;
    .end local v4           #reader:Landroid/util/JsonReader;
    .restart local v3       #reader:Landroid/util/JsonReader;
    :catch_2
    move-exception v0

    .line 106
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    .line 99
    .end local v0           #e:Ljava/io/IOException;
    :catch_3
    move-exception v0

    goto/16 :goto_1
.end method

.method public storeResource(Ljava/io/File;Lmiui/resourcebrowser/model/Resource;)V
    .locals 6
    .parameter "file"
    .parameter "resource"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lmiui/resourcebrowser/controller/local/PersistenceException;
        }
    .end annotation

    .prologue
    .line 199
    const/4 v1, 0x0

    .line 201
    .local v1, writer:Landroid/util/JsonWriter;
    :try_start_0
    new-instance v2, Landroid/util/JsonWriter;

    new-instance v3, Ljava/io/BufferedWriter;

    new-instance v4, Ljava/io/FileWriter;

    invoke-direct {v4, p1}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    invoke-direct {v2, v3}, Landroid/util/JsonWriter;-><init>(Ljava/io/Writer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 202
    .end local v1           #writer:Landroid/util/JsonWriter;
    .local v2, writer:Landroid/util/JsonWriter;
    :try_start_1
    const-string v3, "  "

    invoke-virtual {v2, v3}, Landroid/util/JsonWriter;->setIndent(Ljava/lang/String;)V

    .line 203
    invoke-virtual {v2}, Landroid/util/JsonWriter;->beginObject()Landroid/util/JsonWriter;

    .line 205
    const-string v3, "localId"

    invoke-virtual {v2, v3}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v3

    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getLocalId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 206
    const-string v3, "onlineId"

    invoke-virtual {v2, v3}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v3

    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getOnlineId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 207
    const-string v3, "productId"

    invoke-virtual {v2, v3}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v3

    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getProductId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 208
    const-string v3, "localPath"

    invoke-virtual {v2, v3}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v3

    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 209
    const-string v3, "onlinePath"

    invoke-virtual {v2, v3}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v3

    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getOnlinePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 210
    const-string v3, "filePath"

    invoke-virtual {v2, v3}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v3

    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getFilePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 211
    const-string v3, "downloadPath"

    invoke-virtual {v2, v3}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v3

    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getDownloadPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 212
    const-string v3, "rightsPath"

    invoke-virtual {v2, v3}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v3

    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getRightsPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 213
    const-string v3, "platform"

    invoke-virtual {v2, v3}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v3

    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getPlatform()I

    move-result v4

    int-to-long v4, v4

    invoke-virtual {v3, v4, v5}, Landroid/util/JsonWriter;->value(J)Landroid/util/JsonWriter;

    .line 214
    const-string v3, "status"

    invoke-virtual {v2, v3}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v3

    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getStatus()I

    move-result v4

    int-to-long v4, v4

    invoke-virtual {v3, v4, v5}, Landroid/util/JsonWriter;->value(J)Landroid/util/JsonWriter;

    .line 215
    const-string v3, "hash"

    invoke-virtual {v2, v3}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v3

    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getHash()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 216
    const-string v3, "size"

    invoke-virtual {v2, v3}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v3

    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getSize()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Landroid/util/JsonWriter;->value(J)Landroid/util/JsonWriter;

    .line 217
    const-string v3, "updatedTime"

    invoke-virtual {v2, v3}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v3

    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getUpdatedTime()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Landroid/util/JsonWriter;->value(J)Landroid/util/JsonWriter;

    .line 218
    const-string v3, "title"

    invoke-virtual {v2, v3}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v3

    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 219
    const-string v3, "description"

    invoke-virtual {v2, v3}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v3

    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getDescription()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 220
    const-string v3, "author"

    invoke-virtual {v2, v3}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v3

    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getAuthor()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 221
    const-string v3, "designer"

    invoke-virtual {v2, v3}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v3

    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getDesigner()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 222
    const-string v3, "version"

    invoke-virtual {v2, v3}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v3

    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getVersion()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 223
    const-string v3, "downloadCount"

    invoke-virtual {v2, v3}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v3

    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getDownloadCount()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 225
    const-string v3, "buildInThumbnails"

    invoke-virtual {v2, v3}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 226
    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getBuildInThumbnails()Ljava/util/List;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lmiui/resourcebrowser/controller/local/LocalJSONDataParser;->writeBuildInImagePaths(Landroid/util/JsonWriter;Ljava/util/List;)V

    .line 228
    const-string v3, "buildInPreviews"

    invoke-virtual {v2, v3}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 229
    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getBuildInPreviews()Ljava/util/List;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lmiui/resourcebrowser/controller/local/LocalJSONDataParser;->writeBuildInImagePaths(Landroid/util/JsonWriter;Ljava/util/List;)V

    .line 231
    const-string v3, "thumbnails"

    invoke-virtual {v2, v3}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 232
    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getThumbnails()Ljava/util/List;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lmiui/resourcebrowser/controller/local/LocalJSONDataParser;->writeImagePaths(Landroid/util/JsonWriter;Ljava/util/List;)V

    .line 234
    const-string v3, "previews"

    invoke-virtual {v2, v3}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 235
    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getPreviews()Ljava/util/List;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lmiui/resourcebrowser/controller/local/LocalJSONDataParser;->writeImagePaths(Landroid/util/JsonWriter;Ljava/util/List;)V

    .line 237
    const-string v3, "parentResources"

    invoke-virtual {v2, v3}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 238
    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getParentResources()Ljava/util/List;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lmiui/resourcebrowser/controller/local/LocalJSONDataParser;->writeRelatedResources(Landroid/util/JsonWriter;Ljava/util/List;)V

    .line 240
    const-string v3, "subResources"

    invoke-virtual {v2, v3}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 241
    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getSubResources()Ljava/util/List;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lmiui/resourcebrowser/controller/local/LocalJSONDataParser;->writeRelatedResources(Landroid/util/JsonWriter;Ljava/util/List;)V

    .line 243
    const-string v3, "extraMeta"

    invoke-virtual {v2, v3}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 244
    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getExtraMeta()Ljava/util/Map;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lmiui/resourcebrowser/controller/local/LocalJSONDataParser;->writeExtraMeta(Landroid/util/JsonWriter;Ljava/util/Map;)V

    .line 246
    invoke-virtual {v2}, Landroid/util/JsonWriter;->endObject()Landroid/util/JsonWriter;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    .line 250
    if-eqz v2, :cond_0

    .line 252
    :try_start_2
    invoke-virtual {v2}, Landroid/util/JsonWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 258
    :cond_0
    :goto_0
    return-void

    .line 253
    :catch_0
    move-exception v0

    .line 254
    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 247
    .end local v0           #e:Ljava/io/IOException;
    .end local v2           #writer:Landroid/util/JsonWriter;
    .restart local v1       #writer:Landroid/util/JsonWriter;
    :catch_1
    move-exception v0

    .line 248
    .restart local v0       #e:Ljava/io/IOException;
    :goto_1
    :try_start_3
    new-instance v3, Lmiui/resourcebrowser/controller/local/PersistenceException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lmiui/resourcebrowser/controller/local/PersistenceException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 250
    .end local v0           #e:Ljava/io/IOException;
    :catchall_0
    move-exception v3

    :goto_2
    if-eqz v1, :cond_1

    .line 252
    :try_start_4
    invoke-virtual {v1}, Landroid/util/JsonWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 255
    :cond_1
    :goto_3
    throw v3

    .line 253
    :catch_2
    move-exception v0

    .line 254
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 250
    .end local v0           #e:Ljava/io/IOException;
    .end local v1           #writer:Landroid/util/JsonWriter;
    .restart local v2       #writer:Landroid/util/JsonWriter;
    :catchall_1
    move-exception v3

    move-object v1, v2

    .end local v2           #writer:Landroid/util/JsonWriter;
    .restart local v1       #writer:Landroid/util/JsonWriter;
    goto :goto_2

    .line 247
    .end local v1           #writer:Landroid/util/JsonWriter;
    .restart local v2       #writer:Landroid/util/JsonWriter;
    :catch_3
    move-exception v0

    move-object v1, v2

    .end local v2           #writer:Landroid/util/JsonWriter;
    .restart local v1       #writer:Landroid/util/JsonWriter;
    goto :goto_1
.end method
