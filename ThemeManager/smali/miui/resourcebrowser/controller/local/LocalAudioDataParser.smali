.class public Lmiui/resourcebrowser/controller/local/LocalAudioDataParser;
.super Lmiui/resourcebrowser/controller/local/LocalBareDataParser;
.source "LocalAudioDataParser.java"


# instance fields
.field private mMaxDurationLimit:I

.field private mMinDurationLimit:I

.field private mRingtoneType:I


# direct methods
.method public constructor <init>(Lmiui/resourcebrowser/ResourceContext;)V
    .locals 1
    .parameter "context"

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lmiui/resourcebrowser/controller/local/LocalBareDataParser;-><init>(Lmiui/resourcebrowser/ResourceContext;)V

    .line 14
    const/4 v0, 0x0

    iput v0, p0, Lmiui/resourcebrowser/controller/local/LocalAudioDataParser;->mMinDurationLimit:I

    .line 15
    const v0, 0x7fffffff

    iput v0, p0, Lmiui/resourcebrowser/controller/local/LocalAudioDataParser;->mMaxDurationLimit:I

    .line 21
    return-void
.end method

.method private matchLimitation(J)Z
    .locals 2
    .parameter "duration"

    .prologue
    .line 39
    iget v0, p0, Lmiui/resourcebrowser/controller/local/LocalAudioDataParser;->mMinDurationLimit:I

    int-to-long v0, v0

    cmp-long v0, v0, p1

    if-gtz v0, :cond_0

    iget v0, p0, Lmiui/resourcebrowser/controller/local/LocalAudioDataParser;->mMaxDurationLimit:I

    int-to-long v0, v0

    cmp-long v0, p1, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public loadResource(Ljava/io/File;)Lmiui/resourcebrowser/model/Resource;
    .locals 5
    .parameter "file"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lmiui/resourcebrowser/controller/local/PersistenceException;
        }
    .end annotation

    .prologue
    .line 44
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lmiui/resourcebrowser/util/ResourceHelper;->getLocalRingtoneDuration(Ljava/lang/String;)J

    move-result-wide v0

    .line 45
    .local v0, duration:J
    const-wide/16 v3, 0x0

    cmp-long v3, v0, v3

    if-ltz v3, :cond_0

    invoke-direct {p0, v0, v1}, Lmiui/resourcebrowser/controller/local/LocalAudioDataParser;->matchLimitation(J)Z

    move-result v3

    if-nez v3, :cond_1

    .line 46
    :cond_0
    const/4 v2, 0x0

    .line 50
    :goto_0
    return-object v2

    .line 48
    :cond_1
    invoke-super {p0, p1}, Lmiui/resourcebrowser/controller/local/LocalBareDataParser;->loadResource(Ljava/io/File;)Lmiui/resourcebrowser/model/Resource;

    move-result-object v2

    .line 49
    .local v2, resource:Lmiui/resourcebrowser/model/Resource;
    const-string v3, "duration"

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lmiui/resourcebrowser/model/Resource;->putExtraMeta(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setDurationLimitation(II)V
    .locals 0
    .parameter "minLimit"
    .parameter "maxLimit"

    .prologue
    .line 34
    iput p1, p0, Lmiui/resourcebrowser/controller/local/LocalAudioDataParser;->mMinDurationLimit:I

    .line 35
    iput p2, p0, Lmiui/resourcebrowser/controller/local/LocalAudioDataParser;->mMaxDurationLimit:I

    .line 36
    return-void
.end method

.method public updateState()V
    .locals 7

    .prologue
    const/16 v3, 0x1388

    const/4 v6, 0x2

    .line 25
    iget-object v2, p0, Lmiui/resourcebrowser/controller/local/LocalDataParser;->context:Lmiui/resourcebrowser/ResourceContext;

    const-string v4, "android.intent.extra.ringtone.TYPE"

    invoke-virtual {v2, v4}, Lmiui/resourcebrowser/ResourceContext;->getExtraMeta(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, p0, Lmiui/resourcebrowser/controller/local/LocalAudioDataParser;->mRingtoneType:I

    .line 26
    iget-object v4, p0, Lmiui/resourcebrowser/controller/local/LocalDataParser;->context:Lmiui/resourcebrowser/ResourceContext;

    const-string v5, "resourcebrowser.RINGTONE_MIN_DURATION_LIMIT"

    iget v2, p0, Lmiui/resourcebrowser/controller/local/LocalAudioDataParser;->mRingtoneType:I

    if-ne v2, v6, :cond_0

    const/4 v2, 0x0

    :goto_0
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v4, v5, v2}, Lmiui/resourcebrowser/ResourceContext;->getExtraMeta(Ljava/lang/String;Ljava/io/Serializable;)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 28
    .local v1, minLimit:I
    iget-object v2, p0, Lmiui/resourcebrowser/controller/local/LocalDataParser;->context:Lmiui/resourcebrowser/ResourceContext;

    const-string v4, "resourcebrowser.RINGTONE_MAX_DURATION_LIMIT"

    iget v5, p0, Lmiui/resourcebrowser/controller/local/LocalAudioDataParser;->mRingtoneType:I

    if-ne v5, v6, :cond_1

    :goto_1
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, Lmiui/resourcebrowser/ResourceContext;->getExtraMeta(Ljava/lang/String;Ljava/io/Serializable;)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 30
    .local v0, maxLimit:I
    invoke-virtual {p0, v1, v0}, Lmiui/resourcebrowser/controller/local/LocalAudioDataParser;->setDurationLimitation(II)V

    .line 31
    return-void

    .end local v0           #maxLimit:I
    .end local v1           #minLimit:I
    :cond_0
    move v2, v3

    .line 26
    goto :goto_0

    .line 28
    .restart local v1       #minLimit:I
    :cond_1
    const v3, 0x7fffffff

    goto :goto_1
.end method
