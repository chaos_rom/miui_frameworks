.class public Lmiui/resourcebrowser/controller/local/AudioDataManager;
.super Lmiui/resourcebrowser/controller/local/FileSystemDataManager;
.source "AudioDataManager.java"


# static fields
.field private static final COMMON_RINGTONE_SUFFIX:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 19
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, ".mp3"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, ".ogg"

    aput-object v2, v0, v1

    sput-object v0, Lmiui/resourcebrowser/controller/local/AudioDataManager;->COMMON_RINGTONE_SUFFIX:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lmiui/resourcebrowser/ResourceContext;)V
    .locals 0
    .parameter "context"

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;-><init>(Lmiui/resourcebrowser/ResourceContext;)V

    .line 25
    return-void
.end method

.method private getDefaultOptionResource()Lmiui/resourcebrowser/model/Resource;
    .locals 13

    .prologue
    const/4 v12, 0x0

    .line 54
    invoke-static {}, Lmiui/resourcebrowser/AppInnerContext;->getInstance()Lmiui/resourcebrowser/AppInnerContext;

    move-result-object v9

    invoke-virtual {v9}, Lmiui/resourcebrowser/AppInnerContext;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 55
    .local v0, ctx:Landroid/content/Context;
    new-instance v4, Lmiui/resourcebrowser/model/Resource;

    invoke-direct {v4}, Lmiui/resourcebrowser/model/Resource;-><init>()V

    .line 57
    .local v4, resource:Lmiui/resourcebrowser/model/Resource;
    iget-object v9, p0, Lmiui/resourcebrowser/controller/DataManager;->context:Lmiui/resourcebrowser/ResourceContext;

    const-string v10, "android.intent.extra.ringtone.DEFAULT_URI"

    invoke-virtual {v9, v10}, Lmiui/resourcebrowser/ResourceContext;->getExtraMeta(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 58
    .local v8, uriStr:Ljava/lang/String;
    iget-object v9, p0, Lmiui/resourcebrowser/controller/DataManager;->context:Lmiui/resourcebrowser/ResourceContext;

    const-string v10, "android.intent.extra.ringtone.TYPE"

    invoke-virtual {v9, v10}, Lmiui/resourcebrowser/ResourceContext;->getExtraMeta(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 60
    .local v5, ringtoneType:I
    const/4 v1, 0x0

    .line 61
    .local v1, defaultUri:Landroid/net/Uri;
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 62
    invoke-direct {p0, v5}, Lmiui/resourcebrowser/controller/local/AudioDataManager;->getSystemDefaultRingtoneUri(I)Landroid/net/Uri;

    move-result-object v1

    .line 67
    :goto_0
    invoke-static {v0, v1}, Lmiui/resourcebrowser/util/ResourceHelper;->getPathByUri(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    .line 68
    .local v2, path:Ljava/lang/String;
    invoke-virtual {v4, v2}, Lmiui/resourcebrowser/model/Resource;->setLocalPath(Ljava/lang/String;)V

    .line 69
    invoke-virtual {v4, v2}, Lmiui/resourcebrowser/model/Resource;->setFilePath(Ljava/lang/String;)V

    .line 70
    invoke-virtual {v4, v2}, Lmiui/resourcebrowser/model/Resource;->setDownloadPath(Ljava/lang/String;)V

    .line 72
    invoke-static {v0, v1, v12}, Landroid/media/ExtraRingtone;->getRingtoneTitle(Landroid/content/Context;Landroid/net/Uri;Z)Ljava/lang/String;

    move-result-object v6

    .line 73
    .local v6, songName:Ljava/lang/String;
    invoke-direct {p0, v6}, Lmiui/resourcebrowser/controller/local/AudioDataManager;->removeFileNameSuffix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 74
    const-string v7, ""

    .line 75
    .local v7, title:Ljava/lang/String;
    invoke-direct {p0, v5}, Lmiui/resourcebrowser/controller/local/AudioDataManager;->getSystemDefaultRingtoneNameId(I)I

    move-result v3

    .line 76
    .local v3, resId:I
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 77
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 81
    :goto_1
    invoke-virtual {v4, v7}, Lmiui/resourcebrowser/model/Resource;->setTitle(Ljava/lang/String;)V

    .line 83
    return-object v4

    .line 64
    .end local v2           #path:Ljava/lang/String;
    .end local v3           #resId:I
    .end local v6           #songName:Ljava/lang/String;
    .end local v7           #title:Ljava/lang/String;
    :cond_0
    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_0

    .line 79
    .restart local v2       #path:Ljava/lang/String;
    .restart local v3       #resId:I
    .restart local v6       #songName:Ljava/lang/String;
    .restart local v7       #title:Ljava/lang/String;
    :cond_1
    const-string v9, "%s (%s)"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    invoke-virtual {v11, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v12

    const/4 v11, 0x1

    aput-object v6, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    goto :goto_1
.end method

.method private getSilentOptionResource()Lmiui/resourcebrowser/model/Resource;
    .locals 4

    .prologue
    .line 44
    new-instance v1, Lmiui/resourcebrowser/model/Resource;

    invoke-direct {v1}, Lmiui/resourcebrowser/model/Resource;-><init>()V

    .line 45
    .local v1, resource:Lmiui/resourcebrowser/model/Resource;
    const-string v0, ""

    .line 46
    .local v0, path:Ljava/lang/String;
    invoke-virtual {v1, v0}, Lmiui/resourcebrowser/model/Resource;->setLocalPath(Ljava/lang/String;)V

    .line 47
    invoke-virtual {v1, v0}, Lmiui/resourcebrowser/model/Resource;->setFilePath(Ljava/lang/String;)V

    .line 48
    invoke-virtual {v1, v0}, Lmiui/resourcebrowser/model/Resource;->setDownloadPath(Ljava/lang/String;)V

    .line 49
    invoke-static {}, Lmiui/resourcebrowser/AppInnerContext;->getInstance()Lmiui/resourcebrowser/AppInnerContext;

    move-result-object v2

    invoke-virtual {v2}, Lmiui/resourcebrowser/AppInnerContext;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x60c0006

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmiui/resourcebrowser/model/Resource;->setTitle(Ljava/lang/String;)V

    .line 50
    return-object v1
.end method

.method private getSystemDefaultRingtoneNameId(I)I
    .locals 1
    .parameter "ringtoneType"

    .prologue
    .line 103
    const v0, 0x60c0007

    .line 104
    .local v0, resId:I
    packed-switch p1, :pswitch_data_0

    .line 115
    :goto_0
    :pswitch_0
    return v0

    .line 106
    :pswitch_1
    const v0, 0x60c0008

    .line 107
    goto :goto_0

    .line 109
    :pswitch_2
    const v0, 0x60c0009

    .line 110
    goto :goto_0

    .line 112
    :pswitch_3
    const v0, 0x60c000a

    goto :goto_0

    .line 104
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private getSystemDefaultRingtoneUri(I)Landroid/net/Uri;
    .locals 1
    .parameter "ringtoneType"

    .prologue
    .line 87
    const/4 v0, 0x0

    .line 88
    .local v0, defaultUri:Landroid/net/Uri;
    packed-switch p1, :pswitch_data_0

    .line 99
    :goto_0
    :pswitch_0
    return-object v0

    .line 90
    :pswitch_1
    sget-object v0, Landroid/provider/Settings$System;->DEFAULT_RINGTONE_URI:Landroid/net/Uri;

    .line 91
    goto :goto_0

    .line 93
    :pswitch_2
    sget-object v0, Landroid/provider/Settings$System;->DEFAULT_NOTIFICATION_URI:Landroid/net/Uri;

    .line 94
    goto :goto_0

    .line 96
    :pswitch_3
    sget-object v0, Landroid/provider/Settings$System;->DEFAULT_ALARM_ALERT_URI:Landroid/net/Uri;

    goto :goto_0

    .line 88
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private removeFileNameSuffix(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .parameter "fileName"

    .prologue
    .line 119
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 120
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    .line 121
    .local v2, ingoreCaseName:Ljava/lang/String;
    sget-object v0, Lmiui/resourcebrowser/controller/local/AudioDataManager;->COMMON_RINGTONE_SUFFIX:[Ljava/lang/String;

    .local v0, arr$:[Ljava/lang/String;
    array-length v3, v0

    .local v3, len$:I
    const/4 v1, 0x0

    .local v1, i$:I
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v0, v1

    .line 122
    .local v4, suffix:Ljava/lang/String;
    invoke-virtual {v2, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 123
    const/4 v5, 0x0

    invoke-virtual {v2, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {p1, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 127
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #i$:I
    .end local v2           #ingoreCaseName:Ljava/lang/String;
    .end local v3           #len$:I
    .end local v4           #suffix:Ljava/lang/String;
    .end local p1
    :cond_0
    return-object p1

    .line 121
    .restart local v0       #arr$:[Ljava/lang/String;
    .restart local v1       #i$:I
    .restart local v2       #ingoreCaseName:Ljava/lang/String;
    .restart local v3       #len$:I
    .restart local v4       #suffix:Ljava/lang/String;
    .restart local p1
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected getDataParser()Lmiui/resourcebrowser/controller/local/LocalDataParser;
    .locals 2

    .prologue
    .line 29
    new-instance v0, Lmiui/resourcebrowser/controller/local/LocalAudioDataParser;

    iget-object v1, p0, Lmiui/resourcebrowser/controller/DataManager;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-direct {v0, v1}, Lmiui/resourcebrowser/controller/local/LocalAudioDataParser;-><init>(Lmiui/resourcebrowser/ResourceContext;)V

    return-object v0
.end method

.method protected refreshResources()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 34
    invoke-super {p0}, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->refreshResources()V

    .line 35
    iget-object v0, p0, Lmiui/resourcebrowser/controller/DataManager;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v0}, Lmiui/resourcebrowser/ResourceContext;->isPicker()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiui/resourcebrowser/controller/DataManager;->context:Lmiui/resourcebrowser/ResourceContext;

    const-string v1, "android.intent.extra.ringtone.SHOW_DEFAULT"

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/ResourceContext;->getExtraMeta(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p0, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->dataSet:Ljava/util/List;

    invoke-direct {p0}, Lmiui/resourcebrowser/controller/local/AudioDataManager;->getDefaultOptionResource()Lmiui/resourcebrowser/model/Resource;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 38
    :cond_0
    iget-object v0, p0, Lmiui/resourcebrowser/controller/DataManager;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v0}, Lmiui/resourcebrowser/ResourceContext;->isPicker()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmiui/resourcebrowser/controller/DataManager;->context:Lmiui/resourcebrowser/ResourceContext;

    const-string v1, "android.intent.extra.ringtone.SHOW_SILENT"

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/ResourceContext;->getExtraMeta(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 39
    :cond_1
    iget-object v0, p0, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->dataSet:Ljava/util/List;

    invoke-direct {p0}, Lmiui/resourcebrowser/controller/local/AudioDataManager;->getSilentOptionResource()Lmiui/resourcebrowser/model/Resource;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 41
    :cond_2
    return-void
.end method
