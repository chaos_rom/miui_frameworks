.class public abstract Lmiui/resourcebrowser/controller/local/ContextParser;
.super Ljava/lang/Object;
.source "ContextParser.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract loadResourceContext(Ljava/io/File;)Lmiui/resourcebrowser/ResourceContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lmiui/resourcebrowser/controller/local/PersistenceException;
        }
    .end annotation
.end method

.method public abstract storeResourceContext(Ljava/io/File;Lmiui/resourcebrowser/ResourceContext;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lmiui/resourcebrowser/controller/local/PersistenceException;
        }
    .end annotation
.end method
