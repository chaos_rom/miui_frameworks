.class public Lmiui/resourcebrowser/controller/local/FileSystemDataManager;
.super Lmiui/resourcebrowser/controller/LocalDataManager;
.source "FileSystemDataManager.java"

# interfaces
.implements Lmiui/resourcebrowser/ResourceConstants;


# instance fields
.field private dataFolderCache:Lmiui/cache/FolderCache;

.field protected dataSet:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;"
        }
    .end annotation
.end field

.field private localIdIndex:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;"
        }
    .end annotation
.end field

.field private onlineIdIndex:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;"
        }
    .end annotation
.end field

.field private parser:Lmiui/resourcebrowser/controller/local/LocalDataParser;

.field private searchCache:Lmiui/cache/DataCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lmiui/cache/DataCache",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;>;"
        }
    .end annotation
.end field

.field private searchStrategy:Lmiui/resourcebrowser/controller/strategy/SearchStrategy;

.field private sortStrategy:Lmiui/resourcebrowser/controller/strategy/SortStrategy;

.field private visitedMap:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lmiui/resourcebrowser/ResourceContext;)V
    .locals 2
    .parameter "context"

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lmiui/resourcebrowser/controller/LocalDataManager;-><init>(Lmiui/resourcebrowser/ResourceContext;)V

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->dataSet:Ljava/util/List;

    .line 28
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->localIdIndex:Ljava/util/Map;

    .line 29
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->onlineIdIndex:Ljava/util/Map;

    .line 30
    new-instance v0, Lmiui/cache/DataCache;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Lmiui/cache/DataCache;-><init>(I)V

    iput-object v0, p0, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->searchCache:Lmiui/cache/DataCache;

    .line 32
    new-instance v0, Lmiui/cache/FolderCache;

    invoke-direct {v0}, Lmiui/cache/FolderCache;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->dataFolderCache:Lmiui/cache/FolderCache;

    .line 34
    new-instance v0, Lmiui/resourcebrowser/controller/strategy/DefaultSearchStrategy;

    invoke-direct {v0}, Lmiui/resourcebrowser/controller/strategy/DefaultSearchStrategy;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->searchStrategy:Lmiui/resourcebrowser/controller/strategy/SearchStrategy;

    .line 35
    new-instance v0, Lmiui/resourcebrowser/controller/strategy/ModifiedTimeSortStrategy;

    invoke-direct {v0}, Lmiui/resourcebrowser/controller/strategy/ModifiedTimeSortStrategy;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->sortStrategy:Lmiui/resourcebrowser/controller/strategy/SortStrategy;

    .line 39
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->visitedMap:Ljava/util/Set;

    .line 43
    invoke-virtual {p0}, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->getDataParser()Lmiui/resourcebrowser/controller/local/LocalDataParser;

    move-result-object v0

    iput-object v0, p0, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->parser:Lmiui/resourcebrowser/controller/local/LocalDataParser;

    .line 44
    return-void
.end method

.method private isRelatedResourceValid(Ljava/util/List;)Z
    .locals 9
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/RelatedResource;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, relatedResources:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/RelatedResource;>;"
    const/4 v6, 0x0

    .line 152
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lmiui/resourcebrowser/model/RelatedResource;

    .line 153
    .local v4, relatedResource:Lmiui/resourcebrowser/model/RelatedResource;
    invoke-virtual {v4}, Lmiui/resourcebrowser/model/RelatedResource;->getLocalPath()Ljava/lang/String;

    move-result-object v3

    .line 154
    .local v3, localPath:Ljava/lang/String;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 155
    iget-object v7, p0, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->visitedMap:Ljava/util/Set;

    invoke-interface {v7, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 158
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 159
    .local v2, localFile:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 161
    :try_start_0
    invoke-direct {p0, v2}, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->loadResource(Ljava/io/File;)Lmiui/resourcebrowser/model/Resource;

    move-result-object v5

    .line 162
    .local v5, resource:Lmiui/resourcebrowser/model/Resource;
    invoke-direct {p0, v5}, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->isResourceValid(Lmiui/resourcebrowser/model/Resource;)Z
    :try_end_0
    .catch Lmiui/resourcebrowser/controller/local/PersistenceException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    if-nez v7, :cond_0

    .line 173
    .end local v2           #localFile:Ljava/io/File;
    .end local v3           #localPath:Ljava/lang/String;
    .end local v4           #relatedResource:Lmiui/resourcebrowser/model/RelatedResource;
    .end local v5           #resource:Lmiui/resourcebrowser/model/Resource;
    :cond_1
    :goto_0
    return v6

    .line 165
    .restart local v2       #localFile:Ljava/io/File;
    .restart local v3       #localPath:Ljava/lang/String;
    .restart local v4       #relatedResource:Lmiui/resourcebrowser/model/RelatedResource;
    :catch_0
    move-exception v0

    .line 166
    .local v0, e:Lmiui/resourcebrowser/controller/local/PersistenceException;
    goto :goto_0

    .line 173
    .end local v0           #e:Lmiui/resourcebrowser/controller/local/PersistenceException;
    .end local v2           #localFile:Ljava/io/File;
    .end local v3           #localPath:Ljava/lang/String;
    .end local v4           #relatedResource:Lmiui/resourcebrowser/model/RelatedResource;
    :cond_2
    const/4 v6, 0x1

    goto :goto_0
.end method

.method private isResourceValid(Lmiui/resourcebrowser/model/Resource;)Z
    .locals 5
    .parameter "resource"

    .prologue
    const/4 v2, 0x0

    .line 130
    if-nez p1, :cond_1

    .line 148
    :cond_0
    :goto_0
    return v2

    .line 133
    :cond_1
    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v1

    .line 134
    .local v1, localPath:Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 137
    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getFilePath()Ljava/lang/String;

    move-result-object v0

    .line 138
    .local v0, filePath:Ljava/lang/String;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 141
    :cond_2
    iget-object v3, p0, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->visitedMap:Ljava/util/Set;

    invoke-interface {v3, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 142
    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getSubResources()Ljava/util/List;

    move-result-object v3

    invoke-direct {p0, v3}, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->isRelatedResourceValid(Ljava/util/List;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 145
    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getParentResources()Ljava/util/List;

    move-result-object v3

    invoke-direct {p0, v3}, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->isRelatedResourceValid(Ljava/util/List;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 148
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private loadResource(Ljava/io/File;)Lmiui/resourcebrowser/model/Resource;
    .locals 1
    .parameter "file"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lmiui/resourcebrowser/controller/local/PersistenceException;
        }
    .end annotation

    .prologue
    .line 177
    iget-object v0, p0, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->parser:Lmiui/resourcebrowser/controller/local/LocalDataParser;

    invoke-virtual {v0, p1}, Lmiui/resourcebrowser/controller/local/LocalDataParser;->loadResource(Ljava/io/File;)Lmiui/resourcebrowser/model/Resource;

    move-result-object v0

    return-object v0
.end method

.method private loadResources(Ljava/lang/String;)Ljava/util/List;
    .locals 9
    .parameter "dataFolder"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 105
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 106
    .local v7, resources:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/Resource;>;"
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 107
    .local v3, folder:Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 108
    iget-object v8, p0, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->parser:Lmiui/resourcebrowser/controller/local/LocalDataParser;

    invoke-virtual {v8}, Lmiui/resourcebrowser/controller/local/LocalDataParser;->updateState()V

    .line 109
    invoke-virtual {v3}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .local v0, arr$:[Ljava/io/File;
    array-length v5, v0

    .local v5, len$:I
    const/4 v4, 0x0

    .local v4, i$:I
    :goto_0
    if-ge v4, v5, :cond_2

    aget-object v2, v0, v4

    .line 111
    .local v2, file:Ljava/io/File;
    :try_start_0
    invoke-direct {p0, v2}, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->loadResource(Ljava/io/File;)Lmiui/resourcebrowser/model/Resource;

    move-result-object v6

    .line 112
    .local v6, resource:Lmiui/resourcebrowser/model/Resource;
    if-eqz v6, :cond_0

    .line 113
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lmiui/resourcebrowser/model/Resource;->setLocalPath(Ljava/lang/String;)V

    .line 114
    invoke-direct {p0, v6}, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->isResourceValid(Lmiui/resourcebrowser/model/Resource;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 115
    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 109
    .end local v6           #resource:Lmiui/resourcebrowser/model/Resource;
    :cond_0
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 117
    .restart local v6       #resource:Lmiui/resourcebrowser/model/Resource;
    :cond_1
    invoke-virtual {p0, v6}, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->removeResource(Lmiui/resourcebrowser/model/Resource;)Z
    :try_end_0
    .catch Lmiui/resourcebrowser/controller/local/PersistenceException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 120
    .end local v6           #resource:Lmiui/resourcebrowser/model/Resource;
    :catch_0
    move-exception v1

    .line 121
    .local v1, e:Lmiui/resourcebrowser/controller/local/PersistenceException;
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    goto :goto_1

    .line 125
    .end local v0           #arr$:[Ljava/io/File;
    .end local v1           #e:Lmiui/resourcebrowser/controller/local/PersistenceException;
    .end local v2           #file:Ljava/io/File;
    .end local v4           #i$:I
    .end local v5           #len$:I
    :cond_2
    iget-object v8, p0, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->sortStrategy:Lmiui/resourcebrowser/controller/strategy/SortStrategy;

    invoke-virtual {v8, v7}, Lmiui/resourcebrowser/controller/strategy/SortStrategy;->sort(Ljava/util/List;)Ljava/util/List;

    move-result-object v7

    .line 126
    return-object v7
.end method

.method private removeRelatedResources(Ljava/util/List;)Z
    .locals 13
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/RelatedResource;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, relatedResources:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/RelatedResource;>;"
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 278
    const/4 v1, 0x1

    .line 279
    .local v1, deleted:Z
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, i$:Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lmiui/resourcebrowser/model/RelatedResource;

    .line 280
    .local v7, relatedResource:Lmiui/resourcebrowser/model/RelatedResource;
    invoke-virtual {v7}, Lmiui/resourcebrowser/model/RelatedResource;->getLocalPath()Ljava/lang/String;

    move-result-object v6

    .line 281
    .local v6, localPath:Ljava/lang/String;
    if-eqz v6, :cond_0

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    const-string v12, ""

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_0

    .line 282
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 283
    .local v5, localFile:Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_3

    .line 285
    :try_start_0
    invoke-direct {p0, v5}, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->loadResource(Ljava/io/File;)Lmiui/resourcebrowser/model/Resource;

    move-result-object v8

    .line 286
    .local v8, resource:Lmiui/resourcebrowser/model/Resource;
    invoke-virtual {p0, v8}, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->removeResource(Lmiui/resourcebrowser/model/Resource;)Z
    :try_end_0
    .catch Lmiui/resourcebrowser/controller/local/PersistenceException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v11

    if-eqz v11, :cond_1

    if-eqz v1, :cond_1

    move v1, v9

    :goto_1
    goto :goto_0

    :cond_1
    move v1, v10

    goto :goto_1

    .line 287
    .end local v8           #resource:Lmiui/resourcebrowser/model/Resource;
    :catch_0
    move-exception v2

    .line 288
    .local v2, e:Lmiui/resourcebrowser/controller/local/PersistenceException;
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    move-result v11

    if-eqz v11, :cond_2

    if-eqz v1, :cond_2

    move v1, v9

    .line 289
    :goto_2
    goto :goto_0

    :cond_2
    move v1, v10

    .line 288
    goto :goto_2

    .line 291
    .end local v2           #e:Lmiui/resourcebrowser/controller/local/PersistenceException;
    :cond_3
    invoke-virtual {v7}, Lmiui/resourcebrowser/model/RelatedResource;->getFilePath()Ljava/lang/String;

    move-result-object v3

    .line 292
    .local v3, filePath:Ljava/lang/String;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    const-string v12, ""

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_0

    .line 293
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 294
    .local v0, contentFile:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 295
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v11

    if-eqz v11, :cond_4

    if-eqz v1, :cond_4

    move v1, v9

    :goto_3
    goto :goto_0

    :cond_4
    move v1, v10

    goto :goto_3

    .line 301
    .end local v0           #contentFile:Ljava/io/File;
    .end local v3           #filePath:Ljava/lang/String;
    .end local v5           #localFile:Ljava/io/File;
    .end local v6           #localPath:Ljava/lang/String;
    .end local v7           #relatedResource:Lmiui/resourcebrowser/model/RelatedResource;
    :cond_5
    return v1
.end method

.method private storeResource(Ljava/io/File;Lmiui/resourcebrowser/model/Resource;)V
    .locals 1
    .parameter "file"
    .parameter "resource"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lmiui/resourcebrowser/controller/local/PersistenceException;
        }
    .end annotation

    .prologue
    .line 181
    iget-object v0, p0, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->parser:Lmiui/resourcebrowser/controller/local/LocalDataParser;

    invoke-virtual {v0, p1, p2}, Lmiui/resourcebrowser/controller/local/LocalDataParser;->storeResource(Ljava/io/File;Lmiui/resourcebrowser/model/Resource;)V

    .line 182
    return-void
.end method


# virtual methods
.method public addResource(Lmiui/resourcebrowser/model/Resource;)Z
    .locals 4
    .parameter "resource"

    .prologue
    .line 222
    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v2

    .line 223
    .local v2, localPath:Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 225
    .local v1, file:Ljava/io/File;
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    .line 226
    invoke-direct {p0, v1, p1}, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->storeResource(Ljava/io/File;Lmiui/resourcebrowser/model/Resource;)V

    .line 227
    invoke-virtual {p0}, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->notifyDataSetUpdateSuccessful()V
    :try_end_0
    .catch Lmiui/resourcebrowser/controller/local/PersistenceException; {:try_start_0 .. :try_end_0} :catch_0

    .line 232
    const/4 v3, 0x1

    :goto_0
    return v3

    .line 228
    :catch_0
    move-exception v0

    .line 229
    .local v0, e:Lmiui/resourcebrowser/controller/local/PersistenceException;
    invoke-virtual {p0}, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->notifyDataSetUpdateFailed()V

    .line 230
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public findResources(Ljava/lang/String;)Ljava/util/List;
    .locals 4
    .parameter "keyword"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 186
    iget-object v3, p0, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->searchCache:Lmiui/cache/DataCache;

    invoke-virtual {v3, p1}, Lmiui/cache/DataCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 187
    .local v2, searchResources:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/Resource;>;"
    if-nez v2, :cond_2

    .line 188
    new-instance v2, Ljava/util/ArrayList;

    .end local v2           #searchResources:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/Resource;>;"
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 189
    .restart local v2       #searchResources:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/Resource;>;"
    iget-object v3, p0, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->dataSet:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/resourcebrowser/model/Resource;

    .line 190
    .local v1, resource:Lmiui/resourcebrowser/model/Resource;
    iget-object v3, p0, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->searchStrategy:Lmiui/resourcebrowser/controller/strategy/SearchStrategy;

    invoke-virtual {v3, p1, v1}, Lmiui/resourcebrowser/controller/strategy/SearchStrategy;->isHitted(Ljava/lang/String;Lmiui/resourcebrowser/model/Resource;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 191
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 194
    .end local v1           #resource:Lmiui/resourcebrowser/model/Resource;
    :cond_1
    iget-object v3, p0, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->sortStrategy:Lmiui/resourcebrowser/controller/strategy/SortStrategy;

    invoke-virtual {v3, v2}, Lmiui/resourcebrowser/controller/strategy/SortStrategy;->sort(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    .line 195
    iget-object v3, p0, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->searchCache:Lmiui/cache/DataCache;

    invoke-virtual {v3, p1, v2}, Lmiui/cache/DataCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    .end local v0           #i$:Ljava/util/Iterator;
    :cond_2
    return-object v2
.end method

.method protected getDataParser()Lmiui/resourcebrowser/controller/local/LocalDataParser;
    .locals 2

    .prologue
    .line 53
    iget-object v0, p0, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v0}, Lmiui/resourcebrowser/ResourceContext;->isSelfDescribing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    new-instance v0, Lmiui/resourcebrowser/controller/local/LocalBareDataParser;

    iget-object v1, p0, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-direct {v0, v1}, Lmiui/resourcebrowser/controller/local/LocalBareDataParser;-><init>(Lmiui/resourcebrowser/ResourceContext;)V

    .line 56
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lmiui/resourcebrowser/controller/local/LocalJSONDataParser;

    iget-object v1, p0, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-direct {v0, v1}, Lmiui/resourcebrowser/controller/local/LocalJSONDataParser;-><init>(Lmiui/resourcebrowser/ResourceContext;)V

    goto :goto_0
.end method

.method public getResourceByOnlineId(Ljava/lang/String;)Lmiui/resourcebrowser/model/Resource;
    .locals 2
    .parameter "id"

    .prologue
    .line 212
    iget-object v1, p0, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->onlineIdIndex:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/resourcebrowser/model/Resource;

    .line 217
    .local v0, resource:Lmiui/resourcebrowser/model/Resource;
    return-object v0
.end method

.method public getResources()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 61
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->getResources(Z)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getResources(Z)Ljava/util/List;
    .locals 1
    .parameter "forceRefresh"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 66
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->needRefresh()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 67
    :cond_0
    invoke-virtual {p0}, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->refreshResources()V

    .line 68
    invoke-virtual {p0}, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->notifyDataSetUpdateSuccessful()V

    .line 70
    :cond_1
    iget-object v0, p0, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->dataSet:Ljava/util/List;

    return-object v0
.end method

.method protected needRefresh()Z
    .locals 4

    .prologue
    .line 74
    const/4 v1, 0x0

    .line 75
    .local v1, needRefresh:Z
    iget-object v3, p0, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v3}, Lmiui/resourcebrowser/ResourceContext;->getSourceFolders()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 76
    .local v2, sourceFolder:Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->dataFolderCache:Lmiui/cache/FolderCache;

    invoke-virtual {v3, v2}, Lmiui/cache/FolderCache;->isCacheDirty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 77
    iget-object v3, p0, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->dataFolderCache:Lmiui/cache/FolderCache;

    invoke-virtual {v3, v2}, Lmiui/cache/FolderCache;->get(Ljava/lang/String;)Lmiui/cache/FolderCache$FolderInfo;

    .line 78
    const/4 v1, 0x1

    goto :goto_0

    .line 81
    .end local v2           #sourceFolder:Ljava/lang/String;
    :cond_1
    return v1
.end method

.method protected refreshResources()V
    .locals 7

    .prologue
    .line 85
    iget-object v5, p0, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->dataSet:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->clear()V

    .line 86
    iget-object v5, p0, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v5}, Lmiui/resourcebrowser/ResourceContext;->getSourceFolders()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 87
    .local v4, sourceFolder:Ljava/lang/String;
    iget-object v5, p0, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->dataSet:Ljava/util/List;

    invoke-direct {p0, v4}, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->loadResources(Ljava/lang/String;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 89
    .end local v4           #sourceFolder:Ljava/lang/String;
    :cond_0
    iget-object v5, p0, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->localIdIndex:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->clear()V

    .line 90
    iget-object v5, p0, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->onlineIdIndex:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->clear()V

    .line 91
    iget-object v5, p0, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->searchCache:Lmiui/cache/DataCache;

    invoke-virtual {v5}, Lmiui/cache/DataCache;->clear()V

    .line 92
    iget-object v5, p0, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->dataSet:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmiui/resourcebrowser/model/Resource;

    .line 93
    .local v3, resource:Lmiui/resourcebrowser/model/Resource;
    invoke-virtual {v3}, Lmiui/resourcebrowser/model/Resource;->getLocalId()Ljava/lang/String;

    move-result-object v1

    .line 94
    .local v1, localId:Ljava/lang/String;
    invoke-virtual {v3}, Lmiui/resourcebrowser/model/Resource;->getOnlineId()Ljava/lang/String;

    move-result-object v2

    .line 95
    .local v2, onlineId:Ljava/lang/String;
    if-eqz v1, :cond_2

    .line 96
    iget-object v5, p0, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->localIdIndex:Ljava/util/Map;

    invoke-interface {v5, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    :cond_2
    if-eqz v2, :cond_1

    .line 99
    iget-object v5, p0, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->onlineIdIndex:Ljava/util/Map;

    invoke-interface {v5, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 102
    .end local v1           #localId:Ljava/lang/String;
    .end local v2           #onlineId:Ljava/lang/String;
    .end local v3           #resource:Lmiui/resourcebrowser/model/Resource;
    :cond_3
    return-void
.end method

.method public removeResource(Lmiui/resourcebrowser/model/Resource;)Z
    .locals 9
    .parameter "resource"

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 252
    const/4 v0, 0x1

    .line 253
    .local v0, deleted:Z
    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v3

    .line 254
    .local v3, localPath:Ljava/lang/String;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 255
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    move-result v7

    if-eqz v7, :cond_2

    if-eqz v0, :cond_2

    move v0, v5

    .line 257
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getFilePath()Ljava/lang/String;

    move-result-object v1

    .line 258
    .local v1, filePath:Ljava/lang/String;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 259
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    move-result v7

    if-eqz v7, :cond_3

    if-eqz v0, :cond_3

    move v0, v5

    .line 261
    :cond_1
    :goto_1
    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getBuildInThumbnails()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, i$:Ljava/util/Iterator;
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 262
    .local v4, path:Ljava/lang/String;
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    move-result v7

    if-eqz v7, :cond_4

    if-eqz v0, :cond_4

    move v0, v5

    :goto_3
    goto :goto_2

    .end local v1           #filePath:Ljava/lang/String;
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v4           #path:Ljava/lang/String;
    :cond_2
    move v0, v6

    .line 255
    goto :goto_0

    .restart local v1       #filePath:Ljava/lang/String;
    :cond_3
    move v0, v6

    .line 259
    goto :goto_1

    .restart local v2       #i$:Ljava/util/Iterator;
    .restart local v4       #path:Ljava/lang/String;
    :cond_4
    move v0, v6

    .line 262
    goto :goto_3

    .line 264
    .end local v4           #path:Ljava/lang/String;
    :cond_5
    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getBuildInPreviews()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 265
    .restart local v4       #path:Ljava/lang/String;
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    move-result v7

    if-eqz v7, :cond_6

    if-eqz v0, :cond_6

    move v0, v5

    :goto_5
    goto :goto_4

    :cond_6
    move v0, v6

    goto :goto_5

    .line 267
    .end local v4           #path:Ljava/lang/String;
    :cond_7
    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getSubResources()Ljava/util/List;

    move-result-object v5

    invoke-direct {p0, v5}, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->removeRelatedResources(Ljava/util/List;)Z

    move-result v0

    .line 268
    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getParentResources()Ljava/util/List;

    move-result-object v5

    invoke-direct {p0, v5}, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->removeRelatedResources(Ljava/util/List;)Z

    move-result v0

    .line 269
    if-eqz v0, :cond_8

    .line 270
    invoke-virtual {p0}, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->notifyDataSetUpdateSuccessful()V

    .line 274
    :goto_6
    return v0

    .line 272
    :cond_8
    invoke-virtual {p0}, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->notifyDataSetUpdateFailed()V

    goto :goto_6
.end method

.method public setResourceController(Lmiui/resourcebrowser/controller/ResourceController;)V
    .locals 1
    .parameter "controller"

    .prologue
    .line 48
    invoke-super {p0, p1}, Lmiui/resourcebrowser/controller/LocalDataManager;->setResourceController(Lmiui/resourcebrowser/controller/ResourceController;)V

    .line 49
    iget-object v0, p0, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->parser:Lmiui/resourcebrowser/controller/local/LocalDataParser;

    invoke-virtual {v0, p1}, Lmiui/resourcebrowser/controller/local/LocalDataParser;->setResourceController(Lmiui/resourcebrowser/controller/ResourceController;)V

    .line 50
    return-void
.end method

.method public updateResource(Lmiui/resourcebrowser/model/Resource;)Z
    .locals 4
    .parameter "resource"

    .prologue
    .line 237
    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v2

    .line 238
    .local v2, localPath:Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 240
    .local v1, file:Ljava/io/File;
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    .line 241
    invoke-direct {p0, v1, p1}, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->storeResource(Ljava/io/File;Lmiui/resourcebrowser/model/Resource;)V

    .line 242
    invoke-virtual {p0}, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->notifyDataSetUpdateSuccessful()V
    :try_end_0
    .catch Lmiui/resourcebrowser/controller/local/PersistenceException; {:try_start_0 .. :try_end_0} :catch_0

    .line 247
    const/4 v3, 0x1

    :goto_0
    return v3

    .line 243
    :catch_0
    move-exception v0

    .line 244
    .local v0, e:Lmiui/resourcebrowser/controller/local/PersistenceException;
    invoke-virtual {p0}, Lmiui/resourcebrowser/controller/local/FileSystemDataManager;->notifyDataSetUpdateFailed()V

    .line 245
    const/4 v3, 0x0

    goto :goto_0
.end method
