.class public Lmiui/resourcebrowser/controller/strategy/IncrementalIdGenerationStrategy;
.super Lmiui/resourcebrowser/controller/strategy/IdGenerationStrategy;
.source "IncrementalIdGenerationStrategy.java"

# interfaces
.implements Lmiui/resourcebrowser/ResourceConstants;


# static fields
.field private static final PATH:Ljava/lang/String;

.field private static id:J


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const-wide/16 v8, -0x1

    const-wide/16 v6, 0x3e8

    .line 15
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lmiui/resourcebrowser/controller/strategy/IncrementalIdGenerationStrategy;->CONFIG_PATH:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "id"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lmiui/resourcebrowser/controller/strategy/IncrementalIdGenerationStrategy;->PATH:Ljava/lang/String;

    .line 19
    sput-wide v8, Lmiui/resourcebrowser/controller/strategy/IncrementalIdGenerationStrategy;->id:J

    .line 22
    const/4 v2, 0x0

    .line 23
    .local v2, reader:Ljava/io/BufferedReader;
    new-instance v1, Ljava/io/File;

    sget-object v4, Lmiui/resourcebrowser/controller/strategy/IncrementalIdGenerationStrategy;->PATH:Ljava/lang/String;

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 24
    .local v1, file:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 26
    :try_start_0
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/FileReader;

    sget-object v5, Lmiui/resourcebrowser/controller/strategy/IncrementalIdGenerationStrategy;->PATH:Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_2

    .line 27
    .end local v2           #reader:Ljava/io/BufferedReader;
    .local v3, reader:Ljava/io/BufferedReader;
    :try_start_1
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    sput-wide v4, Lmiui/resourcebrowser/controller/strategy/IncrementalIdGenerationStrategy;->id:J

    .line 28
    sget-wide v4, Lmiui/resourcebrowser/controller/strategy/IncrementalIdGenerationStrategy;->id:J

    cmp-long v4, v4, v6

    if-gez v4, :cond_2

    move-wide v4, v6

    :goto_0
    sput-wide v4, Lmiui/resourcebrowser/controller/strategy/IncrementalIdGenerationStrategy;->id:J
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_4

    move-object v2, v3

    .line 37
    .end local v3           #reader:Ljava/io/BufferedReader;
    .restart local v2       #reader:Ljava/io/BufferedReader;
    :cond_0
    :goto_1
    sget-wide v4, Lmiui/resourcebrowser/controller/strategy/IncrementalIdGenerationStrategy;->id:J

    cmp-long v4, v4, v8

    if-nez v4, :cond_1

    .line 39
    :try_start_2
    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z

    .line 40
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z

    .line 41
    const-wide/16 v4, 0x3e8

    sput-wide v4, Lmiui/resourcebrowser/controller/strategy/IncrementalIdGenerationStrategy;->id:J
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 46
    :cond_1
    return-void

    .line 28
    .end local v2           #reader:Ljava/io/BufferedReader;
    .restart local v3       #reader:Ljava/io/BufferedReader;
    :cond_2
    :try_start_3
    sget-wide v4, Lmiui/resourcebrowser/controller/strategy/IncrementalIdGenerationStrategy;->id:J
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_4

    goto :goto_0

    .line 29
    .end local v3           #reader:Ljava/io/BufferedReader;
    .restart local v2       #reader:Ljava/io/BufferedReader;
    :catch_0
    move-exception v0

    .line 30
    .local v0, e:Ljava/io/FileNotFoundException;
    :goto_2
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_1

    .line 31
    .end local v0           #e:Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v0

    .line 32
    .local v0, e:Ljava/io/IOException;
    :goto_3
    sput-wide v6, Lmiui/resourcebrowser/controller/strategy/IncrementalIdGenerationStrategy;->id:J

    goto :goto_1

    .line 33
    .end local v0           #e:Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 34
    .local v0, e:Ljava/lang/NumberFormatException;
    :goto_4
    sput-wide v6, Lmiui/resourcebrowser/controller/strategy/IncrementalIdGenerationStrategy;->id:J

    goto :goto_1

    .line 42
    .end local v0           #e:Ljava/lang/NumberFormatException;
    :catch_3
    move-exception v0

    .line 43
    .local v0, e:Ljava/io/IOException;
    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "id generation initialize failed"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 33
    .end local v0           #e:Ljava/io/IOException;
    .end local v2           #reader:Ljava/io/BufferedReader;
    .restart local v3       #reader:Ljava/io/BufferedReader;
    :catch_4
    move-exception v0

    move-object v2, v3

    .end local v3           #reader:Ljava/io/BufferedReader;
    .restart local v2       #reader:Ljava/io/BufferedReader;
    goto :goto_4

    .line 31
    .end local v2           #reader:Ljava/io/BufferedReader;
    .restart local v3       #reader:Ljava/io/BufferedReader;
    :catch_5
    move-exception v0

    move-object v2, v3

    .end local v3           #reader:Ljava/io/BufferedReader;
    .restart local v2       #reader:Ljava/io/BufferedReader;
    goto :goto_3

    .line 29
    .end local v2           #reader:Ljava/io/BufferedReader;
    .restart local v3       #reader:Ljava/io/BufferedReader;
    :catch_6
    move-exception v0

    move-object v2, v3

    .end local v3           #reader:Ljava/io/BufferedReader;
    .restart local v2       #reader:Ljava/io/BufferedReader;
    goto :goto_2
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lmiui/resourcebrowser/controller/strategy/IdGenerationStrategy;-><init>()V

    return-void
.end method


# virtual methods
.method public declared-synchronized nextId()Ljava/lang/String;
    .locals 8

    .prologue
    .line 50
    monitor-enter p0

    :try_start_0
    sget-wide v4, Lmiui/resourcebrowser/controller/strategy/IncrementalIdGenerationStrategy;->id:J

    const-wide/16 v6, 0x1

    add-long/2addr v6, v4

    sput-wide v6, Lmiui/resourcebrowser/controller/strategy/IncrementalIdGenerationStrategy;->id:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 51
    .local v1, next:Ljava/lang/String;
    const/4 v2, 0x0

    .line 53
    .local v2, writer:Ljava/io/BufferedWriter;
    :try_start_1
    new-instance v3, Ljava/io/BufferedWriter;

    new-instance v4, Ljava/io/FileWriter;

    sget-object v5, Lmiui/resourcebrowser/controller/strategy/IncrementalIdGenerationStrategy;->PATH:Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 54
    .end local v2           #writer:Ljava/io/BufferedWriter;
    .local v3, writer:Ljava/io/BufferedWriter;
    :try_start_2
    invoke-virtual {v3, v1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4

    .line 58
    if-eqz v3, :cond_2

    .line 60
    :try_start_3
    invoke-virtual {v3}, Ljava/io/BufferedWriter;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    move-object v2, v3

    .line 66
    .end local v3           #writer:Ljava/io/BufferedWriter;
    .restart local v2       #writer:Ljava/io/BufferedWriter;
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v1

    .line 61
    .end local v2           #writer:Ljava/io/BufferedWriter;
    .restart local v3       #writer:Ljava/io/BufferedWriter;
    :catch_0
    move-exception v0

    .line 62
    .local v0, e:Ljava/io/IOException;
    :try_start_4
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-object v2, v3

    .line 63
    .end local v3           #writer:Ljava/io/BufferedWriter;
    .restart local v2       #writer:Ljava/io/BufferedWriter;
    goto :goto_0

    .line 55
    .end local v0           #e:Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 56
    .restart local v0       #e:Ljava/io/IOException;
    :goto_1
    :try_start_5
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 58
    if-eqz v2, :cond_0

    .line 60
    :try_start_6
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->close()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    goto :goto_0

    .line 61
    :catch_2
    move-exception v0

    .line 62
    :try_start_7
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_0

    .line 50
    .end local v0           #e:Ljava/io/IOException;
    .end local v1           #next:Ljava/lang/String;
    .end local v2           #writer:Ljava/io/BufferedWriter;
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 58
    .restart local v1       #next:Ljava/lang/String;
    .restart local v2       #writer:Ljava/io/BufferedWriter;
    :catchall_1
    move-exception v4

    :goto_2
    if-eqz v2, :cond_1

    .line 60
    :try_start_8
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->close()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    .line 63
    :cond_1
    :goto_3
    :try_start_9
    throw v4

    .line 61
    :catch_3
    move-exception v0

    .line 62
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_3

    .line 58
    .end local v0           #e:Ljava/io/IOException;
    .end local v2           #writer:Ljava/io/BufferedWriter;
    .restart local v3       #writer:Ljava/io/BufferedWriter;
    :catchall_2
    move-exception v4

    move-object v2, v3

    .end local v3           #writer:Ljava/io/BufferedWriter;
    .restart local v2       #writer:Ljava/io/BufferedWriter;
    goto :goto_2

    .line 55
    .end local v2           #writer:Ljava/io/BufferedWriter;
    .restart local v3       #writer:Ljava/io/BufferedWriter;
    :catch_4
    move-exception v0

    move-object v2, v3

    .end local v3           #writer:Ljava/io/BufferedWriter;
    .restart local v2       #writer:Ljava/io/BufferedWriter;
    goto :goto_1

    .end local v2           #writer:Ljava/io/BufferedWriter;
    .restart local v3       #writer:Ljava/io/BufferedWriter;
    :cond_2
    move-object v2, v3

    .end local v3           #writer:Ljava/io/BufferedWriter;
    .restart local v2       #writer:Ljava/io/BufferedWriter;
    goto :goto_0
.end method
