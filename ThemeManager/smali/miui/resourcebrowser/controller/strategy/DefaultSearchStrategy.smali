.class public Lmiui/resourcebrowser/controller/strategy/DefaultSearchStrategy;
.super Lmiui/resourcebrowser/controller/strategy/SearchStrategy;
.source "DefaultSearchStrategy.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Lmiui/resourcebrowser/controller/strategy/SearchStrategy;-><init>()V

    return-void
.end method


# virtual methods
.method public isHitted(Ljava/lang/String;Lmiui/resourcebrowser/model/Resource;)Z
    .locals 5
    .parameter "keyword"
    .parameter "resource"

    .prologue
    const/4 v2, 0x1

    .line 9
    if-eqz p2, :cond_2

    .line 10
    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getTitle()Ljava/lang/String;

    move-result-object v1

    .line 11
    .local v1, title:Ljava/lang/String;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 19
    .end local v1           #title:Ljava/lang/String;
    :cond_0
    :goto_0
    return v2

    .line 14
    .restart local v1       #title:Ljava/lang/String;
    :cond_1
    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getDescription()Ljava/lang/String;

    move-result-object v0

    .line 15
    .local v0, description:Ljava/lang/String;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 19
    .end local v0           #description:Ljava/lang/String;
    .end local v1           #title:Ljava/lang/String;
    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method
