.class Lmiui/resourcebrowser/controller/strategy/ModifiedTimeSortStrategy$1;
.super Ljava/lang/Object;
.source "ModifiedTimeSortStrategy.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiui/resourcebrowser/controller/strategy/ModifiedTimeSortStrategy;->sort(Ljava/util/List;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lmiui/resourcebrowser/model/Resource;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/resourcebrowser/controller/strategy/ModifiedTimeSortStrategy;


# direct methods
.method constructor <init>(Lmiui/resourcebrowser/controller/strategy/ModifiedTimeSortStrategy;)V
    .locals 0
    .parameter

    .prologue
    .line 14
    iput-object p1, p0, Lmiui/resourcebrowser/controller/strategy/ModifiedTimeSortStrategy$1;->this$0:Lmiui/resourcebrowser/controller/strategy/ModifiedTimeSortStrategy;

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 14
    check-cast p1, Lmiui/resourcebrowser/model/Resource;

    .end local p1
    check-cast p2, Lmiui/resourcebrowser/model/Resource;

    .end local p2
    invoke-virtual {p0, p1, p2}, Lmiui/resourcebrowser/controller/strategy/ModifiedTimeSortStrategy$1;->compare(Lmiui/resourcebrowser/model/Resource;Lmiui/resourcebrowser/model/Resource;)I

    move-result v0

    return v0
.end method

.method public compare(Lmiui/resourcebrowser/model/Resource;Lmiui/resourcebrowser/model/Resource;)I
    .locals 4
    .parameter "lhs"
    .parameter "rhs"

    .prologue
    .line 17
    new-instance v0, Ljava/io/File;

    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getFilePath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    new-instance v2, Ljava/io/File;

    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getFilePath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    sub-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->signum(J)I

    move-result v0

    return v0
.end method
