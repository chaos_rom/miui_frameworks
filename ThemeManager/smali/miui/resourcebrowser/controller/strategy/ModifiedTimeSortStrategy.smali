.class public Lmiui/resourcebrowser/controller/strategy/ModifiedTimeSortStrategy;
.super Lmiui/resourcebrowser/controller/strategy/SortStrategy;
.source "ModifiedTimeSortStrategy.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lmiui/resourcebrowser/controller/strategy/SortStrategy;-><init>()V

    return-void
.end method


# virtual methods
.method public sort(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 14
    .local p1, resources:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/Resource;>;"
    new-instance v0, Lmiui/resourcebrowser/controller/strategy/ModifiedTimeSortStrategy$1;

    invoke-direct {v0, p0}, Lmiui/resourcebrowser/controller/strategy/ModifiedTimeSortStrategy$1;-><init>(Lmiui/resourcebrowser/controller/strategy/ModifiedTimeSortStrategy;)V

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 21
    return-object p1
.end method
