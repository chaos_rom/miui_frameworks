.class public abstract Lmiui/resourcebrowser/controller/LocalDataManager;
.super Lmiui/resourcebrowser/controller/DataManager;
.source "LocalDataManager.java"


# direct methods
.method public constructor <init>(Lmiui/resourcebrowser/ResourceContext;)V
    .locals 0
    .parameter "context"

    .prologue
    .line 11
    invoke-direct {p0, p1}, Lmiui/resourcebrowser/controller/DataManager;-><init>(Lmiui/resourcebrowser/ResourceContext;)V

    .line 12
    return-void
.end method


# virtual methods
.method public abstract addResource(Lmiui/resourcebrowser/model/Resource;)Z
.end method

.method public abstract findResources(Ljava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getResourceByOnlineId(Ljava/lang/String;)Lmiui/resourcebrowser/model/Resource;
.end method

.method public abstract getResources()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lmiui/resourcebrowser/model/Resource;",
            ">;"
        }
    .end annotation
.end method

.method public abstract removeResource(Lmiui/resourcebrowser/model/Resource;)Z
.end method

.method public abstract updateResource(Lmiui/resourcebrowser/model/Resource;)Z
.end method
