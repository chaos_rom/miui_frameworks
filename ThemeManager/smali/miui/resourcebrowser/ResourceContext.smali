.class public Lmiui/resourcebrowser/ResourceContext;
.super Ljava/lang/Object;
.source "ResourceContext.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final DISPLAY_TYPE_DOUBLE_FLAT:I = 0x6

.field public static final DISPLAY_TYPE_DOUBLE_FLAT_FONT:I = 0x7

.field public static final DISPLAY_TYPE_DOUBLE_FLAT_ICON:I = 0x8

.field public static final DISPLAY_TYPE_SINGLE:I = 0x1

.field public static final DISPLAY_TYPE_SINGLE_DETAIL:I = 0x3

.field public static final DISPLAY_TYPE_SINGLE_GALLERY:I = 0x5

.field public static final DISPLAY_TYPE_SINGLE_MUSIC:I = 0x4

.field public static final DISPLAY_TYPE_SINGLE_SMALL:I = 0x2

.field public static final DISPLAY_TYPE_TRIPLE:I = 0x9

.field public static final DISPLAY_TYPE_TRIPLE_FLAT:I = 0xa

.field public static final DISPLAY_TYPE_TRIPLE_TEXT:I = 0xb

.field public static final RESOURCE_FORMAT_AUDIO:I = 0x3

.field public static final RESOURCE_FORMAT_BUNDLE:I = 0x1

.field public static final RESOURCE_FORMAT_IMAGE:I = 0x2

.field public static final RESOURCE_FORMAT_OTHER:I = 0x5

.field public static final RESOURCE_FORMAT_ZIP:I = 0x4

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private associationCacheFolder:Ljava/lang/String;

.field private asyncImportFolder:Ljava/lang/String;

.field private baseDataCacheFolder:Ljava/lang/String;

.field private baseDataFolder:Ljava/lang/String;

.field private baseImageCacheFolder:Ljava/lang/String;

.field private buildInImageFolder:Ljava/lang/String;

.field private buildInImagePrefixes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private categoryCacheFolder:Ljava/lang/String;

.field private categorySupported:Z

.field private contentFolder:Ljava/lang/String;

.field private currentPlatform:I

.field private currentUsingPath:Ljava/lang/String;

.field private detailActivityClass:Ljava/lang/String;

.field private detailActivityPackage:Ljava/lang/String;

.field private detailCacheFolder:Ljava/lang/String;

.field private displayType:I

.field private downloadFolder:Ljava/lang/String;

.field private extraMeta:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/io/Serializable;",
            ">;"
        }
    .end annotation
.end field

.field private indexFolder:Ljava/lang/String;

.field private listCacheFolder:Ljava/lang/String;

.field private listUrl:Lmiui/resourcebrowser/controller/online/RequestUrl;

.field private metaFolder:Ljava/lang/String;

.field private pageItemCount:I

.field private picker:Z

.field private platformSupported:Z

.field private previewCacheFolder:Ljava/lang/String;

.field private previewImageWidth:I

.field private recommendActivityClass:Ljava/lang/String;

.field private recommendActivityPackage:Ljava/lang/String;

.field private recommendCacheFolder:Ljava/lang/String;

.field private recommendImageCacheFolder:Ljava/lang/String;

.field private recommendImageWidth:I

.field private recommendSupported:Z

.field private resourceCode:Ljava/lang/String;

.field private resourceExtension:Ljava/lang/String;

.field private resourceFormat:I

.field private resourceStamp:Ljava/lang/String;

.field private resourceTitle:Ljava/lang/String;

.field private rightsFolder:Ljava/lang/String;

.field private searchActivityClass:Ljava/lang/String;

.field private searchActivityPackage:Ljava/lang/String;

.field private selfDescribing:Z

.field private sourceFolders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private tabActivityClass:Ljava/lang/String;

.field private tabActivityPackage:Ljava/lang/String;

.field private thumbnailCacheFolder:Ljava/lang/String;

.field private thumbnailImageWidth:I

.field private trackId:Ljava/lang/String;

.field private versionCacheFolder:Ljava/lang/String;

.field private versionSupported:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/ResourceContext;->sourceFolders:Ljava/util/List;

    .line 88
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/ResourceContext;->buildInImagePrefixes:Ljava/util/List;

    .line 93
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lmiui/resourcebrowser/ResourceContext;->extraMeta:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public addBuildInImagePrefix(Ljava/lang/String;)V
    .locals 1
    .parameter "buildInImagePrefix"

    .prologue
    .line 468
    iget-object v0, p0, Lmiui/resourcebrowser/ResourceContext;->buildInImagePrefixes:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 469
    return-void
.end method

.method public addSourceFolder(Ljava/lang/String;)V
    .locals 1
    .parameter "sourceFolder"

    .prologue
    .line 224
    iget-object v0, p0, Lmiui/resourcebrowser/ResourceContext;->sourceFolders:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 225
    return-void
.end method

.method public getAssociationCacheFolder()Ljava/lang/String;
    .locals 1

    .prologue
    .line 316
    iget-object v0, p0, Lmiui/resourcebrowser/ResourceContext;->associationCacheFolder:Ljava/lang/String;

    return-object v0
.end method

.method public getAsyncImportFolder()Ljava/lang/String;
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lmiui/resourcebrowser/ResourceContext;->asyncImportFolder:Ljava/lang/String;

    return-object v0
.end method

.method public getBaseDataCacheFolder()Ljava/lang/String;
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lmiui/resourcebrowser/ResourceContext;->baseDataCacheFolder:Ljava/lang/String;

    return-object v0
.end method

.method public getBaseDataFolder()Ljava/lang/String;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lmiui/resourcebrowser/ResourceContext;->baseDataFolder:Ljava/lang/String;

    return-object v0
.end method

.method public getBaseImageCacheFolder()Ljava/lang/String;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lmiui/resourcebrowser/ResourceContext;->baseImageCacheFolder:Ljava/lang/String;

    return-object v0
.end method

.method public getBuildInImageFolder()Ljava/lang/String;
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lmiui/resourcebrowser/ResourceContext;->buildInImageFolder:Ljava/lang/String;

    return-object v0
.end method

.method public getBuildInImagePrefixes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 460
    iget-object v0, p0, Lmiui/resourcebrowser/ResourceContext;->buildInImagePrefixes:Ljava/util/List;

    return-object v0
.end method

.method public getCategoryCacheFolder()Ljava/lang/String;
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lmiui/resourcebrowser/ResourceContext;->categoryCacheFolder:Ljava/lang/String;

    return-object v0
.end method

.method public getContentFolder()Ljava/lang/String;
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lmiui/resourcebrowser/ResourceContext;->contentFolder:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrentPlatform()I
    .locals 1

    .prologue
    .line 472
    iget v0, p0, Lmiui/resourcebrowser/ResourceContext;->currentPlatform:I

    return v0
.end method

.method public getCurrentUsingPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 480
    iget-object v0, p0, Lmiui/resourcebrowser/ResourceContext;->currentUsingPath:Ljava/lang/String;

    return-object v0
.end method

.method public getDetailActivityClass()Ljava/lang/String;
    .locals 1

    .prologue
    .line 404
    iget-object v0, p0, Lmiui/resourcebrowser/ResourceContext;->detailActivityClass:Ljava/lang/String;

    return-object v0
.end method

.method public getDetailActivityPackage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 396
    iget-object v0, p0, Lmiui/resourcebrowser/ResourceContext;->detailActivityPackage:Ljava/lang/String;

    return-object v0
.end method

.method public getDetailCacheFolder()Ljava/lang/String;
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lmiui/resourcebrowser/ResourceContext;->detailCacheFolder:Ljava/lang/String;

    return-object v0
.end method

.method public getDisplayType()I
    .locals 1

    .prologue
    .line 144
    iget v0, p0, Lmiui/resourcebrowser/ResourceContext;->displayType:I

    return v0
.end method

.method public getDownloadFolder()Ljava/lang/String;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lmiui/resourcebrowser/ResourceContext;->downloadFolder:Ljava/lang/String;

    return-object v0
.end method

.method public getExtraMeta(Ljava/lang/String;)Ljava/io/Serializable;
    .locals 1
    .parameter "key"

    .prologue
    .line 504
    iget-object v0, p0, Lmiui/resourcebrowser/ResourceContext;->extraMeta:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/Serializable;

    return-object v0
.end method

.method public getExtraMeta(Ljava/lang/String;Ljava/io/Serializable;)Ljava/io/Serializable;
    .locals 2
    .parameter "key"
    .parameter "defaultValue"

    .prologue
    .line 508
    iget-object v1, p0, Lmiui/resourcebrowser/ResourceContext;->extraMeta:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/Serializable;

    .line 509
    .local v0, result:Ljava/io/Serializable;
    if-eqz v0, :cond_0

    .end local v0           #result:Ljava/io/Serializable;
    :goto_0
    return-object v0

    .restart local v0       #result:Ljava/io/Serializable;
    :cond_0
    move-object v0, p2

    goto :goto_0
.end method

.method public getExtraMeta()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/io/Serializable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 496
    iget-object v0, p0, Lmiui/resourcebrowser/ResourceContext;->extraMeta:Ljava/util/Map;

    return-object v0
.end method

.method public getIndexFolder()Ljava/lang/String;
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lmiui/resourcebrowser/ResourceContext;->indexFolder:Ljava/lang/String;

    return-object v0
.end method

.method public getListCacheFolder()Ljava/lang/String;
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lmiui/resourcebrowser/ResourceContext;->listCacheFolder:Ljava/lang/String;

    return-object v0
.end method

.method public getListUrl()Lmiui/resourcebrowser/controller/online/RequestUrl;
    .locals 1

    .prologue
    .line 488
    iget-object v0, p0, Lmiui/resourcebrowser/ResourceContext;->listUrl:Lmiui/resourcebrowser/controller/online/RequestUrl;

    return-object v0
.end method

.method public getMetaFolder()Ljava/lang/String;
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lmiui/resourcebrowser/ResourceContext;->metaFolder:Ljava/lang/String;

    return-object v0
.end method

.method public getPageItemCount()I
    .locals 1

    .prologue
    .line 428
    iget v0, p0, Lmiui/resourcebrowser/ResourceContext;->pageItemCount:I

    return v0
.end method

.method public getPreviewCacheFolder()Ljava/lang/String;
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Lmiui/resourcebrowser/ResourceContext;->previewCacheFolder:Ljava/lang/String;

    return-object v0
.end method

.method public getPreviewImageWidth()I
    .locals 1

    .prologue
    .line 452
    iget v0, p0, Lmiui/resourcebrowser/ResourceContext;->previewImageWidth:I

    return v0
.end method

.method public getRecommendActivityClass()Ljava/lang/String;
    .locals 1

    .prologue
    .line 388
    iget-object v0, p0, Lmiui/resourcebrowser/ResourceContext;->recommendActivityClass:Ljava/lang/String;

    return-object v0
.end method

.method public getRecommendActivityPackage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 380
    iget-object v0, p0, Lmiui/resourcebrowser/ResourceContext;->recommendActivityPackage:Ljava/lang/String;

    return-object v0
.end method

.method public getRecommendCacheFolder()Ljava/lang/String;
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Lmiui/resourcebrowser/ResourceContext;->recommendCacheFolder:Ljava/lang/String;

    return-object v0
.end method

.method public getRecommendImageCacheFolder()Ljava/lang/String;
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Lmiui/resourcebrowser/ResourceContext;->recommendImageCacheFolder:Ljava/lang/String;

    return-object v0
.end method

.method public getRecommendImageWidth()I
    .locals 1

    .prologue
    .line 436
    iget v0, p0, Lmiui/resourcebrowser/ResourceContext;->recommendImageWidth:I

    return v0
.end method

.method public getResourceCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lmiui/resourcebrowser/ResourceContext;->resourceCode:Ljava/lang/String;

    return-object v0
.end method

.method public getResourceExtension()Ljava/lang/String;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lmiui/resourcebrowser/ResourceContext;->resourceExtension:Ljava/lang/String;

    return-object v0
.end method

.method public getResourceFormat()I
    .locals 1

    .prologue
    .line 120
    iget v0, p0, Lmiui/resourcebrowser/ResourceContext;->resourceFormat:I

    return v0
.end method

.method public getResourceStamp()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lmiui/resourcebrowser/ResourceContext;->resourceStamp:Ljava/lang/String;

    return-object v0
.end method

.method public getResourceTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lmiui/resourcebrowser/ResourceContext;->resourceTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getRightsFolder()Ljava/lang/String;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lmiui/resourcebrowser/ResourceContext;->rightsFolder:Ljava/lang/String;

    return-object v0
.end method

.method public getSearchActivityClass()Ljava/lang/String;
    .locals 1

    .prologue
    .line 372
    iget-object v0, p0, Lmiui/resourcebrowser/ResourceContext;->searchActivityClass:Ljava/lang/String;

    return-object v0
.end method

.method public getSearchActivityPackage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 364
    iget-object v0, p0, Lmiui/resourcebrowser/ResourceContext;->searchActivityPackage:Ljava/lang/String;

    return-object v0
.end method

.method public getSourceFolders()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 216
    iget-object v0, p0, Lmiui/resourcebrowser/ResourceContext;->sourceFolders:Ljava/util/List;

    return-object v0
.end method

.method public getTabActivityClass()Ljava/lang/String;
    .locals 1

    .prologue
    .line 356
    iget-object v0, p0, Lmiui/resourcebrowser/ResourceContext;->tabActivityClass:Ljava/lang/String;

    return-object v0
.end method

.method public getTabActivityPackage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 348
    iget-object v0, p0, Lmiui/resourcebrowser/ResourceContext;->tabActivityPackage:Ljava/lang/String;

    return-object v0
.end method

.method public getThumbnailCacheFolder()Ljava/lang/String;
    .locals 1

    .prologue
    .line 324
    iget-object v0, p0, Lmiui/resourcebrowser/ResourceContext;->thumbnailCacheFolder:Ljava/lang/String;

    return-object v0
.end method

.method public getThumbnailImageWidth()I
    .locals 1

    .prologue
    .line 444
    iget v0, p0, Lmiui/resourcebrowser/ResourceContext;->thumbnailImageWidth:I

    return v0
.end method

.method public getTrackId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 420
    iget-object v0, p0, Lmiui/resourcebrowser/ResourceContext;->trackId:Ljava/lang/String;

    return-object v0
.end method

.method public getVersionCacheFolder()Ljava/lang/String;
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, Lmiui/resourcebrowser/ResourceContext;->versionCacheFolder:Ljava/lang/String;

    return-object v0
.end method

.method public isCategorySupported()Z
    .locals 1

    .prologue
    .line 160
    iget-boolean v0, p0, Lmiui/resourcebrowser/ResourceContext;->categorySupported:Z

    return v0
.end method

.method public isPicker()Z
    .locals 1

    .prologue
    .line 412
    iget-boolean v0, p0, Lmiui/resourcebrowser/ResourceContext;->picker:Z

    return v0
.end method

.method public isPlatformSupported()Z
    .locals 1

    .prologue
    .line 176
    iget-boolean v0, p0, Lmiui/resourcebrowser/ResourceContext;->platformSupported:Z

    return v0
.end method

.method public isRecommendSupported()Z
    .locals 1

    .prologue
    .line 152
    iget-boolean v0, p0, Lmiui/resourcebrowser/ResourceContext;->recommendSupported:Z

    return v0
.end method

.method public isSelfDescribing()Z
    .locals 1

    .prologue
    .line 136
    iget-boolean v0, p0, Lmiui/resourcebrowser/ResourceContext;->selfDescribing:Z

    return v0
.end method

.method public isVersionSupported()Z
    .locals 1

    .prologue
    .line 168
    iget-boolean v0, p0, Lmiui/resourcebrowser/ResourceContext;->versionSupported:Z

    return v0
.end method

.method public putExtraMeta(Ljava/lang/String;Ljava/io/Serializable;)V
    .locals 1
    .parameter "key"
    .parameter "value"

    .prologue
    .line 513
    iget-object v0, p0, Lmiui/resourcebrowser/ResourceContext;->extraMeta:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 514
    return-void
.end method

.method public setAssociationCacheFolder(Ljava/lang/String;)V
    .locals 0
    .parameter "associationCacheFolder"

    .prologue
    .line 320
    iput-object p1, p0, Lmiui/resourcebrowser/ResourceContext;->associationCacheFolder:Ljava/lang/String;

    .line 321
    return-void
.end method

.method public setAsyncImportFolder(Ljava/lang/String;)V
    .locals 0
    .parameter "asyncImportFolder"

    .prologue
    .line 272
    iput-object p1, p0, Lmiui/resourcebrowser/ResourceContext;->asyncImportFolder:Ljava/lang/String;

    .line 273
    return-void
.end method

.method public setBaseDataCacheFolder(Ljava/lang/String;)V
    .locals 0
    .parameter "baseDataCacheFolder"

    .prologue
    .line 204
    iput-object p1, p0, Lmiui/resourcebrowser/ResourceContext;->baseDataCacheFolder:Ljava/lang/String;

    .line 205
    return-void
.end method

.method public setBaseDataFolder(Ljava/lang/String;)V
    .locals 0
    .parameter "baseDataFolder"

    .prologue
    .line 196
    iput-object p1, p0, Lmiui/resourcebrowser/ResourceContext;->baseDataFolder:Ljava/lang/String;

    .line 197
    return-void
.end method

.method public setBaseImageCacheFolder(Ljava/lang/String;)V
    .locals 0
    .parameter "baseImageCacheFolder"

    .prologue
    .line 212
    iput-object p1, p0, Lmiui/resourcebrowser/ResourceContext;->baseImageCacheFolder:Ljava/lang/String;

    .line 213
    return-void
.end method

.method public setBuildInImageFolder(Ljava/lang/String;)V
    .locals 0
    .parameter "buildInImageFolder"

    .prologue
    .line 256
    iput-object p1, p0, Lmiui/resourcebrowser/ResourceContext;->buildInImageFolder:Ljava/lang/String;

    .line 257
    return-void
.end method

.method public setBuildInImagePrefixes(Ljava/util/List;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 464
    .local p1, buildInImagePrefixes:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lmiui/resourcebrowser/ResourceContext;->buildInImagePrefixes:Ljava/util/List;

    .line 465
    return-void
.end method

.method public setCategoryCacheFolder(Ljava/lang/String;)V
    .locals 0
    .parameter "categoryCacheFolder"

    .prologue
    .line 296
    iput-object p1, p0, Lmiui/resourcebrowser/ResourceContext;->categoryCacheFolder:Ljava/lang/String;

    .line 297
    return-void
.end method

.method public setCategorySupported(Z)V
    .locals 0
    .parameter "categorySupported"

    .prologue
    .line 164
    iput-boolean p1, p0, Lmiui/resourcebrowser/ResourceContext;->categorySupported:Z

    .line 165
    return-void
.end method

.method public setContentFolder(Ljava/lang/String;)V
    .locals 0
    .parameter "contentFolder"

    .prologue
    .line 240
    iput-object p1, p0, Lmiui/resourcebrowser/ResourceContext;->contentFolder:Ljava/lang/String;

    .line 241
    return-void
.end method

.method public setCurrentPlatform(I)V
    .locals 0
    .parameter "currentPlatform"

    .prologue
    .line 476
    iput p1, p0, Lmiui/resourcebrowser/ResourceContext;->currentPlatform:I

    .line 477
    return-void
.end method

.method public setCurrentUsingPath(Ljava/lang/String;)V
    .locals 0
    .parameter "currentUsingPath"

    .prologue
    .line 484
    iput-object p1, p0, Lmiui/resourcebrowser/ResourceContext;->currentUsingPath:Ljava/lang/String;

    .line 485
    return-void
.end method

.method public setDetailActivityClass(Ljava/lang/String;)V
    .locals 0
    .parameter "detailActivityClass"

    .prologue
    .line 408
    iput-object p1, p0, Lmiui/resourcebrowser/ResourceContext;->detailActivityClass:Ljava/lang/String;

    .line 409
    return-void
.end method

.method public setDetailActivityPackage(Ljava/lang/String;)V
    .locals 0
    .parameter "detailActivityPackage"

    .prologue
    .line 400
    iput-object p1, p0, Lmiui/resourcebrowser/ResourceContext;->detailActivityPackage:Ljava/lang/String;

    .line 401
    return-void
.end method

.method public setDetailCacheFolder(Ljava/lang/String;)V
    .locals 0
    .parameter "detailCacheFolder"

    .prologue
    .line 288
    iput-object p1, p0, Lmiui/resourcebrowser/ResourceContext;->detailCacheFolder:Ljava/lang/String;

    .line 289
    return-void
.end method

.method public setDisplayType(I)V
    .locals 0
    .parameter "displayType"

    .prologue
    .line 148
    iput p1, p0, Lmiui/resourcebrowser/ResourceContext;->displayType:I

    .line 149
    return-void
.end method

.method public setDownloadFolder(Ljava/lang/String;)V
    .locals 0
    .parameter "downloadFolder"

    .prologue
    .line 188
    iput-object p1, p0, Lmiui/resourcebrowser/ResourceContext;->downloadFolder:Ljava/lang/String;

    .line 189
    return-void
.end method

.method public setExtraMeta(Ljava/util/Map;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/io/Serializable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 500
    .local p1, extraMeta:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/io/Serializable;>;"
    iput-object p1, p0, Lmiui/resourcebrowser/ResourceContext;->extraMeta:Ljava/util/Map;

    .line 501
    return-void
.end method

.method public setIndexFolder(Ljava/lang/String;)V
    .locals 0
    .parameter "indexFolder"

    .prologue
    .line 264
    iput-object p1, p0, Lmiui/resourcebrowser/ResourceContext;->indexFolder:Ljava/lang/String;

    .line 265
    return-void
.end method

.method public setListCacheFolder(Ljava/lang/String;)V
    .locals 0
    .parameter "listCacheFolder"

    .prologue
    .line 280
    iput-object p1, p0, Lmiui/resourcebrowser/ResourceContext;->listCacheFolder:Ljava/lang/String;

    .line 281
    return-void
.end method

.method public setListUrl(Lmiui/resourcebrowser/controller/online/RequestUrl;)V
    .locals 0
    .parameter "listUrl"

    .prologue
    .line 492
    iput-object p1, p0, Lmiui/resourcebrowser/ResourceContext;->listUrl:Lmiui/resourcebrowser/controller/online/RequestUrl;

    .line 493
    return-void
.end method

.method public setMetaFolder(Ljava/lang/String;)V
    .locals 0
    .parameter "metaFolder"

    .prologue
    .line 232
    iput-object p1, p0, Lmiui/resourcebrowser/ResourceContext;->metaFolder:Ljava/lang/String;

    .line 233
    return-void
.end method

.method public setPageItemCount(I)V
    .locals 0
    .parameter "pageItemCount"

    .prologue
    .line 432
    iput p1, p0, Lmiui/resourcebrowser/ResourceContext;->pageItemCount:I

    .line 433
    return-void
.end method

.method public setPicker(Z)V
    .locals 0
    .parameter "picker"

    .prologue
    .line 416
    iput-boolean p1, p0, Lmiui/resourcebrowser/ResourceContext;->picker:Z

    .line 417
    return-void
.end method

.method public setPlatformSupported(Z)V
    .locals 0
    .parameter "platformSupported"

    .prologue
    .line 180
    iput-boolean p1, p0, Lmiui/resourcebrowser/ResourceContext;->platformSupported:Z

    .line 181
    return-void
.end method

.method public setPreviewCacheFolder(Ljava/lang/String;)V
    .locals 0
    .parameter "previewCacheFolder"

    .prologue
    .line 336
    iput-object p1, p0, Lmiui/resourcebrowser/ResourceContext;->previewCacheFolder:Ljava/lang/String;

    .line 337
    return-void
.end method

.method public setPreviewImageWidth(I)V
    .locals 0
    .parameter "previewImageWidth"

    .prologue
    .line 456
    iput p1, p0, Lmiui/resourcebrowser/ResourceContext;->previewImageWidth:I

    .line 457
    return-void
.end method

.method public setRecommendActivityClass(Ljava/lang/String;)V
    .locals 0
    .parameter "recommendActivityClass"

    .prologue
    .line 392
    iput-object p1, p0, Lmiui/resourcebrowser/ResourceContext;->recommendActivityClass:Ljava/lang/String;

    .line 393
    return-void
.end method

.method public setRecommendActivityPackage(Ljava/lang/String;)V
    .locals 0
    .parameter "recommendActivityPackage"

    .prologue
    .line 384
    iput-object p1, p0, Lmiui/resourcebrowser/ResourceContext;->recommendActivityPackage:Ljava/lang/String;

    .line 385
    return-void
.end method

.method public setRecommendCacheFolder(Ljava/lang/String;)V
    .locals 0
    .parameter "recommendCacheFolder"

    .prologue
    .line 304
    iput-object p1, p0, Lmiui/resourcebrowser/ResourceContext;->recommendCacheFolder:Ljava/lang/String;

    .line 305
    return-void
.end method

.method public setRecommendImageCacheFolder(Ljava/lang/String;)V
    .locals 0
    .parameter "recommendImageCacheFolder"

    .prologue
    .line 344
    iput-object p1, p0, Lmiui/resourcebrowser/ResourceContext;->recommendImageCacheFolder:Ljava/lang/String;

    .line 345
    return-void
.end method

.method public setRecommendImageWidth(I)V
    .locals 0
    .parameter "recommendImageWidth"

    .prologue
    .line 440
    iput p1, p0, Lmiui/resourcebrowser/ResourceContext;->recommendImageWidth:I

    .line 441
    return-void
.end method

.method public setRecommendSupported(Z)V
    .locals 0
    .parameter "recommendSupported"

    .prologue
    .line 156
    iput-boolean p1, p0, Lmiui/resourcebrowser/ResourceContext;->recommendSupported:Z

    .line 157
    return-void
.end method

.method public setResourceCode(Ljava/lang/String;)V
    .locals 0
    .parameter "resourceCode"

    .prologue
    .line 108
    iput-object p1, p0, Lmiui/resourcebrowser/ResourceContext;->resourceCode:Ljava/lang/String;

    .line 109
    return-void
.end method

.method public setResourceExtension(Ljava/lang/String;)V
    .locals 0
    .parameter "resourceExtension"

    .prologue
    .line 132
    iput-object p1, p0, Lmiui/resourcebrowser/ResourceContext;->resourceExtension:Ljava/lang/String;

    .line 133
    return-void
.end method

.method public setResourceFormat(I)V
    .locals 0
    .parameter "resourceFormat"

    .prologue
    .line 124
    iput p1, p0, Lmiui/resourcebrowser/ResourceContext;->resourceFormat:I

    .line 125
    return-void
.end method

.method public setResourceStamp(Ljava/lang/String;)V
    .locals 0
    .parameter "resourceStamp"

    .prologue
    .line 100
    iput-object p1, p0, Lmiui/resourcebrowser/ResourceContext;->resourceStamp:Ljava/lang/String;

    .line 101
    return-void
.end method

.method public setResourceTitle(Ljava/lang/String;)V
    .locals 0
    .parameter "resourceTitle"

    .prologue
    .line 116
    iput-object p1, p0, Lmiui/resourcebrowser/ResourceContext;->resourceTitle:Ljava/lang/String;

    .line 117
    return-void
.end method

.method public setRightsFolder(Ljava/lang/String;)V
    .locals 0
    .parameter "rightsFolder"

    .prologue
    .line 248
    iput-object p1, p0, Lmiui/resourcebrowser/ResourceContext;->rightsFolder:Ljava/lang/String;

    .line 249
    return-void
.end method

.method public setSearchActivityClass(Ljava/lang/String;)V
    .locals 0
    .parameter "searchActivityClass"

    .prologue
    .line 376
    iput-object p1, p0, Lmiui/resourcebrowser/ResourceContext;->searchActivityClass:Ljava/lang/String;

    .line 377
    return-void
.end method

.method public setSearchActivityPackage(Ljava/lang/String;)V
    .locals 0
    .parameter "searchActivityPackage"

    .prologue
    .line 368
    iput-object p1, p0, Lmiui/resourcebrowser/ResourceContext;->searchActivityPackage:Ljava/lang/String;

    .line 369
    return-void
.end method

.method public setSelfDescribing(Z)V
    .locals 0
    .parameter "selfDescribing"

    .prologue
    .line 140
    iput-boolean p1, p0, Lmiui/resourcebrowser/ResourceContext;->selfDescribing:Z

    .line 141
    return-void
.end method

.method public setSourceFolders(Ljava/util/List;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 220
    .local p1, sourceFolders:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lmiui/resourcebrowser/ResourceContext;->sourceFolders:Ljava/util/List;

    .line 221
    return-void
.end method

.method public setTabActivityClass(Ljava/lang/String;)V
    .locals 0
    .parameter "tabActivityClass"

    .prologue
    .line 360
    iput-object p1, p0, Lmiui/resourcebrowser/ResourceContext;->tabActivityClass:Ljava/lang/String;

    .line 361
    return-void
.end method

.method public setTabActivityPackage(Ljava/lang/String;)V
    .locals 0
    .parameter "tabActivityPackage"

    .prologue
    .line 352
    iput-object p1, p0, Lmiui/resourcebrowser/ResourceContext;->tabActivityPackage:Ljava/lang/String;

    .line 353
    return-void
.end method

.method public setThumbnailCacheFolder(Ljava/lang/String;)V
    .locals 0
    .parameter "thumbnailCacheFolder"

    .prologue
    .line 328
    iput-object p1, p0, Lmiui/resourcebrowser/ResourceContext;->thumbnailCacheFolder:Ljava/lang/String;

    .line 329
    return-void
.end method

.method public setThumbnailImageWidth(I)V
    .locals 0
    .parameter "thumbnailImageWidth"

    .prologue
    .line 448
    iput p1, p0, Lmiui/resourcebrowser/ResourceContext;->thumbnailImageWidth:I

    .line 449
    return-void
.end method

.method public setTrackId(Ljava/lang/String;)V
    .locals 0
    .parameter "trackId"

    .prologue
    .line 424
    iput-object p1, p0, Lmiui/resourcebrowser/ResourceContext;->trackId:Ljava/lang/String;

    .line 425
    return-void
.end method

.method public setVersionCacheFolder(Ljava/lang/String;)V
    .locals 0
    .parameter "versionCacheFolder"

    .prologue
    .line 312
    iput-object p1, p0, Lmiui/resourcebrowser/ResourceContext;->versionCacheFolder:Ljava/lang/String;

    .line 313
    return-void
.end method

.method public setVersionSupported(Z)V
    .locals 0
    .parameter "versionSupported"

    .prologue
    .line 172
    iput-boolean p1, p0, Lmiui/resourcebrowser/ResourceContext;->versionSupported:Z

    .line 173
    return-void
.end method
