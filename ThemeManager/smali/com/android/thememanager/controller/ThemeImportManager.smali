.class public Lcom/android/thememanager/controller/ThemeImportManager;
.super Lmiui/resourcebrowser/controller/ImportManager;
.source "ThemeImportManager.java"

# interfaces
.implements Lcom/android/thememanager/ThemeResourceConstants;


# direct methods
.method public constructor <init>(Lmiui/resourcebrowser/ResourceContext;)V
    .locals 0
    .parameter "context"

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lmiui/resourcebrowser/controller/ImportManager;-><init>(Lmiui/resourcebrowser/ResourceContext;)V

    .line 27
    return-void
.end method

.method private buildBundle(Ljava/io/File;Lmiui/resourcebrowser/model/Resource;Lmiui/resourcebrowser/model/Resource;)Lmiui/resourcebrowser/model/Resource;
    .locals 12
    .parameter "folder"
    .parameter "parent"
    .parameter "resource"

    .prologue
    .line 157
    invoke-virtual {p3}, Lmiui/resourcebrowser/model/Resource;->getParentResources()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, i$:Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lmiui/resourcebrowser/model/RelatedResource;

    .line 158
    .local v6, parentResource:Lmiui/resourcebrowser/model/RelatedResource;
    invoke-virtual {v6}, Lmiui/resourcebrowser/model/RelatedResource;->getResourceCode()Ljava/lang/String;

    move-result-object v10

    const-string v11, "theme"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 159
    new-instance v7, Lmiui/resourcebrowser/controller/local/LocalJSONDataParser;

    iget-object v10, p0, Lmiui/resourcebrowser/controller/ImportManager;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-direct {v7, v10}, Lmiui/resourcebrowser/controller/local/LocalJSONDataParser;-><init>(Lmiui/resourcebrowser/ResourceContext;)V

    .line 161
    .local v7, parser:Lmiui/resourcebrowser/controller/local/LocalDataParser;
    :try_start_0
    new-instance v10, Ljava/io/File;

    invoke-virtual {v6}, Lmiui/resourcebrowser/model/RelatedResource;->getLocalPath()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v10}, Lmiui/resourcebrowser/controller/local/LocalJSONDataParser;->loadResource(Ljava/io/File;)Lmiui/resourcebrowser/model/Resource;

    move-result-object v9

    .line 162
    .local v9, r:Lmiui/resourcebrowser/model/Resource;
    if-eqz v9, :cond_0

    .line 163
    invoke-virtual {p3, v9}, Lmiui/resourcebrowser/model/Resource;->updateFrom(Lmiui/resourcebrowser/model/Resource;)V
    :try_end_0
    .catch Lmiui/resourcebrowser/controller/local/PersistenceException; {:try_start_0 .. :try_end_0} :catch_0

    .line 171
    .end local v6           #parentResource:Lmiui/resourcebrowser/model/RelatedResource;
    .end local v7           #parser:Lmiui/resourcebrowser/controller/local/LocalDataParser;
    .end local v9           #r:Lmiui/resourcebrowser/model/Resource;
    :cond_1
    invoke-virtual {p0, p2, p3}, Lcom/android/thememanager/controller/ThemeImportManager;->copyInheritedProperties(Lmiui/resourcebrowser/model/Resource;Lmiui/resourcebrowser/model/Resource;)V

    .line 172
    new-instance v0, Ljava/io/File;

    invoke-virtual {p3}, Lmiui/resourcebrowser/model/Resource;->getDownloadPath()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v0, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 173
    .local v0, downloadFile:Ljava/io/File;
    invoke-virtual {p3}, Lmiui/resourcebrowser/model/Resource;->getLocalId()Ljava/lang/String;

    move-result-object v5

    .line 174
    .local v5, id:Ljava/lang/String;
    if-nez v5, :cond_2

    .line 175
    iget-object v10, p0, Lmiui/resourcebrowser/controller/ImportManager;->idGenerationStrategy:Lmiui/resourcebrowser/controller/strategy/IdGenerationStrategy;

    invoke-virtual {v10}, Lmiui/resourcebrowser/controller/strategy/IdGenerationStrategy;->nextId()Ljava/lang/String;

    move-result-object v5

    .line 178
    :cond_2
    invoke-virtual {p2, v5}, Lmiui/resourcebrowser/model/Resource;->setLocalId(Ljava/lang/String;)V

    .line 179
    const-string v10, "theme"

    invoke-virtual {p0, v0, v5, v10}, Lcom/android/thememanager/controller/ThemeImportManager;->getComponentLocalPath(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p2, v10}, Lmiui/resourcebrowser/model/Resource;->setLocalPath(Ljava/lang/String;)V

    .line 180
    invoke-virtual {p0, v0, v5}, Lcom/android/thememanager/controller/ThemeImportManager;->getBundleFilePath(Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 181
    .local v2, filePath:Ljava/lang/String;
    invoke-virtual {p0, v2}, Lcom/android/thememanager/controller/ThemeImportManager;->createBundleContentFile(Ljava/lang/String;)Z

    .line 182
    invoke-virtual {p2, v2}, Lmiui/resourcebrowser/model/Resource;->setFilePath(Ljava/lang/String;)V

    .line 183
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lmiui/resourcebrowser/util/ResourceHelper;->getFileHash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p2, v10}, Lmiui/resourcebrowser/model/Resource;->setHash(Ljava/lang/String;)V

    .line 184
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v10

    invoke-virtual {p2, v10, v11}, Lmiui/resourcebrowser/model/Resource;->setSize(J)V

    .line 186
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 187
    .local v8, prefixes:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const/4 v3, 0x0

    .local v3, i:I
    :goto_1
    sget-object v10, Lcom/android/thememanager/controller/ThemeImportManager;->COMPONENT_PREVIEW_SHOW_ORDER:[J

    array-length v10, v10

    if-ge v3, v10, :cond_3

    .line 188
    sget-object v10, Lcom/android/thememanager/controller/ThemeImportManager;->COMPONENT_PREVIEW_SHOW_ORDER:[J

    aget-wide v10, v10, v3

    invoke-static {v10, v11}, Lcom/android/thememanager/util/ConstantsHelper;->getPreviewPrefix(J)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v8, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 187
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 166
    .end local v0           #downloadFile:Ljava/io/File;
    .end local v2           #filePath:Ljava/lang/String;
    .end local v3           #i:I
    .end local v5           #id:Ljava/lang/String;
    .end local v8           #prefixes:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .restart local v6       #parentResource:Lmiui/resourcebrowser/model/RelatedResource;
    .restart local v7       #parser:Lmiui/resourcebrowser/controller/local/LocalDataParser;
    :catch_0
    move-exception v1

    .line 167
    .local v1, e:Lmiui/resourcebrowser/controller/local/PersistenceException;
    invoke-virtual {v1}, Lmiui/resourcebrowser/controller/local/PersistenceException;->printStackTrace()V

    goto/16 :goto_0

    .line 190
    .end local v1           #e:Lmiui/resourcebrowser/controller/local/PersistenceException;
    .end local v6           #parentResource:Lmiui/resourcebrowser/model/RelatedResource;
    .end local v7           #parser:Lmiui/resourcebrowser/controller/local/LocalDataParser;
    .restart local v0       #downloadFile:Ljava/io/File;
    .restart local v2       #filePath:Ljava/lang/String;
    .restart local v3       #i:I
    .restart local v5       #id:Ljava/lang/String;
    .restart local v8       #prefixes:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    :cond_3
    invoke-virtual {p0, p1, p2, v8}, Lcom/android/thememanager/controller/ThemeImportManager;->resolveBuildInImages(Ljava/io/File;Lmiui/resourcebrowser/model/Resource;Ljava/util/List;)V

    .line 192
    return-object p2
.end method

.method private buildComponent(Ljava/io/File;Ljava/io/File;Lmiui/resourcebrowser/model/Resource;Lmiui/resourcebrowser/model/Resource;Ljava/lang/String;)Lmiui/resourcebrowser/model/Resource;
    .locals 4
    .parameter "folder"
    .parameter "file"
    .parameter "parent"
    .parameter "resource"
    .parameter "code"

    .prologue
    .line 231
    invoke-virtual {p4}, Lmiui/resourcebrowser/model/Resource;->getLocalId()Ljava/lang/String;

    move-result-object v0

    .line 232
    .local v0, id:Ljava/lang/String;
    if-nez v0, :cond_0

    .line 233
    iget-object v2, p0, Lmiui/resourcebrowser/controller/ImportManager;->idGenerationStrategy:Lmiui/resourcebrowser/controller/strategy/IdGenerationStrategy;

    invoke-virtual {v2}, Lmiui/resourcebrowser/controller/strategy/IdGenerationStrategy;->nextId()Ljava/lang/String;

    move-result-object v0

    .line 235
    :cond_0
    invoke-virtual {p4, v0}, Lmiui/resourcebrowser/model/Resource;->setLocalId(Ljava/lang/String;)V

    .line 236
    invoke-virtual {p0, p2, v0, p5}, Lcom/android/thememanager/controller/ThemeImportManager;->getComponentLocalPath(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p4, v2}, Lmiui/resourcebrowser/model/Resource;->setLocalPath(Ljava/lang/String;)V

    .line 237
    invoke-virtual {p0, p2, v0, p5}, Lcom/android/thememanager/controller/ThemeImportManager;->getComponentFilePath(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p4, v2}, Lmiui/resourcebrowser/model/Resource;->setFilePath(Ljava/lang/String;)V

    .line 238
    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lmiui/resourcebrowser/util/ResourceHelper;->getFileHash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p4, v2}, Lmiui/resourcebrowser/model/Resource;->setHash(Ljava/lang/String;)V

    .line 239
    invoke-virtual {p2}, Ljava/io/File;->length()J

    move-result-wide v2

    invoke-virtual {p4, v2, v3}, Lmiui/resourcebrowser/model/Resource;->setSize(J)V

    .line 241
    iget-object v2, p0, Lmiui/resourcebrowser/controller/ImportManager;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v2}, Lmiui/resourcebrowser/ResourceContext;->getBuildInImagePrefixes()Ljava/util/List;

    move-result-object v2

    invoke-virtual {p0, p1, p4, v2}, Lcom/android/thememanager/controller/ThemeImportManager;->resolveBuildInImages(Ljava/io/File;Lmiui/resourcebrowser/model/Resource;Ljava/util/List;)V

    .line 243
    const-string v2, "theme"

    invoke-virtual {p4}, Lmiui/resourcebrowser/model/Resource;->getParentResources()Ljava/util/List;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lcom/android/thememanager/controller/ThemeImportManager;->getRelatedResource(Ljava/lang/String;Ljava/util/List;)Lmiui/resourcebrowser/model/RelatedResource;

    move-result-object v1

    .line 244
    .local v1, relatedParent:Lmiui/resourcebrowser/model/RelatedResource;
    if-nez v1, :cond_1

    .line 245
    new-instance v1, Lmiui/resourcebrowser/model/RelatedResource;

    .end local v1           #relatedParent:Lmiui/resourcebrowser/model/RelatedResource;
    invoke-direct {v1}, Lmiui/resourcebrowser/model/RelatedResource;-><init>()V

    .line 246
    .restart local v1       #relatedParent:Lmiui/resourcebrowser/model/RelatedResource;
    invoke-virtual {p3}, Lmiui/resourcebrowser/model/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmiui/resourcebrowser/model/RelatedResource;->setLocalPath(Ljava/lang/String;)V

    .line 247
    const-string v2, "theme"

    invoke-virtual {v1, v2}, Lmiui/resourcebrowser/model/RelatedResource;->setResourceCode(Ljava/lang/String;)V

    .line 248
    invoke-virtual {p4, v1}, Lmiui/resourcebrowser/model/Resource;->addParentResources(Lmiui/resourcebrowser/model/RelatedResource;)V

    .line 251
    :cond_1
    return-object p4
.end method

.method private importComponent(Ljava/io/File;Ljava/io/File;Lmiui/resourcebrowser/model/Resource;Lmiui/resourcebrowser/model/Resource;Ljava/lang/String;)Z
    .locals 3
    .parameter "folder"
    .parameter "file"
    .parameter "parent"
    .parameter "resource"
    .parameter "code"

    .prologue
    .line 210
    invoke-direct/range {p0 .. p5}, Lcom/android/thememanager/controller/ThemeImportManager;->buildComponent(Ljava/io/File;Ljava/io/File;Lmiui/resourcebrowser/model/Resource;Lmiui/resourcebrowser/model/Resource;Ljava/lang/String;)Lmiui/resourcebrowser/model/Resource;

    move-result-object p4

    .line 212
    invoke-virtual {p3}, Lmiui/resourcebrowser/model/Resource;->getSubResources()Ljava/util/List;

    move-result-object v2

    invoke-virtual {p0, p5, v2}, Lcom/android/thememanager/controller/ThemeImportManager;->getRelatedResource(Ljava/lang/String;Ljava/util/List;)Lmiui/resourcebrowser/model/RelatedResource;

    move-result-object v1

    .line 213
    .local v1, relatedSub:Lmiui/resourcebrowser/model/RelatedResource;
    if-nez v1, :cond_0

    .line 214
    new-instance v1, Lmiui/resourcebrowser/model/RelatedResource;

    .end local v1           #relatedSub:Lmiui/resourcebrowser/model/RelatedResource;
    invoke-direct {v1}, Lmiui/resourcebrowser/model/RelatedResource;-><init>()V

    .line 215
    .restart local v1       #relatedSub:Lmiui/resourcebrowser/model/RelatedResource;
    invoke-virtual {p4}, Lmiui/resourcebrowser/model/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmiui/resourcebrowser/model/RelatedResource;->setLocalPath(Ljava/lang/String;)V

    .line 216
    invoke-virtual {p4}, Lmiui/resourcebrowser/model/Resource;->getFilePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmiui/resourcebrowser/model/RelatedResource;->setFilePath(Ljava/lang/String;)V

    .line 217
    invoke-virtual {v1, p5}, Lmiui/resourcebrowser/model/RelatedResource;->setResourceCode(Ljava/lang/String;)V

    .line 218
    invoke-virtual {p3, v1}, Lmiui/resourcebrowser/model/Resource;->addSubResources(Lmiui/resourcebrowser/model/RelatedResource;)V

    .line 221
    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-virtual {p4}, Lmiui/resourcebrowser/model/Resource;->getFilePath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 222
    .local v0, contentFile:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    .line 223
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 224
    invoke-virtual {p2, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 226
    iget-object v2, p0, Lmiui/resourcebrowser/controller/ImportManager;->controller:Lmiui/resourcebrowser/controller/ResourceController;

    invoke-virtual {v2}, Lmiui/resourcebrowser/controller/ResourceController;->getLocalDataManager()Lmiui/resourcebrowser/controller/LocalDataManager;

    move-result-object v2

    invoke-virtual {v2, p4}, Lmiui/resourcebrowser/controller/LocalDataManager;->addResource(Lmiui/resourcebrowser/model/Resource;)Z

    .line 227
    const/4 v2, 0x1

    return v2
.end method

.method private importComponents(Ljava/io/File;Lmiui/resourcebrowser/model/Resource;Lmiui/resourcebrowser/model/Resource;)Z
    .locals 10
    .parameter "folder"
    .parameter "parent"
    .parameter "resource"

    .prologue
    .line 196
    const/4 v7, 0x0

    .line 197
    .local v7, imported:Z
    invoke-virtual {p0, p1}, Lcom/android/thememanager/controller/ThemeImportManager;->getComponentsMap(Ljava/io/File;)Ljava/util/Map;

    move-result-object v8

    .line 198
    .local v8, map:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v0, p0, Lmiui/resourcebrowser/controller/ImportManager;->context:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v0}, Lmiui/resourcebrowser/ResourceContext;->getResourceCode()Ljava/lang/String;

    move-result-object v5

    .line 199
    .local v5, code:Ljava/lang/String;
    invoke-interface {v8}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, i$:Ljava/util/Iterator;
    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 200
    .local v9, name:Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1, v9}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 201
    .local v2, file:Ljava/io/File;
    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    .line 202
    invoke-direct/range {v0 .. v5}, Lcom/android/thememanager/controller/ThemeImportManager;->importComponent(Ljava/io/File;Ljava/io/File;Lmiui/resourcebrowser/model/Resource;Lmiui/resourcebrowser/model/Resource;Ljava/lang/String;)Z

    move-result v7

    .line 206
    .end local v2           #file:Ljava/io/File;
    .end local v9           #name:Ljava/lang/String;
    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/android/thememanager/controller/ThemeImportManager;->importComponents(Ljava/io/File;Lmiui/resourcebrowser/model/Resource;)Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz v7, :cond_2

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected buildComponent(Ljava/io/File;Ljava/io/File;Lmiui/resourcebrowser/model/Resource;Ljava/lang/String;)Lmiui/resourcebrowser/model/Resource;
    .locals 4
    .parameter "folder"
    .parameter "file"
    .parameter "parent"
    .parameter "code"

    .prologue
    .line 43
    invoke-super {p0, p1, p2, p3, p4}, Lmiui/resourcebrowser/controller/ImportManager;->buildComponent(Ljava/io/File;Ljava/io/File;Lmiui/resourcebrowser/model/Resource;Ljava/lang/String;)Lmiui/resourcebrowser/model/Resource;

    move-result-object v0

    .line 44
    .local v0, resource:Lmiui/resourcebrowser/model/Resource;
    invoke-static {p4}, Lcom/android/thememanager/util/ConstantsHelper;->getResourceTypeByCode(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    .line 45
    .local v1, resourceType:Ljava/lang/Long;
    if-eqz v1, :cond_0

    .line 46
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/android/thememanager/util/ConstantsHelper;->getPlatform(J)I

    move-result v2

    invoke-virtual {v0, v2}, Lmiui/resourcebrowser/model/Resource;->setPlatform(I)V

    .line 48
    :cond_0
    return-object v0
.end method

.method protected getComponentImagePrefixes(Ljava/lang/String;)Ljava/util/List;
    .locals 4
    .parameter "code"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 54
    .local v0, prefixes:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Lcom/android/thememanager/util/ConstantsHelper;->getResourceTypeByCode(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    .line 55
    .local v1, resourceType:Ljava/lang/Long;
    if-eqz v1, :cond_0

    .line 56
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/android/thememanager/util/ConstantsHelper;->getPreviewPrefix(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 60
    :goto_0
    return-object v0

    .line 58
    :cond_0
    invoke-super {p0, p1}, Lmiui/resourcebrowser/controller/ImportManager;->getComponentImagePrefixes(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method protected getComponentsMap(Ljava/io/File;)Ljava/util/Map;
    .locals 8
    .parameter "folder"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 31
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 32
    .local v3, map:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    sget-object v0, Lcom/android/thememanager/controller/ThemeImportManager;->COMPONENT_IDENTITIES:[Ljava/lang/String;

    .local v0, arr$:[Ljava/lang/String;
    array-length v2, v0

    .local v2, len$:I
    const/4 v1, 0x0

    .local v1, i$:I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v4, v0, v1

    .line 33
    .local v4, name:Ljava/lang/String;
    invoke-static {v4}, Lcom/android/thememanager/util/ConstantsHelper;->getResourceTypeByIdentity(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    .line 34
    .local v5, resourceType:Ljava/lang/Long;
    if-eqz v5, :cond_0

    .line 35
    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/android/thememanager/util/ConstantsHelper;->getResourceCode(J)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v4, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 38
    .end local v4           #name:Ljava/lang/String;
    .end local v5           #resourceType:Ljava/lang/Long;
    :cond_1
    return-object v3
.end method

.method protected importSingle(Lmiui/resourcebrowser/model/Resource;)Z
    .locals 7
    .parameter "resource"

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 105
    const/4 v1, 0x1

    .line 106
    .local v1, imported:Z
    invoke-virtual {p0, p1}, Lcom/android/thememanager/controller/ThemeImportManager;->unzipBundle(Lmiui/resourcebrowser/model/Resource;)Ljava/lang/String;

    move-result-object v3

    .line 107
    .local v3, unzipPath:Ljava/lang/String;
    if-nez v3, :cond_0

    .line 125
    :goto_0
    return v5

    .line 110
    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 111
    .local v0, folder:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 112
    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getOnlineId()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_2

    .line 113
    invoke-virtual {p1, v4}, Lmiui/resourcebrowser/model/Resource;->setStatus(I)V

    .line 118
    :goto_1
    new-instance v6, Lmiui/resourcebrowser/model/Resource;

    invoke-direct {v6}, Lmiui/resourcebrowser/model/Resource;-><init>()V

    invoke-direct {p0, v0, v6, p1}, Lcom/android/thememanager/controller/ThemeImportManager;->buildBundle(Ljava/io/File;Lmiui/resourcebrowser/model/Resource;Lmiui/resourcebrowser/model/Resource;)Lmiui/resourcebrowser/model/Resource;

    move-result-object v2

    .line 119
    .local v2, parent:Lmiui/resourcebrowser/model/Resource;
    invoke-virtual {v2}, Lmiui/resourcebrowser/model/Resource;->getHash()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Lmiui/resourcebrowser/model/Resource;->setHash(Ljava/lang/String;)V

    .line 120
    invoke-direct {p0, v0, v2, p1}, Lcom/android/thememanager/controller/ThemeImportManager;->importComponents(Ljava/io/File;Lmiui/resourcebrowser/model/Resource;Lmiui/resourcebrowser/model/Resource;)Z

    move-result v6

    if-eqz v6, :cond_3

    if-eqz v1, :cond_3

    move v1, v4

    .line 122
    :goto_2
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 123
    iget-object v4, p0, Lmiui/resourcebrowser/controller/ImportManager;->controller:Lmiui/resourcebrowser/controller/ResourceController;

    invoke-virtual {v4}, Lmiui/resourcebrowser/controller/ResourceController;->getLocalDataManager()Lmiui/resourcebrowser/controller/LocalDataManager;

    move-result-object v4

    invoke-virtual {v4, v2}, Lmiui/resourcebrowser/controller/LocalDataManager;->addResource(Lmiui/resourcebrowser/model/Resource;)Z

    .end local v2           #parent:Lmiui/resourcebrowser/model/Resource;
    :cond_1
    move v5, v1

    .line 125
    goto :goto_0

    .line 115
    :cond_2
    const/4 v6, 0x3

    invoke-virtual {p1, v6}, Lmiui/resourcebrowser/model/Resource;->setStatus(I)V

    goto :goto_1

    .restart local v2       #parent:Lmiui/resourcebrowser/model/Resource;
    :cond_3
    move v1, v5

    .line 120
    goto :goto_2
.end method

.method protected resolveBuildInImage(Ljava/io/File;Ljava/lang/String;Lmiui/resourcebrowser/model/Resource;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .parameter "folder"
    .parameter "targetFolder"
    .parameter "resource"
    .parameter "prefix"
    .parameter "ext"

    .prologue
    .line 80
    const/4 v0, 0x0

    .line 81
    .local v0, i:I
    const/4 v5, 0x1

    .line 82
    .local v5, thumbnailSpecial:Z
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "small_"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 84
    .local v6, workingPrefix:Ljava/lang/String;
    :goto_0
    new-instance v2, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    add-int/lit8 v1, v0, 0x1

    .end local v0           #i:I
    .local v1, i:I
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v2, p1, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 85
    .local v2, imageFile:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v7

    if-nez v7, :cond_1

    .line 86
    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    .line 87
    .local v3, name:Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 88
    .local v4, path:Ljava/lang/String;
    invoke-virtual {p3, v4}, Lmiui/resourcebrowser/model/Resource;->addBuildInThumbnail(Ljava/lang/String;)V

    .line 89
    const-string v7, "preview_cover_"

    invoke-virtual {v3, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    if-nez v5, :cond_0

    .line 90
    invoke-virtual {p3, v4}, Lmiui/resourcebrowser/model/Resource;->addBuildInPreview(Ljava/lang/String;)V

    .line 92
    :cond_0
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, v4}, Lmiui/os/Shell;->copy(Ljava/lang/String;Ljava/lang/String;)Z

    move v0, v1

    .line 93
    .end local v1           #i:I
    .restart local v0       #i:I
    goto :goto_0

    .end local v0           #i:I
    .end local v3           #name:Ljava/lang/String;
    .end local v4           #path:Ljava/lang/String;
    .restart local v1       #i:I
    :cond_1
    if-eqz v5, :cond_2

    .line 94
    move-object v6, p4

    .line 95
    const/4 v0, 0x0

    .line 96
    .end local v1           #i:I
    .restart local v0       #i:I
    const/4 v5, 0x0

    goto :goto_0

    .line 101
    .end local v0           #i:I
    .restart local v1       #i:I
    :cond_2
    return-void
.end method

.method protected resolveBuildInImages(Ljava/io/File;Lmiui/resourcebrowser/model/Resource;Ljava/util/List;)V
    .locals 7
    .parameter "folder"
    .parameter "resource"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Lmiui/resourcebrowser/model/Resource;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 64
    .local p3, prefixes:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->clearBuildInThumbnails()V

    .line 65
    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->clearBuildInPreviews()V

    .line 66
    new-instance v1, Ljava/io/File;

    const-string v0, "preview"

    invoke-direct {v1, p1, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 67
    .end local p1
    .local v1, folder:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 68
    invoke-virtual {p2}, Lmiui/resourcebrowser/model/Resource;->getLocalId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/thememanager/controller/ThemeImportManager;->getBuildInImageFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 69
    .local v2, targetFolder:Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 70
    const-string v4, "preview_cover_"

    const-string v5, ".jpg"

    move-object v0, p0

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/android/thememanager/controller/ThemeImportManager;->resolveBuildInImage(Ljava/io/File;Ljava/lang/String;Lmiui/resourcebrowser/model/Resource;Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    const-string v4, "preview_cover_"

    const-string v5, ".png"

    move-object v0, p0

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/android/thememanager/controller/ThemeImportManager;->resolveBuildInImage(Ljava/io/File;Ljava/lang/String;Lmiui/resourcebrowser/model/Resource;Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 73
    .local v4, prefix:Ljava/lang/String;
    const-string v5, ".jpg"

    move-object v0, p0

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/android/thememanager/controller/ThemeImportManager;->resolveBuildInImage(Ljava/io/File;Ljava/lang/String;Lmiui/resourcebrowser/model/Resource;Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    const-string v5, ".png"

    move-object v0, p0

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/android/thememanager/controller/ThemeImportManager;->resolveBuildInImage(Ljava/io/File;Ljava/lang/String;Lmiui/resourcebrowser/model/Resource;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 77
    .end local v2           #targetFolder:Ljava/lang/String;
    .end local v4           #prefix:Ljava/lang/String;
    .end local v6           #i$:Ljava/util/Iterator;
    :cond_0
    return-void
.end method

.method protected unzipBundle(Lmiui/resourcebrowser/model/Resource;)Ljava/lang/String;
    .locals 7
    .parameter "resource"

    .prologue
    .line 129
    invoke-super {p0, p1}, Lmiui/resourcebrowser/controller/ImportManager;->unzipBundle(Lmiui/resourcebrowser/model/Resource;)Ljava/lang/String;

    move-result-object v4

    .line 132
    .local v4, path:Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "fonts/Roboto-Regular.ttf"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 133
    .local v2, font:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_0

    .line 134
    new-instance v0, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "fonts/Arial.ttf"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 135
    .local v0, arial:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 136
    invoke-virtual {v0, v2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 143
    .end local v0           #arial:Ljava/io/File;
    :cond_0
    new-instance v3, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "fonts/DroidSansFallback.ttf"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 144
    .local v3, fontFallback:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 146
    :try_start_0
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z

    .line 147
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lmiui/resourcebrowser/util/ResourceHelper;->getFileHash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lmiui/resourcebrowser/util/ResourceHelper;->setFileHash(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 153
    :cond_1
    :goto_0
    return-object v4

    .line 149
    :catch_0
    move-exception v1

    .line 150
    .local v1, e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method
