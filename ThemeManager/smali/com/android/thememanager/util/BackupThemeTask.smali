.class public Lcom/android/thememanager/util/BackupThemeTask;
.super Landroid/os/AsyncTask;
.source "BackupThemeTask.java"

# interfaces
.implements Lcom/android/thememanager/ThemeResourceConstants;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;",
        "Lcom/android/thememanager/ThemeResourceConstants;"
    }
.end annotation


# static fields
.field public static final BACKUP_PATH:Ljava/lang/String;

.field public static final BACKUP_THEME_PATH:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mProgress:Landroid/app/ProgressDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 28
    sget-object v0, Lcom/android/thememanager/util/BackupThemeTask;->DOWNLOADED_THEME_PATH:Ljava/lang/String;

    sput-object v0, Lcom/android/thememanager/util/BackupThemeTask;->BACKUP_PATH:Ljava/lang/String;

    .line 29
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/thememanager/util/BackupThemeTask;->BACKUP_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/backup.mtz"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/thememanager/util/BackupThemeTask;->BACKUP_THEME_PATH:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .parameter "context"

    .prologue
    .line 36
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/android/thememanager/util/BackupThemeTask;->mContext:Landroid/content/Context;

    .line 38
    return-void
.end method

.method private backupRingtone(Ljava/util/zip/ZipOutputStream;ILjava/lang/String;)V
    .locals 3
    .parameter "out"
    .parameter "type"
    .parameter "targetPath"

    .prologue
    .line 50
    iget-object v2, p0, Lcom/android/thememanager/util/BackupThemeTask;->mContext:Landroid/content/Context;

    invoke-static {v2, p2}, Landroid/media/RingtoneManager;->getActualDefaultRingtoneUri(Landroid/content/Context;I)Landroid/net/Uri;

    move-result-object v1

    .line 51
    .local v1, uri:Landroid/net/Uri;
    iget-object v2, p0, Lcom/android/thememanager/util/BackupThemeTask;->mContext:Landroid/content/Context;

    invoke-static {v2, v1}, Lmiui/resourcebrowser/util/ResourceHelper;->getPathByUri(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 52
    .local v0, srcPath:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 53
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v2, p3}, Lmiui/resourcebrowser/util/ResourceHelper;->zip(Ljava/util/zip/ZipOutputStream;Ljava/io/File;Ljava/lang/String;)Z

    .line 55
    :cond_0
    return-void
.end method

.method private writeDescriptionInfo(Ljava/util/zip/ZipOutputStream;)V
    .locals 6
    .parameter "out"

    .prologue
    .line 70
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 71
    .local v0, builder:Ljava/lang/StringBuilder;
    const-string v2, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    const-string v2, "<theme>"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 75
    const-string v2, "\t<title>"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    iget-object v2, p0, Lcom/android/thememanager/util/BackupThemeTask;->mContext:Landroid/content/Context;

    const v3, 0x7f0b000a

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    const-string v2, "</title>"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    const-string v2, "\t<designer></designer>"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    const-string v2, "\t<author></author>"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83
    const-string v2, "\t<version>1.0</version>"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    const-string v2, "\t<platform>"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 87
    const-string v2, "</platform>"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    const-string v2, "</theme>"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    const-string v2, "%s/description.xml"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/android/thememanager/util/BackupThemeTask;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 92
    .local v1, descriptionPath:Ljava/lang/String;
    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-static {v2, v1}, Lmiui/resourcebrowser/util/ResourceHelper;->writeTo(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;

    .line 94
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v3, "description.xml"

    invoke-static {p1, v2, v3}, Lmiui/resourcebrowser/util/ResourceHelper;->zip(Ljava/util/zip/ZipOutputStream;Ljava/io/File;Ljava/lang/String;)Z

    .line 95
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"

    .prologue
    .line 26
    check-cast p1, [Ljava/lang/Void;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/android/thememanager/util/BackupThemeTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 9
    .parameter "object"

    .prologue
    const/4 v8, -0x1

    .line 99
    new-instance v2, Ljava/io/File;

    sget-object v6, Lcom/android/thememanager/util/BackupThemeTask;->BACKUP_PATH:Ljava/lang/String;

    const-string v7, "backup.mtz1"

    invoke-direct {v2, v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    .local v2, file:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 101
    invoke-virtual {v2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v6

    const/16 v7, 0x1ff

    invoke-static {v6, v7, v8, v8}, Lmiui/os/ExtraFileUtils;->mkdirs(Ljava/io/File;III)Z

    .line 103
    const/4 v3, 0x0

    .line 105
    .local v3, out:Ljava/util/zip/ZipOutputStream;
    :try_start_0
    new-instance v4, Ljava/util/zip/ZipOutputStream;

    new-instance v6, Ljava/io/BufferedOutputStream;

    new-instance v7, Ljava/io/FileOutputStream;

    invoke-direct {v7, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v6, v7}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v4, v6}, Ljava/util/zip/ZipOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 106
    .end local v3           #out:Ljava/util/zip/ZipOutputStream;
    .local v4, out:Ljava/util/zip/ZipOutputStream;
    const/4 v6, 0x0

    :try_start_1
    invoke-virtual {v4, v6}, Ljava/util/zip/ZipOutputStream;->setMethod(I)V

    .line 108
    invoke-direct {p0, v4}, Lcom/android/thememanager/util/BackupThemeTask;->writeDescriptionInfo(Ljava/util/zip/ZipOutputStream;)V

    .line 110
    const-string v0, "/data/system/theme/desk_wallpaper.tmp"

    .line 111
    .local v0, deskWallpaperTmpPath:Ljava/lang/String;
    const-string v6, "/data/data/com.android.settings/files/wallpaper"

    invoke-static {v6, v0}, Lmiui/os/Shell;->copy(Ljava/lang/String;Ljava/lang/String;)Z

    .line 112
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v7, "wallpaper/default_wallpaper.jpg"

    invoke-static {v4, v6, v7}, Lmiui/resourcebrowser/util/ResourceHelper;->zip(Ljava/util/zip/ZipOutputStream;Ljava/io/File;Ljava/lang/String;)Z

    .line 113
    invoke-static {v0}, Lmiui/os/Shell;->remove(Ljava/lang/String;)Z

    .line 115
    new-instance v6, Ljava/io/File;

    const-string v7, "/data/system/theme/lock_wallpaper"

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v7, "wallpaper/default_lock_wallpaper.jpg"

    invoke-static {v4, v6, v7}, Lmiui/resourcebrowser/util/ResourceHelper;->zip(Ljava/util/zip/ZipOutputStream;Ljava/io/File;Ljava/lang/String;)Z

    .line 117
    const/4 v6, 0x1

    const-string v7, "ringtones/ringtone.mp3"

    invoke-direct {p0, v4, v6, v7}, Lcom/android/thememanager/util/BackupThemeTask;->backupRingtone(Ljava/util/zip/ZipOutputStream;ILjava/lang/String;)V

    .line 118
    const/4 v6, 0x2

    const-string v7, "ringtones/notification.mp3"

    invoke-direct {p0, v4, v6, v7}, Lcom/android/thememanager/util/BackupThemeTask;->backupRingtone(Ljava/util/zip/ZipOutputStream;ILjava/lang/String;)V

    .line 119
    const/4 v6, 0x4

    const-string v7, "ringtones/alarm.mp3"

    invoke-direct {p0, v4, v6, v7}, Lcom/android/thememanager/util/BackupThemeTask;->backupRingtone(Ljava/util/zip/ZipOutputStream;ILjava/lang/String;)V

    .line 121
    new-instance v6, Ljava/io/File;

    const-string v7, "/data/system/theme/audioeffect"

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_0

    .line 122
    new-instance v6, Ljava/io/File;

    const-string v7, "/data/system/theme/audioeffect"

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v7, "audioeffect"

    invoke-static {v4, v6, v7}, Lmiui/resourcebrowser/util/ResourceHelper;->zip(Ljava/util/zip/ZipOutputStream;Ljava/io/File;Ljava/lang/String;)Z

    .line 125
    :cond_0
    new-instance v6, Ljava/io/File;

    const-string v7, "/data/system/theme/"

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v7, ""

    invoke-static {v4, v6, v7}, Lmiui/resourcebrowser/util/ResourceHelper;->zip(Ljava/util/zip/ZipOutputStream;Ljava/io/File;Ljava/lang/String;)Z

    .line 127
    new-instance v5, Ljava/io/File;

    sget-object v6, Lcom/android/thememanager/util/BackupThemeTask;->BACKUP_THEME_PATH:Ljava/lang/String;

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 128
    .local v5, target:Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    .line 129
    invoke-virtual {v2, v5}, Ljava/io/File;->renameTo(Ljava/io/File;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    .line 134
    if-eqz v4, :cond_1

    .line 136
    :try_start_2
    invoke-virtual {v4}, Ljava/util/zip/ZipOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 142
    :cond_1
    :goto_0
    const/4 v6, 0x0

    return-object v6

    .line 137
    :catch_0
    move-exception v1

    .line 138
    .local v1, e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 130
    .end local v0           #deskWallpaperTmpPath:Ljava/lang/String;
    .end local v1           #e:Ljava/io/IOException;
    .end local v4           #out:Ljava/util/zip/ZipOutputStream;
    .end local v5           #target:Ljava/io/File;
    .restart local v3       #out:Ljava/util/zip/ZipOutputStream;
    :catch_1
    move-exception v1

    .line 131
    .local v1, e:Ljava/lang/Exception;
    :goto_1
    :try_start_3
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 132
    new-instance v6, Ljava/lang/RuntimeException;

    invoke-direct {v6}, Ljava/lang/RuntimeException;-><init>()V

    throw v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 134
    .end local v1           #e:Ljava/lang/Exception;
    :catchall_0
    move-exception v6

    :goto_2
    if-eqz v3, :cond_2

    .line 136
    :try_start_4
    invoke-virtual {v3}, Ljava/util/zip/ZipOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 139
    :cond_2
    :goto_3
    throw v6

    .line 137
    :catch_2
    move-exception v1

    .line 138
    .local v1, e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 134
    .end local v1           #e:Ljava/io/IOException;
    .end local v3           #out:Ljava/util/zip/ZipOutputStream;
    .restart local v4       #out:Ljava/util/zip/ZipOutputStream;
    :catchall_1
    move-exception v6

    move-object v3, v4

    .end local v4           #out:Ljava/util/zip/ZipOutputStream;
    .restart local v3       #out:Ljava/util/zip/ZipOutputStream;
    goto :goto_2

    .line 130
    .end local v3           #out:Ljava/util/zip/ZipOutputStream;
    .restart local v4       #out:Ljava/util/zip/ZipOutputStream;
    :catch_3
    move-exception v1

    move-object v3, v4

    .end local v4           #out:Ljava/util/zip/ZipOutputStream;
    .restart local v3       #out:Ljava/util/zip/ZipOutputStream;
    goto :goto_1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 26
    check-cast p1, Ljava/lang/Void;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/android/thememanager/util/BackupThemeTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 3
    .parameter "object"

    .prologue
    .line 147
    iget-object v0, p0, Lcom/android/thememanager/util/BackupThemeTask;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 148
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/android/thememanager/util/BackupThemeTask;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0b0009

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 150
    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    .line 42
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/android/thememanager/util/BackupThemeTask;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/thememanager/util/BackupThemeTask;->mProgress:Landroid/app/ProgressDialog;

    .line 43
    iget-object v0, p0, Lcom/android/thememanager/util/BackupThemeTask;->mProgress:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 44
    iget-object v0, p0, Lcom/android/thememanager/util/BackupThemeTask;->mProgress:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/android/thememanager/util/BackupThemeTask;->mContext:Landroid/content/Context;

    const v2, 0x7f0b0008

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 45
    iget-object v0, p0, Lcom/android/thememanager/util/BackupThemeTask;->mProgress:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 46
    iget-object v0, p0, Lcom/android/thememanager/util/BackupThemeTask;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 47
    return-void
.end method
