.class public Lcom/android/thememanager/util/ThemeApplyUtils;
.super Ljava/lang/Object;
.source "ThemeApplyUtils.java"

# interfaces
.implements Lcom/android/thememanager/ThemeResourceConstants;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Landroid/content/Context;Lmiui/resourcebrowser/ResourceContext;Lmiui/resourcebrowser/model/Resource;JJLjava/lang/Runnable;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"
    .parameter "x5"

    .prologue
    .line 18
    invoke-static/range {p0 .. p7}, Lcom/android/thememanager/util/ThemeApplyUtils;->applyTheme(Landroid/content/Context;Lmiui/resourcebrowser/ResourceContext;Lmiui/resourcebrowser/model/Resource;JJLjava/lang/Runnable;)V

    return-void
.end method

.method public static applyResource(Landroid/app/Activity;Lmiui/resourcebrowser/ResourceContext;Lmiui/resourcebrowser/model/Resource;Lcom/android/thememanager/util/ThemeApplyParameters;)V
    .locals 23
    .parameter "context"
    .parameter "resContext"
    .parameter "resource"
    .parameter "params"

    .prologue
    .line 22
    const/16 v16, 0x0

    .line 23
    .local v16, changed:Z
    const/16 v22, 0x1

    .line 25
    .local v22, toastNeeded:Z
    const-string v2, "EXTRA_CTX_RESOURCE_TYPE"

    const-wide/16 v6, -0x1

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, Lmiui/resourcebrowser/ResourceContext;->getExtraMeta(Ljava/lang/String;Ljava/io/Serializable;)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    .line 27
    .local v3, resourceType:J
    const-string v2, "modulesFlag"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lmiui/resourcebrowser/model/Resource;->getExtraMeta(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 28
    .local v19, str:Ljava/lang/String;
    if-eqz v19, :cond_1

    invoke-static/range {v19 .. v19}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v17

    .line 30
    .local v17, moduleFlags:J
    :goto_0
    invoke-virtual/range {p2 .. p2}, Lmiui/resourcebrowser/model/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v5

    .line 32
    .local v5, localPath:Ljava/lang/String;
    const-wide/16 v6, 0x2

    cmp-long v2, v3, v6

    if-nez v2, :cond_2

    .line 33
    const/16 v22, 0x0

    .line 34
    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v2, p0

    invoke-static/range {v2 .. v7}, Lcom/android/thememanager/util/WallpaperUtils;->cropAndApplyWallpaper(Landroid/app/Activity;JLjava/lang/String;ZZ)V

    .line 94
    :goto_1
    if-eqz v22, :cond_0

    .line 95
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, Lcom/android/thememanager/util/ThemeHelper;->showThemeChangedToast(Landroid/content/Context;Z)V

    .line 97
    :cond_0
    :goto_2
    return-void

    .line 28
    .end local v5           #localPath:Ljava/lang/String;
    .end local v17           #moduleFlags:J
    :cond_1
    const-wide/16 v17, 0x0

    goto :goto_0

    .line 35
    .restart local v5       #localPath:Ljava/lang/String;
    .restart local v17       #moduleFlags:J
    :cond_2
    const-wide/16 v6, 0x4

    cmp-long v2, v3, v6

    if-nez v2, :cond_3

    .line 36
    const/16 v22, 0x0

    .line 37
    move-object/from16 v0, p0

    invoke-static {v0, v3, v4, v5}, Lcom/android/thememanager/util/ThemeApplyUtils;->giveTipForLockscreenPaper(Landroid/app/Activity;JLjava/lang/String;)V

    goto :goto_1

    .line 38
    :cond_3
    const-wide/16 v6, 0x40

    cmp-long v2, v3, v6

    if-nez v2, :cond_4

    .line 39
    move-object/from16 v0, p0

    invoke-static {v0, v5}, Lcom/android/thememanager/util/ThemeHelper;->applyBootAudio(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v16

    goto :goto_1

    .line 40
    :cond_4
    const-wide/16 v6, 0x100

    cmp-long v2, v3, v6

    if-nez v2, :cond_5

    .line 41
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-static {v0, v2, v5}, Lcom/android/thememanager/util/ThemeHelper;->setRingtone(Landroid/content/Context;ILjava/lang/String;)Z

    move-result v16

    goto :goto_1

    .line 42
    :cond_5
    const-wide/16 v6, 0x200

    cmp-long v2, v3, v6

    if-nez v2, :cond_6

    .line 43
    const/4 v2, 0x2

    move-object/from16 v0, p0

    invoke-static {v0, v2, v5}, Lcom/android/thememanager/util/ThemeHelper;->setRingtone(Landroid/content/Context;ILjava/lang/String;)Z

    move-result v16

    goto :goto_1

    .line 44
    :cond_6
    const-wide/16 v6, 0x400

    cmp-long v2, v3, v6

    if-nez v2, :cond_7

    .line 45
    const/4 v2, 0x4

    move-object/from16 v0, p0

    invoke-static {v0, v2, v5}, Lcom/android/thememanager/util/ThemeHelper;->setRingtone(Landroid/content/Context;ILjava/lang/String;)Z

    move-result v16

    goto :goto_1

    .line 47
    :cond_7
    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/android/thememanager/util/ThemeApplyParameters;->applyFlags:J

    move-wide/from16 v20, v0

    .line 48
    .local v20, tmpFlags:J
    invoke-static {}, Lcom/android/thememanager/util/ThemeHelper;->supportReplaceFont()Z

    move-result v2

    if-nez v2, :cond_8

    .line 49
    const-wide/32 v6, -0x40011

    and-long v20, v20, v6

    .line 51
    :cond_8
    invoke-static {}, Lcom/android/thememanager/util/ThemeHelper;->supportReplaceAudioEffect()Z

    move-result v2

    if-nez v2, :cond_9

    .line 52
    const-wide/32 v6, -0x8001

    and-long v20, v20, v6

    .line 54
    :cond_9
    move-wide/from16 v12, v20

    .line 55
    .local v12, applyFlags:J
    move-object/from16 v0, p3

    iget-boolean v2, v0, Lcom/android/thememanager/util/ThemeApplyParameters;->removeOthers:Z

    if-eqz v2, :cond_a

    const-wide/16 v6, -0x701

    or-long v10, v12, v6

    .line 57
    .local v10, removeFlags:J
    :goto_3
    invoke-static {v12, v13}, Lcom/android/thememanager/util/ThemeHelper;->getComponentNumber(J)I

    move-result v2

    if-gtz v2, :cond_b

    .line 58
    new-instance v2, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v6, 0x7f0b0024

    invoke-virtual {v2, v6}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v6, 0x1010355

    invoke-virtual {v2, v6}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v6, 0x104000a

    const/4 v7, 0x0

    invoke-virtual {v2, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_2

    .end local v10           #removeFlags:J
    :cond_a
    move-wide v10, v12

    .line 55
    goto :goto_3

    .line 62
    .restart local v10       #removeFlags:J
    :cond_b
    move-wide/from16 v0, v17

    invoke-static {v10, v11, v5, v0, v1}, Lcom/android/thememanager/util/ThemeHelper;->needsFontChange(JLjava/lang/String;J)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 63
    new-instance v15, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 64
    .local v15, builder:Landroid/app/AlertDialog$Builder;
    const v2, 0x7f0b0038

    invoke-virtual {v15, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v6, 0x1010355

    invoke-virtual {v2, v6}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v14, 0x7f0b0039

    new-instance v6, Lcom/android/thememanager/util/ThemeApplyUtils$1;

    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move-object/from16 v9, p2

    invoke-direct/range {v6 .. v13}, Lcom/android/thememanager/util/ThemeApplyUtils$1;-><init>(Landroid/app/Activity;Lmiui/resourcebrowser/ResourceContext;Lmiui/resourcebrowser/model/Resource;JJ)V

    invoke-virtual {v2, v14, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const/high16 v6, 0x104

    const/4 v7, 0x0

    invoke-virtual {v2, v6, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 78
    const-wide/16 v6, 0x10

    cmp-long v2, v3, v6

    if-eqz v2, :cond_c

    .line 79
    const v2, 0x7f0b003a

    new-instance v6, Lcom/android/thememanager/util/ThemeApplyUtils$2;

    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move-object/from16 v9, p2

    invoke-direct/range {v6 .. v13}, Lcom/android/thememanager/util/ThemeApplyUtils$2;-><init>(Landroid/app/Activity;Lmiui/resourcebrowser/ResourceContext;Lmiui/resourcebrowser/model/Resource;JJ)V

    invoke-virtual {v15, v2, v6}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 87
    :cond_c
    invoke-virtual {v15}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_2

    .line 90
    .end local v15           #builder:Landroid/app/AlertDialog$Builder;
    :cond_d
    const/16 v22, 0x0

    .line 91
    const/4 v14, 0x0

    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move-object/from16 v9, p2

    invoke-static/range {v7 .. v14}, Lcom/android/thememanager/util/ThemeApplyUtils;->applyTheme(Landroid/content/Context;Lmiui/resourcebrowser/ResourceContext;Lmiui/resourcebrowser/model/Resource;JJLjava/lang/Runnable;)V

    goto/16 :goto_1
.end method

.method private static applyTheme(Landroid/content/Context;Lmiui/resourcebrowser/ResourceContext;Lmiui/resourcebrowser/model/Resource;JJLjava/lang/Runnable;)V
    .locals 8
    .parameter "context"
    .parameter "resContext"
    .parameter "resource"
    .parameter "removeFlags"
    .parameter "applyFlags"
    .parameter "lastRun"

    .prologue
    .line 118
    new-instance v0, Lcom/android/thememanager/util/ApplyThemeTask;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-wide v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/android/thememanager/util/ApplyThemeTask;-><init>(Landroid/content/Context;Lmiui/resourcebrowser/ResourceContext;Lmiui/resourcebrowser/model/Resource;JJ)V

    .line 119
    .local v0, task:Lcom/android/thememanager/util/ApplyThemeTask;
    if-eqz p7, :cond_0

    .line 120
    invoke-virtual {v0, p7}, Lcom/android/thememanager/util/ApplyThemeTask;->setPostRunnable(Ljava/lang/Runnable;)V

    .line 122
    :cond_0
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/thememanager/util/ApplyThemeTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 123
    return-void
.end method

.method private static giveTipForLockscreenPaper(Landroid/app/Activity;JLjava/lang/String;)V
    .locals 7
    .parameter "activity"
    .parameter "resourceType"
    .parameter "localPath"

    .prologue
    const-wide/16 v1, 0x4

    const/4 v4, 0x0

    .line 100
    cmp-long v0, p1, v1

    if-nez v0, :cond_0

    invoke-static {}, Lmiui/content/res/ThemeResources;->getSystem()Lmiui/content/res/ThemeResourcesSystem;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/content/res/ThemeResourcesSystem;->hasAwesomeLockscreen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0b0025

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1010355

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v6

    .line 106
    .local v6, dialog:Landroid/app/AlertDialog;
    new-instance v0, Lcom/android/thememanager/util/ThemeApplyUtils$3;

    invoke-direct {v0, p0, p3}, Lcom/android/thememanager/util/ThemeApplyUtils$3;-><init>(Landroid/app/Activity;Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 114
    .end local v6           #dialog:Landroid/app/AlertDialog;
    :goto_0
    return-void

    :cond_0
    move-object v0, p0

    move-object v3, p3

    move v5, v4

    .line 112
    invoke-static/range {v0 .. v5}, Lcom/android/thememanager/util/WallpaperUtils;->cropAndApplyWallpaper(Landroid/app/Activity;JLjava/lang/String;ZZ)V

    goto :goto_0
.end method
