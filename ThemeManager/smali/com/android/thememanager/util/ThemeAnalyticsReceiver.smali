.class public Lcom/android/thememanager/util/ThemeAnalyticsReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ThemeAnalyticsReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private getScreenType(Landroid/view/WindowManager;)Ljava/lang/String;
    .locals 6
    .parameter "wm"

    .prologue
    .line 46
    const/4 v3, 0x0

    .line 47
    .local v3, screenType:Ljava/lang/String;
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 48
    .local v1, dm:Landroid/util/DisplayMetrics;
    invoke-interface {p1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 49
    .local v0, display:Landroid/view/Display;
    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 50
    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v2

    .line 51
    .local v2, rotation:I
    if-eqz v2, :cond_0

    const/4 v4, 0x2

    if-ne v2, v4, :cond_1

    .line 52
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget v5, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "*"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 56
    :goto_0
    return-object v3

    .line 54
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget v5, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "*"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method private getThemeId(Landroid/content/SharedPreferences;)Ljava/lang/String;
    .locals 3
    .parameter "pref"

    .prologue
    .line 91
    const-wide/16 v1, -0x1

    invoke-static {v1, v2}, Lcom/android/thememanager/util/ThemeHelper;->loadUserPreferencePath(J)Ljava/lang/String;

    move-result-object v0

    .line 92
    .local v0, themePath:Ljava/lang/String;
    invoke-static {v0}, Lcom/android/thememanager/util/ThemeHelper;->getResourceIdFromPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private getThemeNum()Ljava/lang/String;
    .locals 10

    .prologue
    .line 60
    const-string v7, "Undefined"

    .line 61
    .local v7, themeNum:Ljava/lang/String;
    const/4 v4, 0x0

    .line 62
    .local v4, themeCount:I
    new-instance v5, Ljava/io/File;

    const-string v8, "/data/system/theme/"

    invoke-direct {v5, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 63
    .local v5, themeFolder:Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 64
    invoke-virtual {v5}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v6

    .line 65
    .local v6, themeList:[Ljava/lang/String;
    if-eqz v6, :cond_1

    .line 66
    move-object v0, v6

    .local v0, arr$:[Ljava/lang/String;
    array-length v3, v0

    .local v3, len$:I
    const/4 v2, 0x0

    .local v2, i$:I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 67
    .local v1, fileName:Ljava/lang/String;
    const-string v8, ".mtz"

    invoke-virtual {v1, v8}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 68
    add-int/lit8 v4, v4, 0x1

    .line 66
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 72
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #fileName:Ljava/lang/String;
    .end local v2           #i$:I
    .end local v3           #len$:I
    :cond_1
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 74
    .end local v6           #themeList:[Ljava/lang/String;
    :cond_2
    new-instance v5, Ljava/io/File;

    .end local v5           #themeFolder:Ljava/io/File;
    const-string v8, "/system/media/theme/.data/meta/theme/"

    invoke-direct {v5, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 75
    .restart local v5       #themeFolder:Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v8

    if-eqz v8, :cond_5

    .line 76
    invoke-virtual {v5}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v6

    .line 77
    .restart local v6       #themeList:[Ljava/lang/String;
    if-eqz v6, :cond_4

    .line 78
    move-object v0, v6

    .restart local v0       #arr$:[Ljava/lang/String;
    array-length v3, v0

    .restart local v3       #len$:I
    const/4 v2, 0x0

    .restart local v2       #i$:I
    :goto_1
    if-ge v2, v3, :cond_4

    aget-object v1, v0, v2

    .line 79
    .restart local v1       #fileName:Ljava/lang/String;
    const-string v8, ".mtz"

    invoke-virtual {v1, v8}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 80
    add-int/lit8 v4, v4, 0x1

    .line 78
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 84
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #fileName:Ljava/lang/String;
    .end local v2           #i$:I
    .end local v3           #len$:I
    :cond_4
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 86
    .end local v6           #themeList:[Ljava/lang/String;
    :cond_5
    return-object v7
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 11
    .parameter "context"
    .parameter "intent"

    .prologue
    .line 24
    invoke-static {}, Lmiui/analytics/XiaomiAnalytics;->getInstance()Lmiui/analytics/XiaomiAnalytics;

    move-result-object v8

    .line 25
    .local v8, xiaomiAnalytics:Lmiui/analytics/XiaomiAnalytics;
    const-string v9, "window"

    invoke-virtual {p1, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/WindowManager;

    .line 26
    .local v7, wm:Landroid/view/WindowManager;
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 28
    .local v2, pref:Landroid/content/SharedPreferences;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "MIUI-"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-object v10, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-object v10, Lmiui/os/Build;->ID:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 29
    .local v1, osType:Ljava/lang/String;
    invoke-direct {p0, v7}, Lcom/android/thememanager/util/ThemeAnalyticsReceiver;->getScreenType(Landroid/view/WindowManager;)Ljava/lang/String;

    move-result-object v3

    .line 30
    .local v3, screenType:Ljava/lang/String;
    invoke-direct {p0, v2}, Lcom/android/thememanager/util/ThemeAnalyticsReceiver;->getThemeId(Landroid/content/SharedPreferences;)Ljava/lang/String;

    move-result-object v4

    .line 31
    .local v4, themeId:Ljava/lang/String;
    const-wide/16 v9, -0x1

    invoke-static {v9, v10}, Lcom/android/thememanager/util/ThemeHelper;->loadUserPreferenceName(J)Ljava/lang/String;

    move-result-object v5

    .line 32
    .local v5, themeName:Ljava/lang/String;
    invoke-direct {p0}, Lcom/android/thememanager/util/ThemeAnalyticsReceiver;->getThemeNum()Ljava/lang/String;

    move-result-object v6

    .line 34
    .local v6, themeNum:Ljava/lang/String;
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 35
    .local v0, eventParamMap:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v9, "os_type"

    invoke-static {v1}, Lcom/android/thememanager/util/ThemeHelper;->avoidNullString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v0, v9, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    const-string v9, "screen_type"

    invoke-static {v3}, Lcom/android/thememanager/util/ThemeHelper;->avoidNullString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v0, v9, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    const-string v9, "theme_id"

    invoke-static {v4}, Lcom/android/thememanager/util/ThemeHelper;->avoidNullString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v0, v9, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    const-string v9, "theme_name"

    invoke-static {v5}, Lcom/android/thememanager/util/ThemeHelper;->avoidNullString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v0, v9, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    const-string v9, "theme_num"

    invoke-static {v6}, Lcom/android/thememanager/util/ThemeHelper;->avoidNullString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v0, v9, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    invoke-virtual {v8, p1}, Lmiui/analytics/XiaomiAnalytics;->startSession(Landroid/content/Context;)V

    .line 41
    const-string v9, "daily_data"

    invoke-virtual {v8, v9, v0}, Lmiui/analytics/XiaomiAnalytics;->trackEvent(Ljava/lang/String;Ljava/util/Map;)V

    .line 42
    invoke-virtual {v8}, Lmiui/analytics/XiaomiAnalytics;->endSession()V

    .line 43
    return-void
.end method
