.class public Lcom/android/thememanager/util/ApplyThemeTask;
.super Landroid/os/AsyncTask;
.source "ApplyThemeTask.java"

# interfaces
.implements Lcom/android/thememanager/ThemeResourceConstants;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;",
        "Lcom/android/thememanager/ThemeResourceConstants;"
    }
.end annotation


# instance fields
.field private mApplyFlags:J

.field private mContext:Landroid/content/Context;

.field private mFinishRunnable:Ljava/lang/Runnable;

.field private mModuleFlags:J

.field private mProgress:Landroid/app/ProgressDialog;

.field private mRemoveFlags:J

.field private mResContext:Lmiui/resourcebrowser/ResourceContext;

.field private mResource:Lmiui/resourcebrowser/model/Resource;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lmiui/resourcebrowser/ResourceContext;Lmiui/resourcebrowser/model/Resource;JJ)V
    .locals 7
    .parameter "context"
    .parameter "resContext"
    .parameter "resource"
    .parameter "removeFlags"
    .parameter "applyFlags"

    .prologue
    .line 54
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 55
    iput-object p1, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mContext:Landroid/content/Context;

    .line 56
    iput-object p2, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    .line 57
    iput-object p3, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mResource:Lmiui/resourcebrowser/model/Resource;

    .line 58
    invoke-virtual {p3}, Lmiui/resourcebrowser/model/Resource;->getSubResources()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p3}, Lmiui/resourcebrowser/model/Resource;->getParentResources()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-eqz v3, :cond_0

    .line 59
    invoke-virtual {p3}, Lmiui/resourcebrowser/model/Resource;->getParentResources()Ljava/util/List;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/resourcebrowser/model/RelatedResource;

    .line 60
    .local v1, parent:Lmiui/resourcebrowser/model/RelatedResource;
    new-instance v2, Lmiui/resourcebrowser/controller/local/LocalJSONDataParser;

    iget-object v3, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-direct {v2, v3}, Lmiui/resourcebrowser/controller/local/LocalJSONDataParser;-><init>(Lmiui/resourcebrowser/ResourceContext;)V

    .line 62
    .local v2, parser:Lmiui/resourcebrowser/controller/local/LocalDataParser;
    :try_start_0
    new-instance v3, Ljava/io/File;

    invoke-virtual {v1}, Lmiui/resourcebrowser/model/RelatedResource;->getLocalPath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Lmiui/resourcebrowser/controller/local/LocalJSONDataParser;->loadResource(Ljava/io/File;)Lmiui/resourcebrowser/model/Resource;

    move-result-object v3

    iput-object v3, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mResource:Lmiui/resourcebrowser/model/Resource;
    :try_end_0
    .catch Lmiui/resourcebrowser/controller/local/PersistenceException; {:try_start_0 .. :try_end_0} :catch_0

    .line 67
    .end local v1           #parent:Lmiui/resourcebrowser/model/RelatedResource;
    .end local v2           #parser:Lmiui/resourcebrowser/controller/local/LocalDataParser;
    :cond_0
    :goto_0
    const-wide/16 v3, 0x10

    and-long/2addr v3, p6

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-eqz v3, :cond_1

    .line 68
    const-wide/32 v3, 0x40000

    or-long/2addr p6, v3

    .line 70
    :cond_1
    invoke-virtual {p3}, Lmiui/resourcebrowser/model/Resource;->getPlatform()I

    move-result v3

    invoke-static {v3, p6, p7}, Lcom/android/thememanager/util/ThemeHelper;->getCompatibleFlag(IJ)J

    move-result-wide p6

    .line 71
    const-string v3, "modulesFlag"

    invoke-virtual {p3, v3}, Lmiui/resourcebrowser/model/Resource;->getExtraMeta(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mModuleFlags:J

    .line 72
    or-long v3, p4, p6

    iput-wide v3, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mRemoveFlags:J

    .line 73
    iput-wide p6, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mApplyFlags:J

    .line 74
    return-void

    .line 63
    .restart local v1       #parent:Lmiui/resourcebrowser/model/RelatedResource;
    .restart local v2       #parser:Lmiui/resourcebrowser/controller/local/LocalDataParser;
    :catch_0
    move-exception v0

    .line 64
    .local v0, e:Lmiui/resourcebrowser/controller/local/PersistenceException;
    invoke-virtual {v0}, Lmiui/resourcebrowser/controller/local/PersistenceException;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/android/thememanager/util/ApplyThemeTask;)Landroid/app/ProgressDialog;
    .locals 1
    .parameter "x0"

    .prologue
    .line 39
    iget-object v0, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mProgress:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method private apply(Lmiui/resourcebrowser/model/Resource;JJ)V
    .locals 8
    .parameter "resource"
    .parameter "removeFlags"
    .parameter "applyFlags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 182
    const-wide/16 v4, 0x8

    and-long/2addr v4, p2

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_4

    const/16 v2, 0xa

    .line 183
    .local v2, iconSteps:I
    :goto_0
    iget-object v4, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getSubResources()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v5, v5, 0x8

    add-int/2addr v5, v2

    invoke-virtual {v4, v5}, Landroid/app/ProgressDialog;->setMax(I)V

    .line 186
    iget-object v4, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/android/thememanager/util/ThemeHelper;->createRuntimeFolder(Landroid/content/Context;)V

    .line 187
    iget-object v4, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mProgress:Landroid/app/ProgressDialog;

    iget-object v5, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v5}, Landroid/app/ProgressDialog;->getProgress()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v4, v5}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 190
    invoke-direct {p0, p2, p3}, Lcom/android/thememanager/util/ApplyThemeTask;->getComponentFilterPath(J)Ljava/util/List;

    move-result-object v1

    .line 191
    .local v1, deleteFilters:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const-wide/32 v4, 0x10000000

    and-long/2addr v4, p2

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_5

    const/4 v4, 0x1

    :goto_1
    invoke-direct {p0, v1, v4}, Lcom/android/thememanager/util/ApplyThemeTask;->delete(Ljava/util/List;Z)V

    .line 192
    iget-object v4, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mProgress:Landroid/app/ProgressDialog;

    iget-object v5, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v5}, Landroid/app/ProgressDialog;->getProgress()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v4, v5}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 195
    invoke-direct {p0, p1, p4, p5}, Lcom/android/thememanager/util/ApplyThemeTask;->copyResources(Lmiui/resourcebrowser/model/Resource;J)V

    .line 196
    iget-object v4, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mProgress:Landroid/app/ProgressDialog;

    iget-object v5, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v5}, Landroid/app/ProgressDialog;->getProgress()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v4, v5}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 199
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/android/thememanager/util/ApplyThemeTask;->convertResourcesNames(JJ)V

    .line 200
    iget-object v4, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mProgress:Landroid/app/ProgressDialog;

    iget-object v5, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v5}, Landroid/app/ProgressDialog;->getProgress()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v4, v5}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 203
    const-wide/32 v4, 0x8000

    and-long/2addr v4, p2

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_0

    .line 207
    :cond_0
    iget-object v4, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mProgress:Landroid/app/ProgressDialog;

    iget-object v5, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v5}, Landroid/app/ProgressDialog;->getProgress()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v4, v5}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 210
    const-wide/16 v4, 0x8

    and-long/2addr v4, p2

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_1

    .line 211
    const/4 v4, 0x0

    invoke-static {v4}, Lmiui/content/res/IconCustomizer;->clearCustomizedIcons(Ljava/lang/String;)V

    .line 212
    iget-object v4, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mProgress:Landroid/app/ProgressDialog;

    iget-object v5, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v5}, Landroid/app/ProgressDialog;->getProgress()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v4, v5}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 214
    invoke-static {}, Lmiui/content/res/ThemeResources;->getSystem()Lmiui/content/res/ThemeResourcesSystem;

    move-result-object v4

    invoke-virtual {v4}, Lmiui/content/res/ThemeResourcesSystem;->resetIcons()V

    .line 215
    iget-object v4, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mProgress:Landroid/app/ProgressDialog;

    iget-object v5, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v5}, Landroid/app/ProgressDialog;->getProgress()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v4, v5}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 217
    iget-object v4, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mContext:Landroid/content/Context;

    new-instance v5, Lcom/android/thememanager/util/ApplyThemeTask$1;

    invoke-direct {v5, p0, v2}, Lcom/android/thememanager/util/ApplyThemeTask$1;-><init>(Lcom/android/thememanager/util/ApplyThemeTask;I)V

    invoke-static {v4, v5}, Lmiui/content/res/IconCustomizer;->prepareCustomizedIcons(Landroid/content/Context;Lmiui/content/res/IconCustomizer$CustomizedIconsListener;)V

    .line 237
    iget-object v4, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mProgress:Landroid/app/ProgressDialog;

    iget-object v5, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v5}, Landroid/app/ProgressDialog;->getProgress()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v4, v5}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 241
    :cond_1
    const-wide/16 v4, 0x1000

    and-long/2addr v4, p4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_3

    .line 242
    new-instance v3, Ljava/io/File;

    invoke-static {}, Lcom/android/thememanager/util/ThemeHelper;->getLockstyleAppliedConfigFilePath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 243
    .local v3, oldConfigFile:Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 244
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 251
    :cond_2
    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getFilePath()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/thememanager/util/ThemeHelper;->getLockstyleSDCardConfigPathFromThemePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 252
    .local v0, configFile:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 253
    invoke-static {v0, v3}, Landroid/os/FileUtils;->copyFile(Ljava/io/File;Ljava/io/File;)Z

    .line 254
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x1fd

    const/4 v6, -0x1

    const/4 v7, -0x1

    invoke-static {v4, v5, v6, v7}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    .line 257
    .end local v0           #configFile:Ljava/io/File;
    .end local v3           #oldConfigFile:Ljava/io/File;
    :cond_3
    iget-object v4, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mProgress:Landroid/app/ProgressDialog;

    iget-object v5, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v5}, Landroid/app/ProgressDialog;->getProgress()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v4, v5}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 258
    return-void

    .line 182
    .end local v1           #deleteFilters:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .end local v2           #iconSteps:I
    :cond_4
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 191
    .restart local v1       #deleteFilters:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .restart local v2       #iconSteps:I
    :cond_5
    const/4 v4, 0x0

    goto/16 :goto_1
.end method

.method private containsLockscreenThemeValue()Z
    .locals 5

    .prologue
    .line 150
    const/4 v1, 0x0

    .line 151
    .local v1, ret:Z
    const/4 v2, 0x0

    .line 153
    .local v2, zipfile:Ljava/util/zip/ZipFile;
    :try_start_0
    new-instance v3, Ljava/util/zip/ZipFile;

    const-string v4, "/data/system/theme/lockscreen"

    invoke-direct {v3, v4}, Ljava/util/zip/ZipFile;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 154
    .end local v2           #zipfile:Ljava/util/zip/ZipFile;
    .local v3, zipfile:Ljava/util/zip/ZipFile;
    :try_start_1
    const-string v4, "theme_values.xml"

    invoke-virtual {v3, v4}, Ljava/util/zip/ZipFile;->getEntry(Ljava/lang/String;)Ljava/util/zip/ZipEntry;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    move-result-object v4

    if-eqz v4, :cond_1

    const/4 v1, 0x1

    .line 158
    :goto_0
    if-eqz v3, :cond_3

    .line 160
    :try_start_2
    invoke-virtual {v3}, Ljava/util/zip/ZipFile;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v2, v3

    .line 166
    .end local v3           #zipfile:Ljava/util/zip/ZipFile;
    .restart local v2       #zipfile:Ljava/util/zip/ZipFile;
    :cond_0
    :goto_1
    return v1

    .line 154
    .end local v2           #zipfile:Ljava/util/zip/ZipFile;
    .restart local v3       #zipfile:Ljava/util/zip/ZipFile;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 161
    :catch_0
    move-exception v0

    .line 162
    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move-object v2, v3

    .line 163
    .end local v3           #zipfile:Ljava/util/zip/ZipFile;
    .restart local v2       #zipfile:Ljava/util/zip/ZipFile;
    goto :goto_1

    .line 155
    .end local v0           #e:Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 156
    .local v0, e:Ljava/lang/Exception;
    :goto_2
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 158
    if-eqz v2, :cond_0

    .line 160
    :try_start_4
    invoke-virtual {v2}, Ljava/util/zip/ZipFile;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    .line 161
    :catch_2
    move-exception v0

    .line 162
    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 158
    .end local v0           #e:Ljava/io/IOException;
    :catchall_0
    move-exception v4

    :goto_3
    if-eqz v2, :cond_2

    .line 160
    :try_start_5
    invoke-virtual {v2}, Ljava/util/zip/ZipFile;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 163
    :cond_2
    :goto_4
    throw v4

    .line 161
    :catch_3
    move-exception v0

    .line 162
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 158
    .end local v0           #e:Ljava/io/IOException;
    .end local v2           #zipfile:Ljava/util/zip/ZipFile;
    .restart local v3       #zipfile:Ljava/util/zip/ZipFile;
    :catchall_1
    move-exception v4

    move-object v2, v3

    .end local v3           #zipfile:Ljava/util/zip/ZipFile;
    .restart local v2       #zipfile:Ljava/util/zip/ZipFile;
    goto :goto_3

    .line 155
    .end local v2           #zipfile:Ljava/util/zip/ZipFile;
    .restart local v3       #zipfile:Ljava/util/zip/ZipFile;
    :catch_4
    move-exception v0

    move-object v2, v3

    .end local v3           #zipfile:Ljava/util/zip/ZipFile;
    .restart local v2       #zipfile:Ljava/util/zip/ZipFile;
    goto :goto_2

    .end local v2           #zipfile:Ljava/util/zip/ZipFile;
    .restart local v3       #zipfile:Ljava/util/zip/ZipFile;
    :cond_3
    move-object v2, v3

    .end local v3           #zipfile:Ljava/util/zip/ZipFile;
    .restart local v2       #zipfile:Ljava/util/zip/ZipFile;
    goto :goto_1
.end method

.method private convertResourcesNames(JJ)V
    .locals 9
    .parameter "removeFlags"
    .parameter "applyFlags"

    .prologue
    const-wide/32 v5, 0x40010

    const-wide/16 v7, 0x0

    .line 401
    and-long v3, p1, v5

    cmp-long v3, v3, v7

    if-eqz v3, :cond_0

    .line 403
    const-string v3, "/system/fonts"

    invoke-direct {p0, v3}, Lcom/android/thememanager/util/ApplyThemeTask;->createFontFileLinkForPatchRomDevices(Ljava/lang/String;)V

    .line 405
    :cond_0
    and-long v3, p3, v5

    cmp-long v3, v3, v7

    if-eqz v3, :cond_2

    .line 407
    const-string v3, "/data/system/theme/fonts"

    invoke-static {v3}, Lmiui/os/ExtraFileUtils;->standardizeFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 408
    .local v1, fontFolderPath:Ljava/lang/String;
    iget-wide v3, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mModuleFlags:J

    const-wide/16 v5, 0x10

    and-long/2addr v3, v5

    cmp-long v3, v3, v7

    if-eqz v3, :cond_1

    .line 409
    const-string v3, "/data/system/theme/fonts/Roboto-Regular.ttf"

    invoke-static {v3}, Lmiui/os/ExtraFileUtils;->standardizeFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 410
    .local v2, fontPath:Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 411
    .local v0, file:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v3

    cmp-long v3, v3, v7

    if-lez v3, :cond_3

    .line 412
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Roboto-Bold.ttf"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lmiui/os/Shell;->link(Ljava/lang/String;Ljava/lang/String;)Z

    .line 413
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Roboto-Italic.ttf"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lmiui/os/Shell;->link(Ljava/lang/String;Ljava/lang/String;)Z

    .line 414
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Roboto-BoldItalic.ttf"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lmiui/os/Shell;->link(Ljava/lang/String;Ljava/lang/String;)Z

    .line 420
    .end local v0           #file:Ljava/io/File;
    .end local v2           #fontPath:Ljava/lang/String;
    :cond_1
    :goto_0
    invoke-direct {p0, v1}, Lcom/android/thememanager/util/ApplyThemeTask;->createFontFileLinkForPatchRomDevices(Ljava/lang/String;)V

    .line 422
    .end local v1           #fontFolderPath:Ljava/lang/String;
    :cond_2
    const-string v3, "/data/system/theme/com.android.launcher"

    const-string v4, "/data/system/theme/com.miui.home"

    invoke-static {v3, v4}, Lmiui/os/Shell;->move(Ljava/lang/String;Ljava/lang/String;)Z

    .line 424
    return-void

    .line 417
    .restart local v0       #file:Ljava/io/File;
    .restart local v1       #fontFolderPath:Ljava/lang/String;
    .restart local v2       #fontPath:Ljava/lang/String;
    :cond_3
    invoke-static {v2}, Lmiui/os/Shell;->remove(Ljava/lang/String;)Z

    goto :goto_0
.end method

.method private copyResources(Lmiui/resourcebrowser/model/Resource;J)V
    .locals 21
    .parameter "resource"
    .parameter "applyFlags"

    .prologue
    .line 328
    :try_start_0
    move-object/from16 v0, p0

    move-wide/from16 v1, p2

    invoke-direct {v0, v1, v2}, Lcom/android/thememanager/util/ApplyThemeTask;->getComponentFilterPath(J)Ljava/util/List;

    move-result-object v3

    .line 329
    .local v3, applyFilters:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const-wide/32 v17, 0x10000000

    and-long v17, v17, p2

    const-wide/16 v19, 0x0

    cmp-long v17, v17, v19

    if-eqz v17, :cond_b

    const/4 v7, 0x1

    .line 330
    .local v7, copyThirdAppRes:Z
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lmiui/resourcebrowser/model/Resource;->getSubResources()Ljava/util/List;

    move-result-object v16

    .line 333
    .local v16, subResources:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/RelatedResource;>;"
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->isEmpty()Z

    move-result v17

    if-eqz v17, :cond_1

    .line 334
    move-wide/from16 v5, p2

    .line 335
    .local v5, componentFlag:J
    const-wide/16 v17, 0x1000

    and-long v17, v17, v5

    const-wide/16 v19, 0x0

    cmp-long v17, v17, v19

    if-eqz v17, :cond_c

    .line 336
    const-wide/16 v5, 0x1000

    .line 342
    :cond_0
    new-instance v12, Lmiui/resourcebrowser/model/RelatedResource;

    invoke-direct {v12}, Lmiui/resourcebrowser/model/RelatedResource;-><init>()V

    .line 343
    .local v12, res:Lmiui/resourcebrowser/model/RelatedResource;
    invoke-virtual/range {p1 .. p1}, Lmiui/resourcebrowser/model/Resource;->getFilePath()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lmiui/resourcebrowser/model/RelatedResource;->setFilePath(Ljava/lang/String;)V

    .line 344
    invoke-static {v5, v6}, Lcom/android/thememanager/util/ConstantsHelper;->getResourceCode(J)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lmiui/resourcebrowser/model/RelatedResource;->setResourceCode(Ljava/lang/String;)V

    .line 345
    move-object/from16 v0, v16

    invoke-interface {v0, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 348
    .end local v5           #componentFlag:J
    .end local v12           #res:Lmiui/resourcebrowser/model/RelatedResource;
    :cond_1
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, i$:Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_a

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lmiui/resourcebrowser/model/RelatedResource;

    .line 349
    .local v15, sub:Lmiui/resourcebrowser/model/RelatedResource;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/thememanager/util/ApplyThemeTask;->mProgress:Landroid/app/ProgressDialog;

    move-object/from16 v17, v0

    if-eqz v17, :cond_3

    .line 350
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/thememanager/util/ApplyThemeTask;->mProgress:Landroid/app/ProgressDialog;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/thememanager/util/ApplyThemeTask;->mProgress:Landroid/app/ProgressDialog;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/app/ProgressDialog;->getProgress()I

    move-result v18

    add-int/lit8 v18, v18, 0x1

    invoke-virtual/range {v17 .. v18}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 353
    :cond_3
    invoke-virtual {v15}, Lmiui/resourcebrowser/model/RelatedResource;->getResourceCode()Ljava/lang/String;

    move-result-object v4

    .line 354
    .local v4, code:Ljava/lang/String;
    invoke-static {v4}, Lcom/android/thememanager/util/ConstantsHelper;->getResourceTypeByCode(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v13

    .line 356
    .local v13, resourceType:Ljava/lang/Long;
    const/4 v10, 0x0

    .line 357
    .local v10, identity:Ljava/lang/String;
    if-eqz v13, :cond_4

    .line 358
    invoke-virtual {v13}, Ljava/lang/Long;->longValue()J

    move-result-wide v17

    invoke-static/range {v17 .. v18}, Lcom/android/thememanager/util/ConstantsHelper;->getResourceIdentity(J)Ljava/lang/String;

    move-result-object v10

    .line 360
    :cond_4
    if-nez v10, :cond_5

    .line 361
    move-object v10, v4

    .line 364
    :cond_5
    if-eqz v13, :cond_6

    invoke-virtual {v13}, Ljava/lang/Long;->longValue()J

    move-result-wide v17

    and-long v17, v17, p2

    const-wide/16 v19, 0x0

    cmp-long v17, v17, v19

    if-eqz v17, :cond_2

    :cond_6
    if-nez v13, :cond_7

    const-wide/32 v17, 0x10000000

    and-long v17, v17, p2

    const-wide/16 v19, 0x0

    cmp-long v17, v17, v19

    if-eqz v17, :cond_2

    :cond_7
    invoke-static {v10}, Lcom/android/thememanager/util/ThemeHelper;->isDisablePkgName(Ljava/lang/String;)Z

    move-result v17

    if-nez v17, :cond_2

    .line 370
    const/4 v14, 0x0

    .line 371
    .local v14, runtimePath:Ljava/lang/String;
    if-eqz v13, :cond_8

    .line 372
    invoke-virtual {v13}, Ljava/lang/Long;->longValue()J

    move-result-wide v17

    invoke-static/range {v17 .. v18}, Lcom/android/thememanager/util/ConstantsHelper;->getResourceRuntimePath(J)Ljava/lang/String;

    move-result-object v14

    .line 374
    :cond_8
    if-nez v14, :cond_9

    .line 375
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "/data/system/theme/"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 378
    :cond_9
    invoke-virtual {v15}, Lmiui/resourcebrowser/model/RelatedResource;->getFilePath()Ljava/lang/String;

    move-result-object v11

    .line 379
    .local v11, path:Ljava/lang/String;
    const-string v17, "wallpaper"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_d

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/thememanager/util/ApplyThemeTask;->mApplyFlags:J

    move-wide/from16 v17, v0

    const-wide/16 v19, 0x2

    and-long v17, v17, v19

    const-wide/16 v19, 0x0

    cmp-long v17, v17, v19

    if-eqz v17, :cond_d

    .line 380
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/thememanager/util/ApplyThemeTask;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-static {v0, v11}, Lcom/android/thememanager/util/ThemeHelper;->applyDeskWallpaperOfThemeFile(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 395
    .end local v3           #applyFilters:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .end local v4           #code:Ljava/lang/String;
    .end local v7           #copyThirdAppRes:Z
    .end local v9           #i$:Ljava/util/Iterator;
    .end local v10           #identity:Ljava/lang/String;
    .end local v11           #path:Ljava/lang/String;
    .end local v13           #resourceType:Ljava/lang/Long;
    .end local v14           #runtimePath:Ljava/lang/String;
    .end local v15           #sub:Lmiui/resourcebrowser/model/RelatedResource;
    .end local v16           #subResources:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/RelatedResource;>;"
    :catch_0
    move-exception v8

    .line 396
    .local v8, e:Ljava/lang/Exception;
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    .line 398
    .end local v8           #e:Ljava/lang/Exception;
    :cond_a
    return-void

    .line 329
    .restart local v3       #applyFilters:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    :cond_b
    const/4 v7, 0x0

    goto/16 :goto_0

    .line 338
    .restart local v5       #componentFlag:J
    .restart local v7       #copyThirdAppRes:Z
    .restart local v16       #subResources:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/RelatedResource;>;"
    :cond_c
    :goto_2
    const-wide/16 v17, 0x1

    sub-long v17, v5, v17

    and-long v17, v17, v5

    const-wide/16 v19, 0x0

    cmp-long v17, v17, v19

    if-eqz v17, :cond_0

    .line 339
    const-wide/16 v17, 0x1

    sub-long v17, v5, v17

    and-long v5, v5, v17

    goto :goto_2

    .line 381
    .end local v5           #componentFlag:J
    .restart local v4       #code:Ljava/lang/String;
    .restart local v9       #i$:Ljava/util/Iterator;
    .restart local v10       #identity:Ljava/lang/String;
    .restart local v11       #path:Ljava/lang/String;
    .restart local v13       #resourceType:Ljava/lang/Long;
    .restart local v14       #runtimePath:Ljava/lang/String;
    .restart local v15       #sub:Lmiui/resourcebrowser/model/RelatedResource;
    :cond_d
    :try_start_1
    const-string v17, "lockscreen"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_e

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/thememanager/util/ApplyThemeTask;->mApplyFlags:J

    move-wide/from16 v17, v0

    const-wide/16 v19, 0x4

    and-long v17, v17, v19

    const-wide/16 v19, 0x0

    cmp-long v17, v17, v19

    if-eqz v17, :cond_e

    .line 382
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/thememanager/util/ApplyThemeTask;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-static {v0, v11}, Lcom/android/thememanager/util/ThemeHelper;->applyLockWallpaperOfThemeFile(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 383
    :cond_e
    const-string v17, "ringtone"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_f

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/thememanager/util/ApplyThemeTask;->mApplyFlags:J

    move-wide/from16 v17, v0

    const-wide/16 v19, 0x100

    and-long v17, v17, v19

    const-wide/16 v19, 0x0

    cmp-long v17, v17, v19

    if-eqz v17, :cond_f

    .line 384
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/thememanager/util/ApplyThemeTask;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-static {v0, v1, v11}, Lcom/android/thememanager/util/ThemeHelper;->applyRingtone(Landroid/content/Context;ILjava/lang/String;)V

    goto/16 :goto_1

    .line 385
    :cond_f
    const-string v17, "notification"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_10

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/thememanager/util/ApplyThemeTask;->mApplyFlags:J

    move-wide/from16 v17, v0

    const-wide/16 v19, 0x200

    and-long v17, v17, v19

    const-wide/16 v19, 0x0

    cmp-long v17, v17, v19

    if-eqz v17, :cond_10

    .line 386
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/thememanager/util/ApplyThemeTask;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    const/16 v18, 0x2

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-static {v0, v1, v11}, Lcom/android/thememanager/util/ThemeHelper;->applyRingtone(Landroid/content/Context;ILjava/lang/String;)V

    goto/16 :goto_1

    .line 387
    :cond_10
    const-string v17, "alarm"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_11

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/thememanager/util/ApplyThemeTask;->mApplyFlags:J

    move-wide/from16 v17, v0

    const-wide/16 v19, 0x400

    and-long v17, v17, v19

    const-wide/16 v19, 0x0

    cmp-long v17, v17, v19

    if-eqz v17, :cond_11

    .line 388
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/thememanager/util/ApplyThemeTask;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    const/16 v18, 0x4

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-static {v0, v1, v11}, Lcom/android/thememanager/util/ThemeHelper;->applyRingtone(Landroid/content/Context;ILjava/lang/String;)V

    goto/16 :goto_1

    .line 391
    :cond_11
    new-instance v17, Ljava/io/File;

    move-object/from16 v0, v17

    invoke-direct {v0, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lmiui/os/Shell;->mkdirs(Ljava/lang/String;)Z

    .line 392
    invoke-static {v11, v14}, Lmiui/os/Shell;->copy(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1
.end method

.method private createFontFileLinkForPatchRomDevices(Ljava/lang/String;)V
    .locals 8
    .parameter "fontDirPath"

    .prologue
    .line 427
    if-eqz p1, :cond_0

    const-string v5, "ro.skia.use_data_fonts"

    const/4 v6, 0x0

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_0

    .line 428
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 429
    .local v2, files:[Ljava/io/File;
    if-eqz v2, :cond_0

    .line 430
    move-object v0, v2

    .local v0, arr$:[Ljava/io/File;
    array-length v4, v0

    .local v4, len$:I
    const/4 v3, 0x0

    .local v3, i$:I
    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v1, v0, v3

    .line 431
    .local v1, file:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "/data/fonts/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lmiui/os/Shell;->link(Ljava/lang/String;Ljava/lang/String;)Z

    .line 430
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 435
    .end local v0           #arr$:[Ljava/io/File;
    .end local v1           #file:Ljava/io/File;
    .end local v2           #files:[Ljava/io/File;
    .end local v3           #i$:I
    .end local v4           #len$:I
    :cond_0
    return-void
.end method

.method private delete(Ljava/util/List;Z)V
    .locals 3
    .parameter
    .parameter "removeThirdAppRes"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 318
    .local p1, deleteFilters:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    if-eqz p2, :cond_0

    .line 319
    invoke-direct {p0}, Lcom/android/thememanager/util/ApplyThemeTask;->getAllThirdAppResourcePath()Ljava/util/List;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 321
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 322
    .local v0, filter:Ljava/lang/String;
    invoke-static {v0}, Lmiui/os/Shell;->remove(Ljava/lang/String;)Z

    goto :goto_0

    .line 324
    .end local v0           #filter:Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private getAllThirdAppResourcePath()Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 294
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 295
    .local v5, list:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    new-instance v7, Ljava/io/File;

    const-string v8, "/data/system/theme/"

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v0

    .local v0, arr$:[Ljava/lang/String;
    array-length v4, v0

    .local v4, len$:I
    const/4 v2, 0x0

    .local v2, i$:I
    :goto_0
    if-ge v2, v4, :cond_4

    aget-object v6, v0, v2

    .line 296
    .local v6, path:Ljava/lang/String;
    const-string v7, "preview"

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    const-string v7, "description.xml"

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    const-string v7, "fonts"

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    const-string v7, "lock_wallpaper"

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 295
    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 302
    :cond_1
    const/4 v3, 0x1

    .line 303
    .local v3, isThirdApp:Z
    const/4 v1, 0x0

    .local v1, i:I
    :goto_2
    sget-object v7, Lcom/android/thememanager/util/ApplyThemeTask;->COMPONENT_IDENTITIES:[Ljava/lang/String;

    array-length v7, v7

    if-ge v1, v7, :cond_2

    .line 304
    sget-object v7, Lcom/android/thememanager/util/ApplyThemeTask;->COMPONENT_IDENTITIES:[Ljava/lang/String;

    aget-object v7, v7, v1

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 305
    const/4 v3, 0x0

    .line 309
    :cond_2
    if-eqz v3, :cond_0

    .line 310
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "/data/system/theme/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 303
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 314
    .end local v1           #i:I
    .end local v3           #isThirdApp:Z
    .end local v6           #path:Ljava/lang/String;
    :cond_4
    return-object v5
.end method

.method private getComponentFilterPath(J)Ljava/util/List;
    .locals 12
    .parameter "flags"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const-wide/16 v10, 0x1

    const-wide/16 v8, 0x0

    .line 261
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 262
    .local v2, filters:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const-wide/16 v4, -0x1

    cmp-long v4, p1, v4

    if-eqz v4, :cond_6

    const-wide/16 v4, -0x701

    cmp-long v4, p1, v4

    if-eqz v4, :cond_6

    .line 263
    const/4 v3, 0x0

    .local v3, i:I
    :goto_0
    sget v4, Lcom/android/thememanager/util/ThemeHelper;->THEME_FLAG_COUNT:I

    if-ge v3, v4, :cond_3

    .line 264
    shl-long v0, v10, v3

    .line 265
    .local v0, component:J
    and-long v4, p1, v0

    cmp-long v4, v4, v8

    if-nez v4, :cond_1

    .line 263
    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 269
    :cond_1
    sget-object v4, Lcom/android/thememanager/util/ApplyThemeTask;->RUNTIME_PATHS:[Ljava/lang/String;

    aget-object v4, v4, v3

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 270
    const-wide/16 v4, 0x4000

    cmp-long v4, v0, v4

    if-nez v4, :cond_2

    .line 271
    const-string v4, "/data/system/theme/com.android.launcher"

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 274
    :cond_2
    sget-object v4, Lcom/android/thememanager/util/ApplyThemeTask;->COMPONENT_PREVIEW_PREFIX:[Ljava/lang/String;

    aget-object v4, v4, v3

    if-eqz v4, :cond_0

    .line 275
    const-string v4, "%s%s/%s"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "/data/system/theme/"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "preview"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    sget-object v7, Lcom/android/thememanager/util/ApplyThemeTask;->COMPONENT_PREVIEW_PREFIX:[Ljava/lang/String;

    aget-object v7, v7, v3

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 279
    .end local v0           #component:J
    :cond_3
    const-wide/32 v4, 0x40010

    and-long/2addr v4, p1

    cmp-long v4, v4, v8

    if-eqz v4, :cond_4

    .line 280
    const-string v4, "/data/system/theme/fonts"

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 282
    :cond_4
    and-long v4, p1, v10

    cmp-long v4, v4, v8

    if-eqz v4, :cond_5

    .line 283
    const-string v4, "/data/system/theme/description.xml"

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 290
    .end local v3           #i:I
    :cond_5
    :goto_2
    return-object v2

    .line 286
    :cond_6
    const-string v4, "/data/system/theme/*"

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 287
    const-string v4, "/data/local/bootanimation.zip"

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method private sendThemeConfigurationChangeMsg(J)V
    .locals 4
    .parameter "changeFlag"

    .prologue
    .line 170
    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-eqz v2, :cond_0

    .line 172
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/app/IActivityManager;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 173
    .local v0, curConfig:Landroid/content/res/Configuration;
    iget-object v2, v0, Landroid/content/res/Configuration;->extraConfig:Lmiui/content/res/ExtraConfiguration;

    invoke-virtual {v2, p1, p2}, Lmiui/content/res/ExtraConfiguration;->updateTheme(J)V

    .line 174
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v2

    invoke-interface {v2, v0}, Landroid/app/IActivityManager;->updateConfiguration(Landroid/content/res/Configuration;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 179
    .end local v0           #curConfig:Landroid/content/res/Configuration;
    :cond_0
    :goto_0
    return-void

    .line 175
    :catch_0
    move-exception v1

    .line 176
    .local v1, e:Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"

    .prologue
    .line 39
    check-cast p1, [Ljava/lang/Void;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/android/thememanager/util/ApplyThemeTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 7
    .parameter "object"

    .prologue
    .line 93
    :try_start_0
    iget-object v0, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v6

    .line 94
    .local v6, v:Landroid/view/View;
    :goto_0
    invoke-virtual {v6}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    if-nez v0, :cond_0

    .line 95
    const-wide/16 v0, 0xa

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    goto :goto_0

    .line 99
    .end local v6           #v:Landroid/view/View;
    :catch_0
    move-exception v0

    .line 101
    :goto_1
    const/4 v0, 0x0

    return-object v0

    .line 97
    .restart local v6       #v:Landroid/view/View;
    :cond_0
    iget-object v1, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mResource:Lmiui/resourcebrowser/model/Resource;

    iget-wide v2, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mRemoveFlags:J

    iget-wide v4, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mApplyFlags:J

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/thememanager/util/ApplyThemeTask;->apply(Lmiui/resourcebrowser/model/Resource;JJ)V

    .line 98
    iget-object v0, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mProgress:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->getProgress()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgress(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 39
    check-cast p1, Ljava/lang/Void;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/android/thememanager/util/ApplyThemeTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 13
    .parameter "object"

    .prologue
    .line 106
    iget-object v0, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 108
    iget-object v1, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mResource:Lmiui/resourcebrowser/model/Resource;

    iget-wide v2, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mRemoveFlags:J

    iget-wide v4, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mApplyFlags:J

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/android/thememanager/util/ApplyThemeTask;->saveUserPreference(Lmiui/resourcebrowser/model/Resource;JJ)V

    .line 110
    iget-wide v0, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mRemoveFlags:J

    const-wide/32 v2, 0x10000

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 111
    const-string v10, "clock_changed_time_"

    .line 112
    .local v10, prefix:Ljava/lang/String;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 113
    .local v8, currentTime:J
    iget-object v0, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "1x2"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v8, v9}, Landroid/provider/Settings$System;->putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    .line 114
    iget-object v0, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "2x2"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v8, v9}, Landroid/provider/Settings$System;->putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    .line 115
    iget-object v0, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "2x4"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v8, v9}, Landroid/provider/Settings$System;->putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    .line 116
    iget-object v0, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "4x4"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v8, v9}, Landroid/provider/Settings$System;->putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    .line 119
    .end local v8           #currentTime:J
    .end local v10           #prefix:Ljava/lang/String;
    :cond_0
    iget-wide v6, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mApplyFlags:J

    .line 120
    .local v6, changeFlag:J
    iget-wide v0, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mApplyFlags:J

    const-wide/16 v2, 0x2000

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    iget-wide v0, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mRemoveFlags:J

    const-wide/16 v2, 0x1000

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 121
    invoke-direct {p0}, Lcom/android/thememanager/util/ApplyThemeTask;->containsLockscreenThemeValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 122
    const-wide/16 v0, 0x2000

    or-long/2addr v6, v0

    .line 125
    :cond_1
    const-wide/32 v0, 0x30000

    and-long/2addr v0, v6

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    .line 126
    const-wide/16 v0, 0x4000

    or-long/2addr v6, v0

    .line 128
    :cond_2
    const-wide/32 v0, 0x40010

    and-long/2addr v0, v6

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_3

    .line 129
    const-string v0, "root"

    const-string v1, "setprop debug.skia.free_cache %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/16 v11, 0x3e8

    div-long/2addr v4, v11

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lmiui/util/CommandLineUtils;->run(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)Z

    .line 131
    :cond_3
    const-wide/32 v0, 0x10047899

    and-long/2addr v6, v0

    .line 132
    invoke-direct {p0, v6, v7}, Lcom/android/thememanager/util/ApplyThemeTask;->sendThemeConfigurationChangeMsg(J)V

    .line 134
    iget-object v0, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mContext:Landroid/content/Context;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/thememanager/util/ThemeHelper;->showThemeChangedToast(Landroid/content/Context;Z)V

    .line 136
    iget-object v0, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mFinishRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_4

    .line 137
    iget-object v0, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mFinishRunnable:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 139
    :cond_4
    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    .line 82
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mProgress:Landroid/app/ProgressDialog;

    .line 83
    iget-object v0, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mProgress:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 84
    iget-object v0, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mProgress:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mContext:Landroid/content/Context;

    const v2, 0x7f0b0002

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 85
    iget-object v0, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mProgress:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 86
    iget-object v0, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 87
    iget-object v0, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mProgress:Landroid/app/ProgressDialog;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressNumberFormat(Ljava/lang/String;)V

    .line 88
    return-void
.end method

.method public saveUserPreference(Lmiui/resourcebrowser/model/Resource;JJ)V
    .locals 10
    .parameter "resource"
    .parameter "removeFlags"
    .parameter "applyFlags"

    .prologue
    .line 142
    iget-wide v6, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mModuleFlags:J

    const-wide/16 v8, -0x1

    cmp-long v6, v6, v8

    if-nez v6, :cond_0

    invoke-static {}, Lcom/android/thememanager/util/ThemeHelper;->getAllComponentsCombineFlag()J

    move-result-wide v2

    .line 144
    .local v2, flags:J
    :goto_0
    and-long v4, p4, v2

    .line 145
    .local v4, saveFlags:J
    move-wide v0, p2

    .line 146
    .local v0, clearFlags:J
    invoke-static {p1, v4, v5, v0, v1}, Lcom/android/thememanager/util/ThemeHelper;->saveUserPreference(Lmiui/resourcebrowser/model/Resource;JJ)V

    .line 147
    return-void

    .line 142
    .end local v0           #clearFlags:J
    .end local v2           #flags:J
    .end local v4           #saveFlags:J
    :cond_0
    iget-wide v2, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mModuleFlags:J

    goto :goto_0
.end method

.method public setPostRunnable(Ljava/lang/Runnable;)V
    .locals 0
    .parameter "postRunnable"

    .prologue
    .line 77
    iput-object p1, p0, Lcom/android/thememanager/util/ApplyThemeTask;->mFinishRunnable:Ljava/lang/Runnable;

    .line 78
    return-void
.end method
