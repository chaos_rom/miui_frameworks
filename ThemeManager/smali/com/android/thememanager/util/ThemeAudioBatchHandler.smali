.class public Lcom/android/thememanager/util/ThemeAudioBatchHandler;
.super Lmiui/resourcebrowser/util/AudioBatchResourceHandler;
.source "ThemeAudioBatchHandler.java"


# instance fields
.field private mResourceType:J


# direct methods
.method public constructor <init>(Lmiui/resourcebrowser/activity/ResourceListFragment;Lmiui/resourcebrowser/activity/ResourceAdapter;Lmiui/resourcebrowser/ResourceContext;J)V
    .locals 0
    .parameter "fragment"
    .parameter "adapter"
    .parameter "resContext"
    .parameter "resourceType"

    .prologue
    .line 17
    invoke-direct {p0, p1, p2, p3}, Lmiui/resourcebrowser/util/AudioBatchResourceHandler;-><init>(Lmiui/resourcebrowser/activity/ResourceListFragment;Lmiui/resourcebrowser/activity/ResourceAdapter;Lmiui/resourcebrowser/ResourceContext;)V

    .line 18
    iput-wide p4, p0, Lcom/android/thememanager/util/ThemeAudioBatchHandler;->mResourceType:J

    .line 19
    return-void
.end method


# virtual methods
.method protected handleApplyEvent(Lmiui/resourcebrowser/model/Resource;)V
    .locals 3
    .parameter "r"

    .prologue
    .line 23
    new-instance v0, Lcom/android/thememanager/util/ThemeApplyParameters;

    invoke-direct {v0}, Lcom/android/thememanager/util/ThemeApplyParameters;-><init>()V

    .line 24
    .local v0, params:Lcom/android/thememanager/util/ThemeApplyParameters;
    iget-wide v1, p0, Lcom/android/thememanager/util/ThemeAudioBatchHandler;->mResourceType:J

    iput-wide v1, v0, Lcom/android/thememanager/util/ThemeApplyParameters;->applyFlags:J

    .line 25
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/android/thememanager/util/ThemeApplyParameters;->removeOthers:Z

    .line 26
    iget-object v1, p0, Lcom/android/thememanager/util/ThemeAudioBatchHandler;->mActivity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/android/thememanager/util/ThemeAudioBatchHandler;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-static {v1, v2, p1, v0}, Lcom/android/thememanager/util/ThemeApplyUtils;->applyResource(Landroid/app/Activity;Lmiui/resourcebrowser/ResourceContext;Lmiui/resourcebrowser/model/Resource;Lcom/android/thememanager/util/ThemeApplyParameters;)V

    .line 27
    return-void
.end method
