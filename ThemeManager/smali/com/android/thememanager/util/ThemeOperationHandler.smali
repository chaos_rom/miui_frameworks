.class public Lcom/android/thememanager/util/ThemeOperationHandler;
.super Lmiui/resourcebrowser/view/ResourceOperationHandler;
.source "ThemeOperationHandler.java"


# instance fields
.field private mApplyParams:Lcom/android/thememanager/util/ThemeApplyParameters;

.field private mXiaomiAnalytics:Lmiui/analytics/XiaomiAnalytics;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lmiui/resourcebrowser/ResourceContext;Lmiui/resourcebrowser/view/ResourceOperationView;)V
    .locals 1
    .parameter "context"
    .parameter "resContext"
    .parameter "view"

    .prologue
    .line 22
    invoke-direct {p0, p1, p2, p3}, Lmiui/resourcebrowser/view/ResourceOperationHandler;-><init>(Landroid/content/Context;Lmiui/resourcebrowser/ResourceContext;Lmiui/resourcebrowser/view/ResourceOperationView;)V

    .line 23
    invoke-static {}, Lmiui/analytics/XiaomiAnalytics;->getInstance()Lmiui/analytics/XiaomiAnalytics;

    move-result-object v0

    iput-object v0, p0, Lcom/android/thememanager/util/ThemeOperationHandler;->mXiaomiAnalytics:Lmiui/analytics/XiaomiAnalytics;

    .line 24
    return-void
.end method

.method private dataAnalyticsOnApply()V
    .locals 9

    .prologue
    .line 62
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 63
    .local v2, eventParamMap:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v6, p0, Lcom/android/thememanager/util/ThemeOperationHandler;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-static {v6}, Lcom/android/thememanager/util/ThemeHelper;->getResourceId(Lmiui/resourcebrowser/model/Resource;)Ljava/lang/String;

    move-result-object v3

    .line 64
    .local v3, themeId:Ljava/lang/String;
    iget-object v6, p0, Lcom/android/thememanager/util/ThemeOperationHandler;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v6}, Lmiui/resourcebrowser/model/Resource;->getTitle()Ljava/lang/String;

    move-result-object v4

    .line 65
    .local v4, themeName:Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/thememanager/util/ThemeOperationHandler;->mApplyParams:Lcom/android/thememanager/util/ThemeApplyParameters;

    iget-wide v7, v7, Lcom/android/thememanager/util/ThemeApplyParameters;->applyFlags:J

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 66
    .local v1, applyModules:Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/thememanager/util/ThemeOperationHandler;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v7}, Lmiui/resourcebrowser/ResourceContext;->getResourceCode()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/android/thememanager/util/ConstantsHelper;->getResourceTypeByCode(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 68
    .local v0, applyEntry:Ljava/lang/String;
    const-string v6, "theme_id"

    invoke-static {v3}, Lcom/android/thememanager/util/ThemeHelper;->avoidNullString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v2, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    const-string v6, "theme_name"

    invoke-static {v4}, Lcom/android/thememanager/util/ThemeHelper;->avoidNullString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v2, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    const-string v6, "apply_modules"

    invoke-static {v1}, Lcom/android/thememanager/util/ThemeHelper;->avoidNullString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v2, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    const-string v6, "apply_entry"

    invoke-static {v0}, Lcom/android/thememanager/util/ThemeHelper;->avoidNullString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v2, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    invoke-static {}, Lmiui/analytics/XiaomiAnalytics;->getInstance()Lmiui/analytics/XiaomiAnalytics;

    move-result-object v5

    .line 73
    .local v5, xiaomiAnalytics:Lmiui/analytics/XiaomiAnalytics;
    iget-object v6, p0, Lcom/android/thememanager/util/ThemeOperationHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v6}, Lmiui/analytics/XiaomiAnalytics;->startSession(Landroid/content/Context;)V

    .line 74
    const-string v6, "apply_theme"

    invoke-virtual {v5, v6, v2}, Lmiui/analytics/XiaomiAnalytics;->trackEvent(Ljava/lang/String;Ljava/util/Map;)V

    .line 75
    invoke-virtual {v5}, Lmiui/analytics/XiaomiAnalytics;->endSession()V

    .line 76
    return-void
.end method

.method private dataAnalyticsOnDownload()V
    .locals 4

    .prologue
    .line 79
    iget-object v2, p0, Lcom/android/thememanager/util/ThemeOperationHandler;->mResource:Lmiui/resourcebrowser/model/Resource;

    if-eqz v2, :cond_0

    .line 80
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 81
    .local v0, eventParamMap:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/android/thememanager/util/ThemeOperationHandler;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-static {v2}, Lcom/android/thememanager/util/ThemeHelper;->getResourceId(Lmiui/resourcebrowser/model/Resource;)Ljava/lang/String;

    move-result-object v1

    .line 82
    .local v1, themeId:Ljava/lang/String;
    const-string v2, "theme_id"

    invoke-static {v1}, Lcom/android/thememanager/util/ThemeHelper;->avoidNullString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    const-string v2, "theme_name"

    iget-object v3, p0, Lcom/android/thememanager/util/ThemeOperationHandler;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v3}, Lmiui/resourcebrowser/model/Resource;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/thememanager/util/ThemeHelper;->avoidNullString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    iget-object v2, p0, Lcom/android/thememanager/util/ThemeOperationHandler;->mXiaomiAnalytics:Lmiui/analytics/XiaomiAnalytics;

    iget-object v3, p0, Lcom/android/thememanager/util/ThemeOperationHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lmiui/analytics/XiaomiAnalytics;->startSession(Landroid/content/Context;)V

    .line 85
    iget-object v2, p0, Lcom/android/thememanager/util/ThemeOperationHandler;->mXiaomiAnalytics:Lmiui/analytics/XiaomiAnalytics;

    const-string v3, "download_request"

    invoke-virtual {v2, v3, v0}, Lmiui/analytics/XiaomiAnalytics;->trackEvent(Ljava/lang/String;Ljava/util/Map;)V

    .line 86
    iget-object v2, p0, Lcom/android/thememanager/util/ThemeOperationHandler;->mXiaomiAnalytics:Lmiui/analytics/XiaomiAnalytics;

    invoke-virtual {v2}, Lmiui/analytics/XiaomiAnalytics;->endSession()V

    .line 88
    .end local v0           #eventParamMap:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v1           #themeId:Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private dataAnalyticsOnDownloadSuccessful(Ljava/lang/String;)V
    .locals 3
    .parameter "onlineId"

    .prologue
    .line 91
    iget-object v1, p0, Lcom/android/thememanager/util/ThemeOperationHandler;->mResource:Lmiui/resourcebrowser/model/Resource;

    if-eqz v1, :cond_0

    .line 92
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 93
    .local v0, eventParamMap:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "theme_id"

    invoke-static {p1}, Lcom/android/thememanager/util/ThemeHelper;->avoidNullString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    iget-object v1, p0, Lcom/android/thememanager/util/ThemeOperationHandler;->mXiaomiAnalytics:Lmiui/analytics/XiaomiAnalytics;

    iget-object v2, p0, Lcom/android/thememanager/util/ThemeOperationHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lmiui/analytics/XiaomiAnalytics;->startSession(Landroid/content/Context;)V

    .line 95
    iget-object v1, p0, Lcom/android/thememanager/util/ThemeOperationHandler;->mXiaomiAnalytics:Lmiui/analytics/XiaomiAnalytics;

    const-string v2, "download_request_finish"

    invoke-virtual {v1, v2, v0}, Lmiui/analytics/XiaomiAnalytics;->trackEvent(Ljava/lang/String;Ljava/util/Map;)V

    .line 96
    iget-object v1, p0, Lcom/android/thememanager/util/ThemeOperationHandler;->mXiaomiAnalytics:Lmiui/analytics/XiaomiAnalytics;

    invoke-virtual {v1}, Lmiui/analytics/XiaomiAnalytics;->endSession()V

    .line 98
    .end local v0           #eventParamMap:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_0
    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 52
    const/16 v0, 0x7001

    if-eq p1, v0, :cond_0

    const/16 v0, 0x7002

    if-ne p1, v0, :cond_1

    .line 54
    :cond_0
    iget-object v0, p0, Lcom/android/thememanager/util/ThemeOperationHandler;->mContext:Landroid/content/Context;

    invoke-static {v0, p1, p2}, Lcom/android/thememanager/util/WallpaperUtils;->dealCropWallpaperResult(Landroid/content/Context;II)V

    .line 58
    :goto_0
    return-void

    .line 56
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lmiui/resourcebrowser/view/ResourceOperationHandler;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public onDownloadEventPerformed()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/android/thememanager/util/ThemeOperationHandler;->dataAnalyticsOnDownload()V

    .line 41
    invoke-super {p0}, Lmiui/resourcebrowser/view/ResourceOperationHandler;->onDownloadEventPerformed()V

    .line 42
    return-void
.end method

.method public onDownloadSuccessful(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .parameter "path"
    .parameter "onlineId"

    .prologue
    .line 46
    invoke-direct {p0, p2}, Lcom/android/thememanager/util/ThemeOperationHandler;->dataAnalyticsOnDownloadSuccessful(Ljava/lang/String;)V

    .line 47
    invoke-super {p0, p1, p2}, Lmiui/resourcebrowser/view/ResourceOperationHandler;->onDownloadSuccessful(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    return-void
.end method

.method protected onRealApplyResourceEvent()V
    .locals 4

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/android/thememanager/util/ThemeOperationHandler;->isLegal()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    iget-object v0, p0, Lcom/android/thememanager/util/ThemeOperationHandler;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    iget-object v1, p0, Lcom/android/thememanager/util/ThemeOperationHandler;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    iget-object v2, p0, Lcom/android/thememanager/util/ThemeOperationHandler;->mResource:Lmiui/resourcebrowser/model/Resource;

    iget-object v3, p0, Lcom/android/thememanager/util/ThemeOperationHandler;->mApplyParams:Lcom/android/thememanager/util/ThemeApplyParameters;

    invoke-static {v0, v1, v2, v3}, Lcom/android/thememanager/util/ThemeApplyUtils;->applyResource(Landroid/app/Activity;Lmiui/resourcebrowser/ResourceContext;Lmiui/resourcebrowser/model/Resource;Lcom/android/thememanager/util/ThemeApplyParameters;)V

    .line 34
    invoke-direct {p0}, Lcom/android/thememanager/util/ThemeOperationHandler;->dataAnalyticsOnApply()V

    .line 36
    :cond_0
    return-void
.end method

.method public setApplyParameters(Lcom/android/thememanager/util/ThemeApplyParameters;)V
    .locals 0
    .parameter "params"

    .prologue
    .line 27
    iput-object p1, p0, Lcom/android/thememanager/util/ThemeOperationHandler;->mApplyParams:Lcom/android/thememanager/util/ThemeApplyParameters;

    .line 28
    return-void
.end method
