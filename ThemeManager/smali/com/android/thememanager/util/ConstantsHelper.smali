.class public Lcom/android/thememanager/util/ConstantsHelper;
.super Ljava/lang/Object;
.source "ConstantsHelper.java"

# interfaces
.implements Lcom/android/thememanager/ThemeResourceConstants;


# static fields
.field private static sCodeTypeMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static sIdentityTypeMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .prologue
    const-wide/16 v12, 0x10

    const-wide/16 v10, 0x8

    const-wide/16 v8, 0x4

    const-wide/16 v6, 0x2

    const-wide/16 v4, 0x1

    .line 14
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/thememanager/util/ConstantsHelper;->sCodeTypeMap:Ljava/util/Map;

    .line 15
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/thememanager/util/ConstantsHelper;->sIdentityTypeMap:Ljava/util/Map;

    .line 18
    sget-object v0, Lcom/android/thememanager/util/ConstantsHelper;->sCodeTypeMap:Ljava/util/Map;

    const-string v1, "framework"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 19
    sget-object v0, Lcom/android/thememanager/util/ConstantsHelper;->sCodeTypeMap:Ljava/util/Map;

    const-string v1, "bootanimation"

    const-wide/16 v2, 0x20

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 20
    sget-object v0, Lcom/android/thememanager/util/ConstantsHelper;->sCodeTypeMap:Ljava/util/Map;

    const-string v1, "bootaudio"

    const-wide/16 v2, 0x40

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    sget-object v0, Lcom/android/thememanager/util/ConstantsHelper;->sCodeTypeMap:Ljava/util/Map;

    const-string v1, "ringtone"

    const-wide/16 v2, 0x100

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    sget-object v0, Lcom/android/thememanager/util/ConstantsHelper;->sCodeTypeMap:Ljava/util/Map;

    const-string v1, "notification"

    const-wide/16 v2, 0x200

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 23
    sget-object v0, Lcom/android/thememanager/util/ConstantsHelper;->sCodeTypeMap:Ljava/util/Map;

    const-string v1, "alarm"

    const-wide/16 v2, 0x400

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    sget-object v0, Lcom/android/thememanager/util/ConstantsHelper;->sCodeTypeMap:Ljava/util/Map;

    const-string v1, "wallpaper"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 25
    sget-object v0, Lcom/android/thememanager/util/ConstantsHelper;->sCodeTypeMap:Ljava/util/Map;

    const-string v1, "lockscreen"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    sget-object v0, Lcom/android/thememanager/util/ConstantsHelper;->sCodeTypeMap:Ljava/util/Map;

    const-string v1, "lockstyle"

    const-wide/16 v2, 0x1000

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    sget-object v0, Lcom/android/thememanager/util/ConstantsHelper;->sCodeTypeMap:Ljava/util/Map;

    const-string v1, "fonts"

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    sget-object v0, Lcom/android/thememanager/util/ConstantsHelper;->sCodeTypeMap:Ljava/util/Map;

    const-string v1, "fonts_fallback"

    const-wide/32 v2, 0x40000

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    sget-object v0, Lcom/android/thememanager/util/ConstantsHelper;->sCodeTypeMap:Ljava/util/Map;

    const-string v1, "icons"

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    sget-object v0, Lcom/android/thememanager/util/ConstantsHelper;->sCodeTypeMap:Ljava/util/Map;

    const-string v1, "launcher"

    const-wide/16 v2, 0x4000

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    sget-object v0, Lcom/android/thememanager/util/ConstantsHelper;->sCodeTypeMap:Ljava/util/Map;

    const-string v1, "statusbar"

    const-wide/16 v2, 0x2000

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    sget-object v0, Lcom/android/thememanager/util/ConstantsHelper;->sCodeTypeMap:Ljava/util/Map;

    const-string v1, "contact"

    const-wide/16 v2, 0x800

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    sget-object v0, Lcom/android/thememanager/util/ConstantsHelper;->sCodeTypeMap:Ljava/util/Map;

    const-string v1, "mms"

    const-wide/16 v2, 0x80

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    sget-object v0, Lcom/android/thememanager/util/ConstantsHelper;->sCodeTypeMap:Ljava/util/Map;

    const-string v1, "audioeffect"

    const-wide/32 v2, 0x8000

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    sget-object v0, Lcom/android/thememanager/util/ConstantsHelper;->sCodeTypeMap:Ljava/util/Map;

    const-string v1, "clock"

    const-wide/32 v2, 0x10000

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    sget-object v0, Lcom/android/thememanager/util/ConstantsHelper;->sCodeTypeMap:Ljava/util/Map;

    const-string v1, "photoframe"

    const-wide/32 v2, 0x20000

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    sget-object v0, Lcom/android/thememanager/util/ConstantsHelper;->sCodeTypeMap:Ljava/util/Map;

    const-string v1, "theme"

    const-wide/16 v2, -0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    sget-object v0, Lcom/android/thememanager/util/ConstantsHelper;->sIdentityTypeMap:Ljava/util/Map;

    const-string v1, "framework-res"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    sget-object v0, Lcom/android/thememanager/util/ConstantsHelper;->sIdentityTypeMap:Ljava/util/Map;

    const-string v1, "boots/bootanimation.zip"

    const-wide/16 v2, 0x20

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    sget-object v0, Lcom/android/thememanager/util/ConstantsHelper;->sIdentityTypeMap:Ljava/util/Map;

    const-string v1, "boots/bootaudio.mp3"

    const-wide/16 v2, 0x40

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    sget-object v0, Lcom/android/thememanager/util/ConstantsHelper;->sIdentityTypeMap:Ljava/util/Map;

    const-string v1, "ringtones/ringtone.mp3"

    const-wide/16 v2, 0x100

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    sget-object v0, Lcom/android/thememanager/util/ConstantsHelper;->sIdentityTypeMap:Ljava/util/Map;

    const-string v1, "ringtones/notification.mp3"

    const-wide/16 v2, 0x200

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    sget-object v0, Lcom/android/thememanager/util/ConstantsHelper;->sIdentityTypeMap:Ljava/util/Map;

    const-string v1, "ringtones/alarm.mp3"

    const-wide/16 v2, 0x400

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    sget-object v0, Lcom/android/thememanager/util/ConstantsHelper;->sIdentityTypeMap:Ljava/util/Map;

    const-string v1, "wallpaper/default_wallpaper.jpg"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    sget-object v0, Lcom/android/thememanager/util/ConstantsHelper;->sIdentityTypeMap:Ljava/util/Map;

    const-string v1, "wallpaper/default_lock_wallpaper.jpg"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    sget-object v0, Lcom/android/thememanager/util/ConstantsHelper;->sIdentityTypeMap:Ljava/util/Map;

    const-string v1, "lockscreen"

    const-wide/16 v2, 0x1000

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    sget-object v0, Lcom/android/thememanager/util/ConstantsHelper;->sIdentityTypeMap:Ljava/util/Map;

    const-string v1, "fonts/Roboto-Regular.ttf"

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    sget-object v0, Lcom/android/thememanager/util/ConstantsHelper;->sIdentityTypeMap:Ljava/util/Map;

    const-string v1, "fonts/DroidSansFallback.ttf"

    const-wide/32 v2, 0x40000

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    sget-object v0, Lcom/android/thememanager/util/ConstantsHelper;->sIdentityTypeMap:Ljava/util/Map;

    const-string v1, "icons"

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    sget-object v0, Lcom/android/thememanager/util/ConstantsHelper;->sIdentityTypeMap:Ljava/util/Map;

    const-string v1, "com.miui.home"

    const-wide/16 v2, 0x4000

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    sget-object v0, Lcom/android/thememanager/util/ConstantsHelper;->sIdentityTypeMap:Ljava/util/Map;

    const-string v1, "com.android.systemui"

    const-wide/16 v2, 0x2000

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    sget-object v0, Lcom/android/thememanager/util/ConstantsHelper;->sIdentityTypeMap:Ljava/util/Map;

    const-string v1, "com.android.contacts"

    const-wide/16 v2, 0x800

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    sget-object v0, Lcom/android/thememanager/util/ConstantsHelper;->sIdentityTypeMap:Ljava/util/Map;

    const-string v1, "com.android.mms"

    const-wide/16 v2, 0x80

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    sget-object v0, Lcom/android/thememanager/util/ConstantsHelper;->sIdentityTypeMap:Ljava/util/Map;

    const-string v1, "audioeffect"

    const-wide/32 v2, 0x8000

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    sget-object v0, Lcom/android/thememanager/util/ConstantsHelper;->sIdentityTypeMap:Ljava/util/Map;

    const-string v1, "clock_"

    const-wide/32 v2, 0x10000

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    sget-object v0, Lcom/android/thememanager/util/ConstantsHelper;->sIdentityTypeMap:Ljava/util/Map;

    const-string v1, "photoframe_"

    const-wide/32 v2, 0x20000

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 12
    return-void
.end method

.method public static getPlatform(J)I
    .locals 2
    .parameter "flag"

    .prologue
    .line 116
    const-wide/16 v0, -0x1

    cmp-long v0, p0, v0

    if-nez v0, :cond_0

    .line 117
    const/4 v0, 0x2

    .line 121
    :goto_0
    return v0

    .line 118
    :cond_0
    const-wide/16 v0, 0x1

    cmp-long v0, v0, p0

    if-gtz v0, :cond_1

    const-wide/32 v0, 0x40000

    cmp-long v0, p0, v0

    if-gtz v0, :cond_1

    .line 119
    sget-object v0, Lcom/android/thememanager/util/ConstantsHelper;->COMPONENT_PLATFORMS:[I

    invoke-static {p0, p1}, Lcom/android/thememanager/util/ThemeHelper;->getComponentIndex(J)I

    move-result v1

    aget v0, v0, v1

    goto :goto_0

    .line 121
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getPreviewPrefix(J)Ljava/lang/String;
    .locals 2
    .parameter "resourceType"

    .prologue
    .line 125
    const-wide/16 v0, -0x1

    cmp-long v0, p0, v0

    if-nez v0, :cond_0

    .line 126
    const-string v0, ""

    .line 130
    :goto_0
    return-object v0

    .line 127
    :cond_0
    const-wide/16 v0, 0x1

    cmp-long v0, v0, p0

    if-gtz v0, :cond_1

    const-wide/32 v0, 0x40000

    cmp-long v0, p0, v0

    if-gtz v0, :cond_1

    .line 128
    sget-object v0, Lcom/android/thememanager/util/ConstantsHelper;->COMPONENT_PREVIEW_PREFIX:[Ljava/lang/String;

    invoke-static {p0, p1}, Lcom/android/thememanager/util/ThemeHelper;->getComponentIndex(J)I

    move-result v1

    aget-object v0, v0, v1

    goto :goto_0

    .line 130
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method public static getResourceCode(J)Ljava/lang/String;
    .locals 2
    .parameter "flag"

    .prologue
    .line 87
    const-wide/16 v0, -0x1

    cmp-long v0, p0, v0

    if-nez v0, :cond_0

    .line 88
    const-string v0, "theme"

    .line 92
    :goto_0
    return-object v0

    .line 89
    :cond_0
    const-wide/16 v0, 0x1

    cmp-long v0, v0, p0

    if-gtz v0, :cond_1

    const-wide/32 v0, 0x40000

    cmp-long v0, p0, v0

    if-gtz v0, :cond_1

    .line 90
    sget-object v0, Lcom/android/thememanager/util/ConstantsHelper;->COMPONENT_CODES:[Ljava/lang/String;

    invoke-static {p0, p1}, Lcom/android/thememanager/util/ThemeHelper;->getComponentIndex(J)I

    move-result v1

    aget-object v0, v0, v1

    goto :goto_0

    .line 92
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method public static getResourceIdentity(J)Ljava/lang/String;
    .locals 2
    .parameter "flag"

    .prologue
    .line 78
    const-wide/16 v0, -0x1

    cmp-long v0, p0, v0

    if-nez v0, :cond_0

    .line 79
    const-string v0, ""

    .line 83
    :goto_0
    return-object v0

    .line 80
    :cond_0
    const-wide/16 v0, 0x1

    cmp-long v0, v0, p0

    if-gtz v0, :cond_1

    const-wide/32 v0, 0x40000

    cmp-long v0, p0, v0

    if-gtz v0, :cond_1

    .line 81
    sget-object v0, Lcom/android/thememanager/util/ConstantsHelper;->COMPONENT_IDENTITIES:[Ljava/lang/String;

    invoke-static {p0, p1}, Lcom/android/thememanager/util/ThemeHelper;->getComponentIndex(J)I

    move-result v1

    aget-object v0, v0, v1

    goto :goto_0

    .line 83
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method public static getResourceRuntimePath(J)Ljava/lang/String;
    .locals 2
    .parameter "flag"

    .prologue
    .line 69
    const-wide/16 v0, -0x1

    cmp-long v0, p0, v0

    if-nez v0, :cond_0

    .line 70
    const-string v0, ""

    .line 74
    :goto_0
    return-object v0

    .line 71
    :cond_0
    const-wide/16 v0, 0x1

    cmp-long v0, v0, p0

    if-gtz v0, :cond_1

    const-wide/32 v0, 0x40000

    cmp-long v0, p0, v0

    if-gtz v0, :cond_1

    .line 72
    sget-object v0, Lcom/android/thememanager/util/ConstantsHelper;->RUNTIME_PATHS:[Ljava/lang/String;

    invoke-static {p0, p1}, Lcom/android/thememanager/util/ThemeHelper;->getComponentIndex(J)I

    move-result v1

    aget-object v0, v0, v1

    goto :goto_0

    .line 74
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method public static getResourceStamp(J)Ljava/lang/String;
    .locals 2
    .parameter "flag"

    .prologue
    .line 96
    const-wide/16 v0, -0x1

    cmp-long v0, p0, v0

    if-nez v0, :cond_0

    .line 97
    const-string v0, "Compound"

    .line 101
    :goto_0
    return-object v0

    .line 98
    :cond_0
    const-wide/16 v0, 0x1

    cmp-long v0, v0, p0

    if-gtz v0, :cond_1

    const-wide/32 v0, 0x40000

    cmp-long v0, p0, v0

    if-gtz v0, :cond_1

    .line 99
    sget-object v0, Lcom/android/thememanager/util/ConstantsHelper;->COMPONENT_STAMPS:[Ljava/lang/String;

    invoke-static {p0, p1}, Lcom/android/thememanager/util/ThemeHelper;->getComponentIndex(J)I

    move-result v1

    aget-object v0, v0, v1

    goto :goto_0

    .line 101
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method public static getResourceTypeByCode(Ljava/lang/String;)Ljava/lang/Long;
    .locals 1
    .parameter "code"

    .prologue
    .line 61
    sget-object v0, Lcom/android/thememanager/util/ConstantsHelper;->sCodeTypeMap:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    return-object v0
.end method

.method public static getResourceTypeByIdentity(Ljava/lang/String;)Ljava/lang/Long;
    .locals 1
    .parameter "identity"

    .prologue
    .line 65
    sget-object v0, Lcom/android/thememanager/util/ConstantsHelper;->sIdentityTypeMap:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    return-object v0
.end method

.method public static getTitleResId(J)I
    .locals 2
    .parameter "flag"

    .prologue
    .line 105
    const-wide/16 v0, -0x1

    cmp-long v0, p0, v0

    if-nez v0, :cond_0

    .line 106
    const v0, 0x7f0b0023

    .line 112
    :goto_0
    return v0

    .line 107
    :cond_0
    const-wide/32 v0, 0x10000000

    cmp-long v0, p0, v0

    if-nez v0, :cond_1

    .line 108
    const v0, 0x7f0b0022

    goto :goto_0

    .line 109
    :cond_1
    const-wide/16 v0, 0x1

    cmp-long v0, v0, p0

    if-gtz v0, :cond_2

    const-wide/32 v0, 0x40000

    cmp-long v0, p0, v0

    if-gtz v0, :cond_2

    .line 110
    sget-object v0, Lcom/android/thememanager/util/ConstantsHelper;->COMPONENT_TITLES:[I

    invoke-static {p0, p1}, Lcom/android/thememanager/util/ThemeHelper;->getComponentIndex(J)I

    move-result v1

    aget v0, v0, v1

    goto :goto_0

    .line 112
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
