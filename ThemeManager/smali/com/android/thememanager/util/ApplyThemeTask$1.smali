.class Lcom/android/thememanager/util/ApplyThemeTask$1;
.super Ljava/lang/Object;
.source "ApplyThemeTask.java"

# interfaces
.implements Lmiui/content/res/IconCustomizer$CustomizedIconsListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/thememanager/util/ApplyThemeTask;->apply(Lmiui/resourcebrowser/model/Resource;JJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field originProgress:I

.field final synthetic this$0:Lcom/android/thememanager/util/ApplyThemeTask;

.field totalIcon:I

.field final synthetic val$iconSteps:I


# direct methods
.method constructor <init>(Lcom/android/thememanager/util/ApplyThemeTask;I)V
    .locals 1
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 217
    iput-object p1, p0, Lcom/android/thememanager/util/ApplyThemeTask$1;->this$0:Lcom/android/thememanager/util/ApplyThemeTask;

    iput p2, p0, Lcom/android/thememanager/util/ApplyThemeTask$1;->val$iconSteps:I

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 218
    iput v0, p0, Lcom/android/thememanager/util/ApplyThemeTask$1;->originProgress:I

    .line 219
    iput v0, p0, Lcom/android/thememanager/util/ApplyThemeTask$1;->totalIcon:I

    return-void
.end method


# virtual methods
.method public beforePrepareIcon(I)V
    .locals 1
    .parameter "totalTaskCnt"

    .prologue
    .line 222
    if-lez p1, :cond_0

    .end local p1
    :goto_0
    iput p1, p0, Lcom/android/thememanager/util/ApplyThemeTask$1;->totalIcon:I

    .line 223
    iget-object v0, p0, Lcom/android/thememanager/util/ApplyThemeTask$1;->this$0:Lcom/android/thememanager/util/ApplyThemeTask;

    #getter for: Lcom/android/thememanager/util/ApplyThemeTask;->mProgress:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/android/thememanager/util/ApplyThemeTask;->access$000(Lcom/android/thememanager/util/ApplyThemeTask;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->getProgress()I

    move-result v0

    iput v0, p0, Lcom/android/thememanager/util/ApplyThemeTask$1;->originProgress:I

    .line 224
    return-void

    .line 222
    .restart local p1
    :cond_0
    const/4 p1, 0x1

    goto :goto_0
.end method

.method public finishAllIcons()V
    .locals 3

    .prologue
    .line 234
    iget-object v0, p0, Lcom/android/thememanager/util/ApplyThemeTask$1;->this$0:Lcom/android/thememanager/util/ApplyThemeTask;

    #getter for: Lcom/android/thememanager/util/ApplyThemeTask;->mProgress:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/android/thememanager/util/ApplyThemeTask;->access$000(Lcom/android/thememanager/util/ApplyThemeTask;)Landroid/app/ProgressDialog;

    move-result-object v0

    iget v1, p0, Lcom/android/thememanager/util/ApplyThemeTask$1;->originProgress:I

    iget v2, p0, Lcom/android/thememanager/util/ApplyThemeTask$1;->val$iconSteps:I

    add-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 235
    return-void
.end method

.method public finishPrepareIcon(I)V
    .locals 4
    .parameter "currentTask"

    .prologue
    .line 227
    iget v1, p0, Lcom/android/thememanager/util/ApplyThemeTask$1;->originProgress:I

    iget v2, p0, Lcom/android/thememanager/util/ApplyThemeTask$1;->val$iconSteps:I

    mul-int/2addr v2, p1

    iget v3, p0, Lcom/android/thememanager/util/ApplyThemeTask$1;->totalIcon:I

    div-int/2addr v2, v3

    add-int v0, v1, v2

    .line 228
    .local v0, progress:I
    iget-object v1, p0, Lcom/android/thememanager/util/ApplyThemeTask$1;->this$0:Lcom/android/thememanager/util/ApplyThemeTask;

    #getter for: Lcom/android/thememanager/util/ApplyThemeTask;->mProgress:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/android/thememanager/util/ApplyThemeTask;->access$000(Lcom/android/thememanager/util/ApplyThemeTask;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->getProgress()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 229
    iget-object v1, p0, Lcom/android/thememanager/util/ApplyThemeTask$1;->this$0:Lcom/android/thememanager/util/ApplyThemeTask;

    #getter for: Lcom/android/thememanager/util/ApplyThemeTask;->mProgress:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/android/thememanager/util/ApplyThemeTask;->access$000(Lcom/android/thememanager/util/ApplyThemeTask;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 231
    :cond_0
    return-void
.end method
