.class final Lcom/android/thememanager/util/ThemeApplyUtils$1;
.super Ljava/lang/Object;
.source "ThemeApplyUtils.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/thememanager/util/ThemeApplyUtils;->applyResource(Landroid/app/Activity;Lmiui/resourcebrowser/ResourceContext;Lmiui/resourcebrowser/model/Resource;Lcom/android/thememanager/util/ThemeApplyParameters;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$applyFlags:J

.field final synthetic val$context:Landroid/app/Activity;

.field final synthetic val$removeFlags:J

.field final synthetic val$resContext:Lmiui/resourcebrowser/ResourceContext;

.field final synthetic val$resource:Lmiui/resourcebrowser/model/Resource;


# direct methods
.method constructor <init>(Landroid/app/Activity;Lmiui/resourcebrowser/ResourceContext;Lmiui/resourcebrowser/model/Resource;JJ)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 65
    iput-object p1, p0, Lcom/android/thememanager/util/ThemeApplyUtils$1;->val$context:Landroid/app/Activity;

    iput-object p2, p0, Lcom/android/thememanager/util/ThemeApplyUtils$1;->val$resContext:Lmiui/resourcebrowser/ResourceContext;

    iput-object p3, p0, Lcom/android/thememanager/util/ThemeApplyUtils$1;->val$resource:Lmiui/resourcebrowser/model/Resource;

    iput-wide p4, p0, Lcom/android/thememanager/util/ThemeApplyUtils$1;->val$removeFlags:J

    iput-wide p6, p0, Lcom/android/thememanager/util/ThemeApplyUtils$1;->val$applyFlags:J

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 8
    .parameter "dialog"
    .parameter "which"

    .prologue
    .line 68
    new-instance v7, Lcom/android/thememanager/util/ThemeApplyUtils$1$1;

    invoke-direct {v7, p0}, Lcom/android/thememanager/util/ThemeApplyUtils$1$1;-><init>(Lcom/android/thememanager/util/ThemeApplyUtils$1;)V

    .line 75
    .local v7, lastRun:Ljava/lang/Runnable;
    iget-object v0, p0, Lcom/android/thememanager/util/ThemeApplyUtils$1;->val$context:Landroid/app/Activity;

    iget-object v1, p0, Lcom/android/thememanager/util/ThemeApplyUtils$1;->val$resContext:Lmiui/resourcebrowser/ResourceContext;

    iget-object v2, p0, Lcom/android/thememanager/util/ThemeApplyUtils$1;->val$resource:Lmiui/resourcebrowser/model/Resource;

    iget-wide v3, p0, Lcom/android/thememanager/util/ThemeApplyUtils$1;->val$removeFlags:J

    iget-wide v5, p0, Lcom/android/thememanager/util/ThemeApplyUtils$1;->val$applyFlags:J

    #calls: Lcom/android/thememanager/util/ThemeApplyUtils;->applyTheme(Landroid/content/Context;Lmiui/resourcebrowser/ResourceContext;Lmiui/resourcebrowser/model/Resource;JJLjava/lang/Runnable;)V
    invoke-static/range {v0 .. v7}, Lcom/android/thememanager/util/ThemeApplyUtils;->access$000(Landroid/content/Context;Lmiui/resourcebrowser/ResourceContext;Lmiui/resourcebrowser/model/Resource;JJLjava/lang/Runnable;)V

    .line 76
    return-void
.end method
