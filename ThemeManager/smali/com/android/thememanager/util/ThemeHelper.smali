.class public Lcom/android/thememanager/util/ThemeHelper;
.super Ljava/lang/Object;
.source "ThemeHelper.java"

# interfaces
.implements Lcom/android/thememanager/ThemeResourceConstants;


# static fields
.field public static final GADGET_SIZE_ARRAY:[Ljava/lang/String;

.field private static final LOG2:D

.field public static final THEME_FLAG_COUNT:I

.field private static sAudioEffectMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sAudioEffectOrder:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sDisablePackagesReplaceSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sEditor:Landroid/content/SharedPreferences$Editor;

.field private static sPlatformSupportReplaceFont:Z

.field private static sSharedPreference:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 51
    const-wide/high16 v0, 0x4000

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    sput-wide v0, Lcom/android/thememanager/util/ThemeHelper;->LOG2:D

    .line 55
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "1x2"

    aput-object v2, v0, v1

    const-string v1, "2x2"

    aput-object v1, v0, v3

    const/4 v1, 0x2

    const-string v2, "2x4"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "4x4"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/thememanager/util/ThemeHelper;->GADGET_SIZE_ARRAY:[Ljava/lang/String;

    .line 62
    const-wide/32 v0, 0x40000

    invoke-static {v0, v1}, Lcom/android/thememanager/util/ThemeHelper;->getComponentIndex(J)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/android/thememanager/util/ThemeHelper;->THEME_FLAG_COUNT:I

    .line 75
    sput-boolean v3, Lcom/android/thememanager/util/ThemeHelper;->sPlatformSupportReplaceFont:Z

    .line 76
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/android/thememanager/util/ThemeHelper;->sDisablePackagesReplaceSet:Ljava/util/Set;

    .line 78
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/thememanager/util/ThemeHelper;->sAudioEffectMap:Ljava/util/Map;

    .line 79
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/thememanager/util/ThemeHelper;->sAudioEffectOrder:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static applyBootAudio(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 7
    .parameter "context"
    .parameter "path"

    .prologue
    .line 552
    move-object v1, p1

    .line 553
    .local v1, srcFilePath:Ljava/lang/String;
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 554
    new-instance v4, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v5

    const-string v6, "silent_bootaudio"

    invoke-direct {v4, v5, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 555
    .local v4, tmpEmptyFile:Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_0

    .line 556
    new-instance v5, Ljava/io/ByteArrayInputStream;

    const/4 v6, 0x0

    new-array v6, v6, [B

    invoke-direct {v5, v6}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lmiui/resourcebrowser/util/ResourceHelper;->writeTo(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;

    .line 558
    :cond_0
    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    .line 561
    .end local v4           #tmpEmptyFile:Ljava/io/File;
    :cond_1
    new-instance v2, Ljava/io/File;

    const-string v5, "/data/system/theme/boots/bootaudio.mp3"

    invoke-direct {v2, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 562
    .local v2, targetFile:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 564
    :try_start_0
    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, v1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lmiui/resourcebrowser/util/ResourceHelper;->writeTo(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 568
    :goto_0
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    .line 569
    .local v0, result:Z
    if-eqz v0, :cond_2

    .line 570
    const v5, 0x7f0b000c

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 571
    .local v3, title:Ljava/lang/String;
    const-wide/16 v5, 0x40

    invoke-static {v5, v6, p1, v3}, Lcom/android/thememanager/util/ThemeHelper;->saveUserPreference(JLjava/lang/String;Ljava/lang/String;)V

    .line 574
    .end local v3           #title:Ljava/lang/String;
    :cond_2
    return v0

    .line 565
    .end local v0           #result:Z
    :catch_0
    move-exception v5

    goto :goto_0
.end method

.method public static applyDeskWallpaperOfThemeFile(Landroid/content/Context;Ljava/lang/String;)V
    .locals 8
    .parameter "context"
    .parameter "path"

    .prologue
    .line 448
    const/4 v2, 0x0

    .line 450
    .local v2, is:Ljava/io/InputStream;
    :try_start_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 452
    .local v1, file:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 453
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .end local v2           #is:Ljava/io/InputStream;
    .local v3, is:Ljava/io/InputStream;
    move-object v2, v3

    .line 459
    .end local v3           #is:Ljava/io/InputStream;
    .restart local v2       #is:Ljava/io/InputStream;
    :goto_0
    const-string v5, "wallpaper"

    invoke-virtual {p0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/WallpaperManager;

    .line 460
    .local v4, ws:Landroid/app/WallpaperManager;
    invoke-virtual {v4, v2}, Landroid/app/WallpaperManager;->setStream(Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 463
    if-eqz v2, :cond_0

    .line 465
    :try_start_1
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 471
    .end local v1           #file:Ljava/io/File;
    .end local v4           #ws:Landroid/app/WallpaperManager;
    :cond_0
    :goto_1
    return-void

    .line 455
    .restart local v1       #file:Ljava/io/File;
    :cond_1
    :try_start_2
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x602013e

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v2

    .line 456
    const-wide/16 v5, 0x2

    const-string v7, ""

    invoke-static {v5, v6, v7}, Lcom/android/thememanager/util/ThemeHelper;->saveUserPreference(JLjava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 461
    .end local v1           #file:Ljava/io/File;
    :catch_0
    move-exception v5

    .line 463
    if-eqz v2, :cond_0

    .line 465
    :try_start_3
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 466
    :catch_1
    move-exception v0

    .line 467
    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 466
    .end local v0           #e:Ljava/io/IOException;
    .restart local v1       #file:Ljava/io/File;
    .restart local v4       #ws:Landroid/app/WallpaperManager;
    :catch_2
    move-exception v0

    .line 467
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 463
    .end local v0           #e:Ljava/io/IOException;
    .end local v1           #file:Ljava/io/File;
    .end local v4           #ws:Landroid/app/WallpaperManager;
    :catchall_0
    move-exception v5

    if-eqz v2, :cond_2

    .line 465
    :try_start_4
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 468
    :cond_2
    :goto_2
    throw v5

    .line 466
    :catch_3
    move-exception v0

    .line 467
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2
.end method

.method public static applyLockWallpaperOfThemeFile(Landroid/content/Context;Ljava/lang/String;)V
    .locals 6
    .parameter "context"
    .parameter "path"

    .prologue
    const/4 v5, -0x1

    .line 474
    new-instance v1, Ljava/io/File;

    const-string v2, "/data/system/theme/lock_wallpaper"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 476
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 477
    invoke-static {p0}, Lcom/android/thememanager/util/ThemeHelper;->getScreenDisplayMetrics(Landroid/content/Context;)Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 478
    .local v0, metrics:Landroid/util/DisplayMetrics;
    new-instance v1, Lmiui/util/InputStreamLoader;

    invoke-direct {v1, p1}, Lmiui/util/InputStreamLoader;-><init>(Ljava/lang/String;)V

    const-string v2, "/data/system/theme/lock_wallpaper"

    iget v3, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v4, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v1, v2, v3, v4}, Lmiui/util/ImageUtils;->saveBitmapToLocal(Lmiui/util/InputStreamLoader;Ljava/lang/String;II)Z

    .line 480
    const-string v1, "/data/system/theme/lock_wallpaper"

    const/16 v2, 0x1fd

    invoke-static {v1, v2, v5, v5}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    .line 484
    .end local v0           #metrics:Landroid/util/DisplayMetrics;
    :goto_0
    return-void

    .line 482
    :cond_0
    const-wide/16 v1, 0x4

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/android/thememanager/util/ThemeHelper;->saveUserPreference(JLjava/lang/String;)V

    goto :goto_0
.end method

.method public static applyRingtone(Landroid/content/Context;ILjava/lang/String;)V
    .locals 2
    .parameter "context"
    .parameter "type"
    .parameter "path"

    .prologue
    .line 511
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 512
    .local v0, file:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 513
    const/4 v1, 0x1

    if-ne p1, v1, :cond_1

    .line 514
    sget-object p2, Lcom/android/thememanager/util/ThemeHelper;->DEFAULT_RINGTONE_FILE_PATH:Ljava/lang/String;

    .line 521
    :cond_0
    :goto_0
    invoke-static {p0, p1, p2}, Lcom/android/thememanager/util/ThemeHelper;->setRingtone(Landroid/content/Context;ILjava/lang/String;)Z

    .line 522
    return-void

    .line 515
    :cond_1
    const/4 v1, 0x2

    if-ne p1, v1, :cond_2

    .line 516
    sget-object p2, Lcom/android/thememanager/util/ThemeHelper;->DEFAULT_NOTIFICATION_FILE_PATH:Ljava/lang/String;

    goto :goto_0

    .line 517
    :cond_2
    const/4 v1, 0x4

    if-ne p1, v1, :cond_0

    .line 518
    sget-object p2, Lcom/android/thememanager/util/ThemeHelper;->DEFAULT_ALARM_FILE_PATH:Ljava/lang/String;

    goto :goto_0
.end method

.method public static avoidNullString(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter "str"

    .prologue
    .line 612
    if-nez p0, :cond_0

    const-string p0, "Undefined"

    .end local p0
    :cond_0
    return-object p0
.end method

.method private static clearUserPreference(J)V
    .locals 3
    .parameter "flag"

    .prologue
    .line 427
    sget-object v0, Lcom/android/thememanager/util/ThemeHelper;->sEditor:Landroid/content/SharedPreferences$Editor;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "path-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 428
    sget-object v0, Lcom/android/thememanager/util/ThemeHelper;->sEditor:Landroid/content/SharedPreferences$Editor;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "name-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 429
    sget-object v0, Lcom/android/thememanager/util/ThemeHelper;->sEditor:Landroid/content/SharedPreferences$Editor;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "time-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 430
    return-void
.end method

.method public static createRuntimeFolder(Landroid/content/Context;)V
    .locals 4
    .parameter "context"

    .prologue
    const/4 v3, -0x1

    .line 117
    new-instance v1, Ljava/io/File;

    const-string v2, "/data/system/theme/"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 118
    const-string v1, "/data/system/theme/"

    invoke-static {v1}, Lmiui/os/Shell;->mkdirs(Ljava/lang/String;)Z

    .line 120
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget v0, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 121
    .local v0, uid:I
    const-string v1, "/data/system/theme/"

    invoke-static {v1, v0, v0}, Lmiui/os/Shell;->chown(Ljava/lang/String;II)Z

    .line 122
    new-instance v1, Ljava/io/File;

    sget-object v2, Lcom/android/thememanager/util/ThemeHelper;->DOWNLOADED_THEME_PATH:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/16 v2, 0x1ff

    invoke-static {v1, v2, v3, v3}, Lmiui/os/ExtraFileUtils;->mkdirs(Ljava/io/File;III)Z

    .line 123
    return-void
.end method

.method public static getAllComponentsCombineFlag()J
    .locals 2

    .prologue
    .line 255
    const-wide/32 v0, 0x7ffff

    return-wide v0
.end method

.method public static getCompatibleFlag(IJ)J
    .locals 4
    .parameter "platformVersion"
    .parameter "flag"

    .prologue
    .line 313
    const-wide/16 v0, 0x1

    .local v0, i:J
    :goto_0
    const-wide/32 v2, 0x40000

    cmp-long v2, v0, v2

    if-gtz v2, :cond_1

    .line 314
    invoke-static {v0, v1}, Lcom/android/thememanager/util/ConstantsHelper;->getPlatform(J)I

    move-result v2

    if-ge p0, v2, :cond_0

    .line 315
    const-wide/16 v2, -0x1

    xor-long/2addr v2, v0

    and-long/2addr p1, v2

    .line 313
    :cond_0
    const/4 v2, 0x1

    shl-long/2addr v0, v2

    goto :goto_0

    .line 318
    :cond_1
    return-wide p1
.end method

.method public static getComponentIndex(J)I
    .locals 4
    .parameter "flag"

    .prologue
    .line 235
    const-wide/16 v0, -0x1

    cmp-long v0, p0, v0

    if-nez v0, :cond_0

    .line 236
    const/4 v0, 0x0

    .line 238
    :goto_0
    return v0

    :cond_0
    long-to-double v0, p0

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    sget-wide v2, Lcom/android/thememanager/util/ThemeHelper;->LOG2:D

    div-double/2addr v0, v2

    const-wide v2, 0x3fb999999999999aL

    add-double/2addr v0, v2

    double-to-int v0, v0

    goto :goto_0
.end method

.method public static getComponentNumber(J)I
    .locals 3
    .parameter "flag"

    .prologue
    .line 242
    const/4 v0, 0x0

    .line 243
    .local v0, cnt:I
    const-wide/16 v1, -0x1

    cmp-long v1, p0, v1

    if-nez v1, :cond_0

    .line 244
    sget v1, Lcom/android/thememanager/util/ThemeHelper;->THEME_FLAG_COUNT:I

    add-int/lit8 v1, v1, 0x1

    .line 251
    :goto_0
    return v1

    .line 246
    :cond_0
    :goto_1
    const-wide/16 v1, 0x0

    cmp-long v1, p0, v1

    if-eqz v1, :cond_1

    .line 247
    const-wide/16 v1, 0x1

    sub-long v1, p0, v1

    and-long/2addr p0, v1

    .line 248
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move v1, v0

    .line 251
    goto :goto_0
.end method

.method public static getCurrentLanguage(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .parameter "context"

    .prologue
    .line 82
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getLockstyleAppliedConfigFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 434
    sget-object v0, Lmiui/content/res/ThemeResources;->sAppliedLockstyleConfigPath:Ljava/lang/String;

    return-object v0
.end method

.method public static getLockstyleSDCardConfigPathFromThemePath(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .parameter "path"

    .prologue
    .line 438
    if-nez p0, :cond_0

    .line 439
    const/4 v3, 0x0

    .line 444
    :goto_0
    return-object v3

    .line 441
    :cond_0
    const-string v3, "/"

    invoke-virtual {p0, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 442
    .local v1, start:I
    const-string v3, "."

    invoke-virtual {p0, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 443
    .local v0, end:I
    if-gt v0, v1, :cond_1

    add-int/lit8 v3, v1, 0x1

    invoke-virtual {p0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 444
    .local v2, themeName:Ljava/lang/String;
    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/android/thememanager/util/ThemeHelper;->DOWNLOADED_THEME_PATH:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".lockstyle_config"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".config"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 443
    .end local v2           #themeName:Ljava/lang/String;
    :cond_1
    add-int/lit8 v3, v1, 0x1

    invoke-virtual {p0, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method public static getNameForAudioEffect(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .parameter "ringtonePath"

    .prologue
    const/16 v2, 0x2e

    .line 208
    sget-char v1, Ljava/io/File;->separatorChar:C

    invoke-virtual {p0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 209
    .local v0, name:Ljava/lang/String;
    sget-object v1, Lcom/android/thememanager/util/ThemeHelper;->sAudioEffectMap:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 210
    sget-object v1, Lcom/android/thememanager/util/ThemeHelper;->sAudioEffectMap:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0           #name:Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 212
    .restart local v0       #name:Ljava/lang/String;
    :cond_0
    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-lez v1, :cond_1

    .line 213
    const/4 v1, 0x0

    invoke-virtual {v0, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 216
    :cond_1
    return-object v0
.end method

.method public static getResourceId(Lmiui/resourcebrowser/model/Resource;)Ljava/lang/String;
    .locals 2
    .parameter "resource"

    .prologue
    .line 588
    invoke-virtual {p0}, Lmiui/resourcebrowser/model/Resource;->getOnlineId()Ljava/lang/String;

    move-result-object v0

    .line 589
    .local v0, resourceId:Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 590
    :cond_0
    invoke-virtual {p0}, Lmiui/resourcebrowser/model/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/thememanager/util/ThemeHelper;->getResourceIdFromPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 592
    :cond_1
    return-object v0
.end method

.method public static getResourceIdFromPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .parameter "path"

    .prologue
    .line 596
    const/4 v1, 0x0

    .line 597
    .local v1, resourceId:Ljava/lang/String;
    if-eqz p0, :cond_1

    .line 598
    invoke-static {p0}, Lmiui/resourcebrowser/util/ResourceHelper;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 599
    const-string v2, "."

    invoke-virtual {p0, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 600
    .local v0, index:I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    .line 601
    const/4 v2, 0x0

    invoke-virtual {p0, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 602
    const-string v2, "[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}"

    invoke-virtual {p0, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "default"

    invoke-virtual {p0, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 604
    :cond_0
    move-object v1, p0

    .line 608
    .end local v0           #index:I
    :cond_1
    return-object v1
.end method

.method public static getScreenDisplayMetrics(Landroid/content/Context;)Landroid/util/DisplayMetrics;
    .locals 2
    .parameter "context"

    .prologue
    .line 86
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 87
    .local v0, ret:Landroid/util/DisplayMetrics;
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    if-eqz v1, :cond_0

    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    if-nez v1, :cond_1

    .line 88
    :cond_0
    new-instance v0, Landroid/util/DisplayMetrics;

    .end local v0           #ret:Landroid/util/DisplayMetrics;
    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 90
    .restart local v0       #ret:Landroid/util/DisplayMetrics;
    :try_start_0
    check-cast p0, Landroid/app/Activity;

    .end local p0
    invoke-virtual {p0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    :cond_1
    :goto_0
    return-object v0

    .line 91
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static identifyComponents(Ljava/util/zip/ZipFile;)J
    .locals 2
    .parameter "zipfile"

    .prologue
    .line 259
    const/4 v0, -0x1

    invoke-static {p0, v0}, Lcom/android/thememanager/util/ThemeHelper;->identifyComponents(Ljava/util/zip/ZipFile;I)J

    move-result-wide v0

    return-wide v0
.end method

.method public static identifyComponents(Ljava/util/zip/ZipFile;I)J
    .locals 13
    .parameter "zipfile"
    .parameter "platformVersion"

    .prologue
    const/4 v12, 0x1

    .line 263
    const-wide/16 v3, 0x0

    .line 264
    .local v3, flags:J
    const/4 v5, 0x1

    .local v5, i:I
    :goto_0
    sget v8, Lcom/android/thememanager/util/ThemeHelper;->THEME_FLAG_COUNT:I

    if-ge v5, v8, :cond_1

    .line 265
    sget-object v8, Lcom/android/thememanager/util/ThemeHelper;->COMPONENT_IDENTITIES:[Ljava/lang/String;

    aget-object v8, v8, v5

    invoke-virtual {p0, v8}, Ljava/util/zip/ZipFile;->getEntry(Ljava/lang/String;)Ljava/util/zip/ZipEntry;

    move-result-object v8

    if-eqz v8, :cond_0

    .line 266
    shl-int v8, v12, v5

    int-to-long v8, v8

    or-long/2addr v3, v8

    .line 264
    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 272
    :cond_1
    invoke-virtual {p0}, Ljava/util/zip/ZipFile;->entries()Ljava/util/Enumeration;

    move-result-object v1

    .line 273
    .local v1, entries:Ljava/util/Enumeration;,"Ljava/util/Enumeration<Ljava/util/zip/ZipEntry;>;"
    const/4 v2, 0x0

    .line 274
    .local v2, entry:Ljava/util/zip/ZipEntry;
    const/4 v7, 0x0

    .line 275
    .local v7, path:Ljava/lang/String;
    :cond_2
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v8

    if-eqz v8, :cond_6

    .line 276
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    .end local v2           #entry:Ljava/util/zip/ZipEntry;
    check-cast v2, Ljava/util/zip/ZipEntry;

    .line 277
    .restart local v2       #entry:Ljava/util/zip/ZipEntry;
    invoke-virtual {v2}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v7

    .line 278
    const/4 v5, 0x0

    :goto_1
    sget v8, Lcom/android/thememanager/util/ThemeHelper;->THEME_FLAG_COUNT:I

    if-ge v5, v8, :cond_2

    .line 279
    const-wide/16 v8, 0x1

    shl-long/2addr v8, v5

    and-long/2addr v8, v3

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-nez v8, :cond_5

    sget-object v8, Lcom/android/thememanager/util/ThemeHelper;->COMPONENT_IDENTITIES:[Ljava/lang/String;

    aget-object v8, v8, v5

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 280
    if-nez v5, :cond_4

    invoke-virtual {v2}, Ljava/util/zip/ZipEntry;->isDirectory()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 278
    :cond_3
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 283
    :cond_4
    shl-int v8, v12, v5

    int-to-long v8, v8

    or-long/2addr v3, v8

    goto :goto_2

    .line 284
    :cond_5
    const-string v8, "com.android.launcher"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 285
    const-wide/16 v8, 0x4000

    or-long/2addr v3, v8

    goto :goto_2

    .line 290
    :cond_6
    if-gez p1, :cond_7

    .line 291
    const-string v8, "description.xml"

    invoke-static {p0, v8}, Lmiui/resourcebrowser/util/ResourceHelper;->getDescription(Ljava/util/zip/ZipFile;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v6

    .line 293
    .local v6, nvp:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v6, :cond_8

    :try_start_0
    const-string v8, "uiVersion"

    invoke-interface {v6, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p1

    .line 298
    .end local v6           #nvp:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_7
    :goto_3
    invoke-static {p1, v3, v4}, Lcom/android/thememanager/util/ThemeHelper;->getCompatibleFlag(IJ)J

    move-result-wide v8

    return-wide v8

    .line 293
    .restart local v6       #nvp:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_8
    const/4 p1, 0x0

    goto :goto_3

    .line 294
    :catch_0
    move-exception v0

    .line 295
    .local v0, e:Ljava/lang/NumberFormatException;
    const/4 p1, 0x0

    goto :goto_3
.end method

.method private static initAudioEffectMap(Landroid/content/res/Resources;)V
    .locals 6
    .parameter "res"

    .prologue
    .line 196
    const/high16 v3, 0x7f06

    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v1

    .line 197
    .local v1, key:[Ljava/lang/CharSequence;
    const v3, 0x7f060001

    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v2

    .line 198
    .local v2, value:[Ljava/lang/CharSequence;
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    array-length v3, v1

    array-length v4, v2

    if-ne v3, v4, :cond_0

    .line 199
    sget-object v3, Lcom/android/thememanager/util/ThemeHelper;->sAudioEffectMap:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->clear()V

    .line 200
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_0

    .line 201
    sget-object v3, Lcom/android/thememanager/util/ThemeHelper;->sAudioEffectMap:Ljava/util/Map;

    aget-object v4, v1, v0

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    aget-object v5, v2, v0

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    sget-object v3, Lcom/android/thememanager/util/ThemeHelper;->sAudioEffectOrder:Ljava/util/List;

    aget-object v4, v1, v0

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 200
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 205
    .end local v0           #i:I
    :cond_0
    return-void
.end method

.method private static initPlatformSupport(Landroid/content/Context;)V
    .locals 5
    .parameter "context"

    .prologue
    .line 137
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 139
    .local v2, res:Landroid/content/res/Resources;
    const/high16 v3, 0x7f09

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    sput-boolean v3, Lcom/android/thememanager/util/ThemeHelper;->sPlatformSupportReplaceFont:Z

    .line 141
    const v3, 0x7f060002

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v1

    .line 142
    .local v1, packageName:[Ljava/lang/CharSequence;
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_0

    .line 143
    sget-object v3, Lcom/android/thememanager/util/ThemeHelper;->sDisablePackagesReplaceSet:Ljava/util/Set;

    aget-object v4, v1, v0

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 142
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 145
    :cond_0
    return-void
.end method

.method public static initResource(Landroid/content/Context;)V
    .locals 1
    .parameter "context"

    .prologue
    .line 126
    invoke-static {p0}, Lcom/android/thememanager/util/ThemeHelper;->initSharedPreference(Landroid/content/Context;)V

    .line 127
    invoke-static {p0}, Lcom/android/thememanager/util/ThemeHelper;->initPlatformSupport(Landroid/content/Context;)V

    .line 128
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/android/thememanager/util/ThemeHelper;->initAudioEffectMap(Landroid/content/res/Resources;)V

    .line 129
    return-void
.end method

.method private static initSharedPreference(Landroid/content/Context;)V
    .locals 1
    .parameter "context"

    .prologue
    .line 132
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    sput-object v0, Lcom/android/thememanager/util/ThemeHelper;->sSharedPreference:Landroid/content/SharedPreferences;

    .line 133
    sget-object v0, Lcom/android/thememanager/util/ThemeHelper;->sSharedPreference:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    sput-object v0, Lcom/android/thememanager/util/ThemeHelper;->sEditor:Landroid/content/SharedPreferences$Editor;

    .line 134
    return-void
.end method

.method public static isDisablePkgName(Ljava/lang/String;)Z
    .locals 1
    .parameter "pkgName"

    .prologue
    .line 148
    sget-object v0, Lcom/android/thememanager/util/ThemeHelper;->sDisablePackagesReplaceSet:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static isWallpaperPrefOlderThanSystem(Landroid/content/Context;J)Z
    .locals 5
    .parameter "context"
    .parameter "wallpaperFlag"

    .prologue
    .line 493
    const/4 v2, 0x0

    .line 494
    .local v2, wallpaper:Ljava/io/File;
    const-wide/16 v3, 0x2

    cmp-long v3, p1, v3

    if-nez v3, :cond_1

    .line 495
    new-instance v2, Ljava/io/File;

    .end local v2           #wallpaper:Ljava/io/File;
    const-string v3, "/data/data/com.android.settings/files/wallpaper"

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 500
    .restart local v2       #wallpaper:Ljava/io/File;
    :cond_0
    :goto_0
    if-eqz v2, :cond_2

    .line 501
    invoke-static {p1, p2}, Lcom/android/thememanager/util/ThemeHelper;->loadUserPreferenceTime(J)J

    move-result-wide v0

    .line 502
    .local v0, updateTime:J
    invoke-virtual {v2}, Ljava/io/File;->lastModified()J

    move-result-wide v3

    cmp-long v3, v0, v3

    if-gez v3, :cond_2

    .line 503
    const/4 v3, 0x1

    .line 507
    .end local v0           #updateTime:J
    :goto_1
    return v3

    .line 496
    :cond_1
    const-wide/16 v3, 0x4

    cmp-long v3, p1, v3

    if-nez v3, :cond_0

    .line 497
    new-instance v2, Ljava/io/File;

    .end local v2           #wallpaper:Ljava/io/File;
    const-string v3, "/data/system/theme/lock_wallpaper"

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .restart local v2       #wallpaper:Ljava/io/File;
    goto :goto_0

    .line 507
    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public static loadUserPreferenceName(J)Ljava/lang/String;
    .locals 3
    .parameter "flag"

    .prologue
    .line 358
    sget-object v0, Lcom/android/thememanager/util/ThemeHelper;->sSharedPreference:Landroid/content/SharedPreferences;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "name-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static loadUserPreferencePath(J)Ljava/lang/String;
    .locals 3
    .parameter "flag"

    .prologue
    .line 354
    sget-object v0, Lcom/android/thememanager/util/ThemeHelper;->sSharedPreference:Landroid/content/SharedPreferences;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "path-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static loadUserPreferenceTime(J)J
    .locals 4
    .parameter "flag"

    .prologue
    .line 362
    sget-object v0, Lcom/android/thememanager/util/ThemeHelper;->sSharedPreference:Landroid/content/SharedPreferences;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "time-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static needsFontChange(JLjava/lang/String;J)Z
    .locals 11
    .parameter "applyFlags"
    .parameter "themePath"
    .parameter "themeFlags"

    .prologue
    .line 160
    const-wide/32 v7, 0x40010

    and-long/2addr v7, p0

    const-wide/16 v9, 0x0

    cmp-long v7, v7, v9

    if-eqz v7, :cond_2

    const/4 v4, 0x1

    .line 161
    .local v4, ret:Z
    :goto_0
    if-eqz v4, :cond_1

    .line 162
    new-instance v3, Ljava/io/File;

    const-string v7, "/data/system/theme/fonts"

    invoke-direct {v3, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 163
    .local v3, fontDir:Ljava/io/File;
    const-wide/32 v7, 0x40010

    and-long/2addr v7, p3

    const-wide/16 v9, 0x0

    cmp-long v7, v7, v9

    if-eqz v7, :cond_0

    const-string v7, "/system/media/theme/.data/meta/theme/default.mrm"

    invoke-virtual {v7, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 164
    :cond_0
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    .line 192
    .end local v3           #fontDir:Ljava/io/File;
    :cond_1
    :goto_1
    return v4

    .line 160
    .end local v4           #ret:Z
    :cond_2
    const/4 v4, 0x0

    goto :goto_0

    .line 165
    .restart local v3       #fontDir:Ljava/io/File;
    .restart local v4       #ret:Z
    :cond_3
    sget-object v7, Lcom/android/thememanager/util/BackupThemeTask;->BACKUP_THEME_PATH:Ljava/lang/String;

    invoke-virtual {v7, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 166
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_1

    .line 167
    const/4 v5, 0x0

    .line 169
    .local v5, zipfile:Ljava/util/zip/ZipFile;
    :try_start_0
    new-instance v6, Ljava/util/zip/ZipFile;

    sget-object v7, Lcom/android/thememanager/util/BackupThemeTask;->BACKUP_THEME_PATH:Ljava/lang/String;

    invoke-direct {v6, v7}, Ljava/util/zip/ZipFile;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 171
    .end local v5           #zipfile:Ljava/util/zip/ZipFile;
    .local v6, zipfile:Ljava/util/zip/ZipFile;
    :try_start_1
    invoke-virtual {v6}, Ljava/util/zip/ZipFile;->entries()Ljava/util/Enumeration;

    move-result-object v1

    .line 172
    .local v1, entries:Ljava/util/Enumeration;,"Ljava/util/Enumeration<Ljava/util/zip/ZipEntry;>;"
    :cond_4
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 173
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/zip/ZipEntry;

    .line 174
    .local v2, entry:Ljava/util/zip/ZipEntry;
    invoke-virtual {v2}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, "fonts"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    move-result v7

    if-eqz v7, :cond_4

    .line 175
    const/4 v4, 0x1

    .line 181
    .end local v2           #entry:Ljava/util/zip/ZipEntry;
    :cond_5
    if-eqz v6, :cond_1

    .line 183
    :try_start_2
    invoke-virtual {v6}, Ljava/util/zip/ZipFile;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 184
    :catch_0
    move-exception v0

    .line 185
    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 179
    .end local v0           #e:Ljava/io/IOException;
    .end local v1           #entries:Ljava/util/Enumeration;,"Ljava/util/Enumeration<Ljava/util/zip/ZipEntry;>;"
    .end local v6           #zipfile:Ljava/util/zip/ZipFile;
    .restart local v5       #zipfile:Ljava/util/zip/ZipFile;
    :catch_1
    move-exception v7

    .line 181
    :goto_2
    if-eqz v5, :cond_1

    .line 183
    :try_start_3
    invoke-virtual {v5}, Ljava/util/zip/ZipFile;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    .line 184
    :catch_2
    move-exception v0

    .line 185
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 181
    .end local v0           #e:Ljava/io/IOException;
    :catchall_0
    move-exception v7

    :goto_3
    if-eqz v5, :cond_6

    .line 183
    :try_start_4
    invoke-virtual {v5}, Ljava/util/zip/ZipFile;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 186
    :cond_6
    :goto_4
    throw v7

    .line 184
    :catch_3
    move-exception v0

    .line 185
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 181
    .end local v0           #e:Ljava/io/IOException;
    .end local v5           #zipfile:Ljava/util/zip/ZipFile;
    .restart local v6       #zipfile:Ljava/util/zip/ZipFile;
    :catchall_1
    move-exception v7

    move-object v5, v6

    .end local v6           #zipfile:Ljava/util/zip/ZipFile;
    .restart local v5       #zipfile:Ljava/util/zip/ZipFile;
    goto :goto_3

    .line 179
    .end local v5           #zipfile:Ljava/util/zip/ZipFile;
    .restart local v6       #zipfile:Ljava/util/zip/ZipFile;
    :catch_4
    move-exception v7

    move-object v5, v6

    .end local v6           #zipfile:Ljava/util/zip/ZipFile;
    .restart local v5       #zipfile:Ljava/util/zip/ZipFile;
    goto :goto_2
.end method

.method public static saveUserPreference(JLjava/lang/String;)V
    .locals 1
    .parameter "flag"
    .parameter "path"

    .prologue
    .line 387
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/android/thememanager/util/ThemeHelper;->saveUserPreference(JLjava/lang/String;Ljava/lang/String;)V

    .line 388
    return-void
.end method

.method public static saveUserPreference(JLjava/lang/String;Ljava/lang/String;)V
    .locals 5
    .parameter "flag"
    .parameter "path"
    .parameter "title"

    .prologue
    .line 391
    sget-object v1, Lcom/android/thememanager/util/ThemeHelper;->sSharedPreference:Landroid/content/SharedPreferences;

    monitor-enter v1

    .line 392
    :try_start_0
    invoke-static {p0, p1}, Lcom/android/thememanager/util/ThemeHelper;->clearUserPreference(J)V

    .line 393
    sget-object v0, Lcom/android/thememanager/util/ThemeHelper;->sEditor:Landroid/content/SharedPreferences$Editor;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "path-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 394
    if-eqz p3, :cond_0

    .line 395
    sget-object v0, Lcom/android/thememanager/util/ThemeHelper;->sEditor:Landroid/content/SharedPreferences$Editor;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "name-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, p3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 397
    :cond_0
    sget-object v0, Lcom/android/thememanager/util/ThemeHelper;->sEditor:Landroid/content/SharedPreferences$Editor;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "time-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-interface {v0, v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 398
    sget-object v0, Lcom/android/thememanager/util/ThemeHelper;->sEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 399
    monitor-exit v1

    .line 400
    return-void

    .line 399
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static saveUserPreference(Lmiui/resourcebrowser/model/Resource;JJ)V
    .locals 10
    .parameter "resource"
    .parameter "saveFlags"
    .parameter "clearFlags"

    .prologue
    const-wide/16 v8, 0x0

    .line 403
    sget-object v3, Lcom/android/thememanager/util/ThemeHelper;->sSharedPreference:Landroid/content/SharedPreferences;

    monitor-enter v3

    .line 404
    const-wide/16 v0, 0x1

    .local v0, flag:J
    :goto_0
    const-wide/32 v4, 0x40000

    cmp-long v2, v0, v4

    if-gtz v2, :cond_2

    .line 405
    and-long v4, v0, p1

    cmp-long v2, v4, v8

    if-eqz v2, :cond_1

    .line 406
    :try_start_0
    invoke-static {p0, v0, v1}, Lcom/android/thememanager/util/ThemeHelper;->setUserPreference(Lmiui/resourcebrowser/model/Resource;J)V

    .line 404
    :cond_0
    :goto_1
    const/4 v2, 0x1

    shl-long/2addr v0, v2

    goto :goto_0

    .line 407
    :cond_1
    and-long v4, v0, p3

    cmp-long v2, v4, v8

    if-eqz v2, :cond_0

    .line 408
    invoke-static {v0, v1}, Lcom/android/thememanager/util/ThemeHelper;->clearUserPreference(J)V

    goto :goto_1

    .line 417
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 412
    :cond_2
    or-long v4, p1, p3

    const-wide/16 v6, 0x1

    and-long/2addr v4, v6

    cmp-long v2, v4, v8

    if-eqz v2, :cond_3

    .line 413
    const-wide/16 v4, -0x1

    :try_start_1
    invoke-static {p0, v4, v5}, Lcom/android/thememanager/util/ThemeHelper;->setUserPreference(Lmiui/resourcebrowser/model/Resource;J)V

    .line 416
    :cond_3
    sget-object v2, Lcom/android/thememanager/util/ThemeHelper;->sEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 417
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 418
    return-void
.end method

.method public static saveUserPreferenceTime(JJ)V
    .locals 4
    .parameter "flag"
    .parameter "time"

    .prologue
    .line 380
    sget-object v1, Lcom/android/thememanager/util/ThemeHelper;->sSharedPreference:Landroid/content/SharedPreferences;

    monitor-enter v1

    .line 381
    :try_start_0
    sget-object v0, Lcom/android/thememanager/util/ThemeHelper;->sEditor:Landroid/content/SharedPreferences$Editor;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "time-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, p2, p3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 382
    sget-object v0, Lcom/android/thememanager/util/ThemeHelper;->sEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 383
    monitor-exit v1

    .line 384
    return-void

    .line 383
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static setMusicVolumeType(Landroid/app/Activity;J)V
    .locals 3
    .parameter "activity"
    .parameter "flag"

    .prologue
    .line 98
    const/4 v0, -0x1

    .line 99
    .local v0, playType:I
    const-wide/16 v1, 0x100

    cmp-long v1, p1, v1

    if-nez v1, :cond_1

    .line 100
    const/4 v0, 0x2

    .line 111
    :goto_0
    if-ltz v0, :cond_0

    .line 112
    invoke-virtual {p0, v0}, Landroid/app/Activity;->setVolumeControlStream(I)V

    .line 114
    :cond_0
    return-void

    .line 101
    :cond_1
    const-wide/16 v1, 0x200

    cmp-long v1, p1, v1

    if-nez v1, :cond_2

    .line 102
    const/4 v0, 0x5

    goto :goto_0

    .line 103
    :cond_2
    const-wide/16 v1, 0x400

    cmp-long v1, p1, v1

    if-nez v1, :cond_3

    .line 104
    const/4 v0, 0x4

    goto :goto_0

    .line 105
    :cond_3
    const-wide/32 v1, 0x8000

    cmp-long v1, p1, v1

    if-nez v1, :cond_4

    .line 106
    const/4 v0, 0x1

    goto :goto_0

    .line 108
    :cond_4
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public static setRingtone(Landroid/content/Context;ILjava/lang/String;)Z
    .locals 5
    .parameter "context"
    .parameter "type"
    .parameter "path"

    .prologue
    .line 525
    const/4 v3, 0x0

    .line 526
    .local v3, uri:Landroid/net/Uri;
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 527
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    .line 528
    invoke-static {p2, p1}, Landroid/media/ExtraRingtoneManager;->copyRingtone(Ljava/lang/String;I)V

    .line 530
    :cond_0
    invoke-static {p0, p1, v3}, Landroid/media/RingtoneManager;->setActualDefaultRingtoneUri(Landroid/content/Context;ILandroid/net/Uri;)V

    .line 532
    const-wide/16 v0, 0x100

    .line 533
    .local v0, flag:J
    packed-switch p1, :pswitch_data_0

    .line 546
    :goto_0
    :pswitch_0
    const v4, 0x7f0b000c

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 547
    .local v2, title:Ljava/lang/String;
    invoke-static {v0, v1, p2, v2}, Lcom/android/thememanager/util/ThemeHelper;->saveUserPreference(JLjava/lang/String;Ljava/lang/String;)V

    .line 548
    const/4 v4, 0x1

    return v4

    .line 535
    .end local v2           #title:Ljava/lang/String;
    :pswitch_1
    const-wide/16 v0, 0x100

    .line 536
    goto :goto_0

    .line 539
    :pswitch_2
    const-wide/16 v0, 0x200

    .line 540
    goto :goto_0

    .line 543
    :pswitch_3
    const-wide/16 v0, 0x400

    goto :goto_0

    .line 533
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private static setUserPreference(Lmiui/resourcebrowser/model/Resource;J)V
    .locals 4
    .parameter "resource"
    .parameter "flag"

    .prologue
    .line 421
    sget-object v0, Lcom/android/thememanager/util/ThemeHelper;->sEditor:Landroid/content/SharedPreferences$Editor;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "path-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lmiui/resourcebrowser/model/Resource;->getFilePath()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 422
    sget-object v0, Lcom/android/thememanager/util/ThemeHelper;->sEditor:Landroid/content/SharedPreferences$Editor;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "name-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lmiui/resourcebrowser/model/Resource;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 423
    sget-object v0, Lcom/android/thememanager/util/ThemeHelper;->sEditor:Landroid/content/SharedPreferences$Editor;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "time-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 424
    return-void
.end method

.method public static showThemeChangedToast(Landroid/content/Context;Z)V
    .locals 1
    .parameter "context"
    .parameter "result"

    .prologue
    .line 578
    const-string v0, ""

    invoke-static {p0, p1, v0}, Lcom/android/thememanager/util/ThemeHelper;->showThemeChangedToast(Landroid/content/Context;ZLjava/lang/String;)V

    .line 579
    return-void
.end method

.method public static showThemeChangedToast(Landroid/content/Context;ZLjava/lang/String;)V
    .locals 5
    .parameter "context"
    .parameter "result"
    .parameter "name"

    .prologue
    const/4 v4, 0x1

    .line 582
    if-eqz p1, :cond_0

    const v1, 0x7f0b0003

    :goto_0
    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 584
    .local v0, text:Ljava/lang/String;
    invoke-static {p0, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 585
    return-void

    .line 582
    .end local v0           #text:Ljava/lang/String;
    :cond_0
    const v1, 0x7f0b0004

    goto :goto_0
.end method

.method public static supportReplaceAudioEffect()Z
    .locals 1

    .prologue
    .line 152
    const/4 v0, 0x0

    return v0
.end method

.method public static supportReplaceFont()Z
    .locals 1

    .prologue
    .line 156
    sget-boolean v0, Lcom/android/thememanager/util/ThemeHelper;->sPlatformSupportReplaceFont:Z

    return v0
.end method

.method public static updateLockWallpaperInfo(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .parameter "context"
    .parameter "srcImagePath"

    .prologue
    const/4 v3, -0x1

    .line 487
    const-string v1, "/data/system/theme/lock_wallpaper"

    const/16 v2, 0x1fd

    invoke-static {v1, v2, v3, v3}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    .line 488
    const v1, 0x7f0b000c

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 489
    .local v0, title:Ljava/lang/String;
    const-wide/16 v1, 0x4

    invoke-static {v1, v2, p1, v0}, Lcom/android/thememanager/util/ThemeHelper;->saveUserPreference(JLjava/lang/String;Ljava/lang/String;)V

    .line 490
    return-void
.end method
