.class public Lcom/android/thememanager/util/UIHelper;
.super Ljava/lang/Object;
.source "UIHelper.java"

# interfaces
.implements Lcom/android/thememanager/ThemeResourceConstants;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method

.method public static buildResourceContext(Lmiui/resourcebrowser/ResourceContext;Landroid/content/Intent;Landroid/content/Context;)Lmiui/resourcebrowser/ResourceContext;
    .locals 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 32
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    .line 33
    const-wide/16 v0, -0x1

    .line 34
    const-string v2, "android.intent.action.RINGTONE_PICKER"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 35
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lmiui/resourcebrowser/ResourceContext;->setPicker(Z)V

    .line 36
    const-string v2, "android.intent.extra.ringtone.TYPE"

    const/4 v4, 0x1

    invoke-virtual {p1, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 40
    const/4 v4, 0x7

    if-ne v2, v4, :cond_0

    .line 41
    const/4 v2, 0x2

    .line 44
    :cond_0
    packed-switch v2, :pswitch_data_0

    .line 71
    :goto_0
    :pswitch_0
    const-string v2, "android.intent.extra.ringtone.TYPE"

    invoke-static {v0, v1}, Lcom/android/thememanager/util/UIHelper;->getRingtoneType(J)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p0, v2, v4}, Lmiui/resourcebrowser/ResourceContext;->putExtraMeta(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 73
    const-string v2, "EXTRA_CTX_RESOURCE_TYPE"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {p0, v2, v4}, Lmiui/resourcebrowser/ResourceContext;->putExtraMeta(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 75
    const-wide/32 v4, 0x8000

    cmp-long v2, v0, v4

    if-nez v2, :cond_1

    .line 76
    const-string v2, "android.intent.extra.ringtone.SHOW_SILENT"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {p0, v2, v4}, Lmiui/resourcebrowser/ResourceContext;->putExtraMeta(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 77
    const-string v2, "android.intent.extra.ringtone.SHOW_DEFAULT"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {p0, v2, v4}, Lmiui/resourcebrowser/ResourceContext;->putExtraMeta(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 78
    const-string v2, "EXTRA_CTX_SHOW_RINGTONE_NAME"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {p0, v2, v4}, Lmiui/resourcebrowser/ResourceContext;->putExtraMeta(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 81
    :cond_1
    invoke-static {v0, v1}, Lcom/android/thememanager/util/UIHelper;->getDisplayType(J)I

    move-result v2

    .line 82
    invoke-virtual {p0, v2}, Lmiui/resourcebrowser/ResourceContext;->setDisplayType(I)V

    .line 84
    invoke-static {v0, v1}, Lcom/android/thememanager/util/ConstantsHelper;->getTitleResId(J)I

    move-result v2

    invoke-virtual {p2, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 85
    invoke-virtual {p0, v2}, Lmiui/resourcebrowser/ResourceContext;->setResourceTitle(Ljava/lang/String;)V

    .line 87
    invoke-static {v0, v1}, Lcom/android/thememanager/util/UIHelper;->getResourceFormat(J)I

    move-result v4

    .line 88
    invoke-virtual {p0, v4}, Lmiui/resourcebrowser/ResourceContext;->setResourceFormat(I)V

    .line 90
    invoke-static {v0, v1}, Lcom/android/thememanager/util/UIHelper;->getResourceExtension(J)Ljava/lang/String;

    move-result-object v4

    .line 91
    invoke-virtual {p0, v4}, Lmiui/resourcebrowser/ResourceContext;->setResourceExtension(Ljava/lang/String;)V

    .line 93
    invoke-static {v0, v1}, Lcom/android/thememanager/util/UIHelper;->isSelfDescribing(J)Z

    move-result v4

    .line 94
    invoke-virtual {p0, v4}, Lmiui/resourcebrowser/ResourceContext;->setSelfDescribing(Z)V

    .line 96
    invoke-static {v0, v1}, Lcom/android/thememanager/util/ConstantsHelper;->getResourceStamp(J)Ljava/lang/String;

    move-result-object v4

    .line 97
    invoke-virtual {p0, v4}, Lmiui/resourcebrowser/ResourceContext;->setResourceStamp(Ljava/lang/String;)V

    .line 98
    const-string v4, "android.intent.action.PICK_GADGET"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 99
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "REQUEST_GADGET_SIZE"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 102
    :cond_2
    invoke-static {v0, v1}, Lcom/android/thememanager/util/ConstantsHelper;->getResourceCode(J)Ljava/lang/String;

    move-result-object v3

    .line 103
    invoke-virtual {p0, v3}, Lmiui/resourcebrowser/ResourceContext;->setResourceCode(Ljava/lang/String;)V

    .line 105
    invoke-static {v0, v1}, Lcom/android/thememanager/util/UIHelper;->isRecommendSupported(J)Z

    move-result v2

    .line 106
    invoke-virtual {p0, v2}, Lmiui/resourcebrowser/ResourceContext;->setRecommendSupported(Z)V

    .line 108
    invoke-static {v0, v1}, Lcom/android/thememanager/util/UIHelper;->isCategorySupported(J)Z

    move-result v2

    .line 109
    invoke-virtual {p0, v2}, Lmiui/resourcebrowser/ResourceContext;->setCategorySupported(Z)V

    .line 110
    const-wide/32 v4, 0x10000

    cmp-long v2, v0, v4

    if-eqz v2, :cond_3

    const-wide/32 v4, 0x20000

    cmp-long v2, v0, v4

    if-nez v2, :cond_4

    .line 111
    :cond_3
    invoke-virtual {p0}, Lmiui/resourcebrowser/ResourceContext;->isPicker()Z

    move-result v2

    if-nez v2, :cond_a

    const/4 v2, 0x1

    :goto_1
    invoke-virtual {p0, v2}, Lmiui/resourcebrowser/ResourceContext;->setCategorySupported(Z)V

    .line 114
    :cond_4
    invoke-static {v0, v1}, Lcom/android/thememanager/util/UIHelper;->isVersionSupported(J)Z

    move-result v2

    .line 115
    invoke-virtual {p0, v2}, Lmiui/resourcebrowser/ResourceContext;->setVersionSupported(Z)V

    .line 117
    invoke-static {v0, v1}, Lcom/android/thememanager/util/UIHelper;->isPlatformSupported(J)Z

    move-result v2

    .line 118
    invoke-virtual {p0, v2}, Lmiui/resourcebrowser/ResourceContext;->setPlatformSupported(Z)V

    .line 119
    if-eqz v2, :cond_5

    .line 120
    invoke-static {v0, v1}, Lcom/android/thememanager/util/ConstantsHelper;->getPlatform(J)I

    move-result v2

    .line 121
    invoke-virtual {p0, v2}, Lmiui/resourcebrowser/ResourceContext;->setCurrentPlatform(I)V

    .line 124
    :cond_5
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 125
    const-wide/16 v5, -0x1

    cmp-long v2, v0, v5

    if-nez v2, :cond_b

    .line 126
    const/4 v2, 0x0

    :goto_2
    sget-object v5, Lcom/android/thememanager/util/UIHelper;->COMPONENT_PREVIEW_SHOW_ORDER:[J

    array-length v5, v5

    if-ge v2, v5, :cond_c

    .line 127
    sget-object v5, Lcom/android/thememanager/util/UIHelper;->COMPONENT_PREVIEW_SHOW_ORDER:[J

    aget-wide v5, v5, v2

    invoke-static {v5, v6}, Lcom/android/thememanager/util/ConstantsHelper;->getPreviewPrefix(J)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 126
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 46
    :pswitch_1
    const-wide/16 v0, 0x100

    .line 47
    goto/16 :goto_0

    .line 49
    :pswitch_2
    const-wide/16 v0, 0x200

    .line 50
    goto/16 :goto_0

    .line 52
    :pswitch_3
    const-wide/16 v0, 0x400

    goto/16 :goto_0

    .line 55
    :cond_6
    const-string v0, "android.intent.action.SET_WALLPAPER"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 56
    const-wide/16 v0, 0x2

    goto/16 :goto_0

    .line 57
    :cond_7
    const-string v0, "android.intent.action.PICK_GADGET"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 58
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setPicker(Z)V

    .line 59
    const-string v0, "REQUEST_CURRENT_USING_PATH"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setCurrentUsingPath(Ljava/lang/String;)V

    .line 60
    const-string v0, "REQUEST_TRACK_ID"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setTrackId(Ljava/lang/String;)V

    .line 61
    const-string v0, "REQUEST_GADGET_NAME"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 62
    const-string v1, "clock"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 63
    const-wide/32 v0, 0x10000

    .line 67
    :goto_3
    const-string v2, "EXTRA_CTX_GADGET_FLAG"

    const-string v4, "REQUEST_GADGET_SIZE"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v2, v4}, Lmiui/resourcebrowser/ResourceContext;->putExtraMeta(Ljava/lang/String;Ljava/io/Serializable;)V

    goto/16 :goto_0

    .line 65
    :cond_8
    const-wide/32 v0, 0x20000

    goto :goto_3

    .line 69
    :cond_9
    const-string v0, "REQUEST_RESOURCE_TYPE"

    const-wide/16 v1, -0x1

    invoke-virtual {p1, v0, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto/16 :goto_0

    .line 111
    :cond_a
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 130
    :cond_b
    invoke-static {v0, v1}, Lcom/android/thememanager/util/ConstantsHelper;->getPreviewPrefix(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 132
    :cond_c
    invoke-virtual {p0, v4}, Lmiui/resourcebrowser/ResourceContext;->setBuildInImagePrefixes(Ljava/util/List;)V

    .line 134
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 142
    const-wide/16 v4, 0x2

    cmp-long v2, v0, v4

    if-nez v2, :cond_e

    .line 143
    const-string v2, "/system/media/wallpaper/"

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 144
    const-string v2, "/data/media/wallpaper/"

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 145
    sget-object v2, Lcom/android/thememanager/util/UIHelper;->USER_WALLPAPER_PATH:Ljava/lang/String;

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 146
    sget-object v8, Lcom/android/thememanager/util/UIHelper;->DOWNLOADED_WALLPAPER_PATH:Ljava/lang/String;

    .line 147
    sget-object v7, Lcom/android/thememanager/util/UIHelper;->META_WALLPAPER_PATH:Ljava/lang/String;

    .line 148
    sget-object v6, Lcom/android/thememanager/util/UIHelper;->CONTENT_WALLPAPER_PATH:Ljava/lang/String;

    .line 149
    sget-object v5, Lcom/android/thememanager/util/UIHelper;->RIGHTS_WALLPAPER_PATH:Ljava/lang/String;

    .line 150
    sget-object v4, Lcom/android/thememanager/util/UIHelper;->BUILDIN_IMAGE_WALLPAPER_PATH:Ljava/lang/String;

    .line 151
    sget-object v3, Lcom/android/thememanager/util/UIHelper;->INDEX_WALLPAPER_PATH:Ljava/lang/String;

    .line 152
    sget-object v2, Lcom/android/thememanager/util/UIHelper;->ASYNC_IMPORT_WALLPAPER_PATH:Ljava/lang/String;

    .line 256
    :goto_4
    invoke-virtual {p0, v9}, Lmiui/resourcebrowser/ResourceContext;->setSourceFolders(Ljava/util/List;)V

    .line 257
    invoke-virtual {p0, v8}, Lmiui/resourcebrowser/ResourceContext;->setDownloadFolder(Ljava/lang/String;)V

    .line 258
    invoke-virtual {p0, v7}, Lmiui/resourcebrowser/ResourceContext;->setMetaFolder(Ljava/lang/String;)V

    .line 259
    invoke-virtual {p0, v6}, Lmiui/resourcebrowser/ResourceContext;->setContentFolder(Ljava/lang/String;)V

    .line 260
    invoke-virtual {p0, v5}, Lmiui/resourcebrowser/ResourceContext;->setRightsFolder(Ljava/lang/String;)V

    .line 261
    invoke-virtual {p0, v4}, Lmiui/resourcebrowser/ResourceContext;->setBuildInImageFolder(Ljava/lang/String;)V

    .line 262
    invoke-virtual {p0, v3}, Lmiui/resourcebrowser/ResourceContext;->setIndexFolder(Ljava/lang/String;)V

    .line 263
    invoke-virtual {p0, v2}, Lmiui/resourcebrowser/ResourceContext;->setAsyncImportFolder(Ljava/lang/String;)V

    .line 265
    invoke-virtual {p2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lmiui/resourcebrowser/ResourceContext;->setTabActivityPackage(Ljava/lang/String;)V

    .line 266
    const-class v2, Lcom/android/thememanager/activity/ThemeTabActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lmiui/resourcebrowser/ResourceContext;->setTabActivityClass(Ljava/lang/String;)V

    .line 267
    invoke-virtual {p2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lmiui/resourcebrowser/ResourceContext;->setSearchActivityPackage(Ljava/lang/String;)V

    .line 268
    const-class v2, Lcom/android/thememanager/activity/ThemeSearchListActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lmiui/resourcebrowser/ResourceContext;->setSearchActivityClass(Ljava/lang/String;)V

    .line 269
    invoke-virtual {p2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lmiui/resourcebrowser/ResourceContext;->setRecommendActivityPackage(Ljava/lang/String;)V

    .line 270
    const-class v2, Lcom/android/thememanager/activity/ThemeRecommendListActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lmiui/resourcebrowser/ResourceContext;->setRecommendActivityClass(Ljava/lang/String;)V

    .line 271
    invoke-virtual {p2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lmiui/resourcebrowser/ResourceContext;->setDetailActivityPackage(Ljava/lang/String;)V

    .line 272
    const-wide/16 v2, 0x2

    cmp-long v2, v0, v2

    if-eqz v2, :cond_d

    const-wide/16 v2, 0x4

    cmp-long v0, v0, v2

    if-nez v0, :cond_17

    .line 273
    :cond_d
    const-class v0, Lcom/android/thememanager/activity/WallpaperDetailActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setDetailActivityClass(Ljava/lang/String;)V

    .line 278
    :goto_5
    return-object p0

    .line 153
    :cond_e
    const-wide/16 v4, 0x4

    cmp-long v2, v0, v4

    if-nez v2, :cond_f

    .line 154
    const-string v2, "/system/media/lockscreen/"

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 155
    const-string v2, "/data/media/lockscreen/"

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 156
    sget-object v2, Lcom/android/thememanager/util/UIHelper;->USER_LOCKSCREEN_PATH:Ljava/lang/String;

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 157
    sget-object v8, Lcom/android/thememanager/util/UIHelper;->DOWNLOADED_LOCKSCREEN_PATH:Ljava/lang/String;

    .line 158
    sget-object v7, Lcom/android/thememanager/util/UIHelper;->META_LOCKSCREEN_PATH:Ljava/lang/String;

    .line 159
    sget-object v6, Lcom/android/thememanager/util/UIHelper;->CONTENT_LOCKSCREEN_PATH:Ljava/lang/String;

    .line 160
    sget-object v5, Lcom/android/thememanager/util/UIHelper;->RIGHTS_LOCKSCREEN_PATH:Ljava/lang/String;

    .line 161
    sget-object v4, Lcom/android/thememanager/util/UIHelper;->BUILDIN_IMAGE_LOCKSCREEN_PATH:Ljava/lang/String;

    .line 162
    sget-object v3, Lcom/android/thememanager/util/UIHelper;->INDEX_LOCKSCREEN_PATH:Ljava/lang/String;

    .line 163
    sget-object v2, Lcom/android/thememanager/util/UIHelper;->ASYNC_IMPORT_LOCKSCREEN_PATH:Ljava/lang/String;

    goto/16 :goto_4

    .line 164
    :cond_f
    const-wide/16 v4, 0x100

    cmp-long v2, v0, v4

    if-nez v2, :cond_10

    .line 165
    sget-object v2, Lcom/android/thememanager/util/UIHelper;->USER_RINGTONE_PATH:Ljava/lang/String;

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 166
    const-string v2, "/data/media/audio/ringtones/"

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 167
    const-string v2, "/system/media/audio/ringtones/"

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 168
    sget-object v8, Lcom/android/thememanager/util/UIHelper;->DOWNLOADED_RINGTONE_PATH:Ljava/lang/String;

    .line 169
    sget-object v7, Lcom/android/thememanager/util/UIHelper;->META_RINGTONE_PATH:Ljava/lang/String;

    .line 170
    sget-object v6, Lcom/android/thememanager/util/UIHelper;->CONTENT_RINGTONE_PATH:Ljava/lang/String;

    .line 171
    sget-object v5, Lcom/android/thememanager/util/UIHelper;->RIGHTS_RINGTONE_PATH:Ljava/lang/String;

    .line 172
    sget-object v4, Lcom/android/thememanager/util/UIHelper;->BUILDIN_IMAGE_RINGTONE_PATH:Ljava/lang/String;

    .line 173
    sget-object v3, Lcom/android/thememanager/util/UIHelper;->INDEX_RINGTONE_PATH:Ljava/lang/String;

    .line 174
    sget-object v2, Lcom/android/thememanager/util/UIHelper;->ASYNC_IMPORT_RINGTONE_PATH:Ljava/lang/String;

    goto/16 :goto_4

    .line 175
    :cond_10
    const-wide/16 v4, 0x200

    cmp-long v2, v0, v4

    if-nez v2, :cond_11

    .line 176
    sget-object v2, Lcom/android/thememanager/util/UIHelper;->USER_NOTIFICATION_PATH:Ljava/lang/String;

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 177
    const-string v2, "/data/media/audio/notifications/"

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 178
    const-string v2, "/system/media/audio/notifications/"

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 179
    sget-object v8, Lcom/android/thememanager/util/UIHelper;->DOWNLOADED_NOTIFICATION_PATH:Ljava/lang/String;

    .line 180
    sget-object v7, Lcom/android/thememanager/util/UIHelper;->META_NOTIFICATION_PATH:Ljava/lang/String;

    .line 181
    sget-object v6, Lcom/android/thememanager/util/UIHelper;->CONTENT_NOTIFICATION_PATH:Ljava/lang/String;

    .line 182
    sget-object v5, Lcom/android/thememanager/util/UIHelper;->RIGHTS_NOTIFICATION_PATH:Ljava/lang/String;

    .line 183
    sget-object v4, Lcom/android/thememanager/util/UIHelper;->BUILDIN_IMAGE_NOTIFICATION_PATH:Ljava/lang/String;

    .line 184
    sget-object v3, Lcom/android/thememanager/util/UIHelper;->INDEX_NOTIFICATION_PATH:Ljava/lang/String;

    .line 185
    sget-object v2, Lcom/android/thememanager/util/UIHelper;->ASYNC_IMPORT_NOTIFICATION_PATH:Ljava/lang/String;

    goto/16 :goto_4

    .line 186
    :cond_11
    const-wide/16 v4, 0x400

    cmp-long v2, v0, v4

    if-nez v2, :cond_12

    .line 187
    sget-object v2, Lcom/android/thememanager/util/UIHelper;->USER_ALARM_PATH:Ljava/lang/String;

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 188
    const-string v2, "/data/media/audio/alarms/"

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 189
    const-string v2, "/system/media/audio/alarms/"

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 190
    sget-object v8, Lcom/android/thememanager/util/UIHelper;->DOWNLOADED_ALARM_PATH:Ljava/lang/String;

    .line 191
    sget-object v7, Lcom/android/thememanager/util/UIHelper;->META_ALARM_PATH:Ljava/lang/String;

    .line 192
    sget-object v6, Lcom/android/thememanager/util/UIHelper;->CONTENT_ALARM_PATH:Ljava/lang/String;

    .line 193
    sget-object v5, Lcom/android/thememanager/util/UIHelper;->RIGHTS_ALARM_PATH:Ljava/lang/String;

    .line 194
    sget-object v4, Lcom/android/thememanager/util/UIHelper;->BUILDIN_IMAGE_ALARM_PATH:Ljava/lang/String;

    .line 195
    sget-object v3, Lcom/android/thememanager/util/UIHelper;->INDEX_ALARM_PATH:Ljava/lang/String;

    .line 196
    sget-object v2, Lcom/android/thememanager/util/UIHelper;->ASYNC_IMPORT_ALARM_PATH:Ljava/lang/String;

    goto/16 :goto_4

    .line 197
    :cond_12
    const-wide/16 v4, 0x40

    cmp-long v2, v0, v4

    if-nez v2, :cond_13

    .line 198
    sget-object v2, Lcom/android/thememanager/util/UIHelper;->USER_BOOT_AUDIO_PATH:Ljava/lang/String;

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 199
    const-string v2, "/data/media/audio/ringtones/"

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 200
    const-string v2, "/system/media/audio/ringtones/"

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 201
    sget-object v8, Lcom/android/thememanager/util/UIHelper;->DOWNLOADED_BOOT_AUDIO_PATH:Ljava/lang/String;

    .line 202
    sget-object v7, Lcom/android/thememanager/util/UIHelper;->META_BOOT_AUDIO_PATH:Ljava/lang/String;

    .line 203
    sget-object v6, Lcom/android/thememanager/util/UIHelper;->CONTENT_BOOT_AUDIO_PATH:Ljava/lang/String;

    .line 204
    sget-object v5, Lcom/android/thememanager/util/UIHelper;->RIGHTS_BOOT_AUDIO_PATH:Ljava/lang/String;

    .line 205
    sget-object v4, Lcom/android/thememanager/util/UIHelper;->BUILDIN_IMAGE_BOOT_AUDIO_PATH:Ljava/lang/String;

    .line 206
    sget-object v3, Lcom/android/thememanager/util/UIHelper;->INDEX_BOOT_AUDIO_PATH:Ljava/lang/String;

    .line 207
    sget-object v2, Lcom/android/thememanager/util/UIHelper;->ASYNC_IMPORT_BOOT_AUDIO_PATH:Ljava/lang/String;

    goto/16 :goto_4

    .line 208
    :cond_13
    const-wide/32 v4, 0x10000

    cmp-long v2, v0, v4

    if-nez v2, :cond_14

    .line 209
    const-string v2, "REQUEST_GADGET_SIZE"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 210
    const-string v3, "/system/media/theme/.data/meta/gadget/clock_%s/"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v9, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 211
    const-string v3, "/data/media/theme/.data/meta/gadget/clock_%s/"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v9, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 212
    sget-object v3, Lcom/android/thememanager/util/UIHelper;->USER_GADGET_CLOCK_PATH:Ljava/lang/String;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 213
    sget-object v8, Lcom/android/thememanager/util/UIHelper;->DOWNLOADED_GADGET_CLOCK_PATH:Ljava/lang/String;

    .line 214
    sget-object v7, Lcom/android/thememanager/util/UIHelper;->META_GADGET_CLOCK_PATH:Ljava/lang/String;

    .line 215
    sget-object v6, Lcom/android/thememanager/util/UIHelper;->CONTENT_GADGET_CLOCK_PATH:Ljava/lang/String;

    .line 216
    sget-object v5, Lcom/android/thememanager/util/UIHelper;->RIGHTS_GADGET_CLOCK_PATH:Ljava/lang/String;

    .line 217
    sget-object v4, Lcom/android/thememanager/util/UIHelper;->BUILDIN_IMAGE_GADGET_CLOCK_PATH:Ljava/lang/String;

    .line 218
    sget-object v3, Lcom/android/thememanager/util/UIHelper;->INDEX_GADGET_CLOCK_PATH:Ljava/lang/String;

    .line 219
    sget-object v2, Lcom/android/thememanager/util/UIHelper;->ASYNC_IMPORT_GADGET_CLOCK_PATH:Ljava/lang/String;

    goto/16 :goto_4

    .line 220
    :cond_14
    const-wide/32 v4, 0x20000

    cmp-long v2, v0, v4

    if-nez v2, :cond_15

    .line 221
    const-string v2, "REQUEST_GADGET_SIZE"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 222
    const-string v3, "/system/media/theme/.data/meta/gadget/photo_frame_%s/"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v9, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 223
    const-string v3, "/data/media/theme/.data/meta/gadget/photo_frame_%s/"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v9, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 224
    sget-object v3, Lcom/android/thememanager/util/UIHelper;->USER_GADGET_PHOTO_FRAME_PATH:Ljava/lang/String;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 225
    sget-object v8, Lcom/android/thememanager/util/UIHelper;->DOWNLOADED_GADGET_PHOTO_FRAME_PATH:Ljava/lang/String;

    .line 226
    sget-object v7, Lcom/android/thememanager/util/UIHelper;->META_GADGET_PHOTO_FRAME_PATH:Ljava/lang/String;

    .line 227
    sget-object v6, Lcom/android/thememanager/util/UIHelper;->CONTENT_GADGET_PHOTO_FRAME_PATH:Ljava/lang/String;

    .line 228
    sget-object v5, Lcom/android/thememanager/util/UIHelper;->RIGHTS_GADGET_PHOTO_FRAME_PATH:Ljava/lang/String;

    .line 229
    sget-object v4, Lcom/android/thememanager/util/UIHelper;->BUILDIN_IMAGE_GADGET_PHOTO_FRAME_PATH:Ljava/lang/String;

    .line 230
    sget-object v3, Lcom/android/thememanager/util/UIHelper;->INDEX_GADGET_PHOTO_FRAME_PATH:Ljava/lang/String;

    .line 231
    sget-object v2, Lcom/android/thememanager/util/UIHelper;->ASYNC_IMPORT_GADGET_PHOTO_FRAME_PATH:Ljava/lang/String;

    goto/16 :goto_4

    .line 232
    :cond_15
    const-wide/16 v4, -0x1

    cmp-long v2, v0, v4

    if-nez v2, :cond_16

    .line 233
    const-string v2, "/system/media/theme/.data/meta/theme/"

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 234
    const-string v2, "/data/media/theme/.data/meta/theme/"

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 235
    sget-object v2, Lcom/android/thememanager/util/UIHelper;->USER_THEME_PATH:Ljava/lang/String;

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 236
    sget-object v8, Lcom/android/thememanager/util/UIHelper;->DOWNLOADED_THEME_PATH:Ljava/lang/String;

    .line 237
    sget-object v7, Lcom/android/thememanager/util/UIHelper;->META_THEME_PATH:Ljava/lang/String;

    .line 238
    sget-object v6, Lcom/android/thememanager/util/UIHelper;->CONTENT_THEME_PATH:Ljava/lang/String;

    .line 239
    sget-object v5, Lcom/android/thememanager/util/UIHelper;->RIGHTS_THEME_PATH:Ljava/lang/String;

    .line 240
    sget-object v4, Lcom/android/thememanager/util/UIHelper;->BUILDIN_IMAGE_THEME_PATH:Ljava/lang/String;

    .line 241
    sget-object v3, Lcom/android/thememanager/util/UIHelper;->INDEX_THEME_PATH:Ljava/lang/String;

    .line 242
    sget-object v2, Lcom/android/thememanager/util/UIHelper;->ASYNC_IMPORT_THEME_PATH:Ljava/lang/String;

    goto/16 :goto_4

    .line 244
    :cond_16
    invoke-static {v3}, Lmiui/os/ExtraFileUtils;->standardizeFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 245
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "/system/media/theme/.data/meta/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v9, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 246
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "/data/media/theme/.data/meta/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v9, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 247
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/android/thememanager/util/UIHelper;->USER_RESOURCE_PATH:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 248
    sget-object v8, Lcom/android/thememanager/util/UIHelper;->DOWNLOADED_RESOURCE_PATH:Ljava/lang/String;

    .line 249
    sget-object v7, Lcom/android/thememanager/util/UIHelper;->META_RESOURCE_PATH:Ljava/lang/String;

    .line 250
    sget-object v6, Lcom/android/thememanager/util/UIHelper;->CONTENT_RESOURCE_PATH:Ljava/lang/String;

    .line 251
    sget-object v5, Lcom/android/thememanager/util/UIHelper;->RIGHTS_RESOURCE_PATH:Ljava/lang/String;

    .line 252
    sget-object v4, Lcom/android/thememanager/util/UIHelper;->BUILDIN_IMAGE_RESOURCE_PATH:Ljava/lang/String;

    .line 253
    sget-object v3, Lcom/android/thememanager/util/UIHelper;->INDEX_RESOURCE_PATH:Ljava/lang/String;

    .line 254
    sget-object v2, Lcom/android/thememanager/util/UIHelper;->ASYNC_IMPORT_RESOURCE_PATH:Ljava/lang/String;

    goto/16 :goto_4

    .line 275
    :cond_17
    const-class v0, Lcom/android/thememanager/activity/ThemeDetailActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmiui/resourcebrowser/ResourceContext;->setDetailActivityClass(Ljava/lang/String;)V

    goto/16 :goto_5

    .line 44
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static computeCurrentUsingPath(Landroid/content/Context;J)Ljava/lang/String;
    .locals 4
    .parameter "context"
    .parameter "resourceType"

    .prologue
    .line 401
    const/4 v0, 0x0

    .line 402
    .local v0, filename:Ljava/lang/String;
    const-wide/16 v2, 0x100

    cmp-long v2, p1, v2

    if-nez v2, :cond_1

    .line 403
    const/4 v2, 0x1

    invoke-static {p0, v2}, Landroid/media/RingtoneManager;->getActualDefaultRingtoneUri(Landroid/content/Context;I)Landroid/net/Uri;

    move-result-object v1

    .line 404
    .local v1, uri:Landroid/net/Uri;
    invoke-static {p0, v1}, Lmiui/resourcebrowser/util/ResourceHelper;->getPathByUri(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 427
    .end local v1           #uri:Landroid/net/Uri;
    :cond_0
    :goto_0
    return-object v0

    .line 405
    :cond_1
    const-wide/16 v2, 0x200

    cmp-long v2, p1, v2

    if-nez v2, :cond_2

    .line 406
    const/4 v2, 0x2

    invoke-static {p0, v2}, Landroid/media/RingtoneManager;->getActualDefaultRingtoneUri(Landroid/content/Context;I)Landroid/net/Uri;

    move-result-object v1

    .line 407
    .restart local v1       #uri:Landroid/net/Uri;
    invoke-static {p0, v1}, Lmiui/resourcebrowser/util/ResourceHelper;->getPathByUri(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 408
    goto :goto_0

    .end local v1           #uri:Landroid/net/Uri;
    :cond_2
    const-wide/16 v2, 0x400

    cmp-long v2, p1, v2

    if-nez v2, :cond_3

    .line 409
    const/4 v2, 0x4

    invoke-static {p0, v2}, Landroid/media/RingtoneManager;->getActualDefaultRingtoneUri(Landroid/content/Context;I)Landroid/net/Uri;

    move-result-object v1

    .line 410
    .restart local v1       #uri:Landroid/net/Uri;
    invoke-static {p0, v1}, Lmiui/resourcebrowser/util/ResourceHelper;->getPathByUri(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 411
    goto :goto_0

    .line 412
    .end local v1           #uri:Landroid/net/Uri;
    :cond_3
    invoke-static {p1, p2}, Lcom/android/thememanager/util/ThemeHelper;->loadUserPreferencePath(J)Ljava/lang/String;

    move-result-object v0

    .line 413
    if-eqz v0, :cond_5

    .line 414
    invoke-static {p1, p2}, Lcom/android/thememanager/util/UIHelper;->isImageResource(J)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 415
    const-wide/16 v2, 0x2

    cmp-long v2, p1, v2

    if-nez v2, :cond_4

    const-string v2, "wallpaper"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/WallpaperManager;

    invoke-virtual {v2}, Landroid/app/WallpaperManager;->getWallpaperInfo()Landroid/app/WallpaperInfo;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 418
    const/4 v0, 0x0

    goto :goto_0

    .line 419
    :cond_4
    invoke-static {p0, p1, p2}, Lcom/android/thememanager/util/ThemeHelper;->isWallpaperPrefOlderThanSystem(Landroid/content/Context;J)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 420
    const/4 v0, 0x0

    goto :goto_0

    .line 424
    :cond_5
    const-string v0, "/system/media/theme/.data/meta/theme/default.mrm"

    goto :goto_0
.end method

.method public static getDisplayType(J)I
    .locals 2
    .parameter "resourceType"

    .prologue
    .line 282
    const-wide/16 v0, -0x1

    cmp-long v0, p0, v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x1

    cmp-long v0, p0, v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x20

    cmp-long v0, p0, v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x80

    cmp-long v0, p0, v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x800

    cmp-long v0, p0, v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x2000

    cmp-long v0, p0, v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x1000

    cmp-long v0, p0, v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x4000

    cmp-long v0, p0, v0

    if-eqz v0, :cond_0

    const-wide/32 v0, 0x10000

    cmp-long v0, p0, v0

    if-eqz v0, :cond_0

    const-wide/32 v0, 0x20000

    cmp-long v0, p0, v0

    if-nez v0, :cond_1

    .line 287
    :cond_0
    const/16 v0, 0xb

    .line 299
    :goto_0
    return v0

    .line 288
    :cond_1
    const-wide/16 v0, 0x2

    cmp-long v0, p0, v0

    if-nez v0, :cond_2

    .line 289
    const/4 v0, 0x6

    goto :goto_0

    .line 290
    :cond_2
    const-wide/16 v0, 0x4

    cmp-long v0, p0, v0

    if-nez v0, :cond_3

    .line 291
    const/16 v0, 0x9

    goto :goto_0

    .line 292
    :cond_3
    invoke-static {p0, p1}, Lcom/android/thememanager/util/UIHelper;->isFontResource(J)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 293
    const/4 v0, 0x7

    goto :goto_0

    .line 294
    :cond_4
    const-wide/16 v0, 0x8

    cmp-long v0, p0, v0

    if-nez v0, :cond_5

    .line 295
    const/16 v0, 0x8

    goto :goto_0

    .line 296
    :cond_5
    invoke-static {p0, p1}, Lcom/android/thememanager/util/UIHelper;->isAudioResource(J)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 297
    const/4 v0, 0x4

    goto :goto_0

    .line 299
    :cond_6
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static getResourceExtension(J)Ljava/lang/String;
    .locals 1
    .parameter "resourceType"

    .prologue
    .line 350
    invoke-static {p0, p1}, Lcom/android/thememanager/util/UIHelper;->isBundleResource(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 351
    const-string v0, ".mtz"

    .line 359
    :goto_0
    return-object v0

    .line 352
    :cond_0
    invoke-static {p0, p1}, Lcom/android/thememanager/util/UIHelper;->isImageResource(J)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 353
    const-string v0, ".jpg"

    goto :goto_0

    .line 354
    :cond_1
    invoke-static {p0, p1}, Lcom/android/thememanager/util/UIHelper;->isAudioResource(J)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 355
    const-string v0, ".mp3"

    goto :goto_0

    .line 356
    :cond_2
    invoke-static {p0, p1}, Lcom/android/thememanager/util/UIHelper;->isZipResource(J)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {p0, p1}, Lcom/android/thememanager/util/UIHelper;->isFontResource(J)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 357
    :cond_3
    const-string v0, ".mtz"

    goto :goto_0

    .line 359
    :cond_4
    const-string v0, ".mtz"

    goto :goto_0
.end method

.method public static getResourceFormat(J)I
    .locals 1
    .parameter "resourceType"

    .prologue
    .line 336
    invoke-static {p0, p1}, Lcom/android/thememanager/util/UIHelper;->isBundleResource(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 337
    const/4 v0, 0x1

    .line 345
    :goto_0
    return v0

    .line 338
    :cond_0
    invoke-static {p0, p1}, Lcom/android/thememanager/util/UIHelper;->isImageResource(J)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 339
    const/4 v0, 0x2

    goto :goto_0

    .line 340
    :cond_1
    invoke-static {p0, p1}, Lcom/android/thememanager/util/UIHelper;->isAudioResource(J)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 341
    const/4 v0, 0x3

    goto :goto_0

    .line 342
    :cond_2
    invoke-static {p0, p1}, Lcom/android/thememanager/util/UIHelper;->isZipResource(J)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 343
    const/4 v0, 0x4

    goto :goto_0

    .line 345
    :cond_3
    const/4 v0, 0x5

    goto :goto_0
.end method

.method public static getRingtoneType(J)I
    .locals 3
    .parameter "resourceType"

    .prologue
    const/4 v0, 0x1

    .line 387
    const-wide/16 v1, 0x100

    cmp-long v1, p0, v1

    if-nez v1, :cond_1

    .line 396
    :cond_0
    :goto_0
    return v0

    .line 389
    :cond_1
    const-wide/16 v1, 0x200

    cmp-long v1, p0, v1

    if-nez v1, :cond_2

    .line 390
    const/4 v0, 0x2

    goto :goto_0

    .line 391
    :cond_2
    const-wide/16 v1, 0x400

    cmp-long v1, p0, v1

    if-nez v1, :cond_3

    .line 392
    const/4 v0, 0x4

    goto :goto_0

    .line 393
    :cond_3
    const-wide/16 v1, 0x40

    cmp-long v1, p0, v1

    if-nez v1, :cond_0

    .line 394
    const/16 v0, 0x20

    goto :goto_0
.end method

.method public static isAudioResource(J)Z
    .locals 2
    .parameter "resourceType"

    .prologue
    .line 316
    const-wide/16 v0, 0x40

    cmp-long v0, p0, v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x100

    cmp-long v0, p0, v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x200

    cmp-long v0, p0, v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x400

    cmp-long v0, p0, v0

    if-eqz v0, :cond_0

    const-wide/32 v0, 0x8000

    cmp-long v0, p0, v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isBundleResource(J)Z
    .locals 2
    .parameter "resourceType"

    .prologue
    .line 304
    const-wide/16 v0, -0x1

    cmp-long v0, p0, v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isCategorySupported(J)Z
    .locals 2
    .parameter "resourceType"

    .prologue
    .line 373
    const-wide/32 v0, 0x8000

    cmp-long v0, p0, v0

    if-eqz v0, :cond_0

    invoke-static {p0, p1}, Lcom/android/thememanager/util/UIHelper;->isAudioResource(J)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-static {p0, p1}, Lcom/android/thememanager/util/UIHelper;->isImageResource(J)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p0, p1}, Lcom/android/thememanager/util/UIHelper;->isFontResource(J)Z

    move-result v0

    if-nez v0, :cond_1

    const-wide/32 v0, 0x10000

    cmp-long v0, p0, v0

    if-eqz v0, :cond_1

    const-wide/32 v0, 0x20000

    cmp-long v0, p0, v0

    if-nez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isFontResource(J)Z
    .locals 2
    .parameter "resourceType"

    .prologue
    .line 312
    const-wide/16 v0, 0x10

    cmp-long v0, p0, v0

    if-eqz v0, :cond_0

    const-wide/32 v0, 0x40000

    cmp-long v0, p0, v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isImageResource(J)Z
    .locals 2
    .parameter "resourceType"

    .prologue
    .line 308
    const-wide/16 v0, 0x2

    cmp-long v0, p0, v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x4

    cmp-long v0, p0, v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isPlatformSupported(J)Z
    .locals 1
    .parameter "resourceType"

    .prologue
    .line 383
    invoke-static {p0, p1}, Lcom/android/thememanager/util/UIHelper;->isAudioResource(J)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0, p1}, Lcom/android/thememanager/util/UIHelper;->isImageResource(J)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isRecommendSupported(J)Z
    .locals 1
    .parameter "resourceType"

    .prologue
    .line 369
    const/4 v0, 0x1

    return v0
.end method

.method public static isSelfDescribing(J)Z
    .locals 2
    .parameter "resourceType"

    .prologue
    .line 364
    invoke-static {p0, p1}, Lcom/android/thememanager/util/UIHelper;->getResourceFormat(J)I

    move-result v0

    .line 365
    .local v0, format:I
    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isVersionSupported(J)Z
    .locals 1
    .parameter "resourceType"

    .prologue
    .line 379
    invoke-static {p0, p1}, Lcom/android/thememanager/util/UIHelper;->isAudioResource(J)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0, p1}, Lcom/android/thememanager/util/UIHelper;->isImageResource(J)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isZipResource(J)Z
    .locals 2
    .parameter "resourceType"

    .prologue
    .line 322
    const-wide/16 v0, 0x1

    cmp-long v0, p0, v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x8

    cmp-long v0, p0, v0

    if-eqz v0, :cond_0

    invoke-static {p0, p1}, Lcom/android/thememanager/util/UIHelper;->isFontResource(J)Z

    move-result v0

    if-nez v0, :cond_0

    const-wide/16 v0, 0x1000

    cmp-long v0, p0, v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x20

    cmp-long v0, p0, v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x2000

    cmp-long v0, p0, v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x4000

    cmp-long v0, p0, v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x800

    cmp-long v0, p0, v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x80

    cmp-long v0, p0, v0

    if-eqz v0, :cond_0

    const-wide/32 v0, 0x10000

    cmp-long v0, p0, v0

    if-eqz v0, :cond_0

    const-wide/32 v0, 0x20000

    cmp-long v0, p0, v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static supportImportMenu(J)Z
    .locals 2
    .parameter "resourceType"

    .prologue
    .line 331
    const-wide/32 v0, 0x8000

    cmp-long v0, p0, v0

    if-eqz v0, :cond_0

    invoke-static {p0, p1}, Lcom/android/thememanager/util/UIHelper;->isAudioResource(J)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p0, p1}, Lcom/android/thememanager/util/UIHelper;->isImageResource(J)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
