.class public interface abstract Lcom/android/thememanager/ThemeResourceConstants;
.super Ljava/lang/Object;
.source "ThemeResourceConstants.java"

# interfaces
.implements Lmiui/resourcebrowser/ResourceConstants;


# static fields
.field public static final ASYNC_IMPORT_BOOT_AUDIO_PATH:Ljava/lang/String;

.field public static final ASYNC_IMPORT_GADGET_CLOCK_PATH:Ljava/lang/String;

.field public static final ASYNC_IMPORT_GADGET_PHOTO_FRAME_PATH:Ljava/lang/String;

.field public static final ASYNC_IMPORT_RESOURCE_PATH:Ljava/lang/String;

.field public static final ASYNC_IMPORT_THEME_PATH:Ljava/lang/String;

.field public static final BUILDIN_IMAGE_BOOT_AUDIO_PATH:Ljava/lang/String;

.field public static final BUILDIN_IMAGE_GADGET_CLOCK_PATH:Ljava/lang/String;

.field public static final BUILDIN_IMAGE_GADGET_PHOTO_FRAME_PATH:Ljava/lang/String;

.field public static final BUILDIN_IMAGE_RESOURCE_PATH:Ljava/lang/String;

.field public static final BUILDIN_IMAGE_THEME_PATH:Ljava/lang/String;

.field public static final COMPONENT_CODES:[Ljava/lang/String;

.field public static final COMPONENT_IDENTITIES:[Ljava/lang/String;

.field public static final COMPONENT_PLATFORMS:[I

.field public static final COMPONENT_PREVIEW_PREFIX:[Ljava/lang/String;

.field public static final COMPONENT_PREVIEW_SHOW_ORDER:[J

.field public static final COMPONENT_SELECT_ORDER:[J

.field public static final COMPONENT_STAMPS:[Ljava/lang/String;

.field public static final COMPONENT_TITLES:[I

.field public static final CONTENT_BOOT_AUDIO_PATH:Ljava/lang/String;

.field public static final CONTENT_GADGET_CLOCK_PATH:Ljava/lang/String;

.field public static final CONTENT_GADGET_PHOTO_FRAME_PATH:Ljava/lang/String;

.field public static final CONTENT_RESOURCE_PATH:Ljava/lang/String;

.field public static final CONTENT_THEME_PATH:Ljava/lang/String;

.field public static final DEFAULT_ALARM_FILE_PATH:Ljava/lang/String;

.field public static final DEFAULT_NOTIFICATION_FILE_PATH:Ljava/lang/String;

.field public static final DEFAULT_RINGTONE_FILE_PATH:Ljava/lang/String;

.field public static final DOWNLOADED_BOOT_AUDIO_PATH:Ljava/lang/String;

.field public static final DOWNLOADED_GADGET_CLOCK_PATH:Ljava/lang/String;

.field public static final DOWNLOADED_GADGET_PHOTO_FRAME_PATH:Ljava/lang/String;

.field public static final DOWNLOADED_RESOURCE_PATH:Ljava/lang/String;

.field public static final DOWNLOADED_THEME_PATH:Ljava/lang/String;

.field public static final INDEX_BOOT_AUDIO_PATH:Ljava/lang/String;

.field public static final INDEX_GADGET_CLOCK_PATH:Ljava/lang/String;

.field public static final INDEX_GADGET_PHOTO_FRAME_PATH:Ljava/lang/String;

.field public static final INDEX_RESOURCE_PATH:Ljava/lang/String;

.field public static final INDEX_THEME_PATH:Ljava/lang/String;

.field public static final META_BOOT_AUDIO_PATH:Ljava/lang/String;

.field public static final META_GADGET_CLOCK_PATH:Ljava/lang/String;

.field public static final META_GADGET_PHOTO_FRAME_PATH:Ljava/lang/String;

.field public static final META_RESOURCE_PATH:Ljava/lang/String;

.field public static final META_THEME_PATH:Ljava/lang/String;

.field public static final RIGHTS_BOOT_AUDIO_PATH:Ljava/lang/String;

.field public static final RIGHTS_GADGET_CLOCK_PATH:Ljava/lang/String;

.field public static final RIGHTS_GADGET_PHOTO_FRAME_PATH:Ljava/lang/String;

.field public static final RIGHTS_RESOURCE_PATH:Ljava/lang/String;

.field public static final RIGHTS_THEME_PATH:Ljava/lang/String;

.field public static final RUNTIME_PATHS:[Ljava/lang/String;

.field public static final USER_BOOT_AUDIO_PATH:Ljava/lang/String;

.field public static final USER_GADGET_CLOCK_PATH:Ljava/lang/String;

.field public static final USER_GADGET_PHOTO_FRAME_PATH:Ljava/lang/String;

.field public static final USER_RESOURCE_PATH:Ljava/lang/String;

.field public static final USER_THEME_PATH:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/16 v6, 0x12

    const/16 v5, 0xc

    const/16 v4, 0x13

    const/4 v3, 0x0

    .line 65
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "framework-res"

    aput-object v1, v0, v7

    const/4 v1, 0x1

    const-string v2, "wallpaper/default_wallpaper.jpg"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "wallpaper/default_lock_wallpaper.jpg"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "icons"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "fonts/Roboto-Regular.ttf"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "boots/bootanimation.zip"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "boots/bootaudio.mp3"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "com.android.mms"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "ringtones/ringtone.mp3"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "ringtones/notification.mp3"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "ringtones/alarm.mp3"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "com.android.contacts"

    aput-object v2, v0, v1

    const-string v1, "lockscreen"

    aput-object v1, v0, v5

    const/16 v1, 0xd

    const-string v2, "com.android.systemui"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "com.miui.home"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "audioeffect"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "clock_"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "photoframe_"

    aput-object v2, v0, v1

    const-string v1, "fonts/DroidSansFallback.ttf"

    aput-object v1, v0, v6

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->COMPONENT_IDENTITIES:[Ljava/lang/String;

    .line 108
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "/data/system/theme/framework-res"

    aput-object v1, v0, v7

    const/4 v1, 0x1

    const-string v2, "/data/data/com.android.settings/files/wallpaper"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "/data/system/theme/lock_wallpaper"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "/data/system/theme/icons"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "/data/system/theme/fonts/Roboto-Regular.ttf"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "/data/local/bootanimation.zip"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "/data/system/theme/boots/bootaudio.mp3"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "/data/system/theme/com.android.mms"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "/data/system/theme/ringtones/ringtone.mp3"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "/data/system/theme/ringtones/notification.mp3"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "/data/system/theme/ringtones/alarm.mp3"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "/data/system/theme/com.android.contacts"

    aput-object v2, v0, v1

    const-string v1, "/data/system/theme/lockscreen"

    aput-object v1, v0, v5

    const/16 v1, 0xd

    const-string v2, "/data/system/theme/com.android.systemui"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "/data/system/theme/com.miui.home"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "/data/system/theme/audioeffect"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "/data/system/theme/clock_"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "/data/system/theme/photoframe_"

    aput-object v2, v0, v1

    const-string v1, "/data/system/theme/fonts/DroidSansFallback.ttf"

    aput-object v1, v0, v6

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->RUNTIME_PATHS:[Ljava/lang/String;

    .line 151
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "framework"

    aput-object v1, v0, v7

    const/4 v1, 0x1

    const-string v2, "wallpaper"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "lockscreen"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "icons"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "fonts"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "bootanimation"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "bootaudio"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "mms"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "ringtone"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "notification"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "alarm"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "contact"

    aput-object v2, v0, v1

    const-string v1, "lockstyle"

    aput-object v1, v0, v5

    const/16 v1, 0xd

    const-string v2, "statusbar"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "launcher"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "audioeffect"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "clock"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "photoframe"

    aput-object v2, v0, v1

    const-string v1, "fonts_fallback"

    aput-object v1, v0, v6

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->COMPONENT_CODES:[Ljava/lang/String;

    .line 194
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "FrameWork"

    aput-object v1, v0, v7

    const/4 v1, 0x1

    const-string v2, "DeskWallpaper"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "LockScreenWallpaper"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "Icon"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "Font"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "BootAnimation"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "BootAudio"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "Mms"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "RingtoneAudio"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "NotificationAudio"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "AlarmAudio"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "Contact"

    aput-object v2, v0, v1

    const-string v1, "LockStyle"

    aput-object v1, v0, v5

    const/16 v1, 0xd

    const-string v2, "StatusBar"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "Launcher"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "AudioEffect"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "Clock"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "PhotoFrame"

    aput-object v2, v0, v1

    const-string v1, "FontFallback"

    aput-object v1, v0, v6

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->COMPONENT_STAMPS:[Ljava/lang/String;

    .line 218
    new-array v0, v4, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->COMPONENT_PLATFORMS:[I

    .line 240
    new-array v0, v4, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->COMPONENT_TITLES:[I

    .line 266
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "preview_"

    aput-object v1, v0, v7

    const/4 v1, 0x1

    aput-object v3, v0, v1

    const/4 v1, 0x2

    aput-object v3, v0, v1

    const/4 v1, 0x3

    const-string v2, "preview_icons_"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "preview_fonts_"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "preview_animation_"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    aput-object v3, v0, v1

    const/4 v1, 0x7

    const-string v2, "preview_mms_"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    aput-object v3, v0, v1

    const/16 v1, 0x9

    aput-object v3, v0, v1

    const/16 v1, 0xa

    aput-object v3, v0, v1

    const/16 v1, 0xb

    const-string v2, "preview_contact_"

    aput-object v2, v0, v1

    const-string v1, "preview_lockscreen_"

    aput-object v1, v0, v5

    const/16 v1, 0xd

    const-string v2, "preview_statusbar_"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "preview_launcher_"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    aput-object v3, v0, v1

    const/16 v1, 0x10

    const-string v2, "preview_clock_"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "preview_photo_frame_"

    aput-object v2, v0, v1

    aput-object v3, v0, v6

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->COMPONENT_PREVIEW_PREFIX:[Ljava/lang/String;

    .line 288
    new-array v0, v5, [J

    fill-array-data v0, :array_2

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->COMPONENT_PREVIEW_SHOW_ORDER:[J

    .line 303
    new-array v0, v6, [J

    fill-array-data v0, :array_3

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->COMPONENT_SELECT_ORDER:[J

    .line 338
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/thememanager/ThemeResourceConstants;->USER_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "theme/.db/meta/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->USER_RESOURCE_PATH:Ljava/lang/String;

    .line 339
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/thememanager/ThemeResourceConstants;->USER_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "theme/.db/meta/theme/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->USER_THEME_PATH:Ljava/lang/String;

    .line 340
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/thememanager/ThemeResourceConstants;->USER_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "theme/.db/meta/clock_%s/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->USER_GADGET_CLOCK_PATH:Ljava/lang/String;

    .line 341
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/thememanager/ThemeResourceConstants;->USER_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "theme/.db/meta/photo_frame_%s/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->USER_GADGET_PHOTO_FRAME_PATH:Ljava/lang/String;

    .line 342
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/thememanager/ThemeResourceConstants;->USER_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ringtone/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->USER_BOOT_AUDIO_PATH:Ljava/lang/String;

    .line 344
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/thememanager/ThemeResourceConstants;->DOWNLOADED_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "theme/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->DOWNLOADED_RESOURCE_PATH:Ljava/lang/String;

    .line 345
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/thememanager/ThemeResourceConstants;->DOWNLOADED_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "theme/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->DOWNLOADED_THEME_PATH:Ljava/lang/String;

    .line 346
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/thememanager/ThemeResourceConstants;->DOWNLOADED_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "theme/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->DOWNLOADED_GADGET_CLOCK_PATH:Ljava/lang/String;

    .line 347
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/thememanager/ThemeResourceConstants;->DOWNLOADED_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "theme/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->DOWNLOADED_GADGET_PHOTO_FRAME_PATH:Ljava/lang/String;

    .line 348
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/thememanager/ThemeResourceConstants;->DOWNLOADED_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ringtone/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->DOWNLOADED_BOOT_AUDIO_PATH:Ljava/lang/String;

    .line 350
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/thememanager/ThemeResourceConstants;->META_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "theme/.db/meta/theme/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->META_RESOURCE_PATH:Ljava/lang/String;

    .line 351
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/thememanager/ThemeResourceConstants;->META_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "theme/.db/meta/theme/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->META_THEME_PATH:Ljava/lang/String;

    .line 352
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/thememanager/ThemeResourceConstants;->META_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "theme/.db/meta/theme/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->META_GADGET_CLOCK_PATH:Ljava/lang/String;

    .line 353
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/thememanager/ThemeResourceConstants;->META_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "theme/.db/meta/theme/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->META_GADGET_PHOTO_FRAME_PATH:Ljava/lang/String;

    .line 354
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/thememanager/ThemeResourceConstants;->META_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ringtone/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->META_BOOT_AUDIO_PATH:Ljava/lang/String;

    .line 356
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/thememanager/ThemeResourceConstants;->CONTENT_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "theme/.db/content/theme/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->CONTENT_RESOURCE_PATH:Ljava/lang/String;

    .line 357
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/thememanager/ThemeResourceConstants;->CONTENT_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "theme/.db/content/theme/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->CONTENT_THEME_PATH:Ljava/lang/String;

    .line 358
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/thememanager/ThemeResourceConstants;->CONTENT_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "theme/.db/content/theme/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->CONTENT_GADGET_CLOCK_PATH:Ljava/lang/String;

    .line 359
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/thememanager/ThemeResourceConstants;->CONTENT_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "theme/.db/content/theme/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->CONTENT_GADGET_PHOTO_FRAME_PATH:Ljava/lang/String;

    .line 360
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/thememanager/ThemeResourceConstants;->CONTENT_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ringtone/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->CONTENT_BOOT_AUDIO_PATH:Ljava/lang/String;

    .line 362
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/thememanager/ThemeResourceConstants;->RIGHTS_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "theme/.db/rights/theme/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->RIGHTS_RESOURCE_PATH:Ljava/lang/String;

    .line 363
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/thememanager/ThemeResourceConstants;->RIGHTS_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "theme/.db/rights/theme/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->RIGHTS_THEME_PATH:Ljava/lang/String;

    .line 364
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/thememanager/ThemeResourceConstants;->RIGHTS_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "theme/.db/rights/theme/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->RIGHTS_GADGET_CLOCK_PATH:Ljava/lang/String;

    .line 365
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/thememanager/ThemeResourceConstants;->RIGHTS_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "theme/.db/rights/theme/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->RIGHTS_GADGET_PHOTO_FRAME_PATH:Ljava/lang/String;

    .line 366
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/thememanager/ThemeResourceConstants;->RIGHTS_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ringtone/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->RIGHTS_BOOT_AUDIO_PATH:Ljava/lang/String;

    .line 368
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/thememanager/ThemeResourceConstants;->BUILDIN_IMAGE_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "theme/.db/preview/theme/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->BUILDIN_IMAGE_RESOURCE_PATH:Ljava/lang/String;

    .line 369
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/thememanager/ThemeResourceConstants;->BUILDIN_IMAGE_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "theme/.db/preview/theme/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->BUILDIN_IMAGE_THEME_PATH:Ljava/lang/String;

    .line 370
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/thememanager/ThemeResourceConstants;->BUILDIN_IMAGE_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "theme/.db/preview/theme/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->BUILDIN_IMAGE_GADGET_CLOCK_PATH:Ljava/lang/String;

    .line 371
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/thememanager/ThemeResourceConstants;->BUILDIN_IMAGE_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "theme/.db/preview/theme/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->BUILDIN_IMAGE_GADGET_PHOTO_FRAME_PATH:Ljava/lang/String;

    .line 372
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/thememanager/ThemeResourceConstants;->BUILDIN_IMAGE_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ringtone/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->BUILDIN_IMAGE_BOOT_AUDIO_PATH:Ljava/lang/String;

    .line 374
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/thememanager/ThemeResourceConstants;->INDEX_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "theme/.db/index/theme/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->INDEX_RESOURCE_PATH:Ljava/lang/String;

    .line 375
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/thememanager/ThemeResourceConstants;->INDEX_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "theme/.db/index/theme/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->INDEX_THEME_PATH:Ljava/lang/String;

    .line 376
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/thememanager/ThemeResourceConstants;->INDEX_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "theme/.db/index/theme/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->INDEX_GADGET_CLOCK_PATH:Ljava/lang/String;

    .line 377
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/thememanager/ThemeResourceConstants;->INDEX_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "theme/.db/index/theme/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->INDEX_GADGET_PHOTO_FRAME_PATH:Ljava/lang/String;

    .line 378
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/thememanager/ThemeResourceConstants;->INDEX_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ringtone/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->INDEX_BOOT_AUDIO_PATH:Ljava/lang/String;

    .line 380
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/thememanager/ThemeResourceConstants;->ASYNC_IMPORT_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "theme/.db/import/theme/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->ASYNC_IMPORT_RESOURCE_PATH:Ljava/lang/String;

    .line 381
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/thememanager/ThemeResourceConstants;->ASYNC_IMPORT_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "theme/.db/import/theme/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->ASYNC_IMPORT_THEME_PATH:Ljava/lang/String;

    .line 382
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/thememanager/ThemeResourceConstants;->ASYNC_IMPORT_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "theme/.db/import/theme/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->ASYNC_IMPORT_GADGET_CLOCK_PATH:Ljava/lang/String;

    .line 383
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/thememanager/ThemeResourceConstants;->ASYNC_IMPORT_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "theme/.db/import/theme/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->ASYNC_IMPORT_GADGET_PHOTO_FRAME_PATH:Ljava/lang/String;

    .line 384
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/thememanager/ThemeResourceConstants;->ASYNC_IMPORT_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ringtone/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->ASYNC_IMPORT_BOOT_AUDIO_PATH:Ljava/lang/String;

    .line 387
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/system/media/audio/ringtones/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ro.config.ringtone"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->DEFAULT_RINGTONE_FILE_PATH:Ljava/lang/String;

    .line 388
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/system/media/audio/notifications/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ro.config.notification_sound"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->DEFAULT_NOTIFICATION_FILE_PATH:Ljava/lang/String;

    .line 389
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/system/media/audio/alarms/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ro.config.alarm_alert"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/thememanager/ThemeResourceConstants;->DEFAULT_ALARM_FILE_PATH:Ljava/lang/String;

    return-void

    .line 218
    nop

    :array_0
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
    .end array-data

    .line 240
    :array_1
    .array-data 0x4
        0x10t 0x0t 0xbt 0x7ft
        0x12t 0x0t 0xbt 0x7ft
        0x13t 0x0t 0xbt 0x7ft
        0x14t 0x0t 0xbt 0x7ft
        0x17t 0x0t 0xbt 0x7ft
        0x15t 0x0t 0xbt 0x7ft
        0x16t 0x0t 0xbt 0x7ft
        0x11t 0x0t 0xbt 0x7ft
        0x18t 0x0t 0xbt 0x7ft
        0x19t 0x0t 0xbt 0x7ft
        0x1at 0x0t 0xbt 0x7ft
        0x1bt 0x0t 0xbt 0x7ft
        0x1ct 0x0t 0xbt 0x7ft
        0x1dt 0x0t 0xbt 0x7ft
        0x1et 0x0t 0xbt 0x7ft
        0x1ft 0x0t 0xbt 0x7ft
        0x20t 0x0t 0xbt 0x7ft
        0x21t 0x0t 0xbt 0x7ft
        0x17t 0x0t 0xbt 0x7ft
    .end array-data

    .line 288
    :array_2
    .array-data 0x8
        0x0t 0x10t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x0t 0x40t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x0t 0x20t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x0t 0x8t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x80t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x1t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x2t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x20t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x10t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x0t 0x80t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
    .end array-data

    .line 303
    :array_3
    .array-data 0x8
        0x1t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x20t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x40t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x0t 0x10t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x0t 0x20t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x0t 0x40t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x1t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x2t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x10t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x80t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x0t 0x8t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x0t 0x1t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x0t 0x2t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x0t 0x4t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x0t 0x80t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
    .end array-data
.end method
