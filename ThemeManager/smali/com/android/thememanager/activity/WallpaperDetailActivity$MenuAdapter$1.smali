.class Lcom/android/thememanager/activity/WallpaperDetailActivity$MenuAdapter$1;
.super Ljava/lang/Object;
.source "WallpaperDetailActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/thememanager/activity/WallpaperDetailActivity$MenuAdapter;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/thememanager/activity/WallpaperDetailActivity$MenuAdapter;


# direct methods
.method constructor <init>(Lcom/android/thememanager/activity/WallpaperDetailActivity$MenuAdapter;)V
    .locals 0
    .parameter

    .prologue
    .line 772
    iput-object p1, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity$MenuAdapter$1;->this$1:Lcom/android/thememanager/activity/WallpaperDetailActivity$MenuAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .parameter "dialog"
    .parameter "which"

    .prologue
    .line 775
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity$MenuAdapter$1;->this$1:Lcom/android/thememanager/activity/WallpaperDetailActivity$MenuAdapter;

    iget-object v0, v0, Lcom/android/thememanager/activity/WallpaperDetailActivity$MenuAdapter;->this$0:Lcom/android/thememanager/activity/WallpaperDetailActivity;

    #getter for: Lcom/android/thememanager/activity/WallpaperDetailActivity;->mResController:Lmiui/resourcebrowser/controller/ResourceController;
    invoke-static {v0}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->access$3600(Lcom/android/thememanager/activity/WallpaperDetailActivity;)Lmiui/resourcebrowser/controller/ResourceController;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/resourcebrowser/controller/ResourceController;->getLocalDataManager()Lmiui/resourcebrowser/controller/LocalDataManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity$MenuAdapter$1;->this$1:Lcom/android/thememanager/activity/WallpaperDetailActivity$MenuAdapter;

    iget-object v1, v1, Lcom/android/thememanager/activity/WallpaperDetailActivity$MenuAdapter;->this$0:Lcom/android/thememanager/activity/WallpaperDetailActivity;

    #getter for: Lcom/android/thememanager/activity/WallpaperDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;
    invoke-static {v1}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->access$3500(Lcom/android/thememanager/activity/WallpaperDetailActivity;)Lmiui/resourcebrowser/model/Resource;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/controller/LocalDataManager;->removeResource(Lmiui/resourcebrowser/model/Resource;)Z

    .line 776
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity$MenuAdapter$1;->this$1:Lcom/android/thememanager/activity/WallpaperDetailActivity$MenuAdapter;

    iget-object v0, v0, Lcom/android/thememanager/activity/WallpaperDetailActivity$MenuAdapter;->this$0:Lcom/android/thememanager/activity/WallpaperDetailActivity;

    #getter for: Lcom/android/thememanager/activity/WallpaperDetailActivity;->mIsOnlineResourceSet:Z
    invoke-static {v0}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->access$3700(Lcom/android/thememanager/activity/WallpaperDetailActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 777
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity$MenuAdapter$1;->this$1:Lcom/android/thememanager/activity/WallpaperDetailActivity$MenuAdapter;

    iget-object v0, v0, Lcom/android/thememanager/activity/WallpaperDetailActivity$MenuAdapter;->this$0:Lcom/android/thememanager/activity/WallpaperDetailActivity;

    invoke-virtual {v0}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->setResourceStatus()V

    .line 781
    :goto_0
    return-void

    .line 779
    :cond_0
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity$MenuAdapter$1;->this$1:Lcom/android/thememanager/activity/WallpaperDetailActivity$MenuAdapter;

    iget-object v0, v0, Lcom/android/thememanager/activity/WallpaperDetailActivity$MenuAdapter;->this$0:Lcom/android/thememanager/activity/WallpaperDetailActivity;

    invoke-virtual {v0}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->finish()V

    goto :goto_0
.end method
