.class public Lcom/android/thememanager/activity/ThemeSearchListFragment;
.super Lmiui/resourcebrowser/activity/ResourceSearchListFragment;
.source "ThemeSearchListFragment.java"

# interfaces
.implements Lcom/android/thememanager/ThemeResourceConstants;


# instance fields
.field private mResourceType:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lmiui/resourcebrowser/activity/ResourceSearchListFragment;-><init>()V

    return-void
.end method

.method private refreshCurrentUsingFlags()V
    .locals 4

    .prologue
    .line 39
    iget-object v1, p0, Lcom/android/thememanager/activity/ThemeSearchListFragment;->mActivity:Landroid/app/Activity;

    iget-wide v2, p0, Lcom/android/thememanager/activity/ThemeSearchListFragment;->mResourceType:J

    invoke-static {v1, v2, v3}, Lcom/android/thememanager/util/UIHelper;->computeCurrentUsingPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    .line 40
    .local v0, usingPath:Ljava/lang/String;
    iget-object v1, p0, Lcom/android/thememanager/activity/ThemeSearchListFragment;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v1, v0}, Lmiui/resourcebrowser/ResourceContext;->setCurrentUsingPath(Ljava/lang/String;)V

    .line 41
    iget-object v1, p0, Lcom/android/thememanager/activity/ThemeSearchListFragment;->mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;

    invoke-virtual {v1}, Lmiui/resourcebrowser/activity/ResourceAdapter;->notifyDataSetChanged()V

    .line 42
    return-void
.end method


# virtual methods
.method protected getBatchOperationHandler()Lmiui/resourcebrowser/util/BatchResourceHandler;
    .locals 6

    .prologue
    .line 19
    iget-wide v0, p0, Lcom/android/thememanager/activity/ThemeSearchListFragment;->mResourceType:J

    invoke-static {v0, v1}, Lcom/android/thememanager/util/UIHelper;->isAudioResource(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 20
    new-instance v0, Lcom/android/thememanager/util/ThemeAudioBatchHandler;

    iget-object v2, p0, Lcom/android/thememanager/activity/ThemeSearchListFragment;->mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;

    iget-object v3, p0, Lcom/android/thememanager/activity/ThemeSearchListFragment;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    iget-wide v4, p0, Lcom/android/thememanager/activity/ThemeSearchListFragment;->mResourceType:J

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/thememanager/util/ThemeAudioBatchHandler;-><init>(Lmiui/resourcebrowser/activity/ResourceListFragment;Lmiui/resourcebrowser/activity/ResourceAdapter;Lmiui/resourcebrowser/ResourceContext;J)V

    .line 22
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lmiui/resourcebrowser/activity/ResourceSearchListFragment;->getBatchOperationHandler()Lmiui/resourcebrowser/util/BatchResourceHandler;

    move-result-object v0

    goto :goto_0
.end method

.method protected initParams()V
    .locals 4

    .prologue
    .line 27
    iget-object v0, p0, Lcom/android/thememanager/activity/ThemeSearchListFragment;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    const-string v1, "EXTRA_CTX_RESOURCE_TYPE"

    const-wide/16 v2, -0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmiui/resourcebrowser/ResourceContext;->getExtraMeta(Ljava/lang/String;Ljava/io/Serializable;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/thememanager/activity/ThemeSearchListFragment;->mResourceType:J

    .line 28
    iget-object v0, p0, Lcom/android/thememanager/activity/ThemeSearchListFragment;->mActivity:Landroid/app/Activity;

    iget-wide v1, p0, Lcom/android/thememanager/activity/ThemeSearchListFragment;->mResourceType:J

    invoke-static {v0, v1, v2}, Lcom/android/thememanager/util/ThemeHelper;->setMusicVolumeType(Landroid/app/Activity;J)V

    .line 29
    invoke-super {p0}, Lmiui/resourcebrowser/activity/ResourceSearchListFragment;->initParams()V

    .line 30
    return-void
.end method

.method protected onVisible()V
    .locals 0

    .prologue
    .line 34
    invoke-super {p0}, Lmiui/resourcebrowser/activity/ResourceSearchListFragment;->onVisible()V

    .line 35
    invoke-direct {p0}, Lcom/android/thememanager/activity/ThemeSearchListFragment;->refreshCurrentUsingFlags()V

    .line 36
    return-void
.end method
