.class Lcom/android/thememanager/activity/WallpaperDetailActivity$6;
.super Ljava/lang/Object;
.source "WallpaperDetailActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/thememanager/activity/WallpaperDetailActivity;->setupUI()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/thememanager/activity/WallpaperDetailActivity;


# direct methods
.method constructor <init>(Lcom/android/thememanager/activity/WallpaperDetailActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 175
    iput-object p1, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity$6;->this$0:Lcom/android/thememanager/activity/WallpaperDetailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10
    .parameter "v"

    .prologue
    const/4 v4, 0x0

    const v5, 0x60c0023

    const/4 v9, 0x0

    .line 178
    move-object v3, p1

    check-cast v3, Landroid/widget/TextView;

    .line 179
    .local v3, tv:Landroid/widget/TextView;
    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    iget-object v7, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity$6;->this$0:Lcom/android/thememanager/activity/WallpaperDetailActivity;

    const v8, 0x60c0019

    invoke-virtual {v7, v8}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    if-ne v6, v7, :cond_0

    .line 180
    iget-object v4, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity$6;->this$0:Lcom/android/thememanager/activity/WallpaperDetailActivity;

    iget-object v5, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity$6;->this$0:Lcom/android/thememanager/activity/WallpaperDetailActivity;

    #getter for: Lcom/android/thememanager/activity/WallpaperDetailActivity;->mResourceType:J
    invoke-static {v5}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->access$200(Lcom/android/thememanager/activity/WallpaperDetailActivity;)J

    move-result-wide v5

    iget-object v7, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity$6;->this$0:Lcom/android/thememanager/activity/WallpaperDetailActivity;

    #getter for: Lcom/android/thememanager/activity/WallpaperDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;
    invoke-static {v7}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->access$300(Lcom/android/thememanager/activity/WallpaperDetailActivity;)Lmiui/resourcebrowser/model/Resource;

    move-result-object v7

    #calls: Lcom/android/thememanager/activity/WallpaperDetailActivity;->doApplyWallpaper(JLmiui/resourcebrowser/model/Resource;)V
    invoke-static {v4, v5, v6, v7}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->access$400(Lcom/android/thememanager/activity/WallpaperDetailActivity;JLmiui/resourcebrowser/model/Resource;)V

    .line 208
    :goto_0
    return-void

    .line 182
    :cond_0
    iget-object v6, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity$6;->this$0:Lcom/android/thememanager/activity/WallpaperDetailActivity;

    iget-object v7, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity$6;->this$0:Lcom/android/thememanager/activity/WallpaperDetailActivity;

    #getter for: Lcom/android/thememanager/activity/WallpaperDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;
    invoke-static {v7}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->access$500(Lcom/android/thememanager/activity/WallpaperDetailActivity;)Lmiui/resourcebrowser/model/Resource;

    move-result-object v7

    #calls: Lcom/android/thememanager/activity/WallpaperDetailActivity;->getLocalPreviewPath(Lmiui/resourcebrowser/model/Resource;)Ljava/lang/String;
    invoke-static {v6, v7}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->access$600(Lcom/android/thememanager/activity/WallpaperDetailActivity;Lmiui/resourcebrowser/model/Resource;)Ljava/lang/String;

    move-result-object v2

    .line 183
    .local v2, tmp:Ljava/lang/String;
    if-eqz v2, :cond_2

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 184
    .local v0, cacheFile:Ljava/io/File;
    :goto_1
    iget-object v6, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity$6;->this$0:Lcom/android/thememanager/activity/WallpaperDetailActivity;

    #getter for: Lcom/android/thememanager/activity/WallpaperDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;
    invoke-static {v6}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->access$700(Lcom/android/thememanager/activity/WallpaperDetailActivity;)Lmiui/resourcebrowser/model/Resource;

    move-result-object v6

    invoke-virtual {v6}, Lmiui/resourcebrowser/model/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v2

    .line 185
    if-eqz v2, :cond_3

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 186
    .local v1, destFile:Ljava/io/File;
    :goto_2
    if-eqz v0, :cond_1

    if-nez v1, :cond_4

    .line 187
    :cond_1
    iget-object v4, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity$6;->this$0:Lcom/android/thememanager/activity/WallpaperDetailActivity;

    invoke-static {v4, v5, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    goto :goto_0

    .end local v0           #cacheFile:Ljava/io/File;
    .end local v1           #destFile:Ljava/io/File;
    :cond_2
    move-object v0, v4

    .line 183
    goto :goto_1

    .restart local v0       #cacheFile:Ljava/io/File;
    :cond_3
    move-object v1, v4

    .line 185
    goto :goto_2

    .line 190
    .restart local v1       #destFile:Ljava/io/File;
    :cond_4
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 192
    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lmiui/resourcebrowser/util/ResourceHelper;->writeTo(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 199
    :goto_3
    iget-object v4, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity$6;->this$0:Lcom/android/thememanager/activity/WallpaperDetailActivity;

    invoke-virtual {v4}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->setResourceStatus()V

    .line 200
    iget-object v6, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity$6;->this$0:Lcom/android/thememanager/activity/WallpaperDetailActivity;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_5

    const v4, 0x7f0b004b

    :goto_4
    invoke-static {v6, v4, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_5
    move v4, v5

    goto :goto_4

    .line 204
    :cond_6
    const v4, 0x60c0018

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 205
    invoke-virtual {v3, v9}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0

    .line 193
    :catch_0
    move-exception v4

    goto :goto_3
.end method
