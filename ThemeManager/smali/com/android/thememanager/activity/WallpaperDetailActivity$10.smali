.class Lcom/android/thememanager/activity/WallpaperDetailActivity$10;
.super Ljava/lang/Object;
.source "WallpaperDetailActivity.java"

# interfaces
.implements Lmiui/resourcebrowser/util/ImageCacheDecoder$ImageDecodingListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/thememanager/activity/WallpaperDetailActivity;->getImageDecodingListener()Lmiui/resourcebrowser/util/ImageCacheDecoder$ImageDecodingListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/thememanager/activity/WallpaperDetailActivity;


# direct methods
.method constructor <init>(Lcom/android/thememanager/activity/WallpaperDetailActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 571
    iput-object p1, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity$10;->this$0:Lcom/android/thememanager/activity/WallpaperDetailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleDecodingResult(ZLjava/lang/String;Ljava/lang/String;)V
    .locals 3
    .parameter "result"
    .parameter "localPath"
    .parameter "onlinePath"

    .prologue
    .line 590
    if-eqz p1, :cond_1

    .line 591
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity$10;->this$0:Lcom/android/thememanager/activity/WallpaperDetailActivity;

    #calls: Lcom/android/thememanager/activity/WallpaperDetailActivity;->isVisiableImagePath(Ljava/lang/String;)Z
    invoke-static {v0, p2}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->access$2100(Lcom/android/thememanager/activity/WallpaperDetailActivity;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 592
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity$10;->this$0:Lcom/android/thememanager/activity/WallpaperDetailActivity;

    #calls: Lcom/android/thememanager/activity/WallpaperDetailActivity;->initWallpaperViewBitmap()V
    invoke-static {v0}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->access$2200(Lcom/android/thememanager/activity/WallpaperDetailActivity;)V

    .line 600
    :cond_0
    :goto_0
    return-void

    .line 595
    :cond_1
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity$10;->this$0:Lcom/android/thememanager/activity/WallpaperDetailActivity;

    #getter for: Lcom/android/thememanager/activity/WallpaperDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;
    invoke-static {v0}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->access$2400(Lcom/android/thememanager/activity/WallpaperDetailActivity;)Lmiui/resourcebrowser/model/Resource;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/resourcebrowser/model/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 596
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity$10;->this$0:Lcom/android/thememanager/activity/WallpaperDetailActivity;

    const v1, 0x7f0b0049

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public handleDownloadResult(ZLjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .parameter "result"
    .parameter "localPath"
    .parameter "onlinePath"

    .prologue
    .line 575
    iget-object v1, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity$10;->this$0:Lcom/android/thememanager/activity/WallpaperDetailActivity;

    iget-object v2, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity$10;->this$0:Lcom/android/thememanager/activity/WallpaperDetailActivity;

    #getter for: Lcom/android/thememanager/activity/WallpaperDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;
    invoke-static {v2}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->access$1900(Lcom/android/thememanager/activity/WallpaperDetailActivity;)Lmiui/resourcebrowser/model/Resource;

    move-result-object v2

    #calls: Lcom/android/thememanager/activity/WallpaperDetailActivity;->getOnlinePreviewUrl(Lmiui/resourcebrowser/model/Resource;)Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->access$2000(Lcom/android/thememanager/activity/WallpaperDetailActivity;Lmiui/resourcebrowser/model/Resource;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p3, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    .line 576
    .local v0, currentResource:Z
    if-eqz p1, :cond_2

    .line 577
    iget-object v1, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity$10;->this$0:Lcom/android/thememanager/activity/WallpaperDetailActivity;

    #calls: Lcom/android/thememanager/activity/WallpaperDetailActivity;->isVisiableImagePath(Ljava/lang/String;)Z
    invoke-static {v1, p2}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->access$2100(Lcom/android/thememanager/activity/WallpaperDetailActivity;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 578
    iget-object v1, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity$10;->this$0:Lcom/android/thememanager/activity/WallpaperDetailActivity;

    #calls: Lcom/android/thememanager/activity/WallpaperDetailActivity;->initWallpaperViewBitmap()V
    invoke-static {v1}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->access$2200(Lcom/android/thememanager/activity/WallpaperDetailActivity;)V

    .line 580
    :cond_0
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity$10;->this$0:Lcom/android/thememanager/activity/WallpaperDetailActivity;

    #getter for: Lcom/android/thememanager/activity/WallpaperDetailActivity;->mDownloadBtn:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->access$2300(Lcom/android/thememanager/activity/WallpaperDetailActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    iget-object v2, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity$10;->this$0:Lcom/android/thememanager/activity/WallpaperDetailActivity;

    const v3, 0x60c0018

    invoke-virtual {v2, v3}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 581
    iget-object v1, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity$10;->this$0:Lcom/android/thememanager/activity/WallpaperDetailActivity;

    #getter for: Lcom/android/thememanager/activity/WallpaperDetailActivity;->mDownloadBtn:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->access$2300(Lcom/android/thememanager/activity/WallpaperDetailActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->performClick()Z

    .line 586
    :cond_1
    :goto_0
    return-void

    .line 583
    :cond_2
    if-eqz v0, :cond_1

    .line 584
    iget-object v1, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity$10;->this$0:Lcom/android/thememanager/activity/WallpaperDetailActivity;

    const v2, 0x60c0020

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
