.class Lcom/android/thememanager/activity/LocalThemeListFragment$3;
.super Ljava/lang/Object;
.source "LocalThemeListFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/thememanager/activity/LocalThemeListFragment;->getRingtoneFiltingHeader()Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/thememanager/activity/LocalThemeListFragment;

.field final synthetic val$handler:Landroid/os/Handler;

.field final synthetic val$maxDuration:I

.field final synthetic val$minDuration:I


# direct methods
.method constructor <init>(Lcom/android/thememanager/activity/LocalThemeListFragment;Landroid/os/Handler;II)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 330
    iput-object p1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment$3;->this$0:Lcom/android/thememanager/activity/LocalThemeListFragment;

    iput-object p2, p0, Lcom/android/thememanager/activity/LocalThemeListFragment$3;->val$handler:Landroid/os/Handler;

    iput p3, p0, Lcom/android/thememanager/activity/LocalThemeListFragment$3;->val$minDuration:I

    iput p4, p0, Lcom/android/thememanager/activity/LocalThemeListFragment$3;->val$maxDuration:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .parameter "v"

    .prologue
    const/4 v6, 0x0

    .line 334
    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v2

    if-nez v2, :cond_1

    .line 335
    iget-object v2, p0, Lcom/android/thememanager/activity/LocalThemeListFragment$3;->this$0:Lcom/android/thememanager/activity/LocalThemeListFragment;

    #getter for: Lcom/android/thememanager/activity/LocalThemeListFragment;->mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;
    invoke-static {v2}, Lcom/android/thememanager/activity/LocalThemeListFragment;->access$2400(Lcom/android/thememanager/activity/LocalThemeListFragment;)Lmiui/resourcebrowser/activity/ResourceAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lmiui/resourcebrowser/activity/ResourceAdapter;->clearDataSet()V

    .line 336
    iget-object v2, p0, Lcom/android/thememanager/activity/LocalThemeListFragment$3;->this$0:Lcom/android/thememanager/activity/LocalThemeListFragment;

    #getter for: Lcom/android/thememanager/activity/LocalThemeListFragment;->mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;
    invoke-static {v2}, Lcom/android/thememanager/activity/LocalThemeListFragment;->access$2500(Lcom/android/thememanager/activity/LocalThemeListFragment;)Lmiui/resourcebrowser/activity/ResourceAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lmiui/resourcebrowser/activity/ResourceAdapter;->notifyDataSetInvalidated()V

    .line 337
    iget-object v2, p0, Lcom/android/thememanager/activity/LocalThemeListFragment$3;->val$handler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/android/thememanager/activity/LocalThemeListFragment$3;->val$handler:Landroid/os/Handler;

    iget v4, p0, Lcom/android/thememanager/activity/LocalThemeListFragment$3;->val$minDuration:I

    iget v5, p0, Lcom/android/thememanager/activity/LocalThemeListFragment$3;->val$maxDuration:I

    invoke-virtual {v3, v6, v4, v5}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 339
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 340
    .local v1, parent:Landroid/view/ViewGroup;
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 341
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/view/View;->setSelected(Z)V

    .line 340
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 344
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Landroid/view/View;->setSelected(Z)V

    .line 346
    .end local v0           #i:I
    .end local v1           #parent:Landroid/view/ViewGroup;
    :cond_1
    return-void
.end method
