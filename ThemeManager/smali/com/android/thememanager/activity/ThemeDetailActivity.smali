.class public Lcom/android/thememanager/activity/ThemeDetailActivity;
.super Lmiui/resourcebrowser/activity/ResourceDetailActivity;
.source "ThemeDetailActivity.java"

# interfaces
.implements Lcom/android/thememanager/ThemeResourceConstants;


# instance fields
.field private mModules:J

.field private mResourceType:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;-><init>()V

    return-void
.end method

.method private getApplyComponentFlags()J
    .locals 4

    .prologue
    .line 177
    iget-wide v0, p0, Lcom/android/thememanager/activity/ThemeDetailActivity;->mResourceType:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 178
    iget-wide v0, p0, Lcom/android/thememanager/activity/ThemeDetailActivity;->mResourceType:J

    const-wide/16 v2, -0x701

    and-long/2addr v0, v2

    .line 183
    :goto_0
    return-wide v0

    .line 180
    :cond_0
    iget-wide v0, p0, Lcom/android/thememanager/activity/ThemeDetailActivity;->mResourceType:J

    const-wide/16 v2, 0x1000

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    iget-wide v0, p0, Lcom/android/thememanager/activity/ThemeDetailActivity;->mModules:J

    const-wide/16 v2, 0x4

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 181
    const-wide/16 v0, 0x1004

    goto :goto_0

    .line 183
    :cond_1
    iget-wide v0, p0, Lcom/android/thememanager/activity/ThemeDetailActivity;->mResourceType:J

    goto :goto_0
.end method

.method private getThemeApplyParameters()Lcom/android/thememanager/util/ThemeApplyParameters;
    .locals 5

    .prologue
    .line 170
    new-instance v0, Lcom/android/thememanager/util/ThemeApplyParameters;

    invoke-direct {v0}, Lcom/android/thememanager/util/ThemeApplyParameters;-><init>()V

    .line 171
    .local v0, params:Lcom/android/thememanager/util/ThemeApplyParameters;
    invoke-direct {p0}, Lcom/android/thememanager/activity/ThemeDetailActivity;->getApplyComponentFlags()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/android/thememanager/util/ThemeApplyParameters;->applyFlags:J

    .line 172
    iget-wide v1, p0, Lcom/android/thememanager/activity/ThemeDetailActivity;->mResourceType:J

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, v0, Lcom/android/thememanager/util/ThemeApplyParameters;->removeOthers:Z

    .line 173
    return-object v0

    .line 172
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected buildResourceContext(Lmiui/resourcebrowser/ResourceContext;)Lmiui/resourcebrowser/ResourceContext;
    .locals 1
    .parameter "resContext"

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/android/thememanager/activity/ThemeDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p1, v0, p0}, Lcom/android/thememanager/util/UIHelper;->buildResourceContext(Lmiui/resourcebrowser/ResourceContext;Landroid/content/Intent;Landroid/content/Context;)Lmiui/resourcebrowser/ResourceContext;

    move-result-object p1

    .line 37
    invoke-super {p0, p1}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->buildResourceContext(Lmiui/resourcebrowser/ResourceContext;)Lmiui/resourcebrowser/ResourceContext;

    move-result-object v0

    return-object v0
.end method

.method protected getExternalResource()Lmiui/resourcebrowser/model/Resource;
    .locals 1

    .prologue
    .line 72
    invoke-super {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->getExternalResource()Lmiui/resourcebrowser/model/Resource;

    move-result-object v0

    return-object v0
.end method

.method protected getFormatPlayingRingtoneName(Ljava/lang/String;II)Ljava/lang/String;
    .locals 5
    .parameter "ringtonePath"
    .parameter "current"
    .parameter "total"

    .prologue
    .line 119
    iget-wide v1, p0, Lcom/android/thememanager/activity/ThemeDetailActivity;->mResourceType:J

    const-wide/32 v3, 0x8000

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    .line 120
    invoke-super {p0, p1, p2, p3}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->getFormatPlayingRingtoneName(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v1

    .line 123
    :goto_0
    return-object v1

    .line 122
    :cond_0
    invoke-static {p1}, Lcom/android/thememanager/util/ThemeHelper;->getNameForAudioEffect(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 123
    .local v0, name:Ljava/lang/String;
    const-string v1, "%s (%d/%d)"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v3, 0x1

    add-int/lit8 v4, p2, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method protected getFormatTitleBeforePlayingRingtone()Ljava/lang/String;
    .locals 4

    .prologue
    .line 111
    iget-wide v0, p0, Lcom/android/thememanager/activity/ThemeDetailActivity;->mResourceType:J

    const-wide/32 v2, 0x8000

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 112
    const v0, 0x7f0b0026

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/thememanager/activity/ThemeDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {p0, v3}, Lcom/android/thememanager/activity/ThemeDetailActivity;->getMusicPlayList(Lmiui/resourcebrowser/model/Resource;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/android/thememanager/activity/ThemeDetailActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 114
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->getFormatTitleBeforePlayingRingtone()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected getMusicPlayList(Lmiui/resourcebrowser/model/Resource;)Ljava/util/List;
    .locals 9
    .parameter "resourceItem"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiui/resourcebrowser/model/Resource;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 128
    iget-wide v5, p0, Lcom/android/thememanager/activity/ThemeDetailActivity;->mResourceType:J

    const-wide/32 v7, 0x8000

    cmp-long v5, v5, v7

    if-eqz v5, :cond_1

    .line 129
    invoke-super {p0, p1}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->getMusicPlayList(Lmiui/resourcebrowser/model/Resource;)Ljava/util/List;

    move-result-object v4

    .line 137
    :cond_0
    return-object v4

    .line 131
    :cond_1
    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getStatus()I

    move-result v5

    invoke-static {v5}, Lmiui/resourcebrowser/util/ResourceHelper;->isLocalResource(I)Z

    move-result v0

    .line 132
    .local v0, downloaded:Z
    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getPreviews()Ljava/util/List;

    move-result-object v1

    .line 133
    .local v1, entries:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/PathEntry;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 134
    .local v4, ret:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/resourcebrowser/model/PathEntry;

    .line 135
    .local v2, entry:Lmiui/resourcebrowser/model/PathEntry;
    if-eqz v0, :cond_2

    invoke-virtual {v2}, Lmiui/resourcebrowser/model/PathEntry;->getLocalPath()Ljava/lang/String;

    move-result-object v5

    :goto_1
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    invoke-virtual {v2}, Lmiui/resourcebrowser/model/PathEntry;->getOnlinePath()Ljava/lang/String;

    move-result-object v5

    goto :goto_1
.end method

.method protected getResourceController(Lmiui/resourcebrowser/ResourceContext;)Lmiui/resourcebrowser/controller/ResourceController;
    .locals 1
    .parameter "resContext"

    .prologue
    .line 42
    new-instance v0, Lcom/android/thememanager/controller/ThemeController;

    invoke-direct {v0, p1}, Lcom/android/thememanager/controller/ThemeController;-><init>(Lmiui/resourcebrowser/ResourceContext;)V

    return-object v0
.end method

.method protected getResourceOperationHandler(Lmiui/resourcebrowser/view/ResourceOperationView;)Lmiui/resourcebrowser/view/ResourceOperationHandler;
    .locals 2
    .parameter "view"

    .prologue
    .line 77
    new-instance v0, Lcom/android/thememanager/util/ThemeOperationHandler;

    iget-object v1, p0, Lcom/android/thememanager/activity/ThemeDetailActivity;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-direct {v0, p0, v1, p1}, Lcom/android/thememanager/util/ThemeOperationHandler;-><init>(Landroid/content/Context;Lmiui/resourcebrowser/ResourceContext;Lmiui/resourcebrowser/view/ResourceOperationView;)V

    return-object v0
.end method

.method protected initParams()V
    .locals 4

    .prologue
    .line 47
    invoke-super {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->initParams()V

    .line 48
    iget-object v0, p0, Lcom/android/thememanager/activity/ThemeDetailActivity;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    const-string v1, "EXTRA_CTX_RESOURCE_TYPE"

    const-wide/16 v2, -0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmiui/resourcebrowser/ResourceContext;->getExtraMeta(Ljava/lang/String;Ljava/io/Serializable;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/thememanager/activity/ThemeDetailActivity;->mResourceType:J

    .line 49
    iget-wide v0, p0, Lcom/android/thememanager/activity/ThemeDetailActivity;->mResourceType:J

    invoke-static {p0, v0, v1}, Lcom/android/thememanager/util/ThemeHelper;->setMusicVolumeType(Landroid/app/Activity;J)V

    .line 50
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/android/thememanager/activity/ThemeDetailActivity$1;

    invoke-direct {v1, p0}, Lcom/android/thememanager/activity/ThemeDetailActivity$1;-><init>(Lcom/android/thememanager/activity/ThemeDetailActivity;)V

    const-wide/16 v2, 0x320

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 56
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 159
    invoke-super {p0, p1, p2, p3}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 160
    const/16 v1, 0x67

    if-ne p1, v1, :cond_0

    const/16 v1, 0x68

    if-ne p2, v1, :cond_0

    .line 161
    const-string v1, "RESPONSE_NEEDS_DELETE"

    const/4 v2, 0x0

    invoke-virtual {p3, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 162
    .local v0, needsDelete:Z
    iget-boolean v1, p0, Lcom/android/thememanager/activity/ThemeDetailActivity;->mIsOnlineResourceSet:Z

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    .line 163
    invoke-super {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->onDeleteEventPerformed()V

    .line 164
    invoke-virtual {p0}, Lcom/android/thememanager/activity/ThemeDetailActivity;->finish()V

    .line 167
    .end local v0           #needsDelete:Z
    :cond_0
    return-void
.end method

.method public onApplyEventPerformed()V
    .locals 2

    .prologue
    .line 142
    invoke-super {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->onApplyEventPerformed()V

    .line 143
    iget-object v0, p0, Lcom/android/thememanager/activity/ThemeDetailActivity;->mOperationHandler:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    check-cast v0, Lcom/android/thememanager/util/ThemeOperationHandler;

    invoke-direct {p0}, Lcom/android/thememanager/activity/ThemeDetailActivity;->getThemeApplyParameters()Lcom/android/thememanager/util/ThemeApplyParameters;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/thememanager/util/ThemeOperationHandler;->setApplyParameters(Lcom/android/thememanager/util/ThemeApplyParameters;)V

    .line 144
    return-void
.end method

.method public onMagicEventPerformed()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 148
    invoke-super {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->onMagicEventPerformed()V

    .line 149
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 150
    const-string v1, "REQUEST_SELECTING_THEME"

    iget-object v2, p0, Lcom/android/thememanager/activity/ThemeDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 151
    const-string v1, "REQUEST_APPLY_PARAMS"

    invoke-direct {p0}, Lcom/android/thememanager/activity/ThemeDetailActivity;->getThemeApplyParameters()Lcom/android/thememanager/util/ThemeApplyParameters;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 152
    const-string v1, "REQUEST_SOURCE_TYPE"

    iget v2, p0, Lcom/android/thememanager/activity/ThemeDetailActivity;->mSourceType:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 153
    const/16 v1, 0x67

    invoke-virtual {p0, v0, v1}, Lcom/android/thememanager/activity/ThemeDetailActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 154
    invoke-virtual {p0, v3, v3}, Lcom/android/thememanager/activity/ThemeDetailActivity;->overridePendingTransition(II)V

    .line 155
    return-void
.end method

.method protected setResourceInfo()V
    .locals 11

    .prologue
    const-wide/16 v9, -0x1

    const-wide/16 v5, 0x0

    .line 82
    invoke-super {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->setResourceInfo()V

    .line 83
    iget-wide v3, p0, Lcom/android/thememanager/activity/ThemeDetailActivity;->mResourceType:J

    invoke-static {v3, v4}, Lcom/android/thememanager/util/UIHelper;->isAudioResource(J)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-wide v3, p0, Lcom/android/thememanager/activity/ThemeDetailActivity;->mResourceType:J

    const-wide/32 v7, 0x8000

    cmp-long v3, v3, v7

    if-nez v3, :cond_1

    :cond_0
    iget-wide v3, p0, Lcom/android/thememanager/activity/ThemeDetailActivity;->mResourceType:J

    invoke-static {v3, v4}, Lcom/android/thememanager/util/UIHelper;->isImageResource(J)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 107
    :cond_1
    :goto_0
    return-void

    .line 88
    :cond_2
    :try_start_0
    iget-object v3, p0, Lcom/android/thememanager/activity/ThemeDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    const-string v4, "modulesFlag"

    invoke-virtual {v3, v4}, Lmiui/resourcebrowser/model/Resource;->getExtraMeta(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 89
    .local v2, str:Ljava/lang/String;
    if-eqz v2, :cond_6

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    :goto_1
    iput-wide v3, p0, Lcom/android/thememanager/activity/ThemeDetailActivity;->mModules:J

    .line 90
    new-instance v1, Ljava/io/File;

    iget-object v3, p0, Lcom/android/thememanager/activity/ThemeDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v3}, Lmiui/resourcebrowser/model/Resource;->getDownloadPath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 91
    .local v1, file:Ljava/io/File;
    iget-wide v3, p0, Lcom/android/thememanager/activity/ThemeDetailActivity;->mModules:J

    cmp-long v3, v3, v5

    if-eqz v3, :cond_3

    iget-boolean v3, p0, Lcom/android/thememanager/activity/ThemeDetailActivity;->mIsOnlineResourceSet:Z

    if-eqz v3, :cond_4

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 92
    :cond_3
    const-string v3, "/system/media/theme/.data/meta/theme/default.mrm"

    iget-object v4, p0, Lcom/android/thememanager/activity/ThemeDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v4}, Lmiui/resourcebrowser/model/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 93
    const-wide/16 v3, -0x1

    iput-wide v3, p0, Lcom/android/thememanager/activity/ThemeDetailActivity;->mModules:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 101
    .end local v1           #file:Ljava/io/File;
    .end local v2           #str:Ljava/lang/String;
    :cond_4
    :goto_2
    iget-wide v3, p0, Lcom/android/thememanager/activity/ThemeDetailActivity;->mResourceType:J

    const-wide/16 v5, 0x1000

    cmp-long v3, v3, v5

    if-nez v3, :cond_8

    .line 102
    const-wide/16 v3, 0x1004

    iput-wide v3, p0, Lcom/android/thememanager/activity/ThemeDetailActivity;->mModules:J

    .line 106
    :cond_5
    :goto_3
    iget-object v3, p0, Lcom/android/thememanager/activity/ThemeDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    const-string v4, "modulesFlag"

    iget-wide v5, p0, Lcom/android/thememanager/activity/ThemeDetailActivity;->mModules:J

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lmiui/resourcebrowser/model/Resource;->putExtraMeta(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .restart local v2       #str:Ljava/lang/String;
    :cond_6
    move-wide v3, v5

    .line 89
    goto :goto_1

    .line 95
    .restart local v1       #file:Ljava/io/File;
    :cond_7
    :try_start_1
    new-instance v3, Ljava/util/zip/ZipFile;

    invoke-direct {v3, v1}, Ljava/util/zip/ZipFile;-><init>(Ljava/io/File;)V

    invoke-static {v3}, Lcom/android/thememanager/util/ThemeHelper;->identifyComponents(Ljava/util/zip/ZipFile;)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/android/thememanager/activity/ThemeDetailActivity;->mModules:J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 98
    .end local v1           #file:Ljava/io/File;
    .end local v2           #str:Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 99
    .local v0, e:Ljava/lang/Exception;
    iput-wide v5, p0, Lcom/android/thememanager/activity/ThemeDetailActivity;->mModules:J

    goto :goto_2

    .line 103
    .end local v0           #e:Ljava/lang/Exception;
    :cond_8
    iget-wide v3, p0, Lcom/android/thememanager/activity/ThemeDetailActivity;->mResourceType:J

    cmp-long v3, v3, v9

    if-eqz v3, :cond_5

    .line 104
    iget-wide v3, p0, Lcom/android/thememanager/activity/ThemeDetailActivity;->mResourceType:J

    iput-wide v3, p0, Lcom/android/thememanager/activity/ThemeDetailActivity;->mModules:J

    goto :goto_3
.end method

.method protected setupUI()V
    .locals 4

    .prologue
    .line 60
    invoke-super {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->setupUI()V

    .line 61
    iget-wide v0, p0, Lcom/android/thememanager/activity/ThemeDetailActivity;->mResourceType:J

    const-wide/16 v2, 0x10

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 62
    const v0, 0x602002d

    invoke-virtual {p0, v0}, Lcom/android/thememanager/activity/ThemeDetailActivity;->setScreenViewBackground(I)V

    .line 64
    :cond_0
    iget-wide v0, p0, Lcom/android/thememanager/activity/ThemeDetailActivity;->mResourceType:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lcom/android/thememanager/activity/ThemeDetailActivity;->mResourceType:J

    const-wide/16 v2, 0x1000

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    .line 65
    :cond_1
    iget-object v0, p0, Lcom/android/thememanager/activity/ThemeDetailActivity;->mOperationView:Lmiui/resourcebrowser/view/ResourceOperationView;

    const v1, 0x60201af

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/view/ResourceOperationView;->setMagicButtonResource(I)V

    .line 67
    :cond_2
    return-void
.end method
