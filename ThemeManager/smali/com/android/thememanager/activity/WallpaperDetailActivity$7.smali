.class Lcom/android/thememanager/activity/WallpaperDetailActivity$7;
.super Ljava/lang/Object;
.source "WallpaperDetailActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/thememanager/activity/WallpaperDetailActivity;->setupUI()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/thememanager/activity/WallpaperDetailActivity;


# direct methods
.method constructor <init>(Lcom/android/thememanager/activity/WallpaperDetailActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 210
    iput-object p1, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity$7;->this$0:Lcom/android/thememanager/activity/WallpaperDetailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .parameter "v"

    .prologue
    const/4 v7, 0x0

    .line 213
    iget-object v3, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity$7;->this$0:Lcom/android/thememanager/activity/WallpaperDetailActivity;

    invoke-virtual {v3}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f03000b

    invoke-virtual {v3, v4, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 214
    .local v0, info:Landroid/view/View;
    iget-object v3, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity$7;->this$0:Lcom/android/thememanager/activity/WallpaperDetailActivity;

    #getter for: Lcom/android/thememanager/activity/WallpaperDetailActivity;->mHasInfoView:Z
    invoke-static {v3}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->access$800(Lcom/android/thememanager/activity/WallpaperDetailActivity;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 215
    const v3, 0x7f070022

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iget-object v4, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity$7;->this$0:Lcom/android/thememanager/activity/WallpaperDetailActivity;

    iget-object v5, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity$7;->this$0:Lcom/android/thememanager/activity/WallpaperDetailActivity;

    #getter for: Lcom/android/thememanager/activity/WallpaperDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;
    invoke-static {v5}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->access$900(Lcom/android/thememanager/activity/WallpaperDetailActivity;)Lmiui/resourcebrowser/model/Resource;

    move-result-object v5

    invoke-virtual {v5}, Lmiui/resourcebrowser/model/Resource;->getAuthor()Ljava/lang/String;

    move-result-object v5

    #calls: Lcom/android/thememanager/activity/WallpaperDetailActivity;->checkInfoValue(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v4, v5}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->access$1000(Lcom/android/thememanager/activity/WallpaperDetailActivity;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 216
    const-string v1, ""

    .line 218
    .local v1, size:Ljava/lang/String;
    :try_start_0
    iget-object v3, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity$7;->this$0:Lcom/android/thememanager/activity/WallpaperDetailActivity;

    #getter for: Lcom/android/thememanager/activity/WallpaperDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;
    invoke-static {v3}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->access$1100(Lcom/android/thememanager/activity/WallpaperDetailActivity;)Lmiui/resourcebrowser/model/Resource;

    move-result-object v3

    invoke-virtual {v3}, Lmiui/resourcebrowser/model/Resource;->getSize()J

    move-result-wide v3

    invoke-static {v3, v4}, Lmiui/resourcebrowser/util/ResourceHelper;->formatFileSize(J)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 221
    :goto_0
    const v3, 0x7f070023

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 222
    const v3, 0x7f070025

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iget-object v4, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity$7;->this$0:Lcom/android/thememanager/activity/WallpaperDetailActivity;

    iget-object v5, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity$7;->this$0:Lcom/android/thememanager/activity/WallpaperDetailActivity;

    #getter for: Lcom/android/thememanager/activity/WallpaperDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;
    invoke-static {v5}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->access$1200(Lcom/android/thememanager/activity/WallpaperDetailActivity;)Lmiui/resourcebrowser/model/Resource;

    move-result-object v5

    invoke-virtual {v5}, Lmiui/resourcebrowser/model/Resource;->getUpdatedTime()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    #calls: Lcom/android/thememanager/activity/WallpaperDetailActivity;->checkInfoValue(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v4, v5}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->access$1000(Lcom/android/thememanager/activity/WallpaperDetailActivity;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 224
    iget-object v3, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity$7;->this$0:Lcom/android/thememanager/activity/WallpaperDetailActivity;

    #getter for: Lcom/android/thememanager/activity/WallpaperDetailActivity;->mIsOnlineResourceSet:Z
    invoke-static {v3}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->access$1300(Lcom/android/thememanager/activity/WallpaperDetailActivity;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 225
    const v3, 0x7f070024

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 226
    .local v2, timeTitle:Landroid/widget/TextView;
    const v3, 0x60c01f9

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 228
    .end local v2           #timeTitle:Landroid/widget/TextView;
    :cond_0
    const v3, 0x7f070026

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iget-object v4, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity$7;->this$0:Lcom/android/thememanager/activity/WallpaperDetailActivity;

    iget-object v5, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity$7;->this$0:Lcom/android/thememanager/activity/WallpaperDetailActivity;

    #getter for: Lcom/android/thememanager/activity/WallpaperDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;
    invoke-static {v5}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->access$1400(Lcom/android/thememanager/activity/WallpaperDetailActivity;)Lmiui/resourcebrowser/model/Resource;

    move-result-object v5

    invoke-virtual {v5}, Lmiui/resourcebrowser/model/Resource;->getDownloadCount()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    #calls: Lcom/android/thememanager/activity/WallpaperDetailActivity;->checkInfoValue(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v4, v5}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->access$1000(Lcom/android/thememanager/activity/WallpaperDetailActivity;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 232
    .end local v1           #size:Ljava/lang/String;
    :cond_1
    new-instance v3, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity$7;->this$0:Lcom/android/thememanager/activity/WallpaperDetailActivity;

    invoke-direct {v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x60c0016

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f0b004a

    invoke-virtual {v3, v4, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 238
    return-void

    .line 219
    .restart local v1       #size:Ljava/lang/String;
    :catch_0
    move-exception v3

    goto/16 :goto_0
.end method
