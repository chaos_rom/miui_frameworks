.class Lcom/android/thememanager/activity/LocalThemeListFragment$1;
.super Lmiui/resourcebrowser/controller/local/ImportResourceTask;
.source "LocalThemeListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/thememanager/activity/LocalThemeListFragment;->onActivityResult(IILandroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/thememanager/activity/LocalThemeListFragment;


# direct methods
.method constructor <init>(Lcom/android/thememanager/activity/LocalThemeListFragment;Lmiui/resourcebrowser/ResourceContext;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 104
    iput-object p1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment$1;->this$0:Lcom/android/thememanager/activity/LocalThemeListFragment;

    invoke-direct {p0, p2, p3}, Lmiui/resourcebrowser/controller/local/ImportResourceTask;-><init>(Lmiui/resourcebrowser/ResourceContext;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 2
    .parameter "result"

    .prologue
    .line 128
    iget-object v0, p0, Lcom/android/thememanager/activity/LocalThemeListFragment$1;->this$0:Lcom/android/thememanager/activity/LocalThemeListFragment;

    #getter for: Lcom/android/thememanager/activity/LocalThemeListFragment;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/android/thememanager/activity/LocalThemeListFragment;->access$1300(Lcom/android/thememanager/activity/LocalThemeListFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 129
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lez v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/android/thememanager/activity/LocalThemeListFragment$1;->this$0:Lcom/android/thememanager/activity/LocalThemeListFragment;

    #getter for: Lcom/android/thememanager/activity/LocalThemeListFragment;->mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;
    invoke-static {v0}, Lcom/android/thememanager/activity/LocalThemeListFragment;->access$1400(Lcom/android/thememanager/activity/LocalThemeListFragment;)Lmiui/resourcebrowser/activity/ResourceAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/resourcebrowser/activity/ResourceAdapter;->notifyDataSetChanged()V

    .line 131
    iget-object v0, p0, Lcom/android/thememanager/activity/LocalThemeListFragment$1;->this$0:Lcom/android/thememanager/activity/LocalThemeListFragment;

    #getter for: Lcom/android/thememanager/activity/LocalThemeListFragment;->mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;
    invoke-static {v0}, Lcom/android/thememanager/activity/LocalThemeListFragment;->access$1500(Lcom/android/thememanager/activity/LocalThemeListFragment;)Lmiui/resourcebrowser/activity/ResourceAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/resourcebrowser/activity/ResourceAdapter;->loadData()V

    .line 134
    :cond_0
    iget-object v0, p0, Lcom/android/thememanager/activity/LocalThemeListFragment$1;->this$0:Lcom/android/thememanager/activity/LocalThemeListFragment;

    #getter for: Lcom/android/thememanager/activity/LocalThemeListFragment;->mWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v0}, Lcom/android/thememanager/activity/LocalThemeListFragment;->access$1600(Lcom/android/thememanager/activity/LocalThemeListFragment;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 135
    iget-object v0, p0, Lcom/android/thememanager/activity/LocalThemeListFragment$1;->this$0:Lcom/android/thememanager/activity/LocalThemeListFragment;

    #getter for: Lcom/android/thememanager/activity/LocalThemeListFragment;->mWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v0}, Lcom/android/thememanager/activity/LocalThemeListFragment;->access$1700(Lcom/android/thememanager/activity/LocalThemeListFragment;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 138
    :cond_1
    iget-object v0, p0, Lcom/android/thememanager/activity/LocalThemeListFragment$1;->this$0:Lcom/android/thememanager/activity/LocalThemeListFragment;

    #getter for: Lcom/android/thememanager/activity/LocalThemeListFragment;->mProgressBar:Landroid/view/View;
    invoke-static {v0}, Lcom/android/thememanager/activity/LocalThemeListFragment;->access$1800(Lcom/android/thememanager/activity/LocalThemeListFragment;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 140
    iget-object v0, p0, Lcom/android/thememanager/activity/LocalThemeListFragment$1;->this$0:Lcom/android/thememanager/activity/LocalThemeListFragment;

    #getter for: Lcom/android/thememanager/activity/LocalThemeListFragment;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/android/thememanager/activity/LocalThemeListFragment;->access$1900(Lcom/android/thememanager/activity/LocalThemeListFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 141
    iget-object v0, p0, Lcom/android/thememanager/activity/LocalThemeListFragment$1;->this$0:Lcom/android/thememanager/activity/LocalThemeListFragment;

    #getter for: Lcom/android/thememanager/activity/LocalThemeListFragment;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/android/thememanager/activity/LocalThemeListFragment;->access$2000(Lcom/android/thememanager/activity/LocalThemeListFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 143
    :cond_2
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 104
    check-cast p1, Ljava/lang/Integer;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/android/thememanager/activity/LocalThemeListFragment$1;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 108
    invoke-super {p0}, Lmiui/resourcebrowser/controller/local/ImportResourceTask;->onPreExecute()V

    .line 110
    const-string v0, "Resource Import Tag"

    .line 111
    .local v0, TAG:Ljava/lang/String;
    iget-object v2, p0, Lcom/android/thememanager/activity/LocalThemeListFragment$1;->this$0:Lcom/android/thememanager/activity/LocalThemeListFragment;

    iget-object v1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment$1;->this$0:Lcom/android/thememanager/activity/LocalThemeListFragment;

    #getter for: Lcom/android/thememanager/activity/LocalThemeListFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/android/thememanager/activity/LocalThemeListFragment;->access$100(Lcom/android/thememanager/activity/LocalThemeListFragment;)Landroid/app/Activity;

    move-result-object v1

    const-string v3, "power"

    invoke-virtual {v1, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    const v3, 0x10000006

    const-string v4, "Resource Import Tag"

    invoke-virtual {v1, v3, v4}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    #setter for: Lcom/android/thememanager/activity/LocalThemeListFragment;->mWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v2, v1}, Lcom/android/thememanager/activity/LocalThemeListFragment;->access$002(Lcom/android/thememanager/activity/LocalThemeListFragment;Landroid/os/PowerManager$WakeLock;)Landroid/os/PowerManager$WakeLock;

    .line 113
    iget-object v1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment$1;->this$0:Lcom/android/thememanager/activity/LocalThemeListFragment;

    #getter for: Lcom/android/thememanager/activity/LocalThemeListFragment;->mWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v1}, Lcom/android/thememanager/activity/LocalThemeListFragment;->access$200(Lcom/android/thememanager/activity/LocalThemeListFragment;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 115
    iget-object v1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment$1;->this$0:Lcom/android/thememanager/activity/LocalThemeListFragment;

    #getter for: Lcom/android/thememanager/activity/LocalThemeListFragment;->mProgressBar:Landroid/view/View;
    invoke-static {v1}, Lcom/android/thememanager/activity/LocalThemeListFragment;->access$300(Lcom/android/thememanager/activity/LocalThemeListFragment;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 117
    iget-object v1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment$1;->this$0:Lcom/android/thememanager/activity/LocalThemeListFragment;

    new-instance v2, Landroid/app/ProgressDialog;

    iget-object v3, p0, Lcom/android/thememanager/activity/LocalThemeListFragment$1;->this$0:Lcom/android/thememanager/activity/LocalThemeListFragment;

    #getter for: Lcom/android/thememanager/activity/LocalThemeListFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v3}, Lcom/android/thememanager/activity/LocalThemeListFragment;->access$500(Lcom/android/thememanager/activity/LocalThemeListFragment;)Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    #setter for: Lcom/android/thememanager/activity/LocalThemeListFragment;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v1, v2}, Lcom/android/thememanager/activity/LocalThemeListFragment;->access$402(Lcom/android/thememanager/activity/LocalThemeListFragment;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 118
    iget-object v1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment$1;->this$0:Lcom/android/thememanager/activity/LocalThemeListFragment;

    #getter for: Lcom/android/thememanager/activity/LocalThemeListFragment;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/android/thememanager/activity/LocalThemeListFragment;->access$600(Lcom/android/thememanager/activity/LocalThemeListFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 119
    iget-object v1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment$1;->this$0:Lcom/android/thememanager/activity/LocalThemeListFragment;

    #getter for: Lcom/android/thememanager/activity/LocalThemeListFragment;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/android/thememanager/activity/LocalThemeListFragment;->access$800(Lcom/android/thememanager/activity/LocalThemeListFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    iget-object v2, p0, Lcom/android/thememanager/activity/LocalThemeListFragment$1;->this$0:Lcom/android/thememanager/activity/LocalThemeListFragment;

    #getter for: Lcom/android/thememanager/activity/LocalThemeListFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v2}, Lcom/android/thememanager/activity/LocalThemeListFragment;->access$700(Lcom/android/thememanager/activity/LocalThemeListFragment;)Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f0b004c

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 120
    iget-object v1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment$1;->this$0:Lcom/android/thememanager/activity/LocalThemeListFragment;

    #getter for: Lcom/android/thememanager/activity/LocalThemeListFragment;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/android/thememanager/activity/LocalThemeListFragment;->access$900(Lcom/android/thememanager/activity/LocalThemeListFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 121
    iget-object v1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment$1;->this$0:Lcom/android/thememanager/activity/LocalThemeListFragment;

    #getter for: Lcom/android/thememanager/activity/LocalThemeListFragment;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/android/thememanager/activity/LocalThemeListFragment;->access$1000(Lcom/android/thememanager/activity/LocalThemeListFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setProgressNumberFormat(Ljava/lang/String;)V

    .line 122
    iget-object v1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment$1;->this$0:Lcom/android/thememanager/activity/LocalThemeListFragment;

    #getter for: Lcom/android/thememanager/activity/LocalThemeListFragment;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/android/thememanager/activity/LocalThemeListFragment;->access$1100(Lcom/android/thememanager/activity/LocalThemeListFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    const/16 v2, 0x64

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMax(I)V

    .line 123
    iget-object v1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment$1;->this$0:Lcom/android/thememanager/activity/LocalThemeListFragment;

    #getter for: Lcom/android/thememanager/activity/LocalThemeListFragment;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/android/thememanager/activity/LocalThemeListFragment;->access$1200(Lcom/android/thememanager/activity/LocalThemeListFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V

    .line 124
    return-void
.end method
