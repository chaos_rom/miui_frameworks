.class Lcom/android/thememanager/activity/LocalThemeListFragment$2;
.super Landroid/os/Handler;
.source "LocalThemeListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/thememanager/activity/LocalThemeListFragment;->getRingtoneFiltingHeader()Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/thememanager/activity/LocalThemeListFragment;


# direct methods
.method constructor <init>(Lcom/android/thememanager/activity/LocalThemeListFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 305
    iput-object p1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment$2;->this$0:Lcom/android/thememanager/activity/LocalThemeListFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .parameter "msg"

    .prologue
    .line 308
    iget v1, p1, Landroid/os/Message;->arg1:I

    .line 309
    .local v1, minDuration:I
    iget v0, p1, Landroid/os/Message;->arg2:I

    .line 310
    .local v0, maxDuration:I
    iget-object v2, p0, Lcom/android/thememanager/activity/LocalThemeListFragment$2;->this$0:Lcom/android/thememanager/activity/LocalThemeListFragment;

    #getter for: Lcom/android/thememanager/activity/LocalThemeListFragment;->mResContext:Lmiui/resourcebrowser/ResourceContext;
    invoke-static {v2}, Lcom/android/thememanager/activity/LocalThemeListFragment;->access$2100(Lcom/android/thememanager/activity/LocalThemeListFragment;)Lmiui/resourcebrowser/ResourceContext;

    move-result-object v2

    const-string v3, "resourcebrowser.RINGTONE_MIN_DURATION_LIMIT"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lmiui/resourcebrowser/ResourceContext;->putExtraMeta(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 311
    iget-object v2, p0, Lcom/android/thememanager/activity/LocalThemeListFragment$2;->this$0:Lcom/android/thememanager/activity/LocalThemeListFragment;

    #getter for: Lcom/android/thememanager/activity/LocalThemeListFragment;->mResContext:Lmiui/resourcebrowser/ResourceContext;
    invoke-static {v2}, Lcom/android/thememanager/activity/LocalThemeListFragment;->access$2200(Lcom/android/thememanager/activity/LocalThemeListFragment;)Lmiui/resourcebrowser/ResourceContext;

    move-result-object v2

    const-string v3, "resourcebrowser.RINGTONE_MAX_DURATION_LIMIT"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lmiui/resourcebrowser/ResourceContext;->putExtraMeta(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 313
    iget-object v2, p0, Lcom/android/thememanager/activity/LocalThemeListFragment$2;->this$0:Lcom/android/thememanager/activity/LocalThemeListFragment;

    #getter for: Lcom/android/thememanager/activity/LocalThemeListFragment;->mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;
    invoke-static {v2}, Lcom/android/thememanager/activity/LocalThemeListFragment;->access$2300(Lcom/android/thememanager/activity/LocalThemeListFragment;)Lmiui/resourcebrowser/activity/ResourceAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lmiui/resourcebrowser/activity/ResourceAdapter;->loadData()V

    .line 314
    return-void
.end method
