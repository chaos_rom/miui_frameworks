.class public Lcom/android/thememanager/activity/OnlineThemeListFragment;
.super Lmiui/resourcebrowser/activity/OnlineResourceListFragment;
.source "OnlineThemeListFragment.java"

# interfaces
.implements Lcom/android/thememanager/ThemeResourceConstants;


# instance fields
.field private mResourceType:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;-><init>()V

    return-void
.end method

.method private refreshCurrentUsingFlags()V
    .locals 4

    .prologue
    .line 46
    iget-object v1, p0, Lcom/android/thememanager/activity/OnlineThemeListFragment;->mActivity:Landroid/app/Activity;

    iget-wide v2, p0, Lcom/android/thememanager/activity/OnlineThemeListFragment;->mResourceType:J

    invoke-static {v1, v2, v3}, Lcom/android/thememanager/util/UIHelper;->computeCurrentUsingPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    .line 47
    .local v0, usingPath:Ljava/lang/String;
    iget-object v1, p0, Lcom/android/thememanager/activity/OnlineThemeListFragment;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v1, v0}, Lmiui/resourcebrowser/ResourceContext;->setCurrentUsingPath(Ljava/lang/String;)V

    .line 48
    iget-object v1, p0, Lcom/android/thememanager/activity/OnlineThemeListFragment;->mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;

    invoke-virtual {v1}, Lmiui/resourcebrowser/activity/ResourceAdapter;->notifyDataSetChanged()V

    .line 49
    return-void
.end method


# virtual methods
.method protected getBatchOperationHandler()Lmiui/resourcebrowser/util/BatchResourceHandler;
    .locals 6

    .prologue
    .line 19
    iget-wide v0, p0, Lcom/android/thememanager/activity/OnlineThemeListFragment;->mResourceType:J

    invoke-static {v0, v1}, Lcom/android/thememanager/util/UIHelper;->isAudioResource(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 20
    new-instance v0, Lcom/android/thememanager/util/ThemeAudioBatchHandler;

    iget-object v2, p0, Lcom/android/thememanager/activity/OnlineThemeListFragment;->mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;

    iget-object v3, p0, Lcom/android/thememanager/activity/OnlineThemeListFragment;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    iget-wide v4, p0, Lcom/android/thememanager/activity/OnlineThemeListFragment;->mResourceType:J

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/thememanager/util/ThemeAudioBatchHandler;-><init>(Lmiui/resourcebrowser/activity/ResourceListFragment;Lmiui/resourcebrowser/activity/ResourceAdapter;Lmiui/resourcebrowser/ResourceContext;J)V

    .line 22
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->getBatchOperationHandler()Lmiui/resourcebrowser/util/BatchResourceHandler;

    move-result-object v0

    goto :goto_0
.end method

.method protected initParams()V
    .locals 5

    .prologue
    .line 27
    iget-object v1, p0, Lcom/android/thememanager/activity/OnlineThemeListFragment;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    const-string v2, "EXTRA_CTX_RESOURCE_TYPE"

    const-wide/16 v3, -0x1

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lmiui/resourcebrowser/ResourceContext;->getExtraMeta(Ljava/lang/String;Ljava/io/Serializable;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/thememanager/activity/OnlineThemeListFragment;->mResourceType:J

    .line 28
    iget-object v1, p0, Lcom/android/thememanager/activity/OnlineThemeListFragment;->mActivity:Landroid/app/Activity;

    iget-wide v2, p0, Lcom/android/thememanager/activity/OnlineThemeListFragment;->mResourceType:J

    invoke-static {v1, v2, v3}, Lcom/android/thememanager/util/ThemeHelper;->setMusicVolumeType(Landroid/app/Activity;J)V

    .line 29
    invoke-super {p0}, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->initParams()V

    .line 30
    iget-object v1, p0, Lcom/android/thememanager/activity/OnlineThemeListFragment;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    const-string v2, "EXTRA_CTX_GADGET_FLAG"

    invoke-virtual {v1, v2}, Lmiui/resourcebrowser/ResourceContext;->getExtraMeta(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 31
    .local v0, categoryCode:Ljava/lang/String;
    iget-boolean v1, p0, Lcom/android/thememanager/activity/OnlineThemeListFragment;->mIsRecommendList:Z

    if-eqz v1, :cond_0

    .line 32
    iget-object v1, p0, Lcom/android/thememanager/activity/OnlineThemeListFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "REQUEST_RECOMMEND_ID"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/thememanager/activity/OnlineThemeListFragment;->mRecommendId:Ljava/lang/String;

    .line 33
    iget-object v1, p0, Lcom/android/thememanager/activity/OnlineThemeListFragment;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    iget-object v2, p0, Lcom/android/thememanager/activity/OnlineThemeListFragment;->mService:Lmiui/resourcebrowser/controller/online/OnlineService;

    iget-object v3, p0, Lcom/android/thememanager/activity/OnlineThemeListFragment;->mRecommendId:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Lmiui/resourcebrowser/controller/online/OnlineService;->getRecommendListUrl(Ljava/lang/String;Ljava/lang/String;)Lmiui/resourcebrowser/controller/online/RequestUrl;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmiui/resourcebrowser/ResourceContext;->setListUrl(Lmiui/resourcebrowser/controller/online/RequestUrl;)V

    .line 37
    :goto_0
    return-void

    .line 35
    :cond_0
    iget-object v1, p0, Lcom/android/thememanager/activity/OnlineThemeListFragment;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    iget-object v2, p0, Lcom/android/thememanager/activity/OnlineThemeListFragment;->mService:Lmiui/resourcebrowser/controller/online/OnlineService;

    invoke-virtual {v2, v0}, Lmiui/resourcebrowser/controller/online/OnlineService;->getCommonListUrl(Ljava/lang/String;)Lmiui/resourcebrowser/controller/online/RequestUrl;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmiui/resourcebrowser/ResourceContext;->setListUrl(Lmiui/resourcebrowser/controller/online/RequestUrl;)V

    goto :goto_0
.end method

.method protected onVisible()V
    .locals 0

    .prologue
    .line 41
    invoke-super {p0}, Lmiui/resourcebrowser/activity/OnlineResourceListFragment;->onVisible()V

    .line 42
    invoke-direct {p0}, Lcom/android/thememanager/activity/OnlineThemeListFragment;->refreshCurrentUsingFlags()V

    .line 43
    return-void
.end method
