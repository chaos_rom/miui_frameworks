.class public Lcom/android/thememanager/activity/LocalThemeListFragment;
.super Lmiui/resourcebrowser/activity/LocalResourceListFragment;
.source "LocalThemeListFragment.java"

# interfaces
.implements Lcom/android/thememanager/ThemeResourceConstants;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/thememanager/activity/LocalThemeListFragment$NameComparator;
    }
.end annotation


# instance fields
.field private mListHeaderViewHeight:I

.field private mLockstyleSettingMenu:Landroid/view/MenuItem;

.field private mPickerIntent:Landroid/content/Intent;

.field private mResourceType:J

.field private mThemeSourcePath:Ljava/lang/String;

.field private mThirdAppInfoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Lmiui/resourcebrowser/activity/LocalResourceListFragment;-><init>()V

    .line 55
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mThemeSourcePath:Ljava/lang/String;

    .line 57
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mListHeaderViewHeight:I

    .line 400
    return-void
.end method

.method static synthetic access$002(Lcom/android/thememanager/activity/LocalThemeListFragment;Landroid/os/PowerManager$WakeLock;)Landroid/os/PowerManager$WakeLock;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 45
    iput-object p1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    return-object p1
.end method

.method static synthetic access$100(Lcom/android/thememanager/activity/LocalThemeListFragment;)Landroid/app/Activity;
    .locals 1
    .parameter "x0"

    .prologue
    .line 45
    iget-object v0, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/thememanager/activity/LocalThemeListFragment;)Landroid/app/ProgressDialog;
    .locals 1
    .parameter "x0"

    .prologue
    .line 45
    iget-object v0, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/android/thememanager/activity/LocalThemeListFragment;)Landroid/app/ProgressDialog;
    .locals 1
    .parameter "x0"

    .prologue
    .line 45
    iget-object v0, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/android/thememanager/activity/LocalThemeListFragment;)Landroid/app/ProgressDialog;
    .locals 1
    .parameter "x0"

    .prologue
    .line 45
    iget-object v0, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/android/thememanager/activity/LocalThemeListFragment;)Landroid/app/ProgressDialog;
    .locals 1
    .parameter "x0"

    .prologue
    .line 45
    iget-object v0, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/android/thememanager/activity/LocalThemeListFragment;)Lmiui/resourcebrowser/activity/ResourceAdapter;
    .locals 1
    .parameter "x0"

    .prologue
    .line 45
    iget-object v0, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/android/thememanager/activity/LocalThemeListFragment;)Lmiui/resourcebrowser/activity/ResourceAdapter;
    .locals 1
    .parameter "x0"

    .prologue
    .line 45
    iget-object v0, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/android/thememanager/activity/LocalThemeListFragment;)Landroid/os/PowerManager$WakeLock;
    .locals 1
    .parameter "x0"

    .prologue
    .line 45
    iget-object v0, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/android/thememanager/activity/LocalThemeListFragment;)Landroid/os/PowerManager$WakeLock;
    .locals 1
    .parameter "x0"

    .prologue
    .line 45
    iget-object v0, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/android/thememanager/activity/LocalThemeListFragment;)Landroid/view/View;
    .locals 1
    .parameter "x0"

    .prologue
    .line 45
    iget-object v0, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mProgressBar:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/android/thememanager/activity/LocalThemeListFragment;)Landroid/app/ProgressDialog;
    .locals 1
    .parameter "x0"

    .prologue
    .line 45
    iget-object v0, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/thememanager/activity/LocalThemeListFragment;)Landroid/os/PowerManager$WakeLock;
    .locals 1
    .parameter "x0"

    .prologue
    .line 45
    iget-object v0, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/android/thememanager/activity/LocalThemeListFragment;)Landroid/app/ProgressDialog;
    .locals 1
    .parameter "x0"

    .prologue
    .line 45
    iget-object v0, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/android/thememanager/activity/LocalThemeListFragment;)Lmiui/resourcebrowser/ResourceContext;
    .locals 1
    .parameter "x0"

    .prologue
    .line 45
    iget-object v0, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/android/thememanager/activity/LocalThemeListFragment;)Lmiui/resourcebrowser/ResourceContext;
    .locals 1
    .parameter "x0"

    .prologue
    .line 45
    iget-object v0, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/android/thememanager/activity/LocalThemeListFragment;)Lmiui/resourcebrowser/activity/ResourceAdapter;
    .locals 1
    .parameter "x0"

    .prologue
    .line 45
    iget-object v0, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/android/thememanager/activity/LocalThemeListFragment;)Lmiui/resourcebrowser/activity/ResourceAdapter;
    .locals 1
    .parameter "x0"

    .prologue
    .line 45
    iget-object v0, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/android/thememanager/activity/LocalThemeListFragment;)Lmiui/resourcebrowser/activity/ResourceAdapter;
    .locals 1
    .parameter "x0"

    .prologue
    .line 45
    iget-object v0, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/thememanager/activity/LocalThemeListFragment;)Landroid/view/View;
    .locals 1
    .parameter "x0"

    .prologue
    .line 45
    iget-object v0, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mProgressBar:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$402(Lcom/android/thememanager/activity/LocalThemeListFragment;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 45
    iput-object p1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic access$500(Lcom/android/thememanager/activity/LocalThemeListFragment;)Landroid/app/Activity;
    .locals 1
    .parameter "x0"

    .prologue
    .line 45
    iget-object v0, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$600(Lcom/android/thememanager/activity/LocalThemeListFragment;)Landroid/app/ProgressDialog;
    .locals 1
    .parameter "x0"

    .prologue
    .line 45
    iget-object v0, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$700(Lcom/android/thememanager/activity/LocalThemeListFragment;)Landroid/app/Activity;
    .locals 1
    .parameter "x0"

    .prologue
    .line 45
    iget-object v0, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$800(Lcom/android/thememanager/activity/LocalThemeListFragment;)Landroid/app/ProgressDialog;
    .locals 1
    .parameter "x0"

    .prologue
    .line 45
    iget-object v0, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$900(Lcom/android/thememanager/activity/LocalThemeListFragment;)Landroid/app/ProgressDialog;
    .locals 1
    .parameter "x0"

    .prologue
    .line 45
    iget-object v0, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method private getRingtoneFiltingHeader()Landroid/view/View;
    .locals 19

    .prologue
    .line 274
    new-instance v10, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mActivity:Landroid/app/Activity;

    invoke-direct {v10, v13}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 275
    .local v10, root:Landroid/widget/LinearLayout;
    const/4 v13, 0x0

    invoke-virtual {v10, v13}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 276
    const v13, 0x7f020012

    invoke-virtual {v10, v13}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 277
    invoke-virtual/range {p0 .. p0}, Lcom/android/thememanager/activity/LocalThemeListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f020012

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v13

    invoke-virtual {v13}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mListHeaderViewHeight:I

    .line 279
    const/4 v13, 0x3

    new-array v11, v13, [I

    fill-array-data v11, :array_0

    .line 284
    .local v11, titleId:[I
    const/4 v13, 0x3

    new-array v8, v13, [I

    fill-array-data v8, :array_1

    .line 289
    .local v8, minLimit:[I
    const/4 v13, 0x3

    new-array v6, v13, [I

    fill-array-data v6, :array_2

    .line 294
    .local v6, maxLimit:[I
    const/4 v13, 0x3

    new-array v2, v13, [Z

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-wide v15, v0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mResourceType:J

    const-wide/16 v17, 0x200

    cmp-long v13, v15, v17

    if-eqz v13, :cond_0

    const/4 v13, 0x1

    :goto_0
    aput-boolean v13, v2, v14

    const/4 v14, 0x1

    move-object/from16 v0, p0

    iget-wide v15, v0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mResourceType:J

    const-wide/16 v17, 0x200

    cmp-long v13, v15, v17

    if-nez v13, :cond_1

    const/4 v13, 0x1

    :goto_1
    aput-boolean v13, v2, v14

    const/4 v13, 0x2

    const/4 v14, 0x0

    aput-boolean v14, v2, v13

    .line 299
    .local v2, defaultSelected:[Z
    const/4 v13, 0x3

    new-array v9, v13, [Landroid/graphics/drawable/Drawable;

    const/4 v13, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/android/thememanager/activity/LocalThemeListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f020013

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v14

    aput-object v14, v9, v13

    const/4 v13, 0x1

    invoke-virtual/range {p0 .. p0}, Lcom/android/thememanager/activity/LocalThemeListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f020013

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v14

    aput-object v14, v9, v13

    const/4 v13, 0x2

    const/4 v14, 0x0

    aput-object v14, v9, v13

    .line 305
    .local v9, rightDrawable:[Landroid/graphics/drawable/Drawable;
    new-instance v3, Lcom/android/thememanager/activity/LocalThemeListFragment$2;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/android/thememanager/activity/LocalThemeListFragment$2;-><init>(Lcom/android/thememanager/activity/LocalThemeListFragment;)V

    .line 317
    .local v3, handler:Landroid/os/Handler;
    const/4 v4, 0x0

    .local v4, i:I
    :goto_2
    array-length v13, v11

    if-ge v4, v13, :cond_2

    .line 318
    new-instance v12, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mActivity:Landroid/app/Activity;

    const/4 v14, 0x0

    const v15, 0x10102f5

    invoke-direct {v12, v13, v14, v15}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 319
    .local v12, tv:Landroid/widget/TextView;
    const/16 v13, 0x11

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setGravity(I)V

    .line 320
    aget v13, v11, v4

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(I)V

    .line 321
    const/4 v13, 0x0

    const/4 v14, 0x0

    aget-object v15, v9, v4

    const/16 v16, 0x0

    invoke-virtual/range {v12 .. v16}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 323
    new-instance v1, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mActivity:Landroid/app/Activity;

    invoke-direct {v1, v13}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 324
    .local v1, container:Landroid/widget/LinearLayout;
    new-instance v13, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v14, -0x1

    const/4 v15, -0x1

    invoke-direct {v13, v14, v15}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v12, v13}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 326
    aget-boolean v13, v2, v4

    invoke-virtual {v1, v13}, Landroid/widget/LinearLayout;->setSelected(Z)V

    .line 328
    aget v7, v8, v4

    .line 329
    .local v7, minDuration:I
    aget v5, v6, v4

    .line 330
    .local v5, maxDuration:I
    new-instance v13, Lcom/android/thememanager/activity/LocalThemeListFragment$3;

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v3, v7, v5}, Lcom/android/thememanager/activity/LocalThemeListFragment$3;-><init>(Lcom/android/thememanager/activity/LocalThemeListFragment;Landroid/os/Handler;II)V

    invoke-virtual {v1, v13}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 348
    new-instance v13, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v14, 0x0

    const/4 v15, -0x1

    const/high16 v16, 0x3f80

    invoke-direct/range {v13 .. v16}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v10, v1, v13}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 317
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 294
    .end local v1           #container:Landroid/widget/LinearLayout;
    .end local v2           #defaultSelected:[Z
    .end local v3           #handler:Landroid/os/Handler;
    .end local v4           #i:I
    .end local v5           #maxDuration:I
    .end local v7           #minDuration:I
    .end local v9           #rightDrawable:[Landroid/graphics/drawable/Drawable;
    .end local v12           #tv:Landroid/widget/TextView;
    :cond_0
    const/4 v13, 0x0

    goto/16 :goto_0

    :cond_1
    const/4 v13, 0x0

    goto/16 :goto_1

    .line 351
    .restart local v2       #defaultSelected:[Z
    .restart local v3       #handler:Landroid/os/Handler;
    .restart local v4       #i:I
    .restart local v9       #rightDrawable:[Landroid/graphics/drawable/Drawable;
    :cond_2
    return-object v10

    .line 279
    nop

    :array_0
    .array-data 0x4
        0x40t 0x0t 0xbt 0x7ft
        0x41t 0x0t 0xbt 0x7ft
        0x42t 0x0t 0xbt 0x7ft
    .end array-data

    .line 284
    :array_1
    .array-data 0x4
        0x88t 0x13t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
    .end array-data

    .line 289
    :array_2
    .array-data 0x4
        0xfft 0xfft 0xfft 0x7ft
        0x88t 0x13t 0x0t 0x0t
        0xfft 0xfft 0xfft 0x7ft
    .end array-data
.end method

.method private refreshCurrentUsingFlags()V
    .locals 4

    .prologue
    .line 82
    iget-object v1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mActivity:Landroid/app/Activity;

    iget-wide v2, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mResourceType:J

    invoke-static {v1, v2, v3}, Lcom/android/thememanager/util/UIHelper;->computeCurrentUsingPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    .line 83
    .local v0, usingPath:Ljava/lang/String;
    iget-object v1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v1, v0}, Lmiui/resourcebrowser/ResourceContext;->setCurrentUsingPath(Ljava/lang/String;)V

    .line 84
    iget-object v1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;

    invoke-virtual {v1}, Lmiui/resourcebrowser/activity/ResourceAdapter;->notifyDataSetChanged()V

    .line 85
    return-void
.end method

.method private resolveIntent(Landroid/content/Intent;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 7
    .parameter "targetIntent"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 378
    .local p2, skipList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v6, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 379
    .local v2, pm:Landroid/content/pm/PackageManager;
    const/high16 v6, 0x1

    invoke-virtual {v2, p1, v6}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v3

    .line 381
    .local v3, resolveList:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz p2, :cond_2

    .line 382
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v0, v6, -0x1

    .local v0, i:I
    :goto_0
    if-ltz v0, :cond_2

    .line 383
    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/ResolveInfo;

    .line 384
    .local v4, ri:Landroid/content/pm/ResolveInfo;
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 385
    .local v5, skipItem:Ljava/lang/String;
    iget-object v6, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v6, v6, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v6, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    if-ltz v6, :cond_0

    .line 386
    invoke-interface {v3, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 382
    .end local v5           #skipItem:Ljava/lang/String;
    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 393
    .end local v0           #i:I
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v4           #ri:Landroid/content/pm/ResolveInfo;
    :cond_2
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    if-lez v6, :cond_3

    .line 394
    new-instance v6, Lcom/android/thememanager/activity/LocalThemeListFragment$NameComparator;

    invoke-direct {v6, v2}, Lcom/android/thememanager/activity/LocalThemeListFragment$NameComparator;-><init>(Landroid/content/pm/PackageManager;)V

    invoke-static {v3, v6}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 397
    :cond_3
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v6
.end method

.method private resolveIntent()V
    .locals 5

    .prologue
    .line 355
    iget-object v0, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mThirdAppInfoList:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 356
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 357
    const-class v1, Lcom/android/thememanager/activity/ThemeTabActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 359
    iget-wide v1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mResourceType:J

    const-wide/16 v3, 0x2

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    iget-wide v1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mResourceType:J

    const-wide/16 v3, 0x4

    cmp-long v1, v1, v3

    if-nez v1, :cond_2

    .line 360
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.GET_CONTENT"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mPickerIntent:Landroid/content/Intent;

    .line 361
    iget-object v1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mPickerIntent:Landroid/content/Intent;

    const-string v2, "android.intent.category.OPENABLE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 362
    iget-object v1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mPickerIntent:Landroid/content/Intent;

    const-string v2, "image/*"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 373
    :goto_0
    iget-object v1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mPickerIntent:Landroid/content/Intent;

    invoke-direct {p0, v1, v0}, Lcom/android/thememanager/activity/LocalThemeListFragment;->resolveIntent(Landroid/content/Intent;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mThirdAppInfoList:Ljava/util/ArrayList;

    .line 375
    :cond_1
    return-void

    .line 364
    :cond_2
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.RINGTONE_PICKER"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mPickerIntent:Landroid/content/Intent;

    .line 365
    iget-object v1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mPickerIntent:Landroid/content/Intent;

    const-string v2, "android.intent.extra.ringtone.SHOW_DEFAULT"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 366
    iget-object v1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mPickerIntent:Landroid/content/Intent;

    const-string v2, "android.intent.extra.ringtone.SHOW_SILENT"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 367
    iget-object v1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mPickerIntent:Landroid/content/Intent;

    const-string v2, "android.intent.extra.ringtone.TYPE"

    iget-wide v3, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mResourceType:J

    invoke-static {v3, v4}, Lcom/android/thememanager/util/UIHelper;->getRingtoneType(J)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 371
    const-string v1, "RingtonePickerActivity"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private updateLockstyleSettingMenuStatus()V
    .locals 2

    .prologue
    .line 88
    iget-object v1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mLockstyleSettingMenu:Landroid/view/MenuItem;

    if-eqz v1, :cond_0

    .line 89
    iget-object v1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v1}, Lcom/android/thememanager/view/LockscreenConfigSettings;->containConfigFile(Landroid/content/Context;)Z

    move-result v0

    .line 90
    .local v0, canCustomized:Z
    iget-object v1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mLockstyleSettingMenu:Landroid/view/MenuItem;

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 92
    .end local v0           #canCustomized:Z
    :cond_0
    return-void
.end method


# virtual methods
.method protected getBatchOperationHandler()Lmiui/resourcebrowser/util/BatchResourceHandler;
    .locals 6

    .prologue
    .line 61
    iget-wide v0, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mResourceType:J

    invoke-static {v0, v1}, Lcom/android/thememanager/util/UIHelper;->isAudioResource(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    new-instance v0, Lcom/android/thememanager/util/ThemeAudioBatchHandler;

    iget-object v2, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;

    iget-object v3, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    iget-wide v4, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mResourceType:J

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/thememanager/util/ThemeAudioBatchHandler;-><init>(Lmiui/resourcebrowser/activity/ResourceListFragment;Lmiui/resourcebrowser/activity/ResourceAdapter;Lmiui/resourcebrowser/ResourceContext;J)V

    .line 64
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lmiui/resourcebrowser/activity/LocalResourceListFragment;->getBatchOperationHandler()Lmiui/resourcebrowser/util/BatchResourceHandler;

    move-result-object v0

    goto :goto_0
.end method

.method protected getHeaderView()Landroid/view/View;
    .locals 3

    .prologue
    .line 264
    const/4 v0, 0x0

    .line 265
    .local v0, v:Landroid/view/View;
    iget-wide v1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mResourceType:J

    invoke-static {v1, v2}, Lcom/android/thememanager/util/UIHelper;->isAudioResource(J)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 266
    invoke-direct {p0}, Lcom/android/thememanager/activity/LocalThemeListFragment;->getRingtoneFiltingHeader()Landroid/view/View;

    move-result-object v0

    .line 270
    :goto_0
    return-object v0

    .line 268
    :cond_0
    invoke-super {p0}, Lmiui/resourcebrowser/activity/LocalResourceListFragment;->getHeaderView()Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method protected initParams()V
    .locals 4

    .prologue
    .line 69
    iget-object v0, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    const-string v1, "EXTRA_CTX_RESOURCE_TYPE"

    const-wide/16 v2, -0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmiui/resourcebrowser/ResourceContext;->getExtraMeta(Ljava/lang/String;Ljava/io/Serializable;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mResourceType:J

    .line 70
    iget-object v0, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mActivity:Landroid/app/Activity;

    iget-wide v1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mResourceType:J

    invoke-static {v0, v1, v2}, Lcom/android/thememanager/util/ThemeHelper;->setMusicVolumeType(Landroid/app/Activity;J)V

    .line 71
    invoke-super {p0}, Lmiui/resourcebrowser/activity/LocalResourceListFragment;->initParams()V

    .line 72
    return-void
.end method

.method public initializeDataSet()V
    .locals 3

    .prologue
    .line 252
    invoke-super {p0}, Lmiui/resourcebrowser/activity/LocalResourceListFragment;->initializeDataSet()V

    .line 253
    iget-wide v0, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mResourceType:J

    invoke-static {v0, v1}, Lcom/android/thememanager/util/UIHelper;->isAudioResource(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 254
    iget-object v0, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mAdapter:Lmiui/resourcebrowser/activity/ResourceAdapter;

    invoke-virtual {v0}, Lmiui/resourcebrowser/activity/ResourceAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 255
    iget-object v0, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mListView:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    .line 260
    :cond_0
    :goto_0
    return-void

    .line 257
    :cond_1
    iget-object v0, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mListView:Landroid/widget/ListView;

    const/4 v1, 0x0

    iget v2, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mListHeaderViewHeight:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->scrollBy(II)V

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 14
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 96
    iget-object v1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-virtual {v1}, Lmiui/resourcebrowser/ResourceContext;->isPicker()Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz p3, :cond_1

    .line 97
    invoke-super/range {p0 .. p3}, Lmiui/resourcebrowser/activity/LocalResourceListFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 190
    :cond_0
    :goto_0
    return-void

    .line 99
    :cond_1
    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    .line 101
    :sswitch_0
    if-eqz p3, :cond_0

    .line 102
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v11

    .line 103
    .local v11, sourcePath:Ljava/lang/String;
    const/4 v1, -0x1

    move/from16 v0, p2

    if-ne v0, v1, :cond_0

    .line 104
    new-instance v1, Lcom/android/thememanager/activity/LocalThemeListFragment$1;

    iget-object v2, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "import-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, p0, v2, v3}, Lcom/android/thememanager/activity/LocalThemeListFragment$1;-><init>(Lcom/android/thememanager/activity/LocalThemeListFragment;Lmiui/resourcebrowser/ResourceContext;Ljava/lang/String;)V

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    sget-object v4, Lmiui/resourcebrowser/controller/local/ImportResourceTask;->IMPORT_BY_PATH:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v11, v2, v3

    invoke-virtual {v1, v2}, Lcom/android/thememanager/activity/LocalThemeListFragment$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 146
    const-string v1, "/"

    invoke-virtual {v11, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v10

    .line 147
    .local v10, slashPos:I
    const/4 v1, 0x0

    invoke-virtual {v11, v1, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mThemeSourcePath:Ljava/lang/String;

    goto :goto_0

    .line 153
    .end local v10           #slashPos:I
    .end local v11           #sourcePath:Ljava/lang/String;
    :sswitch_1
    if-eqz p3, :cond_0

    .line 154
    const/4 v8, 0x1

    .line 156
    .local v8, changed:Z
    const-string v1, "android.intent.extra.ringtone.PICKED_URI"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v13

    check-cast v13, Landroid/net/Uri;

    .line 157
    .local v13, uri:Landroid/net/Uri;
    iget-object v1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v1, v13}, Lmiui/resourcebrowser/util/ResourceHelper;->getPathByUri(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v9

    .line 158
    .local v9, path:Ljava/lang/String;
    iget-wide v1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mResourceType:J

    const-wide/16 v3, 0x40

    cmp-long v1, v1, v3

    if-nez v1, :cond_4

    .line 159
    iget-object v1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v1, v9}, Lcom/android/thememanager/util/ThemeHelper;->applyBootAudio(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v8

    .line 169
    :cond_2
    :goto_1
    if-eqz v8, :cond_3

    .line 170
    const v1, 0x7f0b000c

    invoke-virtual {p0, v1}, Lcom/android/thememanager/activity/LocalThemeListFragment;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 171
    .local v12, title:Ljava/lang/String;
    iget-wide v1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mResourceType:J

    invoke-static {v1, v2, v9, v12}, Lcom/android/thememanager/util/ThemeHelper;->saveUserPreference(JLjava/lang/String;Ljava/lang/String;)V

    .line 174
    .end local v12           #title:Ljava/lang/String;
    :cond_3
    iget-object v1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v1, v8}, Lcom/android/thememanager/util/ThemeHelper;->showThemeChangedToast(Landroid/content/Context;Z)V

    goto :goto_0

    .line 160
    :cond_4
    iget-wide v1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mResourceType:J

    const-wide/16 v3, 0x100

    cmp-long v1, v1, v3

    if-nez v1, :cond_5

    .line 161
    iget-object v1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mActivity:Landroid/app/Activity;

    const/4 v2, 0x1

    invoke-static {v1, v2, v13}, Landroid/media/RingtoneManager;->setActualDefaultRingtoneUri(Landroid/content/Context;ILandroid/net/Uri;)V

    goto :goto_1

    .line 162
    :cond_5
    iget-wide v1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mResourceType:J

    const-wide/16 v3, 0x200

    cmp-long v1, v1, v3

    if-nez v1, :cond_6

    .line 163
    iget-object v1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mActivity:Landroid/app/Activity;

    const/4 v2, 0x2

    invoke-static {v1, v2, v13}, Landroid/media/RingtoneManager;->setActualDefaultRingtoneUri(Landroid/content/Context;ILandroid/net/Uri;)V

    goto :goto_1

    .line 165
    :cond_6
    iget-wide v1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mResourceType:J

    const-wide/16 v3, 0x400

    cmp-long v1, v1, v3

    if-nez v1, :cond_2

    .line 166
    iget-object v1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mActivity:Landroid/app/Activity;

    const/4 v2, 0x4

    invoke-static {v1, v2, v13}, Landroid/media/RingtoneManager;->setActualDefaultRingtoneUri(Landroid/content/Context;ILandroid/net/Uri;)V

    goto :goto_1

    .line 179
    .end local v8           #changed:Z
    .end local v9           #path:Ljava/lang/String;
    .end local v13           #uri:Landroid/net/Uri;
    :sswitch_2
    if-eqz p3, :cond_0

    .line 180
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v5

    .line 181
    .local v5, wallpaperUri:Landroid/net/Uri;
    iget-object v1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mActivity:Landroid/app/Activity;

    iget-wide v3, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mResourceType:J

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v2, p0

    invoke-static/range {v1 .. v7}, Lcom/android/thememanager/util/WallpaperUtils;->cropAndApplyWallpaper(Landroid/app/Activity;Landroid/app/Fragment;JLandroid/net/Uri;ZZ)Z

    goto/16 :goto_0

    .line 186
    .end local v5           #wallpaperUri:Landroid/net/Uri;
    :sswitch_3
    iget-object v1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mActivity:Landroid/app/Activity;

    move/from16 v0, p2

    invoke-static {v1, p1, v0}, Lcom/android/thememanager/util/WallpaperUtils;->dealCropWallpaperResult(Landroid/content/Context;II)V

    goto/16 :goto_0

    .line 99
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0x65 -> :sswitch_1
        0x66 -> :sswitch_2
        0x7001 -> :sswitch_3
        0x7002 -> :sswitch_3
    .end sparse-switch
.end method

.method public onFragmentCreateOptionsMenu(Landroid/view/Menu;)Ljava/util/List;
    .locals 8
    .parameter "menu"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Menu;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 194
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 196
    .local v2, ret:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    .line 197
    .local v1, menuId:I
    iget-wide v3, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mResourceType:J

    const-wide/16 v5, 0x1000

    cmp-long v3, v3, v5

    if-nez v3, :cond_0

    .line 198
    const v1, 0x7f0b002a

    .line 199
    invoke-interface {p1, v7, v1, v7, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    .line 200
    .local v0, item:Landroid/view/MenuItem;
    const/4 v3, 0x2

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 201
    iput-object v0, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mLockstyleSettingMenu:Landroid/view/MenuItem;

    .line 202
    invoke-direct {p0}, Lcom/android/thememanager/activity/LocalThemeListFragment;->updateLockstyleSettingMenuStatus()V

    .line 212
    .end local v0           #item:Landroid/view/MenuItem;
    :goto_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 214
    return-object v2

    .line 203
    :cond_0
    iget-wide v3, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mResourceType:J

    invoke-static {v3, v4}, Lcom/android/thememanager/util/UIHelper;->supportImportMenu(J)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 204
    const v1, 0x7f0b0027

    .line 205
    invoke-interface {p1, v7, v1, v7, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    goto :goto_0

    .line 207
    :cond_1
    const v1, 0x7f0b0028

    .line 208
    invoke-interface {p1, v7, v1, v7, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    .line 209
    .restart local v0       #item:Landroid/view/MenuItem;
    const v3, -0x7ffffffe

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 210
    const v3, 0x7f020010

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onFragmentOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 6
    .parameter

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 219
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0b0027

    if-ne v0, v1, :cond_1

    .line 220
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 221
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mThemeSourcePath:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 222
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 223
    const-string v1, "root_directory"

    const-string v2, "/"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 224
    const-string v1, "ext_filter"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "zip"

    aput-object v3, v2, v4

    const-string v3, "mtz"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 227
    const-string v1, "ext_file_first"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 228
    const-string v1, "back_to_parent_directory"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 229
    const/16 v1, 0x64

    invoke-virtual {p0, v0, v1}, Lcom/android/thememanager/activity/LocalThemeListFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 248
    :cond_0
    :goto_0
    return-void

    .line 230
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0b0028

    if-ne v0, v1, :cond_5

    .line 231
    invoke-direct {p0}, Lcom/android/thememanager/activity/LocalThemeListFragment;->resolveIntent()V

    .line 232
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mActivity:Landroid/app/Activity;

    const-class v2, Lcom/android/thememanager/activity/ThirdPartyPickersActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 233
    const-string v1, "android.intent.action.MAIN"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 234
    const-string v1, "android.intent.extra.INTENT"

    iget-object v2, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mPickerIntent:Landroid/content/Intent;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 235
    const-string v1, "extra_resource_type"

    iget-wide v2, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mResourceType:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 236
    const-string v1, "extra_resolve_info_list"

    iget-object v2, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mThirdAppInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 237
    iget-wide v1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mResourceType:J

    invoke-static {v1, v2}, Lcom/android/thememanager/util/UIHelper;->isAudioResource(J)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 238
    const/16 v1, 0x65

    invoke-virtual {p0, v0, v1}, Lcom/android/thememanager/activity/LocalThemeListFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 239
    :cond_2
    const-wide/16 v1, 0x4

    iget-wide v3, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mResourceType:J

    cmp-long v1, v1, v3

    if-eqz v1, :cond_3

    const-wide/16 v1, 0x2

    iget-wide v3, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mResourceType:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_4

    .line 240
    :cond_3
    const/16 v1, 0x66

    invoke-virtual {p0, v0, v1}, Lcom/android/thememanager/activity/LocalThemeListFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 242
    :cond_4
    invoke-virtual {p0, v0}, Lcom/android/thememanager/activity/LocalThemeListFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 244
    :cond_5
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0b002a

    if-ne v0, v1, :cond_0

    .line 245
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/thememanager/activity/LocalThemeListFragment;->mActivity:Landroid/app/Activity;

    const-class v2, Lcom/android/thememanager/view/LockscreenConfigSettings;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 246
    invoke-virtual {p0, v0}, Lcom/android/thememanager/activity/LocalThemeListFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onVisible()V
    .locals 0

    .prologue
    .line 76
    invoke-super {p0}, Lmiui/resourcebrowser/activity/LocalResourceListFragment;->onVisible()V

    .line 77
    invoke-direct {p0}, Lcom/android/thememanager/activity/LocalThemeListFragment;->refreshCurrentUsingFlags()V

    .line 78
    invoke-direct {p0}, Lcom/android/thememanager/activity/LocalThemeListFragment;->updateLockstyleSettingMenuStatus()V

    .line 79
    return-void
.end method
