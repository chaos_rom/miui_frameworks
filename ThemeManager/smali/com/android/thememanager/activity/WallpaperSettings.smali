.class public Lcom/android/thememanager/activity/WallpaperSettings;
.super Lmiui/preference/BasePreferenceActivity;
.source "WallpaperSettings.java"

# interfaces
.implements Lmiui/app/SDCardMonitor$SDCardStatusListener;


# instance fields
.field private mSDCardMonitor:Lmiui/app/SDCardMonitor;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lmiui/preference/BasePreferenceActivity;-><init>()V

    return-void
.end method

.method private checkLiveWallpaperPicker()V
    .locals 5

    .prologue
    .line 48
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 49
    .local v0, intent:Landroid/content/Intent;
    const-string v3, "com.android.wallpaper.livepicker"

    const-string v4, "com.android.wallpaper.livepicker.LiveWallpaperActivity"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 50
    invoke-virtual {p0}, Lcom/android/thememanager/activity/WallpaperSettings;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v0, v4}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 51
    .local v1, list:Ljava/util/List;
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 52
    :cond_0
    const-string v3, "live_wallpaper"

    invoke-virtual {p0, v3}, Lcom/android/thememanager/activity/WallpaperSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    .line 53
    .local v2, pref:Landroid/preference/Preference;
    invoke-virtual {p0}, Lcom/android/thememanager/activity/WallpaperSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 55
    .end local v2           #pref:Landroid/preference/Preference;
    :cond_1
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .parameter "savedInstanceState"

    .prologue
    .line 23
    invoke-super {p0, p1}, Lmiui/preference/BasePreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 24
    const v0, 0x7f03000e

    invoke-virtual {p0, v0}, Lcom/android/thememanager/activity/WallpaperSettings;->addPreferencesFromResource(I)V

    .line 25
    invoke-direct {p0}, Lcom/android/thememanager/activity/WallpaperSettings;->checkLiveWallpaperPicker()V

    .line 26
    invoke-static {p0}, Lmiui/app/SDCardMonitor;->getSDCardMonitor(Landroid/content/Context;)Lmiui/app/SDCardMonitor;

    move-result-object v0

    iput-object v0, p0, Lcom/android/thememanager/activity/WallpaperSettings;->mSDCardMonitor:Lmiui/app/SDCardMonitor;

    .line 27
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperSettings;->mSDCardMonitor:Lmiui/app/SDCardMonitor;

    invoke-virtual {v0, p0}, Lmiui/app/SDCardMonitor;->addListener(Lmiui/app/SDCardMonitor$SDCardStatusListener;)V

    .line 28
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperSettings;->mSDCardMonitor:Lmiui/app/SDCardMonitor;

    invoke-virtual {v0, p0}, Lmiui/app/SDCardMonitor;->removeListener(Lmiui/app/SDCardMonitor$SDCardStatusListener;)V

    .line 38
    invoke-super {p0}, Lmiui/preference/BasePreferenceActivity;->onDestroy()V

    .line 39
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 43
    invoke-super {p0}, Lmiui/preference/BasePreferenceActivity;->onResume()V

    .line 44
    invoke-virtual {p0}, Lcom/android/thememanager/activity/WallpaperSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getRootAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/BaseAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 45
    return-void
.end method

.method public onStatusChanged(Z)V
    .locals 0
    .parameter "mount"

    .prologue
    .line 32
    invoke-static {p0}, Lmiui/resourcebrowser/util/ResourceHelper;->exit(Landroid/app/Activity;)V

    .line 33
    return-void
.end method
