.class public Lcom/android/thememanager/activity/WallpaperDetailActivity;
.super Lmiui/resourcebrowser/activity/ResourceDetailActivity;
.source "WallpaperDetailActivity.java"

# interfaces
.implements Lcom/android/thememanager/ThemeResourceConstants;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/thememanager/activity/WallpaperDetailActivity$MenuAdapter;,
        Lcom/android/thememanager/activity/WallpaperDetailActivity$WallpaperGestureListener;
    }
.end annotation


# instance fields
.field private mDownloadBtn:Landroid/widget/TextView;

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private mImageAsyncDecoder:Lmiui/resourcebrowser/util/ImageCacheDecoder;

.field private mIsLockscreen:Z

.field private mMoreMenuBtn:Landroid/widget/ImageView;

.field private mOperateBarView:Landroid/view/View;

.field private mPopupWindow:Landroid/widget/PopupWindow;

.field private mPreviewMaskView:Landroid/widget/ImageView;

.field private mResourceType:J

.field private mSliderView:Lcom/android/thememanager/view/HorzontalSliderView;

.field private mThumbnailModeOfWallpaperBeforePreview:Z

.field private mTitleAreaView:Landroid/view/View;

.field private mWallpaperHeight:I

.field private mWallpaperView:Lcom/android/thememanager/view/WallpaperView;

.field private mWallpaperWidth:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;-><init>()V

    .line 704
    return-void
.end method

.method static synthetic access$000(Lcom/android/thememanager/activity/WallpaperDetailActivity;Z)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->quitPreviewMode(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/android/thememanager/activity/WallpaperDetailActivity;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->enterPreviewMode()V

    return-void
.end method

.method static synthetic access$1000(Lcom/android/thememanager/activity/WallpaperDetailActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->checkInfoValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1100(Lcom/android/thememanager/activity/WallpaperDetailActivity;)Lmiui/resourcebrowser/model/Resource;
    .locals 1
    .parameter "x0"

    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/android/thememanager/activity/WallpaperDetailActivity;)Lmiui/resourcebrowser/model/Resource;
    .locals 1
    .parameter "x0"

    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/android/thememanager/activity/WallpaperDetailActivity;)Z
    .locals 1
    .parameter "x0"

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mIsOnlineResourceSet:Z

    return v0
.end method

.method static synthetic access$1400(Lcom/android/thememanager/activity/WallpaperDetailActivity;)Lmiui/resourcebrowser/model/Resource;
    .locals 1
    .parameter "x0"

    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/android/thememanager/activity/WallpaperDetailActivity;)Landroid/widget/PopupWindow;
    .locals 1
    .parameter "x0"

    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mPopupWindow:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/android/thememanager/activity/WallpaperDetailActivity;Landroid/widget/PopupWindow;)Landroid/widget/PopupWindow;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 59
    iput-object p1, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mPopupWindow:Landroid/widget/PopupWindow;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/android/thememanager/activity/WallpaperDetailActivity;)Landroid/view/View;
    .locals 1
    .parameter "x0"

    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mOperateBarView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/android/thememanager/activity/WallpaperDetailActivity;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->navigateToPreviousResource()V

    return-void
.end method

.method static synthetic access$1800(Lcom/android/thememanager/activity/WallpaperDetailActivity;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->navigateToNextResource()V

    return-void
.end method

.method static synthetic access$1900(Lcom/android/thememanager/activity/WallpaperDetailActivity;)Lmiui/resourcebrowser/model/Resource;
    .locals 1
    .parameter "x0"

    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/thememanager/activity/WallpaperDetailActivity;)J
    .locals 2
    .parameter "x0"

    .prologue
    .line 59
    iget-wide v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mResourceType:J

    return-wide v0
.end method

.method static synthetic access$2000(Lcom/android/thememanager/activity/WallpaperDetailActivity;Lmiui/resourcebrowser/model/Resource;)Ljava/lang/String;
    .locals 1
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->getOnlinePreviewUrl(Lmiui/resourcebrowser/model/Resource;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2100(Lcom/android/thememanager/activity/WallpaperDetailActivity;Ljava/lang/String;)Z
    .locals 1
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->isVisiableImagePath(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$2200(Lcom/android/thememanager/activity/WallpaperDetailActivity;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->initWallpaperViewBitmap()V

    return-void
.end method

.method static synthetic access$2300(Lcom/android/thememanager/activity/WallpaperDetailActivity;)Landroid/widget/TextView;
    .locals 1
    .parameter "x0"

    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mDownloadBtn:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/android/thememanager/activity/WallpaperDetailActivity;)Lmiui/resourcebrowser/model/Resource;
    .locals 1
    .parameter "x0"

    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/android/thememanager/activity/WallpaperDetailActivity;)Lcom/android/thememanager/view/WallpaperView;
    .locals 1
    .parameter "x0"

    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mWallpaperView:Lcom/android/thememanager/view/WallpaperView;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/android/thememanager/activity/WallpaperDetailActivity;)Landroid/view/View;
    .locals 1
    .parameter "x0"

    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mTitleAreaView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/android/thememanager/activity/WallpaperDetailActivity;Z)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->updateTitleAndOperateBarState(Z)V

    return-void
.end method

.method static synthetic access$2800(Lcom/android/thememanager/activity/WallpaperDetailActivity;Z)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->autoUpdateSliderViewState(Z)V

    return-void
.end method

.method static synthetic access$2900(Lcom/android/thememanager/activity/WallpaperDetailActivity;Z)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->updateSliderViewState(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/android/thememanager/activity/WallpaperDetailActivity;)Lmiui/resourcebrowser/model/Resource;
    .locals 1
    .parameter "x0"

    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/android/thememanager/activity/WallpaperDetailActivity;)Lmiui/resourcebrowser/model/Resource;
    .locals 1
    .parameter "x0"

    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    return-object v0
.end method

.method static synthetic access$3100(Lcom/android/thememanager/activity/WallpaperDetailActivity;)Lmiui/resourcebrowser/model/Resource;
    .locals 1
    .parameter "x0"

    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/android/thememanager/activity/WallpaperDetailActivity;)Lmiui/resourcebrowser/model/Resource;
    .locals 1
    .parameter "x0"

    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/android/thememanager/activity/WallpaperDetailActivity;)Lmiui/resourcebrowser/model/Resource;
    .locals 1
    .parameter "x0"

    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/android/thememanager/activity/WallpaperDetailActivity;)Lmiui/resourcebrowser/model/Resource;
    .locals 1
    .parameter "x0"

    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/android/thememanager/activity/WallpaperDetailActivity;)Lmiui/resourcebrowser/model/Resource;
    .locals 1
    .parameter "x0"

    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/android/thememanager/activity/WallpaperDetailActivity;)Lmiui/resourcebrowser/controller/ResourceController;
    .locals 1
    .parameter "x0"

    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mResController:Lmiui/resourcebrowser/controller/ResourceController;

    return-object v0
.end method

.method static synthetic access$3700(Lcom/android/thememanager/activity/WallpaperDetailActivity;)Z
    .locals 1
    .parameter "x0"

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mIsOnlineResourceSet:Z

    return v0
.end method

.method static synthetic access$400(Lcom/android/thememanager/activity/WallpaperDetailActivity;JLmiui/resourcebrowser/model/Resource;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 59
    invoke-direct {p0, p1, p2, p3}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->doApplyWallpaper(JLmiui/resourcebrowser/model/Resource;)V

    return-void
.end method

.method static synthetic access$500(Lcom/android/thememanager/activity/WallpaperDetailActivity;)Lmiui/resourcebrowser/model/Resource;
    .locals 1
    .parameter "x0"

    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    return-object v0
.end method

.method static synthetic access$600(Lcom/android/thememanager/activity/WallpaperDetailActivity;Lmiui/resourcebrowser/model/Resource;)Ljava/lang/String;
    .locals 1
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->getLocalPreviewPath(Lmiui/resourcebrowser/model/Resource;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/android/thememanager/activity/WallpaperDetailActivity;)Lmiui/resourcebrowser/model/Resource;
    .locals 1
    .parameter "x0"

    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    return-object v0
.end method

.method static synthetic access$800(Lcom/android/thememanager/activity/WallpaperDetailActivity;)Z
    .locals 1
    .parameter "x0"

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mHasInfoView:Z

    return v0
.end method

.method static synthetic access$900(Lcom/android/thememanager/activity/WallpaperDetailActivity;)Lmiui/resourcebrowser/model/Resource;
    .locals 1
    .parameter "x0"

    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    return-object v0
.end method

.method private autoUpdateSliderViewState(Z)V
    .locals 1
    .parameter "visiable"

    .prologue
    .line 652
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mWallpaperView:Lcom/android/thememanager/view/WallpaperView;

    invoke-virtual {v0}, Lcom/android/thememanager/view/WallpaperView;->isThumbnailScanMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 656
    :goto_0
    return-void

    .line 655
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->updateSliderViewState(Z)V

    goto :goto_0
.end method

.method private cacheWallpaperResource(IZ)Landroid/util/Pair;
    .locals 9
    .parameter "offset"
    .parameter "async"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 358
    invoke-direct {p0, p1}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->getAdjResource(I)Lmiui/resourcebrowser/model/Resource;

    move-result-object v2

    .line 359
    .local v2, r:Lmiui/resourcebrowser/model/Resource;
    if-nez v2, :cond_0

    .line 389
    :goto_0
    return-object v7

    .line 363
    :cond_0
    iget v8, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mResourceIndex:I

    add-int v3, v8, p1

    .line 364
    .local v3, resIndex:I
    invoke-virtual {v2}, Lmiui/resourcebrowser/model/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v0

    .line 365
    .local v0, localPath:Ljava/lang/String;
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_1

    iget-boolean v8, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mIsOnlineResourceSet:Z

    if-eqz v8, :cond_1

    .line 366
    invoke-direct {p0, v2}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->getLocalPreviewPath(Lmiui/resourcebrowser/model/Resource;)Ljava/lang/String;

    move-result-object v0

    .line 369
    :cond_1
    const/4 v6, 0x0

    .line 370
    .local v6, useThumbnail:Z
    move-object v4, v0

    .line 371
    .local v4, ret:Ljava/lang/String;
    if-eqz v0, :cond_4

    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 372
    if-eqz p2, :cond_3

    .line 373
    iget-object v8, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mImageAsyncDecoder:Lmiui/resourcebrowser/util/ImageCacheDecoder;

    invoke-virtual {v8, v0, v7, v3}, Lmiui/resourcebrowser/util/ImageCacheDecoder;->decodeImageAsync(Ljava/lang/String;Ljava/lang/String;I)V

    .line 389
    :cond_2
    :goto_1
    new-instance v7, Landroid/util/Pair;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-direct {v7, v4, v8}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 375
    :cond_3
    iget-object v7, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mImageAsyncDecoder:Lmiui/resourcebrowser/util/ImageCacheDecoder;

    const/4 v8, 0x1

    invoke-virtual {v7, v0, v3, v8}, Lmiui/resourcebrowser/util/ImageCacheDecoder;->decodeLocalImage(Ljava/lang/String;IZ)Landroid/graphics/Bitmap;

    goto :goto_1

    .line 378
    :cond_4
    invoke-direct {p0, v2}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->getLocalThumbnailPath(Lmiui/resourcebrowser/model/Resource;)Ljava/lang/String;

    move-result-object v5

    .line 379
    .local v5, thumbnailPath:Ljava/lang/String;
    iget-boolean v7, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mIsOnlineResourceSet:Z

    if-eqz v7, :cond_5

    if-eqz v5, :cond_5

    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 380
    iget-object v7, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mImageAsyncDecoder:Lmiui/resourcebrowser/util/ImageCacheDecoder;

    invoke-direct {p0, v2}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->getOnlineThumbnailUrl(Lmiui/resourcebrowser/model/Resource;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v5, v8, v3}, Lmiui/resourcebrowser/util/ImageCacheDecoder;->decodeImageAsync(Ljava/lang/String;Ljava/lang/String;I)V

    .line 381
    move-object v4, v5

    .line 382
    const/4 v6, 0x1

    .line 384
    :cond_5
    invoke-direct {p0, v2}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->getOnlinePreviewUrl(Lmiui/resourcebrowser/model/Resource;)Ljava/lang/String;

    move-result-object v1

    .line 385
    .local v1, onlineUrl:Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 386
    iget-object v7, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mImageAsyncDecoder:Lmiui/resourcebrowser/util/ImageCacheDecoder;

    invoke-direct {p0, v2}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->getOnlinePreviewUrl(Lmiui/resourcebrowser/model/Resource;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v0, v8, v3}, Lmiui/resourcebrowser/util/ImageCacheDecoder;->decodeImageAsync(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_1
.end method

.method private checkInfoValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .parameter "value"

    .prologue
    .line 298
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 299
    const v0, 0x60c000c

    invoke-virtual {p0, v0}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 301
    :cond_0
    return-object p1
.end method

.method private doApplyWallpaper(JLmiui/resourcebrowser/model/Resource;)V
    .locals 8
    .parameter "resourceType"
    .parameter "resource"

    .prologue
    const-wide/16 v6, 0x0

    .line 278
    invoke-virtual {p3}, Lmiui/resourcebrowser/model/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v3

    .line 279
    .local v3, wallpaperPath:Ljava/lang/String;
    iget-object v4, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mImageAsyncDecoder:Lmiui/resourcebrowser/util/ImageCacheDecoder;

    invoke-virtual {v4, v3}, Lmiui/resourcebrowser/util/ImageCacheDecoder;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 280
    .local v2, bitmap:Landroid/graphics/Bitmap;
    if-nez v2, :cond_0

    .line 281
    iget-object v4, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mImageAsyncDecoder:Lmiui/resourcebrowser/util/ImageCacheDecoder;

    invoke-direct {p0, p3}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->getLocalPreviewPath(Lmiui/resourcebrowser/model/Resource;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lmiui/resourcebrowser/util/ImageCacheDecoder;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 284
    :cond_0
    const/4 v0, 0x1

    .line 285
    .local v0, applyDesk:Z
    const/4 v1, 0x1

    .line 286
    .local v1, applyLock:Z
    const-wide/16 v4, 0x2

    and-long/2addr v4, p1

    cmp-long v4, v4, v6

    if-eqz v4, :cond_1

    .line 287
    :try_start_0
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v4

    invoke-static {p0, v2, v4}, Lcom/android/thememanager/util/WallpaperUtils;->saveDeskWallpaperByDisplay(Landroid/content/Context;Landroid/graphics/Bitmap;Landroid/net/Uri;)Z

    move-result v0

    .line 289
    :cond_1
    const-wide/16 v4, 0x4

    and-long/2addr v4, p1

    cmp-long v4, v4, v6

    if-eqz v4, :cond_2

    .line 290
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v4

    invoke-static {p0, v2, v4}, Lcom/android/thememanager/util/WallpaperUtils;->saveLockWallpaperByDisplay(Landroid/content/Context;Landroid/graphics/Bitmap;Landroid/net/Uri;)Z

    move-result v1

    .line 292
    :cond_2
    if-eqz v0, :cond_3

    if-eqz v1, :cond_3

    const/4 v4, 0x1

    :goto_0
    invoke-static {p0, v4}, Lcom/android/thememanager/util/ThemeHelper;->showThemeChangedToast(Landroid/content/Context;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 295
    :goto_1
    return-void

    .line 292
    :cond_3
    const/4 v4, 0x0

    goto :goto_0

    .line 293
    :catch_0
    move-exception v4

    goto :goto_1
.end method

.method private enterPreviewMode()V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 627
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mWallpaperView:Lcom/android/thememanager/view/WallpaperView;

    invoke-virtual {v0}, Lcom/android/thememanager/view/WallpaperView;->isThumbnailScanMode()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mThumbnailModeOfWallpaperBeforePreview:Z

    .line 628
    iget-boolean v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mThumbnailModeOfWallpaperBeforePreview:Z

    if-eqz v0, :cond_0

    .line 629
    iget-object v3, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mWallpaperView:Lcom/android/thememanager/view/WallpaperView;

    iget-boolean v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mThumbnailModeOfWallpaperBeforePreview:Z

    if-nez v0, :cond_1

    move v0, v2

    :goto_0
    invoke-virtual {v3, v0}, Lcom/android/thememanager/view/WallpaperView;->setScanMode(Z)V

    .line 632
    :cond_0
    iget-object v7, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mPreviewMaskView:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mPreviewMaskView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getWidth()I

    move-result v3

    const-wide/16 v5, 0x12c

    move-object v0, p0

    move v4, v1

    invoke-direct/range {v0 .. v6}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->getAnimation(ZZIIJ)Landroid/view/animation/Animation;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 633
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mPreviewMaskView:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 634
    invoke-direct {p0, v1}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->updateTitleAndOperateBarState(Z)V

    .line 635
    invoke-direct {p0, v1}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->updateSliderViewState(Z)V

    .line 636
    return-void

    :cond_1
    move v0, v1

    .line 629
    goto :goto_0
.end method

.method private getAdjResource(I)Lmiui/resourcebrowser/model/Resource;
    .locals 2
    .parameter "offsetFromCurrent"

    .prologue
    .line 493
    iget v1, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mResourceIndex:I

    add-int v0, v1, p1

    .line 494
    .local v0, index:I
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mGroupDataSet:Lmiui/resourcebrowser/widget/DataGroup;

    invoke-virtual {v1}, Lmiui/resourcebrowser/widget/DataGroup;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 495
    iget-object v1, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mGroupDataSet:Lmiui/resourcebrowser/widget/DataGroup;

    invoke-virtual {v1, v0}, Lmiui/resourcebrowser/widget/DataGroup;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/resourcebrowser/model/Resource;

    .line 497
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getAnimation(ZZII)Landroid/view/animation/Animation;
    .locals 7
    .parameter "alphaAnim"
    .parameter "forEnter"
    .parameter "offsetX"
    .parameter "offsetY"

    .prologue
    .line 792
    const-wide/16 v5, 0xc8

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v6}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->getAnimation(ZZIIJ)Landroid/view/animation/Animation;

    move-result-object v0

    return-object v0
.end method

.method private getAnimation(ZZIIJ)Landroid/view/animation/Animation;
    .locals 12
    .parameter "alphaAnim"
    .parameter "forEnter"
    .parameter "offsetX"
    .parameter "offsetY"
    .parameter "duration"

    .prologue
    .line 796
    const/4 v2, 0x0

    .line 798
    .local v2, anim:Landroid/view/animation/Animation;
    if-eqz p1, :cond_1

    .line 799
    if-eqz p2, :cond_0

    const/4 v5, 0x0

    .line 800
    .local v5, startAlpha:F
    :goto_0
    new-instance v2, Landroid/view/animation/AlphaAnimation;

    .end local v2           #anim:Landroid/view/animation/Animation;
    const/high16 v8, 0x3f80

    sub-float/2addr v8, v5

    invoke-direct {v2, v5, v8}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 808
    .end local v5           #startAlpha:F
    .restart local v2       #anim:Landroid/view/animation/Animation;
    :goto_1
    move-wide/from16 v0, p5

    invoke-virtual {v2, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 809
    return-object v2

    .line 799
    :cond_0
    const/high16 v5, 0x3f80

    goto :goto_0

    .line 802
    :cond_1
    if-eqz p2, :cond_2

    move v6, p3

    .line 803
    .local v6, startX:I
    :goto_2
    sub-int v3, p3, v6

    .line 804
    .local v3, endX:I
    if-eqz p2, :cond_3

    move/from16 v7, p4

    .line 805
    .local v7, startY:I
    :goto_3
    sub-int v4, p4, v7

    .line 806
    .local v4, endY:I
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    .end local v2           #anim:Landroid/view/animation/Animation;
    int-to-float v8, v6

    int-to-float v9, v3

    int-to-float v10, v7

    int-to-float v11, v4

    invoke-direct {v2, v8, v9, v10, v11}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .restart local v2       #anim:Landroid/view/animation/Animation;
    goto :goto_1

    .line 802
    .end local v3           #endX:I
    .end local v4           #endY:I
    .end local v6           #startX:I
    .end local v7           #startY:I
    :cond_2
    const/4 v6, 0x0

    goto :goto_2

    .line 804
    .restart local v3       #endX:I
    .restart local v6       #startX:I
    :cond_3
    const/4 v7, 0x0

    goto :goto_3
.end method

.method private getImageDecodingListener()Lmiui/resourcebrowser/util/ImageCacheDecoder$ImageDecodingListener;
    .locals 1

    .prologue
    .line 571
    new-instance v0, Lcom/android/thememanager/activity/WallpaperDetailActivity$10;

    invoke-direct {v0, p0}, Lcom/android/thememanager/activity/WallpaperDetailActivity$10;-><init>(Lcom/android/thememanager/activity/WallpaperDetailActivity;)V

    return-object v0
.end method

.method private getLocalPreviewPath(Lmiui/resourcebrowser/model/Resource;)Ljava/lang/String;
    .locals 3
    .parameter "resource"

    .prologue
    .line 404
    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getPreviews()Ljava/util/List;

    move-result-object v0

    .line 405
    .local v0, entries:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/PathEntry;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 406
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/resourcebrowser/model/PathEntry;

    .line 407
    .local v1, entry:Lmiui/resourcebrowser/model/PathEntry;
    if-eqz v1, :cond_0

    .line 408
    invoke-virtual {v1}, Lmiui/resourcebrowser/model/PathEntry;->getLocalPath()Ljava/lang/String;

    move-result-object v2

    .line 411
    .end local v1           #entry:Lmiui/resourcebrowser/model/PathEntry;
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private getLocalThumbnailPath(Lmiui/resourcebrowser/model/Resource;)Ljava/lang/String;
    .locals 3
    .parameter "resource"

    .prologue
    .line 393
    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getThumbnails()Ljava/util/List;

    move-result-object v0

    .line 394
    .local v0, entries:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/PathEntry;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 395
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/resourcebrowser/model/PathEntry;

    .line 396
    .local v1, entry:Lmiui/resourcebrowser/model/PathEntry;
    if-eqz v1, :cond_0

    .line 397
    invoke-virtual {v1}, Lmiui/resourcebrowser/model/PathEntry;->getLocalPath()Ljava/lang/String;

    move-result-object v2

    .line 400
    .end local v1           #entry:Lmiui/resourcebrowser/model/PathEntry;
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private getOnlinePreviewUrl(Lmiui/resourcebrowser/model/Resource;)Ljava/lang/String;
    .locals 3
    .parameter "resource"

    .prologue
    .line 426
    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getPreviews()Ljava/util/List;

    move-result-object v0

    .line 427
    .local v0, entries:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/PathEntry;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 428
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/resourcebrowser/model/PathEntry;

    .line 429
    .local v1, entry:Lmiui/resourcebrowser/model/PathEntry;
    if-eqz v1, :cond_0

    .line 430
    invoke-virtual {v1}, Lmiui/resourcebrowser/model/PathEntry;->getOnlinePath()Ljava/lang/String;

    move-result-object v2

    .line 433
    .end local v1           #entry:Lmiui/resourcebrowser/model/PathEntry;
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private getOnlineThumbnailUrl(Lmiui/resourcebrowser/model/Resource;)Ljava/lang/String;
    .locals 3
    .parameter "resource"

    .prologue
    .line 415
    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getThumbnails()Ljava/util/List;

    move-result-object v0

    .line 416
    .local v0, entries:Ljava/util/List;,"Ljava/util/List<Lmiui/resourcebrowser/model/PathEntry;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 417
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/resourcebrowser/model/PathEntry;

    .line 418
    .local v1, entry:Lmiui/resourcebrowser/model/PathEntry;
    if-eqz v1, :cond_0

    .line 419
    invoke-virtual {v1}, Lmiui/resourcebrowser/model/PathEntry;->getOnlinePath()Ljava/lang/String;

    move-result-object v2

    .line 422
    .end local v1           #entry:Lmiui/resourcebrowser/model/PathEntry;
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private getSliderMoveListener()Lcom/android/thememanager/view/HorzontalSliderView$SliderMoveListener;
    .locals 1

    .prologue
    .line 605
    new-instance v0, Lcom/android/thememanager/activity/WallpaperDetailActivity$11;

    invoke-direct {v0, p0}, Lcom/android/thememanager/activity/WallpaperDetailActivity$11;-><init>(Lcom/android/thememanager/activity/WallpaperDetailActivity;)V

    return-object v0
.end method

.method private getWallpaperSwitchListener()Lcom/android/thememanager/view/WallpaperView$WallpaperSwitchListener;
    .locals 1

    .prologue
    .line 552
    new-instance v0, Lcom/android/thememanager/activity/WallpaperDetailActivity$9;

    invoke-direct {v0, p0}, Lcom/android/thememanager/activity/WallpaperDetailActivity$9;-><init>(Lcom/android/thememanager/activity/WallpaperDetailActivity;)V

    return-object v0
.end method

.method private initWallpaperViewBitmap()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 460
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mImageAsyncDecoder:Lmiui/resourcebrowser/util/ImageCacheDecoder;

    if-nez v0, :cond_0

    .line 468
    :goto_0
    return-void

    .line 463
    :cond_0
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mImageAsyncDecoder:Lmiui/resourcebrowser/util/ImageCacheDecoder;

    iget v1, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mResourceIndex:I

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/util/ImageCacheDecoder;->setCurrentUseBitmapIndex(I)V

    .line 464
    const/4 v0, 0x0

    invoke-direct {p0, v0, v2}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->initWallpaperViewBitmap(IZ)V

    .line 465
    invoke-direct {p0, v2, v2}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->initWallpaperViewBitmap(IZ)V

    .line 466
    const/4 v0, -0x1

    invoke-direct {p0, v0, v2}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->initWallpaperViewBitmap(IZ)V

    .line 467
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mWallpaperView:Lcom/android/thememanager/view/WallpaperView;

    invoke-virtual {v0}, Lcom/android/thememanager/view/WallpaperView;->invalidate()V

    goto :goto_0
.end method

.method private initWallpaperViewBitmap(IZ)V
    .locals 8
    .parameter "offset"
    .parameter "async"

    .prologue
    const/4 v1, 0x0

    .line 437
    invoke-direct {p0, p1, p2}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->cacheWallpaperResource(IZ)Landroid/util/Pair;

    move-result-object v6

    .line 438
    .local v6, ret:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Boolean;>;"
    if-eqz v6, :cond_2

    iget-object v0, v6, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    .line 439
    .local v5, useThumbnail:Z
    :goto_0
    iget-object v7, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mImageAsyncDecoder:Lmiui/resourcebrowser/util/ImageCacheDecoder;

    if-eqz v6, :cond_3

    iget-object v0, v6, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    :goto_1
    invoke-virtual {v7, v0}, Lmiui/resourcebrowser/util/ImageCacheDecoder;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 440
    .local v2, b:Landroid/graphics/Bitmap;
    iget v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mResourceIndex:I

    add-int v3, v0, p1

    .line 451
    .local v3, resIndex:I
    if-nez v2, :cond_0

    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mWallpaperView:Lcom/android/thememanager/view/WallpaperView;

    invoke-virtual {v0, p1}, Lcom/android/thememanager/view/WallpaperView;->getUserGivenId(I)I

    move-result v0

    if-ne v0, v3, :cond_0

    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mWallpaperView:Lcom/android/thememanager/view/WallpaperView;

    invoke-virtual {v0, p1}, Lcom/android/thememanager/view/WallpaperView;->showingDeterminateFg(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 454
    :cond_0
    if-ltz v3, :cond_4

    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mDataSet:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_4

    const/4 v4, 0x1

    .line 455
    .local v4, validResource:Z
    :goto_2
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mWallpaperView:Lcom/android/thememanager/view/WallpaperView;

    move v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/android/thememanager/view/WallpaperView;->setBitmapInfo(ILandroid/graphics/Bitmap;IZZ)V

    .line 457
    .end local v4           #validResource:Z
    :cond_1
    return-void

    .end local v2           #b:Landroid/graphics/Bitmap;
    .end local v3           #resIndex:I
    .end local v5           #useThumbnail:Z
    :cond_2
    move v5, v1

    .line 438
    goto :goto_0

    .line 439
    .restart local v5       #useThumbnail:Z
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .restart local v2       #b:Landroid/graphics/Bitmap;
    .restart local v3       #resIndex:I
    :cond_4
    move v4, v1

    .line 454
    goto :goto_2
.end method

.method private isVisiableImagePath(Ljava/lang/String;)Z
    .locals 3
    .parameter "localPath"

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 487
    invoke-direct {p0, v0}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->getAdjResource(I)Lmiui/resourcebrowser/model/Resource;

    move-result-object v2

    invoke-direct {p0, v2, p1}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->pointSameImage(Lmiui/resourcebrowser/model/Resource;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-direct {p0, v1}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->getAdjResource(I)Lmiui/resourcebrowser/model/Resource;

    move-result-object v2

    invoke-direct {p0, v2, p1}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->pointSameImage(Lmiui/resourcebrowser/model/Resource;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, -0x1

    invoke-direct {p0, v2}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->getAdjResource(I)Lmiui/resourcebrowser/model/Resource;

    move-result-object v2

    invoke-direct {p0, v2, p1}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->pointSameImage(Lmiui/resourcebrowser/model/Resource;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0
.end method

.method private pointSameImage(Lmiui/resourcebrowser/model/Resource;Ljava/lang/String;)Z
    .locals 3
    .parameter "res"
    .parameter "decodedImagePath"

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 471
    if-nez p1, :cond_1

    .line 483
    :cond_0
    :goto_0
    return v0

    .line 474
    :cond_1
    invoke-virtual {p1}, Lmiui/resourcebrowser/model/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    .line 475
    goto :goto_0

    .line 477
    :cond_2
    invoke-direct {p0, p1}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->getLocalPreviewPath(Lmiui/resourcebrowser/model/Resource;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    move v0, v1

    .line 478
    goto :goto_0

    .line 480
    :cond_3
    invoke-direct {p0, p1}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->getLocalThumbnailPath(Lmiui/resourcebrowser/model/Resource;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 481
    goto :goto_0
.end method

.method private quitPreviewMode(Z)V
    .locals 9
    .parameter "hasQuitAnim"

    .prologue
    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 639
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mPreviewMaskView:Landroid/widget/ImageView;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 640
    if-eqz p1, :cond_0

    .line 641
    iget-object v7, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mPreviewMaskView:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mPreviewMaskView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getWidth()I

    move-result v3

    const-wide/16 v5, 0xc8

    move-object v0, p0

    move v2, v1

    move v4, v1

    invoke-direct/range {v0 .. v6}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->getAnimation(ZZIIJ)Landroid/view/animation/Animation;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 643
    :cond_0
    invoke-direct {p0, v8}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->updateTitleAndOperateBarState(Z)V

    .line 644
    iget-boolean v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mThumbnailModeOfWallpaperBeforePreview:Z

    if-eqz v0, :cond_1

    .line 645
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mWallpaperView:Lcom/android/thememanager/view/WallpaperView;

    iget-boolean v1, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mThumbnailModeOfWallpaperBeforePreview:Z

    invoke-virtual {v0, v1}, Lcom/android/thememanager/view/WallpaperView;->setScanMode(Z)V

    .line 649
    :goto_0
    return-void

    .line 647
    :cond_1
    invoke-direct {p0, v8}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->updateSliderViewState(Z)V

    goto :goto_0
.end method

.method private saveUserScanMode()V
    .locals 3

    .prologue
    .line 343
    const-string v1, "wallpaper_scan_config"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 344
    .local v0, edit:Landroid/content/SharedPreferences$Editor;
    const-string v1, "scan_mode_thumbnail"

    iget-object v2, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mWallpaperView:Lcom/android/thememanager/view/WallpaperView;

    invoke-virtual {v2}, Lcom/android/thememanager/view/WallpaperView;->isThumbnailScanMode()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 345
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 346
    return-void
.end method

.method private updateSliderViewState(Z)V
    .locals 4
    .parameter "visiable"

    .prologue
    const/4 v0, 0x4

    const/4 v1, 0x0

    .line 659
    iget-boolean v2, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mIsLockscreen:Z

    if-eqz v2, :cond_1

    .line 660
    iget-object v1, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mSliderView:Lcom/android/thememanager/view/HorzontalSliderView;

    invoke-virtual {v1, v0}, Lcom/android/thememanager/view/HorzontalSliderView;->setVisibility(I)V

    .line 670
    :cond_0
    :goto_0
    return-void

    .line 664
    :cond_1
    if-eqz p1, :cond_2

    move v0, v1

    .line 665
    .local v0, finalVisibility:I
    :cond_2
    iget-object v2, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mSliderView:Lcom/android/thememanager/view/HorzontalSliderView;

    invoke-virtual {v2}, Lcom/android/thememanager/view/HorzontalSliderView;->getVisibility()I

    move-result v2

    if-eq v2, v0, :cond_0

    .line 668
    iget-object v2, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mSliderView:Lcom/android/thememanager/view/HorzontalSliderView;

    const/4 v3, 0x1

    invoke-direct {p0, v3, p1, v1, v1}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->getAnimation(ZZII)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/android/thememanager/view/HorzontalSliderView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 669
    iget-object v1, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mSliderView:Lcom/android/thememanager/view/HorzontalSliderView;

    invoke-virtual {v1, v0}, Lcom/android/thememanager/view/HorzontalSliderView;->setVisibility(I)V

    goto :goto_0
.end method

.method private updateTitleAndOperateBarState(Z)V
    .locals 4
    .parameter "visiable"

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 673
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mTitleAreaView:Landroid/view/View;

    iget-object v3, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mTitleAreaView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    neg-int v3, v3

    invoke-direct {p0, v1, p1, v1, v3}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->getAnimation(ZZII)Landroid/view/animation/Animation;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 674
    iget-object v3, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mTitleAreaView:Landroid/view/View;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 675
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mOperateBarView:Landroid/view/View;

    iget-object v3, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mOperateBarView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-direct {p0, v1, p1, v1, v3}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->getAnimation(ZZII)Landroid/view/animation/Animation;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 676
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mOperateBarView:Landroid/view/View;

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 677
    return-void

    :cond_0
    move v0, v2

    .line 674
    goto :goto_0

    :cond_1
    move v1, v2

    .line 676
    goto :goto_1
.end method

.method private userUseThumbnailScanModeLastTime()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 338
    const-string v0, "wallpaper_scan_config"

    invoke-virtual {p0, v0, v2}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "scan_mode_thumbnail"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected bindScreenView()V
    .locals 0

    .prologue
    .line 531
    invoke-direct {p0}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->initWallpaperViewBitmap()V

    .line 532
    return-void
.end method

.method protected changeCurrentResource()V
    .locals 2

    .prologue
    .line 518
    iget v1, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mResourceIndex:I

    invoke-virtual {p0, v1}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->requestResourceDetail(I)V

    .line 519
    iget v1, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mResourceIndex:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v1}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->requestResourceDetail(I)V

    .line 520
    iget v1, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mResourceIndex:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->requestResourceDetail(I)V

    .line 521
    invoke-super {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->changeCurrentResource()V

    .line 523
    const v1, 0x7f07000a

    invoke-virtual {p0, v1}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 524
    .local v0, tv:Landroid/widget/TextView;
    if-eqz v0, :cond_0

    .line 525
    iget-object v1, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v1}, Lmiui/resourcebrowser/model/Resource;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 527
    :cond_0
    return-void
.end method

.method protected getContentView()I
    .locals 1

    .prologue
    .line 92
    const v0, 0x7f03000a

    return v0
.end method

.method protected initParams()V
    .locals 4

    .prologue
    .line 97
    invoke-super {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->initParams()V

    .line 98
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    const-string v1, "EXTRA_CTX_RESOURCE_TYPE"

    const-wide/16 v2, -0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmiui/resourcebrowser/ResourceContext;->getExtraMeta(Ljava/lang/String;Ljava/io/Serializable;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mResourceType:J

    .line 99
    iget-wide v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mResourceType:J

    const-wide/16 v2, 0x4

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mIsLockscreen:Z

    .line 100
    return-void

    .line 99
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 350
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mPreviewMaskView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 351
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->quitPreviewMode(Z)V

    .line 355
    :goto_0
    return-void

    .line 353
    :cond_0
    invoke-super {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 330
    invoke-super {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->onDestroy()V

    .line 331
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mImageAsyncDecoder:Lmiui/resourcebrowser/util/ImageCacheDecoder;

    if-eqz v0, :cond_0

    .line 332
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mImageAsyncDecoder:Lmiui/resourcebrowser/util/ImageCacheDecoder;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/util/ImageCacheDecoder;->clean(Z)V

    .line 334
    :cond_0
    invoke-direct {p0}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->saveUserScanMode()V

    .line 335
    return-void
.end method

.method protected onStart()V
    .locals 0

    .prologue
    .line 315
    invoke-super {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->onStart()V

    .line 316
    invoke-direct {p0}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->initWallpaperViewBitmap()V

    .line 317
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 321
    invoke-super {p0}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->onStop()V

    .line 322
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mImageAsyncDecoder:Lmiui/resourcebrowser/util/ImageCacheDecoder;

    if-eqz v0, :cond_0

    .line 323
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mImageAsyncDecoder:Lmiui/resourcebrowser/util/ImageCacheDecoder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmiui/resourcebrowser/util/ImageCacheDecoder;->clean(Z)V

    .line 324
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mWallpaperView:Lcom/android/thememanager/view/WallpaperView;

    invoke-virtual {v0}, Lcom/android/thememanager/view/WallpaperView;->reset()V

    .line 326
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .parameter "event"

    .prologue
    .line 615
    iget-object v1, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mWallpaperView:Lcom/android/thememanager/view/WallpaperView;

    invoke-virtual {v1}, Lcom/android/thememanager/view/WallpaperView;->hasBeenInitied()Z

    move-result v1

    if-nez v1, :cond_1

    .line 616
    const/4 v0, 0x0

    .line 623
    :cond_0
    :goto_0
    return v0

    .line 619
    :cond_1
    iget-object v1, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v1, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 620
    .local v0, ret:Z
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 621
    iget-object v1, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mWallpaperView:Lcom/android/thememanager/view/WallpaperView;

    invoke-virtual {v1}, Lcom/android/thememanager/view/WallpaperView;->autoSwitchCurreentWallpaper()V

    goto :goto_0
.end method

.method protected requestResourceDetail(I)V
    .locals 2
    .parameter "index"

    .prologue
    .line 502
    if-ltz p1, :cond_0

    iget-object v1, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mGroupDataSet:Lmiui/resourcebrowser/widget/DataGroup;

    invoke-virtual {v1}, Lmiui/resourcebrowser/widget/DataGroup;->size()I

    move-result v1

    if-lt p1, v1, :cond_1

    .line 509
    :cond_0
    :goto_0
    return-void

    .line 505
    :cond_1
    iget-object v1, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mGroupDataSet:Lmiui/resourcebrowser/widget/DataGroup;

    invoke-virtual {v1, p1}, Lmiui/resourcebrowser/widget/DataGroup;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/resourcebrowser/model/Resource;

    .line 506
    .local v0, res:Lmiui/resourcebrowser/model/Resource;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmiui/resourcebrowser/model/Resource;->getPreviews()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lmiui/resourcebrowser/model/Resource;->getPreviews()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 507
    :cond_2
    invoke-super {p0, p1}, Lmiui/resourcebrowser/activity/ResourceDetailActivity;->requestResourceDetail(I)V

    goto :goto_0
.end method

.method protected setResourceStatus()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 536
    iget-object v1, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mDownloadBtn:Landroid/widget/TextView;

    if-nez v1, :cond_0

    .line 549
    :goto_0
    return-void

    .line 539
    :cond_0
    const/4 v0, 0x0

    .line 540
    .local v0, textId:I
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v2}, Lmiui/resourcebrowser/model/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 541
    const v0, 0x60c0019

    .line 542
    iget-object v1, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mMoreMenuBtn:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 547
    :goto_1
    iget-object v1, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mDownloadBtn:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 548
    iget-object v1, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mDownloadBtn:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0

    .line 544
    :cond_1
    const v0, 0x60c0017

    .line 545
    iget-object v1, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mMoreMenuBtn:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto :goto_1
.end method

.method protected setupNavigationButton()V
    .locals 1

    .prologue
    .line 306
    const v0, 0x7f070019

    invoke-virtual {p0, v0}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mPreviousItem:Landroid/view/View;

    .line 307
    const v0, 0x7f07001a

    invoke-virtual {p0, v0}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mNextItem:Landroid/view/View;

    .line 308
    iget-object v0, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mPreviousItem:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 309
    invoke-virtual {p0}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->initNavigationState()V

    .line 311
    :cond_0
    return-void
.end method

.method protected setupUI()V
    .locals 6

    .prologue
    const/4 v3, 0x4

    .line 104
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 105
    .local v0, screenSize:Landroid/graphics/Point;
    invoke-virtual {p0}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 106
    iget-boolean v2, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mIsLockscreen:Z

    if-eqz v2, :cond_1

    const/4 v1, 0x1

    .line 107
    .local v1, widthFactor:I
    :goto_0
    iget v2, v0, Landroid/graphics/Point;->x:I

    mul-int/2addr v2, v1

    iput v2, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mWallpaperWidth:I

    .line 108
    iget v2, v0, Landroid/graphics/Point;->y:I

    iput v2, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mWallpaperHeight:I

    .line 110
    const v2, 0x7f070015

    invoke-virtual {p0, v2}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/thememanager/view/WallpaperView;

    iput-object v2, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mWallpaperView:Lcom/android/thememanager/view/WallpaperView;

    .line 111
    iget-object v2, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mWallpaperView:Lcom/android/thememanager/view/WallpaperView;

    invoke-direct {p0}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->getWallpaperSwitchListener()Lcom/android/thememanager/view/WallpaperView$WallpaperSwitchListener;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/android/thememanager/view/WallpaperView;->regeisterSwitchListener(Lcom/android/thememanager/view/WallpaperView$WallpaperSwitchListener;)V

    .line 112
    iget-object v2, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mWallpaperView:Lcom/android/thememanager/view/WallpaperView;

    iget v4, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mWallpaperWidth:I

    iget v5, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mWallpaperHeight:I

    invoke-virtual {v2, v4, v5}, Lcom/android/thememanager/view/WallpaperView;->setContainingBitmapSize(II)V

    .line 113
    iget-object v2, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mWallpaperView:Lcom/android/thememanager/view/WallpaperView;

    invoke-direct {p0}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->userUseThumbnailScanModeLastTime()Z

    move-result v4

    invoke-virtual {v2, v4}, Lcom/android/thememanager/view/WallpaperView;->initScanMode(Z)V

    .line 115
    const v2, 0x7f07001c

    invoke-virtual {p0, v2}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/thememanager/view/HorzontalSliderView;

    iput-object v2, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mSliderView:Lcom/android/thememanager/view/HorzontalSliderView;

    .line 116
    iget-object v2, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mSliderView:Lcom/android/thememanager/view/HorzontalSliderView;

    invoke-direct {p0}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->getSliderMoveListener()Lcom/android/thememanager/view/HorzontalSliderView$SliderMoveListener;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/android/thememanager/view/HorzontalSliderView;->regeisterMoveListener(Lcom/android/thememanager/view/HorzontalSliderView$SliderMoveListener;)V

    .line 117
    iget-object v4, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mSliderView:Lcom/android/thememanager/view/HorzontalSliderView;

    iget-boolean v2, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mIsLockscreen:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mWallpaperView:Lcom/android/thememanager/view/WallpaperView;

    invoke-virtual {v2}, Lcom/android/thememanager/view/WallpaperView;->isThumbnailScanMode()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    move v2, v3

    :goto_1
    invoke-virtual {v4, v2}, Lcom/android/thememanager/view/HorzontalSliderView;->setVisibility(I)V

    .line 120
    new-instance v2, Landroid/view/GestureDetector;

    new-instance v4, Lcom/android/thememanager/activity/WallpaperDetailActivity$WallpaperGestureListener;

    invoke-direct {v4, p0}, Lcom/android/thememanager/activity/WallpaperDetailActivity$WallpaperGestureListener;-><init>(Lcom/android/thememanager/activity/WallpaperDetailActivity;)V

    invoke-direct {v2, p0, v4}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v2, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mGestureDetector:Landroid/view/GestureDetector;

    .line 122
    new-instance v2, Lcom/android/thememanager/util/WallpaperDecoder;

    const/4 v4, 0x3

    invoke-direct {v2, v4}, Lcom/android/thememanager/util/WallpaperDecoder;-><init>(I)V

    iput-object v2, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mImageAsyncDecoder:Lmiui/resourcebrowser/util/ImageCacheDecoder;

    .line 123
    iget-object v2, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mImageAsyncDecoder:Lmiui/resourcebrowser/util/ImageCacheDecoder;

    iget v4, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mWallpaperWidth:I

    iget v5, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mWallpaperHeight:I

    invoke-virtual {v2, v4, v5}, Lmiui/resourcebrowser/util/ImageCacheDecoder;->setScaledSize(II)V

    .line 124
    iget-object v2, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mImageAsyncDecoder:Lmiui/resourcebrowser/util/ImageCacheDecoder;

    invoke-direct {p0}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->getImageDecodingListener()Lmiui/resourcebrowser/util/ImageCacheDecoder$ImageDecodingListener;

    move-result-object v4

    invoke-virtual {v2, v4}, Lmiui/resourcebrowser/util/ImageCacheDecoder;->registerListener(Lmiui/resourcebrowser/util/ImageCacheDecoder$ImageDecodingListener;)V

    .line 126
    iget-object v2, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    iget v4, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mWallpaperWidth:I

    invoke-virtual {v2, v4}, Lmiui/resourcebrowser/ResourceContext;->setPreviewImageWidth(I)V

    .line 128
    invoke-virtual {p0}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->setupNavigationButton()V

    .line 129
    invoke-virtual {p0}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->changeCurrentResource()V

    .line 131
    const v2, 0x7f070017

    invoke-virtual {p0, v2}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mTitleAreaView:Landroid/view/View;

    .line 132
    iget-object v2, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mTitleAreaView:Landroid/view/View;

    new-instance v4, Lcom/android/thememanager/activity/WallpaperDetailActivity$1;

    invoke-direct {v4, p0}, Lcom/android/thememanager/activity/WallpaperDetailActivity$1;-><init>(Lcom/android/thememanager/activity/WallpaperDetailActivity;)V

    invoke-virtual {v2, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 138
    const v2, 0x7f07001d

    invoke-virtual {p0, v2}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mOperateBarView:Landroid/view/View;

    .line 139
    iget-object v2, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mOperateBarView:Landroid/view/View;

    new-instance v4, Lcom/android/thememanager/activity/WallpaperDetailActivity$2;

    invoke-direct {v4, p0}, Lcom/android/thememanager/activity/WallpaperDetailActivity$2;-><init>(Lcom/android/thememanager/activity/WallpaperDetailActivity;)V

    invoke-virtual {v2, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 145
    const v2, 0x7f070016

    invoke-virtual {p0, v2}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mPreviewMaskView:Landroid/widget/ImageView;

    .line 146
    iget-object v2, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mPreviewMaskView:Landroid/widget/ImageView;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 147
    iget-object v3, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mPreviewMaskView:Landroid/widget/ImageView;

    iget-boolean v2, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mIsLockscreen:Z

    if-eqz v2, :cond_3

    const v2, 0x7f02001a

    :goto_2
    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 149
    iget-object v2, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mPreviewMaskView:Landroid/widget/ImageView;

    sget-object v3, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 150
    iget-object v2, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mPreviewMaskView:Landroid/widget/ImageView;

    new-instance v3, Lcom/android/thememanager/activity/WallpaperDetailActivity$3;

    invoke-direct {v3, p0}, Lcom/android/thememanager/activity/WallpaperDetailActivity$3;-><init>(Lcom/android/thememanager/activity/WallpaperDetailActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 160
    const v2, 0x7f070018

    invoke-virtual {p0, v2}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v3, Lcom/android/thememanager/activity/WallpaperDetailActivity$4;

    invoke-direct {v3, p0}, Lcom/android/thememanager/activity/WallpaperDetailActivity$4;-><init>(Lcom/android/thememanager/activity/WallpaperDetailActivity;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 167
    const v2, 0x7f07001f

    invoke-virtual {p0, v2}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v3, Lcom/android/thememanager/activity/WallpaperDetailActivity$5;

    invoke-direct {v3, p0}, Lcom/android/thememanager/activity/WallpaperDetailActivity$5;-><init>(Lcom/android/thememanager/activity/WallpaperDetailActivity;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 174
    const v2, 0x7f070020

    invoke-virtual {p0, v2}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mDownloadBtn:Landroid/widget/TextView;

    .line 175
    iget-object v2, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mDownloadBtn:Landroid/widget/TextView;

    new-instance v3, Lcom/android/thememanager/activity/WallpaperDetailActivity$6;

    invoke-direct {v3, p0}, Lcom/android/thememanager/activity/WallpaperDetailActivity$6;-><init>(Lcom/android/thememanager/activity/WallpaperDetailActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 210
    const v2, 0x7f07001e

    invoke-virtual {p0, v2}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v3, Lcom/android/thememanager/activity/WallpaperDetailActivity$7;

    invoke-direct {v3, p0}, Lcom/android/thememanager/activity/WallpaperDetailActivity$7;-><init>(Lcom/android/thememanager/activity/WallpaperDetailActivity;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 241
    const v2, 0x7f070021

    invoke-virtual {p0, v2}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mMoreMenuBtn:Landroid/widget/ImageView;

    .line 242
    iget-object v2, p0, Lcom/android/thememanager/activity/WallpaperDetailActivity;->mMoreMenuBtn:Landroid/widget/ImageView;

    new-instance v3, Lcom/android/thememanager/activity/WallpaperDetailActivity$8;

    invoke-direct {v3, p0}, Lcom/android/thememanager/activity/WallpaperDetailActivity$8;-><init>(Lcom/android/thememanager/activity/WallpaperDetailActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 274
    invoke-virtual {p0}, Lcom/android/thememanager/activity/WallpaperDetailActivity;->setResourceStatus()V

    .line 275
    return-void

    .line 106
    .end local v1           #widthFactor:I
    :cond_1
    const/4 v1, 0x2

    goto/16 :goto_0

    .line 117
    .restart local v1       #widthFactor:I
    :cond_2
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 147
    :cond_3
    const v2, 0x7f020016

    goto :goto_2
.end method
