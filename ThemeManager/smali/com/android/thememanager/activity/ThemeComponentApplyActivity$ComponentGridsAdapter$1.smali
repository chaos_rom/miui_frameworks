.class Lcom/android/thememanager/activity/ThemeComponentApplyActivity$ComponentGridsAdapter$1;
.super Ljava/lang/Object;
.source "ThemeComponentApplyActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/thememanager/activity/ThemeComponentApplyActivity$ComponentGridsAdapter;->getChildView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/thememanager/activity/ThemeComponentApplyActivity$ComponentGridsAdapter;


# direct methods
.method constructor <init>(Lcom/android/thememanager/activity/ThemeComponentApplyActivity$ComponentGridsAdapter;)V
    .locals 0
    .parameter

    .prologue
    .line 237
    iput-object p1, p0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity$ComponentGridsAdapter$1;->this$1:Lcom/android/thememanager/activity/ThemeComponentApplyActivity$ComponentGridsAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .parameter "v"

    .prologue
    .line 239
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 240
    .local v1, pos:I
    iget-object v2, p0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity$ComponentGridsAdapter$1;->this$1:Lcom/android/thememanager/activity/ThemeComponentApplyActivity$ComponentGridsAdapter;

    #getter for: Lcom/android/thememanager/activity/ThemeComponentApplyActivity$ComponentGridsAdapter;->mOrderComponentFlag:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/android/thememanager/activity/ThemeComponentApplyActivity$ComponentGridsAdapter;->access$000(Lcom/android/thememanager/activity/ThemeComponentApplyActivity$ComponentGridsAdapter;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 241
    const v2, 0x7f07000c

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 242
    .local v0, check:Landroid/widget/ImageView;
    invoke-virtual {v0}, Landroid/widget/ImageView;->isSelected()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 243
    iget-object v3, p0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity$ComponentGridsAdapter$1;->this$1:Lcom/android/thememanager/activity/ThemeComponentApplyActivity$ComponentGridsAdapter;

    iget-object v2, p0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity$ComponentGridsAdapter$1;->this$1:Lcom/android/thememanager/activity/ThemeComponentApplyActivity$ComponentGridsAdapter;

    #getter for: Lcom/android/thememanager/activity/ThemeComponentApplyActivity$ComponentGridsAdapter;->mOrderComponentFlag:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/android/thememanager/activity/ThemeComponentApplyActivity$ComponentGridsAdapter;->access$000(Lcom/android/thememanager/activity/ThemeComponentApplyActivity$ComponentGridsAdapter;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, -0x1

    xor-long/2addr v4, v6

    invoke-static {v3, v4, v5}, Lcom/android/thememanager/activity/ThemeComponentApplyActivity$ComponentGridsAdapter;->access$174(Lcom/android/thememanager/activity/ThemeComponentApplyActivity$ComponentGridsAdapter;J)J

    .line 247
    :goto_0
    iget-object v2, p0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity$ComponentGridsAdapter$1;->this$1:Lcom/android/thememanager/activity/ThemeComponentApplyActivity$ComponentGridsAdapter;

    invoke-virtual {v2}, Lcom/android/thememanager/activity/ThemeComponentApplyActivity$ComponentGridsAdapter;->notifyDataSetInvalidated()V

    .line 248
    iget-object v2, p0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity$ComponentGridsAdapter$1;->this$1:Lcom/android/thememanager/activity/ThemeComponentApplyActivity$ComponentGridsAdapter;

    iget-object v2, v2, Lcom/android/thememanager/activity/ThemeComponentApplyActivity$ComponentGridsAdapter;->this$0:Lcom/android/thememanager/activity/ThemeComponentApplyActivity;

    #calls: Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->updateComponentNumber()V
    invoke-static {v2}, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->access$200(Lcom/android/thememanager/activity/ThemeComponentApplyActivity;)V

    .line 250
    .end local v0           #check:Landroid/widget/ImageView;
    :cond_0
    return-void

    .line 245
    .restart local v0       #check:Landroid/widget/ImageView;
    :cond_1
    iget-object v3, p0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity$ComponentGridsAdapter$1;->this$1:Lcom/android/thememanager/activity/ThemeComponentApplyActivity$ComponentGridsAdapter;

    iget-object v2, p0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity$ComponentGridsAdapter$1;->this$1:Lcom/android/thememanager/activity/ThemeComponentApplyActivity$ComponentGridsAdapter;

    #getter for: Lcom/android/thememanager/activity/ThemeComponentApplyActivity$ComponentGridsAdapter;->mOrderComponentFlag:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/android/thememanager/activity/ThemeComponentApplyActivity$ComponentGridsAdapter;->access$000(Lcom/android/thememanager/activity/ThemeComponentApplyActivity$ComponentGridsAdapter;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/android/thememanager/activity/ThemeComponentApplyActivity$ComponentGridsAdapter;->access$178(Lcom/android/thememanager/activity/ThemeComponentApplyActivity$ComponentGridsAdapter;J)J

    goto :goto_0
.end method
