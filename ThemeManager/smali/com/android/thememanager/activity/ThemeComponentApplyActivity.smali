.class public Lcom/android/thememanager/activity/ThemeComponentApplyActivity;
.super Lmiui/app/ObservableActivity;
.source "ThemeComponentApplyActivity.java"

# interfaces
.implements Lcom/android/thememanager/ThemeResourceConstants;
.implements Lmiui/app/SDCardMonitor$SDCardStatusListener;
.implements Lmiui/resourcebrowser/util/ResourceDownloadHandler$ResourceDownloadListener;
.implements Lmiui/resourcebrowser/view/ResourceOperationView$ResourceOperationListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/thememanager/activity/ThemeComponentApplyActivity$ComponentGridsAdapter;
    }
.end annotation


# instance fields
.field private mComponentGridView:Lcom/android/thememanager/view/FixedHeightGridView;

.field private mComponentNumText:Landroid/widget/TextView;

.field private mOperationHandler:Lmiui/resourcebrowser/view/ResourceOperationHandler;

.field private mResContext:Lmiui/resourcebrowser/ResourceContext;

.field private mResource:Lmiui/resourcebrowser/model/Resource;

.field private mResourceType:J

.field private mSDCardMonitor:Lmiui/app/SDCardMonitor;

.field private mSourceType:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lmiui/app/ObservableActivity;-><init>()V

    .line 178
    return-void
.end method

.method static synthetic access$200(Lcom/android/thememanager/activity/ThemeComponentApplyActivity;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->updateComponentNumber()V

    return-void
.end method

.method private getGridComponentShowingFlags()J
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    .line 124
    iget-object v4, p0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v4}, Lmiui/resourcebrowser/model/Resource;->getPlatform()I

    move-result v2

    .line 125
    .local v2, platformVersion:I
    iget-object v4, p0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    const-string v5, "modulesFlag"

    invoke-virtual {v4, v5}, Lmiui/resourcebrowser/model/Resource;->getExtraMeta(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 126
    .local v3, str:Ljava/lang/String;
    if-eqz v3, :cond_4

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 127
    .local v0, detailFlags:J
    :goto_0
    cmp-long v4, v0, v6

    if-nez v4, :cond_0

    .line 128
    invoke-static {}, Lcom/android/thememanager/util/ThemeHelper;->getAllComponentsCombineFlag()J

    move-result-wide v0

    .line 131
    :cond_0
    iget-wide v4, p0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->mResourceType:J

    cmp-long v4, v4, v6

    if-nez v4, :cond_1

    .line 132
    const-wide/32 v4, 0x10000000

    or-long/2addr v0, v4

    .line 135
    :cond_1
    invoke-static {}, Lcom/android/thememanager/util/ThemeHelper;->supportReplaceFont()Z

    move-result v4

    if-nez v4, :cond_2

    .line 136
    const-wide/32 v4, -0x40011

    and-long/2addr v0, v4

    .line 138
    :cond_2
    invoke-static {}, Lcom/android/thememanager/util/ThemeHelper;->supportReplaceAudioEffect()Z

    move-result v4

    if-nez v4, :cond_3

    .line 139
    const-wide/32 v4, -0x8001

    and-long/2addr v0, v4

    .line 142
    :cond_3
    invoke-static {v2, v0, v1}, Lcom/android/thememanager/util/ThemeHelper;->getCompatibleFlag(IJ)J

    move-result-wide v0

    .line 144
    return-wide v0

    .line 126
    .end local v0           #detailFlags:J
    :cond_4
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method private getOperationHandler(Lmiui/resourcebrowser/view/ResourceOperationView;)Lcom/android/thememanager/util/ThemeOperationHandler;
    .locals 2
    .parameter "view"

    .prologue
    .line 108
    new-instance v0, Lcom/android/thememanager/util/ThemeOperationHandler;

    iget-object v1, p0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    invoke-direct {v0, p0, v1, p1}, Lcom/android/thememanager/util/ThemeOperationHandler;-><init>(Landroid/content/Context;Lmiui/resourcebrowser/ResourceContext;Lmiui/resourcebrowser/view/ResourceOperationView;)V

    return-object v0
.end method

.method private getSelectComponentFlags()J
    .locals 5

    .prologue
    const-wide/16 v1, -0x1

    .line 171
    iget-object v3, p0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->mComponentGridView:Lcom/android/thememanager/view/FixedHeightGridView;

    invoke-virtual {v3}, Lcom/android/thememanager/view/FixedHeightGridView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity$ComponentGridsAdapter;

    .line 172
    .local v0, adapter:Lcom/android/thememanager/activity/ThemeComponentApplyActivity$ComponentGridsAdapter;
    iget-wide v3, p0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->mResourceType:J

    cmp-long v3, v3, v1

    if-nez v3, :cond_0

    invoke-virtual {v0}, Lcom/android/thememanager/activity/ThemeComponentApplyActivity$ComponentGridsAdapter;->isSelectAllComponent()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 175
    :goto_0
    return-wide v1

    :cond_0
    invoke-virtual {v0}, Lcom/android/thememanager/activity/ThemeComponentApplyActivity$ComponentGridsAdapter;->getSelectComponentFlag()J

    move-result-wide v1

    goto :goto_0
.end method

.method private getThemeApplyParameters()Lcom/android/thememanager/util/ThemeApplyParameters;
    .locals 3

    .prologue
    .line 312
    new-instance v0, Lcom/android/thememanager/util/ThemeApplyParameters;

    invoke-direct {v0}, Lcom/android/thememanager/util/ThemeApplyParameters;-><init>()V

    .line 313
    .local v0, params:Lcom/android/thememanager/util/ThemeApplyParameters;
    invoke-direct {p0}, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->getSelectComponentFlags()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/android/thememanager/util/ThemeApplyParameters;->applyFlags:J

    .line 314
    invoke-direct {p0}, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->needRemoveAllOldTheme()Z

    move-result v1

    iput-boolean v1, v0, Lcom/android/thememanager/util/ThemeApplyParameters;->removeOthers:Z

    .line 315
    return-object v0
.end method

.method private needRemoveAllOldTheme()Z
    .locals 4

    .prologue
    .line 164
    iget-object v0, p0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->mComponentGridView:Lcom/android/thememanager/view/FixedHeightGridView;

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->mResourceType:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 165
    :cond_0
    const/4 v0, 0x0

    .line 167
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->mComponentGridView:Lcom/android/thememanager/view/FixedHeightGridView;

    invoke-virtual {v0}, Lcom/android/thememanager/view/FixedHeightGridView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity$ComponentGridsAdapter;

    invoke-virtual {v0}, Lcom/android/thememanager/activity/ThemeComponentApplyActivity$ComponentGridsAdapter;->needRemoveAllOldTheme()Z

    move-result v0

    goto :goto_0
.end method

.method private setComponentGridViewClickable(Z)V
    .locals 1
    .parameter "clickable"

    .prologue
    .line 148
    iget-object v0, p0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->mComponentGridView:Lcom/android/thememanager/view/FixedHeightGridView;

    invoke-virtual {v0}, Lcom/android/thememanager/view/FixedHeightGridView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity$ComponentGridsAdapter;

    invoke-virtual {v0, p1}, Lcom/android/thememanager/activity/ThemeComponentApplyActivity$ComponentGridsAdapter;->setClickable(Z)V

    .line 149
    return-void
.end method

.method private setupComponentGridView()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 112
    const v0, 0x7f070009

    invoke-virtual {p0, v0}, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/thememanager/view/FixedHeightGridView;

    iput-object v0, p0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->mComponentGridView:Lcom/android/thememanager/view/FixedHeightGridView;

    .line 114
    iget-object v6, p0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->mComponentGridView:Lcom/android/thememanager/view/FixedHeightGridView;

    new-instance v0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity$ComponentGridsAdapter;

    invoke-direct {p0}, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->getGridComponentShowingFlags()J

    move-result-wide v2

    iget-object v1, p0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->mOperationHandler:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    invoke-virtual {v1}, Lmiui/resourcebrowser/view/ResourceOperationHandler;->isLocalResource()Z

    move-result v4

    iget-object v5, p0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->mComponentGridView:Lcom/android/thememanager/view/FixedHeightGridView;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/thememanager/activity/ThemeComponentApplyActivity$ComponentGridsAdapter;-><init>(Lcom/android/thememanager/activity/ThemeComponentApplyActivity;JZLcom/android/thememanager/view/FixedHeightGridView;)V

    invoke-virtual {v6, v0}, Lcom/android/thememanager/view/FixedHeightGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 116
    iget-object v0, p0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->mComponentGridView:Lcom/android/thememanager/view/FixedHeightGridView;

    invoke-virtual {v0, v7}, Lcom/android/thememanager/view/FixedHeightGridView;->setEnabled(Z)V

    .line 117
    iget-object v0, p0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->mComponentGridView:Lcom/android/thememanager/view/FixedHeightGridView;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, v7}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/android/thememanager/view/FixedHeightGridView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    .line 118
    iget-object v0, p0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->mComponentGridView:Lcom/android/thememanager/view/FixedHeightGridView;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/android/thememanager/view/FixedHeightGridView;->setNumColumns(I)V

    .line 119
    iget-object v0, p0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->mComponentGridView:Lcom/android/thememanager/view/FixedHeightGridView;

    invoke-virtual {v0, v7}, Lcom/android/thememanager/view/FixedHeightGridView;->setVisibility(I)V

    .line 120
    invoke-direct {p0}, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->updateComponentNumber()V

    .line 121
    return-void
.end method

.method private setupUI()V
    .locals 4

    .prologue
    .line 90
    invoke-virtual {p0}, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 91
    .local v0, actionBar:Landroid/app/ActionBar;
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 92
    iget-object v2, p0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v2}, Lmiui/resourcebrowser/model/Resource;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 95
    const v2, 0x60b0059

    invoke-virtual {p0, v2}, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lmiui/resourcebrowser/view/ResourceOperationView;

    .line 96
    .local v1, view:Lmiui/resourcebrowser/view/ResourceOperationView;
    const v2, 0x60201ac

    invoke-virtual {v1, v2}, Lmiui/resourcebrowser/view/ResourceOperationView;->setMagicButtonResource(I)V

    .line 97
    invoke-virtual {v1, p0}, Lmiui/resourcebrowser/view/ResourceOperationView;->setResourceOperationListener(Lmiui/resourcebrowser/view/ResourceOperationView$ResourceOperationListener;)V

    .line 98
    invoke-direct {p0, v1}, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->getOperationHandler(Lmiui/resourcebrowser/view/ResourceOperationView;)Lcom/android/thememanager/util/ThemeOperationHandler;

    move-result-object v2

    iput-object v2, p0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->mOperationHandler:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    .line 99
    iget-object v2, p0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->mOperationHandler:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    invoke-static {}, Lmiui/resourcebrowser/AppInnerContext;->getInstance()Lmiui/resourcebrowser/AppInnerContext;

    move-result-object v3

    invoke-virtual {v3}, Lmiui/resourcebrowser/AppInnerContext;->getResourceController()Lmiui/resourcebrowser/controller/ResourceController;

    move-result-object v3

    invoke-virtual {v2, v3}, Lmiui/resourcebrowser/view/ResourceOperationHandler;->setResourceController(Lmiui/resourcebrowser/controller/ResourceController;)V

    .line 100
    iget-object v2, p0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->mOperationHandler:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    invoke-virtual {p0, v2}, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->addObserver(Lmiui/app/ActivityLifecycleObserver;)V

    .line 101
    iget-object v2, p0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->mOperationHandler:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    iget-object v3, p0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    invoke-virtual {v2, v3}, Lmiui/resourcebrowser/view/ResourceOperationHandler;->setResource(Lmiui/resourcebrowser/model/Resource;)V

    .line 104
    invoke-direct {p0}, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->setupComponentGridView()V

    .line 105
    return-void
.end method

.method private updateComponentNumber()V
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v8, 0x0

    .line 152
    const v4, 0x7f070008

    invoke-virtual {p0, v4}, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->mComponentNumText:Landroid/widget/TextView;

    .line 153
    iget-object v4, p0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->mComponentGridView:Lcom/android/thememanager/view/FixedHeightGridView;

    invoke-virtual {v4}, Lcom/android/thememanager/view/FixedHeightGridView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity$ComponentGridsAdapter;

    .line 154
    .local v0, adapter:Lcom/android/thememanager/activity/ThemeComponentApplyActivity$ComponentGridsAdapter;
    invoke-virtual {v0}, Lcom/android/thememanager/activity/ThemeComponentApplyActivity$ComponentGridsAdapter;->getSelectComponentFlag()J

    move-result-wide v2

    .line 155
    .local v2, selectFlags:J
    invoke-static {v2, v3}, Lcom/android/thememanager/util/ThemeHelper;->getComponentNumber(J)I

    move-result v1

    .line 156
    .local v1, componentNum:I
    iget-object v4, p0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->mOperationHandler:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    invoke-virtual {v4}, Lmiui/resourcebrowser/view/ResourceOperationHandler;->isLocalResource()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 157
    iget-object v4, p0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->mComponentNumText:Landroid/widget/TextView;

    const v5, 0x7f0b0032

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {p0, v5, v6}, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 161
    :goto_0
    return-void

    .line 159
    :cond_0
    iget-object v4, p0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->mComponentNumText:Landroid/widget/TextView;

    const v5, 0x7f0b0033

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {p0, v5, v6}, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public finish()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 307
    invoke-super {p0}, Lmiui/app/ObservableActivity;->finish()V

    .line 308
    invoke-virtual {p0, v0, v0}, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->overridePendingTransition(II)V

    .line 309
    return-void
.end method

.method public onApplyEventPerformed()V
    .locals 2

    .prologue
    .line 320
    iget-object v0, p0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->mOperationHandler:Lmiui/resourcebrowser/view/ResourceOperationHandler;

    check-cast v0, Lcom/android/thememanager/util/ThemeOperationHandler;

    invoke-direct {p0}, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->getThemeApplyParameters()Lcom/android/thememanager/util/ThemeApplyParameters;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/thememanager/util/ThemeOperationHandler;->setApplyParameters(Lcom/android/thememanager/util/ThemeApplyParameters;)V

    .line 321
    return-void
.end method

.method public onBuyEventPerformed()V
    .locals 0

    .prologue
    .line 377
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .parameter "savedInstanceState"

    .prologue
    .line 59
    invoke-super {p0, p1}, Lmiui/app/ObservableActivity;->onCreate(Landroid/os/Bundle;)V

    .line 60
    const v2, 0x7f030003

    invoke-virtual {p0, v2}, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->setContentView(I)V

    .line 62
    invoke-virtual {p0}, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 64
    .local v1, intent:Landroid/content/Intent;
    invoke-static {}, Lmiui/resourcebrowser/AppInnerContext;->getInstance()Lmiui/resourcebrowser/AppInnerContext;

    move-result-object v0

    .line 65
    .local v0, appContext:Lmiui/resourcebrowser/AppInnerContext;
    invoke-virtual {v0}, Lmiui/resourcebrowser/AppInnerContext;->getResourceContext()Lmiui/resourcebrowser/ResourceContext;

    move-result-object v2

    iput-object v2, p0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    .line 67
    const-string v2, "REQUEST_SELECTING_THEME"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Lmiui/resourcebrowser/model/Resource;

    iput-object v2, p0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    .line 68
    iget-object v2, p0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->mResource:Lmiui/resourcebrowser/model/Resource;

    if-nez v2, :cond_0

    .line 69
    invoke-virtual {p0}, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->finish()V

    .line 79
    :goto_0
    return-void

    .line 72
    :cond_0
    iget-object v2, p0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->mResContext:Lmiui/resourcebrowser/ResourceContext;

    const-string v3, "EXTRA_CTX_RESOURCE_TYPE"

    const-wide/16 v4, -0x1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lmiui/resourcebrowser/ResourceContext;->getExtraMeta(Ljava/lang/String;Ljava/io/Serializable;)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->mResourceType:J

    .line 73
    const-string v2, "REQUEST_SOURCE_TYPE"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->mSourceType:I

    .line 75
    invoke-static {p0}, Lmiui/app/SDCardMonitor;->getSDCardMonitor(Landroid/content/Context;)Lmiui/app/SDCardMonitor;

    move-result-object v2

    iput-object v2, p0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->mSDCardMonitor:Lmiui/app/SDCardMonitor;

    .line 76
    iget-object v2, p0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->mSDCardMonitor:Lmiui/app/SDCardMonitor;

    invoke-virtual {v2, p0}, Lmiui/app/SDCardMonitor;->addListener(Lmiui/app/SDCardMonitor$SDCardStatusListener;)V

    .line 78
    invoke-direct {p0}, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->setupUI()V

    goto :goto_0
.end method

.method public onDeleteEventPerformed()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 337
    iget v1, p0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->mSourceType:I

    if-ne v1, v2, :cond_0

    .line 338
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 339
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "RESPONSE_NEEDS_DELETE"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 340
    const/16 v1, 0x68

    invoke-virtual {p0, v1, v0}, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->setResult(ILandroid/content/Intent;)V

    .line 341
    invoke-virtual {p0}, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->finish()V

    .line 345
    .end local v0           #intent:Landroid/content/Intent;
    :goto_0
    return-void

    .line 343
    :cond_0
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->setComponentGridViewClickable(Z)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->mSDCardMonitor:Lmiui/app/SDCardMonitor;

    if-eqz v0, :cond_0

    .line 300
    iget-object v0, p0, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->mSDCardMonitor:Lmiui/app/SDCardMonitor;

    invoke-virtual {v0, p0}, Lmiui/app/SDCardMonitor;->removeListener(Lmiui/app/SDCardMonitor$SDCardStatusListener;)V

    .line 302
    :cond_0
    invoke-super {p0}, Lmiui/app/ObservableActivity;->onDestroy()V

    .line 303
    return-void
.end method

.method public onDownloadEventPerformed()V
    .locals 0

    .prologue
    .line 329
    return-void
.end method

.method public onDownloadFailed(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .parameter "path"
    .parameter "onlineId"

    .prologue
    .line 359
    return-void
.end method

.method public onDownloadProgressUpdated(Ljava/lang/String;Ljava/lang/String;II)V
    .locals 0
    .parameter "path"
    .parameter "onlineId"
    .parameter "downloadBytes"
    .parameter "totalBytes"

    .prologue
    .line 363
    return-void
.end method

.method public onDownloadSuccessful(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter "path"
    .parameter "onlineId"

    .prologue
    .line 354
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->setComponentGridViewClickable(Z)V

    .line 355
    return-void
.end method

.method public onMagicEventPerformed()V
    .locals 0

    .prologue
    .line 349
    invoke-virtual {p0}, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->finish()V

    .line 350
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .parameter "item"

    .prologue
    .line 83
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 84
    invoke-virtual {p0}, Lcom/android/thememanager/activity/ThemeComponentApplyActivity;->finish()V

    .line 86
    :cond_0
    invoke-super {p0, p1}, Lmiui/app/ObservableActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onPickEventPerformed()V
    .locals 0

    .prologue
    .line 325
    return-void
.end method

.method public onStatusChanged(Z)V
    .locals 0
    .parameter "mount"

    .prologue
    .line 367
    invoke-static {p0}, Lmiui/resourcebrowser/util/ResourceHelper;->exit(Landroid/app/Activity;)V

    .line 368
    return-void
.end method

.method public onUpdateEventPerformed()V
    .locals 0

    .prologue
    .line 333
    return-void
.end method
