.class public Lcom/android/thememanager/activity/ThemeTabActivity;
.super Lmiui/resourcebrowser/activity/ResourceTabActivity;
.source "ThemeTabActivity.java"

# interfaces
.implements Lcom/android/thememanager/ThemeResourceConstants;


# instance fields
.field private mResourceType:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lmiui/resourcebrowser/activity/ResourceTabActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected buildResourceContext(Lmiui/resourcebrowser/ResourceContext;)Lmiui/resourcebrowser/ResourceContext;
    .locals 3
    .parameter "resContext"

    .prologue
    .line 80
    invoke-virtual {p0}, Lcom/android/thememanager/activity/ThemeTabActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p1, v0, p0}, Lcom/android/thememanager/util/UIHelper;->buildResourceContext(Lmiui/resourcebrowser/ResourceContext;Landroid/content/Intent;Landroid/content/Context;)Lmiui/resourcebrowser/ResourceContext;

    move-result-object p1

    .line 81
    const-string v0, "EXTRA_CTX_RESOURCE_TYPE"

    const-wide/16 v1, -0x1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lmiui/resourcebrowser/ResourceContext;->getExtraMeta(Ljava/lang/String;Ljava/io/Serializable;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/thememanager/activity/ThemeTabActivity;->mResourceType:J

    .line 82
    invoke-super {p0, p1}, Lmiui/resourcebrowser/activity/ResourceTabActivity;->buildResourceContext(Lmiui/resourcebrowser/ResourceContext;)Lmiui/resourcebrowser/ResourceContext;

    move-result-object v0

    return-object v0
.end method

.method protected getActionBarTabs()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/app/ActionBar$Tab;",
            ">;"
        }
    .end annotation

    .prologue
    .line 56
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 57
    .local v1, ret:Ljava/util/List;,"Ljava/util/List<Landroid/app/ActionBar$Tab;>;"
    invoke-virtual {p0}, Lcom/android/thememanager/activity/ThemeTabActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 58
    .local v0, bar:Landroid/app/ActionBar;
    invoke-virtual {v0}, Landroid/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v2

    const v3, 0x7f0b003b

    invoke-virtual {p0, v3}, Lcom/android/thememanager/activity/ThemeTabActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ActionBar$Tab;->setText(Ljava/lang/CharSequence;)Landroid/app/ActionBar$Tab;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 59
    invoke-virtual {v0}, Landroid/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v2

    const v3, 0x7f0b003c

    invoke-virtual {p0, v3}, Lcom/android/thememanager/activity/ThemeTabActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ActionBar$Tab;->setText(Ljava/lang/CharSequence;)Landroid/app/ActionBar$Tab;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 60
    iget-wide v2, p0, Lcom/android/thememanager/activity/ThemeTabActivity;->mResourceType:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 61
    invoke-virtual {v0}, Landroid/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v2

    const v3, 0x7f0b003d

    invoke-virtual {p0, v3}, Lcom/android/thememanager/activity/ThemeTabActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ActionBar$Tab;->setText(Ljava/lang/CharSequence;)Landroid/app/ActionBar$Tab;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    :cond_0
    return-object v1
.end method

.method protected getResourceController(Lmiui/resourcebrowser/ResourceContext;)Lmiui/resourcebrowser/controller/ResourceController;
    .locals 1
    .parameter "resContext"

    .prologue
    .line 87
    new-instance v0, Lcom/android/thememanager/controller/ThemeController;

    invoke-direct {v0, p1}, Lcom/android/thememanager/controller/ThemeController;-><init>(Lmiui/resourcebrowser/ResourceContext;)V

    return-object v0
.end method

.method protected initTabFragment(I)Lmiui/resourcebrowser/activity/BaseFragment;
    .locals 1
    .parameter "tabPosition"

    .prologue
    .line 68
    if-nez p1, :cond_0

    .line 69
    new-instance v0, Lcom/android/thememanager/activity/LocalThemeListFragment;

    invoke-direct {v0}, Lcom/android/thememanager/activity/LocalThemeListFragment;-><init>()V

    .line 75
    :goto_0
    return-object v0

    .line 70
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 71
    new-instance v0, Lcom/android/thememanager/activity/OnlineThemeListFragment;

    invoke-direct {v0}, Lcom/android/thememanager/activity/OnlineThemeListFragment;-><init>()V

    goto :goto_0

    .line 72
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 73
    new-instance v0, Lcom/android/thememanager/activity/ComponentFragment;

    invoke-direct {v0}, Lcom/android/thememanager/activity/ComponentFragment;-><init>()V

    goto :goto_0

    .line 75
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .parameter "savedInstanceState"

    .prologue
    .line 36
    invoke-super {p0, p1}, Lmiui/resourcebrowser/activity/ResourceTabActivity;->onCreate(Landroid/os/Bundle;)V

    .line 38
    invoke-virtual {p0}, Lcom/android/thememanager/activity/ThemeTabActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 40
    .local v0, actionBar:Landroid/app/ActionBar;
    iget-wide v2, p0, Lcom/android/thememanager/activity/ThemeTabActivity;->mResourceType:J

    invoke-static {v2, v3}, Lcom/android/thememanager/util/ConstantsHelper;->getTitleResId(J)I

    move-result v1

    .line 41
    .local v1, titleId:I
    iget-wide v2, p0, Lcom/android/thememanager/activity/ThemeTabActivity;->mResourceType:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 42
    const v1, 0x7f0b0023

    .line 44
    :cond_0
    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(I)V

    .line 46
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    new-instance v3, Lcom/android/thememanager/activity/ThemeTabActivity$1;

    invoke-direct {v3, p0}, Lcom/android/thememanager/activity/ThemeTabActivity$1;-><init>(Lcom/android/thememanager/activity/ThemeTabActivity;)V

    const-wide/16 v4, 0x320

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 52
    return-void
.end method
