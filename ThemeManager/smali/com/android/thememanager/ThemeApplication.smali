.class public Lcom/android/thememanager/ThemeApplication;
.super Landroid/app/Application;
.source "ThemeApplication.java"

# interfaces
.implements Lcom/android/thememanager/ThemeResourceConstants;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 8

    .prologue
    .line 19
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 21
    sget-object v4, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    .line 22
    .local v1, device:Ljava/lang/String;
    const-string v4, "mione"

    invoke-virtual {v1, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    if-gez v4, :cond_0

    const-string v4, "aries"

    invoke-virtual {v1, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    if-gez v4, :cond_0

    .line 24
    invoke-static {}, Ldalvik/system/VMRuntime;->getRuntime()Ldalvik/system/VMRuntime;

    move-result-object v4

    invoke-virtual {v4}, Ldalvik/system/VMRuntime;->clearGrowthLimit()V

    .line 26
    :cond_0
    invoke-static {p0}, Lcom/android/thememanager/util/ThemeHelper;->createRuntimeFolder(Landroid/content/Context;)V

    .line 28
    invoke-static {p0}, Lcom/android/thememanager/util/ThemeHelper;->initResource(Landroid/content/Context;)V

    .line 30
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lcom/android/thememanager/ThemeApplication;->getFilesDir()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v4

    const-string v5, "RestoreFromBackup"

    invoke-direct {v0, v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    .local v0, backupFlagFile:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 32
    const-wide/16 v2, 0x1

    .local v2, flag:J
    :goto_0
    const-wide/32 v4, 0x40000

    cmp-long v4, v2, v4

    if-gtz v4, :cond_2

    .line 33
    const-wide/16 v4, -0x1

    and-long/2addr v4, v2

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_1

    .line 34
    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Lcom/android/thememanager/util/ThemeHelper;->saveUserPreferenceTime(JJ)V

    .line 32
    :cond_1
    const/4 v4, 0x1

    shl-long/2addr v2, v4

    goto :goto_0

    .line 37
    :cond_2
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 40
    .end local v2           #flag:J
    :cond_3
    new-instance v4, Ljava/lang/Thread;

    new-instance v5, Lcom/android/thememanager/ThemeApplication$1;

    invoke-direct {v5, p0}, Lcom/android/thememanager/ThemeApplication$1;-><init>(Lcom/android/thememanager/ThemeApplication;)V

    invoke-direct {v4, v5}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    .line 46
    return-void
.end method
