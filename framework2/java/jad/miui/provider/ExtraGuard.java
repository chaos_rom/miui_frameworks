// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.provider;

import android.content.*;
import android.content.pm.*;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import java.util.List;

public final class ExtraGuard {

    public ExtraGuard() {
    }

    public static boolean checkApk(Context context, Uri uri) {
        if(uri != null) goto _L2; else goto _L1
_L1:
        boolean flag = false;
_L8:
        return flag;
_L2:
        ContentResolver contentresolver;
        String s;
        String s1;
        contentresolver = context.getContentResolver();
        s = null;
        s1 = uri.getScheme();
        if(!"content".equals(s1)) goto _L4; else goto _L3
_L3:
        Cursor cursor = contentresolver.query(uri, null, null, null, null);
        if(cursor != null) {
            if(cursor.moveToFirst())
                s = cursor.getString(cursor.getColumnIndex("_data"));
            cursor.close();
        }
_L6:
        if(!TextUtils.isEmpty(s))
            break; /* Loop/switch isn't completed */
        flag = false;
        continue; /* Loop/switch isn't completed */
_L4:
        if(s1 == null || "file".equals(s1))
            s = uri.getPath();
        if(true) goto _L6; else goto _L5
_L5:
        int i;
        Uri uri1 = getUri(context, "AntiVirusUri");
        if(uri1 == null)
            break MISSING_BLOCK_LABEL_196;
        ContentValues contentvalues = new ContentValues();
        contentvalues.put("cloud_scan", Boolean.valueOf(false));
        String as[] = new String[1];
        as[0] = s;
        i = contentresolver.update(uri1, contentvalues, null, as);
        if(i != 2)
            flag = true;
        else
            flag = false;
        continue; /* Loop/switch isn't completed */
        Exception exception;
        exception;
        flag = true;
        if(true) goto _L8; else goto _L7
_L7:
    }

    public static boolean checkSms(Context context, String s, String s1) {
        boolean flag = true;
        ContentResolver contentresolver;
        Uri uri;
        contentresolver = context.getContentResolver();
        uri = getUri(context, "AntiSmsSpamUri");
        if(uri == null) goto _L2; else goto _L1
_L1:
        int i;
        ContentValues contentvalues = new ContentValues();
        String as[] = new String[2];
        as[0] = s;
        as[1] = s1;
        i = contentresolver.update(uri, contentvalues, null, as);
        if(i != 2)
            flag = false;
_L4:
        return flag;
        Exception exception;
        exception;
_L2:
        flag = false;
        if(true) goto _L4; else goto _L3
_L3:
    }

    public static Uri getUri(Context context, String s) {
        String s1 = android.provider.Settings.Secure.getString(context.getContentResolver(), "safe_guard_choosed");
        if(TextUtils.isEmpty(s1))
            s1 = "com.tencent.tmsprovider";
        Uri uri = null;
        PackageManager packagemanager = context.getPackageManager();
        Intent intent = new Intent("miui.intent.action.safeguard");
        intent.setPackage(s1);
        List list = packagemanager.queryBroadcastReceivers(intent, 128);
        if(list != null && list.size() > 0)
            uri = Uri.parse(((ResolveInfo)list.get(0)).activityInfo.metaData.getString(s));
        return uri;
    }

    private static final String CLOUD_SCAN_KEY = "cloud_scan";
    public static final String DEFAULT_PACKAGE_NAME = "com.tencent.tmsprovider";
    private static final int SMS_BLACK = 2;
    private static final int SMS_WHITE = 1;
    private static final int VIRUS_BLACK = 2;
    private static final int VIRUS_GRAY = 3;
    private static final int VIRUS_WHITE = 1;
}
