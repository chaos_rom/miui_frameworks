// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.provider;

import android.accounts.Account;
import android.content.*;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import java.util.Iterator;
import java.util.List;
import miui.accounts.ExtraAccountManager;

public abstract class MicloudPushProvider extends ContentProvider {
    public static class Watermark {

        public String mName;
        public String mType;
        public long mValue;

        public Watermark(String s, long l, String s1) {
            mName = s;
            mValue = l;
            mType = s1;
        }
    }


    public MicloudPushProvider() {
    }

    private MatrixCursor getWatermarkListCursor(Account account) {
        MatrixCursor matrixcursor = null;
        if(account != null) {
            matrixcursor = new MatrixCursor(PROJECTION);
            Object aobj[];
            for(Iterator iterator = getWatermarkList(getContext(), account).iterator(); iterator.hasNext(); matrixcursor.addRow(aobj)) {
                Watermark watermark = (Watermark)iterator.next();
                aobj = new Object[3];
                aobj[0] = watermark.mName;
                aobj[1] = Long.valueOf(watermark.mValue);
                aobj[2] = watermark.mType;
            }

        }
        return matrixcursor;
    }

    public int delete(Uri uri, String s, String as[]) {
        return 0;
    }

    protected abstract String getAuthority();

    public String getType(Uri uri) {
        return null;
    }

    protected abstract List getWatermarkList(Context context, Account account);

    public Uri insert(Uri uri, ContentValues contentvalues) {
        return null;
    }

    public boolean onCreate() {
        return true;
    }

    public Cursor query(Uri uri, String as[], String s, String as1[], String s1) {
        UriMatcher urimatcher = new UriMatcher(-1);
        urimatcher.addURI(getAuthority(), "watermark_list", 1);
        Account account = ExtraAccountManager.getXiaomiAccount(getContext());
        switch(urimatcher.match(uri)) {
        default:
            throw new IllegalArgumentException((new StringBuilder()).append("Unknown URI ").append(uri).toString());

        case 1: // '\001'
            return getWatermarkListCursor(account);
        }
    }

    public int update(Uri uri, ContentValues contentvalues, String s, String as[]) {
        return 0;
    }

    public static final String NAME_COLUMNS = "name";
    public static final int NAME_COLUMNS_INDEX = 0;
    public static final String PROJECTION[];
    public static final String TYPE_COLUMNS = "type";
    public static final int TYPE_COLUMNS_INDEX = 2;
    public static final String VALUE_COLUMNS = "value";
    public static final int VALUE_COLUMNS_INDEX = 1;
    private static final int WATERMARK_LIST = 1;
    private static final String WATERMARK_LIST_PATH = "watermark_list";

    static  {
        String as[] = new String[3];
        as[0] = "name";
        as[1] = "value";
        as[2] = "type";
        PROJECTION = as;
    }
}
