// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.util;

import android.content.Context;
import android.media.AudioManager;

public class AudioManagerHelper {

    public AudioManagerHelper() {
    }

    public static boolean isSilentEnabled(Context context) {
        boolean flag;
        if(((AudioManager)context.getSystemService("audio")).getRingerMode() != 2)
            flag = true;
        else
            flag = false;
        return flag;
    }

    public static boolean isVibrateEnabled(Context context) {
        boolean flag = true;
        int i = ((AudioManager)context.getSystemService("audio")).getRingerMode();
        if(2 != i) {
            if(flag != i)
                flag = false;
        } else {
            flag = miui.provider.ExtraSettings.System.getBoolean(context.getContentResolver(), "vibrate_in_normal", false);
        }
        return flag;
    }

    public static void saveLastAudibleRingVolume(Context context, int i) {
        if(i > 0)
            android.provider.Settings.System.putInt(context.getContentResolver(), "last_audible_ring_volume", i);
    }

    public static void setVibrateSetting(Context context, boolean flag, boolean flag1) {
        android.content.ContentResolver contentresolver = context.getContentResolver();
        String s;
        if(flag1)
            s = "vibrate_in_silent";
        else
            s = "vibrate_in_normal";
        miui.provider.ExtraSettings.System.putBoolean(contentresolver, s, flag);
        validateRingerMode(context);
    }

    public static void toggleSilent(Context context, int i) {
        AudioManager audiomanager = (AudioManager)context.getSystemService("audio");
        int j;
        if(2 == audiomanager.getRingerMode())
            j = 0;
        else
            j = 2;
        audiomanager.setRingerMode(j);
        if(2 == j) {
            int k = android.provider.Settings.System.getInt(context.getContentResolver(), "last_audible_ring_volume", 0);
            if(k > 0)
                audiomanager.setStreamVolume(2, k, 0);
        }
        validateRingerMode(context);
        if(i != 0)
            audiomanager.adjustStreamVolume(2, 0, i);
    }

    public static void toggleVibrateSetting(Context context) {
        boolean flag;
        if(!isVibrateEnabled(context))
            flag = true;
        else
            flag = false;
        setVibrateSetting(context, flag, isSilentEnabled(context));
    }

    private static void validateRingerMode(Context context) {
        AudioManager audiomanager;
        android.content.ContentResolver contentresolver;
        int i;
        audiomanager = (AudioManager)context.getSystemService("audio");
        contentresolver = context.getContentResolver();
        i = audiomanager.getRingerMode();
        if(2 != i) goto _L2; else goto _L1
_L1:
        miui.provider.ExtraSettings.System.getBoolean(contentresolver, "vibrate_in_normal", false);
_L4:
        return;
_L2:
        boolean flag = miui.provider.ExtraSettings.System.getBoolean(contentresolver, "vibrate_in_silent", true);
        if(i == 0) {
            if(flag)
                audiomanager.setRingerMode(1);
        } else
        if(!flag)
            audiomanager.setRingerMode(0);
        if(true) goto _L4; else goto _L3
_L3:
    }

    public static void validateVibrateSettings(Context context) {
        AudioManager audiomanager = (AudioManager)context.getSystemService("audio");
        android.content.ContentResolver contentresolver = context.getContentResolver();
        String s;
        if(2 == audiomanager.getRingerMode())
            s = "vibrate_in_normal";
        else
            s = "vibrate_in_silent";
        miui.provider.ExtraSettings.System.putBoolean(contentresolver, s, isVibrateEnabled(context));
    }
}
