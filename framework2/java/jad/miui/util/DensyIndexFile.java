// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.util;

import java.io.*;
import java.util.*;

public abstract class DensyIndexFile {
    public static class Builder {
        private static class IndexData {

            DataItemDescription mDataItemDescriptions[];
            ArrayList mDataItemHolders;
            HashMap mDataMap;
            Item mDefaultValue;
            ArrayList mIndexDataGroups;
            IndexGroupDescription mIndexGroupDescriptions[];

            IndexData(int i) {
                mDataMap = new HashMap();
                mDataItemHolders = new ArrayList();
                mIndexDataGroups = new ArrayList();
                mDataItemDescriptions = new DataItemDescription[i];
            }
        }

        private class DataItemHolder {

            ArrayList getAll() {
                return mList;
            }

            Integer put(Object obj) {
                Integer integer = (Integer)mMap.get(obj);
                if(integer == null) {
                    integer = Integer.valueOf(mList.size());
                    mMap.put(obj, integer);
                    mList.add(obj);
                }
                return integer;
            }

            int size() {
                return mList.size();
            }

            private ArrayList mList;
            private HashMap mMap;
            final Builder this$0;

            private DataItemHolder() {
                this$0 = Builder.this;
                super();
                mMap = new HashMap();
                mList = new ArrayList();
            }

        }

        private class Item
            implements Comparable {

            public volatile int compareTo(Object obj) {
                return compareTo((Item)obj);
            }

            public int compareTo(Item item) {
                return mIndex - item.mIndex;
            }

            public boolean equals(Object obj) {
                boolean flag = false;
                if(obj != this) goto _L2; else goto _L1
_L1:
                flag = true;
_L4:
                return flag;
_L2:
                if(obj instanceof Item)
                    if(mIndex == ((Item)obj).mIndex)
                        flag = true;
                    else
                        flag = false;
                if(true) goto _L4; else goto _L3
_L3:
            }

            public int hashCode() {
                return mIndex;
            }

            int mIndex;
            Object mObjects[];
            final Builder this$0;

            public Item(int i, Object aobj[]) {
                this$0 = Builder.this;
                super();
                mIndex = i;
                mObjects = aobj;
            }
        }


        private void build() throws IOException {
            int i = mIndexDataList.size();
            mFileHeader = new FileHeader(i);
label0:
            for(int j = 0; j < i; j++) {
                IndexData indexdata = (IndexData)mIndexDataList.get(j);
                mFileHeader.mDescriptionOffsets[j] = new DescriptionPair(0L, 0L);
                int k = 0;
                do {
                    if(k >= indexdata.mIndexDataGroups.size() || ((ArrayList)indexdata.mIndexDataGroups.get(k)).size() == 0) {
                        indexdata.mIndexGroupDescriptions = new IndexGroupDescription[k];
                        for(int l = 0; l < indexdata.mIndexGroupDescriptions.length; l++) {
                            ArrayList arraylist = (ArrayList)indexdata.mIndexDataGroups.get(l);
                            Collections.sort(arraylist);
                            int i1 = ((Item)arraylist.get(0)).mIndex;
                            int j1 = 1 + ((Item)arraylist.get(-1 + arraylist.size())).mIndex;
                            indexdata.mIndexGroupDescriptions[l] = new IndexGroupDescription(i1, j1, 0L);
                        }

                        continue label0;
                    }
                    k++;
                } while(true);
            }

            writeAll(null);
        }

        private void checkCurrentIndexingDataKind() throws IOException {
            if(mCurrentIndexData == null)
                throw new IOException("Please add a data kind before adding group");
            else
                return;
        }

        private void checkCurrentIndexingGroup() throws IOException {
            checkCurrentIndexingDataKind();
            if(mCurrentIndexData.mIndexDataGroups.size() == 0)
                throw new IOException("Please add a data group before adding data");
            else
                return;
        }

        private int writeAll(DataOutput dataoutput) throws IOException {
            int i = 0;
            int j = 0;
            do {
                if(j >= mFileHeader.mDescriptionOffsets.length)
                    break;
                int k = i + mFileHeader.write(dataoutput);
                mFileHeader.mDescriptionOffsets[j].mIndexGroupDescriptionOffset = k;
                IndexData indexdata = (IndexData)mIndexDataList.get(j);
                if(dataoutput != null)
                    dataoutput.writeInt(indexdata.mIndexGroupDescriptions.length);
                int l = k + 4;
                for(int i1 = 0; i1 < indexdata.mIndexGroupDescriptions.length; i1++)
                    l += indexdata.mIndexGroupDescriptions[i1].write(dataoutput);

                mFileHeader.mDescriptionOffsets[j].mDataItemDescriptionOffset = l;
                if(dataoutput != null)
                    dataoutput.writeInt(indexdata.mDataItemDescriptions.length);
                i = l + 4;
                for(int j1 = 0; j1 < indexdata.mDataItemDescriptions.length; j1++)
                    i += indexdata.mDataItemDescriptions[j1].write(dataoutput);

                for(int k1 = 0; k1 < indexdata.mDataItemDescriptions.length; k1++) {
                    indexdata.mDataItemDescriptions[k1].mOffset = i;
                    i += indexdata.mDataItemDescriptions[k1].writeDataItems(dataoutput, ((DataItemHolder)indexdata.mDataItemHolders.get(k1)).getAll());
                }

                int l1 = 0;
                while(l1 < indexdata.mIndexGroupDescriptions.length)  {
                    indexdata.mIndexGroupDescriptions[l1].mOffset = i;
                    if(dataoutput == null) {
                        int k2 = 0;
                        for(int l2 = 0; l2 < indexdata.mDataItemDescriptions.length; l2++)
                            k2 += indexdata.mDataItemDescriptions[l2].mIndexSize;

                        i += k2 * (indexdata.mIndexGroupDescriptions[l1].mMaxIndex - indexdata.mIndexGroupDescriptions[l1].mMinIndex);
                    } else {
                        int i2 = indexdata.mIndexGroupDescriptions[l1].mMinIndex;
                        while(i2 < indexdata.mIndexGroupDescriptions[l1].mMaxIndex)  {
                            Item item = (Item)indexdata.mDataMap.get(Integer.valueOf(i2));
                            if(item == null)
                                item = indexdata.mDefaultValue;
                            int j2 = 0;
                            while(j2 < indexdata.mDataItemDescriptions.length)  {
                                if(indexdata.mDataItemDescriptions[j2].mIndexSize == 1)
                                    dataoutput.writeByte(((Integer)item.mObjects[j2]).intValue());
                                else
                                if(indexdata.mDataItemDescriptions[j2].mIndexSize == 2)
                                    dataoutput.writeShort(((Integer)item.mObjects[j2]).intValue());
                                else
                                if(indexdata.mDataItemDescriptions[j2].mIndexSize == 4)
                                    dataoutput.writeInt(((Integer)item.mObjects[j2]).intValue());
                                else
                                if(indexdata.mDataItemDescriptions[j2].mIndexSize == 8)
                                    dataoutput.writeLong(((Long)item.mObjects[j2]).longValue());
                                i += indexdata.mDataItemDescriptions[j2].mIndexSize;
                                j2++;
                            }
                            i2++;
                        }
                    }
                    l1++;
                }
                j++;
            } while(true);
            return i;
        }

        public transient void add(int i, Object aobj[]) throws IOException {
            int j;
            checkCurrentIndexingGroup();
            if(mCurrentIndexData.mDataItemDescriptions.length != aobj.length)
                throw new IOException("Different number of objects inputted");
            j = 0;
_L12:
            if(j >= aobj.length)
                break MISSING_BLOCK_LABEL_769;
            mCurrentIndexData.mDataItemDescriptions[j].mType;
            JVM INSTR tableswitch 0 8: default 96
        //                       0 106
        //                       1 147
        //                       2 188
        //                       3 229
        //                       4 270
        //                       5 372
        //                       6 471
        //                       7 570
        //                       8 669;
               goto _L1 _L2 _L3 _L4 _L5 _L6 _L7 _L8 _L9 _L10
_L10:
            break MISSING_BLOCK_LABEL_669;
_L7:
            break; /* Loop/switch isn't completed */
_L1:
            throw new IOException("Unsupported type of objects inputted");
_L2:
            if(!(aobj[j] instanceof Byte))
                throw new IOException((new StringBuilder()).append("Object[").append(j).append("] should be byte").toString());
            break; /* Loop/switch isn't completed */
_L3:
            if(!(aobj[j] instanceof Short))
                throw new IOException((new StringBuilder()).append("Object[").append(j).append("] should be short").toString());
            break; /* Loop/switch isn't completed */
_L4:
            if(!(aobj[j] instanceof Integer))
                throw new IOException((new StringBuilder()).append("Object[").append(j).append("] should be int").toString());
            break; /* Loop/switch isn't completed */
_L5:
            if(!(aobj[j] instanceof Long))
                throw new IOException((new StringBuilder()).append("Object[").append(j).append("] should be long").toString());
            break; /* Loop/switch isn't completed */
_L6:
            if(!(aobj[j] instanceof String))
                throw new IOException((new StringBuilder()).append("Object[").append(j).append("] should be String").toString());
            aobj[j] = ((DataItemHolder)mCurrentIndexData.mDataItemHolders.get(j)).put(aobj[j]);
            mCurrentIndexData.mDataItemDescriptions[j].mIndexSize = DensyIndexFile.getSizeOf(((DataItemHolder)mCurrentIndexData.mDataItemHolders.get(j)).size());
_L13:
            j++;
            if(true) goto _L12; else goto _L11
_L11:
            if(!(aobj[j] instanceof byte[]))
                throw new IOException((new StringBuilder()).append("Object[").append(j).append("] should be byte[]").toString());
            aobj[j] = ((DataItemHolder)mCurrentIndexData.mDataItemHolders.get(j)).put(aobj[j]);
            mCurrentIndexData.mDataItemDescriptions[j].mIndexSize = DensyIndexFile.getSizeOf(((DataItemHolder)mCurrentIndexData.mDataItemHolders.get(j)).size());
              goto _L13
_L8:
            if(!(aobj[j] instanceof short[]))
                throw new IOException((new StringBuilder()).append("Object[").append(j).append("] should be short[]").toString());
            aobj[j] = ((DataItemHolder)mCurrentIndexData.mDataItemHolders.get(j)).put(aobj[j]);
            mCurrentIndexData.mDataItemDescriptions[j].mIndexSize = DensyIndexFile.getSizeOf(((DataItemHolder)mCurrentIndexData.mDataItemHolders.get(j)).size());
              goto _L13
_L9:
            if(!(aobj[j] instanceof int[]))
                throw new IOException((new StringBuilder()).append("Object[").append(j).append("] should be int[]").toString());
            aobj[j] = ((DataItemHolder)mCurrentIndexData.mDataItemHolders.get(j)).put(aobj[j]);
            mCurrentIndexData.mDataItemDescriptions[j].mIndexSize = DensyIndexFile.getSizeOf(((DataItemHolder)mCurrentIndexData.mDataItemHolders.get(j)).size());
              goto _L13
            if(!(aobj[j] instanceof long[]))
                throw new IOException((new StringBuilder()).append("Object[").append(j).append("] should be long[]").toString());
            aobj[j] = ((DataItemHolder)mCurrentIndexData.mDataItemHolders.get(j)).put(aobj[j]);
            mCurrentIndexData.mDataItemDescriptions[j].mIndexSize = DensyIndexFile.getSizeOf(((DataItemHolder)mCurrentIndexData.mDataItemHolders.get(j)).size());
              goto _L13
            Item item = new Item(i, aobj);
            mCurrentIndexData.mDataMap.put(Integer.valueOf(i), item);
            ((ArrayList)mCurrentIndexData.mIndexDataGroups.get(-1 + mCurrentIndexData.mIndexDataGroups.size())).add(item);
            return;
        }

        public void addGroup(int ai[], Object aobj[][]) throws IOException {
            checkCurrentIndexingDataKind();
            if(ai.length == aobj.length) {
                newGroup();
                for(int i = 0; i < ai.length; i++)
                    add(ai[i], aobj[i]);

            } else {
                throw new IOException("Different number between indexes and objects");
            }
        }

        public transient void addKind(Object aobj[]) throws IOException {
            mCurrentIndexData = new IndexData(aobj.length);
            mIndexDataList.add(mCurrentIndexData);
            int i = 0;
            while(i < aobj.length)  {
                mCurrentIndexData.mDataItemHolders.add(new DataItemHolder());
                byte byte0 = 1;
                byte byte1;
                if(aobj[i] instanceof Byte) {
                    byte1 = 0;
                    byte0 = 1;
                    ((DataItemHolder)mCurrentIndexData.mDataItemHolders.get(i)).put(aobj[i]);
                } else
                if(aobj[i] instanceof Short) {
                    byte1 = 1;
                    byte0 = 2;
                    ((DataItemHolder)mCurrentIndexData.mDataItemHolders.get(i)).put(aobj[i]);
                } else
                if(aobj[i] instanceof Integer) {
                    byte1 = 2;
                    byte0 = 4;
                    ((DataItemHolder)mCurrentIndexData.mDataItemHolders.get(i)).put(aobj[i]);
                } else
                if(aobj[i] instanceof Long) {
                    byte1 = 3;
                    byte0 = 8;
                    ((DataItemHolder)mCurrentIndexData.mDataItemHolders.get(i)).put(aobj[i]);
                } else
                if(aobj[i] instanceof String) {
                    byte1 = 4;
                    aobj[i] = ((DataItemHolder)mCurrentIndexData.mDataItemHolders.get(i)).put(aobj[i]);
                } else
                if(aobj[i] instanceof byte[]) {
                    byte1 = 5;
                    aobj[i] = ((DataItemHolder)mCurrentIndexData.mDataItemHolders.get(i)).put(aobj[i]);
                } else
                if(aobj[i] instanceof short[]) {
                    byte1 = 6;
                    aobj[i] = ((DataItemHolder)mCurrentIndexData.mDataItemHolders.get(i)).put(aobj[i]);
                } else
                if(aobj[i] instanceof int[]) {
                    byte1 = 7;
                    aobj[i] = ((DataItemHolder)mCurrentIndexData.mDataItemHolders.get(i)).put(aobj[i]);
                } else
                if(aobj[i] instanceof long[]) {
                    byte1 = 8;
                    aobj[i] = ((DataItemHolder)mCurrentIndexData.mDataItemHolders.get(i)).put(aobj[i]);
                } else {
                    throw new IOException((new StringBuilder()).append("Unsupported type of object[").append(i).append("] inputted").toString());
                }
                mCurrentIndexData.mDataItemDescriptions[i] = new DataItemDescription(byte1, byte0, (byte)0, (byte)0, 0L);
                i++;
            }
            mCurrentIndexData.mDefaultValue = new Item(-1, aobj);
        }

        public void newGroup() {
            if(mCurrentIndexData.mIndexDat