// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.util;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;

// Referenced classes of package miui.util:
//            SimplePool

public class UiUtils {

    public UiUtils() {
    }

    public static boolean getBoolean(Context context, int i) {
        boolean flag = true;
        TypedValue typedvalue = (TypedValue)sTypedValuePool.acquire();
        context.getTheme().resolveAttribute(i, typedvalue, flag);
        if(typedvalue.data == 0)
            flag = false;
        sTypedValuePool.release(typedvalue);
        return flag;
    }

    public static int getColor(Context context, int i) {
        return context.getResources().getColor(resolveAttribute(context, i));
    }

    public static Drawable getDrawable(Context context, int i) {
        int j = resolveAttribute(context, i);
        Drawable drawable;
        if(j > 0)
            drawable = context.getResources().getDrawable(j);
        else
            drawable = null;
        return drawable;
    }

    public static int resolveAttribute(Context context, int i) {
        TypedValue typedvalue = (TypedValue)sTypedValuePool.acquire();
        int j = -1;
        if(context.getTheme().resolveAttribute(i, typedvalue, true))
            j = typedvalue.resourceId;
        sTypedValuePool.release(typedvalue);
        return j;
    }

    static SimplePool.PoolInstance sTypedValuePool = SimplePool.newInsance(new SimplePool.Manager() {

        public TypedValue createInstance() {
            return new TypedValue();
        }

        public volatile Object createInstance() {
            return createInstance();
        }

    }, 4);

}
