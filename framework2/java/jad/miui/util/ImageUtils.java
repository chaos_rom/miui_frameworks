// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.util;

import android.graphics.*;
import java.io.FileOutputStream;
import java.io.InputStream;
import libcore.io.Streams;

// Referenced classes of package miui.util:
//            InputStreamLoader

public class ImageUtils {

    public ImageUtils() {
    }

    public static int computeSampleSize(InputStreamLoader inputstreamloader, int i) {
        int j = 1;
        if(i > 0) {
            android.graphics.BitmapFactory.Options options = getBitmapSize(inputstreamloader);
            for(double d = Math.sqrt(((double)options.outWidth * (double)options.outHeight) / (double)i); (double)(j * 2) <= d; j <<= 1);
        }
        return j;
    }

    public static boolean cropBitmapToAnother(Bitmap bitmap, Bitmap bitmap1, boolean flag) {
        boolean flag1 = true;
        if(bitmap != null && bitmap1 != null) {
            int i = bitmap.getWidth();
            int j = bitmap.getHeight();
            int k = bitmap1.getWidth();
            int l = bitmap1.getHeight();
            float f = Math.max((1.0F * (float)k) / (float)i, (1.0F * (float)l) / (float)j);
            Paint paint = new Paint();
            paint.setFilterBitmap(flag1);
            paint.setAntiAlias(flag1);
            paint.setDither(flag1);
            Canvas canvas = new Canvas(bitmap1);
            canvas.translate(((float)k - f * (float)i) / 2.0F, ((float)l - f * (float)j) / 2.0F);
            canvas.scale(f, f);
            canvas.drawBitmap(bitmap, 0.0F, 0.0F, paint);
            if(flag)
                bitmap.recycle();
        } else {
            flag1 = false;
        }
        return flag1;
    }

    public static final Bitmap getBitmap(InputStreamLoader inputstreamloader, int i) {
        android.graphics.BitmapFactory.Options options;
        Bitmap bitmap;
        int j;
        options = getDefaultOptions();
        options.inSampleSize = computeSampleSize(inputstreamloader, i);
        bitmap = null;
        j = 0;
_L4:
        int k;
        k = j + 1;
        if(j >= 3)
            break MISSING_BLOCK_LABEL_48;
        Bitmap bitmap1 = BitmapFactory.decodeStream(inputstreamloader.get(), null, options);
        bitmap = bitmap1;
        inputstreamloader.close();
_L2:
        return bitmap;
        OutOfMemoryError outofmemoryerror;
        outofmemoryerror;
        System.gc();
        options.inSampleSize = 2 * options.inSampleSize;
        inputstreamloader.close();
        j = k;
        continue; /* Loop/switch isn't completed */
        Exception exception1;
        exception1;
        inputstreamloader.close();
        if(true) goto _L2; else goto _L1
_L1:
        Exception exception;
        exception;
        inputstreamloader.close();
        throw exception;
        if(true) goto _L4; else goto _L3
_L3:
    }

    public static Bitmap getBitmap(InputStreamLoader inputstreamloader, int i, int j) {
        int k = 2 * (i * j);
        if(i <= 0 || j <= 0)
            k = -1;
        Bitmap bitmap = getBitmap(inputstreamloader, k);
        if(k > 0)
            bitmap = scaleBitmapToDesire(bitmap, i, j, true);
        return bitmap;
    }

    public static Bitmap getBitmap(InputStreamLoader inputstreamloader, int i, int j, Bitmap bitmap) {
        Bitmap bitmap1 = null;
        if(bitmap == null || bitmap.isRecycled()) goto _L2; else goto _L1
_L1:
        android.graphics.BitmapFactory.Options options = getBitmapSize(inputstreamloader);
        if(options.outWidth != bitmap.getWidth() || options.outHeight != bitmap.getHeight())
            break MISSING_BLOCK_LABEL_81;
        Bitmap bitmap3;
        android.graphics.BitmapFactory.Options options1 = getDefaultOptions();
        options1.inBitmap = bitmap;
        options1.inSampleSize = 1;
        bitmap3 = BitmapFactory.decodeStream(inputstreamloader.get(), null, options1);
        bitmap1 = bitmap3;
        inputstreamloader.close();
_L4:
        if(bitmap1 == null)
            bitmap.recycle();
_L2:
        Bitmap bitmap2 = bitmap1;
        Exception exception;
        Exception exception1;
        if(bitmap2 != null) {
            if(i > 0 && j > 0)
                bitmap2 = scaleBitmapToDesire(bitmap2, i, j, true);
        } else {
            bitmap2 = getBitmap(inputstreamloader, i, j);
        }
        return bitmap2;
        exception1;
        inputstreamloader.close();
        if(true) goto _L4; else goto _L3
_L3:
        exception;
        inputstreamloader.close();
        throw exception;
    }

    public static final android.graphics.BitmapFactory.Options getBitmapSize(String s) {
        return getBitmapSize(new InputStreamLoader(s));
    }

    public static final android.graphics.BitmapFactory.Options getBitmapSize(InputStreamLoader inputstreamloader) {
        android.graphics.BitmapFactory.Options options = new android.graphics.BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(inputstreamloader.get(), null, options);
        inputstreamloader.close();
_L2:
        return options;
        Exception exception1;
        exception1;
        inputstreamloader.close();
        if(true) goto _L2; else goto _L1
_L1:
        Exception exception;
        exception;
        inputstreamloader.close();
        throw exception;
    }

    public static android.graphics.BitmapFactory.Options getDefaultOptions() {
        android.graphics.BitmapFactory.Options options = new android.graphics.BitmapFactory.Options();
        options.inDither = false;
        options.inJustDecodeBounds = false;
        options.inSampleSize = 1;
        options.inScaled = false;
        return options;
    }

    public static boolean isPngFormat(InputStreamLoader inputstreamloader) {
        boolean flag = false;
        boolean flag1;
        InputStream inputstream = inputstreamloader.get();
        byte abyte0[] = new byte[PNG_HEAD_FORMAT.length];
        if(inputstream.read(abyte0) < abyte0.length)
            break MISSING_BLOCK_LABEL_39;
        flag1 = isPngFormat(abyte0);
        flag = flag1;
        if(inputstreamloader != null)
            inputstreamloader.close();
_L2:
        return flag;
        Exception exception1;
        exception1;
        if(inputstreamloader != null)
            inputstreamloader.close();
        if(true) goto _L2; else goto _L1
_L1:
        Exception exception;
        exception;
        if(inputstreamloader != null)
            inputstreamloader.close();
        throw exception;
    }

    public static boolean isPngFormat(byte abyte0[]) {
        boolean flag = false;
        if(abyte0 != null && abyte0.length >= PNG_HEAD_FORMAT.length) goto _L2; else goto _L1
_L1:
        return flag;
_L2:
        for(int i = 0; i < PNG_HEAD_FORMAT.length; i++)
            if(abyte0[i] != PNG_HEAD_FORMAT[i])
                continue; /* Loop/switch isn't completed */

        flag = true;
        if(true) goto _L1; else goto _L3
_L3:
    }

    public static boolean saveBitmapToLocal(InputStreamLoader inputstreamloader, String s, int i, int j) {
        if(inputstreamloader != null && s != null && i >= 1 && j >= 1) goto _L2; else goto _L1
_L1:
        boolean flag = false;
_L4:
        return flag;
_L2:
        flag = false;
        android.graphics.BitmapFactory.Options options = getBitmapSize(inputstreamloader);
        if(options.outWidth > 0 && options.outHeight > 0)
            if(options.outWidth == i && options.outHeight == j) {
                flag = saveToFile(inputstreamloader, s);
            } else {
                Bitmap bitmap = getBitmap(inputstreamloader, i, j);
                if(bitmap != null) {
                    flag = saveToFile(bitmap, s, isPngFormat(inputstreamloader));
                    bitmap.recycle();
                }
            }
        if(true) goto _L4; else goto _L3
_L3:
    }

    public static boolean saveToFile(Bitmap bitmap, String s) {
        return saveToFile(bitmap, s, false);
    }

    public static boolean saveToFile(Bitmap bitmap, String s, boolean flag) {
        boolean flag1;
label0:
        {
            if(bitmap != null)
                try {
                    FileOutputStream fileoutputstream = new FileOutputStream(s);
                    android.graphics.Bitmap.CompressFormat compressformat;
                    if(flag)
                        compressformat = android.graphics.Bitmap.CompressFormat.PNG;
                    else
                        compressformat = android.graphics.Bitmap.CompressFormat.JPEG;
                    bitmap.compress(compressformat, 100, fileoutputstream);
                    fileoutputstream.close();
                    flag1 = true;
                    break label0;
                }
                catch(Exception exception) { }
            flag1 = false;
        }
        return flag1;
    }

    private static boolean saveToFile(InputStreamLoader inputstreamloader, String s) {
        boolean flag = false;
        FileOutputStream fileoutputstream = new FileOutputStream(s);
        Streams.copy(inputstreamloader.get(), fileoutputstream);
        fileoutputstream.close();
        inputstreamloader.close();
        flag = true;
_L2:
        return flag;
        Exception exception;
        exception;
        if(true) goto _L2; else goto _L1
_L1:
    }

    public static Bitmap scaleBitmapToDesire(Bitmap bitmap, int i, int j, boolean flag) {
        Bitmap bitmap1 = null;
        try {
            int k = bitmap.getWidth();
            int l = bitmap.getHeight();
            if(k == i && l == j) {
                bitmap1 = bitmap;
            } else {
                android.graphics.Bitmap.Config config = android.graphics.Bitmap.Config.ARGB_8888;
                if(bitmap.getConfig() != null)
                    config = bitmap.getConfig();
                bitmap1 = Bitmap.createBitmap(i, j, config);
                cropBitmapToAnother(bitmap, bitmap1, flag);
            }
        }
        catch(Exception exception) { }
        catch(OutOfMemoryError outofmemoryerror) { }
        return bitmap1;
    }

    private static byte PNG_HEAD_FORMAT[];

    static  {
        byte abyte0[] = new byte[8];
        abyte0[0] = -119;
        abyte0[1] = 80;
        abyte0[2] = 78;
        abyte0[3] = 71;
        abyte0[4] = 13;
        abyte0[5] = 10;
        abyte0[6] = 26;
        abyte0[7] = 10;
        PNG_HEAD_FORMAT = abyte0;
    }
}
