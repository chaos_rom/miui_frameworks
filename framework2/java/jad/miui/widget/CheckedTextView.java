// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.Checkable;
import android.widget.TextView;

public class CheckedTextView extends TextView
    implements Checkable {

    public CheckedTextView(Context context) {
        this(context, null);
    }

    public CheckedTextView(Context context, AttributeSet attributeset) {
        this(context, attributeset, 0);
    }

    public CheckedTextView(Context context, AttributeSet attributeset, int i) {
        super(context, attributeset, i);
        TypedArray typedarray = context.obtainStyledAttributes(attributeset, android.R.styleable.CheckedTextView, i, 0);
        Drawable drawable = typedarray.getDrawable(1);
        if(drawable != null)
            setCheckMarkDrawable(drawable);
        setChecked(typedarray.getBoolean(0, false));
        typedarray.recycle();
    }

    public Drawable getCheckMarkDrawable() {
        return getCompoundDrawables()[0];
    }

    public boolean isChecked() {
        return mChecked;
    }

    protected int[] onCreateDrawableState(int i) {
        int ai[] = super.onCreateDrawableState(i + 1);
        if(isChecked())
            mergeDrawableStates(ai, CHECKED_STATE_SET);
        return ai;
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityevent) {
        super.onInitializeAccessibilityEvent(accessibilityevent);
        accessibilityevent.setClassName(miui/widget/CheckedTextView.getName());
        accessibilityevent.setChecked(mChecked);
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilitynodeinfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilitynodeinfo);
        accessibilitynodeinfo.setClassName(miui/widget/CheckedTextView.getName());
        accessibilitynodeinfo.setCheckable(true);
        accessibilitynodeinfo.setChecked(mChecked);
    }

    public void setCheckMarkDrawable(int i) {
        if(i == 0 || i != mCheckMarkResource) {
            mCheckMarkResource = i;
            Drawable drawable = null;
            if(mCheckMarkResource != 0)
                drawable = getResources().getDrawable(mCheckMarkResource);
            setCheckMarkDrawable(drawable);
        }
    }

    public void setCheckMarkDrawable(Drawable drawable) {
        setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
    }

    public void setChecked(boolean flag) {
        if(mChecked != flag) {
            mChecked = flag;
            refreshDrawableState();
            notifyAccessibilityStateChanged();
        }
    }

    public void toggle() {
        boolean flag;
        if(!mChecked)
            flag = true;
        else
            flag = false;
        setChecked(flag);
    }

    private static final int CHECKED_STATE_SET[];
    private int mCheckMarkResource;
    private boolean mChecked;

    static  {
        int ai[] = new int[1];
        ai[0] = 0x10100a0;
        CHECKED_STATE_SET = ai;
    }
}
