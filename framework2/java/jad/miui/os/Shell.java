// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.os;


public class Shell {

    public Shell() {
    }

    public static boolean chmod(String s, int i) {
        return nativeChmod(s, i);
    }

    public static boolean chown(String s, int i, int j) {
        return nativeChown(s, i, j);
    }

    public static boolean copy(String s, String s1) {
        return nativeCopy(s, s1);
    }

    public static boolean link(String s, String s1) {
        return nativeLink(s, s1);
    }

    public static boolean mkdirs(String s) {
        return nativeMkdirs(s);
    }

    public static boolean move(String s, String s1) {
        return nativeMove(s, s1);
    }

    private static native boolean nativeChmod(String s, int i);

    private static native boolean nativeChown(String s, int i, int j);

    private static native boolean nativeCopy(String s, String s1);

    private static native boolean nativeLink(String s, String s1);

    private static native boolean nativeMkdirs(String s);

    private static native boolean nativeMove(String s, String s1);

    private static native boolean nativeRemove(String s);

    private static native boolean nativeRun(String s);

    public static boolean remove(String s) {
        return nativeRemove(s);
    }

    public static transient boolean run(String s, Object aobj[]) {
        String s1;
        if(aobj.length > 0)
            s1 = String.format(s, aobj);
        else
            s1 = s;
        return nativeRun(s1);
    }

    private static final String TAG = "Shell";

    static  {
        System.loadLibrary("shell_jni");
    }
}
