// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.telephony.phonenumber;

import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import com.android.i18n.phonenumbers.PhoneNumberUtil;
import com.android.i18n.phonenumbers.geocoding.PhoneNumberOfflineGeocoder;
import java.io.IOException;
import java.util.*;
import miui.provider.Telocation;

// Referenced classes of package miui.telephony.phonenumber:
//            CountryCode

public class ChineseTelocation {

    private ChineseTelocation() {
        mNormalCustomLocations = new HashMap();
        mGroupCustomLocations = new HashMap();
        mDensyIndexFileReader = new miui.util.DensyIndexFile.Reader("/etc/telocation.idf");
_L1:
        return;
        IOException ioexception;
        ioexception;
        Log.w("ChineseTelocation", "Can't read /etc/telocation.idf, NO mobile telocation supported!");
          goto _L1
    }

    private String findCustomLocation(int i, CharSequence charsequence, int j, int k) {
        String s;
        s = null;
        if(i > 0)
            s = (String)mNormalCustomLocations.get(Integer.valueOf(i));
        if(s != null || mGroupCustomLocations.size() <= 0) goto _L2; else goto _L1
_L1:
        Iterator iterator = mGroupCustomLocations.keySet().iterator();
_L4:
        if(iterator.hasNext()) {
            String s1 = (String)iterator.next();
            if(k != s1.length())
                continue; /* Loop/switch isn't completed */
            boolean flag = true;
            int l = 0;
            int i1 = s1.length();
            do {
label0:
                {
                    if(l < i1) {
                        if(s1.charAt(l) == '#' || s1.charAt(l) == charsequence.charAt(j + l))
                            break label0;
                        flag = false;
                    }
                    if(flag)
                        s = (String)mGroupCustomLocations.get(s1);
                    continue; /* Loop/switch isn't completed */
                }
                l++;
            } while(true);
        }
_L2:
        if(s == null)
            s = "";
        return s;
        if(true) goto _L4; else goto _L3
_L3:
    }

    public static ChineseTelocation getInstance() {
        return sInstance;
    }

    private void initObserver(Context context) {
        mContext = context.getApplicationContext();
        if(mContext == null)
            mContext = context;
        updateTelocationSetting();
        updateCustomLocation();
        Handler handler = new Handler(mContext.getMainLooper());
        mSettingObserver = new ContentObserver(handler) {

            public void onChange(boolean flag) {
                super.onChange(flag);
                Log.d("ChineseTelocation", "telocation setting changed, reloading ...");
                updateTelocationSetting();
            }

            final ChineseTelocation this$0;

             {
                this$0 = ChineseTelocation.this;
                super(handler);
            }
        };
        mContext.getContentResolver().registerContentObserver(android.provider.Settings.System.getUriFor("enable_telocation"), false, mSettingObserver);
        mCustomLocationObserver = new ContentObserver(handler) {

            public void onChange(boolean flag) {
                super.onChange(flag);
                Log.d("ChineseTelocation", "custom location changed, reloading ...");
                updateCustomLocation();
            }

            final ChineseTelocation this$0;

             {
                this$0 = ChineseTelocation.this;
                super(handler);
            }
        };
        mContext.getContentResolver().registerContentObserver(Telocation.CONTENT_CUSTOM_LOCATION_URI, true, mCustomLocationObserver);
    }

    private void updateCustomLocation() {
        mNormalCustomLocations.clear();
        mGroupCustomLocations.clear();
        if(!mAllowTelocation) goto _L2; else goto _L1
_L1:
        Cursor cursor = null;
        ContentResolver contentresolver = mContext.getContentResolver();
        android.net.Uri uri = Telocation.CONTENT_CUSTOM_LOCATION_URI;
        String as[] = new String[4];
        as[0] = "_id";
        as[1] = "number";
        as[2] = "location";
        as[3] = "type";
        cursor = contentresolver.query(uri, as, null, null, null);
        if(cursor == null) goto _L4; else goto _L3
_L3:
        if(!cursor.moveToNext()) goto _L4; else goto _L5
_L5:
        int i;
        String s;
        String s1;
        i = cursor.getInt(3);
        s = cursor.getString(1);
        s1 = cursor.getString(2);
        i;
        JVM INSTR tableswitch 0 2: default 258
    //                   0 148
    //                   1 148
    //                   2 232;
           goto _L3 _L6 _L6 _L7
_L6:
        byte byte0 = 0;
        if(!s.startsWith("+86")) goto _L9; else goto _L8
_L8:
        byte0 = 3;
_L11:
        int j = getUniqId(s, byte0, s.length(), true);
        if(j != 0)
            mNormalCustomLocations.put(Integer.valueOf(j), s1);
          goto _L3
        Exception exception;
        exception;
        if(cursor != null)
            cursor.close();
        throw exception;
_L9:
        if(!s.startsWith("0086")) goto _L11; else goto _L10
_L10:
        byte0 = 4;
          goto _L11
_L7:
        mGroupCustomLocations.put(s, s1);
          goto _L3
_L4:
        if(cursor != null)
            cursor.close();
_L2:
    }

    private void updateTelocationSetting() {
        boolean flag = true;
        if(android.provider.Settings.System.getInt(mContext.getContentResolver(), "enable_telocation", flag) == 0)
            flag = false;
        mAllowTelocation = flag;
        updateCustomLocation();
    }

    protected void finalize() throws Throwable {
        if(mDensyIndexFileReader != null)
            try {
                mDensyIndexFileReader.close();
            }
            catch(IOException ioexception) { }
        if(mSettingObserver != null)
            mContext.getContentResolver().unregisterContentObserver(mSettingObserver);
        if(mCustomLocationObserver != null)
            mContext.getContentResolver().unregisterContentObserver(mCustomLocationObserver);
        super.finalize();
    }

    public String getAreaCode(Context context, CharSequence charsequence, int i, int j) {
        int k = getUniqId(charsequence, i, j, true);
        return (String)mDensyIndexFileReader.get(0, k, 1);
    }

    public String getExternalLocation(Context context, String s, CharSequence charsequence, Locale locale) {
        if(mContext == null)
            initObserver(context);
        if(mAllowTelocation) goto _L2; else goto _L1
_L1:
        String s1 = null;
_L4:
        return s1;
_L2:
        if(TextUtils.isEmpty(s)) {
            s = CountryCode.getUserDefinedCountryCode();
            if(TextUtils.isEmpty(s))
                s = CountryCode.getIccCountryCode();
        }
        String s2;
        if(TextUtils.isEmpty(s))
            break MISSING_BLOCK_LABEL_93;
        s2 = PhoneNumberOfflineGeocoder.getInstance().getDescriptionForNumber(PhoneNumberUtil.getInstance().parse(charsequence.toString(), PhoneNumberUtil.getInstance().getRegionCodeForCountryCode(Integer.parseInt(s))), locale);
        s1 = s2;
        continue; /* Loop/switch isn't completed */
        Exception exception;
        exception;
        s1 = "";
        if(true) goto _L4; else goto _L3
_L3:
    }

    public String getLocation(Context context, CharSequence charsequence, int i, int j, boolean flag) {
        if(mContext == null)
            initObserver(context);
        if(mAllowTelocation) goto _L2; else goto _L1
_L1:
        String s = null;
_L4:
        return s;
_L2:
        int k = -1;
        if(flag)
            k = getUniqId(charsequence, i, j, true);
        s = findCustomLocation(k, charsequence, i, j);
        if(TextUtils.isEmpty(s) && k > 0)
            s = (String)mDensyIndexFileReader.get(0, k, 0);
        if(true) goto _L4; else goto _L3
_L3:
    }

    int getUniqId(CharSequence charsequence, int i, int j, boolean flag) {
        if(j > 0 && charsequence.charAt(i) == '0') {
            i++;
            j--;
        }
        if(j > 1) goto _L2; else goto _L1
_L1:
        int k = 0;
_L4:
        return k;
_L2:
        switch(charsequence.charAt(i)) {
        default:
            if(j > 2) {
                k = 10 * (10 * (-48 + charsequence.charAt(i)) + (-48 + charsequence.charAt(i + 1))) + (-48 + charsequence.charAt(i + 2));
                continue; /* Loop/switch isn't completed */
            }
            break;

        case 48: // '0'
            break;

        case 49: // '1'
            if(charsequence.charAt(i + 1) == '0') {
                k = 10;
                continue; /* Loop/switch isn't completed */
            }
            if(!flag || j <= 6)
                break;
            k = 0xf4240 + 0x186a0 * (-48 + charsequence.charAt(i + 1)) + 10000 * (-48 + charsequence.charAt(i + 2)) + 1000 * (-48 + charsequence.charAt(i + 3)) + 100 * (-48 + charsequence.charAt(i + 4)) + 10 * (-48 + charsequence.charAt(i + 5)) + (-48 + charsequence.charAt(i + 6));
            if(k == 0x150ead && j > 10 && charsequence.charAt(i + 7) == '8' && charsequence.charAt(i + 8) == '0' && charsequence.charAt(i + 9) == '0' && charsequence.charAt(i + 10) == '0')
                k = 0;
            continue; /* Loop/switch isn't completed */

        case 50: // '2'
            k = 20 + (-48 + charsequence.charAt(i + 1));
            continue; /* Loop/switch isn't completed */
        }
        k = 0;
        if(true) goto _L4; else goto _L3
_L3:
    }

    public String parseAreaCode(CharSequence charsequence, int i, int j) {
        int k = getUniqId(charsequence, i, j, false);
        return (String)mDensyIndexFileReader.get(0, k, 1);
    }

    private static final String DATA_PATH = "/etc/telocation.idf";
    private static final String EMPTY = "";
    private static final String TAG = "ChineseTelocation";
    private static final int UNIQUE_ID_NONE;
    private static ChineseTelocation sInstance = new ChineseTelocation();
    private boolean mAllowTelocation;
    private Context mContext;
    private ContentObserver mCustomLocationObserver;
    private miui.util.DensyIndexFile.Reader mDensyIndexFileReader;
    private HashMap mGroupCustomLocations;
    private HashMap mNormalCustomLocations;
    private ContentObserver mSettingObserver;



}
