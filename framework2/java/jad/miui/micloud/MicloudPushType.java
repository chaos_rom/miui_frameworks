// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.micloud;

import android.text.TextUtils;

public class MicloudPushType {

    public MicloudPushType(String s, String s1, String s2, String s3) {
        this(s, s1, s2, s3, "p", 0L);
    }

    public MicloudPushType(String s, String s1, String s2, String s3, String s4, long l) {
        if(TextUtils.isEmpty(s) || TextUtils.isEmpty(s1) || TextUtils.isEmpty(s2) || TextUtils.isEmpty(s3)) {
            throw new IllegalArgumentException("the packageName, contentAuthority, pushType and pushName must not be empty: ");
        } else {
            mPackageName = s;
            mContentAuthority = s1;
            mPushType = s2;
            mPushName = s3;
            mWatermarkType = s4;
            mWatermark = l;
            return;
        }
    }

    public boolean equals(Object obj) {
        boolean flag = true;
        if(obj != this) goto _L2; else goto _L1
_L1:
        return flag;
_L2:
        if(!(obj instanceof MicloudPushType)) {
            flag = false;
        } else {
            MicloudPushType micloudpushtype = (MicloudPushType)obj;
            if(!TextUtils.equals(mPackageName, micloudpushtype.mPackageName) || !TextUtils.equals(mContentAuthority, micloudpushtype.mContentAuthority) || !TextUtils.equals(mPushType, micloudpushtype.mPushType) || !TextUtils.equals(mPushName, micloudpushtype.mPushName) || !TextUtils.equals(mWatermarkType, micloudpushtype.mWatermarkType) || mWatermark != micloudpushtype.mWatermark)
                flag = false;
        }
        if(true) goto _L1; else goto _L3
_L3:
    }

    public String getContentAuthority() {
        return mContentAuthority;
    }

    public String getPackageName() {
        return mPackageName;
    }

    public String getPushName() {
        return mPushName;
    }

    public String getPushType() {
        return mPushType;
    }

    public long getWatermark() {
        return mWatermark;
    }

    public String getWatermarkType() {
        return mWatermarkType;
    }

    public int hashCode() {
        return 31 * (31 * (31 * (31 * (31 * (527 + mPackageName.hashCode()) + mContentAuthority.hashCode()) + mPushType.hashCode()) + mPushName.hashCode()) + mWatermarkType.hashCode()) + (int)(mWatermark ^ mWatermark >>> 32);
    }

    public String toString() {
        return (new StringBuilder()).append("MicloudPushType {packageName=").append(mPackageName).append(", contentAuthority=").append(mContentAuthority).append(", pushType=").append(mPushType).append(", pushName=").append(mPushName).append(", watermarkType=").append(mWatermarkType).append(", watermark=").append(mWatermark).append("}").toString();
    }

    private final String mContentAuthority;
    private final String mPackageName;
    private final String mPushName;
    private final String mPushType;
    private long mWatermark;
    private final String mWatermarkType;
}
