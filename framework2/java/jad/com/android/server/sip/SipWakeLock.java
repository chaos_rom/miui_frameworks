// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package com.android.server.sip;

import android.os.PowerManager;
import java.util.HashSet;

class SipWakeLock {

    SipWakeLock(PowerManager powermanager) {
        mHolders = new HashSet();
        mPowerManager = powermanager;
    }

    /**
     * @deprecated Method acquire is deprecated
     */

    void acquire(long l) {
        this;
        JVM INSTR monitorenter ;
        if(mTimerWakeLock == null) {
            mTimerWakeLock = mPowerManager.newWakeLock(1, "SipWakeLock.timer");
            mTimerWakeLock.setReferenceCounted(true);
        }
        mTimerWakeLock.acquire(l);
        this;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        throw exception;
    }

    /**
     * @deprecated Method acquire is deprecated
     */

    void acquire(Object obj) {
        this;
        JVM INSTR monitorenter ;
        mHolders.add(obj);
        if(mWakeLock == null)
            mWakeLock = mPowerManager.newWakeLock(1, "SipWakeLock");
        if(!mWakeLock.isHeld())
            mWakeLock.acquire();
        this;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        throw exception;
    }

    /**
     * @deprecated Method release is deprecated
     */

    void release(Object obj) {
        this;
        JVM INSTR monitorenter ;
        mHolders.remove(obj);
        if(mWakeLock != null && mHolders.isEmpty() && mWakeLock.isHeld())
            mWakeLock.release();
        this;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        throw exception;
    }

    /**
     * @deprecated Method reset is deprecated
     */

    void reset() {
        this;
        JVM INSTR monitorenter ;
        mHolders.clear();
        release(null);
        this;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        throw exception;
    }

    private static final boolean DEBUG = false;
    private static final String TAG = "SipWakeLock";
    private HashSet mHolders;
    private PowerManager mPowerManager;
    private android.os.PowerManager.WakeLock mTimerWakeLock;
    private android.os.PowerManager.WakeLock mWakeLock;
}
