// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package com.android.server.sip;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.*;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import java.util.*;
import java.util.concurrent.Executor;

class SipWakeupTimer extends BroadcastReceiver {
    private static class MyEventComparator
        implements Comparator {

        public int compare(MyEvent myevent, MyEvent myevent1) {
            if(myevent != myevent1) goto _L2; else goto _L1
_L1:
            int i = 0;
_L4:
            return i;
_L2:
            i = myevent.mMaxPeriod - myevent1.mMaxPeriod;
            if(i == 0)
                i = -1;
            if(true) goto _L4; else goto _L3
_L3:
        }

        public volatile int compare(Object obj, Object obj1) {
            return compare((MyEvent)obj, (MyEvent)obj1);
        }

        public boolean equals(Object obj) {
            boolean flag;
            if(this == obj)
                flag = true;
            else
                flag = false;
            return flag;
        }

        private MyEventComparator() {
        }

    }

    private static class MyEvent {

        private String toString(Object obj) {
            String s = obj.toString();
            int i = s.indexOf("$");
            if(i > 0)
                s = s.substring(i + 1);
            return s;
        }

        public String toString() {
            String s = super.toString();
            String s1 = s.substring(s.indexOf("@"));
            return (new StringBuilder()).append(s1).append(":").append(mPeriod / 1000).append(":").append(mMaxPeriod / 1000).append(":").append(toString(mCallback)).toString();
        }

        Runnable mCallback;
        long mLastTriggerTime;
        int mMaxPeriod;
        int mPeriod;
        long mTriggerTime;

        MyEvent(int i, Runnable runnable, long l) {
            mMaxPeriod = i;
            mPeriod = i;
            mCallback = runnable;
            mLastTriggerTime = l;
        }
    }


    public SipWakeupTimer(Context context, Executor executor) {
        mEventQueue = new TreeSet(new MyEventComparator());
        mContext = context;
        mAlarmManager = (AlarmManager)context.getSystemService("alarm");
        context.registerReceiver(this, new IntentFilter(getAction()));
        mExecutor = executor;
    }

    private void cancelAlarm() {
        mAlarmManager.cancel(mPendingIntent);
        mPendingIntent = null;
    }

    private void execute(long l) {
        if(!stopped() && !mEventQueue.isEmpty()) {
            Iterator iterator = mEventQueue.iterator();
            do {
                if(!iterator.hasNext())
                    break;
                MyEvent myevent = (MyEvent)iterator.next();
                if(myevent.mTriggerTime == l) {
                    myevent.mLastTriggerTime = l;
                    myevent.mTriggerTime = myevent.mTriggerTime + (long)myevent.mPeriod;
                    mExecutor.execute(myevent.mCallback);
                }
            } while(true);
            scheduleNext();
        }
    }

    private String getAction() {
        return toString();
    }

    private void insertEvent(MyEvent myevent) {
        long l = SystemClock.elapsedRealtime();
        if(mEventQueue.isEmpty()) {
            myevent.mTriggerTime = l + (long)myevent.mPeriod;
            mEventQueue.add(myevent);
        } else {
            MyEvent myevent1 = (MyEvent)mEventQueue.first();
            int i = myevent1.mPeriod;
            if(i <= myevent.mMaxPeriod) {
                myevent.mPeriod = i * (myevent.mMaxPeriod / i);
                int j = i * ((myevent.mMaxPeriod - (int)(myevent1.mTriggerTime - l)) / i);
                myevent.mTriggerTime = myevent1.mTriggerTime + (long)j;
                mEventQueue.add(myevent);
            } else {
                long l1 = l + (long)myevent.mPeriod;
                if(myevent1.mTriggerTime < l1) {
                    myevent.mTriggerTime = myevent1.mTriggerTime;
                    myevent.mLastTriggerTime = myevent.mLastTriggerTime - (long)myevent.mPeriod;
                } else {
                    myevent.mTriggerTime = l1;
                }
                mEventQueue.add(myevent);
                recalculatePeriods();
            }
        }
    }

    private void printQueue() {
        int i;
        i = 0;
        Iterator iterator = mEventQueue.iterator();
        do {
            if(!iterator.hasNext())
                break;
            MyEvent myevent = (MyEvent)iterator.next();
            Log.d("_SIP.WkTimer_", (new StringBuilder()).append("     ").append(myevent).append(": scheduled at ").append(showTime(myevent.mTriggerTime)).append(": last at ").append(showTime(myevent.mLastTriggerTime)).toString());
        } while(++i < 5);
        if(mEventQueue.size() <= i) goto _L2; else goto _L1
_L1:
        Log.d("_SIP.WkTimer_", "     .....");
_L4:
        return;
_L2:
        if(i == 0)
            Log.d("_SIP.WkTimer_", "     <empty>");
        if(true) goto _L4; else goto _L3
_L3:
    }

    private void recalculatePeriods() {
        if(!mEventQueue.isEmpty()) {
            MyEvent myevent = (MyEvent)mEventQueue.first();
            int i = myevent.mMaxPeriod;
            long l = myevent.mTriggerTime;
            for(Iterator iterator = mEventQueue.iterator(); iterator.hasNext();) {
                MyEvent myevent1 = (MyEvent)iterator.next();
                myevent1.mPeriod = i * (myevent1.mMaxPeriod / i);
                myevent1.mTriggerTime = l + (long)(i * ((int)((myevent1.mLastTriggerTime + (long)myevent1.mMaxPeriod) - l) / i));
            }

            TreeSet treeset = new TreeSet(mEventQueue.comparator());
            treeset.addAll(mEventQueue);
            mEventQueue.clear();
            mEventQueue = treeset;
        }
    }

    private void scheduleNext() {
        if(!stopped() && !mEventQueue.isEmpty()) {
            if(mPendingIntent != null)
                throw new RuntimeException("pendingIntent is not null!");
            MyEvent myevent = (MyEvent)mEventQueue.first();
            Intent intent = new Intent(getAction());
            intent.putExtra("TriggerTime", myevent.mTriggerTime);
            PendingIntent pendingintent = PendingIntent.getBroadcast(mContext, 0, intent, 0x8000000);
            mPendingIntent = pendingintent;
            mAlarmManager.set(2, myevent.mTriggerTime, pendingintent);
        }
    }

    private String showTime(long l) {
        int i = (int)(l % 1000L);
        int j = (int)(l / 1000L);
        int k = j / 60;
        int i1 = j % 60;
        Object aobj[] = new Object[3];
        aobj[0] = Integer.valueOf(k);
        aobj[1] = Integer.valueOf(i1);
        aobj[2] = Integer.valueOf(i);
        return String.format("%d.%d.%d", aobj);
    }

    private boolean stopped() {
        boolean flag;
        if(mEventQueue == null) {
            Log.w("_SIP.WkTimer_", "Timer stopped");
            flag = true;
        } else {
            flag = false;
        }
        return flag;
    }

    /**
     * @deprecated Method cancel is deprecated
     */

    public void cancel(Runnable runnable) {
        this;
        JVM INSTR monitorenter ;
        if(stopped()) goto _L2; else goto _L1
_L1:
        boolean flag = mEventQueue.isEmpty();
        if(!flag) goto _L3; else goto _L2
_L2:
        this;
        JVM INSTR monitorexit ;
        return;
_L3:
        MyEvent myevent;
        myevent = (MyEvent)mEventQueue.first();
        Iterator iterator = mEventQueue.iterator();
        do {
            if(!iterator.hasNext())
                break;
            if(((MyEvent)iterator.next()).mCallback == runnable)
                iterator.remove();
        } while(true);
        break MISSING_BLOCK_LABEL_87;
        Exception exception;
        exception;
        throw exception;
        if(mEventQueue.isEmpty())
            cancelAlarm();
        else
        if(mEventQueue.first() != myevent) {
            cancelAlarm();
            MyEvent myevent1 = (MyEvent)mEventQueue.first();
            myevent1.mPeriod = myevent1.mMaxPeriod;
            myevent1.mTriggerTime = myevent1.mLastTriggerTime + (long)myevent1.mPeriod;
            recalculatePeriods();
            scheduleNext();
        }
          goto _L2
    }

    /**
     * @deprecated Method onReceive is deprecated
     */

    public void onReceive(Context context, Intent intent) {
        this;
        JVM INSTR monitorenter ;
        String s = intent.getAction();
        if(!getAction().equals(s) || !intent.getExtras().containsKey("TriggerTime")) goto _L2; else goto _L1
_L1:
        mPendingIntent = null;
        execute(intent.getLongExtra("TriggerTime", -1L));
_L4:
        this;
        JVM INSTR monitorexit ;
        return;
_L2:
        Log.d("_SIP.WkTimer_", (new StringBuilder()).append("unrecognized intent: ").append(intent).toString());
        if(true) goto _L4; else goto _L3
_L3:
        Exception exception;
        exception;
        throw exception;
    }

    /**
     * @deprecated Method set is deprecated
     */

    public void set(int i, Runnable runnable) {
        this;
        JVM INSTR monitorenter ;
        boolean flag = stopped();
        if(!flag) goto _L2; else goto _L1
_L1:
        this;
        JVM INSTR monitorexit ;
        return;
_L2:
        MyEvent myevent = new MyEvent(i, runnable, SystemClock.elapsedRealtime());
        insertEvent(myevent);
        if(mEventQueue.first() == myevent) {
            if(mEventQueue.size() > 1)
                cancelAlarm();
            scheduleNext();
        }
        myevent.mTriggerTime;
        if(true) goto _L1; else goto _L3
_L3:
        Exception exception;
        exception;
        throw exception;
    }

    /**
     * @deprecated Method stop is deprecated
     */

    public void stop() {
        this;
        JVM INSTR monitorenter ;
        mContext.unregisterReceiver(this);
        if(mPendingIntent != null) {
            mAlarmManager.cancel(mPendingIntent);
            mPendingIntent = null;
        }
        mEventQueue.clear();
        mEventQueue = null;
        this;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        throw exception;
    }

    private static final boolean DEBUG_TIMER = false;
    private static final String TAG = "_SIP.WkTimer_";
    private static final String TRIGGER_TIME = "TriggerTime";
    private AlarmManager mAlarmManager;
    private Context mContext;
    private TreeSet mEventQueue;
    private Executor mExecutor;
    private PendingIntent mPendingIntent;
}
