// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package com.android.server.sip;

import android.app.PendingIntent;
import android.content.*;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.sip.*;
import android.net.wifi.WifiManager;
import android.os.*;
import android.util.Log;
import java.io.IOException;
import java.net.*;
import java.util.*;
import java.util.concurrent.Executor;
import javax.sip.SipException;

// Referenced classes of package com.android.server.sip:
//            SipWakeLock, SipWakeupTimer, SipSessionListenerProxy, SipSessionGroup

public final class SipService extends android.net.sip.ISipService.Stub {
    private class MyExecutor extends Handler
        implements Executor {

        private void executeInternal(Runnable runnable) {
            runnable.run();
            mMyWakeLock.release(runnable);
_L2:
            return;
            Throwable throwable;
            throwable;
            Log.e("SipService", (new StringBuilder()).append("run task: ").append(runnable).toString(), throwable);
            mMyWakeLock.release(runnable);
            if(true) goto _L2; else goto _L1
_L1:
            Exception exception;
            exception;
            mMyWakeLock.release(runnable);
            throw exception;
        }

        public void execute(Runnable runnable) {
            mMyWakeLock.acquire(runnable);
            Message.obtain(this, 0, runnable).sendToTarget();
        }

        public void handleMessage(Message message) {
            if(message.obj instanceof Runnable)
                executeInternal((Runnable)message.obj);
            else
                Log.w("SipService", (new StringBuilder()).append("can't handle msg: ").append(message).toString());
        }

        final SipService this$0;

        MyExecutor() {
            this$0 = SipService.this;
            super(SipService.createLooper());
        }
    }

    private class ConnectivityReceiver extends BroadcastReceiver {

        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if(bundle != null) {
                final NetworkInfo info = (NetworkInfo)bundle.get("networkInfo");
                mExecutor.execute(new Runnable() {

                    public void run() {
                        onConnectivityChanged(info);
                    }

                    final ConnectivityReceiver this$1;
                    final NetworkInfo val$info;

                 {
                    this$1 = ConnectivityReceiver.this;
                    info = networkinfo;
                    super();
                }
                });
            }
        }

        final SipService this$0;

        private ConnectivityReceiver() {
            this$0 = SipService.this;
            super();
        }

    }

    private class AutoRegistrationProcess extends SipSessionAdapter
        implements Runnable, SipSessionGroup.KeepAliveProcessCallback {

        private int backoffDuration() {
            int i = 10 * mBackoff;
            if(i > 3600)
                i = 3600;
            else
                mBackoff = 2 * mBackoff;
            return i;
        }

        private String getAction() {
            return toString();
        }

        private boolean notCurrentSession(ISipSession isipsession) {
            boolean flag = true;
            if(isipsession != mSession) {
                ((SipSessionGroup.SipSessionImpl)isipsession).setListener(null);
                mMyWakeLock.release(isipsession);
            } else {
                boolean flag1;
                if(!mRunning)
                    flag1 = flag;
                else
                    flag1 = false;
                flag = flag1;
            }
            return flag;
        }

        private void restart(int i) {
            Log.d(TAG, (new StringBuilder()).append("Refresh registration ").append(i).append("s later.").toString());
            mTimer.cancel(this);
            mTimer.set(i * 1000, this);
        }

        private void restartLater() {
            mRegistered = false;
            restart(backoffDuration());
        }

        private void startKeepAliveProcess(int i) {
            if(mKeepAliveSession == null)
                mKeepAliveSession = mSession.duplicate();
            else
                mKeepAliveSession.stopKeepAliveProcess();
            mKeepAliveSession.startKeepAliveProcess(i, this);
_L1:
            return;
            SipException sipexception;
            sipexception;
            Log.e(TAG, (new StringBuilder()).append("failed to start keepalive w interval=").append(i).toString(), sipexception);
              goto _L1
        }

        private void stopKeepAliveProcess() {
            if(mKeepAliveSession != null) {
                mKeepAliveSession.stopKeepAliveProcess();
                mKeepAliveSession = null;
            }
            mKeepAliveSuccessCount = 0;
        }

        public boolean isRegistered() {
            return mRegistered;
        }

        public void onError(int i, String s) {
            onResponse(true);
        }

        public void onKeepAliveIntervalChanged() {
            if(mKeepAliveSession != null) {
                int i = getKeepAliveInterval();
                mKeepAliveSuccessCount = 0;
                startKeepAliveProcess(i);
            }
        }

        public void onRegistering(ISipSession isipsession) {
            SipService sipservice = SipService.this;
            sipservice;
            JVM INSTR monitorenter ;
            if(!notCurrentSession(isipsession)) {
                mRegistered = false;
                mProxy.onRegistering(isipsession);
            }
            return;
        }

        public void onRegistrationDone(ISipSession isipsession, int i) {
            SipService sipservice = SipService.this;
            sipservice;
            JVM INSTR monitorenter ;
            if(!notCurrentSession(isipsession)) goto _L2; else goto _L1
_L2:
            mProxy.onRegistrationDone(isipsession, i);
            if(i <= 0) goto _L4; else goto _L3
_L3:
            mExpiryTime = SystemClock.elapsedRealtime() + (long)(i * 1000);
            if(!mRegistered) {
                mRegistered = true;
                int j = i - 60;
                if(j < 60)
                    j = 60;
                restart(j);
                SipProfile sipprofile = mSession.getLocalProfile();
                if(mKeepAliveSession == null && (isBehindNAT(mLocalIp) || sipprofile.getSendKeepAlive()))
                    startKeepAliveProcess(getKeepAliveInterval());
            }
            mMyWakeLock.release(isipsession);
_L5:
            sipservice;
            JVM INSTR monitorexit ;
            break; /* Loop/switch isn't completed */
            Exception exception;
            exception;
            throw exception;
_L4:
            mRegistered = false;
            mExpiryTime = -1L;
            run();
            if(true) goto _L5; else goto _L1
_L1:
        }

        public void onRegistrationFailed(ISipSession isipsession, int i, String s) {
            SipService sipservice = SipService.this;
            sipservice;
            JVM INSTR monitorenter ;
            if(!notCurrentSession(isipsession)) goto _L2; else goto _L1
_L4:
            restartLater();
_L3:
            mErrorCode = i;
            mErrorMessage = s;
            mProxy.onRegistrationFailed(isipsession, i, s);
            mMyWakeLock.release(isipsession);
            break; /* Loop/switch isn't completed */
            Exception exception;
            exception;
            throw exception;
_L5:
            stop();
            if(true) goto _L3; else goto _L1
_L1:
            return;
_L2:
            i;
            JVM INSTR lookupswitch 2: default 23
        //                       -12: 72
        //                       -8: 72;
               goto _L4 _L5 _L5
        }

        public void onRegistrationTimeout(ISipSession isipsession) {
            SipService sipservice = SipService.this;
            sipservice;
            JVM INSTR monitorenter ;
            if(!notCurrentSession(isipsession)) {
                mErrorCode = -5;
                mProxy.onRegistrationTimeout(isipsession);
                restartLater();
                mMyWakeLock.release(isipsession);
            }
            return;
        }

        public void onResponse(boolean flag) {
            SipService sipservice = SipService.this;
            sipservice;
            JVM INSTR monitorenter ;
            if(!flag) goto _L2; else goto _L1
_L1:
            int i = getKeepAliveInterval();
            if(mKeepAliveSuccessCount < 10) {
                Log.i(TAG, (new StringBuilder()).append("keepalive doesn't work with interval ").append(i).append(", past success count=").append(mKeepAliveSuccessCount).toString());
                if(i > 10) {
                    restartPortMappingLifetimeMeasurement(mSession.getLocalProfile(), i);
                    mKeepAliveSuccessCount = 0;
                }
            } else {
                mKeepAliveSuccessCount = mKeepAliveSuccessCount / 2;
            }
_L5:
            if(mRunning && flag) goto _L4; else goto _L3
            Exception exception;
            exception;
            throw exception;
_L2:
            startPortMappingLifetimeMeasurement(mSession.getLocalProfile());
            mKeepAliveSuccessCount = 1 + mKeepAliveSuccessCount;
              goto _L5
_L4:
            mKeepAliveSession = null;
            mMyWakeLock.acquire(mSession);
            mSession.register(3600);
            sipservice;
            JVM INSTR monitorexit ;
_L3:
        }

        public void run() {
            SipService sipservice = SipService.this;
            sipservice;
            JVM INSTR monitorenter ;
            if(mRunning) {
                mErrorCode = 0;
                mErrorMessage = null;
                if(mNetworkType != -1) {
                    mMyWakeLock.acquire(mSession);
                    mSession.register(3600);
                }
            }
            return;
        }

        public void setListener(ISipSessionListener isipsessionlistener) {
            SipService sipservice = SipService.this;
            sipservice;
            JVM INSTR monitorenter ;
            mProxy.setListener(isipsessionlistener);
            if(mSession != null) goto _L2; else goto _L1
_L1:
            int i = 0;
              goto _L3
_L8:
            mProxy.onRegistering(mSession);
_L7:
            return;
_L2:
            i = mSession.getState();
              goto _L3
_L9:
            if(!mRegistered) goto _L5; else goto _L4
_L4:
            Exception exception;
            Throwable throwable;
            int j = (int)(mExpiryTime - SystemClock.elapsedRealtime());
            mProxy.onRegistrationDone(mSession, j);
            if(true) goto _L7; else goto _L6
_L6:
            JVM INSTR monitorexit ;
            throw exception;
_L5:
            try {
                if(mErrorCode != 0) {
                    if(mErrorCode == -5)
                        mProxy.onRegistrationTimeout(mSession);
                    else
                        mProxy.onRegistrationFailed(mSession, mErrorCode, mErrorMessage);
                } else
                if(mNetworkType == -1)
                    mProxy.onRegistrationFailed(mSession, -10, "no data connection");
                else
                if(!mRunning)
                    mProxy.onRegistrationFailed(mSession, -4, "registration not running");
                else
                    mProxy.onRegistrationFailed(mSession, -9, String.valueOf(i));
            }
            // Misplaced declaration of an exception variable
            catch(Throwable throwable) {
                Log.w(TAG, (new StringBuilder()).append("setListener(): ").append(throwable).toString());
            }
            finally {
                sipservice;
            }
              goto _L7
_L3:
            if(i != 1 && i != 2) goto _L9; else goto _L8
        }

        public void start(SipSessionGroup sipsessiongroup) {
            if(!mRunning) {
                mRunning = true;
                mBackoff = 1;
                mSession = (SipSessionGroup.SipSessionImpl)sipsessiongroup.createSession(this);
                if(mSession != null) {
                    mMyWakeLock.acquire(mSession);
                    mSession.unregister();
                    TAG = (new StringBuilder()).append("SipAutoReg:").append(mSession.getLocalProfile().getUriString()).toString();
                }
            }
        }

        public void stop() {
            if(mRunning) {
                mRunning = false;
                mMyWakeLock.release(mSession);
                if(mSession != null) {
                    mSession.setListener(null);
                    if(mNetworkType != -1 && mRegistered)
                        mSession.unregister();
                }
                mTimer.cancel(this);
                stopKeepAliveProcess();
                mRegistered = false;
                setListener(mProxy.getListener());
            }
        }

        private static final int MIN_KEEPALIVE_SUCCESS_COUNT = 10;
        private String TAG;
        private int mBackoff;
        private int mErrorCode;
        private String mErrorMessage;
        private long mExpiryTime;
        private SipSessionGroup.SipSessionImpl mKeepAliveSession;
        private int mKeepAliveSuccessCount;
        private SipSessionListenerProxy mProxy;
        private boolean mRegistered;
        private boolean mRunning;
        private SipSessionGroup.SipSessionImpl mSession;
        final SipService this$0;

        private AutoRegistrationProcess() {
            this$0 = SipService.this;
            super();
            TAG = "SipAutoReg";
            mProxy = new SipSessionListenerProxy();
            mBackoff = 1;
            mRunning = false;
            mKeepAliveSuccessCount = 0;
        }

    }

    private class IntervalMeasurementProcess
        implements Runnable, SipSessionGroup.KeepAliveProcessCallback {

        private boolean checkTermination() {
            boolean flag;
            if(mMaxInterval - mMinInterval < 5)
                flag = true;
            else
                flag = false;
            return flag;
        }

        private void restart() {
            SipService sipservice = SipService.this;
            sipservice;
            JVM INSTR monitorenter ;
            if(mSession != null) goto _L2; else goto _L1
_L2:
            Log.d("SipKeepAliveInterval", (new StringBuilder()).append("restart measurement w interval=").append(mInterval).toString());
            mSession.stopKeepAliveProcess();
            mPassCount = 0;
            mSession.startKeepAliveProcess(mInterval, this);
_L3:
            sipservice;
            JVM INSTR monitorexit ;
            break; /* Loop/switch isn't completed */
            Exception exception;
            exception;
            throw exception;
            SipException sipexception;
            sipexception;
            Log.e("SipKeepAliveInterval", "restart()", sipexception);
            if(true) goto _L3; else goto _L1
_L1:
        }

        private void restartLater() {
            SipService sipservice = SipService.this;
            sipservice;
            JVM INSTR monitorenter ;
            mTimer.cancel(this);
            mTimer.set(0x1d4c0, this);
            return;
        }

        public void onError(int i, String s) {
            Log.w("SipKeepAliveInterval", (new StringBuilder()).append("interval measurement error: ").append(s).toString());
            restartLater();
        }

        public void onResponse(boolean flag) {
            SipService sipservice = SipService.this;
            sipservice;
            JVM INSTR monitorenter ;
            if(flag) goto _L2; else goto _L1
_L1:
            int i;
            i = 1 + mPassCount;
            mPassCount = i;
            if(i == 10) goto _L4; else goto _L3
_L4:
            if(mKeepAliveInterval > 0)
                mLastGoodKeepAliveInterval = mKeepAliveInterval;
            SipService sipservice1 = SipService.this;
            int j = mInterval;
            mMinInterval = j;
            sipservice1.mKeepAliveInterval = j;
            onKeepAliveIntervalChanged();
_L9:
            if(!checkTermination()) goto _L6; else goto _L5
_L5:
            stop();
            mKeepAliveInterval = mMinInterval;
_L7:
            sipservice;
            JVM INSTR monitorexit ;
            break; /* Loop/switch isn't completed */
            Exception exception;
            exception;
            throw exception;
_L2:
            mMaxInterval = mInterval;
            continue; /* Loop/switch isn't completed */
_L6:
            mInterval = (mMaxInterval + mMinInterval) / 2;
            restart();
            if(true) goto _L7; else goto _L3
_L3:
            return;
            if(true) goto _L9; else goto _L8
_L8:
        }

        public void run() {
            mTimer.cancel(this);
            restart();
        }

        public void start() {
            SipService sipservice = SipService.this;
            sipservice;
            JVM INSTR monitorenter ;
            if(mSession == null) goto _L2; else goto _L1
_L2:
            mInterval = (mMaxInterval + mMinInterval) / 2;
            mPassCount = 0;
            if(mInterval >= 10 && !checkTermination()) goto _L4; else goto _L3
_L3:
            Log.w("SipKeepAliveInterval", (new StringBuilder()).append("measurement aborted; interval=[").append(mMinInterval).append(",").append(mMaxInterval).append("]").toString());
              goto _L1
            Exception exception;
            exception;
            throw exception;
_L4:
            try {
                Log.d("SipKeepAliveInterval", (new StringBuilder()).append("start measurement w interval=").append(mInterval).toString());
                mGroup = new SipSessionGroupExt(mLocalProfile, null, null);
                mGroup.setWakeupTimer(new SipWakeupTimer(mContext, mExecutor));
                mSession = (SipSessionGroup.SipSessionImpl)mGroup.createSession(null);
                mSession.startKeepAliveProcess(mInterval, this);
            }
            catch(Throwable throwable) {
                onError(-4, throwable.toString());
            }
            sipservice;
            JVM INSTR monitorexit ;
_L1:
        }

        public void stop() {
            SipService sipservice = SipService.this;
            sipservice;
            JVM INSTR monitorenter ;
            if(mSession != null) {
                mSession.stopKeepAliveProcess();
                mSession = null;
            }
            if(mGroup != null) {
                mGroup.close();
                mGroup = null;
            }
            mTimer.cancel(this);
            return;
        }

        private static final int MAX_RETRY_COUNT = 5;
        private static final int MIN_INTERVAL = 5;
        private static final int NAT_MEASUREMENT_RETRY_INTERVAL = 120;
        private static final int PASS_THRESHOLD = 10;
        private static final String TAG = "SipKeepAliveInterval";
        private SipSessionGroupExt mGroup;
        private int mInterval;
        private SipProfile mLocalProfile;
        private int mMaxInterval;
        private int mMinInterval;
        private int mPassCount;
        private SipSessionGroup.SipSessionImpl mSession;
        final SipService this$0;

        public IntervalMeasurementProcess(SipProfile sipprofile, int i, int j) {
            this$0 = SipService.this;
            super();
            mMaxInterval = j;
            mMinInterval = i;
            mLocalProfile = sipprofile;
        }
    }

    private class SipSessionGroupExt extends SipSessionAdapter {

        private SipProfile duplicate(SipProfile sipprofile) {
            SipProfile sipprofile1;
            try {
                sipprofile1 = (new android.net.sip.SipProfile.Builder(sipprofile)).setPassword("*").build();
            }
            catch(Exception exception) {
                Log.wtf("SipService", "duplicate()", exception);
                throw new RuntimeException("duplicate profile", exception);
            }
            return sipprofile1;
        }

        private String getUri() {
            return mSipGroup.getLocalProfileUri();
        }

        public void close() {
            mOpenedToReceiveCalls = false;
            mSipGroup.close();
            mAutoRegistration.stop();
        }

        public boolean containsSession(String s) {
            return mSipGroup.containsSession(s);
        }

        public ISipSession createSession(ISipSessionListener isipsessionlistener) {
            return mSipGroup.createSession(isipsessionlistener);
        }

        public SipProfile getLocalProfile() {
            return mSipGroup.getLocalProfile();
        }

        public boolean isOpenedToReceiveCalls() {
            return mOpenedToReceiveCalls;
        }

        public boolean isRegistered() {
            return mAutoRegistration.isRegistered();
        }

        public void onConnectivityChanged(boolean flag) throws SipException {
            mSipGroup.onConnectivityChanged();
            if(flag) {
                mSipGroup.reset();
                if(mOpenedToReceiveCalls)
                    openToReceiveCalls();
            } else {
                mSipGroup.close();
                mAutoRegistration.stop();
            }
        }

        public void onError(ISipSession isipsession, int i, String s) {
        }

        public void onKeepAliveIntervalChanged() {
            mAutoRegistration.onKeepAliveIntervalChanged();
        }

        public void onRinging(ISipSession isipsession, SipProfile sipprofile, String s) {
            SipSessionGroup.SipSessionImpl sipsessionimpl = (SipSessionGroup.SipSessionImpl)isipsession;
            SipService sipservice = SipService.this;
            sipservice;
            JVM INSTR monitorenter ;
            if(isRegistered() && !callingSelf(this, sipsessionimpl)) goto _L2; else goto _L1
_L1:
            sipsessionimpl.endCall();
_L3:
            return;
_L2:
            addPendingSession(sipsessionimpl);
            Intent intent = SipManager.createIncomingCallBroadcast(sipsessionimpl.getCallId(), s);
            mIncomingCallPendingIntent.send(mContext, 101, intent);
_L4:
            sipservice;
            JVM INSTR monitorexit ;
              goto _L3
            Exception exception;
            exception;
            throw exception;
            android.app.PendingIntent.CanceledException canceledexception;
            canceledexception;
            Log.w("SipService", "pendingIntent is canceled, drop incoming call");
            sipsessionimpl.endCall();
              goto _L4
        }

        public void openToReceiveCalls() throws SipException {
            mOpenedToReceiveCalls = true;
            if(mNetworkType != -1) {
                mSipGroup.openToReceiveCalls(this);
                mAutoRegistration.start(mSipGroup);
            }
        }

        public void setIncomingCallPendingIntent(PendingIntent pendingintent) {
            mIncomingCallPendingIntent = pendingintent;
        }

        public void setListener(ISipSessionListener isipsessionlistener) {
            mAutoRegistration.setListener(isipsessionlistener);
        }

        void setWakeupTimer(SipWakeupTimer sipwakeuptimer) {
            mSipGroup.setWakeupTimer(sipwakeuptimer);
        }

        private AutoRegistrationProcess mAutoRegistration;
        private PendingIntent mIncomingCallPendingIntent;
        private boolean mOpenedToReceiveCalls;
        private SipSessionGroup mSipGroup;
        final SipService this$0;

        public SipSessionGroupExt(SipProfile sipprofile, PendingIntent pendingintent, ISipSessionListener isipsessionlistener) throws SipException {
            this$0 = SipService.this;
            super();
            mAutoRegistration = new AutoRegistrationProcess();
            mSipGroup = new SipSessionGroup(duplicate(sipprofile), sipprofile.getPassword(), mTimer, mMyWakeLock);
            mIncomingCallPendingIntent = pendingintent;
            mAutoRegistration.setListener(isipsessionlistener);
        }
    }


    private SipService(Context context) {
        mNetworkType = -1;
        mExecutor = new MyExecutor();
        mSipGroups = new HashMap();
        mPendingSessions = new HashMap();
        mLastGoodKeepAliveInterval = 10;
        mContext = context;
        mConnectivityReceiver = new ConnectivityReceiver();
        mWifiLock = ((WifiManager)context.getSystemService("wifi")).createWifiLock(1, "SipService");
        mWifiLock.setReferenceCounted(false);
        mSipOnWifiOnly = SipManager.isSipWifiOnly(context);
        mMyWakeLock = new SipWakeLock((PowerManager)context.getSystemService("power"));
        mTimer = new SipWakeupTimer(context, mExecutor);
    }

    /**
     * @deprecated Method addPendingSession is deprecated
     */

    private void addPendingSession(ISipSession isipsession) {
        this;
        JVM INSTR monitorenter ;
        cleanUpPendingSessions();
        mPendingSessions.put(isipsession.getCallId(), isipsession);
_L2:
        this;
        JVM INSTR monitorexit ;
        return;
        RemoteException remoteexception;
        remoteexception;
        Log.e("SipService", "addPendingSession()", remoteexception);
        if(true) goto _L2; else goto _L1
_L1:
        Exception exception;
        exception;
        throw exception;
    }

    /**
     * @deprecated Method callingSelf is deprecated
     */

    private boolean callingSelf(SipSessionGroupExt sipsessiongroupext, SipSessionGroup.SipSessionImpl sipsessionimpl) {
        this;
        JVM INSTR monitorenter ;
        String s;
        Iterator iterator;
        s = sipsessionimpl.getCallId();
        iterator = mSipGroups.values().iterator();
_L4:
        if(!iterator.hasNext()) goto _L2; else goto _L1
_L1:
        SipSessionGroupExt sipsessiongroupext1 = (SipSessionGroupExt)iterator.next();
        if(sipsessiongroupext1 == sipsessiongroupext) goto _L4; else goto _L3
_L3:
        boolean flag1 = sipsessiongroupext1.containsSession(s);
        if(!flag1) goto _L4; else goto _L5
_L5:
        boolean flag = true;
_L7:
        this;
        JVM INSTR monitorexit ;
        return flag;
_L2:
        flag = false;
        if(true) goto _L7; else goto _L6
_L6:
        Exception exception;
        exception;
        throw exception;
    }

    private void cleanUpPendingSessions() throws RemoteException {
        java.util.Map.Entry aentry[] = (java.util.Map.Entry[])mPendingSessions.entrySet().toArray(new java.util.Map.Entry[mPendingSessions.size()]);
        int i = aentry.length;
        for(int j = 0; j < i; j++) {
            java.util.Map.Entry entry = aentry[j];
            if(((ISipSession)entry.getValue()).getState() != 3)
                mPendingSessions.remove(entry.getKey());
        }

    }

    private SipSessionGroupExt createGroup(SipProfile sipprofile) throws SipException {
        String s = sipprofile.getUriString();
        SipSessionGroupExt sipsessiongroupext = (SipSessionGroupExt)mSipGroups.get(s);
        if(sipsessiongroupext == null) {
            sipsessiongroupext = new SipSessionGroupExt(sipprofile, null, null);
            mSipGroups.put(s, sipsessiongroupext);
            notifyProfileAdded(sipprofile);
        } else
        if(!isCallerCreator(sipsessiongroupext))
            throw new SipException("only creator can access the profile");
        return sipsessiongroupext;
    }

    private SipSessionGroupExt createGroup(SipProfile sipprofile, PendingIntent pendingintent, ISipSessionListener isipsessionlistener) throws SipException {
        String s = sipprofile.getUriString();
        SipSessionGroupExt sipsessiongroupext = (SipSessionGroupExt)mSipGroups.get(s);
        if(sipsessiongroupext != null) {
            if(!isCallerCreator(sipsessiongroupext))
                throw new SipException("only creator can access the profile");
            sipsessiongroupext.setIncomingCallPendingIntent(pendingintent);
            sipsessiongroupext.setListener(isipsessionlistener);
        } else {
            sipsessiongroupext = new SipSessionGroupExt(sipprofile, pendingintent, isipsessionlistener);
            mSipGroups.put(s, sipsessiongroupext);
            notifyProfileAdded(sipprofile);
        }
        return sipsessiongroupext;
    }

    private static Looper createLooper() {
        HandlerThread handlerthread = new HandlerThread("SipService.Executor");
        handlerthread.start();
        return handlerthread.getLooper();
    }

    private String determineLocalIp() {
        String s1;
        DatagramSocket datagramsocket = new DatagramSocket();
        datagramsocket.connect(InetAddress.getByName("192.168.1.1"), 80);
        s1 = datagramsocket.getLocalAddress().getHostAddress();
        String s = s1;
_L2:
        return s;
        IOException ioexception;
        ioexception;
        s = null;
        if(true) goto _L2; else goto _L1
_L1:
    }

    private int getKeepAliveInterval() {
        int i;
        if(mKeepAliveInterval < 0)
            i = mLastGoodKeepAliveInterval;
        else
            i = mKeepAliveInterval;
        return i;
    }

    private boolean isBehindNAT(String s) {
        boolean flag = true;
        byte abyte0[] = InetAddress.getByName(s).getAddress();
        if(abyte0[0] == 10 || (0xff & abyte0[0]) == 172 && (0xf0 & abyte0[1]) == 16) goto _L2; else goto _L1
_L1:
        if((0xff & abyte0[0]) != 192) goto _L4; else goto _L3
_L3:
        byte byte0 = abyte0[1];
        if((byte0 & 0xff) != 168) goto _L4; else goto _L2
_L2:
        return flag;
        UnknownHostException unknownhostexception;
        unknownhostexception;
        Log.e("SipService", (new StringBuilder()).append("isBehindAT()").append(s).toString(), unknownhostexception);
_L4:
        flag = false;
        if(true) goto _L2; else goto _L5
_L5:
    }

    private boolean isCallerCreator(SipSessionGroupExt sipsessiongroupext) {
        boolean flag;
        if(sipsessiongroupext.getLocalProfile().getCallingUid() == Binder.getCallingUid())
            flag = true;
        else
            flag = false;
        return flag;
    }

    private boolean isCallerCreatorOrRadio(SipSessionGroupExt sipsessiongroupext) {
        boolean flag;
        if(isCallerRadio() || isCallerCreator(sipsessiongroupext))
            flag = true;
        else
            flag = false;
        return flag;
    }

    private boolean isCallerRadio() {
        boolean flag;
        if(Binder.getCallingUid() == 1001)
            flag = true;
        else
            flag = false;
        return flag;
    }

    private void notifyProfileAdded(SipProfile sipprofile) {
        Intent intent = new Intent("com.android.phone.SIP_ADD_PHONE");
        intent.putExtra("android:localSipUri", sipprofile.getUriString());
        mContext.sendBroadcast(intent);
        if(mSipGroups.size() == 1)
            registerReceivers();
    }

    private void notifyProfileRemoved(SipProfile sipprofile) {
        Intent intent = new Intent("com.android.phone.SIP_REMOVE_PHONE");
        intent.putExtra("android:localSipUri", sipprofile.getUriString());
        mContext.sendBroadcast(intent);
        if(mSipGroups.size() == 0)
            unregisterReceivers();
    }

    /**
     * @deprecated Method onConnectivityChanged is deprecated
     */

    private void onConnectivityChanged(NetworkInfo networkinfo) {
        this;
        JVM INSTR monitorenter ;
        if(networkinfo == null)
            break MISSING_BLOCK_LABEL_24;
        if(!networkinfo.isConnected() && networkinfo.getType() == mNetworkType)
            break MISSING_BLOCK_LABEL_41;
        networkinfo = ((ConnectivityManager)mContext.getSystemService("connectivity")).getActiveNetworkInfo();
        if(networkinfo == null || !networkinfo.isConnected()) goto _L2; else goto _L1
_L1:
        int i = networkinfo.getType();
_L5:
        int j;
        if(mSipOnWifiOnly && i != 1)
            i = -1;
        j = mNetworkType;
        if(j != i) goto _L4; else goto _L3
_L3:
        this;
        JVM INSTR monitorexit ;
        return;
_L2:
        i = -1;
          goto _L5
_L4:
        if(mNetworkType != -1) {
            mLocalIp = null;
            stopPortMappingMeasurement();
            for(Iterator iterator1 = mSipGroups.values().iterator(); iterator1.hasNext(); ((SipSessionGroupExt)iterator1.next()).onConnectivityChanged(false));
        }
        break MISSING_BLOCK_LABEL_175;
        SipException sipexception;
        sipexception;
        Log.e("SipService", "onConnectivityChanged()", sipexception);
          goto _L3
        Exception exception;
        exception;
        throw exception;
        mNetworkType = i;
        if(mNetworkType != -1) {
            mLocalIp = determineLocalIp();
            mKeepAliveInterval = -1;
            mLastGoodKeepAliveInterval = 10;
            for(Iterator iterator = mSipGroups.values().iterator(); iterator.hasNext(); ((SipSessionGroupExt)iterator.next()).onConnectivityChanged(true));
        }
        updateWakeLocks();
          goto _L3
    }

    /**
     * @deprecated Method onKeepAliveIntervalChanged is deprecated
     */

    private void onKeepAliveIntervalChanged() {
        this;
        JVM INSTR monitorenter ;
        for(Iterator iterator = mSipGroups.values().iterator(); iterator.hasNext(); ((SipSessionGroupExt)iterator.next()).onKeepAliveIntervalChanged());
        break MISSING_BLOCK_LABEL_46;
        Exception exception;
        exception;
        throw exception;
        this;
        JVM INSTR monitorexit ;
    }

    private void registerReceivers() {
        mContext.registerReceiver(mConnectivityReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }

    private void restartPortMappingLifetimeMeasurement(SipProfile sipprofile, int i) {
        stopPortMappingMeasurement();
        mKeepAliveInterval = -1;
        startPortMappingLifetimeMeasurement(sipprofile, i);
    }

    public static void start(Context context) {
        if(SipManager.isApiSupported(context)) {
            ServiceManager.addService("sip", new SipService(context));
            context.sendBroadcast(new Intent("android.net.sip.SIP_SERVICE_UP"));
        }
    }

    private void startPortMappingLifetimeMeasurement(SipProfile sipprofile) {
        startPortMappingLifetimeMeasurement(sipprofile, 120);
    }

    private void startPortMappingLifetimeMeasurement(SipProfile sipprofile, int i) {
        if(mIntervalMeasurementProcess == null && mKeepAliveInterval == -1 && isBehindNAT(mLocalIp)) {
            Log.d("SipService", (new StringBuilder()).append("start NAT port mapping timeout measurement on ").append(sipprofile.getUriString()).toString());
            int j = mLastGoodKeepAliveInterval;
            if(j >= i) {
                j = 10;
                mLastGoodKeepAliveInterval = j;
                Log.d("SipService", (new StringBuilder()).append("  reset min interval to ").append(j).toString());
            }
            mIntervalMeasurementProcess = new IntervalMeasurementProcess(sipprofile, j, i);
            mIntervalMeasurementProcess.start();
        }
    }

    private void stopPortMappingMeasurement() {
        if(mIntervalMeasurementProcess != null) {
            mIntervalMeasurementProcess.stop();
            mIntervalMeasurementProcess = null;
        }
    }

    private void unregisterReceivers() {
        mContext.unregisterReceiver(mConnectivityReceiver);
        mWifiLock.release();
        mNetworkType = -1;
    }

    private void updateWakeLocks() {
        Iterator iterator = mSipGroups.values().iterator();
_L4:
        if(!iterator.hasNext()) goto _L2; else goto _L1
_L1:
        if(!((SipSessionGroupExt)iterator.next()).isOpenedToReceiveCalls()) goto _L4; else goto _L3
_L3:
        if(mNetworkType == 1 || mNetworkType == -1)
            mWifiLock.acquire();
        else
            mWifiLock.release();
_L6:
        return;
_L2:
        mWifiLock.release();
        mMyWakeLock.reset();
        if(true) goto _L6; else goto _L5
_L5:
    }

    /**
     * @deprecated Method close is deprecated
     */

    public void close(String s) {
        this;
        JVM INSTR monitorenter ;
        SipSessionGroupExt sipsessiongroupext;
        mContext.enforceCallingOrSelfPermission("android.permission.USE_SIP", null);
        sipsessiongroupext = (SipSessionGroupExt)mSipGroups.get(s);
        if(sipsessiongroupext != null) goto _L2; else goto _L1
_L1:
        this;
        JVM INSTR monitorexit ;
        return;
_L2:
        if(isCallerCreatorOrRadio(sipsessiongroupext))
            break MISSING_BLOCK_LABEL_59;
        Log.w("SipService", "only creator or radio can close this profile");
          goto _L1
        Exception exception;
        exception;
        throw exception;
        SipSessionGroupExt sipsessiongroupext1 = (SipSessionGroupExt)mSipGroups.remove(s);
        notifyProfileRemoved(sipsessiongroupext1.getLocalProfile());
        sipsessiongroupext1.close();
        updateWakeLocks();
          goto _L1
    }

    /**
     * @deprecated Method createSession is deprecated
     */

    public ISipSession createSession(SipProfile sipprofile, ISipSessionListener isipsessionlistener) {
        ISipSession isipsession = null;
        this;
        JVM INSTR monitorenter ;
        int i;
        mContext.enforceCallingOrSelfPermission("android.permission.USE_SIP", null);
        sipprofile.setCallingUid(Binder.getCallingUid());
        i = mNetworkType;
        if(i != -1) goto _L2; else goto _L1
_L1:
        this;
        JVM INSTR monitorexit ;
        return isipsession;
_L2:
        ISipSession isipsession1 = createGroup(sipprofile).createSession(isipsessionlistener);
        isipsession = isipsession1;
        continue; /* Loop/switch isn't completed */
        SipException sipexception;
        sipexception;
        if(true) goto _L1; else goto _L3
_L3:
        Exception exception;
        exception;
        throw exception;
    }

    /**
     * @deprecated Method getListOfProfiles is deprecated
     */

    public SipProfile[] getListOfProfiles() {
        this;
        JVM INSTR monitorenter ;
        ArrayList arraylist;
        mContext.enforceCallingOrSelfPermission("android.permission.USE_SIP", null);
        boolean flag = isCallerRadio();
        arraylist = new ArrayList();
        Iterator iterator = mSipGroups.values().iterator();
        do {
            if(!iterator.hasNext())
                break;
            SipSessionGroupExt sipsessiongroupext = (SipSessionGroupExt)iterator.next();
            if(flag || isCallerCreator(sipsessiongroupext))
                arraylist.add(sipsessiongroupext.getLocalProfile());
        } while(true);
        break MISSING_BLOCK_LABEL_95;
        Exception exception;
        exception;
        throw exception;
        SipProfile asipprofile[] = (SipProfile[])arraylist.toArray(new SipProfile[arraylist.size()]);
        this;
        JVM INSTR monitorexit ;
        return asipprofile;
    }

    /**
     * @deprecated Method getPendingSession is deprecated
     */

    public ISipSession getPendingSession(String s) {
        ISipSession isipsession = null;
        this;
        JVM INSTR monitorenter ;
        mContext.enforceCallingOrSelfPermission("android.permission.USE_SIP", null);
        if(s != null) goto _L2; else goto _L1
_L1:
        this;
        JVM INSTR monitorexit ;
        return isipsession;
_L2:
        isipsession = (ISipSession)mPendingSessions.get(s);
        if(true) goto _L1; else goto _L3
_L3:
        Exception exception;
        exception;
        throw exception;
    }

    /**
     * @deprecated Method isOpened is deprecated
     */

    public boolean isOpened(String s) {
        boolean flag = false;
        this;
        JVM INSTR monitorenter ;
        SipSessionGroupExt sipsessiongroupext;
        mContext.enforceCallingOrSelfPermission("android.permission.USE_SIP", null);
        sipsessiongroupext = (SipSessionGroupExt)mSipGroups.get(s);
        if(sipsessiongroupext != null) goto _L2; else goto _L1
_L1:
        this;
        JVM INSTR monitorexit ;
        return flag;
_L2:
        if(isCallerCreatorOrRadio(sipsessiongroupext))
            flag = true;
        else
            Log.w("SipService", "only creator or radio can query on the profile");
        if(true) goto _L1; else goto _L3
_L3:
        Exception exception;
        exception;
        throw exception;
    }

    /**
     * @deprecated Method isRegistered is deprecated
     */

    public boolean isRegistered(String s) {
        boolean flag = false;
        this;
        JVM INSTR monitorenter ;
        SipSessionGroupExt sipsessiongroupext;
        mContext.enforceCallingOrSelfPermission("android.permission.USE_SIP", null);
        sipsessiongroupext = (SipSessionGroupExt)mSipGroups.get(s);
        if(sipsessiongroupext != null) goto _L2; else goto _L1
_L1:
        this;
        JVM INSTR monitorexit ;
        return flag;
_L2:
        if(isCallerCreatorOrRadio(sipsessiongroupext))
            flag = sipsessiongroupext.isRegistered();
        else
            Log.w("SipService", "only creator or radio can query on the profile");
        if(true) goto _L1; else goto _L3
_L3:
        Exception exception;
        exception;
        throw exception;
    }

    /**
     * @deprecated Method open is deprecated
     */

    public void open(SipProfile sipprofile) {
        this;
        JVM INSTR monitorenter ;
        mContext.enforceCallingOrSelfPermission("android.permission.USE_SIP", null);
        sipprofile.setCallingUid(Binder.getCallingUid());
        createGroup(sipprofile);
_L2:
        this;
        JVM INSTR monitorexit ;
        return;
        SipException sipexception;
        sipexception;
        Log.e("SipService", "openToMakeCalls()", sipexception);
        if(true) goto _L2; else goto _L1
_L1:
        Exception exception;
        exception;
        throw exception;
    }

    /**
     * @deprecated Method open3 is deprecated
     */

    public void open3(SipProfile sipprofile, PendingIntent pendingintent, ISipSessionListener isipsessionlistener) {
        this;
        JVM INSTR monitorenter ;
        mContext.enforceCallingOrSelfPermission("android.permission.USE_SIP", null);
        sipprofile.setCallingUid(Binder.getCallingUid());
        if(pendingintent != null) goto _L2; else goto _L1
_L1:
        Log.w("SipService", "incomingCallPendingIntent cannot be null; the profile is not opened");
_L4:
        this;
        JVM INSTR monitorexit ;
        return;
_L2:
        SipSessionGroupExt sipsessiongroupext = createGroup(sipprofile, pendingintent, isipsessionlistener);
        if(sipprofile.getAutoRegistration()) {
            sipsessiongroupext.openToReceiveCalls();
            updateWakeLocks();
        }
        continue; /* Loop/switch isn't completed */
        SipException sipexception;
        sipexception;
        Log.e("SipService", "openToReceiveCalls()", sipexception);
        if(true) goto _L4; else goto _L3
_L3:
        Exception exception;
        exception;
        throw exception;
    }

    /**
     * @deprecated Method setRegistrationListener is deprecated
     */

    public void setRegistrationListener(String s, ISipSessionListener isipsessionlistener) {
        this;
        JVM INSTR monitorenter ;
        SipSessionGroupExt sipsessiongroupext;
        mContext.enforceCallingOrSelfPermission("android.permission.USE_SIP", null);
        sipsessiongroupext = (SipSessionGroupExt)mSipGroups.get(s);
        if(sipsessiongroupext != null) goto _L2; else goto _L1
_L1:
        this;
        JVM INSTR monitorexit ;
        return;
_L2:
        if(!isCallerCreator(sipsessiongroupext))
            break MISSING_BLOCK_LABEL_59;
        sipsessiongroupext.setListener(isipsessionlistener);
          goto _L1
        Exception exception;
        exception;
        throw exception;
        Log.w("SipService", "only creator can set listener on the profile");
          goto _L1
    }

    static final boolean DEBUG = false;
    private static final int DEFAULT_KEEPALIVE_INTERVAL = 10;
    private static final int DEFAULT_MAX_KEEPALIVE_INTERVAL = 120;
    private static final int EXPIRY_TIME = 3600;
    private static final int MIN_EXPIRY_TIME = 60;
    private static final int SHORT_EXPIRY_TIME = 10;
    static final String TAG = "SipService";
    private ConnectivityReceiver mConnectivityReceiver;
    private Context mContext;
    private MyExecutor mExecutor;
    private IntervalMeasurementProcess mIntervalMeasurementProcess;
    private int mKeepAliveInterval;
    private int mLastGoodKeepAliveInterval;
    private String mLocalIp;
    private SipWakeLock mMyWakeLock;
    private int mNetworkType;
    private Map mPendingSessions;
    private Map mSipGroups;
    private boolean mSipOnWifiOnly;
    private SipWakeupTimer mTimer;
    private android.net.wifi.WifiManager.WifiLock mWifiLock;


/*
    static int access$1002(SipService sipservice, int i) {
        sipservice.mLastGoodKeepAliveInterval = i;
        return i;
    }

*/


















/*
    static int access$902(SipService sipservice, int i) {
        sipservice.mKeepAliveInterval = i;
        return i;
    }

*/
}
