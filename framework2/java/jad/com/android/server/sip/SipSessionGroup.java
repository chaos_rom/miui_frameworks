// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package com.android.server.sip;

import android.net.sip.*;
import android.text.TextUtils;
import android.util.Log;
import gov.nist.javax.sip.clientauthutils.AccountManager;
import gov.nist.javax.sip.clientauthutils.UserCredentials;
import gov.nist.javax.sip.header.*;
import gov.nist.javax.sip.header.extensions.ReferredByHeader;
import gov.nist.javax.sip.header.extensions.ReplacesHeader;
import gov.nist.javax.sip.message.SIPMessage;
import gov.nist.javax.sip.message.SIPResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.*;
import java.text.ParseException;
import java.util.*;
import javax.sip.*;
import javax.sip.address.Address;
import javax.sip.address.SipURI;
import javax.sip.header.*;
import javax.sip.message.*;

// Referenced classes of package com.android.server.sip:
//            SipHelper, SipWakeLock, SipWakeupTimer, SipSessionListenerProxy

class SipSessionGroup
    implements SipListener {
    static class KeepAliveProcessCallbackProxy
        implements KeepAliveProcessCallback {

        private void proxy(Runnable runnable) {
            (new Thread(runnable, "SIP-KeepAliveProcessCallbackThread")).start();
        }

        public void onError(final int errorCode, final String description) {
            if(mCallback != null)
                proxy(new Runnable() {

                    public void run() {
                        mCallback.onError(errorCode, description);
_L1:
                        return;
                        Throwable throwable;
                        throwable;
                        Log.w("SipSession", "onError", throwable);
                          goto _L1
                    }

                    final KeepAliveProcessCallbackProxy this$0;
                    final String val$description;
                    final int val$errorCode;

                 {
                    this$0 = KeepAliveProcessCallbackProxy.this;
                    errorCode = i;
                    description = s;
                    super();
                }
                });
        }

        public void onResponse(final boolean portChanged) {
            if(mCallback != null)
                proxy(new Runnable() {

                    public void run() {
                        mCallback.onResponse(portChanged);
_L1:
                        return;
                        Throwable throwable;
                        throwable;
                        Log.w("SipSession", "onResponse", throwable);
                          goto _L1
                    }

                    final KeepAliveProcessCallbackProxy this$0;
                    final boolean val$portChanged;

                 {
                    this$0 = KeepAliveProcessCallbackProxy.this;
                    portChanged = flag;
                    super();
                }
                });
        }

        private KeepAliveProcessCallback mCallback;


        KeepAliveProcessCallbackProxy(KeepAliveProcessCallback keepaliveprocesscallback) {
            mCallback = keepaliveprocesscallback;
        }
    }

    private class MakeCallCommand extends EventObject {

        public SipProfile getPeerProfile() {
            return (SipProfile)getSource();
        }

        public String getSessionDescription() {
            return mSessionDescription;
        }

        public int getTimeout() {
            return mTimeout;
        }

        private String mSessionDescription;
        private int mTimeout;
        final SipSessionGroup this$0;

        public MakeCallCommand(SipProfile sipprofile, String s) {
            this(sipprofile, s, -1);
        }

        public MakeCallCommand(SipProfile sipprofile, String s, int i) {
            this$0 = SipSessionGroup.this;
            super(sipprofile);
            mSessionDescription = s;
            mTimeout = i;
        }
    }

    private class RegisterCommand extends EventObject {

        public int getDuration() {
            return mDuration;
        }

        private int mDuration;
        final SipSessionGroup this$0;

        public RegisterCommand(int i) {
            this$0 = SipSessionGroup.this;
            super(SipSessionGroup.this);
            mDuration = i;
        }
    }

    class SipSessionImpl extends android.net.sip.ISipSession.Stub {
        class KeepAliveProcess extends SipSessionAdapter
            implements Runnable {

            private int getRPortFromResponse(Response response) {
                ViaHeader viaheader = (ViaHeader)(ViaHeader)response.getHeader("Via");
                int i;
                if(viaheader == null)
                    i = -1;
                else
                    i = viaheader.getRPort();
                return i;
            }

            private boolean parseOptionsResult(EventObject eventobject) {
                boolean flag = true;
                if(SipSessionGroup.expectResponse("OPTIONS", eventobject)) {
                    int i = getRPortFromResponse(((ResponseEvent)eventobject).getResponse());
                    if(i != -1) {
                        if(mRPort == 0)
                            mRPort = i;
                        if(mRPort != i) {
                            mPortChanged = flag;
                            mRPort = i;
                        }
                    }
                } else {
                    flag = false;
                }
                return flag;
            }

            private void sendKeepAlive() throws SipException, InterruptedException {
                SipSessionGroup sipsessiongroup = _fld0;
                sipsessiongroup;
                JVM INSTR monitorenter ;
                mState = 9;
                mClientTransaction = mSipHelper.sendOptions(mLocalProfile, mPeerProfile, generateTag());
                mDialog = mClientTransaction.getDialog();
                addSipSession(SipSessionImpl.this);
                startSessionTimer(5);
                return;
            }

            public void onError(ISipSession isipsession, int i, String s) {
                stop();
                mCallback.onError(i, s);
            }

            boolean process(EventObject eventobject) throws SipException {
                boolean flag;
                if(mRunning && mState == 9 && (eventobject instanceof ResponseEvent) && parseOptionsResult(eventobject)) {
                    if(mPortChanged) {
                        resetExternalAddress();
                        stop();
                    } else {
                        cancelSessionTimer();
                        removeSipSession(SipSessionImpl.this);
                    }
                    mCallback.onResponse(mPortChanged);
                    flag = true;
                } else {
                    flag = false;
                }
                return flag;
            }

            public void run() {
                SipSessionGroup sipsessiongroup = _fld0;
                sipsessiongroup;
                JVM INSTR monitorenter ;
                if(mRunning) goto _L2; else goto _L1
_L1:
                return;
_L2:
                sendKeepAlive();
_L4:
                sipsessiongroup;
                JVM INSTR monitorexit ;
                  goto _L1
                Exception exception;
                exception;
                throw exception;
                Throwable throwable;
                throwable;
                if(!mRunning) goto _L4; else goto _L3
_L3:
                SipSessionImpl.this.onError(throwable);
                  goto _L4
            }

            void start(int i, KeepAliveProcessCallback keepaliveprocesscallback) {
                if(!mRunning) {
                    mRunning = true;
                    mInterval = i;
                    mCallback = new KeepAliveProcessCallbackProxy(keepaliveprocesscallback);
                    mWakeupTimer.set(i * 1000, this);
                    run();
                }
            }

            void stop() {
                SipSessionGroup sipsessiongroup = _fld0;
                sipsessiongroup;
                JVM INSTR monitorenter ;
                mRunning = false;
                mWakeupTimer.cancel(this);
                reset();
                return;
            }

            private static final String TAG = "SipKeepAlive";
            private KeepAliveProcessCallback mCallback;
            private int mInterval;
            private boolean mPortChanged;
            private int mRPort;
            private boolean mRunning;
            final SipSessionImpl this$1;

            KeepAliveProcess() {
                this$1 = SipSessionImpl.this;
                super();
                mRunning = false;
                mPortChanged = false;
                mRPort = 0;
            }
        }

        class SessionTimer {

            /**
             * @deprecated Method sleep is deprecated
             */

            private void sleep(int i) {
                this;
                JVM INSTR monitorenter ;
                long l = i * 1000;
                wait(l);
_L2:
                this;
                JVM INSTR monitorexit ;
                return;
                InterruptedException interruptedexception;
                interruptedexception;
                Log.e("SipSession", "session timer interrupted!");
                if(true) goto _L2; else goto _L1
_L1:
                Exception exception;
                exception;
                throw exception;
            }

            private void timeout() {
                SipSessionGroup sipsessiongroup = _fld0;
                sipsessiongroup;
                JVM INSTR monitorenter ;
                onError(-5, "Session timed out!");
                return;
            }

            /**
             * @deprecated Method cancel is deprecated
             */

            void cancel() {
                this;
                JVM INSTR monitorenter ;
                mRunning = false;
                notify();
                this;
                JVM INSTR monitorexit ;
                return;
                Exception exception;
                exception;
                throw exception;
            }

            void start(final int timeout) {
                (new Thread(new Runnable() {

                    public void run() {
                        sleep(timeout);
                        if(mRunning)
                            SessionTimer.this.timeout();
                    }

                    final SessionTimer this$2;
                    final int val$timeout;

                     {
                        this$2 = SessionTimer.this;
                        timeout = i;
                        super();
                    }
                }, "SipSessionTimerThread")).start();
            }

            private boolean mRunning;
            final SipSessionImpl this$1;




            SessionTimer() {
                this$1 = SipSessionImpl.this;
                super();
                mRunning = true;
            }
        }


        private void cancelSessionTimer() {
            if(mSessionTimer != null) {
                mSessionTimer.cancel();
                mSessionTimer = null;
            }
        }

        private String createErrorMessage(Response response) {
            Object aobj[] = new Object[2];
            aobj[0] = response.getReasonPhrase();
            aobj[1] = Integer.valueOf(response.getStatusCode());
            return String.format("%s (%d)", aobj);
        }

        private boolean crossDomainAuthenticationRequired(Response response) {
            String s = getRealmFromResponse(response);
            if(s == null)
                s = "";
            boolean flag;
            if(!mLocalProfile.getSipDomain().trim().equals(s.trim()))
                flag = true;
            else
                flag = false;
            return flag;
        }

        private void doCommandAsync(final EventObject command) {
            (new Thread(new Runnable() {

                public void run() {
                    processCommand(command);
_L1:
                    return;
                    Throwable throwable;
                    throwable;
                    Log.w("SipSession", (new StringBuilder()).append("command error: ").append(command).append(": ").append(mLocalProfile.getUriString()).toString(), getRootCause(throwable));
                    onError(throwable);
                      goto _L1
                }

                final SipSessionImpl this$1;
                final EventObject val$command;

                 {
                    this$1 = SipSessionImpl.this;
                    command = eventobject;
                    super();
                }
            }, "SipSessionAsyncCmdThread")).start();
        }

        private void enableKeepAlive() {
            if(mKeepAliveSession != null)
                mKeepAliveSession.stopKeepAliveProcess();
            else
                mKeepAliveSession = duplicate();
            mKeepAliveSession.startKeepAliveProcess(10, mPeerProfile, null);
_L1:
            return;
            SipException sipexception;
            sipexception;
            Log.w("SipSession", "keepalive cannot be enabled; ignored", sipexception);
            mKeepAliveSession.stopKeepAliveProcess();
              goto _L1
        }

        private void endCallNormally() {
            reset();
            mProxy.onCallEnded(this);
        }

        private void endCallOnBusy() {
            reset();
            mProxy.onCallBusy(this);
        }

        private void endCallOnError(int i, String s) {
            reset();
            mProxy.onError(this, i, s);
        }

        private boolean endingCall(EventObject eventobject) throws SipException {
            boolean flag = true;
            if(!SipSessionGroup.expectResponse("BYE", eventobject)) goto _L2; else goto _L1
_L1:
            ResponseEvent responseevent = (ResponseEvent)eventobject;
            responseevent.getResponse().getStatusCode();
            JVM INSTR lookupswitch 2: default 52
        //                       401: 62
        //                       407: 62;
               goto _L3 _L4 _L4
_L3:
            cancelSessionTimer();
            reset();
_L5:
            return flag;
_L4:
            if(!handleAuthentication(responseevent)) goto _L3; else goto _L5
_L2:
            flag = false;
              goto _L5
        }

        private void establishCall(boolean flag) {
            mState = 8;
            cancelSessionTimer();
            if(!mInCall && flag)
                enableKeepAlive();
            mInCall = true;
            mProxy.onCallEstablished(this, mPeerSessionDescription);
        }

        private AccountManager getAccountManager() {
            return new AccountManager() {

                public UserCredentials getCredentials(ClientTransaction clienttransaction, String s) {
                    return new UserCredentials() {

                        public String getPassword() {
                            return mPassword;
                        }

                        public String getSipDomain() {
                            return mLocalProfile.getSipDomain();
                        }

                        public String getUserName() {
                            String s1 = mLocalProfile.getAuthUserName();
                            if(TextUtils.isEmpty(s1))
                                s1 = mLocalProfile.getUserName();
                            return s1;
                        }

                        final _cls2 this$2;

                         {
                            this$2 = _cls2.this;
                            super();
                        }
                    };
                }

                final SipSessionImpl this$1;

                 {
                    this$1 = SipSessionImpl.this;
                    super();
                }
            };
        }

        private int getErrorCode(int i) {
            i;
            JVM INSTR lookupswitch 10: default 92
        //                       403: 104
        //                       404: 104
        //                       406: 104
        //                       408: 116
        //                       410: 104
        //                       414: 110
        //                       480: 104
        //                       484: 110
        //                       485: 110
        //                       488: 104;
               goto _L1 _L2 _L2 _L2 _L3 _L2 _L4 _L2 _L4 _L4 _L2
_L1:
            byte byte0;
            if(i < 500)
                byte0 = -4;
            else
                byte0 = -2;
_L6:
            return byte0;
_L2:
            byte0 = -7;
            continue; /* Loop/switch isn't completed */
_L4:
            byte0 = -6;
            continue; /* Loop/switch isn't completed */
_L3:
            byte0 = -5;
            if(true) goto _L6; else goto _L5
_L5:
        }

        private int getErrorCode(Throwable throwable) {
            throwable.getMessage();
            byte byte0;
            if(throwable instanceof UnknownHostException)
                byte0 = -12;
            else
            if(throwable instanceof IOException)
                byte0 = -1;
            else
                byte0 = -4;
            return byte0;
        }

        private int getExpiryTime(Response response) {
            int i = -1;
            ContactHeader contactheader = (ContactHeader)response.getHeader("Contact");
            if(contactheader != null)
                i = contactheader.getExpires();
            ExpiresHeader expiresheader = (ExpiresHeader)response.getHeader("Expires");
            if(expiresheader != null && (i < 0 || i > expiresheader.getExpires()))
                i = expiresheader.getExpires();
            if(i <= 0)
                i = 3600;
            ExpiresHeader expiresheader1 = (ExpiresHeader)response.getHeader("Min-Expires");
            if(expiresheader1 != null && i < expiresheader1.getExpires())
                i = expiresheader1.getExpires();
            return i;
        }

        private String getNonceFromResponse(Response response) {
            WWWAuthenticate wwwauthenticate = (WWWAuthenticate)response.getHeader("WWW-Authenticate");
            String s;
            if(wwwauthenticate != null) {
                s = wwwauthenticate.getNonce();
            } else {
                ProxyAuthenticate proxyauthenticate = (ProxyAuthenticate)response.getHeader("Proxy-Authenticate");
                if(proxyauthenticate == null)
                    s = null;
                else
                    s = proxyauthenticate.getNonce();
            }
            return s;
        }

        private String getRealmFromResponse(Response response) {
            WWWAuthenticate wwwauthenticate = (WWWAuthenticate)response.getHeader("WWW-Authenticate");
            String s;
            if(wwwauthenticate != null) {
                s = wwwauthenticate.getRealm();
            } else {
                ProxyAuthenticate proxyauthenticate = (ProxyAuthenticate)response.getHeader("Proxy-Authenticate");
                if(proxyauthenticate == null)
                    s = null;
                else
                    s = proxyauthenticate.getRealm();
            }
            return s;
        }

        private String getResponseString(int i) {
            StatusLine statusline = new StatusLine();
            statusline.setStatusCode(i);
            statusline.setReasonPhrase(SIPResponse.getReasonPhrase(i));
            return statusline.encode();
        }

        private Transaction getTransaction() {
            Object obj;
            if(mClientTransaction != null)
                obj = mClientTransaction;
            else
            if(mServerTransaction != null)
                obj = mServerTransaction;
            else
                obj = null;
            return ((Transaction) (obj));
        }

        private boolean handleAuthentication(ResponseEvent responseevent) throws SipException {
            boolean flag = false;
            Response response = responseevent.getResponse();
            if(getNonceFromResponse(response) == null)
                onError(-2, "server does not provide challenge");
            else
            if(mAuthenticationRetryCount < 2) {
                mClientTransaction = mSipHelper.handleChallenge(responseevent, getAccountManager());
                mDialog = mClientTransaction.getDialog();
                mAuthenticationRetryCount = 1 + mAuthenticationRetryCount;
                if(SipSessionGroup.isLoggable(this, responseevent))
                    Log.d("SipSession", (new StringBuilder()).append("   authentication retry count=").append(mAuthenticationRetryCount).toString());
                flag = true;
            } else
            if(crossDomainAuthenticationRequired(response))
                onError(-11, getRealmFromResponse(response));
            else
                onError(-8, "incorrect username or password");
            return flag;
        }

        private boolean inCall(EventObject eventobject) throws SipException {
            boolean flag;
            if(SipSessionGroup.END_CALL == eventobject) {
                mState = 10;
                mSipHelper.sendBye(mDialog);
                mProxy.onCallEnded(this);
                startSessionTimer(3);
                flag = true;
            } else
            if(SipSessionGroup.isRequestEvent("INVITE", eventobject)) {
                mState = 3;
                RequestEvent requestevent = (RequestEvent)eventobject;
                mInviteReceived = requestevent;
                mPeerSessionDescription = extractContent(requestevent.getRequest());
                mServerTransaction = null;
                mProxy.onRinging(this, mPeerProfile, mPeerSessionDescription);
                flag = true;
            } else
            if(SipSessionGroup.isRequestEvent("BYE", eventobject)) {
                mSipHelper.sendResponse((RequestEvent)eventobject, 200);
                endCallNormally();
                flag = true;
            } else
            if(SipSessionGroup.isRequestEvent("REFER", eventobject))
                flag = processReferRequest((RequestEvent)eventobject);
            else
            if(eventobject instanceof MakeCallCommand) {
                mState = 5;
                mClientTransaction = mSipHelper.sendReinvite(mDialog, ((MakeCallCommand)eventobject).getSessionDescription());
                startSessionTimer(((MakeCallCommand)eventobject).getTimeout());
                flag = true;
            } else
            if((eventobject instanceof ResponseEvent) && SipSessionGroup.expectResponse("NOTIFY", eventobject))
                flag = true;
            else
                flag = false;
            return flag;
        }

        private boolean incomingCall(EventObject eventobject) throws SipException {
            boolean flag;
            if(eventobject instanceof MakeCallCommand) {
                mState = 4;
                mServerTransaction = mSipHelper.sendInviteOk(mInviteReceived, mLocalProfile, ((MakeCallCommand)eventobject).getSessionDescription(), mServerTransaction, mExternalIp, mExternalPort);
                startSessionTimer(((MakeCallCommand)eventobject).getTimeout());
                flag = true;
            } else
            if(SipSessionGroup.END_CALL == eventobject) {
                mSipHelper.sendInviteBusyHere(mInviteReceived, mServerTransaction);
                endCallNormally();
                flag = true;
            } else
            if(SipSessionGroup.isRequestEvent("CANCEL", eventobject)) {
                RequestEvent requestevent = (RequestEvent)eventobject;
                mSipHelper.sendResponse(requestevent, 200);
                mSipHelper.sendInviteRequestTerminated(mInviteReceived.getRequest(), mServerTransaction);
                endCallNormally();
                flag = true;
            } else {
                flag = false;
            }
            return flag;
        }

        private boolean incomingCallToInCall(EventObject eventobject) throws SipException {
            boolean flag = true;
            if(!SipSessionGroup.isRequestEvent("ACK", eventobject)) goto _L2; else goto _L1
_L1:
            String s = extractContent(((RequestEvent)eventobject).getRequest());
            if(s != null)
                mPeerSessionDescription = s;
            if(mPeerSessionDescription == null)
                onError(-4, "peer sdp is empty");
            else
                establishCall(false);
_L4:
            return flag;
_L2:
            if(!SipSessionGroup.isRequestEvent("CANCEL", eventobject))
                flag = false;
            if(true) goto _L4; else goto _L3
_L3:
        }

        private boolean isCurrentTransaction(TransactionTerminatedEvent transactionterminatedevent) {
            Object obj;
            boolean flag = true;
            Object obj1;
            if(transactionterminatedevent.isServerTransaction())
                obj = mServerTransaction;
            else
                obj = mClientTransaction;
            if(transactionterminatedevent.isServerTransaction())
                obj1 = transactionterminatedevent.getServerTransaction();
            else
                obj1 = transactionterminatedevent.getClientTransaction();
            if(obj == obj1 || mState == 9) goto _L2; else goto _L1
_L1:
            Log.d("SipSession", (new StringBuilder()).append("not the current transaction; current=").append(toString(((Transaction) (obj)))).append(", target=").append(toString(((Transaction) (obj1)))).toString());
            flag = false;
_L4:
            return flag;
_L2:
            if(obj != null)
                Log.d("SipSession", (new StringBuilder()).append("transaction terminated: ").append(toString(((Transaction) (obj)))).toString());
            if(true) goto _L4; else goto _L3
_L3:
        }

        private void onError(int i, String s) {
            cancelSessionTimer();
            mState;
            JVM INSTR tableswitch 1 2: default 32
        //                       1 39
        //                       2 39;
               goto _L1 _L2 _L2
_L1:
            endCallOnError(i, s);
_L4:
            return;
_L2:
            onRegistrationFailed(i, s);
            if(true) goto _L4; else goto _L3
_L3:
        }

        private void onError(Throwable throwable) {
            Throwable throwable1 = getRootCause(throwable);
            onError(getErrorCode(throwable1), throwable1.toString());
        }

        private void onError(Response response) {
            int i = response.getStatusCode();
            if(!mInCall && i == 486)
                endCallOnBusy();
            else
                onError(getErrorCode(i), createErrorMessage(response));
        }

        private void onRegistrationDone(int i) {
            reset();
            mProxy.onRegistrationDone(this, i);
        }

        private void onRegistrationFailed(int i, String s) {
            reset();
            mProxy.onRegistrationFailed(this, i, s);
        }

        private void onRegistrationFailed(Throwable throwable) {
            Throwable throwable1 = getRootCause(throwable);
            onRegistrationFailed(getErrorCode(throwable1), throwable1.toString());
        }

        private void onRegistrationFailed(Response response) {
            onRegistrationFailed(getErrorCode(response.getStatusCode()), createErrorMessage(response));
        }

        private boolean outgoingCall(EventObject eventobject) throws SipException {
            boolean flag = true;
            if(!SipSessionGroup.expectResponse("INVITE", eventobject)) goto _L2; else goto _L1
_L1:
            ResponseEvent responseevent;
            Response response;
            int i;
            responseevent = (ResponseEvent)eventobject;
            response = responseevent.getResponse();
            i = response.getStatusCode();
            i;
            JVM INSTR lookupswitch 8: default 112
        //                       180: 159
        //                       181: 159
        //                       182: 159
        //                       183: 159
        //                       200: 188
        //                       401: 261
        //                       407: 261
        //                       491: 157;
               goto _L3 _L4 _L4 _L4 _L4 _L5 _L6 _L6 _L7
_L3:
            if(mReferSession != null)
                mSipHelper.sendReferNotify(mReferSession.mDialog, getResponseString(503));
            if(i < 400) goto _L9; else goto _L8
_L8:
            onError(response);
_L7:
            return flag;
_L4:
            if(mState == 5) {
                mState = 6;
                cancelSessionTimer();
                mProxy.onRingingBack(this);
            }
            continue; /* Loop/switch isn't completed */
_L5:
            if(mReferSession != null) {
                mSipHelper.sendReferNotify(mReferSession.mDialog, getResponseString(200));
                mReferSession = null;
            }
            mSipHelper.sendInviteAck(responseevent, mDialog);
            mPeerSessionDescription = extractContent(response);
            establishCall(flag);
            continue; /* Loop/switch isn't completed */
_L6:
            if(handleAuthentication(responseevent))
                addSipSession(this);
            continue; /* Loop/switch isn't completed */
_L9:
            if(i >= 300)
                flag = false;
            continue; /* Loop/switch isn't completed */
_L2:
            if(SipSessionGroup.END_CALL == eventobject) {
                mState = 7;
                mSipHelper.sendCancel(mClientTransaction);
                startSessionTimer(3);
            } else
            if(SipSessionGroup.isRequestEvent("INVITE", eventobject)) {
                RequestEvent requestevent = (RequestEvent)eventobject;
                mSipHelper.sendInviteBusyHere(requestevent, requestevent.getServerTransaction());
            } else {
                flag = false;
            }
            if(true) goto _L7; else goto _L10
_L10:
        }

        private boolean outgoingCallToReady(EventObject eventobject) throws SipException {
            boolean flag = true;
            if(!(eventobject instanceof ResponseEvent)) goto _L2; else goto _L1
_L1:
            Response response;
            int i;
            response = ((ResponseEvent)eventobject).getResponse();
            i = response.getStatusCode();
            if(!SipSessionGroup.expectResponse("CANCEL", eventobject)) goto _L4; else goto _L3
_L3:
            if(i != 200) goto _L6; else goto _L5
_L5:
            return flag;
_L4:
            if(!SipSessionGroup.expectResponse("INVITE", eventobject)) goto _L8; else goto _L7
_L7:
            i;
            JVM INSTR lookupswitch 2: default 84
        //                       200: 100
        //                       487: 109;
               goto _L6 _L9 _L10
_L6:
            if(i >= 400) {
                onError(response);
                continue; /* Loop/switch isn't completed */
            }
              goto _L11
_L9:
            outgoingCall(eventobject);
            continue; /* Loop/switch isn't completed */
_L10:
            endCallNormally();
            continue; /* Loop/switch isn't completed */
_L8:
            flag = false;
            continue; /* Loop/switch isn't completed */
_L2:
            if(eventobject instanceof TransactionTerminatedEvent)
                onError(new SipException("timed out"));
_L11:
            flag = false;
            if(true) goto _L5; else goto _L12
_L12:
        }

        private void processCommand(EventObject eventobject) throws SipException {
            if(SipSessionGroup.isLoggable(eventobject))
                Log.d("SipSession", (new StringBuilder()).append("process cmd: ").append(eventobject).toString());
            if(!process(eventobject))
                onError(-9, (new StringBuilder()).append("cannot initiate a new transaction to execute: ").append(eventobject).toString());
        }

        private void processDialogTerminated(DialogTerminatedEvent dialogterminatedevent) {
            if(mDialog == dialogterminatedevent.getDialog())
                onError(new SipException("dialog terminated"));
            else
                Log.d("SipSession", (new StringBuilder()).append("not the current dialog; current=").append(mDialog).append(", terminated=").append(dialogterminatedevent.getDialog()).toString());
        }

        private boolean processExceptions(EventObject eventobject) throws SipException {
            if(!SipSessionGroup.isRequestEvent("BYE", eventobject)) goto _L2; else goto _L1
_L1:
            boolean flag;
            mSipHelper.sendResponse((RequestEvent)eventobject, 200);
            endCallNormally();
            flag = true;
_L4:
            return flag;
_L2:
            if(SipSessionGroup.isRequestEvent("CANCEL", eventobject)) {
                mSipHelper.sendResponse((RequestEvent)eventobject, 481);
                flag = true;
                continue; /* Loop/switch isn't completed */
            }
            if(eventobject instanceof TransactionTerminatedEvent) {
                if(isCurrentTransaction((TransactionTerminatedEvent)eventobject)) {
                    if(eventobject instanceof TimeoutEvent)
                        processTimeout((TimeoutEvent)eventobject);
                    else
                        processTransactionTerminated((TransactionTerminatedEvent)eventobject);
                    flag = true;
                    continue; /* Loop/switch isn't completed */
                }
            } else {
                if(SipSessionGroup.isRequestEvent("OPTIONS", eventobject)) {
                    mSipHelper.sendResponse((RequestEvent)eventobject, 200);
                    flag = true;
                    continue; /* Loop/switch isn't completed */
                }
                if(eventobject instanceof DialogTerminatedEvent) {
                    processDialogTerminated((DialogTerminatedEvent)eventobject);
                    flag = true;
                    continue; /* Loop/switch isn't completed */
                }
            }
            flag = false;
            if(true) goto _L4; else goto _L3
_L3:
        }

        private boolean processReferRequest(RequestEvent requestevent) throws SipException {
            boolean flag;
label0:
            {
                flag = false;
                try {
                    ReferToHeader refertoheader = (ReferToHeader)requestevent.getRequest().getHeader("Refer-To");
                    SipURI sipuri = (SipURI)refertoheader.getAddress().getURI();
                    String s = sipuri.getHeader("Replaces");
                    if(sipuri.getUser() == null) {
                        mSipHelper.sendResponse(requestevent, 400);
                        break label0;
                    }
                    mSipHelper.sendResponse(requestevent, 202);
                    SipSessionImpl sipsessionimpl = createNewSession(requestevent, mProxy.getListener(), mSipHelper.getServerTransaction(requestevent), 0);
                    sipsessionimpl.mReferSession = this;
                    sipsessionimpl.mReferredBy = (ReferredByHeader)requestevent.getRequest().getHeader("Referred-By");
                    sipsessionimpl.mReplaces = s;
                    sipsessionimpl.mPeerProfile = SipSessionGroup.createPeerProfile(refertoheader);
                    sipsessionimpl.mProxy.onCallTransferring(sipsessionimpl, null);
                }
                catch(IllegalArgumentException illegalargumentexception) {
                    throw new SipException("createPeerProfile()", illegalargumentexception);
                }
                flag = true;
            }
            return flag;
        }

        private void processTimeout(TimeoutEvent timeoutevent) {
            Log.d("SipSession", "processing Timeout...");
            mState;
            JVM INSTR tableswitch 1 7: default 56
        //                       1 66
        //                       2 66
        //                       3 81
        //                       4 81
        //                       5 81
        //                       6 56
        //                       7 81;
               goto _L1 _L2 _L2 _L3 _L3 _L3 _L1 _L3
_L1:
            Log.d("SipSession", "   do nothing");
_L5:
            return;
_L2:
            reset();
            mProxy.onRegistrationTimeout(this);
            continue; /* Loop/switch isn't completed */
_L3:
            onError(-5, timeoutevent.toString());
            if(true) goto _L5; else goto _L4
_L4:
        }

        private void processTransactionTerminated(TransactionTerminatedEvent transactionterminatedevent) {
            mState;
            JVM INSTR lookupswitch 2: default 32
        //                       0: 68
        //                       8: 68;
               goto _L1 _L2 _L2
_L1:
            Log.d("SipSession", (new StringBuilder()).append("Transaction terminated early: ").append(this).toString());
            onError(-3, "transaction terminated");
_L4:
            return;
_L2:
            Log.d("SipSession", "Transaction terminated; do nothing");
            if(true) goto _L4; else goto _L3
_L3:
        }

        private boolean readyForCall(EventObject eventobject) throws SipException {
            boolean flag = false;
            if(!(eventobject instanceof MakeCallCommand)) goto _L2; else goto _L1
_L1:
            mState = 5;
            MakeCallCommand makecallcommand = (MakeCallCommand)eventobject;
            mPeerProfile = makecallcommand.getPeerProfile();
            if(mReferSession != null)
                mSipHelper.sendReferNotify(mReferSession.mDialog, getResponseString(100));
            mClientTransaction = mSipHelper.sendInvite(mLocalProfile, mPeerProfile, makecallcommand.getSessionDescription(), generateTag(), mReferredBy, mReplaces);
            mDialog = mClientTransaction.getDialog();
            addSipSession(this);
            startSessionTimer(makecallcommand.getTimeout());
            mProxy.onCalling(this);
            flag = true;
_L4:
            return flag;
_L2:
            if(eventobject instanceof RegisterCommand) {
                mState = 1;
                int i = ((RegisterCommand)eventobject).getDuration();
                mClientTransaction = mSipHelper.sendRegister(mLocalProfile, generateTag(), i);
                mDialog = mClientTransaction.getDialog();
                addSipSession(this);
                mProxy.onRegistering(this);
                flag = true;
            } else
            if(SipSessionGroup.DEREGISTER == eventobject) {
                mState = 2;
                mClientTransaction = mSipHelper.sendRegister(mLocalProfile, generateTag(), 0);
                mDialog = mClientTransaction.getDialog();
                addSipSession(this);
                mProxy.onRegistering(this);
                flag = true;
            }
            if(true) goto _L4; else goto _L3
_L3:
        }

        private boolean registeringToReady(EventObject eventobject) throws SipException {
            if(!SipSessionGroup.expectResponse("REGISTER", eventobject)) goto _L2; else goto _L1
_L1:
            ResponseEvent responseevent;
            Response response;
            int i;
            responseevent = (ResponseEvent)eventobject;
            response = responseevent.getResponse();
            i = response.getStatusCode();
            i;
            JVM INSTR lookupswitch 3: default 68
        //                       200: 86
        //                       401: 125
        //                       407: 125;
               goto _L3 _L4 _L5 _L5
_L3:
            if(i < 500) goto _L2; else goto _L6
_L6:
            boolean flag;
            onRegistrationFailed(response);
            flag = true;
_L8:
            return flag;
_L4:
            int j;
            if(mState == 1)
                j = getExpiryTime(((ResponseEvent)eventobject).getResponse());
            else
                j = -1;
            onRegistrationDone(j);
            flag = true;
            continue; /* Loop/switch isn't completed */
_L5:
            handleAuthentication(responseevent);
            flag = true;
            continue; /* Loop/switch isn't completed */
_L2:
            flag = false;
            if(true) goto _L8; else goto _L7
_L7:
        }

        private void reset() {
            mInCall = false;
            removeSipSession(this);
            mPeerProfile = null;
            mState = 0;
            mInviteReceived = null;
            mPeerSessionDescription = null;
            mAuthenticationRetryCount = 0;
            mReferSession = null;
            mReferredBy = null;
            mReplaces = null;
            if(mDialog != null)
                mDialog.delete();
            mDialog = null;
            try {
                if(mServerTransaction != null)
                    mServerTransaction.terminate();
            }
            catch(ObjectInUseException objectinuseexception) { }
            mServerTransaction = null;
            try {
                if(mClientTransaction != null)
                    mClientTransaction.terminate();
            }
            catch(ObjectInUseException objectinuseexception1) { }
            mClientTransaction = null;
            cancelSessionTimer();
            if(mKeepAliveSession != null) {
                mKeepAliveSession.stopKeepAliveProcess();
                mKeepAliveSession = null;
            }
        }

        private void startSessionTimer(int i) {
            if(i > 0) {
                mSessionTimer = new SessionTimer();
                mSessionTimer.start(i);
            }
        }

        private String toString(Transaction transaction) {
            String s;
            if(transaction == null) {
                s = "null";
            } else {
                Request request = transaction.getRequest();
                Dialog dialog = transaction.getDialog();
                CSeqHeader cseqheader = (CSeqHeader)request.getHeader("CSeq");
                Object aobj[] = new Object[4];
                aobj[0] = request.getMethod();
                aobj[1] = Long.valueOf(cseqheader.getSeqNumber());
                aobj[2] = transaction.getState();
                Object obj;
                if(dialog == null)
                    obj = "-";
                else
                    obj = dialog.getState();
                aobj[3] = obj;
                s = String.format("req=%s,%s,s=%s,ds=%s,", aobj);
            }
            return s;
        }

        public void answerCall(String s, int i) {
            SipSessionGroup sipsessiongroup = SipSessionGroup.this;
            sipsessiongroup;
            JVM INSTR monitorenter ;
            if(mPeerProfile != null)
                doCommandAsync(new MakeCallCommand(mPeerProfile, s, i));
            return;
        }

        public void changeCall(String s, int i) {
            SipSessionGroup sipsessiongroup = SipSessionGroup.this;
            sipsessiongroup;
            JVM INSTR monitorenter ;
            if(mPeerProfile != null)
                doCommandAsync(new MakeCallCommand(mPeerProfile, s, i));
            return;
        }

        SipSessionImpl duplicate() {
            return new SipSessionImpl(mProxy.getListener());
        }

        public void endCall() {
            doCommandAsync(SipSessionGroup.END_CALL);
        }

        protected String generateTag() {
            return String.valueOf((long)(4294967296D * Math.random()));
        }

        public String getCallId() {
            return SipHelper.getCallId(getTransaction());
        }

        public String getLocalIp() {
            return mLocalIp;
        }

        public SipProfile getLocalProfile() {
            return mLocalProfile;
        }

        public SipProfile getPeerProfile() {
            return mPeerProfile;
        }

        public int getState() {
            return mState;
        }

        public boolean isInCall() {
            return mInCall;
        }

        public void makeCall(SipProfile sipprofile, String s, int i) {
            doCommandAsync(new MakeCallCommand(sipprofile, s, i));
        }

        public boolean process(EventObject eventobject) throws SipException {
            if(SipSessionGroup.isLoggable(this, eventobject))
                Log.d("SipSession", (new StringBuilder()).append(" ~~~~~   ").append(this).append(": ").append(android.net.sip.SipSession.State.toString(mState)).append(": processing ").append(SipSessionGroup.log(eventobject)).toString());
            SipSessionGroup sipsessiongroup = SipSessionGroup.this;
            sipsessiongroup;
            JVM INSTR monitorenter ;
            if(!isClosed()) goto _L2; else goto _L1
_L1:
            boolean flag2 = false;
              goto _L3
_L2:
            if(mKeepAliveProcess == null || !mKeepAliveProcess.process(eventobject)) goto _L5; else goto _L4
_L4:
            flag2 = true;
              goto _L3
_L5:
            Dialog dialog = null;
            if(!(eventobject instanceof RequestEvent)) goto _L7; else goto _L6
_L6:
            dialog = ((RequestEvent)eventobject).getDialog();
_L18:
            if(dialog != null)
                mDialog = dialog;
            mState;
            JVM INSTR tableswitch 0 10: default 357
        //                       0 274
        //                       1 264
        //                       2 264
        //                       3 284
        //                       4 294
        //                       5 304
        //                       6 304
        //                       7 314
        //                       8 324
        //                       9 357
        //                       10 334;
               goto _L8 _L9 _L10 _L10 _L11 _L12 _L13 _L13 _L14 _L15 _L8 _L16
_L8:
            boolean flag1;
            break MISSING_BLOCK_LABEL_357;
_L19:
            Exception exception;
            boolean flag;
            if(!flag1 && !processExceptions(eventobject))
                flag2 = false;
            else
                flag2 = true;
              goto _L3
            exception;
            throw exception;
_L7:
            if(!(eventobject instanceof ResponseEvent)) goto _L18; else goto _L17
_L17:
            dialog = ((ResponseEvent)eventobject).getDialog();
            extractExternalAddress((ResponseEvent)eventobject);
              goto _L18
_L10:
            flag1 = registeringToReady(eventobject);
              goto _L19
_L9:
            flag1 = readyForCall(eventobject);
              goto _L19
_L11:
            flag1 = incomingCall(eventobject);
              goto _L19
_L12:
            flag1 = incomingCallToInCall(eventobject);
              goto _L19
_L13:
            flag1 = outgoingCall(eventobject);
              goto _L19
_L14:
            flag1 = outgoingCallToReady(eventobject);
              goto _L19
_L15:
            flag1 = inCall(eventobject);
              goto _L19
_L16:
            flag = endingCall(eventobject);
            flag1 = flag;
              goto _L19
_L3:
            return flag2;
            flag1 = false;
              goto _L19
        }

        public void register(int i) {
            doCommandAsync(new RegisterCommand(i));
        }

        public void setListener(ISipSessionListener isipsessionlistener) {
            SipSessionListenerProxy sipsessionlistenerproxy = mProxy;
            if(isipsessionlistener instanceof SipSessionListenerProxy)
                isipsessionlistener = ((SipSessionListenerProxy)isipsessionlistener).getListener();
            sipsessionlistenerproxy.setListener(isipsessionlistener);
        }

        public void startKeepAliveProcess(int i, SipProfile sipprofile, KeepAliveProcessCallback keepaliveprocesscallback) throws SipException {
            SipSessionGroup sipsessiongroup = SipSessionGroup.this;
            sipsessiongroup;
            JVM INSTR monitorenter ;
            if(mKeepAliveProcess != null)
                throw new SipException("Cannot create more than one keepalive process in a SipSession");
            break MISSING_BLOCK_LABEL_35;
            Exception exception;
            exception;
            throw exception;
            mPeerProfile = sipprofile;
            mKeepAliveProcess = new KeepAliveProcess();
            mProxy.setListener(mKeepAliveProcess);
            mKeepAliveProcess.start(i, keepaliveprocesscallback);
            sipsessiongroup;
            JVM INSTR monitorexit ;
        }

        public void startKeepAliveProcess(int i, KeepAliveProcessCallback keepaliveprocesscallback) throws SipException {
            SipSessionGroup sipsessiongroup = SipSessionGroup.this;
            sipsessiongroup;
            JVM INSTR monitorenter ;
            startKeepAliveProcess(i, mLocalProfile, keepaliveprocesscallback);
            return;
        }

        public void stopKeepAliveProcess() {
            SipSessionGroup sipsessiongroup = SipSessionGroup.this;
            sipsessiongroup;
            JVM INSTR monitorenter ;
            if(mKeepAliveProcess != null) {
                mKeepAliveProcess.stop();
                mKeepAliveProcess = null;
            }
            return;
        }

        public String toString() {
            String s2;
            String s1 = super.toString();
            s2 = (new StringBuilder()).append(s1.substring(s1.indexOf("@"))).append(":").append(android.net.sip.SipSession.State.toString(mState)).toString();
            String s = s2;
_L2:
            return s;
            Throwable throwable;
            throwable;
            s = super.toString();
            if(true) goto _L2; else goto _L1
_L1:
        }

        public void unregister() {
            doCommandAsync(SipSessionGroup.DEREGISTER);
        }

        int mAuthenticationRetryCount;
        ClientTransaction mClientTransaction;
        Dialog mDialog;
        boolean mInCall;
        RequestEvent mInviteReceived;
        private KeepAliveProcess mKeepAliveProcess;
        private SipSessionImpl mKeepAliveSession;
        SipProfile mPeerProfile;
        String mPeerSessionDescription;
        SipSessionListenerProxy mProxy;
        SipSessionImpl mReferSession;
        ReferredByHeader mReferredBy;
        String mReplaces;
        ServerTransaction mServerTransaction;
        SessionTimer mSessionTimer;
        int mState;
        final SipSessionGroup this$0;







        public SipSessionImpl(ISipSessionListener isipsessionlistener) {
            this$0 = SipSessionGroup.this;
            super();
            mProxy = new SipSessionListenerProxy();
            mState = 0;
            setListener(isipsessionlistener);
        }
    }

    static interface KeepAliveProcessCallback {

        public abstract void onError(int i, String s);

        public abstract void onResponse(boolean flag);
    }

    private class SipSessionCallReceiverImpl extends SipSessionImpl {

        private int processInviteWithReplaces(RequestEvent requestevent, ReplacesHeader replacesheader) {
            char c;
            SipSessionImpl sipsessionimpl;
            c = '\u01E1';
            String s = replacesheader.getCallId();
            sipsessionimpl = (SipSessionImpl)mSessionMap.get(s);
            if(sipsessionimpl != null) goto _L2; else goto _L1
_L1:
            return c;
_L2:
            Dialog dialog = sipsessionimpl.mDialog;
            if(dialog == null)
                c = '\u025B';
            else
            if(dialog.getLocalTag().equals(replacesheader.getToTag()) && dialog.getRemoteTag().equals(replacesheader.getFromTag())) {
                ReferredByHeader referredbyheader = (ReferredByHeader)requestevent.getRequest().getHeader("Referred-By");
                if(referredbyheader != null && dialog.getRemoteParty().equals(referredbyheader.getAddress()))
                    c = '\310';
            }
            if(true) goto _L1; else goto _L3
_L3:
        }

        private void processNewInviteRequest(RequestEvent requestevent) throws SipException {
            ReplacesHeader replacesheader = (ReplacesHeader)requestevent.getRequest().getHeader("Replaces");
            SipSessionImpl sipsessionimpl = null;
            if(replacesheader != null) {
                int i = processInviteWithReplaces(requestevent, replacesheader);
                if(i == 200) {
                    SipSessionImpl sipsessionimpl1 = (SipSessionImpl)mSessionMap.get(replacesheader.getCallId());
                    sipsessionimpl = createNewSession(requestevent, sipsessionimpl1.mProxy.getListener(), mSipHelper.getServerTransaction(requestevent), 3);
                    sipsessionimpl.mProxy.onCallTransferring(sipsessionimpl, sipsessionimpl.mPeerSessionDescription);
                } else {
                    mSipHelper.sendResponse(requestevent, i);
                }
            } else {
                sipsessionimpl = createNewSession(requestevent, super.mProxy, mSipHelper.sendRinging(requestevent, generateTag()), 3);
                super.mProxy.onRinging(sipsessionimpl, sipsessionimpl.mPeerProfile, sipsessionimpl.mPeerSessionDescription);
            }
            if(sipsessionimpl != null)
                addSipSession(sipsessionimpl);
        }

        public boolean process(EventObject eventobject) throws SipException {
            boolean flag = true;
            if(SipSessionGroup.isLoggable(this, eventobject))
                Log.d("SipSession", (new StringBuilder()).append(" ~~~~~   ").append(this).append(": ").append(android.net.sip.SipSession.State.toString(super.mState)).append(": processing ").append(SipSessionGroup.log(eventobject)).toString());
            if(SipSessionGroup.isRequestEvent("INVITE", eventobject))
                processNewInviteRequest((RequestEvent)eventobject);
            else
            if(SipSessionGroup.isRequestEvent("OPTIONS", eventobject))
                mSipHelper.sendResponse((RequestEvent)eventobject, 200);
            else
                flag = false;
            return flag;
        }

        final SipSessionGroup this$0;

        public SipSessionCallReceiverImpl(ISipSessionListener isipsessionlistener) {
            this$0 = SipSessionGroup.this;
            super(isipsessionlistener);
        }
    }


    public SipSessionGroup(SipProfile sipprofile, String s, SipWakeupTimer sipwakeuptimer, SipWakeLock sipwakelock) throws SipException {
        mSessionMap = new HashMap();
        mLocalProfile = sipprofile;
        mPassword = s;
        mWakeupTimer = sipwakeuptimer;
        mWakeLock = sipwakelock;
        reset();
    }

    /**
     * @deprecated Method addSipSession is deprecated
     */

    private void addSipSession(SipSessionImpl sipsessionimpl) {
        this;
        JVM INSTR monitorenter ;
        removeSipSession(sipsessionimpl);
        String s = sipsessionimpl.getCallId();
        mSessionMap.put(s, sipsessionimpl);
        if(isLoggable(sipsessionimpl)) {
            Log.d("SipSession", (new StringBuilder()).append("+++  add a session with key:  '").append(s).append("'").toString());
            String s1;
            for(Iterator iterator = mSessionMap.keySet().iterator(); iterator.hasNext(); Log.d("SipSession", (new StringBuilder()).append("  ").append(s1).append(": ").append(mSessionMap.get(s1)).toString()))
                s1 = (String)iterator.next();

        }
        break MISSING_BLOCK_LABEL_154;
        Exception exception;
        exception;
        throw exception;
        this;
        JVM INSTR monitorexit ;
    }

    private SipSessionImpl createNewSession(RequestEvent requestevent, ISipSessionListener isipsessionlistener, ServerTransaction servertransaction, int i) throws SipException {
        SipSessionImpl sipsessionimpl = new SipSessionImpl(isipsessionlistener);
        sipsessionimpl.mServerTransaction = servertransaction;
        sipsessionimpl.mState = i;
        sipsessionimpl.mDialog = sipsessionimpl.mServerTransaction.getDialog();
        sipsessionimpl.mInviteReceived = requestevent;
        sipsessionimpl.mPeerProfile = createPeerProfile((HeaderAddress)requestevent.getRequest().getHeader("From"));
        sipsessionimpl.mPeerSessionDescription = extractContent(requestevent.getRequest());
        return sipsessionimpl;
    }

    private static SipProfile createPeerProfile(HeaderAddress headeraddress) throws SipException {
        SipProfile sipprofile;
        try {
            Address address = headeraddress.getAddress();
            SipURI sipuri = (SipURI)address.getURI();
            String s = sipuri.getUser();
            if(s == null)
                s = "anonymous";
            int i = sipuri.getPort();
            android.net.sip.SipProfile.Builder builder = (new android.net.sip.SipProfile.Builder(s, sipuri.getHost())).setDisplayName(address.getDisplayName());
            if(i > 0)
                builder.setPort(i);
            sipprofile = builder.build();
        }
        catch(IllegalArgumentException illegalargumentexception) {
            throw new SipException("createPeerProfile()", illegalargumentexception);
        }
        catch(ParseException parseexception) {
            throw new SipException("createPeerProfile()", parseexception);
        }
        return sipprofile;
    }

    private static boolean expectResponse(int i, String s, EventObject eventobject) {
        if(!(eventobject instanceof ResponseEvent)) goto _L2; else goto _L1
_L1:
        Response response = ((ResponseEvent)eventobject).getResponse();
        if(response.getStatusCode() != i) goto _L2; else goto _L3
_L3:
        boolean flag = s.equalsIgnoreCase(getCseqMethod(response));
_L5:
        return flag;
_L2:
        flag = false;
        if(true) goto _L5; else goto _L4
_L4:
    }

    private static boolean expectResponse(String s, EventObject eventobject) {
        boolean flag;
        if(eventobject instanceof ResponseEvent)
            flag = s.equalsIgnoreCase(getCseqMethod(((ResponseEvent)eventobject).getResponse()));
        else
            flag = false;
        return flag;
    }

    private String extractContent(Message message) {
        String s;
label0:
        {
            byte abyte0[] = message.getRawContent();
            if(abyte0 != null)
                try {
                    if(message instanceof SIPMessage)
                        s = ((SIPMessage)message).getMessageContent();
                    else
                        s = new String(abyte0, "UTF-8");
                    break label0;
                }
                catch(UnsupportedEncodingException unsupportedencodingexception) { }
            s = null;
        }
        return s;
    }

    private void extractExternalAddress(ResponseEvent responseevent) {
        ViaHeader viaheader = (ViaHeader)(ViaHeader)responseevent.getResponse().getHeader("Via");
        if(viaheader != null) goto _L2; else goto _L1
_L1:
        return;
_L2:
        int i = viaheader.getRPort();
        String s = viaheader.getReceived();
        if(i > 0 && s != null) {
            mExternalIp = s;
            mExternalPort = i;
        }
        if(true) goto _L1; else goto _L3
_L3:
    }

    private static String getCseqMethod(Message message) {
        return ((CSeqHeader)message.getHeader("CSeq")).getMethod();
    }

    private Throwable getRootCause(Throwable throwable) {
        for(Throwable throwable1 = throwable.getCause(); throwable1 != null; throwable1 = throwable.getCause())
            throwable = throwable1;

        return throwable;
    }

    /**
     * @deprecated Method getSipSession is deprecated
     */

    private SipSessionImpl getSipSession(EventObject eventobject) {
        this;
        JVM INSTR monitorenter ;
        SipSessionImpl sipsessionimpl;
        String s = SipHelper.getCallId(eventobject);
        sipsessionimpl = (SipSessionImpl)mSessionMap.get(s);
        if(sipsessionimpl != null && isLoggable(sipsessionimpl)) {
            Log.d("SipSession", (new StringBuilder()).append("session key from event: ").append(s).toString());
            Log.d("SipSession", "active sessions:");
            String s1;
            for(Iterator iterator = mSessionMap.keySet().iterator(); iterator.hasNext(); Log.d("SipSession", (new StringBuilder()).append(" ...").append(s1).append(": ").append(mSessionMap.get(s1)).toString()))
                s1 = (String)iterator.next();

        }
        break MISSING_BLOCK_LABEL_163;
        Exception exception;
        exception;
        throw exception;
        if(sipsessionimpl == null)
            break MISSING_BLOCK_LABEL_173;
_L1:
        this;
        JVM INSTR monitorexit ;
        return sipsessionimpl;
        sipsessionimpl = mCallReceiverSession;
          goto _L1
    }

    private String getStackName() {
        return (new StringBuilder()).append("stack").append(System.currentTimeMillis()).toString();
    }

    private static boolean isLoggable(SipSessionImpl sipsessionimpl) {
        if(sipsessionimpl != null)
            switch(sipsessionimpl.mState) {
            }
        return false;
    }

    private static boolean isLoggable(SipSessionImpl sipsessionimpl, EventObject eventobject) {
        if(isLoggable(sipsessionimpl) && eventobject != null)
            if(eventobject instanceof ResponseEvent) {
                if(!"OPTIONS".equals(((ResponseEvent)eventobject).getResponse().getHeader("CSeq")));
            } else
            if(!(eventobject instanceof RequestEvent) || !isRequestEvent("OPTIONS", eventobject));
        return false;
    }

    private static boolean isLoggable(EventObject eventobject) {
        return isLoggable(null, eventobject);
    }

    private static boolean isRequestEvent(String s, EventObject eventobject) {
        if(!(eventobject instanceof RequestEvent)) goto _L2; else goto _L1
_L1:
        boolean flag1 = s.equals(((RequestEvent)eventobject).getRequest().getMethod());
        boolean flag = flag1;
_L4:
        return flag;
        Throwable throwable;
        throwable;
_L2:
        flag = false;
        if(true) goto _L4; else goto _L3
_L3:
    }

    private static String log(EventObject eventobject) {
        String s;
        if(eventobject instanceof RequestEvent)
            s = ((RequestEvent)eventobject).getRequest().toString();
        else
        if(eventobject instanceof ResponseEvent)
            s = ((ResponseEvent)eventobject).getResponse().toString();
        else
            s = eventobject.toString();
        return s;
    }

    /**
     * @deprecated Method process is deprecated
     */

    private void process(EventObject eventobject) {
        this;
        JVM INSTR monitorenter ;
        SipSessionImpl sipsessionimpl = getSipSession(eventobject);
        boolean flag = isLoggable(sipsessionimpl, eventobject);
        if(sipsessionimpl == null || !sipsessionimpl.process(eventobject)) goto _L2; else goto _L1
_L1:
        boolean flag1 = true;
_L3:
        if(flag && flag1)
            Log.d("SipSession", (new StringBuilder()).append("new state after: ").append(android.net.sip.SipSession.State.toString(sipsessionimpl.mState)).toString());
_L4:
        this;
        JVM INSTR monitorexit ;
        return;
_L2:
        flag1 = false;
          goto _L3
        Throwable throwable;
        throwable;
        Log.w("SipSession", (new StringBuilder()).append("event process error: ").append(eventobject).toString(), getRootCause(throwable));
        sipsessionimpl.onError(throwable);
          goto _L4
        Exception exception;
        exception;
        throw exception;
          goto _L3
    }

    /**
     * @deprecated Method removeSipSession is deprecated
     */

    private void removeSipSession(SipSessionImpl sipsessionimpl) {
        this;
        JVM INSTR monitorenter ;
        SipSessionImpl sipsessionimpl1 = mCallReceiverSession;
        if(sipsessionimpl != sipsessionimpl1) goto _L2; else goto _L1
_L1:
        this;
        JVM INSTR monitorexit ;
        return;
_L2:
        String s;
        SipSessionImpl sipsessionimpl2;
        s = sipsessionimpl.getCallId();
        sipsessionimpl2 = (SipSessionImpl)mSessionMap.remove(s);
        if(sipsessionimpl2 == null || sipsessionimpl2 == sipsessionimpl)
            continue; /* Loop/switch isn't completed */
        Log.w("SipSession", (new StringBuilder()).append("session ").append(sipsessionimpl).append(" is not associated with key '").append(s).append("'").toString());
        mSessionMap.put(s, sipsessionimpl2);
        Iterator iterator1 = mSessionMap.entrySet().iterator();
        do {
            if(!iterator1.hasNext())
                continue; /* Loop/switch isn't completed */
            java.util.Map.Entry entry = (java.util.Map.Entry)iterator1.next();
            if(entry.getValue() == sipsessionimpl2) {
                s = (String)entry.getKey();
                mSessionMap.remove(s);
            }
        } while(true);
        Exception exception;
        exception;
        throw exception;
        if(sipsessionimpl2 == null) goto _L1; else goto _L3
_L3:
        if(!isLoggable(sipsessionimpl2)) goto _L1; else goto _L4
_L4:
        Log.d("SipSession", (new StringBuilder()).append("remove session ").append(sipsessionimpl).append(" @key '").append(s).append("'").toString());
        Iterator iterator = mSessionMap.keySet().iterator();
        while(iterator.hasNext())  {
            String s1 = (String)iterator.next();
            Log.d("SipSession", (new StringBuilder()).append("  ").append(s1).append(": ").append(mSessionMap.get(s1)).toString());
        }
          goto _L1
    }

    /**
     * @deprecated Method close is deprecated
     */

    public void close() {
        this;
        JVM INSTR monitorenter ;
        Log.d("SipSession", (new StringBuilder()).append(" close stack for ").append(mLocalProfile.getUriString()).toString());
        onConnectivityChanged();
        mSessionMap.clear();
        closeToNotReceiveCalls();
        if(mSipStack != null) {
            mSipStack.stop();
            mSipStack = null;
            mSipHelper = null;
        }
        resetExternalAddress();
        this;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        throw exception;
    }

    /**
     * @deprecated Method closeToNotReceiveCalls is deprecated
     */

    public void closeToNotReceiveCalls() {
        this;
        JVM INSTR monitorenter ;
        mCallReceiverSession = null;
        this;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        throw exception;
    }

    /**
     * @deprecated Method containsSession is deprecated
     */

    boolean containsSession(String s) {
        this;
        JVM INSTR monitorenter ;
        boolean flag = mSessionMap.containsKey(s);
        this;
        JVM INSTR monitorexit ;
        return flag;
        Exception exception;
        exception;
        throw exception;
    }

    public ISipSession createSession(ISipSessionListener isipsessionlistener) {
        Object obj;
        if(isClosed())
            obj = null;
        else
            obj = new SipSessionImpl(isipsessionlistener);
        return ((ISipSession) (obj));
    }

    public SipProfile getLocalProfile() {
        return mLocalProfile;
    }

    public String getLocalProfileUri() {
        return mLocalProfile.getUriString();
    }

    /**
     * @deprecated Method isClosed is deprecated
     */

    public boolean isClosed() {
        this;
        JVM INSTR monitorenter ;
        SipStack sipstack = mSipStack;
        boolean flag;
        if(sipstack == null)
            flag = true;
        else
            flag = false;
        this;
        JVM INSTR monitorexit ;
        return flag;
        Exception exception;
        exception;
        throw exception;
    }

    /**
     * @deprecated Method onConnectivityChanged is deprecated
     */

    void onConnectivityChanged() {
        this;
        JVM INSTR monitorenter ;
        SipSessionImpl asipsessionimpl[];
        int i;
        int j;
        asipsessionimpl = (SipSessionImpl[])mSessionMap.values().toArray(new SipSessionImpl[mSessionMap.size()]);
        i = asipsessionimpl.length;
        j = 0;
_L1:
        if(j >= i)
            break MISSING_BLOCK_LABEL_62;
        asipsessionimpl[j].onError(-10, "data connection lost");
        j++;
          goto _L1
        this;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        throw exception;
    }

    /**
     * @deprecated Method openToReceiveCalls is deprecated
     */

    public void openToReceiveCalls(ISipSessionListener isipsessionlistener) {
        this;
        JVM INSTR monitorenter ;
        if(mCallReceiverSession != null) goto _L2; else goto _L1
_L1:
        mCallReceiverSession = new SipSessionCallReceiverImpl(isipsessionlistener);
_L4:
        this;
        JVM INSTR monitorexit ;
        return;
_L2:
        mCallReceiverSession.setListener(isipsessionlistener);
        if(true) goto _L4; else goto _L3
_L3:
        Exception exception;
        exception;
        throw exception;
    }

    public void processDialogTerminated(DialogTerminatedEvent dialogterminatedevent) {
        process(dialogterminatedevent);
    }

    public void processIOException(IOExceptionEvent ioexceptionevent) {
        process(ioexceptionevent);
    }

    public void processRequest(RequestEvent requestevent) {
        if(isRequestEvent("INVITE", requestevent))
            mWakeLock.acquire(500L);
        process(requestevent);
    }

    public void processResponse(ResponseEvent responseevent) {
        process(responseevent);
    }

    public void processTimeout(TimeoutEvent timeoutevent) {
        process(timeoutevent);
    }

    public void processTransactionTerminated(TransactionTerminatedEvent transactionterminatedevent) {
        process(transactionterminatedevent);
    }

    /**
     * @deprecated Method reset is deprecated
     */

    void reset() throws SipException {
        this;
        JVM INSTR monitorenter ;
        Properties properties;
        String s;
        int i;
        String s1;
        properties = new Properties();
        s = mLocalProfile.getProtocol();
        i = mLocalProfile.getPort();
        s1 = mLocalProfile.getProxyAddress();
        if(TextUtils.isEmpty(s1)) goto _L2; else goto _L1
_L1:
        properties.setProperty("javax.sip.OUTBOUND_PROXY", (new StringBuilder()).append(s1).append(':').append(i).append('/').append(s).toString());
_L11:
        if(!s1.startsWith("[") || !s1.endsWith("]")) goto _L4; else goto _L3
_L3:
        String s4 = s1.substring(1, -1 + s1.length());
        s1 = s4;
_L4:
        String s3 = null;
        InetAddress ainetaddress[];
        int j;
        int k;
        ainetaddress = InetAddress.getAllByName(s1);
        j = ainetaddress.length;
        k = 0;
_L12:
        if(k >= j) goto _L6; else goto _L5
_L5:
        DatagramSocket datagramsocket;
        InetAddress inetaddress = ainetaddress[k];
        datagramsocket = new DatagramSocket();
        datagramsocket.connect(inetaddress, i);
        if(!datagramsocket.isConnected()) goto _L8; else goto _L7
_L7:
        s3 = datagramsocket.getLocalAddress().getHostAddress();
        i = datagramsocket.getLocalPort();
        datagramsocket.close();
_L6:
        if(s3 != null) goto _L10; else goto _L9
_L9:
        this;
        JVM INSTR monitorexit ;
        return;
_L2:
        String s2 = mLocalProfile.getSipDomain();
        s1 = s2;
          goto _L11
_L8:
        datagramsocket.close();
        k++;
          goto _L12
_L10:
        close();
        mLocalIp = s3;
        properties.setProperty("javax.sip.STACK_NAME", getStackName());
        properties.setProperty("gov.nist.javax.sip.THREAD_POOL_SIZE", "1");
        mSipStack = SipFactory.getInstance().createSipStack(properties);
        SipProvider sipprovider = mSipStack.createSipProvider(mSipStack.createListeningPoint(s3, i, s));
        sipprovider.addSipListener(this);
        mSipHelper = new SipHelper(mSipStack, sipprovider);
        Log.d("SipSession", (new StringBuilder()).append(" start stack for ").append(mLocalProfile.getUriString()).toString());
        mSipStack.start();
          goto _L9
        Exception exception;
        exception;
        throw exception;
        SipException sipexception;
        sipexception;
        throw sipexception;
        Exception exception2;
        exception2;
        throw new SipException("failed to initialize SIP stack", exception2);
        Exception exception1;
        exception1;
          goto _L6
    }

    /**
     * @deprecated Method resetExternalAddress is deprecated
     */

    void resetExternalAddress() {
        this;
        JVM INSTR monitorenter ;
        mExternalIp = null;
        mExternalPort = 0;
        this;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        throw exception;
    }

    void setWakeupTimer(SipWakeupTimer sipwakeuptimer) {
        mWakeupTimer = sipwakeuptimer;
    }

    private static final String ANONYMOUS = "anonymous";
    private static final int CANCEL_CALL_TIMER = 3;
    private static final EventObject CONTINUE_CALL = new EventObject("Continue call");
    private static final boolean DEBUG = false;
    private static final boolean DEBUG_PING = false;
    private static final EventObject DEREGISTER = new EventObject("Deregister");
    private static final EventObject END_CALL = new EventObject("End call");
    private static final int END_CALL_TIMER = 3;
    private static final int EXPIRY_TIME = 3600;
    private static final EventObject HOLD_CALL = new EventObject("Hold call");
    private static final int INCALL_KEEPALIVE_INTERVAL = 10;
    private static final int KEEPALIVE_TIMEOUT = 5;
    private static final String TAG = "SipSession";
    private static final String THREAD_POOL_SIZE = "1";
    private static final long WAKE_LOCK_HOLDING_TIME = 500L;
    private SipSessionImpl mCallReceiverSession;
    private String mExternalIp;
    private int mExternalPort;
    private String mLocalIp;
    private final SipProfile mLocalProfile;
    private final String mPassword;
    private Map mSessionMap;
    private SipHelper mSipHelper;
    private SipStack mSipStack;
    private SipWakeLock mWakeLock;
    private SipWakeupTimer mWakeupTimer;























}
