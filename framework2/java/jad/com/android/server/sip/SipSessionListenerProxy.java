// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package com.android.server.sip;

import android.net.sip.*;
import android.os.DeadObjectException;
import android.util.Log;

class SipSessionListenerProxy extends android.net.sip.ISipSessionListener.Stub {

    SipSessionListenerProxy() {
    }

    private void handle(Throwable throwable, String s) {
        if(!(throwable instanceof DeadObjectException)) goto _L2; else goto _L1
_L1:
        mListener = null;
_L4:
        return;
_L2:
        if(mListener != null)
            Log.w("SipSession", s, throwable);
        if(true) goto _L4; else goto _L3
_L3:
    }

    private void proxy(Runnable runnable) {
        (new Thread(runnable, "SipSessionCallbackThread")).start();
    }

    public ISipSessionListener getListener() {
        return mListener;
    }

    public void onCallBusy(final ISipSession session) {
        if(mListener != null)
            proxy(new Runnable() {

                public void run() {
                    mListener.onCallBusy(session);
_L1:
                    return;
                    Throwable throwable;
                    throwable;
                    handle(throwable, "onCallBusy()");
                      goto _L1
                }

                final SipSessionListenerProxy this$0;
                final ISipSession val$session;

             {
                this$0 = SipSessionListenerProxy.this;
                session = isipsession;
                super();
            }
            });
    }

    public void onCallChangeFailed(final ISipSession session, final int errorCode, final String message) {
        if(mListener != null)
            proxy(new Runnable() {

                public void run() {
                    mListener.onCallChangeFailed(session, errorCode, message);
_L1:
                    return;
                    Throwable throwable;
                    throwable;
                    handle(throwable, "onCallChangeFailed()");
                      goto _L1
                }

                final SipSessionListenerProxy this$0;
                final int val$errorCode;
                final String val$message;
                final ISipSession val$session;

             {
                this$0 = SipSessionListenerProxy.this;
                session = isipsession;
                errorCode = i;
                message = s;
                super();
            }
            });
    }

    public void onCallEnded(final ISipSession session) {
        if(mListener != null)
            proxy(new Runnable() {

                public void run() {
                    mListener.onCallEnded(session);
_L1:
                    return;
                    Throwable throwable;
                    throwable;
                    handle(throwable, "onCallEnded()");
                      goto _L1
                }

                final SipSessionListenerProxy this$0;
                final ISipSession val$session;

             {
                this$0 = SipSessionListenerProxy.this;
                session = isipsession;
                super();
            }
            });
    }

    public void onCallEstablished(final ISipSession session, final String sessionDescription) {
        if(mListener != null)
            proxy(new Runnable() {

                public void run() {
                    mListener.onCallEstablished(session, sessionDescription);
_L1:
                    return;
                    Throwable throwable;
                    throwable;
                    handle(throwable, "onCallEstablished()");
                      goto _L1
                }

                final SipSessionListenerProxy this$0;
                final ISipSession val$session;
                final String val$sessionDescription;

             {
                this$0 = SipSessionListenerProxy.this;
                session = isipsession;
                sessionDescription = s;
                super();
            }
            });
    }

    public void onCallTransferring(final ISipSession newSession, final String sessionDescription) {
        if(mListener != null)
            proxy(new Runnable() {

                public void run() {
                    mListener.onCallTransferring(newSession, sessionDescription);
_L1:
                    return;
                    Throwable throwable;
                    throwable;
                    handle(throwable, "onCallTransferring()");
                      goto _L1
                }

                final SipSessionListenerProxy this$0;
                final ISipSession val$newSession;
                final String val$sessionDescription;

             {
                this$0 = SipSessionListenerProxy.this;
                newSession = isipsession;
                sessionDescription = s;
                super();
            }
            });
    }

    public void onCalling(final ISipSession session) {
        if(mListener != null)
            proxy(new Runnable() {

                public void run() {
                    mListener.onCalling(session);
_L1:
                    return;
                    Throwable throwable;
                    throwable;
                    handle(throwable, "onCalling()");
                      goto _L1
                }

                final SipSessionListenerProxy this$0;
                final ISipSession val$session;

             {
                this$0 = SipSessionListenerProxy.this;
                session = isipsession;
                super();
            }
            });
    }

    public void onError(final ISipSession session, final int errorCode, final String message) {
        if(mListener != null)
            proxy(new Runnable() {

                public void run() {
                    mListener.onError(session, errorCode, message);
_L1:
                    return;
                    Throwable throwable;
                    throwable;
                    handle(throwable, "onError()");
                      goto _L1
                }

                final SipSessionListenerProxy this$0;
                final int val$errorCode;
                final String val$message;
                final ISipSession val$session;

             {
                this$0 = SipSessionListenerProxy.this;
                session = isipsession;
                errorCode = i;
                message = s;
                super();
            }
            });
    }

    public void onRegistering(final ISipSession session) {
        if(mListener != null)
            proxy(new Runnable() {

                public void run() {
                    mListener.onRegistering(session);
_L1:
                    return;
                    Throwable throwable;
                    throwable;
                    handle(throwable, "onRegistering()");
                      goto _L1
                }

                final SipSessionListenerProxy this$0;
                final ISipSession val$session;

             {
                this$0 = SipSessionListenerProxy.this;
                session = isipsession;
                super();
            }
            });
    }

    public void onRegistrationDone(final ISipSession session, final int duration) {
        if(mListener != null)
            proxy(new Runnable() {

                public void run() {
                    mListener.onRegistrationDone(session, duration);
_L1:
                    return;
                    Throwable throwable;
                    throwable;
                    handle(throwable, "onRegistrationDone()");
                      goto _L1
                }

                final SipSessionListenerProxy this$0;
                final int val$duration;
                final ISipSession val$session;

             {
                this$0 = SipSessionListenerProxy.this;
                session = isipsession;
                duration = i;
                super();
            }
            });
    }

    public void onRegistrationFailed(final ISipSession session, final int errorCode, final String message) {
        if(mListener != null)
            proxy(new Runnable() {

                public void run() {
                    mListener.onRegistrationFailed(session, errorCode, message);
_L1:
                    return;
                    Throwable throwable;
                    throwable;
                    handle(throwable, "onRegistrationFailed()");
                      goto _L1
                }

                final SipSessionListenerProxy this$0;
                final int val$errorCode;
                final String val$message;
                final ISipSession val$session;

             {
                this$0 = SipSessionListenerProxy.this;
                session = isipsession;
                errorCode = i;
                message = s;
                super();
            }
            });
    }

    public void onRegistrationTimeout(final ISipSession session) {
        if(mListener != null)
            proxy(new Runnable() {

                public void run() {
                    mListener.onRegistrationTimeout(session);
_L1:
                    return;
                    Throwable throwable;
                    throwable;
                    handle(throwable, "onRegistrationTimeout()");
                      goto _L1
                }

                final SipSessionListenerProxy this$0;
                final ISipSession val$session;

             {
                this$0 = SipSessionListenerProxy.this;
                session = isipsession;
                super();
            }
            });
    }

    public void onRinging(final ISipSession session, final SipProfile caller, final String sessionDescription) {
        if(mListener != null)
            proxy(new Runnable() {

                public void run() {
                    mListener.onRinging(session, caller, sessionDescription);
_L1:
                    return;
                    Throwable throwable;
                    throwable;
                    handle(throwable, "onRinging()");
                      goto _L1
                }

                final SipSessionListenerProxy this$0;
                final SipProfile val$caller;
                final ISipSession val$session;
                final String val$sessionDescription;

             {
                this$0 = SipSessionListenerProxy.this;
                session = isipsession;
                caller = sipprofile;
                sessionDescription = s;
                super();
            }
            });
    }

    public void onRingingBack(final ISipSession session) {
        if(mListener != null)
            proxy(new Runnable() {

                public void run() {
                    mListener.onRingingBack(session);
_L1:
                    return;
                    Throwable throwable;
                    throwable;
                    handle(throwable, "onRingingBack()");
                      goto _L1
                }

                final SipSessionListenerProxy this$0;
                final ISipSession val$session;

             {
                this$0 = SipSessionListenerProxy.this;
                session = isipsession;
                super();
            }
            });
    }

    public void setListener(ISipSessionListener isipsessionlistener) {
        mListener = isipsessionlistener;
    }

    private static final String TAG = "SipSession";
    private ISipSessionListener mListener;


}
