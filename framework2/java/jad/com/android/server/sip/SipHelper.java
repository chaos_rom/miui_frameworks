// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package com.android.server.sip;

import android.net.sip.SipProfile;
import gov.nist.javax.sip.SipStackExt;
import gov.nist.javax.sip.clientauthutils.AccountManager;
import gov.nist.javax.sip.clientauthutils.AuthenticationHelper;
import gov.nist.javax.sip.header.extensions.ReferredByHeader;
import java.text.ParseException;
import java.util.*;
import java.util.regex.Pattern;
import javax.sip.*;
import javax.sip.address.*;
import javax.sip.header.*;
import javax.sip.message.*;

class SipHelper {

    public SipHelper(SipStack sipstack, SipProvider sipprovider) throws PeerUnavailableException {
        mSipStack = sipstack;
        mSipProvider = sipprovider;
        SipFactory sipfactory = SipFactory.getInstance();
        mAddressFactory = sipfactory.createAddressFactory();
        mHeaderFactory = sipfactory.createHeaderFactory();
        mMessageFactory = sipfactory.createMessageFactory();
    }

    private CSeqHeader createCSeqHeader(String s) throws ParseException, InvalidArgumentException {
        long l = (long)(10000D * Math.random());
        return mHeaderFactory.createCSeqHeader(l, s);
    }

    private CallIdHeader createCallIdHeader() {
        return mSipProvider.getNewCallId();
    }

    private ContactHeader createContactHeader(SipProfile sipprofile) throws ParseException, SipException {
        return createContactHeader(sipprofile, null, 0);
    }

    private ContactHeader createContactHeader(SipProfile sipprofile, String s, int i) throws ParseException, SipException {
        SipURI sipuri;
        Address address;
        if(s == null)
            sipuri = createSipUri(sipprofile.getUserName(), sipprofile.getProtocol(), getListeningPoint());
        else
            sipuri = createSipUri(sipprofile.getUserName(), sipprofile.getProtocol(), s, i);
        address = mAddressFactory.createAddress(sipuri);
        address.setDisplayName(sipprofile.getDisplayName());
        return mHeaderFactory.createContactHeader(address);
    }

    private FromHeader createFromHeader(SipProfile sipprofile, String s) throws ParseException {
        return mHeaderFactory.createFromHeader(sipprofile.getSipAddress(), s);
    }

    private MaxForwardsHeader createMaxForwardsHeader() throws InvalidArgumentException {
        return mHeaderFactory.createMaxForwardsHeader(70);
    }

    private MaxForwardsHeader createMaxForwardsHeader(int i) throws InvalidArgumentException {
        return mHeaderFactory.createMaxForwardsHeader(i);
    }

    private Request createRequest(String s, SipProfile sipprofile, SipProfile sipprofile1, String s1) throws ParseException, SipException {
        FromHeader fromheader = createFromHeader(sipprofile, s1);
        ToHeader toheader = createToHeader(sipprofile1);
        SipURI sipuri = sipprofile1.getUri();
        List list = createViaHeaders();
        CallIdHeader callidheader = createCallIdHeader();
        CSeqHeader cseqheader = createCSeqHeader(s);
        MaxForwardsHeader maxforwardsheader = createMaxForwardsHeader();
        Request request = mMessageFactory.createRequest(sipuri, s, callidheader, cseqheader, fromheader, toheader, list, maxforwardsheader);
        request.addHeader(createContactHeader(sipprofile));
        return request;
    }

    private Request createRequest(String s, SipProfile sipprofile, String s1) throws ParseException, SipException {
        FromHeader fromheader = createFromHeader(sipprofile, s1);
        ToHeader toheader = createToHeader(sipprofile);
        String s2 = Pattern.quote((new StringBuilder()).append(sipprofile.getUserName()).append("@").toString());
        SipURI sipuri = mAddressFactory.createSipURI(sipprofile.getUriString().replaceFirst(s2, ""));
        List list = createViaHeaders();
        CallIdHeader callidheader = createCallIdHeader();
        CSeqHeader cseqheader = createCSeqHeader(s);
        MaxForwardsHeader maxforwardsheader = createMaxForwardsHeader();
        Request request = mMessageFactory.createRequest(sipuri, s, callidheader, cseqheader, fromheader, toheader, list, maxforwardsheader);
        request.addHeader(mHeaderFactory.createHeader("User-Agent", "SIPAUA/0.1.001"));
        return request;
    }

    private SipURI createSipUri(String s, String s1, String s2, int i) throws ParseException {
        SipURI sipuri = mAddressFactory.createSipURI(s, s2);
        try {
            sipuri.setPort(i);
            sipuri.setTransportParam(s1);
        }
        catch(InvalidArgumentException invalidargumentexception) {
            throw new RuntimeException(invalidargumentexception);
        }
        return sipuri;
    }

    private SipURI createSipUri(String s, String s1, ListeningPoint listeningpoint) throws ParseException {
        return createSipUri(s, s1, listeningpoint.getIPAddress(), listeningpoint.getPort());
    }

    private ToHeader createToHeader(SipProfile sipprofile) throws ParseException {
        return createToHeader(sipprofile, null);
    }

    private ToHeader createToHeader(SipProfile sipprofile, String s) throws ParseException {
        return mHeaderFactory.createToHeader(sipprofile.getSipAddress(), s);
    }

    private List createViaHeaders() throws ParseException, SipException {
        ArrayList arraylist = new ArrayList(1);
        ListeningPoint listeningpoint = getListeningPoint();
        ViaHeader viaheader = mHeaderFactory.createViaHeader(listeningpoint.getIPAddress(), listeningpoint.getPort(), listeningpoint.getTransport(), null);
        viaheader.setRPort();
        arraylist.add(viaheader);
        return arraylist;
    }

    private ContactHeader createWildcardContactHeader() {
        ContactHeader contactheader = mHeaderFactory.createContactHeader();
        contactheader.setWildCard();
        return contactheader;
    }

    public static String getCallId(EventObject eventobject) {
        String s;
        if(eventobject == null)
            s = null;
        else
        if(eventobject instanceof RequestEvent)
            s = getCallId(((Message) (((RequestEvent)eventobject).getRequest())));
        else
        if(eventobject instanceof ResponseEvent)
            s = getCallId(((Message) (((ResponseEvent)eventobject).getResponse())));
        else
        if(eventobject instanceof DialogTerminatedEvent) {
            ((DialogTerminatedEvent)eventobject).getDialog();
            s = getCallId(((DialogTerminatedEvent)eventobject).getDialog());
        } else
        if(eventobject instanceof TransactionTerminatedEvent) {
            TransactionTerminatedEvent transactionterminatedevent = (TransactionTerminatedEvent)eventobject;
            Object obj1;
            if(transactionterminatedevent.isServerTransaction())
                obj1 = transactionterminatedevent.getServerTransaction();
            else
                obj1 = transactionterminatedevent.getClientTransaction();
            s = getCallId(((Transaction) (obj1)));
        } else {
            Object obj = eventobject.getSource();
            if(obj instanceof Transaction)
                s = getCallId((Transaction)obj);
            else
            if(obj instanceof Dialog)
                s = getCallId((Dialog)obj);
            else
                s = "";
        }
        return s;
    }

    private static String getCallId(Dialog dialog) {
        return dialog.getCallId().getCallId();
    }

    public static String getCallId(Transaction transaction) {
        String s;
        if(transaction != null)
            s = getCallId(((Message) (transaction.getRequest())));
        else
            s = "";
        return s;
    }

    private static String getCallId(Message message) {
        return ((CallIdHeader)message.getHeader("Call-ID")).getCallId();
    }

    private ListeningPoint getListeningPoint() throws SipException {
        ListeningPoint listeningpoint = mSipProvider.getListeningPoint("UDP");
        if(listeningpoint == null)
            listeningpoint = mSipProvider.getListeningPoint("TCP");
        if(listeningpoint == null) {
            ListeningPoint alisteningpoint[] = mSipProvider.getListeningPoints();
            if(alisteningpoint != null && alisteningpoint.length > 0)
                listeningpoint = alisteningpoint[0];
        }
        if(listeningpoint == null)
            throw new SipException("no listening point is available");
        else
            return listeningpoint;
    }

    public ServerTransaction getServerTransaction(RequestEvent requestevent) throws SipException {
        ServerTransaction servertransaction = requestevent.getServerTransaction();
        if(servertransaction == null) {
            Request request = requestevent.getRequest();
            servertransaction = mSipProvider.getNewServerTransaction(request);
        }
        return servertransaction;
    }

    public ClientTransaction handleChallenge(ResponseEvent responseevent, AccountManager accountmanager) throws SipException {
        AuthenticationHelper authenticationhelper = ((SipStackExt)mSipStack).getAuthenticationHelper(accountmanager, mHeaderFactory);
        ClientTransaction clienttransaction = responseevent.getClientTransaction();
        ClientTransaction clienttransaction1 = authenticationhelper.handleChallenge(responseevent.getResponse(), clienttransaction, mSipProvider, 5);
        clienttransaction1.sendRequest();
        return clienttransaction1;
    }

    public void sendBye(Dialog dialog) throws SipException {
        Request request = dialog.createRequest("BYE");
        dialog.sendRequest(mSipProvider.getNewClientTransaction(request));
    }

    public void sendCancel(ClientTransaction clienttransaction) throws SipException {
        Request request = clienttransaction.createCancel();
        mSipProvider.getNewClientTransaction(request).sendRequest();
    }

    public ClientTransaction sendInvite(SipProfile sipprofile, SipProfile sipprofile1, String s, String s1, ReferredByHeader referredbyheader, String s2) throws SipException {
        ClientTransaction clienttransaction;
        try {
            Request request = createRequest("INVITE", sipprofile, sipprofile1, s1);
            if(referredbyheader != null)
                request.addHeader(referredbyheader);
            if(s2 != null)
                request.addHeader(mHeaderFactory.createHeader("Replaces", s2));
            request.setContent(s, mHeaderFactory.createContentTypeHeader("application", "sdp"));
            clienttransaction = mSipProvider.getNewClientTransaction(request);
            clienttransaction.sendRequest();
        }
        catch(ParseException parseexception) {
            throw new SipException("sendInvite()", parseexception);
        }
        return clienttransaction;
    }

    public void sendInviteAck(ResponseEvent responseevent, Dialog dialog) throws SipException {
        dialog.sendAck(dialog.createAck(((CSeqHeader)responseevent.getResponse().getHeader("CSeq")).getSeqNumber()));
    }

    public void sendInviteBusyHere(RequestEvent requestevent, ServerTransaction servertransaction) throws SipException {
        try {
            Request request = requestevent.getRequest();
            Response response = mMessageFactory.createResponse(486, request);
            if(servertransaction == null)
                servertransaction = getServerTransaction(requestevent);
            if(servertransaction.getState() != TransactionState.COMPLETED)
                servertransaction.sendResponse(response);
            return;
        }
        catch(ParseException parseexception) {
            throw new SipException("sendInviteBusyHere()", parseexception);
        }
    }

    public ServerTransaction sendInviteOk(RequestEvent requestevent, SipProfile sipprofile, String s, ServerTransaction servertransaction, String s1, int i) throws SipException {
        try {
            Request request = requestevent.getRequest();
            Response response = mMessageFactory.createResponse(200, request);
            response.addHeader(createContactHeader(sipprofile, s1, i));
            response.setContent(s, mHeaderFactory.createContentTypeHeader("application", "sdp"));
            if(servertransaction == null)
                servertransaction = getServerTransaction(requestevent);
            if(servertransaction.getState() != TransactionState.COMPLETED)
                servertransaction.sendResponse(response);
        }
        catch(ParseException parseexception) {
            throw new SipException("sendInviteOk()", parseexception);
        }
        return servertransaction;
    }

    public void sendInviteRequestTerminated(Request request, ServerTransaction servertransaction) throws SipException {
        try {
            servertransaction.sendResponse(mMessageFactory.createResponse(487, request));
            return;
        }
        catch(ParseException parseexception) {
            throw new SipException("sendInviteRequestTerminated()", parseexception);
        }
    }

    public ClientTransaction sendOptions(SipProfile sipprofile, SipProfile sipprofile1, String s) throws SipException {
        if(sipprofile != sipprofile1) goto _L2; else goto _L1
_L1:
        Request request1 = createRequest("OPTIONS", sipprofile, s);
_L3:
        ClientTransaction clienttransaction = mSipProvider.getNewClientTransaction(request1);
        clienttransaction.sendRequest();
        return clienttransaction;
_L2:
        Request request = createRequest("OPTIONS", sipprofile, sipprofile1, s);
        request1 = request;
          goto _L3
        Exception exception;
        exception;
        throw new SipException("sendOptions()", exception);
    }

    public void sendReferNotify(Dialog dialog, String s) throws SipException {
        try {
            Request request = dialog.createRequest("NOTIFY");
            request.addHeader(mHeaderFactory.createSubscriptionStateHeader("active;expires=60"));
            request.setContent(s, mHeaderFactory.createContentTypeHeader("message", "sipfrag"));
            request.addHeader(mHeaderFactory.createEventHeader("refer"));
            dialog.sendRequest(mSipProvider.getNewClientTransaction(request));
            return;
        }
        catch(ParseException parseexception) {
            throw new SipException("sendReferNotify()", parseexception);
        }
    }

    public ClientTransaction sendRegister(SipProfile sipprofile, String s, int i) throws SipException {
        try {
            Request request = createRequest("REGISTER", sipprofile, s);
            ClientTransaction clienttransaction;
            if(i == 0)
                request.addHeader(createWildcardContactHeader());
            else
                request.addHeader(createContactHeader(sipprofile));
            request.addHeader(mHeaderFactory.createExpiresHeader(i));
            clienttransaction = mSipProvider.getNewClientTransaction(request);
            clienttransaction.sendRequest();
            return clienttransaction;
        }
        catch(ParseException parseexception) {
            throw new SipException("sendRegister()", parseexception);
        }
    }

    public ClientTransaction sendReinvite(Dialog dialog, String s) throws SipException {
        ClientTransaction clienttransaction;
        try {
            Request request = dialog.createRequest("INVITE");
            request.setContent(s, mHeaderFactory.createContentTypeHeader("application", "sdp"));
            ViaHeader viaheader = (ViaHeader)request.getHeader("Via");
            if(viaheader != null)
                viaheader.setRPort();
            clienttransaction = mSipProvider.getNewClientTransaction(request);
            dialog.sendRequest(clienttransaction);
        }
        catch(ParseException parseexception) {
            throw new SipException("sendReinvite()", parseexception);
        }
        return clienttransaction;
    }

    public void sendResponse(RequestEvent requestevent, int i) throws SipException {
        try {
            Request request = requestevent.getRequest();
            Response response = mMessageFactory.createResponse(i, request);
            getServerTransaction(requestevent).sendResponse(response);
            return;
        }
        catch(ParseException parseexception) {
            throw new SipException("sendResponse()", parseexception);
        }
    }

    public ServerTransaction sendRinging(RequestEvent requestevent, String s) throws SipException {
        ServerTransaction servertransaction;
        try {
            Request request = requestevent.getRequest();
            servertransaction = getServerTransaction(requestevent);
            Response response = mMessageFactory.createResponse(180, request);
            ToHeader toheader = (ToHeader)response.getHeader("To");
            toheader.setTag(s);
            response.addHeader(toheader);
            servertransaction.sendResponse(response);
        }
        catch(ParseException parseexception) {
            throw new SipException("sendRinging()", parseexception);
        }
        return servertransaction;
    }

    private static final boolean DEBUG;
    private static final boolean DEBUG_PING;
    private static final String TAG = com/android/server/sip/SipHelper.getSimpleName();
    private AddressFactory mAddressFactory;
    private HeaderFactory mHeaderFactory;
    private MessageFactory mMessageFactory;
    private SipProvider mSipProvider;
    private SipStack mSipStack;

}
