// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.gesture;

import java.util.*;

// Referenced classes of package android.gesture:
//            Learner, Instance, GestureUtils, Prediction

class InstanceLearner extends Learner {

    InstanceLearner() {
    }

    ArrayList classify(int i, int j, float af[]) {
        ArrayList arraylist = new ArrayList();
        ArrayList arraylist1 = getInstances();
        int k = arraylist1.size();
        TreeMap treemap = new TreeMap();
        int l = 0;
        while(l < k)  {
            Instance instance = (Instance)arraylist1.get(l);
            if(instance.vector.length == af.length) {
                double d1;
                double d2;
                Double double1;
                if(i == 2)
                    d1 = GestureUtils.minimumCosineDistance(instance.vector, af, j);
                else
                    d1 = GestureUtils.squaredEuclideanDistance(instance.vector, af);
                if(d1 == 0.0D)
                    d2 = 1.7976931348623157E+308D;
                else
                    d2 = 1.0D / d1;
                double1 = (Double)treemap.get(instance.label);
                if(double1 == null || d2 > double1.doubleValue())
                    treemap.put(instance.label, Double.valueOf(d2));
            }
            l++;
        }
        Prediction prediction;
        for(Iterator iterator = treemap.keySet().iterator(); iterator.hasNext(); arraylist.add(prediction)) {
            String s = (String)iterator.next();
            double d = ((Double)treemap.get(s)).doubleValue();
            prediction = new Prediction(s, d);
        }

        Collections.sort(arraylist, sComparator);
        return arraylist;
    }

    private static final Comparator sComparator = new Comparator() {

        public int compare(Prediction prediction, Prediction prediction1) {
            double d = prediction.score;
            double d1 = prediction1.score;
            byte byte0;
            if(d > d1)
                byte0 = -1;
            else
            if(d < d1)
                byte0 = 1;
            else
                byte0 = 0;
            return byte0;
        }

        public volatile int compare(Object obj, Object obj1) {
            return compare((Prediction)obj, (Prediction)obj1);
        }

    };

}
