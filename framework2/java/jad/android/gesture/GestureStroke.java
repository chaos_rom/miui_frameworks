// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.gesture;

import android.graphics.*;
import java.io.*;
import java.util.ArrayList;

// Referenced classes of package android.gesture:
//            GesturePoint, GestureUtils, OrientedBoundingBox

public class GestureStroke {

    private GestureStroke(RectF rectf, float f, float af[], long al[]) {
        boundingBox = new RectF(rectf.left, rectf.top, rectf.right, rectf.bottom);
        length = f;
        points = (float[])af.clone();
        timestamps = (long[])al.clone();
    }

    public GestureStroke(ArrayList arraylist) {
        int i = arraylist.size();
        float af[] = new float[i * 2];
        long al[] = new long[i];
        RectF rectf = null;
        float f = 0.0F;
        int j = 0;
        int k = 0;
        while(k < i)  {
            GesturePoint gesturepoint = (GesturePoint)arraylist.get(k);
            af[k * 2] = gesturepoint.x;
            af[1 + k * 2] = gesturepoint.y;
            al[j] = gesturepoint.timestamp;
            if(rectf == null) {
                rectf = new RectF();
                rectf.top = gesturepoint.y;
                rectf.left = gesturepoint.x;
                rectf.right = gesturepoint.x;
                rectf.bottom = gesturepoint.y;
                f = 0.0F;
            } else {
                f = (float)((double)f + Math.sqrt(Math.pow(gesturepoint.x - af[2 * (k - 1)], 2D) + Math.pow(gesturepoint.y - af[1 + 2 * (k - 1)], 2D)));
                rectf.union(gesturepoint.x, gesturepoint.y);
            }
            j++;
            k++;
        }
        timestamps = al;
        points = af;
        boundingBox = rectf;
        length = f;
    }

    static GestureStroke deserialize(DataInputStream datainputstream) throws IOException {
        int i = datainputstream.readInt();
        ArrayList arraylist = new ArrayList(i);
        for(int j = 0; j < i; j++)
            arraylist.add(GesturePoint.deserialize(datainputstream));

        return new GestureStroke(arraylist);
    }

    private void makePath() {
        float af[] = points;
        int i = af.length;
        Path path = null;
        float f = 0.0F;
        float f1 = 0.0F;
        int j = 0;
        while(j < i)  {
            float f2 = af[j];
            float f3 = af[j + 1];
            if(path == null) {
                path = new Path();
                path.moveTo(f2, f3);
                f = f2;
                f1 = f3;
            } else {
                float f4 = Math.abs(f2 - f);
                float f5 = Math.abs(f3 - f1);
                if(f4 >= 3F || f5 >= 3F) {
                    path.quadTo(f, f1, (f2 + f) / 2.0F, (f3 + f1) / 2.0F);
                    f = f2;
                    f1 = f3;
                }
            }
            j += 2;
        }
        mCachedPath = path;
    }

    public void clearPath() {
        if(mCachedPath != null)
            mCachedPath.rewind();
    }

    public Object clone() {
        return new GestureStroke(boundingBox, length, points, timestamps);
    }

    public OrientedBoundingBox computeOrientedBoundingBox() {
        return GestureUtils.computeOrientedBoundingBox(points);
    }

    void draw(Canvas canvas, Paint paint) {
        if(mCachedPath == null)
            makePath();
        canvas.drawPath(mCachedPath, paint);
    }

    public Path getPath() {
        if(mCachedPath == null)
            makePath();
        return mCachedPath;
    }

    void serialize(DataOutputStream dataoutputstream) throws IOException {
        float af[] = points;
        long al[] = timestamps;
        int i = points.length;
        dataoutputstream.writeInt(i / 2);
        for(int j = 0; j < i; j += 2) {
            dataoutputstream.writeFloat(af[j]);
            dataoutputstream.writeFloat(af[j + 1]);
            dataoutputstream.writeLong(al[j / 2]);
        }

    }

    public Path toPath(float f, float f1, int i) {
        float af[] = GestureUtils.temporalSampling(this, i);
        RectF rectf = boundingBox;
        GestureUtils.translate(af, -rectf.left, -rectf.top);
        float f2 = f / rectf.width();
        float f3 = f1 / rectf.height();
        float f4;
        float f5;
        float f6;
        Path path;
        int j;
        int k;
        if(f2 > f3)
            f4 = f3;
        else
            f4 = f2;
        GestureUtils.scale(af, f4, f4);
        f5 = 0.0F;
        f6 = 0.0F;
        path = null;
        j = af.length;
        k = 0;
        while(k < j)  {
            float f7 = af[k];
            float f8 = af[k + 1];
            if(path == null) {
                path = new Path();
                path.moveTo(f7, f8);
                f5 = f7;
                f6 = f8;
            } else {
                float f9 = Math.abs(f7 - f5);
                float f10 = Math.abs(f8 - f6);
                if(f9 >= 3F || f10 >= 3F) {
                    path.quadTo(f5, f6, (f7 + f5) / 2.0F, (f8 + f6) / 2.0F);
                    f5 = f7;
                    f6 = f8;
                }
            }
            k += 2;
        }
        return path;
    }

    static final float TOUCH_TOLERANCE = 3F;
    public final RectF boundingBox;
    public final float length;
    private Path mCachedPath;
    public final float points[];
    private final long timestamps[];
}
