// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.gesture;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.*;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import java.util.ArrayList;

// Referenced classes of package android.gesture:
//            Gesture, GesturePoint, GestureUtils, OrientedBoundingBox, 
//            GestureStroke

public class GestureOverlayView extends FrameLayout {
    public static interface OnGesturePerformedListener {

        public abstract void onGesturePerformed(GestureOverlayView gestureoverlayview, Gesture gesture);
    }

    public static interface OnGestureListener {

        public abstract void onGesture(GestureOverlayView gestureoverlayview, MotionEvent motionevent);

        public abstract void onGestureCancelled(GestureOverlayView gestureoverlayview, MotionEvent motionevent);

        public abstract void onGestureEnded(GestureOverlayView gestureoverlayview, MotionEvent motionevent);

        public abstract void onGestureStarted(GestureOverlayView gestureoverlayview, MotionEvent motionevent);
    }

    public static interface OnGesturingListener {

        public abstract void onGesturingEnded(GestureOverlayView gestureoverlayview);

        public abstract void onGesturingStarted(GestureOverlayView gestureoverlayview);
    }

    private class FadeOutRunnable
        implements Runnable {

        public void run() {
            if(mIsFadingOut) {
                long l = AnimationUtils.currentAnimationTimeMillis() - mFadingStart;
                if(l > mFadeDuration) {
                    if(fireActionPerformed)
                        fireOnGesturePerformed();
                    mPreviousWasGesturing = false;
                    mIsFadingOut = false;
                    mFadingHasStarted = false;
                    mPath.rewind();
                    mCurrentGesture = null;
                    setPaintAlpha(255);
                } else {
                    mFadingHasStarted = true;
                    float f = Math.max(0.0F, Math.min(1.0F, (float)l / (float)mFadeDuration));
                    mFadingAlpha = 1.0F - mInterpolator.getInterpolation(f);
                    setPaintAlpha((int)(255F * mFadingAlpha));
                    postDelayed(this, 16L);
                }
            } else
            if(resetMultipleStrokes) {
                mResetGesture = true;
            } else {
                fireOnGesturePerformed();
                mFadingHasStarted = false;
                mPath.rewind();
                mCurrentGesture = null;
                mPreviousWasGesturing = false;
                setPaintAlpha(255);
            }
            invalidate();
        }

        boolean fireActionPerformed;
        boolean resetMultipleStrokes;
        final GestureOverlayView this$0;

        private FadeOutRunnable() {
            this$0 = GestureOverlayView.this;
            super();
        }

    }


    public GestureOverlayView(Context context) {
        super(context);
        mGesturePaint = new Paint();
        mFadeDuration = 150L;
        mFadeOffset = 420L;
        mFadeEnabled = true;
        mCertainGestureColor = -256;
        mUncertainGestureColor = 0x48ffff00;
        mGestureStrokeWidth = 12F;
        mInvalidateExtraBorder = 10;
        mGestureStrokeType = 0;
        mGestureStrokeLengthThreshold = 50F;
        mGestureStrokeSquarenessTreshold = 0.275F;
        mGestureStrokeAngleThreshold = 40F;
        mOrientation = 1;
        mInvalidRect = new Rect();
        mPath = new Path();
        mGestureVisible = true;
        mIsGesturing = false;
        mPreviousWasGesturing = false;
        mInterceptEvents = true;
        mStrokeBuffer = new ArrayList(100);
        mOnGestureListeners = new ArrayList();
        mOnGesturePerformedListeners = new ArrayList();
        mOnGesturingListeners = new ArrayList();
        mIsFadingOut = false;
        mFadingAlpha = 1.0F;
        mInterpolator = new AccelerateDecelerateInterpolator();
        mFadingOut = new FadeOutRunnable();
        init();
    }

    public GestureOverlayView(Context context, AttributeSet attributeset) {
        this(context, attributeset, 0x10103bf);
    }

    public GestureOverlayView(Context context, AttributeSet attributeset, int i) {
        super(context, attributeset, i);
        mGesturePaint = new Paint();
        mFadeDuration = 150L;
        mFadeOffset = 420L;
        mFadeEnabled = true;
        mCertainGestureColor = -256;
        mUncertainGestureColor = 0x48ffff00;
        mGestureStrokeWidth = 12F;
        mInvalidateExtraBorder = 10;
        mGestureStrokeType = 0;
        mGestureStrokeLengthThreshold = 50F;
        mGestureStrokeSquarenessTreshold = 0.275F;
        mGestureStrokeAngleThreshold = 40F;
        mOrientation = 1;
        mInvalidRect = new Rect();
        mPath = new Path();
        mGestureVisible = true;
        mIsGesturing = false;
        mPreviousWasGesturing = false;
        mInterceptEvents = true;
        mStrokeBuffer = new ArrayList(100);
        mOnGestureListeners = new ArrayList();
        mOnGesturePerformedListeners = new ArrayList();
        mOnGesturingListeners = new ArrayList();
        mIsFadingOut = false;
        mFadingAlpha = 1.0F;
        mInterpolator = new AccelerateDecelerateInterpolator();
        mFadingOut = new FadeOutRunnable();
        TypedArray typedarray = context.obtainStyledAttributes(attributeset, com.android.internal.R.styleable.GestureOverlayView, i, 0);
        mGestureStrokeWidth = typedarray.getFloat(1, mGestureStrokeWidth);
        mInvalidateExtraBorder = Math.max(1, -1 + (int)mGestureStrokeWidth);
        mCertainGestureColor = typedarray.getColor(2, mCertainGestureColor);
        mUncertainGestureColor = typedarray.getColor(3, mUncertainGestureColor);
        mFadeDuration = typedarray.getInt(5, (int)mFadeDuration);
        mFadeOffset = typedarray.getInt(4, (int)mFadeOffset);
        mGestureStrokeType = typedarray.getInt(6, mGestureStrokeType);
        mGestureStrokeLengthThreshold = typedarray.getFloat(7, mGestureStrokeLengthThreshold);
        mGestureStrokeAngleThreshold = typedarray.getFloat(9, mGestureStrokeAngleThreshold);
        mGestureStrokeSquarenessTreshold = typedarray.getFloat(8, mGestureStrokeSquarenessTreshold);
        mInterceptEvents = typedarray.getBoolean(10, mInterceptEvents);
        mFadeEnabled = typedarray.getBoolean(11, mFadeEnabled);
        mOrientation = typedarray.getInt(0, mOrientation);
        typedarray.recycle();
        init();
    }

    private void cancelGesture(MotionEvent motionevent) {
        ArrayList arraylist = mOnGestureListeners;
        int i = arraylist.size();
        for(int j = 0; j < i; j++)
            ((OnGestureListener)arraylist.get(j)).onGestureCancelled(this, motionevent);

        clear(false);
    }

    private void clear(boolean flag, boolean flag1, boolean flag2) {
        setPaintAlpha(255);
        removeCallbacks(mFadingOut);
        mResetGesture = false;
        mFadingOut.fireActionPerformed = flag1;
        mFadingOut.resetMultipleStrokes = false;
        if(flag && mCurrentGesture != null) {
            mFadingAlpha = 1.0F;
            mIsFadingOut = true;
            mFadingHasStarted = false;
            mFadingStart = AnimationUtils.currentAnimationTimeMillis() + mFadeOffset;
            postDelayed(mFadingOut, mFadeOffset);
        } else {
            mFadingAlpha = 1.0F;
            mIsFadingOut = false;
            mFadingHasStarted = false;
            if(flag2) {
                mCurrentGesture = null;
                mPath.rewind();
                invalidate();
            } else
            if(flag1)
                postDelayed(mFadingOut, mFadeOffset);
            else
            if(mGestureStrokeType == 1) {
                mFadingOut.resetMultipleStrokes = true;
                postDelayed(mFadingOut, mFadeOffset);
            } else {
                mCurrentGesture = null;
                mPath.rewind();
                invalidate();
            }
        }
    }

    private void fireOnGesturePerformed() {
        ArrayList arraylist = mOnGesturePerformedListeners;
        int i = arraylist.size();
        for(int j = 0; j < i; j++)
            ((OnGesturePerformedListener)arraylist.get(j)).onGesturePerformed(this, mCurrentGesture);

    }

    private void init() {
        setWillNotDraw(false);
        Paint paint = mGesturePaint;
        paint.setAntiAlias(true);
        paint.setColor(mCertainGestureColor);
        paint.setStyle(android.graphics.Paint.Style.STROKE);
        paint.setStrokeJoin(android.graphics.Paint.Join.ROUND);
        paint.setStrokeCap(android.graphics.Paint.Cap.ROUND);
        paint.setStrokeWidth(mGestureStrokeWidth);
        paint.setDither(true);
        mCurrentColor = mCertainGestureColor;
        setPaintAlpha(255);
    }

    private boolean processEvent(MotionEvent motionevent) {
        boolean flag = true;
        motionevent.getAction();
        JVM INSTR tableswitch 0 3: default 36
    //                   0 40
    //                   1 77
    //                   2 52
    //                   3 97;
           goto _L1 _L2 _L3 _L4 _L5
_L1:
        flag = false;
_L6:
        return flag;
_L2:
        touchDown(motionevent);
        invalidate();
          goto _L6
_L4:
        if(!mIsListeningForGestures) goto _L1; else goto _L7
_L7:
        Rect rect = touchMove(motionevent);
        if(rect != null)
            invalidate(rect);
          goto _L6
_L3:
        if(!mIsListeningForGestures) goto _L1; else goto _L8
_L8:
        touchUp(motionevent, false);
        invalidate();
          goto _L6
_L5:
        if(!mIsListeningForGestures) goto _L1; else goto _L9
_L9:
        touchUp(motionevent, flag);
        invalidate();
          goto _L6
    }

    private void setCurrentColor(int i) {
        mCurrentColor = i;
        if(mFadingHasStarted)
            setPaintAlpha((int)(255F * mFadingAlpha));
        else
            setPaintAlpha(255);
        invalidate();
    }

    private void setPaintAlpha(int i) {
        int j = (i + (i >> 7)) * (mCurrentColor >>> 24) >> 8;
        mGesturePaint.setColor((mCurrentColor << 8) >>> 8 | j << 24);
    }

    private void touchDown(MotionEvent motionevent) {
        float f;
        float f1;
        mIsListeningForGestures = true;
        f = motionevent.getX();
        f1 = motionevent.getY();
        mX = f;
        mY = f1;
        mTotalLength = 0.0F;
        mIsGesturing = false;
        if(mGestureStrokeType != 0 && !mResetGesture) goto _L2; else goto _L1
_L1:
        if(mHandleGestureActions)
            setCurrentColor(mUncertainGestureColor);
        mResetGesture = false;
        mCurrentGesture = null;
        mPath.rewind();
_L8:
        if(!mFadingHasStarted) goto _L4; else goto _L3
_L3:
        cancelClearAnimation();
_L6:
        if(mCurrentGesture == null)
            mCurrentGesture = new Gesture();
        mStrokeBuffer.add(new GesturePoint(f, f1, motionevent.getEventTime()));
        mPath.moveTo(f, f1);
        int i = mInvalidateExtraBorder;
        mInvalidRect.set((int)f - i, (int)f1 - i, i + (int)f, i + (int)f1);
        mCurveEndX = f;
        mCurveEndY = f1;
        ArrayList arraylist = mOnGestureListeners;
        int j = arraylist.size();
        for(int k = 0; k < j; k++)
            ((OnGestureListener)arraylist.get(k)).onGestureStarted(this, motionevent);

        break; /* Loop/switch isn't completed */
_L2:
        if((mCurrentGesture == null || mCurrentGesture.getStrokesCount() == 0) && mHandleGestureActions)
            setCurrentColor(mUncertainGestureColor);
        continue; /* Loop/switch isn't completed */
_L4:
        if(mIsFadingOut) {
            setPaintAlpha(255);
            mIsFadingOut = false;
            mFadingHasStarted = false;
            removeCallbacks(mFadingOut);
        }
        if(true) goto _L6; else goto _L5
_L5:
        return;
        if(true) goto _L8; else goto _L7
_L7:
    }

    private Rect touchMove(MotionEvent motionevent) {
        Rect rect = null;
        float f = motionevent.getX();
        float f1 = motionevent.getY();
        float f2 = mX;
        float f3 = mY;
        float f4 = Math.abs(f - f2);
        float f5 = Math.abs(f1 - f3);
        if(f4 >= 3F || f5 >= 3F) {
            rect = mInvalidRect;
            int i = mInvalidateExtraBorder;
            rect.set((int)mCurveEndX - i, (int)mCurveEndY - i, i + (int)mCurveEndX, i + (int)mCurveEndY);
            float f6 = (f + f2) / 2.0F;
            mCurveEndX = f6;
            float f7 = (f1 + f3) / 2.0F;
            mCurveEndY = f7;
            mPath.quadTo(f2, f3, f6, f7);
            rect.union((int)f2 - i, (int)f3 - i, i + (int)f2, i + (int)f3);
            rect.union((int)f6 - i, (int)f7 - i, i + (int)f6, i + (int)f7);
            mX = f;
            mY = f1;
            ArrayList arraylist = mStrokeBuffer;
            GesturePoint gesturepoint = new GesturePoint(f, f1, motionevent.getEventTime());
            arraylist.add(gesturepoint);
            if(mHandleGestureActions && !mIsGesturing) {
                mTotalLength = mTotalLength + (float)Math.sqrt(f4 * f4 + f5 * f5);
                if(mTotalLength > mGestureStrokeLengthThreshold) {
                    OrientedBoundingBox orientedboundingbox = GestureUtils.computeOrientedBoundingBox(mStrokeBuffer);
                    float f8 = Math.abs(orientedboundingbox.orientation);
                    if(f8 > 90F)
                        f8 = 180F - f8;
                    if(orientedboundingbox.squareness > mGestureStrokeSquarenessTreshold || (mOrientation != 1 ? f8 > mGestureStrokeAngleThreshold : f8 < mGestureStrokeAngleThreshold)) {
                        mIsGesturing = true;
                        setCurrentColor(mCertainGestureColor);
                        ArrayList arraylist2 = mOnGesturingListeners;
                        int l = arraylist2.size();
                        for(int i1 = 0; i1 < l; i1++)
                            ((OnGesturingListener)arraylist2.get(i1)).onGesturingStarted(this);

                    }
                }
            }
            ArrayList arraylist1 = mOnGestureListeners;
            int j = arraylist1.size();
            for(int k = 0; k < j; k++)
                ((OnGestureListener)arraylist1.get(k)).onGesture(this, motionevent);

        }
        return rect;
    }

    private void touchUp(MotionEvent motionevent, boolean flag) {
        boolean flag1 = true;
        mIsListeningForGestures = false;
        if(mCurrentGesture != null) {
            mCurrentGesture.addStroke(new GestureStroke(mStrokeBuffer));
            if(!flag) {
                ArrayList arraylist1 = mOnGestureListeners;
                int k = arraylist1.size();
                for(int l = 0; l < k; l++)
                    ((OnGestureListener)arraylist1.get(l)).onGestureEnded(this, motionevent);

                ArrayList arraylist;
                int i;
                int j;
                boolean flag2;
                if(mHandleGestureActions && mFadeEnabled)
                    flag2 = flag1;
                else
                    flag2 = false;
                if(!mHandleGestureActions || !mIsGesturing)
                    flag1 = false;
                clear(flag2, flag1, false);
            } else {
                cancelGesture(motionevent);
            }
        } else {
            cancelGesture(motionevent);
        }
        mStrokeBuffer.clear();
        mPreviousWasGesturing = mIsGesturing;
        mIsGesturing = false;
        arraylist = mOnGesturingListeners;
        i = arraylist.size();
        for(j = 0; j < i; j++)
            ((OnGesturingListener)arraylist.get(j)).onGesturingEnded(this);

    }

    public void addOnGestureListener(OnGestureListener ongesturelistener) {
        mOnGestureListeners.add(ongesturelistener);
    }

    public void addOnGesturePerformedListener(OnGesturePerformedListener ongestureperformedlistener) {
        mOnGesturePerformedListeners.add(ongestureperformedlistener);
        if(mOnGesturePerformedListeners.size() > 0)
            mHandleGestureActions = true;
    }

    public void addOnGesturingListener(OnGesturingListener ongesturinglistener) {
        mOnGesturingListeners.add(ongesturinglistener);
    }

    public void cancelClearAnimation() {
        setPaintAlpha(255);
        mIsFadingOut = false;
        mFadingHasStarted = false;
        removeCallbacks(mFadingOut);
        mPath.rewind();
        mCurrentGesture = null;
    }

    public void cancelGesture() {
        mIsListeningForGestures = false;
        mCurrentGesture.addStroke(new GestureStroke(mStrokeBuffer));
        long l = SystemClock.uptimeMillis();
        MotionEvent motionevent = MotionEvent.obtain(l, l, 3, 0.0F, 0.0F, 0);
        ArrayList arraylist = mOnGestureListeners;
        int i = arraylist.size();
        for(int j = 0; j < i; j++)
            ((OnGestureListener)arraylist.get(j)).onGestureCancelled(this, motionevent);

        motionevent.recycle();
        clear(false);
        mIsGesturing = false;
        mPreviousWasGesturing = false;
        mStrokeBuffer.clear();
        ArrayList arraylist1 = mOnGesturingListeners;
        int k = arraylist1.size();
        for(int i1 = 0; i1 < k; i1++)
            ((OnGesturingListener)arraylist1.get(i1)).onGesturingEnded(this);

    }

    public void clear(boolean flag) {
        clear(flag, false, true);
    }

    public boolean dispatchTouchEvent(MotionEvent motionevent) {
        boolean flag = true;
        if(isEnabled()) {
            boolean flag1;
            if((mIsGesturing || mCurrentGesture != null && mCurrentGesture.getStrokesCount() > 0 && mPreviousWasGesturing) && mInterceptEvents)
                flag1 = flag;
            else
                flag1 = false;
            processEvent(motionevent);
            if(flag1)
                motionevent.setAction(3);
            super.dispatchTouchEvent(motionevent);
        } else {
            flag = super.dispatchTouchEvent(motionevent);
        }
        return flag;
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        if(mCurrentGesture != null && mGestureVisible)
            canvas.drawPath(mPath, mGesturePaint);
    }

    public ArrayList getCurrentStroke() {
        return mStrokeBuffer;
    }

    public long getFadeOffset() {
        return mFadeOffset;
    }

    public Gesture getGesture() {
        return mCurrentGesture;
    }

    public int getGestureColor() {
        return mCertainGestureColor;
    }

    public Paint getGesturePaint() {
        return mGesturePaint;
    }

    public Path getGesturePath() {
        return mPath;
    }

    public Path getGesturePath(Path path) {
        path.set(mPath);
        return path;
    }

    public float getGestureStrokeAngleThreshold() {
        return mGestureStrokeAngleThreshold;
    }

    public float getGestureStrokeLengthThreshold() {
        return mGestureStrokeLengthThreshold;
    }

    public float getGestureStrokeSquarenessTreshold() {
        return mGestureStrokeSquarenessTreshold;
    }

    public int getGestureStrokeType() {
        return mGestureStrokeType;
    }

    public float getGestureStrokeWidth() {
        return mGestureStrokeWidth;
    }

    public int getOrientation() {
        return mOrientation;
    }

    public int getUncertainGestureColor() {
        return mUncertainGestureColor;
    }

    public boolean isEventsInterceptionEnabled() {
        return mInterceptEvents;
    }

    public boolean isFadeEnabled() {
        return mFadeEnabled;
    }

    public boolean isGestureVisible() {
        return mGestureVisible;
    }

    public boolean isGesturing() {
        return mIsGesturing;
    }

    protected void onDetachedFromWindow() {
        cancelClearAnimation();
    }

    public void removeAllOnGestureListeners() {
        mOnGestureListeners.clear();
    }

    public void removeAllOnGesturePerformedListeners() {
        mOnGesturePerformedListeners.clear();
        mHandleGestureActions = false;
    }

    public void removeAllOnGesturingListeners() {
        mOnGesturingListeners.clear();
    }

    public void removeOnGestureListener(OnGestureListener ongesturelistener) {
        mOnGestureListeners.remove(ongesturelistener);
    }

    public void removeOnGesturePerformedListener(OnGesturePerformedListener ongestureperformedlistener) {
        mOnGesturePerformedListeners.remove(ongestureperformedlistener);
        if(mOnGesturePerformedListeners.size() <= 0)
            mHandleGestureActions = false;
    }

    public void removeOnGesturingListener(OnGesturingListener ongesturinglistener) {
        mOnGesturingListeners.remove(ongesturinglistener);
    }

    public void setEventsInterceptionEnabled(boolean flag) {
        mInterceptEvents = flag;
    }

    public void setFadeEnabled(boolean flag) {
        mFadeEnabled = flag;
    }

    public void setFadeOffset(long l) {
        mFadeOffset = l;
    }

    public void setGesture(Gesture gesture) {
        if(mCurrentGesture != null)
            clear(false);
        setCurrentColor(mCertainGestureColor);
        mCurrentGesture = gesture;
        Path path = mCurrentGesture.toPath();
        RectF rectf = new RectF();
        path.computeBounds(rectf, true);
        mPath.rewind();
        mPath.addPath(path, -rectf.left + ((float)getWidth() - rectf.width()) / 2.0F, -rectf.top + ((float)getHeight() - rectf.height()) / 2.0F);
        mResetGesture = true;
        invalidate();
    }

    public void setGestureColor(int i) {
        mCertainGestureColor = i;
    }

    public void setGestureStrokeAngleThreshold(float f) {
        mGestureStrokeAngleThreshold = f;
    }

    public void setGestureStrokeLengthThreshold(float f) {
        mGestureStrokeLengthThreshold = f;
    }

    public void setGestureStrokeSquarenessTreshold(float f) {
        mGestureStrokeSquarenessTreshold = f;
    }

    public void setGestureStrokeType(int i) {
        mGestureStrokeType = i;
    }

    public void setGestureStrokeWidth(float f) {
        mGestureStrokeWidth = f;
        mInvalidateExtraBorder = Math.max(1, -1 + (int)f);
        mGesturePaint.setStrokeWidth(f);
    }

    public void setGestureVisible(boolean flag) {
        mGestureVisible = flag;
    }

    public void setOrientation(int i) {
        mOrientation = i;
    }

    public void setUncertainGestureColor(int i) {
        mUncertainGestureColor = i;
    }

    private static final boolean DITHER_FLAG = true;
    private static final int FADE_ANIMATION_RATE = 16;
    private static final boolean GESTURE_RENDERING_ANTIALIAS = true;
    public static final int GESTURE_STROKE_TYPE_MULTIPLE = 1;
    public static final int GESTURE_STROKE_TYPE_SINGLE = 0;
    public static final int ORIENTATION_HORIZONTAL = 0;
    public static final int ORIENTATION_VERTICAL = 1;
    private int mCertainGestureColor;
    private int mCurrentColor;
    private Gesture mCurrentGesture;
    private float mCurveEndX;
    private float mCurveEndY;
    private long mFadeDuration;
    private boolean mFadeEnabled;
    private long mFadeOffset;
    private float mFadingAlpha;
    private boolean mFadingHasStarted;
    private final FadeOutRunnable mFadingOut;
    private long mFadingStart;
    private final Paint mGesturePaint;
    private float mGestureStrokeAngleThreshold;
    private float mGestureStrokeLengthThreshold;
    private float mGestureStrokeSquarenessTreshold;
    private int mGestureStrokeType;
    private float mGestureStrokeWidth;
    private boolean mGestureVisible;
    private boolean mHandleGestureActions;
    private boolean mInterceptEvents;
    private final AccelerateDecelerateInterpolator mInterpolator;
    private final Rect mInvalidRect;
    private int mInvalidateExtraBorder;
    private boolean mIsFadingOut;
    private boolean mIsGesturing;
    private boolean mIsListeningForGestures;
    private final ArrayList mOnGestureListeners;
    private final ArrayList mOnGesturePerformedListeners;
    private final ArrayList mOnGesturingListeners;
    private int mOrientation;
    private final Path mPath;
    private boolean mPreviousWasGesturing;
    private boolean mResetGesture;
    private final ArrayList mStrokeBuffer;
    private float mTotalLength;
    private int mUncertainGestureColor;
    private float mX;
    private float mY;




/*
    static float access$1002(GestureOverlayView gestureoverlayview, float f) {
        gestureoverlayview.mFadingAlpha = f;
        return f;
    }

*/


/*
    static boolean access$102(GestureOverlayView gestureoverlayview, boolean flag) {
        gestureoverlayview.mIsFadingOut = flag;
        return flag;
    }

*/



/*
    static boolean access$1202(GestureOverlayView gestureoverlayview, boolean flag) {
        gestureoverlayview.mResetGesture = flag;
        return flag;
    }

*/





/*
    static boolean access$502(GestureOverlayView gestureoverlayview, boolean flag) {
        gestureoverlayview.mPreviousWasGesturing = flag;
        return flag;
    }

*/


/*
    static boolean access$602(GestureOverlayView gestureoverlayview, boolean flag) {
        gestureoverlayview.mFadingHasStarted = flag;
        return flag;
    }

*/



/*
    static Gesture access$802(GestureOverlayView gestureoverlayview, Gesture gesture) {
        gestureoverlayview.mCurrentGesture = gesture;
        return gesture;
    }

*/

}
