// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.gesture;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import java.io.*;
import java.lang.ref.WeakReference;

// Referenced classes of package android.gesture:
//            GestureLibrary, GestureStore

public final class GestureLibraries {
    private static class ResourceGestureLibrary extends GestureLibrary {

        public boolean isReadOnly() {
            return true;
        }

        public boolean load() {
            boolean flag;
            Context context;
            java.io.InputStream inputstream;
            flag = false;
            context = (Context)mContext.get();
            if(context == null)
                break MISSING_BLOCK_LABEL_40;
            inputstream = context.getResources().openRawResource(mResourceId);
            super.mStore.load(inputstream, true);
            flag = true;
_L2:
            return flag;
            IOException ioexception;
            ioexception;
            Log.d("Gestures", (new StringBuilder()).append("Could not load the gesture library from raw resource ").append(context.getResources().getResourceName(mResourceId)).toString(), ioexception);
            if(true) goto _L2; else goto _L1
_L1:
        }

        public boolean save() {
            return false;
        }

        private final WeakReference mContext;
        private final int mResourceId;

        public ResourceGestureLibrary(Context context, int i) {
            mContext = new WeakReference(context);
            mResourceId = i;
        }
    }

    private static class FileGestureLibrary extends GestureLibrary {

        public boolean isReadOnly() {
            boolean flag;
            if(!mPath.canWrite())
                flag = true;
            else
                flag = false;
            return flag;
        }

        public boolean load() {
            boolean flag;
            File file;
            flag = false;
            file = mPath;
            if(!file.exists() || !file.canRead())
                break MISSING_BLOCK_LABEL_39;
            super.mStore.load(new FileInputStream(file), true);
            flag = true;
_L2:
            return flag;
            FileNotFoundException filenotfoundexception;
            filenotfoundexception;
            Log.d("Gestures", (new StringBuilder()).append("Could not load the gesture library from ").append(mPath).toString(), filenotfoundexception);
            continue; /* Loop/switch isn't completed */
            IOException ioexception;
            ioexception;
            Log.d("Gestures", (new StringBuilder()).append("Could not load the gesture library from ").append(mPath).toString(), ioexception);
            if(true) goto _L2; else goto _L1
_L1:
        }

        public boolean save() {
            boolean flag = true;
            if(super.mStore.hasChanged()) goto _L2; else goto _L1
_L1:
            return flag;
_L2:
            File file;
            file = mPath;
            File file1 = file.getParentFile();
            if(!file1.exists() && !file1.mkdirs()) {
                flag = false;
                continue; /* Loop/switch isn't completed */
            }
            flag = false;
            file.createNewFile();
            super.mStore.save(new FileOutputStream(file), true);
            flag = true;
            continue; /* Loop/switch isn't completed */
            FileNotFoundException filenotfoundexception;
            filenotfoundexception;
            Log.d("Gestures", (new StringBuilder()).append("Could not save the gesture library in ").append(mPath).toString(), filenotfoundexception);
            continue; /* Loop/switch isn't completed */
            IOException ioexception;
            ioexception;
            Log.d("Gestures", (new StringBuilder()).append("Could not save the gesture library in ").append(mPath).toString(), ioexception);
            if(true) goto _L1; else goto _L3
_L3:
        }

        private final File mPath;

        public FileGestureLibrary(File file) {
            mPath = file;
        }
    }


    private GestureLibraries() {
    }

    public static GestureLibrary fromFile(File file) {
        return new FileGestureLibrary(file);
    }

    public static GestureLibrary fromFile(String s) {
        return fromFile(new File(s));
    }

    public static GestureLibrary fromPrivateFile(Context context, String s) {
        return fromFile(context.getFileStreamPath(s));
    }

    public static GestureLibrary fromRawResource(Context context, int i) {
        return new ResourceGestureLibrary(context, i);
    }
}
