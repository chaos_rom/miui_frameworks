// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.gesture;

import java.io.*;
import java.util.*;

// Referenced classes of package android.gesture:
//            InstanceLearner, Gesture, Instance, Learner, 
//            GestureUtils

public class GestureStore {

    public GestureStore() {
        mSequenceType = 2;
        mOrientationStyle = 2;
        mChanged = false;
        mClassifier = new InstanceLearner();
    }

    private void readFormatV1(DataInputStream datainputstream) throws IOException {
        Learner learner = mClassifier;
        HashMap hashmap = mNamedGestures;
        hashmap.clear();
        int i = datainputstream.readInt();
        for(int j = 0; j < i; j++) {
            String s = datainputstream.readUTF();
            int k = datainputstream.readInt();
            ArrayList arraylist = new ArrayList(k);
            for(int l = 0; l < k; l++) {
                Gesture gesture = Gesture.deserialize(datainputstream);
                arraylist.add(gesture);
                learner.addInstance(Instance.createInstance(mSequenceType, mOrientationStyle, gesture, s));
            }

            hashmap.put(s, arraylist);
        }

    }

    public void addGesture(String s, Gesture gesture) {
        if(s != null && s.length() != 0) {
            ArrayList arraylist = (ArrayList)mNamedGestures.get(s);
            if(arraylist == null) {
                arraylist = new ArrayList();
                mNamedGestures.put(s, arraylist);
            }
            arraylist.add(gesture);
            mClassifier.addInstance(Instance.createInstance(mSequenceType, mOrientationStyle, gesture, s));
            mChanged = true;
        }
    }

    public Set getGestureEntries() {
        return mNamedGestures.keySet();
    }

    public ArrayList getGestures(String s) {
        ArrayList arraylist = (ArrayList)mNamedGestures.get(s);
        ArrayList arraylist1;
        if(arraylist != null)
            arraylist1 = new ArrayList(arraylist);
        else
            arraylist1 = null;
        return arraylist1;
    }

    Learner getLearner() {
        return mClassifier;
    }

    public int getOrientationStyle() {
        return mOrientationStyle;
    }

    public int getSequenceType() {
        return mSequenceType;
    }

    public boolean hasChanged() {
        return mChanged;
    }

    public void load(InputStream inputstream) throws IOException {
        load(inputstream, false);
    }

    public void load(InputStream inputstream, boolean flag) throws IOException {
        Object obj = null;
        if(!(inputstream instanceof BufferedInputStream)) goto _L2; else goto _L1
_L1:
        DataInputStream datainputstream = new DataInputStream(inputstream);
        short word0 = datainputstream.readShort();
        switch(word0) {
        default:
            if(flag)
                GestureUtils.closeStream(datainputstream);
            return;

        case 1: // '\001'
            break;
        }
        break MISSING_BLOCK_LABEL_76;
_L2:
        BufferedInputStream bufferedinputstream = new BufferedInputStream(inputstream, 32768);
        inputstream = bufferedinputstream;
          goto _L1
        readFormatV1(datainputstream);
        Exception exception;
        exception;
        obj = datainputstream;
_L4:
        if(flag)
            GestureUtils.closeStream(((java.io.Closeable) (obj)));
        throw exception;
        exception;
        if(true) goto _L4; else goto _L3
_L3:
    }

    public ArrayList recognize(Gesture gesture) {
        Instance instance = Instance.createInstance(mSequenceType, mOrientationStyle, gesture, null);
        return mClassifier.classify(mSequenceType, mOrientationStyle, instance.vector);
    }

    public void removeEntry(String s) {
        mNamedGestures.remove(s);
        mClassifier.removeInstances(s);
        mChanged = true;
    }

    public void removeGesture(String s, Gesture gesture) {
        ArrayList arraylist = (ArrayList)mNamedGestures.get(s);
        if(arraylist != null) {
            arraylist.remove(gesture);
            if(arraylist.isEmpty())
                mNamedGestures.remove(s);
            mClassifier.removeInstance(gesture.getID());
            mChanged = true;
        }
    }

    public void save(OutputStream outputstream) throws IOException {
        save(outputstream, false);
    }

    public void save(OutputStream outputstream, boolean flag) throws IOException {
        Object obj = null;
        HashMap hashmap = mNamedGestures;
        if(!(outputstream instanceof BufferedOutputStream)) goto _L2; else goto _L1
_L1:
        DataOutputStream dataoutputstream = new DataOutputStream(outputstream);
        Iterator iterator;
        dataoutputstream.writeShort(1);
        dataoutputstream.writeInt(hashmap.size());
        iterator = hashmap.entrySet().iterator();
_L6:
        ArrayList arraylist;
        int i;
        int j;
        if(!iterator.hasNext())
            break; /* Loop/switch isn't completed */
        java.util.Map.Entry entry = (java.util.Map.Entry)iterator.next();
        String s = (String)entry.getKey();
        arraylist = (ArrayList)entry.getValue();
        i = arraylist.size();
        dataoutputstream.writeUTF(s);
        dataoutputstream.writeInt(i);
        j = 0;
_L4:
        if(j >= i)
            break; /* Loop/switch isn't completed */
        ((Gesture)arraylist.get(j)).serialize(dataoutputstream);
        j++;
        if(true) goto _L4; else goto _L3
_L3:
        if(true) goto _L6; else goto _L5
_L2:
        BufferedOutputStream bufferedoutputstream = new BufferedOutputStream(outputstream, 32768);
        outputstream = bufferedoutputstream;
          goto _L1
_L5:
        dataoutputstream.flush();
        mChanged = false;
        if(flag)
            GestureUtils.closeStream(dataoutputstream);
        return;
        Exception exception;
        exception;
_L8:
        if(flag)
            GestureUtils.closeStream(((java.io.Closeable) (obj)));
        throw exception;
        exception;
        obj = dataoutputstream;
        if(true) goto _L8; else goto _L7
_L7:
    }

    public void setOrientationStyle(int i) {
        mOrientationStyle = i;
    }

    public void setSequenceType(int i) {
        mSequenceType = i;
    }

    private static final short FILE_FORMAT_VERSION = 1;
    public static final int ORIENTATION_INVARIANT = 1;
    public static final int ORIENTATION_SENSITIVE = 2;
    static final int ORIENTATION_SENSITIVE_4 = 4;
    static final int ORIENTATION_SENSITIVE_8 = 8;
    private static final boolean PROFILE_LOADING_SAVING = false;
    public static final int SEQUENCE_INVARIANT = 1;
    public static final int SEQUENCE_SENSITIVE = 2;
    private boolean mChanged;
    private Learner mClassifier;
    private final HashMap mNamedGestures = new HashMap();
    private int mOrientationStyle;
    private int mSequenceType;
}
