// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.gesture;

import java.util.ArrayList;

// Referenced classes of package android.gesture:
//            Gesture, GestureUtils, GestureStroke

class Instance {

    private Instance(long l, float af[], String s) {
        id = l;
        vector = af;
        label = s;
    }

    static Instance createInstance(int i, int j, Gesture gesture, String s) {
        Instance instance;
        if(i == 2) {
            float af1[] = temporalSampler(j, gesture);
            instance = new Instance(gesture.getID(), af1, s);
            instance.normalize();
        } else {
            float af[] = spatialSampler(gesture);
            instance = new Instance(gesture.getID(), af, s);
        }
        return instance;
    }

    private void normalize() {
        float af[] = vector;
        float f = 0.0F;
        int i = af.length;
        for(int j = 0; j < i; j++)
            f += af[j] * af[j];

        float f1 = (float)Math.sqrt(f);
        for(int k = 0; k < i; k++)
            af[k] = af[k] / f1;

    }

    private static float[] spatialSampler(Gesture gesture) {
        return GestureUtils.spatialSampling(gesture, 16, false);
    }

    private static float[] temporalSampler(int i, Gesture gesture) {
        float af[] = GestureUtils.temporalSampling((GestureStroke)gesture.getStrokes().get(0), 16);
        float af1[] = GestureUtils.computeCentroid(af);
        float f = (float)Math.atan2(af[1] - af1[1], af[0] - af1[0]);
        float f1 = -f;
        if(i != 1) {
            int j = ORIENTATIONS.length;
            for(int k = 0; k < j; k++) {
                float f2 = ORIENTATIONS[k] - f;
                if(Math.abs(f2) < Math.abs(f1))
                    f1 = f2;
            }

        }
        GestureUtils.translate(af, -af1[0], -af1[1]);
        GestureUtils.rotate(af, f1);
        return af;
    }

    private static final float ORIENTATIONS[];
    private static final int PATCH_SAMPLE_SIZE = 16;
    private static final int SEQUENCE_SAMPLE_SIZE = 16;
    final long id;
    final String label;
    final float vector[];

    static  {
        float af[] = new float[10];
        af[0] = 0.0F;
        af[1] = 0.7853982F;
        af[2] = 1.570796F;
        af[3] = 2.356194F;
        af[4] = 3.141593F;
        af[5] = 0.0F;
        af[6] = -0.7853982F;
        af[7] = -1.570796F;
        af[8] = -2.356194F;
        af[9] = -3.141593F;
        ORIENTATIONS = af;
    }
}
