// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.gesture;

import java.util.ArrayList;

// Referenced classes of package android.gesture:
//            Instance

abstract class Learner {

    Learner() {
    }

    void addInstance(Instance instance) {
        mInstances.add(instance);
    }

    abstract ArrayList classify(int i, int j, float af[]);

    ArrayList getInstances() {
        return mInstances;
    }

    void removeInstance(long l) {
        ArrayList arraylist = mInstances;
        int i = arraylist.size();
        int j = 0;
        do {
label0:
            {
                if(j < i) {
                    Instance instance = (Instance)arraylist.get(j);
                    if(l != instance.id)
                        break label0;
                    arraylist.remove(instance);
                }
                return;
            }
            j++;
        } while(true);
    }

    void removeInstances(String s) {
        ArrayList arraylist = new ArrayList();
        ArrayList arraylist1 = mInstances;
        int i = arraylist1.size();
        for(int j = 0; j < i; j++) {
            Instance instance = (Instance)arraylist1.get(j);
            if(instance.label == null && s == null || instance.label != null && instance.label.equals(s))
                arraylist.add(instance);
        }

        arraylist1.removeAll(arraylist);
    }

    private final ArrayList mInstances = new ArrayList();
}
