// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.filterfw.format;

import android.filterfw.core.MutableFrameFormat;
import android.filterfw.core.NativeBuffer;

public class ObjectFormat {

    public ObjectFormat() {
    }

    private static int bytesPerSampleForClass(Class class1, int i) {
        int j;
        if(i == 2) {
            if(!android/filterfw/core/NativeBuffer.isAssignableFrom(class1))
                throw new IllegalArgumentException((new StringBuilder()).append("Native object-based formats must be of a NativeBuffer subclass! (Received class: ").append(class1).append(").").toString());
            int k;
            try {
                k = ((NativeBuffer)class1.newInstance()).getElementSize();
            }
            catch(Exception exception) {
                throw new RuntimeException((new StringBuilder()).append("Could not determine the size of an element in a native object-based frame of type ").append(class1).append("! Perhaps it is missing a ").append("default constructor?").toString());
            }
            j = k;
        } else {
            j = 1;
        }
        return j;
    }

    public static MutableFrameFormat fromClass(Class class1, int i) {
        return fromClass(class1, 0, i);
    }

    public static MutableFrameFormat fromClass(Class class1, int i, int j) {
        MutableFrameFormat mutableframeformat = new MutableFrameFormat(8, j);
        mutableframeformat.setObjectClass(getBoxedClass(class1));
        if(i != 0)
            mutableframeformat.setDimensions(i);
        mutableframeformat.setBytesPerSample(bytesPerSampleForClass(class1, j));
        return mutableframeformat;
    }

    public static MutableFrameFormat fromObject(Object obj, int i) {
        MutableFrameFormat mutableframeformat;
        if(obj == null)
            mutableframeformat = new MutableFrameFormat(8, i);
        else
            mutableframeformat = fromClass(obj.getClass(), 0, i);
        return mutableframeformat;
    }

    public static MutableFrameFormat fromObject(Object obj, int i, int j) {
        MutableFrameFormat mutableframeformat;
        if(obj == null)
            mutableframeformat = new MutableFrameFormat(8, j);
        else
            mutableframeformat = fromClass(obj.getClass(), i, j);
        return mutableframeformat;
    }

    private static Class getBoxedClass(Class class1) {
        if(class1.isPrimitive())
            if(class1 == Boolean.TYPE)
                class1 = java/lang/Boolean;
            else
            if(class1 == Byte.TYPE)
                class1 = java/lang/Byte;
            else
            if(class1 == Character.TYPE)
                class1 = java/lang/Character;
            else
            if(class1 == Short.TYPE)
                class1 = java/lang/Short;
            else
            if(class1 == Integer.TYPE)
                class1 = java/lang/Integer;
            else
            if(class1 == Long.TYPE)
                class1 = java/lang/Long;
            else
            if(class1 == Float.TYPE)
                class1 = java/lang/Float;
            else
            if(class1 == Double.TYPE)
                class1 = java/lang/Double;
            else
                throw new IllegalArgumentException((new StringBuilder()).append("Unknown primitive type: ").append(class1.getSimpleName()).append("!").toString());
        return class1;
    }
}
