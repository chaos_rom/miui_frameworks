// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.filterfw.format;

import android.filterfw.core.MutableFrameFormat;
import android.graphics.Bitmap;

public class ImageFormat {

    public ImageFormat() {
    }

    public static int bytesPerSampleForColorspace(int i) {
        byte byte0 = 3;
        i;
        JVM INSTR tableswitch 1 4: default 32
    //                   1 64
    //                   2 66
    //                   3 68
    //                   4 66;
           goto _L1 _L2 _L3 _L4 _L3
_L3:
        break; /* Loop/switch isn't completed */
_L1:
        throw new RuntimeException((new StringBuilder()).append("Unknown colorspace id ").append(i).append("!").toString());
_L2:
        byte0 = 1;
_L6:
        return byte0;
_L4:
        byte0 = 4;
        if(true) goto _L6; else goto _L5
_L5:
    }

    public static MutableFrameFormat create(int i) {
        return create(0, 0, i, bytesPerSampleForColorspace(i), 0);
    }

    public static MutableFrameFormat create(int i, int j) {
        return create(0, 0, i, bytesPerSampleForColorspace(i), j);
    }

    public static MutableFrameFormat create(int i, int j, int k, int l) {
        return create(i, j, k, bytesPerSampleForColorspace(k), l);
    }

    public static MutableFrameFormat create(int i, int j, int k, int l, int i1) {
        MutableFrameFormat mutableframeformat = new MutableFrameFormat(2, i1);
        mutableframeformat.setDimensions(i, j);
        mutableframeformat.setBytesPerSample(l);
        mutableframeformat.setMetaValue("colorspace", Integer.valueOf(k));
        if(i1 == 1)
            mutableframeformat.setObjectClass(android/graphics/Bitmap);
        return mutableframeformat;
    }

    public static final int COLORSPACE_GRAY = 1;
    public static final String COLORSPACE_KEY = "colorspace";
    public static final int COLORSPACE_RGB = 2;
    public static final int COLORSPACE_RGBA = 3;
    public static final int COLORSPACE_YUV = 4;
}
