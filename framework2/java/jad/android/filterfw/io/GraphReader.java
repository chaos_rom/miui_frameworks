// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.filterfw.io;

import android.content.Context;
import android.content.res.Resources;
import android.filterfw.core.FilterGraph;
import android.filterfw.core.KeyValueMap;
import java.io.*;

// Referenced classes of package android.filterfw.io:
//            GraphIOException

public abstract class GraphReader {

    public GraphReader() {
        mReferences = new KeyValueMap();
    }

    public void addReference(String s, Object obj) {
        mReferences.put(s, obj);
    }

    public transient void addReferencesByKeysAndValues(Object aobj[]) {
        mReferences.setKeyValues(aobj);
    }

    public void addReferencesByMap(KeyValueMap keyvaluemap) {
        mReferences.putAll(keyvaluemap);
    }

    public FilterGraph readGraphResource(Context context, int i) throws GraphIOException {
        InputStreamReader inputstreamreader = new InputStreamReader(context.getResources().openRawResource(i));
        StringWriter stringwriter = new StringWriter();
        char ac[] = new char[1024];
        do
            try {
                int j = inputstreamreader.read(ac, 0, 1024);
                if(j <= 0)
                    break;
                stringwriter.write(ac, 0, j);
            }
            catch(IOException ioexception) {
                throw new RuntimeException("Could not read specified resource file!");
            }
        while(true);
        return readGraphString(stringwriter.toString());
    }

    public abstract FilterGraph readGraphString(String s) throws GraphIOException;

    public abstract KeyValueMap readKeyValueAssignments(String s) throws GraphIOException;

    protected KeyValueMap mReferences;
}
