// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.filterfw.io;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatternScanner {

    public PatternScanner(String s) {
        mOffset = 0;
        mLineNo = 0;
        mStartOfLine = 0;
        mInput = s;
    }

    public PatternScanner(String s, Pattern pattern) {
        mOffset = 0;
        mLineNo = 0;
        mStartOfLine = 0;
        mInput = s;
        mIgnorePattern = pattern;
        skip(mIgnorePattern);
    }

    public boolean atEnd() {
        boolean flag;
        if(mOffset >= mInput.length())
            flag = true;
        else
            flag = false;
        return flag;
    }

    public String eat(Pattern pattern, String s) {
        String s1 = tryEat(pattern);
        if(s1 == null)
            throw new RuntimeException(unexpectedTokenMessage(s));
        else
            return s1;
    }

    public int lineNo() {
        return mLineNo;
    }

    public boolean peek(Pattern pattern) {
        if(mIgnorePattern != null)
            skip(mIgnorePattern);
        Matcher matcher = pattern.matcher(mInput);
        matcher.region(mOffset, mInput.length());
        return matcher.lookingAt();
    }

    public void skip(Pattern pattern) {
        Matcher matcher = pattern.matcher(mInput);
        matcher.region(mOffset, mInput.length());
        if(matcher.lookingAt()) {
            updateLineCount(mOffset, matcher.end());
            mOffset = matcher.end();
        }
    }

    public String tryEat(Pattern pattern) {
        if(mIgnorePattern != null)
            skip(mIgnorePattern);
        Matcher matcher = pattern.matcher(mInput);
        matcher.region(mOffset, mInput.length());
        String s = null;
        if(matcher.lookingAt()) {
            updateLineCount(mOffset, matcher.end());
            mOffset = matcher.end();
            s = mInput.substring(matcher.start(), matcher.end());
        }
        if(s != null && mIgnorePattern != null)
            skip(mIgnorePattern);
        return s;
    }

    public String unexpectedTokenMessage(String s) {
        String s1 = mInput.substring(mStartOfLine, mOffset);
        return (new StringBuilder()).append("Unexpected token on line ").append(1 + mLineNo).append(" after '").append(s1).append("' <- Expected ").append(s).append("!").toString();
    }

    public void updateLineCount(int i, int j) {
        for(int k = i; k < j; k++)
            if(mInput.charAt(k) == '\n') {
                mLineNo = 1 + mLineNo;
                mStartOfLine = k + 1;
            }

    }

    private Pattern mIgnorePattern;
    private String mInput;
    private int mLineNo;
    private int mOffset;
    private int mStartOfLine;
}
