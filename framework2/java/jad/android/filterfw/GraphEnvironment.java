// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.filterfw;

import android.content.Context;
import android.filterfw.core.AsyncRunner;
import android.filterfw.core.FilterContext;
import android.filterfw.core.FilterGraph;
import android.filterfw.core.FrameManager;
import android.filterfw.core.GraphRunner;
import android.filterfw.core.RoundRobinScheduler;
import android.filterfw.core.SyncRunner;
import android.filterfw.io.GraphIOException;
import android.filterfw.io.GraphReader;
import android.filterfw.io.TextGraphReader;
import java.util.ArrayList;

// Referenced classes of package android.filterfw:
//            MffEnvironment

public class GraphEnvironment extends MffEnvironment {
    private class GraphHandle {

        public AsyncRunner getAsyncRunner(FilterContext filtercontext) {
            if(mAsyncRunner == null) {
                mAsyncRunner = new AsyncRunner(filtercontext, android/filterfw/core/RoundRobinScheduler);
                mAsyncRunner.setGraph(mGraph);
            }
            return mAsyncRunner;
        }

        public FilterGraph getGraph() {
            return mGraph;
        }

        public GraphRunner getSyncRunner(FilterContext filtercontext) {
            if(mSyncRunner == null)
                mSyncRunner = new SyncRunner(filtercontext, mGraph, android/filterfw/core/RoundRobinScheduler);
            return mSyncRunner;
        }

        private AsyncRunner mAsyncRunner;
        private FilterGraph mGraph;
        private SyncRunner mSyncRunner;
        final GraphEnvironment this$0;

        public GraphHandle(FilterGraph filtergraph) {
            this$0 = GraphEnvironment.this;
            super();
            mGraph = filtergraph;
        }
    }


    public GraphEnvironment() {
        super(null);
        mGraphs = new ArrayList();
    }

    public GraphEnvironment(FrameManager framemanager, GraphReader graphreader) {
        super(framemanager);
        mGraphs = new ArrayList();
        mGraphReader = graphreader;
    }

    public int addGraph(FilterGraph filtergraph) {
        GraphHandle graphhandle = new GraphHandle(filtergraph);
        mGraphs.add(graphhandle);
        return -1 + mGraphs.size();
    }

    public transient void addReferences(Object aobj[]) {
        getGraphReader().addReferencesByKeysAndValues(aobj);
    }

    public FilterGraph getGraph(int i) {
        if(i < 0 || i >= mGraphs.size())
            throw new IllegalArgumentException((new StringBuilder()).append("Invalid graph ID ").append(i).append(" specified in runGraph()!").toString());
        else
            return ((GraphHandle)mGraphs.get(i)).getGraph();
    }

    public GraphReader getGraphReader() {
        if(mGraphReader == null)
            mGraphReader = new TextGraphReader();
        return mGraphReader;
    }

    public GraphRunner getRunner(int i, int j) {
        j;
        JVM INSTR tableswitch 1 2: default 24
    //                   1 56
    //                   2 77;
           goto _L1 _L2 _L3
_L1:
        throw new RuntimeException((new StringBuilder()).append("Invalid execution mode ").append(j).append(" specified in getRunner()!").toString());
_L2:
        Object obj = ((GraphHandle)mGraphs.get(i)).getAsyncRunner(getContext());
_L5:
        return ((GraphRunner) (obj));
_L3:
        obj = ((GraphHandle)mGraphs.get(i)).getSyncRunner(getContext());
        if(true) goto _L5; else goto _L4
_L4:
    }

    public int loadGraph(Context context, int i) {
        FilterGraph filtergraph;
        try {
            filtergraph = getGraphReader().readGraphResource(context, i);
        }
        catch(GraphIOException graphioexception) {
            throw new RuntimeException((new StringBuilder()).append("Could not read graph: ").append(graphioexception.getMessage()).toString());
        }
        return addGraph(filtergraph);
    }

    public static final int MODE_ASYNCHRONOUS = 1;
    public static final int MODE_SYNCHRONOUS = 2;
    private GraphReader mGraphReader;
    private ArrayList mGraphs;
}
