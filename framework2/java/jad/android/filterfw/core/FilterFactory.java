// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.filterfw.core;

import android.util.Log;
import dalvik.system.PathClassLoader;
import java.lang.reflect.Constructor;
import java.util.HashSet;
import java.util.Iterator;

// Referenced classes of package android.filterfw.core:
//            Filter

public class FilterFactory {

    public FilterFactory() {
        mPackages = new HashSet();
    }

    public static void addFilterLibrary(String s) {
        if(mLogVerbose)
            Log.v("FilterFactory", (new StringBuilder()).append("Adding filter library ").append(s).toString());
        Object obj = mClassLoaderGuard;
        obj;
        JVM INSTR monitorenter ;
        if(mLibraries.contains(s)) {
            if(mLogVerbose)
                Log.v("FilterFactory", "Library already added");
        } else {
            mLibraries.add(s);
            mCurrentClassLoader = new PathClassLoader(s, mCurrentClassLoader);
        }
        return;
    }

    public static FilterFactory sharedFactory() {
        if(mSharedFactory == null)
            mSharedFactory = new FilterFactory();
        return mSharedFactory;
    }

    public void addPackage(String s) {
        if(mLogVerbose)
            Log.v("FilterFactory", (new StringBuilder()).append("Adding package ").append(s).toString());
        mPackages.add(s);
    }

    public Filter createFilterByClass(Class class1, String s) {
        Constructor constructor;
        Filter filter;
        try {
            class1.asSubclass(android/filterfw/core/Filter);
        }
        catch(ClassCastException classcastexception) {
            throw new IllegalArgumentException((new StringBuilder()).append("Attempting to allocate class '").append(class1).append("' which is not a subclass of Filter!").toString());
        }
        try {
            Class aclass[] = new Class[1];
            aclass[0] = java/lang/String;
            constructor = class1.getConstructor(aclass);
        }
        catch(NoSuchMethodException nosuchmethodexception) {
            throw new IllegalArgumentException((new StringBuilder()).append("The filter class '").append(class1).append("' does not have a constructor of the form <init>(String name)!").toString());
        }
        filter = null;
        try {
            Object aobj[] = new Object[1];
            aobj[0] = s;
            filter = (Filter)constructor.newInstance(aobj);
        }
        catch(Throwable throwable) { }
        if(filter == null)
            throw new IllegalArgumentException((new StringBuilder()).append("Could not construct the filter '").append(s).append("'!").toString());
        else
            return filter;
    }

    public Filter createFilterByClassName(String s, String s1) {
        Class class1;
        Iterator iterator;
        if(mLogVerbose)
            Log.v("FilterFactory", (new StringBuilder()).append("Looking up class ").append(s).toString());
        class1 = null;
        iterator = mPackages.iterator();
_L2:
        String s2;
        if(!iterator.hasNext())
            break MISSING_BLOCK_LABEL_151;
        s2 = (String)iterator.next();
        if(mLogVerbose)
            Log.v("FilterFactory", (new StringBuilder()).append("Trying ").append(s2).append(".").append(s).toString());
        synchronized(mClassLoaderGuard) {
            class1 = mCurrentClassLoader.loadClass((new StringBuilder()).append(s2).append(".").append(s).toString());
        }
        if(class1 == null)
            continue; /* Loop/switch isn't completed */
        if(class1 == null)
            throw new IllegalArgumentException((new StringBuilder()).append("Unknown filter class '").append(s).append("'!").toString());
        else
            return createFilterByClass(class1, s1);
        exception;
        obj;
        JVM INSTR monitorexit ;
        try {
            throw exception;
        }
        catch(ClassNotFoundException classnotfoundexception) { }
        if(true) goto _L2; else goto _L1
_L1:
    }

    private static final String TAG = "FilterFactory";
    private static Object mClassLoaderGuard = new Object();
    private static ClassLoader mCurrentClassLoader = Thread.currentThread().getContextClassLoader();
    private static HashSet mLibraries = new HashSet();
    private static boolean mLogVerbose = Log.isLoggable("FilterFactory", 2);
    private static FilterFactory mSharedFactory;
    private HashSet mPackages;

}
