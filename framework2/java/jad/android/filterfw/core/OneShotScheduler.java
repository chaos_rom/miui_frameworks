// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.filterfw.core;

import android.util.Log;
import java.util.HashMap;

// Referenced classes of package android.filterfw.core:
//            RoundRobinScheduler, Filter, FilterGraph

public class OneShotScheduler extends RoundRobinScheduler {

    public OneShotScheduler(FilterGraph filtergraph) {
        super(filtergraph);
        scheduled = new HashMap();
    }

    public void reset() {
        super.reset();
        scheduled.clear();
    }

    public Filter scheduleNextNode() {
        Filter filter = null;
_L6:
        Filter filter1 = super.scheduleNextNode();
        if(filter1 != null) goto _L2; else goto _L1
_L1:
        if(mLogVerbose)
            Log.v("OneShotScheduler", "No filters available to run.");
        filter1 = null;
_L4:
        return filter1;
_L2:
        if(!scheduled.containsKey(filter1.getName())) {
            if(filter1.getNumberOfConnectedInputs() == 0)
                scheduled.put(filter1.getName(), Integer.valueOf(1));
            if(mLogVerbose)
                Log.v("OneShotScheduler", (new StringBuilder()).append("Scheduling filter \"").append(filter1.getName()).append("\" of type ").append(filter1.getFilterClassName()).toString());
            continue; /* Loop/switch isn't completed */
        }
        if(filter != filter1)
            break; /* Loop/switch isn't completed */
        if(mLogVerbose)
            Log.v("OneShotScheduler", "One pass through graph completed.");
        filter1 = null;
        if(true) goto _L4; else goto _L3
_L3:
        if(filter == null)
            filter = filter1;
        if(true) goto _L6; else goto _L5
_L5:
    }

    private static final String TAG = "OneShotScheduler";
    private final boolean mLogVerbose = Log.isLoggable("OneShotScheduler", 2);
    private HashMap scheduled;
}
