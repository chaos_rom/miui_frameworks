// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.filterfw.core;

import java.io.StringWriter;
import java.util.*;

public class KeyValueMap extends HashMap {

    public KeyValueMap() {
    }

    public static transient KeyValueMap fromKeyValues(Object aobj[]) {
        KeyValueMap keyvaluemap = new KeyValueMap();
        keyvaluemap.setKeyValues(aobj);
        return keyvaluemap;
    }

    public float getFloat(String s) {
        Object obj = get(s);
        Float float1;
        if(obj != null)
            float1 = (Float)obj;
        else
            float1 = null;
        return float1.floatValue();
    }

    public int getInt(String s) {
        Object obj = get(s);
        Integer integer;
        if(obj != null)
            integer = (Integer)obj;
        else
            integer = null;
        return integer.intValue();
    }

    public String getString(String s) {
        Object obj = get(s);
        String s1;
        if(obj != null)
            s1 = (String)obj;
        else
            s1 = null;
        return s1;
    }

    public transient void setKeyValues(Object aobj[]) {
        if(aobj.length % 2 != 0)
            throw new RuntimeException("Key-Value arguments passed into setKeyValues must be an alternating list of keys and values!");
        for(int i = 0; i < aobj.length; i += 2) {
            if(!(aobj[i] instanceof String))
                throw new RuntimeException((new StringBuilder()).append("Key-value argument ").append(i).append(" must be a key of type ").append("String, but found an object of type ").append(aobj[i].getClass()).append("!").toString());
            put((String)aobj[i], aobj[i + 1]);
        }

    }

    public String toString() {
        StringWriter stringwriter = new StringWriter();
        Iterator iterator = entrySet().iterator();
        while(iterator.hasNext())  {
            java.util.Map.Entry entry = (java.util.Map.Entry)iterator.next();
            Object obj = entry.getValue();
            String s;
            if(obj instanceof String)
                s = (new StringBuilder()).append("\"").append(obj).append("\"").toString();
            else
                s = obj.toString();
            stringwriter.write((new StringBuilder()).append((String)entry.getKey()).append(" = ").append(s).append(";\n").toString());
        }
        return stringwriter.toString();
    }
}
