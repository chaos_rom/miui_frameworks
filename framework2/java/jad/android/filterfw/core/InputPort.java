// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.filterfw.core;


// Referenced classes of package android.filterfw.core:
//            FilterPort, OutputPort, Filter, FrameFormat, 
//            FilterContext

public abstract class InputPort extends FilterPort {

    public InputPort(Filter filter, String s) {
        super(filter, s);
    }

    public boolean acceptsFrame() {
        boolean flag;
        if(!hasFrame())
            flag = true;
        else
            flag = false;
        return flag;
    }

    public void close() {
        if(mSourcePort != null && mSourcePort.isOpen())
            mSourcePort.close();
        super.close();
    }

    public boolean filterMustClose() {
        boolean flag;
        if(!isOpen() && isBlocking() && !hasFrame())
            flag = true;
        else
            flag = false;
        return flag;
    }

    public Filter getSourceFilter() {
        Filter filter;
        if(mSourcePort == null)
            filter = null;
        else
            filter = mSourcePort.getFilter();
        return filter;
    }

    public FrameFormat getSourceFormat() {
        FrameFormat frameformat;
        if(mSourcePort != null)
            frameformat = mSourcePort.getPortFormat();
        else
            frameformat = getPortFormat();
        return frameformat;
    }

    public OutputPort getSourcePort() {
        return mSourcePort;
    }

    public Object getTarget() {
        return null;
    }

    public boolean isConnected() {
        boolean flag;
        if(mSourcePort != null)
            flag = true;
        else
            flag = false;
        return flag;
    }

    public boolean isReady() {
        boolean flag;
        if(hasFrame() || !isBlocking())
            flag = true;
        else
            flag = false;
        return flag;
    }

    public void open() {
        super.open();
        if(mSourcePort != null && !mSourcePort.isOpen())
            mSourcePort.open();
    }

    public void setSourcePort(OutputPort outputport) {
        if(mSourcePort != null) {
            throw new RuntimeException((new StringBuilder()).append(this).append(" already connected to ").append(mSourcePort).append("!").toString());
        } else {
            mSourcePort = outputport;
            return;
        }
    }

    public abstract void transfer(FilterContext filtercontext);

    protected OutputPort mSourcePort;
}
