// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.filterfw.core;

import java.util.Arrays;

// Referenced classes of package android.filterfw.core:
//            FrameFormat, KeyValueMap

public class MutableFrameFormat extends FrameFormat {

    public MutableFrameFormat() {
    }

    public MutableFrameFormat(int i, int j) {
        super(i, j);
    }

    public void setBaseType(int i) {
        super.mBaseType = i;
        super.mBytesPerSample = bytesPerSampleOf(i);
    }

    public void setBytesPerSample(int i) {
        super.mBytesPerSample = i;
        super.mSize = -1;
    }

    public void setDimensionCount(int i) {
        super.mDimensions = new int[i];
    }

    public void setDimensions(int i) {
        int ai[] = new int[1];
        ai[0] = i;
        super.mDimensions = ai;
        super.mSize = -1;
    }

    public void setDimensions(int i, int j) {
        int ai[] = new int[2];
        ai[0] = i;
        ai[1] = j;
        super.mDimensions = ai;
        super.mSize = -1;
    }

    public void setDimensions(int i, int j, int k) {
        int ai[] = new int[3];
        ai[0] = i;
        ai[1] = j;
        ai[2] = k;
        super.mDimensions = ai;
        super.mSize = -1;
    }

    public void setDimensions(int ai[]) {
        int ai1[];
        if(ai == null)
            ai1 = null;
        else
            ai1 = Arrays.copyOf(ai, ai.length);
        super.mDimensions = ai1;
        super.mSize = -1;
    }

    public void setMetaValue(String s, Object obj) {
        if(super.mMetaData == null)
            super.mMetaData = new KeyValueMap();
        super.mMetaData.put(s, obj);
    }

    public void setObjectClass(Class class1) {
        super.mObjectClass = class1;
    }

    public void setTarget(int i) {
        super.mTarget = i;
    }
}
