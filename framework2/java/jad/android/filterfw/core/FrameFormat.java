// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.filterfw.core;

import java.util.*;

// Referenced classes of package android.filterfw.core:
//            KeyValueMap, MutableFrameFormat

public class FrameFormat {

    protected FrameFormat() {
        mBaseType = 0;
        mBytesPerSample = 1;
        mSize = -1;
        mTarget = 0;
    }

    public FrameFormat(int i, int j) {
        mBaseType = 0;
        mBytesPerSample = 1;
        mSize = -1;
        mTarget = 0;
        mBaseType = i;
        mTarget = j;
        initDefaults();
    }

    public static String baseTypeToString(int i) {
        i;
        JVM INSTR tableswitch 0 8: default 52
    //                   0 57
    //                   1 63
    //                   2 69
    //                   3 75
    //                   4 81
    //                   5 87
    //                   6 93
    //                   7 99
    //                   8 105;
           goto _L1 _L2 _L3 _L4 _L5 _L6 _L7 _L8 _L9 _L10
_L1:
        String s = "unknown";
_L12:
        return s;
_L2:
        s = "unspecified";
        continue; /* Loop/switch isn't completed */
_L3:
        s = "bit";
        continue; /* Loop/switch isn't completed */
_L4:
        s = "byte";
        continue; /* Loop/switch isn't completed */
_L5:
        s = "int";
        continue; /* Loop/switch isn't completed */
_L6:
        s = "int";
        continue; /* Loop/switch isn't completed */
_L7:
        s = "float";
        continue; /* Loop/switch isn't completed */
_L8:
        s = "double";
        continue; /* Loop/switch isn't completed */
_L9:
        s = "pointer";
        continue; /* Loop/switch isn't completed */
_L10:
        s = "object";
        if(true) goto _L12; else goto _L11
_L11:
    }

    public static int bytesPerSampleOf(int i) {
        int j = 1;
        i;
        JVM INSTR tableswitch 1 7: default 44
    //                   1 44
    //                   2 44
    //                   3 46
    //                   4 51
    //                   5 51
    //                   6 56
    //                   7 51;
           goto _L1 _L1 _L1 _L2 _L3 _L3 _L4 _L3
_L1:
        return j;
_L2:
        j = 2;
        continue; /* Loop/switch isn't completed */
_L3:
        j = 4;
        continue; /* Loop/switch isn't completed */
_L4:
        j = 8;
        if(true) goto _L1; else goto _L5
_L5:
    }

    public static String dimensionsToString(int ai[]) {
        StringBuffer stringbuffer = new StringBuffer();
        if(ai != null) {
            int i = ai.length;
            int j = 0;
            while(j < i)  {
                if(ai[j] == 0)
                    stringbuffer.append("[]");
                else
                    stringbuffer.append((new StringBuilder()).append("[").append(String.valueOf(ai[j])).append("]").toString());
                j++;
            }
        }
        return stringbuffer.toString();
    }

    private void initDefaults() {
        mBytesPerSample = bytesPerSampleOf(mBaseType);
    }

    public static String metaDataToString(KeyValueMap keyvaluemap) {
        String s;
        if(keyvaluemap == null) {
            s = "";
        } else {
            StringBuffer stringbuffer = new StringBuffer();
            stringbuffer.append("{ ");
            java.util.Map.Entry entry;
            for(Iterator iterator = keyvaluemap.entrySet().iterator(); iterator.hasNext(); stringbuffer.append((new StringBuilder()).append((String)entry.getKey()).append(": ").append(entry.getValue()).append(" ").toString()))
                entry = (java.util.Map.Entry)iterator.next();

            stringbuffer.append("}");
            s = stringbuffer.toString();
        }
        return s;
    }

    public static int readTargetString(String s) {
        byte byte0;
        if(s.equalsIgnoreCase("CPU") || s.equalsIgnoreCase("NATIVE"))
            byte0 = 2;
        else
        if(s.equalsIgnoreCase("GPU"))
            byte0 = 3;
        else
        if(s.equalsIgnoreCase("SIMPLE"))
            byte0 = 1;
        else
        if(s.equalsIgnoreCase("VERTEXBUFFER"))
            byte0 = 4;
        else
        if(s.equalsIgnoreCase("UNSPECIFIED"))
            byte0 = 0;
        else
            throw new RuntimeException((new StringBuilder()).append("Unknown target type '").append(s).append("'!").toString());
        return byte0;
    }

    public static String targetToString(int i) {
        i;
        JVM INSTR tableswitch 0 5: default 40
    //                   0 45
    //                   1 51
    //                   2 57
    //                   3 63
    //                   4 69
    //                   5 75;
           goto _L1 _L2 _L3 _L4 _L5 _L6 _L7
_L1:
        String s = "unknown";
_L9:
        return s;
_L2:
        s = "unspecified";
        continue; /* Loop/switch isn't completed */
_L3:
        s = "simple";
        continue; /* Loop/switch isn't completed */
_L4:
        s = "native";
        continue; /* Loop/switch isn't completed */
_L5:
        s = "gpu";
        continue; /* Loop/switch isn't completed */
_L6:
        s = "vbo";
        continue; /* Loop/switch isn't completed */
_L7:
        s = "renderscript";
        if(true) goto _L9; else goto _L8
_L8:
    }

    public static FrameFormat unspecified() {
        return new FrameFormat(0, 0);
    }

    int calcSize(int ai[]) {
        int i;
        if(ai != null && ai.length > 0) {
            i = getBytesPerSample();
            int j = ai.length;
            for(int k = 0; k < j; k++)
                i *= ai[k];

        } else {
            i = 0;
        }
        return i;
    }

    public boolean equals(Object obj) {
        boolean flag = true;
        if(this != obj) goto _L2; else goto _L1
_L1:
        return flag;
_L2:
        if(!(obj instanceof FrameFormat)) {
            flag = false;
        } else {
            FrameFormat frameformat = (FrameFormat)obj;
            if(frameformat.mBaseType != mBaseType || frameformat.mTarget != mTarget || frameformat.mBytesPerSample != mBytesPerSample || !Arrays.equals(frameformat.mDimensions, mDimensions) || !frameformat.mMetaData.equals(mMetaData))
                flag = false;
        }
        if(true) goto _L1; else goto _L3
_L3:
    }

    public int getBaseType() {
        return mBaseType;
    }

    public int getBytesPerSample() {
        return mBytesPerSample;
    }

    public int getDepth() {
        int i;
        if(mDimensions != null && mDimensions.length >= 3)
            i = mDimensions[2];
        else
            i = -1;
        return i;
    }

    public int getDimension(int i) {
        return mDimensions[i];
    }

    public int getDimensionCount() {
        int i;
        if(mDimensions == null)
            i = 0;
        else
            i = mDimensions.length;
        return i;
    }

    public int[] getDimensions() {
        return mDimensions;
    }

    public int getHeight() {
        int i;
        if(mDimensions != null && mDimensions.length >= 2)
            i = mDimensions[1];
        else
            i = -1;
        return i;
    }

    public int getLength() {
        int i;
        if(mDimensions != null && mDimensions.length >= 1)
            i = mDimensions[0];
        else
            i = -1;
        return i;
    }

    public Object getMetaValue(String s) {
        Object obj;
        if(mMetaData != null)
            obj = mMetaData.get(s);
        else
            obj = null;
        return obj;
    }

    public int getNumberOfDimensions() {
        int i;
        if(mDimensions != null)
            i = mDimensions.length;
        else
            i = 0;
        return i;
    }

    public Class getObjectClass() {
        return mObjectClass;
    }

    public int getSize() {
        if(mSize == -1)
            mSize = calcSize(mDimensions);
        return mSize;
    }

    public int getTarget() {
        return mTarget;
    }

    public int getValuesPerSample() {
        return mBytesPerSample / bytesPerSampleOf(mBaseType);
    }

    public int getWidth() {
        return getLength();
    }

    public boolean hasMetaKey(String s) {
        boolean flag;
        if(mMetaData != null)
            flag = mMetaData.containsKey(s);
        else
            flag = false;
        return flag;
    }

    public boolean hasMetaKey(String s, Class class1) {
        boolean flag;
        if(mMetaData != null && mMetaData.containsKey(s)) {
            if(!class1.isAssignableFrom(mMetaData.get(s).getClass()))
                throw new RuntimeException((new StringBuilder()).append("FrameFormat meta-key '").append(s).append("' is of type ").append(mMetaData.get(s).getClass()).append(" but expected to be of type ").append(class1).append("!").toString());
            flag = true;
        } else {
            flag = false;
        }
        return flag;
    }

    public int hashCode() {
        return 0x1073 ^ mBaseType ^ mBytesPerSample ^ getSize();
    }

    public boolean isBinaryDataType() {
        boolean flag = true;
        if(mBaseType < flag || mBaseType > 6)
            flag = false;
        return flag;
    }

    public boolean isCompatibleWith(FrameFormat frameformat) {
        boolean flag;
        flag = false;
        break MISSING_BLOCK_LABEL_2;
label0:
        while(true)  {
            do
                return flag;
            while(frameformat.getBaseType() != 0 && getBaseType() != frameformat.getBaseType() || frameformat.getTarget() != 0 && getTarget() != frameformat.getTarget() || frameformat.getBytesPerSample() != 1 && getBytesPerSample() != frameformat.getBytesPerSample() || frameformat.getDimensionCount() > 0 && getDimensionCount() != frameformat.getDimensionCount());
label1:
            {
                for(int i = 0; i < frameformat.getDimensionCount(); i++) {
                    int j = frameformat.getDimension(i);
                    if(j != 0 && getDimension(i) != j)
                        continue label0;
                }

                if(frameformat.getObjectClass() != null && (getObjectClass() == null || !frameformat.getObjectClass().isAssignableFrom(getObjectClass())))
                    continue;
                if(frameformat.mMetaData == null)
                    break label1;
                Iterator iterator = frameformat.mMetaData.keySet().iterator();
                String s;
                do {
                    if(!iterator.hasNext())
                        break label1;
                    s = (String)iterator.next();
                } while(mMetaData != null && mMetaData.containsKey(s) && mMetaData.get(s).equals(frameformat.mMetaData.get(s)));
                continue;
            }
            flag = true;
        }
    }

    boolean isReplaceableBy(FrameFormat frameformat) {
        boolean flag;
        if(mTarget == frameformat.mTarget && getSize() == frameformat.getSize() && Arrays.equals(frameformat.mDimensions, mDimensions))
            flag = true;
        else
            flag = false;
        return flag;
    }

    public boolean mayBeCompatibleWith(FrameFormat frameformat) {
        boolean flag;
        flag = false;
        break MISSING_BLOCK_LABEL_2;
label0:
        while(true)  {
            do
                return flag;
            while(frameformat.getBaseType() != 0 && getBaseType() != 0 && getBaseType() != frameformat.getBaseType() || frameformat.getTarget() != 0 && getTarget() != 0 && getTarget() != frameformat.getTarget() || frameformat.getBytesPerSample() != 1 && getBytesPerSample() != 1 && getBytesPerSample() != frameformat.getBytesPerSample() || frameformat.getDimensionCount() > 0 && getDimensionCount() > 0 && getDimensionCount() != frameformat.getDimensionCount());
label1:
            {
                for(int i = 0; i < frameformat.getDimensionCount(); i++) {
                    int j = frameformat.getDimension(i);
                    if(j != 0 && getDimension(i) != 0 && getDimension(i) != j)
                        continue label0;
                }

                if(frameformat.getObjectClass() != null && getObjectClass() != null && !frameformat.getObjectClass().isAssignableFrom(getObjectClass()))
                    continue;
                if(frameformat.mMetaData == null || mMetaData == null)
                    break label1;
                Iterator iterator = frameformat.mMetaData.keySet().iterator();
                String s;
                do {
                    if(!iterator.hasNext())
                        break label1;
                    s = (String)iterator.next();
                } while(!mMetaData.containsKey(s) || mMetaData.get(s).equals(frameformat.mMetaData.get(s)));
                continue;
            }
            flag = true;
        }
    }

    public MutableFrameFormat mutableCopy() {
        MutableFrameFormat mutableframeformat = new MutableFrameFormat();
        mutableframeformat.setBaseType(getBaseType());
        mutableframeformat.setTarget(getTarget());
        mutableframeformat.setBytesPerSample(getBytesPerSample());
        mutableframeformat.setDimensions(getDimensions());
        mutableframeformat.setObjectClass(getObjectClass());
        KeyValueMap keyvaluemap;
        if(mMetaData == null)
            keyvaluemap = null;
        else
            keyvaluemap = (KeyValueMap)mMetaData.clone();
        mutableframeformat.mMetaData = keyvaluemap;
        return mutableframeformat;
    }

    public String toString() {
        int i = getValuesPerSample();
        String s;
        String s1;
        String s2;
        if(i == 1)
            s = "";
        else
            s = String.valueOf(i);
        if(mTarget == 0)
            s1 = "";
        else
            s1 = (new StringBuilder()).append(targetToString(mTarget)).append(" ").toString();
        if(mObjectClass == null)
            s2 = "";
        else
            s2 = (new StringBuilder()).append(" class(").append(mObjectClass.getSimpleName()).append(") ").toString();
        return (new StringBuilder()).append(s1).append(baseTypeToString(mBaseType)).append(s).append(dimensionsToString(mDimensions)).append(s2).append(metaDataToString(mMetaData)).toString();
    }

    public static final int BYTES_PER_SAMPLE_UNSPECIFIED = 1;
    protected static final int SIZE_UNKNOWN = -1;
    public static final int SIZE_UNSPECIFIED = 0;
    public static final int TARGET_GPU = 3;
    public static final int TARGET_NATIVE = 2;
    public static final int TARGET_RS = 5;
    public static final int TARGET_SIMPLE = 1;
    public static final int TARGET_UNSPECIFIED = 0;
    public static final int TARGET_VERTEXBUFFER = 4;
    public static final int TYPE_BIT = 1;
    public static final int TYPE_BYTE = 2;
    public static final int TYPE_DOUBLE = 6;
    public static final int TYPE_FLOAT = 5;
    public static final int TYPE_INT16 = 3;
    public static final int TYPE_INT32 = 4;
    public static final int TYPE_OBJECT = 8;
    public static final int TYPE_POINTER = 7;
    public static final int TYPE_UNSPECIFIED;
    protected int mBaseType;
    protected int mBytesPerSample;
    protected int mDimensions[];
    protected KeyValueMap mMetaData;
    protected Class mObjectClass;
    protected int mSize;
    protected int mTarget;
}
