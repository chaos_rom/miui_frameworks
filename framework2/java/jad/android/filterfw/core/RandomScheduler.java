// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.filterfw.core;

import java.util.*;

// Referenced classes of package android.filterfw.core:
//            Scheduler, FilterGraph, Filter

public class RandomScheduler extends Scheduler {

    public RandomScheduler(FilterGraph filtergraph) {
        super(filtergraph);
        mRand = new Random();
    }

    public void reset() {
    }

    public Filter scheduleNextNode() {
        Vector vector = new Vector();
        Iterator iterator = getGraph().getFilters().iterator();
        do {
            if(!iterator.hasNext())
                break;
            Filter filter1 = (Filter)iterator.next();
            if(filter1.canProcess())
                vector.add(filter1);
        } while(true);
        Filter filter;
        if(vector.size() > 0)
            filter = (Filter)vector.elementAt(mRand.nextInt(vector.size()));
        else
            filter = null;
        return filter;
    }

    private Random mRand;
}
