// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.filterfw.core;

import java.lang.reflect.Field;

// Referenced classes of package android.filterfw.core:
//            FieldPort, FilterPort, Program, Filter, 
//            FilterContext

public class ProgramPort extends FieldPort {

    public ProgramPort(Filter filter, String s, String s1, Field field, boolean flag) {
        super(filter, s, field, flag);
        mVarName = s1;
    }

    public String toString() {
        return (new StringBuilder()).append("Program ").append(super.toString()).toString();
    }

    /**
     * @deprecated Method transfer is deprecated
     */

    public void transfer(FilterContext filtercontext) {
        this;
        JVM INSTR monitorenter ;
        boolean flag = super.mValueWaiting;
        if(!flag)
            break MISSING_BLOCK_LABEL_50;
        Object obj = super.mField.get(super.mFilter);
        if(obj != null) {
            ((Program)obj).setHostValue(mVarName, super.mValue);
            super.mValueWaiting = false;
        }
        this;
        JVM INSTR monitorexit ;
        return;
        IllegalAccessException illegalaccessexception;
        illegalaccessexception;
        throw new RuntimeException((new StringBuilder()).append("Access to program field '").append(super.mField.getName()).append("' was denied!").toString());
        Exception exception;
        exception;
        this;
        JVM INSTR monitorexit ;
        throw exception;
        ClassCastException classcastexception;
        classcastexception;
        throw new RuntimeException((new StringBuilder()).append("Non Program field '").append(super.mField.getName()).append("' annotated with ProgramParameter!").toString());
    }

    protected String mVarName;
}
