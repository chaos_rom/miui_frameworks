// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.filterfw.core;

import java.lang.annotation.Annotation;

public interface GenerateProgramPort
    extends Annotation {

    public abstract boolean hasDefault();

    public abstract String name();

    public abstract Class type();

    public abstract String variableName();
}
