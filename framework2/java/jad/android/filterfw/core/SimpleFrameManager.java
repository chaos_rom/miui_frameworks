// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.filterfw.core;


// Referenced classes of package android.filterfw.core:
//            FrameManager, FrameFormat, SimpleFrame, NativeFrame, 
//            GLFrame, VertexFrame, Frame

public class SimpleFrameManager extends FrameManager {

    public SimpleFrameManager() {
    }

    private Frame createNewFrame(FrameFormat frameformat) {
        frameformat.getTarget();
        JVM INSTR tableswitch 1 4: default 36
    //                   1 74
    //                   2 86
    //                   3 99
    //                   4 122;
           goto _L1 _L2 _L3 _L4 _L5
_L1:
        throw new RuntimeException((new StringBuilder()).append("Unsupported frame target type: ").append(FrameFormat.targetToString(frameformat.getTarget())).append("!").toString());
_L2:
        Object obj = new SimpleFrame(frameformat, this);
_L7:
        return ((Frame) (obj));
_L3:
        obj = new NativeFrame(frameformat, this);
        continue; /* Loop/switch isn't completed */
_L4:
        GLFrame glframe = new GLFrame(frameformat, this);
        glframe.init(getGLEnvironment());
        obj = glframe;
        continue; /* Loop/switch isn't completed */
_L5:
        obj = new VertexFrame(frameformat, this);
        if(true) goto _L7; else goto _L6
_L6:
    }

    public Frame newBoundFrame(FrameFormat frameformat, int i, long l) {
        GLFrame glframe;
        switch(frameformat.getTarget()) {
        default:
            throw new RuntimeException((new StringBuilder()).append("Attached frames are not supported for target type: ").append(FrameFormat.targetToString(frameformat.getTarget())).append("!").toString());

        case 3: // '\003'
            glframe = new GLFrame(frameformat, this, i, l);
            break;
        }
        glframe.init(getGLEnvironment());
        return glframe;
    }

    public Frame newFrame(FrameFormat frameformat) {
        return createNewFrame(frameformat);
    }

    public Frame releaseFrame(Frame frame) {
        int i = frame.decRefCount();
        if(i == 0 && frame.hasNativeAllocation()) {
            frame.releaseNativeAllocation();
            frame = null;
        } else
        if(i < 0)
            throw new RuntimeException("Frame reference count dropped below 0!");
        return frame;
    }

    public Frame retainFrame(Frame frame) {
        frame.incRefCount();
        return frame;
    }
}
