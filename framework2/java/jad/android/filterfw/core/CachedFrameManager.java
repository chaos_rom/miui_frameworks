// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.filterfw.core;

import java.util.*;

// Referenced classes of package android.filterfw.core:
//            SimpleFrameManager, Frame, FrameFormat

public class CachedFrameManager extends SimpleFrameManager {

    public CachedFrameManager() {
        mStorageCapacity = 0x1800000;
        mStorageSize = 0;
        mTimeStamp = 0;
        mAvailableFrames = new TreeMap();
    }

    private void dropOldestFrame() {
        int i = ((Integer)mAvailableFrames.firstKey()).intValue();
        Frame frame = (Frame)mAvailableFrames.get(Integer.valueOf(i));
        mStorageSize = mStorageSize - frame.getFormat().getSize();
        frame.releaseNativeAllocation();
        mAvailableFrames.remove(Integer.valueOf(i));
    }

    private Frame findAvailableFrame(FrameFormat frameformat, int i, long l) {
        SortedMap sortedmap = mAvailableFrames;
        sortedmap;
        JVM INSTR monitorenter ;
        Frame frame;
label0:
        {
            for(Iterator iterator = mAvailableFrames.entrySet().iterator(); iterator.hasNext();) {
                java.util.Map.Entry entry = (java.util.Map.Entry)iterator.next();
                frame = (Frame)entry.getValue();
                if(frame.getFormat().isReplaceableBy(frameformat) && i == frame.getBindingType() && (i == 0 || l == frame.getBindingId())) {
                    super.retainFrame(frame);
                    mAvailableFrames.remove(entry.getKey());
                    frame.onFrameFetch();
                    frame.reset(frameformat);
                    mStorageSize = mStorageSize - frameformat.getSize();
                    break label0;
                }
            }

            frame = null;
        }
        return frame;
    }

    private boolean storeFrame(Frame frame) {
        SortedMap sortedmap = mAvailableFrames;
        sortedmap;
        JVM INSTR monitorenter ;
        int i = frame.getFormat().getSize();
        boolean flag;
        if(i > mStorageCapacity) {
            flag = false;
        } else {
            int j;
            for(j = i + mStorageSize; j > mStorageCapacity; j = i + mStorageSize)
                dropOldestFrame();

            frame.onFrameStore();
            mStorageSize = j;
            mAvailableFrames.put(Integer.valueOf(mTimeStamp), frame);
            mTimeStamp = 1 + mTimeStamp;
            flag = true;
        }
        return flag;
    }

    public void clearCache() {
        for(Iterator iterator = mAvailableFrames.values().iterator(); iterator.hasNext(); ((Frame)iterator.next()).releaseNativeAllocation());
        mAvailableFrames.clear();
    }

    public Frame newBoundFrame(FrameFormat frameformat, int i, long l) {
        Frame frame = findAvailableFrame(frameformat, i, l);
        if(frame == null)
            frame = super.newBoundFrame(frameformat, i, l);
        frame.setTimestamp(-2L);
        return frame;
    }

    public Frame newFrame(FrameFormat frameformat) {
        Frame frame = findAvailableFrame(frameformat, 0, 0L);
        if(frame == null)
            frame = super.newFrame(frameformat);
        frame.setTimestamp(-2L);
        return frame;
    }

    public Frame releaseFrame(Frame frame) {
        if(!frame.isReusable()) goto _L2; else goto _L1
_L1:
        int i = frame.decRefCount();
        if(i != 0 || !frame.hasNativeAllocation()) goto _L4; else goto _L3
_L3:
        if(!storeFrame(frame))
            frame.releaseNativeAllocation();
        frame = null;
_L6:
        return frame;
_L4:
        if(i < 0)
            throw new RuntimeException("Frame reference count dropped below 0!");
        continue; /* Loop/switch isn't completed */
_L2:
        super.releaseFrame(frame);
        if(true) goto _L6; else goto _L5
_L5:
    }

    public Frame retainFrame(Frame frame) {
        return super.retainFrame(frame);
    }

    public void tearDown() {
        clearCache();
    }

    private SortedMap mAvailableFrames;
    private int mStorageCapacity;
    private int mStorageSize;
    private int mTimeStamp;
}
