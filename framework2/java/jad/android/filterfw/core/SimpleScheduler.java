// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.filterfw.core;

import java.util.Iterator;
import java.util.Set;

// Referenced classes of package android.filterfw.core:
//            Scheduler, FilterGraph, Filter

public class SimpleScheduler extends Scheduler {

    public SimpleScheduler(FilterGraph filtergraph) {
        super(filtergraph);
    }

    public void reset() {
    }

    public Filter scheduleNextNode() {
        Iterator iterator = getGraph().getFilters().iterator();
_L4:
        if(!iterator.hasNext()) goto _L2; else goto _L1
_L1:
        Filter filter = (Filter)iterator.next();
        if(!filter.canProcess()) goto _L4; else goto _L3
_L3:
        return filter;
_L2:
        filter = null;
        if(true) goto _L3; else goto _L5
_L5:
    }
}
