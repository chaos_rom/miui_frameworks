// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.filterfw.core;


// Referenced classes of package android.filterfw.core:
//            InputPort, Frame, Filter, FilterContext

public class StreamPort extends InputPort {

    public StreamPort(Filter filter, String s) {
        super(filter, s);
    }

    /**
     * @deprecated Method assignFrame is deprecated
     */

    protected void assignFrame(Frame frame, boolean flag) {
        this;
        JVM INSTR monitorenter ;
        assertPortIsOpen();
        checkFrameType(frame, flag);
        if(!flag) goto _L2; else goto _L1
_L1:
        if(mFrame != null)
            mFrame.release();
_L4:
        mFrame = frame.retain();
        mFrame.markReadOnly();
        mPersistent = flag;
        this;
        JVM INSTR monitorexit ;
        return;
_L2:
        if(mFrame == null) goto _L4; else goto _L3
_L3:
        throw new RuntimeException((new StringBuilder()).append("Attempting to push more than one frame on port: ").append(this).append("!").toString());
        Exception exception;
        exception;
        this;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public void clear() {
        if(mFrame != null) {
            mFrame.release();
            mFrame = null;
        }
    }

    /**
     * @deprecated Method hasFrame is deprecated
     */

    public boolean hasFrame() {
        this;
        JVM INSTR monitorenter ;
        Frame frame = mFrame;
        boolean flag;
        if(frame != null)
            flag = true;
        else
            flag = false;
        this;
        JVM INSTR monitorexit ;
        return flag;
        Exception exception;
        exception;
        throw exception;
    }

    /**
     * @deprecated Method pullFrame is deprecated
     */

    public Frame pullFrame() {
        this;
        JVM INSTR monitorenter ;
        if(mFrame == null)
            throw new RuntimeException((new StringBuilder()).append("No frame available to pull on port: ").append(this).append("!").toString());
        break MISSING_BLOCK_LABEL_46;
        Exception exception;
        exception;
        this;
        JVM INSTR monitorexit ;
        throw exception;
        Frame frame;
        frame = mFrame;
        if(!mPersistent)
            break MISSING_BLOCK_LABEL_70;
        mFrame.retain();
_L1:
        this;
        JVM INSTR monitorexit ;
        return frame;
        mFrame = null;
          goto _L1
    }

    public void pushFrame(Frame frame) {
        assignFrame(frame, false);
    }

    public void setFrame(Frame frame) {
        assignFrame(frame, true);
    }

    public String toString() {
        return (new StringBuilder()).append("input ").append(super.toString()).toString();
    }

    /**
     * @deprecated Method transfer is deprecated
     */

    public void transfer(FilterContext filtercontext) {
        this;
        JVM INSTR monitorenter ;
        if(mFrame != null)
            checkFrameManager(mFrame, filtercontext);
        this;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        throw exception;
    }

    private Frame mFrame;
    private boolean mPersistent;
}
