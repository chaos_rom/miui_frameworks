// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.filterfw.core;

import android.filterfw.format.ObjectFormat;
import android.graphics.Bitmap;
import java.io.*;
import java.nio.ByteBuffer;

// Referenced classes of package android.filterfw.core:
//            Frame, FrameFormat, FrameManager

public class SerializedFrame extends Frame {
    private class DirectByteInputStream extends InputStream {

        public final int available() {
            return mSize - mPos;
        }

        public final int read() {
            int i;
            if(mPos < mSize) {
                byte abyte0[] = mBuffer;
                int j = mPos;
                mPos = j + 1;
                i = 0xff & abyte0[j];
            } else {
                i = -1;
            }
            return i;
        }

        public final int read(byte abyte0[], int i, int j) {
            int k;
            if(mPos >= mSize) {
                k = -1;
            } else {
                if(j + mPos > mSize)
                    j = mSize - mPos;
                System.arraycopy(mBuffer, mPos, abyte0, i, j);
                mPos = j + mPos;
                k = j;
            }
            return k;
        }

        public final long skip(long l) {
            if(l + (long)mPos > (long)mSize)
                l = mSize - mPos;
            if(l < 0L)
                l = 0L;
            else
                mPos = (int)(l + (long)mPos);
            return l;
        }

        private byte mBuffer[];
        private int mPos;
        private int mSize;
        final SerializedFrame this$0;

        public DirectByteInputStream(byte abyte0[], int i) {
            this$0 = SerializedFrame.this;
            super();
            mPos = 0;
            mBuffer = abyte0;
            mSize = i;
        }
    }

    private class DirectByteOutputStream extends OutputStream {

        private final void ensureFit(int i) {
            if(i + mOffset > mBuffer.length) {
                byte abyte0[] = mBuffer;
                mBuffer = new byte[Math.max(i + mOffset, 2 * mBuffer.length)];
                System.arraycopy(abyte0, 0, mBuffer, 0, mOffset);
            }
        }

        public byte[] getByteArray() {
            return mBuffer;
        }

        public final DirectByteInputStream getInputStream() {
            return new DirectByteInputStream(mBuffer, mOffset);
        }

        public final int getSize() {
            return mOffset;
        }

        public final void markHeaderEnd() {
            mDataOffset = mOffset;
        }

        public final void reset() {
            mOffset = mDataOffset;
        }

        public final void write(int i) {
            ensureFit(1);
            byte abyte0[] = mBuffer;
            int j = mOffset;
            mOffset = j + 1;
            abyte0[j] = (byte)i;
        }

        public final void write(byte abyte0[]) {
            write(abyte0, 0, abyte0.length);
        }

        public final void write(byte abyte0[], int i, int j) {
            ensureFit(j);
            System.arraycopy(abyte0, i, mBuffer, mOffset, j);
            mOffset = j + mOffset;
        }

        private byte mBuffer[];
        private int mDataOffset;
        private int mOffset;
        final SerializedFrame this$0;

        public DirectByteOutputStream(int i) {
            this$0 = SerializedFrame.this;
            super();
            mBuffer = null;
            mOffset = 0;
            mDataOffset = 0;
            mBuffer = new byte[i];
        }
    }


    SerializedFrame(FrameFormat frameformat, FrameManager framemanager) {
        super(frameformat, framemanager);
        setReusable(false);
        try {
            mByteOutputStream = new DirectByteOutputStream(64);
            mObjectOut = new ObjectOutputStream(mByteOutputStream);
            mByteOutputStream.markHeaderEnd();
            return;
        }
        catch(IOException ioexception) {
            throw new RuntimeException("Could not create serialization streams for SerializedFrame!", ioexception);
        }
    }

    private final Object deserializeObjectValue() {
        Object obj;
        try {
            obj = (new ObjectInputStream(mByteOutputStream.getInputStream())).readObject();
        }
        catch(IOException ioexception) {
            throw new RuntimeException((new StringBuilder()).append("Could not deserialize object in ").append(this).append("!").toString(), ioexception);
        }
        catch(ClassNotFoundException classnotfoundexception) {
            throw new RuntimeException((new StringBuilder()).append("Unable to deserialize object of unknown class in ").append(this).append("!").toString(), classnotfoundexception);
        }
        return obj;
    }

    private final void serializeObjectValue(Object obj) {
        try {
            mByteOutputStream.reset();
            mObjectOut.writeObject(obj);
            mObjectOut.flush();
            mObjectOut.close();
            return;
        }
        catch(IOException ioexception) {
            throw new RuntimeException((new StringBuilder()).append("Could not serialize object ").append(obj).append(" in ").append(this).append("!").toString(), ioexception);
        }
    }

    static SerializedFrame wrapObject(Object obj, FrameManager framemanager) {
        SerializedFrame serializedframe = new SerializedFrame(ObjectFormat.fromObject(obj, 1), framemanager);
        serializedframe.setObjectValue(obj);
        return serializedframe;
    }

    public Bitmap getBitmap() {
        Object obj = deserializeObjectValue();
        Bitmap bitmap;
        if(obj instanceof Bitmap)
            bitmap = (Bitmap)obj;
        else
            bitmap = null;
        return bitmap;
    }

    public ByteBuffer getData() {
        Object obj = deserializeObjectValue();
        ByteBuffer bytebuffer;
        if(obj instanceof ByteBuffer)
            bytebuffer = (ByteBuffer)obj;
        else
            bytebuffer = null;
        return bytebuffer;
    }

    public float[] getFloats() {
        Object obj = deserializeObjectValue();
        float af[];
        if(obj instanceof float[])
            af = (float[])(float[])obj;
        else
            af = null;
        return af;
    }

    public int[] getInts() {
        Object obj = deserializeObjectValue();
        int ai[];
        if(obj instanceof int[])
            ai = (int[])(int[])obj;
        else
            ai = null;
        return ai;
    }

    public Object getObjectValue() {
        return deserializeObjectValue();
    }

    protected boolean hasNativeAllocation() {
        return false;
    }

    protected void releaseNativeAllocation() {
    }

    public void setBitmap(Bitmap bitmap) {
        assertFrameMutable();
        setGenericObjectValue(bitmap);
    }

    public void setData(ByteBuffer bytebuffer, int i, int j) {
        assertFrameMutable();
        setGenericObjectValue(ByteBuffer.wrap(bytebuffer.array(), i, j));
    }

    public void setFloats(float af[]) {
        assertFrameMutable();
        setGenericObjectValue(af);
    }

    protected void setGenericObjectValue(Object obj) {
        serializeObjectValue(obj);
    }

    public void setInts(int ai[]) {
        assertFrameMutable();
        setGenericObjectValue(ai);
    }

    public String toString() {
        return (new StringBuilder()).append("SerializedFrame (").append(getFormat()).append(")").toString();
    }

    private static final int INITIAL_CAPACITY = 64;
    private DirectByteOutputStream mByteOutputStream;
    private ObjectOutputStream mObjectOut;
}
