// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.filterfw.core;

import android.os.ConditionVariable;
import android.util.Log;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

// Referenced classes of package android.filterfw.core:
//            GraphRunner, Scheduler, FilterGraph, FilterContext, 
//            StopWatchMap, Filter

public class SyncRunner extends GraphRunner {

    public SyncRunner(FilterContext filtercontext, FilterGraph filtergraph, Class class1) {
        super(filtercontext);
        mScheduler = null;
        mDoneListener = null;
        mWakeExecutor = new ScheduledThreadPoolExecutor(1);
        mWakeCondition = new ConditionVariable();
        mTimer = null;
        if(mLogVerbose)
            Log.v("SyncRunner", "Initializing SyncRunner");
        if(android/filterfw/core/Scheduler.isAssignableFrom(class1)) {
            try {
                Class aclass[] = new Class[1];
                aclass[0] = android/filterfw/core/FilterGraph;
                Constructor constructor = class1.getConstructor(aclass);
                Object aobj[] = new Object[1];
                aobj[0] = filtergraph;
                mScheduler = (Scheduler)constructor.newInstance(aobj);
            }
            catch(NoSuchMethodException nosuchmethodexception) {
                throw new RuntimeException("Scheduler does not have constructor <init>(FilterGraph)!", nosuchmethodexception);
            }
            catch(InstantiationException instantiationexception) {
                throw new RuntimeException("Could not instantiate the Scheduler instance!", instantiationexception);
            }
            catch(IllegalAccessException illegalaccessexception) {
                throw new RuntimeException("Cannot access Scheduler constructor!", illegalaccessexception);
            }
            catch(InvocationTargetException invocationtargetexception) {
                throw new RuntimeException("Scheduler constructor threw an exception", invocationtargetexception);
            }
            catch(Exception exception) {
                throw new RuntimeException("Could not instantiate Scheduler", exception);
            }
            super.mFilterContext = filtercontext;
            super.mFilterContext.addGraph(filtergraph);
            mTimer = new StopWatchMap();
            if(mLogVerbose)
                Log.v("SyncRunner", "Setting up filters");
            filtergraph.setupFilters();
            return;
        } else {
            throw new IllegalArgumentException("Class provided is not a Scheduler subclass!");
        }
    }

    void assertReadyToStep() {
        if(mScheduler == null)
            throw new RuntimeException("Attempting to run schedule with no scheduler in place!");
        if(getGraph() == null)
            throw new RuntimeException("Calling step on scheduler with no graph in place!");
        else
            return;
    }

    public void beginProcessing() {
        mScheduler.reset();
        getGraph().beginProcessing();
    }

    public void close() {
        if(mLogVerbose)
            Log.v("SyncRunner", "Closing graph.");
        getGraph().closeFilters(super.mFilterContext);
        mScheduler.reset();
    }

    protected int determinePostRunState() {
        byte byte0;
        Iterator iterator;
        byte0 = 4;
        iterator = mScheduler.getGraph().getFilters().iterator();
_L4:
        if(!iterator.hasNext()) goto _L2; else goto _L1
_L1:
        Filter filter = (Filter)iterator.next();
        if(!filter.isOpen()) goto _L4; else goto _L3
_L3:
        if(filter.getStatus() == byte0)
            byte0 = 3;
_L6:
        return byte0;
_L2:
        byte0 = 2;
        if(true) goto _L6; else goto _L5
_L5:
    }

    /**
     * @deprecated Method getError is deprecated
     */

    public Exception getError() {
        this;
        JVM INSTR monitorenter ;
        return null;
    }

    public FilterGraph getGraph() {
        FilterGraph filtergraph;
        if(mScheduler != null)
            filtergraph = mScheduler.getGraph();
        else
            filtergraph = null;
        return filtergraph;
    }

    public boolean isRunning() {
        return false;
    }

    boolean performStep() {
        if(mLogVerbose)
            Log.v("SyncRunner", "Performing one step.");
        Filter filter = mScheduler.scheduleNextNode();
        boolean flag;
        if(filter != null) {
            mTimer.start(filter.getName());
            processFilterNode(filter);
            mTimer.stop(filter.getName());
            flag = true;
        } else {
            flag = false;
        }
        return flag;
    }

    protected void processFilterNode(Filter filter) {
        if(mLogVerbose)
            Log.v("SyncRunner", "Processing filter node");
        filter.performProcess(super.mFilterContext);
        if(filter.getStatus() == 6)
            throw new RuntimeException((new StringBuilder()).append("There was an error executing ").append(filter).append("!").toString());
        if(filter.getStatus() == 4) {
            if(mLogVerbose)
                Log.v("SyncRunner", "Scheduling filter wakeup");
            scheduleFilterWake(filter, filter.getSleepDelay());
        }
    }

    public void run() {
        if(mLogVerbose)
            Log.v("SyncRunner", "Beginning run.");
        assertReadyToStep();
        beginProcessing();
        boolean flag = activateGlContext();
        for(boolean flag1 = true; flag1; flag1 = performStep());
        if(flag)
            deactivateGlContext();
        if(mDoneListener != null) {
            if(mLogVerbose)
                Log.v("SyncRunner", "Calling completion listener.");
            mDoneListener.onRunnerDone(determinePostRunState());
        }
        if(mLogVerbose)
            Log.v("SyncRunner", "Run complete");
    }

    protected void scheduleFilterWake(final Filter filterToSchedule, int i) {
        mWakeCondition.close();
        final ConditionVariable conditionToWake = mWakeCondition;
        mWakeExecutor.schedule(new Runnable() {

            public void run() {
                filterToSchedule.unsetStatus(4);
                conditionToWake.open();
            }

            final SyncRunner this$0;
            final ConditionVariable val$conditionToWake;
            final Filter val$filterToSchedule;

             {
                this$0 = SyncRunner.this;
                filterToSchedule = filter;
                conditionToWake = conditionvariable;
                super();
            }
        }, i, TimeUnit.MILLISECONDS);
    }

    public void setDoneCallback(GraphRunner.OnRunnerDoneListener onrunnerdonelistener) {
        mDoneListener = onrunnerdonelistener;
    }

    public int step() {
        assertReadyToStep();
        if(!getGraph().isReady())
            throw new RuntimeException("Trying to process graph that is not open!");
        int i;
        if(performStep())
            i = 1;
        else
            i = determinePostRunState();
        return i;
    }

    public void stop() {
        throw new RuntimeException("SyncRunner does not support stopping a graph!");
    }

    protected void waitUntilWake() {
        mWakeCondition.block();
    }

    private static final String TAG = "SyncRunner";
    private GraphRunner.OnRunnerDoneListener mDoneListener;
    private final boolean mLogVerbose = Log.isLoggable("SyncRunner", 2);
    private Scheduler mScheduler;
    private StopWatchMap mTimer;
    private ConditionVariable mWakeCondition;
    private ScheduledThreadPoolExecutor mWakeExecutor;
}
