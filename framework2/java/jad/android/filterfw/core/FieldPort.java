// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.filterfw.core;

import java.lang.reflect.Field;

// Referenced classes of package android.filterfw.core:
//            InputPort, FilterPort, Frame, Filter, 
//            FilterContext

public class FieldPort extends InputPort {

    public FieldPort(Filter filter, String s, Field field, boolean flag) {
        super(filter, s);
        mValueWaiting = false;
        mField = field;
        mHasFrame = flag;
    }

    /**
     * @deprecated Method acceptsFrame is deprecated
     */

    public boolean acceptsFrame() {
        this;
        JVM INSTR monitorenter ;
        boolean flag = mValueWaiting;
        boolean flag1;
        if(!flag)
            flag1 = true;
        else
            flag1 = false;
        this;
        JVM INSTR monitorexit ;
        return flag1;
        Exception exception;
        exception;
        throw exception;
    }

    public void clear() {
    }

    public Object getTarget() {
        Object obj1 = mField.get(super.mFilter);
        Object obj = obj1;
_L2:
        return obj;
        IllegalAccessException illegalaccessexception;
        illegalaccessexception;
        obj = null;
        if(true) goto _L2; else goto _L1
_L1:
    }

    /**
     * @deprecated Method hasFrame is deprecated
     */

    public boolean hasFrame() {
        this;
        JVM INSTR monitorenter ;
        boolean flag = mHasFrame;
        this;
        JVM INSTR monitorexit ;
        return flag;
        Exception exception;
        exception;
        throw exception;
    }

    /**
     * @deprecated Method pullFrame is deprecated
     */

    public Frame pullFrame() {
        this;
        JVM INSTR monitorenter ;
        throw new RuntimeException((new StringBuilder()).append("Cannot pull frame on ").append(this).append("!").toString());
        Exception exception;
        exception;
        this;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public void pushFrame(Frame frame) {
        setFieldFrame(frame, false);
    }

    /**
     * @deprecated Method setFieldFrame is deprecated
     */

    protected void setFieldFrame(Frame frame, boolean flag) {
        this;
        JVM INSTR monitorenter ;
        assertPortIsOpen();
        checkFrameType(frame, flag);
        Object obj = frame.getObjectValue();
        if(obj == null && mValue != null || !obj.equals(mValue)) {
            mValue = obj;
            mValueWaiting = true;
        }
        mHasFrame = true;
        this;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        throw exception;
    }

    public void setFrame(Frame frame) {
        setFieldFrame(frame, true);
    }

    public String toString() {
        return (new StringBuilder()).append("field ").append(super.toString()).toString();
    }

    /**
     * @deprecated Method transfer is deprecated
     */

    public void transfer(FilterContext filtercontext) {
        this;
        JVM INSTR monitorenter ;
        boolean flag = mValueWaiting;
        if(!flag)
            break MISSING_BLOCK_LABEL_47;
        mField.set(super.mFilter, mValue);
        mValueWaiting = false;
        if(filtercontext != null)
            super.mFilter.notifyFieldPortValueUpdated(super.mName, filtercontext);
        this;
        JVM INSTR monitorexit ;
        return;
        IllegalAccessException illegalaccessexception;
        illegalaccessexception;
        throw new RuntimeException((new StringBuilder()).append("Access to field '").append(mField.getName()).append("' was denied!").toString());
        Exception exception;
        exception;
        this;
        JVM INSTR monitorexit ;
        throw exception;
    }

    protected Field mField;
    protected boolean mHasFrame;
    protected Object mValue;
    protected boolean mValueWaiting;
}
