// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.filterfw.core;

import android.os.SystemClock;
import android.util.Log;

class StopWatch {

    public StopWatch(String s) {
        STOP_WATCH_LOGGING_PERIOD = 200;
        TAG = "MFF";
        mName = s;
        mStartTime = -1L;
        mTotalTime = 0L;
        mNumCalls = 0;
    }

    public void start() {
        if(mStartTime != -1L) {
            throw new RuntimeException("Calling start with StopWatch already running");
        } else {
            mStartTime = SystemClock.elapsedRealtime();
            return;
        }
    }

    public void stop() {
        if(mStartTime == -1L)
            throw new RuntimeException("Calling stop with StopWatch already stopped");
        long l = SystemClock.elapsedRealtime();
        mTotalTime = mTotalTime + (l - mStartTime);
        mNumCalls = 1 + mNumCalls;
        mStartTime = -1L;
        if(mNumCalls % STOP_WATCH_LOGGING_PERIOD == 0) {
            String s = TAG;
            StringBuilder stringbuilder = (new StringBuilder()).append("AVG ms/call ").append(mName).append(": ");
            Object aobj[] = new Object[1];
            aobj[0] = Float.valueOf((1.0F * (float)mTotalTime) / (float)mNumCalls);
            Log.i(s, stringbuilder.append(String.format("%.1f", aobj)).toString());
            mTotalTime = 0L;
            mNumCalls = 0;
        }
    }

    private int STOP_WATCH_LOGGING_PERIOD;
    private String TAG;
    private String mName;
    private int mNumCalls;
    private long mStartTime;
    private long mTotalTime;
}
