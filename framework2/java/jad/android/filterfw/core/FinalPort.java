// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.filterfw.core;

import java.lang.reflect.Field;

// Referenced classes of package android.filterfw.core:
//            FieldPort, FilterPort, Filter, Frame

public class FinalPort extends FieldPort {

    public FinalPort(Filter filter, String s, Field field, boolean flag) {
        super(filter, s, field, flag);
    }

    /**
     * @deprecated Method setFieldFrame is deprecated
     */

    protected void setFieldFrame(Frame frame, boolean flag) {
        this;
        JVM INSTR monitorenter ;
        assertPortIsOpen();
        checkFrameType(frame, flag);
        if(super.mFilter.getStatus() != 0)
            throw new RuntimeException((new StringBuilder()).append("Attempting to modify ").append(this).append("!").toString());
        break MISSING_BLOCK_LABEL_59;
        Exception exception;
        exception;
        this;
        JVM INSTR monitorexit ;
        throw exception;
        super.setFieldFrame(frame, flag);
        super.transfer(null);
        this;
        JVM INSTR monitorexit ;
    }

    public String toString() {
        return (new StringBuilder()).append("final ").append(super.toString()).toString();
    }
}
