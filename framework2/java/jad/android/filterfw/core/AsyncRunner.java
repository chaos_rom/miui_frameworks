// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.filterfw.core;

import android.os.AsyncTask;
import android.util.Log;

// Referenced classes of package android.filterfw.core:
//            GraphRunner, SimpleScheduler, SyncRunner, FilterContext, 
//            FilterGraph

public class AsyncRunner extends GraphRunner {
    private class AsyncRunnerTask extends AsyncTask {

        protected transient RunnerResult doInBackground(SyncRunner asyncrunner[]) {
            RunnerResult runnerresult = new RunnerResult();
            Exception exception;
            if(asyncrunner.length > 1)
                throw new RuntimeException("More than one runner received!");
            break MISSING_BLOCK_LABEL_68;
            try {
                asyncrunner[0].assertReadyToStep();
                if(mLogVerbose)
                    Log.v("AsyncRunnerTask", "Starting background graph processing.");
                activateGlContext();
                if(mLogVerbose)
                    Log.v("AsyncRunnerTask", "Preparing filter graph for processing.");
                asyncrunner[0].beginProcessing();
                if(mLogVerbose)
                    Log.v("AsyncRunnerTask", "Running graph.");
                runnerresult.status = 1;
                do {
                    if(isCancelled() || runnerresult.status != 1)
                        break;
                    if(!asyncrunner[0].performStep()) {
                        runnerresult.status = asyncrunner[0].determinePostRunState();
                        if(runnerresult.status == 3) {
                            asyncrunner[0].waitUntilWake();
                            runnerresult.status = 1;
                        }
                    }
                } while(true);
                if(isCancelled())
                    runnerresult.status = 5;
            }
            // Misplaced declaration of an exception variable
            catch(Exception exception) {
                runnerresult.exception = exception;
                runnerresult.status = 6;
            }
            try {
                deactivateGlContext();
            }
            catch(Exception exception1) {
                runnerresult.exception = exception1;
                runnerresult.status = 6;
            }
            if(mLogVerbose)
                Log.v("AsyncRunnerTask", "Done with background graph processing.");
            return runnerresult;
        }

        protected volatile Object doInBackground(Object aobj[]) {
            return doInBackground((SyncRunner[])aobj);
        }

        protected void onCancelled(RunnerResult runnerresult) {
            onPostExecute(runnerresult);
        }

        protected volatile void onCancelled(Object obj) {
            onCancelled((RunnerResult)obj);
        }

        protected void onPostExecute(RunnerResult runnerresult) {
            if(mLogVerbose)
                Log.v("AsyncRunnerTask", "Starting post-execute.");
            setRunning(false);
            if(runnerresult == null) {
                runnerresult = new RunnerResult();
                runnerresult.status = 5;
            }
            setException(runnerresult.exception);
            if(runnerresult.status == 5 || runnerresult.status == 6) {
                if(mLogVerbose)
                    Log.v("AsyncRunnerTask", "Closing filters.");
                try {
                    mRunner.close();
                }
                catch(Exception exception) {
                    runnerresult.status = 6;
                    setException(exception);
                }
            }
            if(mDoneListener != null) {
                if(mLogVerbose)
                    Log.v("AsyncRunnerTask", "Calling graph done callback.");
                mDoneListener.onRunnerDone(runnerresult.status);
            }
            if(mLogVerbose)
                Log.v("AsyncRunnerTask", "Completed post-execute.");
        }

        protected volatile void onPostExecute(Object obj) {
            onPostExecute((RunnerResult)obj);
        }

        private static final String TAG = "AsyncRunnerTask";
        final AsyncRunner this$0;

        private AsyncRunnerTask() {
            this$0 = AsyncRunner.this;
            super();
        }

    }

    private class RunnerResult {

        public Exception exception;
        public int status;
        final AsyncRunner this$0;

        private RunnerResult() {
            this$0 = AsyncRunner.this;
            super();
            status = 0;
        }

    }


    public AsyncRunner(FilterContext filtercontext) {
        super(filtercontext);
        mSchedulerClass = android/filterfw/core/SimpleScheduler;
        mLogVerbose = Log.isLoggable("AsyncRunner", 2);
    }

    public AsyncRunner(FilterContext filtercontext, Class class1) {
        super(filtercontext);
        mSchedulerClass = class1;
        mLogVerbose = Log.isLoggable("AsyncRunner", 2);
    }

    /**
     * @deprecated Method setException is deprecated
     */

    private void setException(Exception exception) {
        this;
        JVM INSTR monitorenter ;
        mException = exception;
        this;
        JVM INSTR monitorexit ;
        return;
        Exception exception1;
        exception1;
        throw exception1;
    }

    /**
     * @deprecated Method setRunning is deprecated
     */

    private void setRunning(boolean flag) {
        this;
        JVM INSTR monitorenter ;
        isProcessing = flag;
        this;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        throw exception;
    }

    /**
     * @deprecated Method close is deprecated
     */

    public void close() {
        this;
        JVM INSTR monitorenter ;
        if(isRunning())
            throw new RuntimeException("Cannot close graph while it is running!");
        break MISSING_BLOCK_LABEL_24;
        Exception exception;
        exception;
        this;
        JVM INSTR monitorexit ;
        throw exception;
        if(mLogVerbose)
            Log.v("AsyncRunner", "Closing filters.");
        mRunner.close();
        this;
        JVM INSTR monitorexit ;
    }

    /**
     * @deprecated Method getError is deprecated
     */

    public Exception getError() {
        this;
        JVM INSTR monitorenter ;
        Exception exception1 = mException;
        this;
        JVM INSTR monitorexit ;
        return exception1;
        Exception exception;
        exception;
        throw exception;
    }

    public FilterGraph getGraph() {
        FilterGraph filtergraph;
        if(mRunner != null)
            filtergraph = mRunner.getGraph();
        else
            filtergraph = null;
        return filtergraph;
    }

    /**
     * @deprecated Method isRunning is deprecated
     */

    public boolean isRunning() {
        this;
        JVM INSTR monitorenter ;
        boolean flag = isProcessing;
        this;
        JVM INSTR monitorexit ;
        return flag;
        Exception exception;
        exception;
        throw exception;
    }

    /**
     * @deprecated Method run is deprecated
     */

    public void run() {
        this;
        JVM INSTR monitorenter ;
        if(mLogVerbose)
            Log.v("AsyncRunner", "Running graph.");
        setException(null);
        if(isRunning())
            throw new RuntimeException("Graph is already running!");
        break MISSING_BLOCK_LABEL_44;
        Exception exception;
        exception;
        this;
        JVM INSTR monitorexit ;
        throw exception;
        if(mRunner == null)
            throw new RuntimeException("Cannot run before a graph is set!");
        mRunTask = new AsyncRunnerTask();
        setRunning(true);
        AsyncRunnerTask asyncrunnertask = mRunTask;
        SyncRunner asyncrunner[] = new SyncRunner[1];
        asyncrunner[0] = mRunner;
        asyncrunnertask.execute(asyncrunner);
        this;
        JVM INSTR monitorexit ;
    }

    public void setDoneCallback(GraphRunner.OnRunnerDoneListener onrunnerdonelistener) {
        mDoneListener = onrunnerdonelistener;
    }

    /**
     * @deprecated Method setGraph is deprecated
     */

    public void setGraph(FilterGraph filtergraph) {
        this;
        JVM INSTR monitorenter ;
        if(isRunning())
            throw new RuntimeException("Graph is already running!");
        break MISSING_BLOCK_LABEL_24;
        Exception exception;
        exception;
        this;
        JVM INSTR monitorexit ;
        throw exception;
        mRunner = new SyncRunner(super.mFilterContext, filtergraph, mSchedulerClass);
        this;
        JVM INSTR monitorexit ;
    }

    /**
     * @deprecated Method stop is deprecated
     */

    public void stop() {
        this;
        JVM INSTR monitorenter ;
        if(mRunTask != null && !mRunTask.isCancelled()) {
            if(mLogVerbose)
                Log.v("AsyncRunner", "Stopping graph.");
            mRunTask.cancel(false);
        }
        this;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        throw exception;
    }

    private static final String TAG = "AsyncRunner";
    private boolean isProcessing;
    private GraphRunner.OnRunnerDoneListener mDoneListener;
    private Exception mException;
    private boolean mLogVerbose;
    private AsyncRunnerTask mRunTask;
    private SyncRunner mRunner;
    private Class mSchedulerClass;





}
