// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.filterfw.core;

import java.util.Iterator;
import java.util.Set;

// Referenced classes of package android.filterfw.core:
//            Scheduler, FilterGraph, Filter

public class RoundRobinScheduler extends Scheduler {

    public RoundRobinScheduler(FilterGraph filtergraph) {
        super(filtergraph);
        mLastPos = -1;
    }

    public void reset() {
        mLastPos = -1;
    }

    public Filter scheduleNextNode() {
        mLastPos = i;
_L2:
        return filter1;
        Set set = getGraph().getFilters();
        if(mLastPos >= set.size())
            mLastPos = -1;
        int i = 0;
        Filter filter = null;
        int j = -1;
        Filter filter1;
        for(Iterator iterator = set.iterator(); iterator.hasNext();) {
label0:
            {
                filter1 = (Filter)iterator.next();
                if(filter1.canProcess()) {
                    if(i > mLastPos)
                        break label0;
                    if(filter == null) {
                        filter = filter1;
                        j = i;
                    }
                }
                i++;
            }
        }

        if(filter != null) {
            mLastPos = j;
            filter1 = filter;
        } else {
            filter1 = null;
        }
        if(true) goto _L2; else goto _L1
_L1:
    }

    private int mLastPos;
}
