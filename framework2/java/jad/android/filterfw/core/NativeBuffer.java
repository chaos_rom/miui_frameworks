// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.filterfw.core;


// Referenced classes of package android.filterfw.core:
//            Frame

public class NativeBuffer {

    public NativeBuffer() {
        mDataPointer = 0L;
        mSize = 0;
        mOwnsData = false;
        mRefCount = 1;
    }

    public NativeBuffer(int i) {
        mDataPointer = 0L;
        mSize = 0;
        mOwnsData = false;
        mRefCount = 1;
        allocate(i * getElementSize());
        mOwnsData = true;
    }

    private native boolean allocate(int i);

    private native boolean deallocate(boolean flag);

    private native boolean nativeCopyTo(NativeBuffer nativebuffer);

    protected void assertReadable() {
        if(mDataPointer == 0L || mSize == 0 || mAttachedFrame != null && !mAttachedFrame.hasNativeAllocation())
            throw new NullPointerException("Attempting to read from null data frame!");
        else
            return;
    }

    protected void assertWritable() {
        if(isReadOnly())
            throw new RuntimeException("Attempting to modify read-only native (structured) data!");
        else
            return;
    }

    void attachToFrame(Frame frame) {
        mAttachedFrame = frame;
    }

    public int count() {
        int i;
        if(mDataPointer != 0L)
            i = mSize / getElementSize();
        else
            i = 0;
        return i;
    }

    public int getElementSize() {
        return 1;
    }

    public boolean isReadOnly() {
        boolean flag;
        if(mAttachedFrame != null)
            flag = mAttachedFrame.isReadOnly();
        else
            flag = false;
        return flag;
    }

    public NativeBuffer mutableCopy() {
        NativeBuffer nativebuffer;
        try {
            nativebuffer = (NativeBuffer)getClass().newInstance();
        }
        catch(Exception exception) {
            throw new RuntimeException((new StringBuilder()).append("Unable to allocate a copy of ").append(getClass()).append("! Make ").append("sure the class has a default constructor!").toString());
        }
        if(mSize > 0 && !nativeCopyTo(nativebuffer))
            throw new RuntimeException("Failed to copy NativeBuffer to mutable instance!");
        else
            return nativebuffer;
    }

    public NativeBuffer release() {
        boolean flag = false;
        if(mAttachedFrame == null) goto _L2; else goto _L1
_L1:
        if(mAttachedFrame.release() == null)
            flag = true;
        else
            flag = false;
_L4:
        if(flag) {
            deallocate(mOwnsData);
            this = null;
        }
        return this;
_L2:
        if(mOwnsData) {
            mRefCount = -1 + mRefCount;
            if(mRefCount == 0)
                flag = true;
            else
                flag = false;
        }
        if(true) goto _L4; else goto _L3
_L3:
    }

    public NativeBuffer retain() {
        if(mAttachedFrame == null) goto _L2; else goto _L1
_L1:
        mAttachedFrame.retain();
_L4:
        return this;
_L2:
        if(mOwnsData)
            mRefCount = 1 + mRefCount;
        if(true) goto _L4; else goto _L3
_L3:
    }

    public int size() {
        return mSize;
    }

    private Frame mAttachedFrame;
    private long mDataPointer;
    private boolean mOwnsData;
    private int mRefCount;
    private int mSize;

    static  {
        System.loadLibrary("filterfw");
    }
}
