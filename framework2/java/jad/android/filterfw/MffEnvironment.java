// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.filterfw;

import android.filterfw.core.CachedFrameManager;
import android.filterfw.core.FilterContext;
import android.filterfw.core.FrameManager;
import android.filterfw.core.GLEnvironment;

public class MffEnvironment {

    protected MffEnvironment(FrameManager framemanager) {
        if(framemanager == null)
            framemanager = new CachedFrameManager();
        mContext = new FilterContext();
        mContext.setFrameManager(framemanager);
    }

    public void activateGLEnvironment() {
        if(mContext.getGLEnvironment() != null) {
            mContext.getGLEnvironment().activate();
            return;
        } else {
            throw new NullPointerException("No GLEnvironment in place to activate!");
        }
    }

    public void createGLEnvironment() {
        GLEnvironment glenvironment = new GLEnvironment();
        glenvironment.initWithNewContext();
        setGLEnvironment(glenvironment);
    }

    public void deactivateGLEnvironment() {
        if(mContext.getGLEnvironment() != null) {
            mContext.getGLEnvironment().deactivate();
            return;
        } else {
            throw new NullPointerException("No GLEnvironment in place to deactivate!");
        }
    }

    public FilterContext getContext() {
        return mContext;
    }

    public void setGLEnvironment(GLEnvironment glenvironment) {
        mContext.initGLEnvironment(glenvironment);
    }

    private FilterContext mContext;
}
