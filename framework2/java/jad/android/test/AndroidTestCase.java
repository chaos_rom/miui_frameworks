// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.test;

import android.content.*;
import android.net.Uri;
import android.util.Log;
import java.lang.reflect.Field;
import junit.framework.TestCase;

public class AndroidTestCase extends TestCase {

    public AndroidTestCase() {
    }

    public void assertActivityRequiresPermission(String s, String s1, String s2) {
        Intent intent;
        intent = new Intent();
        intent.setClassName(s, s1);
        intent.addFlags(0x10000000);
        getContext().startActivity(intent);
        fail((new StringBuilder()).append("expected security exception for ").append(s2).toString());
_L1:
        return;
        SecurityException securityexception;
        securityexception;
        assertNotNull("security exception's error message.", securityexception.getMessage());
        assertTrue((new StringBuilder()).append("error message should contain ").append(s2).append(".").toString(), securityexception.getMessage().contains(s2));
          goto _L1
    }

    public void assertReadingContentUriRequiresPermission(Uri uri, String s) {
        getContext().getContentResolver().query(uri, null, null, null, null);
        fail((new StringBuilder()).append("expected SecurityException requiring ").append(s).toString());
_L1:
        return;
        SecurityException securityexception;
        securityexception;
        assertNotNull("security exception's error message.", securityexception.getMessage());
        assertTrue((new StringBuilder()).append("error message should contain ").append(s).append(".").toString(), securityexception.getMessage().contains(s));
          goto _L1
    }

    public void assertWritingContentUriRequiresPermission(Uri uri, String s) {
        getContext().getContentResolver().insert(uri, new ContentValues());
        fail((new StringBuilder()).append("expected SecurityException requiring ").append(s).toString());
_L1:
        return;
        SecurityException securityexception;
        securityexception;
        assertNotNull("security exception's error message.", securityexception.getMessage());
        assertTrue((new StringBuilder()).append("error message should contain ").append(s).append(".").toString(), securityexception.getMessage().contains(s));
          goto _L1
    }

    public Context getContext() {
        return mContext;
    }

    public Context getTestContext() {
        return mTestContext;
    }

    protected void scrubClass(Class class1) throws IllegalAccessException {
        Field afield[] = getClass().getDeclaredFields();
        int i = afield.length;
        int j = 0;
        while(j < i)  {
            Field field = afield[j];
            if(!class1.isAssignableFrom(field.getDeclaringClass()) || field.getType().isPrimitive())
                continue;
            try {
                field.setAccessible(true);
                field.set(this, null);
            }
            catch(Exception exception) {
                Log.d("TestCase", "Error: Could not nullify field!");
            }
            if(field.get(this) != null)
                Log.d("TestCase", "Error: Could not nullify field!");
            j++;
        }
    }

    public void setContext(Context context) {
        mContext = context;
    }

    public void setTestContext(Context context) {
        mTestContext = context;
    }

    protected void setUp() throws Exception {
        super.setUp();
    }

    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testAndroidTestCaseSetupProperly() {
        assertNotNull("Context is null. setContext should be called before tests are run", mContext);
    }

    protected Context mContext;
    private Context mTestContext;
}
