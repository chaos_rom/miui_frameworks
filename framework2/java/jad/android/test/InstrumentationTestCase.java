// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.test;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import java.lang.reflect.*;
import junit.framework.TestCase;

// Referenced classes of package android.test:
//            FlakyTest, UiThreadTest, RepetitiveTest

public class InstrumentationTestCase extends TestCase {

    public InstrumentationTestCase() {
    }

    private void runMethod(Method method, int i) throws Throwable {
        runMethod(method, i, false);
    }

    private void runMethod(Method method, int i, boolean flag) throws Throwable {
        int j = 0;
_L2:
        method.invoke(this, (Object[])null);
        IllegalAccessException illegalaccessexception1;
        illegalaccessexception1 = null;
        j++;
        if(flag) {
            Bundle bundle3 = new Bundle();
            bundle3.putInt("currentiterations", j);
            getInstrumentation().sendStatus(2, bundle3);
        }
_L3:
        Exception exception;
        IllegalAccessException illegalaccessexception;
        InvocationTargetException invocationtargetexception;
        Throwable throwable;
        if(j >= i || !flag && illegalaccessexception1 == null)
            if(illegalaccessexception1 != null)
                throw illegalaccessexception1;
            else
                return;
        if(true) goto _L2; else goto _L1
_L1:
        invocationtargetexception;
        invocationtargetexception.fillInStackTrace();
        throwable = invocationtargetexception.getTargetException();
        illegalaccessexception1 = throwable;
        j++;
        if(flag) {
            Bundle bundle2 = new Bundle();
            bundle2.putInt("currentiterations", j);
            getInstrumentation().sendStatus(2, bundle2);
        }
          goto _L3
        illegalaccessexception;
        illegalaccessexception.fillInStackTrace();
        illegalaccessexception1 = illegalaccessexception;
        j++;
        if(flag) {
            Bundle bundle1 = new Bundle();
            bundle1.putInt("currentiterations", j);
            getInstrumentation().sendStatus(2, bundle1);
        }
          goto _L3
        exception;
        int k = j + 1;
        if(flag) {
            Bundle bundle = new Bundle();
            bundle.putInt("currentiterations", k);
            getInstrumentation().sendStatus(2, bundle);
        }
        throw exception;
    }

    public Instrumentation getInstrumentation() {
        return mInstrumentation;
    }

    public void injectInsrumentation(Instrumentation instrumentation) {
        injectInstrumentation(instrumentation);
    }

    public void injectInstrumentation(Instrumentation instrumentation) {
        mInstrumentation = instrumentation;
    }

    public final Activity launchActivity(String s, Class class1, Bundle bundle) {
        Intent intent = new Intent("android.intent.action.MAIN");
        if(bundle != null)
            intent.putExtras(bundle);
        return launchActivityWithIntent(s, class1, intent);
    }

    public final Activity launchActivityWithIntent(String s, Class class1, Intent intent) {
        intent.setClassName(s, class1.getName());
        intent.addFlags(0x10000000);
        Activity activity = getInstrumentation().startActivitySync(intent);
        getInstrumentation().waitForIdleSync();
        return activity;
    }

    protected void runTest() throws Throwable {
        String s = getName();
        assertNotNull(s);
        Method method = null;
        Method method1;
        try {
            method1 = getClass().getMethod(s, (Class[])null);
        }
        catch(NoSuchMethodException nosuchmethodexception) {
            fail((new StringBuilder()).append("Method \"").append(s).append("\" not found").toString());
            continue;
        }
        method = method1;
        do {
            if(!Modifier.isPublic(method.getModifiers()))
                fail((new StringBuilder()).append("Method \"").append(s).append("\" should be public").toString());
            int i = 1;
            boolean flag = false;
            if(method.isAnnotationPresent(android/test/FlakyTest))
                i = ((FlakyTest)method.getAnnotation(android/test/FlakyTest)).tolerance();
            else
            if(method.isAnnotationPresent(android/test/RepetitiveTest)) {
                i = ((RepetitiveTest)method.getAnnotation(android/test/RepetitiveTest)).numIterations();
                flag = true;
            }
            if(method.isAnnotationPresent(android/test/UiThreadTest)) {
                final int tolerance = i;
                final boolean repetitive = flag;
                final Method testMethod = method;
                final Throwable exceptions[] = new Throwable[1];
                getInstrumentation().runOnMainSync(new Runnable() {

                    public void run() {
                        runMethod(testMethod, tolerance, repetitive);
_L1:
                        return;
                        Throwable throwable;
                        throwable;
                        exceptions[0] = throwable;
                          goto _L1
                    }

                    final InstrumentationTestCase this$0;
                    final Throwable val$exceptions[];
                    final boolean val$repetitive;
                    final Method val$testMethod;
                    final int val$tolerance;

             {
                this$0 = InstrumentationTestCase.this;
                testMethod = method;
                tolerance = i;
                repetitive = flag;
                exceptions = athrowable;
                super();
            }
                });
                if(exceptions[0] != null)
                    throw exceptions[0];
            } else {
                runMethod(method, i, flag);
            }
            return;
        } while(true);
    }

    public void runTestOnUiThread(final Runnable r) throws Throwable {
        final Throwable exceptions[] = new Throwable[1];
        getInstrumentation().runOnMainSync(new Runnable() {

            public void run() {
                r.run();
_L1:
                return;
                Throwable throwable;
                throwable;
                exceptions[0] = throwable;
                  goto _L1
            }

            final InstrumentationTestCase this$0;
            final Throwable val$exceptions[];
            final Runnable val$r;

             {
                this$0 = InstrumentationTestCase.this;
                r = runnable;
                exceptions = athrowable;
                super();
            }
        });
        if(exceptions[0] != null)
            throw exceptions[0];
        else
            return;
    }

    public void sendKeys(String s) {
        String as[];
        int i;
        Instrumentation instrumentation;
        int j;
        as = s.split(" ");
        i = as.length;
        instrumentation = getInstrumentation();
        j = 0;
_L7:
        String s1;
        int k;
        if(j >= i)
            break; /* Loop/switch isn't completed */
        s1 = as[j];
        k = s1.indexOf('*');
        if(k != -1) goto _L2; else goto _L1
_L1:
        int i1 = 1;
_L5:
        int j1;
        if(k != -1)
            s1 = s1.substring(k + 1);
        j1 = 0;
_L4:
        if(j1 >= i1)
            break; /* Loop/switch isn't completed */
        int k1 = android/view/KeyEvent.getField((new StringBuilder()).append("KEYCODE_").append(s1).toString()).getInt(null);
        NumberFormatException numberformatexception;
        int l;
        IllegalAccessException illegalaccessexception;
        NoSuchFieldException nosuchfieldexception;
        try {
            instrumentation.sendKeyDownUpSync(k1);
        }
        catch(SecurityException securityexception) { }
        j1++;
        if(true) goto _L4; else goto _L3
_L2:
        l = Integer.parseInt(s1.substring(0, k));
        i1 = l;
        if(true) goto _L5; else goto _L3
        numberformatexception;
        Log.w("ActivityTestCase", (new StringBuilder()).append("Invalid repeat count: ").append(s1).toString());
_L3:
        j++;
        if(true) goto _L7; else goto _L6
        nosuchfieldexception;
        Log.w("ActivityTestCase", (new StringBuilder()).append("Unknown keycode: KEYCODE_").append(s1).toString());
          goto _L3
        illegalaccessexception;
        Log.w("ActivityTestCase", (new StringBuilder()).append("Unknown keycode: KEYCODE_").append(s1).toString());
          goto _L3
_L6:
        instrumentation.waitForIdleSync();
        return;
    }

    public transient void sendKeys(int ai[]) {
        int i = ai.length;
        Instrumentation instrumentation = getInstrumentation();
        int j = 0;
        while(j < i)  {
            try {
                instrumentation.sendKeyDownUpSync(ai[j]);
            }
            catch(SecurityException securityexception) { }
            j++;
        }
        instrumentation.waitForIdleSync();
    }

    public transient void sendRepeatedKeys(int ai[]) {
        int i = ai.length;
        if((i & 1) == 1)
            throw new IllegalArgumentException("The size of the keys array must be a multiple of 2");
        Instrumentation instrumentation = getInstrumentation();
        int j = 0;
        while(j < i)  {
            int k = ai[j];
            int l = ai[j + 1];
            int i1 = 0;
            while(i1 < k)  {
                try {
                    instrumentation.sendKeyDownUpSync(l);
                }
                catch(SecurityException securityexception) { }
                i1++;
            }
            j += 2;
        }
        instrumentation.waitForIdleSync();
    }

    protected void tearDown() throws Exception {
        Runtime.getRuntime().gc();
        Runtime.getRuntime().runFinalization();
        Runtime.getRuntime().gc();
        super.tearDown();
    }

    private Instrumentation mInstrumentation;

}
