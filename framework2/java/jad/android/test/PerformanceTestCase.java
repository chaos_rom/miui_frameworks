// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.test;


public interface PerformanceTestCase {
    public static interface Intermediates {

        public abstract void addIntermediate(String s);

        public abstract void addIntermediate(String s, long l);

        public abstract void finishTiming(boolean flag);

        public abstract void setInternalIterations(int i);

        public abstract void startTiming(boolean flag);
    }


    public abstract boolean isPerformanceOnly();

    public abstract int startPerformance(Intermediates intermediates);
}
