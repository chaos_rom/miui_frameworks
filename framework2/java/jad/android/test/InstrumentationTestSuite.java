// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.test;

import android.app.Instrumentation;
import junit.framework.*;

// Referenced classes of package android.test:
//            InstrumentationTestCase

public class InstrumentationTestSuite extends TestSuite {

    public InstrumentationTestSuite(Instrumentation instrumentation) {
        mInstrumentation = instrumentation;
    }

    public InstrumentationTestSuite(Class class1, Instrumentation instrumentation) {
        super(class1);
        mInstrumentation = instrumentation;
    }

    public InstrumentationTestSuite(String s, Instrumentation instrumentation) {
        super(s);
        mInstrumentation = instrumentation;
    }

    public void addTestSuite(Class class1) {
        addTest(new InstrumentationTestSuite(class1, mInstrumentation));
    }

    public void runTest(Test test, TestResult testresult) {
        if(test instanceof InstrumentationTestCase)
            ((InstrumentationTestCase)test).injectInstrumentation(mInstrumentation);
        super.runTest(test, testresult);
    }

    private final Instrumentation mInstrumentation;
}
