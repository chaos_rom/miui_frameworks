// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.speech.srec;

import java.io.*;

public class WaveHeader {

    public WaveHeader() {
    }

    public WaveHeader(short word0, short word1, int i, short word2, int j) {
        mFormat = word0;
        mSampleRate = i;
        mNumChannels = word1;
        mBitsPerSample = word2;
        mNumBytes = j;
    }

    private static void readId(InputStream inputstream, String s) throws IOException {
        for(int i = 0; i < s.length(); i++)
            if(s.charAt(i) != inputstream.read())
                throw new IOException((new StringBuilder()).append(s).append(" tag not present").toString());

    }

    private static int readInt(InputStream inputstream) throws IOException {
        return inputstream.read() | inputstream.read() << 8 | inputstream.read() << 16 | inputstream.read() << 24;
    }

    private static short readShort(InputStream inputstream) throws IOException {
        return (short)(inputstream.read() | inputstream.read() << 8);
    }

    private static void writeId(OutputStream outputstream, String s) throws IOException {
        for(int i = 0; i < s.length(); i++)
            outputstream.write(s.charAt(i));

    }

    private static void writeInt(OutputStream outputstream, int i) throws IOException {
        outputstream.write(i >> 0);
        outputstream.write(i >> 8);
        outputstream.write(i >> 16);
        outputstream.write(i >> 24);
    }

    private static void writeShort(OutputStream outputstream, short word0) throws IOException {
        outputstream.write(word0 >> 0);
        outputstream.write(word0 >> 8);
    }

    public short getBitsPerSample() {
        return mBitsPerSample;
    }

    public short getFormat() {
        return mFormat;
    }

    public int getNumBytes() {
        return mNumBytes;
    }

    public short getNumChannels() {
        return mNumChannels;
    }

    public int getSampleRate() {
        return mSampleRate;
    }

    public int read(InputStream inputstream) throws IOException {
        readId(inputstream, "RIFF");
        int _tmp = -36 + readInt(inputstream);
        readId(inputstream, "WAVE");
        readId(inputstream, "fmt ");
        if(16 != readInt(inputstream))
            throw new IOException("fmt chunk length not 16");
        mFormat = readShort(inputstream);
        mNumChannels = readShort(inputstream);
        mSampleRate = readInt(inputstream);
        int i = readInt(inputstream);
        short word0 = readShort(inputstream);
        mBitsPerSample = readShort(inputstream);
        if(i != (mNumChannels * mSampleRate * mBitsPerSample) / 8)
            throw new IOException("fmt.ByteRate field inconsistent");
        if(word0 != (mNumChannels * mBitsPerSample) / 8) {
            throw new IOException("fmt.BlockAlign field inconsistent");
        } else {
            readId(inputstream, "data");
            mNumBytes = readInt(inputstream);
            return 44;
        }
    }

    public WaveHeader setBitsPerSample(short word0) {
        mBitsPerSample = word0;
        return this;
    }

    public WaveHeader setFormat(short word0) {
        mFormat = word0;
        return this;
    }

    public WaveHeader setNumBytes(int i) {
        mNumBytes = i;
        return this;
    }

    public WaveHeader setNumChannels(short word0) {
        mNumChannels = word0;
        return this;
    }

    public WaveHeader setSampleRate(int i) {
        mSampleRate = i;
        return this;
    }

    public String toString() {
        Object aobj[] = new Object[5];
        aobj[0] = Short.valueOf(mFormat);
        aobj[1] = Short.valueOf(mNumChannels);
        aobj[2] = Integer.valueOf(mSampleRate);
        aobj[3] = Short.valueOf(mBitsPerSample);
        aobj[4] = Integer.valueOf(mNumBytes);
        return String.format("WaveHeader format=%d numChannels=%d sampleRate=%d bitsPerSample=%d numBytes=%d", aobj);
    }

    public int write(OutputStream outputstream) throws IOException {
        writeId(outputstream, "RIFF");
        writeInt(outputstream, 36 + mNumBytes);
        writeId(outputstream, "WAVE");
        writeId(outputstream, "fmt ");
        writeInt(outputstream, 16);
        writeShort(outputstream, mFormat);
        writeShort(outputstream, mNumChannels);
        writeInt(outputstream, mSampleRate);
        writeInt(outputstream, (mNumChannels * mSampleRate * mBitsPerSample) / 8);
        writeShort(outputstream, (mNumChannels * mBitsPerSample) / 8);
        writeShort(outputstream, mBitsPerSample);
        writeId(outputstream, "data");
        writeInt(outputstream, mNumBytes);
        return 44;
    }

    public static final short FORMAT_ALAW = 6;
    public static final short FORMAT_PCM = 1;
    public static final short FORMAT_ULAW = 7;
    private static final int HEADER_LENGTH = 44;
    private static final String TAG = "WaveHeader";
    private short mBitsPerSample;
    private short mFormat;
    private int mNumBytes;
    private short mNumChannels;
    private int mSampleRate;
}
