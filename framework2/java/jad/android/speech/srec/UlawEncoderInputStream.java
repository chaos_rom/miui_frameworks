// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.speech.srec;

import java.io.IOException;
import java.io.InputStream;

public final class UlawEncoderInputStream extends InputStream {

    public UlawEncoderInputStream(InputStream inputstream, int i) {
        mMax = 0;
        mBufCount = 0;
        mIn = inputstream;
        mMax = i;
    }

    public static void encode(byte abyte0[], int i, byte abyte1[], int j, int k, int l) {
        if(l <= 0)
            l = 8192;
        int i1 = 0x20000000 / l;
        int j1 = 0;
        int k1 = j;
        int l1 = i;
        while(j1 < k)  {
            int i2 = l1 + 1;
            int j2 = 0xff & abyte0[l1];
            l1 = i2 + 1;
            int k2 = i1 * (j2 + (abyte0[i2] << 8)) >> 16;
            int l2;
            int i3;
            if(k2 >= 0) {
                if(k2 <= 0)
                    l2 = 255;
                else
                if(k2 <= 30)
                    l2 = 240 + (30 - k2 >> 1);
                else
                if(k2 <= 94)
                    l2 = 224 + (94 - k2 >> 2);
                else
                if(k2 <= 222)
                    l2 = 208 + (222 - k2 >> 3);
                else
                if(k2 <= 478)
                    l2 = 192 + (478 - k2 >> 4);
                else
                if(k2 <= 990)
                    l2 = 176 + (990 - k2 >> 5);
                else
                if(k2 <= 2014)
                    l2 = 160 + (2014 - k2 >> 6);
                else
                if(k2 <= 4062)
                    l2 = 144 + (4062 - k2 >> 7);
                else
                if(k2 <= 8158)
                    l2 = 128 + (8158 - k2 >> 8);
                else
                    l2 = 128;
            } else
            if(-1 <= k2)
                l2 = 127;
            else
            if(-31 <= k2)
                l2 = 112 + (k2 + 31 >> 1);
            else
            if(-95 <= k2)
                l2 = 96 + (k2 + 95 >> 2);
            else
            if(-223 <= k2)
                l2 = 80 + (k2 + 223 >> 3);
            else
            if(-479 <= k2)
                l2 = 64 + (k2 + 479 >> 4);
            else
            if(-991 <= k2)
                l2 = 48 + (k2 + 991 >> 5);
            else
            if(-2015 <= k2)
                l2 = 32 + (k2 + 2015 >> 6);
            else
            if(-4063 <= k2)
                l2 = 16 + (k2 + 4063 >> 7);
            else
            if(-8159 <= k2)
                l2 = 0 + (k2 + 8159 >> 8);
            else
                l2 = 0;
            i3 = k1 + 1;
            abyte1[k1] = (byte)l2;
            j1++;
            k1 = i3;
        }
    }

    public static int maxAbsPcm(byte abyte0[], int i, int j) {
        int k = 0;
        int l = 0;
        int i1 = i;
        for(; l < j; l++) {
            int j1 = i1 + 1;
            int k1 = 0xff & abyte0[i1];
            i1 = j1 + 1;
            int l1 = k1 + (abyte0[j1] << 8);
            if(l1 < 0)
                l1 = -l1;
            if(l1 > k)
                k = l1;
        }

        return k;
    }

    public int available() throws IOException {
        return (mIn.available() + mBufCount) / 2;
    }

    public void close() throws IOException {
        if(mIn != null) {
            InputStream inputstream = mIn;
            mIn = null;
            inputstream.close();
        }
    }

    public int read() throws IOException {
        int i = -1;
        if(read(mOneByte, 0, 1) != i)
            i = 0xff & mOneByte[0];
        return i;
    }

    public int read(byte abyte0[]) throws IOException {
        return read(abyte0, 0, abyte0.length);
    }

    public int read(byte abyte0[], int i, int j) throws IOException {
        int k;
        k = -1;
        if(mIn == null)
            throw new IllegalStateException("not open");
          goto _L1
_L5:
        int j1;
        mBufCount = j1 + mBufCount;
_L1:
        if(mBufCount >= 2) goto _L3; else goto _L2
_L2:
        if((j1 = mIn.read(mBuf, mBufCount, Math.min(j * 2, mBuf.length - mBufCount))) != k) goto _L5; else goto _L4
_L4:
        return k;
_L3:
        int l = Math.min(mBufCount / 2, j);
        encode(mBuf, 0, abyte0, i, l, mMax);
        mBufCount = mBufCount - l * 2;
        for(int i1 = 0; i1 < mBufCount; i1++)
            mBuf[i1] = mBuf[i1 + l * 2];

        k = l;
        if(true) goto _L4; else goto _L6
_L6:
    }

    private static final int MAX_ULAW = 8192;
    private static final int SCALE_BITS = 16;
    private static final String TAG = "UlawEncoderInputStream";
    private final byte mBuf[] = new byte[1024];
    private int mBufCount;
    private InputStream mIn;
    private int mMax;
    private final byte mOneByte[] = new byte[1];
}
