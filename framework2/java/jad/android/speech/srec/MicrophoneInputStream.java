// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.speech.srec;

import java.io.IOException;
import java.io.InputStream;

public final class MicrophoneInputStream extends InputStream {

    public MicrophoneInputStream(int i, int j) throws IOException {
        mAudioRecord = 0;
        mOneByte = new byte[1];
        mAudioRecord = AudioRecordNew(i, j);
        if(mAudioRecord == 0)
            throw new IOException("AudioRecord constructor failed - busy?");
        int k = AudioRecordStart(mAudioRecord);
        if(k != 0) {
            close();
            throw new IOException((new StringBuilder()).append("AudioRecord start failed: ").append(k).toString());
        } else {
            return;
        }
    }

    private static native void AudioRecordDelete(int i) throws IOException;

    private static native int AudioRecordNew(int i, int j);

    private static native int AudioRecordRead(int i, byte abyte0[], int j, int k) throws IOException;

    private static native int AudioRecordStart(int i);

    private static native void AudioRecordStop(int i) throws IOException;

    public void close() throws IOException {
        if(mAudioRecord == 0)
            break MISSING_BLOCK_LABEL_26;
        AudioRecordStop(mAudioRecord);
        AudioRecordDelete(mAudioRecord);
        mAudioRecord = 0;
        return;
        Exception exception2;
        exception2;
        mAudioRecord = 0;
        throw exception2;
        Exception exception;
        exception;
        AudioRecordDelete(mAudioRecord);
        mAudioRecord = 0;
        throw exception;
        Exception exception1;
        exception1;
        mAudioRecord = 0;
        throw exception1;
    }

    protected void finalize() throws Throwable {
        if(mAudioRecord != 0) {
            close();
            throw new IOException("someone forgot to close MicrophoneInputStream");
        } else {
            return;
        }
    }

    public int read() throws IOException {
        if(mAudioRecord == 0)
            throw new IllegalStateException("not open");
        int i;
        if(AudioRecordRead(mAudioRecord, mOneByte, 0, 1) == 1)
            i = 0xff & mOneByte[0];
        else
            i = -1;
        return i;
    }

    public int read(byte abyte0[]) throws IOException {
        if(mAudioRecord == 0)
            throw new IllegalStateException("not open");
        else
            return AudioRecordRead(mAudioRecord, abyte0, 0, abyte0.length);
    }

    public int read(byte abyte0[], int i, int j) throws IOException {
        if(mAudioRecord == 0)
            throw new IllegalStateException("not open");
        else
            return AudioRecordRead(mAudioRecord, abyte0, i, j);
    }

    private static final String TAG = "MicrophoneInputStream";
    private int mAudioRecord;
    private byte mOneByte[];

    static  {
        System.loadLibrary("srec_jni");
    }
}
