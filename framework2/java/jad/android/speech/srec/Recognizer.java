// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.speech.srec;

import java.io.*;
import java.util.Locale;

public final class Recognizer {
    public class Grammar {

        public void addWordToSlot(String s, String s1, String s2, int i, String s3) {
            Recognizer.SR_GrammarAddWordToSlot(mGrammar, s, s1, s2, i, s3);
        }

        public void compile() {
            Recognizer.SR_GrammarCompile(mGrammar);
        }

        public void destroy() {
            if(mGrammar != 0) {
                Recognizer.SR_GrammarDestroy(mGrammar);
                mGrammar = 0;
            }
        }

        protected void finalize() {
            if(mGrammar != 0) {
                destroy();
                throw new IllegalStateException("someone forgot to destroy Grammar");
            } else {
                return;
            }
        }

        public void resetAllSlots() {
            Recognizer.SR_GrammarResetAllSlots(mGrammar);
        }

        public void save(String s) throws IOException {
            Recognizer.SR_GrammarSave(mGrammar, s);
        }

        public void setupRecognizer() {
            Recognizer.SR_GrammarSetupRecognizer(mGrammar, mRecognizer);
            mActiveGrammar = this;
        }

        private int mGrammar;
        final Recognizer this$0;


        public Grammar(String s) throws IOException {
            this$0 = Recognizer.this;
            super();
            mGrammar = 0;
            mGrammar = Recognizer.SR_GrammarLoad(s);
            Recognizer.SR_GrammarSetupVocabulary(mGrammar, mVocabulary);
        }
    }


    public Recognizer(String s) throws IOException {
        mVocabulary = 0;
        mRecognizer = 0;
        mActiveGrammar = null;
        mPutAudioBuffer = null;
        PMemInit();
        SR_SessionCreate(s);
        mRecognizer = SR_RecognizerCreate();
        SR_RecognizerSetup(mRecognizer);
        mVocabulary = SR_VocabularyLoad();
    }

    private static native void PMemInit();

    private static native void PMemShutdown();

    private static native String SR_AcousticStateGet(int i);

    private static native void SR_AcousticStateReset(int i);

    private static native void SR_AcousticStateSet(int i, String s);

    private static native void SR_GrammarAddWordToSlot(int i, String s, String s1, String s2, int j, String s3);

    private static native void SR_GrammarAllowAll(int i);

    private static native void SR_GrammarAllowOnly(int i, String s);

    private static native void SR_GrammarCompile(int i);

    private static native int SR_GrammarCreate();

    private static native void SR_GrammarDestroy(int i);

    private static native int SR_GrammarLoad(String s);

    private static native void SR_GrammarResetAllSlots(int i);

    private static native void SR_GrammarSave(int i, String s);

    private static native void SR_GrammarSetupRecognizer(int i, int j);

    private static native void SR_GrammarSetupVocabulary(int i, int j);

    private static native void SR_GrammarUnsetupRecognizer(int i);

    private static native void SR_RecognizerActivateRule(int i, int j, String s, int k);

    private static native int SR_RecognizerAdvance(int i);

    private static native boolean SR_RecognizerCheckGrammarConsistency(int i, int j);

    private static native int SR_RecognizerCreate();

    private static native void SR_RecognizerDeactivateAllRules(int i);

    private static native void SR_RecognizerDeactivateRule(int i, int j, String s);

    private static native void SR_RecognizerDestroy(int i);

    private static native boolean SR_RecognizerGetBoolParameter(int i, String s);

    private static native String SR_RecognizerGetParameter(int i, String s);

    private static native int SR_RecognizerGetSize_tParameter(int i, String s);

    private static native boolean SR_RecognizerHasSetupRules(int i);

    private static native boolean SR_RecognizerIsActiveRule(int i, int j, String s);

    private static native boolean SR_RecognizerIsSetup(int i);

    private static native boolean SR_RecognizerIsSignalClipping(int i);

    private static native boolean SR_RecognizerIsSignalDCOffset(int i);

    private static native boolean SR_RecognizerIsSignalNoisy(int i);

    private static native boolean SR_RecognizerIsSignalTooFewSamples(int i);

    private static native boolean SR_RecognizerIsSignalTooManySamples(int i);

    private static native boolean SR_RecognizerIsSignalTooQuiet(int i);

    private static native int SR_RecognizerPutAudio(int i, byte abyte0[], int j, int k, boolean flag);

    private static native int SR_RecognizerResultGetKeyCount(int i, int j);

    private static native String[] SR_RecognizerResultGetKeyList(int i, int j);

    private static native int SR_RecognizerResultGetSize(int i);

    private static native String SR_RecognizerResultGetValue(int i, int j, String s);

    private static native byte[] SR_RecognizerResultGetWaveform(int i);

    private static native void SR_RecognizerSetBoolParameter(int i, String s, boolean flag);

    private static native void SR_RecognizerSetParameter(int i, String s, String s1);

    private static native void SR_RecognizerSetSize_tParameter(int i, String s, int j);

    private static native void SR_RecognizerSetup(int i);

    private static native void SR_RecognizerSetupRule(int i, int j, String s);

    private static native void SR_RecognizerStart(int i);

    private static native void SR_RecognizerStop(int i);

    private static native void SR_RecognizerUnsetup(int i);

    private static native void SR_SessionCreate(String s);

    private static native void SR_SessionDestroy();

    private static native void SR_VocabularyDestroy(int i);

    private static native String SR_VocabularyGetPronunciation(int i, String s);

    private static native int SR_VocabularyLoad();

    public static String eventToString(int i) {
        i;
        JVM INSTR tableswitch 0 12: default 68
    //                   0 90
    //                   1 96
    //                   2 102
    //                   3 108
    //                   4 114
    //                   5 120
    //                   6 126
    //                   7 132
    //                   8 138
    //                   9 144
    //                   10 150
    //                   11 156
    //                   12 162;
           goto _L1 _L2 _L3 _L4 _L5 _L6 _L7 _L8 _L9 _L10 _L11 _L12 _L13 _L14
_L1:
        String s = (new StringBuilder()).append("EVENT_").append(i).toString();
_L16:
        return s;
_L2:
        s = "EVENT_INVALID";
        continue; /* Loop/switch isn't completed */
_L3:
        s = "EVENT_NO_MATCH";
        continue; /* Loop/switch isn't completed */
_L4:
        s = "EVENT_INCOMPLETE";
        continue; /* Loop/switch isn't completed */
_L5:
        s = "EVENT_STARTED";
        continue; /* Loop/switch isn't completed */
_L6:
        s = "EVENT_STOPPED";
        continue; /* Loop/switch isn't completed */
_L7:
        s = "EVENT_START_OF_VOICING";
        continue; /* Loop/switch isn't completed */
_L8:
        s = "EVENT_END_OF_VOICING";
        continue; /* Loop/switch isn't completed */
_L9:
        s = "EVENT_SPOKE_TOO_SOON";
        continue; /* Loop/switch isn't completed */
_L10:
        s = "EVENT_RECOGNITION_RESULT";
        continue; /* Loop/switch isn't completed */
_L11:
        s = "EVENT_START_OF_UTTERANCE_TIMEOUT";
        continue; /* Loop/switch isn't completed */
_L12:
        s = "EVENT_RECOGNITION_TIMEOUT";
        continue; /* Loop/switch isn't completed */
_L13:
        s = "EVENT_NEED_MORE_AUDIO";
        continue; /* Loop/switch isn't completed */
_L14:
        s = "EVENT_MAX_SPEECH";
        if(true) goto _L16; else goto _L15
_L15:
    }

    public static String getConfigDir(Locale locale) {
        if(locale == null)
            locale = Locale.US;
        String s = (new StringBuilder()).append("/system/usr/srec/config/").append(locale.toString().replace('_', '.').toLowerCase()).toString();
        if(!(new File(s)).isDirectory())
            s = null;
        return s;
    }

    public int advance() {
        return SR_RecognizerAdvance(mRecognizer);
    }

    public void destroy() {
        if(mVocabulary != 0)
            SR_VocabularyDestroy(mVocabulary);
        mVocabulary = 0;
        if(mRecognizer != 0)
            SR_RecognizerUnsetup(mRecognizer);
        if(mRecognizer != 0)
            SR_RecognizerDestroy(mRecognizer);
        mRecognizer = 0;
        SR_SessionDestroy();
        PMemShutdown();
        return;
        Exception exception14;
        exception14;
        PMemShutdown();
        throw exception14;
        Exception exception12;
        exception12;
        mRecognizer = 0;
        SR_SessionDestroy();
        PMemShutdown();
        throw exception12;
        Exception exception13;
        exception13;
        PMemShutdown();
        throw exception13;
        Exception exception8;
        exception8;
        if(mRecognizer != 0)
            SR_RecognizerDestroy(mRecognizer);
        mRecognizer = 0;
        SR_SessionDestroy();
        PMemShutdown();
        throw exception8;
        Exception exception11;
        exception11;
        PMemShutdown();
        throw exception11;
        Exception exception9;
        exception9;
        mRecognizer = 0;
        SR_SessionDestroy();
        PMemShutdown();
        throw exception9;
        Exception exception10;
        exception10;
        PMemShutdown();
        throw exception10;
        Exception exception;
        exception;
        mVocabulary = 0;
        if(mRecognizer != 0)
            SR_RecognizerUnsetup(mRecognizer);
        if(mRecognizer != 0)
            SR_RecognizerDestroy(mRecognizer);
        mRecognizer = 0;
        SR_SessionDestroy();
        PMemShutdown();
        throw exception;
        Exception exception7;
        exception7;
        PMemShutdown();
        throw exception7;
        Exception exception5;
        exception5;
        mRecognizer = 0;
        SR_SessionDestroy();
        PMemShutdown();
        throw exception5;
        Exception exception6;
        exception6;
        PMemShutdown();
        throw exception6;
        Exception exception1;
        exception1;
        if(mRecognizer != 0)
            SR_RecognizerDestroy(mRecognizer);
        mRecognizer = 0;
        SR_SessionDestroy();
        PMemShutdown();
        throw exception1;
        Exception exception4;
        exception4;
        PMemShutdown();
        throw exception4;
        Exception exception2;
        exception2;
        mRecognizer = 0;
        SR_SessionDestroy();
        PMemShutdown();
        throw exception2;
        Exception exception3;
        exception3;
        PMemShutdown();
        throw exception3;
    }

    protected void finalize() throws Throwable {
        if(mVocabulary != 0 || mRecognizer != 0) {
            destroy();
            throw new IllegalStateException("someone forgot to destroy Recognizer");
        } else {
            return;
        }
    }

    public String getAcousticState() {
        return SR_AcousticStateGet(mRecognizer);
    }

    public String getResult(int i, String s) {
        return SR_RecognizerResultGetValue(mRecognizer, i, s);
    }

    public int getResultCount() {
        return SR_RecognizerResultGetSize(mRecognizer);
    }

    public String[] getResultKeys(int i) {
        return SR_RecognizerResultGetKeyList(mRecognizer, i);
    }

    public int putAudio(byte abyte0[], int i, int j, boolean flag) {
        return SR_RecognizerPutAudio(mRecognizer, abyte0, i, j, flag);
    }

    public void putAudio(InputStream inputstream) throws IOException {
        if(mPutAudioBuffer == null)
            mPutAudioBuffer = new byte[512];
        int i = inputstream.read(mPutAudioBuffer);
        if(i == -1)
            SR_RecognizerPutAudio(mRecognizer, mPutAudioBuffer, 0, 0, true);
        else
        if(i != SR_RecognizerPutAudio(mRecognizer, mPutAudioBuffer, 0, i, false))
            throw new IOException((new StringBuilder()).append("SR_RecognizerPutAudio failed nbytes=").append(i).toString());
    }

    public void resetAcousticState() {
        SR_AcousticStateReset(mRecognizer);
    }

    public void setAcousticState(String s) {
        SR_AcousticStateSet(mRecognizer, s);
    }

    public void start() {
        SR_RecognizerActivateRule(mRecognizer, mActiveGrammar.mGrammar, "trash", 1);
        SR_RecognizerStart(mRecognizer);
    }

    public void stop() {
        SR_RecognizerStop(mRecognizer);
        SR_RecognizerDeactivateRule(mRecognizer, mActiveGrammar.mGrammar, "trash");
    }

    public static final int EVENT_END_OF_VOICING = 6;
    public static final int EVENT_INCOMPLETE = 2;
    public static final int EVENT_INVALID = 0;
    public static final int EVENT_MAX_SPEECH = 12;
    public static final int EVENT_NEED_MORE_AUDIO = 11;
    public static final int EVENT_NO_MATCH = 1;
    public static final int EVENT_RECOGNITION_RESULT = 8;
    public static final int EVENT_RECOGNITION_TIMEOUT = 10;
    public static final int EVENT_SPOKE_TOO_SOON = 7;
    public static final int EVENT_STARTED = 3;
    public static final int EVENT_START_OF_UTTERANCE_TIMEOUT = 9;
    public static final int EVENT_START_OF_VOICING = 5;
    public static final int EVENT_STOPPED = 4;
    public static final String KEY_CONFIDENCE = "conf";
    public static final String KEY_LITERAL = "literal";
    public static final String KEY_MEANING = "meaning";
    private static String TAG = "Recognizer";
    private Grammar mActiveGrammar;
    private byte mPutAudioBuffer[];
    private int mRecognizer;
    private int mVocabulary;

    static  {
        System.loadLibrary("srec_jni");
    }











/*
    static Grammar access$802(Recognizer recognizer, Grammar grammar) {
        recognizer.mActiveGrammar = grammar;
        return grammar;
    }

*/

}
