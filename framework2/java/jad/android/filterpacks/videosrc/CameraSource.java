// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.filterpacks.videosrc;

import android.filterfw.core.*;
import android.filterfw.format.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.opengl.Matrix;
import android.util.Log;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

public class CameraSource extends Filter {

    public CameraSource(String s) {
        super(s);
        mCameraId = 0;
        mWidth = 320;
        mHeight = 240;
        mFps = 30;
        mWaitForNewFrame = true;
        onCameraFrameAvailableListener = new android.graphics.SurfaceTexture.OnFrameAvailableListener() {

            public void onFrameAvailable(SurfaceTexture surfacetexture) {
                if(mLogVerbose)
                    Log.v("CameraSource", "New frame from camera");
                CameraSource camerasource = CameraSource.this;
                camerasource;
                JVM INSTR monitorenter ;
                mNewFrameAvailable = true;
                notify();
                return;
            }

            final CameraSource this$0;

             {
                this$0 = CameraSource.this;
                super();
            }
        };
        mCameraTransform = new float[16];
        mMappedCoords = new float[16];
    }

    private void createFormats() {
        mOutputFormat = ImageFormat.create(mWidth, mHeight, 3, 3);
    }

    private int[] findClosestFpsRange(int i, android.hardware.Camera.Parameters parameters) {
        List list = parameters.getSupportedPreviewFpsRange();
        int ai[] = (int[])list.get(0);
        Iterator iterator = list.iterator();
        do {
            if(!iterator.hasNext())
                break;
            int ai1[] = (int[])iterator.next();
            if(ai1[0] < i * 1000 && ai1[1] > i * 1000 && ai1[0] > ai[0] && ai1[1] < ai[1])
                ai = ai1;
        } while(true);
        if(mLogVerbose)
            Log.v("CameraSource", (new StringBuilder()).append("Requested fps: ").append(i).append(".Closest frame rate range: [").append((double)ai[0] / 1000D).append(",").append((double)ai[1] / 1000D).append("]").toString());
        return ai;
    }

    private int[] findClosestSize(int i, int j, android.hardware.Camera.Parameters parameters) {
        List list = parameters.getSupportedPreviewSizes();
        int k = -1;
        int l = -1;
        int i1 = ((android.hardware.Camera.Size)list.get(0)).width;
        int j1 = ((android.hardware.Camera.Size)list.get(0)).height;
        Iterator iterator = list.iterator();
        do {
            if(!iterator.hasNext())
                break;
            android.hardware.Camera.Size size = (android.hardware.Camera.Size)iterator.next();
            if(size.width <= i && size.height <= j && size.width >= k && size.height >= l) {
                k = size.width;
                l = size.height;
            }
            if(size.width < i1 && size.height < j1) {
                i1 = size.width;
                j1 = size.height;
            }
        } while(true);
        if(k == -1) {
            k = i1;
            l = j1;
        }
        if(mLogVerbose)
            Log.v("CameraSource", (new StringBuilder()).append("Requested resolution: (").append(i).append(", ").append(j).append("). Closest match: (").append(k).append(", ").append(l).append(").").toString());
        int ai[] = new int[2];
        ai[0] = k;
        ai[1] = l;
        return ai;
    }

    public void close(FilterContext filtercontext) {
        if(mLogVerbose)
            Log.v("CameraSource", "Closing");
        mCamera.release();
        mCamera = null;
        mSurfaceTexture.release();
        mSurfaceTexture = null;
    }

    public void fieldPortValueUpdated(String s, FilterContext filtercontext) {
        if(s.equals("framerate")) {
            getCameraParameters();
            int ai[] = findClosestFpsRange(mFps, mCameraParameters);
            mCameraParameters.setPreviewFpsRange(ai[0], ai[1]);
            mCamera.setParameters(mCameraParameters);
        }
    }

    /**
     * @deprecated Method getCameraParameters is deprecated
     */

    public android.hardware.Camera.Parameters getCameraParameters() {
        this;
        JVM INSTR monitorenter ;
        boolean flag = false;
        android.hardware.Camera.Parameters parameters;
        if(mCameraParameters == null) {
            if(mCamera == null) {
                mCamera = Camera.open(mCameraId);
                flag = true;
            }
            mCameraParameters = mCamera.getParameters();
            if(flag) {
                mCamera.release();
                mCamera = null;
            }
        }
        int ai[] = findClosestSize(mWidth, mHeight, mCameraParameters);
        mWidth = ai[0];
        mHeight = ai[1];
        mCameraParameters.setPreviewSize(mWidth, mHeight);
        int ai1[] = findClosestFpsRange(mFps, mCameraParameters);
        mCameraParameters.setPreviewFpsRange(ai1[0], ai1[1]);
        parameters = mCameraParameters;
        this;
        JVM INSTR monitorexit ;
        return parameters;
        Exception exception;
        exception;
        throw exception;
    }

    public void open(FilterContext filtercontext) {
        if(mLogVerbose)
            Log.v("CameraSource", "Opening");
        mCamera = Camera.open(mCameraId);
        getCameraParameters();
        mCamera.setParameters(mCameraParameters);
        createFormats();
        mCameraFrame = (GLFrame)filtercontext.getFrameManager().newBoundFrame(mOutputFormat, 104, 0L);
        mSurfaceTexture = new SurfaceTexture(mCameraFrame.getTextureId());
        try {
            mCamera.setPreviewTexture(mSurfaceTexture);
        }
        catch(IOException ioexception) {
            throw new RuntimeException((new StringBuilder()).append("Could not bind camera surface texture: ").append(ioexception.getMessage()).append("!").toString());
        }
        mSurfaceTexture.setOnFrameAvailableListener(onCameraFrameAvailableListener);
        mNewFrameAvailable = false;
        mCamera.startPreview();
    }

    public void prepare(FilterContext filtercontext) {
        if(mLogVerbose)
            Log.v("CameraSource", "Preparing");
        mFrameExtractor = new ShaderProgram(filtercontext, "#extension GL_OES_EGL_image_external : require\nprecision mediump float;\nuniform samplerExternalOES tex_sampler_0;\nvarying vec2 v_texcoord;\nvoid main() {\n  gl_FragColor = texture2D(tex_sampler_0, v_texcoord);\n}\n");
    }

    public void process(FilterContext filtercontext) {
        if(mLogVerbose)
            Log.v("CameraSource", "Processing new frame");
        if(mWaitForNewFrame) {
            do {
                if(mNewFrameAvailable)
                    break;
                if(10 == 0)
                    throw new RuntimeException("Timeout waiting for new frame");
                try {
                    wait(100L);
                }
                catch(InterruptedException interruptedexception) {
                    if(mLogVerbose)
                        Log.v("CameraSource", "Interrupted while waiting for new frame");
                }
            } while(true);
            mNewFrameAvailable = false;
            if(mLogVerbose)
                Log.v("CameraSource", "Got new frame");
        }
        mSurfaceTexture.updateTexImage();
        if(mLogVerbose)
            Log.v("CameraSource", (new StringBuilder()).append("Using frame extractor in thread: ").append(Thread.currentThread()).toString());
        mSurfaceTexture.getTransformMatrix(mCameraTransform);
        Matrix.multiplyMM(mMappedCoords, 0, mCameraTransform, 0, mSourceCoords, 0);
        mFrameExtractor.setSourceRegion(mMappedCoords[0], mMappedCoords[1], mMappedCoords[4], mMappedCoords[5], mMappedCoords[8], mMappedCoords[9], mMappedCoords[12], mMappedCoords[13]);
        Frame frame = filtercontext.getFrameManager().newFrame(mOutputFormat);
        mFrameExtractor.process(mCameraFrame, frame);
        long l = mSurfaceTexture.getTimestamp();
        if(mLogVerbose)
            Log.v("CameraSource", (new StringBuilder()).append("Timestamp: ").append((double)l / 1000000000D).append(" s").toString());
        frame.setTimestamp(l);
        pushOutput("video", frame);
        frame.release();
        if(mLogVerbose)
            Log.v("CameraSource", "Done processing new frame");
    }

    /**
     * @deprecated Method setCameraParameters is deprecated
     */

    public void setCameraParameters(android.hardware.Camera.Parameters parameters) {
        this;
        JVM INSTR monitorenter ;
        parameters.setPreviewSize(mWidth, mHeight);
        mCameraParameters = parameters;
        if(isOpen())
            mCamera.setParameters(mCameraParameters);
        this;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        throw exception;
    }

    public void setupPorts() {
        addOutputPort("video", ImageFormat.create(3, 3));
    }

    public void tearDown(FilterContext filtercontext) {
        if(mCameraFrame != null)
            mCameraFrame.release();
    }

    private static final int NEWFRAME_TIMEOUT = 100;
    private static final int NEWFRAME_TIMEOUT_REPEAT = 10;
    private static final String TAG = "CameraSource";
    private static final String mFrameShader = "#extension GL_OES_EGL_image_external : require\nprecision mediump float;\nuniform samplerExternalOES tex_sampler_0;\nvarying vec2 v_texcoord;\nvoid main() {\n  gl_FragColor = texture2D(tex_sampler_0, v_texcoord);\n}\n";
    private static final float mSourceCoords[];
    private Camera mCamera;
    private GLFrame mCameraFrame;
    private int mCameraId;
    private android.hardware.Camera.Parameters mCameraParameters;
    private float mCameraTransform[];
    private int mFps;
    private ShaderProgram mFrameExtractor;
    private int mHeight;
    private final boolean mLogVerbose = Log.isLoggable("CameraSource", 2);
    private float mMappedCoords[];
    private boolean mNewFrameAvailable;
    private MutableFrameFormat mOutputFormat;
    private SurfaceTexture mSurfaceTexture;
    private boolean mWaitForNewFrame;
    private int mWidth;
    private android.graphics.SurfaceTexture.OnFrameAvailableListener onCameraFrameAvailableListener;

    static  {
        float af[] = new float[16];
        af[0] = 0.0F;
        af[1] = 1.0F;
        af[2] = 0.0F;
        af[3] = 1.0F;
        af[4] = 1.0F;
        af[5] = 1.0F;
        af[6] = 0.0F;
        af[7] = 1.0F;
        af[8] = 0.0F;
        af[9] = 0.0F;
        af[10] = 0.0F;
        af[11] = 1.0F;
        af[12] = 1.0F;
        af[13] = 0.0F;
        af[14] = 0.0F;
        af[15] = 1.0F;
        mSourceCoords = af;
    }



/*
    static boolean access$102(CameraSource camerasource, boolean flag) {
        camerasource.mNewFrameAvailable = flag;
        return flag;
    }

*/
}
