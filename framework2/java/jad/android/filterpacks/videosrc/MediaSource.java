// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.filterpacks.videosrc;

import android.content.res.AssetFileDescriptor;
import android.filterfw.core.*;
import android.filterfw.format.ImageFormat;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.opengl.Matrix;
import android.util.Log;
import android.view.Surface;
import java.io.IOException;

public class MediaSource extends Filter {

    public MediaSource(String s) {
        super(s);
        mSourceUrl = "";
        mSourceAsset = null;
        mSelectedIsUrl = false;
        mWaitForNewFrame = true;
        mLooping = true;
        mVolume = 0.0F;
        mOrientation = 0;
        onVideoSizeChangedListener = new android.media.MediaPlayer.OnVideoSizeChangedListener() {

            public void onVideoSizeChanged(MediaPlayer mediaplayer, int i, int j) {
                if(mLogVerbose)
                    Log.v("MediaSource", (new StringBuilder()).append("MediaPlayer sent dimensions: ").append(i).append(" x ").append(j).toString());
                if(mGotSize) goto _L2; else goto _L1
_L1:
                if(mOrientation == 0 || mOrientation == 180)
                    mOutputFormat.setDimensions(i, j);
                else
                    mOutputFormat.setDimensions(j, i);
                mWidth = i;
                mHeight = j;
_L4:
                synchronized(MediaSource.this) {
                    mGotSize = true;
                    notify();
                }
                return;
_L2:
                if(mOutputFormat.getWidth() != i || mOutputFormat.getHeight() != j)
                    Log.e("MediaSource", "Multiple video size change events received!");
                if(true) goto _L4; else goto _L3
_L3:
                exception;
                mediasource;
                JVM INSTR monitorexit ;
                throw exception;
            }

            final MediaSource this$0;

             {
                this$0 = MediaSource.this;
                super();
            }
        };
        onPreparedListener = new android.media.MediaPlayer.OnPreparedListener() {

            public void onPrepared(MediaPlayer mediaplayer) {
                if(mLogVerbose)
                    Log.v("MediaSource", "MediaPlayer is prepared");
                MediaSource mediasource = MediaSource.this;
                mediasource;
                JVM INSTR monitorenter ;
                mPrepared = true;
                notify();
                return;
            }

            final MediaSource this$0;

             {
                this$0 = MediaSource.this;
                super();
            }
        };
        onCompletionListener = new android.media.MediaPlayer.OnCompletionListener() {

            public void onCompletion(MediaPlayer mediaplayer) {
                if(mLogVerbose)
                    Log.v("MediaSource", "MediaPlayer has completed playback");
                MediaSource mediasource = MediaSource.this;
                mediasource;
                JVM INSTR monitorenter ;
                mCompleted = true;
                return;
            }

            final MediaSource this$0;

             {
                this$0 = MediaSource.this;
                super();
            }
        };
        onMediaFrameAvailableListener = new android.graphics.SurfaceTexture.OnFrameAvailableListener() {

            public void onFrameAvailable(SurfaceTexture surfacetexture) {
                if(mLogVerbose)
                    Log.v("MediaSource", "New frame from media player");
                MediaSource mediasource = MediaSource.this;
                mediasource;
                JVM INSTR monitorenter ;
                if(mLogVerbose)
                    Log.v("MediaSource", "New frame: notify");
                mNewFrameAvailable = true;
                notify();
                if(mLogVerbose)
                    Log.v("MediaSource", "New frame: notify done");
                return;
            }

            final MediaSource this$0;

             {
                this$0 = MediaSource.this;
                super();
            }
        };
        mNewFrameAvailable = false;
    }

    private void createFormats() {
        mOutputFormat = ImageFormat.create(3, 3);
    }

    /**
     * @deprecated Method setupMediaPlayer is deprecated
     */

    private boolean setupMediaPlayer(boolean flag) {
        this;
        JVM INSTR monitorenter ;
        mPrepared = false;
        mGotSize = false;
        mPlaying = false;
        mPaused = false;
        mCompleted = false;
        mNewFrameAvailable = false;
        if(mLogVerbose)
            Log.v("MediaSource", "Setting up playback.");
        if(mMediaPlayer == null)
            break MISSING_BLOCK_LABEL_98;
        if(mLogVerbose)
            Log.v("MediaSource", "Resetting existing MediaPlayer.");
        mMediaPlayer.reset();
_L1:
        if(mMediaPlayer == null)
            throw new RuntimeException("Unable to create a MediaPlayer!");
        break MISSING_BLOCK_LABEL_127;
        Exception exception;
        exception;
        this;
        JVM INSTR monitorexit ;
        throw exception;
        if(mLogVerbose)
            Log.v("MediaSource", "Creating new MediaPlayer.");
        mMediaPlayer = new MediaPlayer();
          goto _L1
        if(!flag)
            break MISSING_BLOCK_LABEL_301;
        if(mLogVerbose)
            Log.v("MediaSource", (new StringBuilder()).append("Setting MediaPlayer source to URI ").append(mSourceUrl).toString());
        mMediaPlayer.setDataSource(mSourceUrl);
_L2:
        mMediaPlayer.setLooping(mLooping);
        mMediaPlayer.setVolume(mVolume, mVolume);
        Surface surface = new Surface(mSurfaceTexture);
        mMediaPlayer.setSurface(surface);
        surface.release();
        mMediaPlayer.setOnVideoSizeChangedListener(onVideoSizeChangedListener);
        mMediaPlayer.setOnPreparedListener(onPreparedListener);
        mMediaPlayer.setOnCompletionListener(onCompletionListener);
        mSurfaceTexture.setOnFrameAvailableListener(onMediaFrameAvailableListener);
        if(mLogVerbose)
            Log.v("MediaSource", "Preparing MediaPlayer.");
        mMediaPlayer.prepareAsync();
        this;
        JVM INSTR monitorexit ;
        return true;
        if(mLogVerbose)
            Log.v("MediaSource", (new StringBuilder()).append("Setting MediaPlayer source to asset ").append(mSourceAsset).toString());
        mMediaPlayer.setDataSource(mSourceAsset.getFileDescriptor(), mSourceAsset.getStartOffset(), mSourceAsset.getLength());
          goto _L2
        IOException ioexception;
        ioexception;
        mMediaPlayer.release();
        mMediaPlayer = null;
        if(flag) {
            Object aobj3[] = new Object[1];
            aobj3[0] = mSourceUrl;
            throw new RuntimeException(String.format("Unable to set MediaPlayer to URL %s!", aobj3), ioexception);
        } else {
            Object aobj2[] = new Object[1];
            aobj2[0] = mSourceAsset;
            throw new RuntimeException(String.format("Unable to set MediaPlayer to asset %s!", aobj2), ioexception);
        }
        IllegalArgumentException illegalargumentexception;
        illegalargumentexception;
        mMediaPlayer.release();
        mMediaPlayer = null;
        if(flag) {
            Object aobj1[] = new Object[1];
            aobj1[0] = mSourceUrl;
            throw new RuntimeException(String.format("Unable to set MediaPlayer to URL %s!", aobj1), illegalargumentexception);
        } else {
            Object aobj[] = new Object[1];
            aobj[0] = mSourceAsset;
            throw new RuntimeException(String.format("Unable to set MediaPlayer to asset %s!", aobj), illegalargumentexception);
        }
    }

    public void close(FilterContext filtercontext) {
        if(mMediaPlayer.isPlaying())
            mMediaPlayer.stop();
        mPrepared = false;
        mGotSize = false;
        mPlaying = false;
        mPaused = false;
        mCompleted = false;
        mNewFrameAvailable = false;
        mMediaPlayer.release();
        mMediaPlayer = null;
        mSurfaceTexture.release();
        mSurfaceTexture = null;
        if(mLogVerbose)
            Log.v("MediaSource", "MediaSource closed");
    }

    public void fieldPortValueUpdated(String s, FilterContext filtercontext) {
        if(mLogVerbose)
            Log.v("MediaSource", "Parameter update");
        if(!s.equals("sourceUrl")) goto _L2; else goto _L1
_L1:
        if(isOpen()) {
            if(mLogVerbose)
                Log.v("MediaSource", "Opening new source URL");
            if(mSelectedIsUrl)
                setupMediaPlayer(mSelectedIsUrl);
        }
_L9:
        return;
_L2:
        if(s.equals("sourceAsset")) {
            if(isOpen()) {
                if(mLogVerbose)
                    Log.v("MediaSource", "Opening new source FD");
                if(!mSelectedIsUrl)
                    setupMediaPlayer(mSelectedIsUrl);
            }
            continue; /* Loop/switch isn't completed */
        }
        if(s.equals("loop")) {
            if(isOpen())
                mMediaPlayer.setLooping(mLooping);
            continue; /* Loop/switch isn't completed */
        }
        if(!s.equals("sourceIsUrl")) goto _L4; else goto _L3
_L3:
        if(!isOpen())
            continue; /* Loop/switch isn't completed */
        if(!mSelectedIsUrl) goto _L6; else goto _L5
_L5:
        if(mLogVerbose)
            Log.v("MediaSource", "Opening new source URL");
_L7:
        setupMediaPlayer(mSelectedIsUrl);
        continue; /* Loop/switch isn't completed */
_L6:
        if(mLogVerbose)
            Log.v("MediaSource", "Opening new source Asset");
        if(true) goto _L7; else goto _L4
_L4:
        if(s.equals("volume")) {
            if(isOpen())
                mMediaPlayer.setVolume(mVolume, mVolume);
        } else
        if(s.equals("orientation") && mGotSize) {
            if(mOrientation == 0 || mOrientation == 180)
                mOutputFormat.setDimensions(mWidth, mHeight);
            else
                mOutputFormat.setDimensions(mHeight, mWidth);
            mOrientationUpdated = true;
        }
        if(true) goto _L9; else goto _L8
_L8:
    }

    public void open(FilterContext filtercontext) {
        if(mLogVerbose) {
            Log.v("MediaSource", "Opening MediaSource");
            if(mSelectedIsUrl)
                Log.v("MediaSource", (new StringBuilder()).append("Current URL is ").append(mSourceUrl).toString());
            else
                Log.v("MediaSource", "Current source is Asset!");
        }
        mMediaFrame = (GLFrame)filtercontext.getFrameManager().newBoundFrame(mOutputFormat, 104, 0L);
        mSurfaceTexture = new SurfaceTexture(mMediaFrame.getTextureId());
        if(!setupMediaPlayer(mSelectedIsUrl))
            throw new RuntimeException("Error setting up MediaPlayer!");
        else
            return;
    }

    /**
     * @deprecated Method pauseVideo is deprecated
     */

    public void pauseVideo(boolean flag) {
        this;
        JVM INSTR monitorenter ;
        if(!isOpen()) goto _L2; else goto _L1
_L1:
        if(!flag || mPaused) goto _L4; else goto _L3
_L3:
        mMediaPlayer.pause();
_L2:
        mPaused = flag;
        this;
        JVM INSTR monitorexit ;
        return;
_L4:
        if(flag) goto _L2; else goto _L5
_L5:
        if(!mPaused) goto _L2; else goto _L6
_L6:
        mMediaPlayer.start();
          goto _L2
        Exception exception;
        exception;
        throw exception;
    }

    protected void prepare(FilterContext filtercontext) {
        if(mLogVerbose)
            Log.v("MediaSource", "Preparing MediaSource");
        mFrameExtractor = new ShaderProgram(filtercontext, "#extension GL_OES_EGL_image_external : require\nprecision mediump float;\nuniform samplerExternalOES tex_sampler_0;\nvarying vec2 v_texcoord;\nvoid main() {\n  gl_FragColor = texture2D(tex_sampler_0, v_texcoord);\n}\n");
        mFrameExtractor.setSourceRect(0.0F, 1.0F, 1.0F, -1F);
        createFormats();
    }

    public void process(FilterContext filtercontext) {
        if(mLogVerbose)
            Log.v("MediaSource", "Processing new frame");
        if(mMediaPlayer == null)
            throw new NullPointerException("Unexpected null media player!");
        if(!mCompleted) goto _L2; else goto _L1
_L1:
        closeOutputPort("video");
_L3:
        return;
_L2:
        int j;
        if(mPlaying)
            break MISSING_BLOCK_LABEL_164;
        j = 0;
        if(mLogVerbose)
            Log.v("MediaSource", "Waiting for preparation to complete");
_L5:
        if(mGotSize && mPrepared)
            break MISSING_BLOCK_LABEL_141;
        Frame frame;
        long l;
        float af[];
        float af1[];
        Object aobj[];
        int i;
        InterruptedException interruptedexception;
        try {
            wait(100L);
        }
        catch(InterruptedException interruptedexception1) { }
        if(!mCompleted)
            continue; /* Loop/switch isn't completed */
        closeOutputPort("video");
          goto _L3
        if(++j != 100) goto _L5; else goto _L4
_L4:
        mMediaPlayer.release();
        throw new RuntimeException("MediaPlayer timed out while preparing!");
        if(mLogVerbose)
            Log.v("MediaSource", "Starting playback");
        mMediaPlayer.start();
        if(mPaused && mPlaying)
            break MISSING_BLOCK_LABEL_313;
        if(!mWaitForNewFrame)
            break MISSING_BLOCK_LABEL_301;
        if(mLogVerbose)
            Log.v("MediaSource", "Waiting for new frame");
        i = 0;
_L6:
label0:
        {
            if(mNewFrameAvailable)
                break MISSING_BLOCK_LABEL_280;
            if(i != 10)
                break label0;
            if(mCompleted)
                closeOutputPort("video");
            else
                throw new RuntimeException("Timeout waiting for new frame!");
        }
          goto _L3
        try {
            wait(100L);
        }
        // Misplaced declaration of an exception variable
        catch(InterruptedException interruptedexception) {
            if(mLogVerbose)
                Log.v("MediaSource", "interrupted");
        }
        i++;
          goto _L6
        mNewFrameAvailable = false;
        if(mLogVerbose)
            Log.v("MediaSource", "Got new frame");
        mSurfaceTexture.updateTexImage();
        mOrientationUpdated = true;
        if(!mOrientationUpdated) goto _L8; else goto _L7
_L7:
        af = new float[16];
        mSurfaceTexture.getTransformMatrix(af);
        af1 = new float[16];
        mOrientation;
        JVM INSTR lookupswitch 3: default 380
    //                   90: 695
    //                   180: 711
    //                   270: 727;
           goto _L9 _L10 _L11 _L12
_L12:
        break MISSING_BLOCK_LABEL_727;
_L9:
        Matrix.multiplyMM(af1, 0, af, 0, mSourceCoords_0, 0);
_L13:
        if(mLogVerbose) {
            Log.v("MediaSource", (new StringBuilder()).append("OrientationHint = ").append(mOrientation).toString());
            aobj = new Object[8];
            aobj[0] = Float.valueOf(af1[4]);
            aobj[1] = Float.valueOf(af1[5]);
            aobj[2] = Float.valueOf(af1[0]);
            aobj[3] = Float.valueOf(af1[1]);
            aobj[4] = Float.valueOf(af1[12]);
            aobj[5] = Float.valueOf(af1[13]);
            aobj[6] = Float.valueOf(af1[8]);
            aobj[7] = Float.valueOf(af1[9]);
            Log.v("MediaSource", String.format("SetSourceRegion: %.2f, %.2f, %.2f, %.2f, %.2f, %.2f, %.2f, %.2f", aobj));
        }
        mFrameExtractor.setSourceRegion(af1[4], af1[5], af1[0], af1[1], af1[12], af1[13], af1[8], af1[9]);
        mOrientationUpdated = false;
_L8:
        frame = filtercontext.getFrameManager().newFrame(mOutputFormat);
        mFrameExtractor.process(mMediaFrame, frame);
        l = mSurfaceTexture.getTimestamp();
        if(mLogVerbose)
            Log.v("MediaSource", (new StringBuilder()).append("Timestamp: ").append((double)l / 1000000000D).append(" s").toString());
        frame.setTimestamp(l);
        pushOutput("video", frame);
        frame.release();
        mPlaying = true;
          goto _L3
_L10:
        Matrix.multiplyMM(af1, 0, af, 0, mSourceCoords_90, 0);
          goto _L13
_L11:
        Matrix.multiplyMM(af1, 0, af, 0, mSourceCoords_180, 0);
          goto _L13
        Matrix.multiplyMM(af1, 0, af, 0, mSourceCoords_270, 0);
          goto _L13
    }

    public void setupPorts() {
        addOutputPort("video", ImageFormat.create(3, 3));
    }

    public void tearDown(FilterContext filtercontext) {
        if(mMediaFrame != null)
            mMediaFrame.release();
    }

    private static final int NEWFRAME_TIMEOUT = 100;
    private static final int NEWFRAME_TIMEOUT_REPEAT = 10;
    private static final int PREP_TIMEOUT = 100;
    private static final int PREP_TIMEOUT_REPEAT = 100;
    private static final String TAG = "MediaSource";
    private static final float mSourceCoords_0[];
    private static final float mSourceCoords_180[];
    private static final float mSourceCoords_270[];
    private static final float mSourceCoords_90[];
    private boolean mCompleted;
    private ShaderProgram mFrameExtractor;
    private final String mFrameShader = "#extension GL_OES_EGL_image_external : require\nprecision mediump float;\nuniform samplerExternalOES tex_sampler_0;\nvarying vec2 v_texcoord;\nvoid main() {\n  gl_FragColor = texture2D(tex_sampler_0, v_texcoord);\n}\n";
    private boolean mGotSize;
    private int mHeight;
    private final boolean mLogVerbose = Log.isLoggable("MediaSource", 2);
    private boolean mLooping;
    private GLFrame mMediaFrame;
    private MediaPlayer mMediaPlayer;
    private boolean mNewFrameAvailable;
    private int mOrientation;
    private boolean mOrientationUpdated;
    private MutableFrameFormat mOutputFormat;
    private boolean mPaused;
    private boolean mPlaying;
    private boolean mPrepared;
    private boolean mSelectedIsUrl;
    private AssetFileDescriptor mSourceAsset;
    private String mSourceUrl;
    private SurfaceTexture mSurfaceTexture;
    private float mVolume;
    private boolean mWaitForNewFrame;
    private int mWidth;
    private android.media.MediaPlayer.OnCompletionListener onCompletionListener;
    private android.graphics.SurfaceTexture.OnFrameAvailableListener onMediaFrameAvailableListener;
    private android.media.MediaPlayer.OnPreparedListener onPreparedListener;
    private android.media.MediaPlayer.OnVideoSizeChangedListener onVideoSizeChangedListener;

    static  {
        float af[] = new float[16];
        af[0] = 1.0F;
        af[1] = 1.0F;
        af[2] = 0.0F;
        af[3] = 1.0F;
        af[4] = 0.0F;
        af[5] = 1.0F;
        af[6] = 0.0F;
        af[7] = 1.0F;
        af[8] = 1.0F;
        af[9] = 0.0F;
        af[10] = 0.0F;
        af[11] = 1.0F;
        af[12] = 0.0F;
        af[13] = 0.0F;
        af[14] = 0.0F;
        af[15] = 1.0F;
        mSourceCoords_0 = af;
        float af1[] = new float[16];
        af1[0] = 0.0F;
        af1[1] = 1.0F;
        af1[2] = 0.0F;
        af1[3] = 1.0F;
        af1[4] = 0.0F;
        af1[5] = 0.0F;
        af1[6] = 0.0F;
        af1[7] = 1.0F;
        af1[8] = 1.0F;
        af1[9] = 1.0F;
        af1[10] = 0.0F;
        af1[11] = 1.0F;
        af1[12] = 1.0F;
        af1[13] = 0.0F;
        af1[14] = 0.0F;
        af1[15] = 1.0F;
        mSourceCoords_270 = af1;
        float af2[] = new float[16];
        af2[0] = 0.0F;
        af2[1] = 0.0F;
        af2[2] = 0.0F;
        af2[3] = 1.0F;
        af2[4] = 1.0F;
        af2[5] = 0.0F;
        af2[6] = 0.0F;
        af2[7] = 1.0F;
        af2[8] = 0.0F;
        af2[9] = 1.0F;
        af2[10] = 0.0F;
        af2[11] = 1.0F;
        af2[12] = 1.0F;
        af2[13] = 1.0F;
        af2[14] = 0.0F;
        af2[15] = 1.0F;
        mSourceCoords_180 = af2;
        float af3[] = new float[16];
        af3[0] = 1.0F;
        af3[1] = 0.0F;
        af3[2] = 0.0F;
        af3[3] = 1.0F;
        af3[4] = 1.0F;
        af3[5] = 1.0F;
        af3[6] = 0.0F;
        af3[7] = 1.0F;
        af3[8] = 0.0F;
        af3[9] = 0.0F;
        af3[10] = 0.0F;
        af3[11] = 1.0F;
        af3[12] = 0.0F;
        af3[13] = 1.0F;
        af3[14] = 0.0F;
        af3[15] = 1.0F;
        mSourceCoords_90 = af3;
    }




/*
    static boolean access$102(MediaSource mediasource, boolean flag) {
        mediasource.mGotSize = flag;
        return flag;
    }

*/




/*
    static int access$402(MediaSource mediasource, int i) {
        mediasource.mWidth = i;
        return i;
    }

*/


/*
    static int access$502(MediaSource mediasource, int i) {
        mediasource.mHeight = i;
        return i;
    }

*/


/*
    static boolean access$602(MediaSource mediasource, boolean flag) {
        mediasource.mPrepared = flag;
        return flag;
    }

*/


/*
    static boolean access$702(MediaSource mediasource, boolean flag) {
        mediasource.mCompleted = flag;
        return flag;
    }

*/


/*
    static boolean access$802(MediaSource mediasource, boolean flag) {
        mediasource.mNewFrameAvailable = flag;
        return flag;
    }

*/
}
