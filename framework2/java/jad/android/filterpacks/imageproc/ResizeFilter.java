// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.filterpacks.imageproc;

import android.filterfw.core.*;
import android.filterfw.format.ImageFormat;

public class ResizeFilter extends Filter {

    public ResizeFilter(String s) {
        super(s);
        mKeepAspectRatio = false;
        mGenerateMipMap = false;
        mLastFormat = null;
    }

    protected void createProgram(FilterContext filtercontext, FrameFormat frameformat) {
        if(mLastFormat == null || mLastFormat.getTarget() != frameformat.getTarget()) goto _L2; else goto _L1
_L1:
        return;
_L2:
        mLastFormat = frameformat;
        switch(frameformat.getTarget()) {
        default:
            throw new RuntimeException("ResizeFilter could not create suitable program!");

        case 2: // '\002'
            throw new RuntimeException("Native ResizeFilter not implemented yet!");

        case 3: // '\003'
            mProgram = ShaderProgram.createIdentity(filtercontext);
            break;
        }
        if(true) goto _L1; else goto _L3
_L3:
    }

    public FrameFormat getOutputFormat(String s, FrameFormat frameformat) {
        return frameformat;
    }

    public void process(FilterContext filtercontext) {
        Frame frame = pullInput("image");
        createProgram(filtercontext, frame.getFormat());
        MutableFrameFormat mutableframeformat = frame.getFormat().mutableCopy();
        if(mKeepAspectRatio) {
            FrameFormat frameformat = frame.getFormat();
            mOHeight = (mOWidth * frameformat.getHeight()) / frameformat.getWidth();
        }
        mutableframeformat.setDimensions(mOWidth, mOHeight);
        Frame frame1 = filtercontext.getFrameManager().newFrame(mutableframeformat);
        if(mGenerateMipMap) {
            GLFrame glframe = (GLFrame)filtercontext.getFrameManager().newFrame(frame.getFormat());
            glframe.setTextureParameter(10241, 9985);
            glframe.setDataFromFrame(frame);
            glframe.generateMipMap();
            mProgram.process(glframe, frame1);
            glframe.release();
        } else {
            mProgram.process(frame, frame1);
        }
        pushOutput("image", frame1);
        frame1.release();
    }

    public void setupPorts() {
        addMaskedInputPort("image", ImageFormat.create(3));
        addOutputBasedOnInput("image", "image");
    }

    private boolean mGenerateMipMap;
    private int mInputChannels;
    private boolean mKeepAspectRatio;
    private FrameFormat mLastFormat;
    private int mOHeight;
    private int mOWidth;
    private MutableFrameFormat mOutputFormat;
    private Program mProgram;
}
