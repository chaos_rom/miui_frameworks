// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.filterpacks.imageproc;

import android.filterfw.core.*;
import android.filterfw.format.ImageFormat;
import android.filterfw.geometry.Point;
import android.filterfw.geometry.Quad;

public class FixedRotationFilter extends Filter {

    public FixedRotationFilter(String s) {
        super(s);
        mRotation = 0;
        mProgram = null;
    }

    public FrameFormat getOutputFormat(String s, FrameFormat frameformat) {
        return frameformat;
    }

    public void process(FilterContext filtercontext) {
        Frame frame = pullInput("image");
        if(mRotation != 0) goto _L2; else goto _L1
_L1:
        pushOutput("image", frame);
_L8:
        return;
_L2:
        MutableFrameFormat mutableframeformat;
        int i;
        int j;
        Point point;
        Point point1;
        Point point2;
        Point point3;
        FrameFormat frameformat = frame.getFormat();
        if(mProgram == null)
            mProgram = ShaderProgram.createIdentity(filtercontext);
        mutableframeformat = frameformat.mutableCopy();
        i = frameformat.getWidth();
        j = frameformat.getHeight();
        point = new Point(0.0F, 0.0F);
        point1 = new Point(1.0F, 0.0F);
        point2 = new Point(0.0F, 1.0F);
        point3 = new Point(1.0F, 1.0F);
        Math.round((float)mRotation / 90F) % 4;
        JVM INSTR tableswitch 1 3: default 144
    //                   1 208
    //                   2 237
    //                   3 257;
           goto _L3 _L4 _L5 _L6
_L6:
        break MISSING_BLOCK_LABEL_257;
_L4:
        break; /* Loop/switch isn't completed */
_L3:
        Quad quad = new Quad(point, point1, point2, point3);
_L9:
        Frame frame1 = filtercontext.getFrameManager().newFrame(mutableframeformat);
        mProgram.setSourceRegion(quad);
        mProgram.process(frame, frame1);
        pushOutput("image", frame1);
        frame1.release();
        if(true) goto _L8; else goto _L7
_L7:
        quad = new Quad(point2, point, point3, point1);
        mutableframeformat.setDimensions(j, i);
          goto _L9
_L5:
        quad = new Quad(point3, point2, point1, point);
          goto _L9
        quad = new Quad(point1, point3, point, point2);
        mutableframeformat.setDimensions(j, i);
          goto _L9
    }

    public void setupPorts() {
        addMaskedInputPort("image", ImageFormat.create(3, 3));
        addOutputBasedOnInput("image", "image");
    }

    private ShaderProgram mProgram;
    private int mRotation;
}
