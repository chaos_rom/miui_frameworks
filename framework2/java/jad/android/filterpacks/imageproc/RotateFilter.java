// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.filterpacks.imageproc;

import android.filterfw.core.*;
import android.filterfw.format.ImageFormat;
import android.filterfw.geometry.Point;
import android.filterfw.geometry.Quad;

public class RotateFilter extends Filter {

    public RotateFilter(String s) {
        super(s);
        mTileSize = 640;
        mWidth = 0;
        mHeight = 0;
        mTarget = 0;
    }

    private void updateParameters() {
        if(mAngle % 90 == 0) {
            float f;
            float f1;
            if(mAngle % 180 == 0) {
                f1 = 0.0F;
                Quad quad;
                if(mAngle % 360 == 0)
                    f = 1.0F;
                else
                    f = -1F;
            } else {
                f = 0.0F;
                if((90 + mAngle) % 360 == 0)
                    f1 = -1F;
                else
                    f1 = 1.0F;
                mOutputWidth = mHeight;
                mOutputHeight = mWidth;
            }
            quad = new Quad(new Point(0.5F * (1.0F + (f1 + -f)), 0.5F * (1.0F + (-f1 - f))), new Point(0.5F * (1.0F + (f + f1)), 0.5F * (1.0F + (f1 - f))), new Point(0.5F * (1.0F + (-f - f1)), 0.5F * (1.0F + (f + -f1))), new Point(0.5F * (1.0F + (f - f1)), 0.5F * (1.0F + (f1 + f))));
            ((ShaderProgram)mProgram).setTargetRegion(quad);
            return;
        } else {
            throw new RuntimeException("degree has to be multiply of 90.");
        }
    }

    public void fieldPortValueUpdated(String s, FilterContext filtercontext) {
        if(mProgram != null)
            updateParameters();
    }

    public void initProgram(FilterContext filtercontext, int i) {
        ShaderProgram shaderprogram;
        switch(i) {
        default:
            throw new RuntimeException((new StringBuilder()).append("Filter Sharpen does not support frames of target ").append(i).append("!").toString());

        case 3: // '\003'
            shaderprogram = ShaderProgram.createIdentity(filtercontext);
            break;
        }
        shaderprogram.setMaximumTileSize(mTileSize);
        shaderprogram.setClearsOutput(true);
        mProgram = shaderprogram;
        mTarget = i;
    }

    public void process(FilterContext filtercontext) {
        Frame frame = pullInput("image");
        FrameFormat frameformat = frame.getFormat();
        if(mProgram == null || frameformat.getTarget() != mTarget)
            initProgram(filtercontext, frameformat.getTarget());
        if(frameformat.getWidth() != mWidth || frameformat.getHeight() != mHeight) {
            mWidth = frameformat.getWidth();
            mHeight = frameformat.getHeight();
            mOutputWidth = mWidth;
            mOutputHeight = mHeight;
            updateParameters();
        }
        android.filterfw.core.MutableFrameFormat mutableframeformat = ImageFormat.create(mOutputWidth, mOutputHeight, 3, 3);
        Frame frame1 = filtercontext.getFrameManager().newFrame(mutableframeformat);
        mProgram.process(frame, frame1);
        pushOutput("image", frame1);
        frame1.release();
    }

    public void setupPorts() {
        addMaskedInputPort("image", ImageFormat.create(3));
        addOutputBasedOnInput("image", "image");
    }

    private int mAngle;
    private int mHeight;
    private int mOutputHeight;
    private int mOutputWidth;
    private Program mProgram;
    private int mTarget;
    private int mTileSize;
    private int mWidth;
}
