// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.filterpacks.imageproc;

import android.filterfw.core.*;
import android.filterfw.format.ImageFormat;
import android.filterfw.format.ObjectFormat;
import android.filterfw.geometry.Point;
import android.filterfw.geometry.Quad;
import android.opengl.GLES20;

public class DrawRectFilter extends Filter {

    public DrawRectFilter(String s) {
        super(s);
        mColorRed = 0.8F;
        mColorGreen = 0.8F;
        mColorBlue = 0.0F;
    }

    private void renderBox(Quad quad) {
        float af[] = new float[4];
        af[0] = mColorRed;
        af[1] = mColorGreen;
        af[2] = mColorBlue;
        af[3] = 1.0F;
        float af1[] = new float[8];
        af1[0] = quad.p0.x;
        af1[1] = quad.p0.y;
        af1[2] = quad.p1.x;
        af1[3] = quad.p1.y;
        af1[4] = quad.p3.x;
        af1[5] = quad.p3.y;
        af1[6] = quad.p2.x;
        af1[7] = quad.p2.y;
        mProgram.setHostValue("color", af);
        mProgram.setAttributeValues("aPosition", af1, 2);
        mProgram.setVertexCount(4);
        mProgram.beginDrawing();
        GLES20.glLineWidth(1.0F);
        GLES20.glDrawArrays(2, 0, 4);
    }

    public FrameFormat getOutputFormat(String s, FrameFormat frameformat) {
        return frameformat;
    }

    public void prepare(FilterContext filtercontext) {
        mProgram = new ShaderProgram(filtercontext, "attribute vec4 aPosition;\nvoid main() {\n  gl_Position = aPosition;\n}\n", "precision mediump float;\nuniform vec4 color;\nvoid main() {\n  gl_FragColor = color;\n}\n");
    }

    public void process(FilterContext filtercontext) {
        Frame frame = pullInput("image");
        Quad quad = ((Quad)pullInput("box").getObjectValue()).scaled(2.0F).translated(-1F, -1F);
        GLFrame glframe = (GLFrame)filtercontext.getFrameManager().duplicateFrame(frame);
        glframe.focus();
        renderBox(quad);
        pushOutput("image", glframe);
        glframe.release();
    }

    public void setupPorts() {
        addMaskedInputPort("image", ImageFormat.create(3, 3));
        addMaskedInputPort("box", ObjectFormat.fromClass(android/filterfw/geometry/Quad, 1));
        addOutputBasedOnInput("image", "image");
    }

    private float mColorBlue;
    private float mColorGreen;
    private float mColorRed;
    private final String mFixedColorFragmentShader = "precision mediump float;\nuniform vec4 color;\nvoid main() {\n  gl_FragColor = color;\n}\n";
    private ShaderProgram mProgram;
    private final String mVertexShader = "attribute vec4 aPosition;\nvoid main() {\n  gl_Position = aPosition;\n}\n";
}
