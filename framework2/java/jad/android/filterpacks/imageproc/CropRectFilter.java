// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.filterpacks.imageproc;

import android.filterfw.core.*;
import android.filterfw.format.ImageFormat;

public class CropRectFilter extends Filter {

    public CropRectFilter(String s) {
        super(s);
        mTileSize = 640;
        mWidth = 0;
        mHeight = 0;
        mTarget = 0;
    }

    public void fieldPortValueUpdated(String s, FilterContext filtercontext) {
        if(mProgram != null)
            updateSourceRect(mWidth, mHeight);
    }

    public void initProgram(FilterContext filtercontext, int i) {
        ShaderProgram shaderprogram;
        switch(i) {
        default:
            throw new RuntimeException((new StringBuilder()).append("Filter Sharpen does not support frames of target ").append(i).append("!").toString());

        case 3: // '\003'
            shaderprogram = ShaderProgram.createIdentity(filtercontext);
            break;
        }
        shaderprogram.setMaximumTileSize(mTileSize);
        mProgram = shaderprogram;
        mTarget = i;
    }

    public void process(FilterContext filtercontext) {
        Frame frame = pullInput("image");
        FrameFormat frameformat = frame.getFormat();
        android.filterfw.core.MutableFrameFormat mutableframeformat = ImageFormat.create(mOutputWidth, mOutputHeight, 3, 3);
        Frame frame1 = filtercontext.getFrameManager().newFrame(mutableframeformat);
        if(mProgram == null || frameformat.getTarget() != mTarget)
            initProgram(filtercontext, frameformat.getTarget());
        if(frameformat.getWidth() != mWidth || frameformat.getHeight() != mHeight)
            updateSourceRect(frameformat.getWidth(), frameformat.getHeight());
        mProgram.process(frame, frame1);
        pushOutput("image", frame1);
        frame1.release();
    }

    public void setupPorts() {
        addMaskedInputPort("image", ImageFormat.create(3));
        addOutputBasedOnInput("image", "image");
    }

    void updateSourceRect(int i, int j) {
        mWidth = i;
        mHeight = j;
        ((ShaderProgram)mProgram).setSourceRect((float)mXorigin / (float)mWidth, (float)mYorigin / (float)mHeight, (float)mOutputWidth / (float)mWidth, (float)mOutputHeight / (float)mHeight);
    }

    private int mHeight;
    private int mOutputHeight;
    private int mOutputWidth;
    private Program mProgram;
    private int mTarget;
    private int mTileSize;
    private int mWidth;
    private int mXorigin;
    private int mYorigin;
}
