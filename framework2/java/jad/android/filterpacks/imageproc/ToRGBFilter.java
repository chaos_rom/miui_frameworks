// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.filterpacks.imageproc;

import android.filterfw.core.*;

public class ToRGBFilter extends Filter {

    public ToRGBFilter(String s) {
        super(s);
        mLastFormat = null;
    }

    public void createProgram(FilterContext filtercontext, FrameFormat frameformat) {
        mInputBPP = frameformat.getBytesPerSample();
        if(mLastFormat == null || mLastFormat.getBytesPerSample() != mInputBPP) goto _L2; else goto _L1
_L1:
        return;
_L2:
        mLastFormat = frameformat;
        switch(mInputBPP) {
        case 2: // '\002'
        case 3: // '\003'
        default:
            throw new RuntimeException((new StringBuilder()).append("Unsupported BytesPerPixel: ").append(mInputBPP).append("!").toString());

        case 1: // '\001'
            mProgram = new NativeProgram("filterpack_imageproc", "gray_to_rgb");
            break;

        case 4: // '\004'
            mProgram = new NativeProgram("filterpack_imageproc", "rgba_to_rgb");
            break;
        }
        if(true) goto _L1; else goto _L3
_L3:
    }

    public FrameFormat getConvertedFormat(FrameFormat frameformat) {
        MutableFrameFormat mutableframeformat = frameformat.mutableCopy();
        mutableframeformat.setMetaValue("colorspace", Integer.valueOf(2));
        mutableframeformat.setBytesPerSample(3);
        return mutableframeformat;
    }

    public FrameFormat getOutputFormat(String s, FrameFormat frameformat) {
        return getConvertedFormat(frameformat);
    }

    public void process(FilterContext filtercontext) {
        Frame frame = pullInput("image");
        createProgram(filtercontext, frame.getFormat());
        Frame frame1 = filtercontext.getFrameManager().newFrame(getConvertedFormat(frame.getFormat()));
        mProgram.process(frame, frame1);
        pushOutput("image", frame1);
        frame1.release();
    }

    public void setupPorts() {
        MutableFrameFormat mutableframeformat = new MutableFrameFormat(2, 2);
        mutableframeformat.setDimensionCount(2);
        addMaskedInputPort("image", mutableframeformat);
        addOutputBasedOnInput("image", "image");
    }

    private int mInputBPP;
    private FrameFormat mLastFormat;
    private Program mProgram;
}
