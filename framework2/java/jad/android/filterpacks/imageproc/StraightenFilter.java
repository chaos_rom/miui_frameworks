// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.filterpacks.imageproc;

import android.filterfw.core.*;
import android.filterfw.format.ImageFormat;
import android.filterfw.geometry.Point;
import android.filterfw.geometry.Quad;

public class StraightenFilter extends Filter {

    public StraightenFilter(String s) {
        super(s);
        mAngle = 0.0F;
        mMaxAngle = 45F;
        mTileSize = 640;
        mWidth = 0;
        mHeight = 0;
        mTarget = 0;
    }

    private void updateParameters() {
        float f = 90F;
        float f1 = (float)Math.cos(0.01745329F * mAngle);
        float f2 = (float)Math.sin(0.01745329F * mAngle);
        if(mMaxAngle <= 0.0F)
            throw new RuntimeException("Max angle is out of range (0-180).");
        Point point;
        Point point1;
        Point point2;
        Point point3;
        float f3;
        float f4;
        float f5;
        Quad quad;
        if(mMaxAngle <= f)
            f = mMaxAngle;
        mMaxAngle = f;
        point = new Point(-f1 * (float)mWidth + f2 * (float)mHeight, -f2 * (float)mWidth - f1 * (float)mHeight);
        point1 = new Point(f1 * (float)mWidth + f2 * (float)mHeight, f2 * (float)mWidth - f1 * (float)mHeight);
        point2 = new Point(-f1 * (float)mWidth - f2 * (float)mHeight, -f2 * (float)mWidth + f1 * (float)mHeight);
        point3 = new Point(f1 * (float)mWidth - f2 * (float)mHeight, f2 * (float)mWidth + f1 * (float)mHeight);
        f3 = Math.max(Math.abs(point.x), Math.abs(point1.x));
        f4 = Math.max(Math.abs(point.y), Math.abs(point1.y));
        f5 = 0.5F * Math.min((float)mWidth / f3, (float)mHeight / f4);
        point.set(0.5F + (f5 * point.x) / (float)mWidth, 0.5F + (f5 * point.y) / (float)mHeight);
        point1.set(0.5F + (f5 * point1.x) / (float)mWidth, 0.5F + (f5 * point1.y) / (float)mHeight);
        point2.set(0.5F + (f5 * point2.x) / (float)mWidth, 0.5F + (f5 * point2.y) / (float)mHeight);
        point3.set(0.5F + (f5 * point3.x) / (float)mWidth, 0.5F + (f5 * point3.y) / (float)mHeight);
        quad = new Quad(point, point1, point2, point3);
        ((ShaderProgram)mProgram).setSourceRegion(quad);
    }

    public void fieldPortValueUpdated(String s, FilterContext filtercontext) {
        if(mProgram != null)
            updateParameters();
    }

    public void initProgram(FilterContext filtercontext, int i) {
        ShaderProgram shaderprogram;
        switch(i) {
        default:
            throw new RuntimeException((new StringBuilder()).append("Filter Sharpen does not support frames of target ").append(i).append("!").toString());

        case 3: // '\003'
            shaderprogram = ShaderProgram.createIdentity(filtercontext);
            break;
        }
        shaderprogram.setMaximumTileSize(mTileSize);
        mProgram = shaderprogram;
        mTarget = i;
    }

    public void process(FilterContext filtercontext) {
        Frame frame = pullInput("image");
        FrameFormat frameformat = frame.getFormat();
        if(mProgram == null || frameformat.getTarget() != mTarget)
            initProgram(filtercontext, frameformat.getTarget());
        if(frameformat.getWidth() != mWidth || frameformat.getHeight() != mHeight) {
            mWidth = frameformat.getWidth();
            mHeight = frameformat.getHeight();
            updateParameters();
        }
        Frame frame1 = filtercontext.getFrameManager().newFrame(frameformat);
        mProgram.process(frame, frame1);
        pushOutput("image", frame1);
        frame1.release();
    }

    public void setupPorts() {
        addMaskedInputPort("image", ImageFormat.create(3));
        addOutputBasedOnInput("image", "image");
    }

    private static final float DEGREE_TO_RADIAN = 0.01745329F;
    private float mAngle;
    private int mHeight;
    private float mMaxAngle;
    private Program mProgram;
    private int mTarget;
    private int mTileSize;
    private int mWidth;
}
