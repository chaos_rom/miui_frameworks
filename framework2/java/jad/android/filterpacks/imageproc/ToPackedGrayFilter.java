// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.filterpacks.imageproc;

import android.filterfw.core.*;
import android.filterfw.format.ImageFormat;

public class ToPackedGrayFilter extends Filter {

    public ToPackedGrayFilter(String s) {
        super(s);
        mOWidth = 0;
        mOHeight = 0;
        mKeepAspectRatio = false;
    }

    private void checkOutputDimensions(int i, int j) {
        if(i <= 0 || j <= 0)
            throw new RuntimeException((new StringBuilder()).append("Invalid output dimensions: ").append(i).append(" ").append(j).toString());
        else
            return;
    }

    private FrameFormat convertInputFormat(FrameFormat frameformat) {
        int i = mOWidth;
        int j = mOHeight;
        int k = frameformat.getWidth();
        int l = frameformat.getHeight();
        if(mOWidth == 0)
            i = k;
        if(mOHeight == 0)
            j = l;
        int i1;
        if(mKeepAspectRatio)
            if(k > l) {
                i = Math.max(i, j);
                j = (i * l) / k;
            } else {
                j = Math.max(i, j);
                i = (j * k) / l;
            }
        if(i > 0 && i < 4)
            i1 = 4;
        else
            i1 = 4 * (i / 4);
        return ImageFormat.create(i1, j, 1, 2);
    }

    public FrameFormat getOutputFormat(String s, FrameFormat frameformat) {
        return convertInputFormat(frameformat);
    }

    public void prepare(FilterContext filtercontext) {
        mProgram = new ShaderProgram(filtercontext, "precision mediump float;\nconst vec4 coeff_y = vec4(0.299, 0.587, 0.114, 0);\nuniform sampler2D tex_sampler_0;\nuniform float pix_stride;\nvarying vec2 v_texcoord;\nvoid main() {\n  for (int i = 0; i < 4; ++i) {\n    vec4 p = texture2D(tex_sampler_0,\n                       v_texcoord + vec2(pix_stride * float(i), 0.0));\n    gl_FragColor[i] = dot(p, coeff_y);\n  }\n}\n");
    }

    public void process(FilterContext filtercontext) {
        Frame frame = pullInput("image");
        FrameFormat frameformat = frame.getFormat();
        FrameFormat frameformat1 = convertInputFormat(frameformat);
        int i = frameformat1.getWidth();
        int j = frameformat1.getHeight();
        checkOutputDimensions(i, j);
        mProgram.setHostValue("pix_stride", Float.valueOf(1.0F / (float)i));
        MutableFrameFormat mutableframeformat = frameformat.mutableCopy();
        mutableframeformat.setDimensions(i / 4, j);
        Frame frame1 = filtercontext.getFrameManager().newFrame(mutableframeformat);
        mProgram.process(frame, frame1);
        Frame frame2 = filtercontext.getFrameManager().newFrame(frameformat1);
        frame2.setDataFromFrame(frame1);
        frame1.release();
        pushOutput("image", frame2);
        frame2.release();
    }

    public void setupPorts() {
        addMaskedInputPort("image", ImageFormat.create(3, 3));
        addOutputBasedOnInput("image", "image");
    }

    private final String mColorToPackedGrayShader = "precision mediump float;\nconst vec4 coeff_y = vec4(0.299, 0.587, 0.114, 0);\nuniform sampler2D tex_sampler_0;\nuniform float pix_stride;\nvarying vec2 v_texcoord;\nvoid main() {\n  for (int i = 0; i < 4; ++i) {\n    vec4 p = texture2D(tex_sampler_0,\n                       v_texcoord + vec2(pix_stride * float(i), 0.0));\n    gl_FragColor[i] = dot(p, coeff_y);\n  }\n}\n";
    private boolean mKeepAspectRatio;
    private int mOHeight;
    private int mOWidth;
    private Program mProgram;
}
