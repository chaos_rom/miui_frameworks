// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.filterpacks.imageproc;

import android.filterfw.core.*;
import android.filterfw.format.ImageFormat;

public class ImageSlicer extends Filter {

    public ImageSlicer(String s) {
        super(s);
        mSliceIndex = 0;
    }

    private void calcOutputFormatForInput(Frame frame) {
        mInputWidth = frame.getFormat().getWidth();
        mInputHeight = frame.getFormat().getHeight();
        mSliceWidth = (-1 + (mInputWidth + mXSlices)) / mXSlices;
        mSliceHeight = (-1 + (mInputHeight + mYSlices)) / mYSlices;
        mOutputWidth = mSliceWidth + 2 * mPadSize;
        mOutputHeight = mSliceHeight + 2 * mPadSize;
    }

    public FrameFormat getOutputFormat(String s, FrameFormat frameformat) {
        return frameformat;
    }

    public void process(FilterContext filtercontext) {
        if(mSliceIndex == 0) {
            mOriginalFrame = pullInput("image");
            calcOutputFormatForInput(mOriginalFrame);
        }
        MutableFrameFormat mutableframeformat = mOriginalFrame.getFormat().mutableCopy();
        mutableframeformat.setDimensions(mOutputWidth, mOutputHeight);
        Frame frame = filtercontext.getFrameManager().newFrame(mutableframeformat);
        if(mProgram == null)
            mProgram = ShaderProgram.createIdentity(filtercontext);
        int i = mSliceIndex % mXSlices;
        int j = mSliceIndex / mXSlices;
        float f = (float)(i * mSliceWidth - mPadSize) / (float)mInputWidth;
        float f1 = (float)(j * mSliceHeight - mPadSize) / (float)mInputHeight;
        ((ShaderProgram)mProgram).setSourceRect(f, f1, (float)mOutputWidth / (float)mInputWidth, (float)mOutputHeight / (float)mInputHeight);
        mProgram.process(mOriginalFrame, frame);
        mSliceIndex = 1 + mSliceIndex;
        if(mSliceIndex == mXSlices * mYSlices) {
            mSliceIndex = 0;
            mOriginalFrame.release();
            setWaitsOnInputPort("image", true);
        } else {
            mOriginalFrame.retain();
            setWaitsOnInputPort("image", false);
        }
        pushOutput("image", frame);
        frame.release();
    }

    public void setupPorts() {
        addMaskedInputPort("image", ImageFormat.create(3, 3));
        addOutputBasedOnInput("image", "image");
    }

    private int mInputHeight;
    private int mInputWidth;
    private Frame mOriginalFrame;
    private int mOutputHeight;
    private int mOutputWidth;
    private int mPadSize;
    private Program mProgram;
    private int mSliceHeight;
    private int mSliceIndex;
    private int mSliceWidth;
    private int mXSlices;
    private int mYSlices;
}
