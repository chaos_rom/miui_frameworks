// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.filterpacks.videosink;


public class MediaRecorderStopException extends RuntimeException {

    public MediaRecorderStopException() {
    }

    public MediaRecorderStopException(String s) {
        super(s);
    }

    public MediaRecorderStopException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public MediaRecorderStopException(Throwable throwable) {
        super(throwable);
    }

    private static final String TAG = "MediaRecorderStopException";
}
