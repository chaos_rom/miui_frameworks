// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.filterpacks.base;

import android.filterfw.core.*;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;

public class OutputStreamTarget extends Filter {

    public OutputStreamTarget(String s) {
        super(s);
    }

    public void process(FilterContext filtercontext) {
        Frame frame = pullInput("data");
        ByteBuffer bytebuffer;
        if(frame.getFormat().getObjectClass() == java/lang/String)
            bytebuffer = ByteBuffer.wrap(((String)frame.getObjectValue()).getBytes());
        else
            bytebuffer = frame.getData();
        try {
            mOutputStream.write(bytebuffer.array(), 0, bytebuffer.limit());
            mOutputStream.flush();
            return;
        }
        catch(IOException ioexception) {
            throw new RuntimeException((new StringBuilder()).append("OutputStreamTarget: Could not write to stream: ").append(ioexception.getMessage()).append("!").toString());
        }
    }

    public void setupPorts() {
        addInputPort("data");
    }

    private OutputStream mOutputStream;
}
