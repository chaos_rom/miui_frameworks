// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.filterpacks.base;

import android.filterfw.core.*;
import android.filterfw.format.ImageFormat;

public class GLTextureTarget extends Filter {

    public GLTextureTarget(String s) {
        super(s);
    }

    public void process(FilterContext filtercontext) {
        Frame frame = pullInput("frame");
        android.filterfw.core.MutableFrameFormat mutableframeformat = ImageFormat.create(frame.getFormat().getWidth(), frame.getFormat().getHeight(), 3, 3);
        Frame frame1 = filtercontext.getFrameManager().newBoundFrame(mutableframeformat, 100, mTexId);
        frame1.setDataFromFrame(frame);
        frame1.release();
    }

    public void setupPorts() {
        addMaskedInputPort("frame", ImageFormat.create(3));
    }

    private int mTexId;
}
