// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.filterpacks.base;

import android.filterfw.core.*;

public class RetargetFilter extends Filter {

    public RetargetFilter(String s) {
        super(s);
        mTarget = -1;
    }

    public FrameFormat getOutputFormat(String s, FrameFormat frameformat) {
        MutableFrameFormat mutableframeformat = frameformat.mutableCopy();
        mutableframeformat.setTarget(mTarget);
        return mutableframeformat;
    }

    public void process(FilterContext filtercontext) {
        Frame frame = pullInput("frame");
        Frame frame1 = filtercontext.getFrameManager().duplicateFrameToTarget(frame, mTarget);
        pushOutput("frame", frame1);
        frame1.release();
    }

    public void setupPorts() {
        mTarget = FrameFormat.readTargetString(mTargetString);
        addInputPort("frame");
        addOutputBasedOnInput("frame", "frame");
    }

    private MutableFrameFormat mOutputFormat;
    private int mTarget;
    private String mTargetString;
}
