// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.filterpacks.base;

import android.filterfw.core.*;

public class FrameFetch extends Filter {

    public FrameFetch(String s) {
        super(s);
        mRepeatFrame = false;
    }

    public void process(FilterContext filtercontext) {
        android.filterfw.core.Frame frame = filtercontext.fetchFrame(mKey);
        if(frame != null) {
            pushOutput("frame", frame);
            if(!mRepeatFrame)
                closeOutputPort("frame");
        } else {
            delayNextProcess(250);
        }
    }

    public void setupPorts() {
        FrameFormat frameformat;
        if(mFormat == null)
            frameformat = FrameFormat.unspecified();
        else
            frameformat = mFormat;
        addOutputPort("frame", frameformat);
    }

    private FrameFormat mFormat;
    private String mKey;
    private boolean mRepeatFrame;
}
