// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.filterpacks.base;

import android.filterfw.core.*;
import android.filterfw.format.PrimitiveFormat;
import java.io.*;
import java.nio.ByteBuffer;

public class InputStreamSource extends Filter {

    public InputStreamSource(String s) {
        super(s);
        mOutputFormat = null;
    }

    public void process(FilterContext filtercontext) {
        int i = 0;
        ByteBuffer bytebuffer;
        Frame frame;
        try {
            ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
            byte abyte0[] = new byte[1024];
            do {
                int j = mInputStream.read(abyte0);
                if(j <= 0)
                    break;
                bytearrayoutputstream.write(abyte0, 0, j);
                i += j;
            } while(true);
            bytebuffer = ByteBuffer.wrap(bytearrayoutputstream.toByteArray());
        }
        catch(IOException ioexception) {
            throw new RuntimeException((new StringBuilder()).append("InputStreamSource: Could not read stream: ").append(ioexception.getMessage()).append("!").toString());
        }
        mOutputFormat.setDimensions(i);
        frame = filtercontext.getFrameManager().newFrame(mOutputFormat);
        frame.setData(bytebuffer);
        pushOutput("data", frame);
        frame.release();
        closeOutputPort("data");
    }

    public void setupPorts() {
        int i = FrameFormat.readTargetString(mTarget);
        if(mOutputFormat == null)
            mOutputFormat = PrimitiveFormat.createByteFormat(i);
        addOutputPort("data", mOutputFormat);
    }

    private InputStream mInputStream;
    private MutableFrameFormat mOutputFormat;
    private String mTarget;
}
