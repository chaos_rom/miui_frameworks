// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.filterpacks.performance;

import android.filterfw.core.*;
import android.filterfw.format.ObjectFormat;
import android.os.SystemClock;

// Referenced classes of package android.filterpacks.performance:
//            Throughput

public class ThroughputFilter extends Filter {

    public ThroughputFilter(String s) {
        super(s);
        mPeriod = 5;
        mLastTime = 0L;
        mTotalFrameCount = 0;
        mPeriodFrameCount = 0;
    }

    public FrameFormat getOutputFormat(String s, FrameFormat frameformat) {
        return frameformat;
    }

    public void open(FilterContext filtercontext) {
        mTotalFrameCount = 0;
        mPeriodFrameCount = 0;
        mLastTime = 0L;
    }

    public void process(FilterContext filtercontext) {
        Frame frame = pullInput("frame");
        pushOutput("frame", frame);
        mTotalFrameCount = 1 + mTotalFrameCount;
        mPeriodFrameCount = 1 + mPeriodFrameCount;
        if(mLastTime == 0L)
            mLastTime = SystemClock.elapsedRealtime();
        long l = SystemClock.elapsedRealtime();
        if(l - mLastTime >= (long)(1000 * mPeriod)) {
            FrameFormat frameformat = frame.getFormat();
            int i = frameformat.getWidth() * frameformat.getHeight();
            Throughput throughput = new Throughput(mTotalFrameCount, mPeriodFrameCount, mPeriod, i);
            Frame frame1 = filtercontext.getFrameManager().newFrame(mOutputFormat);
            frame1.setObjectValue(throughput);
            pushOutput("throughput", frame1);
            mLastTime = l;
            mPeriodFrameCount = 0;
        }
    }

    public void setupPorts() {
        addInputPort("frame");
        mOutputFormat = ObjectFormat.fromClass(android/filterpacks/performance/Throughput, 1);
        addOutputBasedOnInput("frame", "frame");
        addOutputPort("throughput", mOutputFormat);
    }

    private long mLastTime;
    private FrameFormat mOutputFormat;
    private int mPeriod;
    private int mPeriodFrameCount;
    private int mTotalFrameCount;
}
