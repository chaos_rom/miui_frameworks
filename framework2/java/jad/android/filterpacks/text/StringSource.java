// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.filterpacks.text;

import android.filterfw.core.*;
import android.filterfw.format.ObjectFormat;

public class StringSource extends Filter {

    public StringSource(String s) {
        super(s);
    }

    public void process(FilterContext filtercontext) {
        Frame frame = filtercontext.getFrameManager().newFrame(mOutputFormat);
        frame.setObjectValue(mString);
        frame.setTimestamp(-1L);
        pushOutput("string", frame);
        closeOutputPort("string");
    }

    public void setupPorts() {
        mOutputFormat = ObjectFormat.fromClass(java/lang/String, 1);
        addOutputPort("string", mOutputFormat);
    }

    private FrameFormat mOutputFormat;
    private String mString;
}
