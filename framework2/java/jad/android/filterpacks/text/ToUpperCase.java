// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.filterpacks.text;

import android.filterfw.core.*;
import android.filterfw.format.ObjectFormat;

public class ToUpperCase extends Filter {

    public ToUpperCase(String s) {
        super(s);
    }

    public void process(FilterContext filtercontext) {
        String s = (String)pullInput("mixedcase").getObjectValue();
        Frame frame = filtercontext.getFrameManager().newFrame(mOutputFormat);
        frame.setObjectValue(s.toUpperCase());
        pushOutput("uppercase", frame);
    }

    public void setupPorts() {
        mOutputFormat = ObjectFormat.fromClass(java/lang/String, 1);
        addMaskedInputPort("mixedcase", mOutputFormat);
        addOutputPort("uppercase", mOutputFormat);
    }

    private FrameFormat mOutputFormat;
}
