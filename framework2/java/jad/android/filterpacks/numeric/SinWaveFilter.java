// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.filterpacks.numeric;

import android.filterfw.core.*;
import android.filterfw.format.ObjectFormat;

public class SinWaveFilter extends Filter {

    public SinWaveFilter(String s) {
        super(s);
        mStepSize = 0.05F;
        mValue = 0.0F;
    }

    public void open(FilterContext filtercontext) {
        mValue = 0.0F;
    }

    public void process(FilterContext filtercontext) {
        Frame frame = filtercontext.getFrameManager().newFrame(mOutputFormat);
        frame.setObjectValue(Float.valueOf((1.0F + (float)Math.sin(mValue)) / 2.0F));
        pushOutput("value", frame);
        mValue = mValue + mStepSize;
        frame.release();
    }

    public void setupPorts() {
        mOutputFormat = ObjectFormat.fromClass(java/lang/Float, 1);
        addOutputPort("value", mOutputFormat);
    }

    private FrameFormat mOutputFormat;
    private float mStepSize;
    private float mValue;
}
