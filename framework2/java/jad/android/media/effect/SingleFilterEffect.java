// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.media.effect;

import android.filterfw.core.*;

// Referenced classes of package android.media.effect:
//            FilterEffect, EffectContext

public class SingleFilterEffect extends FilterEffect {

    public transient SingleFilterEffect(EffectContext effectcontext, String s, Class class1, String s1, String s2, Object aobj[]) {
        super(effectcontext, s);
        mInputName = s1;
        mOutputName = s2;
        String s3 = class1.getSimpleName();
        Filter filter = FilterFactory.sharedFactory().createFilterByClass(class1, s3);
        filter.initWithAssignmentList(aobj);
        mFunction = new FilterFunction(getFilterContext(), filter);
    }

    public void apply(int i, int j, int k, int l) {
        beginGLEffect();
        Frame frame = frameFromTexture(i, j, k);
        Frame frame1 = frameFromTexture(l, j, k);
        FilterFunction filterfunction = mFunction;
        Object aobj[] = new Object[2];
        aobj[0] = mInputName;
        aobj[1] = frame;
        Frame frame2 = filterfunction.executeWithArgList(aobj);
        frame1.setDataFromFrame(frame2);
        frame.release();
        frame1.release();
        frame2.release();
        endGLEffect();
    }

    public void release() {
        mFunction.tearDown();
        mFunction = null;
    }

    public void setParameter(String s, Object obj) {
        mFunction.setInputValue(s, obj);
    }

    protected FilterFunction mFunction;
    protected String mInputName;
    protected String mOutputName;
}
