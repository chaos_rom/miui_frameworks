// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.media.effect;

import java.lang.reflect.Constructor;

// Referenced classes of package android.media.effect:
//            Effect, EffectContext

public class EffectFactory {

    EffectFactory(EffectContext effectcontext) {
        mEffectContext = effectcontext;
    }

    private static Class getEffectClassByName(String s) {
        Class class1;
        ClassLoader classloader;
        String as[];
        int i;
        int j;
        class1 = null;
        classloader = Thread.currentThread().getContextClassLoader();
        as = EFFECT_PACKAGES;
        i = as.length;
        j = 0;
_L2:
        String s1;
        if(j >= i)
            break MISSING_BLOCK_LABEL_65;
        s1 = as[j];
        Class class2 = classloader.loadClass((new StringBuilder()).append(s1).append(s).toString());
        class1 = class2;
        if(class1 == null)
            break MISSING_BLOCK_LABEL_69;
        return class1;
        ClassNotFoundException classnotfoundexception;
        classnotfoundexception;
        j++;
        if(true) goto _L2; else goto _L1
_L1:
    }

    private Effect instantiateEffect(Class class1, String s) {
        Constructor constructor;
        Effect effect;
        try {
            class1.asSubclass(android/media/effect/Effect);
        }
        catch(ClassCastException classcastexception) {
            throw new IllegalArgumentException((new StringBuilder()).append("Attempting to allocate effect '").append(class1).append("' which is not a subclass of Effect!").toString(), classcastexception);
        }
        try {
            Class aclass[] = new Class[2];
            aclass[0] = android/media/effect/EffectContext;
            aclass[1] = java/lang/String;
            constructor = class1.getConstructor(aclass);
        }
        catch(NoSuchMethodException nosuchmethodexception) {
            throw new RuntimeException((new StringBuilder()).append("The effect class '").append(class1).append("' does not have ").append("the required constructor.").toString(), nosuchmethodexception);
        }
        try {
            Object aobj[] = new Object[2];
            aobj[0] = mEffectContext;
            aobj[1] = s;
            effect = (Effect)constructor.newInstance(aobj);
        }
        catch(Throwable throwable) {
            throw new RuntimeException((new StringBuilder()).append("There was an error constructing the effect '").append(class1).append("'!").toString(), throwable);
        }
        return effect;
    }

    public static boolean isEffectSupported(String s) {
        boolean flag;
        if(getEffectClassByName(s) != null)
            flag = true;
        else
            flag = false;
        return flag;
    }

    public Effect createEffect(String s) {
        Class class1 = getEffectClassByName(s);
        if(class1 == null)
            throw new IllegalArgumentException((new StringBuilder()).append("Cannot instantiate unknown effect '").append(s).append("'!").toString());
        else
            return instantiateEffect(class1, s);
    }

    public static final String EFFECT_AUTOFIX = "android.media.effect.effects.AutoFixEffect";
    public static final String EFFECT_BACKDROPPER = "android.media.effect.effects.BackDropperEffect";
    public static final String EFFECT_BITMAPOVERLAY = "android.media.effect.effects.BitmapOverlayEffect";
    public static final String EFFECT_BLACKWHITE = "android.media.effect.effects.BlackWhiteEffect";
    public static final String EFFECT_BRIGHTNESS = "android.media.effect.effects.BrightnessEffect";
    public static final String EFFECT_CONTRAST = "android.media.effect.effects.ContrastEffect";
    public static final String EFFECT_CROP = "android.media.effect.effects.CropEffect";
    public static final String EFFECT_CROSSPROCESS = "android.media.effect.effects.CrossProcessEffect";
    public static final String EFFECT_DOCUMENTARY = "android.media.effect.effects.DocumentaryEffect";
    public static final String EFFECT_DUOTONE = "android.media.effect.effects.DuotoneEffect";
    public static final String EFFECT_FILLLIGHT = "android.media.effect.effects.FillLightEffect";
    public static final String EFFECT_FISHEYE = "android.media.effect.effects.FisheyeEffect";
    public static final String EFFECT_FLIP = "android.media.effect.effects.FlipEffect";
    public static final String EFFECT_GRAIN = "android.media.effect.effects.GrainEffect";
    public static final String EFFECT_GRAYSCALE = "android.media.effect.effects.GrayscaleEffect";
    public static final String EFFECT_IDENTITY = "IdentityEffect";
    public static final String EFFECT_LOMOISH = "android.media.effect.effects.LomoishEffect";
    public static final String EFFECT_NEGATIVE = "android.media.effect.effects.NegativeEffect";
    private static final String EFFECT_PACKAGES[];
    public static final String EFFECT_POSTERIZE = "android.media.effect.effects.PosterizeEffect";
    public static final String EFFECT_REDEYE = "android.media.effect.effects.RedEyeEffect";
    public static final String EFFECT_ROTATE = "android.media.effect.effects.RotateEffect";
    public static final String EFFECT_SATURATE = "android.media.effect.effects.SaturateEffect";
    public static final String EFFECT_SEPIA = "android.media.effect.effects.SepiaEffect";
    public static final String EFFECT_SHARPEN = "android.media.effect.effects.SharpenEffect";
    public static final String EFFECT_STRAIGHTEN = "android.media.effect.effects.StraightenEffect";
    public static final String EFFECT_TEMPERATURE = "android.media.effect.effects.ColorTemperatureEffect";
    public static final String EFFECT_TINT = "android.media.effect.effects.TintEffect";
    public static final String EFFECT_VIGNETTE = "android.media.effect.effects.VignetteEffect";
    private EffectContext mEffectContext;

    static  {
        String as[] = new String[2];
        as[0] = "android.media.effect.effects.";
        as[1] = "";
        EFFECT_PACKAGES = as;
    }
}
