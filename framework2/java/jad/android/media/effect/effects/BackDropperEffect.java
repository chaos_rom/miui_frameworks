// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.media.effect.effects;

import android.filterfw.core.*;
import android.filterpacks.videoproc.BackDropperFilter;
import android.media.effect.*;

public class BackDropperEffect extends FilterGraphEffect {

    public BackDropperEffect(EffectContext effectcontext, String s) {
        super(effectcontext, s, "@import android.filterpacks.base;\n@import android.filterpacks.videoproc;\n@import android.filterpacks.videosrc;\n\n@filter GLTextureSource foreground {\n  texId = 0;\n  width = 0;\n  height = 0;\n  repeatFrame = true;\n}\n\n@filter MediaSource background {\n  sourceUrl = \"no_file_specified\";\n  waitForNewFrame = false;\n  sourceIsUrl = true;\n}\n\n@filter BackDropperFilter replacer {\n  autowbToggle = 1;\n}\n\n@filter GLTextureTarget output {\n  texId = 0;\n}\n\n@connect foreground[frame]  => replacer[video];\n@connect background[video]  => replacer[background];\n@connect replacer[video]    => output[frame];\n", "foreground", "output", android/filterfw/core/OneShotScheduler);
        mEffectListener = null;
        mLearningListener = new android.filterpacks.videoproc.BackDropperFilter.LearningDoneListener() {

            public void onLearningDone(BackDropperFilter backdropperfilter) {
                if(mEffectListener != null)
                    mEffectListener.onEffectUpdated(BackDropperEffect.this, null);
            }

            final BackDropperEffect this$0;

             {
                this$0 = BackDropperEffect.this;
                super();
            }
        };
        super.mGraph.getFilter("replacer").setInputValue("learningDoneListener", mLearningListener);
    }

    public void setParameter(String s, Object obj) {
        if(s.equals("source"))
            super.mGraph.getFilter("background").setInputValue("sourceUrl", obj);
    }

    public void setUpdateListener(EffectUpdateListener effectupdatelistener) {
        mEffectListener = effectupdatelistener;
    }

    private static final String mGraphDefinition = "@import android.filterpacks.base;\n@import android.filterpacks.videoproc;\n@import android.filterpacks.videosrc;\n\n@filter GLTextureSource foreground {\n  texId = 0;\n  width = 0;\n  height = 0;\n  repeatFrame = true;\n}\n\n@filter MediaSource background {\n  sourceUrl = \"no_file_specified\";\n  waitForNewFrame = false;\n  sourceIsUrl = true;\n}\n\n@filter BackDropperFilter replacer {\n  autowbToggle = 1;\n}\n\n@filter GLTextureTarget output {\n  texId = 0;\n}\n\n@connect foreground[frame]  => replacer[video];\n@connect background[video]  => replacer[background];\n@connect replacer[video]    => output[frame];\n";
    private EffectUpdateListener mEffectListener;
    private android.filterpacks.videoproc.BackDropperFilter.LearningDoneListener mLearningListener;

}
