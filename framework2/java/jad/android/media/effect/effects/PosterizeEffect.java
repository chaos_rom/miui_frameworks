// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.media.effect.effects;

import android.filterpacks.imageproc.PosterizeFilter;
import android.media.effect.EffectContext;
import android.media.effect.SingleFilterEffect;

public class PosterizeEffect extends SingleFilterEffect {

    public PosterizeEffect(EffectContext effectcontext, String s) {
        super(effectcontext, s, android/filterpacks/imageproc/PosterizeFilter, "image", "image", new Object[0]);
    }
}
