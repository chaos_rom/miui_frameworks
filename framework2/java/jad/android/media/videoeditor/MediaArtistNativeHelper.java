// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.media.videoeditor;

import android.graphics.*;
import android.util.Log;
import android.util.Pair;
import android.view.Surface;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.IntBuffer;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Semaphore;

// Referenced classes of package android.media.videoeditor:
//            MediaItem, Transition, MediaVideoItem, MediaImageItem, 
//            VideoEditor, MediaProperties, EffectColor, Effect, 
//            EffectKenBurns, ExtractAudioWaveformProgressListener, AudioTrack, Overlay, 
//            OverlayFrame, VideoEditorProfile

class MediaArtistNativeHelper {
    static interface NativeGetPixelsListCallback {

        public abstract void onThumbnail(int i);
    }

    public static class Properties {

        public String Id;
        public int audioBitrate;
        public int audioChannels;
        public int audioDuration;
        public int audioFormat;
        public int audioSamplingFrequency;
        public int audioVolumeValue;
        public float averageFrameRate;
        public int duration;
        public int fileType;
        public int height;
        public int level;
        public boolean levelSupported;
        public int profile;
        public boolean profileSupported;
        public int videoBitrate;
        public int videoDuration;
        public int videoFormat;
        public int videoRotation;
        public int width;

        public Properties() {
        }
    }

    public static class EditSettings {

        public int audioBitrate;
        public int audioChannels;
        public int audioFormat;
        public int audioSamplingFreq;
        public BackgroundMusicSettings backgroundMusicSettings;
        public ClipSettings clipSettingsArray[];
        public EffectSettings effectSettingsArray[];
        public int maxFileSize;
        public String outputFile;
        public int primaryTrackVolume;
        public TransitionSettings transitionSettingsArray[];
        public int videoBitrate;
        public int videoFormat;
        public int videoFrameRate;
        public int videoFrameSize;
        public int videoLevel;
        public int videoProfile;

        public EditSettings() {
        }
    }

    public static class PreviewClipProperties {

        public Properties clipProperties[];

        public PreviewClipProperties() {
        }
    }

    public static class PreviewSettings {

        public EffectSettings effectSettingsArray[];
        public PreviewClips previewClipsArray[];

        public PreviewSettings() {
        }
    }

    public static class AudioSettings {

        int ExtendedFs;
        int Fs;
        String Id;
        boolean bInDucking_enable;
        boolean bRemoveOriginal;
        long beginCutTime;
        int channels;
        int ducking_lowVolume;
        int ducking_threshold;
        long endCutTime;
        int fileType;
        boolean loop;
        String pFile;
        String pcmFilePath;
        long startMs;
        int volume;

        public AudioSettings() {
        }
    }

    public static class PreviewClips {

        public long beginPlayTime;
        public String clipPath;
        public long endPlayTime;
        public int fileType;
        public int mediaRendering;

        public PreviewClips() {
        }
    }

    public static class EffectSettings {

        public int alphaBlendingEndPercent;
        public int alphaBlendingFadeInTimePercent;
        public int alphaBlendingFadeOutTimePercent;
        public int alphaBlendingMiddlePercent;
        public int alphaBlendingStartPercent;
        public int audioEffectType;
        public int bitmapType;
        public int duration;
        public int durationPercent;
        public int fiftiesFrameRate;
        public int framingBuffer[];
        public String framingFile;
        public boolean framingResize;
        public int framingScaledSize;
        public int height;
        public int rgb16InputColor;
        public int startPercent;
        public int startTime;
        public String text;
        public int textBufferHeight;
        public int textBufferWidth;
        public String textRenderingData;
        public int topLeftX;
        public int topLeftY;
        public int videoEffectType;
        public int width;

        public EffectSettings() {
        }
    }

    public static class AudioEffect {

        public static final int FADE_IN = 8;
        public static final int FADE_OUT = 16;
        public static final int NONE;

        public AudioEffect() {
        }
    }

    public static class BackgroundMusicSettings {

        public long beginLoop;
        public int duckingThreshold;
        public boolean enableDucking;
        public long endLoop;
        public String file;
        public int fileType;
        public long insertionTime;
        public boolean isLooping;
        public int lowVolume;
        public int volumePercent;

        public BackgroundMusicSettings() {
        }
    }

    public static final class TransitionBehaviour {

        public static final int FAST_MIDDLE = 4;
        public static final int LINEAR = 1;
        public static final int SLOW_MIDDLE = 3;
        public static final int SPEED_DOWN = 2;
        public static final int SPEED_UP;

        public TransitionBehaviour() {
        }
    }

    public static final class AudioTransition {

        public static final int CROSS_FADE = 1;
        public static final int NONE;

        public AudioTransition() {
        }
    }

    public static class TransitionSettings {

        public AlphaMagicSettings alphaSettings;
        public int audioTransitionType;
        public int duration;
        public SlideTransitionSettings slideSettings;
        public int transitionBehaviour;
        public int videoTransitionType;

        public TransitionSettings() {
        }
    }

    public static class ClipSettings {

        public int beginCutPercent;
        public int beginCutTime;
        public String clipDecodedPath;
        public String clipOriginalPath;
        public String clipPath;
        public int endCutPercent;
        public int endCutTime;
        public int fileType;
        public int mediaRendering;
        public boolean panZoomEnabled;
        public int panZoomPercentEnd;
        public int panZoomPercentStart;
        public int panZoomTopLeftXEnd;
        public int panZoomTopLeftXStart;
        public int panZoomTopLeftYEnd;
        public int panZoomTopLeftYStart;
        public int rgbHeight;
        public int rgbWidth;
        public int rotationDegree;

        public ClipSettings() {
        }
    }

    public static class SlideTransitionSettings {

        public int direction;

        public SlideTransitionSettings() {
        }
    }

    public static final class SlideDirection {

        public static final int BOTTOM_OUT_TOP_IN = 3;
        public static final int LEFT_OUT_RIGTH_IN = 1;
        public static final int RIGHT_OUT_LEFT_IN = 0;
        public static final int TOP_OUT_BOTTOM_IN = 2;

        public SlideDirection() {
        }
    }

    public static class AlphaMagicSettings {

        public int blendingPercent;
        public String file;
        public boolean invertRotation;
        public int rgbHeight;
        public int rgbWidth;

        public AlphaMagicSettings() {
        }
    }

    public static class VideoTransition {

        public static final int ALPHA_MAGIC = 257;
        public static final int CROSS_FADE = 1;
        public static final int EXTERNAL = 256;
        public static final int FADE_BLACK = 259;
        public static final int NONE = 0;
        public static final int SLIDE_TRANSITION = 258;

        public VideoTransition() {
        }
    }

    public static class VideoEffect {

        public static final int BLACK_AND_WHITE = 257;
        public static final int COLORRGB16 = 267;
        public static final int EXTERNAL = 256;
        public static final int FADE_FROM_BLACK = 8;
        public static final int FADE_TO_BLACK = 16;
        public static final int FIFTIES = 266;
        public static final int FRAMING = 262;
        public static final int GRADIENT = 268;
        public static final int GREEN = 259;
        public static final int NEGATIVE = 261;
        public static final int NONE = 0;
        public static final int PINK = 258;
        public static final int SEPIA = 260;
        public static final int TEXT = 263;
        public static final int ZOOM_IN = 264;
        public static final int ZOOM_OUT = 265;

        public VideoEffect() {
        }
    }

    public final class VideoFrameRate {

        public static final int FR_10_FPS = 2;
        public static final int FR_12_5_FPS = 3;
        public static final int FR_15_FPS = 4;
        public static final int FR_20_FPS = 5;
        public static final int FR_25_FPS = 6;
        public static final int FR_30_FPS = 7;
        public static final int FR_5_FPS = 0;
        public static final int FR_7_5_FPS = 1;
        final MediaArtistNativeHelper this$0;

        public VideoFrameRate() {
            this$0 = MediaArtistNativeHelper.this;
            super();
        }
    }

    public final class VideoFrameSize {

        public static final int CIF = 4;
        public static final int NTSC = 7;
        public static final int QCIF = 2;
        public static final int QQVGA = 1;
        public static final int QVGA = 3;
        public static final int S720p = 12;
        public static final int SIZE_UNDEFINED = -1;
        public static final int SQCIF = 0;
        public static final int V1080p = 13;
        public static final int V720p = 10;
        public static final int VGA = 5;
        public static final int W720p = 11;
        public static final int WVGA = 6;
        public static final int WVGA16x9 = 9;
        public static final int nHD = 8;
        final MediaArtistNativeHelper this$0;

        public VideoFrameSize() {
            this$0 = MediaArtistNativeHelper.this;
            super();
        }
    }

    public final class VideoFormat {

        public static final int H263 = 1;
        public static final int H264 = 2;
        public static final int MPEG4 = 3;
        public static final int NO_VIDEO = 0;
        public static final int NULL_VIDEO = 254;
        public static final int UNSUPPORTED = 255;
        final MediaArtistNativeHelper this$0;

        public VideoFormat() {
            this$0 = MediaArtistNativeHelper.this;
            super();
        }
    }

    public final class Result {

        public static final int ERR_ADDCTS_HIGHER_THAN_VIDEO_DURATION = 40;
        public static final int ERR_ADDVOLUME_EQUALS_ZERO = 39;
        public static final int ERR_ALLOC = 62;
        public static final int ERR_AMR_EDITING_UNSUPPORTED = 19;
        public static final int ERR_ANALYSIS_DATA_SIZE_TOO_SMALL = 15;
        public static final int ERR_AUDIOBITRATE_TOO_HIGH = 121;
        public static final int ERR_AUDIOBITRATE_TOO_LOW = 119;
        public static final int ERR_AUDIO_CANNOT_BE_MIXED = 47;
        public static final int ERR_AUDIO_CONVERSION_FAILED = 114;
        public static final int ERR_AUDIO_MIXING_MP3_UNSUPPORTED = 44;
        public static final int ERR_AUDIO_MIXING_UNSUPPORTED = 43;
        public static final int ERR_BAD_CONTEXT = 63;
        public static final int ERR_BAD_OPTION_ID = 66;
        public static final int ERR_BAD_STREAM_ID = 65;
        public static final int ERR_BEGIN_CUT_EQUALS_END_CUT = 115;
        public static final int ERR_BEGIN_CUT_LARGER_THAN_DURATION = 12;
        public static final int ERR_BEGIN_CUT_LARGER_THAN_END_CUT = 13;
        public static final int ERR_BUFFER_OUT_TOO_SMALL = 2;
        public static final int ERR_CLOCK_BAD_REF_YEAR = 56;
        public static final int ERR_CONTEXT_FAILED = 64;
        public static final int ERR_DECODER_H263_NOT_BASELINE = 135;
        public static final int ERR_DECODER_H263_PROFILE_NOT_SUPPORTED = 134;
        public static final int ERR_DIR_NO_MORE_ENTRY = 59;
        public static final int ERR_DIR_OPEN_FAILED = 57;
        public static final int ERR_DIR_READ_FAILED = 58;
        public static final int ERR_DURATION_IS_NULL = 111;
        public static final int ERR_EDITING_NO_SUPPORTED_STREAM_IN_FILE = 29;
        public static final int ERR_EDITING_NO_SUPPORTED_VIDEO_STREAM_IN_FILE = 30;
        public static final int ERR_EDITING_UNSUPPORTED_AUDIO_FORMAT = 28;
        public static final int ERR_EDITING_UNSUPPORTED_H263_PROFILE = 25;
        public static final int ERR_EDITING_UNSUPPORTED_MPEG4_PROFILE = 26;
        public static final int ERR_EDITING_UNSUPPORTED_MPEG4_RVLC = 27;
        public static final int ERR_EDITING_UNSUPPORTED_VIDEO_FORMAT = 24;
        public static final int ERR_ENCODER_ACCES_UNIT_ERROR = 23;
        public static final int ERR_END_CUT_SMALLER_THAN_BEGIN_CUT = 116;
        public static final int ERR_EXTERNAL_EFFECT_NULL = 10;
        public static final int ERR_EXTERNAL_TRANSITION_NULL = 11;
        public static final int ERR_FEATURE_UNSUPPORTED_WITH_AAC = 46;
        public static final int ERR_FEATURE_UNSUPPORTED_WITH_AUDIO_TRACK = 45;
        public static final int ERR_FEATURE_UNSUPPORTED_WITH_EVRC = 49;
        public static final int ERR_FILE_BAD_MODE_ACCESS = 80;
        public static final int ERR_FILE_INVALID_POSITION = 81;
        public static final int ERR_FILE_LOCKED = 79;
        public static final int ERR_FILE_NOT_FOUND = 1;
        public static final int ERR_H263_FORBIDDEN_IN_MP4_FILE = 112;
        public static final int ERR_H263_PROFILE_NOT_SUPPORTED = 51;
        public static final int ERR_INCOMPATIBLE_VIDEO_DATA_PARTITIONING = 36;
        public static final int ERR_INCOMPATIBLE_VIDEO_FORMAT = 33;
        public static final int ERR_INCOMPATIBLE_VIDEO_FRAME_SIZE = 34;
        public static final int ERR_INCOMPATIBLE_VIDEO_TIME_SCALE = 35;
        public static final int ERR_INPUT_AUDIO_AU_TOO_LARGE = 21;
        public static final int ERR_INPUT_AUDIO_CORRUPTED_AU = 22;
        public static final int ERR_INPUT_FILE_CONTAINS_NO_SUPPORTED_STREAM = 103;
        public static final int ERR_INPUT_VIDEO_AU_TOO_LARGE = 20;
        public static final int ERR_INTERNAL = 255;
        public static final int ERR_INVALID_3GPP_FILE = 16;
        public static final int ERR_INVALID_AAC_SAMPLING_FREQUENCY = 113;
        public static final int ERR_INVALID_AUDIO_EFFECT_TYPE = 6;
        public static final int ERR_INVALID_AUDIO_TRANSITION_TYPE = 8;
        public static final int ERR_INVALID_CLIP_ANALYSIS_PLATFORM = 32;
        public static final int ERR_INVALID_CLIP_ANALYSIS_VERSION = 31;
        public static final int ERR_INVALID_EFFECT_KIND = 4;
        public static final int ERR_INVALID_FILE_TYPE = 3;
        public static final int ERR_INVALID_INPUT_FILE = 104;
        public static final int ERR_INVALID_VIDEO_EFFECT_TYPE = 5;
        public static final int ERR_INVALID_VIDEO_ENCODING_FRAME_RATE = 9;
        public static final int ERR_INVALID_VIDEO_FRAME_RATE_FOR_H263 = 110;
        public static final int ERR_INVALID_VIDEO_FRAME_SIZE_FOR_H263 = 109;
        public static final int ERR_INVALID_VIDEO_TRANSITION_TYPE = 7;
        public static final int ERR_MAXFILESIZE_TOO_SMALL = 117;
        public static final int ERR_NOMORE_SPACE_FOR_FILE = 136;
        public static final int ERR_NOT_IMPLEMENTED = 69;
        public static final int ERR_NO_SUPPORTED_STREAM_IN_FILE = 38;
        public static final int ERR_NO_SUPPORTED_VIDEO_STREAM_IN_FILE = 52;
        public static final int ERR_ONLY_AMRNB_INPUT_CAN_BE_MIXED = 48;
        public static final int ERR_OUTPUT_FILE_SIZE_TOO_SMALL = 122;
        public static final int ERR_OVERLAPPING_TRANSITIONS = 14;
        public static final int ERR_PARAMETER = 60;
        public static final int ERR_READER_UNKNOWN_STREAM_TYPE = 123;
        public static final int ERR_READ_ONLY = 68;
        public static final int ERR_STATE = 61;
        public static final int ERR_STR_BAD_ARGS = 97;
        public static final int ERR_STR_BAD_STRING = 94;
        public static final int ERR_STR_CONV_FAILED = 95;
        public static final int ERR_STR_OVERFLOW = 96;
        public static final int ERR_THREAD_NOT_STARTED = 100;
        public static final int ERR_UNDEFINED_AUDIO_TRACK_FILE_FORMAT = 41;
        public static final int ERR_UNDEFINED_OUTPUT_AUDIO_FORMAT = 108;
        public static final int ERR_UNDEFINED_OUTPUT_VIDEO_FORMAT = 105;
        public static final int ERR_UNDEFINED_OUTPUT_VIDEO_FRAME_RATE = 107;
        public static final int ERR_UNDEFINED_OUTPUT_VIDEO_FRAME_SIZE = 106;
        public static final int ERR_UNSUPPORTED_ADDED_AUDIO_STREAM = 42;
        public static final int ERR_UNSUPPORTED_INPUT_AUDIO_FORMAT = 18;
        public static final int ERR_UNSUPPORTED_INPUT_VIDEO_FORMAT = 17;
        public static final int ERR_UNSUPPORTED_MEDIA_TYPE = 70;
        public static final int ERR_UNSUPPORTED_MP3_ASSEMBLY = 37;
        public static final int ERR_VIDEOBITRATE_TOO_HIGH = 120;
        public static final int ERR_VIDEOBITRATE_TOO_LOW = 118;
        public static final int ERR_WRITE_ONLY = 67;
        public static final int NO_ERROR = 0;
        public static final int WAR_BUFFER_FULL = 76;
        public static final int WAR_DEBLOCKING_FILTER_NOT_IMPLEMENTED = 133;
        public static final int WAR_INVALID_TIME = 73;
        public static final int WAR_MAX_OUTPUT_SIZE_EXCEEDED = 54;
        public static final int WAR_MEDIATYPE_NOT_SUPPORTED = 102;
        public static final int WAR_NO_DATA_YET = 71;
        public static final int WAR_NO_MORE_AU = 74;
        public static final int WAR_NO_MORE_STREAM = 72;
        public static final int WAR_READER_INFORMATION_NOT_PRESENT = 125;
        public static final int WAR_READER_NO_METADATA = 124;
        public static final int WAR_REDIRECT = 77;
        public static final int WAR_STR_NOT_FOUND = 99;
        public static final int WAR_STR_OVERFLOW = 98;
        public static final int WAR_TIMESCALE_TOO_BIG = 55;
        public static final int WAR_TIME_OUT = 75;
        public static final int WAR_TOO_MUCH_STREAMS = 78;
        public static final int WAR_TRANSCODING_DONE = 101;
        public static final int WAR_TRANSCODING_NECESSARY = 53;
        public static final int WAR_VIDEORENDERER_NO_NEW_FRAME = 132;
        public static final int WAR_WRITER_STOP_REQ = 131;
        final MediaArtistNativeHelper this$0;

        public Result() {
            this$0 = MediaArtistNativeHelper.this;
            super();
        }
    }

    public final class MediaRendering {

        public static final int BLACK_BORDERS = 2;
        public static final int CROPPING = 1;
        public static final int RESIZING;
        final MediaArtistNativeHelper this$0;

        public MediaRendering() {
            this$0 = MediaArtistNativeHelper.this;
            super();
        }
    }

    public final class FileType {

        public static final int AMR = 2;
        public static final int GIF = 7;
        public static final int JPG = 5;
        public static final int M4V = 10;
        public static final int MP3 = 3;
        public static final int MP4 = 1;
        public static final int PCM = 4;
        public static final int PNG = 8;
        public static final int THREE_GPP = 0;
        public static final int UNSUPPORTED = 255;
        final MediaArtistNativeHelper this$0;

        public FileType() {
            this$0 = MediaArtistNativeHelper.this;
            super();
        }
    }

    public final class Bitrate {

        public static final int BR_128_KBPS = 0x1f400;
        public static final int BR_12_2_KBPS = 12200;
        public static final int BR_16_KBPS = 16000;
        public static final int BR_192_KBPS = 0x2ee00;
        public static final int BR_24_KBPS = 24000;
        public static final int BR_256_KBPS = 0x3e800;
        public static final int BR_288_KBPS = 0x46500;
        public static final int BR_2_MBPS = 0x1e8480;
        public static final int BR_32_KBPS = 32000;
        public static final int BR_384_KBPS = 0x5dc00;
        public static final int BR_48_KBPS = 48000;
        public static final int BR_512_KBPS = 0x7d000;
        public static final int BR_5_MBPS = 0x4c4b40;
        public static final int BR_64_KBPS = 64000;
        public static final int BR_800_KBPS = 0xc3500;
        public static final int BR_8_MBPS = 0x7a1200;
        public static final int BR_96_KBPS = 0x17700;
        public static final int BR_9_2_KBPS = 9200;
        public static final int UNDEFINED = 0;
        public static final int VARIABLE = -1;
        final MediaArtistNativeHelper this$0;

        public Bitrate() {
            this$0 = MediaArtistNativeHelper.this;
            super();
        }
    }

    public final class AudioSamplingFrequency {

        public static final int FREQ_11025 = 11025;
        public static final int FREQ_12000 = 12000;
        public static final int FREQ_16000 = 16000;
        public static final int FREQ_22050 = 22050;
        public static final int FREQ_24000 = 24000;
        public static final int FREQ_32000 = 32000;
        public static final int FREQ_44100 = 44100;
        public static final int FREQ_48000 = 48000;
        public static final int FREQ_8000 = 8000;
        public static final int FREQ_DEFAULT;
        final MediaArtistNativeHelper this$0;

        public AudioSamplingFrequency() {
            this$0 = MediaArtistNativeHelper.this;
            super();
        }
    }

    public final class AudioFormat {

        public static final int AAC = 2;
        public static final int AAC_PLUS = 3;
        public static final int AMR_NB = 1;
        public static final int ENHANCED_AAC_PLUS = 4;
        public static final int EVRC = 6;
        public static final int MP3 = 5;
        public static final int NO_AUDIO = 0;
        public static final int NULL_AUDIO = 254;
        public static final int PCM = 7;
        public static final int UNSUPPORTED_AUDIO = 255;
        final MediaArtistNativeHelper this$0;

        public AudioFormat() {
            this$0 = MediaArtistNativeHelper.this;
            super();
        }
    }

    public final class Version {

        public Version getVersion() {
            Version version = new Version();
            version.major = 0;
            version.minor = 0;
            version.revision = 1;
            return version;
        }

        private static final int VIDEOEDITOR_MAJOR_VERSION = 0;
        private static final int VIDEOEDITOR_MINOR_VERSION = 0;
        private static final int VIDEOEDITOR_REVISION_VERSION = 1;
        public int major;
        public int minor;
        public int revision;
        final MediaArtistNativeHelper this$0;

        public Version() {
            this$0 = MediaArtistNativeHelper.this;
            super();
        }
    }

    public static interface OnProgressUpdateListener {

        public abstract void OnProgressUpdate(int i, int j);
    }


    public MediaArtistNativeHelper(String s, Semaphore semaphore, VideoEditor videoeditor) {
        mClipProperties = null;
        mAudioSettings = null;
        mAudioTrack = null;
        mInvalidatePreviewArray = true;
        mRegenerateAudio = true;
        mExportFilename = null;
        mExportVideoCodec = 0;
        mExportAudioCodec = 0;
        mTotalClips = 0;
        mErrorFlagSet = false;
        mProjectPath = s;
        if(videoeditor != null) {
            mVideoEditor = videoeditor;
            if(mStoryBoardSettings == null)
                mStoryBoardSettings = new EditSettings();
            mLock = semaphore;
            _init(mProjectPath, "null");
            mAudioTrackPCMFilePath = null;
            return;
        } else {
            mVideoEditor = null;
            throw new IllegalArgumentException("video editor object is null");
        }
    }

    private native void _init(String s, String s1) throws IllegalArgumentException, IllegalStateException, RuntimeException;

    private void adjustMediaItemBoundary(ClipSettings clipsettings, Properties properties, MediaItem mediaitem) {
        if(mediaitem.getBeginTransition() == null || mediaitem.getBeginTransition().getDuration() <= 0L || mediaitem.getEndTransition() == null || mediaitem.getEndTransition().getDuration() <= 0L) goto _L2; else goto _L1
_L1:
        clipsettings.beginCutTime = (int)((long)clipsettings.beginCutTime + mediaitem.getBeginTransition().getDuration());
        clipsettings.endCutTime = (int)((long)clipsettings.endCutTime - mediaitem.getEndTransition().getDuration());
_L4:
        properties.duration = clipsettings.endCutTime - clipsettings.beginCutTime;
        if(properties.videoDuration != 0)
            properties.videoDuration = clipsettings.endCutTime - clipsettings.beginCutTime;
        if(properties.audioDuration != 0)
            properties.audioDuration = clipsettings.endCutTime - clipsettings.beginCutTime;
        return;
_L2:
        if(mediaitem.getBeginTransition() == null && mediaitem.getEndTransition() != null && mediaitem.getEndTransition().getDuration() > 0L)
            clipsettings.endCutTime = (int)((long)clipsettings.endCutTime - mediaitem.getEndTransition().getDuration());
        else
        if(mediaitem.getEndTransition() == null && mediaitem.getBeginTransition() != null && mediaitem.getBeginTransition().getDuration() > 0L)
            clipsettings.beginCutTime = (int)((long)clipsettings.beginCutTime + mediaitem.getBeginTransition().getDuration());
        if(true) goto _L4; else goto _L3
_L3:
    }

    private void adjustVolume(MediaItem mediaitem, PreviewClipProperties previewclipproperties, int i) {
        if(!(mediaitem instanceof MediaVideoItem)) goto _L2; else goto _L1
_L1:
        if(!((MediaVideoItem)mediaitem).isMuted())
            mClipProperties.clipProperties[i].audioVolumeValue = ((MediaVideoItem)mediaitem).getVolume();
        else
            mClipProperties.clipProperties[i].audioVolumeValue = 0;
_L4:
        return;
_L2:
        if(mediaitem instanceof MediaImageItem)
            mClipProperties.clipProperties[i].audioVolumeValue = 0;
        if(true) goto _L4; else goto _L3
_L3:
    }

    private void checkOddSizeImage(MediaItem mediaitem, PreviewClipProperties previewclipproperties, int i) {
        if(mediaitem instanceof MediaImageItem) {
            int j = mClipProperties.clipProperties[i].width;
            int k = mClipProperties.clipProperties[i].height;
            if(j % 2 != 0)
                j--;
            if(k % 2 != 0)
                k--;
            mClipProperties.clipProperties[i].width = j;
            mClipProperties.clipProperties[i].height = k;
        }
    }

    private int findVideoBitrate(int i) {
        i;
        JVM INSTR tableswitch 0 12: default 68
    //                   0 74
    //                   1 74
    //                   2 74
    //                   3 81
    //                   4 81
    //                   5 88
    //                   6 88
    //                   7 88
    //                   8 88
    //                   9 88
    //                   10 95
    //                   11 95
    //                   12 95;
           goto _L1 _L2 _L2 _L2 _L3 _L3 _L4 _L4 _L4 _L4 _L4 _L5 _L5 _L5
_L1:
        int j = 0x7a1200;
_L7:
        return j;
_L2:
        j = 0x1f400;
        continue; /* Loop/switch isn't completed */
_L3:
        j = 0x5dc00;
        continue; /* Loop/switch isn't completed */
_L4:
        j = 0x1e8480;
        continue; /* Loop/switch isn't completed */
_L5:
        j = 0x4c4b40;
        if(true) goto _L7; else goto _L6
_L6:
    }

    private int findVideoResolution(int i, int j) {
        int k = -1;
        i;
        JVM INSTR tableswitch 1 5: default 40
    //                   1 98
    //                   2 124
    //                   3 163
    //                   4 188
    //                   5 201;
           goto _L1 _L2 _L3 _L4 _L5 _L6
_L1:
        if(k == -1) {
            Pair apair[] = MediaProperties.getSupportedResolutions(mVideoEditor.getAspectRatio());
            Pair pair = apair[-1 + apair.length];
            k = findVideoResolution(mVideoEditor.getAspectRatio(), ((Integer)pair.second).intValue());
        }
        return k;
_L2:
        if(j == 480)
            k = 7;
        else
        if(j == 720)
            k = 11;
        continue; /* Loop/switch isn't completed */
_L3:
        if(j == 480)
            k = 9;
        else
        if(j == 720)
            k = 10;
        else
        if(j == 1080)
            k = 13;
        continue; /* Loop/switch isn't completed */
_L4:
        if(j == 480)
            k = 5;
        else
        if(j == 720)
            k = 12;
        continue; /* Loop/switch isn't completed */
_L5:
        if(j == 480)
            k = 6;
        continue; /* Loop/switch isn't completed */
_L6:
        if(j == 144)
            k = 2;
        else
        if(j == 288)
            k = 4;
        if(true) goto _L1; else goto _L7
_L7:
    }

    private void generateTransition(Transition transition, EditSettings editsettings, PreviewClipProperties previewclipproperties, int i) {
        if(!transition.isGenerated())
            transition.generate();
        editsettings.clipSettingsArray[i] = new ClipSettings();
        editsettings.clipSettingsArray[i].clipPath = transition.getFilename();
        editsettings.clipSettingsArray[i].fileType = 0;
        editsettings.clipSettingsArray[i].beginCutTime = 0;
        editsettings.clipSettingsArray[i].endCutTime = (int)transition.getDuration();
        editsettings.clipSettingsArray[i].mediaRendering = 2;
        try {
            previewclipproperties.clipProperties[i] = getMediaProperties(transition.getFilename());
        }
        catch(Exception exception) {
            throw new IllegalArgumentException("Unsupported file or file not found");
        }
        previewclipproperties.clipProperties[i].Id = null;
        previewclipproperties.clipProperties[i].audioVolumeValue = 100;
        previewclipproperties.clipProperties[i].duration = (int)transition.getDuration();
        if(previewclipproperties.clipProperties[i].videoDuration != 0)
            previewclipproperties.clipProperties[i].videoDuration = (int)transition.getDuration();
        if(previewclipproperties.clipProperties[i].audioDuration != 0)
            previewclipproperties.clipProperties[i].audioDuration = (int)transition.getDuration();
    }

    private int getEffectColorType(EffectColor effectcolor) {
        effectcolor.getType();
        JVM INSTR tableswitch 1 5: default 40
    //                   1 45
    //                   2 103
    //                   3 110
    //                   4 117
    //                   5 124;
           goto _L1 _L2 _L3 _L4 _L5 _L6
_L1:
        char c = '\uFFFF';
_L8:
        return c;
_L2:
        if(effectcolor.getColor() == 65280)
            c = '\u0103';
        else
        if(effectcolor.getColor() == 0xff66cc)
            c = '\u0102';
        else
        if(effectcolor.getColor() == 0x7f7f7f)
            c = '\u0101';
        else
            c = '\u010B';
        continue; /* Loop/switch isn't completed */
_L3:
        c = '\u010C';
        continue; /* Loop/switch isn't completed */
_L4:
        c = '\u0104';
        continue; /* Loop/switch isn't completed */
_L5:
        c = '\u0105';
        continue; /* Loop/switch isn't completed */
_L6:
        c = '\u010A';
        if(true) goto _L8; else goto _L7
_L7:
    }

    private int getTotalEffects(List list) {
        int i = 0;
        for(Iterator iterator = list.iterator(); iterator.hasNext();) {
            MediaItem mediaitem = (MediaItem)iterator.next();
            i = i + mediaitem.getAllEffects().size() + mediaitem.getAllOverlays().size();
            Iterator iterator1 = mediaitem.getAllEffects().iterator();
            while(iterator1.hasNext()) 
                if((Effect)iterator1.next() instanceof EffectKenBurns)
                    i--;
        }

        return i;
    }

    private int getTransitionResolution(MediaItem mediaitem, MediaItem mediaitem1) {
        int i;
        int j;
        int k;
        i = 0;
        j = 0;
        k = 0;
        if(mediaitem == null || mediaitem1 == null) goto _L2; else goto _L1
_L1:
        if(mediaitem instanceof MediaVideoItem)
            i = mediaitem.getHeight();
        else
        if(mediaitem instanceof MediaImageItem)
            i = ((MediaImageItem)mediaitem).getScaledHeight();
        if(mediaitem1 instanceof MediaVideoItem)
            j = mediaitem1.getHeight();
        else
        if(mediaitem1 instanceof MediaImageItem)
            j = ((MediaImageItem)mediaitem1).getScaledHeight();
        if(i > j)
            k = findVideoResolution(mVideoEditor.getAspectRatio(), i);
        else
            k = findVideoResolution(mVideoEditor.getAspectRatio(), j);
_L4:
        return k;
_L2:
        if(mediaitem == null && mediaitem1 != null) {
            if(mediaitem1 instanceof MediaVideoItem)
                j = mediaitem1.getHeight();
            else
            if(mediaitem1 instanceof MediaImageItem)
                j = ((MediaImageItem)mediaitem1).getScaledHeight();
            k = findVideoResolution(mVideoEditor.getAspectRatio(), j);
            continue; /* Loop/switch isn't completed */
        }
        if(mediaitem == null || mediaitem1 != null)
            continue; /* Loop/switch isn't completed */
        if(!(mediaitem instanceof MediaVideoItem))
            break; /* Loop/switch isn't completed */
        i = mediaitem.getHeight();
_L6:
        k = findVideoResolution(mVideoEditor.getAspectRatio(), i);
        if(true) goto _L4; else goto _L3
_L3:
        if(!(mediaitem instanceof MediaImageItem)) goto _L6; else goto _L5
_L5:
        i = ((MediaImageItem)mediaitem).getScaledHeight();
          goto _L6
    }

    private static native Version getVersion() throws RuntimeException;

    private void lock() throws InterruptedException {
        if(Log.isLoggable("MediaArtistNativeHelper", 3))
            Log.d("MediaArtistNativeHelper", "lock: grabbing semaphore", new Throwable());
        mLock.acquire();
        if(Log.isLoggable("MediaArtistNativeHelper", 3))
            Log.d("MediaArtistNativeHelper", "lock: grabbed semaphore");
    }

    private native void nativeClearSurface(Surface surface);

    private native int nativeGenerateAudioGraph(String s, String s1, int i, int j, int k);

    private native int nativeGenerateClip(EditSettings editsettings) throws IllegalArgumentException, IllegalStateException, RuntimeException;

    private native int nativeGenerateRawAudio(String s, String s1);

    private native int nativeGetPixels(String s, int ai[], int i, int j, long l);

    private native int nativeGetPixelsList(String s, int ai[], int i, int j, int k, long l, 
            long l1, int ai1[], NativeGetPixelsListCallback nativegetpixelslistcallback);

    private native void nativePopulateSettings(EditSettings editsettings, PreviewClipProperties previewclipproperties, AudioSettings audiosettings) throws IllegalArgumentException, IllegalStateException, RuntimeException;

    private native int nativeRenderMediaItemPreviewFrame(Surface surface, String s, int i, int j, int k, int l, long l1) throws IllegalArgumentException, IllegalStateException, RuntimeException;

    private native int nativeRenderPreviewFrame(Surface surface, long l, int i, int j) throws IllegalArgumentException, IllegalStateException, RuntimeException;

    private native void nativeStartPreview(Surface surface, long l, long l1, int i, boolean flag) throws IllegalArgumentException, IllegalStateException, RuntimeException;

    private native int nativeStopPreview();

    private void onAudioGraphExtractProgressUpdate(int i, boolean flag) {
        if(mExtractAudioWaveformProgressListener != null && i > 0)
            mExtractAudioWaveformProgressListener.onProgress(i);
    }

    private void onPreviewProgressUpdate(int i, boolean flag, boolean flag1, String s, int j, int k) {
        if(mPreviewProgressListener != null) {
            if(mIsFirstProgress) {
                mPreviewProgressListener.onStart(mVideoEditor);
                mIsFirstProgress = false;
            }
            VideoEditor.OverlayData overlaydata;
            if(flag1) {
                overlaydata = new VideoEditor.OverlayData();
                if(s != null)
                    overlaydata.set(BitmapFactory.decodeFile(s), j);
                else
                    overlaydata.setClear();
            } else {
                overlaydata = null;
            }
            if(i != 0)
                mPreviewProgress = i;
            if(flag)
                mPreviewProgressListener.onStop(mVideoEditor);
            else
            if(k != 0)
                mPreviewProgressListener.onError(mVideoEditor, k);
            else
                mPreviewProgressListener.onProgress(mVideoEditor, i, overlaydata);
        }
    }

    private void onProgressUpdate(int i, int j) {
        if(mProcessingState != 20) goto _L2; else goto _L1
_L1:
        if(mExportProgressListener != null && mProgressToApp < j) {
            mExportProgressListener.onProgress(mVideoEditor, mOutputFilename, j);
            mProgressToApp = j;
        }
_L11:
        return;
_L2:
        int k;
        byte byte0;
        k = 0;
        if(mProcessingState == 1)
            byte0 = 2;
        else
            byte0 = 1;
        mProcessingState;
        JVM INSTR tableswitch 1 13: default 132
    //                   1 170
    //                   2 250
    //                   3 255
    //                   4 132
    //                   5 132
    //                   6 132
    //                   7 132
    //                   8 132
    //                   9 132
    //                   10 132
    //                   11 260
    //                   12 294
    //                   13 315;
           goto _L3 _L4 _L5 _L6 _L3 _L3 _L3 _L3 _L3 _L3 _L3 _L7 _L8 _L9
_L9:
        break MISSING_BLOCK_LABEL_315;
_L5:
        break; /* Loop/switch isn't completed */
_L3:
        Log.e("MediaArtistNativeHelper", (new StringBuilder()).append("ERROR unexpected State=").append(mProcessingState).toString());
        continue; /* Loop/switch isn't completed */
_L4:
        k = j;
_L12:
        if(mProgressToApp != k && k != 0) {
            mProgressToApp = k;
            if(mMediaProcessingProgressListener != null)
                mMediaProcessingProgressListener.onProgress(mProcessingObject, byte0, k);
        }
        if(mProgressToApp == 0) {
            if(mMediaProcessingProgressListener != null)
                mMediaProcessingProgressListener.onProgress(mProcessingObject, byte0, k);
            mProgressToApp = 1;
        }
        if(true) goto _L11; else goto _L10
_L10:
        k = j;
          goto _L12
_L6:
        k = j;
          goto _L12
_L7:
        if(j == 0 && mProgressToApp != 0)
            mProgressToApp = 0;
        if(j != 0 || mProgressToApp != 0)
            k = j / 4;
          goto _L12
_L8:
        if(j != 0 || mProgressToApp != 0)
            k = 25 + j / 4;
          goto _L12
        if(j != 0 || mProgressToApp != 0)
            k = 50 + j / 2;
          goto _L12
    }

    private void populateBackgroundMusicProperties(List list) {
        if(list.size() == 1)
            mAudioTrack = (AudioTrack)list.get(0);
        else
            mAudioTrack = null;
        if(mAudioTrack != null) {
            mAudioSettings = new AudioSettings();
            new Properties();
            mAudioSettings.pFile = null;
            mAudioSettings.Id = mAudioTrack.getId();
            Properties properties;
            try {
                properties = getMediaProperties(mAudioTrack.getFilename());
            }
            catch(Exception exception) {
                throw new IllegalArgumentException("Unsupported file or file not found");
            }
            mAudioSettings.bRemoveOriginal = false;
            mAudioSettings.channels = properties.audioChannels;
            mAudioSettings.Fs = properties.audioSamplingFrequency;
            mAudioSettings.loop = mAudioTrack.isLooping();
            mAudioSettings.ExtendedFs = 0;
            mAudioSettings.pFile = mAudioTrack.getFilename();
            mAudioSettings.startMs = mAudioTrack.getStartTime();
            mAudioSettings.beginCutTime = mAudioTrack.getBoundaryBeginTime();
            mAudioSettings.endCutTime = mAudioTrack.getBoundaryEndTime();
            if(mAudioTrack.isMuted())
                mAudioSettings.volume = 0;
            else
                mAudioSettings.volume = mAudioTrack.getVolume();
            mAudioSettings.fileType = properties.fileType;
            mAudioSettings.ducking_lowVolume = mAudioTrack.getDuckedTrackVolume();
            mAudioSettings.ducking_threshold = mAudioTrack.getDuckingThreshhold();
            mAudioSettings.bInDucking_enable = mAudioTrack.isDuckingEnabled();
            mAudioTrackPCMFilePath = String.format((new StringBuilder()).append(mProjectPath).append("/").append("AudioPcm.pcm").toString(), new Object[0]);
            mAudioSettings.pcmFilePath = mAudioTrackPCMFilePath;
            mPreviewEditSettings.backgroundMusicSettings = new BackgroundMusicSettings();
            mPreviewEditSettings.backgroundMusicSettings.file = mAudioTrackPCMFilePath;
            mPreviewEditSettings.backgroundMusicSettings.fileType = properties.fileType;
            mPreviewEditSettings.backgroundMusicSettings.insertionTime = mAudioTrack.getStartTime();
            mPreviewEditSettings.backgroundMusicSettings.volumePercent = mAudioTrack.getVolume();
            mPreviewEditSettings.backgroundMusicSettings.beginLoop = mAudioTrack.getBoundaryBeginTime();
            mPreviewEditSettings.backgroundMusicSettings.endLoop = mAudioTrack.getBoundaryEndTime();
            mPreviewEditSettings.backgroundMusicSettings.enableDucking = mAudioTrack.isDuckingEnabled();
            mPreviewEditSettings.backgroundMusicSettings.duckingThreshold = mAudioTrack.getDuckingThreshhold();
            mPreviewEditSettings.backgroundMusicSettings.lowVolume = mAudioTrack.getDuckedTrackVolume();
            mPreviewEditSettings.backgroundMusicSettings.isLooping = mAudioTrack.isLooping();
            mPreviewEditSettings.primaryTrackVolume = 100;
            mProcessingState = 1;
            mProcessingObject = mAudioTrack;
        } else {
            mAudioSettings = null;
            mPreviewEditSettings.backgroundMusicSettings = null;
            mAudioTrackPCMFilePath = null;
        }
    }

    private int populateEffects(MediaItem mediaitem, EffectSettings aeffectsettings[], int i, int j, int k, int l) {
        if(mediaitem.getBeginTransition() == null || mediaitem.getBeginTransition().getDuration() <= 0L || mediaitem.getEndTransition() == null || mediaitem.getEndTransition().getDuration() <= 0L) goto _L2; else goto _L1
_L1:
        j = (int)((long)j + mediaitem.getBeginTransition().getDuration());
        k = (int)((long)k - mediaitem.getEndTransition().getDuration());
_L4:
        List list;
        list = mediaitem.getAllEffects();
        for(Iterator iterator = mediaitem.getAllOverlays().iterator(); iterator.hasNext();) {
            aeffectsettings[i] = getOverlaySettings((OverlayFrame)(Overlay)iterator.next());
            adjustEffectsStartTimeAndDuration(aeffectsettings[i], j, k);
            EffectSettings effectsettings1 = aeffectsettings[i];
            effectsettings1.startTime = l + effectsettings1.startTime;
            i++;
        }

        break; /* Loop/switch isn't completed */
_L2:
        if(mediaitem.getBeginTransition() == null && mediaitem.getEndTransition() != null && mediaitem.getEndTransition().getDuration() > 0L)
            k = (int)((long)k - mediaitem.getEndTransition().getDuration());
        else
        if(mediaitem.getEndTransition() == null && mediaitem.getBeginTransition() != null && mediaitem.getBeginTransition().getDuration() > 0L)
            j = (int)((long)j + mediaitem.getBeginTransition().getDuration());
        if(true) goto _L4; else goto _L3
_L3:
        Iterator iterator1 = list.iterator();
        do {
            if(!iterator1.hasNext())
                break;
            Effect effect = (Effect)iterator1.next();
            if(effect instanceof EffectColor) {
                aeffectsettings[i] = getEffectSettings((EffectColor)effect);
                adjustEffectsStartTimeAndDuration(aeffectsettings[i], j, k);
                EffectSettings effectsettings = aeffectsettings[i];
                effectsettings.startTime = l + effectsettings.startTime;
                i++;
            }
        } while(true);
        return i;
    }

    private int populateMediaItemProperties(MediaItem mediaitem, int i, int j) {
        mPreviewEditSettings.clipSettingsArray[i] = new ClipSettings();
        if(!(mediaitem instanceof MediaVideoItem)) goto _L2; else goto _L1
_L1:
        mPreviewEditSettings.clipSettingsArray[i] = ((MediaVideoItem)mediaitem).getVideoClipProperties();
        if(((MediaVideoItem)mediaitem).getHeight() > j)
            j = ((MediaVideoItem)mediaitem).getHeight();
_L4:
        if(mPreviewEditSettings.clipSettingsArray[i].fileType == 5) {
            mPreviewEditSettings.clipSettingsArray[i].clipDecodedPath = ((MediaImageItem)mediaitem).getDecodedImageFileName();
            mPreviewEditSettings.clipSettingsArray[i].clipOriginalPath = mPreviewEditSettings.clipSettingsArray[i].clipPath;
        }
        return j;
_L2:
        if(mediaitem instanceof MediaImageItem) {
            mPreviewEditSettings.clipSettingsArray[i] = ((MediaImageItem)mediaitem).getImageClipProperties();
            if(((MediaImageItem)mediaitem).getScaledHeight() > j)
                j = ((MediaImageItem)mediaitem).getScaledHeight();
        }
        if(true) goto _L4; else goto _L3
_L3:
    }

    private void previewFrameEditInfo(String s, int i) {
        mRenderPreviewOverlayFile = s;
        mRenderPreviewRenderingMode = i;
    }

    private native void release() throws IllegalStateException, RuntimeException;

    private native void stopEncoding() throws IllegalStateException, RuntimeException;

    private void unlock() {
        if(Log.isLoggable("MediaArtistNativeHelper", 3))
            Log.d("MediaArtistNativeHelper", "unlock: releasing semaphore");
        mLock.release();
    }

    int GetClosestVideoFrameRate(int i) {
        byte byte0 = 7;
        if(i < 25)
            if(i >= 20)
                byte0 = 6;
            else
            if(i >= 15)
                byte0 = 5;
            else
            if(i >= 12)
                byte0 = 4;
            else
            if(i >= 10)
                byte0 = 3;
            else
            if(i >= byte0)
                byte0 = 2;
            else
            if(i >= 5)
                byte0 = 1;
            else
                byte0 = -1;
        return byte0;
    }

    public void adjustEffectsStartTimeAndDuration(EffectSettings effectsettings, int i, int j) {
        if(effectsettings.startTime <= j && effectsettings.startTime + effectsettings.duration > i) goto _L2; else goto _L1
_L1:
        effectsettings.startTime = 0;
        effectsettings.duration = 0;
_L4:
        return;
_L2:
        if(effectsettings.startTime < i && effectsettings.startTime + effectsettings.duration > i && effectsettings.startTime + effectsettings.duration <= j) {
            int j1 = effectsettings.duration - (i - effectsettings.startTime);
            effectsettings.startTime = 0;
            effectsettings.duration = j1;
        } else
        if(effectsettings.startTime >= i && effectsettings.startTime + effectsettings.duration <= j) {
            effectsettings.startTime = effectsettings.startTime - i;
            effectsettings.duration = effectsettings.duration;
        } else
        if(effectsettings.startTime >= i && effectsettings.startTime + effectsettings.duration > j) {
            int l = effectsettings.startTime - i;
            int i1 = j - effectsettings.startTime;
            effectsettings.startTime = l;
            effectsettings.duration = i1;
        } else
        if(effectsettings.startTime < i && effectsettings.startTime + effectsettings.duration > j) {
            int k = j - i;
            effectsettings.startTime = 0;
            effectsettings.duration = k;
        }
        if(true) goto _L4; else goto _L3
_L3:
    }

    void clearPreviewSurface(Surface surface) {
        nativeClearSurface(surface);
    }

    void doPreview(Surface surface, long l, long l1, boolean flag, int i, 
            VideoEditor.PreviewProgressListener previewprogresslistener) {
        mPreviewProgress = l;
        mIsFirstProgress = true;
        mPreviewProgressListener = previewprogresslistener;
        if(mInvalidatePreviewArray) goto _L2; else goto _L1
_L1:
        int j = 0;
_L4:
        try {
            if(j < mPreviewEditSettings.clipSettingsArray.length) {
                if(mPreviewEditSettings.clipSettingsArray[j].fileType == 5)
                    mPreviewEditSettings.clipSettingsArray[j].clipPath = mPreviewEditSettings.clipSettingsArray[j].clipDecodedPath;
            } else {
                nativePopulateSettings(mPreviewEditSettings, mClipProperties, mAudioSettings);
                nativeStartPreview(surface, l, l1, i, flag);
                return;
            }
        }
        catch(IllegalArgumentException illegalargumentexception) {
            Log.e("MediaArtistNativeHelper", "Illegal argument exception in nativeStartPreview");
            throw illegalargumentexception;
        }
        catch(IllegalStateException illegalstateexception) {
            Log.e("MediaArtistNativeHelper", "Illegal state exception in nativeStartPreview");
            throw illegalstateexception;
        }
        catch(RuntimeException runtimeexception) {
            Log.e("MediaArtistNativeHelper", "Runtime exception in nativeStartPreview");
            throw runtimeexception;
        }
        j++;
        continue; /* Loop/switch isn't completed */
_L2:
        throw new IllegalStateException("generatePreview is in progress");
        if(true) goto _L4; else goto _L3
_L3:
    }

    void export(String s, String s1, int i, int j, List list, List list1, List list2, 
            VideoEditor.ExportProgressListener exportprogresslistener) {
        int i1;
        int j1;
        mExportFilename = s;
        previewStoryBoard(list, list1, list2, null);
        mExportProgressListener = exportprogresslistener;
        VideoEditorProfile videoeditorprofile = VideoEditorProfile.get();
        if(videoeditorprofile == null)
            throw new RuntimeException("Can't get the video editor profile");
        int k = videoeditorprofile.maxOutputVideoFrameHeight;
        int l = videoeditorprofile.maxOutputVideoFrameWidth;
        if(i > k)
            throw new IllegalArgumentException((new StringBuilder()).append("Unsupported export resolution. Supported maximum width:").append(l).append(" height:").append(k).append(" current height:").append(i).toString());
        i1 = VideoEditorProfile.getExportProfile(mExportVideoCodec);
        j1 = VideoEditorProfile.getExportLevel(mExportVideoCodec);
        mProgressToApp = 0;
        j;
        JVM INSTR lookupswitch 13: default 252
    //                   28000: 263
    //                   40000: 487
    //                   64000: 495
    //                   96000: 503
    //                   128000: 511
    //                   192000: 519
    //                   256000: 527
    //                   384000: 535
    //                   512000: 543
    //                   800000: 551
    //                   2000000: 559
    //                   5000000: 567
    //                   8000000: 575;
           goto _L1 _L2 _L3 _L4 _L5 _L6 _L7 _L8 _L9 _L10 _L11 _L12 _L13 _L14
_L1:
        throw new IllegalArgumentException("Argument Bitrate incorrect");
_L2:
        int k1 = 32000;
_L16:
        mPreviewEditSettings.videoFrameRate = 7;
        EditSettings editsettings = mPreviewEditSettings;
        mOutputFilename = s;
        editsettings.outputFile = s;
        int l1 = mVideoEditor.getAspectRatio();
        mPreviewEditSettings.videoFrameSize = findVideoResolution(l1, i);
        mPreviewEditSettings.videoFormat = mExportVideoCodec;
        mPreviewEditSettings.audioFormat = mExportAudioCodec;
        mPreviewEditSettings.videoProfile = i1;
        mPreviewEditSettings.videoLevel = j1;
        mPreviewEditSettings.audioSamplingFreq = 32000;
        mPreviewEditSettings.maxFileSize = 0;
        mPreviewEditSettings.audioChannels = 2;
        mPreviewEditSettings.videoBitrate = k1;
        mPreviewEditSettings.audioBitrate = 0x17700;
        mPreviewEditSettings.transitionSettingsArray = new TransitionSettings[-1 + mTotalClips];
        for(int i2 = 0; i2 < -1 + mTotalClips; i2++) {
            mPreviewEditSettings.transitionSettingsArray[i2] = new TransitionSettings();
            mPreviewEditSettings.transitionSettingsArray[i2].videoTransitionType = 0;
            mPreviewEditSettings.transitionSettingsArray[i2].audioTransitionType = 0;
        }

        break; /* Loop/switch isn't completed */
_L3:
        k1 = 48000;
        continue; /* Loop/switch isn't completed */
_L4:
        k1 = 64000;
        continue; /* Loop/switch isn't completed */
_L5:
        k1 = 0x17700;
        continue; /* Loop/switch isn't completed */
_L6:
        k1 = 0x1f400;
        continue; /* Loop/switch isn't completed */
_L7:
        k1 = 0x2ee00;
        continue; /* Loop/switch isn't completed */
_L8:
        k1 = 0x3e800;
        continue; /* Loop/switch isn't completed */
_L9:
        k1 = 0x5dc00;
        continue; /* Loop/switch isn't completed */
_L10:
        k1 = 0x7d000;
        continue; /* Loop/switch isn't completed */
_L11:
        k1 = 0xc3500;
        continue; /* Loop/switch isn't completed */
_L12:
        k1 = 0x1e8480;
        continue; /* Loop/switch isn't completed */
_L13:
        k1 = 0x4c4b40;
        continue; /* Loop/switch isn't completed */
_L14:
        k1 = 0x7a1200;
        if(true) goto _L16; else goto _L15
_L15:
        for(int j2 = 0; j2 < mPreviewEditSettings.clipSettingsArray.length; j2++)
            if(mPreviewEditSettings.clipSettingsArray[j2].fileType == 5)
                mPreviewEditSettings.clipSettingsArray[j2].clipPath = mPreviewEditSettings.clipSettingsArray[j2].clipOriginalPath;

        nativePopulateSettings(mPreviewEditSettings, mClipProperties, mAudioSettings);
        int k2;
        try {
            mProcessingState = 20;
            mProcessingObject = null;
            k2 = generateClip(mPreviewEditSettings);
            mProcessingState = 0;
        }
        catch(IllegalArgumentException illegalargumentexception) {
            Log.e("MediaArtistNativeHelper", "IllegalArgument for generateClip");
            throw illegalargumentexception;
        }
        catch(IllegalStateException illegalstateexception) {
            Log.e("MediaArtistNativeHelper", "IllegalStateExceptiont for generateClip");
            throw illegalstateexception;
        }
        catch(RuntimeException runtimeexception) {
            Log.e("MediaArtistNativeHelper", "RuntimeException for generateClip");
            throw runtimeexception;
        }
        if(k2 != 0) {
            Log.e("MediaArtistNativeHelper", "RuntimeException for generateClip");
            throw new RuntimeException((new StringBuilder()).append("generateClip failed with error=").append(k2).toString());
        } else {
            mExportProgressListener = null;
            return;
        }
    }

    void generateAudioGraph(String s, String s1, String s2, int i, int j, int k, ExtractAudioWaveformProgressListener extractaudiowaveformprogresslistener, 
            boolean flag) {
        mExtractAudioWaveformProgressListener = extractaudiowaveformprogresslistener;
        String s3;
        if(flag)
            s3 = String.format((new StringBuilder()).append(mProjectPath).append("/").append(s).append(".pcm").toString(), new Object[0]);
        else
            s3 = mAudioTrackPCMFilePath;
        if(flag)
            nativeGenerateRawAudio(s1, s3);
        nativeGenerateAudioGraph(s3, s2, i, j, k);
        if(flag)
            (new File(s3)).delete();
    }

    public int generateClip(EditSettings editsettings) {
        int i = -1;
        int j = nativeGenerateClip(editsettings);
        i = j;
_L2:
        return i;
        IllegalArgumentException illegalargumentexception;
        illegalargumentexception;
        Log.e("MediaArtistNativeHelper", "Illegal Argument exception in load settings");
        continue; /* Loop/switch isn't completed */
        IllegalStateException illegalstateexception;
        illegalstateexception;
        Log.e("MediaArtistNativeHelper", "Illegal state exception in load settings");
        continue; /* Loop/switch isn't completed */
        RuntimeException runtimeexception;
        runtimeexception;
        Log.e("MediaArtistNativeHelper", "Runtime exception in load settings");
        if(true) goto _L2; else goto _L1
_L1:
    }

    String generateEffectClip(MediaItem mediaitem, ClipSettings clipsettings, EditSettings editsettings, String s, int i) {
        EditSettings editsettings1 = new EditSettings();
        editsettings1.clipSettingsArray = new ClipSettings[1];
        editsettings1.clipSettingsArray[0] = clipsettings;
        editsettings1.backgroundMusicSettings = null;
        editsettings1.transitionSettingsArray = null;
        editsettings1.effectSettingsArray = editsettings.effectSettingsArray;
        String s1 = String.format((new StringBuilder()).append(mProjectPath).append("/").append("ClipEffectIntermediate").append("_").append(mediaitem.getId()).append(s).append(".3gp").toString(), new Object[0]);
        File file = new File(s1);
        if(file.exists())
            file.delete();
        int j = VideoEditorProfile.getExportProfile(2);
        int k = VideoEditorProfile.getExportLevel(2);
        editsettings1.videoProfile = j;
        editsettings1.videoLevel = k;
        int l;
        if(mediaitem instanceof MediaVideoItem) {
            MediaVideoItem mediavideoitem = (MediaVideoItem)mediaitem;
            editsettings1.audioFormat = 2;
            editsettings1.audioChannels = 2;
            editsettings1.audioBitrate = 64000;
            editsettings1.audioSamplingFreq = 32000;
            editsettings1.videoFormat = 2;
            editsettings1.videoFrameRate = 7;
            editsettings1.videoFrameSize = findVideoResolution(mVideoEditor.getAspectRatio(), mediavideoitem.getHeight());
            editsettings1.videoBitrate = findVideoBitrate(editsettings1.videoFrameSize);
        } else {
            MediaImageItem mediaimageitem = (MediaImageItem)mediaitem;
            editsettings1.audioBitrate = 64000;
            editsettings1.audioChannels = 2;
            editsettings1.audioFormat = 2;
            editsettings1.audioSamplingFreq = 32000;
            editsettings1.videoFormat = 2;
            editsettings1.videoFrameRate = 7;
            editsettings1.videoFrameSize = findVideoResolution(mVideoEditor.getAspectRatio(), mediaimageitem.getScaledHeight());
            editsettings1.videoBitrate = findVideoBitrate(editsettings1.videoFrameSize);
        }
        editsettings1.outputFile = s1;
        if(i == 1)
            mProcessingState = 11;
        else
        if(i == 2)
            mProcessingState = 12;
        mProcessingObject = mediaitem;
        l = generateClip(editsettings1);
        mProcessingState = 0;
        if(l == 0) {
            clipsettings.clipPath = s1;
            clipsettings.fileType = 0;
            return s1;
        } else {
            throw new RuntimeException("preview generation cannot be completed");
        }
    }

    String generateKenBurnsClip(EditSettings editsettings, MediaImageItem mediaimageitem) {
        editsettings.backgroundMusicSettings = null;
        editsettings.transitionSettingsArray = null;
        editsettings.effectSettingsArray = null;
        String s = String.format((new StringBuilder()).append(mProjectPath).append("/").append("ImageClip-").append(mediaimageitem.getId()).append(".3gp").toString(), new Object[0]);
        File file = new File(s);
        if(file.exists())
            file.delete();
        int i = VideoEditorProfile.getExportProfile(2);
        int j = VideoEditorProfile.getExportLevel(2);
        editsettings.videoProfile = i;
        editsettings.videoLevel = j;
        editsettings.outputFile = s;
        editsettings.audioBitrate = 64000;
        editsettings.audioChannels = 2;
        editsettings.audioFormat = 2;
        editsettings.audioSamplingFreq = 32000;
        editsettings.videoFormat = 2;
        editsettings.videoFrameRate = 7;
        editsettings.videoFrameSize = findVideoResolution(mVideoEditor.getAspectRatio(), mediaimageitem.getScaledHeight());
        editsettings.videoBitrate = findVideoBitrate(editsettings.videoFrameSize);
        mProcessingState = 3;
        mProcessingObject = mediaimageitem;
        int k = generateClip(editsettings);
        mProcessingState = 0;
        if(k != 0)
            throw new RuntimeException("preview generation cannot be completed");
        else
            return s;
    }

    String generateTransitionClip(EditSettings editsettings, String s, MediaItem mediaitem, MediaItem mediaitem1, Transition transition) {
        String s1 = String.format((new StringBuilder()).append(mProjectPath).append("/").append(s).append(".3gp").toString(), new Object[0]);
        int i = VideoEditorProfile.getExportProfile(2);
        int j = VideoEditorProfile.getExportLevel(2);
        editsettings.videoProfile = i;
        editsettings.videoLevel = j;
        editsettings.outputFile = s1;
        editsettings.audioBitrate = 64000;
        editsettings.audioChannels = 2;
        editsettings.audioFormat = 2;
        editsettings.audioSamplingFreq = 32000;
        editsettings.videoFormat = 2;
        editsettings.videoFrameRate = 7;
        editsettings.videoFrameSize = getTransitionResolution(mediaitem, mediaitem1);
        editsettings.videoBitrate = findVideoBitrate(editsettings.videoFrameSize);
        if((new File(s1)).exists())
            (new File(s1)).delete();
        mProcessingState = 13;
        mProcessingObject = transition;
        int k = generateClip(editsettings);
        mProcessingState = 0;
        if(k != 0)
            throw new RuntimeException("preview generation cannot be completed");
        else
            return s1;
    }

    int getAspectRatio(int i, int j) {
        double d;
        byte byte0;
        d = (new BigDecimal((double)i / (double)j)).setScale(3, 4).doubleValue();
        byte0 = 2;
        if(d < 1.7D) goto _L2; else goto _L1
_L1:
        byte0 = 2;
_L4:
        return byte0;
_L2:
        if(d >= 1.6000000000000001D)
            byte0 = 4;
        else
        if(d >= 1.5D)
            byte0 = 1;
        else
        if(d > 1.3D)
            byte0 = 3;
        else
        if(d >= 1.2D)
            byte0 = 5;
        if(true) goto _L4; else goto _L3
_L3:
    }

    int getAudioCodecType(int i) {
        i;
        JVM INSTR tableswitch 1 5: default 36
    //                   1 41
    //                   2 46
    //                   3 36
    //                   4 36
    //                   5 51;
           goto _L1 _L2 _L3 _L1 _L1 _L4
_L1:
        byte byte0 = -1;
_L6:
        return byte0;
_L2:
        byte0 = 1;
        continue; /* Loop/switch isn't completed */
_L3:
        byte0 = 2;
        continue; /* Loop/switch isn't completed */
_L4:
        byte0 = 5;
        if(true) goto _L6; else goto _L5
_L5:
    }

    boolean getAudioflag() {
        return mRegenerateAudio;
    }

    EffectSettings getEffectSettings(EffectColor effectcolor) {
        EffectSettings effectsettings = new EffectSettings();
        effectsettings.startTime = (int)effectcolor.getStartTime();
        effectsettings.duration = (int)effectcolor.getDuration();
        effectsettings.videoEffectType = getEffectColorType(effectcolor);
        effectsettings.audioEffectType = 0;
        effectsettings.startPercent = 0;
        effectsettings.durationPercent = 0;
        effectsettings.framingFile = null;
        effectsettings.topLeftX = 0;
        effectsettings.topLeftY = 0;
        effectsettings.framingResize = false;
        effectsettings.text = null;
        effectsettings.textRenderingData = null;
        effectsettings.textBufferWidth = 0;
        effectsettings.textBufferHeight = 0;
        if(effectcolor.getType() == 5)
            effectsettings.fiftiesFrameRate = 15;
        else
            effectsettings.fiftiesFrameRate = 0;
        if(effectsettings.videoEffectType == 267 || effectsettings.videoEffectType == 268)
            effectsettings.rgb16InputColor = effectcolor.getColor();
        effectsettings.alphaBlendingStartPercent = 0;
        effectsettings.alphaBlendingMiddlePercent = 0;
        effectsettings.alphaBlendingEndPercent = 0;
        effectsettings.alphaBlendingFadeInTimePercent = 0;
        effectsettings.alphaBlendingFadeOutTimePercent = 0;
        return effectsettings;
    }

    int getFileType(int i) {
        i;
        JVM INSTR lookupswitch 8: default 76
    //                   0: 88
    //                   1: 93
    //                   2: 120
    //                   3: 109
    //                   5: 98
    //                   8: 103
    //                   10: 114
    //                   255: 81;
           goto _L1 _L2 _L3 _L4 _L5 _L6 _L7 _L8 _L9
_L1:
        char c = '\uFFFF';
_L11:
        return c;
_L9:
        c = '\377';
        continue; /* Loop/switch isn't completed */
_L2:
        c = '\0';
        continue; /* Loop/switch isn't completed */
_L3:
        c = '\001';
        continue; /* Loop/switch isn't completed */
_L6:
        c = '\005';
        continue; /* Loop/switch isn't completed */
_L7:
        c = '\b';
        continue; /* Loop/switch isn't completed */
_L5:
        c = '\003';
        continue; /* Loop/switch isn't completed */
_L8:
        c = '\n';
        continue; /* Loop/switch isn't completed */
_L4:
        c = '\002';
        if(true) goto _L11; else goto _L10
_L10:
    }

    int getFrameRate(int i) {
        i;
        JVM INSTR tableswitch 0 7: default 48
    //                   0 53
    //                   1 58
    //                   2 64
    //                   3 70
    //                   4 76
    //                   5 82
    //                   6 88
    //                   7 94;
           goto _L1 _L2 _L3 _L4 _L5 _L6 _L7 _L8 _L9
_L1:
        byte byte0 = -1;
_L11:
        return byte0;
_L2:
        byte0 = 5;
        continue; /* Loop/switch isn't completed */
_L3:
        byte0 = 8;
        continue; /* Loop/switch isn't completed */
_L4:
        byte0 = 10;
        continue; /* Loop/switch isn't completed */
_L5:
        byte0 = 13;
        continue; /* Loop/switch isn't completed */
_L6:
        byte0 = 15;
        continue; /* Loop/switch isn't completed */
_L7:
        byte0 = 20;
        continue; /* Loop/switch isn't completed */
_L8:
        byte0 = 25;
        continue; /* Loop/switch isn't completed */
_L9:
        byte0 = 30;
        if(true) goto _L11; else goto _L10
_L10:
    }

    boolean getGeneratePreview() {
        return mInvalidatePreviewArray;
    }

    int getMediaItemFileType(int i) {
        i;
        JVM INSTR lookupswitch 6: default 60
    //                   0: 72
    //                   1: 77
    //                   5: 82
    //                   8: 87
    //                   10: 93
    //                   255: 65;
           goto _L1 _L2 _L3 _L4 _L5 _L6 _L7
_L1:
        char c = '\uFFFF';
_L9:
        return c;
_L7:
        c = '\377';
        continue; /* Loop/switch isn't completed */
_L2:
        c = '\0';
        continue; /* Loop/switch isn't completed */
_L3:
        c = '\001';
        continue; /* Loop/switch isn't completed */
_L4:
        c = '\005';
        continue; /* Loop/switch isn't completed */
_L5:
        c = '\b';
        continue; /* Loop/switch isn't completed */
_L6:
        c = '\n';
        if(true) goto _L9; else goto _L8
_L8:
    }

    int getMediaItemRenderingMode(int i) {
        i;
        JVM INSTR tableswitch 0 2: default 28
    //                   0 33
    //                   1 38
    //                   2 43;
           goto _L1 _L2 _L3 _L4
_L1:
        byte byte0 = -1;
_L6:
        return byte0;
_L2:
        byte0 = 2;
        continue; /* Loop/switch isn't completed */
_L3:
        byte0 = 0;
        continue; /* Loop/switch isn't completed */
_L4:
        byte0 = 1;
        if(true) goto _L6; else goto _L5
_L5:
    }

    native Properties getMediaProperties(String s) throws IllegalArgumentException, IllegalStateException, RuntimeException, Exception;

    EffectSettings getOverlaySettings(OverlayFrame overlayframe) {
        EffectSettings effectsettings;
        Bitmap bitmap;
        int k;
        int l;
        short word0;
        effectsettings = new EffectSettings();
        effectsettings.startTime = (int)overlayframe.getStartTime();
        effectsettings.duration = (int)overlayframe.getDuration();
        effectsettings.videoEffectType = 262;
        effectsettings.audioEffectType = 0;
        effectsettings.startPercent = 0;
        effectsettings.durationPercent = 0;
        effectsettings.framingFile = null;
        bitmap = overlayframe.getBitmap();
        if(bitmap == null)
            break MISSING_BLOCK_LABEL_408;
        effectsettings.framingFile = overlayframe.getFilename();
        if(effectsettings.framingFile == null) {
            int j1;
            short word1;
            try {
                overlayframe.save(mProjectPath);
            }
            catch(IOException ioexception) {
                Log.e("MediaArtistNativeHelper", "getOverlaySettings : File not found");
            }
            effectsettings.framingFile = overlayframe.getFilename();
        }
        if(bitmap.getConfig() == android.graphics.Bitmap.Config.ARGB_8888)
            effectsettings.bitmapType = 6;
        else
        if(bitmap.getConfig() == android.graphics.Bitmap.Config.ARGB_4444) {
            effectsettings.bitmapType = 5;
        } else {
            if(bitmap.getConfig() != android.graphics.Bitmap.Config.RGB_565)
                continue; /* Loop/switch isn't completed */
            effectsettings.bitmapType = 4;
        }
_L2:
        effectsettings.width = bitmap.getWidth();
        effectsettings.height = bitmap.getHeight();
        effectsettings.framingBuffer = new int[effectsettings.width];
        k = 0;
        l = 0;
        word0 = 255;
_L3:
        if(k >= effectsettings.height)
            break MISSING_BLOCK_LABEL_311;
        bitmap.getPixels(effectsettings.framingBuffer, 0, effectsettings.width, 0, k, effectsettings.width, 1);
        for(j1 = 0; j1 < effectsettings.width; j1++) {
            word1 = (short)(0xff & effectsettings.framingBuffer[j1] >> 24);
            if(word1 > l)
                l = word1;
            if(word1 < word0)
                word0 = word1;
        }

        break MISSING_BLOCK_LABEL_305;
        if(bitmap.getConfig() != android.graphics.Bitmap.Config.ALPHA_8) goto _L2; else goto _L1
_L1:
        throw new RuntimeException("Bitmap config not supported");
        k++;
          goto _L3
        int i1 = (100 * ((l + word0) / 2)) / 256;
        effectsettings.alphaBlendingEndPercent = i1;
        effectsettings.alphaBlendingMiddlePercent = i1;
        effectsettings.alphaBlendingStartPercent = i1;
        effectsettings.alphaBlendingFadeInTimePercent = 100;
        effectsettings.alphaBlendingFadeOutTimePercent = 100;
        effectsettings.framingBuffer = null;
        effectsettings.width = overlayframe.getResizedRGBSizeWidth();
        if(effectsettings.width == 0)
            effectsettings.width = bitmap.getWidth();
        effectsettings.height = overlayframe.getResizedRGBSizeHeight();
        if(effectsettings.height == 0)
            effectsettings.height = bitmap.getHeight();
        effectsettings.topLeftX = 0;
        effectsettings.topLeftY = 0;
        effectsettings.framingResize = true;
        effectsettings.text = null;
        effectsettings.textRenderingData = null;
        effectsettings.textBufferWidth = 0;
        effectsettings.textBufferHeight = 0;
        effectsettings.fiftiesFrameRate = 0;
        effectsettings.rgb16InputColor = 0;
        int i;
        int j;
        if(overlayframe.getMediaItem() instanceof MediaImageItem) {
            if(((MediaImageItem)overlayframe.getMediaItem()).getGeneratedImageClip() != null) {
                j = ((MediaImageItem)overlayframe.getMediaItem()).getGeneratedClipHeight();
                i = getAspectRatio(((MediaImageItem)overlayframe.getMediaItem()).getGeneratedClipWidth(), j);
            } else {
                j = ((MediaImageItem)overlayframe.getMediaItem()).getScaledHeight();
                i = overlayframe.getMediaItem().getAspectRatio();
            }
        } else {
            i = overlayframe.getMediaItem().getAspectRatio();
            j = overlayframe.getMediaItem().getHeight();
        }
        effectsettings.framingScaledSize = findVideoResolution(i, j);
        return effectsettings;
    }

    Bitmap getPixels(String s, int i, int j, long l, int k) {
        final Bitmap result[] = new Bitmap[1];
        int ai[] = new int[1];
        ai[0] = 0;
        getPixelsList(s, i, j, l, l, 1, ai, new MediaItem.GetThumbnailListCallback() {

            public void onThumbnail(Bitmap bitmap, int i1) {
                result[0] = bitmap;
            }

            final MediaArtistNativeHelper this$0;
            final Bitmap val$result[];

             {
                this$0 = MediaArtistNativeHelper.this;
                result = abitmap;
                super();
            }
        }, k);
        return result[0];
    }

    void getPixelsList(String s, int i, int j, long l, long l1, 
            int k, int ai[], final MediaItem.GetThumbnailListCallback callback, final int videoRotation) {
        final int decWidth = -2 & i + 1;
        final int decHeight = -2 & j + 1;
        final int thumbnailSize = decWidth * decHeight;
        final int decArray[] = new int[thumbnailSize];
        final IntBuffer decBuffer = IntBuffer.allocate(thumbnailSize);
        final boolean needToMassage;
        final Bitmap tmpBitmap;
        boolean flag;
        final int outWidth;
        final int outHeight;
        if(decWidth != i || decHeight != j || videoRotation != 0)
            needToMassage = true;
        else
            needToMassage = false;
        if(needToMassage)
            tmpBitmap = Bitmap.createBitmap(decWidth, decHeight, android.graphics.Bitmap.Config.ARGB_8888);
        else
            tmpBitmap = null;
        if(videoRotation == 90 || videoRotation == 270)
            flag = true;
        else
            flag = false;
        if(flag)
            outWidth = j;
        else
            outWidth = i;
        if(flag)
            outHeight = i;
        else
            outHeight = j;
        nativeGetPixelsList(s, decArray, decWidth, decHeight, k, l, l1, ai, new NativeGetPixelsListCallback() {

            public void onThumbnail(int i1) {
                Bitmap bitmap = Bitmap.createBitmap(outWidth, outHeight, android.graphics.Bitmap.Config.ARGB_8888);
                decBuffer.put(decArray, 0, thumbnailSize);
                decBuffer.rewind();
                if(!needToMassage) {
                    bitmap.copyPixelsFromBuffer(decBuffer);
                } else {
                    tmpBitmap.copyPixelsFromBuffer(decBuffer);
                    Canvas canvas = new Canvas(bitmap);
                    Matrix matrix = new Matrix();
                    matrix.postScale(1.0F / (float)decWidth, 1.0F / (float)decHeight);
                    matrix.postRotate(videoRotation, 0.5F, 0.5F);
                    matrix.postScale(outWidth, outHeight);
                    canvas.drawBitmap(tmpBitmap, matrix, MediaArtistNativeHelper.sResizePaint);
                }
                callback.onThumbnail(bitmap, i1);
            }

            final MediaArtistNativeHelper this$0;
            final MediaItem.GetThumbnailListCallback val$callback;
            final int val$decArray[];
            final IntBuffer val$decBuffer;
            final int val$decHeight;
            final int val$decWidth;
            final boolean val$needToMassage;
            final int val$outHeight;
            final int val$outWidth;
            final int val$thumbnailSize;
            final Bitmap val$tmpBitmap;
            final int val$videoRotation;

             {
                this$0 = MediaArtistNativeHelper.this;
                outWidth = i;
                outHeight = j;
                decBuffer = intbuffer;
                decArray = ai;
                thumbnailSize = k;
                needToMassage = flag;
                tmpBitmap = bitmap;
                decWidth = l;
                decHeight = i1;
                videoRotation = j1;
                callback = getthumbnaillistcallback;
                super();
            }
        });
        if(tmpBitmap != null)
            tmpBitmap.recycle();
    }

    String getProjectAudioTrackPCMFilePath() {
        return mAudioTrackPCMFilePath;
    }

    String getProjectPath() {
        return mProjectPath;
    }

    int getSlideSettingsDirection(int i) {
        i;
        JVM INSTR tableswitch 0 3: default 32
    //                   0 37
    //                   1 42
    //                   2 47
    //                   3 52;
           goto _L1 _L2 _L3 _L4 _L5
_L1:
        byte byte0 = -1;
_L7:
        return byte0;
_L2:
        byte0 = 0;
        continue; /* Loop/switch isn't completed */
_L3:
        byte0 = 1;
        continue; /* Loop/switch isn't completed */
_L4:
        byte0 = 2;
        continue; /* Loop/switch isn't completed */
_L5:
        byte0 = 3;
        if(true) goto _L7; else goto _L6
_L6:
    }

    int getVideoCodecType(int i) {
        i;
        JVM INSTR tableswitch 1 3: default 28
    //                   1 33
    //                   2 38
    //                   3 43;
           goto _L1 _L2 _L3 _L4
_L1:
        byte byte0 = -1;
_L6:
        return byte0;
_L2:
        byte0 = 1;
        continue; /* Loop/switch isn't completed */
_L3:
        byte0 = 2;
        continue; /* Loop/switch isn't completed */
_L4:
        byte0 = 3;
        if(true) goto _L6; else goto _L5
_L5:
    }

    int getVideoTransitionBehaviour(int i) {
        i;
        JVM INSTR tableswitch 0 4: default 36
    //                   0 41
    //                   1 46
    //                   2 51
    //                   3 56
    //                   4 61;
           goto _L1 _L2 _L3 _L4 _L5 _L6
_L1:
        byte byte0 = -1;
_L8:
        return byte0;
_L2:
        byte0 = 0;
        continue; /* Loop/switch isn't completed */
_L3:
        byte0 = 2;
        continue; /* Loop/switch isn't completed */
_L4:
        byte0 = 1;
        continue; /* Loop/switch isn't completed */
_L5:
        byte0 = 3;
        continue; /* Loop/switch isn't completed */
_L6:
        byte0 = 4;
        if(true) goto _L8; else goto _L7
_L7:
    }

    void initClipSettings(ClipSettings clipsettings) {
        clipsettings.clipPath = null;
        clipsettings.clipDecodedPath = null;
        clipsettings.clipOriginalPath = null;
        clipsettings.fileType = 0;
        clipsettings.endCutTime = 0;
        clipsettings.beginCutTime = 0;
        clipsettings.beginCutPercent = 0;
        clipsettings.endCutPercent = 0;
        clipsettings.panZoomEnabled = false;
        clipsettings.panZoomPercentStart = 0;
        clipsettings.panZoomTopLeftXStart = 0;
        clipsettings.panZoomTopLeftYStart = 0;
        clipsettings.panZoomPercentEnd = 0;
        clipsettings.panZoomTopLeftXEnd = 0;
        clipsettings.panZoomTopLeftYEnd = 0;
        clipsettings.mediaRendering = 0;
        clipsettings.rotationDegree = 0;
    }

    void invalidatePcmFile() {
        if(mAudioTrackPCMFilePath != null) {
            (new File(mAudioTrackPCMFilePath)).delete();
            mAudioTrackPCMFilePath = null;
        }
    }

    int nativeHelperGetAspectRatio() {
        return mVideoEditor.getAspectRatio();
    }

    void previewStoryBoard(List list, List list1, List list2, VideoEditor.MediaProcessingProgressListener mediaprocessingprogresslistener) {
        if(!mInvalidatePreviewArray) goto _L2; else goto _L1
_L1:
        int i;
        int j;
        int k;
        int l;
        int i1;
        int j1;
        i = 0;
        j = 0;
        k = 0;
        l = 0;
        i1 = 0;
        j1 = 0;
        mPreviewEditSettings = new EditSettings();
        mClipProperties = new PreviewClipProperties();
        mTotalClips = 0;
        mTotalClips = list.size();
        Iterator iterator = list1.iterator();
        do {
            if(!iterator.hasNext())
                break;
            if(((Transition)iterator.next()).getDuration() > 0L)
                mTotalClips = 1 + mTotalClips;
        } while(true);
        int k1 = getTotalEffects(list);
        mPreviewEditSettings.clipSettingsArray = new ClipSettings[mTotalClips];
        mPreviewEditSettings.effectSettingsArray = new EffectSettings[k1];
        mClipProperties.clipProperties = new Properties[mTotalClips];
        mMediaProcessingProgressListener = mediaprocessingprogresslistener;
        mProgressToApp = 0;
        if(list.size() <= 0) goto _L4; else goto _L3
_L3:
        int l1 = 0;
_L22:
        if(l1 >= list.size()) goto _L6; else goto _L5
_L5:
        MediaItem mediaitem;
        int i2;
        mediaitem = (MediaItem)list.get(l1);
        Transition transition;
        Transition transition1;
        boolean flag;
        List list3;
        int j2;
        if(mediaitem instanceof MediaVideoItem) {
            l = (int)((MediaVideoItem)mediaitem).getBoundaryBeginTime();
            i1 = (int)((MediaVideoItem)mediaitem).getBoundaryEndTime();
        } else
        if(mediaitem instanceof MediaImageItem) {
            l = 0;
            i1 = (int)((MediaImageItem)mediaitem).getTimelineDuration();
        }
        transition = mediaitem.getBeginTransition();
        if(transition != null && transition.getDuration() > 0L) {
            generateTransition(transition, mPreviewEditSettings, mClipProperties, i);
            j += mClipProperties.clipProperties[i].duration;
            i++;
        }
        k = populateMediaItemProperties(mediaitem, i, k);
        if(!(mediaitem instanceof MediaImageItem)) goto _L8; else goto _L7
_L7:
        i2 = 0;
        flag = false;
        list3 = mediaitem.getAllEffects();
_L20:
        j2 = list3.size();
        if(i2 >= j2) goto _L10; else goto _L9
_L9:
        if(!(list3.get(i2) instanceof EffectKenBurns)) goto _L12; else goto _L11
_L11:
        flag = true;
_L10:
        if(!flag) goto _L14; else goto _L13
_L13:
        if(((MediaImageItem)mediaitem).getGeneratedImageClip() == null) goto _L16; else goto _L15
_L15:
        mClipProperties.clipProperties[i] = getMediaProperties(((MediaImageItem)mediaitem).getGeneratedImageClip());
_L21:
        mClipProperties.clipProperties[i].Id = mediaitem.getId();
        checkOddSizeImage(mediaitem, mClipProperties, i);
        adjustVolume(mediaitem, mClipProperties, i);
        adjustMediaItemBoundary(mPreviewEditSettings.clipSettingsArray[i], mClipProperties.clipProperties[i], mediaitem);
        j1 = populateEffects(mediaitem, mPreviewEditSettings.effectSettingsArray, j1, l, i1, j);
        j += mClipProperties.clipProperties[i].duration;
        i++;
        if(l1 != -1 + list.size()) goto _L18; else goto _L17
_L17:
        transition1 = mediaitem.getEndTransition();
        if(transition1 == null || transition1.getDuration() <= 0L) goto _L18; else goto _L19
_L19:
        generateTransition(transition1, mPreviewEditSettings, mClipProperties, i);
_L6:
        if(!mErrorFlagSet) {
            mPreviewEditSettings.videoFrameSize = findVideoResolution(mVideoEditor.getAspectRatio(), k);
            populateBackgroundMusicProperties(list2);
            Exception exception;
            Exception exception1;
            Exception exception2;
            try {
                nativePopulateSettings(mPreviewEditSettings, mClipProperties, mAudioSettings);
            }
            catch(IllegalArgumentException illegalargumentexception) {
                Log.e("MediaArtistNativeHelper", "Illegal argument exception in nativePopulateSettings");
                throw illegalargumentexception;
            }
            catch(IllegalStateException illegalstateexception) {
                Log.e("MediaArtistNativeHelper", "Illegal state exception in nativePopulateSettings");
                throw illegalstateexception;
            }
            catch(RuntimeException runtimeexception) {
                Log.e("MediaArtistNativeHelper", "Runtime exception in nativePopulateSettings");
                throw runtimeexception;
            }
            mInvalidatePreviewArray = false;
            mProcessingState = 0;
        }
_L4:
        if(mErrorFlagSet) {
            mErrorFlagSet = false;
            throw new RuntimeException("preview generation cannot be completed");
        }
        break; /* Loop/switch isn't completed */
_L12:
        i2++;
          goto _L20
_L16:
        try {
            mClipProperties.clipProperties[i] = getMediaProperties(((MediaImageItem)mediaitem).getScaledImageFileName());
            mClipProperties.clipProperties[i].width = ((MediaImageItem)mediaitem).getScaledWidth();
            mClipProperties.clipProperties[i].height = ((MediaImageItem)mediaitem).getScaledHeight();
        }
        // Misplaced declaration of an exception variable
        catch(Exception exception2) {
            throw new IllegalArgumentException("Unsupported file or file not found");
        }
          goto _L21
_L14:
        try {
            mClipProperties.clipProperties[i] = getMediaProperties(((MediaImageItem)mediaitem).getScaledImageFileName());
        }
        // Misplaced declaration of an exception variable
        catch(Exception exception1) {
            throw new IllegalArgumentException("Unsupported file or file not found");
        }
        mClipProperties.clipProperties[i].width = ((MediaImageItem)mediaitem).getScaledWidth();
        mClipProperties.clipProperties[i].height = ((MediaImageItem)mediaitem).getScaledHeight();
          goto _L21
_L8:
        try {
            mClipProperties.clipProperties[i] = getMediaProperties(mediaitem.getFilename());
        }
        // Misplaced declaration of an exception variable
        catch(Exception exception) {
            throw new IllegalArgumentException("Unsupported file or file not found");
        }
          goto _L21
_L18:
        l1++;
        if(true) goto _L22; else goto _L2
_L2:
    }

    void releaseNativeHelper() throws InterruptedException {
        release();
    }

    long renderMediaItemPreviewFrame(Surface surface, String s, long l, int i, int j) {
        int k;
        try {
            k = nativeRenderMediaItemPreviewFrame(surface, s, i, j, 0, 0, l);
        }
        catch(IllegalArgumentException illegalargumentexception) {
            Log.e("MediaArtistNativeHelper", "Illegal Argument exception in renderMediaItemPreviewFrame");
            throw illegalargumentexception;
        }
        catch(IllegalStateException illegalstateexception) {
            Log.e("MediaArtistNativeHelper", "Illegal state exception in renderMediaItemPreviewFrame");
            throw illegalstateexception;
        }
        catch(RuntimeException runtimeexception) {
            Log.e("MediaArtistNativeHelper", "Runtime exception in renderMediaItemPreviewFrame");
            throw runtimeexception;
        }
        return (long)k;
    }

    long renderPreviewFrame(Surface surface, long l, int i, int j, VideoEditor.OverlayData overlaydata) {
        int k;
        if(mInvalidatePreviewArray) {
            if(Log.isLoggable("MediaArtistNativeHelper", 3))
                Log.d("MediaArtistNativeHelper", "Call generate preview first");
            throw new IllegalStateException("Call generate preview first");
        }
        k = 0;
_L2:
        long l1;
        try {
            if(k < mPreviewEditSettings.clipSettingsArray.length) {
                if(mPreviewEditSettings.clipSettingsArray[k].fileType == 5)
                    mPreviewEditSettings.clipSettingsArray[k].clipPath = mPreviewEditSettings.clipSettingsArray[k].clipDecodedPath;
                break MISSING_BLOCK_LABEL_213;
            }
            mRenderPreviewOverlayFile = null;
            mRenderPreviewRenderingMode = 0;
            nativePopulateSettings(mPreviewEditSettings, mClipProperties, mAudioSettings);
            l1 = nativeRenderPreviewFrame(surface, l, i, j);
            if(mRenderPreviewOverlayFile != null)
                overlaydata.set(BitmapFactory.decodeFile(mRenderPreviewOverlayFile), mRenderPreviewRenderingMode);
            else
                overlaydata.setClear();
            break; /* Loop/switch isn't completed */
        }
        catch(IllegalArgumentException illegalargumentexception) {
            Log.e("MediaArtistNativeHelper", "Illegal Argument exception in nativeRenderPreviewFrame");
            throw illegalargumentexception;
        }
        catch(IllegalStateException illegalstateexception) {
            Log.e("MediaArtistNativeHelper", "Illegal state exception in nativeRenderPreviewFrame");
            throw illegalstateexception;
        }
        catch(RuntimeException runtimeexception) {
            Log.e("MediaArtistNativeHelper", "Runtime exception in nativeRenderPreviewFrame");
            throw runtimeexception;
        }
        k++;
        if(true) goto _L2; else goto _L1
_L1:
        return l1;
    }

    void setAudioCodec(int i) {
        mExportAudioCodec = i;
    }

    void setAudioflag(boolean flag) {
        if(!(new File(String.format((new StringBuilder()).append(mProjectPath).append("/").append("AudioPcm.pcm").toString(), new Object[0]))).exists())
            flag = true;
        mRegenerateAudio = flag;
    }

    void setGeneratePreview(boolean flag) {
        boolean flag1 = false;
        lock();
        flag1 = true;
        mInvalidatePreviewArray = flag;
        if(flag1)
            unlock();
_L2:
        return;
        InterruptedException interruptedexception;
        interruptedexception;
        Log.e("MediaArtistNativeHelper", "Runtime exception in renderMediaItemPreviewFrame");
        if(flag1)
            unlock();
        if(true) goto _L2; else goto _L1
_L1:
        Exception exception;
        exception;
        if(flag1)
            unlock();
        throw exception;
    }

    void setVideoCodec(int i) {
        mExportVideoCodec = i;
    }

    void stop(String s) {
        try {
            stopEncoding();
            (new File(mExportFilename)).delete();
            return;
        }
        catch(IllegalStateException illegalstateexception) {
            Log.e("MediaArtistNativeHelper", "Illegal state exception in unload settings");
            throw illegalstateexception;
        }
        catch(RuntimeException runtimeexception) {
            Log.e("MediaArtistNativeHelper", "Runtime exception in unload settings");
            throw runtimeexception;
        }
    }

    long stopPreview() {
        return (long)nativeStopPreview();
    }

    private static final String AUDIO_TRACK_PCM_FILE = "AudioPcm.pcm";
    private static final int MAX_THUMBNAIL_PERMITTED = 8;
    public static final int PROCESSING_AUDIO_PCM = 1;
    public static final int PROCESSING_EXPORT = 20;
    public static final int PROCESSING_INTERMEDIATE1 = 11;
    public static final int PROCESSING_INTERMEDIATE2 = 12;
    public static final int PROCESSING_INTERMEDIATE3 = 13;
    public static final int PROCESSING_KENBURNS = 3;
    public static final int PROCESSING_NONE = 0;
    public static final int PROCESSING_TRANSITION = 2;
    private static final String TAG = "MediaArtistNativeHelper";
    public static final int TASK_ENCODING = 2;
    public static final int TASK_LOADING_SETTINGS = 1;
    private static final Paint sResizePaint = new Paint(2);
    private AudioSettings mAudioSettings;
    private AudioTrack mAudioTrack;
    private String mAudioTrackPCMFilePath;
    private PreviewClipProperties mClipProperties;
    private boolean mErrorFlagSet;
    private int mExportAudioCodec;
    private String mExportFilename;
    private VideoEditor.ExportProgressListener mExportProgressListener;
    private int mExportVideoCodec;
    private ExtractAudioWaveformProgressListener mExtractAudioWaveformProgressListener;
    private boolean mInvalidatePreviewArray;
    private boolean mIsFirstProgress;
    private final Semaphore mLock;
    private int mManualEditContext;
    private VideoEditor.MediaProcessingProgressListener mMediaProcessingProgressListener;
    private String mOutputFilename;
    private EditSettings mPreviewEditSettings;
    private long mPreviewProgress;
    private VideoEditor.PreviewProgressListener mPreviewProgressListener;
    private Object mProcessingObject;
    private int mProcessingState;
    private int mProgressToApp;
    private final String mProjectPath;
    private boolean mRegenerateAudio;
    private String mRenderPreviewOverlayFile;
    private int mRenderPreviewRenderingMode;
    private EditSettings mStoryBoardSettings;
    private int mTotalClips;
    private final VideoEditor mVideoEditor;

    static  {
        System.loadLibrary("videoeditor_jni");
    }

}
