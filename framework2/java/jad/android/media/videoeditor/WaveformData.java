// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.media.videoeditor;

import java.io.*;

public class WaveformData {

    private WaveformData() throws IOException {
        mFrameDurationMs = 0;
        mFramesCount = 0;
        mGains = null;
    }

    WaveformData(String s) throws IOException {
        FileInputStream fileinputstream;
        if(s == null)
            throw new IllegalArgumentException("WaveformData : filename is null");
        fileinputstream = null;
        FileInputStream fileinputstream1 = new FileInputStream(new File(s));
        int i1;
        byte abyte0[] = new byte[4];
        fileinputstream1.read(abyte0, 0, 4);
        int i = 0;
        int j = 0;
        for(int k = 0; k < 4; k++)
            i = i << 8 | 0xff & abyte0[k];

        mFrameDurationMs = i;
        byte abyte1[] = new byte[4];
        fileinputstream1.read(abyte1, 0, 4);
        for(int l = 0; l < 4; l++)
            j = j << 8 | 0xff & abyte1[l];

        mFramesCount = j;
        mGains = new short[mFramesCount];
        i1 = 0;
_L1:
        if(i1 >= mFramesCount)
            break MISSING_BLOCK_LABEL_186;
        mGains[i1] = (short)fileinputstream1.read();
        i1++;
          goto _L1
        if(fileinputstream1 != null)
            fileinputstream1.close();
        return;
        Exception exception;
        exception;
_L3:
        if(fileinputstream != null)
            fileinputstream.close();
        throw exception;
        exception;
        fileinputstream = fileinputstream1;
        if(true) goto _L3; else goto _L2
_L2:
    }

    public int getFrameDuration() {
        return mFrameDurationMs;
    }

    public short[] getFrameGains() {
        return mGains;
    }

    public int getFramesCount() {
        return mFramesCount;
    }

    private final int mFrameDurationMs;
    private final int mFramesCount;
    private final short mGains[];
}
