// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.media.videoeditor;

import android.graphics.Rect;

// Referenced classes of package android.media.videoeditor:
//            Effect, MediaItem

public class EffectKenBurns extends Effect {

    private EffectKenBurns() {
        this(null, null, null, null, 0L, 0L);
    }

    public EffectKenBurns(MediaItem mediaitem, String s, Rect rect, Rect rect1, long l, long l1) {
        super(mediaitem, s, l, l1);
        if(rect.width() <= 0 || rect.height() <= 0)
            throw new IllegalArgumentException("Invalid Start rectangle");
        if(rect1.width() <= 0 || rect1.height() <= 0) {
            throw new IllegalArgumentException("Invalid End rectangle");
        } else {
            mStartRect = rect;
            mEndRect = rect1;
            return;
        }
    }

    public Rect getEndRect() {
        return mEndRect;
    }

    void getKenBurnsSettings(Rect rect, Rect rect1) {
        rect.left = getStartRect().left;
        rect.top = getStartRect().top;
        rect.right = getStartRect().right;
        rect.bottom = getStartRect().bottom;
        rect1.left = getEndRect().left;
        rect1.top = getEndRect().top;
        rect1.right = getEndRect().right;
        rect1.bottom = getEndRect().bottom;
    }

    public Rect getStartRect() {
        return mStartRect;
    }

    private Rect mEndRect;
    private Rect mStartRect;
}
