// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.media.videoeditor;

import android.graphics.Bitmap;
import android.graphics.Rect;
import android.media.MediaMetadataRetriever;
import android.os.*;
import android.util.Log;
import android.util.Xml;
import android.view.Surface;
import android.view.SurfaceHolder;
import java.io.*;
import java.util.*;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import org.xmlpull.v1.*;

// Referenced classes of package android.media.videoeditor:
//            VideoEditor, MediaArtistNativeHelper, MediaItem, Transition, 
//            MediaVideoItem, Overlay, EffectKenBurns, MediaImageItem, 
//            AudioTrack, EffectColor, OverlayFrame, TransitionAlpha, 
//            TransitionCrossfade, TransitionSliding, TransitionFadeBlack, Effect

public class VideoEditorImpl
    implements VideoEditor {

    public VideoEditorImpl(String s) throws IOException {
        mMediaItems = new ArrayList();
        mAudioTracks = new ArrayList();
        mTransitions = new ArrayList();
        mPreviewInProgress = false;
        if(SystemProperties.get("libc.debug.malloc").equals("1")) {
            mMallocDebug = true;
            try {
                dumpHeap("HeapAtStart");
            }
            catch(Exception exception1) {
                Log.e("VideoEditorImpl", "dumpHeap returned error in constructor");
            }
        } else {
            mMallocDebug = false;
        }
        mLock = new Semaphore(1, true);
        mMANativeHelper = new MediaArtistNativeHelper(s, mLock, this);
        mProjectPath = s;
        if(!(new File(s, "videoeditor.xml")).exists())
            break MISSING_BLOCK_LABEL_159;
        load();
_L1:
        return;
        Exception exception;
        exception;
        exception.printStackTrace();
        throw new IOException(exception.toString());
        mAspectRatio = 2;
        mDurationMs = 0L;
          goto _L1
    }

    private void computeTimelineDuration() {
        mDurationMs = 0L;
        int i = mMediaItems.size();
        for(int j = 0; j < i; j++) {
            MediaItem mediaitem = (MediaItem)mMediaItems.get(j);
            mDurationMs = mDurationMs + mediaitem.getTimelineDuration();
            if(mediaitem.getEndTransition() != null && j < i - 1)
                mDurationMs = mDurationMs - mediaitem.getEndTransition().getDuration();
        }

    }

    private static void dumpHeap(String s) throws Exception {
        System.gc();
        System.runFinalization();
        Thread.sleep(1000L);
        if("mounted".equals(Environment.getExternalStorageState())) {
            String s1 = Environment.getExternalStorageDirectory().toString();
            if((new File((new StringBuilder()).append(s1).append("/").append(s).append(".dump").toString())).exists())
                (new File((new StringBuilder()).append(s1).append("/").append(s).append(".dump").toString())).delete();
            FileOutputStream fileoutputstream = new FileOutputStream((new StringBuilder()).append(s1).append("/").append(s).append(".dump").toString());
            Debug.dumpNativeHeap(fileoutputstream.getFD());
            fileoutputstream.close();
        }
    }

    private void generateProjectThumbnail() {
        Bitmap bitmap1;
        if((new File((new StringBuilder()).append(mProjectPath).append("/").append("thumbnail.jpg").toString())).exists())
            (new File((new StringBuilder()).append(mProjectPath).append("/").append("thumbnail.jpg").toString())).delete();
        if(mMediaItems.size() <= 0)
            break MISSING_BLOCK_LABEL_278;
        MediaItem mediaitem = (MediaItem)mMediaItems.get(0);
        int i = (480 * mediaitem.getWidth()) / mediaitem.getHeight();
        String s = mediaitem.getFilename();
        FileOutputStream fileoutputstream;
        if(mediaitem instanceof MediaVideoItem) {
            MediaMetadataRetriever mediametadataretriever = new MediaMetadataRetriever();
            mediametadataretriever.setDataSource(s);
            Bitmap bitmap = mediametadataretriever.getFrameAtTime();
            mediametadataretriever.release();
            if(bitmap == null)
                throw new IllegalArgumentException((new StringBuilder()).append("Thumbnail extraction from ").append(s).append(" failed").toString());
            bitmap1 = Bitmap.createScaledBitmap(bitmap, i, 480, true);
        } else {
            Bitmap bitmap2;
            try {
                bitmap2 = mediaitem.getThumbnail(i, 480, 500L);
            }
            catch(IllegalArgumentException illegalargumentexception) {
                throw new IllegalArgumentException((new StringBuilder()).append("Project thumbnail extraction from ").append(s).append(" failed").toString());
            }
            catch(IOException ioexception1) {
                throw new IllegalArgumentException("IO Error creating project thumbnail");
            }
            bitmap1 = bitmap2;
        }
        fileoutputstream = new FileOutputStream((new StringBuilder()).append(mProjectPath).append("/").append("thumbnail.jpg").toString());
        bitmap1.compress(android.graphics.Bitmap.CompressFormat.JPEG, 100, fileoutputstream);
        fileoutputstream.flush();
        fileoutputstream.close();
        bitmap1.recycle();
        return;
        IOException ioexception;
        ioexception;
        throw new IllegalArgumentException("Error creating project thumbnail");
        Exception exception;
        exception;
        bitmap1.recycle();
        throw exception;
    }

    private void load() throws FileNotFoundException, XmlPullParserException, IOException {
        FileInputStream fileinputstream = new FileInputStream(new File(mProjectPath, "videoeditor.xml"));
        ArrayList arraylist;
        XmlPullParser xmlpullparser;
        int i;
        MediaItem mediaitem;
        Overlay overlay;
        boolean flag;
        arraylist = new ArrayList();
        xmlpullparser = Xml.newPullParser();
        xmlpullparser.setInput(fileinputstream, "UTF-8");
        i = xmlpullparser.getEventType();
        mediaitem = null;
        overlay = null;
        flag = false;
          goto _L1
_L4:
        i = xmlpullparser.next();
          goto _L1
_L11:
        String s1 = xmlpullparser.getName();
        if(!"project".equals(s1)) goto _L3; else goto _L2
_L2:
        mAspectRatio = Integer.parseInt(xmlpullparser.getAttributeValue("", "aspect_ratio"));
        boolean flag5 = Boolean.parseBoolean(xmlpullparser.getAttributeValue("", "regeneratePCMFlag"));
        mMANativeHelper.setAudioflag(flag5);
          goto _L4
        Exception exception;
        exception;
        if(fileinputstream != null)
            fileinputstream.close();
        throw exception;
_L3:
        String s3;
        if(!"media_item".equals(s1))
            break MISSING_BLOCK_LABEL_264;
        s3 = xmlpullparser.getAttributeValue("", "id");
        mediaitem = parseMediaItem(xmlpullparser);
        mMediaItems.add(mediaitem);
          goto _L4
        Exception exception5;
        exception5;
        Log.w("VideoEditorImpl", (new StringBuilder()).append("Cannot load media item: ").append(s3).toString(), exception5);
        mediaitem = null;
        if(mMediaItems.size() == 0)
            flag = true;
        arraylist.add(s3);
          goto _L4
        boolean flag1 = "transition".equals(s1);
        if(!flag1)
            break MISSING_BLOCK_LABEL_323;
        Transition transition = parseTransition(xmlpullparser, arraylist);
        if(transition != null)
            mTransitions.add(transition);
          goto _L4
        Exception exception4;
        exception4;
        Log.w("VideoEditorImpl", "Cannot load transition", exception4);
          goto _L4
        boolean flag2 = "overlay".equals(s1);
        if(!flag2)
            break MISSING_BLOCK_LABEL_378;
        if(mediaitem == null) goto _L4; else goto _L5
_L5:
        overlay = parseOverlay(xmlpullparser, mediaitem);
        mediaitem.addOverlay(overlay);
          goto _L4
        Exception exception3;
        exception3;
        Log.w("VideoEditorImpl", "Cannot load overlay", exception3);
          goto _L4
        if(!"overlay_user_attributes".equals(s1))
            break MISSING_BLOCK_LABEL_441;
        if(overlay != null) {
            int j = xmlpullparser.getAttributeCount();
            int k = 0;
            while(k < j)  {
                overlay.setUserAttribute(xmlpullparser.getAttributeName(k), xmlpullparser.getAttributeValue(k));
                k++;
            }
        }
          goto _L4
        boolean flag3 = "effect".equals(s1);
        if(!flag3)
            break MISSING_BLOCK_LABEL_617;
        if(mediaitem == null) goto _L4; else goto _L6
_L6:
        Effect effect = parseEffect(xmlpullparser, mediaitem);
        mediaitem.addEffect(effect);
        if(effect instanceof EffectKenBurns) {
            if(!Boolean.parseBoolean(xmlpullparser.getAttributeValue("", "is_image_clip_generated")))
                break MISSING_BLOCK_LABEL_596;
            String s2 = xmlpullparser.getAttributeValue("", "generated_image_clip");
            File file = new File(s2);
            if(!file.exists())
                break MISSING_BLOCK_LABEL_575;
            ((MediaImageItem)mediaitem).setGeneratedImageClip(s2);
            ((MediaImageItem)mediaitem).setRegenerateClip(false);
        }
          goto _L4
        Exception exception2;
        exception2;
        Log.w("VideoEditorImpl", "Cannot load effect", exception2);
          goto _L4
        ((MediaImageItem)mediaitem).setGeneratedImageClip(null);
        ((MediaImageItem)mediaitem).setRegenerateClip(true);
          goto _L4
        ((MediaImageItem)mediaitem).setGeneratedImageClip(null);
        ((MediaImageItem)mediaitem).setRegenerateClip(true);
          goto _L4
        boolean flag4 = "audio_track".equals(s1);
        if(!flag4) goto _L4; else goto _L7
_L7:
        addAudioTrack(parseAudioTrack(xmlpullparser));
          goto _L4
        Exception exception1;
        exception1;
        Log.w("VideoEditorImpl", "Cannot load audio track", exception1);
          goto _L4
_L12:
        String s = xmlpullparser.getName();
        if("media_item".equals(s))
            mediaitem = null;
        else
        if("overlay".equals(s))
            overlay = null;
          goto _L4
_L9:
        computeTimelineDuration();
        if(flag)
            generateProjectThumbnail();
        if(fileinputstream != null)
            fileinputstream.close();
        return;
_L1:
        if(i == 1) goto _L9; else goto _L8
_L8:
        i;
        JVM INSTR tableswitch 2 3: default 66
    //                   2 78
    //                   3 660;
           goto _L10 _L11 _L12
_L10:
        if(true) goto _L4; else goto _L13
_L13:
    }

    private void lock() throws InterruptedException {
        if(Log.isLoggable("VideoEditorImpl", 3))
            Log.d("VideoEditorImpl", "lock: grabbing semaphore", new Throwable());
        mLock.acquire();
        if(Log.isLoggable("VideoEditorImpl", 3))
            Log.d("VideoEditorImpl", "lock: grabbed semaphore");
    }

    private boolean lock(long l) throws InterruptedException {
        if(Log.isLoggable("VideoEditorImpl", 3))
            Log.d("VideoEditorImpl", (new StringBuilder()).append("lock: grabbing semaphore with timeout ").append(l).toString(), new Throwable());
        boolean flag = mLock.tryAcquire(l, TimeUnit.MILLISECONDS);
        if(Log.isLoggable("VideoEditorImpl", 3))
            Log.d("VideoEditorImpl", (new StringBuilder()).append("lock: grabbed semaphore status ").append(flag).toString());
        return flag;
    }

    private AudioTrack parseAudioTrack(XmlPullParser xmlpullparser) throws IOException {
        String s = xmlpullparser.getAttributeValue("", "id");
        String s1 = xmlpullparser.getAttributeValue("", "filename");
        long l = Long.parseLong(xmlpullparser.getAttributeValue("", "start_time"));
        long l1 = Long.parseLong(xmlpullparser.getAttributeValue("", "begin_time"));
        long l2 = Long.parseLong(xmlpullparser.getAttributeValue("", "end_time"));
        int i = Integer.parseInt(xmlpullparser.getAttributeValue("", "volume"));
        boolean flag = Boolean.parseBoolean(xmlpullparser.getAttributeValue("", "muted"));
        return new AudioTrack(this, s, s1, l, l1, l2, Boolean.parseBoolean(xmlpullparser.getAttributeValue("", "loop")), i, flag, Boolean.parseBoolean(xmlpullparser.getAttributeValue("", "ducking_enabled")), Integer.parseInt(xmlpullparser.getAttributeValue("", "ducking_threshold")), Integer.parseInt(xmlpullparser.getAttributeValue("", "ducking_volume")), xmlpullparser.getAttributeValue("", "waveform"));
    }

    private Effect parseEffect(XmlPullParser xmlpullparser, MediaItem mediaitem) {
        String s = xmlpullparser.getAttributeValue("", "id");
        String s1 = xmlpullparser.getAttributeValue("", "type");
        long l = Long.parseLong(xmlpullparser.getAttributeValue("", "duration"));
        long l1 = Long.parseLong(xmlpullparser.getAttributeValue("", "begin_time"));
        Object obj;
        if(android/media/videoeditor/EffectColor.getSimpleName().equals(s1)) {
            int i = Integer.parseInt(xmlpullparser.getAttributeValue("", "color_type"));
            int j;
            if(i == 1 || i == 2)
                j = Integer.parseInt(xmlpullparser.getAttributeValue("", "color_value"));
            else
                j = 0;
            obj = new EffectColor(mediaitem, s, l1, l, i, j);
        } else
        if(android/media/videoeditor/EffectKenBurns.getSimpleName().equals(s1)) {
            Rect rect = new Rect(Integer.parseInt(xmlpullparser.getAttributeValue("", "start_l")), Integer.parseInt(xmlpullparser.getAttributeValue("", "start_t")), Integer.parseInt(xmlpullparser.getAttributeValue("", "start_r")), Integer.parseInt(xmlpullparser.getAttributeValue("", "start_b")));
            Rect rect1 = new Rect(Integer.parseInt(xmlpullparser.getAttributeValue("", "end_l")), Integer.parseInt(xmlpullparser.getAttributeValue("", "end_t")), Integer.parseInt(xmlpullparser.getAttributeValue("", "end_r")), Integer.parseInt(xmlpullparser.getAttributeValue("", "end_b")));
            obj = new EffectKenBurns(mediaitem, s, rect, rect1, l1, l);
        } else {
            throw new IllegalArgumentException((new StringBuilder()).append("Invalid effect type: ").append(s1).toString());
        }
        return ((Effect) (obj));
    }

    private MediaItem parseMediaItem(XmlPullParser xmlpullparser) throws IOException {
        String s = xmlpullparser.getAttributeValue("", "id");
        String s1 = xmlpullparser.getAttributeValue("", "type");
        String s2 = xmlpullparser.getAttributeValue("", "filename");
        int i = Integer.parseInt(xmlpullparser.getAttributeValue("", "rendering_mode"));
        Object obj;
        if(android/media/videoeditor/MediaImageItem.getSimpleName().equals(s1))
            obj = new MediaImageItem(this, s, s2, Long.parseLong(xmlpullparser.getAttributeValue("", "duration")), i);
        else
        if(android/media/videoeditor/MediaVideoItem.getSimpleName().equals(s1)) {
            long l = Long.parseLong(xmlpullparser.getAttributeValue("", "begin_time"));
            long l1 = Long.parseLong(xmlpullparser.getAttributeValue("", "end_time"));
            int j = Integer.parseInt(xmlpullparser.getAttributeValue("", "volume"));
            boolean flag = Boolean.parseBoolean(xmlpullparser.getAttributeValue("", "muted"));
            String s3 = xmlpullparser.getAttributeValue("", "waveform");
            obj = new MediaVideoItem(this, s, s2, i, l, l1, j, flag, s3);
            long l2 = Long.parseLong(xmlpullparser.getAttributeValue("", "begin_time"));
            long l3 = Long.parseLong(xmlpullparser.getAttributeValue("", "end_time"));
            ((MediaVideoItem)obj).setExtractBoundaries(l2, l3);
            int k = Integer.parseInt(xmlpullparser.getAttributeValue("", "volume"));
            ((MediaVideoItem)obj).setVolume(k);
        } else {
            throw new IllegalArgumentException((new StringBuilder()).append("Unknown media item type: ").append(s1).toString());
        }
        return ((MediaItem) (obj));
    }

    private Overlay parseOverlay(XmlPullParser xmlpullparser, MediaItem mediaitem) {
        String s = xmlpullparser.getAttributeValue("", "id");
        String s1 = xmlpullparser.getAttributeValue("", "type");
        long l = Long.parseLong(xmlpullparser.getAttributeValue("", "duration"));
        long l1 = Long.parseLong(xmlpullparser.getAttributeValue("", "begin_time"));
        if(android/media/videoeditor/OverlayFrame.getSimpleName().equals(s1)) {
            OverlayFrame overlayframe = new OverlayFrame(mediaitem, s, xmlpullparser.getAttributeValue("", "filename"), l1, l);
            String s2 = xmlpullparser.getAttributeValue("", "overlay_rgb_filename");
            if(s2 != null) {
                ((OverlayFrame)overlayframe).setFilename(s2);
                int i = Integer.parseInt(xmlpullparser.getAttributeValue("", "overlay_frame_width"));
                int j = Integer.parseInt(xmlpullparser.getAttributeValue("", "overlay_frame_height"));
                ((OverlayFrame)overlayframe).setOverlayFrameWidth(i);
                ((OverlayFrame)overlayframe).setOverlayFrameHeight(j);
                int k = Integer.parseInt(xmlpullparser.getAttributeValue("", "resized_RGBframe_width"));
                int i1 = Integer.parseInt(xmlpullparser.getAttributeValue("", "resized_RGBframe_height"));
                ((OverlayFrame)overlayframe).setResizedRGBSize(k, i1);
            }
            return overlayframe;
        } else {
            throw new IllegalArgumentException((new StringBuilder()).append("Invalid overlay type: ").append(s1).toString());
        }
    }

    private Transition parseTransition(XmlPullParser xmlpullparser, List list) {
        String s;
        String s1;
        long l;
        int i;
        String s2;
        s = xmlpullparser.getAttributeValue("", "id");
        s1 = xmlpullparser.getAttributeValue("", "type");
        l = Long.parseLong(xmlpullparser.getAttributeValue("", "duration"));
        i = Integer.parseInt(xmlpullparser.getAttributeValue("", "behavior"));
        s2 = xmlpullparser.getAttributeValue("", "before_media_item");
        if(s2 == null) goto _L2; else goto _L1
_L1:
        if(!list.contains(s2)) goto _L4; else goto _L3
_L3:
        Object obj = null;
_L6:
        return ((Transition) (obj));
_L4:
        MediaItem mediaitem;
        mediaitem = getMediaItem(s2);
        break MISSING_BLOCK_LABEL_100;
_L2:
        mediaitem = null;
        String s3 = xmlpullparser.getAttributeValue("", "after_media_item");
        MediaItem mediaitem1;
        if(s3 != null) {
            if(list.contains(s3)) {
                obj = null;
                continue; /* Loop/switch isn't completed */
            }
            mediaitem1 = getMediaItem(s3);
        } else {
            mediaitem1 = null;
        }
        if(android/media/videoeditor/TransitionAlpha.getSimpleName().equals(s1)) {
            int k = Integer.parseInt(xmlpullparser.getAttributeValue("", "blending"));
            obj = new TransitionAlpha(s, mediaitem1, mediaitem, l, i, xmlpullparser.getAttributeValue("", "mask"), k, Boolean.getBoolean(xmlpullparser.getAttributeValue("", "invert")));
        } else
        if(android/media/videoeditor/TransitionCrossfade.getSimpleName().equals(s1))
            obj = new TransitionCrossfade(s, mediaitem1, mediaitem, l, i);
        else
        if(android/media/videoeditor/TransitionSliding.getSimpleName().equals(s1)) {
            int j = Integer.parseInt(xmlpullparser.getAttributeValue("", "direction"));
            obj = new TransitionSliding(s, mediaitem1, mediaitem, l, i, j);
        } else {
label0:
            {
                if(!android/media/videoeditor/TransitionFadeBlack.getSimpleName().equals(s1))
                    break label0;
                obj = new TransitionFadeBlack(s, mediaitem1, mediaitem, l, i);
            }
        }
        if(Boolean.parseBoolean(xmlpullparser.getAttributeValue("", "is_transition_generated"))) {
            String s4 = xmlpullparser.getAttributeValue("", "generated_transition_clip");
            if((new File(s4)).exists())
                ((Transition) (obj)).setFilename(s4);
            else
                ((Transition) (obj)).setFilename(null);
        }
        if(mediaitem != null)
            mediaitem.setBeginTransition(((Transition) (obj)));
        if(mediaitem1 != null)
            mediaitem1.setEndTransition(((Transition) (obj)));
        continue; /* Loop/switch isn't completed */
        throw new IllegalArgumentException((new StringBuilder()).append("Invalid transition type: ").append(s1).toString());
        if(true) goto _L6; else goto _L5
_L5:
    }

    private void removeAdjacentTransitions(MediaItem mediaitem) {
        Transition transition = mediaitem.getBeginTransition();
        if(transition != null) {
            if(transition.getAfterMediaItem() != null)
                transition.getAfterMediaItem().setEndTransition(null);
            transition.invalidate();
            mTransitions.remove(transition);
        }
        Transition transition1 = mediaitem.getEndTransition();
        if(transition1 != null) {
            if(transition1.getBeforeMediaItem() != null)
                transition1.getBeforeMediaItem().setBeginTransition(null);
            transition1.invalidate();
            mTransitions.remove(transition1);
        }
        mediaitem.setBeginTransition(null);
        mediaitem.setEndTransition(null);
    }

    /**
     * @deprecated Method removeMediaItem is deprecated
     */

    private MediaItem removeMediaItem(String s, boolean flag) {
        this;
        JVM INSTR monitorenter ;
        MediaItem mediaitem;
        String s1 = ((MediaItem)mMediaItems.get(0)).getId();
        mediaitem = getMediaItem(s);
        if(mediaitem != null) {
            mMANativeHelper.setGeneratePreview(true);
            mMediaItems.remove(mediaitem);
            removeAdjacentTransitions(mediaitem);
            computeTimelineDuration();
        }
        if(s1.equals(s))
            generateProjectThumbnail();
        this;
        JVM INSTR monitorexit ;
        return mediaitem;
        Exception exception;
        exception;
        throw exception;
    }

    private void removeTransitionAfter(int i) {
        MediaItem mediaitem = (MediaItem)mMediaItems.get(i);
        Iterator iterator = mTransitions.iterator();
        do {
            if(!iterator.hasNext())
                break;
            Transition transition = (Transition)iterator.next();
            if(transition.getAfterMediaItem() != mediaitem)
                continue;
            mMANativeHelper.setGeneratePreview(true);
            iterator.remove();
            transition.invalidate();
            mediaitem.setEndTransition(null);
            if(i < -1 + mMediaItems.size())
                ((MediaItem)mMediaItems.get(i + 1)).setBeginTransition(null);
            break;
        } while(true);
    }

    private void removeTransitionBefore(int i) {
        MediaItem mediaitem = (MediaItem)mMediaItems.get(i);
        Iterator iterator = mTransitions.iterator();
        do {
            if(!iterator.hasNext())
                break;
            Transition transition = (Transition)iterator.next();
            if(transition.getBeforeMediaItem() != mediaitem)
                continue;
            mMANativeHelper.setGeneratePreview(true);
            iterator.remove();
            transition.invalidate();
            mediaitem.setBeginTransition(null);
            if(i > 0)
                ((MediaItem)mMediaItems.get(i - 1)).setEndTransition(null);
            break;
        } while(true);
    }

    private void unlock() {
        if(Log.isLoggable("VideoEditorImpl", 3))
            Log.d("VideoEditorImpl", "unlock: releasing semaphore");
        mLock.release();
    }

    /**
     * @deprecated Method addAudioTrack is deprecated
     */

    public void addAudioTrack(AudioTrack audiotrack) {
        this;
        JVM INSTR monitorenter ;
        if(audiotrack != null)
            break MISSING_BLOCK_LABEL_22;
        throw new IllegalArgumentException("Audio Track is null");
        Exception exception;
        exception;
        this;
        JVM INSTR monitorexit ;
        throw exception;
        if(mAudioTracks.size() == 1)
            throw new IllegalArgumentException("No more tracks can be added");
        mMANativeHelper.setGeneratePreview(true);
        mAudioTracks.add(audiotrack);
        if((new File(String.format((new StringBuilder()).append(mProjectPath).append("/").append("AudioPcm").append(audiotrack.getId()).append(".pcm").toString(), new Object[0]))).exists())
            mMANativeHelper.setAudioflag(false);
        this;
        JVM INSTR monitorexit ;
    }

    /**
     * @deprecated Method addMediaItem is deprecated
     */

    public void addMediaItem(MediaItem mediaitem) {
        this;
        JVM INSTR monitorenter ;
        if(mediaitem != null)
            break MISSING_BLOCK_LABEL_24;
        throw new IllegalArgumentException("Media item is null");
        Exception exception;
        exception;
        this;
        JVM INSTR monitorexit ;
        throw exception;
        if(mMediaItems.contains(mediaitem))
            throw new IllegalArgumentException((new StringBuilder()).append("Media item already exists: ").append(mediaitem.getId()).toString());
        mMANativeHelper.setGeneratePreview(true);
        int i = mMediaItems.size();
        if(i > 0)
            removeTransitionAfter(i - 1);
        mMediaItems.add(mediaitem);
        computeTimelineDuration();
        if(mMediaItems.size() == 1)
            generateProjectThumbnail();
        this;
        JVM INSTR monitorexit ;
    }

    /**
     * @deprecated Method addTransition is deprecated
     */

    public void addTransition(Transition transition) {
        this;
        JVM INSTR monitorenter ;
        if(transition != null)
            break MISSING_BLOCK_LABEL_24;
        throw new IllegalArgumentException("Null Transition");
        Exception exception;
        exception;
        this;
        JVM INSTR monitorexit ;
        throw exception;
        MediaItem mediaitem = transition.getBeforeMediaItem();
        MediaItem mediaitem1 = transition.getAfterMediaItem();
        if(mMediaItems == null)
            throw new IllegalArgumentException("No media items are added");
        if(mediaitem1 != null && mediaitem != null) {
            int i = mMediaItems.indexOf(mediaitem1);
            int j = mMediaItems.indexOf(mediaitem);
            if(i == -1 || j == -1)
                throw new IllegalArgumentException("Either of the mediaItem is not found in the list");
            if(i != j - 1)
                throw new IllegalArgumentException("MediaItems are not in sequence");
        }
        mMANativeHelper.setGeneratePreview(true);
        mTransitions.add(transition);
        if(mediaitem1 != null) {
            if(mediaitem1.getEndTransition() != null) {
                mediaitem1.getEndTransition().invalidate();
                mTransitions.remove(mediaitem1.getEndTransition());
            }
            mediaitem1.setEndTransition(transition);
        }
        if(mediaitem != null) {
            if(mediaitem.getBeginTransition() != null) {
                mediaitem.getBeginTransition().invalidate();
                mTransitions.remove(mediaitem.getBeginTransition());
            }
            mediaitem.setBeginTransition(transition);
        }
        computeTimelineDuration();
        this;
        JVM INSTR monitorexit ;
    }

    public void cancelExport(String s) {
        if(mMANativeHelper != null && s != null)
            mMANativeHelper.stop(s);
    }

    public void clearSurface(SurfaceHolder surfaceholder) {
        if(surfaceholder == null)
            throw new IllegalArgumentException("Invalid surface holder");
        Surface surface = surfaceholder.getSurface();
        if(surface == null)
            throw new IllegalArgumentException("Surface could not be retrieved from surface holder");
        if(!surface.isValid())
            throw new IllegalStateException("Surface is not valid");
        if(mMANativeHelper != null)
            mMANativeHelper.clearPreviewSurface(surface);
        else
            Log.w("VideoEditorImpl", "Native helper was not ready!");
    }

    public void export(String s, int i, int j, int k, int l, VideoEditor.ExportProgressListener exportprogresslistener) throws IOException {
        if(s == null)
            throw new IllegalArgumentException("export: filename is null");
        File file = new File(s);
        if(file == null)
            throw new IOException((new StringBuilder()).append(s).append("can not be created").toString());
        if(mMediaItems.size() == 0)
            throw new IllegalStateException("No MediaItems added");
        switch(i) {
        default:
            throw new IllegalArgumentException((new StringBuilder()).append("Unsupported height value ").append(i).toString());

        case 144: 
        case 288: 
        case 360: 
        case 480: 
        case 720: 
        case 1080: 
            switch(j) {
            default:
                throw new IllegalArgumentException((new StringBuilder()).append("Unsupported bitrate value ").append(j).toString());

            case 28000: 
            case 40000: 
            case 64000: 
            case 96000: 
            case 128000: 
            case 192000: 
            case 256000: 
            case 384000: 
            case 512000: 
            case 800000: 
            case 2000000: 
            case 5000000: 
            case 8000000: 
                computeTimelineDuration();
                break;
            }
            break;
        }
        if(0x80000000L <= (mDurationMs * (0x17700L + (long)j)) / 8000L)
            throw new IllegalStateException("Export Size is more than 2GB");
        k;
        JVM INSTR tableswitch 1 2: default 372
    //                   1 461
    //                   2 401;
           goto _L1 _L2 _L3
_L1:
        throw new IllegalArgumentException((new StringBuilder()).append("Unsupported audio codec type ").append(k).toString());
_L3:
        byte byte0 = 2;
_L8:
        l;
        JVM INSTR tableswitch 1 3: default 432
    //                   1 467
    //                   2 519
    //                   3 525;
           goto _L4 _L5 _L6 _L7
_L4:
        throw new IllegalArgumentException((new StringBuilder()).append("Unsupported video codec type ").append(l).toString());
_L2:
        byte0 = 1;
          goto _L8
_L5:
        byte byte1 = 1;
_L13:
        boolean flag = false;
        lock();
        flag = true;
        if(mMANativeHelper == null)
            throw new IllegalStateException("The video editor is not initialized");
          goto _L9
        InterruptedException interruptedexception;
        interruptedexception;
        Log.e("VideoEditorImpl", "Sem acquire NOT successful in export");
        if(flag)
            unlock();
_L11:
        return;
_L6:
        byte1 = 2;
        continue; /* Loop/switch isn't completed */
_L7:
        byte1 = 3;
        continue; /* Loop/switch isn't completed */
_L9:
        mMANativeHelper.setAudioCodec(byte0);
        mMANativeHelper.setVideoCodec(byte1);
        mMANativeHelper.export(s, mProjectPath, i, j, mMediaItems, mTransitions, mAudioTracks, exportprogresslistener);
        if(flag)
            unlock();
        if(true) goto _L11; else goto _L10
_L10:
        Exception exception;
        exception;
        if(flag)
            unlock();
        throw exception;
        if(true) goto _L13; else goto _L12
_L12:
    }

    public void export(String s, int i, int j, VideoEditor.ExportProgressListener exportprogresslistener) throws IOException {
        export(s, i, j, 2, 2, exportprogresslistener);
    }

    public void generatePreview(VideoEditor.MediaProcessingProgressListener mediaprocessingprogresslistener) {
        boolean flag = false;
        lock();
        flag = true;
        if(mMANativeHelper == null)
            throw new IllegalStateException("The video editor is not initialized");
          goto _L1
        InterruptedException interruptedexception;
        interruptedexception;
        Log.e("VideoEditorImpl", "Sem acquire NOT successful in previewStoryBoard");
        if(flag)
            unlock();
_L3:
        return;
_L1:
        if(mMediaItems.size() > 0 || mAudioTracks.size() > 0)
            mMANativeHelper.previewStoryBoard(mMediaItems, mTransitions, mAudioTracks, mediaprocessingprogresslistener);
        if(flag)
            unlock();
        if(true) goto _L3; else goto _L2
_L2:
        Exception exception;
        exception;
        if(flag)
            unlock();
        throw exception;
    }

    public List getAllAudioTracks() {
        return mAudioTracks;
    }

    public List getAllMediaItems() {
        return mMediaItems;
    }

    public List getAllTransitions() {
        return mTransitions;
    }

    public int getAspectRatio() {
        return mAspectRatio;
    }

    public AudioTrack getAudioTrack(String s) {
        Iterator iterator = mAudioTracks.iterator();
_L4:
        if(!iterator.hasNext()) goto _L2; else goto _L1
_L1:
        AudioTrack audiotrack = (AudioTrack)iterator.next();
        if(!audiotrack.getId().equals(s)) goto _L4; else goto _L3
_L3:
        return audiotrack;
_L2:
        audiotrack = null;
        if(true) goto _L3; else goto _L5
_L5:
    }

    public long getDuration() {
        computeTimelineDuration();
        return mDurationMs;
    }

    /**
     * @deprecated Method getMediaItem is deprecated
     */

    public MediaItem getMediaItem(String s) {
        this;
        JVM INSTR monitorenter ;
        Iterator iterator = mMediaItems.iterator();
_L4:
        if(!iterator.hasNext()) goto _L2; else goto _L1
_L1:
        MediaItem mediaitem;
        boolean flag;
        mediaitem = (MediaItem)iterator.next();
        flag = mediaitem.getId().equals(s);
        if(!flag) goto _L4; else goto _L3
_L3:
        this;
        JVM INSTR monitorexit ;
        return mediaitem;
_L2:
        mediaitem = null;
        if(true) goto _L3; else goto _L5
_L5:
        Exception exception;
        exception;
        throw exception;
    }

    MediaArtistNativeHelper getNativeContext() {
        return mMANativeHelper;
    }

    public String getPath() {
        return mProjectPath;
    }

    public Transition getTransition(String s) {
        Iterator iterator = mTransitions.iterator();
_L4:
        if(!iterator.hasNext()) goto _L2; else goto _L1
_L1:
        Transition transition = (Transition)iterator.next();
        if(!transition.getId().equals(s)) goto _L4; else goto _L3
_L3:
        return transition;
_L2:
        transition = null;
        if(true) goto _L3; else goto _L5
_L5:
    }

    /**
     * @deprecated Method insertAudioTrack is deprecated
     */

    public void insertAudioTrack(AudioTrack audiotrack, String s) {
        this;
        JVM INSTR monitorenter ;
        if(mAudioTracks.size() == 1)
            throw new IllegalArgumentException("No more tracks can be added");
        break MISSING_BLOCK_LABEL_31;
        Exception exception;
        exception;
        this;
        JVM INSTR monitorexit ;
        throw exception;
        if(s != null) goto _L2; else goto _L1
_L1:
        mMANativeHelper.setGeneratePreview(true);
        mAudioTracks.add(0, audiotrack);
_L3:
        this;
        JVM INSTR monitorexit ;
        return;
_L2:
        int i;
        int j;
        i = mAudioTracks.size();
        j = 0;
_L4:
        if(j < i) {
            if(!((AudioTrack)mAudioTracks.get(j)).getId().equals(s))
                break MISSING_BLOCK_LABEL_155;
            mMANativeHelper.setGeneratePreview(true);
            mAudioTracks.add(j + 1, audiotrack);
        } else {
            throw new IllegalArgumentException((new StringBuilder()).append("AudioTrack not found: ").append(s).toString());
        }
          goto _L3
        j++;
          goto _L4
    }

    /**
     * @deprecated Method insertMediaItem is deprecated
     */

    public void insertMediaItem(MediaItem mediaitem, String s) {
        this;
        JVM INSTR monitorenter ;
        if(mMediaItems.contains(mediaitem))
            throw new IllegalArgumentException((new StringBuilder()).append("Media item already exists: ").append(mediaitem.getId()).toString());
        break MISSING_BLOCK_LABEL_51;
        Exception exception;
        exception;
        this;
        JVM INSTR monitorexit ;
        throw exception;
        if(s != null) goto _L2; else goto _L1
_L1:
        mMANativeHelper.setGeneratePreview(true);
        if(mMediaItems.size() > 0)
            removeTransitionBefore(0);
        mMediaItems.add(0, mediaitem);
        computeTimelineDuration();
        generateProjectThumbnail();
_L3:
        this;
        JVM INSTR monitorexit ;
        return;
_L2:
        int i;
        int j;
        i = mMediaItems.size();
        j = 0;
_L4:
        if(j < i) {
            if(!((MediaItem)mMediaItems.get(j)).getId().equals(s))
                break MISSING_BLOCK_LABEL_210;
            mMANativeHelper.setGeneratePreview(true);
            removeTransitionAfter(j);
            mMediaItems.add(j + 1, mediaitem);
            computeTimelineDuration();
        } else {
            throw new IllegalArgumentException((new StringBuilder()).append("MediaItem not found: ").append(s).toString());
        }
          goto _L3
        j++;
          goto _L4
    }

    /**
     * @deprecated Method moveAudioTrack is deprecated
     */

    public void moveAudioTrack(String s, String s1) {
        this;
        JVM INSTR monitorenter ;
        throw new IllegalStateException("Not supported");
        Exception exception;
        exception;
        this;
        JVM INSTR monitorexit ;
        throw exception;
    }

    /**
     * @deprecated Method moveMediaItem is deprecated
     */

    public void moveMediaItem(String s, String s1) {
        this;
        JVM INSTR monitorenter ;
        MediaItem mediaitem;
        mediaitem = removeMediaItem(s, true);
        if(mediaitem == null)
            throw new IllegalArgumentException((new StringBuilder()).append("Target MediaItem not found: ").append(s).toString());
        break MISSING_BLOCK_LABEL_48;
        Exception exception;
        exception;
        this;
        JVM INSTR monitorexit ;
        throw exception;
        if(s1 != null) goto _L2; else goto _L1
_L1:
        if(mMediaItems.size() <= 0) goto _L4; else goto _L3
_L3:
        mMANativeHelper.setGeneratePreview(true);
        removeTransitionBefore(0);
        mMediaItems.add(0, mediaitem);
        computeTimelineDuration();
        generateProjectThumbnail();
_L5:
        this;
        JVM INSTR monitorexit ;
        return;
_L4:
        throw new IllegalStateException("Cannot move media item (it is the only item)");
_L2:
        int i;
        int j;
        i = mMediaItems.size();
        j = 0;
_L6:
        if(j < i) {
            if(!((MediaItem)mMediaItems.get(j)).getId().equals(s1))
                break MISSING_BLOCK_LABEL_220;
            mMANativeHelper.setGeneratePreview(true);
            removeTransitionAfter(j);
            mMediaItems.add(j + 1, mediaitem);
            computeTimelineDuration();
        } else {
            throw new IllegalArgumentException((new StringBuilder()).append("MediaItem not found: ").append(s1).toString());
        }
          goto _L5
        j++;
          goto _L6
    }

    public void release() {
        boolean flag;
        stopPreview();
        flag = false;
        lock();
        flag = true;
        if(mMANativeHelper != null) {
            mMediaItems.clear();
            mAudioTracks.clear();
            mTransitions.clear();
            mMANativeHelper.releaseNativeHelper();
            mMANativeHelper = null;
        }
        if(flag)
            unlock();
_L1:
        if(!mMallocDebug)
            break MISSING_BLOCK_LABEL_80;
        dumpHeap("HeapAtEnd");
_L2:
        return;
        Exception exception1;
        exception1;
        Log.e("VideoEditorImpl", "Sem acquire NOT successful in export", exception1);
        if(flag)
            unlock();
          goto _L1
        Exception exception;
        exception;
        if(flag)
            unlock();
        throw exception;
        Exception exception2;
        exception2;
        Log.e("VideoEditorImpl", "dumpHeap returned error in release");
          goto _L2
    }

    /**
     * @deprecated Method removeAllMediaItems is deprecated
     */

    public void removeAllMediaItems() {
        this;
        JVM INSTR monitorenter ;
        mMANativeHelper.setGeneratePreview(true);
        mMediaItems.clear();
        for(Iterator iterator = mTransitions.iterator(); iterator.hasNext(); ((Transition)iterator.next()).invalidate());
        break MISSING_BLOCK_LABEL_58;
        Exception exception;
        exception;
        throw exception;
        mTransitions.clear();
        mDurationMs = 0L;
        if((new File((new StringBuilder()).append(mProjectPath).append("/").append("thumbnail.jpg").toString())).exists())
            (new File((new StringBuilder()).append(mProjectPath).append("/").append("thumbnail.jpg").toString())).delete();
        this;
        JVM INSTR monitorexit ;
    }

    /**
     * @deprecated Method removeAudioTrack is deprecated
     */

    public AudioTrack removeAudioTrack(String s) {
        this;
        JVM INSTR monitorenter ;
        AudioTrack audiotrack;
        audiotrack = getAudioTrack(s);
        if(audiotrack == null)
            break MISSING_BLOCK_LABEL_54;
        mMANativeHelper.setGeneratePreview(true);
        mAudioTracks.remove(audiotrack);
        audiotrack.invalidate();
        mMANativeHelper.invalidatePcmFile();
        mMANativeHelper.setAudioflag(true);
        this;
        JVM INSTR monitorexit ;
        return audiotrack;
        throw new IllegalArgumentException(" No more audio tracks");
        Exception exception;
        exception;
        this;
        JVM INSTR monitorexit ;
        throw exception;
    }

    /**
     * @deprecated Method removeMediaItem is deprecated
     */

    public MediaItem removeMediaItem(String s) {
        this;
        JVM INSTR monitorenter ;
        String s1;
        MediaItem mediaitem;
        s1 = ((MediaItem)mMediaItems.get(0)).getId();
        mediaitem = getMediaItem(s);
        if(mediaitem == null)
            break MISSING_BLOCK_LABEL_149;
        mMANativeHelper.setGeneratePreview(true);
        mMediaItems.remove(mediaitem);
        if(mediaitem instanceof MediaImageItem)
            ((MediaImageItem)mediaitem).invalidate();
        List list = mediaitem.getAllOverlays();
        if(list.size() > 0) {
            Iterator iterator = list.iterator();
            do {
                if(!iterator.hasNext())
                    break;
                Overlay overlay = (Overlay)iterator.next();
                if(overlay instanceof OverlayFrame)
                    ((OverlayFrame)overlay).invalidate();
            } while(true);
        }
        break MISSING_BLOCK_LABEL_139;
        Exception exception;
        exception;
        throw exception;
        removeAdjacentTransitions(mediaitem);
        computeTimelineDuration();
        if(s1.equals(s))
            generateProjectThumbnail();
        if(mediaitem instanceof MediaVideoItem)
            ((MediaVideoItem)mediaitem).invalidate();
        this;
        JVM INSTR monitorexit ;
        return mediaitem;
    }

    /**
     * @deprecated Method removeTransition is deprecated
     */

    public Transition removeTransition(String s) {
        this;
        JVM INSTR monitorenter ;
        Transition transition;
        transition = getTransition(s);
        if(transition == null)
            throw new IllegalStateException((new StringBuilder()).append("Transition not found: ").append(s).toString());
        break MISSING_BLOCK_LABEL_45;
        Exception exception;
        exception;
        this;
        JVM INSTR monitorexit ;
        throw exception;
        mMANativeHelper.setGeneratePreview(true);
        MediaItem mediaitem = transition.getAfterMediaItem();
        if(mediaitem != null)
            mediaitem.setEndTransition(null);
        MediaItem mediaitem1 = transition.getBeforeMediaItem();
        if(mediaitem1 != null)
            mediaitem1.setBeginTransition(null);
        mTransitions.remove(transition);
        transition.invalidate();
        computeTimelineDuration();
        this;
        JVM INSTR monitorexit ;
        return transition;
    }

    public long renderPreviewFrame(SurfaceHolder surfaceholder, long l, VideoEditor.OverlayData overlaydata) {
        Surface surface;
        boolean flag;
label0:
        {
            if(surfaceholder == null)
                throw new IllegalArgumentException("Surface Holder is null");
            surface = surfaceholder.getSurface();
            if(surface == null)
                throw new IllegalArgumentException("Surface could not be retrieved from Surface holder");
            if(!surface.isValid())
                throw new IllegalStateException("Surface is not valid");
            if(l < 0L)
                throw new IllegalArgumentException("requested time not correct");
            if(l > mDurationMs)
                throw new IllegalArgumentException("requested time more than duration");
            flag = false;
            try {
                flag = lock(500L);
                if(!flag)
                    throw new IllegalStateException("Timeout waiting for semaphore");
                break label0;
            }
            catch(InterruptedException interruptedexception) { }
            finally {
                if(flag)
                    unlock();
                throw exception;
            }
        }
        Log.w("VideoEditorImpl", "The thread was interrupted", new Throwable());
        throw new IllegalStateException("The thread was interrupted");
        if(mMANativeHelper == null)
            throw new IllegalStateException("The video editor is not initialized");
        if(mMediaItems.size() <= 0) goto _L2; else goto _L1
_L1:
        long l2;
        Rect rect = surfaceholder.getSurfaceFrame();
        l2 = mMANativeHelper.renderPreviewFrame(surface, l, rect.width(), rect.height(), overlaydata);
        long l1 = l2;
_L4:
        if(flag)
            unlock();
        return l1;
_L2:
        l1 = 0L;
        if(true) goto _L4; else goto _L3
_L3:
    }

    public void save() throws IOException {
        XmlSerializer xmlserializer = Xml.newSerializer();
        StringWriter stringwriter = new StringWriter();
        xmlserializer.setOutput(stringwriter);
        xmlserializer.startDocument("UTF-8", Boolean.valueOf(true));
        xmlserializer.startTag("", "project");
        xmlserializer.attribute("", "aspect_ratio", Integer.toString(mAspectRatio));
        xmlserializer.attribute("", "regeneratePCMFlag", Boolean.toString(mMANativeHelper.getAudioflag()));
        xmlserializer.startTag("", "media_items");
        for(Iterator iterator = mMediaItems.iterator(); iterator.hasNext(); xmlserializer.endTag("", "media_item")) {
            MediaItem mediaitem2 = (MediaItem)iterator.next();
            xmlserializer.startTag("", "media_item");
            xmlserializer.attribute("", "id", mediaitem2.getId());
            xmlserializer.attribute("", "type", mediaitem2.getClass().getSimpleName());
            xmlserializer.attribute("", "filename", mediaitem2.getFilename());
            xmlserializer.attribute("", "rendering_mode", Integer.toString(mediaitem2.getRenderingMode()));
            List list;
            if(mediaitem2 instanceof MediaVideoItem) {
                MediaVideoItem mediavideoitem = (MediaVideoItem)mediaitem2;
                xmlserializer.attribute("", "begin_time", Long.toString(mediavideoitem.getBoundaryBeginTime()));
                xmlserializer.attribute("", "end_time", Long.toString(mediavideoitem.getBoundaryEndTime()));
                xmlserializer.attribute("", "volume", Integer.toString(mediavideoitem.getVolume()));
                xmlserializer.attribute("", "muted", Boolean.toString(mediavideoitem.isMuted()));
                if(mediavideoitem.getAudioWaveformFilename() != null)
                    xmlserializer.attribute("", "waveform", mediavideoitem.getAudioWaveformFilename());
            } else
            if(mediaitem2 instanceof MediaImageItem)
                xmlserializer.attribute("", "duration", Long.toString(mediaitem2.getTimelineDuration()));
            list = mediaitem2.getAllOverlays();
            if(list.size() > 0) {
                xmlserializer.startTag("", "overlays");
                for(Iterator iterator4 = list.iterator(); iterator4.hasNext(); xmlserializer.endTag("", "overlay")) {
                    Overlay overlay = (Overlay)iterator4.next();
                    xmlserializer.startTag("", "overlay");
                    xmlserializer.attribute("", "id", overlay.getId());
                    xmlserializer.attribute("", "type", overlay.getClass().getSimpleName());
                    xmlserializer.attribute("", "begin_time", Long.toString(overlay.getStartTime()));
                    xmlserializer.attribute("", "duration", Long.toString(overlay.getDuration()));
                    if(overlay instanceof OverlayFrame) {
                        OverlayFrame overlayframe = (OverlayFrame)overlay;
                        overlayframe.save(getPath());
                        if(overlayframe.getBitmapImageFileName() != null)
                            xmlserializer.attribute("", "filename", overlayframe.getBitmapImageFileName());
                        if(overlayframe.getFilename() != null) {
                            xmlserializer.attribute("", "overlay_rgb_filename", overlayframe.getFilename());
                            xmlserializer.attribute("", "overlay_frame_width", Integer.toString(overlayframe.getOverlayFrameWidth()));
                            xmlserializer.attribute("", "overlay_frame_height", Integer.toString(overlayframe.getOverlayFrameHeight()));
                            xmlserializer.attribute("", "resized_RGBframe_width", Integer.toString(overlayframe.getResizedRGBSizeWidth()));
                            xmlserializer.attribute("", "resized_RGBframe_height", Integer.toString(overlayframe.getResizedRGBSizeHeight()));
                        }
                    }
                    xmlserializer.startTag("", "overlay_user_attributes");
                    Map map = overlay.getUserAttributes();
                    for(Iterator iterator5 = map.keySet().iterator(); iterator5.hasNext();) {
                        String s = (String)iterator5.next();
                        String s1 = (String)map.get(s);
                        if(s1 != null)
                            xmlserializer.attribute("", s, s1);
                    }

                    xmlserializer.endTag("", "overlay_user_attributes");
                }

                xmlserializer.endTag("", "overlays");
            }
            List list1 = mediaitem2.getAllEffects();
            if(list1.size() <= 0)
                continue;
            xmlserializer.startTag("", "effects");
            Iterator iterator3 = list1.iterator();
            while(iterator3.hasNext())  {
                Effect effect = (Effect)iterator3.next();
                xmlserializer.startTag("", "effect");
                xmlserializer.attribute("", "id", effect.getId());
                xmlserializer.attribute("", "type", effect.getClass().getSimpleName());
                xmlserializer.attribute("", "begin_time", Long.toString(effect.getStartTime()));
                xmlserializer.attribute("", "duration", Long.toString(effect.getDuration()));
                if(effect instanceof EffectColor) {
                    EffectColor effectcolor = (EffectColor)effect;
                    xmlserializer.attribute("", "color_type", Integer.toString(effectcolor.getType()));
                    if(effectcolor.getType() == 1 || effectcolor.getType() == 2)
                        xmlserializer.attribute("", "color_value", Integer.toString(effectcolor.getColor()));
                } else
                if(effect instanceof EffectKenBurns) {
                    Rect rect = ((EffectKenBurns)effect).getStartRect();
                    xmlserializer.attribute("", "start_l", Integer.toString(rect.left));
                    xmlserializer.attribute("", "start_t", Integer.toString(rect.top));
                    xmlserializer.attribute("", "start_r", Integer.toString(rect.right));
                    xmlserializer.attribute("", "start_b", Integer.toString(rect.bottom));
                    Rect rect1 = ((EffectKenBurns)effect).getEndRect();
                    xmlserializer.attribute("", "end_l", Integer.toString(rect1.left));
                    xmlserializer.attribute("", "end_t", Integer.toString(rect1.top));
                    xmlserializer.attribute("", "end_r", Integer.toString(rect1.right));
                    xmlserializer.attribute("", "end_b", Integer.toString(rect1.bottom));
                    MediaItem mediaitem3 = effect.getMediaItem();
                    if(((MediaImageItem)mediaitem3).getGeneratedImageClip() != null) {
                        xmlserializer.attribute("", "is_image_clip_generated", Boolean.toString(true));
                        xmlserializer.attribute("", "generated_image_clip", ((MediaImageItem)mediaitem3).getGeneratedImageClip());
                    } else {
                        xmlserializer.attribute("", "is_image_clip_generated", Boolean.toString(false));
                    }
                }
                xmlserializer.endTag("", "effect");
            }
            xmlserializer.endTag("", "effects");
        }

        xmlserializer.endTag("", "media_items");
        xmlserializer.startTag("", "transitions");
        Iterator iterator1 = mTransitions.iterator();
        while(iterator1.hasNext())  {
            Transition transition = (Transition)iterator1.next();
            xmlserializer.startTag("", "transition");
            xmlserializer.attribute("", "id", transition.getId());
            xmlserializer.attribute("", "type", transition.getClass().getSimpleName());
            xmlserializer.attribute("", "duration", Long.toString(transition.getDuration()));
            xmlserializer.attribute("", "behavior", Integer.toString(transition.getBehavior()));
            xmlserializer.attribute("", "is_transition_generated", Boolean.toString(transition.isGenerated()));
            if(transition.isGenerated())
                xmlserializer.attribute("", "generated_transition_clip", transition.mFilename);
            MediaItem mediaitem = transition.getAfterMediaItem();
            if(mediaitem != null)
                xmlserializer.attribute("", "after_media_item", mediaitem.getId());
            MediaItem mediaitem1 = transition.getBeforeMediaItem();
            if(mediaitem1 != null)
                xmlserializer.attribute("", "before_media_item", mediaitem1.getId());
            if(transition instanceof TransitionSliding)
                xmlserializer.attribute("", "direction", Integer.toString(((TransitionSliding)transition).getDirection()));
            else
            if(transition instanceof TransitionAlpha) {
                TransitionAlpha transitionalpha = (TransitionAlpha)transition;
                xmlserializer.attribute("", "blending", Integer.toString(transitionalpha.getBlendingPercent()));
                xmlserializer.attribute("", "invert", Boolean.toString(transitionalpha.isInvert()));
                if(transitionalpha.getMaskFilename() != null)
                    xmlserializer.attribute("", "mask", transitionalpha.getMaskFilename());
            }
            xmlserializer.endTag("", "transition");
        }
        xmlserializer.endTag("", "transitions");
        xmlserializer.startTag("", "audio_tracks");
        for(Iterator iterator2 = mAudioTracks.iterator(); iterator2.hasNext(); xmlserializer.endTag("", "audio_track")) {
            AudioTrack audiotrack = (AudioTrack)iterator2.next();
            xmlserializer.startTag("", "audio_track");
            xmlserializer.attribute("", "id", audiotrack.getId());
            xmlserializer.attribute("", "filename", audiotrack.getFilename());
            xmlserializer.attribute("", "start_time", Long.toString(audiotrack.getStartTime()));
            xmlserializer.attribute("", "begin_time", Long.toString(audiotrack.getBoundaryBeginTime()));
            xmlserializer.attribute("", "end_time", Long.toString(audiotrack.getBoundaryEndTime()));
            xmlserializer.attribute("", "volume", Integer.toString(audiotrack.getVolume()));
            xmlserializer.attribute("", "ducking_enabled", Boolean.toString(audiotrack.isDuckingEnabled()));
            xmlserializer.attribute("", "ducking_volume", Integer.toString(audiotrack.getDuckedTrackVolume()));
            xmlserializer.attribute("", "ducking_threshold", Integer.toString(audiotrack.getDuckingThreshhold()));
            xmlserializer.attribute("", "muted", Boolean.toString(audiotrack.isMuted()));
            xmlserializer.attribute("", "loop", Boolean.toString(audiotrack.isLooping()));
            if(audiotrack.getAudioWaveformFilename() != null)
                xmlserializer.attribute("", "waveform", audiotrack.getAudioWaveformFilename());
        }

        xmlserializer.endTag("", "audio_tracks");
        xmlserializer.endTag("", "project");
        xmlserializer.endDocument();
        FileOutputStream fileoutputstream = new FileOutputStream(new File(getPath(), "videoeditor.xml"));
        fileoutputstream.write(stringwriter.toString().getBytes());
        fileoutputstream.flush();
        fileoutputstream.close();
    }

    public void setAspectRatio(int i) {
        mAspectRatio = i;
        mMANativeHelper.setGeneratePreview(true);
        for(Iterator iterator = mTransitions.iterator(); iterator.hasNext(); ((Transition)iterator.next()).invalidate());
        for(Iterator iterator1 = mMediaItems.iterator(); iterator1.hasNext();) {
            Iterator iterator2 = ((MediaItem)iterator1.next()).getAllOverlays().iterator();
            while(iterator2.hasNext()) 
                ((OverlayFrame)(Overlay)iterator2.next()).invalidateGeneratedFiles();
        }

    }

    public void startPreview(SurfaceHolder surfaceholder, long l, long l1, boolean flag, int i, 
            VideoEditor.PreviewProgressListener previewprogresslistener) {
        Surface surface;
        if(surfaceholder == null)
            throw new IllegalArgumentException();
        surface = surfaceholder.getSurface();
        if(surface == null)
            throw new IllegalArgumentException("Surface could not be retrieved from surface holder");
        if(!surface.isValid())
            throw new IllegalStateException("Surface is not valid");
        if(previewprogresslistener == null)
            throw new IllegalArgumentException();
        if(l >= mDurationMs)
            throw new IllegalArgumentException("Requested time not correct");
        if(l < 0L)
            throw new IllegalArgumentException("Requested time not correct");
        if(mPreviewInProgress)
            break MISSING_BLOCK_LABEL_236;
        try {
            if(!lock(500L))
                throw new IllegalStateException("Timeout waiting for semaphore");
        }
        catch(InterruptedException interruptedexception) {
            Log.w("VideoEditorImpl", "The thread was interrupted", new Throwable());
            throw new IllegalStateException("The thread was interrupted");
        }
        if(mMANativeHelper == null)
            throw new IllegalStateException("The video editor is not initialized");
        if(mMediaItems.size() > 0) {
            mPreviewInProgress = true;
            mMANativeHelper.previewStoryBoard(mMediaItems, mTransitions, mAudioTracks, null);
            mMANativeHelper.doPreview(surface, l, l1, flag, i, previewprogresslistener);
        }
        return;
        throw new IllegalStateException("Preview already in progress");
    }

    public long stopPreview() {
        if(!mPreviewInProgress) goto _L2; else goto _L1
_L1:
        long l1 = mMANativeHelper.stopPreview();
        long l;
        mPreviewInProgress = false;
        unlock();
        l = l1;
_L4:
        return l;
        Exception exception;
        exception;
        mPreviewInProgress = false;
        unlock();
        throw exception;
_L2:
        l = 0L;
        if(true) goto _L4; else goto _L3
_L3:
    }

    void updateTimelineDuration() {
        computeTimelineDuration();
    }

    private static final String ATTR_AFTER_MEDIA_ITEM_ID = "after_media_item";
    private static final String ATTR_ASPECT_RATIO = "aspect_ratio";
    private static final String ATTR_AUDIO_WAVEFORM_FILENAME = "waveform";
    private static final String ATTR_BEFORE_MEDIA_ITEM_ID = "before_media_item";
    private static final String ATTR_BEGIN_TIME = "begin_time";
    private static final String ATTR_BEHAVIOR = "behavior";
    private static final String ATTR_BLENDING = "blending";
    private static final String ATTR_COLOR_EFFECT_TYPE = "color_type";
    private static final String ATTR_COLOR_EFFECT_VALUE = "color_value";
    private static final String ATTR_DIRECTION = "direction";
    private static final String ATTR_DUCKED_TRACK_VOLUME = "ducking_volume";
    private static final String ATTR_DUCK_ENABLED = "ducking_enabled";
    private static final String ATTR_DUCK_THRESHOLD = "ducking_threshold";
    private static final String ATTR_DURATION = "duration";
    private static final String ATTR_END_RECT_BOTTOM = "end_b";
    private static final String ATTR_END_RECT_LEFT = "end_l";
    private static final String ATTR_END_RECT_RIGHT = "end_r";
    private static final String ATTR_END_RECT_TOP = "end_t";
    private static final String ATTR_END_TIME = "end_time";
    private static final String ATTR_FILENAME = "filename";
    private static final String ATTR_GENERATED_IMAGE_CLIP = "generated_image_clip";
    private static final String ATTR_GENERATED_TRANSITION_CLIP = "generated_transition_clip";
    private static final String ATTR_ID = "id";
    private static final String ATTR_INVERT = "invert";
    private static final String ATTR_IS_IMAGE_CLIP_GENERATED = "is_image_clip_generated";
    private static final String ATTR_IS_TRANSITION_GENERATED = "is_transition_generated";
    private static final String ATTR_LOOP = "loop";
    private static final String ATTR_MASK = "mask";
    private static final String ATTR_MUTED = "muted";
    private static final String ATTR_OVERLAY_FRAME_HEIGHT = "overlay_frame_height";
    private static final String ATTR_OVERLAY_FRAME_WIDTH = "overlay_frame_width";
    private static final String ATTR_OVERLAY_RESIZED_RGB_FRAME_HEIGHT = "resized_RGBframe_height";
    private static final String ATTR_OVERLAY_RESIZED_RGB_FRAME_WIDTH = "resized_RGBframe_width";
    private static final String ATTR_OVERLAY_RGB_FILENAME = "overlay_rgb_filename";
    private static final String ATTR_REGENERATE_PCM = "regeneratePCMFlag";
    private static final String ATTR_RENDERING_MODE = "rendering_mode";
    private static final String ATTR_START_RECT_BOTTOM = "start_b";
    private static final String ATTR_START_RECT_LEFT = "start_l";
    private static final String ATTR_START_RECT_RIGHT = "start_r";
    private static final String ATTR_START_RECT_TOP = "start_t";
    private static final String ATTR_START_TIME = "start_time";
    private static final String ATTR_TYPE = "type";
    private static final String ATTR_VOLUME = "volume";
    private static final int ENGINE_ACCESS_MAX_TIMEOUT_MS = 500;
    private static final String PROJECT_FILENAME = "videoeditor.xml";
    private static final String TAG = "VideoEditorImpl";
    private static final String TAG_AUDIO_TRACK = "audio_track";
    private static final String TAG_AUDIO_TRACKS = "audio_tracks";
    private static final String TAG_EFFECT = "effect";
    private static final String TAG_EFFECTS = "effects";
    private static final String TAG_MEDIA_ITEM = "media_item";
    private static final String TAG_MEDIA_ITEMS = "media_items";
    private static final String TAG_OVERLAY = "overlay";
    private static final String TAG_OVERLAYS = "overlays";
    private static final String TAG_OVERLAY_USER_ATTRIBUTES = "overlay_user_attributes";
    private static final String TAG_PROJECT = "project";
    private static final String TAG_TRANSITION = "transition";
    private static final String TAG_TRANSITIONS = "transitions";
    private int mAspectRatio;
    private final List mAudioTracks;
    private long mDurationMs;
    private final Semaphore mLock;
    private MediaArtistNativeHelper mMANativeHelper;
    private final boolean mMallocDebug;
    private final List mMediaItems;
    private boolean mPreviewInProgress;
    private final String mProjectPath;
    private final List mTransitions;
}
