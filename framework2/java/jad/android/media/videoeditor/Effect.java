// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.media.videoeditor;


// Referenced classes of package android.media.videoeditor:
//            MediaItem, MediaArtistNativeHelper

public abstract class Effect {

    private Effect() {
        mMediaItem = null;
        mUniqueId = null;
        mStartTimeMs = 0L;
        mDurationMs = 0L;
    }

    public Effect(MediaItem mediaitem, String s, long l, long l1) {
        if(mediaitem == null)
            throw new IllegalArgumentException("Media item cannot be null");
        if(l < 0L || l1 < 0L)
            throw new IllegalArgumentException("Invalid start time Or/And Duration");
        if(l + l1 > mediaitem.getDuration()) {
            throw new IllegalArgumentException("Invalid start time and duration");
        } else {
            mMediaItem = mediaitem;
            mUniqueId = s;
            mStartTimeMs = l;
            mDurationMs = l1;
            return;
        }
    }

    public boolean equals(Object obj) {
        boolean flag;
        if(!(obj instanceof Effect))
            flag = false;
        else
            flag = mUniqueId.equals(((Effect)obj).mUniqueId);
        return flag;
    }

    public long getDuration() {
        return mDurationMs;
    }

    public String getId() {
        return mUniqueId;
    }

    public MediaItem getMediaItem() {
        return mMediaItem;
    }

    public long getStartTime() {
        return mStartTimeMs;
    }

    public int hashCode() {
        return mUniqueId.hashCode();
    }

    public void setDuration(long l) {
        if(l < 0L)
            throw new IllegalArgumentException("Invalid duration");
        if(l + mStartTimeMs > mMediaItem.getDuration()) {
            throw new IllegalArgumentException("Duration is too large");
        } else {
            getMediaItem().getNativeContext().setGeneratePreview(true);
            long l1 = mDurationMs;
            mDurationMs = l;
            mMediaItem.invalidateTransitions(mStartTimeMs, l1, mStartTimeMs, mDurationMs);
            return;
        }
    }

    public void setStartTime(long l) {
        if(l + mDurationMs > mMediaItem.getDuration()) {
            throw new IllegalArgumentException("Start time is too large");
        } else {
            getMediaItem().getNativeContext().setGeneratePreview(true);
            long l1 = mStartTimeMs;
            mStartTimeMs = l;
            mMediaItem.invalidateTransitions(l1, mDurationMs, mStartTimeMs, mDurationMs);
            return;
        }
    }

    public void setStartTimeAndDuration(long l, long l1) {
        if(l + l1 > mMediaItem.getDuration()) {
            throw new IllegalArgumentException("Invalid start time or duration");
        } else {
            getMediaItem().getNativeContext().setGeneratePreview(true);
            long l2 = mStartTimeMs;
            long l3 = mDurationMs;
            mStartTimeMs = l;
            mDurationMs = l1;
            mMediaItem.invalidateTransitions(l2, l3, mStartTimeMs, mDurationMs);
            return;
        }
    }

    protected long mDurationMs;
    private final MediaItem mMediaItem;
    protected long mStartTimeMs;
    private final String mUniqueId;
}
