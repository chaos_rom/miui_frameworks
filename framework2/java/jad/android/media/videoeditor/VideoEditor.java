// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.media.videoeditor;

import android.graphics.*;
import android.view.SurfaceHolder;
import java.io.IOException;
import java.util.List;

// Referenced classes of package android.media.videoeditor:
//            AudioTrack, MediaItem, Transition

public interface VideoEditor {
    public static final class OverlayData {

        public boolean needsRendering() {
            boolean flag;
            if(mClear || mOverlayBitmap != null)
                flag = true;
            else
                flag = false;
            return flag;
        }

        public void release() {
            if(mOverlayBitmap != null) {
                mOverlayBitmap.recycle();
                mOverlayBitmap = null;
            }
        }

        public void renderOverlay(Bitmap bitmap) {
            if(!mClear) goto _L2; else goto _L1
_L1:
            bitmap.eraseColor(0);
_L4:
            return;
_L2:
            Canvas canvas;
            Rect rect;
            Rect rect1;
            if(mOverlayBitmap == null)
                continue; /* Loop/switch isn't completed */
            canvas = new Canvas(bitmap);
            switch(mRenderingMode) {
            default:
                throw new IllegalStateException((new StringBuilder()).append("Rendering mode: ").append(mRenderingMode).toString());

            case 1: // '\001'
                break MISSING_BLOCK_LABEL_342;

            case 2: // '\002'
                break; /* Loop/switch isn't completed */

            case 0: // '\0'
                rect1 = new Rect(0, 0, canvas.getWidth(), canvas.getHeight());
                rect = new Rect(0, 0, mOverlayBitmap.getWidth(), mOverlayBitmap.getHeight());
                break;
            }
_L5:
            bitmap.eraseColor(0);
            canvas.drawBitmap(mOverlayBitmap, rect, rect1, sResizePaint);
            mOverlayBitmap.recycle();
            if(true) goto _L4; else goto _L3
_L3:
            int l1;
            int i2;
            int j2;
            int k2;
            if((float)mOverlayBitmap.getWidth() / (float)mOverlayBitmap.getHeight() > (float)canvas.getWidth() / (float)canvas.getHeight()) {
                int l2 = (canvas.getWidth() * mOverlayBitmap.getHeight()) / mOverlayBitmap.getWidth();
                l1 = 0;
                i2 = (canvas.getHeight() - l2) / 2;
                j2 = canvas.getWidth();
                k2 = i2 + l2;
            } else {
                int k1 = (canvas.getHeight() * mOverlayBitmap.getWidth()) / mOverlayBitmap.getHeight();
                l1 = (canvas.getWidth() - k1) / 2;
                i2 = 0;
                j2 = l1 + k1;
                k2 = canvas.getHeight();
            }
            rect1 = new Rect(l1, i2, j2, k2);
            rect = new Rect(0, 0, mOverlayBitmap.getWidth(), mOverlayBitmap.getHeight());
              goto _L5
            int j;
            int k;
            int l;
            int i1;
            if((float)mOverlayBitmap.getWidth() / (float)mOverlayBitmap.getHeight() < (float)canvas.getWidth() / (float)canvas.getHeight()) {
                int j1 = (mOverlayBitmap.getWidth() * canvas.getHeight()) / canvas.getWidth();
                j = 0;
                k = (mOverlayBitmap.getHeight() - j1) / 2;
                l = mOverlayBitmap.getWidth();
                i1 = k + j1;
            } else {
                int i = (mOverlayBitmap.getHeight() * canvas.getWidth()) / canvas.getHeight();
                j = (mOverlayBitmap.getWidth() - i) / 2;
                k = 0;
                l = j + i;
                i1 = mOverlayBitmap.getHeight();
            }
            rect = new Rect(j, k, l, i1);
            rect1 = new Rect(0, 0, canvas.getWidth(), canvas.getHeight());
              goto _L5
        }

        void set(Bitmap bitmap, int i) {
            mOverlayBitmap = bitmap;
            mRenderingMode = i;
            mClear = false;
        }

        void setClear() {
            mClear = true;
        }

        private static final Paint sResizePaint = new Paint(2);
        private boolean mClear;
        private Bitmap mOverlayBitmap;
        private int mRenderingMode;


        public OverlayData() {
            mOverlayBitmap = null;
            mRenderingMode = 2;
            mClear = false;
        }
    }

    public static interface MediaProcessingProgressListener {

        public abstract void onProgress(Object obj, int i, int j);

        public static final int ACTION_DECODE = 2;
        public static final int ACTION_ENCODE = 1;
    }

    public static interface ExportProgressListener {

        public abstract void onProgress(VideoEditor videoeditor, String s, int i);
    }

    public static interface PreviewProgressListener {

        public abstract void onError(VideoEditor videoeditor, int i);

        public abstract void onProgress(VideoEditor videoeditor, long l, OverlayData overlaydata);

        public abstract void onStart(VideoEditor videoeditor);

        public abstract void onStop(VideoEditor videoeditor);
    }


    public abstract void addAudioTrack(AudioTrack audiotrack);

    public abstract void addMediaItem(MediaItem mediaitem);

    public abstract void addTransition(Transition transition);

    public abstract void cancelExport(String s);

    public abstract void clearSurface(SurfaceHolder surfaceholder);

    public abstract void export(String s, int i, int j, int k, int l, ExportProgressListener exportprogresslistener) throws IOException;

    public abstract void export(String s, int i, int j, ExportProgressListener exportprogresslistener) throws IOException;

    public abstract void generatePreview(MediaProcessingProgressListener mediaprocessingprogresslistener);

    public abstract List getAllAudioTracks();

    public abstract List getAllMediaItems();

    public abstract List getAllTransitions();

    public abstract int getAspectRatio();

    public abstract AudioTrack getAudioTrack(String s);

    public abstract long getDuration();

    public abstract MediaItem getMediaItem(String s);

    public abstract String getPath();

    public abstract Transition getTransition(String s);

    public abstract void insertAudioTrack(AudioTrack audiotrack, String s);

    public abstract void insertMediaItem(MediaItem mediaitem, String s);

    public abstract void moveAudioTrack(String s, String s1);

    public abstract void moveMediaItem(String s, String s1);

    public abstract void release();

    public abstract void removeAllMediaItems();

    public abstract AudioTrack removeAudioTrack(String s);

    public abstract MediaItem removeMediaItem(String s);

    public abstract Transition removeTransition(String s);

    public abstract long renderPreviewFrame(SurfaceHolder surfaceholder, long l, OverlayData overlaydata);

    public abstract void save() throws IOException;

    public abstract void setAspectRatio(int i);

    public abstract void startPreview(SurfaceHolder surfaceholder, long l, long l1, boolean flag, int i, 
            PreviewProgressListener previewprogresslistener);

    public abstract long stopPreview();

    public static final int DURATION_OF_STORYBOARD = -1;
    public static final long MAX_SUPPORTED_FILE_SIZE = 0x80000000L;
    public static final String THUMBNAIL_FILENAME = "thumbnail.jpg";
}
