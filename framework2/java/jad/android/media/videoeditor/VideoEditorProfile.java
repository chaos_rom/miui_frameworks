// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.media.videoeditor;


public class VideoEditorProfile {

    private VideoEditorProfile(int i, int j, int k, int l) {
        maxInputVideoFrameWidth = i;
        maxInputVideoFrameHeight = j;
        maxOutputVideoFrameWidth = k;
        maxOutputVideoFrameHeight = l;
    }

    public static VideoEditorProfile get() {
        return native_get_videoeditor_profile();
    }

    public static int getExportLevel(int i) {
        switch(i) {
        default:
            throw new IllegalArgumentException((new StringBuilder()).append("Unsupported video codec").append(i).toString());

        case 1: // '\001'
        case 2: // '\002'
        case 3: // '\003'
            return native_get_videoeditor_export_level(i);
        }
    }

    public static int getExportProfile(int i) {
        switch(i) {
        default:
            throw new IllegalArgumentException((new StringBuilder()).append("Unsupported video codec").append(i).toString());

        case 1: // '\001'
        case 2: // '\002'
        case 3: // '\003'
            return native_get_videoeditor_export_profile(i);
        }
    }

    private static final native int native_get_videoeditor_export_level(int i);

    private static final native int native_get_videoeditor_export_profile(int i);

    private static final native VideoEditorProfile native_get_videoeditor_profile();

    private static final native void native_init();

    public int maxInputVideoFrameHeight;
    public int maxInputVideoFrameWidth;
    public int maxOutputVideoFrameHeight;
    public int maxOutputVideoFrameWidth;

    static  {
        System.loadLibrary("media_jni");
        native_init();
    }
}
