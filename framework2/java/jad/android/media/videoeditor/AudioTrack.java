// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.media.videoeditor;

import java.io.File;
import java.io.IOException;
import java.lang.ref.SoftReference;

// Referenced classes of package android.media.videoeditor:
//            VideoEditorImpl, MediaArtistNativeHelper, WaveformData, VideoEditor, 
//            ExtractAudioWaveformProgressListener

public class AudioTrack {

    private AudioTrack() throws IOException {
        this(null, null, null);
    }

    public AudioTrack(VideoEditor videoeditor, String s, String s1) throws IOException {
        this(videoeditor, s, s1, 0L, 0L, -1L, false, 100, false, false, 0, 0, null);
    }

    AudioTrack(VideoEditor videoeditor, String s, String s1, long l, long l1, 
            long l2, boolean flag, int i, boolean flag1, boolean flag2, int j, 
            int k, String s2) throws IOException {
        File file = new File(s1);
        if(!file.exists())
            throw new IOException((new StringBuilder()).append(s1).append(" not found ! ").toString());
        if(0x80000000L <= file.length())
            throw new IllegalArgumentException("File size is more than 2GB");
        MediaArtistNativeHelper.Properties properties;
        if(videoeditor instanceof VideoEditorImpl) {
            mMANativeHelper = ((VideoEditorImpl)videoeditor).getNativeContext();
            int i1;
            try {
                properties = mMANativeHelper.getMediaProperties(s1);
            }
            catch(Exception exception) {
                throw new IllegalArgumentException((new StringBuilder()).append(exception.getMessage()).append(" : ").append(s1).toString());
            }
            i1 = mMANativeHelper.getFileType(properties.fileType);
            switch(i1) {
            default:
                throw new IllegalArgumentException((new StringBuilder()).append("Unsupported input file type: ").append(i1).toString());

            case 0: // '\0'
            case 1: // '\001'
            case 2: // '\002'
            case 3: // '\003'
                switch(mMANativeHelper.getAudioCodecType(properties.audioFormat)) {
                case 3: // '\003'
                case 4: // '\004'
                case 6: // '\006'
                case 7: // '\007'
                default:
                    throw new IllegalArgumentException("Unsupported Audio Codec Format in Input File");

                case 1: // '\001'
                case 2: // '\002'
                case 5: // '\005'
                case 8: // '\b'
                    break;
                }
                break;
            }
        } else {
            throw new IllegalArgumentException("editor is not of type VideoEditorImpl");
        }
        if(l2 == -1L)
            l2 = properties.audioDuration;
        mUniqueId = s;
        mFilename = s1;
        mStartTimeMs = l;
        mDurationMs = properties.audioDuration;
        mAudioChannels = properties.audioChannels;
        mAudioBitrate = properties.audioBitrate;
        mAudioSamplingFrequency = properties.audioSamplingFrequency;
        mAudioType = properties.audioFormat;
        mTimelineDurationMs = l2 - l1;
        mVolumePercent = i;
        mBeginBoundaryTimeMs = l1;
        mEndBoundaryTimeMs = l2;
        mLoop = flag;
        mMuted = flag1;
        mIsDuckingEnabled = flag2;
        mDuckingThreshold = j;
        mDuckedTrackVolume = k;
        mAudioWaveformFilename = s2;
        if(s2 != null)
            mWaveformData = new SoftReference(new WaveformData(s2));
        else
            mWaveformData = null;
    }

    public void disableDucking() {
        if(mIsDuckingEnabled) {
            mMANativeHelper.setGeneratePreview(true);
            mIsDuckingEnabled = false;
        }
    }

    public void disableLoop() {
        if(mLoop) {
            mMANativeHelper.setGeneratePreview(true);
            mLoop = false;
        }
    }

    public void enableDucking(int i, int j) {
        if(i < 0 || i > 90)
            throw new IllegalArgumentException((new StringBuilder()).append("Invalid threshold value: ").append(i).toString());
        if(j < 0 || j > 100) {
            throw new IllegalArgumentException((new StringBuilder()).append("Invalid duckedTrackVolume value: ").append(j).toString());
        } else {
            mMANativeHelper.setGeneratePreview(true);
            mDuckingThreshold = i;
            mDuckedTrackVolume = j;
            mIsDuckingEnabled = true;
            return;
        }
    }

    public void enableLoop() {
        if(!mLoop) {
            mMANativeHelper.setGeneratePreview(true);
            mLoop = true;
        }
    }

    public boolean equals(Object obj) {
        boolean flag;
        if(!(obj instanceof AudioTrack))
            flag = false;
        else
            flag = mUniqueId.equals(((AudioTrack)obj).mUniqueId);
        return flag;
    }

    public void extractAudioWaveform(ExtractAudioWaveformProgressListener extractaudiowaveformprogresslistener) throws IOException {
        if(mAudioWaveformFilename != null) goto _L2; else goto _L1
_L1:
        String s1;
        int i;
        String s = mMANativeHelper.getProjectPath();
        s1 = String.format((new StringBuilder()).append(s).append("/audioWaveformFile-").append(getId()).append(".dat").toString(), new Object[0]);
        i = mMANativeHelper.getAudioCodecType(mAudioType);
        i;
        JVM INSTR tableswitch 1 8: default 116
    //                   1 144
    //                   2 215
    //                   3 116
    //                   4 116
    //                   5 227
    //                   6 116
    //                   7 116
    //                   8 203;
           goto _L3 _L4 _L5 _L3 _L3 _L6 _L3 _L3 _L7
_L3:
        throw new IllegalStateException((new StringBuilder()).append("Unsupported codec type: ").append(i).toString());
_L4:
        byte byte0;
        char c;
        byte0 = 5;
        c = '\240';
_L9:
        mMANativeHelper.generateAudioGraph(mUniqueId, mFilename, s1, byte0, 2, c, extractaudiowaveformprogresslistener, false);
        mAudioWaveformFilename = s1;
_L2:
        mWaveformData = new SoftReference(new WaveformData(mAudioWaveformFilename));
        return;
_L7:
        byte0 = 10;
        c = '\u0140';
        continue; /* Loop/switch isn't completed */
_L5:
        byte0 = 32;
        c = '\u0400';
        continue; /* Loop/switch isn't completed */
_L6:
        byte0 = 36;
        c = '\u0480';
        if(true) goto _L9; else goto _L8
_L8:
    }

    public int getAudioBitrate() {
        return mAudioBitrate;
    }

    public int getAudioChannels() {
        return mAudioChannels;
    }

    public int getAudioSamplingFrequency() {
        return mAudioSamplingFrequency;
    }

    public int getAudioType() {
        return mAudioType;
    }

    String getAudioWaveformFilename() {
        return mAudioWaveformFilename;
    }

    public long getBoundaryBeginTime() {
        return mBeginBoundaryTimeMs;
    }

    public long getBoundaryEndTime() {
        return mEndBoundaryTimeMs;
    }

    public int getDuckedTrackVolume() {
        return mDuckedTrackVolume;
    }

    public int getDuckingThreshhold() {
        return mDuckingThreshold;
    }

    public long getDuration() {
        return mDurationMs;
    }

    public String getFilename() {
        return mFilename;
    }

    public String getId() {
        return mUniqueId;
    }

    public long getStartTime() {
        return mStartTimeMs;
    }

    public long getTimelineDuration() {
        return mTimelineDurationMs;
    }

    public int getVolume() {
        return mVolumePercent;
    }

    public WaveformData getWaveformData() throws IOException {
        if(mWaveformData != null) goto _L2; else goto _L1
_L1:
        WaveformData waveformdata = null;
_L4:
        return waveformdata;
_L2:
        waveformdata = (WaveformData)mWaveformData.get();
        if(waveformdata == null)
            if(mAudioWaveformFilename != null) {
                try {
                    waveformdata = new WaveformData(mAudioWaveformFilename);
                }
                catch(IOException ioexception) {
                    throw ioexception;
                }
                mWaveformData = new SoftReference(waveformdata);
            } else {
                waveformdata = null;
            }
        if(true) goto _L4; else goto _L3
_L3:
    }

    public int hashCode() {
        return mUniqueId.hashCode();
    }

    void invalidate() {
        if(mAudioWaveformFilename != null) {
            (new File(mAudioWaveformFilename)).delete();
            mAudioWaveformFilename = null;
            mWaveformData = null;
        }
    }

    public boolean isDuckingEnabled() {
        return mIsDuckingEnabled;
    }

    public boolean isLooping() {
        return mLoop;
    }

    public boolean isMuted() {
        return mMuted;
    }

    public void setExtractBoundaries(long l, long l1) {
        if(l > mDurationMs)
            throw new IllegalArgumentException("Invalid start time");
        if(l1 > mDurationMs)
            throw new IllegalArgumentException("Invalid end time");
        if(l < 0L)
            throw new IllegalArgumentException("Invalid start time; is < 0");
        if(l1 < 0L) {
            throw new IllegalArgumentException("Invalid end time; is < 0");
        } else {
            mMANativeHelper.setGeneratePreview(true);
            mBeginBoundaryTimeMs = l;
            mEndBoundaryTimeMs = l1;
            mTimelineDurationMs = mEndBoundaryTimeMs - mBeginBoundaryTimeMs;
            return;
        }
    }

    public void setMute(boolean flag) {
        mMANativeHelper.setGeneratePreview(true);
        mMuted = flag;
    }

    public void setVolume(int i) {
        if(i > 100)
            throw new IllegalArgumentException("Volume set exceeds maximum allowed value");
        if(i < 0) {
            throw new IllegalArgumentException("Invalid Volume ");
        } else {
            mMANativeHelper.setGeneratePreview(true);
            mVolumePercent = i;
            return;
        }
    }

    private final int mAudioBitrate;
    private final int mAudioChannels;
    private final int mAudioSamplingFrequency;
    private final int mAudioType;
    private String mAudioWaveformFilename;
    private long mBeginBoundaryTimeMs;
    private int mDuckedTrackVolume;
    private int mDuckingThreshold;
    private final long mDurationMs;
    private long mEndBoundaryTimeMs;
    private final String mFilename;
    private boolean mIsDuckingEnabled;
    private boolean mLoop;
    private final MediaArtistNativeHelper mMANativeHelper;
    private boolean mMuted;
    private long mStartTimeMs;
    private long mTimelineDurationMs;
    private final String mUniqueId;
    private int mVolumePercent;
    private SoftReference mWaveformData;
}
