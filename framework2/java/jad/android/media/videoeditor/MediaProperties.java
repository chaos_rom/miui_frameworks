// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.media.videoeditor;

import android.util.Pair;

// Referenced classes of package android.media.videoeditor:
//            VideoEditorProfile

public class MediaProperties {
    public final class MPEG4Level {

        public static final int MPEG4Level0 = 1;
        public static final int MPEG4Level0b = 2;
        public static final int MPEG4Level1 = 4;
        public static final int MPEG4Level2 = 8;
        public static final int MPEG4Level3 = 16;
        public static final int MPEG4Level4 = 32;
        public static final int MPEG4Level4a = 64;
        public static final int MPEG4Level5 = 128;
        public static final int MPEG4LevelUnknown = 0x7fffffff;
        final MediaProperties this$0;

        public MPEG4Level() {
            this$0 = MediaProperties.this;
            super();
        }
    }

    public final class MPEG4Profile {

        public static final int MPEG4ProfileAdvancedCoding = 4096;
        public static final int MPEG4ProfileAdvancedCore = 8192;
        public static final int MPEG4ProfileAdvancedRealTime = 1024;
        public static final int MPEG4ProfileAdvancedScalable = 16384;
        public static final int MPEG4ProfileAdvancedSimple = 32768;
        public static final int MPEG4ProfileBasicAnimated = 256;
        public static final int MPEG4ProfileCore = 4;
        public static final int MPEG4ProfileCoreScalable = 2048;
        public static final int MPEG4ProfileHybrid = 512;
        public static final int MPEG4ProfileMain = 8;
        public static final int MPEG4ProfileNbit = 16;
        public static final int MPEG4ProfileScalableTexture = 32;
        public static final int MPEG4ProfileSimple = 1;
        public static final int MPEG4ProfileSimpleFBA = 128;
        public static final int MPEG4ProfileSimpleFace = 64;
        public static final int MPEG4ProfileSimpleScalable = 2;
        public static final int MPEG4ProfileUnknown = 0x7fffffff;
        final MediaProperties this$0;

        public MPEG4Profile() {
            this$0 = MediaProperties.this;
            super();
        }
    }

    public final class H263Level {

        public static final int H263Level10 = 1;
        public static final int H263Level20 = 2;
        public static final int H263Level30 = 4;
        public static final int H263Level40 = 8;
        public static final int H263Level45 = 16;
        public static final int H263Level50 = 32;
        public static final int H263Level60 = 64;
        public static final int H263Level70 = 128;
        public static final int H263LevelUnknown = 0x7fffffff;
        final MediaProperties this$0;

        public H263Level() {
            this$0 = MediaProperties.this;
            super();
        }
    }

    public final class H263Profile {

        public static final int H263ProfileBackwardCompatible = 4;
        public static final int H263ProfileBaseline = 1;
        public static final int H263ProfileH320Coding = 2;
        public static final int H263ProfileHighCompression = 32;
        public static final int H263ProfileHighLatency = 256;
        public static final int H263ProfileISWV2 = 8;
        public static final int H263ProfileISWV3 = 16;
        public static final int H263ProfileInterlace = 128;
        public static final int H263ProfileInternet = 64;
        public static final int H263ProfileUnknown = 0x7fffffff;
        final MediaProperties this$0;

        public H263Profile() {
            this$0 = MediaProperties.this;
            super();
        }
    }

    public final class H264Level {

        public static final int H264Level1 = 1;
        public static final int H264Level11 = 4;
        public static final int H264Level12 = 8;
        public static final int H264Level13 = 16;
        public static final int H264Level1b = 2;
        public static final int H264Level2 = 32;
        public static final int H264Level21 = 64;
        public static final int H264Level22 = 128;
        public static final int H264Level3 = 256;
        public static final int H264Level31 = 512;
        public static final int H264Level32 = 1024;
        public static final int H264Level4 = 2048;
        public static final int H264Level41 = 4096;
        public static final int H264Level42 = 8192;
        public static final int H264Level5 = 16384;
        public static final int H264Level51 = 32768;
        public static final int H264LevelUnknown = 0x7fffffff;
        final MediaProperties this$0;

        public H264Level() {
            this$0 = MediaProperties.this;
            super();
        }
    }

    public final class H264Profile {

        public static final int H264ProfileBaseline = 1;
        public static final int H264ProfileExtended = 4;
        public static final int H264ProfileHigh = 8;
        public static final int H264ProfileHigh10 = 16;
        public static final int H264ProfileHigh422 = 32;
        public static final int H264ProfileHigh444 = 64;
        public static final int H264ProfileMain = 2;
        public static final int H264ProfileUnknown = 0x7fffffff;
        final MediaProperties this$0;

        public H264Profile() {
            this$0 = MediaProperties.this;
            super();
        }
    }


    private MediaProperties() {
    }

    public static int[] getAllSupportedAspectRatios() {
        return ASPECT_RATIOS;
    }

    public static int[] getSupportedAudioCodecs() {
        return SUPPORTED_ACODECS;
    }

    public static int getSupportedAudioTrackCount() {
        return 1;
    }

    public static int getSupportedMaxVolume() {
        return 100;
    }

    public static Pair[] getSupportedResolutions(int i) {
        i;
        JVM INSTR tableswitch 1 5: default 36
    //                   1 63
    //                   2 106
    //                   3 85
    //                   4 92
    //                   5 99;
           goto _L1 _L2 _L3 _L4 _L5 _L6
_L1:
        throw new IllegalArgumentException((new StringBuilder()).append("Unknown aspect ratio: ").append(i).toString());
_L2:
        Pair apair[] = ASPECT_RATIO_3_2_RESOLUTIONS;
_L8:
        VideoEditorProfile videoeditorprofile;
        videoeditorprofile = VideoEditorProfile.get();
        if(videoeditorprofile == null)
            throw new RuntimeException("Can't get the video editor profile");
        break; /* Loop/switch isn't completed */
_L4:
        apair = ASPECT_RATIO_4_3_RESOLUTIONS;
        continue; /* Loop/switch isn't completed */
_L5:
        apair = ASPECT_RATIO_5_3_RESOLUTIONS;
        continue; /* Loop/switch isn't completed */
_L6:
        apair = ASPECT_RATIO_11_9_RESOLUTIONS;
        continue; /* Loop/switch isn't completed */
_L3:
        apair = ASPECT_RATIO_16_9_RESOLUTIONS;
        if(true) goto _L8; else goto _L7
_L7:
        int j = videoeditorprofile.maxOutputVideoFrameWidth;
        int k = videoeditorprofile.maxOutputVideoFrameHeight;
        Pair apair1[] = new Pair[apair.length];
        int l = 0;
        for(int i1 = 0; i1 < apair.length; i1++)
            if(((Integer)apair[i1].first).intValue() <= j && ((Integer)apair[i1].second).intValue() <= k) {
                apair1[l] = apair[i1];
                l++;
            }

        Pair apair2[] = new Pair[l];
        System.arraycopy(apair1, 0, apair2, 0, l);
        return apair2;
    }

    public static int[] getSupportedVideoBitrates() {
        return SUPPORTED_BITRATES;
    }

    public static int[] getSupportedVideoCodecs() {
        return SUPPORTED_VCODECS;
    }

    public static int[] getSupportedVideoFileFormat() {
        return SUPPORTED_VIDEO_FILE_FORMATS;
    }

    public static final int ACODEC_AAC_LC = 2;
    public static final int ACODEC_AAC_PLUS = 3;
    public static final int ACODEC_AMRNB = 1;
    public static final int ACODEC_AMRWB = 8;
    public static final int ACODEC_ENHANCED_AAC_PLUS = 4;
    public static final int ACODEC_EVRC = 6;
    public static final int ACODEC_MP3 = 5;
    public static final int ACODEC_NO_AUDIO = 0;
    public static final int ACODEC_OGG = 9;
    private static final int ASPECT_RATIOS[];
    public static final int ASPECT_RATIO_11_9 = 5;
    private static final Pair ASPECT_RATIO_11_9_RESOLUTIONS[];
    public static final int ASPECT_RATIO_16_9 = 2;
    private static final Pair ASPECT_RATIO_16_9_RESOLUTIONS[];
    public static final int ASPECT_RATIO_3_2 = 1;
    private static final Pair ASPECT_RATIO_3_2_RESOLUTIONS[];
    public static final int ASPECT_RATIO_4_3 = 3;
    private static final Pair ASPECT_RATIO_4_3_RESOLUTIONS[];
    public static final int ASPECT_RATIO_5_3 = 4;
    private static final Pair ASPECT_RATIO_5_3_RESOLUTIONS[];
    public static final int ASPECT_RATIO_UNDEFINED = 0;
    public static final int AUDIO_MAX_TRACK_COUNT = 1;
    public static final int AUDIO_MAX_VOLUME_PERCENT = 100;
    public static final int BITRATE_128K = 0x1f400;
    public static final int BITRATE_192K = 0x2ee00;
    public static final int BITRATE_256K = 0x3e800;
    public static final int BITRATE_28K = 28000;
    public static final int BITRATE_2M = 0x1e8480;
    public static final int BITRATE_384K = 0x5dc00;
    public static final int BITRATE_40K = 40000;
    public static final int BITRATE_512K = 0x7d000;
    public static final int BITRATE_5M = 0x4c4b40;
    public static final int BITRATE_64K = 64000;
    public static final int BITRATE_800K = 0xc3500;
    public static final int BITRATE_8M = 0x7a1200;
    public static final int BITRATE_96K = 0x17700;
    public static final int DEFAULT_CHANNEL_COUNT = 2;
    public static final int DEFAULT_SAMPLING_FREQUENCY = 32000;
    public static final int FILE_3GP = 0;
    public static final int FILE_AMR = 2;
    public static final int FILE_JPEG = 5;
    public static final int FILE_M4V = 10;
    public static final int FILE_MP3 = 3;
    public static final int FILE_MP4 = 1;
    public static final int FILE_PNG = 8;
    public static final int FILE_UNSUPPORTED = 255;
    public static final int HEIGHT_1080 = 1080;
    public static final int HEIGHT_144 = 144;
    public static final int HEIGHT_288 = 288;
    public static final int HEIGHT_360 = 360;
    public static final int HEIGHT_480 = 480;
    public static final int HEIGHT_720 = 720;
    public static final int SAMPLES_PER_FRAME_AAC = 1024;
    public static final int SAMPLES_PER_FRAME_AMRNB = 160;
    public static final int SAMPLES_PER_FRAME_AMRWB = 320;
    public static final int SAMPLES_PER_FRAME_MP3 = 1152;
    private static final int SUPPORTED_ACODECS[];
    private static final int SUPPORTED_BITRATES[];
    private static final int SUPPORTED_VCODECS[];
    private static final int SUPPORTED_VIDEO_FILE_FORMATS[];
    public static final int UNDEFINED_VIDEO_PROFILE = 255;
    public static final int VCODEC_H263 = 1;
    public static final int VCODEC_H264 = 2;
    public static final int VCODEC_MPEG4 = 3;

    static  {
        int ai[] = new int[5];
        ai[0] = 1;
        ai[1] = 2;
        ai[2] = 3;
        ai[3] = 4;
        ai[4] = 5;
        ASPECT_RATIOS = ai;
        Pair apair[] = new Pair[2];
        apair[0] = new Pair(Integer.valueOf(720), Integer.valueOf(480));
        apair[1] = new Pair(Integer.valueOf(1080), Integer.valueOf(720));
        ASPECT_RATIO_3_2_RESOLUTIONS = apair;
        Pair apair1[] = new Pair[2];
        apair1[0] = new Pair(Integer.valueOf(640), Integer.valueOf(480));
        apair1[1] = new Pair(Integer.valueOf(960), Integer.valueOf(720));
        ASPECT_RATIO_4_3_RESOLUTIONS = apair1;
        Pair apair2[] = new Pair[1];
        apair2[0] = new Pair(Integer.valueOf(800), Integer.valueOf(480));
        ASPECT_RATIO_5_3_RESOLUTIONS = apair2;
        Pair apair3[] = new Pair[2];
        apair3[0] = new Pair(Integer.valueOf(176), Integer.valueOf(144));
        apair3[1] = new Pair(Integer.valueOf(352), Integer.valueOf(288));
        ASPECT_RATIO_11_9_RESOLUTIONS = apair3;
        Pair apair4[] = new Pair[3];
        apair4[0] = new Pair(Integer.valueOf(848), Integer.valueOf(480));
        apair4[1] = new Pair(Integer.valueOf(1280), Integer.valueOf(720));
        apair4[2] = new Pair(Integer.valueOf(1920), Integer.valueOf(1080));
        ASPECT_RATIO_16_9_RESOLUTIONS = apair4;
        int ai1[] = new int[13];
        ai1[0] = 28000;
        ai1[1] = 40000;
        ai1[2] = 64000;
        ai1[3] = 0x17700;
        ai1[4] = 0x1f400;
        ai1[5] = 0x2ee00;
        ai1[6] = 0x3e800;
        ai1[7] = 0x5dc00;
        ai1[8] = 0x7d000;
        ai1[9] = 0xc3500;
        ai1[10] = 0x1e8480;
        ai1[11] = 0x4c4b40;
        ai1[12] = 0x7a1200;
        SUPPORTED_BITRATES = ai1;
        int ai2[] = new int[3];
        ai2[0] = 2;
        ai2[1] = 1;
        ai2[2] = 3;
        SUPPORTED_VCODECS = ai2;
        int ai3[] = new int[3];
        ai3[0] = 2;
        ai3[1] = 1;
        ai3[2] = 8;
        SUPPORTED_ACODECS = ai3;
        int ai4[] = new int[3];
        ai4[0] = 0;
        ai4[1] = 1;
        ai4[2] = 10;
        SUPPORTED_VIDEO_FILE_FORMATS = ai4;
    }
}
