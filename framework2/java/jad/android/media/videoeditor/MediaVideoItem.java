// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.media.videoeditor;

import android.graphics.Bitmap;
import android.view.SurfaceHolder;
import java.io.File;
import java.io.IOException;
import java.lang.ref.SoftReference;

// Referenced classes of package android.media.videoeditor:
//            MediaItem, VideoEditorImpl, MediaArtistNativeHelper, VideoEditorProfile, 
//            WaveformData, EffectKenBurns, Transition, VideoEditor, 
//            Effect, ExtractAudioWaveformProgressListener

public class MediaVideoItem extends MediaItem {

    private MediaVideoItem() throws IOException {
        this(null, null, null, 0);
    }

    public MediaVideoItem(VideoEditor videoeditor, String s, String s1, int i) throws IOException {
        this(videoeditor, s, s1, i, 0L, -1L, 100, false, null);
    }

    MediaVideoItem(VideoEditor videoeditor, String s, String s1, int i, long l, long l1, int j, boolean flag, String s2) throws IOException {
        super(videoeditor, s, s1, i);
        if(videoeditor instanceof VideoEditorImpl) {
            mMANativeHelper = ((VideoEditorImpl)videoeditor).getNativeContext();
            mVideoEditor = (VideoEditorImpl)videoeditor;
        }
        MediaArtistNativeHelper.Properties properties;
        VideoEditorProfile videoeditorprofile;
        try {
            properties = mMANativeHelper.getMediaProperties(s1);
        }
        catch(Exception exception) {
            throw new IllegalArgumentException((new StringBuilder()).append(exception.getMessage()).append(" : ").append(s1).toString());
        }
        videoeditorprofile = VideoEditorProfile.get();
        if(videoeditorprofile == null)
            throw new RuntimeException("Can't get the video editor profile");
        int k = videoeditorprofile.maxInputVideoFrameWidth;
        int i1 = videoeditorprofile.maxInputVideoFrameHeight;
        if(properties.width > k || properties.height > i1)
            throw new IllegalArgumentException((new StringBuilder()).append("Unsupported import resolution. Supported maximum width:").append(k).append(" height:").append(i1).append(", current width:").append(properties.width).append(" height:").append(properties.height).toString());
        if(!properties.profileSupported)
            throw new IllegalArgumentException((new StringBuilder()).append("Unsupported video profile ").append(properties.profile).toString());
        if(!properties.levelSupported)
            throw new IllegalArgumentException((new StringBuilder()).append("Unsupported video level ").append(properties.level).toString());
        switch(mMANativeHelper.getFileType(properties.fileType)) {
        default:
            throw new IllegalArgumentException("Unsupported Input File Type");

        case 0: // '\0'
        case 1: // '\001'
        case 10: // '\n'
            switch(mMANativeHelper.getVideoCodecType(properties.videoFormat)) {
            default:
                throw new IllegalArgumentException("Unsupported Video Codec Format in Input File");

            case 1: // '\001'
            case 2: // '\002'
            case 3: // '\003'
                mWidth = properties.width;
                break;
            }
            break;
        }
        mHeight = properties.height;
        mAspectRatio = mMANativeHelper.getAspectRatio(properties.width, properties.height);
        mFileType = mMANativeHelper.getFileType(properties.fileType);
        mVideoType = mMANativeHelper.getVideoCodecType(properties.videoFormat);
        mVideoProfile = properties.profile;
        mVideoLevel = properties.level;
        mDurationMs = properties.videoDuration;
        mVideoBitrate = properties.videoBitrate;
        mAudioBitrate = properties.audioBitrate;
        mFps = (int)properties.averageFrameRate;
        mAudioType = mMANativeHelper.getAudioCodecType(properties.audioFormat);
        mAudioChannels = properties.audioChannels;
        mAudioSamplingFrequency = properties.audioSamplingFrequency;
        mBeginBoundaryTimeMs = l;
        if(l1 == -1L)
            l1 = mDurationMs;
        mEndBoundaryTimeMs = l1;
        mVolumePercentage = j;
        mMuted = flag;
        mAudioWaveformFilename = s2;
        if(s2 != null)
            mWaveformData = new SoftReference(new WaveformData(s2));
        else
            mWaveformData = null;
        mVideoRotationDegree = properties.videoRotation;
    }

    public void addEffect(Effect effect) {
        if(effect instanceof EffectKenBurns) {
            throw new IllegalArgumentException("Ken Burns effects cannot be applied to MediaVideoItem");
        } else {
            super.addEffect(effect);
            return;
        }
    }

    public void extractAudioWaveform(ExtractAudioWaveformProgressListener extractaudiowaveformprogresslistener) throws IOException {
        byte byte0;
        char c;
        String s;
        byte0 = 0;
        c = '\0';
        s = mMANativeHelper.getProjectPath();
        if(mAudioWaveformFilename != null) goto _L2; else goto _L1
_L1:
        String s1 = String.format((new StringBuilder()).append(s).append("/").append("audioWaveformFile-").append(getId()).append(".dat").toString(), new Object[0]);
        if(mMANativeHelper.getAudioCodecType(mAudioType) != 1) goto _L4; else goto _L3
_L3:
        byte0 = 5;
        c = '\240';
_L6:
        mMANativeHelper.generateAudioGraph(getId(), super.mFilename, s1, byte0, 2, c, extractaudiowaveformprogresslistener, true);
        mAudioWaveformFilename = s1;
_L2:
        mWaveformData = new SoftReference(new WaveformData(mAudioWaveformFilename));
        return;
_L4:
        if(mMANativeHelper.getAudioCodecType(mAudioType) == 8) {
            byte0 = 10;
            c = '\u0140';
        } else
        if(mMANativeHelper.getAudioCodecType(mAudioType) == 2) {
            byte0 = 32;
            c = '\u0400';
        }
        if(true) goto _L6; else goto _L5
_L5:
    }

    public int getAspectRatio() {
        return mAspectRatio;
    }

    public int getAudioBitrate() {
        return mAudioBitrate;
    }

    public int getAudioChannels() {
        return mAudioChannels;
    }

    public int getAudioSamplingFrequency() {
        return mAudioSamplingFrequency;
    }

    public int getAudioType() {
        return mAudioType;
    }

    String getAudioWaveformFilename() {
        return mAudioWaveformFilename;
    }

    public long getBoundaryBeginTime() {
        return mBeginBoundaryTimeMs;
    }

    public long getBoundaryEndTime() {
        return mEndBoundaryTimeMs;
    }

    public long getDuration() {
        return mDurationMs;
    }

    public int getFileType() {
        return mFileType;
    }

    public int getFps() {
        return mFps;
    }

    public int getHeight() {
        int i;
        if(mVideoRotationDegree == 90 || mVideoRotationDegree == 270)
            i = mWidth;
        else
            i = mHeight;
        return i;
    }

    public Bitmap getThumbnail(int i, int j, long l) {
        if(l > mDurationMs)
            throw new IllegalArgumentException("Time Exceeds duration");
        if(l < 0L)
            throw new IllegalArgumentException("Invalid Time duration");
        if(i <= 0 || j <= 0)
            throw new IllegalArgumentException("Invalid Dimensions");
        if(mVideoRotationDegree == 90 || mVideoRotationDegree == 270) {
            int k = i;
            i = j;
            j = k;
        }
        MediaArtistNativeHelper mediaartistnativehelper = mMANativeHelper;
        String s = getFilename();
        int i1 = mVideoRotationDegree;
        return mediaartistnativehelper.getPixels(s, i, j, l, i1);
    }

    public void getThumbnailList(int i, int j, long l, long l1, int k, 
            int ai[], MediaItem.GetThumbnailListCallback getthumbnaillistcallback) throws IOException {
        if(l > l1)
            throw new IllegalArgumentException("Start time is greater than end time");
        if(l1 > mDurationMs)
            throw new IllegalArgumentException("End time is greater than file duration");
        if(j <= 0 || i <= 0)
            throw new IllegalArgumentException("Invalid dimension");
        if(mVideoRotationDegree == 90 || mVideoRotationDegree == 270) {
            int i1 = i;
            i = j;
            j = i1;
        }
        MediaArtistNativeHelper mediaartistnativehelper = mMANativeHelper;
        String s = getFilename();
        int j1 = mVideoRotationDegree;
        mediaartistnativehelper.getPixelsList(s, i, j, l, l1, k, ai, getthumbnaillistcallback, j1);
    }

    public long getTimelineDuration() {
        return mEndBoundaryTimeMs - mBeginBoundaryTimeMs;
    }

    public int getVideoBitrate() {
        return mVideoBitrate;
    }

    MediaArtistNativeHelper.ClipSettings getVideoClipProperties() {
        MediaArtistNativeHelper.ClipSettings clipsettings = new MediaArtistNativeHelper.ClipSettings();
        clipsettings.clipPath = getFilename();
        clipsettings.fileType = mMANativeHelper.getMediaItemFileType(getFileType());
        clipsettings.beginCutTime = (int)getBoundaryBeginTime();
        clipsettings.endCutTime = (int)getBoundaryEndTime();
        clipsettings.mediaRendering = mMANativeHelper.getMediaItemRenderingMode(getRenderingMode());
        clipsettings.rotationDegree = mVideoRotationDegree;
        return clipsettings;
    }

    public int getVideoLevel() {
        return mVideoLevel;
    }

    public int getVideoProfile() {
        return mVideoProfile;
    }

    public int getVideoType() {
        return mVideoType;
    }

    public int getVolume() {
        return mVolumePercentage;
    }

    public WaveformData getWaveformData() throws IOException {
        if(mWaveformData != null) goto _L2; else goto _L1
_L1:
        WaveformData waveformdata = null;
_L4:
        return waveformdata;
_L2:
        waveformdata = (WaveformData)mWaveformData.get();
        if(waveformdata == null)
            if(mAudioWaveformFilename != null) {
                try {
                    waveformdata = new WaveformData(mAudioWaveformFilename);
                }
                catch(IOException ioexception) {
                    throw ioexception;
                }
                mWaveformData = new SoftReference(waveformdata);
            } else {
                waveformdata = null;
            }
        if(true) goto _L4; else goto _L3
_L3:
    }

    public int getWidth() {
        int i;
        if(mVideoRotationDegree == 90 || mVideoRotationDegree == 270)
            i = mHeight;
        else
            i = mWidth;
        return i;
    }

    void invalidate() {
        if(mAudioWaveformFilename != null) {
            (new File(mAudioWaveformFilename)).delete();
            mAudioWaveformFilename = null;
        }
    }

    void invalidateTransitions(long l, long l1) {
        if(super.mBeginTransition != null && isOverlapping(l, l1, mBeginBoundaryTimeMs, super.mBeginTransition.getDuration()))
            super.mBeginTransition.invalidate();
        if(super.mEndTransition != null) {
            long l2 = super.mEndTransition.getDuration();
            if(isOverlapping(l, l1, mEndBoundaryTimeMs - l2, l2))
                super.mEndTransition.invalidate();
        }
    }

    void invalidateTransitions(long l, long l1, long l2, long l3) {
        if(super.mBeginTransition != null) {
            long l5 = super.mBeginTransition.getDuration();
            boolean flag2 = isOverlapping(l, l1, mBeginBoundaryTimeMs, l5);
            boolean flag3 = isOverlapping(l2, l3, mBeginBoundaryTimeMs, l5);
            boolean flag;
            if(flag3 != flag2)
                super.mBeginTransition.invalidate();
            else
            if(flag3 && (l != l2 || l + l1 <= l5 || l2 + l3 <= l5))
                super.mBeginTransition.invalidate();
        }
        if(super.mEndTransition != null) {
            long l4 = super.mEndTransition.getDuration();
            flag = isOverlapping(l, l1, mEndBoundaryTimeMs - l4, l4);
            boolean flag1 = isOverlapping(l2, l3, mEndBoundaryTimeMs - l4, l4);
            if(flag1 != flag)
                super.mEndTransition.invalidate();
            else
            if(flag1 && (l + l1 != l2 + l3 || l > mEndBoundaryTimeMs - l4 || l2 > mEndBoundaryTimeMs - l4))
                super.mEndTransition.invalidate();
        }
    }

    public boolean isMuted() {
        return mMuted;
    }

    public long renderFrame(SurfaceHolder surfaceholder, long l) {
        long l1 = 0L;
        if(surfaceholder == null)
            throw new IllegalArgumentException("Surface Holder is null");
        if(l > mDurationMs || l < l1)
            throw new IllegalArgumentException("requested time not correct");
        android.view.Surface surface = surfaceholder.getSurface();
        if(surface == null)
            throw new RuntimeException("Surface could not be retrieved from Surface holder");
        if(super.mFilename != null)
            l1 = mMANativeHelper.renderMediaItemPreviewFrame(surface, super.mFilename, l, mWidth, mHeight);
        return l1;
    }

    public void setExtractBoundaries(long l, long l1) {
        if(l > mDurationMs)
            throw new IllegalArgumentException("setExtractBoundaries: Invalid start time");
        if(l1 > mDurationMs)
            throw new IllegalArgumentException("setExtractBoundaries: Invalid end time");
        if(l1 != -1L && l >= l1)
            throw new IllegalArgumentException("setExtractBoundaries: Start time is greater than end time");
        if(l < 0L || l1 != -1L && l1 < 0L)
            throw new IllegalArgumentException("setExtractBoundaries: Start time or end time is negative");
        mMANativeHelper.setGeneratePreview(true);
        if(l != mBeginBoundaryTimeMs && super.mBeginTransition != null)
            super.mBeginTransition.invalidate();
        if(l1 != mEndBoundaryTimeMs && super.mEndTransition != null)
            super.mEndTransition.invalidate();
        mBeginBoundaryTimeMs = l;
        mEndBoundaryTimeMs = l1;
        adjustTransitions();
        mVideoEditor.updateTimelineDuration();
    }

    public void setMute(boolean flag) {
        mMANativeHelper.setGeneratePreview(true);
        mMuted = flag;
        if(super.mBeginTransition != null)
            super.mBeginTransition.invalidate();
        if(super.mEndTransition != null)
            super.mEndTransition.invalidate();
    }

    public void setVolume(int i) {
        if(i < 0 || i > 100) {
            throw new IllegalArgumentException("Invalid volume");
        } else {
            mVolumePercentage = i;
            return;
        }
    }

    private final int mAspectRatio;
    private final int mAudioBitrate;
    private final int mAudioChannels;
    private final int mAudioSamplingFrequency;
    private final int mAudioType;
    private String mAudioWaveformFilename;
    private long mBeginBoundaryTimeMs;
    private final long mDurationMs;
    private long mEndBoundaryTimeMs;
    private final int mFileType;
    private final int mFps;
    private final int mHeight;
    private MediaArtistNativeHelper mMANativeHelper;
    private boolean mMuted;
    private final int mVideoBitrate;
    private VideoEditorImpl mVideoEditor;
    private final int mVideoLevel;
    private final int mVideoProfile;
    private final int mVideoRotationDegree;
    private final int mVideoType;
    private int mVolumePercentage;
    private SoftReference mWaveformData;
    private final int mWidth;
}
