// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.media.videoeditor;

import android.graphics.*;
import android.util.Log;
import android.util.Pair;
import java.io.*;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.*;

// Referenced classes of package android.media.videoeditor:
//            MediaItem, VideoEditorImpl, MediaArtistNativeHelper, MediaProperties, 
//            Effect, Overlay, EffectKenBurns, Transition, 
//            VideoEditor

public class MediaImageItem extends MediaItem {

    private MediaImageItem() throws IOException {
        this(null, null, null, 0L, 0);
    }

    public MediaImageItem(VideoEditor videoeditor, String s, String s1, long l, int i) throws IOException {
        super(videoeditor, s, s1, i);
        mMANativeHelper = ((VideoEditorImpl)videoeditor).getNativeContext();
        mVideoEditor = (VideoEditorImpl)videoeditor;
        try {
            MediaArtistNativeHelper.Properties properties = mMANativeHelper.getMediaProperties(s1);
            switch(mMANativeHelper.getFileType(properties.fileType)) {
            case 6: // '\006'
            case 7: // '\007'
            default:
                throw new IllegalArgumentException("Unsupported Input File Type");

            case 5: // '\005'
            case 8: // '\b'
                break;
            }
        }
        catch(Exception exception) {
            throw new IllegalArgumentException((new StringBuilder()).append("Unsupported file or file not found: ").append(s1).toString());
        }
        mFileName = s1;
        android.graphics.BitmapFactory.Options options = new android.graphics.BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(s1, options);
        mWidth = options.outWidth;
        mHeight = options.outHeight;
        mDurationMs = l;
        mDecodedFilename = String.format((new StringBuilder()).append(mMANativeHelper.getProjectPath()).append("/").append("decoded").append(getId()).append(".rgb").toString(), new Object[0]);
        Pair apair[];
        Pair pair;
        Bitmap bitmap;
        int j;
        int k;
        try {
            mAspectRatio = mMANativeHelper.getAspectRatio(mWidth, mHeight);
        }
        catch(IllegalArgumentException illegalargumentexception) {
            throw new IllegalArgumentException("Null width and height");
        }
        mGeneratedClipHeight = 0;
        mGeneratedClipWidth = 0;
        apair = MediaProperties.getSupportedResolutions(mAspectRatio);
        pair = apair[-1 + apair.length];
        if(mWidth > ((Integer)pair.first).intValue() || mHeight > ((Integer)pair.second).intValue()) {
            bitmap = scaleImage(s1, ((Integer)pair.first).intValue(), ((Integer)pair.second).intValue());
            mScaledFilename = String.format((new StringBuilder()).append(mMANativeHelper.getProjectPath()).append("/").append("scaled").append(getId()).append(".JPG").toString(), new Object[0]);
            if(!(new File(mScaledFilename)).exists()) {
                super.mRegenerateClip = true;
                FileOutputStream fileoutputstream1 = new FileOutputStream(mScaledFilename);
                bitmap.compress(android.graphics.Bitmap.CompressFormat.JPEG, 50, fileoutputstream1);
                fileoutputstream1.close();
            }
            mScaledWidth = (bitmap.getWidth() >> 1) << 1;
            mScaledHeight = (bitmap.getHeight() >> 1) << 1;
        } else {
            mScaledFilename = s1;
            mScaledWidth = (mWidth >> 1) << 1;
            mScaledHeight = (mHeight >> 1) << 1;
            bitmap = BitmapFactory.decodeFile(mScaledFilename);
        }
        j = mScaledWidth;
        k = mScaledHeight;
        if(!(new File(mDecodedFilename)).exists()) {
            FileOutputStream fileoutputstream = new FileOutputStream(mDecodedFilename);
            DataOutputStream dataoutputstream = new DataOutputStream(fileoutputstream);
            int ai[] = new int[j];
            ByteBuffer bytebuffer = ByteBuffer.allocate(4 * ai.length);
            byte abyte0[] = bytebuffer.array();
            for(int i1 = 0; i1 < k; i1++) {
                bitmap.getPixels(ai, 0, mScaledWidth, 0, i1, j, 1);
                bytebuffer.asIntBuffer().put(ai, 0, j);
                dataoutputstream.write(abyte0);
            }

            fileoutputstream.close();
        }
        bitmap.recycle();
    }

    private List adjustEffects() {
        ArrayList arraylist = new ArrayList();
        Iterator iterator = getAllEffects().iterator();
        do {
            if(!iterator.hasNext())
                break;
            Effect effect = (Effect)iterator.next();
            long l;
            long l1;
            if(effect.getStartTime() > getDuration())
                l = 0L;
            else
                l = effect.getStartTime();
            if(l + effect.getDuration() > getDuration())
                l1 = getDuration() - l;
            else
                l1 = effect.getDuration();
            if(l != effect.getStartTime() || l1 != effect.getDuration()) {
                effect.setStartTimeAndDuration(l, l1);
                arraylist.add(effect);
            }
        } while(true);
        return arraylist;
    }

    private List adjustOverlays() {
        ArrayList arraylist = new ArrayList();
        Iterator iterator = getAllOverlays().iterator();
        do {
            if(!iterator.hasNext())
                break;
            Overlay overlay = (Overlay)iterator.next();
            long l;
            long l1;
            if(overlay.getStartTime() > getDuration())
                l = 0L;
            else
                l = overlay.getStartTime();
            if(l + overlay.getDuration() > getDuration())
                l1 = getDuration() - l;
            else
                l1 = overlay.getDuration();
            if(l != overlay.getStartTime() || l1 != overlay.getDuration()) {
                overlay.setStartTimeAndDuration(l, l1);
                arraylist.add(overlay);
            }
        } while(true);
        return arraylist;
    }

    private MediaArtistNativeHelper.ClipSettings getKenBurns(EffectKenBurns effectkenburns) {
        Rect rect = new Rect();
        Rect rect1 = new Rect();
        MediaArtistNativeHelper.ClipSettings clipsettings = new MediaArtistNativeHelper.ClipSettings();
        effectkenburns.getKenBurnsSettings(rect, rect1);
        int i = getWidth();
        int j = getHeight();
        if(rect.left < 0 || rect.left > i || rect.right < 0 || rect.right > i || rect.top < 0 || rect.top > j || rect.bottom < 0 || rect.bottom > j || rect1.left < 0 || rect1.left > i || rect1.right < 0 || rect1.right > i || rect1.top < 0 || rect1.top > j || rect1.bottom < 0 || rect1.bottom > j)
            throw new IllegalArgumentException("Illegal arguments for KebBurns");
        if((i - (rect.right - rect.left) == 0 || j - (rect.bottom - rect.top) == 0) && (i - (rect1.right - rect1.left) == 0 || j - (rect1.bottom - rect1.top) == 0)) {
            setRegenerateClip(false);
            clipsettings.clipPath = getDecodedImageFileName();
            clipsettings.fileType = 5;
            clipsettings.beginCutTime = 0;
            clipsettings.endCutTime = (int)getTimelineDuration();
            clipsettings.beginCutPercent = 0;
            clipsettings.endCutPercent = 0;
            clipsettings.panZoomEnabled = false;
            clipsettings.panZoomPercentStart = 0;
            clipsettings.panZoomTopLeftXStart = 0;
            clipsettings.panZoomTopLeftYStart = 0;
            clipsettings.panZoomPercentEnd = 0;
            clipsettings.panZoomTopLeftXEnd = 0;
            clipsettings.panZoomTopLeftYEnd = 0;
            clipsettings.mediaRendering = mMANativeHelper.getMediaItemRenderingMode(getRenderingMode());
            clipsettings.rgbWidth = getScaledWidth();
            clipsettings.rgbHeight = getScaledHeight();
        } else {
            int k = (1000 * rect.width()) / i;
            int l = (1000 * rect1.width()) / i;
            clipsettings.clipPath = getDecodedImageFileName();
            clipsettings.fileType = mMANativeHelper.getMediaItemFileType(getFileType());
            clipsettings.beginCutTime = 0;
            clipsettings.endCutTime = (int)getTimelineDuration();
            clipsettings.beginCutPercent = 0;
            clipsettings.endCutPercent = 0;
            clipsettings.panZoomEnabled = true;
            clipsettings.panZoomPercentStart = k;
            clipsettings.panZoomTopLeftXStart = (1000 * rect.left) / i;
            clipsettings.panZoomTopLeftYStart = (1000 * rect.top) / j;
            clipsettings.panZoomPercentEnd = l;
            clipsettings.panZoomTopLeftXEnd = (1000 * rect1.left) / i;
            clipsettings.panZoomTopLeftYEnd = (1000 * rect1.top) / j;
            clipsettings.mediaRendering = mMANativeHelper.getMediaItemRenderingMode(getRenderingMode());
            clipsettings.rgbWidth = getScaledWidth();
            clipsettings.rgbHeight = getScaledHeight();
        }
        return clipsettings;
    }

    private int getWidthByAspectRatioAndHeight(int i, int j) {
        char c = '\0';
        i;
        JVM INSTR tableswitch 1 5: default 36
    //                   1 47
    //                   2 74
    //                   3 130
    //                   4 155
    //                   5 169;
           goto _L1 _L2 _L3 _L4 _L5 _L6
_L1:
        throw new IllegalArgumentException("Illegal arguments for aspectRatio");
_L2:
        if(j != 480) goto _L8; else goto _L7
_L7:
        c = '\u02D0';
_L10:
        return c;
_L8:
        if(j == 720)
            c = '\u0438';
        continue; /* Loop/switch isn't completed */
_L3:
        if(j == 360)
            c = '\u0280';
        else
        if(j == 480)
            c = '\u0356';
        else
        if(j == 720)
            c = '\u0500';
        else
        if(j == 1080)
            c = '\u0780';
        continue; /* Loop/switch isn't completed */
_L4:
        if(j == 480)
            c = '\u0280';
        if(j == 720)
            c = '\u03C0';
        continue; /* Loop/switch isn't completed */
_L5:
        if(j == 480)
            c = '\u0320';
        continue; /* Loop/switch isn't completed */
_L6:
        if(j == 144)
            c = '\260';
        if(true) goto _L10; else goto _L9
_L9:
    }

    private void invalidateBeginTransition(List list, List list1) {
label0:
        {
            if(super.mBeginTransition == null || !super.mBeginTransition.isGenerated())
                break label0;
            long l = super.mBeginTransition.getDuration();
            Iterator iterator = list.iterator();
            do {
                if(!iterator.hasNext())
                    break;
                if(((Effect)iterator.next()).getStartTime() >= l)
                    continue;
                super.mBeginTransition.invalidate();
                break;
            } while(true);
            if(!super.mBeginTransition.isGenerated())
                break label0;
            Iterator iterator1 = list1.iterator();
            do
                if(!iterator1.hasNext())
                    break label0;
            while(((Overlay)iterator1.next()).getStartTime() >= l);
            super.mBeginTransition.invalidate();
        }
    }

    private void invalidateEndTransition() {
label0:
        {
            if(super.mEndTransition == null || !super.mEndTransition.isGenerated())
                break label0;
            long l = super.mEndTransition.getDuration();
            Iterator iterator = getAllEffects().iterator();
            do {
                if(!iterator.hasNext())
                    break;
                Effect effect = (Effect)iterator.next();
                if(effect.getStartTime() + effect.getDuration() <= mDurationMs - l)
                    continue;
                super.mEndTransition.invalidate();
                break;
            } while(true);
            if(!super.mEndTransition.isGenerated())
                break label0;
            Iterator iterator1 = getAllOverlays().iterator();
            Overlay overlay;
            do {
                if(!iterator1.hasNext())
                    break label0;
                overlay = (Overlay)iterator1.next();
            } while(overlay.getStartTime() + overlay.getDuration() <= mDurationMs - l);
            super.mEndTransition.invalidate();
        }
    }

    public static int nextPowerOf2(int i) {
        int j = i - 1;
        int k = j | j >>> 16;
        int l = k | k >>> 8;
        int i1 = l | l >>> 4;
        int j1 = i1 | i1 >>> 2;
        return 1 + (j1 | j1 >>> 1);
    }

    private Bitmap scaleImage(String s, int i, int j) throws IOException {
        android.graphics.BitmapFactory.Options options = new android.graphics.BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(s, options);
        int k = options.outWidth;
        int l = options.outHeight;
        if(Log.isLoggable("MediaImageItem", 3))
            Log.d("MediaImageItem", (new StringBuilder()).append("generateThumbnail: Input: ").append(k).append("x").append(l).append(", resize to: ").append(i).append("x").append(j).toString());
        float f2;
        float f3;
        Bitmap bitmap;
        if(k > i || l > j) {
            float f = (float)k / (float)i;
            float f1 = (float)l / (float)j;
            if(f > f1) {
                f2 = i;
                int i1;
                android.graphics.BitmapFactory.Options options1;
                if((float)l / f < (float)j)
                    f3 = (float)Math.ceil((float)l / f);
                else
                    f3 = (float)Math.floor((float)l / f);
            } else {
                if((float)k / f1 > (float)i)
                    f2 = (float)Math.floor((float)k / f1);
                else
                    f2 = (float)Math.ceil((float)k / f1);
                f3 = j;
            }
            i1 = nextPowerOf2((int)Math.ceil(Math.max((float)k / f2, (float)l / f3)));
            options1 = new android.graphics.BitmapFactory.Options();
            options1.inSampleSize = i1;
            bitmap = BitmapFactory.decodeFile(s, options1);
        } else {
            f2 = i;
            f3 = j;
            bitmap = BitmapFactory.decodeFile(s);
        }
        if(bitmap == null) {
            Log.e("MediaImageItem", "generateThumbnail: Cannot decode image bytes");
            throw new IOException((new StringBuilder()).append("Cannot decode file: ").append(super.mFilename).toString());
        } else {
            Bitmap bitmap1 = Bitmap.createBitmap((int)f2, (int)f3, android.graphics.Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap1);
            canvas.drawBitmap(bitmap, new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight()), new Rect(0, 0, (int)f2, (int)f3), sResizePaint);
            canvas.setBitmap(null);
            bitmap.recycle();
            return bitmap1;
        }
    }

    MediaArtistNativeHelper.ClipSettings generateKenburnsClip(EffectKenBurns effectkenburns) {
        MediaArtistNativeHelper.EditSettings editsettings = new MediaArtistNativeHelper.EditSettings();
        editsettings.clipSettingsArray = new MediaArtistNativeHelper.ClipSettings[1];
        MediaArtistNativeHelper.ClipSettings clipsettings = new MediaArtistNativeHelper.ClipSettings();
        initClipSettings(clipsettings);
        editsettings.clipSettingsArray[0] = getKenBurns(effectkenburns);
        if(getGeneratedImageClip() == null && getRegenerateClip()) {
            String s = mMANativeHelper.generateKenBurnsClip(editsettings, this);
            setGeneratedImageClip(s);
            setRegenerateClip(false);
            clipsettings.clipPath = s;
            clipsettings.fileType = 0;
            mGeneratedClipHeight = getScaledHeight();
            mGeneratedClipWidth = getWidthByAspectRatioAndHeight(mVideoEditor.getAspectRatio(), mGeneratedClipHeight);
        } else
        if(getGeneratedImageClip() == null) {
            clipsettings.clipPath = getDecodedImageFileName();
            clipsettings.fileType = 5;
            clipsettings.rgbWidth = getScaledWidth();
            clipsettings.rgbHeight = getScaledHeight();
        } else {
            clipsettings.clipPath = getGeneratedImageClip();
            clipsettings.fileType = 0;
        }
        clipsettings.mediaRendering = mMANativeHelper.getMediaItemRenderingMode(getRenderingMode());
        clipsettings.beginCutTime = 0;
        clipsettings.endCutTime = (int)getTimelineDuration();
        return clipsettings;
    }

    public int getAspectRatio() {
        return mAspectRatio;
    }

    String getDecodedImageFileName() {
        return mDecodedFilename;
    }

    public long getDuration() {
        return mDurationMs;
    }

    public int getFileType() {
        char c;
        if(super.mFilename.endsWith(".jpg") || super.mFilename.endsWith(".jpeg") || super.mFilename.endsWith(".JPG") || super.mFilename.endsWith(".JPEG"))
            c = '\005';
        else
        if(super.mFilename.endsWith(".png") || super.mFilename.endsWith(".PNG"))
            c = '\b';
        else
            c = '\377';
        return c;
    }

    int getGeneratedClipHeight() {
        return mGeneratedClipHeight;
    }

    int getGeneratedClipWidth() {
        return mGeneratedClipWidth;
    }

    String getGeneratedImageClip() {
        return super.getGeneratedImageClip();
    }

    public int getHeight() {
        return mHeight;
    }

    MediaArtistNativeHelper.ClipSettings getImageClipProperties() {
        MediaArtistNativeHelper.ClipSettings clipsettings = new MediaArtistNativeHelper.ClipSettings();
        EffectKenBurns effectkenburns = null;
        boolean flag = false;
        Iterator iterator = getAllEffects().iterator();
        do {
            if(!iterator.hasNext())
                break;
            Effect effect = (Effect)iterator.next();
            if(!(effect instanceof EffectKenBurns))
                continue;
            effectkenburns = (EffectKenBurns)effect;
            flag = true;
            break;
        } while(true);
        if(flag) {
            clipsettings = generateKenburnsClip(effectkenburns);
        } else {
            initClipSettings(clipsettings);
            clipsettings.clipPath = getDecodedImageFileName();
            clipsettings.fileType = 5;
            clipsettings.beginCutTime = 0;
            clipsettings.endCutTime = (int)getTimelineDuration();
            clipsettings.mediaRendering = mMANativeHelper.getMediaItemRenderingMode(getRenderingMode());
            clipsettings.rgbWidth = getScaledWidth();
            clipsettings.rgbHeight = getScaledHeight();
        }
        return clipsettings;
    }

    public int getScaledHeight() {
        return mScaledHeight;
    }

    String getScaledImageFileName() {
        return mScaledFilename;
    }

    public int getScaledWidth() {
        return mScaledWidth;
    }

    public Bitmap getThumbnail(int i, int j, long l) throws IOException {
        Bitmap bitmap;
        if(getGeneratedImageClip() != null)
            bitmap = mMANativeHelper.getPixels(getGeneratedImageClip(), i, j, l, 0);
        else
            bitmap = scaleImage(super.mFilename, i, j);
        return bitmap;
    }

    public void getThumbnailList(int i, int j, long l, long l1, int k, 
            int ai[], MediaItem.GetThumbnailListCallback getthumbnaillistcallback) throws IOException {
        if(getGeneratedImageClip() == null) {
            Bitmap bitmap = scaleImage(super.mFilename, i, j);
            for(int i1 = 0; i1 < ai.length; i1++)
                getthumbnaillistcallback.onThumbnail(bitmap, ai[i1]);

        } else {
            if(l > l1)
                throw new IllegalArgumentException("Start time is greater than end time");
            if(l1 > mDurationMs)
                throw new IllegalArgumentException("End time is greater than file duration");
            mMANativeHelper.getPixelsList(getGeneratedImageClip(), i, j, l, l1, k, ai, getthumbnaillistcallback, 0);
        }
    }

    public long getTimelineDuration() {
        return mDurationMs;
    }

    public int getWidth() {
        return mWidth;
    }

    void invalidate() {
        if(getGeneratedImageClip() != null) {
            (new File(getGeneratedImageClip())).delete();
            setGeneratedImageClip(null);
            setRegenerateClip(true);
        }
        if(mScaledFilename != null) {
            if(mFileName != mScaledFilename)
                (new File(mScaledFilename)).delete();
            mScaledFilename = null;
        }
        if(mDecodedFilename != null) {
            (new File(mDecodedFilename)).delete();
            mDecodedFilename = null;
        }
    }

    void invalidateTransitions(long l, long l1) {
        if(super.mBeginTransition != null && isOverlapping(l, l1, 0L, super.mBeginTransition.getDuration()))
            super.mBeginTransition.invalidate();
        if(super.mEndTransition != null) {
            long l2 = super.mEndTransition.getDuration();
            if(isOverlapping(l, l1, getDuration() - l2, l2))
                super.mEndTransition.invalidate();
        }
    }

    void invalidateTransitions(long l, long l1, long l2, long l3) {
        if(super.mBeginTransition != null) {
            long l5 = super.mBeginTransition.getDuration();
            boolean flag2 = isOverlapping(l, l1, 0L, l5);
            boolean flag3 = isOverlapping(l2, l3, 0L, l5);
            boolean flag;
            if(flag3 != flag2)
                super.mBeginTransition.invalidate();
            else
            if(flag3 && (l != l2 || l + l1 <= l5 || l2 + l3 <= l5))
                super.mBeginTransition.invalidate();
        }
        if(super.mEndTransition != null) {
            long l4 = super.mEndTransition.getDuration();
            flag = isOverlapping(l, l1, mDurationMs - l4, l4);
            boolean flag1 = isOverlapping(l2, l3, mDurationMs - l4, l4);
            if(flag1 != flag)
                super.mEndTransition.invalidate();
            else
            if(flag1 && (l + l1 != l2 + l3 || l > mDurationMs - l4 || l2 > mDurationMs - l4))
                super.mEndTransition.invalidate();
        }
    }

    public void setDuration(long l) {
        if(l != mDurationMs) {
            mMANativeHelper.setGeneratePreview(true);
            invalidateEndTransition();
            mDurationMs = l;
            adjustTransitions();
            List list = adjustOverlays();
            invalidateBeginTransition(adjustEffects(), list);
            invalidateEndTransition();
            if(getGeneratedImageClip() != null) {
                (new File(getGeneratedImageClip())).delete();
                setGeneratedImageClip(null);
                super.setRegenerateClip(true);
            }
            mVideoEditor.updateTimelineDuration();
        }
    }

    void setGeneratedImageClip(String s) {
        super.setGeneratedImageClip(s);
        mGeneratedClipHeight = getScaledHeight();
        mGeneratedClipWidth = getWidthByAspectRatioAndHeight(mVideoEditor.getAspectRatio(), mGeneratedClipHeight);
    }

    private static final String TAG = "MediaImageItem";
    private static final Paint sResizePaint = new Paint(2);
    private final int mAspectRatio;
    private String mDecodedFilename;
    private long mDurationMs;
    private String mFileName;
    private int mGeneratedClipHeight;
    private int mGeneratedClipWidth;
    private final int mHeight;
    private final MediaArtistNativeHelper mMANativeHelper;
    private String mScaledFilename;
    private int mScaledHeight;
    private int mScaledWidth;
    private final VideoEditorImpl mVideoEditor;
    private final int mWidth;

}
