// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.media.videoeditor;


// Referenced classes of package android.media.videoeditor:
//            Effect, MediaItem

public class EffectColor extends Effect {

    private EffectColor() {
        this(null, null, 0L, 0L, 0, 0);
    }

    public EffectColor(MediaItem mediaitem, String s, long l, long l1, int i, 
            int j) {
        super(mediaitem, s, l, l1);
        i;
        JVM INSTR tableswitch 1 5: default 44
    //                   1 72
    //                   2 72
    //                   3 149
    //                   4 149
    //                   5 149;
           goto _L1 _L2 _L2 _L3 _L3 _L3
_L1:
        throw new IllegalArgumentException((new StringBuilder()).append("Invalid type: ").append(i).toString());
_L2:
        switch(j) {
        default:
            throw new IllegalArgumentException((new StringBuilder()).append("Invalid Color: ").append(j).toString());

        case 65280: 
        case 8355711: 
        case 16737996: 
            mColor = j;
            break;
        }
_L5:
        mType = i;
        return;
_L3:
        mColor = -1;
        if(true) goto _L5; else goto _L4
_L4:
    }

    public int getColor() {
        return mColor;
    }

    public int getType() {
        return mType;
    }

    public static final int GRAY = 0x7f7f7f;
    public static final int GREEN = 65280;
    public static final int PINK = 0xff66cc;
    public static final int TYPE_COLOR = 1;
    public static final int TYPE_FIFTIES = 5;
    public static final int TYPE_GRADIENT = 2;
    public static final int TYPE_NEGATIVE = 4;
    public static final int TYPE_SEPIA = 3;
    private final int mColor;
    private final int mType;
}
