// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.media.videoeditor;

import android.graphics.*;
import android.util.Pair;
import java.io.*;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;

// Referenced classes of package android.media.videoeditor:
//            Overlay, MediaItem, MediaArtistNativeHelper, MediaProperties

public class OverlayFrame extends Overlay {

    private OverlayFrame() {
        this(null, null, (String)null, 0L, 0L);
    }

    public OverlayFrame(MediaItem mediaitem, String s, Bitmap bitmap, long l, long l1) {
        super(mediaitem, s, l, l1);
        mBitmap = bitmap;
        mFilename = null;
        mBitmapFileName = null;
        mResizedRGBWidth = 0;
        mResizedRGBHeight = 0;
    }

    OverlayFrame(MediaItem mediaitem, String s, String s1, long l, long l1) {
        super(mediaitem, s, l, l1);
        mBitmapFileName = s1;
        mBitmap = BitmapFactory.decodeFile(mBitmapFileName);
        mFilename = null;
        mResizedRGBWidth = 0;
        mResizedRGBHeight = 0;
    }

    void generateOverlayWithRenderingMode(MediaItem mediaitem, OverlayFrame overlayframe, int i, int j) throws FileNotFoundException, IOException {
        FileOutputStream fileoutputstream;
        int k = mediaitem.getRenderingMode();
        Bitmap bitmap = overlayframe.getBitmap();
        int l = overlayframe.getResizedRGBSizeHeight();
        int i1 = overlayframe.getResizedRGBSizeWidth();
        if(i1 == 0)
            i1 = bitmap.getWidth();
        if(l == 0)
            l = bitmap.getHeight();
        if(i1 != j || l != i || !(new File(overlayframe.getFilename())).exists()) {
            Bitmap bitmap1 = Bitmap.createBitmap(j, i, android.graphics.Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap1);
            Rect rect;
            Rect rect1;
            Paint paint;
            String s;
            DataOutputStream dataoutputstream;
            int ai[];
            ByteBuffer bytebuffer;
            byte abyte0[];
            int i3;
            switch(k) {
            default:
                throw new IllegalStateException((new StringBuilder()).append("Rendering mode: ").append(k).toString());

            case 1: // '\001'
                int k5 = canvas.getWidth();
                int l5 = canvas.getHeight();
                rect1 = new Rect(0, 0, k5, l5);
                int i6 = bitmap.getWidth();
                int j6 = bitmap.getHeight();
                rect = new Rect(0, 0, i6, j6);
                break;

            case 0: // '\0'
                int l3;
                int i4;
                int j4;
                int k4;
                int l4;
                int i5;
                if((float)bitmap.getWidth() / (float)bitmap.getHeight() > (float)canvas.getWidth() / (float)canvas.getHeight()) {
                    int j5 = (canvas.getWidth() * bitmap.getHeight()) / bitmap.getWidth();
                    l3 = 0;
                    i4 = (canvas.getHeight() - j5) / 2;
                    j4 = canvas.getWidth();
                    k4 = i4 + j5;
                } else {
                    int k3 = (canvas.getHeight() * bitmap.getWidth()) / bitmap.getHeight();
                    l3 = (canvas.getWidth() - k3) / 2;
                    i4 = 0;
                    j4 = l3 + k3;
                    k4 = canvas.getHeight();
                }
                rect1 = new Rect(l3, i4, j4, k4);
                l4 = bitmap.getWidth();
                i5 = bitmap.getHeight();
                rect = new Rect(0, 0, l4, i5);
                continue; /* Loop/switch isn't completed */

            case 2: // '\002'
                int k1;
                int l1;
                int i2;
                int j2;
                int k2;
                int l2;
                if((float)bitmap.getWidth() / (float)bitmap.getHeight() < (float)canvas.getWidth() / (float)canvas.getHeight()) {
                    int j3 = (bitmap.getWidth() * canvas.getHeight()) / canvas.getWidth();
                    k1 = 0;
                    l1 = (bitmap.getHeight() - j3) / 2;
                    i2 = bitmap.getWidth();
                    j2 = l1 + j3;
                } else {
                    int j1 = (bitmap.getHeight() * canvas.getWidth()) / canvas.getHeight();
                    k1 = (bitmap.getWidth() - j1) / 2;
                    l1 = 0;
                    i2 = k1 + j1;
                    j2 = bitmap.getHeight();
                }
                rect = new Rect(k1, l1, i2, j2);
                k2 = canvas.getWidth();
                l2 = canvas.getHeight();
                rect1 = new Rect(0, 0, k2, l2);
                continue; /* Loop/switch isn't completed */
            }
            break;
        }
          goto _L1
_L3:
        paint = sResizePaint;
        canvas.drawBitmap(bitmap, rect, rect1, paint);
        canvas.setBitmap(null);
        s = overlayframe.getFilename();
        if(s != null)
            (new File(s)).delete();
        fileoutputstream = new FileOutputStream(s);
        dataoutputstream = new DataOutputStream(fileoutputstream);
        ai = new int[j];
        bytebuffer = ByteBuffer.allocate(4 * ai.length);
        abyte0 = bytebuffer.array();
        for(i3 = 0; i3 < i; i3++) {
            bitmap1.getPixels(ai, 0, j, 0, i3, j, 1);
            bytebuffer.asIntBuffer().put(ai, 0, j);
            dataoutputstream.write(abyte0);
        }

        fileoutputstream.flush();
        fileoutputstream.close();
        overlayframe.setResizedRGBSize(j, i);
_L1:
        return;
        if(true) goto _L3; else goto _L2
_L2:
    }

    public Bitmap getBitmap() {
        return mBitmap;
    }

    String getBitmapImageFileName() {
        return mBitmapFileName;
    }

    String getFilename() {
        return mFilename;
    }

    int getOverlayFrameHeight() {
        return mOFHeight;
    }

    int getOverlayFrameWidth() {
        return mOFWidth;
    }

    int getResizedRGBSizeHeight() {
        return mResizedRGBHeight;
    }

    int getResizedRGBSizeWidth() {
        return mResizedRGBWidth;
    }

    void invalidate() {
        if(mBitmap != null) {
            mBitmap.recycle();
            mBitmap = null;
        }
        if(mFilename != null) {
            (new File(mFilename)).delete();
            mFilename = null;
        }
        if(mBitmapFileName != null) {
            (new File(mBitmapFileName)).delete();
            mBitmapFileName = null;
        }
    }

    void invalidateGeneratedFiles() {
        if(mFilename != null) {
            (new File(mFilename)).delete();
            mFilename = null;
        }
        if(mBitmapFileName != null) {
            (new File(mBitmapFileName)).delete();
            mBitmapFileName = null;
        }
    }

    String save(String s) throws FileNotFoundException, IOException {
        String s1;
        if(mFilename != null) {
            s1 = mFilename;
        } else {
            mBitmapFileName = (new StringBuilder()).append(s).append("/").append("Overlay").append(getId()).append(".png").toString();
            if(!(new File(mBitmapFileName)).exists()) {
                FileOutputStream fileoutputstream = new FileOutputStream(mBitmapFileName);
                mBitmap.compress(android.graphics.Bitmap.CompressFormat.PNG, 100, fileoutputstream);
                fileoutputstream.flush();
                fileoutputstream.close();
            }
            mOFWidth = mBitmap.getWidth();
            mOFHeight = mBitmap.getHeight();
            mFilename = (new StringBuilder()).append(s).append("/").append("Overlay").append(getId()).append(".rgb").toString();
            Pair apair[] = MediaProperties.getSupportedResolutions(super.getMediaItem().getNativeContext().nativeHelperGetAspectRatio());
            Pair pair = apair[-1 + apair.length];
            generateOverlayWithRenderingMode(super.getMediaItem(), this, ((Integer)pair.second).intValue(), ((Integer)pair.first).intValue());
            s1 = mFilename;
        }
        return s1;
    }

    public void setBitmap(Bitmap bitmap) {
        getMediaItem().getNativeContext().setGeneratePreview(true);
        invalidate();
        mBitmap = bitmap;
        if(mFilename != null) {
            (new File(mFilename)).delete();
            mFilename = null;
        }
        getMediaItem().invalidateTransitions(super.mStartTimeMs, super.mDurationMs);
    }

    void setFilename(String s) {
        mFilename = s;
    }

    void setOverlayFrameHeight(int i) {
        mOFHeight = i;
    }

    void setOverlayFrameWidth(int i) {
        mOFWidth = i;
    }

    void setResizedRGBSize(int i, int j) {
        mResizedRGBWidth = i;
        mResizedRGBHeight = j;
    }

    private static final Paint sResizePaint = new Paint(2);
    private Bitmap mBitmap;
    private String mBitmapFileName;
    private String mFilename;
    private int mOFHeight;
    private int mOFWidth;
    private int mResizedRGBHeight;
    private int mResizedRGBWidth;

}
