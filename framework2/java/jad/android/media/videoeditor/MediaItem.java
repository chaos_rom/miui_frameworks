// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.media.videoeditor;

import android.graphics.Bitmap;
import java.io.*;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.*;

// Referenced classes of package android.media.videoeditor:
//            VideoEditorImpl, VideoEditor, Effect, MediaArtistNativeHelper, 
//            EffectKenBurns, Overlay, OverlayFrame, MediaVideoItem, 
//            MediaImageItem, Transition

public abstract class MediaItem {
    public static interface GetThumbnailListCallback {

        public abstract void onThumbnail(Bitmap bitmap, int i);
    }


    protected MediaItem(VideoEditor videoeditor, String s, String s1, int i) throws IOException {
        mBlankFrameGenerated = false;
        mBlankFrameFilename = null;
        if(s1 == null)
            throw new IllegalArgumentException("MediaItem : filename is null");
        File file = new File(s1);
        if(!file.exists())
            throw new IOException((new StringBuilder()).append(s1).append(" not found ! ").toString());
        if(0x80000000L <= file.length()) {
            throw new IllegalArgumentException("File size is more than 2GB");
        } else {
            mUniqueId = s;
            mFilename = s1;
            mRenderingMode = i;
            mEffects = new ArrayList();
            mOverlays = new ArrayList();
            mBeginTransition = null;
            mEndTransition = null;
            mMANativeHelper = ((VideoEditorImpl)videoeditor).getNativeContext();
            mProjectPath = videoeditor.getPath();
            mRegenerateClip = false;
            mGeneratedImageClip = null;
            return;
        }
    }

    public void addEffect(Effect effect) {
        if(effect == null)
            throw new IllegalArgumentException("NULL effect cannot be applied");
        if(effect.getMediaItem() != this)
            throw new IllegalArgumentException("Media item mismatch");
        if(mEffects.contains(effect))
            throw new IllegalArgumentException((new StringBuilder()).append("Effect already exists: ").append(effect.getId()).toString());
        if(effect.getStartTime() + effect.getDuration() > getDuration())
            throw new IllegalArgumentException("Effect start time + effect duration > media clip duration");
        mMANativeHelper.setGeneratePreview(true);
        mEffects.add(effect);
        invalidateTransitions(effect.getStartTime(), effect.getDuration());
        if(effect instanceof EffectKenBurns)
            mRegenerateClip = true;
    }

    public void addOverlay(Overlay overlay) throws FileNotFoundException, IOException {
        if(overlay == null)
            throw new IllegalArgumentException("NULL Overlay cannot be applied");
        if(overlay.getMediaItem() != this)
            throw new IllegalArgumentException("Media item mismatch");
        if(mOverlays.contains(overlay))
            throw new IllegalArgumentException((new StringBuilder()).append("Overlay already exists: ").append(overlay.getId()).toString());
        if(overlay.getStartTime() + overlay.getDuration() > getDuration())
            throw new IllegalArgumentException("Overlay start time + overlay duration > media clip duration");
        if(overlay instanceof OverlayFrame) {
            Bitmap bitmap = ((OverlayFrame)overlay).getBitmap();
            if(bitmap == null)
                throw new IllegalArgumentException("Overlay bitmap not specified");
            int i;
            int j;
            if(this instanceof MediaVideoItem) {
                i = getWidth();
                j = getHeight();
            } else {
                i = ((MediaImageItem)this).getScaledWidth();
                j = ((MediaImageItem)this).getScaledHeight();
            }
            if(bitmap.getWidth() != i || bitmap.getHeight() != j) {
                throw new IllegalArgumentException("Bitmap dimensions must match media item dimensions");
            } else {
                mMANativeHelper.setGeneratePreview(true);
                ((OverlayFrame)overlay).save(mProjectPath);
                mOverlays.add(overlay);
                invalidateTransitions(overlay.getStartTime(), overlay.getDuration());
                return;
            }
        } else {
            throw new IllegalArgumentException("Overlay not supported");
        }
    }

    protected void adjustTransitions() {
        if(mBeginTransition != null) {
            long l1 = mBeginTransition.getMaximumDuration();
            if(mBeginTransition.getDuration() > l1)
                mBeginTransition.setDuration(l1);
        }
        if(mEndTransition != null) {
            long l = mEndTransition.getMaximumDuration();
            if(mEndTransition.getDuration() > l)
                mEndTransition.setDuration(l);
        }
    }

    public boolean equals(Object obj) {
        boolean flag;
        if(!(obj instanceof MediaItem))
            flag = false;
        else
            flag = mUniqueId.equals(((MediaItem)obj).mUniqueId);
        return flag;
    }

    void generateBlankFrame(MediaArtistNativeHelper.ClipSettings clipsettings) {
        if(mBlankFrameGenerated) goto _L2; else goto _L1
_L1:
        FileOutputStream fileoutputstream;
        mBlankFrameFilename = String.format((new StringBuilder()).append(mProjectPath).append("/").append("ghost.rgb").toString(), new Object[0]);
        fileoutputstream = null;
        FileOutputStream fileoutputstream1 = new FileOutputStream(mBlankFrameFilename);
        fileoutputstream = fileoutputstream1;
_L4:
        DataOutputStream dataoutputstream = new DataOutputStream(fileoutputstream);
        int ai[] = new int[64];
        ByteBuffer bytebuffer = ByteBuffer.allocate(4 * ai.length);
        byte abyte0[] = bytebuffer.array();
        int i = 0;
        while(i < 64)  {
            bytebuffer.asIntBuffer().put(ai, 0, 64);
            try {
                dataoutputstream.write(abyte0);
            }
            catch(IOException ioexception1) { }
            i++;
        }
        try {
            fileoutputstream.close();
        }
        catch(IOException ioexception) { }
        mBlankFrameGenerated = true;
_L2:
        clipsettings.clipPath = mBlankFrameFilename;
        clipsettings.fileType = 5;
        clipsettings.beginCutTime = 0;
        clipsettings.endCutTime = 0;
        clipsettings.mediaRendering = 0;
        clipsettings.rgbWidth = 64;
        clipsettings.rgbHeight = 64;
        return;
        IOException ioexception2;
        ioexception2;
        if(true) goto _L4; else goto _L3
_L3:
    }

    public List getAllEffects() {
        return mEffects;
    }

    public List getAllOverlays() {
        return mOverlays;
    }

    public abstract int getAspectRatio();

    public Transition getBeginTransition() {
        return mBeginTransition;
    }

    MediaArtistNativeHelper.ClipSettings getClipSettings() {
        MediaArtistNativeHelper.ClipSettings clipsettings;
        clipsettings = new MediaArtistNativeHelper.ClipSettings();
        initClipSettings(clipsettings);
        if(!(this instanceof MediaVideoItem)) goto _L2; else goto _L1
_L1:
        MediaVideoItem mediavideoitem = (MediaVideoItem)this;
        clipsettings.clipPath = mediavideoitem.getFilename();
        clipsettings.fileType = mMANativeHelper.getMediaItemFileType(mediavideoitem.getFileType());
        clipsettings.beginCutTime = (int)mediavideoitem.getBoundaryBeginTime();
        clipsettings.endCutTime = (int)mediavideoitem.getBoundaryEndTime();
        clipsettings.mediaRendering = mMANativeHelper.getMediaItemRenderingMode(mediavideoitem.getRenderingMode());
_L4:
        return clipsettings;
_L2:
        if(this instanceof MediaImageItem)
            clipsettings = ((MediaImageItem)this).getImageClipProperties();
        if(true) goto _L4; else goto _L3
_L3:
    }

    public abstract long getDuration();

    public Effect getEffect(String s) {
        Iterator iterator = mEffects.iterator();
_L4:
        if(!iterator.hasNext()) goto _L2; else goto _L1
_L1:
        Effect effect = (Effect)iterator.next();
        if(!effect.getId().equals(s)) goto _L4; else goto _L3
_L3:
        return effect;
_L2:
        effect = null;
        if(true) goto _L3; else goto _L5
_L5:
    }

    public Transition getEndTransition() {
        return mEndTransition;
    }

    public abstract int getFileType();

    public String getFilename() {
        return mFilename;
    }

    String getGeneratedImageClip() {
        return mGeneratedImageClip;
    }

    public abstract int getHeight();

    public String getId() {
        return mUniqueId;
    }

    MediaArtistNativeHelper getNativeContext() {
        return mMANativeHelper;
    }

    public Overlay getOverlay(String s) {
        Iterator iterator = mOverlays.iterator();
_L4:
        if(!iterator.hasNext()) goto _L2; else goto _L1
_L1:
        Overlay overlay = (Overlay)iterator.next();
        if(!overlay.getId().equals(s)) goto _L4; else goto _L3
_L3:
        return overlay;
_L2:
        overlay = null;
        if(true) goto _L3; else goto _L5
_L5:
    }

    boolean getRegenerateClip() {
        return mRegenerateClip;
    }

    public int getRenderingMode() {
        return mRenderingMode;
    }

    public abstract Bitmap getThumbnail(int i, int j, long l) throws IOException;

    public abstract void getThumbnailList(int i, int j, long l, long l1, int k, 
            int ai[], GetThumbnailListCallback getthumbnaillistcallback) throws IOException;

    public Bitmap[] getThumbnailList(int i, int j, long l, long l1, int k) throws IOException {
        final Bitmap bitmaps[] = new Bitmap[k];
        int ai[] = new int[k];
        for(int i1 = 0; i1 < k; i1++)
            ai[i1] = i1;

        getThumbnailList(i, j, l, l1, k, ai, new GetThumbnailListCallback() {

            public void onThumbnail(Bitmap bitmap, int j1) {
                bitmaps[j1] = bitmap;
            }

            final MediaItem this$0;
            final Bitmap val$bitmaps[];

             {
                this$0 = MediaItem.this;
                bitmaps = abitmap;
                super();
            }
        });
        return bitmaps;
    }

    public abstract long getTimelineDuration();

    public abstract int getWidth();

    public int hashCode() {
        return mUniqueId.hashCode();
    }

    void initClipSettings(MediaArtistNativeHelper.ClipSettings clipsettings) {
        clipsettings.clipPath = null;
        clipsettings.clipDecodedPath = null;
        clipsettings.clipOriginalPath = null;
        clipsettings.fileType = 0;
        clipsettings.endCutTime = 0;
        clipsettings.beginCutTime = 0;
        clipsettings.beginCutPercent = 0;
        clipsettings.endCutPercent = 0;
        clipsettings.panZoomEnabled = false;
        clipsettings.panZoomPercentStart = 0;
        clipsettings.panZoomTopLeftXStart = 0;
        clipsettings.panZoomTopLeftYStart = 0;
        clipsettings.panZoomPercentEnd = 0;
        clipsettings.panZoomTopLeftXEnd = 0;
        clipsettings.panZoomTopLeftYEnd = 0;
        clipsettings.mediaRendering = 0;
        clipsettings.rgbWidth = 0;
        clipsettings.rgbHeight = 0;
    }

    void invalidateBlankFrame() {
        if(mBlankFrameFilename != null && (new File(mBlankFrameFilename)).exists()) {
            (new File(mBlankFrameFilename)).delete();
            mBlankFrameFilename = null;
        }
    }

    abstract void invalidateTransitions(long l, long l1);

    abstract void invalidateTransitions(long l, long l1, long l2, long l3);

    protected boolean isOverlapping(long l, long l1, long l2, long l3) {
        boolean flag;
        flag = false;
        break MISSING_BLOCK_LABEL_3;
        if(l + l1 > l2 && l < l2 + l3)
            flag = true;
        return flag;
    }

    public Effect removeEffect(String s) {
        Iterator iterator = mEffects.iterator();
_L4:
        if(!iterator.hasNext()) goto _L2; else goto _L1
_L1:
        Effect effect = (Effect)iterator.next();
        if(!effect.getId().equals(s)) goto _L4; else goto _L3
_L3:
        mMANativeHelper.setGeneratePreview(true);
        mEffects.remove(effect);
        invalidateTransitions(effect.getStartTime(), effect.getDuration());
        if(effect instanceof EffectKenBurns) {
            if(mGeneratedImageClip != null) {
                (new File(mGeneratedImageClip)).delete();
                mGeneratedImageClip = null;
            }
            mRegenerateClip = false;
        }
_L6:
        return effect;
_L2:
        effect = null;
        if(true) goto _L6; else goto _L5
_L5:
    }

    public Overlay removeOverlay(String s) {
        Iterator iterator = mOverlays.iterator();
_L4:
        if(!iterator.hasNext()) goto _L2; else goto _L1
_L1:
        Overlay overlay = (Overlay)iterator.next();
        if(!overlay.getId().equals(s)) goto _L4; else goto _L3
_L3:
        mMANativeHelper.setGeneratePreview(true);
        mOverlays.remove(overlay);
        if(overlay instanceof OverlayFrame)
            ((OverlayFrame)overlay).invalidate();
        invalidateTransitions(overlay.getStartTime(), overlay.getDuration());
_L6:
        return overlay;
_L2:
        overlay = null;
        if(true) goto _L6; else goto _L5
_L5:
    }

    void setBeginTransition(Transition transition) {
        mBeginTransition = transition;
    }

    void setEndTransition(Transition transition) {
        mEndTransition = transition;
    }

    void setGeneratedImageClip(String s) {
        mGeneratedImageClip = s;
    }

    void setRegenerateClip(boolean flag) {
        mRegenerateClip = flag;
    }

    public void setRenderingMode(int i) {
        switch(i) {
        default:
            throw new IllegalArgumentException("Invalid Rendering Mode");

        case 0: // '\0'
        case 1: // '\001'
        case 2: // '\002'
            mMANativeHelper.setGeneratePreview(true);
            break;
        }
        mRenderingMode = i;
        if(mBeginTransition != null)
            mBeginTransition.invalidate();
        if(mEndTransition != null)
            mEndTransition.invalidate();
        for(Iterator iterator = mOverlays.iterator(); iterator.hasNext(); ((OverlayFrame)(Overlay)iterator.next()).invalidateGeneratedFiles());
    }

    public static final int END_OF_FILE = -1;
    public static final int RENDERING_MODE_BLACK_BORDER = 0;
    public static final int RENDERING_MODE_CROPPING = 2;
    public static final int RENDERING_MODE_STRETCH = 1;
    protected Transition mBeginTransition;
    private String mBlankFrameFilename;
    private boolean mBlankFrameGenerated;
    private final List mEffects;
    protected Transition mEndTransition;
    protected final String mFilename;
    protected String mGeneratedImageClip;
    private final MediaArtistNativeHelper mMANativeHelper;
    private final List mOverlays;
    private final String mProjectPath;
    protected boolean mRegenerateClip;
    private int mRenderingMode;
    private final String mUniqueId;
}
