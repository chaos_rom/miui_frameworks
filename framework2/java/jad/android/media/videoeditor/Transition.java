// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.media.videoeditor;

import java.io.File;
import java.util.*;

// Referenced classes of package android.media.videoeditor:
//            MediaItem, MediaArtistNativeHelper, TransitionAlpha, TransitionSliding, 
//            TransitionCrossfade, TransitionFadeBlack, Overlay, OverlayFrame, 
//            Effect, EffectColor, MediaVideoItem

public abstract class Transition {

    private Transition() {
        this(null, null, null, 0L, 0);
    }

    protected Transition(String s, MediaItem mediaitem, MediaItem mediaitem1, long l, int i) {
        if(i < 0 || i > 4)
            throw new IllegalArgumentException((new StringBuilder()).append("Invalid behavior: ").append(i).toString());
        if(mediaitem == null && mediaitem1 == null)
            throw new IllegalArgumentException("Null media items");
        mUniqueId = s;
        mAfterMediaItem = mediaitem;
        mBeforeMediaItem = mediaitem1;
        mDurationMs = l;
        mBehavior = i;
        mNativeHelper = null;
        if(l > getMaximumDuration())
            throw new IllegalArgumentException("The duration is too large");
        if(mediaitem != null)
            mNativeHelper = mediaitem.getNativeContext();
        else
            mNativeHelper = mediaitem1.getNativeContext();
    }

    public boolean equals(Object obj) {
        boolean flag;
        if(!(obj instanceof Transition))
            flag = false;
        else
            flag = mUniqueId.equals(((Transition)obj).mUniqueId);
        return flag;
    }

    void generate() {
        MediaItem mediaitem = getAfterMediaItem();
        MediaItem mediaitem1 = getBeforeMediaItem();
        MediaArtistNativeHelper.ClipSettings clipsettings = new MediaArtistNativeHelper.ClipSettings();
        MediaArtistNativeHelper.ClipSettings clipsettings1 = new MediaArtistNativeHelper.ClipSettings();
        MediaArtistNativeHelper.EditSettings editsettings = new MediaArtistNativeHelper.EditSettings();
        MediaArtistNativeHelper.TransitionSettings transitionsettings;
        if(mNativeHelper == null)
            if(mediaitem != null)
                mNativeHelper = mediaitem.getNativeContext();
            else
            if(mediaitem1 != null)
                mNativeHelper = mediaitem1.getNativeContext();
        transitionsettings = getTransitionSettings();
        if(mediaitem != null && mediaitem1 != null) {
            clipsettings = mediaitem.getClipSettings();
            clipsettings1 = mediaitem1.getClipSettings();
            clipsettings.beginCutTime = (int)((long)clipsettings.endCutTime - mDurationMs);
            clipsettings1.endCutTime = (int)((long)clipsettings1.beginCutTime + mDurationMs);
            List list2 = isEffectandOverlayOverlapping(mediaitem, clipsettings, 1);
            List list3 = isEffectandOverlayOverlapping(mediaitem1, clipsettings1, 2);
            for(int j1 = 0; j1 < list3.size(); j1++) {
                MediaArtistNativeHelper.EffectSettings effectsettings1 = (MediaArtistNativeHelper.EffectSettings)list3.get(j1);
                effectsettings1.startTime = (int)((long)effectsettings1.startTime + mDurationMs);
            }

            editsettings.effectSettingsArray = new MediaArtistNativeHelper.EffectSettings[list2.size() + list3.size()];
            int k1 = 0;
            int l1;
            for(l1 = 0; k1 < list2.size(); l1++) {
                editsettings.effectSettingsArray[l1] = (MediaArtistNativeHelper.EffectSettings)list2.get(k1);
                k1++;
            }

            for(int i2 = 0; i2 < list3.size();) {
                editsettings.effectSettingsArray[l1] = (MediaArtistNativeHelper.EffectSettings)list3.get(i2);
                i2++;
                l1++;
            }

        } else
        if(mediaitem == null && mediaitem1 != null) {
            mediaitem1.generateBlankFrame(clipsettings);
            clipsettings1 = mediaitem1.getClipSettings();
            clipsettings.endCutTime = (int)(50L + mDurationMs);
            clipsettings1.endCutTime = (int)((long)clipsettings1.beginCutTime + mDurationMs);
            List list1 = isEffectandOverlayOverlapping(mediaitem1, clipsettings1, 2);
            for(int k = 0; k < list1.size(); k++) {
                MediaArtistNativeHelper.EffectSettings effectsettings = (MediaArtistNativeHelper.EffectSettings)list1.get(k);
                effectsettings.startTime = (int)((long)effectsettings.startTime + mDurationMs);
            }

            editsettings.effectSettingsArray = new MediaArtistNativeHelper.EffectSettings[list1.size()];
            int l = 0;
            for(int i1 = 0; l < list1.size(); i1++) {
                editsettings.effectSettingsArray[i1] = (MediaArtistNativeHelper.EffectSettings)list1.get(l);
                l++;
            }

        } else
        if(mediaitem != null && mediaitem1 == null) {
            clipsettings = mediaitem.getClipSettings();
            mediaitem.generateBlankFrame(clipsettings1);
            clipsettings.beginCutTime = (int)((long)clipsettings.endCutTime - mDurationMs);
            clipsettings1.endCutTime = (int)(50L + mDurationMs);
            List list = isEffectandOverlayOverlapping(mediaitem, clipsettings, 1);
            editsettings.effectSettingsArray = new MediaArtistNativeHelper.EffectSettings[list.size()];
            int i = 0;
            for(int j = 0; i < list.size(); j++) {
                editsettings.effectSettingsArray[j] = (MediaArtistNativeHelper.EffectSettings)list.get(i);
                i++;
            }

        }
        editsettings.clipSettingsArray = new MediaArtistNativeHelper.ClipSettings[2];
        editsettings.clipSettingsArray[0] = clipsettings;
        editsettings.clipSettingsArray[1] = clipsettings1;
        editsettings.backgroundMusicSettings = null;
        editsettings.transitionSettingsArray = new MediaArtistNativeHelper.TransitionSettings[1];
        editsettings.transitionSettingsArray[0] = transitionsettings;
        setFilename(mNativeHelper.generateTransitionClip(editsettings, mUniqueId, mediaitem, mediaitem1, this));
    }

    public MediaItem getAfterMediaItem() {
        return mAfterMediaItem;
    }

    public MediaItem getBeforeMediaItem() {
        return mBeforeMediaItem;
    }

    public int getBehavior() {
        return mBehavior;
    }

    public long getDuration() {
        return mDurationMs;
    }

    String getFilename() {
        return mFilename;
    }

    public String getId() {
        return mUniqueId;
    }

    public long getMaximumDuration() {
        long l;
        if(mAfterMediaItem == null)
            l = mBeforeMediaItem.getTimelineDuration() / 2L;
        else
        if(mBeforeMediaItem == null)
            l = mAfterMediaItem.getTimelineDuration() / 2L;
        else
            l = Math.min(mAfterMediaItem.getTimelineDuration(), mBeforeMediaItem.getTimelineDuration()) / 2L;
        return l;
    }

    MediaArtistNativeHelper.TransitionSettings getTransitionSettings() {
        MediaArtistNativeHelper.TransitionSettings transitionsettings;
        transitionsettings = new MediaArtistNativeHelper.TransitionSettings();
        transitionsettings.duration = (int)getDuration();
        if(!(this instanceof TransitionAlpha)) goto _L2; else goto _L1
_L1:
        TransitionAlpha transitionalpha = (TransitionAlpha)this;
        transitionsettings.videoTransitionType = 257;
        transitionsettings.audioTransitionType = 1;
        transitionsettings.transitionBehaviour = mNativeHelper.getVideoTransitionBehaviour(transitionalpha.getBehavior());
        transitionsettings.alphaSettings = new MediaArtistNativeHelper.AlphaMagicSettings();
        transitionsettings.slideSettings = null;
        transitionsettings.alphaSettings.file = transitionalpha.getPNGMaskFilename();
        transitionsettings.alphaSettings.blendingPercent = transitionalpha.getBlendingPercent();
        transitionsettings.alphaSettings.invertRotation = transitionalpha.isInvert();
        transitionsettings.alphaSettings.rgbWidth = transitionalpha.getRGBFileWidth();
        transitionsettings.alphaSettings.rgbHeight = transitionalpha.getRGBFileHeight();
_L4:
        return transitionsettings;
_L2:
        if(this instanceof TransitionSliding) {
            TransitionSliding transitionsliding = (TransitionSliding)this;
            transitionsettings.videoTransitionType = 258;
            transitionsettings.audioTransitionType = 1;
            transitionsettings.transitionBehaviour = mNativeHelper.getVideoTransitionBehaviour(transitionsliding.getBehavior());
            transitionsettings.alphaSettings = null;
            transitionsettings.slideSettings = new MediaArtistNativeHelper.SlideTransitionSettings();
            transitionsettings.slideSettings.direction = mNativeHelper.getSlideSettingsDirection(transitionsliding.getDirection());
        } else
        if(this instanceof TransitionCrossfade) {
            TransitionCrossfade transitioncrossfade = (TransitionCrossfade)this;
            transitionsettings.videoTransitionType = 1;
            transitionsettings.audioTransitionType = 1;
            transitionsettings.transitionBehaviour = mNativeHelper.getVideoTransitionBehaviour(transitioncrossfade.getBehavior());
            transitionsettings.alphaSettings = null;
            transitionsettings.slideSettings = null;
        } else
        if(this instanceof TransitionFadeBlack) {
            TransitionFadeBlack transitionfadeblack = (TransitionFadeBlack)this;
            transitionsettings.videoTransitionType = 259;
            transitionsettings.audioTransitionType = 1;
            transitionsettings.transitionBehaviour = mNativeHelper.getVideoTransitionBehaviour(transitionfadeblack.getBehavior());
            transitionsettings.alphaSettings = null;
            transitionsettings.slideSettings = null;
        }
        if(true) goto _L4; else goto _L3
_L3:
    }

    public int hashCode() {
        return mUniqueId.hashCode();
    }

    void invalidate() {
        if(mFilename != null) {
            (new File(mFilename)).delete();
            mFilename = null;
        }
    }

    List isEffectandOverlayOverlapping(MediaItem mediaitem, MediaArtistNativeHelper.ClipSettings clipsettings, int i) {
        ArrayList arraylist = new ArrayList();
        Iterator iterator = mediaitem.getAllOverlays().iterator();
        do {
            if(!iterator.hasNext())
                break;
            Overlay overlay = (Overlay)iterator.next();
            MediaArtistNativeHelper.EffectSettings effectsettings1 = mNativeHelper.getOverlaySettings((OverlayFrame)overlay);
            mNativeHelper.adjustEffectsStartTimeAndDuration(effectsettings1, clipsettings.beginCutTime, clipsettings.endCutTime);
            if(effectsettings1.duration != 0)
                arraylist.add(effectsettings1);
        } while(true);
        Iterator iterator1 = mediaitem.getAllEffects().iterator();
        do {
            if(!iterator1.hasNext())
                break;
            Effect effect = (Effect)iterator1.next();
            if(effect instanceof EffectColor) {
                MediaArtistNativeHelper.EffectSettings effectsettings = mNativeHelper.getEffectSettings((EffectColor)effect);
                mNativeHelper.adjustEffectsStartTimeAndDuration(effectsettings, clipsettings.beginCutTime, clipsettings.endCutTime);
                if(effectsettings.duration != 0) {
                    if(mediaitem instanceof MediaVideoItem)
                        effectsettings.fiftiesFrameRate = mNativeHelper.GetClosestVideoFrameRate(((MediaVideoItem)mediaitem).getFps());
                    arraylist.add(effectsettings);
                }
            }
        } while(true);
        return arraylist;
    }

    boolean isGenerated() {
        boolean flag;
        if(mFilename != null)
            flag = true;
        else
            flag = false;
        return flag;
    }

    public void setDuration(long l) {
        if(l > getMaximumDuration()) {
            throw new IllegalArgumentException("The duration is too large");
        } else {
            mDurationMs = l;
            invalidate();
            mNativeHelper.setGeneratePreview(true);
            return;
        }
    }

    void setFilename(String s) {
        mFilename = s;
    }

    public static final int BEHAVIOR_LINEAR = 2;
    private static final int BEHAVIOR_MAX_VALUE = 4;
    public static final int BEHAVIOR_MIDDLE_FAST = 4;
    public static final int BEHAVIOR_MIDDLE_SLOW = 3;
    private static final int BEHAVIOR_MIN_VALUE = 0;
    public static final int BEHAVIOR_SPEED_DOWN = 1;
    public static final int BEHAVIOR_SPEED_UP;
    private final MediaItem mAfterMediaItem;
    private final MediaItem mBeforeMediaItem;
    protected final int mBehavior;
    protected long mDurationMs;
    protected String mFilename;
    protected MediaArtistNativeHelper mNativeHelper;
    private final String mUniqueId;
}
