// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.media.videoeditor;


// Referenced classes of package android.media.videoeditor:
//            Transition, MediaItem

public class TransitionSliding extends Transition {

    private TransitionSliding() {
        this(null, null, null, 0L, 0, 0);
    }

    public TransitionSliding(String s, MediaItem mediaitem, MediaItem mediaitem1, long l, int i, int j) {
        super(s, mediaitem, mediaitem1, l, i);
        switch(j) {
        default:
            throw new IllegalArgumentException("Invalid direction");

        case 0: // '\0'
        case 1: // '\001'
        case 2: // '\002'
        case 3: // '\003'
            mSlidingDirection = j;
            break;
        }
    }

    void generate() {
        super.generate();
    }

    public int getDirection() {
        return mSlidingDirection;
    }

    public static final int DIRECTION_BOTTOM_OUT_TOP_IN = 3;
    public static final int DIRECTION_LEFT_OUT_RIGHT_IN = 1;
    public static final int DIRECTION_RIGHT_OUT_LEFT_IN = 0;
    public static final int DIRECTION_TOP_OUT_BOTTOM_IN = 2;
    private final int mSlidingDirection;
}
