// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.media.videoeditor;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.*;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;

// Referenced classes of package android.media.videoeditor:
//            Transition, MediaArtistNativeHelper, MediaItem

public class TransitionAlpha extends Transition {

    private TransitionAlpha() {
        this(null, null, null, 0L, 0, null, 0, false);
    }

    public TransitionAlpha(String s, MediaItem mediaitem, MediaItem mediaitem1, long l, int i, String s1, 
            int j, boolean flag) {
        FileOutputStream fileoutputstream;
        super(s, mediaitem, mediaitem1, l, i);
        android.graphics.BitmapFactory.Options options = new android.graphics.BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        if(!(new File(s1)).exists())
            throw new IllegalArgumentException((new StringBuilder()).append("File not Found ").append(s1).toString());
        BitmapFactory.decodeFile(s1, options);
        mWidth = options.outWidth;
        mHeight = options.outHeight;
        mRGBMaskFile = String.format((new StringBuilder()).append(super.mNativeHelper.getProjectPath()).append("/").append("mask").append(s).append(".rgb").toString(), new Object[0]);
        fileoutputstream = null;
        FileOutputStream fileoutputstream1 = new FileOutputStream(mRGBMaskFile);
        fileoutputstream = fileoutputstream1;
_L2:
        DataOutputStream dataoutputstream = new DataOutputStream(fileoutputstream);
        if(fileoutputstream != null) {
            Bitmap bitmap = BitmapFactory.decodeFile(s1);
            int ai[] = new int[mWidth];
            ByteBuffer bytebuffer = ByteBuffer.allocate(4 * ai.length);
            byte abyte0[] = bytebuffer.array();
            int k = 0;
            while(k < mHeight)  {
                bitmap.getPixels(ai, 0, mWidth, 0, k, mWidth, 1);
                bytebuffer.asIntBuffer().put(ai, 0, mWidth);
                try {
                    dataoutputstream.write(abyte0);
                }
                catch(IOException ioexception1) { }
                k++;
            }
            bitmap.recycle();
            try {
                fileoutputstream.close();
            }
            catch(IOException ioexception) { }
        }
        mMaskFilename = s1;
        mBlendingPercent = j;
        mIsInvert = flag;
        return;
        IOException ioexception2;
        ioexception2;
        if(true) goto _L2; else goto _L1
_L1:
    }

    public void generate() {
        super.generate();
    }

    public int getBlendingPercent() {
        return mBlendingPercent;
    }

    public String getMaskFilename() {
        return mMaskFilename;
    }

    public String getPNGMaskFilename() {
        return mRGBMaskFile;
    }

    public int getRGBFileHeight() {
        return mHeight;
    }

    public int getRGBFileWidth() {
        return mWidth;
    }

    public boolean isInvert() {
        return mIsInvert;
    }

    private final int mBlendingPercent;
    private int mHeight;
    private final boolean mIsInvert;
    private final String mMaskFilename;
    private String mRGBMaskFile;
    private int mWidth;
}
