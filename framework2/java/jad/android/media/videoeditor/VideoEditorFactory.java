// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.media.videoeditor;

import java.io.*;

// Referenced classes of package android.media.videoeditor:
//            VideoEditorImpl, VideoEditor

public class VideoEditorFactory {

    public VideoEditorFactory() {
    }

    public static VideoEditor create(String s) throws IOException {
        File file = new File(s);
        if(!file.exists()) {
            if(!file.mkdirs())
                throw new FileNotFoundException((new StringBuilder()).append("Cannot create project path: ").append(s).toString());
            if(!(new File(file, ".nomedia")).createNewFile())
                throw new FileNotFoundException("Cannot create file .nomedia");
        }
        return new VideoEditorImpl(s);
    }

    public static VideoEditor load(String s, boolean flag) throws IOException {
        VideoEditorImpl videoeditorimpl = new VideoEditorImpl(s);
        if(flag)
            videoeditorimpl.generatePreview(null);
        return videoeditorimpl;
    }
}
