package android.media.effect.effects;

import android.filterpacks.imageproc.SharpenFilter;
import android.media.effect.EffectContext;
import android.media.effect.SingleFilterEffect;

public class SharpenEffect extends SingleFilterEffect
{
    public SharpenEffect(EffectContext paramEffectContext, String paramString)
    {
        super(paramEffectContext, paramString, SharpenFilter.class, "image", "image", new Object[0]);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.media.effect.effects.SharpenEffect
 * JD-Core Version:        0.6.2
 */