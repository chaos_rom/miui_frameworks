package android.media.effect.effects;

import android.filterpacks.imageproc.FillLightFilter;
import android.media.effect.EffectContext;
import android.media.effect.SingleFilterEffect;

public class FillLightEffect extends SingleFilterEffect
{
    public FillLightEffect(EffectContext paramEffectContext, String paramString)
    {
        super(paramEffectContext, paramString, FillLightFilter.class, "image", "image", new Object[0]);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.media.effect.effects.FillLightEffect
 * JD-Core Version:        0.6.2
 */