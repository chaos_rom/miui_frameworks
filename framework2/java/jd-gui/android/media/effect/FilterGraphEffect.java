package android.media.effect;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterGraph;
import android.filterfw.core.GraphRunner;
import android.filterfw.core.SyncRunner;
import android.filterfw.io.GraphIOException;
import android.filterfw.io.TextGraphReader;

public class FilterGraphEffect extends FilterEffect
{
    private static final String TAG = "FilterGraphEffect";
    protected FilterGraph mGraph;
    protected String mInputName;
    protected String mOutputName;
    protected GraphRunner mRunner;
    protected Class mSchedulerClass;

    public FilterGraphEffect(EffectContext paramEffectContext, String paramString1, String paramString2, String paramString3, String paramString4, Class paramClass)
    {
        super(paramEffectContext, paramString1);
        this.mInputName = paramString3;
        this.mOutputName = paramString4;
        this.mSchedulerClass = paramClass;
        createGraph(paramString2);
    }

    private void createGraph(String paramString)
    {
        TextGraphReader localTextGraphReader = new TextGraphReader();
        try
        {
            this.mGraph = localTextGraphReader.readGraphString(paramString);
            if (this.mGraph == null)
                throw new RuntimeException("Could not setup effect");
        }
        catch (GraphIOException localGraphIOException)
        {
            throw new RuntimeException("Could not setup effect", localGraphIOException);
        }
        this.mRunner = new SyncRunner(getFilterContext(), this.mGraph, this.mSchedulerClass);
    }

    public void apply(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        beginGLEffect();
        Filter localFilter1 = this.mGraph.getFilter(this.mInputName);
        if (localFilter1 != null)
        {
            localFilter1.setInputValue("texId", Integer.valueOf(paramInt1));
            localFilter1.setInputValue("width", Integer.valueOf(paramInt2));
            localFilter1.setInputValue("height", Integer.valueOf(paramInt3));
            Filter localFilter2 = this.mGraph.getFilter(this.mOutputName);
            if (localFilter2 == null)
                break label107;
            localFilter2.setInputValue("texId", Integer.valueOf(paramInt4));
        }
        try
        {
            this.mRunner.run();
            endGLEffect();
            return;
            throw new RuntimeException("Internal error applying effect");
            label107: throw new RuntimeException("Internal error applying effect");
        }
        catch (RuntimeException localRuntimeException)
        {
            throw new RuntimeException("Internal error applying effect: ", localRuntimeException);
        }
    }

    public void release()
    {
        this.mGraph.tearDown(getFilterContext());
        this.mGraph = null;
    }

    public void setParameter(String paramString, Object paramObject)
    {
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.media.effect.FilterGraphEffect
 * JD-Core Version:        0.6.2
 */