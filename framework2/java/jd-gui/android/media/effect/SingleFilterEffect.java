package android.media.effect;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterFactory;
import android.filterfw.core.FilterFunction;
import android.filterfw.core.Frame;

public class SingleFilterEffect extends FilterEffect
{
    protected FilterFunction mFunction;
    protected String mInputName;
    protected String mOutputName;

    public SingleFilterEffect(EffectContext paramEffectContext, String paramString1, Class paramClass, String paramString2, String paramString3, Object[] paramArrayOfObject)
    {
        super(paramEffectContext, paramString1);
        this.mInputName = paramString2;
        this.mOutputName = paramString3;
        String str = paramClass.getSimpleName();
        Filter localFilter = FilterFactory.sharedFactory().createFilterByClass(paramClass, str);
        localFilter.initWithAssignmentList(paramArrayOfObject);
        this.mFunction = new FilterFunction(getFilterContext(), localFilter);
    }

    public void apply(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        beginGLEffect();
        Frame localFrame1 = frameFromTexture(paramInt1, paramInt2, paramInt3);
        Frame localFrame2 = frameFromTexture(paramInt4, paramInt2, paramInt3);
        FilterFunction localFilterFunction = this.mFunction;
        Object[] arrayOfObject = new Object[2];
        arrayOfObject[0] = this.mInputName;
        arrayOfObject[1] = localFrame1;
        Frame localFrame3 = localFilterFunction.executeWithArgList(arrayOfObject);
        localFrame2.setDataFromFrame(localFrame3);
        localFrame1.release();
        localFrame2.release();
        localFrame3.release();
        endGLEffect();
    }

    public void release()
    {
        this.mFunction.tearDown();
        this.mFunction = null;
    }

    public void setParameter(String paramString, Object paramObject)
    {
        this.mFunction.setInputValue(paramString, paramObject);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.media.effect.SingleFilterEffect
 * JD-Core Version:        0.6.2
 */