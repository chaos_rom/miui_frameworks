package android.media.effect;

public abstract interface EffectUpdateListener
{
    public abstract void onEffectUpdated(Effect paramEffect, Object paramObject);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.media.effect.EffectUpdateListener
 * JD-Core Version:        0.6.2
 */