package android.media.effect;

public class EffectFactory
{
    public static final String EFFECT_AUTOFIX = "android.media.effect.effects.AutoFixEffect";
    public static final String EFFECT_BACKDROPPER = "android.media.effect.effects.BackDropperEffect";
    public static final String EFFECT_BITMAPOVERLAY = "android.media.effect.effects.BitmapOverlayEffect";
    public static final String EFFECT_BLACKWHITE = "android.media.effect.effects.BlackWhiteEffect";
    public static final String EFFECT_BRIGHTNESS = "android.media.effect.effects.BrightnessEffect";
    public static final String EFFECT_CONTRAST = "android.media.effect.effects.ContrastEffect";
    public static final String EFFECT_CROP = "android.media.effect.effects.CropEffect";
    public static final String EFFECT_CROSSPROCESS = "android.media.effect.effects.CrossProcessEffect";
    public static final String EFFECT_DOCUMENTARY = "android.media.effect.effects.DocumentaryEffect";
    public static final String EFFECT_DUOTONE = "android.media.effect.effects.DuotoneEffect";
    public static final String EFFECT_FILLLIGHT = "android.media.effect.effects.FillLightEffect";
    public static final String EFFECT_FISHEYE = "android.media.effect.effects.FisheyeEffect";
    public static final String EFFECT_FLIP = "android.media.effect.effects.FlipEffect";
    public static final String EFFECT_GRAIN = "android.media.effect.effects.GrainEffect";
    public static final String EFFECT_GRAYSCALE = "android.media.effect.effects.GrayscaleEffect";
    public static final String EFFECT_IDENTITY = "IdentityEffect";
    public static final String EFFECT_LOMOISH = "android.media.effect.effects.LomoishEffect";
    public static final String EFFECT_NEGATIVE = "android.media.effect.effects.NegativeEffect";
    private static final String[] EFFECT_PACKAGES = arrayOfString;
    public static final String EFFECT_POSTERIZE = "android.media.effect.effects.PosterizeEffect";
    public static final String EFFECT_REDEYE = "android.media.effect.effects.RedEyeEffect";
    public static final String EFFECT_ROTATE = "android.media.effect.effects.RotateEffect";
    public static final String EFFECT_SATURATE = "android.media.effect.effects.SaturateEffect";
    public static final String EFFECT_SEPIA = "android.media.effect.effects.SepiaEffect";
    public static final String EFFECT_SHARPEN = "android.media.effect.effects.SharpenEffect";
    public static final String EFFECT_STRAIGHTEN = "android.media.effect.effects.StraightenEffect";
    public static final String EFFECT_TEMPERATURE = "android.media.effect.effects.ColorTemperatureEffect";
    public static final String EFFECT_TINT = "android.media.effect.effects.TintEffect";
    public static final String EFFECT_VIGNETTE = "android.media.effect.effects.VignetteEffect";
    private EffectContext mEffectContext;

    static
    {
        String[] arrayOfString = new String[2];
        arrayOfString[0] = "android.media.effect.effects.";
        arrayOfString[1] = "";
    }

    EffectFactory(EffectContext paramEffectContext)
    {
        this.mEffectContext = paramEffectContext;
    }

    private static Class getEffectClassByName(String paramString)
    {
        Object localObject = null;
        ClassLoader localClassLoader = Thread.currentThread().getContextClassLoader();
        String[] arrayOfString = EFFECT_PACKAGES;
        int i = arrayOfString.length;
        int j = 0;
        while (true)
        {
            String str;
            if (j < i)
                str = arrayOfString[j];
            try
            {
                Class localClass = localClassLoader.loadClass(str + paramString);
                localObject = localClass;
                if (localObject != null)
                    return localObject;
            }
            catch (ClassNotFoundException localClassNotFoundException)
            {
                j++;
            }
        }
    }

    // ERROR //
    private Effect instantiateEffect(Class paramClass, String paramString)
    {
        // Byte code:
        //     0: aload_1
        //     1: ldc 149
        //     3: invokevirtual 155	java/lang/Class:asSubclass	(Ljava/lang/Class;)Ljava/lang/Class;
        //     6: pop
        //     7: iconst_2
        //     8: anewarray 151	java/lang/Class
        //     11: astore 6
        //     13: aload 6
        //     15: iconst_0
        //     16: ldc 157
        //     18: aastore
        //     19: aload 6
        //     21: iconst_1
        //     22: ldc 97
        //     24: aastore
        //     25: aload_1
        //     26: aload 6
        //     28: invokevirtual 161	java/lang/Class:getConstructor	([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
        //     31: astore 7
        //     33: iconst_2
        //     34: anewarray 4	java/lang/Object
        //     37: astore 9
        //     39: aload 9
        //     41: iconst_0
        //     42: aload_0
        //     43: getfield 109	android/media/effect/EffectFactory:mEffectContext	Landroid/media/effect/EffectContext;
        //     46: aastore
        //     47: aload 9
        //     49: iconst_1
        //     50: aload_2
        //     51: aastore
        //     52: aload 7
        //     54: aload 9
        //     56: invokevirtual 167	java/lang/reflect/Constructor:newInstance	([Ljava/lang/Object;)Ljava/lang/Object;
        //     59: checkcast 149	android/media/effect/Effect
        //     62: astore 10
        //     64: aload 10
        //     66: areturn
        //     67: astore_3
        //     68: new 169	java/lang/IllegalArgumentException
        //     71: dup
        //     72: new 125	java/lang/StringBuilder
        //     75: dup
        //     76: invokespecial 126	java/lang/StringBuilder:<init>	()V
        //     79: ldc 171
        //     81: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     84: aload_1
        //     85: invokevirtual 174	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     88: ldc 176
        //     90: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     93: invokevirtual 134	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     96: aload_3
        //     97: invokespecial 179	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
        //     100: athrow
        //     101: astore 5
        //     103: new 181	java/lang/RuntimeException
        //     106: dup
        //     107: new 125	java/lang/StringBuilder
        //     110: dup
        //     111: invokespecial 126	java/lang/StringBuilder:<init>	()V
        //     114: ldc 183
        //     116: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     119: aload_1
        //     120: invokevirtual 174	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     123: ldc 185
        //     125: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     128: ldc 187
        //     130: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     133: invokevirtual 134	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     136: aload 5
        //     138: invokespecial 188	java/lang/RuntimeException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
        //     141: athrow
        //     142: astore 8
        //     144: new 181	java/lang/RuntimeException
        //     147: dup
        //     148: new 125	java/lang/StringBuilder
        //     151: dup
        //     152: invokespecial 126	java/lang/StringBuilder:<init>	()V
        //     155: ldc 190
        //     157: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     160: aload_1
        //     161: invokevirtual 174	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     164: ldc 192
        //     166: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     169: invokevirtual 134	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     172: aload 8
        //     174: invokespecial 188	java/lang/RuntimeException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
        //     177: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     0	7	67	java/lang/ClassCastException
        //     7	33	101	java/lang/NoSuchMethodException
        //     33	64	142	java/lang/Throwable
    }

    public static boolean isEffectSupported(String paramString)
    {
        if (getEffectClassByName(paramString) != null);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public Effect createEffect(String paramString)
    {
        Class localClass = getEffectClassByName(paramString);
        if (localClass == null)
            throw new IllegalArgumentException("Cannot instantiate unknown effect '" + paramString + "'!");
        return instantiateEffect(localClass, paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.media.effect.EffectFactory
 * JD-Core Version:        0.6.2
 */