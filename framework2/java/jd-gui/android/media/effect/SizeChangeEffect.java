package android.media.effect;

import android.filterfw.core.FilterFunction;
import android.filterfw.core.Frame;
import android.filterfw.core.FrameFormat;

public class SizeChangeEffect extends SingleFilterEffect
{
    public SizeChangeEffect(EffectContext paramEffectContext, String paramString1, Class paramClass, String paramString2, String paramString3, Object[] paramArrayOfObject)
    {
        super(paramEffectContext, paramString1, paramClass, paramString2, paramString3, paramArrayOfObject);
    }

    public void apply(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        beginGLEffect();
        Frame localFrame1 = frameFromTexture(paramInt1, paramInt2, paramInt3);
        FilterFunction localFilterFunction = this.mFunction;
        Object[] arrayOfObject = new Object[2];
        arrayOfObject[0] = this.mInputName;
        arrayOfObject[1] = localFrame1;
        Frame localFrame2 = localFilterFunction.executeWithArgList(arrayOfObject);
        Frame localFrame3 = frameFromTexture(paramInt4, localFrame2.getFormat().getWidth(), localFrame2.getFormat().getHeight());
        localFrame3.setDataFromFrame(localFrame2);
        localFrame1.release();
        localFrame3.release();
        localFrame2.release();
        endGLEffect();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.media.effect.SizeChangeEffect
 * JD-Core Version:        0.6.2
 */