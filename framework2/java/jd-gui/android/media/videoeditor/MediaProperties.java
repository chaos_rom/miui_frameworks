package android.media.videoeditor;

import android.util.Pair;

public class MediaProperties
{
    public static final int ACODEC_AAC_LC = 2;
    public static final int ACODEC_AAC_PLUS = 3;
    public static final int ACODEC_AMRNB = 1;
    public static final int ACODEC_AMRWB = 8;
    public static final int ACODEC_ENHANCED_AAC_PLUS = 4;
    public static final int ACODEC_EVRC = 6;
    public static final int ACODEC_MP3 = 5;
    public static final int ACODEC_NO_AUDIO = 0;
    public static final int ACODEC_OGG = 9;
    private static final int[] ASPECT_RATIOS;
    public static final int ASPECT_RATIO_11_9 = 5;
    private static final Pair<Integer, Integer>[] ASPECT_RATIO_11_9_RESOLUTIONS;
    public static final int ASPECT_RATIO_16_9 = 2;
    private static final Pair<Integer, Integer>[] ASPECT_RATIO_16_9_RESOLUTIONS;
    public static final int ASPECT_RATIO_3_2 = 1;
    private static final Pair<Integer, Integer>[] ASPECT_RATIO_3_2_RESOLUTIONS;
    public static final int ASPECT_RATIO_4_3 = 3;
    private static final Pair<Integer, Integer>[] ASPECT_RATIO_4_3_RESOLUTIONS;
    public static final int ASPECT_RATIO_5_3 = 4;
    private static final Pair<Integer, Integer>[] ASPECT_RATIO_5_3_RESOLUTIONS;
    public static final int ASPECT_RATIO_UNDEFINED = 0;
    public static final int AUDIO_MAX_TRACK_COUNT = 1;
    public static final int AUDIO_MAX_VOLUME_PERCENT = 100;
    public static final int BITRATE_128K = 128000;
    public static final int BITRATE_192K = 192000;
    public static final int BITRATE_256K = 256000;
    public static final int BITRATE_28K = 28000;
    public static final int BITRATE_2M = 2000000;
    public static final int BITRATE_384K = 384000;
    public static final int BITRATE_40K = 40000;
    public static final int BITRATE_512K = 512000;
    public static final int BITRATE_5M = 5000000;
    public static final int BITRATE_64K = 64000;
    public static final int BITRATE_800K = 800000;
    public static final int BITRATE_8M = 8000000;
    public static final int BITRATE_96K = 96000;
    public static final int DEFAULT_CHANNEL_COUNT = 2;
    public static final int DEFAULT_SAMPLING_FREQUENCY = 32000;
    public static final int FILE_3GP = 0;
    public static final int FILE_AMR = 2;
    public static final int FILE_JPEG = 5;
    public static final int FILE_M4V = 10;
    public static final int FILE_MP3 = 3;
    public static final int FILE_MP4 = 1;
    public static final int FILE_PNG = 8;
    public static final int FILE_UNSUPPORTED = 255;
    public static final int HEIGHT_1080 = 1080;
    public static final int HEIGHT_144 = 144;
    public static final int HEIGHT_288 = 288;
    public static final int HEIGHT_360 = 360;
    public static final int HEIGHT_480 = 480;
    public static final int HEIGHT_720 = 720;
    public static final int SAMPLES_PER_FRAME_AAC = 1024;
    public static final int SAMPLES_PER_FRAME_AMRNB = 160;
    public static final int SAMPLES_PER_FRAME_AMRWB = 320;
    public static final int SAMPLES_PER_FRAME_MP3 = 1152;
    private static final int[] SUPPORTED_ACODECS;
    private static final int[] SUPPORTED_BITRATES;
    private static final int[] SUPPORTED_VCODECS;
    private static final int[] SUPPORTED_VIDEO_FILE_FORMATS = arrayOfInt5;
    public static final int UNDEFINED_VIDEO_PROFILE = 255;
    public static final int VCODEC_H263 = 1;
    public static final int VCODEC_H264 = 2;
    public static final int VCODEC_MPEG4 = 3;

    static
    {
        int[] arrayOfInt1 = new int[5];
        arrayOfInt1[0] = 1;
        arrayOfInt1[1] = 2;
        arrayOfInt1[2] = 3;
        arrayOfInt1[3] = 4;
        arrayOfInt1[4] = 5;
        ASPECT_RATIOS = arrayOfInt1;
        Pair[] arrayOfPair1 = new Pair[2];
        arrayOfPair1[0] = new Pair(Integer.valueOf(720), Integer.valueOf(480));
        arrayOfPair1[1] = new Pair(Integer.valueOf(1080), Integer.valueOf(720));
        ASPECT_RATIO_3_2_RESOLUTIONS = arrayOfPair1;
        Pair[] arrayOfPair2 = new Pair[2];
        arrayOfPair2[0] = new Pair(Integer.valueOf(640), Integer.valueOf(480));
        arrayOfPair2[1] = new Pair(Integer.valueOf(960), Integer.valueOf(720));
        ASPECT_RATIO_4_3_RESOLUTIONS = arrayOfPair2;
        Pair[] arrayOfPair3 = new Pair[1];
        arrayOfPair3[0] = new Pair(Integer.valueOf(800), Integer.valueOf(480));
        ASPECT_RATIO_5_3_RESOLUTIONS = arrayOfPair3;
        Pair[] arrayOfPair4 = new Pair[2];
        arrayOfPair4[0] = new Pair(Integer.valueOf(176), Integer.valueOf(144));
        arrayOfPair4[1] = new Pair(Integer.valueOf(352), Integer.valueOf(288));
        ASPECT_RATIO_11_9_RESOLUTIONS = arrayOfPair4;
        Pair[] arrayOfPair5 = new Pair[3];
        arrayOfPair5[0] = new Pair(Integer.valueOf(848), Integer.valueOf(480));
        arrayOfPair5[1] = new Pair(Integer.valueOf(1280), Integer.valueOf(720));
        arrayOfPair5[2] = new Pair(Integer.valueOf(1920), Integer.valueOf(1080));
        ASPECT_RATIO_16_9_RESOLUTIONS = arrayOfPair5;
        int[] arrayOfInt2 = new int[13];
        arrayOfInt2[0] = 28000;
        arrayOfInt2[1] = 40000;
        arrayOfInt2[2] = 64000;
        arrayOfInt2[3] = 96000;
        arrayOfInt2[4] = 128000;
        arrayOfInt2[5] = 192000;
        arrayOfInt2[6] = 256000;
        arrayOfInt2[7] = 384000;
        arrayOfInt2[8] = 512000;
        arrayOfInt2[9] = 800000;
        arrayOfInt2[10] = 2000000;
        arrayOfInt2[11] = 5000000;
        arrayOfInt2[12] = 8000000;
        SUPPORTED_BITRATES = arrayOfInt2;
        int[] arrayOfInt3 = new int[3];
        arrayOfInt3[0] = 2;
        arrayOfInt3[1] = 1;
        arrayOfInt3[2] = 3;
        SUPPORTED_VCODECS = arrayOfInt3;
        int[] arrayOfInt4 = new int[3];
        arrayOfInt4[0] = 2;
        arrayOfInt4[1] = 1;
        arrayOfInt4[2] = 8;
        SUPPORTED_ACODECS = arrayOfInt4;
        int[] arrayOfInt5 = new int[3];
        arrayOfInt5[0] = 0;
        arrayOfInt5[1] = 1;
        arrayOfInt5[2] = 10;
    }

    public static int[] getAllSupportedAspectRatios()
    {
        return ASPECT_RATIOS;
    }

    public static int[] getSupportedAudioCodecs()
    {
        return SUPPORTED_ACODECS;
    }

    public static int getSupportedAudioTrackCount()
    {
        return 1;
    }

    public static int getSupportedMaxVolume()
    {
        return 100;
    }

    public static Pair<Integer, Integer>[] getSupportedResolutions(int paramInt)
    {
        Pair[] arrayOfPair1;
        switch (paramInt)
        {
        default:
            throw new IllegalArgumentException("Unknown aspect ratio: " + paramInt);
        case 1:
            arrayOfPair1 = ASPECT_RATIO_3_2_RESOLUTIONS;
        case 3:
        case 4:
        case 5:
        case 2:
        }
        VideoEditorProfile localVideoEditorProfile;
        while (true)
        {
            localVideoEditorProfile = VideoEditorProfile.get();
            if (localVideoEditorProfile != null)
                break;
            throw new RuntimeException("Can't get the video editor profile");
            arrayOfPair1 = ASPECT_RATIO_4_3_RESOLUTIONS;
            continue;
            arrayOfPair1 = ASPECT_RATIO_5_3_RESOLUTIONS;
            continue;
            arrayOfPair1 = ASPECT_RATIO_11_9_RESOLUTIONS;
            continue;
            arrayOfPair1 = ASPECT_RATIO_16_9_RESOLUTIONS;
        }
        int i = localVideoEditorProfile.maxOutputVideoFrameWidth;
        int j = localVideoEditorProfile.maxOutputVideoFrameHeight;
        Pair[] arrayOfPair2 = new Pair[arrayOfPair1.length];
        int k = 0;
        for (int m = 0; m < arrayOfPair1.length; m++)
            if ((((Integer)arrayOfPair1[m].first).intValue() <= i) && (((Integer)arrayOfPair1[m].second).intValue() <= j))
            {
                arrayOfPair2[k] = arrayOfPair1[m];
                k++;
            }
        Pair[] arrayOfPair3 = new Pair[k];
        System.arraycopy(arrayOfPair2, 0, arrayOfPair3, 0, k);
        return arrayOfPair3;
    }

    public static int[] getSupportedVideoBitrates()
    {
        return SUPPORTED_BITRATES;
    }

    public static int[] getSupportedVideoCodecs()
    {
        return SUPPORTED_VCODECS;
    }

    public static int[] getSupportedVideoFileFormat()
    {
        return SUPPORTED_VIDEO_FILE_FORMATS;
    }

    public final class MPEG4Level
    {
        public static final int MPEG4Level0 = 1;
        public static final int MPEG4Level0b = 2;
        public static final int MPEG4Level1 = 4;
        public static final int MPEG4Level2 = 8;
        public static final int MPEG4Level3 = 16;
        public static final int MPEG4Level4 = 32;
        public static final int MPEG4Level4a = 64;
        public static final int MPEG4Level5 = 128;
        public static final int MPEG4LevelUnknown = 2147483647;

        public MPEG4Level()
        {
        }
    }

    public final class MPEG4Profile
    {
        public static final int MPEG4ProfileAdvancedCoding = 4096;
        public static final int MPEG4ProfileAdvancedCore = 8192;
        public static final int MPEG4ProfileAdvancedRealTime = 1024;
        public static final int MPEG4ProfileAdvancedScalable = 16384;
        public static final int MPEG4ProfileAdvancedSimple = 32768;
        public static final int MPEG4ProfileBasicAnimated = 256;
        public static final int MPEG4ProfileCore = 4;
        public static final int MPEG4ProfileCoreScalable = 2048;
        public static final int MPEG4ProfileHybrid = 512;
        public static final int MPEG4ProfileMain = 8;
        public static final int MPEG4ProfileNbit = 16;
        public static final int MPEG4ProfileScalableTexture = 32;
        public static final int MPEG4ProfileSimple = 1;
        public static final int MPEG4ProfileSimpleFBA = 128;
        public static final int MPEG4ProfileSimpleFace = 64;
        public static final int MPEG4ProfileSimpleScalable = 2;
        public static final int MPEG4ProfileUnknown = 2147483647;

        public MPEG4Profile()
        {
        }
    }

    public final class H263Level
    {
        public static final int H263Level10 = 1;
        public static final int H263Level20 = 2;
        public static final int H263Level30 = 4;
        public static final int H263Level40 = 8;
        public static final int H263Level45 = 16;
        public static final int H263Level50 = 32;
        public static final int H263Level60 = 64;
        public static final int H263Level70 = 128;
        public static final int H263LevelUnknown = 2147483647;

        public H263Level()
        {
        }
    }

    public final class H263Profile
    {
        public static final int H263ProfileBackwardCompatible = 4;
        public static final int H263ProfileBaseline = 1;
        public static final int H263ProfileH320Coding = 2;
        public static final int H263ProfileHighCompression = 32;
        public static final int H263ProfileHighLatency = 256;
        public static final int H263ProfileISWV2 = 8;
        public static final int H263ProfileISWV3 = 16;
        public static final int H263ProfileInterlace = 128;
        public static final int H263ProfileInternet = 64;
        public static final int H263ProfileUnknown = 2147483647;

        public H263Profile()
        {
        }
    }

    public final class H264Level
    {
        public static final int H264Level1 = 1;
        public static final int H264Level11 = 4;
        public static final int H264Level12 = 8;
        public static final int H264Level13 = 16;
        public static final int H264Level1b = 2;
        public static final int H264Level2 = 32;
        public static final int H264Level21 = 64;
        public static final int H264Level22 = 128;
        public static final int H264Level3 = 256;
        public static final int H264Level31 = 512;
        public static final int H264Level32 = 1024;
        public static final int H264Level4 = 2048;
        public static final int H264Level41 = 4096;
        public static final int H264Level42 = 8192;
        public static final int H264Level5 = 16384;
        public static final int H264Level51 = 32768;
        public static final int H264LevelUnknown = 2147483647;

        public H264Level()
        {
        }
    }

    public final class H264Profile
    {
        public static final int H264ProfileBaseline = 1;
        public static final int H264ProfileExtended = 4;
        public static final int H264ProfileHigh = 8;
        public static final int H264ProfileHigh10 = 16;
        public static final int H264ProfileHigh422 = 32;
        public static final int H264ProfileHigh444 = 64;
        public static final int H264ProfileMain = 2;
        public static final int H264ProfileUnknown = 2147483647;

        public H264Profile()
        {
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.media.videoeditor.MediaProperties
 * JD-Core Version:        0.6.2
 */