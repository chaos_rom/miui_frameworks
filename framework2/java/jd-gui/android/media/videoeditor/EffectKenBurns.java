package android.media.videoeditor;

import android.graphics.Rect;

public class EffectKenBurns extends Effect
{
    private Rect mEndRect;
    private Rect mStartRect;

    private EffectKenBurns()
    {
        this(null, null, null, null, 0L, 0L);
    }

    public EffectKenBurns(MediaItem paramMediaItem, String paramString, Rect paramRect1, Rect paramRect2, long paramLong1, long paramLong2)
    {
        super(paramMediaItem, paramString, paramLong1, paramLong2);
        if ((paramRect1.width() <= 0) || (paramRect1.height() <= 0))
            throw new IllegalArgumentException("Invalid Start rectangle");
        if ((paramRect2.width() <= 0) || (paramRect2.height() <= 0))
            throw new IllegalArgumentException("Invalid End rectangle");
        this.mStartRect = paramRect1;
        this.mEndRect = paramRect2;
    }

    public Rect getEndRect()
    {
        return this.mEndRect;
    }

    void getKenBurnsSettings(Rect paramRect1, Rect paramRect2)
    {
        paramRect1.left = getStartRect().left;
        paramRect1.top = getStartRect().top;
        paramRect1.right = getStartRect().right;
        paramRect1.bottom = getStartRect().bottom;
        paramRect2.left = getEndRect().left;
        paramRect2.top = getEndRect().top;
        paramRect2.right = getEndRect().right;
        paramRect2.bottom = getEndRect().bottom;
    }

    public Rect getStartRect()
    {
        return this.mStartRect;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.media.videoeditor.EffectKenBurns
 * JD-Core Version:        0.6.2
 */