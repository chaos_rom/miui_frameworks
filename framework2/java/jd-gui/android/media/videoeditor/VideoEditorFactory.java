package android.media.videoeditor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class VideoEditorFactory
{
    public static VideoEditor create(String paramString)
        throws IOException
    {
        File localFile = new File(paramString);
        if (!localFile.exists())
        {
            if (!localFile.mkdirs())
                throw new FileNotFoundException("Cannot create project path: " + paramString);
            if (!new File(localFile, ".nomedia").createNewFile())
                throw new FileNotFoundException("Cannot create file .nomedia");
        }
        return new VideoEditorImpl(paramString);
    }

    public static VideoEditor load(String paramString, boolean paramBoolean)
        throws IOException
    {
        VideoEditorImpl localVideoEditorImpl = new VideoEditorImpl(paramString);
        if (paramBoolean)
            localVideoEditorImpl.generatePreview(null);
        return localVideoEditorImpl;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.media.videoeditor.VideoEditorFactory
 * JD-Core Version:        0.6.2
 */