package android.media.videoeditor;

public class TransitionCrossfade extends Transition
{
    private TransitionCrossfade()
    {
        this(null, null, null, 0L, 0);
    }

    public TransitionCrossfade(String paramString, MediaItem paramMediaItem1, MediaItem paramMediaItem2, long paramLong, int paramInt)
    {
        super(paramString, paramMediaItem1, paramMediaItem2, paramLong, paramInt);
    }

    void generate()
    {
        super.generate();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.media.videoeditor.TransitionCrossfade
 * JD-Core Version:        0.6.2
 */