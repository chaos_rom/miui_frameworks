package android.media.videoeditor;

public class TransitionSliding extends Transition
{
    public static final int DIRECTION_BOTTOM_OUT_TOP_IN = 3;
    public static final int DIRECTION_LEFT_OUT_RIGHT_IN = 1;
    public static final int DIRECTION_RIGHT_OUT_LEFT_IN = 0;
    public static final int DIRECTION_TOP_OUT_BOTTOM_IN = 2;
    private final int mSlidingDirection;

    private TransitionSliding()
    {
        this(null, null, null, 0L, 0, 0);
    }

    public TransitionSliding(String paramString, MediaItem paramMediaItem1, MediaItem paramMediaItem2, long paramLong, int paramInt1, int paramInt2)
    {
        super(paramString, paramMediaItem1, paramMediaItem2, paramLong, paramInt1);
        switch (paramInt2)
        {
        default:
            throw new IllegalArgumentException("Invalid direction");
        case 0:
        case 1:
        case 2:
        case 3:
        }
        this.mSlidingDirection = paramInt2;
    }

    void generate()
    {
        super.generate();
    }

    public int getDirection()
    {
        return this.mSlidingDirection;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.media.videoeditor.TransitionSliding
 * JD-Core Version:        0.6.2
 */