package android.media.videoeditor;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class Transition
{
    public static final int BEHAVIOR_LINEAR = 2;
    private static final int BEHAVIOR_MAX_VALUE = 4;
    public static final int BEHAVIOR_MIDDLE_FAST = 4;
    public static final int BEHAVIOR_MIDDLE_SLOW = 3;
    private static final int BEHAVIOR_MIN_VALUE = 0;
    public static final int BEHAVIOR_SPEED_DOWN = 1;
    public static final int BEHAVIOR_SPEED_UP;
    private final MediaItem mAfterMediaItem;
    private final MediaItem mBeforeMediaItem;
    protected final int mBehavior;
    protected long mDurationMs;
    protected String mFilename;
    protected MediaArtistNativeHelper mNativeHelper;
    private final String mUniqueId;

    private Transition()
    {
        this(null, null, null, 0L, 0);
    }

    protected Transition(String paramString, MediaItem paramMediaItem1, MediaItem paramMediaItem2, long paramLong, int paramInt)
    {
        if ((paramInt < 0) || (paramInt > 4))
            throw new IllegalArgumentException("Invalid behavior: " + paramInt);
        if ((paramMediaItem1 == null) && (paramMediaItem2 == null))
            throw new IllegalArgumentException("Null media items");
        this.mUniqueId = paramString;
        this.mAfterMediaItem = paramMediaItem1;
        this.mBeforeMediaItem = paramMediaItem2;
        this.mDurationMs = paramLong;
        this.mBehavior = paramInt;
        this.mNativeHelper = null;
        if (paramLong > getMaximumDuration())
            throw new IllegalArgumentException("The duration is too large");
        if (paramMediaItem1 != null);
        for (this.mNativeHelper = paramMediaItem1.getNativeContext(); ; this.mNativeHelper = paramMediaItem2.getNativeContext())
            return;
    }

    public boolean equals(Object paramObject)
    {
        if (!(paramObject instanceof Transition));
        for (boolean bool = false; ; bool = this.mUniqueId.equals(((Transition)paramObject).mUniqueId))
            return bool;
    }

    void generate()
    {
        MediaItem localMediaItem1 = getAfterMediaItem();
        MediaItem localMediaItem2 = getBeforeMediaItem();
        MediaArtistNativeHelper.ClipSettings localClipSettings1 = new MediaArtistNativeHelper.ClipSettings();
        MediaArtistNativeHelper.ClipSettings localClipSettings2 = new MediaArtistNativeHelper.ClipSettings();
        MediaArtistNativeHelper.EditSettings localEditSettings = new MediaArtistNativeHelper.EditSettings();
        if (this.mNativeHelper == null)
        {
            if (localMediaItem1 == null)
                break label183;
            this.mNativeHelper = localMediaItem1.getNativeContext();
        }
        MediaArtistNativeHelper.TransitionSettings localTransitionSettings;
        List localList3;
        List localList4;
        while (true)
        {
            localTransitionSettings = getTransitionSettings();
            if ((localMediaItem1 == null) || (localMediaItem2 == null))
                break label312;
            localClipSettings1 = localMediaItem1.getClipSettings();
            localClipSettings2 = localMediaItem2.getClipSettings();
            localClipSettings1.beginCutTime = ((int)(localClipSettings1.endCutTime - this.mDurationMs));
            localClipSettings2.endCutTime = ((int)(localClipSettings2.beginCutTime + this.mDurationMs));
            localList3 = isEffectandOverlayOverlapping(localMediaItem1, localClipSettings1, 1);
            localList4 = isEffectandOverlayOverlapping(localMediaItem2, localClipSettings2, 2);
            for (int i1 = 0; i1 < localList4.size(); i1++)
            {
                MediaArtistNativeHelper.EffectSettings localEffectSettings2 = (MediaArtistNativeHelper.EffectSettings)localList4.get(i1);
                localEffectSettings2.startTime = ((int)(localEffectSettings2.startTime + this.mDurationMs));
            }
            label183: if (localMediaItem2 != null)
                this.mNativeHelper = localMediaItem2.getNativeContext();
        }
        localEditSettings.effectSettingsArray = new MediaArtistNativeHelper.EffectSettings[localList3.size() + localList4.size()];
        int i2 = 0;
        for (int i3 = 0; i2 < localList3.size(); i3++)
        {
            localEditSettings.effectSettingsArray[i3] = ((MediaArtistNativeHelper.EffectSettings)localList3.get(i2));
            i2++;
        }
        int i4 = 0;
        while (i4 < localList4.size())
        {
            localEditSettings.effectSettingsArray[i3] = ((MediaArtistNativeHelper.EffectSettings)localList4.get(i4));
            i4++;
            i3++;
            continue;
            label312: List localList2;
            int m;
            int n;
            if ((localMediaItem1 == null) && (localMediaItem2 != null))
            {
                localMediaItem2.generateBlankFrame(localClipSettings1);
                localClipSettings2 = localMediaItem2.getClipSettings();
                localClipSettings1.endCutTime = ((int)(50L + this.mDurationMs));
                localClipSettings2.endCutTime = ((int)(localClipSettings2.beginCutTime + this.mDurationMs));
                localList2 = isEffectandOverlayOverlapping(localMediaItem2, localClipSettings2, 2);
                for (int k = 0; k < localList2.size(); k++)
                {
                    MediaArtistNativeHelper.EffectSettings localEffectSettings1 = (MediaArtistNativeHelper.EffectSettings)localList2.get(k);
                    localEffectSettings1.startTime = ((int)(localEffectSettings1.startTime + this.mDurationMs));
                }
                localEditSettings.effectSettingsArray = new MediaArtistNativeHelper.EffectSettings[localList2.size()];
                m = 0;
                n = 0;
            }
            while (m < localList2.size())
            {
                localEditSettings.effectSettingsArray[n] = ((MediaArtistNativeHelper.EffectSettings)localList2.get(m));
                m++;
                n++;
                continue;
                if ((localMediaItem1 != null) && (localMediaItem2 == null))
                {
                    localClipSettings1 = localMediaItem1.getClipSettings();
                    localMediaItem1.generateBlankFrame(localClipSettings2);
                    localClipSettings1.beginCutTime = ((int)(localClipSettings1.endCutTime - this.mDurationMs));
                    localClipSettings2.endCutTime = ((int)(50L + this.mDurationMs));
                    List localList1 = isEffectandOverlayOverlapping(localMediaItem1, localClipSettings1, 1);
                    localEditSettings.effectSettingsArray = new MediaArtistNativeHelper.EffectSettings[localList1.size()];
                    int i = 0;
                    for (int j = 0; i < localList1.size(); j++)
                    {
                        localEditSettings.effectSettingsArray[j] = ((MediaArtistNativeHelper.EffectSettings)localList1.get(i));
                        i++;
                    }
                }
            }
        }
        localEditSettings.clipSettingsArray = new MediaArtistNativeHelper.ClipSettings[2];
        localEditSettings.clipSettingsArray[0] = localClipSettings1;
        localEditSettings.clipSettingsArray[1] = localClipSettings2;
        localEditSettings.backgroundMusicSettings = null;
        localEditSettings.transitionSettingsArray = new MediaArtistNativeHelper.TransitionSettings[1];
        localEditSettings.transitionSettingsArray[0] = localTransitionSettings;
        setFilename(this.mNativeHelper.generateTransitionClip(localEditSettings, this.mUniqueId, localMediaItem1, localMediaItem2, this));
    }

    public MediaItem getAfterMediaItem()
    {
        return this.mAfterMediaItem;
    }

    public MediaItem getBeforeMediaItem()
    {
        return this.mBeforeMediaItem;
    }

    public int getBehavior()
    {
        return this.mBehavior;
    }

    public long getDuration()
    {
        return this.mDurationMs;
    }

    String getFilename()
    {
        return this.mFilename;
    }

    public String getId()
    {
        return this.mUniqueId;
    }

    public long getMaximumDuration()
    {
        long l;
        if (this.mAfterMediaItem == null)
            l = this.mBeforeMediaItem.getTimelineDuration() / 2L;
        while (true)
        {
            return l;
            if (this.mBeforeMediaItem == null)
                l = this.mAfterMediaItem.getTimelineDuration() / 2L;
            else
                l = Math.min(this.mAfterMediaItem.getTimelineDuration(), this.mBeforeMediaItem.getTimelineDuration()) / 2L;
        }
    }

    MediaArtistNativeHelper.TransitionSettings getTransitionSettings()
    {
        MediaArtistNativeHelper.TransitionSettings localTransitionSettings = new MediaArtistNativeHelper.TransitionSettings();
        localTransitionSettings.duration = ((int)getDuration());
        if ((this instanceof TransitionAlpha))
        {
            TransitionAlpha localTransitionAlpha = (TransitionAlpha)this;
            localTransitionSettings.videoTransitionType = 257;
            localTransitionSettings.audioTransitionType = 1;
            localTransitionSettings.transitionBehaviour = this.mNativeHelper.getVideoTransitionBehaviour(localTransitionAlpha.getBehavior());
            localTransitionSettings.alphaSettings = new MediaArtistNativeHelper.AlphaMagicSettings();
            localTransitionSettings.slideSettings = null;
            localTransitionSettings.alphaSettings.file = localTransitionAlpha.getPNGMaskFilename();
            localTransitionSettings.alphaSettings.blendingPercent = localTransitionAlpha.getBlendingPercent();
            localTransitionSettings.alphaSettings.invertRotation = localTransitionAlpha.isInvert();
            localTransitionSettings.alphaSettings.rgbWidth = localTransitionAlpha.getRGBFileWidth();
            localTransitionSettings.alphaSettings.rgbHeight = localTransitionAlpha.getRGBFileHeight();
        }
        while (true)
        {
            return localTransitionSettings;
            if ((this instanceof TransitionSliding))
            {
                TransitionSliding localTransitionSliding = (TransitionSliding)this;
                localTransitionSettings.videoTransitionType = 258;
                localTransitionSettings.audioTransitionType = 1;
                localTransitionSettings.transitionBehaviour = this.mNativeHelper.getVideoTransitionBehaviour(localTransitionSliding.getBehavior());
                localTransitionSettings.alphaSettings = null;
                localTransitionSettings.slideSettings = new MediaArtistNativeHelper.SlideTransitionSettings();
                localTransitionSettings.slideSettings.direction = this.mNativeHelper.getSlideSettingsDirection(localTransitionSliding.getDirection());
            }
            else if ((this instanceof TransitionCrossfade))
            {
                TransitionCrossfade localTransitionCrossfade = (TransitionCrossfade)this;
                localTransitionSettings.videoTransitionType = 1;
                localTransitionSettings.audioTransitionType = 1;
                localTransitionSettings.transitionBehaviour = this.mNativeHelper.getVideoTransitionBehaviour(localTransitionCrossfade.getBehavior());
                localTransitionSettings.alphaSettings = null;
                localTransitionSettings.slideSettings = null;
            }
            else if ((this instanceof TransitionFadeBlack))
            {
                TransitionFadeBlack localTransitionFadeBlack = (TransitionFadeBlack)this;
                localTransitionSettings.videoTransitionType = 259;
                localTransitionSettings.audioTransitionType = 1;
                localTransitionSettings.transitionBehaviour = this.mNativeHelper.getVideoTransitionBehaviour(localTransitionFadeBlack.getBehavior());
                localTransitionSettings.alphaSettings = null;
                localTransitionSettings.slideSettings = null;
            }
        }
    }

    public int hashCode()
    {
        return this.mUniqueId.hashCode();
    }

    void invalidate()
    {
        if (this.mFilename != null)
        {
            new File(this.mFilename).delete();
            this.mFilename = null;
        }
    }

    List<MediaArtistNativeHelper.EffectSettings> isEffectandOverlayOverlapping(MediaItem paramMediaItem, MediaArtistNativeHelper.ClipSettings paramClipSettings, int paramInt)
    {
        ArrayList localArrayList = new ArrayList();
        Iterator localIterator1 = paramMediaItem.getAllOverlays().iterator();
        while (localIterator1.hasNext())
        {
            Overlay localOverlay = (Overlay)localIterator1.next();
            MediaArtistNativeHelper.EffectSettings localEffectSettings2 = this.mNativeHelper.getOverlaySettings((OverlayFrame)localOverlay);
            this.mNativeHelper.adjustEffectsStartTimeAndDuration(localEffectSettings2, paramClipSettings.beginCutTime, paramClipSettings.endCutTime);
            if (localEffectSettings2.duration != 0)
                localArrayList.add(localEffectSettings2);
        }
        Iterator localIterator2 = paramMediaItem.getAllEffects().iterator();
        while (localIterator2.hasNext())
        {
            Effect localEffect = (Effect)localIterator2.next();
            if ((localEffect instanceof EffectColor))
            {
                MediaArtistNativeHelper.EffectSettings localEffectSettings1 = this.mNativeHelper.getEffectSettings((EffectColor)localEffect);
                this.mNativeHelper.adjustEffectsStartTimeAndDuration(localEffectSettings1, paramClipSettings.beginCutTime, paramClipSettings.endCutTime);
                if (localEffectSettings1.duration != 0)
                {
                    if ((paramMediaItem instanceof MediaVideoItem))
                        localEffectSettings1.fiftiesFrameRate = this.mNativeHelper.GetClosestVideoFrameRate(((MediaVideoItem)paramMediaItem).getFps());
                    localArrayList.add(localEffectSettings1);
                }
            }
        }
        return localArrayList;
    }

    boolean isGenerated()
    {
        if (this.mFilename != null);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void setDuration(long paramLong)
    {
        if (paramLong > getMaximumDuration())
            throw new IllegalArgumentException("The duration is too large");
        this.mDurationMs = paramLong;
        invalidate();
        this.mNativeHelper.setGeneratePreview(true);
    }

    void setFilename(String paramString)
    {
        this.mFilename = paramString;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.media.videoeditor.Transition
 * JD-Core Version:        0.6.2
 */