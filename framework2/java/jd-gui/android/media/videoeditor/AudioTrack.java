package android.media.videoeditor;

import java.io.File;
import java.io.IOException;
import java.lang.ref.SoftReference;

public class AudioTrack
{
    private final int mAudioBitrate;
    private final int mAudioChannels;
    private final int mAudioSamplingFrequency;
    private final int mAudioType;
    private String mAudioWaveformFilename;
    private long mBeginBoundaryTimeMs;
    private int mDuckedTrackVolume;
    private int mDuckingThreshold;
    private final long mDurationMs;
    private long mEndBoundaryTimeMs;
    private final String mFilename;
    private boolean mIsDuckingEnabled;
    private boolean mLoop;
    private final MediaArtistNativeHelper mMANativeHelper;
    private boolean mMuted;
    private long mStartTimeMs;
    private long mTimelineDurationMs;
    private final String mUniqueId;
    private int mVolumePercent;
    private SoftReference<WaveformData> mWaveformData;

    private AudioTrack()
        throws IOException
    {
        this(null, null, null);
    }

    public AudioTrack(VideoEditor paramVideoEditor, String paramString1, String paramString2)
        throws IOException
    {
        this(paramVideoEditor, paramString1, paramString2, 0L, 0L, -1L, false, 100, false, false, 0, 0, null);
    }

    AudioTrack(VideoEditor paramVideoEditor, String paramString1, String paramString2, long paramLong1, long paramLong2, long paramLong3, boolean paramBoolean1, int paramInt1, boolean paramBoolean2, boolean paramBoolean3, int paramInt2, int paramInt3, String paramString3)
        throws IOException
    {
        File localFile = new File(paramString2);
        if (!localFile.exists())
            throw new IOException(paramString2 + " not found ! ");
        if (2147483648L <= localFile.length())
            throw new IllegalArgumentException("File size is more than 2GB");
        if ((paramVideoEditor instanceof VideoEditorImpl))
            this.mMANativeHelper = ((VideoEditorImpl)paramVideoEditor).getNativeContext();
        MediaArtistNativeHelper.Properties localProperties;
        try
        {
            localProperties = this.mMANativeHelper.getMediaProperties(paramString2);
            int i = this.mMANativeHelper.getFileType(localProperties.fileType);
            switch (i)
            {
            default:
                throw new IllegalArgumentException("Unsupported input file type: " + i);
                throw new IllegalArgumentException("editor is not of type VideoEditorImpl");
            case 0:
            case 1:
            case 2:
            case 3:
            }
        }
        catch (Exception localException)
        {
            throw new IllegalArgumentException(localException.getMessage() + " : " + paramString2);
        }
        switch (this.mMANativeHelper.getAudioCodecType(localProperties.audioFormat))
        {
        case 3:
        case 4:
        case 6:
        case 7:
        default:
            throw new IllegalArgumentException("Unsupported Audio Codec Format in Input File");
        case 1:
        case 2:
        case 5:
        case 8:
        }
        if (paramLong3 == -1L)
            paramLong3 = localProperties.audioDuration;
        this.mUniqueId = paramString1;
        this.mFilename = paramString2;
        this.mStartTimeMs = paramLong1;
        this.mDurationMs = localProperties.audioDuration;
        this.mAudioChannels = localProperties.audioChannels;
        this.mAudioBitrate = localProperties.audioBitrate;
        this.mAudioSamplingFrequency = localProperties.audioSamplingFrequency;
        this.mAudioType = localProperties.audioFormat;
        this.mTimelineDurationMs = (paramLong3 - paramLong2);
        this.mVolumePercent = paramInt1;
        this.mBeginBoundaryTimeMs = paramLong2;
        this.mEndBoundaryTimeMs = paramLong3;
        this.mLoop = paramBoolean1;
        this.mMuted = paramBoolean2;
        this.mIsDuckingEnabled = paramBoolean3;
        this.mDuckingThreshold = paramInt2;
        this.mDuckedTrackVolume = paramInt3;
        this.mAudioWaveformFilename = paramString3;
        if (paramString3 != null);
        for (this.mWaveformData = new SoftReference(new WaveformData(paramString3)); ; this.mWaveformData = null)
            return;
    }

    public void disableDucking()
    {
        if (this.mIsDuckingEnabled)
        {
            this.mMANativeHelper.setGeneratePreview(true);
            this.mIsDuckingEnabled = false;
        }
    }

    public void disableLoop()
    {
        if (this.mLoop)
        {
            this.mMANativeHelper.setGeneratePreview(true);
            this.mLoop = false;
        }
    }

    public void enableDucking(int paramInt1, int paramInt2)
    {
        if ((paramInt1 < 0) || (paramInt1 > 90))
            throw new IllegalArgumentException("Invalid threshold value: " + paramInt1);
        if ((paramInt2 < 0) || (paramInt2 > 100))
            throw new IllegalArgumentException("Invalid duckedTrackVolume value: " + paramInt2);
        this.mMANativeHelper.setGeneratePreview(true);
        this.mDuckingThreshold = paramInt1;
        this.mDuckedTrackVolume = paramInt2;
        this.mIsDuckingEnabled = true;
    }

    public void enableLoop()
    {
        if (!this.mLoop)
        {
            this.mMANativeHelper.setGeneratePreview(true);
            this.mLoop = true;
        }
    }

    public boolean equals(Object paramObject)
    {
        if (!(paramObject instanceof AudioTrack));
        for (boolean bool = false; ; bool = this.mUniqueId.equals(((AudioTrack)paramObject).mUniqueId))
            return bool;
    }

    public void extractAudioWaveform(ExtractAudioWaveformProgressListener paramExtractAudioWaveformProgressListener)
        throws IOException
    {
        String str2;
        int j;
        int k;
        if (this.mAudioWaveformFilename == null)
        {
            String str1 = this.mMANativeHelper.getProjectPath();
            str2 = String.format(str1 + "/audioWaveformFile-" + getId() + ".dat", new Object[0]);
            int i = this.mMANativeHelper.getAudioCodecType(this.mAudioType);
            switch (i)
            {
            case 3:
            case 4:
            case 6:
            case 7:
            default:
                throw new IllegalStateException("Unsupported codec type: " + i);
            case 1:
                j = 5;
                k = 160;
            case 8:
            case 2:
            case 5:
            }
        }
        while (true)
        {
            this.mMANativeHelper.generateAudioGraph(this.mUniqueId, this.mFilename, str2, j, 2, k, paramExtractAudioWaveformProgressListener, false);
            this.mAudioWaveformFilename = str2;
            this.mWaveformData = new SoftReference(new WaveformData(this.mAudioWaveformFilename));
            return;
            j = 10;
            k = 320;
            continue;
            j = 32;
            k = 1024;
            continue;
            j = 36;
            k = 1152;
        }
    }

    public int getAudioBitrate()
    {
        return this.mAudioBitrate;
    }

    public int getAudioChannels()
    {
        return this.mAudioChannels;
    }

    public int getAudioSamplingFrequency()
    {
        return this.mAudioSamplingFrequency;
    }

    public int getAudioType()
    {
        return this.mAudioType;
    }

    String getAudioWaveformFilename()
    {
        return this.mAudioWaveformFilename;
    }

    public long getBoundaryBeginTime()
    {
        return this.mBeginBoundaryTimeMs;
    }

    public long getBoundaryEndTime()
    {
        return this.mEndBoundaryTimeMs;
    }

    public int getDuckedTrackVolume()
    {
        return this.mDuckedTrackVolume;
    }

    public int getDuckingThreshhold()
    {
        return this.mDuckingThreshold;
    }

    public long getDuration()
    {
        return this.mDurationMs;
    }

    public String getFilename()
    {
        return this.mFilename;
    }

    public String getId()
    {
        return this.mUniqueId;
    }

    public long getStartTime()
    {
        return this.mStartTimeMs;
    }

    public long getTimelineDuration()
    {
        return this.mTimelineDurationMs;
    }

    public int getVolume()
    {
        return this.mVolumePercent;
    }

    public WaveformData getWaveformData()
        throws IOException
    {
        WaveformData localWaveformData;
        if (this.mWaveformData == null)
            localWaveformData = null;
        while (true)
        {
            return localWaveformData;
            localWaveformData = (WaveformData)this.mWaveformData.get();
            if (localWaveformData == null)
                if (this.mAudioWaveformFilename != null)
                    try
                    {
                        localWaveformData = new WaveformData(this.mAudioWaveformFilename);
                        this.mWaveformData = new SoftReference(localWaveformData);
                    }
                    catch (IOException localIOException)
                    {
                        throw localIOException;
                    }
                else
                    localWaveformData = null;
        }
    }

    public int hashCode()
    {
        return this.mUniqueId.hashCode();
    }

    void invalidate()
    {
        if (this.mAudioWaveformFilename != null)
        {
            new File(this.mAudioWaveformFilename).delete();
            this.mAudioWaveformFilename = null;
            this.mWaveformData = null;
        }
    }

    public boolean isDuckingEnabled()
    {
        return this.mIsDuckingEnabled;
    }

    public boolean isLooping()
    {
        return this.mLoop;
    }

    public boolean isMuted()
    {
        return this.mMuted;
    }

    public void setExtractBoundaries(long paramLong1, long paramLong2)
    {
        if (paramLong1 > this.mDurationMs)
            throw new IllegalArgumentException("Invalid start time");
        if (paramLong2 > this.mDurationMs)
            throw new IllegalArgumentException("Invalid end time");
        if (paramLong1 < 0L)
            throw new IllegalArgumentException("Invalid start time; is < 0");
        if (paramLong2 < 0L)
            throw new IllegalArgumentException("Invalid end time; is < 0");
        this.mMANativeHelper.setGeneratePreview(true);
        this.mBeginBoundaryTimeMs = paramLong1;
        this.mEndBoundaryTimeMs = paramLong2;
        this.mTimelineDurationMs = (this.mEndBoundaryTimeMs - this.mBeginBoundaryTimeMs);
    }

    public void setMute(boolean paramBoolean)
    {
        this.mMANativeHelper.setGeneratePreview(true);
        this.mMuted = paramBoolean;
    }

    public void setVolume(int paramInt)
    {
        if (paramInt > 100)
            throw new IllegalArgumentException("Volume set exceeds maximum allowed value");
        if (paramInt < 0)
            throw new IllegalArgumentException("Invalid Volume ");
        this.mMANativeHelper.setGeneratePreview(true);
        this.mVolumePercent = paramInt;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.media.videoeditor.AudioTrack
 * JD-Core Version:        0.6.2
 */