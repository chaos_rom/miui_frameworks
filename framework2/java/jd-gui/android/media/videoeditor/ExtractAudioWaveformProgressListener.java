package android.media.videoeditor;

public abstract interface ExtractAudioWaveformProgressListener
{
    public abstract void onProgress(int paramInt);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.media.videoeditor.ExtractAudioWaveformProgressListener
 * JD-Core Version:        0.6.2
 */