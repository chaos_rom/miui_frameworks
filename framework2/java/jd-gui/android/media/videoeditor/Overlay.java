package android.media.videoeditor;

import java.util.HashMap;
import java.util.Map;

public abstract class Overlay
{
    protected long mDurationMs;
    private final MediaItem mMediaItem;
    protected long mStartTimeMs;
    private final String mUniqueId;
    private final Map<String, String> mUserAttributes;

    private Overlay()
    {
        this(null, null, 0L, 0L);
    }

    public Overlay(MediaItem paramMediaItem, String paramString, long paramLong1, long paramLong2)
    {
        if (paramMediaItem == null)
            throw new IllegalArgumentException("Media item cannot be null");
        if ((paramLong1 < 0L) || (paramLong2 < 0L))
            throw new IllegalArgumentException("Invalid start time and/OR duration");
        if (paramLong1 + paramLong2 > paramMediaItem.getDuration())
            throw new IllegalArgumentException("Invalid start time and duration");
        this.mMediaItem = paramMediaItem;
        this.mUniqueId = paramString;
        this.mStartTimeMs = paramLong1;
        this.mDurationMs = paramLong2;
        this.mUserAttributes = new HashMap();
    }

    public boolean equals(Object paramObject)
    {
        if (!(paramObject instanceof Overlay));
        for (boolean bool = false; ; bool = this.mUniqueId.equals(((Overlay)paramObject).mUniqueId))
            return bool;
    }

    public long getDuration()
    {
        return this.mDurationMs;
    }

    public String getId()
    {
        return this.mUniqueId;
    }

    public MediaItem getMediaItem()
    {
        return this.mMediaItem;
    }

    public long getStartTime()
    {
        return this.mStartTimeMs;
    }

    public Map<String, String> getUserAttributes()
    {
        return this.mUserAttributes;
    }

    public int hashCode()
    {
        return this.mUniqueId.hashCode();
    }

    public void setDuration(long paramLong)
    {
        if (paramLong < 0L)
            throw new IllegalArgumentException("Invalid duration");
        if (paramLong + this.mStartTimeMs > this.mMediaItem.getDuration())
            throw new IllegalArgumentException("Duration is too large");
        getMediaItem().getNativeContext().setGeneratePreview(true);
        long l = this.mDurationMs;
        this.mDurationMs = paramLong;
        this.mMediaItem.invalidateTransitions(this.mStartTimeMs, l, this.mStartTimeMs, this.mDurationMs);
    }

    public void setStartTime(long paramLong)
    {
        if (paramLong + this.mDurationMs > this.mMediaItem.getDuration())
            throw new IllegalArgumentException("Start time is too large");
        getMediaItem().getNativeContext().setGeneratePreview(true);
        long l = this.mStartTimeMs;
        this.mStartTimeMs = paramLong;
        this.mMediaItem.invalidateTransitions(l, this.mDurationMs, this.mStartTimeMs, this.mDurationMs);
    }

    public void setStartTimeAndDuration(long paramLong1, long paramLong2)
    {
        if (paramLong1 + paramLong2 > this.mMediaItem.getDuration())
            throw new IllegalArgumentException("Invalid start time or duration");
        getMediaItem().getNativeContext().setGeneratePreview(true);
        long l1 = this.mStartTimeMs;
        long l2 = this.mDurationMs;
        this.mStartTimeMs = paramLong1;
        this.mDurationMs = paramLong2;
        this.mMediaItem.invalidateTransitions(l1, l2, this.mStartTimeMs, this.mDurationMs);
    }

    public void setUserAttribute(String paramString1, String paramString2)
    {
        this.mUserAttributes.put(paramString1, paramString2);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.media.videoeditor.Overlay
 * JD-Core Version:        0.6.2
 */