package android.media.videoeditor;

import android.graphics.Bitmap;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class MediaItem
{
    public static final int END_OF_FILE = -1;
    public static final int RENDERING_MODE_BLACK_BORDER = 0;
    public static final int RENDERING_MODE_CROPPING = 2;
    public static final int RENDERING_MODE_STRETCH = 1;
    protected Transition mBeginTransition;
    private String mBlankFrameFilename = null;
    private boolean mBlankFrameGenerated = false;
    private final List<Effect> mEffects;
    protected Transition mEndTransition;
    protected final String mFilename;
    protected String mGeneratedImageClip;
    private final MediaArtistNativeHelper mMANativeHelper;
    private final List<Overlay> mOverlays;
    private final String mProjectPath;
    protected boolean mRegenerateClip;
    private int mRenderingMode;
    private final String mUniqueId;

    protected MediaItem(VideoEditor paramVideoEditor, String paramString1, String paramString2, int paramInt)
        throws IOException
    {
        if (paramString2 == null)
            throw new IllegalArgumentException("MediaItem : filename is null");
        File localFile = new File(paramString2);
        if (!localFile.exists())
            throw new IOException(paramString2 + " not found ! ");
        if (2147483648L <= localFile.length())
            throw new IllegalArgumentException("File size is more than 2GB");
        this.mUniqueId = paramString1;
        this.mFilename = paramString2;
        this.mRenderingMode = paramInt;
        this.mEffects = new ArrayList();
        this.mOverlays = new ArrayList();
        this.mBeginTransition = null;
        this.mEndTransition = null;
        this.mMANativeHelper = ((VideoEditorImpl)paramVideoEditor).getNativeContext();
        this.mProjectPath = paramVideoEditor.getPath();
        this.mRegenerateClip = false;
        this.mGeneratedImageClip = null;
    }

    public void addEffect(Effect paramEffect)
    {
        if (paramEffect == null)
            throw new IllegalArgumentException("NULL effect cannot be applied");
        if (paramEffect.getMediaItem() != this)
            throw new IllegalArgumentException("Media item mismatch");
        if (this.mEffects.contains(paramEffect))
            throw new IllegalArgumentException("Effect already exists: " + paramEffect.getId());
        if (paramEffect.getStartTime() + paramEffect.getDuration() > getDuration())
            throw new IllegalArgumentException("Effect start time + effect duration > media clip duration");
        this.mMANativeHelper.setGeneratePreview(true);
        this.mEffects.add(paramEffect);
        invalidateTransitions(paramEffect.getStartTime(), paramEffect.getDuration());
        if ((paramEffect instanceof EffectKenBurns))
            this.mRegenerateClip = true;
    }

    public void addOverlay(Overlay paramOverlay)
        throws FileNotFoundException, IOException
    {
        if (paramOverlay == null)
            throw new IllegalArgumentException("NULL Overlay cannot be applied");
        if (paramOverlay.getMediaItem() != this)
            throw new IllegalArgumentException("Media item mismatch");
        if (this.mOverlays.contains(paramOverlay))
            throw new IllegalArgumentException("Overlay already exists: " + paramOverlay.getId());
        if (paramOverlay.getStartTime() + paramOverlay.getDuration() > getDuration())
            throw new IllegalArgumentException("Overlay start time + overlay duration > media clip duration");
        if ((paramOverlay instanceof OverlayFrame))
        {
            Bitmap localBitmap = ((OverlayFrame)paramOverlay).getBitmap();
            if (localBitmap == null)
                throw new IllegalArgumentException("Overlay bitmap not specified");
            int i;
            if ((this instanceof MediaVideoItem))
                i = getWidth();
            for (int j = getHeight(); (localBitmap.getWidth() != i) || (localBitmap.getHeight() != j); j = ((MediaImageItem)this).getScaledHeight())
            {
                throw new IllegalArgumentException("Bitmap dimensions must match media item dimensions");
                i = ((MediaImageItem)this).getScaledWidth();
            }
            this.mMANativeHelper.setGeneratePreview(true);
            ((OverlayFrame)paramOverlay).save(this.mProjectPath);
            this.mOverlays.add(paramOverlay);
            invalidateTransitions(paramOverlay.getStartTime(), paramOverlay.getDuration());
            return;
        }
        throw new IllegalArgumentException("Overlay not supported");
    }

    protected void adjustTransitions()
    {
        if (this.mBeginTransition != null)
        {
            long l2 = this.mBeginTransition.getMaximumDuration();
            if (this.mBeginTransition.getDuration() > l2)
                this.mBeginTransition.setDuration(l2);
        }
        if (this.mEndTransition != null)
        {
            long l1 = this.mEndTransition.getMaximumDuration();
            if (this.mEndTransition.getDuration() > l1)
                this.mEndTransition.setDuration(l1);
        }
    }

    public boolean equals(Object paramObject)
    {
        if (!(paramObject instanceof MediaItem));
        for (boolean bool = false; ; bool = this.mUniqueId.equals(((MediaItem)paramObject).mUniqueId))
            return bool;
    }

    void generateBlankFrame(MediaArtistNativeHelper.ClipSettings paramClipSettings)
    {
        Object localObject;
        if (!this.mBlankFrameGenerated)
        {
            this.mBlankFrameFilename = String.format(this.mProjectPath + "/" + "ghost.rgb", new Object[0]);
            localObject = null;
        }
        try
        {
            FileOutputStream localFileOutputStream = new FileOutputStream(this.mBlankFrameFilename);
            localObject = localFileOutputStream;
            label61: DataOutputStream localDataOutputStream = new DataOutputStream(localObject);
            int[] arrayOfInt = new int[64];
            ByteBuffer localByteBuffer = ByteBuffer.allocate(4 * arrayOfInt.length);
            byte[] arrayOfByte = localByteBuffer.array();
            int i = 0;
            while (true)
            {
                if (i < 64)
                    localByteBuffer.asIntBuffer().put(arrayOfInt, 0, 64);
                try
                {
                    localDataOutputStream.write(arrayOfByte);
                    i++;
                }
                catch (IOException localIOException2)
                {
                    try
                    {
                        localObject.close();
                        label135: this.mBlankFrameGenerated = true;
                        paramClipSettings.clipPath = this.mBlankFrameFilename;
                        paramClipSettings.fileType = 5;
                        paramClipSettings.beginCutTime = 0;
                        paramClipSettings.endCutTime = 0;
                        paramClipSettings.mediaRendering = 0;
                        paramClipSettings.rgbWidth = 64;
                        paramClipSettings.rgbHeight = 64;
                        return;
                        localIOException2 = localIOException2;
                    }
                    catch (IOException localIOException1)
                    {
                        break label135;
                    }
                }
            }
        }
        catch (IOException localIOException3)
        {
            break label61;
        }
    }

    public List<Effect> getAllEffects()
    {
        return this.mEffects;
    }

    public List<Overlay> getAllOverlays()
    {
        return this.mOverlays;
    }

    public abstract int getAspectRatio();

    public Transition getBeginTransition()
    {
        return this.mBeginTransition;
    }

    MediaArtistNativeHelper.ClipSettings getClipSettings()
    {
        MediaArtistNativeHelper.ClipSettings localClipSettings = new MediaArtistNativeHelper.ClipSettings();
        initClipSettings(localClipSettings);
        if ((this instanceof MediaVideoItem))
        {
            MediaVideoItem localMediaVideoItem = (MediaVideoItem)this;
            localClipSettings.clipPath = localMediaVideoItem.getFilename();
            localClipSettings.fileType = this.mMANativeHelper.getMediaItemFileType(localMediaVideoItem.getFileType());
            localClipSettings.beginCutTime = ((int)localMediaVideoItem.getBoundaryBeginTime());
            localClipSettings.endCutTime = ((int)localMediaVideoItem.getBoundaryEndTime());
            localClipSettings.mediaRendering = this.mMANativeHelper.getMediaItemRenderingMode(localMediaVideoItem.getRenderingMode());
        }
        while (true)
        {
            return localClipSettings;
            if ((this instanceof MediaImageItem))
                localClipSettings = ((MediaImageItem)this).getImageClipProperties();
        }
    }

    public abstract long getDuration();

    public Effect getEffect(String paramString)
    {
        Iterator localIterator = this.mEffects.iterator();
        Effect localEffect;
        do
        {
            if (!localIterator.hasNext())
                break;
            localEffect = (Effect)localIterator.next();
        }
        while (!localEffect.getId().equals(paramString));
        while (true)
        {
            return localEffect;
            localEffect = null;
        }
    }

    public Transition getEndTransition()
    {
        return this.mEndTransition;
    }

    public abstract int getFileType();

    public String getFilename()
    {
        return this.mFilename;
    }

    String getGeneratedImageClip()
    {
        return this.mGeneratedImageClip;
    }

    public abstract int getHeight();

    public String getId()
    {
        return this.mUniqueId;
    }

    MediaArtistNativeHelper getNativeContext()
    {
        return this.mMANativeHelper;
    }

    public Overlay getOverlay(String paramString)
    {
        Iterator localIterator = this.mOverlays.iterator();
        Overlay localOverlay;
        do
        {
            if (!localIterator.hasNext())
                break;
            localOverlay = (Overlay)localIterator.next();
        }
        while (!localOverlay.getId().equals(paramString));
        while (true)
        {
            return localOverlay;
            localOverlay = null;
        }
    }

    boolean getRegenerateClip()
    {
        return this.mRegenerateClip;
    }

    public int getRenderingMode()
    {
        return this.mRenderingMode;
    }

    public abstract Bitmap getThumbnail(int paramInt1, int paramInt2, long paramLong)
        throws IOException;

    public abstract void getThumbnailList(int paramInt1, int paramInt2, long paramLong1, long paramLong2, int paramInt3, int[] paramArrayOfInt, GetThumbnailListCallback paramGetThumbnailListCallback)
        throws IOException;

    public Bitmap[] getThumbnailList(int paramInt1, int paramInt2, long paramLong1, long paramLong2, int paramInt3)
        throws IOException
    {
        final Bitmap[] arrayOfBitmap = new Bitmap[paramInt3];
        int[] arrayOfInt = new int[paramInt3];
        for (int i = 0; i < paramInt3; i++)
            arrayOfInt[i] = i;
        getThumbnailList(paramInt1, paramInt2, paramLong1, paramLong2, paramInt3, arrayOfInt, new GetThumbnailListCallback()
        {
            public void onThumbnail(Bitmap paramAnonymousBitmap, int paramAnonymousInt)
            {
                arrayOfBitmap[paramAnonymousInt] = paramAnonymousBitmap;
            }
        });
        return arrayOfBitmap;
    }

    public abstract long getTimelineDuration();

    public abstract int getWidth();

    public int hashCode()
    {
        return this.mUniqueId.hashCode();
    }

    void initClipSettings(MediaArtistNativeHelper.ClipSettings paramClipSettings)
    {
        paramClipSettings.clipPath = null;
        paramClipSettings.clipDecodedPath = null;
        paramClipSettings.clipOriginalPath = null;
        paramClipSettings.fileType = 0;
        paramClipSettings.endCutTime = 0;
        paramClipSettings.beginCutTime = 0;
        paramClipSettings.beginCutPercent = 0;
        paramClipSettings.endCutPercent = 0;
        paramClipSettings.panZoomEnabled = false;
        paramClipSettings.panZoomPercentStart = 0;
        paramClipSettings.panZoomTopLeftXStart = 0;
        paramClipSettings.panZoomTopLeftYStart = 0;
        paramClipSettings.panZoomPercentEnd = 0;
        paramClipSettings.panZoomTopLeftXEnd = 0;
        paramClipSettings.panZoomTopLeftYEnd = 0;
        paramClipSettings.mediaRendering = 0;
        paramClipSettings.rgbWidth = 0;
        paramClipSettings.rgbHeight = 0;
    }

    void invalidateBlankFrame()
    {
        if ((this.mBlankFrameFilename != null) && (new File(this.mBlankFrameFilename).exists()))
        {
            new File(this.mBlankFrameFilename).delete();
            this.mBlankFrameFilename = null;
        }
    }

    abstract void invalidateTransitions(long paramLong1, long paramLong2);

    abstract void invalidateTransitions(long paramLong1, long paramLong2, long paramLong3, long paramLong4);

    protected boolean isOverlapping(long paramLong1, long paramLong2, long paramLong3, long paramLong4)
    {
        boolean bool = false;
        if (paramLong1 + paramLong2 <= paramLong3);
        while (true)
        {
            return bool;
            if (paramLong1 < paramLong3 + paramLong4)
                bool = true;
        }
    }

    public Effect removeEffect(String paramString)
    {
        Iterator localIterator = this.mEffects.iterator();
        Effect localEffect;
        while (true)
            if (localIterator.hasNext())
            {
                localEffect = (Effect)localIterator.next();
                if (localEffect.getId().equals(paramString))
                {
                    this.mMANativeHelper.setGeneratePreview(true);
                    this.mEffects.remove(localEffect);
                    invalidateTransitions(localEffect.getStartTime(), localEffect.getDuration());
                    if ((localEffect instanceof EffectKenBurns))
                    {
                        if (this.mGeneratedImageClip != null)
                        {
                            new File(this.mGeneratedImageClip).delete();
                            this.mGeneratedImageClip = null;
                        }
                        this.mRegenerateClip = false;
                    }
                }
            }
        while (true)
        {
            return localEffect;
            localEffect = null;
        }
    }

    public Overlay removeOverlay(String paramString)
    {
        Iterator localIterator = this.mOverlays.iterator();
        Overlay localOverlay;
        while (localIterator.hasNext())
        {
            localOverlay = (Overlay)localIterator.next();
            if (localOverlay.getId().equals(paramString))
            {
                this.mMANativeHelper.setGeneratePreview(true);
                this.mOverlays.remove(localOverlay);
                if ((localOverlay instanceof OverlayFrame))
                    ((OverlayFrame)localOverlay).invalidate();
                invalidateTransitions(localOverlay.getStartTime(), localOverlay.getDuration());
            }
        }
        while (true)
        {
            return localOverlay;
            localOverlay = null;
        }
    }

    void setBeginTransition(Transition paramTransition)
    {
        this.mBeginTransition = paramTransition;
    }

    void setEndTransition(Transition paramTransition)
    {
        this.mEndTransition = paramTransition;
    }

    void setGeneratedImageClip(String paramString)
    {
        this.mGeneratedImageClip = paramString;
    }

    void setRegenerateClip(boolean paramBoolean)
    {
        this.mRegenerateClip = paramBoolean;
    }

    public void setRenderingMode(int paramInt)
    {
        switch (paramInt)
        {
        default:
            throw new IllegalArgumentException("Invalid Rendering Mode");
        case 0:
        case 1:
        case 2:
        }
        this.mMANativeHelper.setGeneratePreview(true);
        this.mRenderingMode = paramInt;
        if (this.mBeginTransition != null)
            this.mBeginTransition.invalidate();
        if (this.mEndTransition != null)
            this.mEndTransition.invalidate();
        Iterator localIterator = this.mOverlays.iterator();
        while (localIterator.hasNext())
            ((OverlayFrame)localIterator.next()).invalidateGeneratedFiles();
    }

    public static abstract interface GetThumbnailListCallback
    {
        public abstract void onThumbnail(Bitmap paramBitmap, int paramInt);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.media.videoeditor.MediaItem
 * JD-Core Version:        0.6.2
 */