package android.media.videoeditor;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import android.util.Pair;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MediaImageItem extends MediaItem
{
    private static final String TAG = "MediaImageItem";
    private static final Paint sResizePaint = new Paint(2);
    private final int mAspectRatio;
    private String mDecodedFilename;
    private long mDurationMs;
    private String mFileName;
    private int mGeneratedClipHeight;
    private int mGeneratedClipWidth;
    private final int mHeight;
    private final MediaArtistNativeHelper mMANativeHelper;
    private String mScaledFilename;
    private int mScaledHeight;
    private int mScaledWidth;
    private final VideoEditorImpl mVideoEditor;
    private final int mWidth;

    private MediaImageItem()
        throws IOException
    {
        this(null, null, null, 0L, 0);
    }

    public MediaImageItem(VideoEditor paramVideoEditor, String paramString1, String paramString2, long paramLong, int paramInt)
        throws IOException
    {
        super(paramVideoEditor, paramString1, paramString2, paramInt);
        this.mMANativeHelper = ((VideoEditorImpl)paramVideoEditor).getNativeContext();
        this.mVideoEditor = ((VideoEditorImpl)paramVideoEditor);
        try
        {
            MediaArtistNativeHelper.Properties localProperties = this.mMANativeHelper.getMediaProperties(paramString2);
            switch (this.mMANativeHelper.getFileType(localProperties.fileType))
            {
            case 6:
            case 7:
            default:
                throw new IllegalArgumentException("Unsupported Input File Type");
            case 5:
            case 8:
            }
        }
        catch (Exception localException)
        {
            throw new IllegalArgumentException("Unsupported file or file not found: " + paramString2);
        }
        this.mFileName = paramString2;
        BitmapFactory.Options localOptions = new BitmapFactory.Options();
        localOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(paramString2, localOptions);
        this.mWidth = localOptions.outWidth;
        this.mHeight = localOptions.outHeight;
        this.mDurationMs = paramLong;
        this.mDecodedFilename = String.format(this.mMANativeHelper.getProjectPath() + "/" + "decoded" + getId() + ".rgb", new Object[0]);
        Bitmap localBitmap;
        FileOutputStream localFileOutputStream1;
        while (true)
        {
            try
            {
                this.mAspectRatio = this.mMANativeHelper.getAspectRatio(this.mWidth, this.mHeight);
                this.mGeneratedClipHeight = 0;
                this.mGeneratedClipWidth = 0;
                Pair[] arrayOfPair = MediaProperties.getSupportedResolutions(this.mAspectRatio);
                Pair localPair = arrayOfPair[(-1 + arrayOfPair.length)];
                if ((this.mWidth > ((Integer)localPair.first).intValue()) || (this.mHeight > ((Integer)localPair.second).intValue()))
                {
                    localBitmap = scaleImage(paramString2, ((Integer)localPair.first).intValue(), ((Integer)localPair.second).intValue());
                    this.mScaledFilename = String.format(this.mMANativeHelper.getProjectPath() + "/" + "scaled" + getId() + ".JPG", new Object[0]);
                    if (!new File(this.mScaledFilename).exists())
                    {
                        this.mRegenerateClip = true;
                        FileOutputStream localFileOutputStream2 = new FileOutputStream(this.mScaledFilename);
                        localBitmap.compress(Bitmap.CompressFormat.JPEG, 50, localFileOutputStream2);
                        localFileOutputStream2.close();
                    }
                    this.mScaledWidth = (localBitmap.getWidth() >> 1 << 1);
                    this.mScaledHeight = (localBitmap.getHeight() >> 1 << 1);
                    int i = this.mScaledWidth;
                    int j = this.mScaledHeight;
                    if (new File(this.mDecodedFilename).exists())
                        break label659;
                    localFileOutputStream1 = new FileOutputStream(this.mDecodedFilename);
                    DataOutputStream localDataOutputStream = new DataOutputStream(localFileOutputStream1);
                    int[] arrayOfInt = new int[i];
                    ByteBuffer localByteBuffer = ByteBuffer.allocate(4 * arrayOfInt.length);
                    byte[] arrayOfByte = localByteBuffer.array();
                    int k = 0;
                    if (k >= j)
                        break;
                    localBitmap.getPixels(arrayOfInt, 0, this.mScaledWidth, 0, k, i, 1);
                    localByteBuffer.asIntBuffer().put(arrayOfInt, 0, i);
                    localDataOutputStream.write(arrayOfByte);
                    k++;
                    continue;
                }
            }
            catch (IllegalArgumentException localIllegalArgumentException)
            {
                throw new IllegalArgumentException("Null width and height");
            }
            this.mScaledFilename = paramString2;
            this.mScaledWidth = (this.mWidth >> 1 << 1);
            this.mScaledHeight = (this.mHeight >> 1 << 1);
            localBitmap = BitmapFactory.decodeFile(this.mScaledFilename);
        }
        localFileOutputStream1.close();
        label659: localBitmap.recycle();
    }

    private List<Effect> adjustEffects()
    {
        ArrayList localArrayList = new ArrayList();
        Iterator localIterator = getAllEffects().iterator();
        label131: 
        while (localIterator.hasNext())
        {
            Effect localEffect = (Effect)localIterator.next();
            long l1;
            if (localEffect.getStartTime() > getDuration())
            {
                l1 = 0L;
                label52: if (l1 + localEffect.getDuration() <= getDuration())
                    break label124;
            }
            label124: for (long l2 = getDuration() - l1; ; l2 = localEffect.getDuration())
            {
                if ((l1 == localEffect.getStartTime()) && (l2 == localEffect.getDuration()))
                    break label131;
                localEffect.setStartTimeAndDuration(l1, l2);
                localArrayList.add(localEffect);
                break;
                l1 = localEffect.getStartTime();
                break label52;
            }
        }
        return localArrayList;
    }

    private List<Overlay> adjustOverlays()
    {
        ArrayList localArrayList = new ArrayList();
        Iterator localIterator = getAllOverlays().iterator();
        label131: 
        while (localIterator.hasNext())
        {
            Overlay localOverlay = (Overlay)localIterator.next();
            long l1;
            if (localOverlay.getStartTime() > getDuration())
            {
                l1 = 0L;
                label52: if (l1 + localOverlay.getDuration() <= getDuration())
                    break label124;
            }
            label124: for (long l2 = getDuration() - l1; ; l2 = localOverlay.getDuration())
            {
                if ((l1 == localOverlay.getStartTime()) && (l2 == localOverlay.getDuration()))
                    break label131;
                localOverlay.setStartTimeAndDuration(l1, l2);
                localArrayList.add(localOverlay);
                break;
                l1 = localOverlay.getStartTime();
                break label52;
            }
        }
        return localArrayList;
    }

    private MediaArtistNativeHelper.ClipSettings getKenBurns(EffectKenBurns paramEffectKenBurns)
    {
        Rect localRect1 = new Rect();
        Rect localRect2 = new Rect();
        MediaArtistNativeHelper.ClipSettings localClipSettings = new MediaArtistNativeHelper.ClipSettings();
        paramEffectKenBurns.getKenBurnsSettings(localRect1, localRect2);
        int i = getWidth();
        int j = getHeight();
        if ((localRect1.left < 0) || (localRect1.left > i) || (localRect1.right < 0) || (localRect1.right > i) || (localRect1.top < 0) || (localRect1.top > j) || (localRect1.bottom < 0) || (localRect1.bottom > j) || (localRect2.left < 0) || (localRect2.left > i) || (localRect2.right < 0) || (localRect2.right > i) || (localRect2.top < 0) || (localRect2.top > j) || (localRect2.bottom < 0) || (localRect2.bottom > j))
            throw new IllegalArgumentException("Illegal arguments for KebBurns");
        if (((i - (localRect1.right - localRect1.left) == 0) || (j - (localRect1.bottom - localRect1.top) == 0)) && ((i - (localRect2.right - localRect2.left) == 0) || (j - (localRect2.bottom - localRect2.top) == 0)))
        {
            setRegenerateClip(false);
            localClipSettings.clipPath = getDecodedImageFileName();
            localClipSettings.fileType = 5;
            localClipSettings.beginCutTime = 0;
            localClipSettings.endCutTime = ((int)getTimelineDuration());
            localClipSettings.beginCutPercent = 0;
            localClipSettings.endCutPercent = 0;
            localClipSettings.panZoomEnabled = false;
            localClipSettings.panZoomPercentStart = 0;
            localClipSettings.panZoomTopLeftXStart = 0;
            localClipSettings.panZoomTopLeftYStart = 0;
            localClipSettings.panZoomPercentEnd = 0;
            localClipSettings.panZoomTopLeftXEnd = 0;
            localClipSettings.panZoomTopLeftYEnd = 0;
            localClipSettings.mediaRendering = this.mMANativeHelper.getMediaItemRenderingMode(getRenderingMode());
            localClipSettings.rgbWidth = getScaledWidth();
        }
        for (localClipSettings.rgbHeight = getScaledHeight(); ; localClipSettings.rgbHeight = getScaledHeight())
        {
            return localClipSettings;
            int k = 1000 * localRect1.width() / i;
            int m = 1000 * localRect2.width() / i;
            localClipSettings.clipPath = getDecodedImageFileName();
            localClipSettings.fileType = this.mMANativeHelper.getMediaItemFileType(getFileType());
            localClipSettings.beginCutTime = 0;
            localClipSettings.endCutTime = ((int)getTimelineDuration());
            localClipSettings.beginCutPercent = 0;
            localClipSettings.endCutPercent = 0;
            localClipSettings.panZoomEnabled = true;
            localClipSettings.panZoomPercentStart = k;
            localClipSettings.panZoomTopLeftXStart = (1000 * localRect1.left / i);
            localClipSettings.panZoomTopLeftYStart = (1000 * localRect1.top / j);
            localClipSettings.panZoomPercentEnd = m;
            localClipSettings.panZoomTopLeftXEnd = (1000 * localRect2.left / i);
            localClipSettings.panZoomTopLeftYEnd = (1000 * localRect2.top / j);
            localClipSettings.mediaRendering = this.mMANativeHelper.getMediaItemRenderingMode(getRenderingMode());
            localClipSettings.rgbWidth = getScaledWidth();
        }
    }

    private int getWidthByAspectRatioAndHeight(int paramInt1, int paramInt2)
    {
        int i = 0;
        switch (paramInt1)
        {
        default:
            throw new IllegalArgumentException("Illegal arguments for aspectRatio");
        case 1:
            if (paramInt2 == 480)
                i = 720;
            break;
        case 2:
        case 3:
        case 4:
        case 5:
        }
        while (true)
        {
            return i;
            if (paramInt2 == 720)
            {
                i = 1080;
                continue;
                if (paramInt2 == 360)
                {
                    i = 640;
                }
                else if (paramInt2 == 480)
                {
                    i = 854;
                }
                else if (paramInt2 == 720)
                {
                    i = 1280;
                }
                else if (paramInt2 == 1080)
                {
                    i = 1920;
                    continue;
                    if (paramInt2 == 480)
                        i = 640;
                    if (paramInt2 == 720)
                    {
                        i = 960;
                        continue;
                        if (paramInt2 == 480)
                        {
                            i = 800;
                            continue;
                            if (paramInt2 == 144)
                                i = 176;
                        }
                    }
                }
            }
        }
    }

    private void invalidateBeginTransition(List<Effect> paramList, List<Overlay> paramList1)
    {
        if ((this.mBeginTransition != null) && (this.mBeginTransition.isGenerated()))
        {
            long l = this.mBeginTransition.getDuration();
            Iterator localIterator1 = paramList.iterator();
            while (localIterator1.hasNext())
                if (((Effect)localIterator1.next()).getStartTime() < l)
                    this.mBeginTransition.invalidate();
            if (this.mBeginTransition.isGenerated())
            {
                Iterator localIterator2 = paramList1.iterator();
                while (localIterator2.hasNext())
                    if (((Overlay)localIterator2.next()).getStartTime() < l)
                        this.mBeginTransition.invalidate();
            }
        }
    }

    private void invalidateEndTransition()
    {
        if ((this.mEndTransition != null) && (this.mEndTransition.isGenerated()))
        {
            long l = this.mEndTransition.getDuration();
            Iterator localIterator1 = getAllEffects().iterator();
            while (localIterator1.hasNext())
            {
                Effect localEffect = (Effect)localIterator1.next();
                if (localEffect.getStartTime() + localEffect.getDuration() > this.mDurationMs - l)
                    this.mEndTransition.invalidate();
            }
            if (this.mEndTransition.isGenerated())
            {
                Iterator localIterator2 = getAllOverlays().iterator();
                while (localIterator2.hasNext())
                {
                    Overlay localOverlay = (Overlay)localIterator2.next();
                    if (localOverlay.getStartTime() + localOverlay.getDuration() > this.mDurationMs - l)
                        this.mEndTransition.invalidate();
                }
            }
        }
    }

    public static int nextPowerOf2(int paramInt)
    {
        int i = paramInt - 1;
        int j = i | i >>> 16;
        int k = j | j >>> 8;
        int m = k | k >>> 4;
        int n = m | m >>> 2;
        return 1 + (n | n >>> 1);
    }

    private Bitmap scaleImage(String paramString, int paramInt1, int paramInt2)
        throws IOException
    {
        BitmapFactory.Options localOptions1 = new BitmapFactory.Options();
        localOptions1.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(paramString, localOptions1);
        int i = localOptions1.outWidth;
        int j = localOptions1.outHeight;
        if (Log.isLoggable("MediaImageItem", 3))
            Log.d("MediaImageItem", "generateThumbnail: Input: " + i + "x" + j + ", resize to: " + paramInt1 + "x" + paramInt2);
        float f1;
        float f2;
        float f3;
        float f4;
        BitmapFactory.Options localOptions2;
        if ((i > paramInt1) || (j > paramInt2))
        {
            f1 = i / paramInt1;
            f2 = j / paramInt2;
            if (f1 > f2)
            {
                f3 = paramInt1;
                if (j / f1 < paramInt2)
                {
                    f4 = (float)Math.ceil(j / f1);
                    int k = nextPowerOf2((int)Math.ceil(Math.max(i / f3, j / f4)));
                    localOptions2 = new BitmapFactory.Options();
                    localOptions2.inSampleSize = k;
                }
            }
        }
        for (Bitmap localBitmap1 = BitmapFactory.decodeFile(paramString, localOptions2); ; localBitmap1 = BitmapFactory.decodeFile(paramString))
        {
            if (localBitmap1 != null)
                break label343;
            Log.e("MediaImageItem", "generateThumbnail: Cannot decode image bytes");
            throw new IOException("Cannot decode file: " + this.mFilename);
            f4 = (float)Math.floor(j / f1);
            break;
            if (i / f2 > paramInt1);
            for (f3 = (float)Math.floor(i / f2); ; f3 = (float)Math.ceil(i / f2))
            {
                f4 = paramInt2;
                break;
            }
            f3 = paramInt1;
            f4 = paramInt2;
        }
        label343: Bitmap localBitmap2 = Bitmap.createBitmap((int)f3, (int)f4, Bitmap.Config.ARGB_8888);
        Canvas localCanvas = new Canvas(localBitmap2);
        localCanvas.drawBitmap(localBitmap1, new Rect(0, 0, localBitmap1.getWidth(), localBitmap1.getHeight()), new Rect(0, 0, (int)f3, (int)f4), sResizePaint);
        localCanvas.setBitmap(null);
        localBitmap1.recycle();
        return localBitmap2;
    }

    MediaArtistNativeHelper.ClipSettings generateKenburnsClip(EffectKenBurns paramEffectKenBurns)
    {
        MediaArtistNativeHelper.EditSettings localEditSettings = new MediaArtistNativeHelper.EditSettings();
        localEditSettings.clipSettingsArray = new MediaArtistNativeHelper.ClipSettings[1];
        MediaArtistNativeHelper.ClipSettings localClipSettings = new MediaArtistNativeHelper.ClipSettings();
        initClipSettings(localClipSettings);
        localEditSettings.clipSettingsArray[0] = getKenBurns(paramEffectKenBurns);
        if ((getGeneratedImageClip() == null) && (getRegenerateClip()))
        {
            String str = this.mMANativeHelper.generateKenBurnsClip(localEditSettings, this);
            setGeneratedImageClip(str);
            setRegenerateClip(false);
            localClipSettings.clipPath = str;
            localClipSettings.fileType = 0;
            this.mGeneratedClipHeight = getScaledHeight();
            this.mGeneratedClipWidth = getWidthByAspectRatioAndHeight(this.mVideoEditor.getAspectRatio(), this.mGeneratedClipHeight);
        }
        while (true)
        {
            localClipSettings.mediaRendering = this.mMANativeHelper.getMediaItemRenderingMode(getRenderingMode());
            localClipSettings.beginCutTime = 0;
            localClipSettings.endCutTime = ((int)getTimelineDuration());
            return localClipSettings;
            if (getGeneratedImageClip() == null)
            {
                localClipSettings.clipPath = getDecodedImageFileName();
                localClipSettings.fileType = 5;
                localClipSettings.rgbWidth = getScaledWidth();
                localClipSettings.rgbHeight = getScaledHeight();
            }
            else
            {
                localClipSettings.clipPath = getGeneratedImageClip();
                localClipSettings.fileType = 0;
            }
        }
    }

    public int getAspectRatio()
    {
        return this.mAspectRatio;
    }

    String getDecodedImageFileName()
    {
        return this.mDecodedFilename;
    }

    public long getDuration()
    {
        return this.mDurationMs;
    }

    public int getFileType()
    {
        int i;
        if ((this.mFilename.endsWith(".jpg")) || (this.mFilename.endsWith(".jpeg")) || (this.mFilename.endsWith(".JPG")) || (this.mFilename.endsWith(".JPEG")))
            i = 5;
        while (true)
        {
            return i;
            if ((this.mFilename.endsWith(".png")) || (this.mFilename.endsWith(".PNG")))
                i = 8;
            else
                i = 255;
        }
    }

    int getGeneratedClipHeight()
    {
        return this.mGeneratedClipHeight;
    }

    int getGeneratedClipWidth()
    {
        return this.mGeneratedClipWidth;
    }

    String getGeneratedImageClip()
    {
        return super.getGeneratedImageClip();
    }

    public int getHeight()
    {
        return this.mHeight;
    }

    MediaArtistNativeHelper.ClipSettings getImageClipProperties()
    {
        MediaArtistNativeHelper.ClipSettings localClipSettings = new MediaArtistNativeHelper.ClipSettings();
        EffectKenBurns localEffectKenBurns = null;
        int i = 0;
        Iterator localIterator = getAllEffects().iterator();
        while (localIterator.hasNext())
        {
            Effect localEffect = (Effect)localIterator.next();
            if ((localEffect instanceof EffectKenBurns))
            {
                localEffectKenBurns = (EffectKenBurns)localEffect;
                i = 1;
            }
        }
        if (i != 0)
            localClipSettings = generateKenburnsClip(localEffectKenBurns);
        while (true)
        {
            return localClipSettings;
            initClipSettings(localClipSettings);
            localClipSettings.clipPath = getDecodedImageFileName();
            localClipSettings.fileType = 5;
            localClipSettings.beginCutTime = 0;
            localClipSettings.endCutTime = ((int)getTimelineDuration());
            localClipSettings.mediaRendering = this.mMANativeHelper.getMediaItemRenderingMode(getRenderingMode());
            localClipSettings.rgbWidth = getScaledWidth();
            localClipSettings.rgbHeight = getScaledHeight();
        }
    }

    public int getScaledHeight()
    {
        return this.mScaledHeight;
    }

    String getScaledImageFileName()
    {
        return this.mScaledFilename;
    }

    public int getScaledWidth()
    {
        return this.mScaledWidth;
    }

    public Bitmap getThumbnail(int paramInt1, int paramInt2, long paramLong)
        throws IOException
    {
        if (getGeneratedImageClip() != null);
        for (Bitmap localBitmap = this.mMANativeHelper.getPixels(getGeneratedImageClip(), paramInt1, paramInt2, paramLong, 0); ; localBitmap = scaleImage(this.mFilename, paramInt1, paramInt2))
            return localBitmap;
    }

    public void getThumbnailList(int paramInt1, int paramInt2, long paramLong1, long paramLong2, int paramInt3, int[] paramArrayOfInt, MediaItem.GetThumbnailListCallback paramGetThumbnailListCallback)
        throws IOException
    {
        if (getGeneratedImageClip() == null)
        {
            Bitmap localBitmap = scaleImage(this.mFilename, paramInt1, paramInt2);
            for (int i = 0; i < paramArrayOfInt.length; i++)
                paramGetThumbnailListCallback.onThumbnail(localBitmap, paramArrayOfInt[i]);
        }
        if (paramLong1 > paramLong2)
            throw new IllegalArgumentException("Start time is greater than end time");
        if (paramLong2 > this.mDurationMs)
            throw new IllegalArgumentException("End time is greater than file duration");
        this.mMANativeHelper.getPixelsList(getGeneratedImageClip(), paramInt1, paramInt2, paramLong1, paramLong2, paramInt3, paramArrayOfInt, paramGetThumbnailListCallback, 0);
    }

    public long getTimelineDuration()
    {
        return this.mDurationMs;
    }

    public int getWidth()
    {
        return this.mWidth;
    }

    void invalidate()
    {
        if (getGeneratedImageClip() != null)
        {
            new File(getGeneratedImageClip()).delete();
            setGeneratedImageClip(null);
            setRegenerateClip(true);
        }
        if (this.mScaledFilename != null)
        {
            if (this.mFileName != this.mScaledFilename)
                new File(this.mScaledFilename).delete();
            this.mScaledFilename = null;
        }
        if (this.mDecodedFilename != null)
        {
            new File(this.mDecodedFilename).delete();
            this.mDecodedFilename = null;
        }
    }

    void invalidateTransitions(long paramLong1, long paramLong2)
    {
        if ((this.mBeginTransition != null) && (isOverlapping(paramLong1, paramLong2, 0L, this.mBeginTransition.getDuration())))
            this.mBeginTransition.invalidate();
        if (this.mEndTransition != null)
        {
            long l = this.mEndTransition.getDuration();
            if (isOverlapping(paramLong1, paramLong2, getDuration() - l, l))
                this.mEndTransition.invalidate();
        }
    }

    void invalidateTransitions(long paramLong1, long paramLong2, long paramLong3, long paramLong4)
    {
        long l2;
        boolean bool4;
        if (this.mBeginTransition != null)
        {
            l2 = this.mBeginTransition.getDuration();
            boolean bool3 = isOverlapping(paramLong1, paramLong2, 0L, l2);
            bool4 = isOverlapping(paramLong3, paramLong4, 0L, l2);
            if (bool4 == bool3)
                break label121;
            this.mBeginTransition.invalidate();
        }
        label163: label213: 
        while (true)
        {
            long l1;
            boolean bool2;
            if (this.mEndTransition != null)
            {
                l1 = this.mEndTransition.getDuration();
                boolean bool1 = isOverlapping(paramLong1, paramLong2, this.mDurationMs - l1, l1);
                bool2 = isOverlapping(paramLong3, paramLong4, this.mDurationMs - l1, l1);
                if (bool2 == bool1)
                    break label163;
                this.mEndTransition.invalidate();
            }
            while (true)
            {
                return;
                label121: if ((!bool4) || ((paramLong1 == paramLong3) && (paramLong1 + paramLong2 > l2) && (paramLong3 + paramLong4 > l2)))
                    break label213;
                this.mBeginTransition.invalidate();
                break;
                if ((bool2) && ((paramLong1 + paramLong2 != paramLong3 + paramLong4) || (paramLong1 > this.mDurationMs - l1) || (paramLong3 > this.mDurationMs - l1)))
                    this.mEndTransition.invalidate();
            }
        }
    }

    public void setDuration(long paramLong)
    {
        if (paramLong == this.mDurationMs);
        while (true)
        {
            return;
            this.mMANativeHelper.setGeneratePreview(true);
            invalidateEndTransition();
            this.mDurationMs = paramLong;
            adjustTransitions();
            List localList = adjustOverlays();
            invalidateBeginTransition(adjustEffects(), localList);
            invalidateEndTransition();
            if (getGeneratedImageClip() != null)
            {
                new File(getGeneratedImageClip()).delete();
                setGeneratedImageClip(null);
                super.setRegenerateClip(true);
            }
            this.mVideoEditor.updateTimelineDuration();
        }
    }

    void setGeneratedImageClip(String paramString)
    {
        super.setGeneratedImageClip(paramString);
        this.mGeneratedClipHeight = getScaledHeight();
        this.mGeneratedClipWidth = getWidthByAspectRatioAndHeight(this.mVideoEditor.getAspectRatio(), this.mGeneratedClipHeight);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.media.videoeditor.MediaImageItem
 * JD-Core Version:        0.6.2
 */