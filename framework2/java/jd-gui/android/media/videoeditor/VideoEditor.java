package android.media.videoeditor;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.SurfaceHolder;
import java.io.IOException;
import java.util.List;

public abstract interface VideoEditor
{
    public static final int DURATION_OF_STORYBOARD = -1;
    public static final long MAX_SUPPORTED_FILE_SIZE = 2147483648L;
    public static final String THUMBNAIL_FILENAME = "thumbnail.jpg";

    public abstract void addAudioTrack(AudioTrack paramAudioTrack);

    public abstract void addMediaItem(MediaItem paramMediaItem);

    public abstract void addTransition(Transition paramTransition);

    public abstract void cancelExport(String paramString);

    public abstract void clearSurface(SurfaceHolder paramSurfaceHolder);

    public abstract void export(String paramString, int paramInt1, int paramInt2, int paramInt3, int paramInt4, ExportProgressListener paramExportProgressListener)
        throws IOException;

    public abstract void export(String paramString, int paramInt1, int paramInt2, ExportProgressListener paramExportProgressListener)
        throws IOException;

    public abstract void generatePreview(MediaProcessingProgressListener paramMediaProcessingProgressListener);

    public abstract List<AudioTrack> getAllAudioTracks();

    public abstract List<MediaItem> getAllMediaItems();

    public abstract List<Transition> getAllTransitions();

    public abstract int getAspectRatio();

    public abstract AudioTrack getAudioTrack(String paramString);

    public abstract long getDuration();

    public abstract MediaItem getMediaItem(String paramString);

    public abstract String getPath();

    public abstract Transition getTransition(String paramString);

    public abstract void insertAudioTrack(AudioTrack paramAudioTrack, String paramString);

    public abstract void insertMediaItem(MediaItem paramMediaItem, String paramString);

    public abstract void moveAudioTrack(String paramString1, String paramString2);

    public abstract void moveMediaItem(String paramString1, String paramString2);

    public abstract void release();

    public abstract void removeAllMediaItems();

    public abstract AudioTrack removeAudioTrack(String paramString);

    public abstract MediaItem removeMediaItem(String paramString);

    public abstract Transition removeTransition(String paramString);

    public abstract long renderPreviewFrame(SurfaceHolder paramSurfaceHolder, long paramLong, OverlayData paramOverlayData);

    public abstract void save()
        throws IOException;

    public abstract void setAspectRatio(int paramInt);

    public abstract void startPreview(SurfaceHolder paramSurfaceHolder, long paramLong1, long paramLong2, boolean paramBoolean, int paramInt, PreviewProgressListener paramPreviewProgressListener);

    public abstract long stopPreview();

    public static final class OverlayData
    {
        private static final Paint sResizePaint = new Paint(2);
        private boolean mClear = false;
        private Bitmap mOverlayBitmap = null;
        private int mRenderingMode = 2;

        public boolean needsRendering()
        {
            if ((this.mClear) || (this.mOverlayBitmap != null));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public void release()
        {
            if (this.mOverlayBitmap != null)
            {
                this.mOverlayBitmap.recycle();
                this.mOverlayBitmap = null;
            }
        }

        public void renderOverlay(Bitmap paramBitmap)
        {
            if (this.mClear)
                paramBitmap.eraseColor(0);
            Canvas localCanvas;
            Rect localRect2;
            Rect localRect1;
            while (true)
            {
                return;
                if (this.mOverlayBitmap != null)
                {
                    localCanvas = new Canvas(paramBitmap);
                    switch (this.mRenderingMode)
                    {
                    default:
                        throw new IllegalStateException("Rendering mode: " + this.mRenderingMode);
                    case 0:
                        localRect2 = new Rect(0, 0, localCanvas.getWidth(), localCanvas.getHeight());
                        localRect1 = new Rect(0, 0, this.mOverlayBitmap.getWidth(), this.mOverlayBitmap.getHeight());
                        paramBitmap.eraseColor(0);
                        localCanvas.drawBitmap(this.mOverlayBitmap, localRect1, localRect2, sResizePaint);
                        this.mOverlayBitmap.recycle();
                    case 2:
                    case 1:
                    }
                }
            }
            int i7;
            int i3;
            int i4;
            int i5;
            if (this.mOverlayBitmap.getWidth() / this.mOverlayBitmap.getHeight() > localCanvas.getWidth() / localCanvas.getHeight())
            {
                i7 = localCanvas.getWidth() * this.mOverlayBitmap.getHeight() / this.mOverlayBitmap.getWidth();
                i3 = 0;
                i4 = (localCanvas.getHeight() - i7) / 2;
                i5 = localCanvas.getWidth();
            }
            for (int i6 = i4 + i7; ; i6 = localCanvas.getHeight())
            {
                localRect2 = new Rect(i3, i4, i5, i6);
                localRect1 = new Rect(0, 0, this.mOverlayBitmap.getWidth(), this.mOverlayBitmap.getHeight());
                break;
                int i2 = localCanvas.getHeight() * this.mOverlayBitmap.getWidth() / this.mOverlayBitmap.getHeight();
                i3 = (localCanvas.getWidth() - i2) / 2;
                i4 = 0;
                i5 = i3 + i2;
            }
            int i1;
            int j;
            int k;
            int m;
            if (this.mOverlayBitmap.getWidth() / this.mOverlayBitmap.getHeight() < localCanvas.getWidth() / localCanvas.getHeight())
            {
                i1 = this.mOverlayBitmap.getWidth() * localCanvas.getHeight() / localCanvas.getWidth();
                j = 0;
                k = (this.mOverlayBitmap.getHeight() - i1) / 2;
                m = this.mOverlayBitmap.getWidth();
            }
            for (int n = k + i1; ; n = this.mOverlayBitmap.getHeight())
            {
                localRect1 = new Rect(j, k, m, n);
                localRect2 = new Rect(0, 0, localCanvas.getWidth(), localCanvas.getHeight());
                break;
                int i = this.mOverlayBitmap.getHeight() * localCanvas.getWidth() / localCanvas.getHeight();
                j = (this.mOverlayBitmap.getWidth() - i) / 2;
                k = 0;
                m = j + i;
            }
        }

        void set(Bitmap paramBitmap, int paramInt)
        {
            this.mOverlayBitmap = paramBitmap;
            this.mRenderingMode = paramInt;
            this.mClear = false;
        }

        void setClear()
        {
            this.mClear = true;
        }
    }

    public static abstract interface MediaProcessingProgressListener
    {
        public static final int ACTION_DECODE = 2;
        public static final int ACTION_ENCODE = 1;

        public abstract void onProgress(Object paramObject, int paramInt1, int paramInt2);
    }

    public static abstract interface ExportProgressListener
    {
        public abstract void onProgress(VideoEditor paramVideoEditor, String paramString, int paramInt);
    }

    public static abstract interface PreviewProgressListener
    {
        public abstract void onError(VideoEditor paramVideoEditor, int paramInt);

        public abstract void onProgress(VideoEditor paramVideoEditor, long paramLong, VideoEditor.OverlayData paramOverlayData);

        public abstract void onStart(VideoEditor paramVideoEditor);

        public abstract void onStop(VideoEditor paramVideoEditor);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.media.videoeditor.VideoEditor
 * JD-Core Version:        0.6.2
 */