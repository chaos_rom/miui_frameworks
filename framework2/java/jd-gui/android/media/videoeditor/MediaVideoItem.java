package android.media.videoeditor;

import android.graphics.Bitmap;
import android.view.Surface;
import android.view.SurfaceHolder;
import java.io.File;
import java.io.IOException;
import java.lang.ref.SoftReference;

public class MediaVideoItem extends MediaItem
{
    private final int mAspectRatio;
    private final int mAudioBitrate;
    private final int mAudioChannels;
    private final int mAudioSamplingFrequency;
    private final int mAudioType;
    private String mAudioWaveformFilename;
    private long mBeginBoundaryTimeMs;
    private final long mDurationMs;
    private long mEndBoundaryTimeMs;
    private final int mFileType;
    private final int mFps;
    private final int mHeight;
    private MediaArtistNativeHelper mMANativeHelper;
    private boolean mMuted;
    private final int mVideoBitrate;
    private VideoEditorImpl mVideoEditor;
    private final int mVideoLevel;
    private final int mVideoProfile;
    private final int mVideoRotationDegree;
    private final int mVideoType;
    private int mVolumePercentage;
    private SoftReference<WaveformData> mWaveformData;
    private final int mWidth;

    private MediaVideoItem()
        throws IOException
    {
        this(null, null, null, 0);
    }

    public MediaVideoItem(VideoEditor paramVideoEditor, String paramString1, String paramString2, int paramInt)
        throws IOException
    {
        this(paramVideoEditor, paramString1, paramString2, paramInt, 0L, -1L, 100, false, null);
    }

    MediaVideoItem(VideoEditor paramVideoEditor, String paramString1, String paramString2, int paramInt1, long paramLong1, long paramLong2, int paramInt2, boolean paramBoolean, String paramString3)
        throws IOException
    {
        super(paramVideoEditor, paramString1, paramString2, paramInt1);
        if ((paramVideoEditor instanceof VideoEditorImpl))
        {
            this.mMANativeHelper = ((VideoEditorImpl)paramVideoEditor).getNativeContext();
            this.mVideoEditor = ((VideoEditorImpl)paramVideoEditor);
        }
        MediaArtistNativeHelper.Properties localProperties;
        VideoEditorProfile localVideoEditorProfile;
        try
        {
            localProperties = this.mMANativeHelper.getMediaProperties(paramString2);
            localVideoEditorProfile = VideoEditorProfile.get();
            if (localVideoEditorProfile == null)
                throw new RuntimeException("Can't get the video editor profile");
        }
        catch (Exception localException)
        {
            throw new IllegalArgumentException(localException.getMessage() + " : " + paramString2);
        }
        int i = localVideoEditorProfile.maxInputVideoFrameWidth;
        int j = localVideoEditorProfile.maxInputVideoFrameHeight;
        if ((localProperties.width > i) || (localProperties.height > j))
            throw new IllegalArgumentException("Unsupported import resolution. Supported maximum width:" + i + " height:" + j + ", current width:" + localProperties.width + " height:" + localProperties.height);
        if (!localProperties.profileSupported)
            throw new IllegalArgumentException("Unsupported video profile " + localProperties.profile);
        if (!localProperties.levelSupported)
            throw new IllegalArgumentException("Unsupported video level " + localProperties.level);
        switch (this.mMANativeHelper.getFileType(localProperties.fileType))
        {
        default:
            throw new IllegalArgumentException("Unsupported Input File Type");
        case 0:
        case 1:
        case 10:
        }
        switch (this.mMANativeHelper.getVideoCodecType(localProperties.videoFormat))
        {
        default:
            throw new IllegalArgumentException("Unsupported Video Codec Format in Input File");
        case 1:
        case 2:
        case 3:
        }
        this.mWidth = localProperties.width;
        this.mHeight = localProperties.height;
        this.mAspectRatio = this.mMANativeHelper.getAspectRatio(localProperties.width, localProperties.height);
        this.mFileType = this.mMANativeHelper.getFileType(localProperties.fileType);
        this.mVideoType = this.mMANativeHelper.getVideoCodecType(localProperties.videoFormat);
        this.mVideoProfile = localProperties.profile;
        this.mVideoLevel = localProperties.level;
        this.mDurationMs = localProperties.videoDuration;
        this.mVideoBitrate = localProperties.videoBitrate;
        this.mAudioBitrate = localProperties.audioBitrate;
        this.mFps = ((int)localProperties.averageFrameRate);
        this.mAudioType = this.mMANativeHelper.getAudioCodecType(localProperties.audioFormat);
        this.mAudioChannels = localProperties.audioChannels;
        this.mAudioSamplingFrequency = localProperties.audioSamplingFrequency;
        this.mBeginBoundaryTimeMs = paramLong1;
        if (paramLong2 == -1L)
            paramLong2 = this.mDurationMs;
        this.mEndBoundaryTimeMs = paramLong2;
        this.mVolumePercentage = paramInt2;
        this.mMuted = paramBoolean;
        this.mAudioWaveformFilename = paramString3;
        if (paramString3 != null);
        for (this.mWaveformData = new SoftReference(new WaveformData(paramString3)); ; this.mWaveformData = null)
        {
            this.mVideoRotationDegree = localProperties.videoRotation;
            return;
        }
    }

    public void addEffect(Effect paramEffect)
    {
        if ((paramEffect instanceof EffectKenBurns))
            throw new IllegalArgumentException("Ken Burns effects cannot be applied to MediaVideoItem");
        super.addEffect(paramEffect);
    }

    public void extractAudioWaveform(ExtractAudioWaveformProgressListener paramExtractAudioWaveformProgressListener)
        throws IOException
    {
        int i = 0;
        int j = 0;
        String str1 = this.mMANativeHelper.getProjectPath();
        String str2;
        if (this.mAudioWaveformFilename == null)
        {
            str2 = String.format(str1 + "/" + "audioWaveformFile-" + getId() + ".dat", new Object[0]);
            if (this.mMANativeHelper.getAudioCodecType(this.mAudioType) != 1)
                break label139;
            i = 5;
            j = 160;
        }
        while (true)
        {
            this.mMANativeHelper.generateAudioGraph(getId(), this.mFilename, str2, i, 2, j, paramExtractAudioWaveformProgressListener, true);
            this.mAudioWaveformFilename = str2;
            this.mWaveformData = new SoftReference(new WaveformData(this.mAudioWaveformFilename));
            return;
            label139: if (this.mMANativeHelper.getAudioCodecType(this.mAudioType) == 8)
            {
                i = 10;
                j = 320;
            }
            else if (this.mMANativeHelper.getAudioCodecType(this.mAudioType) == 2)
            {
                i = 32;
                j = 1024;
            }
        }
    }

    public int getAspectRatio()
    {
        return this.mAspectRatio;
    }

    public int getAudioBitrate()
    {
        return this.mAudioBitrate;
    }

    public int getAudioChannels()
    {
        return this.mAudioChannels;
    }

    public int getAudioSamplingFrequency()
    {
        return this.mAudioSamplingFrequency;
    }

    public int getAudioType()
    {
        return this.mAudioType;
    }

    String getAudioWaveformFilename()
    {
        return this.mAudioWaveformFilename;
    }

    public long getBoundaryBeginTime()
    {
        return this.mBeginBoundaryTimeMs;
    }

    public long getBoundaryEndTime()
    {
        return this.mEndBoundaryTimeMs;
    }

    public long getDuration()
    {
        return this.mDurationMs;
    }

    public int getFileType()
    {
        return this.mFileType;
    }

    public int getFps()
    {
        return this.mFps;
    }

    public int getHeight()
    {
        if ((this.mVideoRotationDegree == 90) || (this.mVideoRotationDegree == 270));
        for (int i = this.mWidth; ; i = this.mHeight)
            return i;
    }

    public Bitmap getThumbnail(int paramInt1, int paramInt2, long paramLong)
    {
        if (paramLong > this.mDurationMs)
            throw new IllegalArgumentException("Time Exceeds duration");
        if (paramLong < 0L)
            throw new IllegalArgumentException("Invalid Time duration");
        if ((paramInt1 <= 0) || (paramInt2 <= 0))
            throw new IllegalArgumentException("Invalid Dimensions");
        if ((this.mVideoRotationDegree == 90) || (this.mVideoRotationDegree == 270))
        {
            int i = paramInt1;
            paramInt1 = paramInt2;
            paramInt2 = i;
        }
        MediaArtistNativeHelper localMediaArtistNativeHelper = this.mMANativeHelper;
        String str = getFilename();
        int j = this.mVideoRotationDegree;
        return localMediaArtistNativeHelper.getPixels(str, paramInt1, paramInt2, paramLong, j);
    }

    public void getThumbnailList(int paramInt1, int paramInt2, long paramLong1, long paramLong2, int paramInt3, int[] paramArrayOfInt, MediaItem.GetThumbnailListCallback paramGetThumbnailListCallback)
        throws IOException
    {
        if (paramLong1 > paramLong2)
            throw new IllegalArgumentException("Start time is greater than end time");
        if (paramLong2 > this.mDurationMs)
            throw new IllegalArgumentException("End time is greater than file duration");
        if ((paramInt2 <= 0) || (paramInt1 <= 0))
            throw new IllegalArgumentException("Invalid dimension");
        if ((this.mVideoRotationDegree == 90) || (this.mVideoRotationDegree == 270))
        {
            int i = paramInt1;
            paramInt1 = paramInt2;
            paramInt2 = i;
        }
        MediaArtistNativeHelper localMediaArtistNativeHelper = this.mMANativeHelper;
        String str = getFilename();
        int j = this.mVideoRotationDegree;
        localMediaArtistNativeHelper.getPixelsList(str, paramInt1, paramInt2, paramLong1, paramLong2, paramInt3, paramArrayOfInt, paramGetThumbnailListCallback, j);
    }

    public long getTimelineDuration()
    {
        return this.mEndBoundaryTimeMs - this.mBeginBoundaryTimeMs;
    }

    public int getVideoBitrate()
    {
        return this.mVideoBitrate;
    }

    MediaArtistNativeHelper.ClipSettings getVideoClipProperties()
    {
        MediaArtistNativeHelper.ClipSettings localClipSettings = new MediaArtistNativeHelper.ClipSettings();
        localClipSettings.clipPath = getFilename();
        localClipSettings.fileType = this.mMANativeHelper.getMediaItemFileType(getFileType());
        localClipSettings.beginCutTime = ((int)getBoundaryBeginTime());
        localClipSettings.endCutTime = ((int)getBoundaryEndTime());
        localClipSettings.mediaRendering = this.mMANativeHelper.getMediaItemRenderingMode(getRenderingMode());
        localClipSettings.rotationDegree = this.mVideoRotationDegree;
        return localClipSettings;
    }

    public int getVideoLevel()
    {
        return this.mVideoLevel;
    }

    public int getVideoProfile()
    {
        return this.mVideoProfile;
    }

    public int getVideoType()
    {
        return this.mVideoType;
    }

    public int getVolume()
    {
        return this.mVolumePercentage;
    }

    public WaveformData getWaveformData()
        throws IOException
    {
        WaveformData localWaveformData;
        if (this.mWaveformData == null)
            localWaveformData = null;
        while (true)
        {
            return localWaveformData;
            localWaveformData = (WaveformData)this.mWaveformData.get();
            if (localWaveformData == null)
                if (this.mAudioWaveformFilename != null)
                    try
                    {
                        localWaveformData = new WaveformData(this.mAudioWaveformFilename);
                        this.mWaveformData = new SoftReference(localWaveformData);
                    }
                    catch (IOException localIOException)
                    {
                        throw localIOException;
                    }
                else
                    localWaveformData = null;
        }
    }

    public int getWidth()
    {
        if ((this.mVideoRotationDegree == 90) || (this.mVideoRotationDegree == 270));
        for (int i = this.mHeight; ; i = this.mWidth)
            return i;
    }

    void invalidate()
    {
        if (this.mAudioWaveformFilename != null)
        {
            new File(this.mAudioWaveformFilename).delete();
            this.mAudioWaveformFilename = null;
        }
    }

    void invalidateTransitions(long paramLong1, long paramLong2)
    {
        if ((this.mBeginTransition != null) && (isOverlapping(paramLong1, paramLong2, this.mBeginBoundaryTimeMs, this.mBeginTransition.getDuration())))
            this.mBeginTransition.invalidate();
        if (this.mEndTransition != null)
        {
            long l = this.mEndTransition.getDuration();
            if (isOverlapping(paramLong1, paramLong2, this.mEndBoundaryTimeMs - l, l))
                this.mEndTransition.invalidate();
        }
    }

    void invalidateTransitions(long paramLong1, long paramLong2, long paramLong3, long paramLong4)
    {
        long l2;
        boolean bool4;
        if (this.mBeginTransition != null)
        {
            l2 = this.mBeginTransition.getDuration();
            boolean bool3 = isOverlapping(paramLong1, paramLong2, this.mBeginBoundaryTimeMs, l2);
            bool4 = isOverlapping(paramLong3, paramLong4, this.mBeginBoundaryTimeMs, l2);
            if (bool4 == bool3)
                break label127;
            this.mBeginTransition.invalidate();
        }
        label169: label219: 
        while (true)
        {
            long l1;
            boolean bool2;
            if (this.mEndTransition != null)
            {
                l1 = this.mEndTransition.getDuration();
                boolean bool1 = isOverlapping(paramLong1, paramLong2, this.mEndBoundaryTimeMs - l1, l1);
                bool2 = isOverlapping(paramLong3, paramLong4, this.mEndBoundaryTimeMs - l1, l1);
                if (bool2 == bool1)
                    break label169;
                this.mEndTransition.invalidate();
            }
            while (true)
            {
                return;
                label127: if ((!bool4) || ((paramLong1 == paramLong3) && (paramLong1 + paramLong2 > l2) && (paramLong3 + paramLong4 > l2)))
                    break label219;
                this.mBeginTransition.invalidate();
                break;
                if ((bool2) && ((paramLong1 + paramLong2 != paramLong3 + paramLong4) || (paramLong1 > this.mEndBoundaryTimeMs - l1) || (paramLong3 > this.mEndBoundaryTimeMs - l1)))
                    this.mEndTransition.invalidate();
            }
        }
    }

    public boolean isMuted()
    {
        return this.mMuted;
    }

    public long renderFrame(SurfaceHolder paramSurfaceHolder, long paramLong)
    {
        long l = 0L;
        if (paramSurfaceHolder == null)
            throw new IllegalArgumentException("Surface Holder is null");
        if ((paramLong > this.mDurationMs) || (paramLong < l))
            throw new IllegalArgumentException("requested time not correct");
        Surface localSurface = paramSurfaceHolder.getSurface();
        if (localSurface == null)
            throw new RuntimeException("Surface could not be retrieved from Surface holder");
        if (this.mFilename != null)
            l = this.mMANativeHelper.renderMediaItemPreviewFrame(localSurface, this.mFilename, paramLong, this.mWidth, this.mHeight);
        return l;
    }

    public void setExtractBoundaries(long paramLong1, long paramLong2)
    {
        if (paramLong1 > this.mDurationMs)
            throw new IllegalArgumentException("setExtractBoundaries: Invalid start time");
        if (paramLong2 > this.mDurationMs)
            throw new IllegalArgumentException("setExtractBoundaries: Invalid end time");
        if ((paramLong2 != -1L) && (paramLong1 >= paramLong2))
            throw new IllegalArgumentException("setExtractBoundaries: Start time is greater than end time");
        if ((paramLong1 < 0L) || ((paramLong2 != -1L) && (paramLong2 < 0L)))
            throw new IllegalArgumentException("setExtractBoundaries: Start time or end time is negative");
        this.mMANativeHelper.setGeneratePreview(true);
        if ((paramLong1 != this.mBeginBoundaryTimeMs) && (this.mBeginTransition != null))
            this.mBeginTransition.invalidate();
        if ((paramLong2 != this.mEndBoundaryTimeMs) && (this.mEndTransition != null))
            this.mEndTransition.invalidate();
        this.mBeginBoundaryTimeMs = paramLong1;
        this.mEndBoundaryTimeMs = paramLong2;
        adjustTransitions();
        this.mVideoEditor.updateTimelineDuration();
    }

    public void setMute(boolean paramBoolean)
    {
        this.mMANativeHelper.setGeneratePreview(true);
        this.mMuted = paramBoolean;
        if (this.mBeginTransition != null)
            this.mBeginTransition.invalidate();
        if (this.mEndTransition != null)
            this.mEndTransition.invalidate();
    }

    public void setVolume(int paramInt)
    {
        if ((paramInt < 0) || (paramInt > 100))
            throw new IllegalArgumentException("Invalid volume");
        this.mVolumePercentage = paramInt;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.media.videoeditor.MediaVideoItem
 * JD-Core Version:        0.6.2
 */