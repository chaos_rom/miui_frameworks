package android.media.videoeditor;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;

public class TransitionAlpha extends Transition
{
    private final int mBlendingPercent;
    private int mHeight;
    private final boolean mIsInvert;
    private final String mMaskFilename;
    private String mRGBMaskFile;
    private int mWidth;

    private TransitionAlpha()
    {
        this(null, null, null, 0L, 0, null, 0, false);
    }

    public TransitionAlpha(String paramString1, MediaItem paramMediaItem1, MediaItem paramMediaItem2, long paramLong, int paramInt1, String paramString2, int paramInt2, boolean paramBoolean)
    {
        super(paramString1, paramMediaItem1, paramMediaItem2, paramLong, paramInt1);
        BitmapFactory.Options localOptions = new BitmapFactory.Options();
        localOptions.inJustDecodeBounds = true;
        if (!new File(paramString2).exists())
            throw new IllegalArgumentException("File not Found " + paramString2);
        BitmapFactory.decodeFile(paramString2, localOptions);
        this.mWidth = localOptions.outWidth;
        this.mHeight = localOptions.outHeight;
        this.mRGBMaskFile = String.format(this.mNativeHelper.getProjectPath() + "/" + "mask" + paramString1 + ".rgb", new Object[0]);
        Object localObject = null;
        try
        {
            FileOutputStream localFileOutputStream = new FileOutputStream(this.mRGBMaskFile);
            localObject = localFileOutputStream;
            label165: DataOutputStream localDataOutputStream = new DataOutputStream(localObject);
            Bitmap localBitmap;
            int[] arrayOfInt;
            ByteBuffer localByteBuffer;
            byte[] arrayOfByte;
            int i;
            if (localObject != null)
            {
                localBitmap = BitmapFactory.decodeFile(paramString2);
                arrayOfInt = new int[this.mWidth];
                localByteBuffer = ByteBuffer.allocate(4 * arrayOfInt.length);
                arrayOfByte = localByteBuffer.array();
                i = 0;
            }
            while (true)
            {
                if (i < this.mHeight)
                {
                    localBitmap.getPixels(arrayOfInt, 0, this.mWidth, 0, i, this.mWidth, 1);
                    localByteBuffer.asIntBuffer().put(arrayOfInt, 0, this.mWidth);
                }
                try
                {
                    localDataOutputStream.write(arrayOfByte);
                    i++;
                    continue;
                    localBitmap.recycle();
                }
                catch (IOException localIOException2)
                {
                    try
                    {
                        localObject.close();
                        label284: this.mMaskFilename = paramString2;
                        this.mBlendingPercent = paramInt2;
                        this.mIsInvert = paramBoolean;
                        return;
                        localIOException2 = localIOException2;
                    }
                    catch (IOException localIOException1)
                    {
                        break label284;
                    }
                }
            }
        }
        catch (IOException localIOException3)
        {
            break label165;
        }
    }

    public void generate()
    {
        super.generate();
    }

    public int getBlendingPercent()
    {
        return this.mBlendingPercent;
    }

    public String getMaskFilename()
    {
        return this.mMaskFilename;
    }

    public String getPNGMaskFilename()
    {
        return this.mRGBMaskFile;
    }

    public int getRGBFileHeight()
    {
        return this.mHeight;
    }

    public int getRGBFileWidth()
    {
        return this.mWidth;
    }

    public boolean isInvert()
    {
        return this.mIsInvert;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.media.videoeditor.TransitionAlpha
 * JD-Core Version:        0.6.2
 */