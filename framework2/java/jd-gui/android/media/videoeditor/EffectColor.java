package android.media.videoeditor;

public class EffectColor extends Effect
{
    public static final int GRAY = 8355711;
    public static final int GREEN = 65280;
    public static final int PINK = 16737996;
    public static final int TYPE_COLOR = 1;
    public static final int TYPE_FIFTIES = 5;
    public static final int TYPE_GRADIENT = 2;
    public static final int TYPE_NEGATIVE = 4;
    public static final int TYPE_SEPIA = 3;
    private final int mColor;
    private final int mType;

    private EffectColor()
    {
        this(null, null, 0L, 0L, 0, 0);
    }

    public EffectColor(MediaItem paramMediaItem, String paramString, long paramLong1, long paramLong2, int paramInt1, int paramInt2)
    {
        super(paramMediaItem, paramString, paramLong1, paramLong2);
        switch (paramInt1)
        {
        default:
            throw new IllegalArgumentException("Invalid type: " + paramInt1);
        case 1:
        case 2:
            switch (paramInt2)
            {
            default:
                throw new IllegalArgumentException("Invalid Color: " + paramInt2);
            case 65280:
            case 8355711:
            case 16737996:
            }
            break;
        case 3:
        case 4:
        case 5:
        }
        for (this.mColor = paramInt2; ; this.mColor = -1)
        {
            this.mType = paramInt1;
            return;
        }
    }

    public int getColor()
    {
        return this.mColor;
    }

    public int getType()
    {
        return this.mType;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.media.videoeditor.EffectColor
 * JD-Core Version:        0.6.2
 */