package android.media.videoeditor;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Rect;
import android.media.MediaMetadataRetriever;
import android.os.Debug;
import android.os.Environment;
import android.util.Log;
import android.util.Xml;
import android.view.Surface;
import android.view.SurfaceHolder;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

public class VideoEditorImpl
    implements VideoEditor
{
    private static final String ATTR_AFTER_MEDIA_ITEM_ID = "after_media_item";
    private static final String ATTR_ASPECT_RATIO = "aspect_ratio";
    private static final String ATTR_AUDIO_WAVEFORM_FILENAME = "waveform";
    private static final String ATTR_BEFORE_MEDIA_ITEM_ID = "before_media_item";
    private static final String ATTR_BEGIN_TIME = "begin_time";
    private static final String ATTR_BEHAVIOR = "behavior";
    private static final String ATTR_BLENDING = "blending";
    private static final String ATTR_COLOR_EFFECT_TYPE = "color_type";
    private static final String ATTR_COLOR_EFFECT_VALUE = "color_value";
    private static final String ATTR_DIRECTION = "direction";
    private static final String ATTR_DUCKED_TRACK_VOLUME = "ducking_volume";
    private static final String ATTR_DUCK_ENABLED = "ducking_enabled";
    private static final String ATTR_DUCK_THRESHOLD = "ducking_threshold";
    private static final String ATTR_DURATION = "duration";
    private static final String ATTR_END_RECT_BOTTOM = "end_b";
    private static final String ATTR_END_RECT_LEFT = "end_l";
    private static final String ATTR_END_RECT_RIGHT = "end_r";
    private static final String ATTR_END_RECT_TOP = "end_t";
    private static final String ATTR_END_TIME = "end_time";
    private static final String ATTR_FILENAME = "filename";
    private static final String ATTR_GENERATED_IMAGE_CLIP = "generated_image_clip";
    private static final String ATTR_GENERATED_TRANSITION_CLIP = "generated_transition_clip";
    private static final String ATTR_ID = "id";
    private static final String ATTR_INVERT = "invert";
    private static final String ATTR_IS_IMAGE_CLIP_GENERATED = "is_image_clip_generated";
    private static final String ATTR_IS_TRANSITION_GENERATED = "is_transition_generated";
    private static final String ATTR_LOOP = "loop";
    private static final String ATTR_MASK = "mask";
    private static final String ATTR_MUTED = "muted";
    private static final String ATTR_OVERLAY_FRAME_HEIGHT = "overlay_frame_height";
    private static final String ATTR_OVERLAY_FRAME_WIDTH = "overlay_frame_width";
    private static final String ATTR_OVERLAY_RESIZED_RGB_FRAME_HEIGHT = "resized_RGBframe_height";
    private static final String ATTR_OVERLAY_RESIZED_RGB_FRAME_WIDTH = "resized_RGBframe_width";
    private static final String ATTR_OVERLAY_RGB_FILENAME = "overlay_rgb_filename";
    private static final String ATTR_REGENERATE_PCM = "regeneratePCMFlag";
    private static final String ATTR_RENDERING_MODE = "rendering_mode";
    private static final String ATTR_START_RECT_BOTTOM = "start_b";
    private static final String ATTR_START_RECT_LEFT = "start_l";
    private static final String ATTR_START_RECT_RIGHT = "start_r";
    private static final String ATTR_START_RECT_TOP = "start_t";
    private static final String ATTR_START_TIME = "start_time";
    private static final String ATTR_TYPE = "type";
    private static final String ATTR_VOLUME = "volume";
    private static final int ENGINE_ACCESS_MAX_TIMEOUT_MS = 500;
    private static final String PROJECT_FILENAME = "videoeditor.xml";
    private static final String TAG = "VideoEditorImpl";
    private static final String TAG_AUDIO_TRACK = "audio_track";
    private static final String TAG_AUDIO_TRACKS = "audio_tracks";
    private static final String TAG_EFFECT = "effect";
    private static final String TAG_EFFECTS = "effects";
    private static final String TAG_MEDIA_ITEM = "media_item";
    private static final String TAG_MEDIA_ITEMS = "media_items";
    private static final String TAG_OVERLAY = "overlay";
    private static final String TAG_OVERLAYS = "overlays";
    private static final String TAG_OVERLAY_USER_ATTRIBUTES = "overlay_user_attributes";
    private static final String TAG_PROJECT = "project";
    private static final String TAG_TRANSITION = "transition";
    private static final String TAG_TRANSITIONS = "transitions";
    private int mAspectRatio;
    private final List<AudioTrack> mAudioTracks;
    private long mDurationMs;
    private final Semaphore mLock;
    private MediaArtistNativeHelper mMANativeHelper;
    private final boolean mMallocDebug;
    private final List<MediaItem> mMediaItems;
    private boolean mPreviewInProgress;
    private final String mProjectPath;
    private final List<Transition> mTransitions;

    // ERROR //
    public VideoEditorImpl(String paramString)
        throws IOException
    {
        // Byte code:
        //     0: aload_0
        //     1: invokespecial 208	java/lang/Object:<init>	()V
        //     4: aload_0
        //     5: new 210	java/util/ArrayList
        //     8: dup
        //     9: invokespecial 211	java/util/ArrayList:<init>	()V
        //     12: putfield 213	android/media/videoeditor/VideoEditorImpl:mMediaItems	Ljava/util/List;
        //     15: aload_0
        //     16: new 210	java/util/ArrayList
        //     19: dup
        //     20: invokespecial 211	java/util/ArrayList:<init>	()V
        //     23: putfield 215	android/media/videoeditor/VideoEditorImpl:mAudioTracks	Ljava/util/List;
        //     26: aload_0
        //     27: new 210	java/util/ArrayList
        //     30: dup
        //     31: invokespecial 211	java/util/ArrayList:<init>	()V
        //     34: putfield 217	android/media/videoeditor/VideoEditorImpl:mTransitions	Ljava/util/List;
        //     37: aload_0
        //     38: iconst_0
        //     39: putfield 219	android/media/videoeditor/VideoEditorImpl:mPreviewInProgress	Z
        //     42: ldc 221
        //     44: invokestatic 227	android/os/SystemProperties:get	(Ljava/lang/String;)Ljava/lang/String;
        //     47: ldc 229
        //     49: invokevirtual 235	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     52: ifeq +82 -> 134
        //     55: aload_0
        //     56: iconst_1
        //     57: putfield 237	android/media/videoeditor/VideoEditorImpl:mMallocDebug	Z
        //     60: ldc 239
        //     62: invokestatic 242	android/media/videoeditor/VideoEditorImpl:dumpHeap	(Ljava/lang/String;)V
        //     65: aload_0
        //     66: new 244	java/util/concurrent/Semaphore
        //     69: dup
        //     70: iconst_1
        //     71: iconst_1
        //     72: invokespecial 247	java/util/concurrent/Semaphore:<init>	(IZ)V
        //     75: putfield 249	android/media/videoeditor/VideoEditorImpl:mLock	Ljava/util/concurrent/Semaphore;
        //     78: aload_0
        //     79: new 251	android/media/videoeditor/MediaArtistNativeHelper
        //     82: dup
        //     83: aload_1
        //     84: aload_0
        //     85: getfield 249	android/media/videoeditor/VideoEditorImpl:mLock	Ljava/util/concurrent/Semaphore;
        //     88: aload_0
        //     89: invokespecial 254	android/media/videoeditor/MediaArtistNativeHelper:<init>	(Ljava/lang/String;Ljava/util/concurrent/Semaphore;Landroid/media/videoeditor/VideoEditor;)V
        //     92: putfield 256	android/media/videoeditor/VideoEditorImpl:mMANativeHelper	Landroid/media/videoeditor/MediaArtistNativeHelper;
        //     95: aload_0
        //     96: aload_1
        //     97: putfield 258	android/media/videoeditor/VideoEditorImpl:mProjectPath	Ljava/lang/String;
        //     100: new 260	java/io/File
        //     103: dup
        //     104: aload_1
        //     105: ldc 142
        //     107: invokespecial 263	java/io/File:<init>	(Ljava/lang/String;Ljava/lang/String;)V
        //     110: invokevirtual 267	java/io/File:exists	()Z
        //     113: ifeq +46 -> 159
        //     116: aload_0
        //     117: invokespecial 270	android/media/videoeditor/VideoEditorImpl:load	()V
        //     120: return
        //     121: astore_3
        //     122: ldc 145
        //     124: ldc_w 272
        //     127: invokestatic 278	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     130: pop
        //     131: goto -66 -> 65
        //     134: aload_0
        //     135: iconst_0
        //     136: putfield 237	android/media/videoeditor/VideoEditorImpl:mMallocDebug	Z
        //     139: goto -74 -> 65
        //     142: astore_2
        //     143: aload_2
        //     144: invokevirtual 281	java/lang/Exception:printStackTrace	()V
        //     147: new 203	java/io/IOException
        //     150: dup
        //     151: aload_2
        //     152: invokevirtual 285	java/lang/Exception:toString	()Ljava/lang/String;
        //     155: invokespecial 287	java/io/IOException:<init>	(Ljava/lang/String;)V
        //     158: athrow
        //     159: aload_0
        //     160: iconst_2
        //     161: putfield 289	android/media/videoeditor/VideoEditorImpl:mAspectRatio	I
        //     164: aload_0
        //     165: lconst_0
        //     166: putfield 291	android/media/videoeditor/VideoEditorImpl:mDurationMs	J
        //     169: goto -49 -> 120
        //
        // Exception table:
        //     from	to	target	type
        //     60	65	121	java/lang/Exception
        //     116	120	142	java/lang/Exception
    }

    private void computeTimelineDuration()
    {
        this.mDurationMs = 0L;
        int i = this.mMediaItems.size();
        for (int j = 0; j < i; j++)
        {
            MediaItem localMediaItem = (MediaItem)this.mMediaItems.get(j);
            this.mDurationMs += localMediaItem.getTimelineDuration();
            if ((localMediaItem.getEndTransition() != null) && (j < i - 1))
                this.mDurationMs -= localMediaItem.getEndTransition().getDuration();
        }
    }

    private static void dumpHeap(String paramString)
        throws Exception
    {
        System.gc();
        System.runFinalization();
        Thread.sleep(1000L);
        if ("mounted".equals(Environment.getExternalStorageState()))
        {
            String str = Environment.getExternalStorageDirectory().toString();
            if (new File(str + "/" + paramString + ".dump").exists())
                new File(str + "/" + paramString + ".dump").delete();
            FileOutputStream localFileOutputStream = new FileOutputStream(str + "/" + paramString + ".dump");
            Debug.dumpNativeHeap(localFileOutputStream.getFD());
            localFileOutputStream.close();
        }
    }

    private void generateProjectThumbnail()
    {
        if (new File(this.mProjectPath + "/" + "thumbnail.jpg").exists())
            new File(this.mProjectPath + "/" + "thumbnail.jpg").delete();
        MediaItem localMediaItem;
        int i;
        String str;
        Object localObject1;
        if (this.mMediaItems.size() > 0)
        {
            localMediaItem = (MediaItem)this.mMediaItems.get(0);
            i = 480 * localMediaItem.getWidth() / localMediaItem.getHeight();
            str = localMediaItem.getFilename();
            if (!(localMediaItem instanceof MediaVideoItem))
                break label279;
            MediaMetadataRetriever localMediaMetadataRetriever = new MediaMetadataRetriever();
            localMediaMetadataRetriever.setDataSource(str);
            Bitmap localBitmap1 = localMediaMetadataRetriever.getFrameAtTime();
            localMediaMetadataRetriever.release();
            if (localBitmap1 == null)
                throw new IllegalArgumentException("Thumbnail extraction from " + str + " failed");
            localObject1 = Bitmap.createScaledBitmap(localBitmap1, i, 480, true);
        }
        try
        {
            while (true)
            {
                FileOutputStream localFileOutputStream = new FileOutputStream(this.mProjectPath + "/" + "thumbnail.jpg");
                ((Bitmap)localObject1).compress(Bitmap.CompressFormat.JPEG, 100, localFileOutputStream);
                localFileOutputStream.flush();
                localFileOutputStream.close();
                return;
                try
                {
                    label279: Bitmap localBitmap2 = localMediaItem.getThumbnail(i, 480, 500L);
                    localObject1 = localBitmap2;
                }
                catch (IllegalArgumentException localIllegalArgumentException)
                {
                    throw new IllegalArgumentException("Project thumbnail extraction from " + str + " failed");
                }
                catch (IOException localIOException2)
                {
                    throw new IllegalArgumentException("IO Error creating project thumbnail");
                }
            }
        }
        catch (IOException localIOException1)
        {
            throw new IllegalArgumentException("Error creating project thumbnail");
        }
        finally
        {
            ((Bitmap)localObject1).recycle();
        }
    }

    private void load()
        throws FileNotFoundException, XmlPullParserException, IOException
    {
        FileInputStream localFileInputStream = new FileInputStream(new File(this.mProjectPath, "videoeditor.xml"));
        int i;
        int j;
        while (true)
        {
            ArrayList localArrayList;
            XmlPullParser localXmlPullParser;
            MediaItem localMediaItem;
            Overlay localOverlay;
            String str2;
            try
            {
                localArrayList = new ArrayList();
                localXmlPullParser = Xml.newPullParser();
                localXmlPullParser.setInput(localFileInputStream, "UTF-8");
                i = localXmlPullParser.getEventType();
                localMediaItem = null;
                localOverlay = null;
                j = 0;
                break;
                i = localXmlPullParser.next();
                break;
                str2 = localXmlPullParser.getName();
                if ("project".equals(str2))
                {
                    this.mAspectRatio = Integer.parseInt(localXmlPullParser.getAttributeValue("", "aspect_ratio"));
                    boolean bool5 = Boolean.parseBoolean(localXmlPullParser.getAttributeValue("", "regeneratePCMFlag"));
                    this.mMANativeHelper.setAudioflag(bool5);
                    continue;
                }
            }
            finally
            {
                if (localFileInputStream != null)
                    localFileInputStream.close();
            }
            if ("media_item".equals(str2))
            {
                str4 = localXmlPullParser.getAttributeValue("", "id");
                try
                {
                    localMediaItem = parseMediaItem(localXmlPullParser);
                    this.mMediaItems.add(localMediaItem);
                }
                catch (Exception localException5)
                {
                    Log.w("VideoEditorImpl", "Cannot load media item: " + str4, localException5);
                    localMediaItem = null;
                    if (this.mMediaItems.size() == 0)
                        j = 1;
                    localArrayList.add(str4);
                }
            }
            else
            {
                bool1 = "transition".equals(str2);
                if (bool1)
                {
                    try
                    {
                        localTransition = parseTransition(localXmlPullParser, localArrayList);
                        if (localTransition == null)
                            continue;
                        this.mTransitions.add(localTransition);
                    }
                    catch (Exception localException4)
                    {
                        Log.w("VideoEditorImpl", "Cannot load transition", localException4);
                    }
                }
                else
                {
                    bool2 = "overlay".equals(str2);
                    if (bool2)
                    {
                        if (localMediaItem != null)
                            try
                            {
                                localOverlay = parseOverlay(localXmlPullParser, localMediaItem);
                                localMediaItem.addOverlay(localOverlay);
                            }
                            catch (Exception localException3)
                            {
                                Log.w("VideoEditorImpl", "Cannot load overlay", localException3);
                            }
                    }
                    else if ("overlay_user_attributes".equals(str2))
                    {
                        if (localOverlay != null)
                        {
                            k = localXmlPullParser.getAttributeCount();
                            for (m = 0; m < k; m++)
                                localOverlay.setUserAttribute(localXmlPullParser.getAttributeName(m), localXmlPullParser.getAttributeValue(m));
                        }
                    }
                    else
                    {
                        bool3 = "effect".equals(str2);
                        if (bool3)
                        {
                            if (localMediaItem != null)
                            {
                                try
                                {
                                    localEffect = parseEffect(localXmlPullParser, localMediaItem);
                                    localMediaItem.addEffect(localEffect);
                                    if (!(localEffect instanceof EffectKenBurns))
                                        continue;
                                    if (!Boolean.parseBoolean(localXmlPullParser.getAttributeValue("", "is_image_clip_generated")))
                                        break label596;
                                    str3 = localXmlPullParser.getAttributeValue("", "generated_image_clip");
                                    localFile = new File(str3);
                                    if (localFile.exists() != true)
                                        break label575;
                                    ((MediaImageItem)localMediaItem).setGeneratedImageClip(str3);
                                    ((MediaImageItem)localMediaItem).setRegenerateClip(false);
                                }
                                catch (Exception localException2)
                                {
                                    Log.w("VideoEditorImpl", "Cannot load effect", localException2);
                                }
                                continue;
                                label575: ((MediaImageItem)localMediaItem).setGeneratedImageClip(null);
                                ((MediaImageItem)localMediaItem).setRegenerateClip(true);
                                continue;
                                label596: ((MediaImageItem)localMediaItem).setGeneratedImageClip(null);
                                ((MediaImageItem)localMediaItem).setRegenerateClip(true);
                            }
                        }
                        else
                        {
                            bool4 = "audio_track".equals(str2);
                            if (bool4)
                            {
                                try
                                {
                                    addAudioTrack(parseAudioTrack(localXmlPullParser));
                                }
                                catch (Exception localException1)
                                {
                                    Log.w("VideoEditorImpl", "Cannot load audio track", localException1);
                                }
                                continue;
                                str1 = localXmlPullParser.getName();
                                if ("media_item".equals(str1))
                                    localMediaItem = null;
                                else if ("overlay".equals(str1))
                                    localOverlay = null;
                            }
                        }
                    }
                }
            }
        }
        while (i == 1)
        {
            String str4;
            boolean bool1;
            Transition localTransition;
            boolean bool2;
            int k;
            int m;
            boolean bool3;
            Effect localEffect;
            String str3;
            File localFile;
            boolean bool4;
            String str1;
            computeTimelineDuration();
            if (j != 0)
                generateProjectThumbnail();
            if (localFileInputStream != null)
                localFileInputStream.close();
            return;
        }
        switch (i)
        {
        default:
        case 2:
        case 3:
        }
    }

    private void lock()
        throws InterruptedException
    {
        if (Log.isLoggable("VideoEditorImpl", 3))
            Log.d("VideoEditorImpl", "lock: grabbing semaphore", new Throwable());
        this.mLock.acquire();
        if (Log.isLoggable("VideoEditorImpl", 3))
            Log.d("VideoEditorImpl", "lock: grabbed semaphore");
    }

    private boolean lock(long paramLong)
        throws InterruptedException
    {
        if (Log.isLoggable("VideoEditorImpl", 3))
            Log.d("VideoEditorImpl", "lock: grabbing semaphore with timeout " + paramLong, new Throwable());
        boolean bool = this.mLock.tryAcquire(paramLong, TimeUnit.MILLISECONDS);
        if (Log.isLoggable("VideoEditorImpl", 3))
            Log.d("VideoEditorImpl", "lock: grabbed semaphore status " + bool);
        return bool;
    }

    private AudioTrack parseAudioTrack(XmlPullParser paramXmlPullParser)
        throws IOException
    {
        String str1 = paramXmlPullParser.getAttributeValue("", "id");
        String str2 = paramXmlPullParser.getAttributeValue("", "filename");
        long l1 = Long.parseLong(paramXmlPullParser.getAttributeValue("", "start_time"));
        long l2 = Long.parseLong(paramXmlPullParser.getAttributeValue("", "begin_time"));
        long l3 = Long.parseLong(paramXmlPullParser.getAttributeValue("", "end_time"));
        int i = Integer.parseInt(paramXmlPullParser.getAttributeValue("", "volume"));
        boolean bool = Boolean.parseBoolean(paramXmlPullParser.getAttributeValue("", "muted"));
        return new AudioTrack(this, str1, str2, l1, l2, l3, Boolean.parseBoolean(paramXmlPullParser.getAttributeValue("", "loop")), i, bool, Boolean.parseBoolean(paramXmlPullParser.getAttributeValue("", "ducking_enabled")), Integer.parseInt(paramXmlPullParser.getAttributeValue("", "ducking_threshold")), Integer.parseInt(paramXmlPullParser.getAttributeValue("", "ducking_volume")), paramXmlPullParser.getAttributeValue("", "waveform"));
    }

    private Effect parseEffect(XmlPullParser paramXmlPullParser, MediaItem paramMediaItem)
    {
        String str1 = paramXmlPullParser.getAttributeValue("", "id");
        String str2 = paramXmlPullParser.getAttributeValue("", "type");
        long l1 = Long.parseLong(paramXmlPullParser.getAttributeValue("", "duration"));
        long l2 = Long.parseLong(paramXmlPullParser.getAttributeValue("", "begin_time"));
        int i;
        int j;
        if (EffectColor.class.getSimpleName().equals(str2))
        {
            i = Integer.parseInt(paramXmlPullParser.getAttributeValue("", "color_type"));
            if ((i == 1) || (i == 2))
                j = Integer.parseInt(paramXmlPullParser.getAttributeValue("", "color_value"));
        }
        Rect localRect1;
        Rect localRect2;
        for (Object localObject = new EffectColor(paramMediaItem, str1, l2, l1, i, j); ; localObject = new EffectKenBurns(paramMediaItem, str1, localRect1, localRect2, l2, l1))
        {
            return localObject;
            j = 0;
            break;
            if (!EffectKenBurns.class.getSimpleName().equals(str2))
                break label309;
            localRect1 = new Rect(Integer.parseInt(paramXmlPullParser.getAttributeValue("", "start_l")), Integer.parseInt(paramXmlPullParser.getAttributeValue("", "start_t")), Integer.parseInt(paramXmlPullParser.getAttributeValue("", "start_r")), Integer.parseInt(paramXmlPullParser.getAttributeValue("", "start_b")));
            localRect2 = new Rect(Integer.parseInt(paramXmlPullParser.getAttributeValue("", "end_l")), Integer.parseInt(paramXmlPullParser.getAttributeValue("", "end_t")), Integer.parseInt(paramXmlPullParser.getAttributeValue("", "end_r")), Integer.parseInt(paramXmlPullParser.getAttributeValue("", "end_b")));
        }
        label309: throw new IllegalArgumentException("Invalid effect type: " + str2);
    }

    private MediaItem parseMediaItem(XmlPullParser paramXmlPullParser)
        throws IOException
    {
        String str1 = paramXmlPullParser.getAttributeValue("", "id");
        String str2 = paramXmlPullParser.getAttributeValue("", "type");
        String str3 = paramXmlPullParser.getAttributeValue("", "filename");
        int i = Integer.parseInt(paramXmlPullParser.getAttributeValue("", "rendering_mode"));
        Object localObject;
        if (MediaImageItem.class.getSimpleName().equals(str2))
            localObject = new MediaImageItem(this, str1, str3, Long.parseLong(paramXmlPullParser.getAttributeValue("", "duration")), i);
        while (true)
        {
            return localObject;
            if (!MediaVideoItem.class.getSimpleName().equals(str2))
                break;
            long l1 = Long.parseLong(paramXmlPullParser.getAttributeValue("", "begin_time"));
            long l2 = Long.parseLong(paramXmlPullParser.getAttributeValue("", "end_time"));
            int j = Integer.parseInt(paramXmlPullParser.getAttributeValue("", "volume"));
            boolean bool = Boolean.parseBoolean(paramXmlPullParser.getAttributeValue("", "muted"));
            String str4 = paramXmlPullParser.getAttributeValue("", "waveform");
            localObject = new MediaVideoItem(this, str1, str3, i, l1, l2, j, bool, str4);
            long l3 = Long.parseLong(paramXmlPullParser.getAttributeValue("", "begin_time"));
            long l4 = Long.parseLong(paramXmlPullParser.getAttributeValue("", "end_time"));
            ((MediaVideoItem)localObject).setExtractBoundaries(l3, l4);
            int k = Integer.parseInt(paramXmlPullParser.getAttributeValue("", "volume"));
            ((MediaVideoItem)localObject).setVolume(k);
        }
        throw new IllegalArgumentException("Unknown media item type: " + str2);
    }

    private Overlay parseOverlay(XmlPullParser paramXmlPullParser, MediaItem paramMediaItem)
    {
        String str1 = paramXmlPullParser.getAttributeValue("", "id");
        String str2 = paramXmlPullParser.getAttributeValue("", "type");
        long l1 = Long.parseLong(paramXmlPullParser.getAttributeValue("", "duration"));
        long l2 = Long.parseLong(paramXmlPullParser.getAttributeValue("", "begin_time"));
        if (OverlayFrame.class.getSimpleName().equals(str2))
        {
            OverlayFrame localOverlayFrame = new OverlayFrame(paramMediaItem, str1, paramXmlPullParser.getAttributeValue("", "filename"), l2, l1);
            String str3 = paramXmlPullParser.getAttributeValue("", "overlay_rgb_filename");
            if (str3 != null)
            {
                ((OverlayFrame)localOverlayFrame).setFilename(str3);
                int i = Integer.parseInt(paramXmlPullParser.getAttributeValue("", "overlay_frame_width"));
                int j = Integer.parseInt(paramXmlPullParser.getAttributeValue("", "overlay_frame_height"));
                ((OverlayFrame)localOverlayFrame).setOverlayFrameWidth(i);
                ((OverlayFrame)localOverlayFrame).setOverlayFrameHeight(j);
                int k = Integer.parseInt(paramXmlPullParser.getAttributeValue("", "resized_RGBframe_width"));
                int m = Integer.parseInt(paramXmlPullParser.getAttributeValue("", "resized_RGBframe_height"));
                ((OverlayFrame)localOverlayFrame).setResizedRGBSize(k, m);
            }
            return localOverlayFrame;
        }
        throw new IllegalArgumentException("Invalid overlay type: " + str2);
    }

    private Transition parseTransition(XmlPullParser paramXmlPullParser, List<String> paramList)
    {
        String str1 = paramXmlPullParser.getAttributeValue("", "id");
        String str2 = paramXmlPullParser.getAttributeValue("", "type");
        long l = Long.parseLong(paramXmlPullParser.getAttributeValue("", "duration"));
        int i = Integer.parseInt(paramXmlPullParser.getAttributeValue("", "behavior"));
        String str3 = paramXmlPullParser.getAttributeValue("", "before_media_item");
        Object localObject;
        if (str3 != null)
            if (paramList.contains(str3))
            {
                localObject = null;
                return localObject;
            }
        String str4;
        for (MediaItem localMediaItem1 = getMediaItem(str3); ; localMediaItem1 = null)
        {
            str4 = paramXmlPullParser.getAttributeValue("", "after_media_item");
            if (str4 == null)
                break label304;
            if (!paramList.contains(str4))
                break label141;
            localObject = null;
            break;
        }
        label141: MediaItem localMediaItem2 = getMediaItem(str4);
        label149: if (TransitionAlpha.class.getSimpleName().equals(str2))
        {
            int k = Integer.parseInt(paramXmlPullParser.getAttributeValue("", "blending"));
            localObject = new TransitionAlpha(str1, localMediaItem2, localMediaItem1, l, i, paramXmlPullParser.getAttributeValue("", "mask"), k, Boolean.getBoolean(paramXmlPullParser.getAttributeValue("", "invert")));
            label224: if (Boolean.parseBoolean(paramXmlPullParser.getAttributeValue("", "is_transition_generated")) == true)
            {
                String str5 = paramXmlPullParser.getAttributeValue("", "generated_transition_clip");
                if (!new File(str5).exists())
                    break label462;
                ((Transition)localObject).setFilename(str5);
            }
        }
        while (true)
        {
            if (localMediaItem1 != null)
                localMediaItem1.setBeginTransition((Transition)localObject);
            if (localMediaItem2 == null)
                break;
            localMediaItem2.setEndTransition((Transition)localObject);
            break;
            label304: localMediaItem2 = null;
            break label149;
            if (TransitionCrossfade.class.getSimpleName().equals(str2))
            {
                localObject = new TransitionCrossfade(str1, localMediaItem2, localMediaItem1, l, i);
                break label224;
            }
            if (TransitionSliding.class.getSimpleName().equals(str2))
            {
                int j = Integer.parseInt(paramXmlPullParser.getAttributeValue("", "direction"));
                localObject = new TransitionSliding(str1, localMediaItem2, localMediaItem1, l, i, j);
                break label224;
            }
            if (TransitionFadeBlack.class.getSimpleName().equals(str2))
            {
                localObject = new TransitionFadeBlack(str1, localMediaItem2, localMediaItem1, l, i);
                break label224;
            }
            throw new IllegalArgumentException("Invalid transition type: " + str2);
            label462: ((Transition)localObject).setFilename(null);
        }
    }

    private void removeAdjacentTransitions(MediaItem paramMediaItem)
    {
        Transition localTransition1 = paramMediaItem.getBeginTransition();
        if (localTransition1 != null)
        {
            if (localTransition1.getAfterMediaItem() != null)
                localTransition1.getAfterMediaItem().setEndTransition(null);
            localTransition1.invalidate();
            this.mTransitions.remove(localTransition1);
        }
        Transition localTransition2 = paramMediaItem.getEndTransition();
        if (localTransition2 != null)
        {
            if (localTransition2.getBeforeMediaItem() != null)
                localTransition2.getBeforeMediaItem().setBeginTransition(null);
            localTransition2.invalidate();
            this.mTransitions.remove(localTransition2);
        }
        paramMediaItem.setBeginTransition(null);
        paramMediaItem.setEndTransition(null);
    }

    /** @deprecated */
    private MediaItem removeMediaItem(String paramString, boolean paramBoolean)
    {
        try
        {
            String str = ((MediaItem)this.mMediaItems.get(0)).getId();
            MediaItem localMediaItem = getMediaItem(paramString);
            if (localMediaItem != null)
            {
                this.mMANativeHelper.setGeneratePreview(true);
                this.mMediaItems.remove(localMediaItem);
                removeAdjacentTransitions(localMediaItem);
                computeTimelineDuration();
            }
            if (str.equals(paramString))
                generateProjectThumbnail();
            return localMediaItem;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private void removeTransitionAfter(int paramInt)
    {
        MediaItem localMediaItem = (MediaItem)this.mMediaItems.get(paramInt);
        Iterator localIterator = this.mTransitions.iterator();
        while (localIterator.hasNext())
        {
            Transition localTransition = (Transition)localIterator.next();
            if (localTransition.getAfterMediaItem() == localMediaItem)
            {
                this.mMANativeHelper.setGeneratePreview(true);
                localIterator.remove();
                localTransition.invalidate();
                localMediaItem.setEndTransition(null);
                if (paramInt < -1 + this.mMediaItems.size())
                    ((MediaItem)this.mMediaItems.get(paramInt + 1)).setBeginTransition(null);
            }
        }
    }

    private void removeTransitionBefore(int paramInt)
    {
        MediaItem localMediaItem = (MediaItem)this.mMediaItems.get(paramInt);
        Iterator localIterator = this.mTransitions.iterator();
        while (localIterator.hasNext())
        {
            Transition localTransition = (Transition)localIterator.next();
            if (localTransition.getBeforeMediaItem() == localMediaItem)
            {
                this.mMANativeHelper.setGeneratePreview(true);
                localIterator.remove();
                localTransition.invalidate();
                localMediaItem.setBeginTransition(null);
                if (paramInt > 0)
                    ((MediaItem)this.mMediaItems.get(paramInt - 1)).setEndTransition(null);
            }
        }
    }

    private void unlock()
    {
        if (Log.isLoggable("VideoEditorImpl", 3))
            Log.d("VideoEditorImpl", "unlock: releasing semaphore");
        this.mLock.release();
    }

    /** @deprecated */
    public void addAudioTrack(AudioTrack paramAudioTrack)
    {
        if (paramAudioTrack == null)
            try
            {
                throw new IllegalArgumentException("Audio Track is null");
            }
            finally
            {
            }
        if (this.mAudioTracks.size() == 1)
            throw new IllegalArgumentException("No more tracks can be added");
        this.mMANativeHelper.setGeneratePreview(true);
        this.mAudioTracks.add(paramAudioTrack);
        if (new File(String.format(this.mProjectPath + "/" + "AudioPcm" + paramAudioTrack.getId() + ".pcm", new Object[0])).exists())
            this.mMANativeHelper.setAudioflag(false);
    }

    /** @deprecated */
    public void addMediaItem(MediaItem paramMediaItem)
    {
        if (paramMediaItem == null)
            try
            {
                throw new IllegalArgumentException("Media item is null");
            }
            finally
            {
            }
        if (this.mMediaItems.contains(paramMediaItem))
            throw new IllegalArgumentException("Media item already exists: " + paramMediaItem.getId());
        this.mMANativeHelper.setGeneratePreview(true);
        int i = this.mMediaItems.size();
        if (i > 0)
            removeTransitionAfter(i - 1);
        this.mMediaItems.add(paramMediaItem);
        computeTimelineDuration();
        if (this.mMediaItems.size() == 1)
            generateProjectThumbnail();
    }

    /** @deprecated */
    public void addTransition(Transition paramTransition)
    {
        if (paramTransition == null)
            try
            {
                throw new IllegalArgumentException("Null Transition");
            }
            finally
            {
            }
        MediaItem localMediaItem1 = paramTransition.getBeforeMediaItem();
        MediaItem localMediaItem2 = paramTransition.getAfterMediaItem();
        if (this.mMediaItems == null)
            throw new IllegalArgumentException("No media items are added");
        if ((localMediaItem2 != null) && (localMediaItem1 != null))
        {
            int i = this.mMediaItems.indexOf(localMediaItem2);
            int j = this.mMediaItems.indexOf(localMediaItem1);
            if ((i == -1) || (j == -1))
                throw new IllegalArgumentException("Either of the mediaItem is not found in the list");
            if (i != j - 1)
                throw new IllegalArgumentException("MediaItems are not in sequence");
        }
        this.mMANativeHelper.setGeneratePreview(true);
        this.mTransitions.add(paramTransition);
        if (localMediaItem2 != null)
        {
            if (localMediaItem2.getEndTransition() != null)
            {
                localMediaItem2.getEndTransition().invalidate();
                this.mTransitions.remove(localMediaItem2.getEndTransition());
            }
            localMediaItem2.setEndTransition(paramTransition);
        }
        if (localMediaItem1 != null)
        {
            if (localMediaItem1.getBeginTransition() != null)
            {
                localMediaItem1.getBeginTransition().invalidate();
                this.mTransitions.remove(localMediaItem1.getBeginTransition());
            }
            localMediaItem1.setBeginTransition(paramTransition);
        }
        computeTimelineDuration();
    }

    public void cancelExport(String paramString)
    {
        if ((this.mMANativeHelper != null) && (paramString != null))
            this.mMANativeHelper.stop(paramString);
    }

    public void clearSurface(SurfaceHolder paramSurfaceHolder)
    {
        if (paramSurfaceHolder == null)
            throw new IllegalArgumentException("Invalid surface holder");
        Surface localSurface = paramSurfaceHolder.getSurface();
        if (localSurface == null)
            throw new IllegalArgumentException("Surface could not be retrieved from surface holder");
        if (!localSurface.isValid())
            throw new IllegalStateException("Surface is not valid");
        if (this.mMANativeHelper != null)
            this.mMANativeHelper.clearPreviewSurface(localSurface);
        while (true)
        {
            return;
            Log.w("VideoEditorImpl", "Native helper was not ready!");
        }
    }

    public void export(String paramString, int paramInt1, int paramInt2, int paramInt3, int paramInt4, VideoEditor.ExportProgressListener paramExportProgressListener)
        throws IOException
    {
        if (paramString == null)
            throw new IllegalArgumentException("export: filename is null");
        File localFile = new File(paramString);
        if (localFile == null)
            throw new IOException(paramString + "can not be created");
        if (this.mMediaItems.size() == 0)
            throw new IllegalStateException("No MediaItems added");
        switch (paramInt1)
        {
        default:
            throw new IllegalArgumentException("Unsupported height value " + paramInt1);
        case 144:
        case 288:
        case 360:
        case 480:
        case 720:
        case 1080:
        }
        switch (paramInt2)
        {
        default:
            throw new IllegalArgumentException("Unsupported bitrate value " + paramInt2);
        case 28000:
        case 40000:
        case 64000:
        case 96000:
        case 128000:
        case 192000:
        case 256000:
        case 384000:
        case 512000:
        case 800000:
        case 2000000:
        case 5000000:
        case 8000000:
        }
        computeTimelineDuration();
        if (2147483648L <= this.mDurationMs * (96000L + paramInt2) / 8000L)
            throw new IllegalStateException("Export Size is more than 2GB");
        switch (paramInt3)
        {
        default:
            throw new IllegalArgumentException("Unsupported audio codec type " + paramInt3);
        case 2:
        case 1:
        }
        for (int i = 2; ; i = 1)
            switch (paramInt4)
            {
            default:
                throw new IllegalArgumentException("Unsupported video codec type " + paramInt4);
            case 1:
            case 2:
            case 3:
            }
        int j = 1;
        while (true)
        {
            int k = 0;
            try
            {
                lock();
                k = 1;
                if (this.mMANativeHelper == null)
                    throw new IllegalStateException("The video editor is not initialized");
            }
            catch (InterruptedException localInterruptedException)
            {
                Log.e("VideoEditorImpl", "Sem acquire NOT successful in export");
                while (true)
                {
                    return;
                    j = 2;
                    break;
                    j = 3;
                    break;
                    this.mMANativeHelper.setAudioCodec(i);
                    this.mMANativeHelper.setVideoCodec(j);
                    this.mMANativeHelper.export(paramString, this.mProjectPath, paramInt1, paramInt2, this.mMediaItems, this.mTransitions, this.mAudioTracks, paramExportProgressListener);
                    if (k != 0)
                        unlock();
                }
            }
            finally
            {
                if (k != 0)
                    unlock();
            }
        }
    }

    public void export(String paramString, int paramInt1, int paramInt2, VideoEditor.ExportProgressListener paramExportProgressListener)
        throws IOException
    {
        export(paramString, paramInt1, paramInt2, 2, 2, paramExportProgressListener);
    }

    public void generatePreview(VideoEditor.MediaProcessingProgressListener paramMediaProcessingProgressListener)
    {
        int i = 0;
        try
        {
            lock();
            i = 1;
            if (this.mMANativeHelper == null)
                throw new IllegalStateException("The video editor is not initialized");
        }
        catch (InterruptedException localInterruptedException)
        {
            Log.e("VideoEditorImpl", "Sem acquire NOT successful in previewStoryBoard");
            while (true)
            {
                return;
                if ((this.mMediaItems.size() > 0) || (this.mAudioTracks.size() > 0))
                    this.mMANativeHelper.previewStoryBoard(this.mMediaItems, this.mTransitions, this.mAudioTracks, paramMediaProcessingProgressListener);
                if (i != 0)
                    unlock();
            }
        }
        finally
        {
            if (i != 0)
                unlock();
        }
    }

    public List<AudioTrack> getAllAudioTracks()
    {
        return this.mAudioTracks;
    }

    public List<MediaItem> getAllMediaItems()
    {
        return this.mMediaItems;
    }

    public List<Transition> getAllTransitions()
    {
        return this.mTransitions;
    }

    public int getAspectRatio()
    {
        return this.mAspectRatio;
    }

    public AudioTrack getAudioTrack(String paramString)
    {
        Iterator localIterator = this.mAudioTracks.iterator();
        AudioTrack localAudioTrack;
        do
        {
            if (!localIterator.hasNext())
                break;
            localAudioTrack = (AudioTrack)localIterator.next();
        }
        while (!localAudioTrack.getId().equals(paramString));
        while (true)
        {
            return localAudioTrack;
            localAudioTrack = null;
        }
    }

    public long getDuration()
    {
        computeTimelineDuration();
        return this.mDurationMs;
    }

    /** @deprecated */
    public MediaItem getMediaItem(String paramString)
    {
        try
        {
            Iterator localIterator = this.mMediaItems.iterator();
            while (localIterator.hasNext())
            {
                localMediaItem = (MediaItem)localIterator.next();
                boolean bool = localMediaItem.getId().equals(paramString);
                if (bool)
                    return localMediaItem;
            }
            MediaItem localMediaItem = null;
        }
        finally
        {
        }
    }

    MediaArtistNativeHelper getNativeContext()
    {
        return this.mMANativeHelper;
    }

    public String getPath()
    {
        return this.mProjectPath;
    }

    public Transition getTransition(String paramString)
    {
        Iterator localIterator = this.mTransitions.iterator();
        Transition localTransition;
        do
        {
            if (!localIterator.hasNext())
                break;
            localTransition = (Transition)localIterator.next();
        }
        while (!localTransition.getId().equals(paramString));
        while (true)
        {
            return localTransition;
            localTransition = null;
        }
    }

    /** @deprecated */
    public void insertAudioTrack(AudioTrack paramAudioTrack, String paramString)
    {
        try
        {
            if (this.mAudioTracks.size() == 1)
                throw new IllegalArgumentException("No more tracks can be added");
        }
        finally
        {
        }
        if (paramString == null)
        {
            this.mMANativeHelper.setGeneratePreview(true);
            this.mAudioTracks.add(0, paramAudioTrack);
            return;
        }
        int i = this.mAudioTracks.size();
        for (int j = 0; ; j++)
        {
            if (j < i)
            {
                if (!((AudioTrack)this.mAudioTracks.get(j)).getId().equals(paramString))
                    continue;
                this.mMANativeHelper.setGeneratePreview(true);
                this.mAudioTracks.add(j + 1, paramAudioTrack);
                break;
            }
            throw new IllegalArgumentException("AudioTrack not found: " + paramString);
        }
    }

    /** @deprecated */
    public void insertMediaItem(MediaItem paramMediaItem, String paramString)
    {
        try
        {
            if (this.mMediaItems.contains(paramMediaItem))
                throw new IllegalArgumentException("Media item already exists: " + paramMediaItem.getId());
        }
        finally
        {
        }
        if (paramString == null)
        {
            this.mMANativeHelper.setGeneratePreview(true);
            if (this.mMediaItems.size() > 0)
                removeTransitionBefore(0);
            this.mMediaItems.add(0, paramMediaItem);
            computeTimelineDuration();
            generateProjectThumbnail();
            return;
        }
        int i = this.mMediaItems.size();
        for (int j = 0; ; j++)
        {
            if (j < i)
            {
                if (!((MediaItem)this.mMediaItems.get(j)).getId().equals(paramString))
                    continue;
                this.mMANativeHelper.setGeneratePreview(true);
                removeTransitionAfter(j);
                this.mMediaItems.add(j + 1, paramMediaItem);
                computeTimelineDuration();
                break;
            }
            throw new IllegalArgumentException("MediaItem not found: " + paramString);
        }
    }

    /** @deprecated */
    public void moveAudioTrack(String paramString1, String paramString2)
    {
        try
        {
            throw new IllegalStateException("Not supported");
        }
        finally
        {
        }
    }

    /** @deprecated */
    public void moveMediaItem(String paramString1, String paramString2)
    {
        MediaItem localMediaItem;
        try
        {
            localMediaItem = removeMediaItem(paramString1, true);
            if (localMediaItem == null)
                throw new IllegalArgumentException("Target MediaItem not found: " + paramString1);
        }
        finally
        {
        }
        if (paramString2 == null)
        {
            if (this.mMediaItems.size() > 0)
            {
                this.mMANativeHelper.setGeneratePreview(true);
                removeTransitionBefore(0);
                this.mMediaItems.add(0, localMediaItem);
                computeTimelineDuration();
                generateProjectThumbnail();
                return;
            }
            throw new IllegalStateException("Cannot move media item (it is the only item)");
        }
        int i = this.mMediaItems.size();
        for (int j = 0; ; j++)
        {
            if (j < i)
            {
                if (!((MediaItem)this.mMediaItems.get(j)).getId().equals(paramString2))
                    continue;
                this.mMANativeHelper.setGeneratePreview(true);
                removeTransitionAfter(j);
                this.mMediaItems.add(j + 1, localMediaItem);
                computeTimelineDuration();
                break;
            }
            throw new IllegalArgumentException("MediaItem not found: " + paramString2);
        }
    }

    // ERROR //
    public void release()
    {
        // Byte code:
        //     0: aload_0
        //     1: invokevirtual 933	android/media/videoeditor/VideoEditorImpl:stopPreview	()J
        //     4: pop2
        //     5: iconst_0
        //     6: istore_3
        //     7: aload_0
        //     8: invokespecial 868	android/media/videoeditor/VideoEditorImpl:lock	()V
        //     11: iconst_1
        //     12: istore_3
        //     13: aload_0
        //     14: getfield 256	android/media/videoeditor/VideoEditorImpl:mMANativeHelper	Landroid/media/videoeditor/MediaArtistNativeHelper;
        //     17: ifnull +42 -> 59
        //     20: aload_0
        //     21: getfield 213	android/media/videoeditor/VideoEditorImpl:mMediaItems	Ljava/util/List;
        //     24: invokeinterface 936 1 0
        //     29: aload_0
        //     30: getfield 215	android/media/videoeditor/VideoEditorImpl:mAudioTracks	Ljava/util/List;
        //     33: invokeinterface 936 1 0
        //     38: aload_0
        //     39: getfield 217	android/media/videoeditor/VideoEditorImpl:mTransitions	Ljava/util/List;
        //     42: invokeinterface 936 1 0
        //     47: aload_0
        //     48: getfield 256	android/media/videoeditor/VideoEditorImpl:mMANativeHelper	Landroid/media/videoeditor/MediaArtistNativeHelper;
        //     51: invokevirtual 939	android/media/videoeditor/MediaArtistNativeHelper:releaseNativeHelper	()V
        //     54: aload_0
        //     55: aconst_null
        //     56: putfield 256	android/media/videoeditor/VideoEditorImpl:mMANativeHelper	Landroid/media/videoeditor/MediaArtistNativeHelper;
        //     59: iload_3
        //     60: ifeq +7 -> 67
        //     63: aload_0
        //     64: invokespecial 874	android/media/videoeditor/VideoEditorImpl:unlock	()V
        //     67: aload_0
        //     68: getfield 237	android/media/videoeditor/VideoEditorImpl:mMallocDebug	Z
        //     71: ifeq +9 -> 80
        //     74: ldc_w 941
        //     77: invokestatic 242	android/media/videoeditor/VideoEditorImpl:dumpHeap	(Ljava/lang/String;)V
        //     80: return
        //     81: astore 5
        //     83: ldc 145
        //     85: ldc_w 872
        //     88: aload 5
        //     90: invokestatic 943	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     93: pop
        //     94: iload_3
        //     95: ifeq -28 -> 67
        //     98: aload_0
        //     99: invokespecial 874	android/media/videoeditor/VideoEditorImpl:unlock	()V
        //     102: goto -35 -> 67
        //     105: astore 4
        //     107: iload_3
        //     108: ifeq +7 -> 115
        //     111: aload_0
        //     112: invokespecial 874	android/media/videoeditor/VideoEditorImpl:unlock	()V
        //     115: aload 4
        //     117: athrow
        //     118: astore 7
        //     120: ldc 145
        //     122: ldc_w 945
        //     125: invokestatic 278	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     128: pop
        //     129: goto -49 -> 80
        //
        // Exception table:
        //     from	to	target	type
        //     7	59	81	java/lang/Exception
        //     7	59	105	finally
        //     83	94	105	finally
        //     74	80	118	java/lang/Exception
    }

    /** @deprecated */
    public void removeAllMediaItems()
    {
        try
        {
            this.mMANativeHelper.setGeneratePreview(true);
            this.mMediaItems.clear();
            Iterator localIterator = this.mTransitions.iterator();
            while (localIterator.hasNext())
                ((Transition)localIterator.next()).invalidate();
        }
        finally
        {
        }
        this.mTransitions.clear();
        this.mDurationMs = 0L;
        if (new File(this.mProjectPath + "/" + "thumbnail.jpg").exists())
            new File(this.mProjectPath + "/" + "thumbnail.jpg").delete();
    }

    /** @deprecated */
    public AudioTrack removeAudioTrack(String paramString)
    {
        try
        {
            AudioTrack localAudioTrack = getAudioTrack(paramString);
            if (localAudioTrack != null)
            {
                this.mMANativeHelper.setGeneratePreview(true);
                this.mAudioTracks.remove(localAudioTrack);
                localAudioTrack.invalidate();
                this.mMANativeHelper.invalidatePcmFile();
                this.mMANativeHelper.setAudioflag(true);
                return localAudioTrack;
            }
            throw new IllegalArgumentException(" No more audio tracks");
        }
        finally
        {
        }
    }

    /** @deprecated */
    public MediaItem removeMediaItem(String paramString)
    {
        String str;
        MediaItem localMediaItem;
        try
        {
            str = ((MediaItem)this.mMediaItems.get(0)).getId();
            localMediaItem = getMediaItem(paramString);
            if (localMediaItem == null)
                break label149;
            this.mMANativeHelper.setGeneratePreview(true);
            this.mMediaItems.remove(localMediaItem);
            if ((localMediaItem instanceof MediaImageItem))
                ((MediaImageItem)localMediaItem).invalidate();
            List localList = localMediaItem.getAllOverlays();
            if (localList.size() > 0)
            {
                Iterator localIterator = localList.iterator();
                while (localIterator.hasNext())
                {
                    Overlay localOverlay = (Overlay)localIterator.next();
                    if ((localOverlay instanceof OverlayFrame))
                        ((OverlayFrame)localOverlay).invalidate();
                }
            }
        }
        finally
        {
        }
        removeAdjacentTransitions(localMediaItem);
        computeTimelineDuration();
        label149: if (str.equals(paramString))
            generateProjectThumbnail();
        if ((localMediaItem instanceof MediaVideoItem))
            ((MediaVideoItem)localMediaItem).invalidate();
        return localMediaItem;
    }

    /** @deprecated */
    public Transition removeTransition(String paramString)
    {
        Transition localTransition;
        try
        {
            localTransition = getTransition(paramString);
            if (localTransition == null)
                throw new IllegalStateException("Transition not found: " + paramString);
        }
        finally
        {
        }
        this.mMANativeHelper.setGeneratePreview(true);
        MediaItem localMediaItem1 = localTransition.getAfterMediaItem();
        if (localMediaItem1 != null)
            localMediaItem1.setEndTransition(null);
        MediaItem localMediaItem2 = localTransition.getBeforeMediaItem();
        if (localMediaItem2 != null)
            localMediaItem2.setBeginTransition(null);
        this.mTransitions.remove(localTransition);
        localTransition.invalidate();
        computeTimelineDuration();
        return localTransition;
    }

    public long renderPreviewFrame(SurfaceHolder paramSurfaceHolder, long paramLong, VideoEditor.OverlayData paramOverlayData)
    {
        if (paramSurfaceHolder == null)
            throw new IllegalArgumentException("Surface Holder is null");
        Surface localSurface = paramSurfaceHolder.getSurface();
        if (localSurface == null)
            throw new IllegalArgumentException("Surface could not be retrieved from Surface holder");
        if (!localSurface.isValid())
            throw new IllegalStateException("Surface is not valid");
        if (paramLong < 0L)
            throw new IllegalArgumentException("requested time not correct");
        if (paramLong > this.mDurationMs)
            throw new IllegalArgumentException("requested time more than duration");
        boolean bool = false;
        try
        {
            bool = lock(500L);
            if (!bool)
                throw new IllegalStateException("Timeout waiting for semaphore");
        }
        catch (InterruptedException localInterruptedException)
        {
            Log.w("VideoEditorImpl", "The thread was interrupted", new Throwable());
            throw new IllegalStateException("The thread was interrupted");
        }
        finally
        {
            if (bool)
                unlock();
        }
        if (this.mMANativeHelper == null)
            throw new IllegalStateException("The video editor is not initialized");
        long l2;
        if (this.mMediaItems.size() > 0)
        {
            Rect localRect = paramSurfaceHolder.getSurfaceFrame();
            l2 = this.mMANativeHelper.renderPreviewFrame(localSurface, paramLong, localRect.width(), localRect.height(), paramOverlayData);
        }
        for (long l1 = l2; ; l1 = 0L)
        {
            if (bool)
                unlock();
            return l1;
        }
    }

    public void save()
        throws IOException
    {
        XmlSerializer localXmlSerializer = Xml.newSerializer();
        StringWriter localStringWriter = new StringWriter();
        localXmlSerializer.setOutput(localStringWriter);
        localXmlSerializer.startDocument("UTF-8", Boolean.valueOf(true));
        localXmlSerializer.startTag("", "project");
        localXmlSerializer.attribute("", "aspect_ratio", Integer.toString(this.mAspectRatio));
        localXmlSerializer.attribute("", "regeneratePCMFlag", Boolean.toString(this.mMANativeHelper.getAudioflag()));
        localXmlSerializer.startTag("", "media_items");
        Iterator localIterator1 = this.mMediaItems.iterator();
        while (localIterator1.hasNext())
        {
            MediaItem localMediaItem3 = (MediaItem)localIterator1.next();
            localXmlSerializer.startTag("", "media_item");
            localXmlSerializer.attribute("", "id", localMediaItem3.getId());
            localXmlSerializer.attribute("", "type", localMediaItem3.getClass().getSimpleName());
            localXmlSerializer.attribute("", "filename", localMediaItem3.getFilename());
            localXmlSerializer.attribute("", "rendering_mode", Integer.toString(localMediaItem3.getRenderingMode()));
            Iterator localIterator5;
            if ((localMediaItem3 instanceof MediaVideoItem))
            {
                MediaVideoItem localMediaVideoItem = (MediaVideoItem)localMediaItem3;
                localXmlSerializer.attribute("", "begin_time", Long.toString(localMediaVideoItem.getBoundaryBeginTime()));
                localXmlSerializer.attribute("", "end_time", Long.toString(localMediaVideoItem.getBoundaryEndTime()));
                localXmlSerializer.attribute("", "volume", Integer.toString(localMediaVideoItem.getVolume()));
                localXmlSerializer.attribute("", "muted", Boolean.toString(localMediaVideoItem.isMuted()));
                if (localMediaVideoItem.getAudioWaveformFilename() != null)
                    localXmlSerializer.attribute("", "waveform", localMediaVideoItem.getAudioWaveformFilename());
                List localList1 = localMediaItem3.getAllOverlays();
                if (localList1.size() > 0)
                {
                    localXmlSerializer.startTag("", "overlays");
                    localIterator5 = localList1.iterator();
                }
            }
            else
            {
                while (true)
                {
                    if (!localIterator5.hasNext())
                        break label789;
                    Overlay localOverlay = (Overlay)localIterator5.next();
                    localXmlSerializer.startTag("", "overlay");
                    localXmlSerializer.attribute("", "id", localOverlay.getId());
                    localXmlSerializer.attribute("", "type", localOverlay.getClass().getSimpleName());
                    localXmlSerializer.attribute("", "begin_time", Long.toString(localOverlay.getStartTime()));
                    localXmlSerializer.attribute("", "duration", Long.toString(localOverlay.getDuration()));
                    if ((localOverlay instanceof OverlayFrame))
                    {
                        OverlayFrame localOverlayFrame = (OverlayFrame)localOverlay;
                        localOverlayFrame.save(getPath());
                        if (localOverlayFrame.getBitmapImageFileName() != null)
                            localXmlSerializer.attribute("", "filename", localOverlayFrame.getBitmapImageFileName());
                        if (localOverlayFrame.getFilename() != null)
                        {
                            localXmlSerializer.attribute("", "overlay_rgb_filename", localOverlayFrame.getFilename());
                            localXmlSerializer.attribute("", "overlay_frame_width", Integer.toString(localOverlayFrame.getOverlayFrameWidth()));
                            localXmlSerializer.attribute("", "overlay_frame_height", Integer.toString(localOverlayFrame.getOverlayFrameHeight()));
                            localXmlSerializer.attribute("", "resized_RGBframe_width", Integer.toString(localOverlayFrame.getResizedRGBSizeWidth()));
                            localXmlSerializer.attribute("", "resized_RGBframe_height", Integer.toString(localOverlayFrame.getResizedRGBSizeHeight()));
                        }
                    }
                    localXmlSerializer.startTag("", "overlay_user_attributes");
                    Map localMap = localOverlay.getUserAttributes();
                    Iterator localIterator6 = localMap.keySet().iterator();
                    while (true)
                    {
                        List localList2;
                        Iterator localIterator4;
                        Effect localEffect;
                        EffectColor localEffectColor;
                        Iterator localIterator2;
                        Transition localTransition;
                        MediaItem localMediaItem1;
                        MediaItem localMediaItem2;
                        Iterator localIterator3;
                        FileOutputStream localFileOutputStream;
                        if (localIterator6.hasNext())
                        {
                            String str1 = (String)localIterator6.next();
                            String str2 = (String)localMap.get(str1);
                            if (str2 != null)
                            {
                                localXmlSerializer.attribute("", str1, str2);
                                continue;
                                if (!(localMediaItem3 instanceof MediaImageItem))
                                    break;
                                localXmlSerializer.attribute("", "duration", Long.toString(localMediaItem3.getTimelineDuration()));
                                break;
                            }
                        }
                    }
                    localXmlSerializer.endTag("", "overlay_user_attributes");
                    localXmlSerializer.endTag("", "overlay");
                }
                label789: localXmlSerializer.endTag("", "overlays");
            }
            localList2 = localMediaItem3.getAllEffects();
            if (localList2.size() > 0)
            {
                localXmlSerializer.startTag("", "effects");
                localIterator4 = localList2.iterator();
                if (localIterator4.hasNext())
                {
                    localEffect = (Effect)localIterator4.next();
                    localXmlSerializer.startTag("", "effect");
                    localXmlSerializer.attribute("", "id", localEffect.getId());
                    localXmlSerializer.attribute("", "type", localEffect.getClass().getSimpleName());
                    localXmlSerializer.attribute("", "begin_time", Long.toString(localEffect.getStartTime()));
                    localXmlSerializer.attribute("", "duration", Long.toString(localEffect.getDuration()));
                    if ((localEffect instanceof EffectColor))
                    {
                        localEffectColor = (EffectColor)localEffect;
                        localXmlSerializer.attribute("", "color_type", Integer.toString(localEffectColor.getType()));
                        if ((localEffectColor.getType() == 1) || (localEffectColor.getType() == 2))
                            localXmlSerializer.attribute("", "color_value", Integer.toString(localEffectColor.getColor()));
                    }
                    while (true)
                    {
                        localXmlSerializer.endTag("", "effect");
                        break;
                        if ((localEffect instanceof EffectKenBurns))
                        {
                            Rect localRect1 = ((EffectKenBurns)localEffect).getStartRect();
                            localXmlSerializer.attribute("", "start_l", Integer.toString(localRect1.left));
                            localXmlSerializer.attribute("", "start_t", Integer.toString(localRect1.top));
                            localXmlSerializer.attribute("", "start_r", Integer.toString(localRect1.right));
                            localXmlSerializer.attribute("", "start_b", Integer.toString(localRect1.bottom));
                            Rect localRect2 = ((EffectKenBurns)localEffect).getEndRect();
                            localXmlSerializer.attribute("", "end_l", Integer.toString(localRect2.left));
                            localXmlSerializer.attribute("", "end_t", Integer.toString(localRect2.top));
                            localXmlSerializer.attribute("", "end_r", Integer.toString(localRect2.right));
                            localXmlSerializer.attribute("", "end_b", Integer.toString(localRect2.bottom));
                            MediaItem localMediaItem4 = localEffect.getMediaItem();
                            if (((MediaImageItem)localMediaItem4).getGeneratedImageClip() != null)
                            {
                                localXmlSerializer.attribute("", "is_image_clip_generated", Boolean.toString(true));
                                localXmlSerializer.attribute("", "generated_image_clip", ((MediaImageItem)localMediaItem4).getGeneratedImageClip());
                            }
                            else
                            {
                                localXmlSerializer.attribute("", "is_image_clip_generated", Boolean.toString(false));
                            }
                        }
                    }
                }
                localXmlSerializer.endTag("", "effects");
            }
            localXmlSerializer.endTag("", "media_item");
        }
        localXmlSerializer.endTag("", "media_items");
        localXmlSerializer.startTag("", "transitions");
        localIterator2 = this.mTransitions.iterator();
        if (localIterator2.hasNext())
        {
            localTransition = (Transition)localIterator2.next();
            localXmlSerializer.startTag("", "transition");
            localXmlSerializer.attribute("", "id", localTransition.getId());
            localXmlSerializer.attribute("", "type", localTransition.getClass().getSimpleName());
            localXmlSerializer.attribute("", "duration", Long.toString(localTransition.getDuration()));
            localXmlSerializer.attribute("", "behavior", Integer.toString(localTransition.getBehavior()));
            localXmlSerializer.attribute("", "is_transition_generated", Boolean.toString(localTransition.isGenerated()));
            if (localTransition.isGenerated() == true)
                localXmlSerializer.attribute("", "generated_transition_clip", localTransition.mFilename);
            localMediaItem1 = localTransition.getAfterMediaItem();
            if (localMediaItem1 != null)
                localXmlSerializer.attribute("", "after_media_item", localMediaItem1.getId());
            localMediaItem2 = localTransition.getBeforeMediaItem();
            if (localMediaItem2 != null)
                localXmlSerializer.attribute("", "before_media_item", localMediaItem2.getId());
            if ((localTransition instanceof TransitionSliding))
                localXmlSerializer.attribute("", "direction", Integer.toString(((TransitionSliding)localTransition).getDirection()));
            while (true)
            {
                localXmlSerializer.endTag("", "transition");
                break;
                if ((localTransition instanceof TransitionAlpha))
                {
                    TransitionAlpha localTransitionAlpha = (TransitionAlpha)localTransition;
                    localXmlSerializer.attribute("", "blending", Integer.toString(localTransitionAlpha.getBlendingPercent()));
                    localXmlSerializer.attribute("", "invert", Boolean.toString(localTransitionAlpha.isInvert()));
                    if (localTransitionAlpha.getMaskFilename() != null)
                        localXmlSerializer.attribute("", "mask", localTransitionAlpha.getMaskFilename());
                }
            }
        }
        localXmlSerializer.endTag("", "transitions");
        localXmlSerializer.startTag("", "audio_tracks");
        localIterator3 = this.mAudioTracks.iterator();
        while (localIterator3.hasNext())
        {
            AudioTrack localAudioTrack = (AudioTrack)localIterator3.next();
            localXmlSerializer.startTag("", "audio_track");
            localXmlSerializer.attribute("", "id", localAudioTrack.getId());
            localXmlSerializer.attribute("", "filename", localAudioTrack.getFilename());
            localXmlSerializer.attribute("", "start_time", Long.toString(localAudioTrack.getStartTime()));
            localXmlSerializer.attribute("", "begin_time", Long.toString(localAudioTrack.getBoundaryBeginTime()));
            localXmlSerializer.attribute("", "end_time", Long.toString(localAudioTrack.getBoundaryEndTime()));
            localXmlSerializer.attribute("", "volume", Integer.toString(localAudioTrack.getVolume()));
            localXmlSerializer.attribute("", "ducking_enabled", Boolean.toString(localAudioTrack.isDuckingEnabled()));
            localXmlSerializer.attribute("", "ducking_volume", Integer.toString(localAudioTrack.getDuckedTrackVolume()));
            localXmlSerializer.attribute("", "ducking_threshold", Integer.toString(localAudioTrack.getDuckingThreshhold()));
            localXmlSerializer.attribute("", "muted", Boolean.toString(localAudioTrack.isMuted()));
            localXmlSerializer.attribute("", "loop", Boolean.toString(localAudioTrack.isLooping()));
            if (localAudioTrack.getAudioWaveformFilename() != null)
                localXmlSerializer.attribute("", "waveform", localAudioTrack.getAudioWaveformFilename());
            localXmlSerializer.endTag("", "audio_track");
        }
        localXmlSerializer.endTag("", "audio_tracks");
        localXmlSerializer.endTag("", "project");
        localXmlSerializer.endDocument();
        localFileOutputStream = new FileOutputStream(new File(getPath(), "videoeditor.xml"));
        localFileOutputStream.write(localStringWriter.toString().getBytes());
        localFileOutputStream.flush();
        localFileOutputStream.close();
    }

    public void setAspectRatio(int paramInt)
    {
        this.mAspectRatio = paramInt;
        this.mMANativeHelper.setGeneratePreview(true);
        Iterator localIterator1 = this.mTransitions.iterator();
        while (localIterator1.hasNext())
            ((Transition)localIterator1.next()).invalidate();
        Iterator localIterator2 = this.mMediaItems.iterator();
        while (localIterator2.hasNext())
        {
            Iterator localIterator3 = ((MediaItem)localIterator2.next()).getAllOverlays().iterator();
            while (localIterator3.hasNext())
                ((OverlayFrame)localIterator3.next()).invalidateGeneratedFiles();
        }
    }

    public void startPreview(SurfaceHolder paramSurfaceHolder, long paramLong1, long paramLong2, boolean paramBoolean, int paramInt, VideoEditor.PreviewProgressListener paramPreviewProgressListener)
    {
        if (paramSurfaceHolder == null)
            throw new IllegalArgumentException();
        Surface localSurface = paramSurfaceHolder.getSurface();
        if (localSurface == null)
            throw new IllegalArgumentException("Surface could not be retrieved from surface holder");
        if (!localSurface.isValid())
            throw new IllegalStateException("Surface is not valid");
        if (paramPreviewProgressListener == null)
            throw new IllegalArgumentException();
        if (paramLong1 >= this.mDurationMs)
            throw new IllegalArgumentException("Requested time not correct");
        if (paramLong1 < 0L)
            throw new IllegalArgumentException("Requested time not correct");
        if (!this.mPreviewInProgress)
        {
            try
            {
                if (!lock(500L))
                    throw new IllegalStateException("Timeout waiting for semaphore");
            }
            catch (InterruptedException localInterruptedException)
            {
                Log.w("VideoEditorImpl", "The thread was interrupted", new Throwable());
                throw new IllegalStateException("The thread was interrupted");
            }
            if (this.mMANativeHelper == null)
                throw new IllegalStateException("The video editor is not initialized");
            if (this.mMediaItems.size() > 0)
            {
                this.mPreviewInProgress = true;
                this.mMANativeHelper.previewStoryBoard(this.mMediaItems, this.mTransitions, this.mAudioTracks, null);
                this.mMANativeHelper.doPreview(localSurface, paramLong1, paramLong2, paramBoolean, paramInt, paramPreviewProgressListener);
            }
            return;
        }
        throw new IllegalStateException("Preview already in progress");
    }

    public long stopPreview()
    {
        if (this.mPreviewInProgress);
        while (true)
        {
            try
            {
                long l2 = this.mMANativeHelper.stopPreview();
                this.mPreviewInProgress = false;
                unlock();
                l1 = l2;
                return l1;
            }
            finally
            {
                this.mPreviewInProgress = false;
                unlock();
            }
            long l1 = 0L;
        }
    }

    void updateTimelineDuration()
    {
        computeTimelineDuration();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.media.videoeditor.VideoEditorImpl
 * JD-Core Version:        0.6.2
 */