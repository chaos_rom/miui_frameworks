package android.media.videoeditor;

public class VideoEditorProfile
{
    public int maxInputVideoFrameHeight;
    public int maxInputVideoFrameWidth;
    public int maxOutputVideoFrameHeight;
    public int maxOutputVideoFrameWidth;

    static
    {
        System.loadLibrary("media_jni");
        native_init();
    }

    private VideoEditorProfile(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        this.maxInputVideoFrameWidth = paramInt1;
        this.maxInputVideoFrameHeight = paramInt2;
        this.maxOutputVideoFrameWidth = paramInt3;
        this.maxOutputVideoFrameHeight = paramInt4;
    }

    public static VideoEditorProfile get()
    {
        return native_get_videoeditor_profile();
    }

    public static int getExportLevel(int paramInt)
    {
        switch (paramInt)
        {
        default:
            throw new IllegalArgumentException("Unsupported video codec" + paramInt);
        case 1:
        case 2:
        case 3:
        }
        return native_get_videoeditor_export_level(paramInt);
    }

    public static int getExportProfile(int paramInt)
    {
        switch (paramInt)
        {
        default:
            throw new IllegalArgumentException("Unsupported video codec" + paramInt);
        case 1:
        case 2:
        case 3:
        }
        return native_get_videoeditor_export_profile(paramInt);
    }

    private static final native int native_get_videoeditor_export_level(int paramInt);

    private static final native int native_get_videoeditor_export_profile(int paramInt);

    private static final native VideoEditorProfile native_get_videoeditor_profile();

    private static final native void native_init();
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.media.videoeditor.VideoEditorProfile
 * JD-Core Version:        0.6.2
 */