package android.media.videoeditor;

import java.io.IOException;

public class WaveformData
{
    private final int mFrameDurationMs = 0;
    private final int mFramesCount = 0;
    private final short[] mGains = null;

    private WaveformData()
        throws IOException
    {
    }

    // ERROR //
    WaveformData(java.lang.String paramString)
        throws IOException
    {
        // Byte code:
        //     0: aload_0
        //     1: invokespecial 15	java/lang/Object:<init>	()V
        //     4: aload_1
        //     5: ifnonnull +13 -> 18
        //     8: new 24	java/lang/IllegalArgumentException
        //     11: dup
        //     12: ldc 26
        //     14: invokespecial 28	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     17: athrow
        //     18: aconst_null
        //     19: astore_2
        //     20: new 30	java/io/FileInputStream
        //     23: dup
        //     24: new 32	java/io/File
        //     27: dup
        //     28: aload_1
        //     29: invokespecial 33	java/io/File:<init>	(Ljava/lang/String;)V
        //     32: invokespecial 36	java/io/FileInputStream:<init>	(Ljava/io/File;)V
        //     35: astore_3
        //     36: iconst_4
        //     37: newarray byte
        //     39: astore 5
        //     41: aload_3
        //     42: aload 5
        //     44: iconst_0
        //     45: iconst_4
        //     46: invokevirtual 40	java/io/FileInputStream:read	([BII)I
        //     49: pop
        //     50: iconst_0
        //     51: istore 7
        //     53: iconst_0
        //     54: istore 8
        //     56: iconst_0
        //     57: istore 9
        //     59: iload 9
        //     61: iconst_4
        //     62: if_icmpge +26 -> 88
        //     65: iload 7
        //     67: bipush 8
        //     69: ishl
        //     70: sipush 255
        //     73: aload 5
        //     75: iload 9
        //     77: baload
        //     78: iand
        //     79: ior
        //     80: istore 7
        //     82: iinc 9 1
        //     85: goto -26 -> 59
        //     88: aload_0
        //     89: iload 7
        //     91: putfield 17	android/media/videoeditor/WaveformData:mFrameDurationMs	I
        //     94: iconst_4
        //     95: newarray byte
        //     97: astore 10
        //     99: aload_3
        //     100: aload 10
        //     102: iconst_0
        //     103: iconst_4
        //     104: invokevirtual 40	java/io/FileInputStream:read	([BII)I
        //     107: pop
        //     108: iconst_0
        //     109: istore 12
        //     111: iload 12
        //     113: iconst_4
        //     114: if_icmpge +26 -> 140
        //     117: iload 8
        //     119: bipush 8
        //     121: ishl
        //     122: sipush 255
        //     125: aload 10
        //     127: iload 12
        //     129: baload
        //     130: iand
        //     131: ior
        //     132: istore 8
        //     134: iinc 12 1
        //     137: goto -26 -> 111
        //     140: aload_0
        //     141: iload 8
        //     143: putfield 19	android/media/videoeditor/WaveformData:mFramesCount	I
        //     146: aload_0
        //     147: aload_0
        //     148: getfield 19	android/media/videoeditor/WaveformData:mFramesCount	I
        //     151: newarray short
        //     153: putfield 21	android/media/videoeditor/WaveformData:mGains	[S
        //     156: iconst_0
        //     157: istore 13
        //     159: iload 13
        //     161: aload_0
        //     162: getfield 19	android/media/videoeditor/WaveformData:mFramesCount	I
        //     165: if_icmpge +21 -> 186
        //     168: aload_0
        //     169: getfield 21	android/media/videoeditor/WaveformData:mGains	[S
        //     172: iload 13
        //     174: aload_3
        //     175: invokevirtual 43	java/io/FileInputStream:read	()I
        //     178: i2s
        //     179: sastore
        //     180: iinc 13 1
        //     183: goto -24 -> 159
        //     186: aload_3
        //     187: ifnull +7 -> 194
        //     190: aload_3
        //     191: invokevirtual 46	java/io/FileInputStream:close	()V
        //     194: return
        //     195: astore 4
        //     197: aload_2
        //     198: ifnull +7 -> 205
        //     201: aload_2
        //     202: invokevirtual 46	java/io/FileInputStream:close	()V
        //     205: aload 4
        //     207: athrow
        //     208: astore 4
        //     210: aload_3
        //     211: astore_2
        //     212: goto -15 -> 197
        //
        // Exception table:
        //     from	to	target	type
        //     20	36	195	finally
        //     36	180	208	finally
    }

    public int getFrameDuration()
    {
        return this.mFrameDurationMs;
    }

    public short[] getFrameGains()
    {
        return this.mGains;
    }

    public int getFramesCount()
    {
        return this.mFramesCount;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.media.videoeditor.WaveformData
 * JD-Core Version:        0.6.2
 */