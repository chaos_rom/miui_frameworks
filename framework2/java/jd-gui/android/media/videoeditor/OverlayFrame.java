package android.media.videoeditor;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Pair;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;

public class OverlayFrame extends Overlay
{
    private static final Paint sResizePaint = new Paint(2);
    private Bitmap mBitmap;
    private String mBitmapFileName;
    private String mFilename;
    private int mOFHeight;
    private int mOFWidth;
    private int mResizedRGBHeight;
    private int mResizedRGBWidth;

    private OverlayFrame()
    {
        this(null, null, (String)null, 0L, 0L);
    }

    public OverlayFrame(MediaItem paramMediaItem, String paramString, Bitmap paramBitmap, long paramLong1, long paramLong2)
    {
        super(paramMediaItem, paramString, paramLong1, paramLong2);
        this.mBitmap = paramBitmap;
        this.mFilename = null;
        this.mBitmapFileName = null;
        this.mResizedRGBWidth = 0;
        this.mResizedRGBHeight = 0;
    }

    OverlayFrame(MediaItem paramMediaItem, String paramString1, String paramString2, long paramLong1, long paramLong2)
    {
        super(paramMediaItem, paramString1, paramLong1, paramLong2);
        this.mBitmapFileName = paramString2;
        this.mBitmap = BitmapFactory.decodeFile(this.mBitmapFileName);
        this.mFilename = null;
        this.mResizedRGBWidth = 0;
        this.mResizedRGBHeight = 0;
    }

    void generateOverlayWithRenderingMode(MediaItem paramMediaItem, OverlayFrame paramOverlayFrame, int paramInt1, int paramInt2)
        throws FileNotFoundException, IOException
    {
        int i = paramMediaItem.getRenderingMode();
        Bitmap localBitmap1 = paramOverlayFrame.getBitmap();
        int j = paramOverlayFrame.getResizedRGBSizeHeight();
        int k = paramOverlayFrame.getResizedRGBSizeWidth();
        if (k == 0)
            k = localBitmap1.getWidth();
        if (j == 0)
            j = localBitmap1.getHeight();
        if ((k != paramInt2) || (j != paramInt1) || (!new File(paramOverlayFrame.getFilename()).exists()))
        {
            Bitmap localBitmap2 = Bitmap.createBitmap(paramInt2, paramInt1, Bitmap.Config.ARGB_8888);
            Canvas localCanvas = new Canvas(localBitmap2);
            Rect localRect2;
            Rect localRect1;
            FileOutputStream localFileOutputStream;
            DataOutputStream localDataOutputStream;
            int[] arrayOfInt;
            ByteBuffer localByteBuffer;
            byte[] arrayOfByte;
            int i6;
            switch (i)
            {
            default:
                throw new IllegalStateException("Rendering mode: " + i);
            case 1:
                int i16 = localCanvas.getWidth();
                int i17 = localCanvas.getHeight();
                localRect2 = new Rect(0, 0, i16, i17);
                int i18 = localBitmap1.getWidth();
                int i19 = localBitmap1.getHeight();
                localRect1 = new Rect(0, 0, i18, i19);
                Paint localPaint = sResizePaint;
                localCanvas.drawBitmap(localBitmap1, localRect1, localRect2, localPaint);
                localCanvas.setBitmap(null);
                String str = paramOverlayFrame.getFilename();
                if (str != null)
                    new File(str).delete();
                localFileOutputStream = new FileOutputStream(str);
                localDataOutputStream = new DataOutputStream(localFileOutputStream);
                arrayOfInt = new int[paramInt2];
                localByteBuffer = ByteBuffer.allocate(4 * arrayOfInt.length);
                arrayOfByte = localByteBuffer.array();
                i6 = 0;
            case 0:
            case 2:
            }
            while (i6 < paramInt1)
            {
                localBitmap2.getPixels(arrayOfInt, 0, paramInt2, 0, i6, paramInt2, 1);
                localByteBuffer.asIntBuffer().put(arrayOfInt, 0, paramInt2);
                localDataOutputStream.write(arrayOfByte);
                i6++;
                continue;
                int i15;
                int i9;
                int i10;
                int i11;
                if (localBitmap1.getWidth() / localBitmap1.getHeight() > localCanvas.getWidth() / localCanvas.getHeight())
                {
                    i15 = localCanvas.getWidth() * localBitmap1.getHeight() / localBitmap1.getWidth();
                    i9 = 0;
                    i10 = (localCanvas.getHeight() - i15) / 2;
                    i11 = localCanvas.getWidth();
                }
                for (int i12 = i10 + i15; ; i12 = localCanvas.getHeight())
                {
                    localRect2 = new Rect(i9, i10, i11, i12);
                    int i13 = localBitmap1.getWidth();
                    int i14 = localBitmap1.getHeight();
                    localRect1 = new Rect(0, 0, i13, i14);
                    break;
                    int i8 = localCanvas.getHeight() * localBitmap1.getWidth() / localBitmap1.getHeight();
                    i9 = (localCanvas.getWidth() - i8) / 2;
                    i10 = 0;
                    i11 = i9 + i8;
                }
                int i7;
                int n;
                int i1;
                int i2;
                if (localBitmap1.getWidth() / localBitmap1.getHeight() < localCanvas.getWidth() / localCanvas.getHeight())
                {
                    i7 = localBitmap1.getWidth() * localCanvas.getHeight() / localCanvas.getWidth();
                    n = 0;
                    i1 = (localBitmap1.getHeight() - i7) / 2;
                    i2 = localBitmap1.getWidth();
                }
                for (int i3 = i1 + i7; ; i3 = localBitmap1.getHeight())
                {
                    localRect1 = new Rect(n, i1, i2, i3);
                    int i4 = localCanvas.getWidth();
                    int i5 = localCanvas.getHeight();
                    localRect2 = new Rect(0, 0, i4, i5);
                    break;
                    int m = localBitmap1.getHeight() * localCanvas.getWidth() / localCanvas.getHeight();
                    n = (localBitmap1.getWidth() - m) / 2;
                    i1 = 0;
                    i2 = n + m;
                }
            }
            localFileOutputStream.flush();
            localFileOutputStream.close();
            paramOverlayFrame.setResizedRGBSize(paramInt2, paramInt1);
        }
    }

    public Bitmap getBitmap()
    {
        return this.mBitmap;
    }

    String getBitmapImageFileName()
    {
        return this.mBitmapFileName;
    }

    String getFilename()
    {
        return this.mFilename;
    }

    int getOverlayFrameHeight()
    {
        return this.mOFHeight;
    }

    int getOverlayFrameWidth()
    {
        return this.mOFWidth;
    }

    int getResizedRGBSizeHeight()
    {
        return this.mResizedRGBHeight;
    }

    int getResizedRGBSizeWidth()
    {
        return this.mResizedRGBWidth;
    }

    void invalidate()
    {
        if (this.mBitmap != null)
        {
            this.mBitmap.recycle();
            this.mBitmap = null;
        }
        if (this.mFilename != null)
        {
            new File(this.mFilename).delete();
            this.mFilename = null;
        }
        if (this.mBitmapFileName != null)
        {
            new File(this.mBitmapFileName).delete();
            this.mBitmapFileName = null;
        }
    }

    void invalidateGeneratedFiles()
    {
        if (this.mFilename != null)
        {
            new File(this.mFilename).delete();
            this.mFilename = null;
        }
        if (this.mBitmapFileName != null)
        {
            new File(this.mBitmapFileName).delete();
            this.mBitmapFileName = null;
        }
    }

    String save(String paramString)
        throws FileNotFoundException, IOException
    {
        if (this.mFilename != null);
        for (String str = this.mFilename; ; str = this.mFilename)
        {
            return str;
            this.mBitmapFileName = (paramString + "/" + "Overlay" + getId() + ".png");
            if (!new File(this.mBitmapFileName).exists())
            {
                FileOutputStream localFileOutputStream = new FileOutputStream(this.mBitmapFileName);
                this.mBitmap.compress(Bitmap.CompressFormat.PNG, 100, localFileOutputStream);
                localFileOutputStream.flush();
                localFileOutputStream.close();
            }
            this.mOFWidth = this.mBitmap.getWidth();
            this.mOFHeight = this.mBitmap.getHeight();
            this.mFilename = (paramString + "/" + "Overlay" + getId() + ".rgb");
            Pair[] arrayOfPair = MediaProperties.getSupportedResolutions(super.getMediaItem().getNativeContext().nativeHelperGetAspectRatio());
            Pair localPair = arrayOfPair[(-1 + arrayOfPair.length)];
            generateOverlayWithRenderingMode(super.getMediaItem(), this, ((Integer)localPair.second).intValue(), ((Integer)localPair.first).intValue());
        }
    }

    public void setBitmap(Bitmap paramBitmap)
    {
        getMediaItem().getNativeContext().setGeneratePreview(true);
        invalidate();
        this.mBitmap = paramBitmap;
        if (this.mFilename != null)
        {
            new File(this.mFilename).delete();
            this.mFilename = null;
        }
        getMediaItem().invalidateTransitions(this.mStartTimeMs, this.mDurationMs);
    }

    void setFilename(String paramString)
    {
        this.mFilename = paramString;
    }

    void setOverlayFrameHeight(int paramInt)
    {
        this.mOFHeight = paramInt;
    }

    void setOverlayFrameWidth(int paramInt)
    {
        this.mOFWidth = paramInt;
    }

    void setResizedRGBSize(int paramInt1, int paramInt2)
    {
        this.mResizedRGBWidth = paramInt1;
        this.mResizedRGBHeight = paramInt2;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.media.videoeditor.OverlayFrame
 * JD-Core Version:        0.6.2
 */