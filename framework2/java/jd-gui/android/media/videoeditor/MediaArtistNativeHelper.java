package android.media.videoeditor;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.util.Log;
import android.util.Pair;
import android.view.Surface;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.IntBuffer;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Semaphore;

class MediaArtistNativeHelper
{
    private static final String AUDIO_TRACK_PCM_FILE = "AudioPcm.pcm";
    private static final int MAX_THUMBNAIL_PERMITTED = 8;
    public static final int PROCESSING_AUDIO_PCM = 1;
    public static final int PROCESSING_EXPORT = 20;
    public static final int PROCESSING_INTERMEDIATE1 = 11;
    public static final int PROCESSING_INTERMEDIATE2 = 12;
    public static final int PROCESSING_INTERMEDIATE3 = 13;
    public static final int PROCESSING_KENBURNS = 3;
    public static final int PROCESSING_NONE = 0;
    public static final int PROCESSING_TRANSITION = 2;
    private static final String TAG = "MediaArtistNativeHelper";
    public static final int TASK_ENCODING = 2;
    public static final int TASK_LOADING_SETTINGS = 1;
    private static final Paint sResizePaint = new Paint(2);
    private AudioSettings mAudioSettings = null;
    private AudioTrack mAudioTrack = null;
    private String mAudioTrackPCMFilePath;
    private PreviewClipProperties mClipProperties = null;
    private boolean mErrorFlagSet = false;
    private int mExportAudioCodec = 0;
    private String mExportFilename = null;
    private VideoEditor.ExportProgressListener mExportProgressListener;
    private int mExportVideoCodec = 0;
    private ExtractAudioWaveformProgressListener mExtractAudioWaveformProgressListener;
    private boolean mInvalidatePreviewArray = true;
    private boolean mIsFirstProgress;
    private final Semaphore mLock;
    private int mManualEditContext;
    private VideoEditor.MediaProcessingProgressListener mMediaProcessingProgressListener;
    private String mOutputFilename;
    private EditSettings mPreviewEditSettings;
    private long mPreviewProgress;
    private VideoEditor.PreviewProgressListener mPreviewProgressListener;
    private Object mProcessingObject;
    private int mProcessingState;
    private int mProgressToApp;
    private final String mProjectPath;
    private boolean mRegenerateAudio = true;
    private String mRenderPreviewOverlayFile;
    private int mRenderPreviewRenderingMode;
    private EditSettings mStoryBoardSettings;
    private int mTotalClips = 0;
    private final VideoEditor mVideoEditor;

    static
    {
        System.loadLibrary("videoeditor_jni");
    }

    public MediaArtistNativeHelper(String paramString, Semaphore paramSemaphore, VideoEditor paramVideoEditor)
    {
        this.mProjectPath = paramString;
        if (paramVideoEditor != null)
        {
            this.mVideoEditor = paramVideoEditor;
            if (this.mStoryBoardSettings == null)
                this.mStoryBoardSettings = new EditSettings();
            this.mLock = paramSemaphore;
            _init(this.mProjectPath, "null");
            this.mAudioTrackPCMFilePath = null;
            return;
        }
        this.mVideoEditor = null;
        throw new IllegalArgumentException("video editor object is null");
    }

    private native void _init(String paramString1, String paramString2)
        throws IllegalArgumentException, IllegalStateException, RuntimeException;

    private void adjustMediaItemBoundary(ClipSettings paramClipSettings, Properties paramProperties, MediaItem paramMediaItem)
    {
        if ((paramMediaItem.getBeginTransition() != null) && (paramMediaItem.getBeginTransition().getDuration() > 0L) && (paramMediaItem.getEndTransition() != null) && (paramMediaItem.getEndTransition().getDuration() > 0L))
        {
            paramClipSettings.beginCutTime = ((int)(paramClipSettings.beginCutTime + paramMediaItem.getBeginTransition().getDuration()));
            paramClipSettings.endCutTime = ((int)(paramClipSettings.endCutTime - paramMediaItem.getEndTransition().getDuration()));
        }
        while (true)
        {
            paramProperties.duration = (paramClipSettings.endCutTime - paramClipSettings.beginCutTime);
            if (paramProperties.videoDuration != 0)
                paramProperties.videoDuration = (paramClipSettings.endCutTime - paramClipSettings.beginCutTime);
            if (paramProperties.audioDuration != 0)
                paramProperties.audioDuration = (paramClipSettings.endCutTime - paramClipSettings.beginCutTime);
            return;
            if ((paramMediaItem.getBeginTransition() == null) && (paramMediaItem.getEndTransition() != null) && (paramMediaItem.getEndTransition().getDuration() > 0L))
                paramClipSettings.endCutTime = ((int)(paramClipSettings.endCutTime - paramMediaItem.getEndTransition().getDuration()));
            else if ((paramMediaItem.getEndTransition() == null) && (paramMediaItem.getBeginTransition() != null) && (paramMediaItem.getBeginTransition().getDuration() > 0L))
                paramClipSettings.beginCutTime = ((int)(paramClipSettings.beginCutTime + paramMediaItem.getBeginTransition().getDuration()));
        }
    }

    private void adjustVolume(MediaItem paramMediaItem, PreviewClipProperties paramPreviewClipProperties, int paramInt)
    {
        if ((paramMediaItem instanceof MediaVideoItem))
            if (!((MediaVideoItem)paramMediaItem).isMuted())
                this.mClipProperties.clipProperties[paramInt].audioVolumeValue = ((MediaVideoItem)paramMediaItem).getVolume();
        while (true)
        {
            return;
            this.mClipProperties.clipProperties[paramInt].audioVolumeValue = 0;
            continue;
            if ((paramMediaItem instanceof MediaImageItem))
                this.mClipProperties.clipProperties[paramInt].audioVolumeValue = 0;
        }
    }

    private void checkOddSizeImage(MediaItem paramMediaItem, PreviewClipProperties paramPreviewClipProperties, int paramInt)
    {
        if ((paramMediaItem instanceof MediaImageItem))
        {
            int i = this.mClipProperties.clipProperties[paramInt].width;
            int j = this.mClipProperties.clipProperties[paramInt].height;
            if (i % 2 != 0)
                i--;
            if (j % 2 != 0)
                j--;
            this.mClipProperties.clipProperties[paramInt].width = i;
            this.mClipProperties.clipProperties[paramInt].height = j;
        }
    }

    private int findVideoBitrate(int paramInt)
    {
        int i;
        switch (paramInt)
        {
        default:
            i = 8000000;
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
        case 10:
        case 11:
        case 12:
        }
        while (true)
        {
            return i;
            i = 128000;
            continue;
            i = 384000;
            continue;
            i = 2000000;
            continue;
            i = 5000000;
        }
    }

    private int findVideoResolution(int paramInt1, int paramInt2)
    {
        int i = -1;
        switch (paramInt1)
        {
        default:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        }
        while (true)
        {
            if (i == -1)
            {
                Pair[] arrayOfPair = MediaProperties.getSupportedResolutions(this.mVideoEditor.getAspectRatio());
                Pair localPair = arrayOfPair[(-1 + arrayOfPair.length)];
                i = findVideoResolution(this.mVideoEditor.getAspectRatio(), ((Integer)localPair.second).intValue());
            }
            return i;
            if (paramInt2 == 480)
            {
                i = 7;
            }
            else if (paramInt2 == 720)
            {
                i = 11;
                continue;
                if (paramInt2 == 480)
                {
                    i = 9;
                }
                else if (paramInt2 == 720)
                {
                    i = 10;
                }
                else if (paramInt2 == 1080)
                {
                    i = 13;
                    continue;
                    if (paramInt2 == 480)
                    {
                        i = 5;
                    }
                    else if (paramInt2 == 720)
                    {
                        i = 12;
                        continue;
                        if (paramInt2 == 480)
                        {
                            i = 6;
                            continue;
                            if (paramInt2 == 144)
                                i = 2;
                            else if (paramInt2 == 288)
                                i = 4;
                        }
                    }
                }
            }
        }
    }

    private void generateTransition(Transition paramTransition, EditSettings paramEditSettings, PreviewClipProperties paramPreviewClipProperties, int paramInt)
    {
        if (!paramTransition.isGenerated())
            paramTransition.generate();
        paramEditSettings.clipSettingsArray[paramInt] = new ClipSettings();
        paramEditSettings.clipSettingsArray[paramInt].clipPath = paramTransition.getFilename();
        paramEditSettings.clipSettingsArray[paramInt].fileType = 0;
        paramEditSettings.clipSettingsArray[paramInt].beginCutTime = 0;
        paramEditSettings.clipSettingsArray[paramInt].endCutTime = ((int)paramTransition.getDuration());
        paramEditSettings.clipSettingsArray[paramInt].mediaRendering = 2;
        try
        {
            paramPreviewClipProperties.clipProperties[paramInt] = getMediaProperties(paramTransition.getFilename());
            paramPreviewClipProperties.clipProperties[paramInt].Id = null;
            paramPreviewClipProperties.clipProperties[paramInt].audioVolumeValue = 100;
            paramPreviewClipProperties.clipProperties[paramInt].duration = ((int)paramTransition.getDuration());
            if (paramPreviewClipProperties.clipProperties[paramInt].videoDuration != 0)
                paramPreviewClipProperties.clipProperties[paramInt].videoDuration = ((int)paramTransition.getDuration());
            if (paramPreviewClipProperties.clipProperties[paramInt].audioDuration != 0)
                paramPreviewClipProperties.clipProperties[paramInt].audioDuration = ((int)paramTransition.getDuration());
            return;
        }
        catch (Exception localException)
        {
        }
        throw new IllegalArgumentException("Unsupported file or file not found");
    }

    private int getEffectColorType(EffectColor paramEffectColor)
    {
        int i;
        switch (paramEffectColor.getType())
        {
        default:
            i = -1;
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        }
        while (true)
        {
            return i;
            if (paramEffectColor.getColor() == 65280)
            {
                i = 259;
            }
            else if (paramEffectColor.getColor() == 16737996)
            {
                i = 258;
            }
            else if (paramEffectColor.getColor() == 8355711)
            {
                i = 257;
            }
            else
            {
                i = 267;
                continue;
                i = 268;
                continue;
                i = 260;
                continue;
                i = 261;
                continue;
                i = 266;
            }
        }
    }

    private int getTotalEffects(List<MediaItem> paramList)
    {
        int i = 0;
        Iterator localIterator1 = paramList.iterator();
        while (localIterator1.hasNext())
        {
            MediaItem localMediaItem = (MediaItem)localIterator1.next();
            i = i + localMediaItem.getAllEffects().size() + localMediaItem.getAllOverlays().size();
            Iterator localIterator2 = localMediaItem.getAllEffects().iterator();
            while (localIterator2.hasNext())
                if (((Effect)localIterator2.next() instanceof EffectKenBurns))
                    i--;
        }
        return i;
    }

    private int getTransitionResolution(MediaItem paramMediaItem1, MediaItem paramMediaItem2)
    {
        int i = 0;
        int j = 0;
        int k = 0;
        if ((paramMediaItem1 != null) && (paramMediaItem2 != null))
            if ((paramMediaItem1 instanceof MediaVideoItem))
            {
                i = paramMediaItem1.getHeight();
                if (!(paramMediaItem2 instanceof MediaVideoItem))
                    break label84;
                j = paramMediaItem2.getHeight();
                label41: if (i <= j)
                    break label103;
                k = findVideoResolution(this.mVideoEditor.getAspectRatio(), i);
            }
        label84: label103: 
        do
        {
            while (true)
            {
                return k;
                if (!(paramMediaItem1 instanceof MediaImageItem))
                    break;
                i = ((MediaImageItem)paramMediaItem1).getScaledHeight();
                break;
                if (!(paramMediaItem2 instanceof MediaImageItem))
                    break label41;
                j = ((MediaImageItem)paramMediaItem2).getScaledHeight();
                break label41;
                k = findVideoResolution(this.mVideoEditor.getAspectRatio(), j);
            }
            if ((paramMediaItem1 == null) && (paramMediaItem2 != null))
            {
                if ((paramMediaItem2 instanceof MediaVideoItem))
                    j = paramMediaItem2.getHeight();
                while (true)
                {
                    k = findVideoResolution(this.mVideoEditor.getAspectRatio(), j);
                    break;
                    if ((paramMediaItem2 instanceof MediaImageItem))
                        j = ((MediaImageItem)paramMediaItem2).getScaledHeight();
                }
            }
        }
        while ((paramMediaItem1 == null) || (paramMediaItem2 != null));
        if ((paramMediaItem1 instanceof MediaVideoItem))
            i = paramMediaItem1.getHeight();
        while (true)
        {
            k = findVideoResolution(this.mVideoEditor.getAspectRatio(), i);
            break;
            if ((paramMediaItem1 instanceof MediaImageItem))
                i = ((MediaImageItem)paramMediaItem1).getScaledHeight();
        }
    }

    private static native Version getVersion()
        throws RuntimeException;

    private void lock()
        throws InterruptedException
    {
        if (Log.isLoggable("MediaArtistNativeHelper", 3))
            Log.d("MediaArtistNativeHelper", "lock: grabbing semaphore", new Throwable());
        this.mLock.acquire();
        if (Log.isLoggable("MediaArtistNativeHelper", 3))
            Log.d("MediaArtistNativeHelper", "lock: grabbed semaphore");
    }

    private native void nativeClearSurface(Surface paramSurface);

    private native int nativeGenerateAudioGraph(String paramString1, String paramString2, int paramInt1, int paramInt2, int paramInt3);

    private native int nativeGenerateClip(EditSettings paramEditSettings)
        throws IllegalArgumentException, IllegalStateException, RuntimeException;

    private native int nativeGenerateRawAudio(String paramString1, String paramString2);

    private native int nativeGetPixels(String paramString, int[] paramArrayOfInt, int paramInt1, int paramInt2, long paramLong);

    private native int nativeGetPixelsList(String paramString, int[] paramArrayOfInt1, int paramInt1, int paramInt2, int paramInt3, long paramLong1, long paramLong2, int[] paramArrayOfInt2, NativeGetPixelsListCallback paramNativeGetPixelsListCallback);

    private native void nativePopulateSettings(EditSettings paramEditSettings, PreviewClipProperties paramPreviewClipProperties, AudioSettings paramAudioSettings)
        throws IllegalArgumentException, IllegalStateException, RuntimeException;

    private native int nativeRenderMediaItemPreviewFrame(Surface paramSurface, String paramString, int paramInt1, int paramInt2, int paramInt3, int paramInt4, long paramLong)
        throws IllegalArgumentException, IllegalStateException, RuntimeException;

    private native int nativeRenderPreviewFrame(Surface paramSurface, long paramLong, int paramInt1, int paramInt2)
        throws IllegalArgumentException, IllegalStateException, RuntimeException;

    private native void nativeStartPreview(Surface paramSurface, long paramLong1, long paramLong2, int paramInt, boolean paramBoolean)
        throws IllegalArgumentException, IllegalStateException, RuntimeException;

    private native int nativeStopPreview();

    private void onAudioGraphExtractProgressUpdate(int paramInt, boolean paramBoolean)
    {
        if ((this.mExtractAudioWaveformProgressListener != null) && (paramInt > 0))
            this.mExtractAudioWaveformProgressListener.onProgress(paramInt);
    }

    private void onPreviewProgressUpdate(int paramInt1, boolean paramBoolean1, boolean paramBoolean2, String paramString, int paramInt2, int paramInt3)
    {
        VideoEditor.OverlayData localOverlayData;
        if (this.mPreviewProgressListener != null)
        {
            if (this.mIsFirstProgress)
            {
                this.mPreviewProgressListener.onStart(this.mVideoEditor);
                this.mIsFirstProgress = false;
            }
            if (!paramBoolean2)
                break label98;
            localOverlayData = new VideoEditor.OverlayData();
            if (paramString == null)
                break label90;
            localOverlayData.set(BitmapFactory.decodeFile(paramString), paramInt2);
            if (paramInt1 != 0)
                this.mPreviewProgress = paramInt1;
            if (!paramBoolean1)
                break label104;
            this.mPreviewProgressListener.onStop(this.mVideoEditor);
        }
        while (true)
        {
            return;
            label90: localOverlayData.setClear();
            break;
            label98: localOverlayData = null;
            break;
            label104: if (paramInt3 != 0)
                this.mPreviewProgressListener.onError(this.mVideoEditor, paramInt3);
            else
                this.mPreviewProgressListener.onProgress(this.mVideoEditor, paramInt1, localOverlayData);
        }
    }

    private void onProgressUpdate(int paramInt1, int paramInt2)
    {
        if (this.mProcessingState == 20)
        {
            if ((this.mExportProgressListener != null) && (this.mProgressToApp < paramInt2))
            {
                this.mExportProgressListener.onProgress(this.mVideoEditor, this.mOutputFilename, paramInt2);
                this.mProgressToApp = paramInt2;
            }
            label47: return;
        }
        int i = 0;
        if (this.mProcessingState == 1);
        for (int j = 2; ; j = 1)
            switch (this.mProcessingState)
            {
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            default:
                Log.e("MediaArtistNativeHelper", "ERROR unexpected State=" + this.mProcessingState);
                break label47;
            case 1:
            case 2:
            case 3:
            case 11:
            case 12:
            case 13:
            }
        i = paramInt2;
        while (true)
        {
            if ((this.mProgressToApp != i) && (i != 0))
            {
                this.mProgressToApp = i;
                if (this.mMediaProcessingProgressListener != null)
                    this.mMediaProcessingProgressListener.onProgress(this.mProcessingObject, j, i);
            }
            if (this.mProgressToApp != 0)
                break;
            if (this.mMediaProcessingProgressListener != null)
                this.mMediaProcessingProgressListener.onProgress(this.mProcessingObject, j, i);
            this.mProgressToApp = 1;
            break;
            i = paramInt2;
            continue;
            i = paramInt2;
            continue;
            if ((paramInt2 == 0) && (this.mProgressToApp != 0))
                this.mProgressToApp = 0;
            if ((paramInt2 != 0) || (this.mProgressToApp != 0))
            {
                i = paramInt2 / 4;
                continue;
                if ((paramInt2 != 0) || (this.mProgressToApp != 0))
                {
                    i = 25 + paramInt2 / 4;
                    continue;
                    if ((paramInt2 != 0) || (this.mProgressToApp != 0))
                        i = 50 + paramInt2 / 2;
                }
            }
        }
    }

    private void populateBackgroundMusicProperties(List<AudioTrack> paramList)
    {
        if (paramList.size() == 1)
        {
            this.mAudioTrack = ((AudioTrack)paramList.get(0));
            if (this.mAudioTrack == null)
                break label556;
            this.mAudioSettings = new AudioSettings();
            new Properties();
            this.mAudioSettings.pFile = null;
            this.mAudioSettings.Id = this.mAudioTrack.getId();
        }
        while (true)
        {
            try
            {
                Properties localProperties = getMediaProperties(this.mAudioTrack.getFilename());
                this.mAudioSettings.bRemoveOriginal = false;
                this.mAudioSettings.channels = localProperties.audioChannels;
                this.mAudioSettings.Fs = localProperties.audioSamplingFrequency;
                this.mAudioSettings.loop = this.mAudioTrack.isLooping();
                this.mAudioSettings.ExtendedFs = 0;
                this.mAudioSettings.pFile = this.mAudioTrack.getFilename();
                this.mAudioSettings.startMs = this.mAudioTrack.getStartTime();
                this.mAudioSettings.beginCutTime = this.mAudioTrack.getBoundaryBeginTime();
                this.mAudioSettings.endCutTime = this.mAudioTrack.getBoundaryEndTime();
                if (this.mAudioTrack.isMuted())
                {
                    this.mAudioSettings.volume = 0;
                    this.mAudioSettings.fileType = localProperties.fileType;
                    this.mAudioSettings.ducking_lowVolume = this.mAudioTrack.getDuckedTrackVolume();
                    this.mAudioSettings.ducking_threshold = this.mAudioTrack.getDuckingThreshhold();
                    this.mAudioSettings.bInDucking_enable = this.mAudioTrack.isDuckingEnabled();
                    this.mAudioTrackPCMFilePath = String.format(this.mProjectPath + "/" + "AudioPcm.pcm", new Object[0]);
                    this.mAudioSettings.pcmFilePath = this.mAudioTrackPCMFilePath;
                    this.mPreviewEditSettings.backgroundMusicSettings = new BackgroundMusicSettings();
                    this.mPreviewEditSettings.backgroundMusicSettings.file = this.mAudioTrackPCMFilePath;
                    this.mPreviewEditSettings.backgroundMusicSettings.fileType = localProperties.fileType;
                    this.mPreviewEditSettings.backgroundMusicSettings.insertionTime = this.mAudioTrack.getStartTime();
                    this.mPreviewEditSettings.backgroundMusicSettings.volumePercent = this.mAudioTrack.getVolume();
                    this.mPreviewEditSettings.backgroundMusicSettings.beginLoop = this.mAudioTrack.getBoundaryBeginTime();
                    this.mPreviewEditSettings.backgroundMusicSettings.endLoop = this.mAudioTrack.getBoundaryEndTime();
                    this.mPreviewEditSettings.backgroundMusicSettings.enableDucking = this.mAudioTrack.isDuckingEnabled();
                    this.mPreviewEditSettings.backgroundMusicSettings.duckingThreshold = this.mAudioTrack.getDuckingThreshhold();
                    this.mPreviewEditSettings.backgroundMusicSettings.lowVolume = this.mAudioTrack.getDuckedTrackVolume();
                    this.mPreviewEditSettings.backgroundMusicSettings.isLooping = this.mAudioTrack.isLooping();
                    this.mPreviewEditSettings.primaryTrackVolume = 100;
                    this.mProcessingState = 1;
                    this.mProcessingObject = this.mAudioTrack;
                    return;
                    this.mAudioTrack = null;
                }
            }
            catch (Exception localException)
            {
                throw new IllegalArgumentException("Unsupported file or file not found");
            }
            this.mAudioSettings.volume = this.mAudioTrack.getVolume();
            continue;
            label556: this.mAudioSettings = null;
            this.mPreviewEditSettings.backgroundMusicSettings = null;
            this.mAudioTrackPCMFilePath = null;
        }
    }

    private int populateEffects(MediaItem paramMediaItem, EffectSettings[] paramArrayOfEffectSettings, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        if ((paramMediaItem.getBeginTransition() != null) && (paramMediaItem.getBeginTransition().getDuration() > 0L) && (paramMediaItem.getEndTransition() != null) && (paramMediaItem.getEndTransition().getDuration() > 0L))
        {
            paramInt2 = (int)(paramInt2 + paramMediaItem.getBeginTransition().getDuration());
            paramInt3 = (int)(paramInt3 - paramMediaItem.getEndTransition().getDuration());
        }
        List localList;
        while (true)
        {
            localList = paramMediaItem.getAllEffects();
            Iterator localIterator1 = paramMediaItem.getAllOverlays().iterator();
            while (localIterator1.hasNext())
            {
                paramArrayOfEffectSettings[paramInt1] = getOverlaySettings((OverlayFrame)(Overlay)localIterator1.next());
                adjustEffectsStartTimeAndDuration(paramArrayOfEffectSettings[paramInt1], paramInt2, paramInt3);
                EffectSettings localEffectSettings2 = paramArrayOfEffectSettings[paramInt1];
                localEffectSettings2.startTime = (paramInt4 + localEffectSettings2.startTime);
                paramInt1++;
            }
            if ((paramMediaItem.getBeginTransition() == null) && (paramMediaItem.getEndTransition() != null) && (paramMediaItem.getEndTransition().getDuration() > 0L))
                paramInt3 = (int)(paramInt3 - paramMediaItem.getEndTransition().getDuration());
            else if ((paramMediaItem.getEndTransition() == null) && (paramMediaItem.getBeginTransition() != null) && (paramMediaItem.getBeginTransition().getDuration() > 0L))
                paramInt2 = (int)(paramInt2 + paramMediaItem.getBeginTransition().getDuration());
        }
        Iterator localIterator2 = localList.iterator();
        while (localIterator2.hasNext())
        {
            Effect localEffect = (Effect)localIterator2.next();
            if ((localEffect instanceof EffectColor))
            {
                paramArrayOfEffectSettings[paramInt1] = getEffectSettings((EffectColor)localEffect);
                adjustEffectsStartTimeAndDuration(paramArrayOfEffectSettings[paramInt1], paramInt2, paramInt3);
                EffectSettings localEffectSettings1 = paramArrayOfEffectSettings[paramInt1];
                localEffectSettings1.startTime = (paramInt4 + localEffectSettings1.startTime);
                paramInt1++;
            }
        }
        return paramInt1;
    }

    private int populateMediaItemProperties(MediaItem paramMediaItem, int paramInt1, int paramInt2)
    {
        this.mPreviewEditSettings.clipSettingsArray[paramInt1] = new ClipSettings();
        if ((paramMediaItem instanceof MediaVideoItem))
        {
            this.mPreviewEditSettings.clipSettingsArray[paramInt1] = ((MediaVideoItem)paramMediaItem).getVideoClipProperties();
            if (((MediaVideoItem)paramMediaItem).getHeight() > paramInt2)
                paramInt2 = ((MediaVideoItem)paramMediaItem).getHeight();
        }
        while (true)
        {
            if (this.mPreviewEditSettings.clipSettingsArray[paramInt1].fileType == 5)
            {
                this.mPreviewEditSettings.clipSettingsArray[paramInt1].clipDecodedPath = ((MediaImageItem)paramMediaItem).getDecodedImageFileName();
                this.mPreviewEditSettings.clipSettingsArray[paramInt1].clipOriginalPath = this.mPreviewEditSettings.clipSettingsArray[paramInt1].clipPath;
            }
            return paramInt2;
            if ((paramMediaItem instanceof MediaImageItem))
            {
                this.mPreviewEditSettings.clipSettingsArray[paramInt1] = ((MediaImageItem)paramMediaItem).getImageClipProperties();
                if (((MediaImageItem)paramMediaItem).getScaledHeight() > paramInt2)
                    paramInt2 = ((MediaImageItem)paramMediaItem).getScaledHeight();
            }
        }
    }

    private void previewFrameEditInfo(String paramString, int paramInt)
    {
        this.mRenderPreviewOverlayFile = paramString;
        this.mRenderPreviewRenderingMode = paramInt;
    }

    private native void release()
        throws IllegalStateException, RuntimeException;

    private native void stopEncoding()
        throws IllegalStateException, RuntimeException;

    private void unlock()
    {
        if (Log.isLoggable("MediaArtistNativeHelper", 3))
            Log.d("MediaArtistNativeHelper", "unlock: releasing semaphore");
        this.mLock.release();
    }

    int GetClosestVideoFrameRate(int paramInt)
    {
        int i = 7;
        if (paramInt >= 25);
        while (true)
        {
            return i;
            if (paramInt >= 20)
                i = 6;
            else if (paramInt >= 15)
                i = 5;
            else if (paramInt >= 12)
                i = 4;
            else if (paramInt >= 10)
                i = 3;
            else if (paramInt >= i)
                i = 2;
            else if (paramInt >= 5)
                i = 1;
            else
                i = -1;
        }
    }

    public void adjustEffectsStartTimeAndDuration(EffectSettings paramEffectSettings, int paramInt1, int paramInt2)
    {
        if ((paramEffectSettings.startTime > paramInt2) || (paramEffectSettings.startTime + paramEffectSettings.duration <= paramInt1))
        {
            paramEffectSettings.startTime = 0;
            paramEffectSettings.duration = 0;
        }
        while (true)
        {
            return;
            if ((paramEffectSettings.startTime < paramInt1) && (paramEffectSettings.startTime + paramEffectSettings.duration > paramInt1) && (paramEffectSettings.startTime + paramEffectSettings.duration <= paramInt2))
            {
                int m = paramEffectSettings.duration - (paramInt1 - paramEffectSettings.startTime);
                paramEffectSettings.startTime = 0;
                paramEffectSettings.duration = m;
            }
            else if ((paramEffectSettings.startTime >= paramInt1) && (paramEffectSettings.startTime + paramEffectSettings.duration <= paramInt2))
            {
                paramEffectSettings.startTime -= paramInt1;
                paramEffectSettings.duration = paramEffectSettings.duration;
            }
            else if ((paramEffectSettings.startTime >= paramInt1) && (paramEffectSettings.startTime + paramEffectSettings.duration > paramInt2))
            {
                int j = paramEffectSettings.startTime - paramInt1;
                int k = paramInt2 - paramEffectSettings.startTime;
                paramEffectSettings.startTime = j;
                paramEffectSettings.duration = k;
            }
            else if ((paramEffectSettings.startTime < paramInt1) && (paramEffectSettings.startTime + paramEffectSettings.duration > paramInt2))
            {
                int i = paramInt2 - paramInt1;
                paramEffectSettings.startTime = 0;
                paramEffectSettings.duration = i;
            }
        }
    }

    void clearPreviewSurface(Surface paramSurface)
    {
        nativeClearSurface(paramSurface);
    }

    void doPreview(Surface paramSurface, long paramLong1, long paramLong2, boolean paramBoolean, int paramInt, VideoEditor.PreviewProgressListener paramPreviewProgressListener)
    {
        this.mPreviewProgress = paramLong1;
        this.mIsFirstProgress = true;
        this.mPreviewProgressListener = paramPreviewProgressListener;
        if (!this.mInvalidatePreviewArray);
        for (int i = 0; ; i++)
        {
            try
            {
                if (i < this.mPreviewEditSettings.clipSettingsArray.length)
                {
                    if (this.mPreviewEditSettings.clipSettingsArray[i].fileType != 5)
                        continue;
                    this.mPreviewEditSettings.clipSettingsArray[i].clipPath = this.mPreviewEditSettings.clipSettingsArray[i].clipDecodedPath;
                    continue;
                }
                nativePopulateSettings(this.mPreviewEditSettings, this.mClipProperties, this.mAudioSettings);
                nativeStartPreview(paramSurface, paramLong1, paramLong2, paramInt, paramBoolean);
                return;
            }
            catch (IllegalArgumentException localIllegalArgumentException)
            {
                Log.e("MediaArtistNativeHelper", "Illegal argument exception in nativeStartPreview");
                throw localIllegalArgumentException;
            }
            catch (IllegalStateException localIllegalStateException)
            {
                Log.e("MediaArtistNativeHelper", "Illegal state exception in nativeStartPreview");
                throw localIllegalStateException;
            }
            catch (RuntimeException localRuntimeException)
            {
                Log.e("MediaArtistNativeHelper", "Runtime exception in nativeStartPreview");
                throw localRuntimeException;
            }
            throw new IllegalStateException("generatePreview is in progress");
        }
    }

    void export(String paramString1, String paramString2, int paramInt1, int paramInt2, List<MediaItem> paramList, List<Transition> paramList1, List<AudioTrack> paramList2, VideoEditor.ExportProgressListener paramExportProgressListener)
    {
        this.mExportFilename = paramString1;
        previewStoryBoard(paramList, paramList1, paramList2, null);
        this.mExportProgressListener = paramExportProgressListener;
        VideoEditorProfile localVideoEditorProfile = VideoEditorProfile.get();
        if (localVideoEditorProfile == null)
            throw new RuntimeException("Can't get the video editor profile");
        int i = localVideoEditorProfile.maxOutputVideoFrameHeight;
        int j = localVideoEditorProfile.maxOutputVideoFrameWidth;
        if (paramInt1 > i)
            throw new IllegalArgumentException("Unsupported export resolution. Supported maximum width:" + j + " height:" + i + " current height:" + paramInt1);
        int k = VideoEditorProfile.getExportProfile(this.mExportVideoCodec);
        int m = VideoEditorProfile.getExportLevel(this.mExportVideoCodec);
        this.mProgressToApp = 0;
        int n;
        switch (paramInt2)
        {
        default:
            throw new IllegalArgumentException("Argument Bitrate incorrect");
        case 28000:
            n = 32000;
        case 40000:
        case 64000:
        case 96000:
        case 128000:
        case 192000:
        case 256000:
        case 384000:
        case 512000:
        case 800000:
        case 2000000:
        case 5000000:
        case 8000000:
        }
        while (true)
        {
            this.mPreviewEditSettings.videoFrameRate = 7;
            EditSettings localEditSettings = this.mPreviewEditSettings;
            this.mOutputFilename = paramString1;
            localEditSettings.outputFile = paramString1;
            int i1 = this.mVideoEditor.getAspectRatio();
            this.mPreviewEditSettings.videoFrameSize = findVideoResolution(i1, paramInt1);
            this.mPreviewEditSettings.videoFormat = this.mExportVideoCodec;
            this.mPreviewEditSettings.audioFormat = this.mExportAudioCodec;
            this.mPreviewEditSettings.videoProfile = k;
            this.mPreviewEditSettings.videoLevel = m;
            this.mPreviewEditSettings.audioSamplingFreq = 32000;
            this.mPreviewEditSettings.maxFileSize = 0;
            this.mPreviewEditSettings.audioChannels = 2;
            this.mPreviewEditSettings.videoBitrate = n;
            this.mPreviewEditSettings.audioBitrate = 96000;
            this.mPreviewEditSettings.transitionSettingsArray = new TransitionSettings[-1 + this.mTotalClips];
            for (int i2 = 0; i2 < -1 + this.mTotalClips; i2++)
            {
                this.mPreviewEditSettings.transitionSettingsArray[i2] = new TransitionSettings();
                this.mPreviewEditSettings.transitionSettingsArray[i2].videoTransitionType = 0;
                this.mPreviewEditSettings.transitionSettingsArray[i2].audioTransitionType = 0;
            }
            n = 48000;
            continue;
            n = 64000;
            continue;
            n = 96000;
            continue;
            n = 128000;
            continue;
            n = 192000;
            continue;
            n = 256000;
            continue;
            n = 384000;
            continue;
            n = 512000;
            continue;
            n = 800000;
            continue;
            n = 2000000;
            continue;
            n = 5000000;
            continue;
            n = 8000000;
        }
        for (int i3 = 0; i3 < this.mPreviewEditSettings.clipSettingsArray.length; i3++)
            if (this.mPreviewEditSettings.clipSettingsArray[i3].fileType == 5)
                this.mPreviewEditSettings.clipSettingsArray[i3].clipPath = this.mPreviewEditSettings.clipSettingsArray[i3].clipOriginalPath;
        nativePopulateSettings(this.mPreviewEditSettings, this.mClipProperties, this.mAudioSettings);
        try
        {
            this.mProcessingState = 20;
            this.mProcessingObject = null;
            int i4 = generateClip(this.mPreviewEditSettings);
            this.mProcessingState = 0;
            if (i4 != 0)
            {
                Log.e("MediaArtistNativeHelper", "RuntimeException for generateClip");
                throw new RuntimeException("generateClip failed with error=" + i4);
            }
        }
        catch (IllegalArgumentException localIllegalArgumentException)
        {
            Log.e("MediaArtistNativeHelper", "IllegalArgument for generateClip");
            throw localIllegalArgumentException;
        }
        catch (IllegalStateException localIllegalStateException)
        {
            Log.e("MediaArtistNativeHelper", "IllegalStateExceptiont for generateClip");
            throw localIllegalStateException;
        }
        catch (RuntimeException localRuntimeException)
        {
            Log.e("MediaArtistNativeHelper", "RuntimeException for generateClip");
            throw localRuntimeException;
        }
        this.mExportProgressListener = null;
    }

    void generateAudioGraph(String paramString1, String paramString2, String paramString3, int paramInt1, int paramInt2, int paramInt3, ExtractAudioWaveformProgressListener paramExtractAudioWaveformProgressListener, boolean paramBoolean)
    {
        this.mExtractAudioWaveformProgressListener = paramExtractAudioWaveformProgressListener;
        if (paramBoolean);
        for (String str = String.format(this.mProjectPath + "/" + paramString1 + ".pcm", new Object[0]); ; str = this.mAudioTrackPCMFilePath)
        {
            if (paramBoolean)
                nativeGenerateRawAudio(paramString2, str);
            nativeGenerateAudioGraph(str, paramString3, paramInt1, paramInt2, paramInt3);
            if (paramBoolean)
                new File(str).delete();
            return;
        }
    }

    public int generateClip(EditSettings paramEditSettings)
    {
        int i = -1;
        try
        {
            int j = nativeGenerateClip(paramEditSettings);
            i = j;
            return i;
        }
        catch (IllegalArgumentException localIllegalArgumentException)
        {
            while (true)
                Log.e("MediaArtistNativeHelper", "Illegal Argument exception in load settings");
        }
        catch (IllegalStateException localIllegalStateException)
        {
            while (true)
                Log.e("MediaArtistNativeHelper", "Illegal state exception in load settings");
        }
        catch (RuntimeException localRuntimeException)
        {
            while (true)
                Log.e("MediaArtistNativeHelper", "Runtime exception in load settings");
        }
    }

    String generateEffectClip(MediaItem paramMediaItem, ClipSettings paramClipSettings, EditSettings paramEditSettings, String paramString, int paramInt)
    {
        EditSettings localEditSettings = new EditSettings();
        localEditSettings.clipSettingsArray = new ClipSettings[1];
        localEditSettings.clipSettingsArray[0] = paramClipSettings;
        localEditSettings.backgroundMusicSettings = null;
        localEditSettings.transitionSettingsArray = null;
        localEditSettings.effectSettingsArray = paramEditSettings.effectSettingsArray;
        String str = String.format(this.mProjectPath + "/" + "ClipEffectIntermediate" + "_" + paramMediaItem.getId() + paramString + ".3gp", new Object[0]);
        File localFile = new File(str);
        if (localFile.exists())
            localFile.delete();
        int i = VideoEditorProfile.getExportProfile(2);
        int j = VideoEditorProfile.getExportLevel(2);
        localEditSettings.videoProfile = i;
        localEditSettings.videoLevel = j;
        if ((paramMediaItem instanceof MediaVideoItem))
        {
            MediaVideoItem localMediaVideoItem = (MediaVideoItem)paramMediaItem;
            localEditSettings.audioFormat = 2;
            localEditSettings.audioChannels = 2;
            localEditSettings.audioBitrate = 64000;
            localEditSettings.audioSamplingFreq = 32000;
            localEditSettings.videoFormat = 2;
            localEditSettings.videoFrameRate = 7;
            localEditSettings.videoFrameSize = findVideoResolution(this.mVideoEditor.getAspectRatio(), localMediaVideoItem.getHeight());
            localEditSettings.videoBitrate = findVideoBitrate(localEditSettings.videoFrameSize);
            localEditSettings.outputFile = str;
            if (paramInt != 1)
                break label394;
            this.mProcessingState = 11;
        }
        while (true)
        {
            this.mProcessingObject = paramMediaItem;
            int k = generateClip(localEditSettings);
            this.mProcessingState = 0;
            if (k != 0)
                break label409;
            paramClipSettings.clipPath = str;
            paramClipSettings.fileType = 0;
            return str;
            MediaImageItem localMediaImageItem = (MediaImageItem)paramMediaItem;
            localEditSettings.audioBitrate = 64000;
            localEditSettings.audioChannels = 2;
            localEditSettings.audioFormat = 2;
            localEditSettings.audioSamplingFreq = 32000;
            localEditSettings.videoFormat = 2;
            localEditSettings.videoFrameRate = 7;
            localEditSettings.videoFrameSize = findVideoResolution(this.mVideoEditor.getAspectRatio(), localMediaImageItem.getScaledHeight());
            localEditSettings.videoBitrate = findVideoBitrate(localEditSettings.videoFrameSize);
            break;
            label394: if (paramInt == 2)
                this.mProcessingState = 12;
        }
        label409: throw new RuntimeException("preview generation cannot be completed");
    }

    String generateKenBurnsClip(EditSettings paramEditSettings, MediaImageItem paramMediaImageItem)
    {
        paramEditSettings.backgroundMusicSettings = null;
        paramEditSettings.transitionSettingsArray = null;
        paramEditSettings.effectSettingsArray = null;
        String str = String.format(this.mProjectPath + "/" + "ImageClip-" + paramMediaImageItem.getId() + ".3gp", new Object[0]);
        File localFile = new File(str);
        if (localFile.exists())
            localFile.delete();
        int i = VideoEditorProfile.getExportProfile(2);
        int j = VideoEditorProfile.getExportLevel(2);
        paramEditSettings.videoProfile = i;
        paramEditSettings.videoLevel = j;
        paramEditSettings.outputFile = str;
        paramEditSettings.audioBitrate = 64000;
        paramEditSettings.audioChannels = 2;
        paramEditSettings.audioFormat = 2;
        paramEditSettings.audioSamplingFreq = 32000;
        paramEditSettings.videoFormat = 2;
        paramEditSettings.videoFrameRate = 7;
        paramEditSettings.videoFrameSize = findVideoResolution(this.mVideoEditor.getAspectRatio(), paramMediaImageItem.getScaledHeight());
        paramEditSettings.videoBitrate = findVideoBitrate(paramEditSettings.videoFrameSize);
        this.mProcessingState = 3;
        this.mProcessingObject = paramMediaImageItem;
        int k = generateClip(paramEditSettings);
        this.mProcessingState = 0;
        if (k != 0)
            throw new RuntimeException("preview generation cannot be completed");
        return str;
    }

    String generateTransitionClip(EditSettings paramEditSettings, String paramString, MediaItem paramMediaItem1, MediaItem paramMediaItem2, Transition paramTransition)
    {
        String str = String.format(this.mProjectPath + "/" + paramString + ".3gp", new Object[0]);
        int i = VideoEditorProfile.getExportProfile(2);
        int j = VideoEditorProfile.getExportLevel(2);
        paramEditSettings.videoProfile = i;
        paramEditSettings.videoLevel = j;
        paramEditSettings.outputFile = str;
        paramEditSettings.audioBitrate = 64000;
        paramEditSettings.audioChannels = 2;
        paramEditSettings.audioFormat = 2;
        paramEditSettings.audioSamplingFreq = 32000;
        paramEditSettings.videoFormat = 2;
        paramEditSettings.videoFrameRate = 7;
        paramEditSettings.videoFrameSize = getTransitionResolution(paramMediaItem1, paramMediaItem2);
        paramEditSettings.videoBitrate = findVideoBitrate(paramEditSettings.videoFrameSize);
        if (new File(str).exists())
            new File(str).delete();
        this.mProcessingState = 13;
        this.mProcessingObject = paramTransition;
        int k = generateClip(paramEditSettings);
        this.mProcessingState = 0;
        if (k != 0)
            throw new RuntimeException("preview generation cannot be completed");
        return str;
    }

    int getAspectRatio(int paramInt1, int paramInt2)
    {
        double d = new BigDecimal(paramInt1 / paramInt2).setScale(3, 4).doubleValue();
        int i = 2;
        if (d >= 1.7D)
            i = 2;
        while (true)
        {
            return i;
            if (d >= 1.6D)
                i = 4;
            else if (d >= 1.5D)
                i = 1;
            else if (d > 1.3D)
                i = 3;
            else if (d >= 1.2D)
                i = 5;
        }
    }

    int getAudioCodecType(int paramInt)
    {
        int i;
        switch (paramInt)
        {
        case 3:
        case 4:
        default:
            i = -1;
        case 1:
        case 2:
        case 5:
        }
        while (true)
        {
            return i;
            i = 1;
            continue;
            i = 2;
            continue;
            i = 5;
        }
    }

    boolean getAudioflag()
    {
        return this.mRegenerateAudio;
    }

    EffectSettings getEffectSettings(EffectColor paramEffectColor)
    {
        EffectSettings localEffectSettings = new EffectSettings();
        localEffectSettings.startTime = ((int)paramEffectColor.getStartTime());
        localEffectSettings.duration = ((int)paramEffectColor.getDuration());
        localEffectSettings.videoEffectType = getEffectColorType(paramEffectColor);
        localEffectSettings.audioEffectType = 0;
        localEffectSettings.startPercent = 0;
        localEffectSettings.durationPercent = 0;
        localEffectSettings.framingFile = null;
        localEffectSettings.topLeftX = 0;
        localEffectSettings.topLeftY = 0;
        localEffectSettings.framingResize = false;
        localEffectSettings.text = null;
        localEffectSettings.textRenderingData = null;
        localEffectSettings.textBufferWidth = 0;
        localEffectSettings.textBufferHeight = 0;
        if (paramEffectColor.getType() == 5);
        for (localEffectSettings.fiftiesFrameRate = 15; ; localEffectSettings.fiftiesFrameRate = 0)
        {
            if ((localEffectSettings.videoEffectType == 267) || (localEffectSettings.videoEffectType == 268))
                localEffectSettings.rgb16InputColor = paramEffectColor.getColor();
            localEffectSettings.alphaBlendingStartPercent = 0;
            localEffectSettings.alphaBlendingMiddlePercent = 0;
            localEffectSettings.alphaBlendingEndPercent = 0;
            localEffectSettings.alphaBlendingFadeInTimePercent = 0;
            localEffectSettings.alphaBlendingFadeOutTimePercent = 0;
            return localEffectSettings;
        }
    }

    int getFileType(int paramInt)
    {
        int i;
        switch (paramInt)
        {
        default:
            i = -1;
        case 255:
        case 0:
        case 1:
        case 5:
        case 8:
        case 3:
        case 10:
        case 2:
        }
        while (true)
        {
            return i;
            i = 255;
            continue;
            i = 0;
            continue;
            i = 1;
            continue;
            i = 5;
            continue;
            i = 8;
            continue;
            i = 3;
            continue;
            i = 10;
            continue;
            i = 2;
        }
    }

    int getFrameRate(int paramInt)
    {
        int i;
        switch (paramInt)
        {
        default:
            i = -1;
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        }
        while (true)
        {
            return i;
            i = 5;
            continue;
            i = 8;
            continue;
            i = 10;
            continue;
            i = 13;
            continue;
            i = 15;
            continue;
            i = 20;
            continue;
            i = 25;
            continue;
            i = 30;
        }
    }

    boolean getGeneratePreview()
    {
        return this.mInvalidatePreviewArray;
    }

    int getMediaItemFileType(int paramInt)
    {
        int i;
        switch (paramInt)
        {
        default:
            i = -1;
        case 255:
        case 0:
        case 1:
        case 5:
        case 8:
        case 10:
        }
        while (true)
        {
            return i;
            i = 255;
            continue;
            i = 0;
            continue;
            i = 1;
            continue;
            i = 5;
            continue;
            i = 8;
            continue;
            i = 10;
        }
    }

    int getMediaItemRenderingMode(int paramInt)
    {
        int i;
        switch (paramInt)
        {
        default:
            i = -1;
        case 0:
        case 1:
        case 2:
        }
        while (true)
        {
            return i;
            i = 2;
            continue;
            i = 0;
            continue;
            i = 1;
        }
    }

    native Properties getMediaProperties(String paramString)
        throws IllegalArgumentException, IllegalStateException, RuntimeException, Exception;

    EffectSettings getOverlaySettings(OverlayFrame paramOverlayFrame)
    {
        EffectSettings localEffectSettings = new EffectSettings();
        localEffectSettings.startTime = ((int)paramOverlayFrame.getStartTime());
        localEffectSettings.duration = ((int)paramOverlayFrame.getDuration());
        localEffectSettings.videoEffectType = 262;
        localEffectSettings.audioEffectType = 0;
        localEffectSettings.startPercent = 0;
        localEffectSettings.durationPercent = 0;
        localEffectSettings.framingFile = null;
        Bitmap localBitmap = paramOverlayFrame.getBitmap();
        if (localBitmap != null)
        {
            localEffectSettings.framingFile = paramOverlayFrame.getFilename();
            if (localEffectSettings.framingFile == null);
            int m;
            int n;
            while (true)
            {
                int k;
                try
                {
                    paramOverlayFrame.save(this.mProjectPath);
                    localEffectSettings.framingFile = paramOverlayFrame.getFilename();
                    if (localBitmap.getConfig() == Bitmap.Config.ARGB_8888)
                    {
                        localEffectSettings.bitmapType = 6;
                        localEffectSettings.width = localBitmap.getWidth();
                        localEffectSettings.height = localBitmap.getHeight();
                        localEffectSettings.framingBuffer = new int[localEffectSettings.width];
                        k = 0;
                        m = 0;
                        n = 255;
                        if (k >= localEffectSettings.height)
                            break;
                        localBitmap.getPixels(localEffectSettings.framingBuffer, 0, localEffectSettings.width, 0, k, localEffectSettings.width, 1);
                        int i2 = 0;
                        if (i2 >= localEffectSettings.width)
                            break label305;
                        int i3 = (short)(0xFF & localEffectSettings.framingBuffer[i2] >> 24);
                        if (i3 > m)
                            m = i3;
                        if (i3 < n)
                            n = i3;
                        i2++;
                        continue;
                    }
                }
                catch (IOException localIOException)
                {
                    Log.e("MediaArtistNativeHelper", "getOverlaySettings : File not found");
                    continue;
                    if (localBitmap.getConfig() == Bitmap.Config.ARGB_4444)
                    {
                        localEffectSettings.bitmapType = 5;
                        continue;
                    }
                    if (localBitmap.getConfig() == Bitmap.Config.RGB_565)
                    {
                        localEffectSettings.bitmapType = 4;
                        continue;
                    }
                    if (localBitmap.getConfig() != Bitmap.Config.ALPHA_8)
                        continue;
                    throw new RuntimeException("Bitmap config not supported");
                }
                label305: k++;
            }
            int i1 = 100 * ((m + n) / 2) / 256;
            localEffectSettings.alphaBlendingEndPercent = i1;
            localEffectSettings.alphaBlendingMiddlePercent = i1;
            localEffectSettings.alphaBlendingStartPercent = i1;
            localEffectSettings.alphaBlendingFadeInTimePercent = 100;
            localEffectSettings.alphaBlendingFadeOutTimePercent = 100;
            localEffectSettings.framingBuffer = null;
            localEffectSettings.width = paramOverlayFrame.getResizedRGBSizeWidth();
            if (localEffectSettings.width == 0)
                localEffectSettings.width = localBitmap.getWidth();
            localEffectSettings.height = paramOverlayFrame.getResizedRGBSizeHeight();
            if (localEffectSettings.height == 0)
                localEffectSettings.height = localBitmap.getHeight();
        }
        localEffectSettings.topLeftX = 0;
        localEffectSettings.topLeftY = 0;
        localEffectSettings.framingResize = true;
        localEffectSettings.text = null;
        localEffectSettings.textRenderingData = null;
        localEffectSettings.textBufferWidth = 0;
        localEffectSettings.textBufferHeight = 0;
        localEffectSettings.fiftiesFrameRate = 0;
        localEffectSettings.rgb16InputColor = 0;
        int j;
        int i;
        if ((paramOverlayFrame.getMediaItem() instanceof MediaImageItem))
            if (((MediaImageItem)paramOverlayFrame.getMediaItem()).getGeneratedImageClip() != null)
            {
                j = ((MediaImageItem)paramOverlayFrame.getMediaItem()).getGeneratedClipHeight();
                i = getAspectRatio(((MediaImageItem)paramOverlayFrame.getMediaItem()).getGeneratedClipWidth(), j);
            }
        while (true)
        {
            localEffectSettings.framingScaledSize = findVideoResolution(i, j);
            return localEffectSettings;
            j = ((MediaImageItem)paramOverlayFrame.getMediaItem()).getScaledHeight();
            i = paramOverlayFrame.getMediaItem().getAspectRatio();
            continue;
            i = paramOverlayFrame.getMediaItem().getAspectRatio();
            j = paramOverlayFrame.getMediaItem().getHeight();
        }
    }

    Bitmap getPixels(String paramString, int paramInt1, int paramInt2, long paramLong, int paramInt3)
    {
        final Bitmap[] arrayOfBitmap = new Bitmap[1];
        int[] arrayOfInt = new int[1];
        arrayOfInt[0] = 0;
        getPixelsList(paramString, paramInt1, paramInt2, paramLong, paramLong, 1, arrayOfInt, new MediaItem.GetThumbnailListCallback()
        {
            public void onThumbnail(Bitmap paramAnonymousBitmap, int paramAnonymousInt)
            {
                arrayOfBitmap[0] = paramAnonymousBitmap;
            }
        }
        , paramInt3);
        return arrayOfBitmap[0];
    }

    void getPixelsList(String paramString, int paramInt1, int paramInt2, long paramLong1, long paramLong2, int paramInt3, int[] paramArrayOfInt, final MediaItem.GetThumbnailListCallback paramGetThumbnailListCallback, final int paramInt4)
    {
        final int i = 0xFFFFFFFE & paramInt1 + 1;
        final int j = 0xFFFFFFFE & paramInt2 + 1;
        final int k = i * j;
        final int[] arrayOfInt = new int[k];
        final IntBuffer localIntBuffer = IntBuffer.allocate(k);
        final boolean bool;
        final Bitmap localBitmap;
        label73: int m;
        label91: final int n;
        if ((i != paramInt1) || (j != paramInt2) || (paramInt4 != 0))
        {
            bool = true;
            if (!bool)
                break label174;
            localBitmap = Bitmap.createBitmap(i, j, Bitmap.Config.ARGB_8888);
            if ((paramInt4 != 90) && (paramInt4 != 270))
                break label180;
            m = 1;
            if (m == 0)
                break label186;
            n = paramInt2;
            label99: if (m == 0)
                break label192;
        }
        label174: label180: label186: label192: for (final int i1 = paramInt1; ; i1 = paramInt2)
        {
            nativeGetPixelsList(paramString, arrayOfInt, i, j, paramInt3, paramLong1, paramLong2, paramArrayOfInt, new NativeGetPixelsListCallback()
            {
                public void onThumbnail(int paramAnonymousInt)
                {
                    Bitmap localBitmap = Bitmap.createBitmap(n, i1, Bitmap.Config.ARGB_8888);
                    localIntBuffer.put(arrayOfInt, 0, k);
                    localIntBuffer.rewind();
                    if (!bool)
                        localBitmap.copyPixelsFromBuffer(localIntBuffer);
                    while (true)
                    {
                        paramGetThumbnailListCallback.onThumbnail(localBitmap, paramAnonymousInt);
                        return;
                        localBitmap.copyPixelsFromBuffer(localIntBuffer);
                        Canvas localCanvas = new Canvas(localBitmap);
                        Matrix localMatrix = new Matrix();
                        localMatrix.postScale(1.0F / i, 1.0F / j);
                        localMatrix.postRotate(paramInt4, 0.5F, 0.5F);
                        localMatrix.postScale(n, i1);
                        localCanvas.drawBitmap(localBitmap, localMatrix, MediaArtistNativeHelper.sResizePaint);
                    }
                }
            });
            if (localBitmap != null)
                localBitmap.recycle();
            return;
            bool = false;
            break;
            localBitmap = null;
            break label73;
            m = 0;
            break label91;
            n = paramInt1;
            break label99;
        }
    }

    String getProjectAudioTrackPCMFilePath()
    {
        return this.mAudioTrackPCMFilePath;
    }

    String getProjectPath()
    {
        return this.mProjectPath;
    }

    int getSlideSettingsDirection(int paramInt)
    {
        int i;
        switch (paramInt)
        {
        default:
            i = -1;
        case 0:
        case 1:
        case 2:
        case 3:
        }
        while (true)
        {
            return i;
            i = 0;
            continue;
            i = 1;
            continue;
            i = 2;
            continue;
            i = 3;
        }
    }

    int getVideoCodecType(int paramInt)
    {
        int i;
        switch (paramInt)
        {
        default:
            i = -1;
        case 1:
        case 2:
        case 3:
        }
        while (true)
        {
            return i;
            i = 1;
            continue;
            i = 2;
            continue;
            i = 3;
        }
    }

    int getVideoTransitionBehaviour(int paramInt)
    {
        int i;
        switch (paramInt)
        {
        default:
            i = -1;
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        }
        while (true)
        {
            return i;
            i = 0;
            continue;
            i = 2;
            continue;
            i = 1;
            continue;
            i = 3;
            continue;
            i = 4;
        }
    }

    void initClipSettings(ClipSettings paramClipSettings)
    {
        paramClipSettings.clipPath = null;
        paramClipSettings.clipDecodedPath = null;
        paramClipSettings.clipOriginalPath = null;
        paramClipSettings.fileType = 0;
        paramClipSettings.endCutTime = 0;
        paramClipSettings.beginCutTime = 0;
        paramClipSettings.beginCutPercent = 0;
        paramClipSettings.endCutPercent = 0;
        paramClipSettings.panZoomEnabled = false;
        paramClipSettings.panZoomPercentStart = 0;
        paramClipSettings.panZoomTopLeftXStart = 0;
        paramClipSettings.panZoomTopLeftYStart = 0;
        paramClipSettings.panZoomPercentEnd = 0;
        paramClipSettings.panZoomTopLeftXEnd = 0;
        paramClipSettings.panZoomTopLeftYEnd = 0;
        paramClipSettings.mediaRendering = 0;
        paramClipSettings.rotationDegree = 0;
    }

    void invalidatePcmFile()
    {
        if (this.mAudioTrackPCMFilePath != null)
        {
            new File(this.mAudioTrackPCMFilePath).delete();
            this.mAudioTrackPCMFilePath = null;
        }
    }

    int nativeHelperGetAspectRatio()
    {
        return this.mVideoEditor.getAspectRatio();
    }

    // ERROR //
    void previewStoryBoard(List<MediaItem> paramList, List<Transition> paramList1, List<AudioTrack> paramList2, VideoEditor.MediaProcessingProgressListener paramMediaProcessingProgressListener)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 199	android/media/videoeditor/MediaArtistNativeHelper:mInvalidatePreviewArray	Z
        //     4: ifeq +916 -> 920
        //     7: iconst_0
        //     8: istore 5
        //     10: iconst_0
        //     11: istore 6
        //     13: iconst_0
        //     14: istore 7
        //     16: iconst_0
        //     17: istore 8
        //     19: iconst_0
        //     20: istore 9
        //     22: iconst_0
        //     23: istore 10
        //     25: aload_0
        //     26: new 16	android/media/videoeditor/MediaArtistNativeHelper$EditSettings
        //     29: dup
        //     30: invokespecial 218	android/media/videoeditor/MediaArtistNativeHelper$EditSettings:<init>	()V
        //     33: putfield 657	android/media/videoeditor/MediaArtistNativeHelper:mPreviewEditSettings	Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;
        //     36: aload_0
        //     37: new 19	android/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties
        //     40: dup
        //     41: invokespecial 1152	android/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties:<init>	()V
        //     44: putfield 193	android/media/videoeditor/MediaArtistNativeHelper:mClipProperties	Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;
        //     47: aload_0
        //     48: iconst_0
        //     49: putfield 209	android/media/videoeditor/MediaArtistNativeHelper:mTotalClips	I
        //     52: aload_0
        //     53: aload_1
        //     54: invokeinterface 406 1 0
        //     59: putfield 209	android/media/videoeditor/MediaArtistNativeHelper:mTotalClips	I
        //     62: aload_2
        //     63: invokeinterface 390 1 0
        //     68: astore 11
        //     70: aload 11
        //     72: invokeinterface 395 1 0
        //     77: ifeq +34 -> 111
        //     80: aload 11
        //     82: invokeinterface 399 1 0
        //     87: checkcast 250	android/media/videoeditor/Transition
        //     90: invokevirtual 254	android/media/videoeditor/Transition:getDuration	()J
        //     93: lconst_0
        //     94: lcmp
        //     95: ifle -25 -> 70
        //     98: aload_0
        //     99: iconst_1
        //     100: aload_0
        //     101: getfield 209	android/media/videoeditor/MediaArtistNativeHelper:mTotalClips	I
        //     104: iadd
        //     105: putfield 209	android/media/videoeditor/MediaArtistNativeHelper:mTotalClips	I
        //     108: goto -38 -> 70
        //     111: aload_0
        //     112: aload_1
        //     113: invokespecial 1154	android/media/videoeditor/MediaArtistNativeHelper:getTotalEffects	(Ljava/util/List;)I
        //     116: istore 12
        //     118: aload_0
        //     119: getfield 657	android/media/videoeditor/MediaArtistNativeHelper:mPreviewEditSettings	Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;
        //     122: aload_0
        //     123: getfield 209	android/media/videoeditor/MediaArtistNativeHelper:mTotalClips	I
        //     126: anewarray 49	android/media/videoeditor/MediaArtistNativeHelper$ClipSettings
        //     129: putfield 346	android/media/videoeditor/MediaArtistNativeHelper$EditSettings:clipSettingsArray	[Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;
        //     132: aload_0
        //     133: getfield 657	android/media/videoeditor/MediaArtistNativeHelper:mPreviewEditSettings	Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;
        //     136: iload 12
        //     138: anewarray 31	android/media/videoeditor/MediaArtistNativeHelper$EffectSettings
        //     141: putfield 891	android/media/videoeditor/MediaArtistNativeHelper$EditSettings:effectSettingsArray	[Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;
        //     144: aload_0
        //     145: getfield 193	android/media/videoeditor/MediaArtistNativeHelper:mClipProperties	Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;
        //     148: aload_0
        //     149: getfield 209	android/media/videoeditor/MediaArtistNativeHelper:mTotalClips	I
        //     152: anewarray 13	android/media/videoeditor/MediaArtistNativeHelper$Properties
        //     155: putfield 284	android/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties:clipProperties	[Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;
        //     158: aload_0
        //     159: aload 4
        //     161: putfield 555	android/media/videoeditor/MediaArtistNativeHelper:mMediaProcessingProgressListener	Landroid/media/videoeditor/VideoEditor$MediaProcessingProgressListener;
        //     164: aload_0
        //     165: iconst_0
        //     166: putfield 528	android/media/videoeditor/MediaArtistNativeHelper:mProgressToApp	I
        //     169: aload_1
        //     170: invokeinterface 406 1 0
        //     175: ifle +448 -> 623
        //     178: iconst_0
        //     179: istore 13
        //     181: iload 13
        //     183: aload_1
        //     184: invokeinterface 406 1 0
        //     189: if_icmpge +374 -> 563
        //     192: aload_1
        //     193: iload 13
        //     195: invokeinterface 568 2 0
        //     200: checkcast 244	android/media/videoeditor/MediaItem
        //     203: astore 20
        //     205: aload 20
        //     207: instanceof 276
        //     210: ifeq +436 -> 646
        //     213: aload 20
        //     215: checkcast 276	android/media/videoeditor/MediaVideoItem
        //     218: invokevirtual 1155	android/media/videoeditor/MediaVideoItem:getBoundaryBeginTime	()J
        //     221: l2i
        //     222: istore 8
        //     224: aload 20
        //     226: checkcast 276	android/media/videoeditor/MediaVideoItem
        //     229: invokevirtual 1156	android/media/videoeditor/MediaVideoItem:getBoundaryEndTime	()J
        //     232: l2i
        //     233: istore 9
        //     235: aload 20
        //     237: invokevirtual 248	android/media/videoeditor/MediaItem:getBeginTransition	()Landroid/media/videoeditor/Transition;
        //     240: astore 21
        //     242: aload 21
        //     244: ifnull +50 -> 294
        //     247: aload 21
        //     249: invokevirtual 254	android/media/videoeditor/Transition:getDuration	()J
        //     252: lconst_0
        //     253: lcmp
        //     254: ifle +40 -> 294
        //     257: aload_0
        //     258: aload 21
        //     260: aload_0
        //     261: getfield 657	android/media/videoeditor/MediaArtistNativeHelper:mPreviewEditSettings	Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;
        //     264: aload_0
        //     265: getfield 193	android/media/videoeditor/MediaArtistNativeHelper:mClipProperties	Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;
        //     268: iload 5
        //     270: invokespecial 1158	android/media/videoeditor/MediaArtistNativeHelper:generateTransition	(Landroid/media/videoeditor/Transition;Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;I)V
        //     273: iload 6
        //     275: aload_0
        //     276: getfield 193	android/media/videoeditor/MediaArtistNativeHelper:mClipProperties	Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;
        //     279: getfield 284	android/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties:clipProperties	[Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;
        //     282: iload 5
        //     284: aaload
        //     285: getfield 266	android/media/videoeditor/MediaArtistNativeHelper$Properties:duration	I
        //     288: iadd
        //     289: istore 6
        //     291: iinc 5 1
        //     294: aload_0
        //     295: aload 20
        //     297: iload 5
        //     299: iload 7
        //     301: invokespecial 1160	android/media/videoeditor/MediaArtistNativeHelper:populateMediaItemProperties	(Landroid/media/videoeditor/MediaItem;II)I
        //     304: istore 7
        //     306: aload 20
        //     308: instanceof 293
        //     311: ifeq +526 -> 837
        //     314: iconst_0
        //     315: istore 24
        //     317: iconst_0
        //     318: istore 25
        //     320: aload 20
        //     322: invokevirtual 403	android/media/videoeditor/MediaItem:getAllEffects	()Ljava/util/List;
        //     325: astore 26
        //     327: aload 26
        //     329: invokeinterface 406 1 0
        //     334: istore 27
        //     336: iload 24
        //     338: iload 27
        //     340: if_icmpge +21 -> 361
        //     343: aload 26
        //     345: iload 24
        //     347: invokeinterface 568 2 0
        //     352: instanceof 413
        //     355: ifeq +316 -> 671
        //     358: iconst_1
        //     359: istore 25
        //     361: iload 25
        //     363: ifeq +394 -> 757
        //     366: aload 20
        //     368: checkcast 293	android/media/videoeditor/MediaImageItem
        //     371: invokevirtual 1074	android/media/videoeditor/MediaImageItem:getGeneratedImageClip	()Ljava/lang/String;
        //     374: ifnull +303 -> 677
        //     377: aload_0
        //     378: getfield 193	android/media/videoeditor/MediaArtistNativeHelper:mClipProperties	Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;
        //     381: getfield 284	android/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties:clipProperties	[Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;
        //     384: iload 5
        //     386: aload_0
        //     387: aload 20
        //     389: checkcast 293	android/media/videoeditor/MediaImageItem
        //     392: invokevirtual 1074	android/media/videoeditor/MediaImageItem:getGeneratedImageClip	()Ljava/lang/String;
        //     395: invokevirtual 364	android/media/videoeditor/MediaArtistNativeHelper:getMediaProperties	(Ljava/lang/String;)Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;
        //     398: aastore
        //     399: aload_0
        //     400: getfield 193	android/media/videoeditor/MediaArtistNativeHelper:mClipProperties	Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;
        //     403: getfield 284	android/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties:clipProperties	[Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;
        //     406: iload 5
        //     408: aaload
        //     409: aload 20
        //     411: invokevirtual 896	android/media/videoeditor/MediaItem:getId	()Ljava/lang/String;
        //     414: putfield 367	android/media/videoeditor/MediaArtistNativeHelper$Properties:Id	Ljava/lang/String;
        //     417: aload_0
        //     418: aload 20
        //     420: aload_0
        //     421: getfield 193	android/media/videoeditor/MediaArtistNativeHelper:mClipProperties	Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;
        //     424: iload 5
        //     426: invokespecial 1162	android/media/videoeditor/MediaArtistNativeHelper:checkOddSizeImage	(Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;I)V
        //     429: aload_0
        //     430: aload 20
        //     432: aload_0
        //     433: getfield 193	android/media/videoeditor/MediaArtistNativeHelper:mClipProperties	Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;
        //     436: iload 5
        //     438: invokespecial 1164	android/media/videoeditor/MediaArtistNativeHelper:adjustVolume	(Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;I)V
        //     441: aload_0
        //     442: aload_0
        //     443: getfield 657	android/media/videoeditor/MediaArtistNativeHelper:mPreviewEditSettings	Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;
        //     446: getfield 346	android/media/videoeditor/MediaArtistNativeHelper$EditSettings:clipSettingsArray	[Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;
        //     449: iload 5
        //     451: aaload
        //     452: aload_0
        //     453: getfield 193	android/media/videoeditor/MediaArtistNativeHelper:mClipProperties	Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;
        //     456: getfield 284	android/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties:clipProperties	[Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;
        //     459: iload 5
        //     461: aaload
        //     462: aload 20
        //     464: invokespecial 1166	android/media/videoeditor/MediaArtistNativeHelper:adjustMediaItemBoundary	(Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;Landroid/media/videoeditor/MediaItem;)V
        //     467: aload_0
        //     468: aload 20
        //     470: aload_0
        //     471: getfield 657	android/media/videoeditor/MediaArtistNativeHelper:mPreviewEditSettings	Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;
        //     474: getfield 891	android/media/videoeditor/MediaArtistNativeHelper$EditSettings:effectSettingsArray	[Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;
        //     477: iload 10
        //     479: iload 8
        //     481: iload 9
        //     483: iload 6
        //     485: invokespecial 1168	android/media/videoeditor/MediaArtistNativeHelper:populateEffects	(Landroid/media/videoeditor/MediaItem;[Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;IIII)I
        //     488: istore 10
        //     490: iload 6
        //     492: aload_0
        //     493: getfield 193	android/media/videoeditor/MediaArtistNativeHelper:mClipProperties	Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;
        //     496: getfield 284	android/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties:clipProperties	[Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;
        //     499: iload 5
        //     501: aaload
        //     502: getfield 266	android/media/videoeditor/MediaArtistNativeHelper$Properties:duration	I
        //     505: iadd
        //     506: istore 6
        //     508: iinc 5 1
        //     511: iload 13
        //     513: bipush 255
        //     515: aload_1
        //     516: invokeinterface 406 1 0
        //     521: iadd
        //     522: if_icmpne +350 -> 872
        //     525: aload 20
        //     527: invokevirtual 257	android/media/videoeditor/MediaItem:getEndTransition	()Landroid/media/videoeditor/Transition;
        //     530: astore 23
        //     532: aload 23
        //     534: ifnull +338 -> 872
        //     537: aload 23
        //     539: invokevirtual 254	android/media/videoeditor/Transition:getDuration	()J
        //     542: lconst_0
        //     543: lcmp
        //     544: ifle +328 -> 872
        //     547: aload_0
        //     548: aload 23
        //     550: aload_0
        //     551: getfield 657	android/media/videoeditor/MediaArtistNativeHelper:mPreviewEditSettings	Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;
        //     554: aload_0
        //     555: getfield 193	android/media/videoeditor/MediaArtistNativeHelper:mClipProperties	Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;
        //     558: iload 5
        //     560: invokespecial 1158	android/media/videoeditor/MediaArtistNativeHelper:generateTransition	(Landroid/media/videoeditor/Transition;Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;I)V
        //     563: aload_0
        //     564: getfield 211	android/media/videoeditor/MediaArtistNativeHelper:mErrorFlagSet	Z
        //     567: ifne +56 -> 623
        //     570: aload_0
        //     571: getfield 657	android/media/videoeditor/MediaArtistNativeHelper:mPreviewEditSettings	Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;
        //     574: aload_0
        //     575: aload_0
        //     576: getfield 215	android/media/videoeditor/MediaArtistNativeHelper:mVideoEditor	Landroid/media/videoeditor/VideoEditor;
        //     579: invokeinterface 314 1 0
        //     584: iload 7
        //     586: invokespecial 332	android/media/videoeditor/MediaArtistNativeHelper:findVideoResolution	(II)I
        //     589: putfield 809	android/media/videoeditor/MediaArtistNativeHelper$EditSettings:videoFrameSize	I
        //     592: aload_0
        //     593: aload_3
        //     594: invokespecial 1170	android/media/videoeditor/MediaArtistNativeHelper:populateBackgroundMusicProperties	(Ljava/util/List;)V
        //     597: aload_0
        //     598: aload_0
        //     599: getfield 657	android/media/videoeditor/MediaArtistNativeHelper:mPreviewEditSettings	Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;
        //     602: aload_0
        //     603: getfield 193	android/media/videoeditor/MediaArtistNativeHelper:mClipProperties	Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;
        //     606: aload_0
        //     607: getfield 195	android/media/videoeditor/MediaArtistNativeHelper:mAudioSettings	Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;
        //     610: invokespecial 755	android/media/videoeditor/MediaArtistNativeHelper:nativePopulateSettings	(Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;)V
        //     613: aload_0
        //     614: iconst_0
        //     615: putfield 199	android/media/videoeditor/MediaArtistNativeHelper:mInvalidatePreviewArray	Z
        //     618: aload_0
        //     619: iconst_0
        //     620: putfield 524	android/media/videoeditor/MediaArtistNativeHelper:mProcessingState	I
        //     623: aload_0
        //     624: getfield 211	android/media/videoeditor/MediaArtistNativeHelper:mErrorFlagSet	Z
        //     627: ifeq +293 -> 920
        //     630: aload_0
        //     631: iconst_0
        //     632: putfield 211	android/media/videoeditor/MediaArtistNativeHelper:mErrorFlagSet	Z
        //     635: new 238	java/lang/RuntimeException
        //     638: dup
        //     639: ldc_w 905
        //     642: invokespecial 780	java/lang/RuntimeException:<init>	(Ljava/lang/String;)V
        //     645: athrow
        //     646: aload 20
        //     648: instanceof 293
        //     651: ifeq -416 -> 235
        //     654: iconst_0
        //     655: istore 8
        //     657: aload 20
        //     659: checkcast 293	android/media/videoeditor/MediaImageItem
        //     662: invokevirtual 1173	android/media/videoeditor/MediaImageItem:getTimelineDuration	()J
        //     665: l2i
        //     666: istore 9
        //     668: goto -433 -> 235
        //     671: iinc 24 1
        //     674: goto -347 -> 327
        //     677: aload_0
        //     678: getfield 193	android/media/videoeditor/MediaArtistNativeHelper:mClipProperties	Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;
        //     681: getfield 284	android/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties:clipProperties	[Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;
        //     684: iload 5
        //     686: aload_0
        //     687: aload 20
        //     689: checkcast 293	android/media/videoeditor/MediaImageItem
        //     692: invokevirtual 1176	android/media/videoeditor/MediaImageItem:getScaledImageFileName	()Ljava/lang/String;
        //     695: invokevirtual 364	android/media/videoeditor/MediaArtistNativeHelper:getMediaProperties	(Ljava/lang/String;)Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;
        //     698: aastore
        //     699: aload_0
        //     700: getfield 193	android/media/videoeditor/MediaArtistNativeHelper:mClipProperties	Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;
        //     703: getfield 284	android/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties:clipProperties	[Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;
        //     706: iload 5
        //     708: aaload
        //     709: aload 20
        //     711: checkcast 293	android/media/videoeditor/MediaImageItem
        //     714: invokevirtual 1179	android/media/videoeditor/MediaImageItem:getScaledWidth	()I
        //     717: putfield 297	android/media/videoeditor/MediaArtistNativeHelper$Properties:width	I
        //     720: aload_0
        //     721: getfield 193	android/media/videoeditor/MediaArtistNativeHelper:mClipProperties	Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;
        //     724: getfield 284	android/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties:clipProperties	[Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;
        //     727: iload 5
        //     729: aaload
        //     730: aload 20
        //     732: checkcast 293	android/media/videoeditor/MediaImageItem
        //     735: invokevirtual 421	android/media/videoeditor/MediaImageItem:getScaledHeight	()I
        //     738: putfield 300	android/media/videoeditor/MediaArtistNativeHelper$Properties:height	I
        //     741: goto -342 -> 399
        //     744: astore 29
        //     746: new 230	java/lang/IllegalArgumentException
        //     749: dup
        //     750: ldc_w 369
        //     753: invokespecial 234	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     756: athrow
        //     757: aload_0
        //     758: getfield 193	android/media/videoeditor/MediaArtistNativeHelper:mClipProperties	Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;
        //     761: getfield 284	android/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties:clipProperties	[Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;
        //     764: iload 5
        //     766: aload_0
        //     767: aload 20
        //     769: checkcast 293	android/media/videoeditor/MediaImageItem
        //     772: invokevirtual 1176	android/media/videoeditor/MediaImageItem:getScaledImageFileName	()Ljava/lang/String;
        //     775: invokevirtual 364	android/media/videoeditor/MediaArtistNativeHelper:getMediaProperties	(Ljava/lang/String;)Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;
        //     778: aastore
        //     779: aload_0
        //     780: getfield 193	android/media/videoeditor/MediaArtistNativeHelper:mClipProperties	Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;
        //     783: getfield 284	android/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties:clipProperties	[Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;
        //     786: iload 5
        //     788: aaload
        //     789: aload 20
        //     791: checkcast 293	android/media/videoeditor/MediaImageItem
        //     794: invokevirtual 1179	android/media/videoeditor/MediaImageItem:getScaledWidth	()I
        //     797: putfield 297	android/media/videoeditor/MediaArtistNativeHelper$Properties:width	I
        //     800: aload_0
        //     801: getfield 193	android/media/videoeditor/MediaArtistNativeHelper:mClipProperties	Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;
        //     804: getfield 284	android/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties:clipProperties	[Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;
        //     807: iload 5
        //     809: aaload
        //     810: aload 20
        //     812: checkcast 293	android/media/videoeditor/MediaImageItem
        //     815: invokevirtual 421	android/media/videoeditor/MediaImageItem:getScaledHeight	()I
        //     818: putfield 300	android/media/videoeditor/MediaArtistNativeHelper$Properties:height	I
        //     821: goto -422 -> 399
        //     824: astore 28
        //     826: new 230	java/lang/IllegalArgumentException
        //     829: dup
        //     830: ldc_w 369
        //     833: invokespecial 234	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     836: athrow
        //     837: aload_0
        //     838: getfield 193	android/media/videoeditor/MediaArtistNativeHelper:mClipProperties	Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;
        //     841: getfield 284	android/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties:clipProperties	[Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;
        //     844: iload 5
        //     846: aload_0
        //     847: aload 20
        //     849: invokevirtual 1180	android/media/videoeditor/MediaItem:getFilename	()Ljava/lang/String;
        //     852: invokevirtual 364	android/media/videoeditor/MediaArtistNativeHelper:getMediaProperties	(Ljava/lang/String;)Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;
        //     855: aastore
        //     856: goto -457 -> 399
        //     859: astore 22
        //     861: new 230	java/lang/IllegalArgumentException
        //     864: dup
        //     865: ldc_w 369
        //     868: invokespecial 234	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     871: athrow
        //     872: iinc 13 1
        //     875: goto -694 -> 181
        //     878: astore 18
        //     880: ldc 124
        //     882: ldc_w 1182
        //     885: invokestatic 553	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     888: pop
        //     889: aload 18
        //     891: athrow
        //     892: astore 16
        //     894: ldc 124
        //     896: ldc_w 1184
        //     899: invokestatic 553	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     902: pop
        //     903: aload 16
        //     905: athrow
        //     906: astore 14
        //     908: ldc 124
        //     910: ldc_w 1186
        //     913: invokestatic 553	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     916: pop
        //     917: aload 14
        //     919: athrow
        //     920: return
        //
        // Exception table:
        //     from	to	target	type
        //     366	399	744	java/lang/Exception
        //     677	741	744	java/lang/Exception
        //     757	779	824	java/lang/Exception
        //     837	856	859	java/lang/Exception
        //     597	613	878	java/lang/IllegalArgumentException
        //     597	613	892	java/lang/IllegalStateException
        //     597	613	906	java/lang/RuntimeException
    }

    void releaseNativeHelper()
        throws InterruptedException
    {
        release();
    }

    long renderMediaItemPreviewFrame(Surface paramSurface, String paramString, long paramLong, int paramInt1, int paramInt2)
    {
        try
        {
            int i = nativeRenderMediaItemPreviewFrame(paramSurface, paramString, paramInt1, paramInt2, 0, 0, paramLong);
            return i;
        }
        catch (IllegalArgumentException localIllegalArgumentException)
        {
            Log.e("MediaArtistNativeHelper", "Illegal Argument exception in renderMediaItemPreviewFrame");
            throw localIllegalArgumentException;
        }
        catch (IllegalStateException localIllegalStateException)
        {
            Log.e("MediaArtistNativeHelper", "Illegal state exception in renderMediaItemPreviewFrame");
            throw localIllegalStateException;
        }
        catch (RuntimeException localRuntimeException)
        {
            Log.e("MediaArtistNativeHelper", "Runtime exception in renderMediaItemPreviewFrame");
            throw localRuntimeException;
        }
    }

    long renderPreviewFrame(Surface paramSurface, long paramLong, int paramInt1, int paramInt2, VideoEditor.OverlayData paramOverlayData)
    {
        if (this.mInvalidatePreviewArray)
        {
            if (Log.isLoggable("MediaArtistNativeHelper", 3))
                Log.d("MediaArtistNativeHelper", "Call generate preview first");
            throw new IllegalStateException("Call generate preview first");
        }
        long l;
        for (int i = 0; ; i++)
            try
            {
                if (i < this.mPreviewEditSettings.clipSettingsArray.length)
                {
                    if (this.mPreviewEditSettings.clipSettingsArray[i].fileType == 5)
                        this.mPreviewEditSettings.clipSettingsArray[i].clipPath = this.mPreviewEditSettings.clipSettingsArray[i].clipDecodedPath;
                }
                else
                {
                    this.mRenderPreviewOverlayFile = null;
                    this.mRenderPreviewRenderingMode = 0;
                    nativePopulateSettings(this.mPreviewEditSettings, this.mClipProperties, this.mAudioSettings);
                    l = nativeRenderPreviewFrame(paramSurface, paramLong, paramInt1, paramInt2);
                    if (this.mRenderPreviewOverlayFile != null)
                        paramOverlayData.set(BitmapFactory.decodeFile(this.mRenderPreviewOverlayFile), this.mRenderPreviewRenderingMode);
                    else
                        paramOverlayData.setClear();
                }
            }
            catch (IllegalArgumentException localIllegalArgumentException)
            {
                Log.e("MediaArtistNativeHelper", "Illegal Argument exception in nativeRenderPreviewFrame");
                throw localIllegalArgumentException;
            }
            catch (IllegalStateException localIllegalStateException)
            {
                Log.e("MediaArtistNativeHelper", "Illegal state exception in nativeRenderPreviewFrame");
                throw localIllegalStateException;
            }
            catch (RuntimeException localRuntimeException)
            {
                Log.e("MediaArtistNativeHelper", "Runtime exception in nativeRenderPreviewFrame");
                throw localRuntimeException;
            }
        return l;
    }

    void setAudioCodec(int paramInt)
    {
        this.mExportAudioCodec = paramInt;
    }

    void setAudioflag(boolean paramBoolean)
    {
        if (!new File(String.format(this.mProjectPath + "/" + "AudioPcm.pcm", new Object[0])).exists())
            paramBoolean = true;
        this.mRegenerateAudio = paramBoolean;
    }

    void setGeneratePreview(boolean paramBoolean)
    {
        int i = 0;
        try
        {
            lock();
            i = 1;
            this.mInvalidatePreviewArray = paramBoolean;
            return;
        }
        catch (InterruptedException localInterruptedException)
        {
            while (true)
            {
                Log.e("MediaArtistNativeHelper", "Runtime exception in renderMediaItemPreviewFrame");
                if (i != 0)
                    unlock();
            }
        }
        finally
        {
            if (i != 0)
                unlock();
        }
    }

    void setVideoCodec(int paramInt)
    {
        this.mExportVideoCodec = paramInt;
    }

    void stop(String paramString)
    {
        try
        {
            stopEncoding();
            new File(this.mExportFilename).delete();
            return;
        }
        catch (IllegalStateException localIllegalStateException)
        {
            Log.e("MediaArtistNativeHelper", "Illegal state exception in unload settings");
            throw localIllegalStateException;
        }
        catch (RuntimeException localRuntimeException)
        {
            Log.e("MediaArtistNativeHelper", "Runtime exception in unload settings");
            throw localRuntimeException;
        }
    }

    long stopPreview()
    {
        return nativeStopPreview();
    }

    static abstract interface NativeGetPixelsListCallback
    {
        public abstract void onThumbnail(int paramInt);
    }

    public static class Properties
    {
        public String Id;
        public int audioBitrate;
        public int audioChannels;
        public int audioDuration;
        public int audioFormat;
        public int audioSamplingFrequency;
        public int audioVolumeValue;
        public float averageFrameRate;
        public int duration;
        public int fileType;
        public int height;
        public int level;
        public boolean levelSupported;
        public int profile;
        public boolean profileSupported;
        public int videoBitrate;
        public int videoDuration;
        public int videoFormat;
        public int videoRotation;
        public int width;
    }

    public static class EditSettings
    {
        public int audioBitrate;
        public int audioChannels;
        public int audioFormat;
        public int audioSamplingFreq;
        public MediaArtistNativeHelper.BackgroundMusicSettings backgroundMusicSettings;
        public MediaArtistNativeHelper.ClipSettings[] clipSettingsArray;
        public MediaArtistNativeHelper.EffectSettings[] effectSettingsArray;
        public int maxFileSize;
        public String outputFile;
        public int primaryTrackVolume;
        public MediaArtistNativeHelper.TransitionSettings[] transitionSettingsArray;
        public int videoBitrate;
        public int videoFormat;
        public int videoFrameRate;
        public int videoFrameSize;
        public int videoLevel;
        public int videoProfile;
    }

    public static class PreviewClipProperties
    {
        public MediaArtistNativeHelper.Properties[] clipProperties;
    }

    public static class PreviewSettings
    {
        public MediaArtistNativeHelper.EffectSettings[] effectSettingsArray;
        public MediaArtistNativeHelper.PreviewClips[] previewClipsArray;
    }

    public static class AudioSettings
    {
        int ExtendedFs;
        int Fs;
        String Id;
        boolean bInDucking_enable;
        boolean bRemoveOriginal;
        long beginCutTime;
        int channels;
        int ducking_lowVolume;
        int ducking_threshold;
        long endCutTime;
        int fileType;
        boolean loop;
        String pFile;
        String pcmFilePath;
        long startMs;
        int volume;
    }

    public static class PreviewClips
    {
        public long beginPlayTime;
        public String clipPath;
        public long endPlayTime;
        public int fileType;
        public int mediaRendering;
    }

    public static class EffectSettings
    {
        public int alphaBlendingEndPercent;
        public int alphaBlendingFadeInTimePercent;
        public int alphaBlendingFadeOutTimePercent;
        public int alphaBlendingMiddlePercent;
        public int alphaBlendingStartPercent;
        public int audioEffectType;
        public int bitmapType;
        public int duration;
        public int durationPercent;
        public int fiftiesFrameRate;
        public int[] framingBuffer;
        public String framingFile;
        public boolean framingResize;
        public int framingScaledSize;
        public int height;
        public int rgb16InputColor;
        public int startPercent;
        public int startTime;
        public String text;
        public int textBufferHeight;
        public int textBufferWidth;
        public String textRenderingData;
        public int topLeftX;
        public int topLeftY;
        public int videoEffectType;
        public int width;
    }

    public static class AudioEffect
    {
        public static final int FADE_IN = 8;
        public static final int FADE_OUT = 16;
        public static final int NONE;
    }

    public static class BackgroundMusicSettings
    {
        public long beginLoop;
        public int duckingThreshold;
        public boolean enableDucking;
        public long endLoop;
        public String file;
        public int fileType;
        public long insertionTime;
        public boolean isLooping;
        public int lowVolume;
        public int volumePercent;
    }

    public static final class TransitionBehaviour
    {
        public static final int FAST_MIDDLE = 4;
        public static final int LINEAR = 1;
        public static final int SLOW_MIDDLE = 3;
        public static final int SPEED_DOWN = 2;
        public static final int SPEED_UP;
    }

    public static final class AudioTransition
    {
        public static final int CROSS_FADE = 1;
        public static final int NONE;
    }

    public static class TransitionSettings
    {
        public MediaArtistNativeHelper.AlphaMagicSettings alphaSettings;
        public int audioTransitionType;
        public int duration;
        public MediaArtistNativeHelper.SlideTransitionSettings slideSettings;
        public int transitionBehaviour;
        public int videoTransitionType;
    }

    public static class ClipSettings
    {
        public int beginCutPercent;
        public int beginCutTime;
        public String clipDecodedPath;
        public String clipOriginalPath;
        public String clipPath;
        public int endCutPercent;
        public int endCutTime;
        public int fileType;
        public int mediaRendering;
        public boolean panZoomEnabled;
        public int panZoomPercentEnd;
        public int panZoomPercentStart;
        public int panZoomTopLeftXEnd;
        public int panZoomTopLeftXStart;
        public int panZoomTopLeftYEnd;
        public int panZoomTopLeftYStart;
        public int rgbHeight;
        public int rgbWidth;
        public int rotationDegree;
    }

    public static class SlideTransitionSettings
    {
        public int direction;
    }

    public static final class SlideDirection
    {
        public static final int BOTTOM_OUT_TOP_IN = 3;
        public static final int LEFT_OUT_RIGTH_IN = 1;
        public static final int RIGHT_OUT_LEFT_IN = 0;
        public static final int TOP_OUT_BOTTOM_IN = 2;
    }

    public static class AlphaMagicSettings
    {
        public int blendingPercent;
        public String file;
        public boolean invertRotation;
        public int rgbHeight;
        public int rgbWidth;
    }

    public static class VideoTransition
    {
        public static final int ALPHA_MAGIC = 257;
        public static final int CROSS_FADE = 1;
        public static final int EXTERNAL = 256;
        public static final int FADE_BLACK = 259;
        public static final int NONE = 0;
        public static final int SLIDE_TRANSITION = 258;
    }

    public static class VideoEffect
    {
        public static final int BLACK_AND_WHITE = 257;
        public static final int COLORRGB16 = 267;
        public static final int EXTERNAL = 256;
        public static final int FADE_FROM_BLACK = 8;
        public static final int FADE_TO_BLACK = 16;
        public static final int FIFTIES = 266;
        public static final int FRAMING = 262;
        public static final int GRADIENT = 268;
        public static final int GREEN = 259;
        public static final int NEGATIVE = 261;
        public static final int NONE = 0;
        public static final int PINK = 258;
        public static final int SEPIA = 260;
        public static final int TEXT = 263;
        public static final int ZOOM_IN = 264;
        public static final int ZOOM_OUT = 265;
    }

    public final class VideoFrameRate
    {
        public static final int FR_10_FPS = 2;
        public static final int FR_12_5_FPS = 3;
        public static final int FR_15_FPS = 4;
        public static final int FR_20_FPS = 5;
        public static final int FR_25_FPS = 6;
        public static final int FR_30_FPS = 7;
        public static final int FR_5_FPS = 0;
        public static final int FR_7_5_FPS = 1;

        public VideoFrameRate()
        {
        }
    }

    public final class VideoFrameSize
    {
        public static final int CIF = 4;
        public static final int NTSC = 7;
        public static final int QCIF = 2;
        public static final int QQVGA = 1;
        public static final int QVGA = 3;
        public static final int S720p = 12;
        public static final int SIZE_UNDEFINED = -1;
        public static final int SQCIF = 0;
        public static final int V1080p = 13;
        public static final int V720p = 10;
        public static final int VGA = 5;
        public static final int W720p = 11;
        public static final int WVGA = 6;
        public static final int WVGA16x9 = 9;
        public static final int nHD = 8;

        public VideoFrameSize()
        {
        }
    }

    public final class VideoFormat
    {
        public static final int H263 = 1;
        public static final int H264 = 2;
        public static final int MPEG4 = 3;
        public static final int NO_VIDEO = 0;
        public static final int NULL_VIDEO = 254;
        public static final int UNSUPPORTED = 255;

        public VideoFormat()
        {
        }
    }

    public final class Result
    {
        public static final int ERR_ADDCTS_HIGHER_THAN_VIDEO_DURATION = 40;
        public static final int ERR_ADDVOLUME_EQUALS_ZERO = 39;
        public static final int ERR_ALLOC = 62;
        public static final int ERR_AMR_EDITING_UNSUPPORTED = 19;
        public static final int ERR_ANALYSIS_DATA_SIZE_TOO_SMALL = 15;
        public static final int ERR_AUDIOBITRATE_TOO_HIGH = 121;
        public static final int ERR_AUDIOBITRATE_TOO_LOW = 119;
        public static final int ERR_AUDIO_CANNOT_BE_MIXED = 47;
        public static final int ERR_AUDIO_CONVERSION_FAILED = 114;
        public static final int ERR_AUDIO_MIXING_MP3_UNSUPPORTED = 44;
        public static final int ERR_AUDIO_MIXING_UNSUPPORTED = 43;
        public static final int ERR_BAD_CONTEXT = 63;
        public static final int ERR_BAD_OPTION_ID = 66;
        public static final int ERR_BAD_STREAM_ID = 65;
        public static final int ERR_BEGIN_CUT_EQUALS_END_CUT = 115;
        public static final int ERR_BEGIN_CUT_LARGER_THAN_DURATION = 12;
        public static final int ERR_BEGIN_CUT_LARGER_THAN_END_CUT = 13;
        public static final int ERR_BUFFER_OUT_TOO_SMALL = 2;
        public static final int ERR_CLOCK_BAD_REF_YEAR = 56;
        public static final int ERR_CONTEXT_FAILED = 64;
        public static final int ERR_DECODER_H263_NOT_BASELINE = 135;
        public static final int ERR_DECODER_H263_PROFILE_NOT_SUPPORTED = 134;
        public static final int ERR_DIR_NO_MORE_ENTRY = 59;
        public static final int ERR_DIR_OPEN_FAILED = 57;
        public static final int ERR_DIR_READ_FAILED = 58;
        public static final int ERR_DURATION_IS_NULL = 111;
        public static final int ERR_EDITING_NO_SUPPORTED_STREAM_IN_FILE = 29;
        public static final int ERR_EDITING_NO_SUPPORTED_VIDEO_STREAM_IN_FILE = 30;
        public static final int ERR_EDITING_UNSUPPORTED_AUDIO_FORMAT = 28;
        public static final int ERR_EDITING_UNSUPPORTED_H263_PROFILE = 25;
        public static final int ERR_EDITING_UNSUPPORTED_MPEG4_PROFILE = 26;
        public static final int ERR_EDITING_UNSUPPORTED_MPEG4_RVLC = 27;
        public static final int ERR_EDITING_UNSUPPORTED_VIDEO_FORMAT = 24;
        public static final int ERR_ENCODER_ACCES_UNIT_ERROR = 23;
        public static final int ERR_END_CUT_SMALLER_THAN_BEGIN_CUT = 116;
        public static final int ERR_EXTERNAL_EFFECT_NULL = 10;
        public static final int ERR_EXTERNAL_TRANSITION_NULL = 11;
        public static final int ERR_FEATURE_UNSUPPORTED_WITH_AAC = 46;
        public static final int ERR_FEATURE_UNSUPPORTED_WITH_AUDIO_TRACK = 45;
        public static final int ERR_FEATURE_UNSUPPORTED_WITH_EVRC = 49;
        public static final int ERR_FILE_BAD_MODE_ACCESS = 80;
        public static final int ERR_FILE_INVALID_POSITION = 81;
        public static final int ERR_FILE_LOCKED = 79;
        public static final int ERR_FILE_NOT_FOUND = 1;
        public static final int ERR_H263_FORBIDDEN_IN_MP4_FILE = 112;
        public static final int ERR_H263_PROFILE_NOT_SUPPORTED = 51;
        public static final int ERR_INCOMPATIBLE_VIDEO_DATA_PARTITIONING = 36;
        public static final int ERR_INCOMPATIBLE_VIDEO_FORMAT = 33;
        public static final int ERR_INCOMPATIBLE_VIDEO_FRAME_SIZE = 34;
        public static final int ERR_INCOMPATIBLE_VIDEO_TIME_SCALE = 35;
        public static final int ERR_INPUT_AUDIO_AU_TOO_LARGE = 21;
        public static final int ERR_INPUT_AUDIO_CORRUPTED_AU = 22;
        public static final int ERR_INPUT_FILE_CONTAINS_NO_SUPPORTED_STREAM = 103;
        public static final int ERR_INPUT_VIDEO_AU_TOO_LARGE = 20;
        public static final int ERR_INTERNAL = 255;
        public static final int ERR_INVALID_3GPP_FILE = 16;
        public static final int ERR_INVALID_AAC_SAMPLING_FREQUENCY = 113;
        public static final int ERR_INVALID_AUDIO_EFFECT_TYPE = 6;
        public static final int ERR_INVALID_AUDIO_TRANSITION_TYPE = 8;
        public static final int ERR_INVALID_CLIP_ANALYSIS_PLATFORM = 32;
        public static final int ERR_INVALID_CLIP_ANALYSIS_VERSION = 31;
        public static final int ERR_INVALID_EFFECT_KIND = 4;
        public static final int ERR_INVALID_FILE_TYPE = 3;
        public static final int ERR_INVALID_INPUT_FILE = 104;
        public static final int ERR_INVALID_VIDEO_EFFECT_TYPE = 5;
        public static final int ERR_INVALID_VIDEO_ENCODING_FRAME_RATE = 9;
        public static final int ERR_INVALID_VIDEO_FRAME_RATE_FOR_H263 = 110;
        public static final int ERR_INVALID_VIDEO_FRAME_SIZE_FOR_H263 = 109;
        public static final int ERR_INVALID_VIDEO_TRANSITION_TYPE = 7;
        public static final int ERR_MAXFILESIZE_TOO_SMALL = 117;
        public static final int ERR_NOMORE_SPACE_FOR_FILE = 136;
        public static final int ERR_NOT_IMPLEMENTED = 69;
        public static final int ERR_NO_SUPPORTED_STREAM_IN_FILE = 38;
        public static final int ERR_NO_SUPPORTED_VIDEO_STREAM_IN_FILE = 52;
        public static final int ERR_ONLY_AMRNB_INPUT_CAN_BE_MIXED = 48;
        public static final int ERR_OUTPUT_FILE_SIZE_TOO_SMALL = 122;
        public static final int ERR_OVERLAPPING_TRANSITIONS = 14;
        public static final int ERR_PARAMETER = 60;
        public static final int ERR_READER_UNKNOWN_STREAM_TYPE = 123;
        public static final int ERR_READ_ONLY = 68;
        public static final int ERR_STATE = 61;
        public static final int ERR_STR_BAD_ARGS = 97;
        public static final int ERR_STR_BAD_STRING = 94;
        public static final int ERR_STR_CONV_FAILED = 95;
        public static final int ERR_STR_OVERFLOW = 96;
        public static final int ERR_THREAD_NOT_STARTED = 100;
        public static final int ERR_UNDEFINED_AUDIO_TRACK_FILE_FORMAT = 41;
        public static final int ERR_UNDEFINED_OUTPUT_AUDIO_FORMAT = 108;
        public static final int ERR_UNDEFINED_OUTPUT_VIDEO_FORMAT = 105;
        public static final int ERR_UNDEFINED_OUTPUT_VIDEO_FRAME_RATE = 107;
        public static final int ERR_UNDEFINED_OUTPUT_VIDEO_FRAME_SIZE = 106;
        public static final int ERR_UNSUPPORTED_ADDED_AUDIO_STREAM = 42;
        public static final int ERR_UNSUPPORTED_INPUT_AUDIO_FORMAT = 18;
        public static final int ERR_UNSUPPORTED_INPUT_VIDEO_FORMAT = 17;
        public static final int ERR_UNSUPPORTED_MEDIA_TYPE = 70;
        public static final int ERR_UNSUPPORTED_MP3_ASSEMBLY = 37;
        public static final int ERR_VIDEOBITRATE_TOO_HIGH = 120;
        public static final int ERR_VIDEOBITRATE_TOO_LOW = 118;
        public static final int ERR_WRITE_ONLY = 67;
        public static final int NO_ERROR = 0;
        public static final int WAR_BUFFER_FULL = 76;
        public static final int WAR_DEBLOCKING_FILTER_NOT_IMPLEMENTED = 133;
        public static final int WAR_INVALID_TIME = 73;
        public static final int WAR_MAX_OUTPUT_SIZE_EXCEEDED = 54;
        public static final int WAR_MEDIATYPE_NOT_SUPPORTED = 102;
        public static final int WAR_NO_DATA_YET = 71;
        public static final int WAR_NO_MORE_AU = 74;
        public static final int WAR_NO_MORE_STREAM = 72;
        public static final int WAR_READER_INFORMATION_NOT_PRESENT = 125;
        public static final int WAR_READER_NO_METADATA = 124;
        public static final int WAR_REDIRECT = 77;
        public static final int WAR_STR_NOT_FOUND = 99;
        public static final int WAR_STR_OVERFLOW = 98;
        public static final int WAR_TIMESCALE_TOO_BIG = 55;
        public static final int WAR_TIME_OUT = 75;
        public static final int WAR_TOO_MUCH_STREAMS = 78;
        public static final int WAR_TRANSCODING_DONE = 101;
        public static final int WAR_TRANSCODING_NECESSARY = 53;
        public static final int WAR_VIDEORENDERER_NO_NEW_FRAME = 132;
        public static final int WAR_WRITER_STOP_REQ = 131;

        public Result()
        {
        }
    }

    public final class MediaRendering
    {
        public static final int BLACK_BORDERS = 2;
        public static final int CROPPING = 1;
        public static final int RESIZING;

        public MediaRendering()
        {
        }
    }

    public final class FileType
    {
        public static final int AMR = 2;
        public static final int GIF = 7;
        public static final int JPG = 5;
        public static final int M4V = 10;
        public static final int MP3 = 3;
        public static final int MP4 = 1;
        public static final int PCM = 4;
        public static final int PNG = 8;
        public static final int THREE_GPP = 0;
        public static final int UNSUPPORTED = 255;

        public FileType()
        {
        }
    }

    public final class Bitrate
    {
        public static final int BR_128_KBPS = 128000;
        public static final int BR_12_2_KBPS = 12200;
        public static final int BR_16_KBPS = 16000;
        public static final int BR_192_KBPS = 192000;
        public static final int BR_24_KBPS = 24000;
        public static final int BR_256_KBPS = 256000;
        public static final int BR_288_KBPS = 288000;
        public static final int BR_2_MBPS = 2000000;
        public static final int BR_32_KBPS = 32000;
        public static final int BR_384_KBPS = 384000;
        public static final int BR_48_KBPS = 48000;
        public static final int BR_512_KBPS = 512000;
        public static final int BR_5_MBPS = 5000000;
        public static final int BR_64_KBPS = 64000;
        public static final int BR_800_KBPS = 800000;
        public static final int BR_8_MBPS = 8000000;
        public static final int BR_96_KBPS = 96000;
        public static final int BR_9_2_KBPS = 9200;
        public static final int UNDEFINED = 0;
        public static final int VARIABLE = -1;

        public Bitrate()
        {
        }
    }

    public final class AudioSamplingFrequency
    {
        public static final int FREQ_11025 = 11025;
        public static final int FREQ_12000 = 12000;
        public static final int FREQ_16000 = 16000;
        public static final int FREQ_22050 = 22050;
        public static final int FREQ_24000 = 24000;
        public static final int FREQ_32000 = 32000;
        public static final int FREQ_44100 = 44100;
        public static final int FREQ_48000 = 48000;
        public static final int FREQ_8000 = 8000;
        public static final int FREQ_DEFAULT;

        public AudioSamplingFrequency()
        {
        }
    }

    public final class AudioFormat
    {
        public static final int AAC = 2;
        public static final int AAC_PLUS = 3;
        public static final int AMR_NB = 1;
        public static final int ENHANCED_AAC_PLUS = 4;
        public static final int EVRC = 6;
        public static final int MP3 = 5;
        public static final int NO_AUDIO = 0;
        public static final int NULL_AUDIO = 254;
        public static final int PCM = 7;
        public static final int UNSUPPORTED_AUDIO = 255;

        public AudioFormat()
        {
        }
    }

    public final class Version
    {
        private static final int VIDEOEDITOR_MAJOR_VERSION = 0;
        private static final int VIDEOEDITOR_MINOR_VERSION = 0;
        private static final int VIDEOEDITOR_REVISION_VERSION = 1;
        public int major;
        public int minor;
        public int revision;

        public Version()
        {
        }

        public Version getVersion()
        {
            Version localVersion = new Version(MediaArtistNativeHelper.this);
            localVersion.major = 0;
            localVersion.minor = 0;
            localVersion.revision = 1;
            return localVersion;
        }
    }

    public static abstract interface OnProgressUpdateListener
    {
        public abstract void OnProgressUpdate(int paramInt1, int paramInt2);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.media.videoeditor.MediaArtistNativeHelper
 * JD-Core Version:        0.6.2
 */