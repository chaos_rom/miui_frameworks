package android.media.videoeditor;

public class TransitionFadeBlack extends Transition
{
    private TransitionFadeBlack()
    {
        this(null, null, null, 0L, 0);
    }

    public TransitionFadeBlack(String paramString, MediaItem paramMediaItem1, MediaItem paramMediaItem2, long paramLong, int paramInt)
    {
        super(paramString, paramMediaItem1, paramMediaItem2, paramLong, paramInt);
    }

    void generate()
    {
        super.generate();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.media.videoeditor.TransitionFadeBlack
 * JD-Core Version:        0.6.2
 */