package android.media.videoeditor;

public abstract class Effect
{
    protected long mDurationMs;
    private final MediaItem mMediaItem;
    protected long mStartTimeMs;
    private final String mUniqueId;

    private Effect()
    {
        this.mMediaItem = null;
        this.mUniqueId = null;
        this.mStartTimeMs = 0L;
        this.mDurationMs = 0L;
    }

    public Effect(MediaItem paramMediaItem, String paramString, long paramLong1, long paramLong2)
    {
        if (paramMediaItem == null)
            throw new IllegalArgumentException("Media item cannot be null");
        if ((paramLong1 < 0L) || (paramLong2 < 0L))
            throw new IllegalArgumentException("Invalid start time Or/And Duration");
        if (paramLong1 + paramLong2 > paramMediaItem.getDuration())
            throw new IllegalArgumentException("Invalid start time and duration");
        this.mMediaItem = paramMediaItem;
        this.mUniqueId = paramString;
        this.mStartTimeMs = paramLong1;
        this.mDurationMs = paramLong2;
    }

    public boolean equals(Object paramObject)
    {
        if (!(paramObject instanceof Effect));
        for (boolean bool = false; ; bool = this.mUniqueId.equals(((Effect)paramObject).mUniqueId))
            return bool;
    }

    public long getDuration()
    {
        return this.mDurationMs;
    }

    public String getId()
    {
        return this.mUniqueId;
    }

    public MediaItem getMediaItem()
    {
        return this.mMediaItem;
    }

    public long getStartTime()
    {
        return this.mStartTimeMs;
    }

    public int hashCode()
    {
        return this.mUniqueId.hashCode();
    }

    public void setDuration(long paramLong)
    {
        if (paramLong < 0L)
            throw new IllegalArgumentException("Invalid duration");
        if (paramLong + this.mStartTimeMs > this.mMediaItem.getDuration())
            throw new IllegalArgumentException("Duration is too large");
        getMediaItem().getNativeContext().setGeneratePreview(true);
        long l = this.mDurationMs;
        this.mDurationMs = paramLong;
        this.mMediaItem.invalidateTransitions(this.mStartTimeMs, l, this.mStartTimeMs, this.mDurationMs);
    }

    public void setStartTime(long paramLong)
    {
        if (paramLong + this.mDurationMs > this.mMediaItem.getDuration())
            throw new IllegalArgumentException("Start time is too large");
        getMediaItem().getNativeContext().setGeneratePreview(true);
        long l = this.mStartTimeMs;
        this.mStartTimeMs = paramLong;
        this.mMediaItem.invalidateTransitions(l, this.mDurationMs, this.mStartTimeMs, this.mDurationMs);
    }

    public void setStartTimeAndDuration(long paramLong1, long paramLong2)
    {
        if (paramLong1 + paramLong2 > this.mMediaItem.getDuration())
            throw new IllegalArgumentException("Invalid start time or duration");
        getMediaItem().getNativeContext().setGeneratePreview(true);
        long l1 = this.mStartTimeMs;
        long l2 = this.mDurationMs;
        this.mStartTimeMs = paramLong1;
        this.mDurationMs = paramLong2;
        this.mMediaItem.invalidateTransitions(l1, l2, this.mStartTimeMs, this.mDurationMs);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.media.videoeditor.Effect
 * JD-Core Version:        0.6.2
 */