package android.filterpacks.text;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterContext;
import android.filterfw.core.Frame;
import android.filterfw.core.FrameFormat;
import android.filterfw.core.FrameManager;
import android.filterfw.format.ObjectFormat;

public class ToUpperCase extends Filter
{
    private FrameFormat mOutputFormat;

    public ToUpperCase(String paramString)
    {
        super(paramString);
    }

    public void process(FilterContext paramFilterContext)
    {
        String str = (String)pullInput("mixedcase").getObjectValue();
        Frame localFrame = paramFilterContext.getFrameManager().newFrame(this.mOutputFormat);
        localFrame.setObjectValue(str.toUpperCase());
        pushOutput("uppercase", localFrame);
    }

    public void setupPorts()
    {
        this.mOutputFormat = ObjectFormat.fromClass(String.class, 1);
        addMaskedInputPort("mixedcase", this.mOutputFormat);
        addOutputPort("uppercase", this.mOutputFormat);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterpacks.text.ToUpperCase
 * JD-Core Version:        0.6.2
 */