package android.filterpacks.text;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterContext;
import android.filterfw.core.Frame;
import android.filterfw.core.FrameFormat;
import android.filterfw.core.FrameManager;
import android.filterfw.core.GenerateFieldPort;
import android.filterfw.format.ObjectFormat;

public class StringSource extends Filter
{
    private FrameFormat mOutputFormat;

    @GenerateFieldPort(name="stringValue")
    private String mString;

    public StringSource(String paramString)
    {
        super(paramString);
    }

    public void process(FilterContext paramFilterContext)
    {
        Frame localFrame = paramFilterContext.getFrameManager().newFrame(this.mOutputFormat);
        localFrame.setObjectValue(this.mString);
        localFrame.setTimestamp(-1L);
        pushOutput("string", localFrame);
        closeOutputPort("string");
    }

    public void setupPorts()
    {
        this.mOutputFormat = ObjectFormat.fromClass(String.class, 1);
        addOutputPort("string", this.mOutputFormat);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterpacks.text.StringSource
 * JD-Core Version:        0.6.2
 */