package android.filterpacks.text;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterContext;
import android.filterfw.core.Frame;
import android.filterfw.format.ObjectFormat;
import android.util.Log;

public class StringLogger extends Filter
{
    public StringLogger(String paramString)
    {
        super(paramString);
    }

    public void process(FilterContext paramFilterContext)
    {
        Log.i("StringLogger", pullInput("string").getObjectValue().toString());
    }

    public void setupPorts()
    {
        addMaskedInputPort("string", ObjectFormat.fromClass(Object.class, 1));
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterpacks.text.StringLogger
 * JD-Core Version:        0.6.2
 */