package android.filterpacks.performance;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterContext;
import android.filterfw.core.Frame;
import android.filterfw.core.FrameFormat;
import android.filterfw.core.FrameManager;
import android.filterfw.core.GenerateFieldPort;
import android.filterfw.format.ObjectFormat;
import android.os.SystemClock;

public class ThroughputFilter extends Filter
{
    private long mLastTime = 0L;
    private FrameFormat mOutputFormat;

    @GenerateFieldPort(hasDefault=true, name="period")
    private int mPeriod = 5;
    private int mPeriodFrameCount = 0;
    private int mTotalFrameCount = 0;

    public ThroughputFilter(String paramString)
    {
        super(paramString);
    }

    public FrameFormat getOutputFormat(String paramString, FrameFormat paramFrameFormat)
    {
        return paramFrameFormat;
    }

    public void open(FilterContext paramFilterContext)
    {
        this.mTotalFrameCount = 0;
        this.mPeriodFrameCount = 0;
        this.mLastTime = 0L;
    }

    public void process(FilterContext paramFilterContext)
    {
        Frame localFrame1 = pullInput("frame");
        pushOutput("frame", localFrame1);
        this.mTotalFrameCount = (1 + this.mTotalFrameCount);
        this.mPeriodFrameCount = (1 + this.mPeriodFrameCount);
        if (this.mLastTime == 0L)
            this.mLastTime = SystemClock.elapsedRealtime();
        long l = SystemClock.elapsedRealtime();
        if (l - this.mLastTime >= 1000 * this.mPeriod)
        {
            FrameFormat localFrameFormat = localFrame1.getFormat();
            int i = localFrameFormat.getWidth() * localFrameFormat.getHeight();
            Throughput localThroughput = new Throughput(this.mTotalFrameCount, this.mPeriodFrameCount, this.mPeriod, i);
            Frame localFrame2 = paramFilterContext.getFrameManager().newFrame(this.mOutputFormat);
            localFrame2.setObjectValue(localThroughput);
            pushOutput("throughput", localFrame2);
            this.mLastTime = l;
            this.mPeriodFrameCount = 0;
        }
    }

    public void setupPorts()
    {
        addInputPort("frame");
        this.mOutputFormat = ObjectFormat.fromClass(Throughput.class, 1);
        addOutputBasedOnInput("frame", "frame");
        addOutputPort("throughput", this.mOutputFormat);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterpacks.performance.ThroughputFilter
 * JD-Core Version:        0.6.2
 */