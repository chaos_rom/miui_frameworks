package android.filterpacks.performance;

public class Throughput
{
    private final int mPeriodFrames;
    private final int mPeriodTime;
    private final int mPixels;
    private final int mTotalFrames;

    public Throughput(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        this.mTotalFrames = paramInt1;
        this.mPeriodFrames = paramInt2;
        this.mPeriodTime = paramInt3;
        this.mPixels = paramInt4;
    }

    public float getFramesPerSecond()
    {
        return this.mPeriodFrames / this.mPeriodTime;
    }

    public float getNanosPerPixel()
    {
        return (float)(1000000.0D * (this.mPeriodTime / this.mPeriodFrames) / this.mPixels);
    }

    public int getPeriodFrameCount()
    {
        return this.mPeriodFrames;
    }

    public int getPeriodTime()
    {
        return this.mPeriodTime;
    }

    public int getTotalFrameCount()
    {
        return this.mTotalFrames;
    }

    public String toString()
    {
        return getFramesPerSecond() + " FPS";
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterpacks.performance.Throughput
 * JD-Core Version:        0.6.2
 */