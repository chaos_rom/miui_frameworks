package android.filterpacks.videosink;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterContext;
import android.filterfw.core.Frame;
import android.filterfw.core.FrameManager;
import android.filterfw.core.GLEnvironment;
import android.filterfw.core.GLFrame;
import android.filterfw.core.GenerateFieldPort;
import android.filterfw.core.MutableFrameFormat;
import android.filterfw.core.ShaderProgram;
import android.filterfw.format.ImageFormat;
import android.filterfw.geometry.Point;
import android.filterfw.geometry.Quad;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.media.MediaRecorder.OnErrorListener;
import android.media.MediaRecorder.OnInfoListener;
import android.util.Log;
import java.io.FileDescriptor;
import java.io.IOException;

public class MediaEncoderFilter extends Filter
{
    private static final int NO_AUDIO_SOURCE = -1;
    private static final String TAG = "MediaEncoderFilter";

    @GenerateFieldPort(hasDefault=true, name="audioSource")
    private int mAudioSource = -1;
    private boolean mCaptureTimeLapse = false;

    @GenerateFieldPort(hasDefault=true, name="errorListener")
    private MediaRecorder.OnErrorListener mErrorListener = null;

    @GenerateFieldPort(hasDefault=true, name="outputFileDescriptor")
    private FileDescriptor mFd = null;

    @GenerateFieldPort(hasDefault=true, name="framerate")
    private int mFps = 30;

    @GenerateFieldPort(hasDefault=true, name="height")
    private int mHeight = 0;

    @GenerateFieldPort(hasDefault=true, name="infoListener")
    private MediaRecorder.OnInfoListener mInfoListener = null;
    private long mLastTimeLapseFrameRealTimestampNs = 0L;
    private boolean mLogVerbose = Log.isLoggable("MediaEncoderFilter", 2);

    @GenerateFieldPort(hasDefault=true, name="maxDurationMs")
    private int mMaxDurationMs = 0;

    @GenerateFieldPort(hasDefault=true, name="maxFileSize")
    private long mMaxFileSize = 0L;
    private MediaRecorder mMediaRecorder;
    private int mNumFramesEncoded = 0;

    @GenerateFieldPort(hasDefault=true, name="orientationHint")
    private int mOrientationHint = 0;

    @GenerateFieldPort(hasDefault=true, name="outputFile")
    private String mOutputFile = new String("/sdcard/MediaEncoderOut.mp4");

    @GenerateFieldPort(hasDefault=true, name="outputFormat")
    private int mOutputFormat = 2;

    @GenerateFieldPort(hasDefault=true, name="recordingProfile")
    private CamcorderProfile mProfile = null;
    private ShaderProgram mProgram;

    @GenerateFieldPort(hasDefault=true, name="recording")
    private boolean mRecording = true;
    private boolean mRecordingActive = false;

    @GenerateFieldPort(hasDefault=true, name="recordingDoneListener")
    private OnRecordingDoneListener mRecordingDoneListener = null;
    private GLFrame mScreen;

    @GenerateFieldPort(hasDefault=true, name="inputRegion")
    private Quad mSourceRegion = new Quad(new Point(0.0F, 0.0F), new Point(1.0F, 0.0F), new Point(0.0F, 1.0F), new Point(1.0F, 1.0F));
    private int mSurfaceId;

    @GenerateFieldPort(hasDefault=true, name="timelapseRecordingIntervalUs")
    private long mTimeBetweenTimeLapseFrameCaptureUs = 0L;
    private long mTimestampNs = 0L;

    @GenerateFieldPort(hasDefault=true, name="videoEncoder")
    private int mVideoEncoder = 2;

    @GenerateFieldPort(hasDefault=true, name="width")
    private int mWidth = 0;

    public MediaEncoderFilter(String paramString)
    {
        super(paramString);
    }

    private void startRecording(FilterContext paramFilterContext)
    {
        if (this.mLogVerbose)
            Log.v("MediaEncoderFilter", "Starting recording");
        MutableFrameFormat localMutableFrameFormat = new MutableFrameFormat(2, 3);
        localMutableFrameFormat.setBytesPerSample(4);
        int i;
        if ((this.mWidth > 0) && (this.mHeight > 0))
            i = 1;
        while (true)
        {
            int j;
            int k;
            if ((this.mProfile != null) && (i == 0))
            {
                j = this.mProfile.videoFrameWidth;
                k = this.mProfile.videoFrameHeight;
                localMutableFrameFormat.setDimensions(j, k);
                this.mScreen = ((GLFrame)paramFilterContext.getFrameManager().newBoundFrame(localMutableFrameFormat, 101, 0L));
                this.mMediaRecorder = new MediaRecorder();
                updateMediaRecorderParams();
            }
            try
            {
                this.mMediaRecorder.prepare();
                this.mMediaRecorder.start();
                if (this.mLogVerbose)
                    Log.v("MediaEncoderFilter", "Open: registering surface from Mediarecorder");
                this.mSurfaceId = paramFilterContext.getGLEnvironment().registerSurfaceFromMediaRecorder(this.mMediaRecorder);
                this.mNumFramesEncoded = 0;
                this.mRecordingActive = true;
                return;
                i = 0;
                continue;
                j = this.mWidth;
                k = this.mHeight;
            }
            catch (IllegalStateException localIllegalStateException)
            {
                throw localIllegalStateException;
            }
            catch (IOException localIOException)
            {
                throw new RuntimeException("IOException inMediaRecorder.prepare()!", localIOException);
            }
            catch (Exception localException)
            {
                throw new RuntimeException("Unknown Exception inMediaRecorder.prepare()!", localException);
            }
        }
    }

    private void stopRecording(FilterContext paramFilterContext)
    {
        if (this.mLogVerbose)
            Log.v("MediaEncoderFilter", "Stopping recording");
        this.mRecordingActive = false;
        this.mNumFramesEncoded = 0;
        GLEnvironment localGLEnvironment = paramFilterContext.getGLEnvironment();
        if (this.mLogVerbose)
        {
            Object[] arrayOfObject = new Object[1];
            arrayOfObject[0] = Integer.valueOf(this.mSurfaceId);
            Log.v("MediaEncoderFilter", String.format("Unregistering surface %d", arrayOfObject));
        }
        localGLEnvironment.unregisterSurfaceId(this.mSurfaceId);
        try
        {
            this.mMediaRecorder.stop();
            this.mMediaRecorder.release();
            this.mMediaRecorder = null;
            this.mScreen.release();
            this.mScreen = null;
            if (this.mRecordingDoneListener != null)
                this.mRecordingDoneListener.onRecordingDone();
            return;
        }
        catch (RuntimeException localRuntimeException)
        {
            throw new MediaRecorderStopException("MediaRecorder.stop() failed!", localRuntimeException);
        }
    }

    private void updateMediaRecorderParams()
    {
        boolean bool;
        if (this.mTimeBetweenTimeLapseFrameCaptureUs > 0L)
            bool = true;
        while (true)
        {
            this.mCaptureTimeLapse = bool;
            this.mMediaRecorder.setVideoSource(2);
            if ((!this.mCaptureTimeLapse) && (this.mAudioSource != -1))
                this.mMediaRecorder.setAudioSource(this.mAudioSource);
            if (this.mProfile != null)
            {
                this.mMediaRecorder.setProfile(this.mProfile);
                this.mFps = this.mProfile.videoFrameRate;
                if ((this.mWidth > 0) && (this.mHeight > 0))
                    this.mMediaRecorder.setVideoSize(this.mWidth, this.mHeight);
                label109: this.mMediaRecorder.setOrientationHint(this.mOrientationHint);
                this.mMediaRecorder.setOnInfoListener(this.mInfoListener);
                this.mMediaRecorder.setOnErrorListener(this.mErrorListener);
                if (this.mFd == null)
                    break label239;
                this.mMediaRecorder.setOutputFile(this.mFd);
            }
            try
            {
                while (true)
                {
                    this.mMediaRecorder.setMaxFileSize(this.mMaxFileSize);
                    this.mMediaRecorder.setMaxDuration(this.mMaxDurationMs);
                    return;
                    bool = false;
                    break;
                    this.mMediaRecorder.setOutputFormat(this.mOutputFormat);
                    this.mMediaRecorder.setVideoEncoder(this.mVideoEncoder);
                    this.mMediaRecorder.setVideoSize(this.mWidth, this.mHeight);
                    this.mMediaRecorder.setVideoFrameRate(this.mFps);
                    break label109;
                    label239: this.mMediaRecorder.setOutputFile(this.mOutputFile);
                }
            }
            catch (Exception localException)
            {
                while (true)
                    Log.w("MediaEncoderFilter", "Setting maxFileSize on MediaRecorder unsuccessful! " + localException.getMessage());
            }
        }
    }

    private void updateSourceRegion()
    {
        Quad localQuad = new Quad();
        localQuad.p0 = this.mSourceRegion.p2;
        localQuad.p1 = this.mSourceRegion.p3;
        localQuad.p2 = this.mSourceRegion.p0;
        localQuad.p3 = this.mSourceRegion.p1;
        this.mProgram.setSourceRegion(localQuad);
    }

    public void close(FilterContext paramFilterContext)
    {
        if (this.mLogVerbose)
            Log.v("MediaEncoderFilter", "Closing");
        if (this.mRecordingActive)
            stopRecording(paramFilterContext);
    }

    public void fieldPortValueUpdated(String paramString, FilterContext paramFilterContext)
    {
        if (this.mLogVerbose)
            Log.v("MediaEncoderFilter", "Port " + paramString + " has been updated");
        if (paramString.equals("recording"));
        do
            while (true)
            {
                return;
                if (!paramString.equals("inputRegion"))
                    break;
                if (isOpen())
                    updateSourceRegion();
            }
        while ((!isOpen()) || (!this.mRecordingActive));
        throw new RuntimeException("Cannot change recording parameters when the filter is recording!");
    }

    public void open(FilterContext paramFilterContext)
    {
        if (this.mLogVerbose)
            Log.v("MediaEncoderFilter", "Opening");
        updateSourceRegion();
        if (this.mRecording)
            startRecording(paramFilterContext);
    }

    public void prepare(FilterContext paramFilterContext)
    {
        if (this.mLogVerbose)
            Log.v("MediaEncoderFilter", "Preparing");
        this.mProgram = ShaderProgram.createIdentity(paramFilterContext);
        this.mRecordingActive = false;
    }

    public void process(FilterContext paramFilterContext)
    {
        GLEnvironment localGLEnvironment = paramFilterContext.getGLEnvironment();
        Frame localFrame = pullInput("videoframe");
        if ((!this.mRecordingActive) && (this.mRecording))
            startRecording(paramFilterContext);
        if ((this.mRecordingActive) && (!this.mRecording))
            stopRecording(paramFilterContext);
        if (!this.mRecordingActive);
        do
        {
            return;
            if (!this.mCaptureTimeLapse)
                break;
        }
        while (skipFrameAndModifyTimestamp(localFrame.getTimestamp()));
        while (true)
        {
            localGLEnvironment.activateSurfaceWithId(this.mSurfaceId);
            this.mProgram.process(localFrame, this.mScreen);
            localGLEnvironment.setSurfaceTimestamp(this.mTimestampNs);
            localGLEnvironment.swapBuffers();
            this.mNumFramesEncoded = (1 + this.mNumFramesEncoded);
            break;
            this.mTimestampNs = localFrame.getTimestamp();
        }
    }

    public void setupPorts()
    {
        addMaskedInputPort("videoframe", ImageFormat.create(3, 3));
    }

    public boolean skipFrameAndModifyTimestamp(long paramLong)
    {
        boolean bool = false;
        if (this.mNumFramesEncoded == 0)
        {
            this.mLastTimeLapseFrameRealTimestampNs = paramLong;
            this.mTimestampNs = paramLong;
            if (this.mLogVerbose)
                Log.v("MediaEncoderFilter", "timelapse: FIRST frame, last real t= " + this.mLastTimeLapseFrameRealTimestampNs + ", setting t = " + this.mTimestampNs);
        }
        while (true)
        {
            return bool;
            if ((this.mNumFramesEncoded >= 2) && (paramLong < this.mLastTimeLapseFrameRealTimestampNs + 1000L * this.mTimeBetweenTimeLapseFrameCaptureUs))
            {
                if (this.mLogVerbose)
                    Log.v("MediaEncoderFilter", "timelapse: skipping intermediate frame");
                bool = true;
            }
            else
            {
                if (this.mLogVerbose)
                    Log.v("MediaEncoderFilter", "timelapse: encoding frame, Timestamp t = " + paramLong + ", last real t= " + this.mLastTimeLapseFrameRealTimestampNs + ", interval = " + this.mTimeBetweenTimeLapseFrameCaptureUs);
                this.mLastTimeLapseFrameRealTimestampNs = paramLong;
                this.mTimestampNs += 1000000000L / this.mFps;
                if (this.mLogVerbose)
                    Log.v("MediaEncoderFilter", "timelapse: encoding frame, setting t = " + this.mTimestampNs + ", delta t = " + 1000000000L / this.mFps + ", fps = " + this.mFps);
            }
        }
    }

    public void tearDown(FilterContext paramFilterContext)
    {
        if (this.mMediaRecorder != null)
            this.mMediaRecorder.release();
        if (this.mScreen != null)
            this.mScreen.release();
    }

    public static abstract interface OnRecordingDoneListener
    {
        public abstract void onRecordingDone();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterpacks.videosink.MediaEncoderFilter
 * JD-Core Version:        0.6.2
 */