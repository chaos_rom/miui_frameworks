package android.filterpacks.videosink;

public class MediaRecorderStopException extends RuntimeException
{
    private static final String TAG = "MediaRecorderStopException";

    public MediaRecorderStopException()
    {
    }

    public MediaRecorderStopException(String paramString)
    {
        super(paramString);
    }

    public MediaRecorderStopException(String paramString, Throwable paramThrowable)
    {
        super(paramString, paramThrowable);
    }

    public MediaRecorderStopException(Throwable paramThrowable)
    {
        super(paramThrowable);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterpacks.videosink.MediaRecorderStopException
 * JD-Core Version:        0.6.2
 */