package android.filterpacks.base;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterContext;
import android.filterfw.core.Frame;
import android.filterfw.core.FrameFormat;
import android.filterfw.core.FrameManager;
import android.filterfw.core.GenerateFieldPort;
import android.filterfw.core.GenerateFinalPort;
import android.filterfw.core.MutableFrameFormat;
import android.filterfw.format.PrimitiveFormat;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

public class InputStreamSource extends Filter
{

    @GenerateFieldPort(name="stream")
    private InputStream mInputStream;

    @GenerateFinalPort(hasDefault=true, name="format")
    private MutableFrameFormat mOutputFormat = null;

    @GenerateFinalPort(name="target")
    private String mTarget;

    public InputStreamSource(String paramString)
    {
        super(paramString);
    }

    public void process(FilterContext paramFilterContext)
    {
        int i = 0;
        try
        {
            ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
            byte[] arrayOfByte = new byte[1024];
            while (true)
            {
                int j = this.mInputStream.read(arrayOfByte);
                if (j <= 0)
                    break;
                localByteArrayOutputStream.write(arrayOfByte, 0, j);
                i += j;
            }
            ByteBuffer localByteBuffer = ByteBuffer.wrap(localByteArrayOutputStream.toByteArray());
            this.mOutputFormat.setDimensions(i);
            Frame localFrame = paramFilterContext.getFrameManager().newFrame(this.mOutputFormat);
            localFrame.setData(localByteBuffer);
            pushOutput("data", localFrame);
            localFrame.release();
            closeOutputPort("data");
            return;
        }
        catch (IOException localIOException)
        {
            throw new RuntimeException("InputStreamSource: Could not read stream: " + localIOException.getMessage() + "!");
        }
    }

    public void setupPorts()
    {
        int i = FrameFormat.readTargetString(this.mTarget);
        if (this.mOutputFormat == null)
            this.mOutputFormat = PrimitiveFormat.createByteFormat(i);
        addOutputPort("data", this.mOutputFormat);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterpacks.base.InputStreamSource
 * JD-Core Version:        0.6.2
 */