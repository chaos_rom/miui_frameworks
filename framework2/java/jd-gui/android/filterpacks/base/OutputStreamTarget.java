package android.filterpacks.base;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterContext;
import android.filterfw.core.Frame;
import android.filterfw.core.FrameFormat;
import android.filterfw.core.GenerateFieldPort;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;

public class OutputStreamTarget extends Filter
{

    @GenerateFieldPort(name="stream")
    private OutputStream mOutputStream;

    public OutputStreamTarget(String paramString)
    {
        super(paramString);
    }

    public void process(FilterContext paramFilterContext)
    {
        Frame localFrame = pullInput("data");
        ByteBuffer localByteBuffer;
        if (localFrame.getFormat().getObjectClass() == String.class)
            localByteBuffer = ByteBuffer.wrap(((String)localFrame.getObjectValue()).getBytes());
        try
        {
            while (true)
            {
                this.mOutputStream.write(localByteBuffer.array(), 0, localByteBuffer.limit());
                this.mOutputStream.flush();
                return;
                localByteBuffer = localFrame.getData();
            }
        }
        catch (IOException localIOException)
        {
            throw new RuntimeException("OutputStreamTarget: Could not write to stream: " + localIOException.getMessage() + "!");
        }
    }

    public void setupPorts()
    {
        addInputPort("data");
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterpacks.base.OutputStreamTarget
 * JD-Core Version:        0.6.2
 */