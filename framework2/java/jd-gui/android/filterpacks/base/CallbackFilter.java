package android.filterpacks.base;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterContext;
import android.filterfw.core.FilterContext.OnFrameReceivedListener;
import android.filterfw.core.Frame;
import android.filterfw.core.GenerateFieldPort;
import android.filterfw.core.GenerateFinalPort;
import android.os.Handler;
import android.os.Looper;

public class CallbackFilter extends Filter
{

    @GenerateFinalPort(hasDefault=true, name="callUiThread")
    private boolean mCallbacksOnUiThread = true;

    @GenerateFieldPort(hasDefault=true, name="listener")
    private FilterContext.OnFrameReceivedListener mListener;
    private Handler mUiThreadHandler;

    @GenerateFieldPort(hasDefault=true, name="userData")
    private Object mUserData;

    public CallbackFilter(String paramString)
    {
        super(paramString);
    }

    public void prepare(FilterContext paramFilterContext)
    {
        if (this.mCallbacksOnUiThread)
            this.mUiThreadHandler = new Handler(Looper.getMainLooper());
    }

    public void process(FilterContext paramFilterContext)
    {
        Frame localFrame = pullInput("frame");
        if (this.mListener != null)
        {
            if (this.mCallbacksOnUiThread)
            {
                localFrame.retain();
                CallbackRunnable localCallbackRunnable = new CallbackRunnable(this.mListener, this, localFrame, this.mUserData);
                if (!this.mUiThreadHandler.post(localCallbackRunnable))
                    throw new RuntimeException("Unable to send callback to UI thread!");
            }
            else
            {
                this.mListener.onFrameReceived(this, localFrame, this.mUserData);
            }
            return;
        }
        throw new RuntimeException("CallbackFilter received frame, but no listener set!");
    }

    public void setupPorts()
    {
        addInputPort("frame");
    }

    private class CallbackRunnable
        implements Runnable
    {
        private Filter mFilter;
        private Frame mFrame;
        private FilterContext.OnFrameReceivedListener mListener;
        private Object mUserData;

        public CallbackRunnable(FilterContext.OnFrameReceivedListener paramFilter, Filter paramFrame, Frame paramObject, Object arg5)
        {
            this.mListener = paramFilter;
            this.mFilter = paramFrame;
            this.mFrame = paramObject;
            Object localObject;
            this.mUserData = localObject;
        }

        public void run()
        {
            this.mListener.onFrameReceived(this.mFilter, this.mFrame, this.mUserData);
            this.mFrame.release();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterpacks.base.CallbackFilter
 * JD-Core Version:        0.6.2
 */