package android.filterpacks.base;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterContext;
import android.filterfw.core.Frame;
import android.filterfw.core.GenerateFieldPort;

public class FrameStore extends Filter
{

    @GenerateFieldPort(name="key")
    private String mKey;

    public FrameStore(String paramString)
    {
        super(paramString);
    }

    public void process(FilterContext paramFilterContext)
    {
        Frame localFrame = pullInput("frame");
        paramFilterContext.storeFrame(this.mKey, localFrame);
    }

    public void setupPorts()
    {
        addInputPort("frame");
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterpacks.base.FrameStore
 * JD-Core Version:        0.6.2
 */