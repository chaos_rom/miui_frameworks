package android.filterpacks.base;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterContext;
import android.filterfw.core.Frame;
import android.filterfw.core.FrameFormat;
import android.filterfw.core.FrameManager;
import android.filterfw.core.GenerateFieldPort;
import android.filterfw.core.MutableFrameFormat;
import android.filterfw.format.ImageFormat;

public class GLTextureTarget extends Filter
{

    @GenerateFieldPort(name="texId")
    private int mTexId;

    public GLTextureTarget(String paramString)
    {
        super(paramString);
    }

    public void process(FilterContext paramFilterContext)
    {
        Frame localFrame1 = pullInput("frame");
        MutableFrameFormat localMutableFrameFormat = ImageFormat.create(localFrame1.getFormat().getWidth(), localFrame1.getFormat().getHeight(), 3, 3);
        Frame localFrame2 = paramFilterContext.getFrameManager().newBoundFrame(localMutableFrameFormat, 100, this.mTexId);
        localFrame2.setDataFromFrame(localFrame1);
        localFrame2.release();
    }

    public void setupPorts()
    {
        addMaskedInputPort("frame", ImageFormat.create(3));
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterpacks.base.GLTextureTarget
 * JD-Core Version:        0.6.2
 */