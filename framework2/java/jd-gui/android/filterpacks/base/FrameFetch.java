package android.filterpacks.base;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterContext;
import android.filterfw.core.Frame;
import android.filterfw.core.FrameFormat;
import android.filterfw.core.GenerateFieldPort;
import android.filterfw.core.GenerateFinalPort;

public class FrameFetch extends Filter
{

    @GenerateFinalPort(hasDefault=true, name="format")
    private FrameFormat mFormat;

    @GenerateFieldPort(name="key")
    private String mKey;

    @GenerateFieldPort(hasDefault=true, name="repeatFrame")
    private boolean mRepeatFrame = false;

    public FrameFetch(String paramString)
    {
        super(paramString);
    }

    public void process(FilterContext paramFilterContext)
    {
        Frame localFrame = paramFilterContext.fetchFrame(this.mKey);
        if (localFrame != null)
        {
            pushOutput("frame", localFrame);
            if (!this.mRepeatFrame)
                closeOutputPort("frame");
        }
        while (true)
        {
            return;
            delayNextProcess(250);
        }
    }

    public void setupPorts()
    {
        if (this.mFormat == null);
        for (FrameFormat localFrameFormat = FrameFormat.unspecified(); ; localFrameFormat = this.mFormat)
        {
            addOutputPort("frame", localFrameFormat);
            return;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterpacks.base.FrameFetch
 * JD-Core Version:        0.6.2
 */