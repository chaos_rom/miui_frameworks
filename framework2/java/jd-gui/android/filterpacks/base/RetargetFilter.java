package android.filterpacks.base;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterContext;
import android.filterfw.core.Frame;
import android.filterfw.core.FrameFormat;
import android.filterfw.core.FrameManager;
import android.filterfw.core.GenerateFinalPort;
import android.filterfw.core.MutableFrameFormat;

public class RetargetFilter extends Filter
{
    private MutableFrameFormat mOutputFormat;
    private int mTarget = -1;

    @GenerateFinalPort(hasDefault=false, name="target")
    private String mTargetString;

    public RetargetFilter(String paramString)
    {
        super(paramString);
    }

    public FrameFormat getOutputFormat(String paramString, FrameFormat paramFrameFormat)
    {
        MutableFrameFormat localMutableFrameFormat = paramFrameFormat.mutableCopy();
        localMutableFrameFormat.setTarget(this.mTarget);
        return localMutableFrameFormat;
    }

    public void process(FilterContext paramFilterContext)
    {
        Frame localFrame1 = pullInput("frame");
        Frame localFrame2 = paramFilterContext.getFrameManager().duplicateFrameToTarget(localFrame1, this.mTarget);
        pushOutput("frame", localFrame2);
        localFrame2.release();
    }

    public void setupPorts()
    {
        this.mTarget = FrameFormat.readTargetString(this.mTargetString);
        addInputPort("frame");
        addOutputBasedOnInput("frame", "frame");
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterpacks.base.RetargetFilter
 * JD-Core Version:        0.6.2
 */