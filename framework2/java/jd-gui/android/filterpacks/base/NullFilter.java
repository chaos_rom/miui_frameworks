package android.filterpacks.base;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterContext;

public class NullFilter extends Filter
{
    public NullFilter(String paramString)
    {
        super(paramString);
    }

    public void process(FilterContext paramFilterContext)
    {
        pullInput("frame");
    }

    public void setupPorts()
    {
        addInputPort("frame");
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterpacks.base.NullFilter
 * JD-Core Version:        0.6.2
 */