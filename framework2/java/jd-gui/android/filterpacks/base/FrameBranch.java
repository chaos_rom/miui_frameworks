package android.filterpacks.base;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterContext;
import android.filterfw.core.Frame;
import android.filterfw.core.FrameFormat;
import android.filterfw.core.GenerateFinalPort;

public class FrameBranch extends Filter
{

    @GenerateFinalPort(hasDefault=true, name="outputs")
    private int mNumberOfOutputs = 2;

    public FrameBranch(String paramString)
    {
        super(paramString);
    }

    public FrameFormat getOutputFormat(String paramString, FrameFormat paramFrameFormat)
    {
        return paramFrameFormat;
    }

    public void process(FilterContext paramFilterContext)
    {
        Frame localFrame = pullInput("in");
        for (int i = 0; i < this.mNumberOfOutputs; i++)
            pushOutput("out" + i, localFrame);
    }

    public void setupPorts()
    {
        addInputPort("in");
        for (int i = 0; i < this.mNumberOfOutputs; i++)
            addOutputBasedOnInput("out" + i, "in");
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterpacks.base.FrameBranch
 * JD-Core Version:        0.6.2
 */