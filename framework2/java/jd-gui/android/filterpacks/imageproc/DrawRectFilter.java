package android.filterpacks.imageproc;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterContext;
import android.filterfw.core.Frame;
import android.filterfw.core.FrameFormat;
import android.filterfw.core.FrameManager;
import android.filterfw.core.GLFrame;
import android.filterfw.core.GenerateFieldPort;
import android.filterfw.core.ShaderProgram;
import android.filterfw.format.ImageFormat;
import android.filterfw.format.ObjectFormat;
import android.filterfw.geometry.Point;
import android.filterfw.geometry.Quad;
import android.opengl.GLES20;

public class DrawRectFilter extends Filter
{

    @GenerateFieldPort(hasDefault=true, name="colorBlue")
    private float mColorBlue = 0.0F;

    @GenerateFieldPort(hasDefault=true, name="colorGreen")
    private float mColorGreen = 0.8F;

    @GenerateFieldPort(hasDefault=true, name="colorRed")
    private float mColorRed = 0.8F;
    private final String mFixedColorFragmentShader = "precision mediump float;\nuniform vec4 color;\nvoid main() {\n    gl_FragColor = color;\n}\n";
    private ShaderProgram mProgram;
    private final String mVertexShader = "attribute vec4 aPosition;\nvoid main() {\n    gl_Position = aPosition;\n}\n";

    public DrawRectFilter(String paramString)
    {
        super(paramString);
    }

    private void renderBox(Quad paramQuad)
    {
        float[] arrayOfFloat1 = new float[4];
        arrayOfFloat1[0] = this.mColorRed;
        arrayOfFloat1[1] = this.mColorGreen;
        arrayOfFloat1[2] = this.mColorBlue;
        arrayOfFloat1[3] = 1.0F;
        float[] arrayOfFloat2 = new float[8];
        arrayOfFloat2[0] = paramQuad.p0.x;
        arrayOfFloat2[1] = paramQuad.p0.y;
        arrayOfFloat2[2] = paramQuad.p1.x;
        arrayOfFloat2[3] = paramQuad.p1.y;
        arrayOfFloat2[4] = paramQuad.p3.x;
        arrayOfFloat2[5] = paramQuad.p3.y;
        arrayOfFloat2[6] = paramQuad.p2.x;
        arrayOfFloat2[7] = paramQuad.p2.y;
        this.mProgram.setHostValue("color", arrayOfFloat1);
        this.mProgram.setAttributeValues("aPosition", arrayOfFloat2, 2);
        this.mProgram.setVertexCount(4);
        this.mProgram.beginDrawing();
        GLES20.glLineWidth(1.0F);
        GLES20.glDrawArrays(2, 0, 4);
    }

    public FrameFormat getOutputFormat(String paramString, FrameFormat paramFrameFormat)
    {
        return paramFrameFormat;
    }

    public void prepare(FilterContext paramFilterContext)
    {
        this.mProgram = new ShaderProgram(paramFilterContext, "attribute vec4 aPosition;\nvoid main() {\n    gl_Position = aPosition;\n}\n", "precision mediump float;\nuniform vec4 color;\nvoid main() {\n    gl_FragColor = color;\n}\n");
    }

    public void process(FilterContext paramFilterContext)
    {
        Frame localFrame = pullInput("image");
        Quad localQuad = ((Quad)pullInput("box").getObjectValue()).scaled(2.0F).translated(-1.0F, -1.0F);
        GLFrame localGLFrame = (GLFrame)paramFilterContext.getFrameManager().duplicateFrame(localFrame);
        localGLFrame.focus();
        renderBox(localQuad);
        pushOutput("image", localGLFrame);
        localGLFrame.release();
    }

    public void setupPorts()
    {
        addMaskedInputPort("image", ImageFormat.create(3, 3));
        addMaskedInputPort("box", ObjectFormat.fromClass(Quad.class, 1));
        addOutputBasedOnInput("image", "image");
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterpacks.imageproc.DrawRectFilter
 * JD-Core Version:        0.6.2
 */