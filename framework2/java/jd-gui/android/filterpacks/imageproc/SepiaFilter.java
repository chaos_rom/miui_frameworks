package android.filterpacks.imageproc;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterContext;
import android.filterfw.core.Frame;
import android.filterfw.core.FrameFormat;
import android.filterfw.core.FrameManager;
import android.filterfw.core.GenerateFieldPort;
import android.filterfw.core.Program;
import android.filterfw.core.ShaderProgram;
import android.filterfw.format.ImageFormat;

public class SepiaFilter extends Filter
{
    private Program mProgram;
    private final String mSepiaShader = "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform mat3 matrix;\nvarying vec2 v_texcoord;\nvoid main() {\n    vec4 color = texture2D(tex_sampler_0, v_texcoord);\n    vec3 new_color = min(matrix * color.rgb, 1.0);\n    gl_FragColor = vec4(new_color.rgb, color.a);\n}\n";
    private int mTarget = 0;

    @GenerateFieldPort(hasDefault=true, name="tile_size")
    private int mTileSize = 640;

    public SepiaFilter(String paramString)
    {
        super(paramString);
    }

    private void initParameters()
    {
        float[] arrayOfFloat = new float[9];
        arrayOfFloat[0] = 0.3930664F;
        arrayOfFloat[1] = 0.3491211F;
        arrayOfFloat[2] = 0.2719727F;
        arrayOfFloat[3] = 0.769043F;
        arrayOfFloat[4] = 0.6860352F;
        arrayOfFloat[5] = 0.5356445F;
        arrayOfFloat[6] = 0.1889648F;
        arrayOfFloat[7] = 0.1679688F;
        arrayOfFloat[8] = 0.1308594F;
        this.mProgram.setHostValue("matrix", arrayOfFloat);
    }

    public FrameFormat getOutputFormat(String paramString, FrameFormat paramFrameFormat)
    {
        return paramFrameFormat;
    }

    public void initProgram(FilterContext paramFilterContext, int paramInt)
    {
        switch (paramInt)
        {
        default:
            throw new RuntimeException("Filter Sharpen does not support frames of target " + paramInt + "!");
        case 3:
        }
        ShaderProgram localShaderProgram = new ShaderProgram(paramFilterContext, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform mat3 matrix;\nvarying vec2 v_texcoord;\nvoid main() {\n    vec4 color = texture2D(tex_sampler_0, v_texcoord);\n    vec3 new_color = min(matrix * color.rgb, 1.0);\n    gl_FragColor = vec4(new_color.rgb, color.a);\n}\n");
        localShaderProgram.setMaximumTileSize(this.mTileSize);
        this.mProgram = localShaderProgram;
        this.mTarget = paramInt;
    }

    public void process(FilterContext paramFilterContext)
    {
        Frame localFrame1 = pullInput("image");
        FrameFormat localFrameFormat = localFrame1.getFormat();
        Frame localFrame2 = paramFilterContext.getFrameManager().newFrame(localFrameFormat);
        if ((this.mProgram == null) || (localFrameFormat.getTarget() != this.mTarget))
        {
            initProgram(paramFilterContext, localFrameFormat.getTarget());
            initParameters();
        }
        this.mProgram.process(localFrame1, localFrame2);
        pushOutput("image", localFrame2);
        localFrame2.release();
    }

    public void setupPorts()
    {
        addMaskedInputPort("image", ImageFormat.create(3));
        addOutputBasedOnInput("image", "image");
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterpacks.imageproc.SepiaFilter
 * JD-Core Version:        0.6.2
 */