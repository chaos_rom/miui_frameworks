package android.filterpacks.imageproc;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterContext;
import android.filterfw.core.Frame;
import android.filterfw.core.FrameFormat;
import android.filterfw.core.FrameManager;
import android.filterfw.core.Program;
import android.filterfw.format.ImageFormat;
import java.lang.reflect.Field;

public abstract class ImageCombineFilter extends Filter
{
    protected int mCurrentTarget = 0;
    protected String[] mInputNames;
    protected String mOutputName;
    protected String mParameterName;
    protected Program mProgram;

    public ImageCombineFilter(String paramString1, String[] paramArrayOfString, String paramString2, String paramString3)
    {
        super(paramString1);
        this.mInputNames = paramArrayOfString;
        this.mOutputName = paramString2;
        this.mParameterName = paramString3;
    }

    private void assertAllInputTargetsMatch()
    {
        int i = getInputFormat(this.mInputNames[0]).getTarget();
        String[] arrayOfString = this.mInputNames;
        int j = arrayOfString.length;
        for (int k = 0; k < j; k++)
            if (i != getInputFormat(arrayOfString[k]).getTarget())
                throw new RuntimeException("Type mismatch of input formats in filter " + this + ". All input frames must have the same target!");
    }

    protected abstract Program getNativeProgram(FilterContext paramFilterContext);

    public FrameFormat getOutputFormat(String paramString, FrameFormat paramFrameFormat)
    {
        return paramFrameFormat;
    }

    protected abstract Program getShaderProgram(FilterContext paramFilterContext);

    public void process(FilterContext paramFilterContext)
    {
        Frame[] arrayOfFrame = new Frame[this.mInputNames.length];
        String[] arrayOfString = this.mInputNames;
        int i = arrayOfString.length;
        int j = 0;
        int m;
        for (int k = 0; j < i; k = m)
        {
            String str = arrayOfString[j];
            m = k + 1;
            arrayOfFrame[k] = pullInput(str);
            j++;
        }
        Frame localFrame = paramFilterContext.getFrameManager().newFrame(arrayOfFrame[0].getFormat());
        updateProgramWithTarget(arrayOfFrame[0].getFormat().getTarget(), paramFilterContext);
        this.mProgram.process(arrayOfFrame, localFrame);
        pushOutput(this.mOutputName, localFrame);
        localFrame.release();
    }

    public void setupPorts()
    {
        if (this.mParameterName != null);
        try
        {
            Field localField = ImageCombineFilter.class.getDeclaredField("mProgram");
            addProgramPort(this.mParameterName, this.mParameterName, localField, Float.TYPE, false);
            String[] arrayOfString = this.mInputNames;
            int i = arrayOfString.length;
            for (int j = 0; j < i; j++)
                addMaskedInputPort(arrayOfString[j], ImageFormat.create(3));
        }
        catch (NoSuchFieldException localNoSuchFieldException)
        {
            throw new RuntimeException("Internal Error: mProgram field not found!");
        }
        addOutputBasedOnInput(this.mOutputName, this.mInputNames[0]);
    }

    protected void updateProgramWithTarget(int paramInt, FilterContext paramFilterContext)
    {
        if (paramInt != this.mCurrentTarget)
        {
            switch (paramInt)
            {
            default:
                this.mProgram = null;
            case 2:
            case 3:
            }
            while (this.mProgram == null)
            {
                throw new RuntimeException("Could not create a program for image filter " + this + "!");
                this.mProgram = getNativeProgram(paramFilterContext);
                continue;
                this.mProgram = getShaderProgram(paramFilterContext);
            }
            initProgramInputs(this.mProgram, paramFilterContext);
            this.mCurrentTarget = paramInt;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterpacks.imageproc.ImageCombineFilter
 * JD-Core Version:        0.6.2
 */