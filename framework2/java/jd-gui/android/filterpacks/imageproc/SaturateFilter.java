package android.filterpacks.imageproc;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterContext;
import android.filterfw.core.Frame;
import android.filterfw.core.FrameFormat;
import android.filterfw.core.FrameManager;
import android.filterfw.core.GenerateFieldPort;
import android.filterfw.core.Program;
import android.filterfw.core.ShaderProgram;
import android.filterfw.format.ImageFormat;

public class SaturateFilter extends Filter
{
    private Program mBenProgram;
    private final String mBenSaturateShader = "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform float scale;\nuniform float shift;\nuniform vec3 weights;\nvarying vec2 v_texcoord;\nvoid main() {\n    vec4 color = texture2D(tex_sampler_0, v_texcoord);\n    float kv = dot(color.rgb, weights) + shift;\n    vec3 new_color = scale * color.rgb + (1.0 - scale) * kv;\n    gl_FragColor = vec4(new_color, color.a);\n}\n";
    private Program mHerfProgram;
    private final String mHerfSaturateShader = "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform vec3 weights;\nuniform vec3 exponents;\nvarying vec2 v_texcoord;\nvoid main() {\n    vec4 color = texture2D(tex_sampler_0, v_texcoord);\n    float de = dot(color.rgb, weights);\n    float inv_de = 1.0 / de;\n    vec3 new_color = de * pow(color.rgb * inv_de, exponents);\n    float max_color = max(max(max(new_color.r, new_color.g), new_color.b), 1.0);\n    gl_FragColor = vec4(new_color / max_color, color.a);\n}\n";

    @GenerateFieldPort(hasDefault=true, name="scale")
    private float mScale = 0.0F;
    private int mTarget = 0;

    @GenerateFieldPort(hasDefault=true, name="tile_size")
    private int mTileSize = 640;

    public SaturateFilter(String paramString)
    {
        super(paramString);
    }

    private void initParameters()
    {
        float[] arrayOfFloat = new float[3];
        arrayOfFloat[0] = 0.25F;
        arrayOfFloat[1] = 0.625F;
        arrayOfFloat[2] = 0.125F;
        this.mBenProgram.setHostValue("weights", arrayOfFloat);
        this.mBenProgram.setHostValue("shift", Float.valueOf(0.003921569F));
        this.mHerfProgram.setHostValue("weights", arrayOfFloat);
        updateParameters();
    }

    private void updateParameters()
    {
        if (this.mScale > 0.0F)
        {
            float[] arrayOfFloat = new float[3];
            arrayOfFloat[0] = (1.0F + 0.9F * this.mScale);
            arrayOfFloat[1] = (1.0F + 2.1F * this.mScale);
            arrayOfFloat[2] = (1.0F + 2.7F * this.mScale);
            this.mHerfProgram.setHostValue("exponents", arrayOfFloat);
        }
        while (true)
        {
            return;
            this.mBenProgram.setHostValue("scale", Float.valueOf(1.0F + this.mScale));
        }
    }

    public void fieldPortValueUpdated(String paramString, FilterContext paramFilterContext)
    {
        if ((this.mBenProgram != null) && (this.mHerfProgram != null))
            updateParameters();
    }

    public FrameFormat getOutputFormat(String paramString, FrameFormat paramFrameFormat)
    {
        return paramFrameFormat;
    }

    public void initProgram(FilterContext paramFilterContext, int paramInt)
    {
        switch (paramInt)
        {
        default:
            throw new RuntimeException("Filter Sharpen does not support frames of target " + paramInt + "!");
        case 3:
        }
        ShaderProgram localShaderProgram1 = new ShaderProgram(paramFilterContext, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform float scale;\nuniform float shift;\nuniform vec3 weights;\nvarying vec2 v_texcoord;\nvoid main() {\n    vec4 color = texture2D(tex_sampler_0, v_texcoord);\n    float kv = dot(color.rgb, weights) + shift;\n    vec3 new_color = scale * color.rgb + (1.0 - scale) * kv;\n    gl_FragColor = vec4(new_color, color.a);\n}\n");
        localShaderProgram1.setMaximumTileSize(this.mTileSize);
        this.mBenProgram = localShaderProgram1;
        ShaderProgram localShaderProgram2 = new ShaderProgram(paramFilterContext, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform vec3 weights;\nuniform vec3 exponents;\nvarying vec2 v_texcoord;\nvoid main() {\n    vec4 color = texture2D(tex_sampler_0, v_texcoord);\n    float de = dot(color.rgb, weights);\n    float inv_de = 1.0 / de;\n    vec3 new_color = de * pow(color.rgb * inv_de, exponents);\n    float max_color = max(max(max(new_color.r, new_color.g), new_color.b), 1.0);\n    gl_FragColor = vec4(new_color / max_color, color.a);\n}\n");
        localShaderProgram2.setMaximumTileSize(this.mTileSize);
        this.mHerfProgram = localShaderProgram2;
        this.mTarget = paramInt;
    }

    public void process(FilterContext paramFilterContext)
    {
        Frame localFrame1 = pullInput("image");
        FrameFormat localFrameFormat = localFrame1.getFormat();
        if ((this.mBenProgram == null) || (localFrameFormat.getTarget() != this.mTarget))
        {
            initProgram(paramFilterContext, localFrameFormat.getTarget());
            initParameters();
        }
        Frame localFrame2 = paramFilterContext.getFrameManager().newFrame(localFrameFormat);
        if (this.mScale > 0.0F)
            this.mHerfProgram.process(localFrame1, localFrame2);
        while (true)
        {
            pushOutput("image", localFrame2);
            localFrame2.release();
            return;
            this.mBenProgram.process(localFrame1, localFrame2);
        }
    }

    public void setupPorts()
    {
        addMaskedInputPort("image", ImageFormat.create(3));
        addOutputBasedOnInput("image", "image");
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterpacks.imageproc.SaturateFilter
 * JD-Core Version:        0.6.2
 */