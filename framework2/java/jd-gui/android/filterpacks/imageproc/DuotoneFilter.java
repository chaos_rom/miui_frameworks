package android.filterpacks.imageproc;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterContext;
import android.filterfw.core.Frame;
import android.filterfw.core.FrameFormat;
import android.filterfw.core.FrameManager;
import android.filterfw.core.GenerateFieldPort;
import android.filterfw.core.Program;
import android.filterfw.core.ShaderProgram;
import android.filterfw.format.ImageFormat;
import android.graphics.Color;

public class DuotoneFilter extends Filter
{
    private final String mDuotoneShader = "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform vec3 first;\nuniform vec3 second;\nvarying vec2 v_texcoord;\nvoid main() {\n    vec4 color = texture2D(tex_sampler_0, v_texcoord);\n    float energy = (color.r + color.g + color.b) * 0.3333;\n    vec3 new_color = (1.0 - energy) * first + energy * second;\n    gl_FragColor = vec4(new_color.rgb, color.a);\n}\n";

    @GenerateFieldPort(hasDefault=true, name="first_color")
    private int mFirstColor = -65536;
    private Program mProgram;

    @GenerateFieldPort(hasDefault=true, name="second_color")
    private int mSecondColor = -256;
    private int mTarget = 0;

    @GenerateFieldPort(hasDefault=true, name="tile_size")
    private int mTileSize = 640;

    public DuotoneFilter(String paramString)
    {
        super(paramString);
    }

    private void updateParameters()
    {
        float[] arrayOfFloat1 = new float[3];
        arrayOfFloat1[0] = (Color.red(this.mFirstColor) / 255.0F);
        arrayOfFloat1[1] = (Color.green(this.mFirstColor) / 255.0F);
        arrayOfFloat1[2] = (Color.blue(this.mFirstColor) / 255.0F);
        float[] arrayOfFloat2 = new float[3];
        arrayOfFloat2[0] = (Color.red(this.mSecondColor) / 255.0F);
        arrayOfFloat2[1] = (Color.green(this.mSecondColor) / 255.0F);
        arrayOfFloat2[2] = (Color.blue(this.mSecondColor) / 255.0F);
        this.mProgram.setHostValue("first", arrayOfFloat1);
        this.mProgram.setHostValue("second", arrayOfFloat2);
    }

    public FrameFormat getOutputFormat(String paramString, FrameFormat paramFrameFormat)
    {
        return paramFrameFormat;
    }

    public void initProgram(FilterContext paramFilterContext, int paramInt)
    {
        switch (paramInt)
        {
        default:
            throw new RuntimeException("Filter Duotone does not support frames of target " + paramInt + "!");
        case 3:
        }
        ShaderProgram localShaderProgram = new ShaderProgram(paramFilterContext, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform vec3 first;\nuniform vec3 second;\nvarying vec2 v_texcoord;\nvoid main() {\n    vec4 color = texture2D(tex_sampler_0, v_texcoord);\n    float energy = (color.r + color.g + color.b) * 0.3333;\n    vec3 new_color = (1.0 - energy) * first + energy * second;\n    gl_FragColor = vec4(new_color.rgb, color.a);\n}\n");
        localShaderProgram.setMaximumTileSize(this.mTileSize);
        this.mProgram = localShaderProgram;
        this.mTarget = paramInt;
    }

    public void process(FilterContext paramFilterContext)
    {
        Frame localFrame1 = pullInput("image");
        FrameFormat localFrameFormat = localFrame1.getFormat();
        Frame localFrame2 = paramFilterContext.getFrameManager().newFrame(localFrameFormat);
        if ((this.mProgram == null) || (localFrameFormat.getTarget() != this.mTarget))
            initProgram(paramFilterContext, localFrameFormat.getTarget());
        updateParameters();
        this.mProgram.process(localFrame1, localFrame2);
        pushOutput("image", localFrame2);
        localFrame2.release();
    }

    public void setupPorts()
    {
        addMaskedInputPort("image", ImageFormat.create(3));
        addOutputBasedOnInput("image", "image");
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterpacks.imageproc.DuotoneFilter
 * JD-Core Version:        0.6.2
 */