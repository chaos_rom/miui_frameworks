package android.filterpacks.imageproc;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterContext;
import android.filterfw.core.Frame;
import android.filterfw.core.FrameFormat;
import android.filterfw.core.FrameManager;
import android.filterfw.core.GenerateFieldPort;
import android.filterfw.core.Program;
import android.filterfw.core.ShaderProgram;
import android.filterfw.format.ImageFormat;

public class VignetteFilter extends Filter
{
    private int mHeight = 0;
    private Program mProgram;

    @GenerateFieldPort(hasDefault=true, name="scale")
    private float mScale = 0.0F;
    private final float mShade = 0.85F;
    private final float mSlope = 20.0F;
    private int mTarget = 0;

    @GenerateFieldPort(hasDefault=true, name="tile_size")
    private int mTileSize = 640;
    private final String mVignetteShader = "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform float range;\nuniform float inv_max_dist;\nuniform float shade;\nuniform vec2 scale;\nvarying vec2 v_texcoord;\nvoid main() {\n    const float slope = 20.0;\n    vec2 coord = v_texcoord - vec2(0.5, 0.5);\n    float dist = length(coord * scale);\n    float lumen = shade / (1.0 + exp((dist * inv_max_dist - range) * slope)) + (1.0 - shade);\n    vec4 color = texture2D(tex_sampler_0, v_texcoord);\n    gl_FragColor = vec4(color.rgb * lumen, color.a);\n}\n";
    private int mWidth = 0;

    public VignetteFilter(String paramString)
    {
        super(paramString);
    }

    private void initParameters()
    {
        float[] arrayOfFloat;
        if (this.mProgram != null)
        {
            arrayOfFloat = new float[2];
            if (this.mWidth <= this.mHeight)
                break label108;
            arrayOfFloat[0] = 1.0F;
            arrayOfFloat[1] = (this.mHeight / this.mWidth);
        }
        while (true)
        {
            float f = 0.5F * (float)Math.sqrt(arrayOfFloat[0] * arrayOfFloat[0] + arrayOfFloat[1] * arrayOfFloat[1]);
            this.mProgram.setHostValue("scale", arrayOfFloat);
            this.mProgram.setHostValue("inv_max_dist", Float.valueOf(1.0F / f));
            this.mProgram.setHostValue("shade", Float.valueOf(0.85F));
            updateParameters();
            return;
            label108: arrayOfFloat[0] = (this.mWidth / this.mHeight);
            arrayOfFloat[1] = 1.0F;
        }
    }

    private void updateParameters()
    {
        this.mProgram.setHostValue("range", Float.valueOf(1.3F - 0.7F * (float)Math.sqrt(this.mScale)));
    }

    public void fieldPortValueUpdated(String paramString, FilterContext paramFilterContext)
    {
        if (this.mProgram != null)
            updateParameters();
    }

    public FrameFormat getOutputFormat(String paramString, FrameFormat paramFrameFormat)
    {
        return paramFrameFormat;
    }

    public void initProgram(FilterContext paramFilterContext, int paramInt)
    {
        switch (paramInt)
        {
        default:
            throw new RuntimeException("Filter Sharpen does not support frames of target " + paramInt + "!");
        case 3:
        }
        ShaderProgram localShaderProgram = new ShaderProgram(paramFilterContext, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform float range;\nuniform float inv_max_dist;\nuniform float shade;\nuniform vec2 scale;\nvarying vec2 v_texcoord;\nvoid main() {\n    const float slope = 20.0;\n    vec2 coord = v_texcoord - vec2(0.5, 0.5);\n    float dist = length(coord * scale);\n    float lumen = shade / (1.0 + exp((dist * inv_max_dist - range) * slope)) + (1.0 - shade);\n    vec4 color = texture2D(tex_sampler_0, v_texcoord);\n    gl_FragColor = vec4(color.rgb * lumen, color.a);\n}\n");
        localShaderProgram.setMaximumTileSize(this.mTileSize);
        this.mProgram = localShaderProgram;
        this.mTarget = paramInt;
    }

    public void process(FilterContext paramFilterContext)
    {
        Frame localFrame1 = pullInput("image");
        FrameFormat localFrameFormat = localFrame1.getFormat();
        if ((this.mProgram == null) || (localFrameFormat.getTarget() != this.mTarget))
            initProgram(paramFilterContext, localFrameFormat.getTarget());
        if ((localFrameFormat.getWidth() != this.mWidth) || (localFrameFormat.getHeight() != this.mHeight))
        {
            this.mWidth = localFrameFormat.getWidth();
            this.mHeight = localFrameFormat.getHeight();
            initParameters();
        }
        Frame localFrame2 = paramFilterContext.getFrameManager().newFrame(localFrameFormat);
        this.mProgram.process(localFrame1, localFrame2);
        pushOutput("image", localFrame2);
        localFrame2.release();
    }

    public void setupPorts()
    {
        addMaskedInputPort("image", ImageFormat.create(3));
        addOutputBasedOnInput("image", "image");
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterpacks.imageproc.VignetteFilter
 * JD-Core Version:        0.6.2
 */