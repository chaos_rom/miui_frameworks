package android.filterpacks.imageproc;

import android.filterfw.core.FilterContext;
import android.filterfw.core.Program;
import android.filterfw.core.ShaderProgram;

public class BlendFilter extends ImageCombineFilter
{
    private final String mBlendShader = "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform sampler2D tex_sampler_1;\nuniform float blend;\nvarying vec2 v_texcoord;\nvoid main() {\n    vec4 colorL = texture2D(tex_sampler_0, v_texcoord);\n    vec4 colorR = texture2D(tex_sampler_1, v_texcoord);\n    float weight = colorR.a * blend;\n    gl_FragColor = mix(colorL, colorR, weight);\n}\n";

    public BlendFilter(String paramString)
    {
        super(paramString, arrayOfString, "blended", "blend");
    }

    protected Program getNativeProgram(FilterContext paramFilterContext)
    {
        throw new RuntimeException("TODO: Write native implementation for Blend!");
    }

    protected Program getShaderProgram(FilterContext paramFilterContext)
    {
        return new ShaderProgram(paramFilterContext, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform sampler2D tex_sampler_1;\nuniform float blend;\nvarying vec2 v_texcoord;\nvoid main() {\n    vec4 colorL = texture2D(tex_sampler_0, v_texcoord);\n    vec4 colorR = texture2D(tex_sampler_1, v_texcoord);\n    float weight = colorR.a * blend;\n    gl_FragColor = mix(colorL, colorR, weight);\n}\n");
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterpacks.imageproc.BlendFilter
 * JD-Core Version:        0.6.2
 */