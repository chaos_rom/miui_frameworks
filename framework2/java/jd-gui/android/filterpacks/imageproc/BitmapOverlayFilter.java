package android.filterpacks.imageproc;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterContext;
import android.filterfw.core.Frame;
import android.filterfw.core.FrameFormat;
import android.filterfw.core.FrameManager;
import android.filterfw.core.GenerateFieldPort;
import android.filterfw.core.MutableFrameFormat;
import android.filterfw.core.Program;
import android.filterfw.core.ShaderProgram;
import android.filterfw.format.ImageFormat;
import android.graphics.Bitmap;

public class BitmapOverlayFilter extends Filter
{

    @GenerateFieldPort(name="bitmap")
    private Bitmap mBitmap;
    private Frame mFrame;
    private final String mOverlayShader = "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform sampler2D tex_sampler_1;\nvarying vec2 v_texcoord;\nvoid main() {\n    vec4 original = texture2D(tex_sampler_0, v_texcoord);\n    vec4 mask = texture2D(tex_sampler_1, v_texcoord);\n    gl_FragColor = vec4(original.rgb * (1.0 - mask.a) + mask.rgb, 1.0);\n}\n";
    private Program mProgram;
    private int mTarget = 0;

    @GenerateFieldPort(hasDefault=true, name="tile_size")
    private int mTileSize = 640;

    public BitmapOverlayFilter(String paramString)
    {
        super(paramString);
    }

    private Frame createBitmapFrame(FilterContext paramFilterContext)
    {
        MutableFrameFormat localMutableFrameFormat = ImageFormat.create(this.mBitmap.getWidth(), this.mBitmap.getHeight(), 3, 3);
        Frame localFrame = paramFilterContext.getFrameManager().newFrame(localMutableFrameFormat);
        localFrame.setBitmap(this.mBitmap);
        this.mBitmap.recycle();
        this.mBitmap = null;
        return localFrame;
    }

    public FrameFormat getOutputFormat(String paramString, FrameFormat paramFrameFormat)
    {
        return paramFrameFormat;
    }

    public void initProgram(FilterContext paramFilterContext, int paramInt)
    {
        switch (paramInt)
        {
        default:
            throw new RuntimeException("Filter FisheyeFilter does not support frames of target " + paramInt + "!");
        case 3:
        }
        ShaderProgram localShaderProgram = new ShaderProgram(paramFilterContext, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform sampler2D tex_sampler_1;\nvarying vec2 v_texcoord;\nvoid main() {\n    vec4 original = texture2D(tex_sampler_0, v_texcoord);\n    vec4 mask = texture2D(tex_sampler_1, v_texcoord);\n    gl_FragColor = vec4(original.rgb * (1.0 - mask.a) + mask.rgb, 1.0);\n}\n");
        localShaderProgram.setMaximumTileSize(this.mTileSize);
        this.mProgram = localShaderProgram;
        this.mTarget = paramInt;
    }

    public void process(FilterContext paramFilterContext)
    {
        Frame localFrame1 = pullInput("image");
        FrameFormat localFrameFormat = localFrame1.getFormat();
        Frame localFrame2 = paramFilterContext.getFrameManager().newFrame(localFrameFormat);
        if ((this.mProgram == null) || (localFrameFormat.getTarget() != this.mTarget))
            initProgram(paramFilterContext, localFrameFormat.getTarget());
        if (this.mBitmap != null)
        {
            Frame localFrame3 = createBitmapFrame(paramFilterContext);
            Frame[] arrayOfFrame = new Frame[2];
            arrayOfFrame[0] = localFrame1;
            arrayOfFrame[1] = localFrame3;
            this.mProgram.process(arrayOfFrame, localFrame2);
            localFrame3.release();
        }
        while (true)
        {
            pushOutput("image", localFrame2);
            localFrame2.release();
            return;
            localFrame2.setDataFromFrame(localFrame1);
        }
    }

    public void setupPorts()
    {
        addMaskedInputPort("image", ImageFormat.create(3));
        addOutputBasedOnInput("image", "image");
    }

    public void tearDown(FilterContext paramFilterContext)
    {
        if (this.mFrame != null)
        {
            this.mFrame.release();
            this.mFrame = null;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterpacks.imageproc.BitmapOverlayFilter
 * JD-Core Version:        0.6.2
 */