package android.filterpacks.imageproc;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterContext;
import android.filterfw.core.Frame;
import android.filterfw.core.FrameFormat;
import android.filterfw.core.FrameManager;
import android.filterfw.core.GenerateFieldPort;
import android.filterfw.core.MutableFrameFormat;
import android.filterfw.core.Program;
import android.filterfw.core.ShaderProgram;
import android.filterfw.format.ImageFormat;
import android.filterfw.geometry.Point;
import android.filterfw.geometry.Quad;

public class RotateFilter extends Filter
{

    @GenerateFieldPort(name="angle")
    private int mAngle;
    private int mHeight = 0;
    private int mOutputHeight;
    private int mOutputWidth;
    private Program mProgram;
    private int mTarget = 0;

    @GenerateFieldPort(hasDefault=true, name="tile_size")
    private int mTileSize = 640;
    private int mWidth = 0;

    public RotateFilter(String paramString)
    {
        super(paramString);
    }

    private void updateParameters()
    {
        if (this.mAngle % 90 == 0)
        {
            if (this.mAngle % 180 == 0)
            {
                f2 = 0.0F;
                if (this.mAngle % 360 == 0);
                for (f1 = 1.0F; ; f1 = -1.0F)
                {
                    Quad localQuad = new Quad(new Point(0.5F * (1.0F + (f2 + -f1)), 0.5F * (1.0F + (-f2 - f1))), new Point(0.5F * (1.0F + (f1 + f2)), 0.5F * (1.0F + (f2 - f1))), new Point(0.5F * (1.0F + (-f1 - f2)), 0.5F * (1.0F + (f1 + -f2))), new Point(0.5F * (1.0F + (f1 - f2)), 0.5F * (1.0F + (f2 + f1))));
                    ((ShaderProgram)this.mProgram).setTargetRegion(localQuad);
                    return;
                }
            }
            float f1 = 0.0F;
            if ((90 + this.mAngle) % 360 == 0);
            for (float f2 = -1.0F; ; f2 = 1.0F)
            {
                this.mOutputWidth = this.mHeight;
                this.mOutputHeight = this.mWidth;
                break;
            }
        }
        throw new RuntimeException("degree has to be multiply of 90.");
    }

    public void fieldPortValueUpdated(String paramString, FilterContext paramFilterContext)
    {
        if (this.mProgram != null)
            updateParameters();
    }

    public void initProgram(FilterContext paramFilterContext, int paramInt)
    {
        switch (paramInt)
        {
        default:
            throw new RuntimeException("Filter Sharpen does not support frames of target " + paramInt + "!");
        case 3:
        }
        ShaderProgram localShaderProgram = ShaderProgram.createIdentity(paramFilterContext);
        localShaderProgram.setMaximumTileSize(this.mTileSize);
        localShaderProgram.setClearsOutput(true);
        this.mProgram = localShaderProgram;
        this.mTarget = paramInt;
    }

    public void process(FilterContext paramFilterContext)
    {
        Frame localFrame1 = pullInput("image");
        FrameFormat localFrameFormat = localFrame1.getFormat();
        if ((this.mProgram == null) || (localFrameFormat.getTarget() != this.mTarget))
            initProgram(paramFilterContext, localFrameFormat.getTarget());
        if ((localFrameFormat.getWidth() != this.mWidth) || (localFrameFormat.getHeight() != this.mHeight))
        {
            this.mWidth = localFrameFormat.getWidth();
            this.mHeight = localFrameFormat.getHeight();
            this.mOutputWidth = this.mWidth;
            this.mOutputHeight = this.mHeight;
            updateParameters();
        }
        MutableFrameFormat localMutableFrameFormat = ImageFormat.create(this.mOutputWidth, this.mOutputHeight, 3, 3);
        Frame localFrame2 = paramFilterContext.getFrameManager().newFrame(localMutableFrameFormat);
        this.mProgram.process(localFrame1, localFrame2);
        pushOutput("image", localFrame2);
        localFrame2.release();
    }

    public void setupPorts()
    {
        addMaskedInputPort("image", ImageFormat.create(3));
        addOutputBasedOnInput("image", "image");
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterpacks.imageproc.RotateFilter
 * JD-Core Version:        0.6.2
 */