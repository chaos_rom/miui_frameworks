package android.filterpacks.imageproc;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterContext;
import android.filterfw.core.Frame;
import android.filterfw.core.FrameFormat;
import android.filterfw.core.FrameManager;
import android.filterfw.core.GenerateFieldPort;
import android.filterfw.core.Program;
import android.filterfw.core.ShaderProgram;
import android.filterfw.format.ImageFormat;
import android.filterfw.geometry.Point;
import android.filterfw.geometry.Quad;

public class StraightenFilter extends Filter
{
    private static final float DEGREE_TO_RADIAN = 0.01745329F;

    @GenerateFieldPort(hasDefault=true, name="angle")
    private float mAngle = 0.0F;
    private int mHeight = 0;

    @GenerateFieldPort(hasDefault=true, name="maxAngle")
    private float mMaxAngle = 45.0F;
    private Program mProgram;
    private int mTarget = 0;

    @GenerateFieldPort(hasDefault=true, name="tile_size")
    private int mTileSize = 640;
    private int mWidth = 0;

    public StraightenFilter(String paramString)
    {
        super(paramString);
    }

    private void updateParameters()
    {
        float f1 = 90.0F;
        float f2 = (float)Math.cos(0.01745329F * this.mAngle);
        float f3 = (float)Math.sin(0.01745329F * this.mAngle);
        if (this.mMaxAngle <= 0.0F)
            throw new RuntimeException("Max angle is out of range (0-180).");
        if (this.mMaxAngle > f1);
        while (true)
        {
            this.mMaxAngle = f1;
            Point localPoint1 = new Point(-f2 * this.mWidth + f3 * this.mHeight, -f3 * this.mWidth - f2 * this.mHeight);
            Point localPoint2 = new Point(f2 * this.mWidth + f3 * this.mHeight, f3 * this.mWidth - f2 * this.mHeight);
            Point localPoint3 = new Point(-f2 * this.mWidth - f3 * this.mHeight, -f3 * this.mWidth + f2 * this.mHeight);
            Point localPoint4 = new Point(f2 * this.mWidth - f3 * this.mHeight, f3 * this.mWidth + f2 * this.mHeight);
            float f4 = Math.max(Math.abs(localPoint1.x), Math.abs(localPoint2.x));
            float f5 = Math.max(Math.abs(localPoint1.y), Math.abs(localPoint2.y));
            float f6 = 0.5F * Math.min(this.mWidth / f4, this.mHeight / f5);
            localPoint1.set(0.5F + f6 * localPoint1.x / this.mWidth, 0.5F + f6 * localPoint1.y / this.mHeight);
            localPoint2.set(0.5F + f6 * localPoint2.x / this.mWidth, 0.5F + f6 * localPoint2.y / this.mHeight);
            localPoint3.set(0.5F + f6 * localPoint3.x / this.mWidth, 0.5F + f6 * localPoint3.y / this.mHeight);
            localPoint4.set(0.5F + f6 * localPoint4.x / this.mWidth, 0.5F + f6 * localPoint4.y / this.mHeight);
            Quad localQuad = new Quad(localPoint1, localPoint2, localPoint3, localPoint4);
            ((ShaderProgram)this.mProgram).setSourceRegion(localQuad);
            return;
            f1 = this.mMaxAngle;
        }
    }

    public void fieldPortValueUpdated(String paramString, FilterContext paramFilterContext)
    {
        if (this.mProgram != null)
            updateParameters();
    }

    public void initProgram(FilterContext paramFilterContext, int paramInt)
    {
        switch (paramInt)
        {
        default:
            throw new RuntimeException("Filter Sharpen does not support frames of target " + paramInt + "!");
        case 3:
        }
        ShaderProgram localShaderProgram = ShaderProgram.createIdentity(paramFilterContext);
        localShaderProgram.setMaximumTileSize(this.mTileSize);
        this.mProgram = localShaderProgram;
        this.mTarget = paramInt;
    }

    public void process(FilterContext paramFilterContext)
    {
        Frame localFrame1 = pullInput("image");
        FrameFormat localFrameFormat = localFrame1.getFormat();
        if ((this.mProgram == null) || (localFrameFormat.getTarget() != this.mTarget))
            initProgram(paramFilterContext, localFrameFormat.getTarget());
        if ((localFrameFormat.getWidth() != this.mWidth) || (localFrameFormat.getHeight() != this.mHeight))
        {
            this.mWidth = localFrameFormat.getWidth();
            this.mHeight = localFrameFormat.getHeight();
            updateParameters();
        }
        Frame localFrame2 = paramFilterContext.getFrameManager().newFrame(localFrameFormat);
        this.mProgram.process(localFrame1, localFrame2);
        pushOutput("image", localFrame2);
        localFrame2.release();
    }

    public void setupPorts()
    {
        addMaskedInputPort("image", ImageFormat.create(3));
        addOutputBasedOnInput("image", "image");
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterpacks.imageproc.StraightenFilter
 * JD-Core Version:        0.6.2
 */