package android.filterpacks.imageproc;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterContext;
import android.filterfw.core.Frame;
import android.filterfw.core.FrameFormat;
import android.filterfw.core.FrameManager;
import android.filterfw.core.GLFrame;
import android.filterfw.core.GenerateFieldPort;
import android.filterfw.core.MutableFrameFormat;
import android.filterfw.core.Program;
import android.filterfw.core.ShaderProgram;
import android.filterfw.format.ImageFormat;

public class ResizeFilter extends Filter
{

    @GenerateFieldPort(hasDefault=true, name="generateMipMap")
    private boolean mGenerateMipMap = false;
    private int mInputChannels;

    @GenerateFieldPort(hasDefault=true, name="keepAspectRatio")
    private boolean mKeepAspectRatio = false;
    private FrameFormat mLastFormat = null;

    @GenerateFieldPort(name="oheight")
    private int mOHeight;

    @GenerateFieldPort(name="owidth")
    private int mOWidth;
    private MutableFrameFormat mOutputFormat;
    private Program mProgram;

    public ResizeFilter(String paramString)
    {
        super(paramString);
    }

    protected void createProgram(FilterContext paramFilterContext, FrameFormat paramFrameFormat)
    {
        if ((this.mLastFormat != null) && (this.mLastFormat.getTarget() == paramFrameFormat.getTarget()));
        while (true)
        {
            return;
            this.mLastFormat = paramFrameFormat;
            switch (paramFrameFormat.getTarget())
            {
            default:
                throw new RuntimeException("ResizeFilter could not create suitable program!");
            case 2:
                throw new RuntimeException("Native ResizeFilter not implemented yet!");
            case 3:
            }
            this.mProgram = ShaderProgram.createIdentity(paramFilterContext);
        }
    }

    public FrameFormat getOutputFormat(String paramString, FrameFormat paramFrameFormat)
    {
        return paramFrameFormat;
    }

    public void process(FilterContext paramFilterContext)
    {
        Frame localFrame1 = pullInput("image");
        createProgram(paramFilterContext, localFrame1.getFormat());
        MutableFrameFormat localMutableFrameFormat = localFrame1.getFormat().mutableCopy();
        if (this.mKeepAspectRatio)
        {
            FrameFormat localFrameFormat = localFrame1.getFormat();
            this.mOHeight = (this.mOWidth * localFrameFormat.getHeight() / localFrameFormat.getWidth());
        }
        localMutableFrameFormat.setDimensions(this.mOWidth, this.mOHeight);
        Frame localFrame2 = paramFilterContext.getFrameManager().newFrame(localMutableFrameFormat);
        if (this.mGenerateMipMap)
        {
            GLFrame localGLFrame = (GLFrame)paramFilterContext.getFrameManager().newFrame(localFrame1.getFormat());
            localGLFrame.setTextureParameter(10241, 9985);
            localGLFrame.setDataFromFrame(localFrame1);
            localGLFrame.generateMipMap();
            this.mProgram.process(localGLFrame, localFrame2);
            localGLFrame.release();
        }
        while (true)
        {
            pushOutput("image", localFrame2);
            localFrame2.release();
            return;
            this.mProgram.process(localFrame1, localFrame2);
        }
    }

    public void setupPorts()
    {
        addMaskedInputPort("image", ImageFormat.create(3));
        addOutputBasedOnInput("image", "image");
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterpacks.imageproc.ResizeFilter
 * JD-Core Version:        0.6.2
 */