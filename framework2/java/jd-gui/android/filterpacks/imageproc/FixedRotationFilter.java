package android.filterpacks.imageproc;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterContext;
import android.filterfw.core.Frame;
import android.filterfw.core.FrameFormat;
import android.filterfw.core.FrameManager;
import android.filterfw.core.GenerateFieldPort;
import android.filterfw.core.MutableFrameFormat;
import android.filterfw.core.ShaderProgram;
import android.filterfw.format.ImageFormat;
import android.filterfw.geometry.Point;
import android.filterfw.geometry.Quad;

public class FixedRotationFilter extends Filter
{
    private ShaderProgram mProgram = null;

    @GenerateFieldPort(hasDefault=true, name="rotation")
    private int mRotation = 0;

    public FixedRotationFilter(String paramString)
    {
        super(paramString);
    }

    public FrameFormat getOutputFormat(String paramString, FrameFormat paramFrameFormat)
    {
        return paramFrameFormat;
    }

    public void process(FilterContext paramFilterContext)
    {
        Frame localFrame1 = pullInput("image");
        if (this.mRotation == 0)
        {
            pushOutput("image", localFrame1);
            return;
        }
        FrameFormat localFrameFormat = localFrame1.getFormat();
        if (this.mProgram == null)
            this.mProgram = ShaderProgram.createIdentity(paramFilterContext);
        MutableFrameFormat localMutableFrameFormat = localFrameFormat.mutableCopy();
        int i = localFrameFormat.getWidth();
        int j = localFrameFormat.getHeight();
        Point localPoint1 = new Point(0.0F, 0.0F);
        Point localPoint2 = new Point(1.0F, 0.0F);
        Point localPoint3 = new Point(0.0F, 1.0F);
        Point localPoint4 = new Point(1.0F, 1.0F);
        Quad localQuad;
        switch (Math.round(this.mRotation / 90.0F) % 4)
        {
        default:
            localQuad = new Quad(localPoint1, localPoint2, localPoint3, localPoint4);
        case 1:
        case 2:
        case 3:
        }
        while (true)
        {
            Frame localFrame2 = paramFilterContext.getFrameManager().newFrame(localMutableFrameFormat);
            this.mProgram.setSourceRegion(localQuad);
            this.mProgram.process(localFrame1, localFrame2);
            pushOutput("image", localFrame2);
            localFrame2.release();
            break;
            localQuad = new Quad(localPoint3, localPoint1, localPoint4, localPoint2);
            localMutableFrameFormat.setDimensions(j, i);
            continue;
            localQuad = new Quad(localPoint4, localPoint3, localPoint2, localPoint1);
            continue;
            localQuad = new Quad(localPoint2, localPoint4, localPoint1, localPoint3);
            localMutableFrameFormat.setDimensions(j, i);
        }
    }

    public void setupPorts()
    {
        addMaskedInputPort("image", ImageFormat.create(3, 3));
        addOutputBasedOnInput("image", "image");
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterpacks.imageproc.FixedRotationFilter
 * JD-Core Version:        0.6.2
 */