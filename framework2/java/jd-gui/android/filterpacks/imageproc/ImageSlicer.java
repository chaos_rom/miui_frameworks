package android.filterpacks.imageproc;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterContext;
import android.filterfw.core.Frame;
import android.filterfw.core.FrameFormat;
import android.filterfw.core.FrameManager;
import android.filterfw.core.GenerateFieldPort;
import android.filterfw.core.MutableFrameFormat;
import android.filterfw.core.Program;
import android.filterfw.core.ShaderProgram;
import android.filterfw.format.ImageFormat;

public class ImageSlicer extends Filter
{
    private int mInputHeight;
    private int mInputWidth;
    private Frame mOriginalFrame;
    private int mOutputHeight;
    private int mOutputWidth;

    @GenerateFieldPort(name="padSize")
    private int mPadSize;
    private Program mProgram;
    private int mSliceHeight;
    private int mSliceIndex = 0;
    private int mSliceWidth;

    @GenerateFieldPort(name="xSlices")
    private int mXSlices;

    @GenerateFieldPort(name="ySlices")
    private int mYSlices;

    public ImageSlicer(String paramString)
    {
        super(paramString);
    }

    private void calcOutputFormatForInput(Frame paramFrame)
    {
        this.mInputWidth = paramFrame.getFormat().getWidth();
        this.mInputHeight = paramFrame.getFormat().getHeight();
        this.mSliceWidth = ((-1 + (this.mInputWidth + this.mXSlices)) / this.mXSlices);
        this.mSliceHeight = ((-1 + (this.mInputHeight + this.mYSlices)) / this.mYSlices);
        this.mOutputWidth = (this.mSliceWidth + 2 * this.mPadSize);
        this.mOutputHeight = (this.mSliceHeight + 2 * this.mPadSize);
    }

    public FrameFormat getOutputFormat(String paramString, FrameFormat paramFrameFormat)
    {
        return paramFrameFormat;
    }

    public void process(FilterContext paramFilterContext)
    {
        if (this.mSliceIndex == 0)
        {
            this.mOriginalFrame = pullInput("image");
            calcOutputFormatForInput(this.mOriginalFrame);
        }
        MutableFrameFormat localMutableFrameFormat = this.mOriginalFrame.getFormat().mutableCopy();
        localMutableFrameFormat.setDimensions(this.mOutputWidth, this.mOutputHeight);
        Frame localFrame = paramFilterContext.getFrameManager().newFrame(localMutableFrameFormat);
        if (this.mProgram == null)
            this.mProgram = ShaderProgram.createIdentity(paramFilterContext);
        int i = this.mSliceIndex % this.mXSlices;
        int j = this.mSliceIndex / this.mXSlices;
        float f1 = (i * this.mSliceWidth - this.mPadSize) / this.mInputWidth;
        float f2 = (j * this.mSliceHeight - this.mPadSize) / this.mInputHeight;
        ((ShaderProgram)this.mProgram).setSourceRect(f1, f2, this.mOutputWidth / this.mInputWidth, this.mOutputHeight / this.mInputHeight);
        this.mProgram.process(this.mOriginalFrame, localFrame);
        this.mSliceIndex = (1 + this.mSliceIndex);
        if (this.mSliceIndex == this.mXSlices * this.mYSlices)
        {
            this.mSliceIndex = 0;
            this.mOriginalFrame.release();
            setWaitsOnInputPort("image", true);
        }
        while (true)
        {
            pushOutput("image", localFrame);
            localFrame.release();
            return;
            this.mOriginalFrame.retain();
            setWaitsOnInputPort("image", false);
        }
    }

    public void setupPorts()
    {
        addMaskedInputPort("image", ImageFormat.create(3, 3));
        addOutputBasedOnInput("image", "image");
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterpacks.imageproc.ImageSlicer
 * JD-Core Version:        0.6.2
 */