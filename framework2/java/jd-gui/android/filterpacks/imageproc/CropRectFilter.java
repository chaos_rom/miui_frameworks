package android.filterpacks.imageproc;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterContext;
import android.filterfw.core.Frame;
import android.filterfw.core.FrameFormat;
import android.filterfw.core.FrameManager;
import android.filterfw.core.GenerateFieldPort;
import android.filterfw.core.MutableFrameFormat;
import android.filterfw.core.Program;
import android.filterfw.core.ShaderProgram;
import android.filterfw.format.ImageFormat;

public class CropRectFilter extends Filter
{
    private int mHeight = 0;

    @GenerateFieldPort(name="height")
    private int mOutputHeight;

    @GenerateFieldPort(name="width")
    private int mOutputWidth;
    private Program mProgram;
    private int mTarget = 0;

    @GenerateFieldPort(hasDefault=true, name="tile_size")
    private int mTileSize = 640;
    private int mWidth = 0;

    @GenerateFieldPort(name="xorigin")
    private int mXorigin;

    @GenerateFieldPort(name="yorigin")
    private int mYorigin;

    public CropRectFilter(String paramString)
    {
        super(paramString);
    }

    public void fieldPortValueUpdated(String paramString, FilterContext paramFilterContext)
    {
        if (this.mProgram != null)
            updateSourceRect(this.mWidth, this.mHeight);
    }

    public void initProgram(FilterContext paramFilterContext, int paramInt)
    {
        switch (paramInt)
        {
        default:
            throw new RuntimeException("Filter Sharpen does not support frames of target " + paramInt + "!");
        case 3:
        }
        ShaderProgram localShaderProgram = ShaderProgram.createIdentity(paramFilterContext);
        localShaderProgram.setMaximumTileSize(this.mTileSize);
        this.mProgram = localShaderProgram;
        this.mTarget = paramInt;
    }

    public void process(FilterContext paramFilterContext)
    {
        Frame localFrame1 = pullInput("image");
        FrameFormat localFrameFormat = localFrame1.getFormat();
        MutableFrameFormat localMutableFrameFormat = ImageFormat.create(this.mOutputWidth, this.mOutputHeight, 3, 3);
        Frame localFrame2 = paramFilterContext.getFrameManager().newFrame(localMutableFrameFormat);
        if ((this.mProgram == null) || (localFrameFormat.getTarget() != this.mTarget))
            initProgram(paramFilterContext, localFrameFormat.getTarget());
        if ((localFrameFormat.getWidth() != this.mWidth) || (localFrameFormat.getHeight() != this.mHeight))
            updateSourceRect(localFrameFormat.getWidth(), localFrameFormat.getHeight());
        this.mProgram.process(localFrame1, localFrame2);
        pushOutput("image", localFrame2);
        localFrame2.release();
    }

    public void setupPorts()
    {
        addMaskedInputPort("image", ImageFormat.create(3));
        addOutputBasedOnInput("image", "image");
    }

    void updateSourceRect(int paramInt1, int paramInt2)
    {
        this.mWidth = paramInt1;
        this.mHeight = paramInt2;
        ((ShaderProgram)this.mProgram).setSourceRect(this.mXorigin / this.mWidth, this.mYorigin / this.mHeight, this.mOutputWidth / this.mWidth, this.mOutputHeight / this.mHeight);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterpacks.imageproc.CropRectFilter
 * JD-Core Version:        0.6.2
 */