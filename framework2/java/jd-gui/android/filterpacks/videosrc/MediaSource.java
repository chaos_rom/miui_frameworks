package android.filterpacks.videosrc;

import android.content.res.AssetFileDescriptor;
import android.filterfw.core.Filter;
import android.filterfw.core.FilterContext;
import android.filterfw.core.Frame;
import android.filterfw.core.FrameManager;
import android.filterfw.core.GLFrame;
import android.filterfw.core.GenerateFieldPort;
import android.filterfw.core.GenerateFinalPort;
import android.filterfw.core.MutableFrameFormat;
import android.filterfw.core.ShaderProgram;
import android.filterfw.format.ImageFormat;
import android.graphics.SurfaceTexture;
import android.graphics.SurfaceTexture.OnFrameAvailableListener;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnVideoSizeChangedListener;
import android.opengl.Matrix;
import android.util.Log;
import android.view.Surface;
import java.io.IOException;

public class MediaSource extends Filter
{
    private static final int NEWFRAME_TIMEOUT = 100;
    private static final int NEWFRAME_TIMEOUT_REPEAT = 10;
    private static final int PREP_TIMEOUT = 100;
    private static final int PREP_TIMEOUT_REPEAT = 100;
    private static final String TAG = "MediaSource";
    private static final float[] mSourceCoords_0;
    private static final float[] mSourceCoords_180;
    private static final float[] mSourceCoords_270;
    private static final float[] mSourceCoords_90 = arrayOfFloat4;
    private boolean mCompleted;
    private ShaderProgram mFrameExtractor;
    private final String mFrameShader = "#extension GL_OES_EGL_image_external : require\nprecision mediump float;\nuniform samplerExternalOES tex_sampler_0;\nvarying vec2 v_texcoord;\nvoid main() {\n    gl_FragColor = texture2D(tex_sampler_0, v_texcoord);\n}\n";
    private boolean mGotSize;
    private int mHeight;
    private final boolean mLogVerbose = Log.isLoggable("MediaSource", 2);

    @GenerateFieldPort(hasDefault=true, name="loop")
    private boolean mLooping = true;
    private GLFrame mMediaFrame;
    private MediaPlayer mMediaPlayer;
    private boolean mNewFrameAvailable = false;

    @GenerateFieldPort(hasDefault=true, name="orientation")
    private int mOrientation = 0;
    private boolean mOrientationUpdated;
    private MutableFrameFormat mOutputFormat;
    private boolean mPaused;
    private boolean mPlaying;
    private boolean mPrepared;

    @GenerateFieldPort(hasDefault=true, name="sourceIsUrl")
    private boolean mSelectedIsUrl = false;

    @GenerateFieldPort(hasDefault=true, name="sourceAsset")
    private AssetFileDescriptor mSourceAsset = null;

    @GenerateFieldPort(hasDefault=true, name="sourceUrl")
    private String mSourceUrl = "";
    private SurfaceTexture mSurfaceTexture;

    @GenerateFieldPort(hasDefault=true, name="volume")
    private float mVolume = 0.0F;

    @GenerateFinalPort(hasDefault=true, name="waitForNewFrame")
    private boolean mWaitForNewFrame = true;
    private int mWidth;
    private MediaPlayer.OnCompletionListener onCompletionListener = new MediaPlayer.OnCompletionListener()
    {
        public void onCompletion(MediaPlayer paramAnonymousMediaPlayer)
        {
            if (MediaSource.this.mLogVerbose)
                Log.v("MediaSource", "MediaPlayer has completed playback");
            synchronized (MediaSource.this)
            {
                MediaSource.access$702(MediaSource.this, true);
                return;
            }
        }
    };
    private SurfaceTexture.OnFrameAvailableListener onMediaFrameAvailableListener = new SurfaceTexture.OnFrameAvailableListener()
    {
        public void onFrameAvailable(SurfaceTexture paramAnonymousSurfaceTexture)
        {
            if (MediaSource.this.mLogVerbose)
                Log.v("MediaSource", "New frame from media player");
            synchronized (MediaSource.this)
            {
                if (MediaSource.this.mLogVerbose)
                    Log.v("MediaSource", "New frame: notify");
                MediaSource.access$802(MediaSource.this, true);
                MediaSource.this.notify();
                if (MediaSource.this.mLogVerbose)
                    Log.v("MediaSource", "New frame: notify done");
                return;
            }
        }
    };
    private MediaPlayer.OnPreparedListener onPreparedListener = new MediaPlayer.OnPreparedListener()
    {
        public void onPrepared(MediaPlayer paramAnonymousMediaPlayer)
        {
            if (MediaSource.this.mLogVerbose)
                Log.v("MediaSource", "MediaPlayer is prepared");
            synchronized (MediaSource.this)
            {
                MediaSource.access$602(MediaSource.this, true);
                MediaSource.this.notify();
                return;
            }
        }
    };
    private MediaPlayer.OnVideoSizeChangedListener onVideoSizeChangedListener = new MediaPlayer.OnVideoSizeChangedListener()
    {
        public void onVideoSizeChanged(MediaPlayer paramAnonymousMediaPlayer, int paramAnonymousInt1, int paramAnonymousInt2)
        {
            if (MediaSource.this.mLogVerbose)
                Log.v("MediaSource", "MediaPlayer sent dimensions: " + paramAnonymousInt1 + " x " + paramAnonymousInt2);
            if (!MediaSource.this.mGotSize)
                if ((MediaSource.this.mOrientation == 0) || (MediaSource.this.mOrientation == 180))
                    MediaSource.this.mOutputFormat.setDimensions(paramAnonymousInt1, paramAnonymousInt2);
            while (true)
            {
                MediaSource.access$402(MediaSource.this, paramAnonymousInt1);
                MediaSource.access$502(MediaSource.this, paramAnonymousInt2);
                synchronized (MediaSource.this)
                {
                    do
                    {
                        MediaSource.access$102(MediaSource.this, true);
                        MediaSource.this.notify();
                        return;
                        MediaSource.this.mOutputFormat.setDimensions(paramAnonymousInt2, paramAnonymousInt1);
                        break;
                    }
                    while ((MediaSource.this.mOutputFormat.getWidth() == paramAnonymousInt1) && (MediaSource.this.mOutputFormat.getHeight() == paramAnonymousInt2));
                    Log.e("MediaSource", "Multiple video size change events received!");
                }
            }
        }
    };

    static
    {
        float[] arrayOfFloat1 = new float[16];
        arrayOfFloat1[0] = 1.0F;
        arrayOfFloat1[1] = 1.0F;
        arrayOfFloat1[2] = 0.0F;
        arrayOfFloat1[3] = 1.0F;
        arrayOfFloat1[4] = 0.0F;
        arrayOfFloat1[5] = 1.0F;
        arrayOfFloat1[6] = 0.0F;
        arrayOfFloat1[7] = 1.0F;
        arrayOfFloat1[8] = 1.0F;
        arrayOfFloat1[9] = 0.0F;
        arrayOfFloat1[10] = 0.0F;
        arrayOfFloat1[11] = 1.0F;
        arrayOfFloat1[12] = 0.0F;
        arrayOfFloat1[13] = 0.0F;
        arrayOfFloat1[14] = 0.0F;
        arrayOfFloat1[15] = 1.0F;
        mSourceCoords_0 = arrayOfFloat1;
        float[] arrayOfFloat2 = new float[16];
        arrayOfFloat2[0] = 0.0F;
        arrayOfFloat2[1] = 1.0F;
        arrayOfFloat2[2] = 0.0F;
        arrayOfFloat2[3] = 1.0F;
        arrayOfFloat2[4] = 0.0F;
        arrayOfFloat2[5] = 0.0F;
        arrayOfFloat2[6] = 0.0F;
        arrayOfFloat2[7] = 1.0F;
        arrayOfFloat2[8] = 1.0F;
        arrayOfFloat2[9] = 1.0F;
        arrayOfFloat2[10] = 0.0F;
        arrayOfFloat2[11] = 1.0F;
        arrayOfFloat2[12] = 1.0F;
        arrayOfFloat2[13] = 0.0F;
        arrayOfFloat2[14] = 0.0F;
        arrayOfFloat2[15] = 1.0F;
        mSourceCoords_270 = arrayOfFloat2;
        float[] arrayOfFloat3 = new float[16];
        arrayOfFloat3[0] = 0.0F;
        arrayOfFloat3[1] = 0.0F;
        arrayOfFloat3[2] = 0.0F;
        arrayOfFloat3[3] = 1.0F;
        arrayOfFloat3[4] = 1.0F;
        arrayOfFloat3[5] = 0.0F;
        arrayOfFloat3[6] = 0.0F;
        arrayOfFloat3[7] = 1.0F;
        arrayOfFloat3[8] = 0.0F;
        arrayOfFloat3[9] = 1.0F;
        arrayOfFloat3[10] = 0.0F;
        arrayOfFloat3[11] = 1.0F;
        arrayOfFloat3[12] = 1.0F;
        arrayOfFloat3[13] = 1.0F;
        arrayOfFloat3[14] = 0.0F;
        arrayOfFloat3[15] = 1.0F;
        mSourceCoords_180 = arrayOfFloat3;
        float[] arrayOfFloat4 = new float[16];
        arrayOfFloat4[0] = 1.0F;
        arrayOfFloat4[1] = 0.0F;
        arrayOfFloat4[2] = 0.0F;
        arrayOfFloat4[3] = 1.0F;
        arrayOfFloat4[4] = 1.0F;
        arrayOfFloat4[5] = 1.0F;
        arrayOfFloat4[6] = 0.0F;
        arrayOfFloat4[7] = 1.0F;
        arrayOfFloat4[8] = 0.0F;
        arrayOfFloat4[9] = 0.0F;
        arrayOfFloat4[10] = 0.0F;
        arrayOfFloat4[11] = 1.0F;
        arrayOfFloat4[12] = 0.0F;
        arrayOfFloat4[13] = 1.0F;
        arrayOfFloat4[14] = 0.0F;
        arrayOfFloat4[15] = 1.0F;
    }

    public MediaSource(String paramString)
    {
        super(paramString);
    }

    private void createFormats()
    {
        this.mOutputFormat = ImageFormat.create(3, 3);
    }

    /** @deprecated */
    private boolean setupMediaPlayer(boolean paramBoolean)
    {
        while (true)
        {
            try
            {
                this.mPrepared = false;
                this.mGotSize = false;
                this.mPlaying = false;
                this.mPaused = false;
                this.mCompleted = false;
                this.mNewFrameAvailable = false;
                if (this.mLogVerbose)
                    Log.v("MediaSource", "Setting up playback.");
                if (this.mMediaPlayer != null)
                {
                    if (this.mLogVerbose)
                        Log.v("MediaSource", "Resetting existing MediaPlayer.");
                    this.mMediaPlayer.reset();
                    if (this.mMediaPlayer != null)
                        break;
                    throw new RuntimeException("Unable to create a MediaPlayer!");
                }
            }
            finally
            {
            }
            if (this.mLogVerbose)
                Log.v("MediaSource", "Creating new MediaPlayer.");
            this.mMediaPlayer = new MediaPlayer();
        }
        if (paramBoolean);
        try
        {
            if (this.mLogVerbose)
                Log.v("MediaSource", "Setting MediaPlayer source to URI " + this.mSourceUrl);
            this.mMediaPlayer.setDataSource(this.mSourceUrl);
            while (true)
            {
                this.mMediaPlayer.setLooping(this.mLooping);
                this.mMediaPlayer.setVolume(this.mVolume, this.mVolume);
                Surface localSurface = new Surface(this.mSurfaceTexture);
                this.mMediaPlayer.setSurface(localSurface);
                localSurface.release();
                this.mMediaPlayer.setOnVideoSizeChangedListener(this.onVideoSizeChangedListener);
                this.mMediaPlayer.setOnPreparedListener(this.onPreparedListener);
                this.mMediaPlayer.setOnCompletionListener(this.onCompletionListener);
                this.mSurfaceTexture.setOnFrameAvailableListener(this.onMediaFrameAvailableListener);
                if (this.mLogVerbose)
                    Log.v("MediaSource", "Preparing MediaPlayer.");
                this.mMediaPlayer.prepareAsync();
                return true;
                if (this.mLogVerbose)
                    Log.v("MediaSource", "Setting MediaPlayer source to asset " + this.mSourceAsset);
                this.mMediaPlayer.setDataSource(this.mSourceAsset.getFileDescriptor(), this.mSourceAsset.getStartOffset(), this.mSourceAsset.getLength());
            }
        }
        catch (IOException localIOException)
        {
            this.mMediaPlayer.release();
            this.mMediaPlayer = null;
            if (paramBoolean)
            {
                Object[] arrayOfObject4 = new Object[1];
                arrayOfObject4[0] = this.mSourceUrl;
                throw new RuntimeException(String.format("Unable to set MediaPlayer to URL %s!", arrayOfObject4), localIOException);
            }
            Object[] arrayOfObject3 = new Object[1];
            arrayOfObject3[0] = this.mSourceAsset;
            throw new RuntimeException(String.format("Unable to set MediaPlayer to asset %s!", arrayOfObject3), localIOException);
        }
        catch (IllegalArgumentException localIllegalArgumentException)
        {
            this.mMediaPlayer.release();
            this.mMediaPlayer = null;
            if (paramBoolean)
            {
                Object[] arrayOfObject2 = new Object[1];
                arrayOfObject2[0] = this.mSourceUrl;
                throw new RuntimeException(String.format("Unable to set MediaPlayer to URL %s!", arrayOfObject2), localIllegalArgumentException);
            }
            Object[] arrayOfObject1 = new Object[1];
            arrayOfObject1[0] = this.mSourceAsset;
            throw new RuntimeException(String.format("Unable to set MediaPlayer to asset %s!", arrayOfObject1), localIllegalArgumentException);
        }
    }

    public void close(FilterContext paramFilterContext)
    {
        if (this.mMediaPlayer.isPlaying())
            this.mMediaPlayer.stop();
        this.mPrepared = false;
        this.mGotSize = false;
        this.mPlaying = false;
        this.mPaused = false;
        this.mCompleted = false;
        this.mNewFrameAvailable = false;
        this.mMediaPlayer.release();
        this.mMediaPlayer = null;
        this.mSurfaceTexture.release();
        this.mSurfaceTexture = null;
        if (this.mLogVerbose)
            Log.v("MediaSource", "MediaSource closed");
    }

    public void fieldPortValueUpdated(String paramString, FilterContext paramFilterContext)
    {
        if (this.mLogVerbose)
            Log.v("MediaSource", "Parameter update");
        if (paramString.equals("sourceUrl"))
            if (isOpen())
            {
                if (this.mLogVerbose)
                    Log.v("MediaSource", "Opening new source URL");
                if (this.mSelectedIsUrl)
                    setupMediaPlayer(this.mSelectedIsUrl);
            }
        do
            while (true)
            {
                return;
                if (paramString.equals("sourceAsset"))
                {
                    if (isOpen())
                    {
                        if (this.mLogVerbose)
                            Log.v("MediaSource", "Opening new source FD");
                        if (!this.mSelectedIsUrl)
                            setupMediaPlayer(this.mSelectedIsUrl);
                    }
                }
                else if (paramString.equals("loop"))
                {
                    if (isOpen())
                        this.mMediaPlayer.setLooping(this.mLooping);
                }
                else if (paramString.equals("sourceIsUrl"))
                {
                    if (isOpen())
                    {
                        if (this.mSelectedIsUrl)
                            if (this.mLogVerbose)
                                Log.v("MediaSource", "Opening new source URL");
                        while (true)
                        {
                            setupMediaPlayer(this.mSelectedIsUrl);
                            break;
                            if (this.mLogVerbose)
                                Log.v("MediaSource", "Opening new source Asset");
                        }
                    }
                }
                else
                {
                    if (!paramString.equals("volume"))
                        break;
                    if (isOpen())
                        this.mMediaPlayer.setVolume(this.mVolume, this.mVolume);
                }
            }
        while ((!paramString.equals("orientation")) || (!this.mGotSize));
        if ((this.mOrientation == 0) || (this.mOrientation == 180))
            this.mOutputFormat.setDimensions(this.mWidth, this.mHeight);
        while (true)
        {
            this.mOrientationUpdated = true;
            break;
            this.mOutputFormat.setDimensions(this.mHeight, this.mWidth);
        }
    }

    public void open(FilterContext paramFilterContext)
    {
        if (this.mLogVerbose)
        {
            Log.v("MediaSource", "Opening MediaSource");
            if (!this.mSelectedIsUrl)
                break label113;
            Log.v("MediaSource", "Current URL is " + this.mSourceUrl);
        }
        while (true)
        {
            this.mMediaFrame = ((GLFrame)paramFilterContext.getFrameManager().newBoundFrame(this.mOutputFormat, 104, 0L));
            this.mSurfaceTexture = new SurfaceTexture(this.mMediaFrame.getTextureId());
            if (setupMediaPlayer(this.mSelectedIsUrl))
                break;
            throw new RuntimeException("Error setting up MediaPlayer!");
            label113: Log.v("MediaSource", "Current source is Asset!");
        }
    }

    /** @deprecated */
    public void pauseVideo(boolean paramBoolean)
    {
        try
        {
            if (isOpen())
            {
                if ((!paramBoolean) || (this.mPaused))
                    break label35;
                this.mMediaPlayer.pause();
            }
            while (true)
            {
                this.mPaused = paramBoolean;
                return;
                label35: if ((!paramBoolean) && (this.mPaused))
                    this.mMediaPlayer.start();
            }
        }
        finally
        {
        }
    }

    protected void prepare(FilterContext paramFilterContext)
    {
        if (this.mLogVerbose)
            Log.v("MediaSource", "Preparing MediaSource");
        this.mFrameExtractor = new ShaderProgram(paramFilterContext, "#extension GL_OES_EGL_image_external : require\nprecision mediump float;\nuniform samplerExternalOES tex_sampler_0;\nvarying vec2 v_texcoord;\nvoid main() {\n    gl_FragColor = texture2D(tex_sampler_0, v_texcoord);\n}\n");
        this.mFrameExtractor.setSourceRect(0.0F, 1.0F, 1.0F, -1.0F);
        createFormats();
    }

    public void process(FilterContext paramFilterContext)
    {
        if (this.mLogVerbose)
            Log.v("MediaSource", "Processing new frame");
        if (this.mMediaPlayer == null)
            throw new NullPointerException("Unexpected null media player!");
        if (this.mCompleted)
            closeOutputPort("video");
        while (true)
        {
            return;
            int j;
            if (!this.mPlaying)
            {
                j = 0;
                if (this.mLogVerbose)
                    Log.v("MediaSource", "Waiting for preparation to complete");
                label75: if ((this.mGotSize) && (this.mPrepared));
            }
            try
            {
                wait(100L);
                label96: if (this.mCompleted)
                {
                    closeOutputPort("video");
                    continue;
                }
                j++;
                if (j != 100)
                    break label75;
                this.mMediaPlayer.release();
                throw new RuntimeException("MediaPlayer timed out while preparing!");
                if (this.mLogVerbose)
                    Log.v("MediaSource", "Starting playback");
                this.mMediaPlayer.start();
                if ((!this.mPaused) || (!this.mPlaying))
                {
                    if (this.mWaitForNewFrame)
                    {
                        if (this.mLogVerbose)
                            Log.v("MediaSource", "Waiting for new frame");
                        int i = 0;
                        while (true)
                            if (!this.mNewFrameAvailable)
                            {
                                if (i == 10)
                                {
                                    if (this.mCompleted)
                                    {
                                        closeOutputPort("video");
                                        break;
                                    }
                                    throw new RuntimeException("Timeout waiting for new frame!");
                                }
                                try
                                {
                                    wait(100L);
                                    i++;
                                }
                                catch (InterruptedException localInterruptedException1)
                                {
                                    while (true)
                                        if (this.mLogVerbose)
                                            Log.v("MediaSource", "interrupted");
                                }
                            }
                        this.mNewFrameAvailable = false;
                        if (this.mLogVerbose)
                            Log.v("MediaSource", "Got new frame");
                    }
                    this.mSurfaceTexture.updateTexImage();
                    this.mOrientationUpdated = true;
                }
                float[] arrayOfFloat1;
                float[] arrayOfFloat2;
                if (this.mOrientationUpdated)
                {
                    arrayOfFloat1 = new float[16];
                    this.mSurfaceTexture.getTransformMatrix(arrayOfFloat1);
                    arrayOfFloat2 = new float[16];
                    switch (this.mOrientation)
                    {
                    default:
                        Matrix.multiplyMM(arrayOfFloat2, 0, arrayOfFloat1, 0, mSourceCoords_0, 0);
                    case 90:
                    case 180:
                    case 270:
                    }
                }
                while (true)
                {
                    if (this.mLogVerbose)
                    {
                        Log.v("MediaSource", "OrientationHint = " + this.mOrientation);
                        Object[] arrayOfObject = new Object[8];
                        arrayOfObject[0] = Float.valueOf(arrayOfFloat2[4]);
                        arrayOfObject[1] = Float.valueOf(arrayOfFloat2[5]);
                        arrayOfObject[2] = Float.valueOf(arrayOfFloat2[0]);
                        arrayOfObject[3] = Float.valueOf(arrayOfFloat2[1]);
                        arrayOfObject[4] = Float.valueOf(arrayOfFloat2[12]);
                        arrayOfObject[5] = Float.valueOf(arrayOfFloat2[13]);
                        arrayOfObject[6] = Float.valueOf(arrayOfFloat2[8]);
                        arrayOfObject[7] = Float.valueOf(arrayOfFloat2[9]);
                        Log.v("MediaSource", String.format("SetSourceRegion: %.2f, %.2f, %.2f, %.2f, %.2f, %.2f, %.2f, %.2f", arrayOfObject));
                    }
                    this.mFrameExtractor.setSourceRegion(arrayOfFloat2[4], arrayOfFloat2[5], arrayOfFloat2[0], arrayOfFloat2[1], arrayOfFloat2[12], arrayOfFloat2[13], arrayOfFloat2[8], arrayOfFloat2[9]);
                    this.mOrientationUpdated = false;
                    Frame localFrame = paramFilterContext.getFrameManager().newFrame(this.mOutputFormat);
                    this.mFrameExtractor.process(this.mMediaFrame, localFrame);
                    long l = this.mSurfaceTexture.getTimestamp();
                    if (this.mLogVerbose)
                        Log.v("MediaSource", "Timestamp: " + l / 1000000000.0D + " s");
                    localFrame.setTimestamp(l);
                    pushOutput("video", localFrame);
                    localFrame.release();
                    this.mPlaying = true;
                    break;
                    Matrix.multiplyMM(arrayOfFloat2, 0, arrayOfFloat1, 0, mSourceCoords_90, 0);
                    continue;
                    Matrix.multiplyMM(arrayOfFloat2, 0, arrayOfFloat1, 0, mSourceCoords_180, 0);
                    continue;
                    Matrix.multiplyMM(arrayOfFloat2, 0, arrayOfFloat1, 0, mSourceCoords_270, 0);
                }
            }
            catch (InterruptedException localInterruptedException2)
            {
                break label96;
            }
        }
    }

    public void setupPorts()
    {
        addOutputPort("video", ImageFormat.create(3, 3));
    }

    public void tearDown(FilterContext paramFilterContext)
    {
        if (this.mMediaFrame != null)
            this.mMediaFrame.release();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterpacks.videosrc.MediaSource
 * JD-Core Version:        0.6.2
 */