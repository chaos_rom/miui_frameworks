package android.filterpacks.videosrc;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterContext;
import android.filterfw.core.Frame;
import android.filterfw.core.FrameFormat;
import android.filterfw.core.FrameManager;
import android.filterfw.core.GLEnvironment;
import android.filterfw.core.GLFrame;
import android.filterfw.core.GenerateFieldPort;
import android.filterfw.core.GenerateFinalPort;
import android.filterfw.core.MutableFrameFormat;
import android.filterfw.core.ShaderProgram;
import android.filterfw.format.ImageFormat;
import android.filterfw.geometry.Point;
import android.filterfw.geometry.Quad;
import android.graphics.SurfaceTexture;
import android.util.Log;

public class SurfaceTextureTarget extends Filter
{
    private static final String TAG = "SurfaceTextureTarget";
    private final int RENDERMODE_CUSTOMIZE = 3;
    private final int RENDERMODE_FILL_CROP = 2;
    private final int RENDERMODE_FIT = 1;
    private final int RENDERMODE_STRETCH = 0;
    private float mAspectRatio = 1.0F;
    private boolean mLogVerbose = Log.isLoggable("SurfaceTextureTarget", 2);
    private ShaderProgram mProgram;
    private int mRenderMode = 1;

    @GenerateFieldPort(hasDefault=true, name="renderMode")
    private String mRenderModeString;
    private GLFrame mScreen;

    @GenerateFinalPort(name="height")
    private int mScreenHeight;

    @GenerateFinalPort(name="width")
    private int mScreenWidth;

    @GenerateFieldPort(hasDefault=true, name="sourceQuad")
    private Quad mSourceQuad = new Quad(new Point(0.0F, 1.0F), new Point(1.0F, 1.0F), new Point(0.0F, 0.0F), new Point(1.0F, 0.0F));
    private int mSurfaceId;

    @GenerateFinalPort(name="surfaceTexture")
    private SurfaceTexture mSurfaceTexture;

    @GenerateFieldPort(hasDefault=true, name="targetQuad")
    private Quad mTargetQuad = new Quad(new Point(0.0F, 0.0F), new Point(1.0F, 0.0F), new Point(0.0F, 1.0F), new Point(1.0F, 1.0F));

    public SurfaceTextureTarget(String paramString)
    {
        super(paramString);
    }

    private void updateTargetRect()
    {
        if (this.mLogVerbose)
            Log.v("SurfaceTextureTarget", "updateTargetRect. Thread: " + Thread.currentThread());
        float f2;
        if ((this.mScreenWidth > 0) && (this.mScreenHeight > 0) && (this.mProgram != null))
        {
            float f1 = this.mScreenWidth / this.mScreenHeight;
            f2 = f1 / this.mAspectRatio;
            if (this.mLogVerbose)
                Log.v("SurfaceTextureTarget", "UTR. screen w = " + this.mScreenWidth + " x screen h = " + this.mScreenHeight + " Screen AR: " + f1 + ", frame AR: " + this.mAspectRatio + ", relative AR: " + f2);
            if ((f2 == 1.0F) && (this.mRenderMode != 3))
            {
                this.mProgram.setTargetRect(0.0F, 0.0F, 1.0F, 1.0F);
                this.mProgram.setClearsOutput(false);
            }
        }
        else
        {
            return;
        }
        switch (this.mRenderMode)
        {
        default:
        case 0:
        case 1:
        case 2:
        case 3:
        }
        while (true)
        {
            if (this.mLogVerbose)
                Log.v("SurfaceTextureTarget", "UTR. quad: " + this.mTargetQuad);
            this.mProgram.setTargetRegion(this.mTargetQuad);
            break;
            this.mTargetQuad.p0.set(0.0F, 0.0F);
            this.mTargetQuad.p1.set(1.0F, 0.0F);
            this.mTargetQuad.p2.set(0.0F, 1.0F);
            this.mTargetQuad.p3.set(1.0F, 1.0F);
            this.mProgram.setClearsOutput(false);
            continue;
            if (f2 > 1.0F)
            {
                this.mTargetQuad.p0.set(0.5F - 0.5F / f2, 0.0F);
                this.mTargetQuad.p1.set(0.5F + 0.5F / f2, 0.0F);
                this.mTargetQuad.p2.set(0.5F - 0.5F / f2, 1.0F);
                this.mTargetQuad.p3.set(0.5F + 0.5F / f2, 1.0F);
            }
            while (true)
            {
                this.mProgram.setClearsOutput(true);
                break;
                this.mTargetQuad.p0.set(0.0F, 0.5F - 0.5F * f2);
                this.mTargetQuad.p1.set(1.0F, 0.5F - 0.5F * f2);
                this.mTargetQuad.p2.set(0.0F, 0.5F + 0.5F * f2);
                this.mTargetQuad.p3.set(1.0F, 0.5F + 0.5F * f2);
            }
            if (f2 > 1.0F)
            {
                this.mTargetQuad.p0.set(0.0F, 0.5F - 0.5F * f2);
                this.mTargetQuad.p1.set(1.0F, 0.5F - 0.5F * f2);
                this.mTargetQuad.p2.set(0.0F, 0.5F + 0.5F * f2);
                this.mTargetQuad.p3.set(1.0F, 0.5F + 0.5F * f2);
            }
            while (true)
            {
                this.mProgram.setClearsOutput(true);
                break;
                this.mTargetQuad.p0.set(0.5F - 0.5F / f2, 0.0F);
                this.mTargetQuad.p1.set(0.5F + 0.5F / f2, 0.0F);
                this.mTargetQuad.p2.set(0.5F - 0.5F / f2, 1.0F);
                this.mTargetQuad.p3.set(0.5F + 0.5F / f2, 1.0F);
            }
            this.mProgram.setSourceRegion(this.mSourceQuad);
        }
    }

    /** @deprecated */
    public void close(FilterContext paramFilterContext)
    {
        try
        {
            if (this.mSurfaceId > 0)
            {
                paramFilterContext.getGLEnvironment().unregisterSurfaceId(this.mSurfaceId);
                this.mSurfaceId = -1;
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void disconnect(FilterContext paramFilterContext)
    {
        try
        {
            if (this.mLogVerbose)
                Log.v("SurfaceTextureTarget", "disconnect");
            if (this.mSurfaceTexture == null)
                Log.d("SurfaceTextureTarget", "SurfaceTexture is already null. Nothing to disconnect.");
            while (true)
            {
                return;
                this.mSurfaceTexture = null;
                if (this.mSurfaceId > 0)
                {
                    paramFilterContext.getGLEnvironment().unregisterSurfaceId(this.mSurfaceId);
                    this.mSurfaceId = -1;
                }
            }
        }
        finally
        {
        }
    }

    public void fieldPortValueUpdated(String paramString, FilterContext paramFilterContext)
    {
        if (this.mLogVerbose)
            Log.v("SurfaceTextureTarget", "FPVU. Thread: " + Thread.currentThread());
        updateRenderMode();
    }

    /** @deprecated */
    public void open(FilterContext paramFilterContext)
    {
        try
        {
            if (this.mSurfaceTexture == null)
            {
                Log.e("SurfaceTextureTarget", "SurfaceTexture is null!!");
                throw new RuntimeException("Could not register SurfaceTexture: " + this.mSurfaceTexture);
            }
        }
        finally
        {
        }
        this.mSurfaceId = paramFilterContext.getGLEnvironment().registerSurfaceTexture(this.mSurfaceTexture, this.mScreenWidth, this.mScreenHeight);
        if (this.mSurfaceId <= 0)
            throw new RuntimeException("Could not register SurfaceTexture: " + this.mSurfaceTexture);
    }

    public void prepare(FilterContext paramFilterContext)
    {
        if (this.mLogVerbose)
            Log.v("SurfaceTextureTarget", "Prepare. Thread: " + Thread.currentThread());
        this.mProgram = ShaderProgram.createIdentity(paramFilterContext);
        this.mProgram.setSourceRect(0.0F, 1.0F, 1.0F, -1.0F);
        this.mProgram.setClearColor(0.0F, 0.0F, 0.0F);
        updateRenderMode();
        MutableFrameFormat localMutableFrameFormat = new MutableFrameFormat(2, 3);
        localMutableFrameFormat.setBytesPerSample(4);
        localMutableFrameFormat.setDimensions(this.mScreenWidth, this.mScreenHeight);
        this.mScreen = ((GLFrame)paramFilterContext.getFrameManager().newBoundFrame(localMutableFrameFormat, 101, 0L));
    }

    /** @deprecated */
    public void process(FilterContext paramFilterContext)
    {
        while (true)
        {
            Frame localFrame1;
            try
            {
                int i = this.mSurfaceId;
                if (i <= 0)
                    return;
                GLEnvironment localGLEnvironment = paramFilterContext.getGLEnvironment();
                localFrame1 = pullInput("frame");
                int j = 0;
                float f = localFrame1.getFormat().getWidth() / localFrame1.getFormat().getHeight();
                if (f != this.mAspectRatio)
                {
                    if (this.mLogVerbose)
                        Log.v("SurfaceTextureTarget", "Process. New aspect ratio: " + f + ", previously: " + this.mAspectRatio + ". Thread: " + Thread.currentThread());
                    this.mAspectRatio = f;
                    updateTargetRect();
                }
                if (localFrame1.getFormat().getTarget() != 3)
                {
                    localFrame2 = paramFilterContext.getFrameManager().duplicateFrameToTarget(localFrame1, 3);
                    j = 1;
                    localGLEnvironment.activateSurfaceWithId(this.mSurfaceId);
                    this.mProgram.process(localFrame2, this.mScreen);
                    localGLEnvironment.setSurfaceTimestamp(localFrame1.getTimestamp());
                    localGLEnvironment.swapBuffers();
                    if (j == 0)
                        continue;
                    localFrame2.release();
                    continue;
                }
            }
            finally
            {
            }
            Frame localFrame2 = localFrame1;
        }
    }

    /** @deprecated */
    public void setupPorts()
    {
        try
        {
            if (this.mSurfaceTexture == null)
                throw new RuntimeException("Null SurfaceTexture passed to SurfaceTextureTarget");
        }
        finally
        {
        }
        addMaskedInputPort("frame", ImageFormat.create(3));
    }

    public void tearDown(FilterContext paramFilterContext)
    {
        if (this.mScreen != null)
            this.mScreen.release();
    }

    public void updateRenderMode()
    {
        if (this.mLogVerbose)
            Log.v("SurfaceTextureTarget", "updateRenderMode. Thread: " + Thread.currentThread());
        if (this.mRenderModeString != null)
        {
            if (!this.mRenderModeString.equals("stretch"))
                break label65;
            this.mRenderMode = 0;
        }
        while (true)
        {
            updateTargetRect();
            return;
            label65: if (this.mRenderModeString.equals("fit"))
            {
                this.mRenderMode = 1;
            }
            else if (this.mRenderModeString.equals("fill_crop"))
            {
                this.mRenderMode = 2;
            }
            else
            {
                if (!this.mRenderModeString.equals("customize"))
                    break;
                this.mRenderMode = 3;
            }
        }
        throw new RuntimeException("Unknown render mode '" + this.mRenderModeString + "'!");
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterpacks.videosrc.SurfaceTextureTarget
 * JD-Core Version:        0.6.2
 */