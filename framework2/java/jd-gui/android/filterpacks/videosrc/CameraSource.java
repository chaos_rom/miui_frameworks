package android.filterpacks.videosrc;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterContext;
import android.filterfw.core.Frame;
import android.filterfw.core.FrameManager;
import android.filterfw.core.GLFrame;
import android.filterfw.core.GenerateFieldPort;
import android.filterfw.core.GenerateFinalPort;
import android.filterfw.core.MutableFrameFormat;
import android.filterfw.core.ShaderProgram;
import android.filterfw.format.ImageFormat;
import android.graphics.SurfaceTexture;
import android.graphics.SurfaceTexture.OnFrameAvailableListener;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.Size;
import android.opengl.Matrix;
import android.util.Log;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

public class CameraSource extends Filter
{
    private static final int NEWFRAME_TIMEOUT = 100;
    private static final int NEWFRAME_TIMEOUT_REPEAT = 10;
    private static final String TAG = "CameraSource";
    private static final String mFrameShader = "#extension GL_OES_EGL_image_external : require\nprecision mediump float;\nuniform samplerExternalOES tex_sampler_0;\nvarying vec2 v_texcoord;\nvoid main() {\n    gl_FragColor = texture2D(tex_sampler_0, v_texcoord);\n}\n";
    private static final float[] mSourceCoords = arrayOfFloat;
    private Camera mCamera;
    private GLFrame mCameraFrame;

    @GenerateFieldPort(hasDefault=true, name="id")
    private int mCameraId = 0;
    private Camera.Parameters mCameraParameters;
    private float[] mCameraTransform = new float[16];

    @GenerateFieldPort(hasDefault=true, name="framerate")
    private int mFps = 30;
    private ShaderProgram mFrameExtractor;

    @GenerateFieldPort(hasDefault=true, name="height")
    private int mHeight = 240;
    private final boolean mLogVerbose = Log.isLoggable("CameraSource", 2);
    private float[] mMappedCoords = new float[16];
    private boolean mNewFrameAvailable;
    private MutableFrameFormat mOutputFormat;
    private SurfaceTexture mSurfaceTexture;

    @GenerateFinalPort(hasDefault=true, name="waitForNewFrame")
    private boolean mWaitForNewFrame = true;

    @GenerateFieldPort(hasDefault=true, name="width")
    private int mWidth = 320;
    private SurfaceTexture.OnFrameAvailableListener onCameraFrameAvailableListener = new SurfaceTexture.OnFrameAvailableListener()
    {
        public void onFrameAvailable(SurfaceTexture paramAnonymousSurfaceTexture)
        {
            if (CameraSource.this.mLogVerbose)
                Log.v("CameraSource", "New frame from camera");
            synchronized (CameraSource.this)
            {
                CameraSource.access$102(CameraSource.this, true);
                CameraSource.this.notify();
                return;
            }
        }
    };

    static
    {
        float[] arrayOfFloat = new float[16];
        arrayOfFloat[0] = 0.0F;
        arrayOfFloat[1] = 1.0F;
        arrayOfFloat[2] = 0.0F;
        arrayOfFloat[3] = 1.0F;
        arrayOfFloat[4] = 1.0F;
        arrayOfFloat[5] = 1.0F;
        arrayOfFloat[6] = 0.0F;
        arrayOfFloat[7] = 1.0F;
        arrayOfFloat[8] = 0.0F;
        arrayOfFloat[9] = 0.0F;
        arrayOfFloat[10] = 0.0F;
        arrayOfFloat[11] = 1.0F;
        arrayOfFloat[12] = 1.0F;
        arrayOfFloat[13] = 0.0F;
        arrayOfFloat[14] = 0.0F;
        arrayOfFloat[15] = 1.0F;
    }

    public CameraSource(String paramString)
    {
        super(paramString);
    }

    private void createFormats()
    {
        this.mOutputFormat = ImageFormat.create(this.mWidth, this.mHeight, 3, 3);
    }

    private int[] findClosestFpsRange(int paramInt, Camera.Parameters paramParameters)
    {
        List localList = paramParameters.getSupportedPreviewFpsRange();
        Object localObject = (int[])localList.get(0);
        Iterator localIterator = localList.iterator();
        while (localIterator.hasNext())
        {
            int[] arrayOfInt = (int[])localIterator.next();
            if ((arrayOfInt[0] < paramInt * 1000) && (arrayOfInt[1] > paramInt * 1000) && (arrayOfInt[0] > localObject[0]) && (arrayOfInt[1] < localObject[1]))
                localObject = arrayOfInt;
        }
        if (this.mLogVerbose)
            Log.v("CameraSource", "Requested fps: " + paramInt + ".Closest frame rate range: [" + localObject[0] / 1000.0D + "," + localObject[1] / 1000.0D + "]");
        return localObject;
    }

    private int[] findClosestSize(int paramInt1, int paramInt2, Camera.Parameters paramParameters)
    {
        List localList = paramParameters.getSupportedPreviewSizes();
        int i = -1;
        int j = -1;
        int k = ((Camera.Size)localList.get(0)).width;
        int m = ((Camera.Size)localList.get(0)).height;
        Iterator localIterator = localList.iterator();
        while (localIterator.hasNext())
        {
            Camera.Size localSize = (Camera.Size)localIterator.next();
            if ((localSize.width <= paramInt1) && (localSize.height <= paramInt2) && (localSize.width >= i) && (localSize.height >= j))
            {
                i = localSize.width;
                j = localSize.height;
            }
            if ((localSize.width < k) && (localSize.height < m))
            {
                k = localSize.width;
                m = localSize.height;
            }
        }
        if (i == -1)
        {
            i = k;
            j = m;
        }
        if (this.mLogVerbose)
            Log.v("CameraSource", "Requested resolution: (" + paramInt1 + ", " + paramInt2 + "). Closest match: (" + i + ", " + j + ").");
        int[] arrayOfInt = new int[2];
        arrayOfInt[0] = i;
        arrayOfInt[1] = j;
        return arrayOfInt;
    }

    public void close(FilterContext paramFilterContext)
    {
        if (this.mLogVerbose)
            Log.v("CameraSource", "Closing");
        this.mCamera.release();
        this.mCamera = null;
        this.mSurfaceTexture.release();
        this.mSurfaceTexture = null;
    }

    public void fieldPortValueUpdated(String paramString, FilterContext paramFilterContext)
    {
        if (paramString.equals("framerate"))
        {
            getCameraParameters();
            int[] arrayOfInt = findClosestFpsRange(this.mFps, this.mCameraParameters);
            this.mCameraParameters.setPreviewFpsRange(arrayOfInt[0], arrayOfInt[1]);
            this.mCamera.setParameters(this.mCameraParameters);
        }
    }

    /** @deprecated */
    public Camera.Parameters getCameraParameters()
    {
        int i = 0;
        try
        {
            if (this.mCameraParameters == null)
            {
                if (this.mCamera == null)
                {
                    this.mCamera = Camera.open(this.mCameraId);
                    i = 1;
                }
                this.mCameraParameters = this.mCamera.getParameters();
                if (i != 0)
                {
                    this.mCamera.release();
                    this.mCamera = null;
                }
            }
            int[] arrayOfInt1 = findClosestSize(this.mWidth, this.mHeight, this.mCameraParameters);
            this.mWidth = arrayOfInt1[0];
            this.mHeight = arrayOfInt1[1];
            this.mCameraParameters.setPreviewSize(this.mWidth, this.mHeight);
            int[] arrayOfInt2 = findClosestFpsRange(this.mFps, this.mCameraParameters);
            this.mCameraParameters.setPreviewFpsRange(arrayOfInt2[0], arrayOfInt2[1]);
            Camera.Parameters localParameters = this.mCameraParameters;
            return localParameters;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void open(FilterContext paramFilterContext)
    {
        if (this.mLogVerbose)
            Log.v("CameraSource", "Opening");
        this.mCamera = Camera.open(this.mCameraId);
        getCameraParameters();
        this.mCamera.setParameters(this.mCameraParameters);
        createFormats();
        this.mCameraFrame = ((GLFrame)paramFilterContext.getFrameManager().newBoundFrame(this.mOutputFormat, 104, 0L));
        this.mSurfaceTexture = new SurfaceTexture(this.mCameraFrame.getTextureId());
        try
        {
            this.mCamera.setPreviewTexture(this.mSurfaceTexture);
            this.mSurfaceTexture.setOnFrameAvailableListener(this.onCameraFrameAvailableListener);
            this.mNewFrameAvailable = false;
            this.mCamera.startPreview();
            return;
        }
        catch (IOException localIOException)
        {
            throw new RuntimeException("Could not bind camera surface texture: " + localIOException.getMessage() + "!");
        }
    }

    public void prepare(FilterContext paramFilterContext)
    {
        if (this.mLogVerbose)
            Log.v("CameraSource", "Preparing");
        this.mFrameExtractor = new ShaderProgram(paramFilterContext, "#extension GL_OES_EGL_image_external : require\nprecision mediump float;\nuniform samplerExternalOES tex_sampler_0;\nvarying vec2 v_texcoord;\nvoid main() {\n    gl_FragColor = texture2D(tex_sampler_0, v_texcoord);\n}\n");
    }

    public void process(FilterContext paramFilterContext)
    {
        if (this.mLogVerbose)
            Log.v("CameraSource", "Processing new frame");
        if (this.mWaitForNewFrame)
        {
            while (!this.mNewFrameAvailable)
            {
                if (10 == 0)
                    throw new RuntimeException("Timeout waiting for new frame");
                try
                {
                    wait(100L);
                }
                catch (InterruptedException localInterruptedException)
                {
                }
                if (this.mLogVerbose)
                    Log.v("CameraSource", "Interrupted while waiting for new frame");
            }
            this.mNewFrameAvailable = false;
            if (this.mLogVerbose)
                Log.v("CameraSource", "Got new frame");
        }
        this.mSurfaceTexture.updateTexImage();
        if (this.mLogVerbose)
            Log.v("CameraSource", "Using frame extractor in thread: " + Thread.currentThread());
        this.mSurfaceTexture.getTransformMatrix(this.mCameraTransform);
        Matrix.multiplyMM(this.mMappedCoords, 0, this.mCameraTransform, 0, mSourceCoords, 0);
        this.mFrameExtractor.setSourceRegion(this.mMappedCoords[0], this.mMappedCoords[1], this.mMappedCoords[4], this.mMappedCoords[5], this.mMappedCoords[8], this.mMappedCoords[9], this.mMappedCoords[12], this.mMappedCoords[13]);
        Frame localFrame = paramFilterContext.getFrameManager().newFrame(this.mOutputFormat);
        this.mFrameExtractor.process(this.mCameraFrame, localFrame);
        long l = this.mSurfaceTexture.getTimestamp();
        if (this.mLogVerbose)
            Log.v("CameraSource", "Timestamp: " + l / 1000000000.0D + " s");
        localFrame.setTimestamp(l);
        pushOutput("video", localFrame);
        localFrame.release();
        if (this.mLogVerbose)
            Log.v("CameraSource", "Done processing new frame");
    }

    /** @deprecated */
    public void setCameraParameters(Camera.Parameters paramParameters)
    {
        try
        {
            paramParameters.setPreviewSize(this.mWidth, this.mHeight);
            this.mCameraParameters = paramParameters;
            if (isOpen())
                this.mCamera.setParameters(this.mCameraParameters);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void setupPorts()
    {
        addOutputPort("video", ImageFormat.create(3, 3));
    }

    public void tearDown(FilterContext paramFilterContext)
    {
        if (this.mCameraFrame != null)
            this.mCameraFrame.release();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterpacks.videosrc.CameraSource
 * JD-Core Version:        0.6.2
 */