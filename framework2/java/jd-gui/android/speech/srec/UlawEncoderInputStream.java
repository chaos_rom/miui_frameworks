package android.speech.srec;

import java.io.IOException;
import java.io.InputStream;

public final class UlawEncoderInputStream extends InputStream
{
    private static final int MAX_ULAW = 8192;
    private static final int SCALE_BITS = 16;
    private static final String TAG = "UlawEncoderInputStream";
    private final byte[] mBuf = new byte[1024];
    private int mBufCount = 0;
    private InputStream mIn;
    private int mMax = 0;
    private final byte[] mOneByte = new byte[1];

    public UlawEncoderInputStream(InputStream paramInputStream, int paramInt)
    {
        this.mIn = paramInputStream;
        this.mMax = paramInt;
    }

    public static void encode(byte[] paramArrayOfByte1, int paramInt1, byte[] paramArrayOfByte2, int paramInt2, int paramInt3, int paramInt4)
    {
        if (paramInt4 <= 0)
            paramInt4 = 8192;
        int i = 536870912 / paramInt4;
        int j = 0;
        int k = paramInt2;
        int m = paramInt1;
        if (j < paramInt3)
        {
            int n = m + 1;
            int i1 = 0xFF & paramArrayOfByte1[m];
            m = n + 1;
            int i2 = i * (i1 + (paramArrayOfByte1[n] << 8)) >> 16;
            int i3;
            if (i2 >= 0)
            {
                if (i2 <= 0)
                    i3 = 255;
                while (true)
                {
                    int i4 = k + 1;
                    paramArrayOfByte2[k] = ((byte)i3);
                    j++;
                    k = i4;
                    break;
                    if (i2 <= 30)
                        i3 = 240 + (30 - i2 >> 1);
                    else if (i2 <= 94)
                        i3 = 224 + (94 - i2 >> 2);
                    else if (i2 <= 222)
                        i3 = 208 + (222 - i2 >> 3);
                    else if (i2 <= 478)
                        i3 = 192 + (478 - i2 >> 4);
                    else if (i2 <= 990)
                        i3 = 176 + (990 - i2 >> 5);
                    else if (i2 <= 2014)
                        i3 = 160 + (2014 - i2 >> 6);
                    else if (i2 <= 4062)
                        i3 = 144 + (4062 - i2 >> 7);
                    else if (i2 <= 8158)
                        i3 = 128 + (8158 - i2 >> 8);
                    else
                        i3 = 128;
                }
            }
            if (-1 <= i2)
                i3 = 127;
            while (true)
            {
                break;
                if (-31 <= i2)
                    i3 = 112 + (i2 + 31 >> 1);
                else if (-95 <= i2)
                    i3 = 96 + (i2 + 95 >> 2);
                else if (-223 <= i2)
                    i3 = 80 + (i2 + 223 >> 3);
                else if (-479 <= i2)
                    i3 = 64 + (i2 + 479 >> 4);
                else if (-991 <= i2)
                    i3 = 48 + (i2 + 991 >> 5);
                else if (-2015 <= i2)
                    i3 = 32 + (i2 + 2015 >> 6);
                else if (-4063 <= i2)
                    i3 = 16 + (i2 + 4063 >> 7);
                else if (-8159 <= i2)
                    i3 = 0 + (i2 + 8159 >> 8);
                else
                    i3 = 0;
            }
        }
    }

    public static int maxAbsPcm(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    {
        int i = 0;
        int j = 0;
        int k = paramInt1;
        while (j < paramInt2)
        {
            int m = k + 1;
            int n = 0xFF & paramArrayOfByte[k];
            k = m + 1;
            int i1 = n + (paramArrayOfByte[m] << 8);
            if (i1 < 0)
                i1 = -i1;
            if (i1 > i)
                i = i1;
            j++;
        }
        return i;
    }

    public int available()
        throws IOException
    {
        return (this.mIn.available() + this.mBufCount) / 2;
    }

    public void close()
        throws IOException
    {
        if (this.mIn != null)
        {
            InputStream localInputStream = this.mIn;
            this.mIn = null;
            localInputStream.close();
        }
    }

    public int read()
        throws IOException
    {
        int i = -1;
        if (read(this.mOneByte, 0, 1) == i);
        while (true)
        {
            return i;
            i = 0xFF & this.mOneByte[0];
        }
    }

    public int read(byte[] paramArrayOfByte)
        throws IOException
    {
        return read(paramArrayOfByte, 0, paramArrayOfByte.length);
    }

    public int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
        throws IOException
    {
        int i = -1;
        if (this.mIn == null)
            throw new IllegalStateException("not open");
        int m;
        do
        {
            this.mBufCount = (m + this.mBufCount);
            if (this.mBufCount >= 2)
                break;
            m = this.mIn.read(this.mBuf, this.mBufCount, Math.min(paramInt2 * 2, this.mBuf.length - this.mBufCount));
        }
        while (m != i);
        while (true)
        {
            return i;
            int j = Math.min(this.mBufCount / 2, paramInt2);
            encode(this.mBuf, 0, paramArrayOfByte, paramInt1, j, this.mMax);
            this.mBufCount -= j * 2;
            for (int k = 0; k < this.mBufCount; k++)
                this.mBuf[k] = this.mBuf[(k + j * 2)];
            i = j;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.speech.srec.UlawEncoderInputStream
 * JD-Core Version:        0.6.2
 */