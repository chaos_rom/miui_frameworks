package android.speech.srec;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;

public final class Recognizer
{
    public static final int EVENT_END_OF_VOICING = 6;
    public static final int EVENT_INCOMPLETE = 2;
    public static final int EVENT_INVALID = 0;
    public static final int EVENT_MAX_SPEECH = 12;
    public static final int EVENT_NEED_MORE_AUDIO = 11;
    public static final int EVENT_NO_MATCH = 1;
    public static final int EVENT_RECOGNITION_RESULT = 8;
    public static final int EVENT_RECOGNITION_TIMEOUT = 10;
    public static final int EVENT_SPOKE_TOO_SOON = 7;
    public static final int EVENT_STARTED = 3;
    public static final int EVENT_START_OF_UTTERANCE_TIMEOUT = 9;
    public static final int EVENT_START_OF_VOICING = 5;
    public static final int EVENT_STOPPED = 4;
    public static final String KEY_CONFIDENCE = "conf";
    public static final String KEY_LITERAL = "literal";
    public static final String KEY_MEANING = "meaning";
    private static String TAG = "Recognizer";
    private Grammar mActiveGrammar = null;
    private byte[] mPutAudioBuffer = null;
    private int mRecognizer = 0;
    private int mVocabulary = 0;

    static
    {
        System.loadLibrary("srec_jni");
    }

    public Recognizer(String paramString)
        throws IOException
    {
        PMemInit();
        SR_SessionCreate(paramString);
        this.mRecognizer = SR_RecognizerCreate();
        SR_RecognizerSetup(this.mRecognizer);
        this.mVocabulary = SR_VocabularyLoad();
    }

    private static native void PMemInit();

    private static native void PMemShutdown();

    private static native String SR_AcousticStateGet(int paramInt);

    private static native void SR_AcousticStateReset(int paramInt);

    private static native void SR_AcousticStateSet(int paramInt, String paramString);

    private static native void SR_GrammarAddWordToSlot(int paramInt1, String paramString1, String paramString2, String paramString3, int paramInt2, String paramString4);

    private static native void SR_GrammarAllowAll(int paramInt);

    private static native void SR_GrammarAllowOnly(int paramInt, String paramString);

    private static native void SR_GrammarCompile(int paramInt);

    private static native int SR_GrammarCreate();

    private static native void SR_GrammarDestroy(int paramInt);

    private static native int SR_GrammarLoad(String paramString);

    private static native void SR_GrammarResetAllSlots(int paramInt);

    private static native void SR_GrammarSave(int paramInt, String paramString);

    private static native void SR_GrammarSetupRecognizer(int paramInt1, int paramInt2);

    private static native void SR_GrammarSetupVocabulary(int paramInt1, int paramInt2);

    private static native void SR_GrammarUnsetupRecognizer(int paramInt);

    private static native void SR_RecognizerActivateRule(int paramInt1, int paramInt2, String paramString, int paramInt3);

    private static native int SR_RecognizerAdvance(int paramInt);

    private static native boolean SR_RecognizerCheckGrammarConsistency(int paramInt1, int paramInt2);

    private static native int SR_RecognizerCreate();

    private static native void SR_RecognizerDeactivateAllRules(int paramInt);

    private static native void SR_RecognizerDeactivateRule(int paramInt1, int paramInt2, String paramString);

    private static native void SR_RecognizerDestroy(int paramInt);

    private static native boolean SR_RecognizerGetBoolParameter(int paramInt, String paramString);

    private static native String SR_RecognizerGetParameter(int paramInt, String paramString);

    private static native int SR_RecognizerGetSize_tParameter(int paramInt, String paramString);

    private static native boolean SR_RecognizerHasSetupRules(int paramInt);

    private static native boolean SR_RecognizerIsActiveRule(int paramInt1, int paramInt2, String paramString);

    private static native boolean SR_RecognizerIsSetup(int paramInt);

    private static native boolean SR_RecognizerIsSignalClipping(int paramInt);

    private static native boolean SR_RecognizerIsSignalDCOffset(int paramInt);

    private static native boolean SR_RecognizerIsSignalNoisy(int paramInt);

    private static native boolean SR_RecognizerIsSignalTooFewSamples(int paramInt);

    private static native boolean SR_RecognizerIsSignalTooManySamples(int paramInt);

    private static native boolean SR_RecognizerIsSignalTooQuiet(int paramInt);

    private static native int SR_RecognizerPutAudio(int paramInt1, byte[] paramArrayOfByte, int paramInt2, int paramInt3, boolean paramBoolean);

    private static native int SR_RecognizerResultGetKeyCount(int paramInt1, int paramInt2);

    private static native String[] SR_RecognizerResultGetKeyList(int paramInt1, int paramInt2);

    private static native int SR_RecognizerResultGetSize(int paramInt);

    private static native String SR_RecognizerResultGetValue(int paramInt1, int paramInt2, String paramString);

    private static native byte[] SR_RecognizerResultGetWaveform(int paramInt);

    private static native void SR_RecognizerSetBoolParameter(int paramInt, String paramString, boolean paramBoolean);

    private static native void SR_RecognizerSetParameter(int paramInt, String paramString1, String paramString2);

    private static native void SR_RecognizerSetSize_tParameter(int paramInt1, String paramString, int paramInt2);

    private static native void SR_RecognizerSetup(int paramInt);

    private static native void SR_RecognizerSetupRule(int paramInt1, int paramInt2, String paramString);

    private static native void SR_RecognizerStart(int paramInt);

    private static native void SR_RecognizerStop(int paramInt);

    private static native void SR_RecognizerUnsetup(int paramInt);

    private static native void SR_SessionCreate(String paramString);

    private static native void SR_SessionDestroy();

    private static native void SR_VocabularyDestroy(int paramInt);

    private static native String SR_VocabularyGetPronunciation(int paramInt, String paramString);

    private static native int SR_VocabularyLoad();

    public static String eventToString(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        default:
            str = "EVENT_" + paramInt;
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
        case 10:
        case 11:
        case 12:
        }
        while (true)
        {
            return str;
            str = "EVENT_INVALID";
            continue;
            str = "EVENT_NO_MATCH";
            continue;
            str = "EVENT_INCOMPLETE";
            continue;
            str = "EVENT_STARTED";
            continue;
            str = "EVENT_STOPPED";
            continue;
            str = "EVENT_START_OF_VOICING";
            continue;
            str = "EVENT_END_OF_VOICING";
            continue;
            str = "EVENT_SPOKE_TOO_SOON";
            continue;
            str = "EVENT_RECOGNITION_RESULT";
            continue;
            str = "EVENT_START_OF_UTTERANCE_TIMEOUT";
            continue;
            str = "EVENT_RECOGNITION_TIMEOUT";
            continue;
            str = "EVENT_NEED_MORE_AUDIO";
            continue;
            str = "EVENT_MAX_SPEECH";
        }
    }

    public static String getConfigDir(Locale paramLocale)
    {
        if (paramLocale == null)
            paramLocale = Locale.US;
        String str = "/system/usr/srec/config/" + paramLocale.toString().replace('_', '.').toLowerCase();
        if (new File(str).isDirectory());
        while (true)
        {
            return str;
            str = null;
        }
    }

    public int advance()
    {
        return SR_RecognizerAdvance(this.mRecognizer);
    }

    public void destroy()
    {
        try
        {
            if (this.mVocabulary != 0)
                SR_VocabularyDestroy(this.mVocabulary);
            this.mVocabulary = 0;
            try
            {
                if (this.mRecognizer != 0)
                    SR_RecognizerUnsetup(this.mRecognizer);
                try
                {
                    if (this.mRecognizer != 0)
                        SR_RecognizerDestroy(this.mRecognizer);
                    this.mRecognizer = 0;
                    try
                    {
                        SR_SessionDestroy();
                        return;
                    }
                    finally
                    {
                        PMemShutdown();
                    }
                }
                finally
                {
                    this.mRecognizer = 0;
                }
            }
            finally
            {
            }
        }
        finally
        {
            this.mVocabulary = 0;
        }
    }

    protected void finalize()
        throws Throwable
    {
        if ((this.mVocabulary != 0) || (this.mRecognizer != 0))
        {
            destroy();
            throw new IllegalStateException("someone forgot to destroy Recognizer");
        }
    }

    public String getAcousticState()
    {
        return SR_AcousticStateGet(this.mRecognizer);
    }

    public String getResult(int paramInt, String paramString)
    {
        return SR_RecognizerResultGetValue(this.mRecognizer, paramInt, paramString);
    }

    public int getResultCount()
    {
        return SR_RecognizerResultGetSize(this.mRecognizer);
    }

    public String[] getResultKeys(int paramInt)
    {
        return SR_RecognizerResultGetKeyList(this.mRecognizer, paramInt);
    }

    public int putAudio(byte[] paramArrayOfByte, int paramInt1, int paramInt2, boolean paramBoolean)
    {
        return SR_RecognizerPutAudio(this.mRecognizer, paramArrayOfByte, paramInt1, paramInt2, paramBoolean);
    }

    public void putAudio(InputStream paramInputStream)
        throws IOException
    {
        if (this.mPutAudioBuffer == null)
            this.mPutAudioBuffer = new byte[512];
        int i = paramInputStream.read(this.mPutAudioBuffer);
        if (i == -1)
            SR_RecognizerPutAudio(this.mRecognizer, this.mPutAudioBuffer, 0, 0, true);
        while (i == SR_RecognizerPutAudio(this.mRecognizer, this.mPutAudioBuffer, 0, i, false))
            return;
        throw new IOException("SR_RecognizerPutAudio failed nbytes=" + i);
    }

    public void resetAcousticState()
    {
        SR_AcousticStateReset(this.mRecognizer);
    }

    public void setAcousticState(String paramString)
    {
        SR_AcousticStateSet(this.mRecognizer, paramString);
    }

    public void start()
    {
        SR_RecognizerActivateRule(this.mRecognizer, this.mActiveGrammar.mGrammar, "trash", 1);
        SR_RecognizerStart(this.mRecognizer);
    }

    public void stop()
    {
        SR_RecognizerStop(this.mRecognizer);
        SR_RecognizerDeactivateRule(this.mRecognizer, this.mActiveGrammar.mGrammar, "trash");
    }

    public class Grammar
    {
        private int mGrammar = 0;

        public Grammar(String arg2)
            throws IOException
        {
            String str;
            this.mGrammar = Recognizer.SR_GrammarLoad(str);
            Recognizer.SR_GrammarSetupVocabulary(this.mGrammar, Recognizer.this.mVocabulary);
        }

        public void addWordToSlot(String paramString1, String paramString2, String paramString3, int paramInt, String paramString4)
        {
            Recognizer.SR_GrammarAddWordToSlot(this.mGrammar, paramString1, paramString2, paramString3, paramInt, paramString4);
        }

        public void compile()
        {
            Recognizer.SR_GrammarCompile(this.mGrammar);
        }

        public void destroy()
        {
            if (this.mGrammar != 0)
            {
                Recognizer.SR_GrammarDestroy(this.mGrammar);
                this.mGrammar = 0;
            }
        }

        protected void finalize()
        {
            if (this.mGrammar != 0)
            {
                destroy();
                throw new IllegalStateException("someone forgot to destroy Grammar");
            }
        }

        public void resetAllSlots()
        {
            Recognizer.SR_GrammarResetAllSlots(this.mGrammar);
        }

        public void save(String paramString)
            throws IOException
        {
            Recognizer.SR_GrammarSave(this.mGrammar, paramString);
        }

        public void setupRecognizer()
        {
            Recognizer.SR_GrammarSetupRecognizer(this.mGrammar, Recognizer.this.mRecognizer);
            Recognizer.access$802(Recognizer.this, this);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.speech.srec.Recognizer
 * JD-Core Version:        0.6.2
 */