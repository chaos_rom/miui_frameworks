package android.speech.srec;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class WaveHeader
{
    public static final short FORMAT_ALAW = 6;
    public static final short FORMAT_PCM = 1;
    public static final short FORMAT_ULAW = 7;
    private static final int HEADER_LENGTH = 44;
    private static final String TAG = "WaveHeader";
    private short mBitsPerSample;
    private short mFormat;
    private int mNumBytes;
    private short mNumChannels;
    private int mSampleRate;

    public WaveHeader()
    {
    }

    public WaveHeader(short paramShort1, short paramShort2, int paramInt1, short paramShort3, int paramInt2)
    {
        this.mFormat = paramShort1;
        this.mSampleRate = paramInt1;
        this.mNumChannels = paramShort2;
        this.mBitsPerSample = paramShort3;
        this.mNumBytes = paramInt2;
    }

    private static void readId(InputStream paramInputStream, String paramString)
        throws IOException
    {
        for (int i = 0; i < paramString.length(); i++)
            if (paramString.charAt(i) != paramInputStream.read())
                throw new IOException(paramString + " tag not present");
    }

    private static int readInt(InputStream paramInputStream)
        throws IOException
    {
        return paramInputStream.read() | paramInputStream.read() << 8 | paramInputStream.read() << 16 | paramInputStream.read() << 24;
    }

    private static short readShort(InputStream paramInputStream)
        throws IOException
    {
        return (short)(paramInputStream.read() | paramInputStream.read() << 8);
    }

    private static void writeId(OutputStream paramOutputStream, String paramString)
        throws IOException
    {
        for (int i = 0; i < paramString.length(); i++)
            paramOutputStream.write(paramString.charAt(i));
    }

    private static void writeInt(OutputStream paramOutputStream, int paramInt)
        throws IOException
    {
        paramOutputStream.write(paramInt >> 0);
        paramOutputStream.write(paramInt >> 8);
        paramOutputStream.write(paramInt >> 16);
        paramOutputStream.write(paramInt >> 24);
    }

    private static void writeShort(OutputStream paramOutputStream, short paramShort)
        throws IOException
    {
        paramOutputStream.write(paramShort >> 0);
        paramOutputStream.write(paramShort >> 8);
    }

    public short getBitsPerSample()
    {
        return this.mBitsPerSample;
    }

    public short getFormat()
    {
        return this.mFormat;
    }

    public int getNumBytes()
    {
        return this.mNumBytes;
    }

    public short getNumChannels()
    {
        return this.mNumChannels;
    }

    public int getSampleRate()
    {
        return this.mSampleRate;
    }

    public int read(InputStream paramInputStream)
        throws IOException
    {
        readId(paramInputStream, "RIFF");
        (-36 + readInt(paramInputStream));
        readId(paramInputStream, "WAVE");
        readId(paramInputStream, "fmt ");
        if (16 != readInt(paramInputStream))
            throw new IOException("fmt chunk length not 16");
        this.mFormat = readShort(paramInputStream);
        this.mNumChannels = readShort(paramInputStream);
        this.mSampleRate = readInt(paramInputStream);
        int i = readInt(paramInputStream);
        int j = readShort(paramInputStream);
        this.mBitsPerSample = readShort(paramInputStream);
        if (i != this.mNumChannels * this.mSampleRate * this.mBitsPerSample / 8)
            throw new IOException("fmt.ByteRate field inconsistent");
        if (j != this.mNumChannels * this.mBitsPerSample / 8)
            throw new IOException("fmt.BlockAlign field inconsistent");
        readId(paramInputStream, "data");
        this.mNumBytes = readInt(paramInputStream);
        return 44;
    }

    public WaveHeader setBitsPerSample(short paramShort)
    {
        this.mBitsPerSample = paramShort;
        return this;
    }

    public WaveHeader setFormat(short paramShort)
    {
        this.mFormat = paramShort;
        return this;
    }

    public WaveHeader setNumBytes(int paramInt)
    {
        this.mNumBytes = paramInt;
        return this;
    }

    public WaveHeader setNumChannels(short paramShort)
    {
        this.mNumChannels = paramShort;
        return this;
    }

    public WaveHeader setSampleRate(int paramInt)
    {
        this.mSampleRate = paramInt;
        return this;
    }

    public String toString()
    {
        Object[] arrayOfObject = new Object[5];
        arrayOfObject[0] = Short.valueOf(this.mFormat);
        arrayOfObject[1] = Short.valueOf(this.mNumChannels);
        arrayOfObject[2] = Integer.valueOf(this.mSampleRate);
        arrayOfObject[3] = Short.valueOf(this.mBitsPerSample);
        arrayOfObject[4] = Integer.valueOf(this.mNumBytes);
        return String.format("WaveHeader format=%d numChannels=%d sampleRate=%d bitsPerSample=%d numBytes=%d", arrayOfObject);
    }

    public int write(OutputStream paramOutputStream)
        throws IOException
    {
        writeId(paramOutputStream, "RIFF");
        writeInt(paramOutputStream, 36 + this.mNumBytes);
        writeId(paramOutputStream, "WAVE");
        writeId(paramOutputStream, "fmt ");
        writeInt(paramOutputStream, 16);
        writeShort(paramOutputStream, this.mFormat);
        writeShort(paramOutputStream, this.mNumChannels);
        writeInt(paramOutputStream, this.mSampleRate);
        writeInt(paramOutputStream, this.mNumChannels * this.mSampleRate * this.mBitsPerSample / 8);
        writeShort(paramOutputStream, this.mNumChannels * this.mBitsPerSample / 8);
        writeShort(paramOutputStream, this.mBitsPerSample);
        writeId(paramOutputStream, "data");
        writeInt(paramOutputStream, this.mNumBytes);
        return 44;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.speech.srec.WaveHeader
 * JD-Core Version:        0.6.2
 */