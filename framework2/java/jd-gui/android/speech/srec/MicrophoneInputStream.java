package android.speech.srec;

import java.io.IOException;
import java.io.InputStream;

public final class MicrophoneInputStream extends InputStream
{
    private static final String TAG = "MicrophoneInputStream";
    private int mAudioRecord = 0;
    private byte[] mOneByte = new byte[1];

    static
    {
        System.loadLibrary("srec_jni");
    }

    public MicrophoneInputStream(int paramInt1, int paramInt2)
        throws IOException
    {
        this.mAudioRecord = AudioRecordNew(paramInt1, paramInt2);
        if (this.mAudioRecord == 0)
            throw new IOException("AudioRecord constructor failed - busy?");
        int i = AudioRecordStart(this.mAudioRecord);
        if (i != 0)
        {
            close();
            throw new IOException("AudioRecord start failed: " + i);
        }
    }

    private static native void AudioRecordDelete(int paramInt)
        throws IOException;

    private static native int AudioRecordNew(int paramInt1, int paramInt2);

    private static native int AudioRecordRead(int paramInt1, byte[] paramArrayOfByte, int paramInt2, int paramInt3)
        throws IOException;

    private static native int AudioRecordStart(int paramInt);

    private static native void AudioRecordStop(int paramInt)
        throws IOException;

    public void close()
        throws IOException
    {
        if (this.mAudioRecord != 0);
        try
        {
            AudioRecordStop(this.mAudioRecord);
            try
            {
                AudioRecordDelete(this.mAudioRecord);
                return;
            }
            finally
            {
                this.mAudioRecord = 0;
            }
        }
        finally
        {
        }
    }

    protected void finalize()
        throws Throwable
    {
        if (this.mAudioRecord != 0)
        {
            close();
            throw new IOException("someone forgot to close MicrophoneInputStream");
        }
    }

    public int read()
        throws IOException
    {
        if (this.mAudioRecord == 0)
            throw new IllegalStateException("not open");
        if (AudioRecordRead(this.mAudioRecord, this.mOneByte, 0, 1) == 1);
        for (int i = 0xFF & this.mOneByte[0]; ; i = -1)
            return i;
    }

    public int read(byte[] paramArrayOfByte)
        throws IOException
    {
        if (this.mAudioRecord == 0)
            throw new IllegalStateException("not open");
        return AudioRecordRead(this.mAudioRecord, paramArrayOfByte, 0, paramArrayOfByte.length);
    }

    public int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
        throws IOException
    {
        if (this.mAudioRecord == 0)
            throw new IllegalStateException("not open");
        return AudioRecordRead(this.mAudioRecord, paramArrayOfByte, paramInt1, paramInt2);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.speech.srec.MicrophoneInputStream
 * JD-Core Version:        0.6.2
 */