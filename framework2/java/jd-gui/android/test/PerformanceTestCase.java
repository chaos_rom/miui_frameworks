package android.test;

public abstract interface PerformanceTestCase
{
    public abstract boolean isPerformanceOnly();

    public abstract int startPerformance(Intermediates paramIntermediates);

    public static abstract interface Intermediates
    {
        public abstract void addIntermediate(String paramString);

        public abstract void addIntermediate(String paramString, long paramLong);

        public abstract void finishTiming(boolean paramBoolean);

        public abstract void setInternalIterations(int paramInt);

        public abstract void startTiming(boolean paramBoolean);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.test.PerformanceTestCase
 * JD-Core Version:        0.6.2
 */