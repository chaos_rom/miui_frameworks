package android.test;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import java.lang.reflect.Field;
import junit.framework.TestCase;

public class AndroidTestCase extends TestCase
{
    protected Context mContext;
    private Context mTestContext;

    public void assertActivityRequiresPermission(String paramString1, String paramString2, String paramString3)
    {
        Intent localIntent = new Intent();
        localIntent.setClassName(paramString1, paramString2);
        localIntent.addFlags(268435456);
        try
        {
            getContext().startActivity(localIntent);
            fail("expected security exception for " + paramString3);
            return;
        }
        catch (SecurityException localSecurityException)
        {
            while (true)
            {
                assertNotNull("security exception's error message.", localSecurityException.getMessage());
                assertTrue("error message should contain " + paramString3 + ".", localSecurityException.getMessage().contains(paramString3));
            }
        }
    }

    public void assertReadingContentUriRequiresPermission(Uri paramUri, String paramString)
    {
        try
        {
            getContext().getContentResolver().query(paramUri, null, null, null, null);
            fail("expected SecurityException requiring " + paramString);
            return;
        }
        catch (SecurityException localSecurityException)
        {
            while (true)
            {
                assertNotNull("security exception's error message.", localSecurityException.getMessage());
                assertTrue("error message should contain " + paramString + ".", localSecurityException.getMessage().contains(paramString));
            }
        }
    }

    public void assertWritingContentUriRequiresPermission(Uri paramUri, String paramString)
    {
        try
        {
            getContext().getContentResolver().insert(paramUri, new ContentValues());
            fail("expected SecurityException requiring " + paramString);
            return;
        }
        catch (SecurityException localSecurityException)
        {
            while (true)
            {
                assertNotNull("security exception's error message.", localSecurityException.getMessage());
                assertTrue("error message should contain " + paramString + ".", localSecurityException.getMessage().contains(paramString));
            }
        }
    }

    public Context getContext()
    {
        return this.mContext;
    }

    public Context getTestContext()
    {
        return this.mTestContext;
    }

    protected void scrubClass(Class<?> paramClass)
        throws IllegalAccessException
    {
        Field[] arrayOfField = getClass().getDeclaredFields();
        int i = arrayOfField.length;
        int j = 0;
        while (true)
            if (j < i)
            {
                Field localField = arrayOfField[j];
                if ((paramClass.isAssignableFrom(localField.getDeclaringClass())) && (!localField.getType().isPrimitive()));
                try
                {
                    localField.setAccessible(true);
                    localField.set(this, null);
                    if (localField.get(this) != null)
                        Log.d("TestCase", "Error: Could not nullify field!");
                    j++;
                }
                catch (Exception localException)
                {
                    while (true)
                        Log.d("TestCase", "Error: Could not nullify field!");
                }
            }
    }

    public void setContext(Context paramContext)
    {
        this.mContext = paramContext;
    }

    public void setTestContext(Context paramContext)
    {
        this.mTestContext = paramContext;
    }

    protected void setUp()
        throws Exception
    {
        super.setUp();
    }

    protected void tearDown()
        throws Exception
    {
        super.tearDown();
    }

    public void testAndroidTestCaseSetupProperly()
    {
        assertNotNull("Context is null. setContext should be called before tests are run", this.mContext);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.test.AndroidTestCase
 * JD-Core Version:        0.6.2
 */