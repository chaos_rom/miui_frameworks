package android.test;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Intent;
import android.os.Bundle;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import junit.framework.TestCase;

public class InstrumentationTestCase extends TestCase
{
    private Instrumentation mInstrumentation;

    private void runMethod(Method paramMethod, int paramInt)
        throws Throwable
    {
        runMethod(paramMethod, paramInt, false);
    }

    private void runMethod(Method paramMethod, int paramInt, boolean paramBoolean)
        throws Throwable
    {
        int i = 0;
        try
        {
            do
            {
                paramMethod.invoke(this, (Object[])null);
                localObject2 = null;
                i++;
                if (paramBoolean)
                {
                    Bundle localBundle4 = new Bundle();
                    localBundle4.putInt("currentiterations", i);
                    getInstrumentation().sendStatus(2, localBundle4);
                }
            }
            while ((i < paramInt) && ((paramBoolean) || (localObject2 != null)));
            if (localObject2 != null)
                throw localObject2;
        }
        catch (InvocationTargetException localInvocationTargetException)
        {
            while (true)
            {
                localInvocationTargetException.fillInStackTrace();
                Throwable localThrowable = localInvocationTargetException.getTargetException();
                localObject2 = localThrowable;
                i++;
                if (paramBoolean)
                {
                    Bundle localBundle3 = new Bundle();
                    localBundle3.putInt("currentiterations", i);
                    getInstrumentation().sendStatus(2, localBundle3);
                }
            }
        }
        catch (IllegalAccessException localIllegalAccessException)
        {
            while (true)
            {
                localIllegalAccessException.fillInStackTrace();
                Object localObject2 = localIllegalAccessException;
                i++;
                if (paramBoolean)
                {
                    Bundle localBundle2 = new Bundle();
                    localBundle2.putInt("currentiterations", i);
                    getInstrumentation().sendStatus(2, localBundle2);
                }
            }
        }
        finally
        {
            int j = i + 1;
            if (paramBoolean)
            {
                Bundle localBundle1 = new Bundle();
                localBundle1.putInt("currentiterations", j);
                getInstrumentation().sendStatus(2, localBundle1);
            }
        }
    }

    public Instrumentation getInstrumentation()
    {
        return this.mInstrumentation;
    }

    @Deprecated
    public void injectInsrumentation(Instrumentation paramInstrumentation)
    {
        injectInstrumentation(paramInstrumentation);
    }

    public void injectInstrumentation(Instrumentation paramInstrumentation)
    {
        this.mInstrumentation = paramInstrumentation;
    }

    public final <T extends Activity> T launchActivity(String paramString, Class<T> paramClass, Bundle paramBundle)
    {
        Intent localIntent = new Intent("android.intent.action.MAIN");
        if (paramBundle != null)
            localIntent.putExtras(paramBundle);
        return launchActivityWithIntent(paramString, paramClass, localIntent);
    }

    public final <T extends Activity> T launchActivityWithIntent(String paramString, Class<T> paramClass, Intent paramIntent)
    {
        paramIntent.setClassName(paramString, paramClass.getName());
        paramIntent.addFlags(268435456);
        Activity localActivity = getInstrumentation().startActivitySync(paramIntent);
        getInstrumentation().waitForIdleSync();
        return localActivity;
    }

    protected void runTest()
        throws Throwable
    {
        String str = getName();
        assertNotNull(str);
        Object localObject1 = null;
        try
        {
            Method localMethod = getClass().getMethod(str, (Class[])null);
            localObject1 = localMethod;
            if (!Modifier.isPublic(localObject1.getModifiers()))
                fail("Method \"" + str + "\" should be public");
            i = 1;
            bool1 = false;
            if (localObject1.isAnnotationPresent(FlakyTest.class))
            {
                i = ((FlakyTest)localObject1.getAnnotation(FlakyTest.class)).tolerance();
                if (!localObject1.isAnnotationPresent(UiThreadTest.class))
                    break label219;
                final int j = i;
                final boolean bool2 = bool1;
                final Object localObject2 = localObject1;
                final Throwable[] arrayOfThrowable = new Throwable[1];
                getInstrumentation().runOnMainSync(new Runnable()
                {
                    public void run()
                    {
                        try
                        {
                            InstrumentationTestCase.this.runMethod(localObject2, j, bool2);
                            return;
                        }
                        catch (Throwable localThrowable)
                        {
                            while (true)
                                arrayOfThrowable[0] = localThrowable;
                        }
                    }
                });
                if (arrayOfThrowable[0] == null)
                    return;
                throw arrayOfThrowable[0];
            }
        }
        catch (NoSuchMethodException localNoSuchMethodException)
        {
            int i;
            boolean bool1;
            while (true)
            {
                fail("Method \"" + str + "\" not found");
                continue;
                if (localObject1.isAnnotationPresent(RepetitiveTest.class))
                {
                    i = ((RepetitiveTest)localObject1.getAnnotation(RepetitiveTest.class)).numIterations();
                    bool1 = true;
                }
            }
            label219: runMethod(localObject1, i, bool1);
        }
    }

    public void runTestOnUiThread(final Runnable paramRunnable)
        throws Throwable
    {
        final Throwable[] arrayOfThrowable = new Throwable[1];
        getInstrumentation().runOnMainSync(new Runnable()
        {
            public void run()
            {
                try
                {
                    paramRunnable.run();
                    return;
                }
                catch (Throwable localThrowable)
                {
                    while (true)
                        arrayOfThrowable[0] = localThrowable;
                }
            }
        });
        if (arrayOfThrowable[0] != null)
            throw arrayOfThrowable[0];
    }

    // ERROR //
    public void sendKeys(String paramString)
    {
        // Byte code:
        //     0: aload_1
        //     1: ldc 198
        //     3: invokevirtual 204	java/lang/String:split	(Ljava/lang/String;)[Ljava/lang/String;
        //     6: astore_2
        //     7: aload_2
        //     8: arraylength
        //     9: istore_3
        //     10: aload_0
        //     11: invokevirtual 48	android/test/InstrumentationTestCase:getInstrumentation	()Landroid/app/Instrumentation;
        //     14: astore 4
        //     16: iconst_0
        //     17: istore 5
        //     19: iload 5
        //     21: iload_3
        //     22: if_icmpge +216 -> 238
        //     25: aload_2
        //     26: iload 5
        //     28: aaload
        //     29: astore 6
        //     31: aload 6
        //     33: bipush 42
        //     35: invokevirtual 208	java/lang/String:indexOf	(I)I
        //     38: istore 7
        //     40: iload 7
        //     42: bipush 255
        //     44: if_icmpne +78 -> 122
        //     47: iconst_1
        //     48: istore 11
        //     50: iload 7
        //     52: bipush 255
        //     54: if_icmpeq +14 -> 68
        //     57: aload 6
        //     59: iload 7
        //     61: iconst_1
        //     62: iadd
        //     63: invokevirtual 212	java/lang/String:substring	(I)Ljava/lang/String;
        //     66: astore 6
        //     68: iconst_0
        //     69: istore 12
        //     71: iload 12
        //     73: iload 11
        //     75: if_icmpge +95 -> 170
        //     78: ldc 214
        //     80: new 141	java/lang/StringBuilder
        //     83: dup
        //     84: invokespecial 142	java/lang/StringBuilder:<init>	()V
        //     87: ldc 216
        //     89: invokevirtual 148	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     92: aload 6
        //     94: invokevirtual 148	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     97: invokevirtual 153	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     100: invokevirtual 220	java/lang/Class:getField	(Ljava/lang/String;)Ljava/lang/reflect/Field;
        //     103: aconst_null
        //     104: invokevirtual 226	java/lang/reflect/Field:getInt	(Ljava/lang/Object;)I
        //     107: istore 17
        //     109: aload 4
        //     111: iload 17
        //     113: invokevirtual 230	android/app/Instrumentation:sendKeyDownUpSync	(I)V
        //     116: iinc 12 1
        //     119: goto -48 -> 71
        //     122: aload 6
        //     124: iconst_0
        //     125: iload 7
        //     127: invokevirtual 233	java/lang/String:substring	(II)Ljava/lang/String;
        //     130: invokestatic 239	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     133: istore 10
        //     135: iload 10
        //     137: istore 11
        //     139: goto -89 -> 50
        //     142: astore 8
        //     144: ldc 241
        //     146: new 141	java/lang/StringBuilder
        //     149: dup
        //     150: invokespecial 142	java/lang/StringBuilder:<init>	()V
        //     153: ldc 243
        //     155: invokevirtual 148	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     158: aload 6
        //     160: invokevirtual 148	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     163: invokevirtual 153	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     166: invokestatic 249	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     169: pop
        //     170: iinc 5 1
        //     173: goto -154 -> 19
        //     176: astore 15
        //     178: ldc 241
        //     180: new 141	java/lang/StringBuilder
        //     183: dup
        //     184: invokespecial 142	java/lang/StringBuilder:<init>	()V
        //     187: ldc 251
        //     189: invokevirtual 148	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     192: aload 6
        //     194: invokevirtual 148	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     197: invokevirtual 153	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     200: invokestatic 249	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     203: pop
        //     204: goto -34 -> 170
        //     207: astore 13
        //     209: ldc 241
        //     211: new 141	java/lang/StringBuilder
        //     214: dup
        //     215: invokespecial 142	java/lang/StringBuilder:<init>	()V
        //     218: ldc 251
        //     220: invokevirtual 148	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     223: aload 6
        //     225: invokevirtual 148	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     228: invokevirtual 153	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     231: invokestatic 249	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     234: pop
        //     235: goto -65 -> 170
        //     238: aload 4
        //     240: invokevirtual 109	android/app/Instrumentation:waitForIdleSync	()V
        //     243: return
        //     244: astore 18
        //     246: goto -130 -> 116
        //
        // Exception table:
        //     from	to	target	type
        //     122	135	142	java/lang/NumberFormatException
        //     78	109	176	java/lang/NoSuchFieldException
        //     109	116	176	java/lang/NoSuchFieldException
        //     78	109	207	java/lang/IllegalAccessException
        //     109	116	207	java/lang/IllegalAccessException
        //     109	116	244	java/lang/SecurityException
    }

    public void sendKeys(int[] paramArrayOfInt)
    {
        int i = paramArrayOfInt.length;
        Instrumentation localInstrumentation = getInstrumentation();
        int j = 0;
        while (true)
        {
            if (j < i);
            try
            {
                localInstrumentation.sendKeyDownUpSync(paramArrayOfInt[j]);
                label25: j++;
                continue;
                localInstrumentation.waitForIdleSync();
                return;
            }
            catch (SecurityException localSecurityException)
            {
                break label25;
            }
        }
    }

    public void sendRepeatedKeys(int[] paramArrayOfInt)
    {
        int i = paramArrayOfInt.length;
        if ((i & 0x1) == 1)
            throw new IllegalArgumentException("The size of the keys array must be a multiple of 2");
        Instrumentation localInstrumentation = getInstrumentation();
        int j = 0;
        while (true)
        {
            int m;
            int n;
            if (j < i)
            {
                int k = paramArrayOfInt[j];
                m = paramArrayOfInt[(j + 1)];
                n = 0;
                label52: if (n >= k);
            }
            try
            {
                localInstrumentation.sendKeyDownUpSync(m);
                label65: n++;
                break label52;
                j += 2;
                continue;
                localInstrumentation.waitForIdleSync();
                return;
            }
            catch (SecurityException localSecurityException)
            {
                break label65;
            }
        }
    }

    protected void tearDown()
        throws Exception
    {
        Runtime.getRuntime().gc();
        Runtime.getRuntime().runFinalization();
        Runtime.getRuntime().gc();
        super.tearDown();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.test.InstrumentationTestCase
 * JD-Core Version:        0.6.2
 */