package android.test;

import android.app.Instrumentation;
import junit.framework.Test;
import junit.framework.TestResult;
import junit.framework.TestSuite;

public class InstrumentationTestSuite extends TestSuite
{
    private final Instrumentation mInstrumentation;

    public InstrumentationTestSuite(Instrumentation paramInstrumentation)
    {
        this.mInstrumentation = paramInstrumentation;
    }

    public InstrumentationTestSuite(Class paramClass, Instrumentation paramInstrumentation)
    {
        super(paramClass);
        this.mInstrumentation = paramInstrumentation;
    }

    public InstrumentationTestSuite(String paramString, Instrumentation paramInstrumentation)
    {
        super(paramString);
        this.mInstrumentation = paramInstrumentation;
    }

    public void addTestSuite(Class paramClass)
    {
        addTest(new InstrumentationTestSuite(paramClass, this.mInstrumentation));
    }

    public void runTest(Test paramTest, TestResult paramTestResult)
    {
        if ((paramTest instanceof InstrumentationTestCase))
            ((InstrumentationTestCase)paramTest).injectInstrumentation(this.mInstrumentation);
        super.runTest(paramTest, paramTestResult);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.test.InstrumentationTestSuite
 * JD-Core Version:        0.6.2
 */