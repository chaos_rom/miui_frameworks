package android.gesture;

public class Prediction
{
    public final String name;
    public double score;

    Prediction(String paramString, double paramDouble)
    {
        this.name = paramString;
        this.score = paramDouble;
    }

    public String toString()
    {
        return this.name;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.gesture.Prediction
 * JD-Core Version:        0.6.2
 */