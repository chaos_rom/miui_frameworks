package android.gesture;

import java.util.ArrayList;

abstract class Learner
{
    private final ArrayList<Instance> mInstances = new ArrayList();

    void addInstance(Instance paramInstance)
    {
        this.mInstances.add(paramInstance);
    }

    abstract ArrayList<Prediction> classify(int paramInt1, int paramInt2, float[] paramArrayOfFloat);

    ArrayList<Instance> getInstances()
    {
        return this.mInstances;
    }

    void removeInstance(long paramLong)
    {
        ArrayList localArrayList = this.mInstances;
        int i = localArrayList.size();
        for (int j = 0; ; j++)
            if (j < i)
            {
                Instance localInstance = (Instance)localArrayList.get(j);
                if (paramLong == localInstance.id)
                    localArrayList.remove(localInstance);
            }
            else
            {
                return;
            }
    }

    void removeInstances(String paramString)
    {
        ArrayList localArrayList1 = new ArrayList();
        ArrayList localArrayList2 = this.mInstances;
        int i = localArrayList2.size();
        for (int j = 0; j < i; j++)
        {
            Instance localInstance = (Instance)localArrayList2.get(j);
            if (((localInstance.label == null) && (paramString == null)) || ((localInstance.label != null) && (localInstance.label.equals(paramString))))
                localArrayList1.add(localInstance);
        }
        localArrayList2.removeAll(localArrayList1);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.gesture.Learner
 * JD-Core Version:        0.6.2
 */