package android.gesture;

import android.graphics.Matrix;
import android.graphics.Path;

public class OrientedBoundingBox
{
    public final float centerX;
    public final float centerY;
    public final float height;
    public final float orientation;
    public final float squareness;
    public final float width;

    OrientedBoundingBox(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5)
    {
        this.orientation = paramFloat1;
        this.width = paramFloat4;
        this.height = paramFloat5;
        this.centerX = paramFloat2;
        this.centerY = paramFloat3;
        float f = paramFloat4 / paramFloat5;
        if (f > 1.0F);
        for (this.squareness = (1.0F / f); ; this.squareness = f)
            return;
    }

    public Path toPath()
    {
        Path localPath = new Path();
        float[] arrayOfFloat = new float[2];
        arrayOfFloat[0] = (-this.width / 2.0F);
        arrayOfFloat[1] = (this.height / 2.0F);
        Matrix localMatrix = new Matrix();
        localMatrix.setRotate(this.orientation);
        localMatrix.postTranslate(this.centerX, this.centerY);
        localMatrix.mapPoints(arrayOfFloat);
        localPath.moveTo(arrayOfFloat[0], arrayOfFloat[1]);
        arrayOfFloat[0] = (-this.width / 2.0F);
        arrayOfFloat[1] = (-this.height / 2.0F);
        localMatrix.mapPoints(arrayOfFloat);
        localPath.lineTo(arrayOfFloat[0], arrayOfFloat[1]);
        arrayOfFloat[0] = (this.width / 2.0F);
        arrayOfFloat[1] = (-this.height / 2.0F);
        localMatrix.mapPoints(arrayOfFloat);
        localPath.lineTo(arrayOfFloat[0], arrayOfFloat[1]);
        arrayOfFloat[0] = (this.width / 2.0F);
        arrayOfFloat[1] = (this.height / 2.0F);
        localMatrix.mapPoints(arrayOfFloat);
        localPath.lineTo(arrayOfFloat[0], arrayOfFloat[1]);
        localPath.close();
        return localPath;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.gesture.OrientedBoundingBox
 * JD-Core Version:        0.6.2
 */