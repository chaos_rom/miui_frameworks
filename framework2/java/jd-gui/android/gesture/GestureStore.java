package android.gesture;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

public class GestureStore
{
    private static final short FILE_FORMAT_VERSION = 1;
    public static final int ORIENTATION_INVARIANT = 1;
    public static final int ORIENTATION_SENSITIVE = 2;
    static final int ORIENTATION_SENSITIVE_4 = 4;
    static final int ORIENTATION_SENSITIVE_8 = 8;
    private static final boolean PROFILE_LOADING_SAVING = false;
    public static final int SEQUENCE_INVARIANT = 1;
    public static final int SEQUENCE_SENSITIVE = 2;
    private boolean mChanged = false;
    private Learner mClassifier = new InstanceLearner();
    private final HashMap<String, ArrayList<Gesture>> mNamedGestures = new HashMap();
    private int mOrientationStyle = 2;
    private int mSequenceType = 2;

    private void readFormatV1(DataInputStream paramDataInputStream)
        throws IOException
    {
        Learner localLearner = this.mClassifier;
        HashMap localHashMap = this.mNamedGestures;
        localHashMap.clear();
        int i = paramDataInputStream.readInt();
        for (int j = 0; j < i; j++)
        {
            String str = paramDataInputStream.readUTF();
            int k = paramDataInputStream.readInt();
            ArrayList localArrayList = new ArrayList(k);
            for (int m = 0; m < k; m++)
            {
                Gesture localGesture = Gesture.deserialize(paramDataInputStream);
                localArrayList.add(localGesture);
                localLearner.addInstance(Instance.createInstance(this.mSequenceType, this.mOrientationStyle, localGesture, str));
            }
            localHashMap.put(str, localArrayList);
        }
    }

    public void addGesture(String paramString, Gesture paramGesture)
    {
        if ((paramString == null) || (paramString.length() == 0));
        while (true)
        {
            return;
            ArrayList localArrayList = (ArrayList)this.mNamedGestures.get(paramString);
            if (localArrayList == null)
            {
                localArrayList = new ArrayList();
                this.mNamedGestures.put(paramString, localArrayList);
            }
            localArrayList.add(paramGesture);
            this.mClassifier.addInstance(Instance.createInstance(this.mSequenceType, this.mOrientationStyle, paramGesture, paramString));
            this.mChanged = true;
        }
    }

    public Set<String> getGestureEntries()
    {
        return this.mNamedGestures.keySet();
    }

    public ArrayList<Gesture> getGestures(String paramString)
    {
        ArrayList localArrayList1 = (ArrayList)this.mNamedGestures.get(paramString);
        if (localArrayList1 != null);
        for (ArrayList localArrayList2 = new ArrayList(localArrayList1); ; localArrayList2 = null)
            return localArrayList2;
    }

    Learner getLearner()
    {
        return this.mClassifier;
    }

    public int getOrientationStyle()
    {
        return this.mOrientationStyle;
    }

    public int getSequenceType()
    {
        return this.mSequenceType;
    }

    public boolean hasChanged()
    {
        return this.mChanged;
    }

    public void load(InputStream paramInputStream)
        throws IOException
    {
        load(paramInputStream, false);
    }

    public void load(InputStream paramInputStream, boolean paramBoolean)
        throws IOException
    {
        Object localObject1 = null;
        try
        {
            if ((paramInputStream instanceof BufferedInputStream));
            while (true)
            {
                DataInputStream localDataInputStream = new DataInputStream(paramInputStream);
                try
                {
                    int i = localDataInputStream.readShort();
                    switch (i)
                    {
                    default:
                        if (paramBoolean)
                            GestureUtils.closeStream(localDataInputStream);
                        return;
                        BufferedInputStream localBufferedInputStream = new BufferedInputStream(paramInputStream, 32768);
                        paramInputStream = localBufferedInputStream;
                        break;
                    case 1:
                        readFormatV1(localDataInputStream);
                    }
                }
                finally
                {
                    localObject1 = localDataInputStream;
                }
            }
            label90: if (paramBoolean)
                GestureUtils.closeStream(localObject1);
            throw localObject2;
        }
        finally
        {
            break label90;
        }
    }

    public ArrayList<Prediction> recognize(Gesture paramGesture)
    {
        Instance localInstance = Instance.createInstance(this.mSequenceType, this.mOrientationStyle, paramGesture, null);
        return this.mClassifier.classify(this.mSequenceType, this.mOrientationStyle, localInstance.vector);
    }

    public void removeEntry(String paramString)
    {
        this.mNamedGestures.remove(paramString);
        this.mClassifier.removeInstances(paramString);
        this.mChanged = true;
    }

    public void removeGesture(String paramString, Gesture paramGesture)
    {
        ArrayList localArrayList = (ArrayList)this.mNamedGestures.get(paramString);
        if (localArrayList == null);
        while (true)
        {
            return;
            localArrayList.remove(paramGesture);
            if (localArrayList.isEmpty())
                this.mNamedGestures.remove(paramString);
            this.mClassifier.removeInstance(paramGesture.getID());
            this.mChanged = true;
        }
    }

    public void save(OutputStream paramOutputStream)
        throws IOException
    {
        save(paramOutputStream, false);
    }

    // ERROR //
    public void save(OutputStream paramOutputStream, boolean paramBoolean)
        throws IOException
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_3
        //     2: aload_0
        //     3: getfield 41	android/gesture/GestureStore:mNamedGestures	Ljava/util/HashMap;
        //     6: astore 5
        //     8: aload_1
        //     9: instanceof 188
        //     12: ifeq +139 -> 151
        //     15: new 190	java/io/DataOutputStream
        //     18: dup
        //     19: aload_1
        //     20: invokespecial 192	java/io/DataOutputStream:<init>	(Ljava/io/OutputStream;)V
        //     23: astore 7
        //     25: aload 7
        //     27: iconst_1
        //     28: invokevirtual 195	java/io/DataOutputStream:writeShort	(I)V
        //     31: aload 7
        //     33: aload 5
        //     35: invokevirtual 198	java/util/HashMap:size	()I
        //     38: invokevirtual 201	java/io/DataOutputStream:writeInt	(I)V
        //     41: aload 5
        //     43: invokevirtual 204	java/util/HashMap:entrySet	()Ljava/util/Set;
        //     46: invokeinterface 210 1 0
        //     51: astore 8
        //     53: aload 8
        //     55: invokeinterface 215 1 0
        //     60: ifeq +109 -> 169
        //     63: aload 8
        //     65: invokeinterface 219 1 0
        //     70: checkcast 221	java/util/Map$Entry
        //     73: astore 9
        //     75: aload 9
        //     77: invokeinterface 224 1 0
        //     82: checkcast 100	java/lang/String
        //     85: astore 10
        //     87: aload 9
        //     89: invokeinterface 227 1 0
        //     94: checkcast 67	java/util/ArrayList
        //     97: astore 11
        //     99: aload 11
        //     101: invokevirtual 228	java/util/ArrayList:size	()I
        //     104: istore 12
        //     106: aload 7
        //     108: aload 10
        //     110: invokevirtual 231	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
        //     113: aload 7
        //     115: iload 12
        //     117: invokevirtual 201	java/io/DataOutputStream:writeInt	(I)V
        //     120: iconst_0
        //     121: istore 13
        //     123: iload 13
        //     125: iload 12
        //     127: if_icmpge -74 -> 53
        //     130: aload 11
        //     132: iload 13
        //     134: invokevirtual 234	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     137: checkcast 72	android/gesture/Gesture
        //     140: aload 7
        //     142: invokevirtual 238	android/gesture/Gesture:serialize	(Ljava/io/DataOutputStream;)V
        //     145: iinc 13 1
        //     148: goto -25 -> 123
        //     151: new 188	java/io/BufferedOutputStream
        //     154: dup
        //     155: aload_1
        //     156: ldc 144
        //     158: invokespecial 241	java/io/BufferedOutputStream:<init>	(Ljava/io/OutputStream;I)V
        //     161: astore 6
        //     163: aload 6
        //     165: astore_1
        //     166: goto -151 -> 15
        //     169: aload 7
        //     171: invokevirtual 244	java/io/DataOutputStream:flush	()V
        //     174: aload_0
        //     175: iconst_0
        //     176: putfield 43	android/gesture/GestureStore:mChanged	Z
        //     179: iload_2
        //     180: ifeq +8 -> 188
        //     183: aload 7
        //     185: invokestatic 143	android/gesture/GestureUtils:closeStream	(Ljava/io/Closeable;)V
        //     188: return
        //     189: astore 4
        //     191: iload_2
        //     192: ifeq +7 -> 199
        //     195: aload_3
        //     196: invokestatic 143	android/gesture/GestureUtils:closeStream	(Ljava/io/Closeable;)V
        //     199: aload 4
        //     201: athrow
        //     202: astore 4
        //     204: aload 7
        //     206: astore_3
        //     207: goto -16 -> 191
        //
        // Exception table:
        //     from	to	target	type
        //     2	25	189	finally
        //     151	163	189	finally
        //     25	145	202	finally
        //     169	179	202	finally
    }

    public void setOrientationStyle(int paramInt)
    {
        this.mOrientationStyle = paramInt;
    }

    public void setSequenceType(int paramInt)
    {
        this.mSequenceType = paramInt;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.gesture.GestureStore
 * JD-Core Version:        0.6.2
 */