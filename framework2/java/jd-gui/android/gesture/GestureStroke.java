package android.gesture;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class GestureStroke
{
    static final float TOUCH_TOLERANCE = 3.0F;
    public final RectF boundingBox;
    public final float length;
    private Path mCachedPath;
    public final float[] points;
    private final long[] timestamps;

    private GestureStroke(RectF paramRectF, float paramFloat, float[] paramArrayOfFloat, long[] paramArrayOfLong)
    {
        this.boundingBox = new RectF(paramRectF.left, paramRectF.top, paramRectF.right, paramRectF.bottom);
        this.length = paramFloat;
        this.points = ((float[])paramArrayOfFloat.clone());
        this.timestamps = ((long[])paramArrayOfLong.clone());
    }

    public GestureStroke(ArrayList<GesturePoint> paramArrayList)
    {
        int i = paramArrayList.size();
        float[] arrayOfFloat = new float[i * 2];
        long[] arrayOfLong = new long[i];
        RectF localRectF = null;
        float f = 0.0F;
        int j = 0;
        int k = 0;
        if (k < i)
        {
            GesturePoint localGesturePoint = (GesturePoint)paramArrayList.get(k);
            arrayOfFloat[(k * 2)] = localGesturePoint.x;
            arrayOfFloat[(1 + k * 2)] = localGesturePoint.y;
            arrayOfLong[j] = localGesturePoint.timestamp;
            if (localRectF == null)
            {
                localRectF = new RectF();
                localRectF.top = localGesturePoint.y;
                localRectF.left = localGesturePoint.x;
                localRectF.right = localGesturePoint.x;
                localRectF.bottom = localGesturePoint.y;
                f = 0.0F;
            }
            while (true)
            {
                j++;
                k++;
                break;
                f = (float)(f + Math.sqrt(Math.pow(localGesturePoint.x - arrayOfFloat[(2 * (k - 1))], 2.0D) + Math.pow(localGesturePoint.y - arrayOfFloat[(1 + 2 * (k - 1))], 2.0D)));
                localRectF.union(localGesturePoint.x, localGesturePoint.y);
            }
        }
        this.timestamps = arrayOfLong;
        this.points = arrayOfFloat;
        this.boundingBox = localRectF;
        this.length = f;
    }

    static GestureStroke deserialize(DataInputStream paramDataInputStream)
        throws IOException
    {
        int i = paramDataInputStream.readInt();
        ArrayList localArrayList = new ArrayList(i);
        for (int j = 0; j < i; j++)
            localArrayList.add(GesturePoint.deserialize(paramDataInputStream));
        return new GestureStroke(localArrayList);
    }

    private void makePath()
    {
        float[] arrayOfFloat = this.points;
        int i = arrayOfFloat.length;
        Path localPath = null;
        float f1 = 0.0F;
        float f2 = 0.0F;
        int j = 0;
        if (j < i)
        {
            float f3 = arrayOfFloat[j];
            float f4 = arrayOfFloat[(j + 1)];
            if (localPath == null)
            {
                localPath = new Path();
                localPath.moveTo(f3, f4);
                f1 = f3;
            }
            for (f2 = f4; ; f2 = f4)
            {
                float f5;
                float f6;
                do
                {
                    j += 2;
                    break;
                    f5 = Math.abs(f3 - f1);
                    f6 = Math.abs(f4 - f2);
                }
                while ((f5 < 3.0F) && (f6 < 3.0F));
                localPath.quadTo(f1, f2, (f3 + f1) / 2.0F, (f4 + f2) / 2.0F);
                f1 = f3;
            }
        }
        this.mCachedPath = localPath;
    }

    public void clearPath()
    {
        if (this.mCachedPath != null)
            this.mCachedPath.rewind();
    }

    public Object clone()
    {
        return new GestureStroke(this.boundingBox, this.length, this.points, this.timestamps);
    }

    public OrientedBoundingBox computeOrientedBoundingBox()
    {
        return GestureUtils.computeOrientedBoundingBox(this.points);
    }

    void draw(Canvas paramCanvas, Paint paramPaint)
    {
        if (this.mCachedPath == null)
            makePath();
        paramCanvas.drawPath(this.mCachedPath, paramPaint);
    }

    public Path getPath()
    {
        if (this.mCachedPath == null)
            makePath();
        return this.mCachedPath;
    }

    void serialize(DataOutputStream paramDataOutputStream)
        throws IOException
    {
        float[] arrayOfFloat = this.points;
        long[] arrayOfLong = this.timestamps;
        int i = this.points.length;
        paramDataOutputStream.writeInt(i / 2);
        for (int j = 0; j < i; j += 2)
        {
            paramDataOutputStream.writeFloat(arrayOfFloat[j]);
            paramDataOutputStream.writeFloat(arrayOfFloat[(j + 1)]);
            paramDataOutputStream.writeLong(arrayOfLong[(j / 2)]);
        }
    }

    public Path toPath(float paramFloat1, float paramFloat2, int paramInt)
    {
        float[] arrayOfFloat = GestureUtils.temporalSampling(this, paramInt);
        RectF localRectF = this.boundingBox;
        GestureUtils.translate(arrayOfFloat, -localRectF.left, -localRectF.top);
        float f1 = paramFloat1 / localRectF.width();
        float f2 = paramFloat2 / localRectF.height();
        float f3;
        float f4;
        Path localPath;
        int j;
        label88: float f6;
        float f7;
        if (f1 > f2)
        {
            f3 = f2;
            GestureUtils.scale(arrayOfFloat, f3, f3);
            f4 = 0.0F;
            f5 = 0.0F;
            localPath = null;
            int i = arrayOfFloat.length;
            j = 0;
            if (j >= i)
                break label225;
            f6 = arrayOfFloat[j];
            f7 = arrayOfFloat[(j + 1)];
            if (localPath != null)
                break label155;
            localPath = new Path();
            localPath.moveTo(f6, f7);
            f4 = f6;
        }
        for (float f5 = f7; ; f5 = f7)
        {
            label155: float f8;
            float f9;
            do
            {
                j += 2;
                break label88;
                f3 = f1;
                break;
                f8 = Math.abs(f6 - f4);
                f9 = Math.abs(f7 - f5);
            }
            while ((f8 < 3.0F) && (f9 < 3.0F));
            localPath.quadTo(f4, f5, (f6 + f4) / 2.0F, (f7 + f5) / 2.0F);
            f4 = f6;
        }
        label225: return localPath;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.gesture.GestureStroke
 * JD-Core Version:        0.6.2
 */