package android.gesture;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeMap;

class InstanceLearner extends Learner
{
    private static final Comparator<Prediction> sComparator = new Comparator()
    {
        public int compare(Prediction paramAnonymousPrediction1, Prediction paramAnonymousPrediction2)
        {
            double d1 = paramAnonymousPrediction1.score;
            double d2 = paramAnonymousPrediction2.score;
            int i;
            if (d1 > d2)
                i = -1;
            while (true)
            {
                return i;
                if (d1 < d2)
                    i = 1;
                else
                    i = 0;
            }
        }
    };

    ArrayList<Prediction> classify(int paramInt1, int paramInt2, float[] paramArrayOfFloat)
    {
        ArrayList localArrayList1 = new ArrayList();
        ArrayList localArrayList2 = getInstances();
        int i = localArrayList2.size();
        TreeMap localTreeMap = new TreeMap();
        int j = 0;
        while (j < i)
        {
            Instance localInstance = (Instance)localArrayList2.get(j);
            if (localInstance.vector.length != paramArrayOfFloat.length)
            {
                j++;
            }
            else
            {
                double d2;
                if (paramInt1 == 2)
                {
                    d2 = GestureUtils.minimumCosineDistance(localInstance.vector, paramArrayOfFloat, paramInt2);
                    label88: if (d2 != 0.0D)
                        break label165;
                }
                label165: for (double d3 = 1.7976931348623157E+308D; ; d3 = 1.0D / d2)
                {
                    Double localDouble = (Double)localTreeMap.get(localInstance.label);
                    if ((localDouble != null) && (d3 <= localDouble.doubleValue()))
                        break;
                    localTreeMap.put(localInstance.label, Double.valueOf(d3));
                    break;
                    d2 = GestureUtils.squaredEuclideanDistance(localInstance.vector, paramArrayOfFloat);
                    break label88;
                }
            }
        }
        Iterator localIterator = localTreeMap.keySet().iterator();
        while (localIterator.hasNext())
        {
            String str = (String)localIterator.next();
            double d1 = ((Double)localTreeMap.get(str)).doubleValue();
            Prediction localPrediction = new Prediction(str, d1);
            localArrayList1.add(localPrediction);
        }
        Collections.sort(localArrayList1, sComparator);
        return localArrayList1;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.gesture.InstanceLearner
 * JD-Core Version:        0.6.2
 */