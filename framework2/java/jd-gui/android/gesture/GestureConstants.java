package android.gesture;

abstract interface GestureConstants
{
    public static final int IO_BUFFER_SIZE = 32768;
    public static final String LOG_TAG = "Gestures";
    public static final int STROKE_POINT_BUFFER_SIZE = 100;
    public static final int STROKE_STRING_BUFFER_SIZE = 1024;
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.gesture.GestureConstants
 * JD-Core Version:        0.6.2
 */