package android.gesture;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Join;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.RectF;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.Log;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

public class Gesture
    implements Parcelable
{
    private static final boolean BITMAP_RENDERING_ANTIALIAS = true;
    private static final boolean BITMAP_RENDERING_DITHER = true;
    private static final int BITMAP_RENDERING_WIDTH = 2;
    public static final Parcelable.Creator<Gesture> CREATOR = new Parcelable.Creator()
    {
        public Gesture createFromParcel(Parcel paramAnonymousParcel)
        {
            Object localObject1 = null;
            long l = paramAnonymousParcel.readLong();
            DataInputStream localDataInputStream = new DataInputStream(new ByteArrayInputStream(paramAnonymousParcel.createByteArray()));
            try
            {
                Gesture localGesture = Gesture.deserialize(localDataInputStream);
                localObject1 = localGesture;
                GestureUtils.closeStream(localDataInputStream);
                if (localObject1 != null)
                    Gesture.access$002(localObject1, l);
                return localObject1;
            }
            catch (IOException localIOException)
            {
                while (true)
                {
                    Log.e("Gestures", "Error reading Gesture from parcel:", localIOException);
                    GestureUtils.closeStream(localDataInputStream);
                }
            }
            finally
            {
                GestureUtils.closeStream(localDataInputStream);
            }
        }

        public Gesture[] newArray(int paramAnonymousInt)
        {
            return new Gesture[paramAnonymousInt];
        }
    };
    private static final long GESTURE_ID_BASE = System.currentTimeMillis();
    private static final AtomicInteger sGestureCount = new AtomicInteger(0);
    private final RectF mBoundingBox = new RectF();
    private long mGestureID = GESTURE_ID_BASE + sGestureCount.incrementAndGet();
    private final ArrayList<GestureStroke> mStrokes = new ArrayList();

    static Gesture deserialize(DataInputStream paramDataInputStream)
        throws IOException
    {
        Gesture localGesture = new Gesture();
        localGesture.mGestureID = paramDataInputStream.readLong();
        int i = paramDataInputStream.readInt();
        for (int j = 0; j < i; j++)
            localGesture.addStroke(GestureStroke.deserialize(paramDataInputStream));
        return localGesture;
    }

    public void addStroke(GestureStroke paramGestureStroke)
    {
        this.mStrokes.add(paramGestureStroke);
        this.mBoundingBox.union(paramGestureStroke.boundingBox);
    }

    public Object clone()
    {
        Gesture localGesture = new Gesture();
        localGesture.mBoundingBox.set(this.mBoundingBox.left, this.mBoundingBox.top, this.mBoundingBox.right, this.mBoundingBox.bottom);
        int i = this.mStrokes.size();
        for (int j = 0; j < i; j++)
        {
            GestureStroke localGestureStroke = (GestureStroke)this.mStrokes.get(j);
            localGesture.mStrokes.add((GestureStroke)localGestureStroke.clone());
        }
        return localGesture;
    }

    public int describeContents()
    {
        return 0;
    }

    public RectF getBoundingBox()
    {
        return this.mBoundingBox;
    }

    public long getID()
    {
        return this.mGestureID;
    }

    public float getLength()
    {
        int i = 0;
        ArrayList localArrayList = this.mStrokes;
        int j = localArrayList.size();
        for (int k = 0; k < j; k++)
            i = (int)(i + ((GestureStroke)localArrayList.get(k)).length);
        return i;
    }

    public ArrayList<GestureStroke> getStrokes()
    {
        return this.mStrokes;
    }

    public int getStrokesCount()
    {
        return this.mStrokes.size();
    }

    void serialize(DataOutputStream paramDataOutputStream)
        throws IOException
    {
        ArrayList localArrayList = this.mStrokes;
        int i = localArrayList.size();
        paramDataOutputStream.writeLong(this.mGestureID);
        paramDataOutputStream.writeInt(i);
        for (int j = 0; j < i; j++)
            ((GestureStroke)localArrayList.get(j)).serialize(paramDataOutputStream);
    }

    void setID(long paramLong)
    {
        this.mGestureID = paramLong;
    }

    public Bitmap toBitmap(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        Bitmap localBitmap = Bitmap.createBitmap(paramInt1, paramInt2, Bitmap.Config.ARGB_8888);
        Canvas localCanvas = new Canvas(localBitmap);
        Paint localPaint = new Paint();
        localPaint.setAntiAlias(true);
        localPaint.setDither(true);
        localPaint.setColor(paramInt4);
        localPaint.setStyle(Paint.Style.STROKE);
        localPaint.setStrokeJoin(Paint.Join.ROUND);
        localPaint.setStrokeCap(Paint.Cap.ROUND);
        localPaint.setStrokeWidth(2.0F);
        Path localPath = toPath();
        RectF localRectF = new RectF();
        localPath.computeBounds(localRectF, true);
        float f1 = (paramInt1 - paramInt3 * 2) / localRectF.width();
        float f2 = (paramInt2 - paramInt3 * 2) / localRectF.height();
        if (f1 > f2);
        for (float f3 = f2; ; f3 = f1)
        {
            localPaint.setStrokeWidth(2.0F / f3);
            localPath.offset(-localRectF.left + (paramInt1 - f3 * localRectF.width()) / 2.0F, -localRectF.top + (paramInt2 - f3 * localRectF.height()) / 2.0F);
            localCanvas.translate(paramInt3, paramInt3);
            localCanvas.scale(f3, f3);
            localCanvas.drawPath(localPath, localPaint);
            return localBitmap;
        }
    }

    public Bitmap toBitmap(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
    {
        Bitmap localBitmap = Bitmap.createBitmap(paramInt1, paramInt2, Bitmap.Config.ARGB_8888);
        Canvas localCanvas = new Canvas(localBitmap);
        localCanvas.translate(paramInt3, paramInt3);
        Paint localPaint = new Paint();
        localPaint.setAntiAlias(true);
        localPaint.setDither(true);
        localPaint.setColor(paramInt5);
        localPaint.setStyle(Paint.Style.STROKE);
        localPaint.setStrokeJoin(Paint.Join.ROUND);
        localPaint.setStrokeCap(Paint.Cap.ROUND);
        localPaint.setStrokeWidth(2.0F);
        ArrayList localArrayList = this.mStrokes;
        int i = localArrayList.size();
        for (int j = 0; j < i; j++)
            localCanvas.drawPath(((GestureStroke)localArrayList.get(j)).toPath(paramInt1 - paramInt3 * 2, paramInt2 - paramInt3 * 2, paramInt4), localPaint);
        return localBitmap;
    }

    public Path toPath()
    {
        return toPath(null);
    }

    public Path toPath(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        return toPath(null, paramInt1, paramInt2, paramInt3, paramInt4);
    }

    public Path toPath(Path paramPath)
    {
        if (paramPath == null)
            paramPath = new Path();
        ArrayList localArrayList = this.mStrokes;
        int i = localArrayList.size();
        for (int j = 0; j < i; j++)
            paramPath.addPath(((GestureStroke)localArrayList.get(j)).getPath());
        return paramPath;
    }

    public Path toPath(Path paramPath, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        if (paramPath == null)
            paramPath = new Path();
        ArrayList localArrayList = this.mStrokes;
        int i = localArrayList.size();
        for (int j = 0; j < i; j++)
            paramPath.addPath(((GestureStroke)localArrayList.get(j)).toPath(paramInt1 - paramInt3 * 2, paramInt2 - paramInt3 * 2, paramInt4));
        return paramPath;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeLong(this.mGestureID);
        int i = 0;
        ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream(32768);
        DataOutputStream localDataOutputStream = new DataOutputStream(localByteArrayOutputStream);
        try
        {
            serialize(localDataOutputStream);
            i = 1;
            GestureUtils.closeStream(localDataOutputStream);
            GestureUtils.closeStream(localByteArrayOutputStream);
            if (i != 0)
                paramParcel.writeByteArray(localByteArrayOutputStream.toByteArray());
            return;
        }
        catch (IOException localIOException)
        {
            while (true)
            {
                Log.e("Gestures", "Error writing Gesture to parcel:", localIOException);
                GestureUtils.closeStream(localDataOutputStream);
                GestureUtils.closeStream(localByteArrayOutputStream);
            }
        }
        finally
        {
            GestureUtils.closeStream(localDataOutputStream);
            GestureUtils.closeStream(localByteArrayOutputStream);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.gesture.Gesture
 * JD-Core Version:        0.6.2
 */