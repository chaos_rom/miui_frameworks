package android.gesture;

import java.util.ArrayList;

class Instance
{
    private static final float[] ORIENTATIONS = arrayOfFloat;
    private static final int PATCH_SAMPLE_SIZE = 16;
    private static final int SEQUENCE_SAMPLE_SIZE = 16;
    final long id;
    final String label;
    final float[] vector;

    static
    {
        float[] arrayOfFloat = new float[10];
        arrayOfFloat[0] = 0.0F;
        arrayOfFloat[1] = 0.7853982F;
        arrayOfFloat[2] = 1.570796F;
        arrayOfFloat[3] = 2.356195F;
        arrayOfFloat[4] = 3.141593F;
        arrayOfFloat[5] = 0.0F;
        arrayOfFloat[6] = -0.7853982F;
        arrayOfFloat[7] = -1.570796F;
        arrayOfFloat[8] = -2.356195F;
        arrayOfFloat[9] = -3.141593F;
    }

    private Instance(long paramLong, float[] paramArrayOfFloat, String paramString)
    {
        this.id = paramLong;
        this.vector = paramArrayOfFloat;
        this.label = paramString;
    }

    static Instance createInstance(int paramInt1, int paramInt2, Gesture paramGesture, String paramString)
    {
        Instance localInstance;
        if (paramInt1 == 2)
        {
            float[] arrayOfFloat2 = temporalSampler(paramInt2, paramGesture);
            localInstance = new Instance(paramGesture.getID(), arrayOfFloat2, paramString);
            localInstance.normalize();
        }
        while (true)
        {
            return localInstance;
            float[] arrayOfFloat1 = spatialSampler(paramGesture);
            localInstance = new Instance(paramGesture.getID(), arrayOfFloat1, paramString);
        }
    }

    private void normalize()
    {
        float[] arrayOfFloat = this.vector;
        float f1 = 0.0F;
        int i = arrayOfFloat.length;
        for (int j = 0; j < i; j++)
            f1 += arrayOfFloat[j] * arrayOfFloat[j];
        float f2 = (float)Math.sqrt(f1);
        for (int k = 0; k < i; k++)
            arrayOfFloat[k] /= f2;
    }

    private static float[] spatialSampler(Gesture paramGesture)
    {
        return GestureUtils.spatialSampling(paramGesture, 16, false);
    }

    private static float[] temporalSampler(int paramInt, Gesture paramGesture)
    {
        float[] arrayOfFloat1 = GestureUtils.temporalSampling((GestureStroke)paramGesture.getStrokes().get(0), 16);
        float[] arrayOfFloat2 = GestureUtils.computeCentroid(arrayOfFloat1);
        float f1 = (float)Math.atan2(arrayOfFloat1[1] - arrayOfFloat2[1], arrayOfFloat1[0] - arrayOfFloat2[0]);
        float f2 = -f1;
        if (paramInt != 1)
        {
            int i = ORIENTATIONS.length;
            for (int j = 0; j < i; j++)
            {
                float f3 = ORIENTATIONS[j] - f1;
                if (Math.abs(f3) < Math.abs(f2))
                    f2 = f3;
            }
        }
        GestureUtils.translate(arrayOfFloat1, -arrayOfFloat2[0], -arrayOfFloat2[1]);
        GestureUtils.rotate(arrayOfFloat1, f2);
        return arrayOfFloat1;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.gesture.Instance
 * JD-Core Version:        0.6.2
 */