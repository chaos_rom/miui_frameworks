package android.gesture;

import android.graphics.RectF;
import android.util.Log;
import java.io.Closeable;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

public final class GestureUtils
{
    private static final float NONUNIFORM_SCALE = 0.0F;
    private static final float SCALING_THRESHOLD = 0.26F;

    static void closeStream(Closeable paramCloseable)
    {
        if (paramCloseable != null);
        try
        {
            paramCloseable.close();
            return;
        }
        catch (IOException localIOException)
        {
            while (true)
                Log.e("Gestures", "Could not close stream", localIOException);
        }
    }

    static float[] computeCentroid(float[] paramArrayOfFloat)
    {
        float f1 = 0.0F;
        float f2 = 0.0F;
        int i = paramArrayOfFloat.length;
        int k;
        for (int j = 0; j < i; j = k + 1)
        {
            f1 += paramArrayOfFloat[j];
            k = j + 1;
            f2 += paramArrayOfFloat[k];
        }
        float[] arrayOfFloat = new float[2];
        arrayOfFloat[0] = (2.0F * f1 / i);
        arrayOfFloat[1] = (2.0F * f2 / i);
        return arrayOfFloat;
    }

    private static float[][] computeCoVariance(float[] paramArrayOfFloat)
    {
        int[] arrayOfInt = new int[2];
        arrayOfInt[0] = 2;
        arrayOfInt[1] = 2;
        float[][] arrayOfFloat = (float[][])Array.newInstance(Float.TYPE, arrayOfInt);
        arrayOfFloat[0][0] = 0.0F;
        arrayOfFloat[0][1] = 0.0F;
        arrayOfFloat[1][0] = 0.0F;
        arrayOfFloat[1][1] = 0.0F;
        int i = paramArrayOfFloat.length;
        int k;
        for (int j = 0; j < i; j = k + 1)
        {
            float f1 = paramArrayOfFloat[j];
            k = j + 1;
            float f2 = paramArrayOfFloat[k];
            float[] arrayOfFloat5 = arrayOfFloat[0];
            arrayOfFloat5[0] += f1 * f1;
            float[] arrayOfFloat6 = arrayOfFloat[0];
            arrayOfFloat6[1] += f1 * f2;
            arrayOfFloat[1][0] = arrayOfFloat[0][1];
            float[] arrayOfFloat7 = arrayOfFloat[1];
            arrayOfFloat7[1] += f2 * f2;
        }
        float[] arrayOfFloat1 = arrayOfFloat[0];
        arrayOfFloat1[0] /= i / 2;
        float[] arrayOfFloat2 = arrayOfFloat[0];
        arrayOfFloat2[1] /= i / 2;
        float[] arrayOfFloat3 = arrayOfFloat[1];
        arrayOfFloat3[0] /= i / 2;
        float[] arrayOfFloat4 = arrayOfFloat[1];
        arrayOfFloat4[1] /= i / 2;
        return arrayOfFloat;
    }

    private static float[] computeOrientation(float[][] paramArrayOfFloat)
    {
        float[] arrayOfFloat = new float[2];
        if ((paramArrayOfFloat[0][1] == 0.0F) || (paramArrayOfFloat[1][0] == 0.0F))
        {
            arrayOfFloat[0] = 1.0F;
            arrayOfFloat[1] = 0.0F;
        }
        float f1 = -paramArrayOfFloat[0][0] - paramArrayOfFloat[1][1];
        float f2 = paramArrayOfFloat[0][0] * paramArrayOfFloat[1][1] - paramArrayOfFloat[0][1] * paramArrayOfFloat[1][0];
        float f3 = f1 / 2.0F;
        float f4 = (float)Math.sqrt(Math.pow(f3, 2.0D) - f2);
        float f5 = f4 + -f3;
        float f6 = -f3 - f4;
        if (f5 == f6)
        {
            arrayOfFloat[0] = 0.0F;
            arrayOfFloat[1] = 0.0F;
            return arrayOfFloat;
        }
        if (f5 > f6);
        for (float f7 = f5; ; f7 = f6)
        {
            arrayOfFloat[0] = 1.0F;
            arrayOfFloat[1] = ((f7 - paramArrayOfFloat[0][0]) / paramArrayOfFloat[0][1]);
            break;
        }
    }

    public static OrientedBoundingBox computeOrientedBoundingBox(ArrayList<GesturePoint> paramArrayList)
    {
        int i = paramArrayList.size();
        float[] arrayOfFloat = new float[i * 2];
        for (int j = 0; j < i; j++)
        {
            GesturePoint localGesturePoint = (GesturePoint)paramArrayList.get(j);
            int k = j * 2;
            arrayOfFloat[k] = localGesturePoint.x;
            arrayOfFloat[(k + 1)] = localGesturePoint.y;
        }
        return computeOrientedBoundingBox(arrayOfFloat, computeCentroid(arrayOfFloat));
    }

    public static OrientedBoundingBox computeOrientedBoundingBox(float[] paramArrayOfFloat)
    {
        int i = paramArrayOfFloat.length;
        float[] arrayOfFloat = new float[i];
        for (int j = 0; j < i; j++)
            arrayOfFloat[j] = paramArrayOfFloat[j];
        return computeOrientedBoundingBox(arrayOfFloat, computeCentroid(arrayOfFloat));
    }

    private static OrientedBoundingBox computeOrientedBoundingBox(float[] paramArrayOfFloat1, float[] paramArrayOfFloat2)
    {
        translate(paramArrayOfFloat1, -paramArrayOfFloat2[0], -paramArrayOfFloat2[1]);
        float[] arrayOfFloat = computeOrientation(computeCoVariance(paramArrayOfFloat1));
        float f1;
        if ((arrayOfFloat[0] == 0.0F) && (arrayOfFloat[1] == 0.0F))
            f1 = -1.570796F;
        float f2;
        float f3;
        float f4;
        float f5;
        while (true)
        {
            f2 = 3.4028235E+38F;
            f3 = 3.4028235E+38F;
            f4 = 1.4E-45F;
            f5 = 1.4E-45F;
            int i = paramArrayOfFloat1.length;
            int k;
            for (int j = 0; j < i; j = k + 1)
            {
                if (paramArrayOfFloat1[j] < f2)
                    f2 = paramArrayOfFloat1[j];
                if (paramArrayOfFloat1[j] > f4)
                    f4 = paramArrayOfFloat1[j];
                k = j + 1;
                if (paramArrayOfFloat1[k] < f3)
                    f3 = paramArrayOfFloat1[k];
                if (paramArrayOfFloat1[k] > f5)
                    f5 = paramArrayOfFloat1[k];
            }
            f1 = (float)Math.atan2(arrayOfFloat[1], arrayOfFloat[0]);
            rotate(paramArrayOfFloat1, -f1);
        }
        return new OrientedBoundingBox((float)(180.0F * f1 / 3.141592653589793D), paramArrayOfFloat2[0], paramArrayOfFloat2[1], f4 - f2, f5 - f3);
    }

    static float computeStraightness(float[] paramArrayOfFloat)
    {
        float f1 = computeTotalLength(paramArrayOfFloat);
        float f2 = paramArrayOfFloat[2] - paramArrayOfFloat[0];
        float f3 = paramArrayOfFloat[3] - paramArrayOfFloat[1];
        return (float)Math.sqrt(f2 * f2 + f3 * f3) / f1;
    }

    static float computeStraightness(float[] paramArrayOfFloat, float paramFloat)
    {
        float f1 = paramArrayOfFloat[2] - paramArrayOfFloat[0];
        float f2 = paramArrayOfFloat[3] - paramArrayOfFloat[1];
        return (float)Math.sqrt(f1 * f1 + f2 * f2) / paramFloat;
    }

    static float computeTotalLength(float[] paramArrayOfFloat)
    {
        float f1 = 0.0F;
        int i = -4 + paramArrayOfFloat.length;
        for (int j = 0; j < i; j += 2)
        {
            float f2 = paramArrayOfFloat[(j + 2)] - paramArrayOfFloat[j];
            float f3 = paramArrayOfFloat[(j + 3)] - paramArrayOfFloat[(j + 1)];
            f1 = (float)(f1 + Math.sqrt(f2 * f2 + f3 * f3));
        }
        return f1;
    }

    static float cosineDistance(float[] paramArrayOfFloat1, float[] paramArrayOfFloat2)
    {
        float f = 0.0F;
        int i = paramArrayOfFloat1.length;
        for (int j = 0; j < i; j++)
            f += paramArrayOfFloat1[j] * paramArrayOfFloat2[j];
        return (float)Math.acos(f);
    }

    static float minimumCosineDistance(float[] paramArrayOfFloat1, float[] paramArrayOfFloat2, int paramInt)
    {
        int i = paramArrayOfFloat1.length;
        float f1 = 0.0F;
        float f2 = 0.0F;
        for (int j = 0; j < i; j += 2)
        {
            f1 += paramArrayOfFloat1[j] * paramArrayOfFloat2[j] + paramArrayOfFloat1[(j + 1)] * paramArrayOfFloat2[(j + 1)];
            f2 += paramArrayOfFloat1[j] * paramArrayOfFloat2[(j + 1)] - paramArrayOfFloat1[(j + 1)] * paramArrayOfFloat2[j];
        }
        float f4;
        double d1;
        float f3;
        if (f1 != 0.0F)
        {
            f4 = f2 / f1;
            d1 = Math.atan(f4);
            if ((paramInt > 2) && (Math.abs(d1) >= 3.141592653589793D / paramInt))
                f3 = (float)Math.acos(f1);
        }
        while (true)
        {
            return f3;
            double d2 = Math.cos(d1);
            double d3 = d2 * f4;
            f3 = (float)Math.acos(d2 * f1 + d3 * f2);
            continue;
            f3 = 1.570796F;
        }
    }

    private static void plot(float paramFloat1, float paramFloat2, float[] paramArrayOfFloat, int paramInt)
    {
        if (paramFloat1 < 0.0F)
            paramFloat1 = 0.0F;
        if (paramFloat2 < 0.0F)
            paramFloat2 = 0.0F;
        int i = (int)Math.floor(paramFloat1);
        int j = (int)Math.ceil(paramFloat1);
        int k = (int)Math.floor(paramFloat2);
        int m = (int)Math.ceil(paramFloat2);
        if ((paramFloat1 == i) && (paramFloat2 == k))
        {
            int i4 = j + m * paramInt;
            if (paramArrayOfFloat[i4] < 1.0F)
                paramArrayOfFloat[i4] = 1.0F;
        }
        while (true)
        {
            return;
            double d1 = Math.pow(i - paramFloat1, 2.0D);
            double d2 = Math.pow(k - paramFloat2, 2.0D);
            double d3 = Math.pow(j - paramFloat1, 2.0D);
            double d4 = Math.pow(m - paramFloat2, 2.0D);
            float f1 = (float)Math.sqrt(d1 + d2);
            float f2 = (float)Math.sqrt(d3 + d2);
            float f3 = (float)Math.sqrt(d1 + d4);
            float f4 = (float)Math.sqrt(d3 + d4);
            float f5 = f4 + (f3 + (f1 + f2));
            float f6 = f1 / f5;
            int n = i + k * paramInt;
            if (f6 > paramArrayOfFloat[n])
                paramArrayOfFloat[n] = f6;
            float f7 = f2 / f5;
            int i1 = j + k * paramInt;
            if (f7 > paramArrayOfFloat[i1])
                paramArrayOfFloat[i1] = f7;
            float f8 = f3 / f5;
            int i2 = i + m * paramInt;
            if (f8 > paramArrayOfFloat[i2])
                paramArrayOfFloat[i2] = f8;
            float f9 = f4 / f5;
            int i3 = j + m * paramInt;
            if (f9 > paramArrayOfFloat[i3])
                paramArrayOfFloat[i3] = f9;
        }
    }

    static float[] rotate(float[] paramArrayOfFloat, float paramFloat)
    {
        float f1 = (float)Math.cos(paramFloat);
        float f2 = (float)Math.sin(paramFloat);
        int i = paramArrayOfFloat.length;
        for (int j = 0; j < i; j += 2)
        {
            float f3 = f1 * paramArrayOfFloat[j] - f2 * paramArrayOfFloat[(j + 1)];
            float f4 = f2 * paramArrayOfFloat[j] + f1 * paramArrayOfFloat[(j + 1)];
            paramArrayOfFloat[j] = f3;
            paramArrayOfFloat[(j + 1)] = f4;
        }
        return paramArrayOfFloat;
    }

    static float[] scale(float[] paramArrayOfFloat, float paramFloat1, float paramFloat2)
    {
        int i = paramArrayOfFloat.length;
        for (int j = 0; j < i; j += 2)
        {
            paramArrayOfFloat[j] = (paramFloat1 * paramArrayOfFloat[j]);
            int k = j + 1;
            paramArrayOfFloat[k] = (paramFloat2 * paramArrayOfFloat[k]);
        }
        return paramArrayOfFloat;
    }

    public static float[] spatialSampling(Gesture paramGesture, int paramInt)
    {
        return spatialSampling(paramGesture, paramInt, false);
    }

    public static float[] spatialSampling(Gesture paramGesture, int paramInt, boolean paramBoolean)
    {
        float f1 = paramInt - 1;
        float[] arrayOfFloat1 = new float[paramInt * paramInt];
        Arrays.fill(arrayOfFloat1, 0.0F);
        RectF localRectF = paramGesture.getBoundingBox();
        float f2 = localRectF.width();
        float f3 = localRectF.height();
        float f4 = f1 / f2;
        float f5 = f1 / f3;
        float f28;
        label74: float f8;
        float f9;
        float f10;
        float f11;
        ArrayList localArrayList;
        int i;
        if (paramBoolean)
            if (f4 < f5)
            {
                f28 = f4;
                f4 = f28;
                f5 = f28;
                f8 = -localRectF.centerX();
                f9 = -localRectF.centerY();
                f10 = f1 / 2.0F;
                f11 = f1 / 2.0F;
                localArrayList = paramGesture.getStrokes();
                i = localArrayList.size();
            }
        for (int j = 0; ; j++)
        {
            if (j >= i)
                break label724;
            float[] arrayOfFloat2 = ((GestureStroke)localArrayList.get(j)).points;
            int k = arrayOfFloat2.length;
            float[] arrayOfFloat3 = new float[k];
            int m = 0;
            while (true)
                if (m < k)
                {
                    arrayOfFloat3[m] = (f10 + f4 * (f8 + arrayOfFloat2[m]));
                    arrayOfFloat3[(m + 1)] = (f11 + f5 * (f9 + arrayOfFloat2[(m + 1)]));
                    m += 2;
                    continue;
                    f28 = f5;
                    break;
                    float f6 = f2 / f3;
                    if (f6 > 1.0F)
                        f6 = 1.0F / f6;
                    if (f6 < 0.26F)
                    {
                        if (f4 < f5);
                        for (float f27 = f4; ; f27 = f5)
                        {
                            f4 = f27;
                            f5 = f27;
                            break;
                        }
                    }
                    if (f4 > f5)
                    {
                        float f26 = f5 * NONUNIFORM_SCALE;
                        if (f26 >= f4)
                            break label74;
                        f4 = f26;
                        break label74;
                    }
                    float f7 = f4 * NONUNIFORM_SCALE;
                    if (f7 >= f5)
                        break label74;
                    f5 = f7;
                    break label74;
                }
            float f12 = -1.0F;
            float f13 = -1.0F;
            for (int n = 0; n < k; n += 2)
            {
                float f14;
                if (arrayOfFloat3[n] < 0.0F)
                {
                    f14 = 0.0F;
                    if (arrayOfFloat3[(n + 1)] >= 0.0F)
                        break label490;
                }
                label490: for (float f15 = 0.0F; ; f15 = arrayOfFloat3[(n + 1)])
                {
                    if (f14 > f1)
                        f14 = f1;
                    if (f15 > f1)
                        f15 = f1;
                    plot(f14, f15, arrayOfFloat1, paramInt);
                    if (f12 == -1.0F)
                        break label704;
                    if (f12 <= f14)
                        break label502;
                    float f23 = (float)Math.ceil(f14);
                    float f24 = (f13 - f15) / (f12 - f14);
                    while (f23 < f12)
                    {
                        float f25 = f15 + f24 * (f23 - f14);
                        plot(f23, f25, arrayOfFloat1, paramInt);
                        f23 += 1.0F;
                    }
                    f14 = arrayOfFloat3[n];
                    break;
                }
                label502: if (f12 < f14)
                {
                    float f20 = (float)Math.ceil(f12);
                    float f21 = (f13 - f15) / (f12 - f14);
                    while (f20 < f14)
                    {
                        float f22 = f15 + f21 * (f20 - f14);
                        plot(f20, f22, arrayOfFloat1, paramInt);
                        f20 += 1.0F;
                    }
                }
                if (f13 > f15)
                {
                    float f18 = (float)Math.ceil(f15);
                    float f19 = (f12 - f14) / (f13 - f15);
                    while (f18 < f13)
                    {
                        plot(f14 + f19 * (f18 - f15), f18, arrayOfFloat1, paramInt);
                        f18 += 1.0F;
                    }
                }
                if (f13 < f15)
                {
                    float f16 = (float)Math.ceil(f13);
                    float f17 = (f12 - f14) / (f13 - f15);
                    while (f16 < f15)
                    {
                        plot(f14 + f17 * (f16 - f15), f16, arrayOfFloat1, paramInt);
                        f16 += 1.0F;
                    }
                }
                label704: f12 = f14;
                f13 = f15;
            }
        }
        label724: return arrayOfFloat1;
    }

    static float squaredEuclideanDistance(float[] paramArrayOfFloat1, float[] paramArrayOfFloat2)
    {
        float f1 = 0.0F;
        int i = paramArrayOfFloat1.length;
        for (int j = 0; j < i; j++)
        {
            float f2 = paramArrayOfFloat1[j] - paramArrayOfFloat2[j];
            f1 += f2 * f2;
        }
        return f1 / i;
    }

    public static float[] temporalSampling(GestureStroke paramGestureStroke, int paramInt)
    {
        float f1 = paramGestureStroke.length / (paramInt - 1);
        int i = paramInt * 2;
        float[] arrayOfFloat1 = new float[i];
        float f2 = 0.0F;
        float[] arrayOfFloat2 = paramGestureStroke.points;
        float f3 = arrayOfFloat2[0];
        float f4 = arrayOfFloat2[1];
        float f5 = 1.4E-45F;
        float f6 = 1.4E-45F;
        arrayOfFloat1[0] = f3;
        int j = 0 + 1;
        arrayOfFloat1[j] = f4;
        int k = j + 1;
        int m = 0;
        int n = arrayOfFloat2.length / 2;
        while (true)
        {
            if (m < n)
            {
                if (f5 != 1.4E-45F)
                    break label159;
                m++;
                if (m < n);
            }
            else
            {
                for (int i1 = k; i1 < i; i1 += 2)
                {
                    arrayOfFloat1[i1] = f3;
                    arrayOfFloat1[(i1 + 1)] = f4;
                }
            }
            f5 = arrayOfFloat2[(m * 2)];
            f6 = arrayOfFloat2[(1 + m * 2)];
            label159: float f7 = f5 - f3;
            float f8 = f6 - f4;
            float f9 = (float)Math.sqrt(f7 * f7 + f8 * f8);
            if (f2 + f9 >= f1)
            {
                float f10 = (f1 - f2) / f9;
                float f11 = f3 + f10 * f7;
                float f12 = f4 + f10 * f8;
                arrayOfFloat1[k] = f11;
                int i2 = k + 1;
                arrayOfFloat1[i2] = f12;
                k = i2 + 1;
                f3 = f11;
                f4 = f12;
                f2 = 0.0F;
            }
            else
            {
                f3 = f5;
                f4 = f6;
                f5 = 1.4E-45F;
                f6 = 1.4E-45F;
                f2 += f9;
            }
        }
        return arrayOfFloat1;
    }

    static float[] translate(float[] paramArrayOfFloat, float paramFloat1, float paramFloat2)
    {
        int i = paramArrayOfFloat.length;
        for (int j = 0; j < i; j += 2)
        {
            paramArrayOfFloat[j] = (paramFloat1 + paramArrayOfFloat[j]);
            int k = j + 1;
            paramArrayOfFloat[k] = (paramFloat2 + paramArrayOfFloat[k]);
        }
        return paramArrayOfFloat;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.gesture.GestureUtils
 * JD-Core Version:        0.6.2
 */