package android.gesture;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Join;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import com.android.internal.R.styleable;
import java.util.ArrayList;

public class GestureOverlayView extends FrameLayout
{
    private static final boolean DITHER_FLAG = true;
    private static final int FADE_ANIMATION_RATE = 16;
    private static final boolean GESTURE_RENDERING_ANTIALIAS = true;
    public static final int GESTURE_STROKE_TYPE_MULTIPLE = 1;
    public static final int GESTURE_STROKE_TYPE_SINGLE = 0;
    public static final int ORIENTATION_HORIZONTAL = 0;
    public static final int ORIENTATION_VERTICAL = 1;
    private int mCertainGestureColor = -256;
    private int mCurrentColor;
    private Gesture mCurrentGesture;
    private float mCurveEndX;
    private float mCurveEndY;
    private long mFadeDuration = 150L;
    private boolean mFadeEnabled = true;
    private long mFadeOffset = 420L;
    private float mFadingAlpha = 1.0F;
    private boolean mFadingHasStarted;
    private final FadeOutRunnable mFadingOut = new FadeOutRunnable(null);
    private long mFadingStart;
    private final Paint mGesturePaint = new Paint();
    private float mGestureStrokeAngleThreshold = 40.0F;
    private float mGestureStrokeLengthThreshold = 50.0F;
    private float mGestureStrokeSquarenessTreshold = 0.275F;
    private int mGestureStrokeType = 0;
    private float mGestureStrokeWidth = 12.0F;
    private boolean mGestureVisible = true;
    private boolean mHandleGestureActions;
    private boolean mInterceptEvents = true;
    private final AccelerateDecelerateInterpolator mInterpolator = new AccelerateDecelerateInterpolator();
    private final Rect mInvalidRect = new Rect();
    private int mInvalidateExtraBorder = 10;
    private boolean mIsFadingOut = false;
    private boolean mIsGesturing = false;
    private boolean mIsListeningForGestures;
    private final ArrayList<OnGestureListener> mOnGestureListeners = new ArrayList();
    private final ArrayList<OnGesturePerformedListener> mOnGesturePerformedListeners = new ArrayList();
    private final ArrayList<OnGesturingListener> mOnGesturingListeners = new ArrayList();
    private int mOrientation = 1;
    private final Path mPath = new Path();
    private boolean mPreviousWasGesturing = false;
    private boolean mResetGesture;
    private final ArrayList<GesturePoint> mStrokeBuffer = new ArrayList(100);
    private float mTotalLength;
    private int mUncertainGestureColor = 1224736512;
    private float mX;
    private float mY;

    public GestureOverlayView(Context paramContext)
    {
        super(paramContext);
        init();
    }

    public GestureOverlayView(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 16843711);
    }

    public GestureOverlayView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.GestureOverlayView, paramInt, 0);
        this.mGestureStrokeWidth = localTypedArray.getFloat(1, this.mGestureStrokeWidth);
        this.mInvalidateExtraBorder = Math.max(1, -1 + (int)this.mGestureStrokeWidth);
        this.mCertainGestureColor = localTypedArray.getColor(2, this.mCertainGestureColor);
        this.mUncertainGestureColor = localTypedArray.getColor(3, this.mUncertainGestureColor);
        this.mFadeDuration = localTypedArray.getInt(5, (int)this.mFadeDuration);
        this.mFadeOffset = localTypedArray.getInt(4, (int)this.mFadeOffset);
        this.mGestureStrokeType = localTypedArray.getInt(6, this.mGestureStrokeType);
        this.mGestureStrokeLengthThreshold = localTypedArray.getFloat(7, this.mGestureStrokeLengthThreshold);
        this.mGestureStrokeAngleThreshold = localTypedArray.getFloat(9, this.mGestureStrokeAngleThreshold);
        this.mGestureStrokeSquarenessTreshold = localTypedArray.getFloat(8, this.mGestureStrokeSquarenessTreshold);
        this.mInterceptEvents = localTypedArray.getBoolean(10, this.mInterceptEvents);
        this.mFadeEnabled = localTypedArray.getBoolean(11, this.mFadeEnabled);
        this.mOrientation = localTypedArray.getInt(0, this.mOrientation);
        localTypedArray.recycle();
        init();
    }

    private void cancelGesture(MotionEvent paramMotionEvent)
    {
        ArrayList localArrayList = this.mOnGestureListeners;
        int i = localArrayList.size();
        for (int j = 0; j < i; j++)
            ((OnGestureListener)localArrayList.get(j)).onGestureCancelled(this, paramMotionEvent);
        clear(false);
    }

    private void clear(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
    {
        setPaintAlpha(255);
        removeCallbacks(this.mFadingOut);
        this.mResetGesture = false;
        this.mFadingOut.fireActionPerformed = paramBoolean2;
        this.mFadingOut.resetMultipleStrokes = false;
        if ((paramBoolean1) && (this.mCurrentGesture != null))
        {
            this.mFadingAlpha = 1.0F;
            this.mIsFadingOut = true;
            this.mFadingHasStarted = false;
            this.mFadingStart = (AnimationUtils.currentAnimationTimeMillis() + this.mFadeOffset);
            postDelayed(this.mFadingOut, this.mFadeOffset);
        }
        while (true)
        {
            return;
            this.mFadingAlpha = 1.0F;
            this.mIsFadingOut = false;
            this.mFadingHasStarted = false;
            if (paramBoolean3)
            {
                this.mCurrentGesture = null;
                this.mPath.rewind();
                invalidate();
            }
            else if (paramBoolean2)
            {
                postDelayed(this.mFadingOut, this.mFadeOffset);
            }
            else if (this.mGestureStrokeType == 1)
            {
                this.mFadingOut.resetMultipleStrokes = true;
                postDelayed(this.mFadingOut, this.mFadeOffset);
            }
            else
            {
                this.mCurrentGesture = null;
                this.mPath.rewind();
                invalidate();
            }
        }
    }

    private void fireOnGesturePerformed()
    {
        ArrayList localArrayList = this.mOnGesturePerformedListeners;
        int i = localArrayList.size();
        for (int j = 0; j < i; j++)
            ((OnGesturePerformedListener)localArrayList.get(j)).onGesturePerformed(this, this.mCurrentGesture);
    }

    private void init()
    {
        setWillNotDraw(false);
        Paint localPaint = this.mGesturePaint;
        localPaint.setAntiAlias(true);
        localPaint.setColor(this.mCertainGestureColor);
        localPaint.setStyle(Paint.Style.STROKE);
        localPaint.setStrokeJoin(Paint.Join.ROUND);
        localPaint.setStrokeCap(Paint.Cap.ROUND);
        localPaint.setStrokeWidth(this.mGestureStrokeWidth);
        localPaint.setDither(true);
        this.mCurrentColor = this.mCertainGestureColor;
        setPaintAlpha(255);
    }

    private boolean processEvent(MotionEvent paramMotionEvent)
    {
        boolean bool = true;
        switch (paramMotionEvent.getAction())
        {
        default:
            bool = false;
        case 0:
        case 2:
        case 1:
        case 3:
        }
        while (true)
        {
            return bool;
            touchDown(paramMotionEvent);
            invalidate();
            continue;
            if (!this.mIsListeningForGestures)
                break;
            Rect localRect = touchMove(paramMotionEvent);
            if (localRect != null)
            {
                invalidate(localRect);
                continue;
                if (!this.mIsListeningForGestures)
                    break;
                touchUp(paramMotionEvent, false);
                invalidate();
                continue;
                if (!this.mIsListeningForGestures)
                    break;
                touchUp(paramMotionEvent, bool);
                invalidate();
            }
        }
    }

    private void setCurrentColor(int paramInt)
    {
        this.mCurrentColor = paramInt;
        if (this.mFadingHasStarted)
            setPaintAlpha((int)(255.0F * this.mFadingAlpha));
        while (true)
        {
            invalidate();
            return;
            setPaintAlpha(255);
        }
    }

    private void setPaintAlpha(int paramInt)
    {
        int i = (paramInt + (paramInt >> 7)) * (this.mCurrentColor >>> 24) >> 8;
        this.mGesturePaint.setColor(this.mCurrentColor << 8 >>> 8 | i << 24);
    }

    private void touchDown(MotionEvent paramMotionEvent)
    {
        this.mIsListeningForGestures = true;
        float f1 = paramMotionEvent.getX();
        float f2 = paramMotionEvent.getY();
        this.mX = f1;
        this.mY = f2;
        this.mTotalLength = 0.0F;
        this.mIsGesturing = false;
        if ((this.mGestureStrokeType == 0) || (this.mResetGesture))
        {
            if (this.mHandleGestureActions)
                setCurrentColor(this.mUncertainGestureColor);
            this.mResetGesture = false;
            this.mCurrentGesture = null;
            this.mPath.rewind();
            if (!this.mFadingHasStarted)
                break label264;
            cancelClearAnimation();
        }
        while (true)
        {
            if (this.mCurrentGesture == null)
                this.mCurrentGesture = new Gesture();
            this.mStrokeBuffer.add(new GesturePoint(f1, f2, paramMotionEvent.getEventTime()));
            this.mPath.moveTo(f1, f2);
            int i = this.mInvalidateExtraBorder;
            this.mInvalidRect.set((int)f1 - i, (int)f2 - i, i + (int)f1, i + (int)f2);
            this.mCurveEndX = f1;
            this.mCurveEndY = f2;
            ArrayList localArrayList = this.mOnGestureListeners;
            int j = localArrayList.size();
            for (int k = 0; k < j; k++)
                ((OnGestureListener)localArrayList.get(k)).onGestureStarted(this, paramMotionEvent);
            if (((this.mCurrentGesture != null) && (this.mCurrentGesture.getStrokesCount() != 0)) || (!this.mHandleGestureActions))
                break;
            setCurrentColor(this.mUncertainGestureColor);
            break;
            label264: if (this.mIsFadingOut)
            {
                setPaintAlpha(255);
                this.mIsFadingOut = false;
                this.mFadingHasStarted = false;
                removeCallbacks(this.mFadingOut);
            }
        }
    }

    private Rect touchMove(MotionEvent paramMotionEvent)
    {
        Rect localRect = null;
        float f1 = paramMotionEvent.getX();
        float f2 = paramMotionEvent.getY();
        float f3 = this.mX;
        float f4 = this.mY;
        float f5 = Math.abs(f1 - f3);
        float f6 = Math.abs(f2 - f4);
        if ((f5 >= 3.0F) || (f6 >= 3.0F))
        {
            localRect = this.mInvalidRect;
            int i = this.mInvalidateExtraBorder;
            localRect.set((int)this.mCurveEndX - i, (int)this.mCurveEndY - i, i + (int)this.mCurveEndX, i + (int)this.mCurveEndY);
            float f7 = (f1 + f3) / 2.0F;
            this.mCurveEndX = f7;
            float f8 = (f2 + f4) / 2.0F;
            this.mCurveEndY = f8;
            this.mPath.quadTo(f3, f4, f7, f8);
            localRect.union((int)f3 - i, (int)f4 - i, i + (int)f3, i + (int)f4);
            localRect.union((int)f7 - i, (int)f8 - i, i + (int)f7, i + (int)f8);
            this.mX = f1;
            this.mY = f2;
            ArrayList localArrayList1 = this.mStrokeBuffer;
            GesturePoint localGesturePoint = new GesturePoint(f1, f2, paramMotionEvent.getEventTime());
            localArrayList1.add(localGesturePoint);
            if ((this.mHandleGestureActions) && (!this.mIsGesturing))
            {
                this.mTotalLength += (float)Math.sqrt(f5 * f5 + f6 * f6);
                if (this.mTotalLength > this.mGestureStrokeLengthThreshold)
                {
                    OrientedBoundingBox localOrientedBoundingBox = GestureUtils.computeOrientedBoundingBox(this.mStrokeBuffer);
                    float f9 = Math.abs(localOrientedBoundingBox.orientation);
                    if (f9 > 90.0F)
                        f9 = 180.0F - f9;
                    if (localOrientedBoundingBox.squareness <= this.mGestureStrokeSquarenessTreshold)
                    {
                        if (this.mOrientation != 1)
                            break label426;
                        if (f9 >= this.mGestureStrokeAngleThreshold)
                            break label436;
                    }
                    label426: 
                    while (f9 > this.mGestureStrokeAngleThreshold)
                    {
                        this.mIsGesturing = true;
                        setCurrentColor(this.mCertainGestureColor);
                        ArrayList localArrayList3 = this.mOnGesturingListeners;
                        int m = localArrayList3.size();
                        for (int n = 0; n < m; n++)
                            ((OnGesturingListener)localArrayList3.get(n)).onGesturingStarted(this);
                    }
                }
            }
            label436: ArrayList localArrayList2 = this.mOnGestureListeners;
            int j = localArrayList2.size();
            for (int k = 0; k < j; k++)
                ((OnGestureListener)localArrayList2.get(k)).onGesture(this, paramMotionEvent);
        }
        return localRect;
    }

    private void touchUp(MotionEvent paramMotionEvent, boolean paramBoolean)
    {
        boolean bool1 = true;
        this.mIsListeningForGestures = false;
        boolean bool2;
        if (this.mCurrentGesture != null)
        {
            this.mCurrentGesture.addStroke(new GestureStroke(this.mStrokeBuffer));
            if (!paramBoolean)
            {
                ArrayList localArrayList2 = this.mOnGestureListeners;
                int k = localArrayList2.size();
                for (int m = 0; m < k; m++)
                    ((OnGestureListener)localArrayList2.get(m)).onGestureEnded(this, paramMotionEvent);
                if ((this.mHandleGestureActions) && (this.mFadeEnabled))
                {
                    bool2 = bool1;
                    if ((!this.mHandleGestureActions) || (!this.mIsGesturing))
                        break label192;
                    label113: clear(bool2, bool1, false);
                }
            }
        }
        while (true)
        {
            this.mStrokeBuffer.clear();
            this.mPreviousWasGesturing = this.mIsGesturing;
            this.mIsGesturing = false;
            ArrayList localArrayList1 = this.mOnGesturingListeners;
            int i = localArrayList1.size();
            for (int j = 0; j < i; j++)
                ((OnGesturingListener)localArrayList1.get(j)).onGesturingEnded(this);
            bool2 = false;
            break;
            label192: bool1 = false;
            break label113;
            cancelGesture(paramMotionEvent);
            continue;
            cancelGesture(paramMotionEvent);
        }
    }

    public void addOnGestureListener(OnGestureListener paramOnGestureListener)
    {
        this.mOnGestureListeners.add(paramOnGestureListener);
    }

    public void addOnGesturePerformedListener(OnGesturePerformedListener paramOnGesturePerformedListener)
    {
        this.mOnGesturePerformedListeners.add(paramOnGesturePerformedListener);
        if (this.mOnGesturePerformedListeners.size() > 0)
            this.mHandleGestureActions = true;
    }

    public void addOnGesturingListener(OnGesturingListener paramOnGesturingListener)
    {
        this.mOnGesturingListeners.add(paramOnGesturingListener);
    }

    public void cancelClearAnimation()
    {
        setPaintAlpha(255);
        this.mIsFadingOut = false;
        this.mFadingHasStarted = false;
        removeCallbacks(this.mFadingOut);
        this.mPath.rewind();
        this.mCurrentGesture = null;
    }

    public void cancelGesture()
    {
        this.mIsListeningForGestures = false;
        this.mCurrentGesture.addStroke(new GestureStroke(this.mStrokeBuffer));
        long l = SystemClock.uptimeMillis();
        MotionEvent localMotionEvent = MotionEvent.obtain(l, l, 3, 0.0F, 0.0F, 0);
        ArrayList localArrayList1 = this.mOnGestureListeners;
        int i = localArrayList1.size();
        for (int j = 0; j < i; j++)
            ((OnGestureListener)localArrayList1.get(j)).onGestureCancelled(this, localMotionEvent);
        localMotionEvent.recycle();
        clear(false);
        this.mIsGesturing = false;
        this.mPreviousWasGesturing = false;
        this.mStrokeBuffer.clear();
        ArrayList localArrayList2 = this.mOnGesturingListeners;
        int k = localArrayList2.size();
        for (int m = 0; m < k; m++)
            ((OnGesturingListener)localArrayList2.get(m)).onGesturingEnded(this);
    }

    public void clear(boolean paramBoolean)
    {
        clear(paramBoolean, false, true);
    }

    public boolean dispatchTouchEvent(MotionEvent paramMotionEvent)
    {
        boolean bool1 = true;
        boolean bool2;
        if (isEnabled())
            if (((this.mIsGesturing) || ((this.mCurrentGesture != null) && (this.mCurrentGesture.getStrokesCount() > 0) && (this.mPreviousWasGesturing))) && (this.mInterceptEvents))
            {
                bool2 = bool1;
                processEvent(paramMotionEvent);
                if (bool2)
                    paramMotionEvent.setAction(3);
                super.dispatchTouchEvent(paramMotionEvent);
            }
        while (true)
        {
            return bool1;
            bool2 = false;
            break;
            bool1 = super.dispatchTouchEvent(paramMotionEvent);
        }
    }

    public void draw(Canvas paramCanvas)
    {
        super.draw(paramCanvas);
        if ((this.mCurrentGesture != null) && (this.mGestureVisible))
            paramCanvas.drawPath(this.mPath, this.mGesturePaint);
    }

    public ArrayList<GesturePoint> getCurrentStroke()
    {
        return this.mStrokeBuffer;
    }

    public long getFadeOffset()
    {
        return this.mFadeOffset;
    }

    public Gesture getGesture()
    {
        return this.mCurrentGesture;
    }

    public int getGestureColor()
    {
        return this.mCertainGestureColor;
    }

    public Paint getGesturePaint()
    {
        return this.mGesturePaint;
    }

    public Path getGesturePath()
    {
        return this.mPath;
    }

    public Path getGesturePath(Path paramPath)
    {
        paramPath.set(this.mPath);
        return paramPath;
    }

    public float getGestureStrokeAngleThreshold()
    {
        return this.mGestureStrokeAngleThreshold;
    }

    public float getGestureStrokeLengthThreshold()
    {
        return this.mGestureStrokeLengthThreshold;
    }

    public float getGestureStrokeSquarenessTreshold()
    {
        return this.mGestureStrokeSquarenessTreshold;
    }

    public int getGestureStrokeType()
    {
        return this.mGestureStrokeType;
    }

    public float getGestureStrokeWidth()
    {
        return this.mGestureStrokeWidth;
    }

    public int getOrientation()
    {
        return this.mOrientation;
    }

    public int getUncertainGestureColor()
    {
        return this.mUncertainGestureColor;
    }

    public boolean isEventsInterceptionEnabled()
    {
        return this.mInterceptEvents;
    }

    public boolean isFadeEnabled()
    {
        return this.mFadeEnabled;
    }

    public boolean isGestureVisible()
    {
        return this.mGestureVisible;
    }

    public boolean isGesturing()
    {
        return this.mIsGesturing;
    }

    protected void onDetachedFromWindow()
    {
        cancelClearAnimation();
    }

    public void removeAllOnGestureListeners()
    {
        this.mOnGestureListeners.clear();
    }

    public void removeAllOnGesturePerformedListeners()
    {
        this.mOnGesturePerformedListeners.clear();
        this.mHandleGestureActions = false;
    }

    public void removeAllOnGesturingListeners()
    {
        this.mOnGesturingListeners.clear();
    }

    public void removeOnGestureListener(OnGestureListener paramOnGestureListener)
    {
        this.mOnGestureListeners.remove(paramOnGestureListener);
    }

    public void removeOnGesturePerformedListener(OnGesturePerformedListener paramOnGesturePerformedListener)
    {
        this.mOnGesturePerformedListeners.remove(paramOnGesturePerformedListener);
        if (this.mOnGesturePerformedListeners.size() <= 0)
            this.mHandleGestureActions = false;
    }

    public void removeOnGesturingListener(OnGesturingListener paramOnGesturingListener)
    {
        this.mOnGesturingListeners.remove(paramOnGesturingListener);
    }

    public void setEventsInterceptionEnabled(boolean paramBoolean)
    {
        this.mInterceptEvents = paramBoolean;
    }

    public void setFadeEnabled(boolean paramBoolean)
    {
        this.mFadeEnabled = paramBoolean;
    }

    public void setFadeOffset(long paramLong)
    {
        this.mFadeOffset = paramLong;
    }

    public void setGesture(Gesture paramGesture)
    {
        if (this.mCurrentGesture != null)
            clear(false);
        setCurrentColor(this.mCertainGestureColor);
        this.mCurrentGesture = paramGesture;
        Path localPath = this.mCurrentGesture.toPath();
        RectF localRectF = new RectF();
        localPath.computeBounds(localRectF, true);
        this.mPath.rewind();
        this.mPath.addPath(localPath, -localRectF.left + (getWidth() - localRectF.width()) / 2.0F, -localRectF.top + (getHeight() - localRectF.height()) / 2.0F);
        this.mResetGesture = true;
        invalidate();
    }

    public void setGestureColor(int paramInt)
    {
        this.mCertainGestureColor = paramInt;
    }

    public void setGestureStrokeAngleThreshold(float paramFloat)
    {
        this.mGestureStrokeAngleThreshold = paramFloat;
    }

    public void setGestureStrokeLengthThreshold(float paramFloat)
    {
        this.mGestureStrokeLengthThreshold = paramFloat;
    }

    public void setGestureStrokeSquarenessTreshold(float paramFloat)
    {
        this.mGestureStrokeSquarenessTreshold = paramFloat;
    }

    public void setGestureStrokeType(int paramInt)
    {
        this.mGestureStrokeType = paramInt;
    }

    public void setGestureStrokeWidth(float paramFloat)
    {
        this.mGestureStrokeWidth = paramFloat;
        this.mInvalidateExtraBorder = Math.max(1, -1 + (int)paramFloat);
        this.mGesturePaint.setStrokeWidth(paramFloat);
    }

    public void setGestureVisible(boolean paramBoolean)
    {
        this.mGestureVisible = paramBoolean;
    }

    public void setOrientation(int paramInt)
    {
        this.mOrientation = paramInt;
    }

    public void setUncertainGestureColor(int paramInt)
    {
        this.mUncertainGestureColor = paramInt;
    }

    public static abstract interface OnGesturePerformedListener
    {
        public abstract void onGesturePerformed(GestureOverlayView paramGestureOverlayView, Gesture paramGesture);
    }

    public static abstract interface OnGestureListener
    {
        public abstract void onGesture(GestureOverlayView paramGestureOverlayView, MotionEvent paramMotionEvent);

        public abstract void onGestureCancelled(GestureOverlayView paramGestureOverlayView, MotionEvent paramMotionEvent);

        public abstract void onGestureEnded(GestureOverlayView paramGestureOverlayView, MotionEvent paramMotionEvent);

        public abstract void onGestureStarted(GestureOverlayView paramGestureOverlayView, MotionEvent paramMotionEvent);
    }

    public static abstract interface OnGesturingListener
    {
        public abstract void onGesturingEnded(GestureOverlayView paramGestureOverlayView);

        public abstract void onGesturingStarted(GestureOverlayView paramGestureOverlayView);
    }

    private class FadeOutRunnable
        implements Runnable
    {
        boolean fireActionPerformed;
        boolean resetMultipleStrokes;

        private FadeOutRunnable()
        {
        }

        public void run()
        {
            long l;
            if (GestureOverlayView.this.mIsFadingOut)
            {
                l = AnimationUtils.currentAnimationTimeMillis() - GestureOverlayView.this.mFadingStart;
                if (l > GestureOverlayView.this.mFadeDuration)
                {
                    if (this.fireActionPerformed)
                        GestureOverlayView.this.fireOnGesturePerformed();
                    GestureOverlayView.access$502(GestureOverlayView.this, false);
                    GestureOverlayView.access$102(GestureOverlayView.this, false);
                    GestureOverlayView.access$602(GestureOverlayView.this, false);
                    GestureOverlayView.this.mPath.rewind();
                    GestureOverlayView.access$802(GestureOverlayView.this, null);
                    GestureOverlayView.this.setPaintAlpha(255);
                }
            }
            while (true)
            {
                GestureOverlayView.this.invalidate();
                return;
                GestureOverlayView.access$602(GestureOverlayView.this, true);
                float f = Math.max(0.0F, Math.min(1.0F, (float)l / (float)GestureOverlayView.this.mFadeDuration));
                GestureOverlayView.access$1002(GestureOverlayView.this, 1.0F - GestureOverlayView.this.mInterpolator.getInterpolation(f));
                GestureOverlayView.this.setPaintAlpha((int)(255.0F * GestureOverlayView.this.mFadingAlpha));
                GestureOverlayView.this.postDelayed(this, 16L);
                continue;
                if (this.resetMultipleStrokes)
                {
                    GestureOverlayView.access$1202(GestureOverlayView.this, true);
                }
                else
                {
                    GestureOverlayView.this.fireOnGesturePerformed();
                    GestureOverlayView.access$602(GestureOverlayView.this, false);
                    GestureOverlayView.this.mPath.rewind();
                    GestureOverlayView.access$802(GestureOverlayView.this, null);
                    GestureOverlayView.access$502(GestureOverlayView.this, false);
                    GestureOverlayView.this.setPaintAlpha(255);
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.gesture.GestureOverlayView
 * JD-Core Version:        0.6.2
 */