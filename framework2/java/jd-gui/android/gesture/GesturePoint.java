package android.gesture;

import java.io.DataInputStream;
import java.io.IOException;

public class GesturePoint
{
    public final long timestamp;
    public final float x;
    public final float y;

    public GesturePoint(float paramFloat1, float paramFloat2, long paramLong)
    {
        this.x = paramFloat1;
        this.y = paramFloat2;
        this.timestamp = paramLong;
    }

    static GesturePoint deserialize(DataInputStream paramDataInputStream)
        throws IOException
    {
        return new GesturePoint(paramDataInputStream.readFloat(), paramDataInputStream.readFloat(), paramDataInputStream.readLong());
    }

    public Object clone()
    {
        return new GesturePoint(this.x, this.y, this.timestamp);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.gesture.GesturePoint
 * JD-Core Version:        0.6.2
 */