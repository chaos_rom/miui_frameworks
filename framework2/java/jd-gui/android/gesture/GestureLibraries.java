package android.gesture;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;

public final class GestureLibraries
{
    public static GestureLibrary fromFile(File paramFile)
    {
        return new FileGestureLibrary(paramFile);
    }

    public static GestureLibrary fromFile(String paramString)
    {
        return fromFile(new File(paramString));
    }

    public static GestureLibrary fromPrivateFile(Context paramContext, String paramString)
    {
        return fromFile(paramContext.getFileStreamPath(paramString));
    }

    public static GestureLibrary fromRawResource(Context paramContext, int paramInt)
    {
        return new ResourceGestureLibrary(paramContext, paramInt);
    }

    private static class ResourceGestureLibrary extends GestureLibrary
    {
        private final WeakReference<Context> mContext;
        private final int mResourceId;

        public ResourceGestureLibrary(Context paramContext, int paramInt)
        {
            this.mContext = new WeakReference(paramContext);
            this.mResourceId = paramInt;
        }

        public boolean isReadOnly()
        {
            return true;
        }

        public boolean load()
        {
            boolean bool = false;
            Context localContext = (Context)this.mContext.get();
            InputStream localInputStream;
            if (localContext != null)
                localInputStream = localContext.getResources().openRawResource(this.mResourceId);
            try
            {
                this.mStore.load(localInputStream, true);
                bool = true;
                return bool;
            }
            catch (IOException localIOException)
            {
                while (true)
                    Log.d("Gestures", "Could not load the gesture library from raw resource " + localContext.getResources().getResourceName(this.mResourceId), localIOException);
            }
        }

        public boolean save()
        {
            return false;
        }
    }

    private static class FileGestureLibrary extends GestureLibrary
    {
        private final File mPath;

        public FileGestureLibrary(File paramFile)
        {
            this.mPath = paramFile;
        }

        public boolean isReadOnly()
        {
            if (!this.mPath.canWrite());
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public boolean load()
        {
            boolean bool = false;
            File localFile = this.mPath;
            if ((localFile.exists()) && (localFile.canRead()));
            try
            {
                this.mStore.load(new FileInputStream(localFile), true);
                bool = true;
                return bool;
            }
            catch (FileNotFoundException localFileNotFoundException)
            {
                while (true)
                    Log.d("Gestures", "Could not load the gesture library from " + this.mPath, localFileNotFoundException);
            }
            catch (IOException localIOException)
            {
                while (true)
                    Log.d("Gestures", "Could not load the gesture library from " + this.mPath, localIOException);
            }
        }

        public boolean save()
        {
            boolean bool = true;
            if (!this.mStore.hasChanged());
            while (true)
            {
                return bool;
                File localFile1 = this.mPath;
                File localFile2 = localFile1.getParentFile();
                if ((!localFile2.exists()) && (!localFile2.mkdirs()))
                {
                    bool = false;
                }
                else
                {
                    bool = false;
                    try
                    {
                        localFile1.createNewFile();
                        this.mStore.save(new FileOutputStream(localFile1), true);
                        bool = true;
                    }
                    catch (FileNotFoundException localFileNotFoundException)
                    {
                        Log.d("Gestures", "Could not save the gesture library in " + this.mPath, localFileNotFoundException);
                    }
                    catch (IOException localIOException)
                    {
                        Log.d("Gestures", "Could not save the gesture library in " + this.mPath, localIOException);
                    }
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.gesture.GestureLibraries
 * JD-Core Version:        0.6.2
 */