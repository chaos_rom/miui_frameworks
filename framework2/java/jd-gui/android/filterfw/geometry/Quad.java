package android.filterfw.geometry;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Quad
{
    public Point p0;
    public Point p1;
    public Point p2;
    public Point p3;

    public Quad()
    {
    }

    public Quad(Point paramPoint1, Point paramPoint2, Point paramPoint3, Point paramPoint4)
    {
        this.p0 = paramPoint1;
        this.p1 = paramPoint2;
        this.p2 = paramPoint3;
        this.p3 = paramPoint4;
    }

    public boolean IsInUnitRange()
    {
        if ((this.p0.IsInUnitRange()) && (this.p1.IsInUnitRange()) && (this.p2.IsInUnitRange()) && (this.p3.IsInUnitRange()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public Rectangle boundingBox()
    {
        Float[] arrayOfFloat1 = new Float[4];
        arrayOfFloat1[0] = Float.valueOf(this.p0.x);
        arrayOfFloat1[1] = Float.valueOf(this.p1.x);
        arrayOfFloat1[2] = Float.valueOf(this.p2.x);
        arrayOfFloat1[3] = Float.valueOf(this.p3.x);
        List localList1 = Arrays.asList(arrayOfFloat1);
        Float[] arrayOfFloat2 = new Float[4];
        arrayOfFloat2[0] = Float.valueOf(this.p0.y);
        arrayOfFloat2[1] = Float.valueOf(this.p1.y);
        arrayOfFloat2[2] = Float.valueOf(this.p2.y);
        arrayOfFloat2[3] = Float.valueOf(this.p3.y);
        List localList2 = Arrays.asList(arrayOfFloat2);
        float f1 = ((Float)Collections.min(localList1)).floatValue();
        float f2 = ((Float)Collections.min(localList2)).floatValue();
        float f3 = ((Float)Collections.max(localList1)).floatValue();
        float f4 = ((Float)Collections.max(localList2)).floatValue();
        return new Rectangle(f1, f2, f3 - f1, f4 - f2);
    }

    public float getBoundingHeight()
    {
        Float[] arrayOfFloat = new Float[4];
        arrayOfFloat[0] = Float.valueOf(this.p0.y);
        arrayOfFloat[1] = Float.valueOf(this.p1.y);
        arrayOfFloat[2] = Float.valueOf(this.p2.y);
        arrayOfFloat[3] = Float.valueOf(this.p3.y);
        List localList = Arrays.asList(arrayOfFloat);
        return ((Float)Collections.max(localList)).floatValue() - ((Float)Collections.min(localList)).floatValue();
    }

    public float getBoundingWidth()
    {
        Float[] arrayOfFloat = new Float[4];
        arrayOfFloat[0] = Float.valueOf(this.p0.x);
        arrayOfFloat[1] = Float.valueOf(this.p1.x);
        arrayOfFloat[2] = Float.valueOf(this.p2.x);
        arrayOfFloat[3] = Float.valueOf(this.p3.x);
        List localList = Arrays.asList(arrayOfFloat);
        return ((Float)Collections.max(localList)).floatValue() - ((Float)Collections.min(localList)).floatValue();
    }

    public Quad scaled(float paramFloat)
    {
        return new Quad(this.p0.times(paramFloat), this.p1.times(paramFloat), this.p2.times(paramFloat), this.p3.times(paramFloat));
    }

    public Quad scaled(float paramFloat1, float paramFloat2)
    {
        return new Quad(this.p0.mult(paramFloat1, paramFloat2), this.p1.mult(paramFloat1, paramFloat2), this.p2.mult(paramFloat1, paramFloat2), this.p3.mult(paramFloat1, paramFloat2));
    }

    public String toString()
    {
        return "{" + this.p0 + ", " + this.p1 + ", " + this.p2 + ", " + this.p3 + "}";
    }

    public Quad translated(float paramFloat1, float paramFloat2)
    {
        return new Quad(this.p0.plus(paramFloat1, paramFloat2), this.p1.plus(paramFloat1, paramFloat2), this.p2.plus(paramFloat1, paramFloat2), this.p3.plus(paramFloat1, paramFloat2));
    }

    public Quad translated(Point paramPoint)
    {
        return new Quad(this.p0.plus(paramPoint), this.p1.plus(paramPoint), this.p2.plus(paramPoint), this.p3.plus(paramPoint));
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterfw.geometry.Quad
 * JD-Core Version:        0.6.2
 */