package android.filterfw.geometry;

public class Point
{
    public float x;
    public float y;

    public Point()
    {
    }

    public Point(float paramFloat1, float paramFloat2)
    {
        this.x = paramFloat1;
        this.y = paramFloat2;
    }

    public boolean IsInUnitRange()
    {
        if ((this.x >= 0.0F) && (this.x <= 1.0F) && (this.y >= 0.0F) && (this.y <= 1.0F));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public float distanceTo(Point paramPoint)
    {
        return paramPoint.minus(this).length();
    }

    public float length()
    {
        return (float)Math.sqrt(this.x * this.x + this.y * this.y);
    }

    public Point minus(float paramFloat1, float paramFloat2)
    {
        return new Point(this.x - paramFloat1, this.y - paramFloat2);
    }

    public Point minus(Point paramPoint)
    {
        return minus(paramPoint.x, paramPoint.y);
    }

    public Point mult(float paramFloat1, float paramFloat2)
    {
        return new Point(paramFloat1 * this.x, paramFloat2 * this.y);
    }

    public Point normalize()
    {
        return scaledTo(1.0F);
    }

    public Point plus(float paramFloat1, float paramFloat2)
    {
        return new Point(paramFloat1 + this.x, paramFloat2 + this.y);
    }

    public Point plus(Point paramPoint)
    {
        return plus(paramPoint.x, paramPoint.y);
    }

    public Point rotated(float paramFloat)
    {
        return new Point((float)(Math.cos(paramFloat) * this.x - Math.sin(paramFloat) * this.y), (float)(Math.sin(paramFloat) * this.x + Math.cos(paramFloat) * this.y));
    }

    public Point rotated90(int paramInt)
    {
        float f1 = this.x;
        float f2 = this.y;
        for (int i = 0; i < paramInt; i++)
        {
            float f3 = f1;
            f1 = f2;
            f2 = -f3;
        }
        return new Point(f1, f2);
    }

    public Point rotatedAround(Point paramPoint, float paramFloat)
    {
        return minus(paramPoint).rotated(paramFloat).plus(paramPoint);
    }

    public Point scaledTo(float paramFloat)
    {
        return times(paramFloat / length());
    }

    public void set(float paramFloat1, float paramFloat2)
    {
        this.x = paramFloat1;
        this.y = paramFloat2;
    }

    public Point times(float paramFloat)
    {
        return new Point(paramFloat * this.x, paramFloat * this.y);
    }

    public String toString()
    {
        return "(" + this.x + ", " + this.y + ")";
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterfw.geometry.Point
 * JD-Core Version:        0.6.2
 */