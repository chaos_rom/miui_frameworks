package android.filterfw;

import android.content.Context;
import android.filterfw.core.AsyncRunner;
import android.filterfw.core.FilterContext;
import android.filterfw.core.FilterGraph;
import android.filterfw.core.FrameManager;
import android.filterfw.core.GraphRunner;
import android.filterfw.core.RoundRobinScheduler;
import android.filterfw.core.SyncRunner;
import android.filterfw.io.GraphIOException;
import android.filterfw.io.GraphReader;
import android.filterfw.io.TextGraphReader;
import java.util.ArrayList;

public class GraphEnvironment extends MffEnvironment
{
    public static final int MODE_ASYNCHRONOUS = 1;
    public static final int MODE_SYNCHRONOUS = 2;
    private GraphReader mGraphReader;
    private ArrayList<GraphHandle> mGraphs = new ArrayList();

    public GraphEnvironment()
    {
        super(null);
    }

    public GraphEnvironment(FrameManager paramFrameManager, GraphReader paramGraphReader)
    {
        super(paramFrameManager);
        this.mGraphReader = paramGraphReader;
    }

    public int addGraph(FilterGraph paramFilterGraph)
    {
        GraphHandle localGraphHandle = new GraphHandle(paramFilterGraph);
        this.mGraphs.add(localGraphHandle);
        return -1 + this.mGraphs.size();
    }

    public void addReferences(Object[] paramArrayOfObject)
    {
        getGraphReader().addReferencesByKeysAndValues(paramArrayOfObject);
    }

    public FilterGraph getGraph(int paramInt)
    {
        if ((paramInt < 0) || (paramInt >= this.mGraphs.size()))
            throw new IllegalArgumentException("Invalid graph ID " + paramInt + " specified in runGraph()!");
        return ((GraphHandle)this.mGraphs.get(paramInt)).getGraph();
    }

    public GraphReader getGraphReader()
    {
        if (this.mGraphReader == null)
            this.mGraphReader = new TextGraphReader();
        return this.mGraphReader;
    }

    public GraphRunner getRunner(int paramInt1, int paramInt2)
    {
        switch (paramInt2)
        {
        default:
            throw new RuntimeException("Invalid execution mode " + paramInt2 + " specified in getRunner()!");
        case 1:
        case 2:
        }
        for (Object localObject = ((GraphHandle)this.mGraphs.get(paramInt1)).getAsyncRunner(getContext()); ; localObject = ((GraphHandle)this.mGraphs.get(paramInt1)).getSyncRunner(getContext()))
            return localObject;
    }

    public int loadGraph(Context paramContext, int paramInt)
    {
        try
        {
            FilterGraph localFilterGraph = getGraphReader().readGraphResource(paramContext, paramInt);
            return addGraph(localFilterGraph);
        }
        catch (GraphIOException localGraphIOException)
        {
            throw new RuntimeException("Could not read graph: " + localGraphIOException.getMessage());
        }
    }

    private class GraphHandle
    {
        private AsyncRunner mAsyncRunner;
        private FilterGraph mGraph;
        private SyncRunner mSyncRunner;

        public GraphHandle(FilterGraph arg2)
        {
            Object localObject;
            this.mGraph = localObject;
        }

        public AsyncRunner getAsyncRunner(FilterContext paramFilterContext)
        {
            if (this.mAsyncRunner == null)
            {
                this.mAsyncRunner = new AsyncRunner(paramFilterContext, RoundRobinScheduler.class);
                this.mAsyncRunner.setGraph(this.mGraph);
            }
            return this.mAsyncRunner;
        }

        public FilterGraph getGraph()
        {
            return this.mGraph;
        }

        public GraphRunner getSyncRunner(FilterContext paramFilterContext)
        {
            if (this.mSyncRunner == null)
                this.mSyncRunner = new SyncRunner(paramFilterContext, this.mGraph, RoundRobinScheduler.class);
            return this.mSyncRunner;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterfw.GraphEnvironment
 * JD-Core Version:        0.6.2
 */