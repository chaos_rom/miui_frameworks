package android.filterfw;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterFactory;
import android.filterfw.core.FilterFunction;
import android.filterfw.core.FrameManager;

public class FilterFunctionEnvironment extends MffEnvironment
{
    public FilterFunctionEnvironment()
    {
        super(null);
    }

    public FilterFunctionEnvironment(FrameManager paramFrameManager)
    {
        super(paramFrameManager);
    }

    public FilterFunction createFunction(Class paramClass, Object[] paramArrayOfObject)
    {
        String str = "FilterFunction(" + paramClass.getSimpleName() + ")";
        Filter localFilter = FilterFactory.sharedFactory().createFilterByClass(paramClass, str);
        localFilter.initWithAssignmentList(paramArrayOfObject);
        return new FilterFunction(getContext(), localFilter);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterfw.FilterFunctionEnvironment
 * JD-Core Version:        0.6.2
 */