package android.filterfw.core;

import java.util.Arrays;

public class MutableFrameFormat extends FrameFormat
{
    public MutableFrameFormat()
    {
    }

    public MutableFrameFormat(int paramInt1, int paramInt2)
    {
        super(paramInt1, paramInt2);
    }

    public void setBaseType(int paramInt)
    {
        this.mBaseType = paramInt;
        this.mBytesPerSample = bytesPerSampleOf(paramInt);
    }

    public void setBytesPerSample(int paramInt)
    {
        this.mBytesPerSample = paramInt;
        this.mSize = -1;
    }

    public void setDimensionCount(int paramInt)
    {
        this.mDimensions = new int[paramInt];
    }

    public void setDimensions(int paramInt)
    {
        int[] arrayOfInt = new int[1];
        arrayOfInt[0] = paramInt;
        this.mDimensions = arrayOfInt;
        this.mSize = -1;
    }

    public void setDimensions(int paramInt1, int paramInt2)
    {
        int[] arrayOfInt = new int[2];
        arrayOfInt[0] = paramInt1;
        arrayOfInt[1] = paramInt2;
        this.mDimensions = arrayOfInt;
        this.mSize = -1;
    }

    public void setDimensions(int paramInt1, int paramInt2, int paramInt3)
    {
        int[] arrayOfInt = new int[3];
        arrayOfInt[0] = paramInt1;
        arrayOfInt[1] = paramInt2;
        arrayOfInt[2] = paramInt3;
        this.mDimensions = arrayOfInt;
        this.mSize = -1;
    }

    public void setDimensions(int[] paramArrayOfInt)
    {
        if (paramArrayOfInt == null);
        for (int[] arrayOfInt = null; ; arrayOfInt = Arrays.copyOf(paramArrayOfInt, paramArrayOfInt.length))
        {
            this.mDimensions = arrayOfInt;
            this.mSize = -1;
            return;
        }
    }

    public void setMetaValue(String paramString, Object paramObject)
    {
        if (this.mMetaData == null)
            this.mMetaData = new KeyValueMap();
        this.mMetaData.put(paramString, paramObject);
    }

    public void setObjectClass(Class paramClass)
    {
        this.mObjectClass = paramClass;
    }

    public void setTarget(int paramInt)
    {
        this.mTarget = paramInt;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterfw.core.MutableFrameFormat
 * JD-Core Version:        0.6.2
 */