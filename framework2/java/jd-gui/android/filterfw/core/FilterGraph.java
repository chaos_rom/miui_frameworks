package android.filterfw.core;

import android.filterpacks.base.FrameBranch;
import android.filterpacks.base.NullFilter;
import android.util.Log;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Stack;

public class FilterGraph
{
    public static final int AUTOBRANCH_OFF = 0;
    public static final int AUTOBRANCH_SYNCED = 1;
    public static final int AUTOBRANCH_UNSYNCED = 2;
    public static final int TYPECHECK_DYNAMIC = 1;
    public static final int TYPECHECK_OFF = 0;
    public static final int TYPECHECK_STRICT = 2;
    private String TAG = "FilterGraph";
    private int mAutoBranchMode = 0;
    private boolean mDiscardUnconnectedOutputs = false;
    private HashSet<Filter> mFilters = new HashSet();
    private boolean mIsReady = false;
    private boolean mLogVerbose = Log.isLoggable(this.TAG, 2);
    private HashMap<String, Filter> mNameMap = new HashMap();
    private HashMap<OutputPort, LinkedList<InputPort>> mPreconnections = new HashMap();
    private int mTypeCheckMode = 2;

    private void checkConnections()
    {
    }

    private void connectPorts()
    {
        int i = 1;
        Iterator localIterator1 = this.mPreconnections.entrySet().iterator();
        while (true)
        {
            int j;
            if (localIterator1.hasNext())
            {
                Map.Entry localEntry = (Map.Entry)localIterator1.next();
                OutputPort localOutputPort = (OutputPort)localEntry.getKey();
                LinkedList localLinkedList = (LinkedList)localEntry.getValue();
                if (localLinkedList.size() == 1)
                {
                    localOutputPort.connectTo((InputPort)localLinkedList.get(0));
                }
                else
                {
                    if (this.mAutoBranchMode == 0)
                        throw new RuntimeException("Attempting to connect " + localOutputPort + " to multiple " + "filter ports! Enable auto-branching to allow this.");
                    if (this.mLogVerbose)
                        Log.v(this.TAG, "Creating branch for " + localOutputPort + "!");
                    if (this.mAutoBranchMode == 1)
                    {
                        StringBuilder localStringBuilder = new StringBuilder().append("branch");
                        j = i + 1;
                        FrameBranch localFrameBranch = new FrameBranch(i);
                        new KeyValueMap();
                        Object[] arrayOfObject = new Object[2];
                        arrayOfObject[0] = "outputs";
                        arrayOfObject[1] = Integer.valueOf(localLinkedList.size());
                        localFrameBranch.initWithAssignmentList(arrayOfObject);
                        addFilter(localFrameBranch);
                        localOutputPort.connectTo(localFrameBranch.getInputPort("in"));
                        Iterator localIterator2 = localLinkedList.iterator();
                        Iterator localIterator3 = localFrameBranch.getOutputPorts().iterator();
                        while (localIterator3.hasNext())
                            ((OutputPort)localIterator3.next()).connectTo((InputPort)localIterator2.next());
                    }
                    throw new RuntimeException("TODO: Unsynced branches not implemented yet!");
                }
            }
            else
            {
                this.mPreconnections.clear();
                return;
                i = j;
            }
        }
    }

    private void discardUnconnectedOutputs()
    {
        LinkedList localLinkedList = new LinkedList();
        Iterator localIterator1 = this.mFilters.iterator();
        while (localIterator1.hasNext())
        {
            Filter localFilter = (Filter)localIterator1.next();
            int i = 0;
            Iterator localIterator3 = localFilter.getOutputPorts().iterator();
            while (localIterator3.hasNext())
            {
                OutputPort localOutputPort = (OutputPort)localIterator3.next();
                if (!localOutputPort.isConnected())
                {
                    if (this.mLogVerbose)
                        Log.v(this.TAG, "Autoconnecting unconnected " + localOutputPort + " to Null filter.");
                    NullFilter localNullFilter = new NullFilter(localFilter.getName() + "ToNull" + i);
                    localNullFilter.init();
                    localLinkedList.add(localNullFilter);
                    localOutputPort.connectTo(localNullFilter.getInputPort("frame"));
                    i++;
                }
            }
        }
        Iterator localIterator2 = localLinkedList.iterator();
        while (localIterator2.hasNext())
            addFilter((Filter)localIterator2.next());
    }

    private HashSet<Filter> getSourceFilters()
    {
        HashSet localHashSet = new HashSet();
        Iterator localIterator = getFilters().iterator();
        while (localIterator.hasNext())
        {
            Filter localFilter = (Filter)localIterator.next();
            if (localFilter.getNumberOfConnectedInputs() == 0)
            {
                if (this.mLogVerbose)
                    Log.v(this.TAG, "Found source filter: " + localFilter);
                localHashSet.add(localFilter);
            }
        }
        return localHashSet;
    }

    private void preconnect(OutputPort paramOutputPort, InputPort paramInputPort)
    {
        LinkedList localLinkedList = (LinkedList)this.mPreconnections.get(paramOutputPort);
        if (localLinkedList == null)
        {
            localLinkedList = new LinkedList();
            this.mPreconnections.put(paramOutputPort, localLinkedList);
        }
        localLinkedList.add(paramInputPort);
    }

    private boolean readyForProcessing(Filter paramFilter, Set<Filter> paramSet)
    {
        boolean bool = false;
        if (paramSet.contains(paramFilter));
        while (true)
        {
            return bool;
            Iterator localIterator = paramFilter.getInputPorts().iterator();
            while (true)
                if (localIterator.hasNext())
                {
                    Filter localFilter = ((InputPort)localIterator.next()).getSourceFilter();
                    if ((localFilter != null) && (!paramSet.contains(localFilter)))
                        break;
                }
            bool = true;
        }
    }

    private void removeFilter(Filter paramFilter)
    {
        this.mFilters.remove(paramFilter);
        this.mNameMap.remove(paramFilter.getName());
    }

    private void runTypeCheck()
    {
        Stack localStack = new Stack();
        HashSet localHashSet = new HashSet();
        localStack.addAll(getSourceFilters());
        while (!localStack.empty())
        {
            Filter localFilter1 = (Filter)localStack.pop();
            localHashSet.add(localFilter1);
            updateOutputs(localFilter1);
            if (this.mLogVerbose)
                Log.v(this.TAG, "Running type check on " + localFilter1 + "...");
            runTypeCheckOn(localFilter1);
            Iterator localIterator = localFilter1.getOutputPorts().iterator();
            while (localIterator.hasNext())
            {
                Filter localFilter2 = ((OutputPort)localIterator.next()).getTargetFilter();
                if ((localFilter2 != null) && (readyForProcessing(localFilter2, localHashSet)))
                    localStack.push(localFilter2);
            }
        }
        if (localHashSet.size() != getFilters().size())
            throw new RuntimeException("Could not schedule all filters! Is your graph malformed?");
    }

    private void runTypeCheckOn(Filter paramFilter)
    {
        Iterator localIterator = paramFilter.getInputPorts().iterator();
        while (localIterator.hasNext())
        {
            InputPort localInputPort = (InputPort)localIterator.next();
            if (this.mLogVerbose)
                Log.v(this.TAG, "Type checking port " + localInputPort);
            FrameFormat localFrameFormat1 = localInputPort.getSourceFormat();
            FrameFormat localFrameFormat2 = localInputPort.getPortFormat();
            if ((localFrameFormat1 != null) && (localFrameFormat2 != null))
            {
                if (this.mLogVerbose)
                    Log.v(this.TAG, "Checking " + localFrameFormat1 + " against " + localFrameFormat2 + ".");
                boolean bool = true;
                switch (this.mTypeCheckMode)
                {
                default:
                case 0:
                case 1:
                case 2:
                }
                while (!bool)
                {
                    throw new RuntimeException("Type mismatch: Filter " + paramFilter + " expects a " + "format of type " + localFrameFormat2 + " but got a format of type " + localFrameFormat1 + "!");
                    localInputPort.setChecksType(false);
                    continue;
                    bool = localFrameFormat1.mayBeCompatibleWith(localFrameFormat2);
                    localInputPort.setChecksType(true);
                    continue;
                    bool = localFrameFormat1.isCompatibleWith(localFrameFormat2);
                    localInputPort.setChecksType(false);
                }
            }
        }
    }

    private void updateOutputs(Filter paramFilter)
    {
        Iterator localIterator = paramFilter.getOutputPorts().iterator();
        while (localIterator.hasNext())
        {
            OutputPort localOutputPort = (OutputPort)localIterator.next();
            InputPort localInputPort = localOutputPort.getBasePort();
            if (localInputPort != null)
            {
                FrameFormat localFrameFormat1 = localInputPort.getSourceFormat();
                FrameFormat localFrameFormat2 = paramFilter.getOutputFormat(localOutputPort.getName(), localFrameFormat1);
                if (localFrameFormat2 == null)
                    throw new RuntimeException("Filter did not return an output format for " + localOutputPort + "!");
                localOutputPort.setPortFormat(localFrameFormat2);
            }
        }
    }

    public boolean addFilter(Filter paramFilter)
    {
        if (!containsFilter(paramFilter))
        {
            this.mFilters.add(paramFilter);
            this.mNameMap.put(paramFilter.getName(), paramFilter);
        }
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void beginProcessing()
    {
        if (this.mLogVerbose)
            Log.v(this.TAG, "Opening all filter connections...");
        Iterator localIterator = this.mFilters.iterator();
        while (localIterator.hasNext())
            ((Filter)localIterator.next()).openOutputs();
        this.mIsReady = true;
    }

    public void closeFilters(FilterContext paramFilterContext)
    {
        if (this.mLogVerbose)
            Log.v(this.TAG, "Closing all filters...");
        Iterator localIterator = this.mFilters.iterator();
        while (localIterator.hasNext())
            ((Filter)localIterator.next()).performClose(paramFilterContext);
        this.mIsReady = false;
    }

    public void connect(Filter paramFilter1, String paramString1, Filter paramFilter2, String paramString2)
    {
        if ((paramFilter1 == null) || (paramFilter2 == null))
            throw new IllegalArgumentException("Passing null Filter in connect()!");
        if ((!containsFilter(paramFilter1)) || (!containsFilter(paramFilter2)))
            throw new RuntimeException("Attempting to connect filter not in graph!");
        OutputPort localOutputPort = paramFilter1.getOutputPort(paramString1);
        InputPort localInputPort = paramFilter2.getInputPort(paramString2);
        if (localOutputPort == null)
            throw new RuntimeException("Unknown output port '" + paramString1 + "' on Filter " + paramFilter1 + "!");
        if (localInputPort == null)
            throw new RuntimeException("Unknown input port '" + paramString2 + "' on Filter " + paramFilter2 + "!");
        preconnect(localOutputPort, localInputPort);
    }

    public void connect(String paramString1, String paramString2, String paramString3, String paramString4)
    {
        Filter localFilter1 = getFilter(paramString1);
        Filter localFilter2 = getFilter(paramString3);
        if (localFilter1 == null)
            throw new RuntimeException("Attempting to connect unknown source filter '" + paramString1 + "'!");
        if (localFilter2 == null)
            throw new RuntimeException("Attempting to connect unknown target filter '" + paramString3 + "'!");
        connect(localFilter1, paramString2, localFilter2, paramString4);
    }

    public boolean containsFilter(Filter paramFilter)
    {
        return this.mFilters.contains(paramFilter);
    }

    public void flushFrames()
    {
        Iterator localIterator = this.mFilters.iterator();
        while (localIterator.hasNext())
            ((Filter)localIterator.next()).clearOutputs();
    }

    public Filter getFilter(String paramString)
    {
        return (Filter)this.mNameMap.get(paramString);
    }

    public Set<Filter> getFilters()
    {
        return this.mFilters;
    }

    public boolean isReady()
    {
        return this.mIsReady;
    }

    public void setAutoBranchMode(int paramInt)
    {
        this.mAutoBranchMode = paramInt;
    }

    public void setDiscardUnconnectedOutputs(boolean paramBoolean)
    {
        this.mDiscardUnconnectedOutputs = paramBoolean;
    }

    public void setTypeCheckMode(int paramInt)
    {
        this.mTypeCheckMode = paramInt;
    }

    void setupFilters()
    {
        if (this.mDiscardUnconnectedOutputs)
            discardUnconnectedOutputs();
        connectPorts();
        checkConnections();
        runTypeCheck();
    }

    public void tearDown(FilterContext paramFilterContext)
    {
        if (!this.mFilters.isEmpty())
        {
            flushFrames();
            Iterator localIterator = this.mFilters.iterator();
            while (localIterator.hasNext())
                ((Filter)localIterator.next()).performTearDown(paramFilterContext);
            this.mFilters.clear();
            this.mNameMap.clear();
            this.mIsReady = false;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterfw.core.FilterGraph
 * JD-Core Version:        0.6.2
 */