package android.filterfw.core;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

public class CachedFrameManager extends SimpleFrameManager
{
    private SortedMap<Integer, Frame> mAvailableFrames = new TreeMap();
    private int mStorageCapacity = 25165824;
    private int mStorageSize = 0;
    private int mTimeStamp = 0;

    private void dropOldestFrame()
    {
        int i = ((Integer)this.mAvailableFrames.firstKey()).intValue();
        Frame localFrame = (Frame)this.mAvailableFrames.get(Integer.valueOf(i));
        this.mStorageSize -= localFrame.getFormat().getSize();
        localFrame.releaseNativeAllocation();
        this.mAvailableFrames.remove(Integer.valueOf(i));
    }

    private Frame findAvailableFrame(FrameFormat paramFrameFormat, int paramInt, long paramLong)
    {
        Frame localFrame;
        synchronized (this.mAvailableFrames)
        {
            Iterator localIterator = this.mAvailableFrames.entrySet().iterator();
            while (localIterator.hasNext())
            {
                Map.Entry localEntry = (Map.Entry)localIterator.next();
                localFrame = (Frame)localEntry.getValue();
                if ((localFrame.getFormat().isReplaceableBy(paramFrameFormat)) && (paramInt == localFrame.getBindingType()) && ((paramInt == 0) || (paramLong == localFrame.getBindingId())))
                {
                    super.retainFrame(localFrame);
                    this.mAvailableFrames.remove(localEntry.getKey());
                    localFrame.onFrameFetch();
                    localFrame.reset(paramFrameFormat);
                    this.mStorageSize -= paramFrameFormat.getSize();
                    break label165;
                }
            }
            localFrame = null;
        }
        label165: return localFrame;
    }

    private boolean storeFrame(Frame paramFrame)
    {
        boolean bool;
        synchronized (this.mAvailableFrames)
        {
            int i = paramFrame.getFormat().getSize();
            if (i > this.mStorageCapacity)
            {
                bool = false;
            }
            else
            {
                for (int j = i + this.mStorageSize; j > this.mStorageCapacity; j = i + this.mStorageSize)
                    dropOldestFrame();
                paramFrame.onFrameStore();
                this.mStorageSize = j;
                this.mAvailableFrames.put(Integer.valueOf(this.mTimeStamp), paramFrame);
                this.mTimeStamp = (1 + this.mTimeStamp);
                bool = true;
            }
        }
        return bool;
    }

    public void clearCache()
    {
        Iterator localIterator = this.mAvailableFrames.values().iterator();
        while (localIterator.hasNext())
            ((Frame)localIterator.next()).releaseNativeAllocation();
        this.mAvailableFrames.clear();
    }

    public Frame newBoundFrame(FrameFormat paramFrameFormat, int paramInt, long paramLong)
    {
        Frame localFrame = findAvailableFrame(paramFrameFormat, paramInt, paramLong);
        if (localFrame == null)
            localFrame = super.newBoundFrame(paramFrameFormat, paramInt, paramLong);
        localFrame.setTimestamp(-2L);
        return localFrame;
    }

    public Frame newFrame(FrameFormat paramFrameFormat)
    {
        Frame localFrame = findAvailableFrame(paramFrameFormat, 0, 0L);
        if (localFrame == null)
            localFrame = super.newFrame(paramFrameFormat);
        localFrame.setTimestamp(-2L);
        return localFrame;
    }

    public Frame releaseFrame(Frame paramFrame)
    {
        int i;
        if (paramFrame.isReusable())
        {
            i = paramFrame.decRefCount();
            if ((i == 0) && (paramFrame.hasNativeAllocation()))
            {
                if (!storeFrame(paramFrame))
                    paramFrame.releaseNativeAllocation();
                paramFrame = null;
            }
        }
        while (true)
        {
            return paramFrame;
            if (i < 0)
            {
                throw new RuntimeException("Frame reference count dropped below 0!");
                super.releaseFrame(paramFrame);
            }
        }
    }

    public Frame retainFrame(Frame paramFrame)
    {
        return super.retainFrame(paramFrame);
    }

    public void tearDown()
    {
        clearCache();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterfw.core.CachedFrameManager
 * JD-Core Version:        0.6.2
 */