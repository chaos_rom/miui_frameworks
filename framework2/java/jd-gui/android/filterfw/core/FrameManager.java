package android.filterfw.core;

public abstract class FrameManager
{
    private FilterContext mContext;

    public Frame duplicateFrame(Frame paramFrame)
    {
        Frame localFrame = newFrame(paramFrame.getFormat());
        localFrame.setDataFromFrame(paramFrame);
        return localFrame;
    }

    public Frame duplicateFrameToTarget(Frame paramFrame, int paramInt)
    {
        MutableFrameFormat localMutableFrameFormat = paramFrame.getFormat().mutableCopy();
        localMutableFrameFormat.setTarget(paramInt);
        Frame localFrame = newFrame(localMutableFrameFormat);
        localFrame.setDataFromFrame(paramFrame);
        return localFrame;
    }

    public FilterContext getContext()
    {
        return this.mContext;
    }

    public GLEnvironment getGLEnvironment()
    {
        if (this.mContext != null);
        for (GLEnvironment localGLEnvironment = this.mContext.getGLEnvironment(); ; localGLEnvironment = null)
            return localGLEnvironment;
    }

    public abstract Frame newBoundFrame(FrameFormat paramFrameFormat, int paramInt, long paramLong);

    public abstract Frame newFrame(FrameFormat paramFrameFormat);

    public abstract Frame releaseFrame(Frame paramFrame);

    public abstract Frame retainFrame(Frame paramFrame);

    void setContext(FilterContext paramFilterContext)
    {
        this.mContext = paramFilterContext;
    }

    public void tearDown()
    {
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterfw.core.FrameManager
 * JD-Core Version:        0.6.2
 */