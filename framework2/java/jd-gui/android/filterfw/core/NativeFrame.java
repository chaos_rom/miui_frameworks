package android.filterfw.core;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import java.nio.ByteBuffer;

public class NativeFrame extends Frame
{
    private int nativeFrameId = -1;

    static
    {
        System.loadLibrary("filterfw");
    }

    NativeFrame(FrameFormat paramFrameFormat, FrameManager paramFrameManager)
    {
        super(paramFrameFormat, paramFrameManager);
        int i = paramFrameFormat.getSize();
        nativeAllocate(i);
        if (i != 0);
        for (boolean bool = true; ; bool = false)
        {
            setReusable(bool);
            return;
        }
    }

    private native boolean getNativeBitmap(Bitmap paramBitmap, int paramInt1, int paramInt2);

    private native boolean getNativeBuffer(NativeBuffer paramNativeBuffer);

    private native int getNativeCapacity();

    private native byte[] getNativeData(int paramInt);

    private native float[] getNativeFloats(int paramInt);

    private native int[] getNativeInts(int paramInt);

    private native boolean nativeAllocate(int paramInt);

    private native boolean nativeCopyFromGL(GLFrame paramGLFrame);

    private native boolean nativeCopyFromNative(NativeFrame paramNativeFrame);

    private native boolean nativeDeallocate();

    private static native int nativeFloatSize();

    private static native int nativeIntSize();

    private native boolean setNativeBitmap(Bitmap paramBitmap, int paramInt1, int paramInt2);

    private native boolean setNativeData(byte[] paramArrayOfByte, int paramInt1, int paramInt2);

    private native boolean setNativeFloats(float[] paramArrayOfFloat);

    private native boolean setNativeInts(int[] paramArrayOfInt);

    public Bitmap getBitmap()
    {
        if (getFormat().getNumberOfDimensions() != 2)
            throw new RuntimeException("Attempting to get Bitmap for non 2-dimensional native frame!");
        Bitmap localBitmap = Bitmap.createBitmap(getFormat().getWidth(), getFormat().getHeight(), Bitmap.Config.ARGB_8888);
        if (!getNativeBitmap(localBitmap, localBitmap.getByteCount(), getFormat().getBytesPerSample()))
            throw new RuntimeException("Could not get bitmap data from native frame!");
        return localBitmap;
    }

    public int getCapacity()
    {
        return getNativeCapacity();
    }

    public ByteBuffer getData()
    {
        byte[] arrayOfByte = getNativeData(getFormat().getSize());
        if (arrayOfByte == null);
        for (ByteBuffer localByteBuffer = null; ; localByteBuffer = ByteBuffer.wrap(arrayOfByte))
            return localByteBuffer;
    }

    public float[] getFloats()
    {
        return getNativeFloats(getFormat().getSize());
    }

    public int[] getInts()
    {
        return getNativeInts(getFormat().getSize());
    }

    public Object getObjectValue()
    {
        Object localObject;
        if (getFormat().getBaseType() != 8)
            localObject = getData();
        while (true)
        {
            return localObject;
            Class localClass = getFormat().getObjectClass();
            if (localClass == null)
                throw new RuntimeException("Attempting to get object data from frame that does not specify a structure object class!");
            if (!NativeBuffer.class.isAssignableFrom(localClass))
                throw new RuntimeException("NativeFrame object class must be a subclass of NativeBuffer!");
            try
            {
                localObject = (NativeBuffer)localClass.newInstance();
                if (!getNativeBuffer((NativeBuffer)localObject))
                    throw new RuntimeException("Could not get the native structured data for frame!");
            }
            catch (Exception localException)
            {
                throw new RuntimeException("Could not instantiate new structure instance of type '" + localClass + "'!");
            }
            ((NativeBuffer)localObject).attachToFrame(this);
        }
    }

    /** @deprecated */
    protected boolean hasNativeAllocation()
    {
        try
        {
            int i = this.nativeFrameId;
            if (i != -1)
            {
                bool = true;
                return bool;
            }
            boolean bool = false;
        }
        finally
        {
        }
    }

    /** @deprecated */
    protected void releaseNativeAllocation()
    {
        try
        {
            nativeDeallocate();
            this.nativeFrameId = -1;
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void setBitmap(Bitmap paramBitmap)
    {
        assertFrameMutable();
        if (getFormat().getNumberOfDimensions() != 2)
            throw new RuntimeException("Attempting to set Bitmap for non 2-dimensional native frame!");
        if ((getFormat().getWidth() != paramBitmap.getWidth()) || (getFormat().getHeight() != paramBitmap.getHeight()))
            throw new RuntimeException("Bitmap dimensions do not match native frame dimensions!");
        Bitmap localBitmap = convertBitmapToRGBA(paramBitmap);
        if (!setNativeBitmap(localBitmap, localBitmap.getByteCount(), getFormat().getBytesPerSample()))
            throw new RuntimeException("Could not set native frame bitmap data!");
    }

    public void setData(ByteBuffer paramByteBuffer, int paramInt1, int paramInt2)
    {
        assertFrameMutable();
        byte[] arrayOfByte = paramByteBuffer.array();
        if (paramInt2 + paramInt1 > paramByteBuffer.limit())
            throw new RuntimeException("Offset and length exceed buffer size in native setData: " + (paramInt2 + paramInt1) + " bytes given, but only " + paramByteBuffer.limit() + " bytes available!");
        if (getFormat().getSize() != paramInt2)
            throw new RuntimeException("Data size in setData does not match native frame size: Frame size is " + getFormat().getSize() + " bytes, but " + paramInt2 + " bytes given!");
        if (!setNativeData(arrayOfByte, paramInt1, paramInt2))
            throw new RuntimeException("Could not set native frame data!");
    }

    public void setDataFromFrame(Frame paramFrame)
    {
        if (getFormat().getSize() < paramFrame.getFormat().getSize())
            throw new RuntimeException("Attempting to assign frame of size " + paramFrame.getFormat().getSize() + " to " + "smaller native frame of size " + getFormat().getSize() + "!");
        if ((paramFrame instanceof NativeFrame))
            nativeCopyFromNative((NativeFrame)paramFrame);
        while (true)
        {
            return;
            if ((paramFrame instanceof GLFrame))
                nativeCopyFromGL((GLFrame)paramFrame);
            else if ((paramFrame instanceof SimpleFrame))
                setObjectValue(paramFrame.getObjectValue());
            else
                super.setDataFromFrame(paramFrame);
        }
    }

    public void setFloats(float[] paramArrayOfFloat)
    {
        assertFrameMutable();
        if (paramArrayOfFloat.length * nativeFloatSize() > getFormat().getSize())
            throw new RuntimeException("NativeFrame cannot hold " + paramArrayOfFloat.length + " floats. (Can only hold " + getFormat().getSize() / nativeFloatSize() + " floats).");
        if (!setNativeFloats(paramArrayOfFloat))
            throw new RuntimeException("Could not set int values for native frame!");
    }

    public void setInts(int[] paramArrayOfInt)
    {
        assertFrameMutable();
        if (paramArrayOfInt.length * nativeIntSize() > getFormat().getSize())
            throw new RuntimeException("NativeFrame cannot hold " + paramArrayOfInt.length + " integers. (Can only hold " + getFormat().getSize() / nativeIntSize() + " integers).");
        if (!setNativeInts(paramArrayOfInt))
            throw new RuntimeException("Could not set int values for native frame!");
    }

    public String toString()
    {
        return "NativeFrame id: " + this.nativeFrameId + " (" + getFormat() + ") of size " + getCapacity();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterfw.core.NativeFrame
 * JD-Core Version:        0.6.2
 */