package android.filterfw.core;

import java.lang.reflect.Field;

public class FieldPort extends InputPort
{
    protected Field mField;
    protected boolean mHasFrame;
    protected Object mValue;
    protected boolean mValueWaiting = false;

    public FieldPort(Filter paramFilter, String paramString, Field paramField, boolean paramBoolean)
    {
        super(paramFilter, paramString);
        this.mField = paramField;
        this.mHasFrame = paramBoolean;
    }

    /** @deprecated */
    public boolean acceptsFrame()
    {
        try
        {
            boolean bool1 = this.mValueWaiting;
            if (!bool1)
            {
                bool2 = true;
                return bool2;
            }
            boolean bool2 = false;
        }
        finally
        {
        }
    }

    public void clear()
    {
    }

    public Object getTarget()
    {
        try
        {
            Object localObject2 = this.mField.get(this.mFilter);
            localObject1 = localObject2;
            return localObject1;
        }
        catch (IllegalAccessException localIllegalAccessException)
        {
            while (true)
                Object localObject1 = null;
        }
    }

    /** @deprecated */
    public boolean hasFrame()
    {
        try
        {
            boolean bool = this.mHasFrame;
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public Frame pullFrame()
    {
        try
        {
            throw new RuntimeException("Cannot pull frame on " + this + "!");
        }
        finally
        {
        }
    }

    public void pushFrame(Frame paramFrame)
    {
        setFieldFrame(paramFrame, false);
    }

    /** @deprecated */
    protected void setFieldFrame(Frame paramFrame, boolean paramBoolean)
    {
        try
        {
            assertPortIsOpen();
            checkFrameType(paramFrame, paramBoolean);
            Object localObject2 = paramFrame.getObjectValue();
            if (((localObject2 == null) && (this.mValue != null)) || (!localObject2.equals(this.mValue)))
            {
                this.mValue = localObject2;
                this.mValueWaiting = true;
            }
            this.mHasFrame = true;
            return;
        }
        finally
        {
            localObject1 = finally;
            throw localObject1;
        }
    }

    public void setFrame(Frame paramFrame)
    {
        setFieldFrame(paramFrame, true);
    }

    public String toString()
    {
        return "field " + super.toString();
    }

    /** @deprecated */
    public void transfer(FilterContext paramFilterContext)
    {
        try
        {
            boolean bool = this.mValueWaiting;
            if (bool);
            try
            {
                this.mField.set(this.mFilter, this.mValue);
                this.mValueWaiting = false;
                if (paramFilterContext != null)
                    this.mFilter.notifyFieldPortValueUpdated(this.mName, paramFilterContext);
                return;
            }
            catch (IllegalAccessException localIllegalAccessException)
            {
                throw new RuntimeException("Access to field '" + this.mField.getName() + "' was denied!");
            }
        }
        finally
        {
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterfw.core.FieldPort
 * JD-Core Version:        0.6.2
 */