package android.filterfw.core;

import java.lang.reflect.Field;

public class FinalPort extends FieldPort
{
    public FinalPort(Filter paramFilter, String paramString, Field paramField, boolean paramBoolean)
    {
        super(paramFilter, paramString, paramField, paramBoolean);
    }

    /** @deprecated */
    protected void setFieldFrame(Frame paramFrame, boolean paramBoolean)
    {
        try
        {
            assertPortIsOpen();
            checkFrameType(paramFrame, paramBoolean);
            if (this.mFilter.getStatus() != 0)
                throw new RuntimeException("Attempting to modify " + this + "!");
        }
        finally
        {
        }
        super.setFieldFrame(paramFrame, paramBoolean);
        super.transfer(null);
    }

    public String toString()
    {
        return "final " + super.toString();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterfw.core.FinalPort
 * JD-Core Version:        0.6.2
 */