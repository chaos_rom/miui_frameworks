package android.filterfw.core;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

public class KeyValueMap extends HashMap<String, Object>
{
    public static KeyValueMap fromKeyValues(Object[] paramArrayOfObject)
    {
        KeyValueMap localKeyValueMap = new KeyValueMap();
        localKeyValueMap.setKeyValues(paramArrayOfObject);
        return localKeyValueMap;
    }

    public float getFloat(String paramString)
    {
        Object localObject = get(paramString);
        if (localObject != null);
        for (Float localFloat = (Float)localObject; ; localFloat = null)
            return localFloat.floatValue();
    }

    public int getInt(String paramString)
    {
        Object localObject = get(paramString);
        if (localObject != null);
        for (Integer localInteger = (Integer)localObject; ; localInteger = null)
            return localInteger.intValue();
    }

    public String getString(String paramString)
    {
        Object localObject = get(paramString);
        if (localObject != null);
        for (String str = (String)localObject; ; str = null)
            return str;
    }

    public void setKeyValues(Object[] paramArrayOfObject)
    {
        if (paramArrayOfObject.length % 2 != 0)
            throw new RuntimeException("Key-Value arguments passed into setKeyValues must be an alternating list of keys and values!");
        for (int i = 0; i < paramArrayOfObject.length; i += 2)
        {
            if (!(paramArrayOfObject[i] instanceof String))
                throw new RuntimeException("Key-value argument " + i + " must be a key of type " + "String, but found an object of type " + paramArrayOfObject[i].getClass() + "!");
            put((String)paramArrayOfObject[i], paramArrayOfObject[(i + 1)]);
        }
    }

    public String toString()
    {
        StringWriter localStringWriter = new StringWriter();
        Iterator localIterator = entrySet().iterator();
        if (localIterator.hasNext())
        {
            Map.Entry localEntry = (Map.Entry)localIterator.next();
            Object localObject = localEntry.getValue();
            if ((localObject instanceof String));
            for (String str = "\"" + localObject + "\""; ; str = localObject.toString())
            {
                localStringWriter.write((String)localEntry.getKey() + " = " + str + ";\n");
                break;
            }
        }
        return localStringWriter.toString();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterfw.core.KeyValueMap
 * JD-Core Version:        0.6.2
 */