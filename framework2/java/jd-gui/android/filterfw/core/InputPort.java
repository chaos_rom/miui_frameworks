package android.filterfw.core;

public abstract class InputPort extends FilterPort
{
    protected OutputPort mSourcePort;

    public InputPort(Filter paramFilter, String paramString)
    {
        super(paramFilter, paramString);
    }

    public boolean acceptsFrame()
    {
        if (!hasFrame());
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void close()
    {
        if ((this.mSourcePort != null) && (this.mSourcePort.isOpen()))
            this.mSourcePort.close();
        super.close();
    }

    public boolean filterMustClose()
    {
        if ((!isOpen()) && (isBlocking()) && (!hasFrame()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public Filter getSourceFilter()
    {
        if (this.mSourcePort == null);
        for (Filter localFilter = null; ; localFilter = this.mSourcePort.getFilter())
            return localFilter;
    }

    public FrameFormat getSourceFormat()
    {
        if (this.mSourcePort != null);
        for (FrameFormat localFrameFormat = this.mSourcePort.getPortFormat(); ; localFrameFormat = getPortFormat())
            return localFrameFormat;
    }

    public OutputPort getSourcePort()
    {
        return this.mSourcePort;
    }

    public Object getTarget()
    {
        return null;
    }

    public boolean isConnected()
    {
        if (this.mSourcePort != null);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isReady()
    {
        if ((hasFrame()) || (!isBlocking()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void open()
    {
        super.open();
        if ((this.mSourcePort != null) && (!this.mSourcePort.isOpen()))
            this.mSourcePort.open();
    }

    public void setSourcePort(OutputPort paramOutputPort)
    {
        if (this.mSourcePort != null)
            throw new RuntimeException(this + " already connected to " + this.mSourcePort + "!");
        this.mSourcePort = paramOutputPort;
    }

    public abstract void transfer(FilterContext paramFilterContext);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterfw.core.InputPort
 * JD-Core Version:        0.6.2
 */