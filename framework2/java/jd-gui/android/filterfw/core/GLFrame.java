package android.filterfw.core;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Rect;
import android.opengl.GLES20;
import java.nio.ByteBuffer;

public class GLFrame extends Frame
{
    public static final int EXISTING_FBO_BINDING = 101;
    public static final int EXISTING_TEXTURE_BINDING = 100;
    public static final int EXTERNAL_TEXTURE = 104;
    public static final int NEW_FBO_BINDING = 103;
    public static final int NEW_TEXTURE_BINDING = 102;
    private int glFrameId = -1;
    private GLEnvironment mGLEnvironment;
    private boolean mOwnsTexture = true;

    static
    {
        System.loadLibrary("filterfw");
    }

    GLFrame(FrameFormat paramFrameFormat, FrameManager paramFrameManager)
    {
        super(paramFrameFormat, paramFrameManager);
    }

    GLFrame(FrameFormat paramFrameFormat, FrameManager paramFrameManager, int paramInt, long paramLong)
    {
        super(paramFrameFormat, paramFrameManager, paramInt, paramLong);
    }

    private void assertGLEnvValid()
    {
        if (!this.mGLEnvironment.isContextActive())
        {
            if (GLEnvironment.isAnyContextActive())
                throw new RuntimeException("Attempting to access " + this + " with foreign GL " + "context active!");
            throw new RuntimeException("Attempting to access " + this + " with no GL context " + " active!");
        }
    }

    private native boolean generateNativeMipMap();

    private native boolean getNativeBitmap(Bitmap paramBitmap);

    private native byte[] getNativeData();

    private native int getNativeFboId();

    private native float[] getNativeFloats();

    private native int[] getNativeInts();

    private native int getNativeTextureId();

    private void initNew(boolean paramBoolean)
    {
        if (paramBoolean)
        {
            if (!nativeAllocateExternal(this.mGLEnvironment))
                throw new RuntimeException("Could not allocate external GL frame!");
        }
        else if (!nativeAllocate(this.mGLEnvironment, getFormat().getWidth(), getFormat().getHeight()))
            throw new RuntimeException("Could not allocate GL frame!");
    }

    private void initWithFbo(int paramInt)
    {
        int i = getFormat().getWidth();
        int j = getFormat().getHeight();
        if (!nativeAllocateWithFbo(this.mGLEnvironment, paramInt, i, j))
            throw new RuntimeException("Could not allocate FBO backed GL frame!");
    }

    private void initWithTexture(int paramInt)
    {
        int i = getFormat().getWidth();
        int j = getFormat().getHeight();
        if (!nativeAllocateWithTexture(this.mGLEnvironment, paramInt, i, j))
            throw new RuntimeException("Could not allocate texture backed GL frame!");
        this.mOwnsTexture = false;
        markReadOnly();
    }

    private native boolean nativeAllocate(GLEnvironment paramGLEnvironment, int paramInt1, int paramInt2);

    private native boolean nativeAllocateExternal(GLEnvironment paramGLEnvironment);

    private native boolean nativeAllocateWithFbo(GLEnvironment paramGLEnvironment, int paramInt1, int paramInt2, int paramInt3);

    private native boolean nativeAllocateWithTexture(GLEnvironment paramGLEnvironment, int paramInt1, int paramInt2, int paramInt3);

    private native boolean nativeCopyFromGL(GLFrame paramGLFrame);

    private native boolean nativeCopyFromNative(NativeFrame paramNativeFrame);

    private native boolean nativeDeallocate();

    private native boolean nativeDetachTexFromFbo();

    private native boolean nativeFocus();

    private native boolean nativeReattachTexToFbo();

    private native boolean nativeResetParams();

    private native boolean setNativeBitmap(Bitmap paramBitmap, int paramInt);

    private native boolean setNativeData(byte[] paramArrayOfByte, int paramInt1, int paramInt2);

    private native boolean setNativeFloats(float[] paramArrayOfFloat);

    private native boolean setNativeInts(int[] paramArrayOfInt);

    private native boolean setNativeTextureParam(int paramInt1, int paramInt2);

    private native boolean setNativeViewport(int paramInt1, int paramInt2, int paramInt3, int paramInt4);

    void flushGPU(String paramString)
    {
        StopWatchMap localStopWatchMap = GLFrameTimer.get();
        if (localStopWatchMap.LOG_MFF_RUNNING_TIMES)
        {
            localStopWatchMap.start("glFinish " + paramString);
            GLES20.glFinish();
            localStopWatchMap.stop("glFinish " + paramString);
        }
    }

    public void focus()
    {
        if (!nativeFocus())
            throw new RuntimeException("Could not focus on GLFrame for drawing!");
    }

    public void generateMipMap()
    {
        assertFrameMutable();
        assertGLEnvValid();
        if (!generateNativeMipMap())
            throw new RuntimeException("Could not generate mip-map for GL frame!");
    }

    public Bitmap getBitmap()
    {
        assertGLEnvValid();
        flushGPU("getBitmap");
        Bitmap localBitmap = Bitmap.createBitmap(getFormat().getWidth(), getFormat().getHeight(), Bitmap.Config.ARGB_8888);
        if (!getNativeBitmap(localBitmap))
            throw new RuntimeException("Could not get bitmap data from GL frame!");
        return localBitmap;
    }

    public ByteBuffer getData()
    {
        assertGLEnvValid();
        flushGPU("getData");
        return ByteBuffer.wrap(getNativeData());
    }

    public int getFboId()
    {
        return getNativeFboId();
    }

    public float[] getFloats()
    {
        assertGLEnvValid();
        flushGPU("getFloats");
        return getNativeFloats();
    }

    public GLEnvironment getGLEnvironment()
    {
        return this.mGLEnvironment;
    }

    public int[] getInts()
    {
        assertGLEnvValid();
        flushGPU("getInts");
        return getNativeInts();
    }

    public Object getObjectValue()
    {
        assertGLEnvValid();
        return ByteBuffer.wrap(getNativeData());
    }

    public int getTextureId()
    {
        return getNativeTextureId();
    }

    /** @deprecated */
    protected boolean hasNativeAllocation()
    {
        try
        {
            int i = this.glFrameId;
            if (i != -1)
            {
                bool = true;
                return bool;
            }
            boolean bool = false;
        }
        finally
        {
        }
    }

    void init(GLEnvironment paramGLEnvironment)
    {
        FrameFormat localFrameFormat = getFormat();
        this.mGLEnvironment = paramGLEnvironment;
        if (localFrameFormat.getBytesPerSample() != 4)
            throw new IllegalArgumentException("GL frames must have 4 bytes per sample!");
        if (localFrameFormat.getDimensionCount() != 2)
            throw new IllegalArgumentException("GL frames must be 2-dimensional!");
        if (getFormat().getSize() < 0)
            throw new IllegalArgumentException("Initializing GL frame with zero size!");
        int i = getBindingType();
        boolean bool = true;
        if (i == 0)
            initNew(false);
        while (true)
        {
            setReusable(bool);
            return;
            if (i == 104)
            {
                initNew(true);
                bool = false;
            }
            else if (i == 100)
            {
                initWithTexture((int)getBindingId());
            }
            else if (i == 101)
            {
                initWithFbo((int)getBindingId());
            }
            else if (i == 102)
            {
                initWithTexture((int)getBindingId());
            }
            else
            {
                if (i != 103)
                    break;
                initWithFbo((int)getBindingId());
            }
        }
        throw new RuntimeException("Attempting to create GL frame with unknown binding type " + i + "!");
    }

    protected void onFrameFetch()
    {
        if (!this.mOwnsTexture)
            nativeReattachTexToFbo();
    }

    protected void onFrameStore()
    {
        if (!this.mOwnsTexture)
            nativeDetachTexFromFbo();
    }

    /** @deprecated */
    protected void releaseNativeAllocation()
    {
        try
        {
            nativeDeallocate();
            this.glFrameId = -1;
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    protected void reset(FrameFormat paramFrameFormat)
    {
        if (!nativeResetParams())
            throw new RuntimeException("Could not reset GLFrame texture parameters!");
        super.reset(paramFrameFormat);
    }

    public void setBitmap(Bitmap paramBitmap)
    {
        assertFrameMutable();
        assertGLEnvValid();
        if ((getFormat().getWidth() != paramBitmap.getWidth()) || (getFormat().getHeight() != paramBitmap.getHeight()))
            throw new RuntimeException("Bitmap dimensions do not match GL frame dimensions!");
        Bitmap localBitmap = convertBitmapToRGBA(paramBitmap);
        if (!setNativeBitmap(localBitmap, localBitmap.getByteCount()))
            throw new RuntimeException("Could not set GL frame bitmap data!");
    }

    public void setData(ByteBuffer paramByteBuffer, int paramInt1, int paramInt2)
    {
        assertFrameMutable();
        assertGLEnvValid();
        byte[] arrayOfByte = paramByteBuffer.array();
        if (getFormat().getSize() != arrayOfByte.length)
            throw new RuntimeException("Data size in setData does not match GL frame size!");
        if (!setNativeData(arrayOfByte, paramInt1, paramInt2))
            throw new RuntimeException("Could not set GL frame data!");
    }

    public void setDataFromFrame(Frame paramFrame)
    {
        assertGLEnvValid();
        if (getFormat().getSize() < paramFrame.getFormat().getSize())
            throw new RuntimeException("Attempting to assign frame of size " + paramFrame.getFormat().getSize() + " to " + "smaller GL frame of size " + getFormat().getSize() + "!");
        if ((paramFrame instanceof NativeFrame))
            nativeCopyFromNative((NativeFrame)paramFrame);
        while (true)
        {
            return;
            if ((paramFrame instanceof GLFrame))
                nativeCopyFromGL((GLFrame)paramFrame);
            else if ((paramFrame instanceof SimpleFrame))
                setObjectValue(paramFrame.getObjectValue());
            else
                super.setDataFromFrame(paramFrame);
        }
    }

    public void setFloats(float[] paramArrayOfFloat)
    {
        assertFrameMutable();
        assertGLEnvValid();
        if (!setNativeFloats(paramArrayOfFloat))
            throw new RuntimeException("Could not set int values for GL frame!");
    }

    public void setInts(int[] paramArrayOfInt)
    {
        assertFrameMutable();
        assertGLEnvValid();
        if (!setNativeInts(paramArrayOfInt))
            throw new RuntimeException("Could not set int values for GL frame!");
    }

    public void setTextureParameter(int paramInt1, int paramInt2)
    {
        assertFrameMutable();
        assertGLEnvValid();
        if (!setNativeTextureParam(paramInt1, paramInt2))
            throw new RuntimeException("Could not set texture value " + paramInt1 + " = " + paramInt2 + " " + "for GLFrame!");
    }

    public void setViewport(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        assertFrameMutable();
        setNativeViewport(paramInt1, paramInt2, paramInt3, paramInt4);
    }

    public void setViewport(Rect paramRect)
    {
        assertFrameMutable();
        setNativeViewport(paramRect.left, paramRect.top, paramRect.right - paramRect.left, paramRect.bottom - paramRect.top);
    }

    public String toString()
    {
        return "GLFrame id: " + this.glFrameId + " (" + getFormat() + ") with texture ID " + getTextureId() + ", FBO ID " + getFboId();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterfw.core.GLFrame
 * JD-Core Version:        0.6.2
 */