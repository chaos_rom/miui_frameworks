package android.filterfw.core;

import java.util.Iterator;
import java.util.Set;

public class SimpleScheduler extends Scheduler
{
    public SimpleScheduler(FilterGraph paramFilterGraph)
    {
        super(paramFilterGraph);
    }

    public void reset()
    {
    }

    public Filter scheduleNextNode()
    {
        Iterator localIterator = getGraph().getFilters().iterator();
        Filter localFilter;
        do
        {
            if (!localIterator.hasNext())
                break;
            localFilter = (Filter)localIterator.next();
        }
        while (!localFilter.canProcess());
        while (true)
        {
            return localFilter;
            localFilter = null;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterfw.core.SimpleScheduler
 * JD-Core Version:        0.6.2
 */