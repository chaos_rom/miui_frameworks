package android.filterfw.core;

import android.graphics.Bitmap;
import java.nio.ByteBuffer;

public class VertexFrame extends Frame
{
    private int vertexFrameId = -1;

    static
    {
        System.loadLibrary("filterfw");
    }

    VertexFrame(FrameFormat paramFrameFormat, FrameManager paramFrameManager)
    {
        super(paramFrameFormat, paramFrameManager);
        if (getFormat().getSize() <= 0)
            throw new IllegalArgumentException("Initializing vertex frame with zero size!");
        if (!nativeAllocate(getFormat().getSize()))
            throw new RuntimeException("Could not allocate vertex frame!");
    }

    private native int getNativeVboId();

    private native boolean nativeAllocate(int paramInt);

    private native boolean nativeDeallocate();

    private native boolean setNativeData(byte[] paramArrayOfByte, int paramInt1, int paramInt2);

    private native boolean setNativeFloats(float[] paramArrayOfFloat);

    private native boolean setNativeInts(int[] paramArrayOfInt);

    public Bitmap getBitmap()
    {
        throw new RuntimeException("Vertex frames do not support reading data!");
    }

    public ByteBuffer getData()
    {
        throw new RuntimeException("Vertex frames do not support reading data!");
    }

    public float[] getFloats()
    {
        throw new RuntimeException("Vertex frames do not support reading data!");
    }

    public int[] getInts()
    {
        throw new RuntimeException("Vertex frames do not support reading data!");
    }

    public Object getObjectValue()
    {
        throw new RuntimeException("Vertex frames do not support reading data!");
    }

    public int getVboId()
    {
        return getNativeVboId();
    }

    /** @deprecated */
    protected boolean hasNativeAllocation()
    {
        try
        {
            int i = this.vertexFrameId;
            if (i != -1)
            {
                bool = true;
                return bool;
            }
            boolean bool = false;
        }
        finally
        {
        }
    }

    /** @deprecated */
    protected void releaseNativeAllocation()
    {
        try
        {
            nativeDeallocate();
            this.vertexFrameId = -1;
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void setBitmap(Bitmap paramBitmap)
    {
        throw new RuntimeException("Unsupported: Cannot set vertex frame bitmap value!");
    }

    public void setData(ByteBuffer paramByteBuffer, int paramInt1, int paramInt2)
    {
        assertFrameMutable();
        byte[] arrayOfByte = paramByteBuffer.array();
        if (getFormat().getSize() != arrayOfByte.length)
            throw new RuntimeException("Data size in setData does not match vertex frame size!");
        if (!setNativeData(arrayOfByte, paramInt1, paramInt2))
            throw new RuntimeException("Could not set vertex frame data!");
    }

    public void setDataFromFrame(Frame paramFrame)
    {
        super.setDataFromFrame(paramFrame);
    }

    public void setFloats(float[] paramArrayOfFloat)
    {
        assertFrameMutable();
        if (!setNativeFloats(paramArrayOfFloat))
            throw new RuntimeException("Could not set int values for vertex frame!");
    }

    public void setInts(int[] paramArrayOfInt)
    {
        assertFrameMutable();
        if (!setNativeInts(paramArrayOfInt))
            throw new RuntimeException("Could not set int values for vertex frame!");
    }

    public String toString()
    {
        return "VertexFrame (" + getFormat() + ") with VBO ID " + getVboId();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterfw.core.VertexFrame
 * JD-Core Version:        0.6.2
 */