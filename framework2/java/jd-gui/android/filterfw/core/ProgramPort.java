package android.filterfw.core;

import java.lang.reflect.Field;

public class ProgramPort extends FieldPort
{
    protected String mVarName;

    public ProgramPort(Filter paramFilter, String paramString1, String paramString2, Field paramField, boolean paramBoolean)
    {
        super(paramFilter, paramString1, paramField, paramBoolean);
        this.mVarName = paramString2;
    }

    public String toString()
    {
        return "Program " + super.toString();
    }

    /** @deprecated */
    // ERROR //
    public void transfer(FilterContext paramFilterContext)
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: getfield 39	android/filterfw/core/FieldPort:mValueWaiting	Z
        //     6: istore_3
        //     7: iload_3
        //     8: ifeq +42 -> 50
        //     11: aload_0
        //     12: getfield 43	android/filterfw/core/FieldPort:mField	Ljava/lang/reflect/Field;
        //     15: aload_0
        //     16: getfield 49	android/filterfw/core/FilterPort:mFilter	Landroid/filterfw/core/Filter;
        //     19: invokevirtual 55	java/lang/reflect/Field:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     22: astore 6
        //     24: aload 6
        //     26: ifnull +24 -> 50
        //     29: aload 6
        //     31: checkcast 57	android/filterfw/core/Program
        //     34: aload_0
        //     35: getfield 13	android/filterfw/core/ProgramPort:mVarName	Ljava/lang/String;
        //     38: aload_0
        //     39: getfield 61	android/filterfw/core/FieldPort:mValue	Ljava/lang/Object;
        //     42: invokevirtual 65	android/filterfw/core/Program:setHostValue	(Ljava/lang/String;Ljava/lang/Object;)V
        //     45: aload_0
        //     46: iconst_0
        //     47: putfield 39	android/filterfw/core/FieldPort:mValueWaiting	Z
        //     50: aload_0
        //     51: monitorexit
        //     52: return
        //     53: astore 5
        //     55: new 67	java/lang/RuntimeException
        //     58: dup
        //     59: new 17	java/lang/StringBuilder
        //     62: dup
        //     63: invokespecial 20	java/lang/StringBuilder:<init>	()V
        //     66: ldc 69
        //     68: invokevirtual 26	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     71: aload_0
        //     72: getfield 43	android/filterfw/core/FieldPort:mField	Ljava/lang/reflect/Field;
        //     75: invokevirtual 72	java/lang/reflect/Field:getName	()Ljava/lang/String;
        //     78: invokevirtual 26	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     81: ldc 74
        //     83: invokevirtual 26	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     86: invokevirtual 29	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     89: invokespecial 77	java/lang/RuntimeException:<init>	(Ljava/lang/String;)V
        //     92: athrow
        //     93: astore_2
        //     94: aload_0
        //     95: monitorexit
        //     96: aload_2
        //     97: athrow
        //     98: astore 4
        //     100: new 67	java/lang/RuntimeException
        //     103: dup
        //     104: new 17	java/lang/StringBuilder
        //     107: dup
        //     108: invokespecial 20	java/lang/StringBuilder:<init>	()V
        //     111: ldc 79
        //     113: invokevirtual 26	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     116: aload_0
        //     117: getfield 43	android/filterfw/core/FieldPort:mField	Ljava/lang/reflect/Field;
        //     120: invokevirtual 72	java/lang/reflect/Field:getName	()Ljava/lang/String;
        //     123: invokevirtual 26	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     126: ldc 81
        //     128: invokevirtual 26	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     131: invokevirtual 29	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     134: invokespecial 77	java/lang/RuntimeException:<init>	(Ljava/lang/String;)V
        //     137: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     11	50	53	java/lang/IllegalAccessException
        //     2	7	93	finally
        //     11	50	93	finally
        //     55	93	93	finally
        //     100	138	93	finally
        //     11	50	98	java/lang/ClassCastException
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterfw.core.ProgramPort
 * JD-Core Version:        0.6.2
 */