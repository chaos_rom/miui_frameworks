package android.filterfw.core;

import android.util.Log;
import java.util.HashMap;

public class OneShotScheduler extends RoundRobinScheduler
{
    private static final String TAG = "OneShotScheduler";
    private final boolean mLogVerbose = Log.isLoggable("OneShotScheduler", 2);
    private HashMap<String, Integer> scheduled = new HashMap();

    public OneShotScheduler(FilterGraph paramFilterGraph)
    {
        super(paramFilterGraph);
    }

    public void reset()
    {
        super.reset();
        this.scheduled.clear();
    }

    public Filter scheduleNextNode()
    {
        Object localObject = null;
        while (true)
        {
            Filter localFilter = super.scheduleNextNode();
            if (localFilter == null)
            {
                if (this.mLogVerbose)
                    Log.v("OneShotScheduler", "No filters available to run.");
                localFilter = null;
            }
            while (true)
            {
                return localFilter;
                if (!this.scheduled.containsKey(localFilter.getName()))
                {
                    if (localFilter.getNumberOfConnectedInputs() == 0)
                        this.scheduled.put(localFilter.getName(), Integer.valueOf(1));
                    if (this.mLogVerbose)
                        Log.v("OneShotScheduler", "Scheduling filter \"" + localFilter.getName() + "\" of type " + localFilter.getFilterClassName());
                }
                else
                {
                    if (localObject != localFilter)
                        break;
                    if (this.mLogVerbose)
                        Log.v("OneShotScheduler", "One pass through graph completed.");
                    localFilter = null;
                }
            }
            if (localObject == null)
                localObject = localFilter;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterfw.core.OneShotScheduler
 * JD-Core Version:        0.6.2
 */