package android.filterfw.core;

public abstract class Program
{
    public abstract Object getHostValue(String paramString);

    public void process(Frame paramFrame1, Frame paramFrame2)
    {
        Frame[] arrayOfFrame = new Frame[1];
        arrayOfFrame[0] = paramFrame1;
        process(arrayOfFrame, paramFrame2);
    }

    public abstract void process(Frame[] paramArrayOfFrame, Frame paramFrame);

    public void reset()
    {
    }

    public abstract void setHostValue(String paramString, Object paramObject);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterfw.core.Program
 * JD-Core Version:        0.6.2
 */