package android.filterfw.core;

import java.util.Iterator;
import java.util.Set;

public class RoundRobinScheduler extends Scheduler
{
    private int mLastPos = -1;

    public RoundRobinScheduler(FilterGraph paramFilterGraph)
    {
        super(paramFilterGraph);
    }

    public void reset()
    {
        this.mLastPos = -1;
    }

    public Filter scheduleNextNode()
    {
        Set localSet = getGraph().getFilters();
        if (this.mLastPos >= localSet.size())
            this.mLastPos = -1;
        int i = 0;
        Object localObject1 = null;
        int j = -1;
        Iterator localIterator = localSet.iterator();
        Object localObject2;
        while (localIterator.hasNext())
        {
            localObject2 = (Filter)localIterator.next();
            if (((Filter)localObject2).canProcess())
            {
                if (i > this.mLastPos)
                    break label97;
                if (localObject1 == null)
                {
                    localObject1 = localObject2;
                    j = i;
                }
            }
            i++;
            continue;
            label97: this.mLastPos = i;
        }
        while (true)
        {
            return localObject2;
            if (localObject1 != null)
            {
                this.mLastPos = j;
                localObject2 = localObject1;
            }
            else
            {
                localObject2 = null;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterfw.core.RoundRobinScheduler
 * JD-Core Version:        0.6.2
 */