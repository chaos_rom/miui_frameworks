package android.filterfw.core;

public class StreamPort extends InputPort
{
    private Frame mFrame;
    private boolean mPersistent;

    public StreamPort(Filter paramFilter, String paramString)
    {
        super(paramFilter, paramString);
    }

    /** @deprecated */
    protected void assignFrame(Frame paramFrame, boolean paramBoolean)
    {
        try
        {
            assertPortIsOpen();
            checkFrameType(paramFrame, paramBoolean);
            if (paramBoolean)
                if (this.mFrame != null)
                    this.mFrame.release();
            while (this.mFrame == null)
            {
                this.mFrame = paramFrame.retain();
                this.mFrame.markReadOnly();
                this.mPersistent = paramBoolean;
                return;
            }
            throw new RuntimeException("Attempting to push more than one frame on port: " + this + "!");
        }
        finally
        {
        }
    }

    public void clear()
    {
        if (this.mFrame != null)
        {
            this.mFrame.release();
            this.mFrame = null;
        }
    }

    /** @deprecated */
    public boolean hasFrame()
    {
        try
        {
            Frame localFrame = this.mFrame;
            if (localFrame != null)
            {
                bool = true;
                return bool;
            }
            boolean bool = false;
        }
        finally
        {
        }
    }

    /** @deprecated */
    public Frame pullFrame()
    {
        try
        {
            if (this.mFrame == null)
                throw new RuntimeException("No frame available to pull on port: " + this + "!");
        }
        finally
        {
        }
        Frame localFrame = this.mFrame;
        if (this.mPersistent)
            this.mFrame.retain();
        while (true)
        {
            return localFrame;
            this.mFrame = null;
        }
    }

    public void pushFrame(Frame paramFrame)
    {
        assignFrame(paramFrame, false);
    }

    public void setFrame(Frame paramFrame)
    {
        assignFrame(paramFrame, true);
    }

    public String toString()
    {
        return "input " + super.toString();
    }

    /** @deprecated */
    public void transfer(FilterContext paramFilterContext)
    {
        try
        {
            if (this.mFrame != null)
                checkFrameManager(this.mFrame, paramFilterContext);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterfw.core.StreamPort
 * JD-Core Version:        0.6.2
 */