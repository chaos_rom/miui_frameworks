package android.filterfw.core;

import java.util.HashMap;

public class StopWatchMap
{
    public boolean LOG_MFF_RUNNING_TIMES = false;
    private HashMap<String, StopWatch> mStopWatches = null;

    public void start(String paramString)
    {
        if (!this.LOG_MFF_RUNNING_TIMES);
        while (true)
        {
            return;
            if (!this.mStopWatches.containsKey(paramString))
                this.mStopWatches.put(paramString, new StopWatch(paramString));
            ((StopWatch)this.mStopWatches.get(paramString)).start();
        }
    }

    public void stop(String paramString)
    {
        if (!this.LOG_MFF_RUNNING_TIMES);
        while (true)
        {
            return;
            if (!this.mStopWatches.containsKey(paramString))
                throw new RuntimeException("Calling stop with unknown stopWatchName: " + paramString);
            ((StopWatch)this.mStopWatches.get(paramString)).stop();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterfw.core.StopWatchMap
 * JD-Core Version:        0.6.2
 */