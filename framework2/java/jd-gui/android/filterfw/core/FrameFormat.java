package android.filterfw.core;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

public class FrameFormat
{
    public static final int BYTES_PER_SAMPLE_UNSPECIFIED = 1;
    protected static final int SIZE_UNKNOWN = -1;
    public static final int SIZE_UNSPECIFIED = 0;
    public static final int TARGET_GPU = 3;
    public static final int TARGET_NATIVE = 2;
    public static final int TARGET_RS = 5;
    public static final int TARGET_SIMPLE = 1;
    public static final int TARGET_UNSPECIFIED = 0;
    public static final int TARGET_VERTEXBUFFER = 4;
    public static final int TYPE_BIT = 1;
    public static final int TYPE_BYTE = 2;
    public static final int TYPE_DOUBLE = 6;
    public static final int TYPE_FLOAT = 5;
    public static final int TYPE_INT16 = 3;
    public static final int TYPE_INT32 = 4;
    public static final int TYPE_OBJECT = 8;
    public static final int TYPE_POINTER = 7;
    public static final int TYPE_UNSPECIFIED;
    protected int mBaseType = 0;
    protected int mBytesPerSample = 1;
    protected int[] mDimensions;
    protected KeyValueMap mMetaData;
    protected Class mObjectClass;
    protected int mSize = -1;
    protected int mTarget = 0;

    protected FrameFormat()
    {
    }

    public FrameFormat(int paramInt1, int paramInt2)
    {
        this.mBaseType = paramInt1;
        this.mTarget = paramInt2;
        initDefaults();
    }

    public static String baseTypeToString(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        default:
            str = "unknown";
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        }
        while (true)
        {
            return str;
            str = "unspecified";
            continue;
            str = "bit";
            continue;
            str = "byte";
            continue;
            str = "int";
            continue;
            str = "int";
            continue;
            str = "float";
            continue;
            str = "double";
            continue;
            str = "pointer";
            continue;
            str = "object";
        }
    }

    public static int bytesPerSampleOf(int paramInt)
    {
        int i = 1;
        switch (paramInt)
        {
        case 1:
        case 2:
        default:
        case 3:
        case 4:
        case 5:
        case 7:
        case 6:
        }
        while (true)
        {
            return i;
            i = 2;
            continue;
            i = 4;
            continue;
            i = 8;
        }
    }

    public static String dimensionsToString(int[] paramArrayOfInt)
    {
        StringBuffer localStringBuffer = new StringBuffer();
        if (paramArrayOfInt != null)
        {
            int i = paramArrayOfInt.length;
            int j = 0;
            if (j < i)
            {
                if (paramArrayOfInt[j] == 0)
                    localStringBuffer.append("[]");
                while (true)
                {
                    j++;
                    break;
                    localStringBuffer.append("[" + String.valueOf(paramArrayOfInt[j]) + "]");
                }
            }
        }
        return localStringBuffer.toString();
    }

    private void initDefaults()
    {
        this.mBytesPerSample = bytesPerSampleOf(this.mBaseType);
    }

    public static String metaDataToString(KeyValueMap paramKeyValueMap)
    {
        if (paramKeyValueMap == null);
        StringBuffer localStringBuffer;
        for (String str = ""; ; str = localStringBuffer.toString())
        {
            return str;
            localStringBuffer = new StringBuffer();
            localStringBuffer.append("{ ");
            Iterator localIterator = paramKeyValueMap.entrySet().iterator();
            while (localIterator.hasNext())
            {
                Map.Entry localEntry = (Map.Entry)localIterator.next();
                localStringBuffer.append((String)localEntry.getKey() + ": " + localEntry.getValue() + " ");
            }
            localStringBuffer.append("}");
        }
    }

    public static int readTargetString(String paramString)
    {
        int i;
        if ((paramString.equalsIgnoreCase("CPU")) || (paramString.equalsIgnoreCase("NATIVE")))
            i = 2;
        while (true)
        {
            return i;
            if (paramString.equalsIgnoreCase("GPU"))
            {
                i = 3;
            }
            else if (paramString.equalsIgnoreCase("SIMPLE"))
            {
                i = 1;
            }
            else if (paramString.equalsIgnoreCase("VERTEXBUFFER"))
            {
                i = 4;
            }
            else
            {
                if (!paramString.equalsIgnoreCase("UNSPECIFIED"))
                    break;
                i = 0;
            }
        }
        throw new RuntimeException("Unknown target type '" + paramString + "'!");
    }

    public static String targetToString(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        default:
            str = "unknown";
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        }
        while (true)
        {
            return str;
            str = "unspecified";
            continue;
            str = "simple";
            continue;
            str = "native";
            continue;
            str = "gpu";
            continue;
            str = "vbo";
            continue;
            str = "renderscript";
        }
    }

    public static FrameFormat unspecified()
    {
        return new FrameFormat(0, 0);
    }

    int calcSize(int[] paramArrayOfInt)
    {
        int i;
        int j;
        int k;
        if ((paramArrayOfInt != null) && (paramArrayOfInt.length > 0))
        {
            i = getBytesPerSample();
            j = paramArrayOfInt.length;
            k = 0;
        }
        while (k < j)
        {
            i *= paramArrayOfInt[k];
            k++;
            continue;
            i = 0;
        }
        return i;
    }

    public boolean equals(Object paramObject)
    {
        boolean bool = true;
        if (this == paramObject);
        while (true)
        {
            return bool;
            if (!(paramObject instanceof FrameFormat))
            {
                bool = false;
            }
            else
            {
                FrameFormat localFrameFormat = (FrameFormat)paramObject;
                if ((localFrameFormat.mBaseType != this.mBaseType) || (localFrameFormat.mTarget != this.mTarget) || (localFrameFormat.mBytesPerSample != this.mBytesPerSample) || (!Arrays.equals(localFrameFormat.mDimensions, this.mDimensions)) || (!localFrameFormat.mMetaData.equals(this.mMetaData)))
                    bool = false;
            }
        }
    }

    public int getBaseType()
    {
        return this.mBaseType;
    }

    public int getBytesPerSample()
    {
        return this.mBytesPerSample;
    }

    public int getDepth()
    {
        if ((this.mDimensions != null) && (this.mDimensions.length >= 3));
        for (int i = this.mDimensions[2]; ; i = -1)
            return i;
    }

    public int getDimension(int paramInt)
    {
        return this.mDimensions[paramInt];
    }

    public int getDimensionCount()
    {
        if (this.mDimensions == null);
        for (int i = 0; ; i = this.mDimensions.length)
            return i;
    }

    public int[] getDimensions()
    {
        return this.mDimensions;
    }

    public int getHeight()
    {
        if ((this.mDimensions != null) && (this.mDimensions.length >= 2));
        for (int i = this.mDimensions[1]; ; i = -1)
            return i;
    }

    public int getLength()
    {
        if ((this.mDimensions != null) && (this.mDimensions.length >= 1));
        for (int i = this.mDimensions[0]; ; i = -1)
            return i;
    }

    public Object getMetaValue(String paramString)
    {
        if (this.mMetaData != null);
        for (Object localObject = this.mMetaData.get(paramString); ; localObject = null)
            return localObject;
    }

    public int getNumberOfDimensions()
    {
        if (this.mDimensions != null);
        for (int i = this.mDimensions.length; ; i = 0)
            return i;
    }

    public Class getObjectClass()
    {
        return this.mObjectClass;
    }

    public int getSize()
    {
        if (this.mSize == -1)
            this.mSize = calcSize(this.mDimensions);
        return this.mSize;
    }

    public int getTarget()
    {
        return this.mTarget;
    }

    public int getValuesPerSample()
    {
        return this.mBytesPerSample / bytesPerSampleOf(this.mBaseType);
    }

    public int getWidth()
    {
        return getLength();
    }

    public boolean hasMetaKey(String paramString)
    {
        if (this.mMetaData != null);
        for (boolean bool = this.mMetaData.containsKey(paramString); ; bool = false)
            return bool;
    }

    public boolean hasMetaKey(String paramString, Class paramClass)
    {
        if ((this.mMetaData != null) && (this.mMetaData.containsKey(paramString)))
            if (!paramClass.isAssignableFrom(this.mMetaData.get(paramString).getClass()))
                throw new RuntimeException("FrameFormat meta-key '" + paramString + "' is of type " + this.mMetaData.get(paramString).getClass() + " but expected to be of type " + paramClass + "!");
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public int hashCode()
    {
        return 0x1073 ^ this.mBaseType ^ this.mBytesPerSample ^ getSize();
    }

    public boolean isBinaryDataType()
    {
        int i = 1;
        if ((this.mBaseType >= i) && (this.mBaseType <= 6));
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    public boolean isCompatibleWith(FrameFormat paramFrameFormat)
    {
        boolean bool = false;
        if ((paramFrameFormat.getBaseType() != 0) && (getBaseType() != paramFrameFormat.getBaseType()));
        while (true)
        {
            return bool;
            if (((paramFrameFormat.getTarget() == 0) || (getTarget() == paramFrameFormat.getTarget())) && ((paramFrameFormat.getBytesPerSample() == 1) || (getBytesPerSample() == paramFrameFormat.getBytesPerSample())) && ((paramFrameFormat.getDimensionCount() <= 0) || (getDimensionCount() == paramFrameFormat.getDimensionCount())))
            {
                for (int i = 0; ; i++)
                {
                    if (i >= paramFrameFormat.getDimensionCount())
                        break label115;
                    int j = paramFrameFormat.getDimension(i);
                    if ((j != 0) && (getDimension(i) != j))
                        break;
                }
                label115: if ((paramFrameFormat.getObjectClass() == null) || ((getObjectClass() != null) && (paramFrameFormat.getObjectClass().isAssignableFrom(getObjectClass()))))
                {
                    if (paramFrameFormat.mMetaData != null)
                    {
                        Iterator localIterator = paramFrameFormat.mMetaData.keySet().iterator();
                        while (true)
                            if (localIterator.hasNext())
                            {
                                String str = (String)localIterator.next();
                                if ((this.mMetaData == null) || (!this.mMetaData.containsKey(str)))
                                    break;
                                if (!this.mMetaData.get(str).equals(paramFrameFormat.mMetaData.get(str)))
                                    break;
                            }
                    }
                    bool = true;
                }
            }
        }
    }

    boolean isReplaceableBy(FrameFormat paramFrameFormat)
    {
        if ((this.mTarget == paramFrameFormat.mTarget) && (getSize() == paramFrameFormat.getSize()) && (Arrays.equals(paramFrameFormat.mDimensions, this.mDimensions)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean mayBeCompatibleWith(FrameFormat paramFrameFormat)
    {
        boolean bool = false;
        if ((paramFrameFormat.getBaseType() != 0) && (getBaseType() != 0) && (getBaseType() != paramFrameFormat.getBaseType()));
        while (true)
        {
            return bool;
            if (((paramFrameFormat.getTarget() == 0) || (getTarget() == 0) || (getTarget() == paramFrameFormat.getTarget())) && ((paramFrameFormat.getBytesPerSample() == 1) || (getBytesPerSample() == 1) || (getBytesPerSample() == paramFrameFormat.getBytesPerSample())) && ((paramFrameFormat.getDimensionCount() <= 0) || (getDimensionCount() <= 0) || (getDimensionCount() == paramFrameFormat.getDimensionCount())))
            {
                for (int i = 0; ; i++)
                {
                    if (i >= paramFrameFormat.getDimensionCount())
                        break label152;
                    int j = paramFrameFormat.getDimension(i);
                    if ((j != 0) && (getDimension(i) != 0) && (getDimension(i) != j))
                        break;
                }
                label152: if ((paramFrameFormat.getObjectClass() == null) || (getObjectClass() == null) || (paramFrameFormat.getObjectClass().isAssignableFrom(getObjectClass())))
                {
                    if ((paramFrameFormat.mMetaData != null) && (this.mMetaData != null))
                    {
                        Iterator localIterator = paramFrameFormat.mMetaData.keySet().iterator();
                        while (true)
                            if (localIterator.hasNext())
                            {
                                String str = (String)localIterator.next();
                                if ((this.mMetaData.containsKey(str)) && (!this.mMetaData.get(str).equals(paramFrameFormat.mMetaData.get(str))))
                                    break;
                            }
                    }
                    bool = true;
                }
            }
        }
    }

    public MutableFrameFormat mutableCopy()
    {
        MutableFrameFormat localMutableFrameFormat = new MutableFrameFormat();
        localMutableFrameFormat.setBaseType(getBaseType());
        localMutableFrameFormat.setTarget(getTarget());
        localMutableFrameFormat.setBytesPerSample(getBytesPerSample());
        localMutableFrameFormat.setDimensions(getDimensions());
        localMutableFrameFormat.setObjectClass(getObjectClass());
        if (this.mMetaData == null);
        for (KeyValueMap localKeyValueMap = null; ; localKeyValueMap = (KeyValueMap)this.mMetaData.clone())
        {
            localMutableFrameFormat.mMetaData = localKeyValueMap;
            return localMutableFrameFormat;
        }
    }

    public String toString()
    {
        int i = getValuesPerSample();
        String str1;
        String str2;
        if (i == 1)
        {
            str1 = "";
            if (this.mTarget != 0)
                break label96;
            str2 = "";
            label23: if (this.mObjectClass != null)
                break label125;
        }
        label96: label125: for (String str3 = ""; ; str3 = " class(" + this.mObjectClass.getSimpleName() + ") ")
        {
            return str2 + baseTypeToString(this.mBaseType) + str1 + dimensionsToString(this.mDimensions) + str3 + metaDataToString(this.mMetaData);
            str1 = String.valueOf(i);
            break;
            str2 = targetToString(this.mTarget) + " ";
            break label23;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterfw.core.FrameFormat
 * JD-Core Version:        0.6.2
 */