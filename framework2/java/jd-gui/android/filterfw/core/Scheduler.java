package android.filterfw.core;

public abstract class Scheduler
{
    private FilterGraph mGraph;

    Scheduler(FilterGraph paramFilterGraph)
    {
        this.mGraph = paramFilterGraph;
    }

    boolean finished()
    {
        return true;
    }

    FilterGraph getGraph()
    {
        return this.mGraph;
    }

    abstract void reset();

    abstract Filter scheduleNextNode();
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterfw.core.Scheduler
 * JD-Core Version:        0.6.2
 */