package android.filterfw.core;

import android.filterfw.format.ObjectFormat;
import android.graphics.Bitmap;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;

public class SerializedFrame extends Frame
{
    private static final int INITIAL_CAPACITY = 64;
    private DirectByteOutputStream mByteOutputStream;
    private ObjectOutputStream mObjectOut;

    SerializedFrame(FrameFormat paramFrameFormat, FrameManager paramFrameManager)
    {
        super(paramFrameFormat, paramFrameManager);
        setReusable(false);
        try
        {
            this.mByteOutputStream = new DirectByteOutputStream(64);
            this.mObjectOut = new ObjectOutputStream(this.mByteOutputStream);
            this.mByteOutputStream.markHeaderEnd();
            return;
        }
        catch (IOException localIOException)
        {
            throw new RuntimeException("Could not create serialization streams for SerializedFrame!", localIOException);
        }
    }

    private final Object deserializeObjectValue()
    {
        try
        {
            Object localObject = new ObjectInputStream(this.mByteOutputStream.getInputStream()).readObject();
            return localObject;
        }
        catch (IOException localIOException)
        {
            throw new RuntimeException("Could not deserialize object in " + this + "!", localIOException);
        }
        catch (ClassNotFoundException localClassNotFoundException)
        {
            throw new RuntimeException("Unable to deserialize object of unknown class in " + this + "!", localClassNotFoundException);
        }
    }

    private final void serializeObjectValue(Object paramObject)
    {
        try
        {
            this.mByteOutputStream.reset();
            this.mObjectOut.writeObject(paramObject);
            this.mObjectOut.flush();
            this.mObjectOut.close();
            return;
        }
        catch (IOException localIOException)
        {
            throw new RuntimeException("Could not serialize object " + paramObject + " in " + this + "!", localIOException);
        }
    }

    static SerializedFrame wrapObject(Object paramObject, FrameManager paramFrameManager)
    {
        SerializedFrame localSerializedFrame = new SerializedFrame(ObjectFormat.fromObject(paramObject, 1), paramFrameManager);
        localSerializedFrame.setObjectValue(paramObject);
        return localSerializedFrame;
    }

    public Bitmap getBitmap()
    {
        Object localObject = deserializeObjectValue();
        if ((localObject instanceof Bitmap));
        for (Bitmap localBitmap = (Bitmap)localObject; ; localBitmap = null)
            return localBitmap;
    }

    public ByteBuffer getData()
    {
        Object localObject = deserializeObjectValue();
        if ((localObject instanceof ByteBuffer));
        for (ByteBuffer localByteBuffer = (ByteBuffer)localObject; ; localByteBuffer = null)
            return localByteBuffer;
    }

    public float[] getFloats()
    {
        Object localObject = deserializeObjectValue();
        if ((localObject instanceof float[]));
        for (float[] arrayOfFloat = (float[])localObject; ; arrayOfFloat = null)
            return arrayOfFloat;
    }

    public int[] getInts()
    {
        Object localObject = deserializeObjectValue();
        if ((localObject instanceof int[]));
        for (int[] arrayOfInt = (int[])localObject; ; arrayOfInt = null)
            return arrayOfInt;
    }

    public Object getObjectValue()
    {
        return deserializeObjectValue();
    }

    protected boolean hasNativeAllocation()
    {
        return false;
    }

    protected void releaseNativeAllocation()
    {
    }

    public void setBitmap(Bitmap paramBitmap)
    {
        assertFrameMutable();
        setGenericObjectValue(paramBitmap);
    }

    public void setData(ByteBuffer paramByteBuffer, int paramInt1, int paramInt2)
    {
        assertFrameMutable();
        setGenericObjectValue(ByteBuffer.wrap(paramByteBuffer.array(), paramInt1, paramInt2));
    }

    public void setFloats(float[] paramArrayOfFloat)
    {
        assertFrameMutable();
        setGenericObjectValue(paramArrayOfFloat);
    }

    protected void setGenericObjectValue(Object paramObject)
    {
        serializeObjectValue(paramObject);
    }

    public void setInts(int[] paramArrayOfInt)
    {
        assertFrameMutable();
        setGenericObjectValue(paramArrayOfInt);
    }

    public String toString()
    {
        return "SerializedFrame (" + getFormat() + ")";
    }

    private class DirectByteInputStream extends InputStream
    {
        private byte[] mBuffer;
        private int mPos = 0;
        private int mSize;

        public DirectByteInputStream(byte[] paramInt, int arg3)
        {
            this.mBuffer = paramInt;
            int i;
            this.mSize = i;
        }

        public final int available()
        {
            return this.mSize - this.mPos;
        }

        public final int read()
        {
            byte[] arrayOfByte;
            int j;
            if (this.mPos < this.mSize)
            {
                arrayOfByte = this.mBuffer;
                j = this.mPos;
                this.mPos = (j + 1);
            }
            for (int i = 0xFF & arrayOfByte[j]; ; i = -1)
                return i;
        }

        public final int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
        {
            if (this.mPos >= this.mSize);
            for (int i = -1; ; i = paramInt2)
            {
                return i;
                if (paramInt2 + this.mPos > this.mSize)
                    paramInt2 = this.mSize - this.mPos;
                System.arraycopy(this.mBuffer, this.mPos, paramArrayOfByte, paramInt1, paramInt2);
                this.mPos = (paramInt2 + this.mPos);
            }
        }

        public final long skip(long paramLong)
        {
            if (paramLong + this.mPos > this.mSize)
                paramLong = this.mSize - this.mPos;
            if (paramLong < 0L)
                paramLong = 0L;
            while (true)
            {
                return paramLong;
                this.mPos = ((int)(paramLong + this.mPos));
            }
        }
    }

    private class DirectByteOutputStream extends OutputStream
    {
        private byte[] mBuffer = null;
        private int mDataOffset = 0;
        private int mOffset = 0;

        public DirectByteOutputStream(int arg2)
        {
            int i;
            this.mBuffer = new byte[i];
        }

        private final void ensureFit(int paramInt)
        {
            if (paramInt + this.mOffset > this.mBuffer.length)
            {
                byte[] arrayOfByte = this.mBuffer;
                this.mBuffer = new byte[Math.max(paramInt + this.mOffset, 2 * this.mBuffer.length)];
                System.arraycopy(arrayOfByte, 0, this.mBuffer, 0, this.mOffset);
            }
        }

        public byte[] getByteArray()
        {
            return this.mBuffer;
        }

        public final SerializedFrame.DirectByteInputStream getInputStream()
        {
            return new SerializedFrame.DirectByteInputStream(SerializedFrame.this, this.mBuffer, this.mOffset);
        }

        public final int getSize()
        {
            return this.mOffset;
        }

        public final void markHeaderEnd()
        {
            this.mDataOffset = this.mOffset;
        }

        public final void reset()
        {
            this.mOffset = this.mDataOffset;
        }

        public final void write(int paramInt)
        {
            ensureFit(1);
            byte[] arrayOfByte = this.mBuffer;
            int i = this.mOffset;
            this.mOffset = (i + 1);
            arrayOfByte[i] = ((byte)paramInt);
        }

        public final void write(byte[] paramArrayOfByte)
        {
            write(paramArrayOfByte, 0, paramArrayOfByte.length);
        }

        public final void write(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
        {
            ensureFit(paramInt2);
            System.arraycopy(paramArrayOfByte, paramInt1, this.mBuffer, this.mOffset, paramInt2);
            this.mOffset = (paramInt2 + this.mOffset);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterfw.core.SerializedFrame
 * JD-Core Version:        0.6.2
 */