package android.filterfw.core;

public class OutputPort extends FilterPort
{
    protected InputPort mBasePort;
    protected InputPort mTargetPort;

    public OutputPort(Filter paramFilter, String paramString)
    {
        super(paramFilter, paramString);
    }

    public void clear()
    {
        if (this.mTargetPort != null)
            this.mTargetPort.clear();
    }

    public void close()
    {
        super.close();
        if ((this.mTargetPort != null) && (this.mTargetPort.isOpen()))
            this.mTargetPort.close();
    }

    public void connectTo(InputPort paramInputPort)
    {
        if (this.mTargetPort != null)
            throw new RuntimeException(this + " already connected to " + this.mTargetPort + "!");
        this.mTargetPort = paramInputPort;
        this.mTargetPort.setSourcePort(this);
    }

    public boolean filterMustClose()
    {
        if ((!isOpen()) && (isBlocking()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public InputPort getBasePort()
    {
        return this.mBasePort;
    }

    public Filter getTargetFilter()
    {
        if (this.mTargetPort == null);
        for (Filter localFilter = null; ; localFilter = this.mTargetPort.getFilter())
            return localFilter;
    }

    public InputPort getTargetPort()
    {
        return this.mTargetPort;
    }

    public boolean hasFrame()
    {
        if (this.mTargetPort == null);
        for (boolean bool = false; ; bool = this.mTargetPort.hasFrame())
            return bool;
    }

    public boolean isConnected()
    {
        if (this.mTargetPort != null);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isReady()
    {
        if (((isOpen()) && (this.mTargetPort.acceptsFrame())) || (!isBlocking()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void open()
    {
        super.open();
        if ((this.mTargetPort != null) && (!this.mTargetPort.isOpen()))
            this.mTargetPort.open();
    }

    public Frame pullFrame()
    {
        throw new RuntimeException("Cannot pull frame on " + this + "!");
    }

    public void pushFrame(Frame paramFrame)
    {
        if (this.mTargetPort == null)
            throw new RuntimeException("Attempting to push frame on unconnected port: " + this + "!");
        this.mTargetPort.pushFrame(paramFrame);
    }

    public void setBasePort(InputPort paramInputPort)
    {
        this.mBasePort = paramInputPort;
    }

    public void setFrame(Frame paramFrame)
    {
        assertPortIsOpen();
        if (this.mTargetPort == null)
            throw new RuntimeException("Attempting to set frame on unconnected port: " + this + "!");
        this.mTargetPort.setFrame(paramFrame);
    }

    public String toString()
    {
        return "output " + super.toString();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterfw.core.OutputPort
 * JD-Core Version:        0.6.2
 */