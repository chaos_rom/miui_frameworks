package android.filterfw.core;

import android.util.Log;
import dalvik.system.PathClassLoader;
import java.util.HashSet;
import java.util.Iterator;

public class FilterFactory
{
    private static final String TAG = "FilterFactory";
    private static Object mClassLoaderGuard = new Object();
    private static ClassLoader mCurrentClassLoader = Thread.currentThread().getContextClassLoader();
    private static HashSet<String> mLibraries = new HashSet();
    private static boolean mLogVerbose = Log.isLoggable("FilterFactory", 2);
    private static FilterFactory mSharedFactory;
    private HashSet<String> mPackages = new HashSet();

    public static void addFilterLibrary(String paramString)
    {
        if (mLogVerbose)
            Log.v("FilterFactory", "Adding filter library " + paramString);
        synchronized (mClassLoaderGuard)
        {
            if (mLibraries.contains(paramString))
            {
                if (mLogVerbose)
                    Log.v("FilterFactory", "Library already added");
            }
            else
            {
                mLibraries.add(paramString);
                mCurrentClassLoader = new PathClassLoader(paramString, mCurrentClassLoader);
            }
        }
    }

    public static FilterFactory sharedFactory()
    {
        if (mSharedFactory == null)
            mSharedFactory = new FilterFactory();
        return mSharedFactory;
    }

    public void addPackage(String paramString)
    {
        if (mLogVerbose)
            Log.v("FilterFactory", "Adding package " + paramString);
        this.mPackages.add(paramString);
    }

    // ERROR //
    public Filter createFilterByClass(Class paramClass, String paramString)
    {
        // Byte code:
        //     0: aload_1
        //     1: ldc 105
        //     3: invokevirtual 111	java/lang/Class:asSubclass	(Ljava/lang/Class;)Ljava/lang/Class;
        //     6: pop
        //     7: iconst_1
        //     8: anewarray 107	java/lang/Class
        //     11: astore 6
        //     13: aload 6
        //     15: iconst_0
        //     16: ldc 113
        //     18: aastore
        //     19: aload_1
        //     20: aload 6
        //     22: invokevirtual 117	java/lang/Class:getConstructor	([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
        //     25: astore 7
        //     27: aconst_null
        //     28: astore 8
        //     30: iconst_1
        //     31: anewarray 4	java/lang/Object
        //     34: astore 10
        //     36: aload 10
        //     38: iconst_0
        //     39: aload_2
        //     40: aastore
        //     41: aload 7
        //     43: aload 10
        //     45: invokevirtual 123	java/lang/reflect/Constructor:newInstance	([Ljava/lang/Object;)Ljava/lang/Object;
        //     48: checkcast 105	android/filterfw/core/Filter
        //     51: astore 8
        //     53: aload 8
        //     55: ifnonnull +102 -> 157
        //     58: new 125	java/lang/IllegalArgumentException
        //     61: dup
        //     62: new 58	java/lang/StringBuilder
        //     65: dup
        //     66: invokespecial 59	java/lang/StringBuilder:<init>	()V
        //     69: ldc 127
        //     71: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     74: aload_2
        //     75: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     78: ldc 129
        //     80: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     83: invokevirtual 69	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     86: invokespecial 131	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     89: athrow
        //     90: astore_3
        //     91: new 125	java/lang/IllegalArgumentException
        //     94: dup
        //     95: new 58	java/lang/StringBuilder
        //     98: dup
        //     99: invokespecial 59	java/lang/StringBuilder:<init>	()V
        //     102: ldc 133
        //     104: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     107: aload_1
        //     108: invokevirtual 136	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     111: ldc 138
        //     113: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     116: invokevirtual 69	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     119: invokespecial 131	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     122: athrow
        //     123: astore 5
        //     125: new 125	java/lang/IllegalArgumentException
        //     128: dup
        //     129: new 58	java/lang/StringBuilder
        //     132: dup
        //     133: invokespecial 59	java/lang/StringBuilder:<init>	()V
        //     136: ldc 140
        //     138: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     141: aload_1
        //     142: invokevirtual 136	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     145: ldc 142
        //     147: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     150: invokevirtual 69	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     153: invokespecial 131	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     156: athrow
        //     157: aload 8
        //     159: areturn
        //     160: astore 9
        //     162: goto -109 -> 53
        //
        // Exception table:
        //     from	to	target	type
        //     0	7	90	java/lang/ClassCastException
        //     7	27	123	java/lang/NoSuchMethodException
        //     30	53	160	java/lang/Throwable
    }

    public Filter createFilterByClassName(String paramString1, String paramString2)
    {
        if (mLogVerbose)
            Log.v("FilterFactory", "Looking up class " + paramString1);
        Class localClass = null;
        Iterator localIterator = this.mPackages.iterator();
        while (true)
        {
            String str;
            if (localIterator.hasNext())
                str = (String)localIterator.next();
            try
            {
                if (mLogVerbose)
                    Log.v("FilterFactory", "Trying " + str + "." + paramString1);
                synchronized (mClassLoaderGuard)
                {
                    localClass = mCurrentClassLoader.loadClass(str + "." + paramString1);
                    if (localClass != null)
                    {
                        if (localClass != null)
                            break;
                        throw new IllegalArgumentException("Unknown filter class '" + paramString1 + "'!");
                    }
                }
            }
            catch (ClassNotFoundException localClassNotFoundException)
            {
            }
        }
        return createFilterByClass(localClass, paramString2);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterfw.core.FilterFactory
 * JD-Core Version:        0.6.2
 */