package android.filterfw.core;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

public class FilterFunction
{
    private Filter mFilter;
    private FilterContext mFilterContext;
    private boolean mFilterIsSetup = false;
    private FrameHolderPort[] mResultHolders;

    public FilterFunction(FilterContext paramFilterContext, Filter paramFilter)
    {
        this.mFilterContext = paramFilterContext;
        this.mFilter = paramFilter;
    }

    private void connectFilterOutputs()
    {
        int i = 0;
        this.mResultHolders = new FrameHolderPort[this.mFilter.getNumberOfOutputs()];
        Iterator localIterator = this.mFilter.getOutputPorts().iterator();
        while (localIterator.hasNext())
        {
            OutputPort localOutputPort = (OutputPort)localIterator.next();
            this.mResultHolders[i] = new FrameHolderPort();
            localOutputPort.connectTo(this.mResultHolders[i]);
            i++;
        }
    }

    public void close()
    {
        this.mFilter.performClose(this.mFilterContext);
    }

    public Frame execute(KeyValueMap paramKeyValueMap)
    {
        int i = this.mFilter.getNumberOfOutputs();
        if (i > 1)
            throw new RuntimeException("Calling execute on filter " + this.mFilter + " with multiple " + "outputs! Use executeMulti() instead!");
        if (!this.mFilterIsSetup)
        {
            connectFilterOutputs();
            this.mFilterIsSetup = true;
        }
        int j = 0;
        GLEnvironment localGLEnvironment = this.mFilterContext.getGLEnvironment();
        if ((localGLEnvironment != null) && (!localGLEnvironment.isActive()))
        {
            localGLEnvironment.activate();
            j = 1;
        }
        Iterator localIterator = paramKeyValueMap.entrySet().iterator();
        while (localIterator.hasNext())
        {
            Map.Entry localEntry = (Map.Entry)localIterator.next();
            if ((localEntry.getValue() instanceof Frame))
                this.mFilter.pushInputFrame((String)localEntry.getKey(), (Frame)localEntry.getValue());
            else
                this.mFilter.pushInputValue((String)localEntry.getKey(), localEntry.getValue());
        }
        if (this.mFilter.getStatus() != 3)
            this.mFilter.openOutputs();
        this.mFilter.performProcess(this.mFilterContext);
        Frame localFrame = null;
        if ((i == 1) && (this.mResultHolders[0].hasFrame()))
            localFrame = this.mResultHolders[0].pullFrame();
        if (j != 0)
            localGLEnvironment.deactivate();
        return localFrame;
    }

    public Frame executeWithArgList(Object[] paramArrayOfObject)
    {
        return execute(KeyValueMap.fromKeyValues(paramArrayOfObject));
    }

    public FilterContext getContext()
    {
        return this.mFilterContext;
    }

    public Filter getFilter()
    {
        return this.mFilter;
    }

    public void setInputFrame(String paramString, Frame paramFrame)
    {
        this.mFilter.setInputFrame(paramString, paramFrame);
    }

    public void setInputValue(String paramString, Object paramObject)
    {
        this.mFilter.setInputValue(paramString, paramObject);
    }

    public void tearDown()
    {
        this.mFilter.performTearDown(this.mFilterContext);
        this.mFilter = null;
    }

    public String toString()
    {
        return this.mFilter.getName();
    }

    private class FrameHolderPort extends StreamPort
    {
        public FrameHolderPort()
        {
            super("holder");
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterfw.core.FilterFunction
 * JD-Core Version:        0.6.2
 */