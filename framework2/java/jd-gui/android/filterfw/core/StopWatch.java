package android.filterfw.core;

import android.os.SystemClock;
import android.util.Log;

class StopWatch
{
    private int STOP_WATCH_LOGGING_PERIOD = 200;
    private String TAG = "MFF";
    private String mName;
    private int mNumCalls;
    private long mStartTime;
    private long mTotalTime;

    public StopWatch(String paramString)
    {
        this.mName = paramString;
        this.mStartTime = -1L;
        this.mTotalTime = 0L;
        this.mNumCalls = 0;
    }

    public void start()
    {
        if (this.mStartTime != -1L)
            throw new RuntimeException("Calling start with StopWatch already running");
        this.mStartTime = SystemClock.elapsedRealtime();
    }

    public void stop()
    {
        if (this.mStartTime == -1L)
            throw new RuntimeException("Calling stop with StopWatch already stopped");
        long l = SystemClock.elapsedRealtime();
        this.mTotalTime += l - this.mStartTime;
        this.mNumCalls = (1 + this.mNumCalls);
        this.mStartTime = -1L;
        if (this.mNumCalls % this.STOP_WATCH_LOGGING_PERIOD == 0)
        {
            String str = this.TAG;
            StringBuilder localStringBuilder = new StringBuilder().append("AVG ms/call ").append(this.mName).append(": ");
            Object[] arrayOfObject = new Object[1];
            arrayOfObject[0] = Float.valueOf(1.0F * (float)this.mTotalTime / this.mNumCalls);
            Log.i(str, String.format("%.1f", arrayOfObject));
            this.mTotalTime = 0L;
            this.mNumCalls = 0;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterfw.core.StopWatch
 * JD-Core Version:        0.6.2
 */