package android.filterfw.core;

import android.filterfw.format.ObjectFormat;
import android.graphics.Bitmap;
import java.nio.ByteBuffer;

public class SimpleFrame extends Frame
{
    private Object mObject;

    SimpleFrame(FrameFormat paramFrameFormat, FrameManager paramFrameManager)
    {
        super(paramFrameFormat, paramFrameManager);
        initWithFormat(paramFrameFormat);
        setReusable(false);
    }

    private void initWithFormat(FrameFormat paramFrameFormat)
    {
        int i = paramFrameFormat.getLength();
        switch (paramFrameFormat.getBaseType())
        {
        default:
            this.mObject = null;
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        }
        while (true)
        {
            return;
            this.mObject = new byte[i];
            continue;
            this.mObject = new short[i];
            continue;
            this.mObject = new int[i];
            continue;
            this.mObject = new float[i];
            continue;
            this.mObject = new double[i];
        }
    }

    private void setFormatObjectClass(Class paramClass)
    {
        MutableFrameFormat localMutableFrameFormat = getFormat().mutableCopy();
        localMutableFrameFormat.setObjectClass(paramClass);
        setFormat(localMutableFrameFormat);
    }

    static SimpleFrame wrapObject(Object paramObject, FrameManager paramFrameManager)
    {
        SimpleFrame localSimpleFrame = new SimpleFrame(ObjectFormat.fromObject(paramObject, 1), paramFrameManager);
        localSimpleFrame.setObjectValue(paramObject);
        return localSimpleFrame;
    }

    public Bitmap getBitmap()
    {
        if ((this.mObject instanceof Bitmap));
        for (Bitmap localBitmap = (Bitmap)this.mObject; ; localBitmap = null)
            return localBitmap;
    }

    public ByteBuffer getData()
    {
        if ((this.mObject instanceof ByteBuffer));
        for (ByteBuffer localByteBuffer = (ByteBuffer)this.mObject; ; localByteBuffer = null)
            return localByteBuffer;
    }

    public float[] getFloats()
    {
        if ((this.mObject instanceof float[]));
        for (float[] arrayOfFloat = (float[])this.mObject; ; arrayOfFloat = null)
            return arrayOfFloat;
    }

    public int[] getInts()
    {
        if ((this.mObject instanceof int[]));
        for (int[] arrayOfInt = (int[])this.mObject; ; arrayOfInt = null)
            return arrayOfInt;
    }

    public Object getObjectValue()
    {
        return this.mObject;
    }

    protected boolean hasNativeAllocation()
    {
        return false;
    }

    protected void releaseNativeAllocation()
    {
    }

    public void setBitmap(Bitmap paramBitmap)
    {
        assertFrameMutable();
        setGenericObjectValue(paramBitmap);
    }

    public void setData(ByteBuffer paramByteBuffer, int paramInt1, int paramInt2)
    {
        assertFrameMutable();
        setGenericObjectValue(ByteBuffer.wrap(paramByteBuffer.array(), paramInt1, paramInt2));
    }

    public void setFloats(float[] paramArrayOfFloat)
    {
        assertFrameMutable();
        setGenericObjectValue(paramArrayOfFloat);
    }

    protected void setGenericObjectValue(Object paramObject)
    {
        FrameFormat localFrameFormat = getFormat();
        if (localFrameFormat.getObjectClass() == null)
            setFormatObjectClass(paramObject.getClass());
        while (localFrameFormat.getObjectClass().isAssignableFrom(paramObject.getClass()))
        {
            this.mObject = paramObject;
            return;
        }
        throw new RuntimeException("Attempting to set object value of type '" + paramObject.getClass() + "' on " + "SimpleFrame of type '" + localFrameFormat.getObjectClass() + "'!");
    }

    public void setInts(int[] paramArrayOfInt)
    {
        assertFrameMutable();
        setGenericObjectValue(paramArrayOfInt);
    }

    public String toString()
    {
        return "SimpleFrame (" + getFormat() + ")";
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterfw.core.SimpleFrame
 * JD-Core Version:        0.6.2
 */