package android.filterfw.core;

public class ProtocolException extends RuntimeException
{
    public ProtocolException()
    {
    }

    public ProtocolException(String paramString)
    {
        super(paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterfw.core.ProtocolException
 * JD-Core Version:        0.6.2
 */