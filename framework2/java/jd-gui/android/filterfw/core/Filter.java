package android.filterfw.core;

import android.filterfw.format.ObjectFormat;
import android.filterfw.io.GraphIOException;
import android.filterfw.io.TextGraphReader;
import android.util.Log;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

public abstract class Filter
{
    static final int STATUS_ERROR = 6;
    static final int STATUS_FINISHED = 5;
    static final int STATUS_PREINIT = 0;
    static final int STATUS_PREPARED = 2;
    static final int STATUS_PROCESSING = 3;
    static final int STATUS_RELEASED = 7;
    static final int STATUS_SLEEPING = 4;
    static final int STATUS_UNPREPARED = 1;
    private static final String TAG = "Filter";
    private long mCurrentTimestamp;
    private HashSet<Frame> mFramesToRelease;
    private HashMap<String, Frame> mFramesToSet;
    private int mInputCount = -1;
    private HashMap<String, InputPort> mInputPorts;
    private boolean mIsOpen = false;
    private boolean mLogVerbose;
    private String mName;
    private int mOutputCount = -1;
    private HashMap<String, OutputPort> mOutputPorts;
    private int mSleepDelay;
    private int mStatus = 0;

    public Filter(String paramString)
    {
        this.mName = paramString;
        this.mFramesToRelease = new HashSet();
        this.mFramesToSet = new HashMap();
        this.mStatus = 0;
        this.mLogVerbose = Log.isLoggable("Filter", 2);
    }

    private final void addAndSetFinalPorts(KeyValueMap paramKeyValueMap)
    {
        Field[] arrayOfField = getClass().getDeclaredFields();
        int i = arrayOfField.length;
        int j = 0;
        if (j < i)
        {
            Field localField = arrayOfField[j];
            Annotation localAnnotation = localField.getAnnotation(GenerateFinalPort.class);
            GenerateFinalPort localGenerateFinalPort;
            String str;
            if (localAnnotation != null)
            {
                localGenerateFinalPort = (GenerateFinalPort)localAnnotation;
                if (!localGenerateFinalPort.name().isEmpty())
                    break label117;
                str = localField.getName();
                label67: addFieldPort(str, localField, localGenerateFinalPort.hasDefault(), true);
                if (!paramKeyValueMap.containsKey(str))
                    break label129;
                setImmediateInputValue(str, paramKeyValueMap.get(str));
                paramKeyValueMap.remove(str);
            }
            label117: 
            while (localGenerateFinalPort.hasDefault())
            {
                j++;
                break;
                str = localGenerateFinalPort.name();
                break label67;
            }
            label129: throw new RuntimeException("No value specified for final input port '" + str + "' of filter " + this + "!");
        }
    }

    private final void addAnnotatedPorts()
    {
        Field[] arrayOfField = getClass().getDeclaredFields();
        int i = arrayOfField.length;
        int j = 0;
        if (j < i)
        {
            Field localField = arrayOfField[j];
            Annotation localAnnotation1 = localField.getAnnotation(GenerateFieldPort.class);
            if (localAnnotation1 != null)
                addFieldGenerator((GenerateFieldPort)localAnnotation1, localField);
            while (true)
            {
                j++;
                break;
                Annotation localAnnotation2 = localField.getAnnotation(GenerateProgramPort.class);
                if (localAnnotation2 != null)
                {
                    addProgramGenerator((GenerateProgramPort)localAnnotation2, localField);
                }
                else
                {
                    Annotation localAnnotation3 = localField.getAnnotation(GenerateProgramPorts.class);
                    if (localAnnotation3 != null)
                    {
                        GenerateProgramPort[] arrayOfGenerateProgramPort = ((GenerateProgramPorts)localAnnotation3).value();
                        int k = arrayOfGenerateProgramPort.length;
                        for (int m = 0; m < k; m++)
                            addProgramGenerator(arrayOfGenerateProgramPort[m], localField);
                    }
                }
            }
        }
    }

    private final void addFieldGenerator(GenerateFieldPort paramGenerateFieldPort, Field paramField)
    {
        if (paramGenerateFieldPort.name().isEmpty());
        for (String str = paramField.getName(); ; str = paramGenerateFieldPort.name())
        {
            addFieldPort(str, paramField, paramGenerateFieldPort.hasDefault(), false);
            return;
        }
    }

    private final void addProgramGenerator(GenerateProgramPort paramGenerateProgramPort, Field paramField)
    {
        String str1 = paramGenerateProgramPort.name();
        if (paramGenerateProgramPort.variableName().isEmpty());
        for (String str2 = str1; ; str2 = paramGenerateProgramPort.variableName())
        {
            addProgramPort(str1, str2, paramField, paramGenerateProgramPort.type(), paramGenerateProgramPort.hasDefault());
            return;
        }
    }

    private final void closePorts()
    {
        if (this.mLogVerbose)
            Log.v("Filter", "Closing all ports on " + this + "!");
        Iterator localIterator1 = this.mInputPorts.values().iterator();
        while (localIterator1.hasNext())
            ((InputPort)localIterator1.next()).close();
        Iterator localIterator2 = this.mOutputPorts.values().iterator();
        while (localIterator2.hasNext())
            ((OutputPort)localIterator2.next()).close();
    }

    private final boolean filterMustClose()
    {
        boolean bool = true;
        Iterator localIterator1 = this.mInputPorts.values().iterator();
        while (true)
            if (localIterator1.hasNext())
            {
                InputPort localInputPort = (InputPort)localIterator1.next();
                if (localInputPort.filterMustClose())
                    if (this.mLogVerbose)
                        Log.v("Filter", "Filter " + this + " must close due to port " + localInputPort);
            }
        while (true)
        {
            return bool;
            Iterator localIterator2 = this.mOutputPorts.values().iterator();
            while (true)
                if (localIterator2.hasNext())
                {
                    OutputPort localOutputPort = (OutputPort)localIterator2.next();
                    if (localOutputPort.filterMustClose())
                    {
                        if (!this.mLogVerbose)
                            break;
                        Log.v("Filter", "Filter " + this + " must close due to port " + localOutputPort);
                        break;
                    }
                }
            bool = false;
        }
    }

    private final void initFinalPorts(KeyValueMap paramKeyValueMap)
    {
        this.mInputPorts = new HashMap();
        this.mOutputPorts = new HashMap();
        addAndSetFinalPorts(paramKeyValueMap);
    }

    private final void initRemainingPorts(KeyValueMap paramKeyValueMap)
    {
        addAnnotatedPorts();
        setupPorts();
        setInitialInputValues(paramKeyValueMap);
    }

    private final boolean inputConditionsMet()
    {
        Iterator localIterator = this.mInputPorts.values().iterator();
        while (true)
            if (localIterator.hasNext())
            {
                InputPort localInputPort = (InputPort)localIterator.next();
                if (!localInputPort.isReady())
                    if (this.mLogVerbose)
                        Log.v("Filter", "Input condition not met: " + localInputPort + "!");
            }
        for (boolean bool = false; ; bool = true)
            return bool;
    }

    public static final boolean isAvailable(String paramString)
    {
        boolean bool = false;
        ClassLoader localClassLoader = Thread.currentThread().getContextClassLoader();
        try
        {
            localClass = localClassLoader.loadClass(paramString);
        }
        catch (ClassNotFoundException localClassNotFoundException)
        {
            try
            {
                Class localClass;
                localClass.asSubclass(Filter.class);
                bool = true;
                while (true)
                {
                    label26: return bool;
                    localClassNotFoundException = localClassNotFoundException;
                }
            }
            catch (ClassCastException localClassCastException)
            {
                break label26;
            }
        }
    }

    private final boolean outputConditionsMet()
    {
        Iterator localIterator = this.mOutputPorts.values().iterator();
        while (true)
            if (localIterator.hasNext())
            {
                OutputPort localOutputPort = (OutputPort)localIterator.next();
                if (!localOutputPort.isReady())
                    if (this.mLogVerbose)
                        Log.v("Filter", "Output condition not met: " + localOutputPort + "!");
            }
        for (boolean bool = false; ; bool = true)
            return bool;
    }

    private final void releasePulledFrames(FilterContext paramFilterContext)
    {
        Iterator localIterator = this.mFramesToRelease.iterator();
        while (localIterator.hasNext())
        {
            Frame localFrame = (Frame)localIterator.next();
            paramFilterContext.getFrameManager().releaseFrame(localFrame);
        }
        this.mFramesToRelease.clear();
    }

    private final void setImmediateInputValue(String paramString, Object paramObject)
    {
        if (this.mLogVerbose)
            Log.v("Filter", "Setting immediate value " + paramObject + " for port " + paramString + "!");
        InputPort localInputPort = getInputPort(paramString);
        localInputPort.open();
        localInputPort.setFrame(SimpleFrame.wrapObject(paramObject, null));
    }

    private final void setInitialInputValues(KeyValueMap paramKeyValueMap)
    {
        Iterator localIterator = paramKeyValueMap.entrySet().iterator();
        while (localIterator.hasNext())
        {
            Map.Entry localEntry = (Map.Entry)localIterator.next();
            setInputValue((String)localEntry.getKey(), localEntry.getValue());
        }
    }

    private final void transferInputFrames(FilterContext paramFilterContext)
    {
        Iterator localIterator = this.mInputPorts.values().iterator();
        while (localIterator.hasNext())
            ((InputPort)localIterator.next()).transfer(paramFilterContext);
    }

    private final Frame wrapInputValue(String paramString, Object paramObject)
    {
        int i = 1;
        MutableFrameFormat localMutableFrameFormat = ObjectFormat.fromObject(paramObject, i);
        FrameFormat localFrameFormat;
        Class localClass;
        if (paramObject == null)
        {
            localFrameFormat = getInputPort(paramString).getPortFormat();
            if (localFrameFormat == null)
            {
                localClass = null;
                localMutableFrameFormat.setObjectClass(localClass);
            }
        }
        else
        {
            if (((paramObject instanceof Number)) || ((paramObject instanceof Boolean)) || ((paramObject instanceof String)) || (!(paramObject instanceof Serializable)))
                break label101;
            label66: if (i == 0)
                break label106;
        }
        label101: label106: for (Object localObject = new SerializedFrame(localMutableFrameFormat, null); ; localObject = new SimpleFrame(localMutableFrameFormat, null))
        {
            ((Frame)localObject).setObjectValue(paramObject);
            return localObject;
            localClass = localFrameFormat.getObjectClass();
            break;
            i = 0;
            break label66;
        }
    }

    protected void addFieldPort(String paramString, Field paramField, boolean paramBoolean1, boolean paramBoolean2)
    {
        paramField.setAccessible(true);
        if (paramBoolean2);
        for (Object localObject = new FinalPort(this, paramString, paramField, paramBoolean1); ; localObject = new FieldPort(this, paramString, paramField, paramBoolean1))
        {
            if (this.mLogVerbose)
                Log.v("Filter", "Filter " + this + " adding " + localObject);
            ((InputPort)localObject).setPortFormat(ObjectFormat.fromClass(paramField.getType(), 1));
            this.mInputPorts.put(paramString, localObject);
            return;
        }
    }

    protected void addInputPort(String paramString)
    {
        addMaskedInputPort(paramString, null);
    }

    protected void addMaskedInputPort(String paramString, FrameFormat paramFrameFormat)
    {
        StreamPort localStreamPort = new StreamPort(this, paramString);
        if (this.mLogVerbose)
            Log.v("Filter", "Filter " + this + " adding " + localStreamPort);
        this.mInputPorts.put(paramString, localStreamPort);
        localStreamPort.setPortFormat(paramFrameFormat);
    }

    protected void addOutputBasedOnInput(String paramString1, String paramString2)
    {
        OutputPort localOutputPort = new OutputPort(this, paramString1);
        if (this.mLogVerbose)
            Log.v("Filter", "Filter " + this + " adding " + localOutputPort);
        localOutputPort.setBasePort(getInputPort(paramString2));
        this.mOutputPorts.put(paramString1, localOutputPort);
    }

    protected void addOutputPort(String paramString, FrameFormat paramFrameFormat)
    {
        OutputPort localOutputPort = new OutputPort(this, paramString);
        if (this.mLogVerbose)
            Log.v("Filter", "Filter " + this + " adding " + localOutputPort);
        localOutputPort.setPortFormat(paramFrameFormat);
        this.mOutputPorts.put(paramString, localOutputPort);
    }

    protected void addProgramPort(String paramString1, String paramString2, Field paramField, Class paramClass, boolean paramBoolean)
    {
        paramField.setAccessible(true);
        ProgramPort localProgramPort = new ProgramPort(this, paramString1, paramString2, paramField, paramBoolean);
        if (this.mLogVerbose)
            Log.v("Filter", "Filter " + this + " adding " + localProgramPort);
        localProgramPort.setPortFormat(ObjectFormat.fromClass(paramClass, 1));
        this.mInputPorts.put(paramString1, localProgramPort);
    }

    /** @deprecated */
    final boolean canProcess()
    {
        boolean bool1 = false;
        try
        {
            if (this.mLogVerbose)
                Log.v("Filter", "Checking if can process: " + this + " (" + this.mStatus + ").");
            if ((this.mStatus <= 3) && (inputConditionsMet()))
            {
                boolean bool2 = outputConditionsMet();
                if (bool2)
                    bool1 = true;
            }
            return bool1;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    final void clearInputs()
    {
        Iterator localIterator = this.mInputPorts.values().iterator();
        while (localIterator.hasNext())
            ((InputPort)localIterator.next()).clear();
    }

    final void clearOutputs()
    {
        Iterator localIterator = this.mOutputPorts.values().iterator();
        while (localIterator.hasNext())
            ((OutputPort)localIterator.next()).clear();
    }

    public void close(FilterContext paramFilterContext)
    {
    }

    protected void closeOutputPort(String paramString)
    {
        getOutputPort(paramString).close();
    }

    protected void delayNextProcess(int paramInt)
    {
        this.mSleepDelay = paramInt;
        this.mStatus = 4;
    }

    public void fieldPortValueUpdated(String paramString, FilterContext paramFilterContext)
    {
    }

    public String getFilterClassName()
    {
        return getClass().getSimpleName();
    }

    public final FrameFormat getInputFormat(String paramString)
    {
        return getInputPort(paramString).getSourceFormat();
    }

    public final InputPort getInputPort(String paramString)
    {
        if (this.mInputPorts == null)
            throw new NullPointerException("Attempting to access input port '" + paramString + "' of " + this + " before Filter has been initialized!");
        InputPort localInputPort = (InputPort)this.mInputPorts.get(paramString);
        if (localInputPort == null)
            throw new IllegalArgumentException("Unknown input port '" + paramString + "' on filter " + this + "!");
        return localInputPort;
    }

    final Collection<InputPort> getInputPorts()
    {
        return this.mInputPorts.values();
    }

    public final String getName()
    {
        return this.mName;
    }

    public final int getNumberOfConnectedInputs()
    {
        int i = 0;
        Iterator localIterator = this.mInputPorts.values().iterator();
        while (localIterator.hasNext())
            if (((InputPort)localIterator.next()).isConnected())
                i++;
        return i;
    }

    public final int getNumberOfConnectedOutputs()
    {
        int i = 0;
        Iterator localIterator = this.mOutputPorts.values().iterator();
        while (localIterator.hasNext())
            if (((OutputPort)localIterator.next()).isConnected())
                i++;
        return i;
    }

    public final int getNumberOfInputs()
    {
        if (this.mOutputPorts == null);
        for (int i = 0; ; i = this.mInputPorts.size())
            return i;
    }

    public final int getNumberOfOutputs()
    {
        if (this.mInputPorts == null);
        for (int i = 0; ; i = this.mOutputPorts.size())
            return i;
    }

    public FrameFormat getOutputFormat(String paramString, FrameFormat paramFrameFormat)
    {
        return null;
    }

    public final OutputPort getOutputPort(String paramString)
    {
        if (this.mInputPorts == null)
            throw new NullPointerException("Attempting to access output port '" + paramString + "' of " + this + " before Filter has been initialized!");
        OutputPort localOutputPort = (OutputPort)this.mOutputPorts.get(paramString);
        if (localOutputPort == null)
            throw new IllegalArgumentException("Unknown output port '" + paramString + "' on filter " + this + "!");
        return localOutputPort;
    }

    final Collection<OutputPort> getOutputPorts()
    {
        return this.mOutputPorts.values();
    }

    public final int getSleepDelay()
    {
        return 250;
    }

    /** @deprecated */
    final int getStatus()
    {
        try
        {
            int i = this.mStatus;
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public final void init()
        throws ProtocolException
    {
        initWithValueMap(new KeyValueMap());
    }

    protected void initProgramInputs(Program paramProgram, FilterContext paramFilterContext)
    {
        if (paramProgram != null)
        {
            Iterator localIterator = this.mInputPorts.values().iterator();
            while (localIterator.hasNext())
            {
                InputPort localInputPort = (InputPort)localIterator.next();
                if (localInputPort.getTarget() == paramProgram)
                    localInputPort.transfer(paramFilterContext);
            }
        }
    }

    public final void initWithAssignmentList(Object[] paramArrayOfObject)
    {
        KeyValueMap localKeyValueMap = new KeyValueMap();
        localKeyValueMap.setKeyValues(paramArrayOfObject);
        initWithValueMap(localKeyValueMap);
    }

    public final void initWithAssignmentString(String paramString)
    {
        try
        {
            initWithValueMap(new TextGraphReader().readKeyValueAssignments(paramString));
            return;
        }
        catch (GraphIOException localGraphIOException)
        {
            throw new IllegalArgumentException(localGraphIOException.getMessage());
        }
    }

    public final void initWithValueMap(KeyValueMap paramKeyValueMap)
    {
        initFinalPorts(paramKeyValueMap);
        initRemainingPorts(paramKeyValueMap);
        this.mStatus = 1;
    }

    public boolean isOpen()
    {
        return this.mIsOpen;
    }

    final void notifyFieldPortValueUpdated(String paramString, FilterContext paramFilterContext)
    {
        if ((this.mStatus == 3) || (this.mStatus == 2))
            fieldPortValueUpdated(paramString, paramFilterContext);
    }

    public void open(FilterContext paramFilterContext)
    {
    }

    final void openOutputs()
    {
        if (this.mLogVerbose)
            Log.v("Filter", "Opening all output ports on " + this + "!");
        Iterator localIterator = this.mOutputPorts.values().iterator();
        while (localIterator.hasNext())
        {
            OutputPort localOutputPort = (OutputPort)localIterator.next();
            if (!localOutputPort.isOpen())
                localOutputPort.open();
        }
    }

    protected void parametersUpdated(Set<String> paramSet)
    {
    }

    /** @deprecated */
    final void performClose(FilterContext paramFilterContext)
    {
        try
        {
            if (this.mIsOpen)
            {
                if (this.mLogVerbose)
                    Log.v("Filter", "Closing " + this);
                this.mIsOpen = false;
                this.mStatus = 2;
                close(paramFilterContext);
                closePorts();
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    final void performOpen(FilterContext paramFilterContext)
    {
        try
        {
            if (this.mIsOpen)
                break label181;
            if (this.mStatus == 1)
            {
                if (this.mLogVerbose)
                    Log.v("Filter", "Preparing " + this);
                prepare(paramFilterContext);
                this.mStatus = 2;
            }
            if (this.mStatus == 2)
            {
                if (this.mLogVerbose)
                    Log.v("Filter", "Opening " + this);
                open(paramFilterContext);
                this.mStatus = 3;
            }
            if (this.mStatus != 3)
                throw new RuntimeException("Filter " + this + " was brought into invalid state during " + "opening (state: " + this.mStatus + ")!");
        }
        finally
        {
        }
        this.mIsOpen = true;
        label181:
    }

    /** @deprecated */
    final void performProcess(FilterContext paramFilterContext)
    {
        try
        {
            if (this.mStatus == 7)
                throw new RuntimeException("Filter " + this + " is already torn down!");
        }
        finally
        {
        }
        transferInputFrames(paramFilterContext);
        if (this.mStatus < 3)
            performOpen(paramFilterContext);
        if (this.mLogVerbose)
            Log.v("Filter", "Processing " + this);
        this.mCurrentTimestamp = -1L;
        process(paramFilterContext);
        releasePulledFrames(paramFilterContext);
        if (filterMustClose())
            performClose(paramFilterContext);
    }

    /** @deprecated */
    final void performTearDown(FilterContext paramFilterContext)
    {
        try
        {
            performClose(paramFilterContext);
            if (this.mStatus != 7)
            {
                tearDown(paramFilterContext);
                this.mStatus = 7;
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    protected void prepare(FilterContext paramFilterContext)
    {
    }

    public abstract void process(FilterContext paramFilterContext);

    protected final Frame pullInput(String paramString)
    {
        Frame localFrame = getInputPort(paramString).pullFrame();
        if (this.mCurrentTimestamp == -1L)
        {
            this.mCurrentTimestamp = localFrame.getTimestamp();
            if (this.mLogVerbose)
                Log.v("Filter", "Default-setting current timestamp from input port " + paramString + " to " + this.mCurrentTimestamp);
        }
        this.mFramesToRelease.add(localFrame);
        return localFrame;
    }

    /** @deprecated */
    final void pushInputFrame(String paramString, Frame paramFrame)
    {
        try
        {
            InputPort localInputPort = getInputPort(paramString);
            if (!localInputPort.isOpen())
                localInputPort.open();
            localInputPort.pushFrame(paramFrame);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    final void pushInputValue(String paramString, Object paramObject)
    {
        try
        {
            pushInputFrame(paramString, wrapInputValue(paramString, paramObject));
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    protected final void pushOutput(String paramString, Frame paramFrame)
    {
        if (paramFrame.getTimestamp() == -2L)
        {
            if (this.mLogVerbose)
                Log.v("Filter", "Default-setting output Frame timestamp on port " + paramString + " to " + this.mCurrentTimestamp);
            paramFrame.setTimestamp(this.mCurrentTimestamp);
        }
        getOutputPort(paramString).pushFrame(paramFrame);
    }

    public void setInputFrame(String paramString, Frame paramFrame)
    {
        InputPort localInputPort = getInputPort(paramString);
        if (!localInputPort.isOpen())
            localInputPort.open();
        localInputPort.setFrame(paramFrame);
    }

    public final void setInputValue(String paramString, Object paramObject)
    {
        setInputFrame(paramString, wrapInputValue(paramString, paramObject));
    }

    protected void setWaitsOnInputPort(String paramString, boolean paramBoolean)
    {
        getInputPort(paramString).setBlocking(paramBoolean);
    }

    protected void setWaitsOnOutputPort(String paramString, boolean paramBoolean)
    {
        getOutputPort(paramString).setBlocking(paramBoolean);
    }

    public abstract void setupPorts();

    public void tearDown(FilterContext paramFilterContext)
    {
    }

    public String toString()
    {
        return "'" + getName() + "' (" + getFilterClassName() + ")";
    }

    protected void transferInputPortFrame(String paramString, FilterContext paramFilterContext)
    {
        getInputPort(paramString).transfer(paramFilterContext);
    }

    /** @deprecated */
    final void unsetStatus(int paramInt)
    {
        try
        {
            this.mStatus &= (paramInt ^ 0xFFFFFFFF);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterfw.core.Filter
 * JD-Core Version:        0.6.2
 */