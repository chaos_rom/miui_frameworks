package android.filterfw.core;

import android.os.AsyncTask;
import android.util.Log;

public class AsyncRunner extends GraphRunner
{
    private static final String TAG = "AsyncRunner";
    private boolean isProcessing;
    private GraphRunner.OnRunnerDoneListener mDoneListener;
    private Exception mException;
    private boolean mLogVerbose;
    private AsyncRunnerTask mRunTask;
    private SyncRunner mRunner;
    private Class mSchedulerClass;

    public AsyncRunner(FilterContext paramFilterContext)
    {
        super(paramFilterContext);
        this.mSchedulerClass = SimpleScheduler.class;
        this.mLogVerbose = Log.isLoggable("AsyncRunner", 2);
    }

    public AsyncRunner(FilterContext paramFilterContext, Class paramClass)
    {
        super(paramFilterContext);
        this.mSchedulerClass = paramClass;
        this.mLogVerbose = Log.isLoggable("AsyncRunner", 2);
    }

    /** @deprecated */
    private void setException(Exception paramException)
    {
        try
        {
            this.mException = paramException;
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    private void setRunning(boolean paramBoolean)
    {
        try
        {
            this.isProcessing = paramBoolean;
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void close()
    {
        try
        {
            if (isRunning())
                throw new RuntimeException("Cannot close graph while it is running!");
        }
        finally
        {
        }
        if (this.mLogVerbose)
            Log.v("AsyncRunner", "Closing filters.");
        this.mRunner.close();
    }

    /** @deprecated */
    public Exception getError()
    {
        try
        {
            Exception localException = this.mException;
            return localException;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public FilterGraph getGraph()
    {
        if (this.mRunner != null);
        for (FilterGraph localFilterGraph = this.mRunner.getGraph(); ; localFilterGraph = null)
            return localFilterGraph;
    }

    /** @deprecated */
    public boolean isRunning()
    {
        try
        {
            boolean bool = this.isProcessing;
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void run()
    {
        try
        {
            if (this.mLogVerbose)
                Log.v("AsyncRunner", "Running graph.");
            setException(null);
            if (isRunning())
                throw new RuntimeException("Graph is already running!");
        }
        finally
        {
        }
        if (this.mRunner == null)
            throw new RuntimeException("Cannot run before a graph is set!");
        this.mRunTask = new AsyncRunnerTask(null);
        setRunning(true);
        AsyncRunnerTask localAsyncRunnerTask = this.mRunTask;
        SyncRunner[] arrayOfSyncRunner = new SyncRunner[1];
        arrayOfSyncRunner[0] = this.mRunner;
        localAsyncRunnerTask.execute(arrayOfSyncRunner);
    }

    public void setDoneCallback(GraphRunner.OnRunnerDoneListener paramOnRunnerDoneListener)
    {
        this.mDoneListener = paramOnRunnerDoneListener;
    }

    /** @deprecated */
    public void setGraph(FilterGraph paramFilterGraph)
    {
        try
        {
            if (isRunning())
                throw new RuntimeException("Graph is already running!");
        }
        finally
        {
        }
        this.mRunner = new SyncRunner(this.mFilterContext, paramFilterGraph, this.mSchedulerClass);
    }

    /** @deprecated */
    public void stop()
    {
        try
        {
            if ((this.mRunTask != null) && (!this.mRunTask.isCancelled()))
            {
                if (this.mLogVerbose)
                    Log.v("AsyncRunner", "Stopping graph.");
                this.mRunTask.cancel(false);
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private class AsyncRunnerTask extends AsyncTask<SyncRunner, Void, AsyncRunner.RunnerResult>
    {
        private static final String TAG = "AsyncRunnerTask";

        private AsyncRunnerTask()
        {
        }

        protected AsyncRunner.RunnerResult doInBackground(SyncRunner[] paramArrayOfSyncRunner)
        {
            AsyncRunner.RunnerResult localRunnerResult = new AsyncRunner.RunnerResult(AsyncRunner.this, null);
            try
            {
                if (paramArrayOfSyncRunner.length > 1)
                    throw new RuntimeException("More than one runner received!");
            }
            catch (Exception localException1)
            {
                localRunnerResult.exception = localException1;
                localRunnerResult.status = 6;
            }
            try
            {
                while (true)
                {
                    AsyncRunner.this.deactivateGlContext();
                    if (AsyncRunner.this.mLogVerbose)
                        Log.v("AsyncRunnerTask", "Done with background graph processing.");
                    return localRunnerResult;
                    paramArrayOfSyncRunner[0].assertReadyToStep();
                    if (AsyncRunner.this.mLogVerbose)
                        Log.v("AsyncRunnerTask", "Starting background graph processing.");
                    AsyncRunner.this.activateGlContext();
                    if (AsyncRunner.this.mLogVerbose)
                        Log.v("AsyncRunnerTask", "Preparing filter graph for processing.");
                    paramArrayOfSyncRunner[0].beginProcessing();
                    if (AsyncRunner.this.mLogVerbose)
                        Log.v("AsyncRunnerTask", "Running graph.");
                    localRunnerResult.status = 1;
                    while ((!isCancelled()) && (localRunnerResult.status == 1))
                        if (!paramArrayOfSyncRunner[0].performStep())
                        {
                            localRunnerResult.status = paramArrayOfSyncRunner[0].determinePostRunState();
                            if (localRunnerResult.status == 3)
                            {
                                paramArrayOfSyncRunner[0].waitUntilWake();
                                localRunnerResult.status = 1;
                            }
                        }
                    if (isCancelled())
                        localRunnerResult.status = 5;
                }
            }
            catch (Exception localException2)
            {
                while (true)
                {
                    localRunnerResult.exception = localException2;
                    localRunnerResult.status = 6;
                }
            }
        }

        protected void onCancelled(AsyncRunner.RunnerResult paramRunnerResult)
        {
            onPostExecute(paramRunnerResult);
        }

        protected void onPostExecute(AsyncRunner.RunnerResult paramRunnerResult)
        {
            if (AsyncRunner.this.mLogVerbose)
                Log.v("AsyncRunnerTask", "Starting post-execute.");
            AsyncRunner.this.setRunning(false);
            if (paramRunnerResult == null)
            {
                paramRunnerResult = new AsyncRunner.RunnerResult(AsyncRunner.this, null);
                paramRunnerResult.status = 5;
            }
            AsyncRunner.this.setException(paramRunnerResult.exception);
            if ((paramRunnerResult.status == 5) || (paramRunnerResult.status == 6))
                if (AsyncRunner.this.mLogVerbose)
                    Log.v("AsyncRunnerTask", "Closing filters.");
            try
            {
                AsyncRunner.this.mRunner.close();
                if (AsyncRunner.this.mDoneListener != null)
                {
                    if (AsyncRunner.this.mLogVerbose)
                        Log.v("AsyncRunnerTask", "Calling graph done callback.");
                    AsyncRunner.this.mDoneListener.onRunnerDone(paramRunnerResult.status);
                }
                if (AsyncRunner.this.mLogVerbose)
                    Log.v("AsyncRunnerTask", "Completed post-execute.");
                return;
            }
            catch (Exception localException)
            {
                while (true)
                {
                    paramRunnerResult.status = 6;
                    AsyncRunner.this.setException(localException);
                }
            }
        }
    }

    private class RunnerResult
    {
        public Exception exception;
        public int status = 0;

        private RunnerResult()
        {
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterfw.core.AsyncRunner
 * JD-Core Version:        0.6.2
 */