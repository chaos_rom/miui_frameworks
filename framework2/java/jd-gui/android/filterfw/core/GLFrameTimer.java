package android.filterfw.core;

class GLFrameTimer
{
    private static StopWatchMap mTimer = null;

    public static StopWatchMap get()
    {
        if (mTimer == null)
            mTimer = new StopWatchMap();
        return mTimer;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterfw.core.GLFrameTimer
 * JD-Core Version:        0.6.2
 */