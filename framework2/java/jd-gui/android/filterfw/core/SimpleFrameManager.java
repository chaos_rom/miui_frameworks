package android.filterfw.core;

public class SimpleFrameManager extends FrameManager
{
    private Frame createNewFrame(FrameFormat paramFrameFormat)
    {
        Object localObject;
        switch (paramFrameFormat.getTarget())
        {
        default:
            throw new RuntimeException("Unsupported frame target type: " + FrameFormat.targetToString(paramFrameFormat.getTarget()) + "!");
        case 1:
            localObject = new SimpleFrame(paramFrameFormat, this);
        case 2:
        case 3:
        case 4:
        }
        while (true)
        {
            return localObject;
            localObject = new NativeFrame(paramFrameFormat, this);
            continue;
            GLFrame localGLFrame = new GLFrame(paramFrameFormat, this);
            localGLFrame.init(getGLEnvironment());
            localObject = localGLFrame;
            continue;
            localObject = new VertexFrame(paramFrameFormat, this);
        }
    }

    public Frame newBoundFrame(FrameFormat paramFrameFormat, int paramInt, long paramLong)
    {
        switch (paramFrameFormat.getTarget())
        {
        default:
            throw new RuntimeException("Attached frames are not supported for target type: " + FrameFormat.targetToString(paramFrameFormat.getTarget()) + "!");
        case 3:
        }
        GLFrame localGLFrame = new GLFrame(paramFrameFormat, this, paramInt, paramLong);
        localGLFrame.init(getGLEnvironment());
        return localGLFrame;
    }

    public Frame newFrame(FrameFormat paramFrameFormat)
    {
        return createNewFrame(paramFrameFormat);
    }

    public Frame releaseFrame(Frame paramFrame)
    {
        int i = paramFrame.decRefCount();
        if ((i == 0) && (paramFrame.hasNativeAllocation()))
        {
            paramFrame.releaseNativeAllocation();
            paramFrame = null;
        }
        while (i >= 0)
            return paramFrame;
        throw new RuntimeException("Frame reference count dropped below 0!");
    }

    public Frame retainFrame(Frame paramFrame)
    {
        paramFrame.incRefCount();
        return paramFrame;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterfw.core.SimpleFrameManager
 * JD-Core Version:        0.6.2
 */