package android.filterfw.core;

import android.os.ConditionVariable;
import android.util.Log;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class SyncRunner extends GraphRunner
{
    private static final String TAG = "SyncRunner";
    private GraphRunner.OnRunnerDoneListener mDoneListener = null;
    private final boolean mLogVerbose = Log.isLoggable("SyncRunner", 2);
    private Scheduler mScheduler = null;
    private StopWatchMap mTimer = null;
    private ConditionVariable mWakeCondition = new ConditionVariable();
    private ScheduledThreadPoolExecutor mWakeExecutor = new ScheduledThreadPoolExecutor(1);

    public SyncRunner(FilterContext paramFilterContext, FilterGraph paramFilterGraph, Class paramClass)
    {
        super(paramFilterContext);
        if (this.mLogVerbose)
            Log.v("SyncRunner", "Initializing SyncRunner");
        if (Scheduler.class.isAssignableFrom(paramClass))
            try
            {
                Class[] arrayOfClass = new Class[1];
                arrayOfClass[0] = FilterGraph.class;
                Constructor localConstructor = paramClass.getConstructor(arrayOfClass);
                Object[] arrayOfObject = new Object[1];
                arrayOfObject[0] = paramFilterGraph;
                this.mScheduler = ((Scheduler)localConstructor.newInstance(arrayOfObject));
                this.mFilterContext = paramFilterContext;
                this.mFilterContext.addGraph(paramFilterGraph);
                this.mTimer = new StopWatchMap();
                if (this.mLogVerbose)
                    Log.v("SyncRunner", "Setting up filters");
                paramFilterGraph.setupFilters();
                return;
            }
            catch (NoSuchMethodException localNoSuchMethodException)
            {
                throw new RuntimeException("Scheduler does not have constructor <init>(FilterGraph)!", localNoSuchMethodException);
            }
            catch (InstantiationException localInstantiationException)
            {
                throw new RuntimeException("Could not instantiate the Scheduler instance!", localInstantiationException);
            }
            catch (IllegalAccessException localIllegalAccessException)
            {
                throw new RuntimeException("Cannot access Scheduler constructor!", localIllegalAccessException);
            }
            catch (InvocationTargetException localInvocationTargetException)
            {
                throw new RuntimeException("Scheduler constructor threw an exception", localInvocationTargetException);
            }
            catch (Exception localException)
            {
                throw new RuntimeException("Could not instantiate Scheduler", localException);
            }
        throw new IllegalArgumentException("Class provided is not a Scheduler subclass!");
    }

    void assertReadyToStep()
    {
        if (this.mScheduler == null)
            throw new RuntimeException("Attempting to run schedule with no scheduler in place!");
        if (getGraph() == null)
            throw new RuntimeException("Calling step on scheduler with no graph in place!");
    }

    public void beginProcessing()
    {
        this.mScheduler.reset();
        getGraph().beginProcessing();
    }

    public void close()
    {
        if (this.mLogVerbose)
            Log.v("SyncRunner", "Closing graph.");
        getGraph().closeFilters(this.mFilterContext);
        this.mScheduler.reset();
    }

    protected int determinePostRunState()
    {
        int i = 4;
        Iterator localIterator = this.mScheduler.getGraph().getFilters().iterator();
        while (true)
            if (localIterator.hasNext())
            {
                Filter localFilter = (Filter)localIterator.next();
                if (localFilter.isOpen())
                    if (localFilter.getStatus() != i)
                        break;
            }
        for (i = 3; ; i = 2)
            return i;
    }

    /** @deprecated */
    public Exception getError()
    {
        return null;
    }

    public FilterGraph getGraph()
    {
        if (this.mScheduler != null);
        for (FilterGraph localFilterGraph = this.mScheduler.getGraph(); ; localFilterGraph = null)
            return localFilterGraph;
    }

    public boolean isRunning()
    {
        return false;
    }

    boolean performStep()
    {
        if (this.mLogVerbose)
            Log.v("SyncRunner", "Performing one step.");
        Filter localFilter = this.mScheduler.scheduleNextNode();
        if (localFilter != null)
        {
            this.mTimer.start(localFilter.getName());
            processFilterNode(localFilter);
            this.mTimer.stop(localFilter.getName());
        }
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    protected void processFilterNode(Filter paramFilter)
    {
        if (this.mLogVerbose)
            Log.v("SyncRunner", "Processing filter node");
        paramFilter.performProcess(this.mFilterContext);
        if (paramFilter.getStatus() == 6)
            throw new RuntimeException("There was an error executing " + paramFilter + "!");
        if (paramFilter.getStatus() == 4)
        {
            if (this.mLogVerbose)
                Log.v("SyncRunner", "Scheduling filter wakeup");
            scheduleFilterWake(paramFilter, paramFilter.getSleepDelay());
        }
    }

    public void run()
    {
        if (this.mLogVerbose)
            Log.v("SyncRunner", "Beginning run.");
        assertReadyToStep();
        beginProcessing();
        boolean bool1 = activateGlContext();
        for (boolean bool2 = true; bool2; bool2 = performStep());
        if (bool1)
            deactivateGlContext();
        if (this.mDoneListener != null)
        {
            if (this.mLogVerbose)
                Log.v("SyncRunner", "Calling completion listener.");
            this.mDoneListener.onRunnerDone(determinePostRunState());
        }
        if (this.mLogVerbose)
            Log.v("SyncRunner", "Run complete");
    }

    protected void scheduleFilterWake(final Filter paramFilter, int paramInt)
    {
        this.mWakeCondition.close();
        final ConditionVariable localConditionVariable = this.mWakeCondition;
        this.mWakeExecutor.schedule(new Runnable()
        {
            public void run()
            {
                paramFilter.unsetStatus(4);
                localConditionVariable.open();
            }
        }
        , paramInt, TimeUnit.MILLISECONDS);
    }

    public void setDoneCallback(GraphRunner.OnRunnerDoneListener paramOnRunnerDoneListener)
    {
        this.mDoneListener = paramOnRunnerDoneListener;
    }

    public int step()
    {
        assertReadyToStep();
        if (!getGraph().isReady())
            throw new RuntimeException("Trying to process graph that is not open!");
        if (performStep());
        for (int i = 1; ; i = determinePostRunState())
            return i;
    }

    public void stop()
    {
        throw new RuntimeException("SyncRunner does not support stopping a graph!");
    }

    protected void waitUntilWake()
    {
        this.mWakeCondition.block();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterfw.core.SyncRunner
 * JD-Core Version:        0.6.2
 */