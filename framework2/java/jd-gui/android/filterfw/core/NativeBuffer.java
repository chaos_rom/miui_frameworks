package android.filterfw.core;

public class NativeBuffer
{
    private Frame mAttachedFrame;
    private long mDataPointer = 0L;
    private boolean mOwnsData = false;
    private int mRefCount = 1;
    private int mSize = 0;

    static
    {
        System.loadLibrary("filterfw");
    }

    public NativeBuffer()
    {
    }

    public NativeBuffer(int paramInt)
    {
        allocate(paramInt * getElementSize());
        this.mOwnsData = true;
    }

    private native boolean allocate(int paramInt);

    private native boolean deallocate(boolean paramBoolean);

    private native boolean nativeCopyTo(NativeBuffer paramNativeBuffer);

    protected void assertReadable()
    {
        if ((this.mDataPointer == 0L) || (this.mSize == 0) || ((this.mAttachedFrame != null) && (!this.mAttachedFrame.hasNativeAllocation())))
            throw new NullPointerException("Attempting to read from null data frame!");
    }

    protected void assertWritable()
    {
        if (isReadOnly())
            throw new RuntimeException("Attempting to modify read-only native (structured) data!");
    }

    void attachToFrame(Frame paramFrame)
    {
        this.mAttachedFrame = paramFrame;
    }

    public int count()
    {
        if (this.mDataPointer != 0L);
        for (int i = this.mSize / getElementSize(); ; i = 0)
            return i;
    }

    public int getElementSize()
    {
        return 1;
    }

    public boolean isReadOnly()
    {
        if (this.mAttachedFrame != null);
        for (boolean bool = this.mAttachedFrame.isReadOnly(); ; bool = false)
            return bool;
    }

    public NativeBuffer mutableCopy()
    {
        NativeBuffer localNativeBuffer;
        try
        {
            localNativeBuffer = (NativeBuffer)getClass().newInstance();
            if ((this.mSize > 0) && (!nativeCopyTo(localNativeBuffer)))
                throw new RuntimeException("Failed to copy NativeBuffer to mutable instance!");
        }
        catch (Exception localException)
        {
            throw new RuntimeException("Unable to allocate a copy of " + getClass() + "! Make " + "sure the class has a default constructor!");
        }
        return localNativeBuffer;
    }

    public NativeBuffer release()
    {
        int i = 0;
        if (this.mAttachedFrame != null)
            if (this.mAttachedFrame.release() == null)
                i = 1;
        while (!this.mOwnsData)
            while (true)
            {
                if (i != 0)
                {
                    deallocate(this.mOwnsData);
                    this = null;
                }
                return this;
                i = 0;
            }
        this.mRefCount = (-1 + this.mRefCount);
        if (this.mRefCount == 0);
        for (i = 1; ; i = 0)
            break;
    }

    public NativeBuffer retain()
    {
        if (this.mAttachedFrame != null)
            this.mAttachedFrame.retain();
        while (true)
        {
            return this;
            if (this.mOwnsData)
                this.mRefCount = (1 + this.mRefCount);
        }
    }

    public int size()
    {
        return this.mSize;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterfw.core.NativeBuffer
 * JD-Core Version:        0.6.2
 */