package android.filterfw.core;

public abstract class GraphRunner
{
    public static final int RESULT_BLOCKED = 4;
    public static final int RESULT_ERROR = 6;
    public static final int RESULT_FINISHED = 2;
    public static final int RESULT_RUNNING = 1;
    public static final int RESULT_SLEEPING = 3;
    public static final int RESULT_STOPPED = 5;
    public static final int RESULT_UNKNOWN;
    protected FilterContext mFilterContext = null;

    public GraphRunner(FilterContext paramFilterContext)
    {
        this.mFilterContext = paramFilterContext;
    }

    protected boolean activateGlContext()
    {
        GLEnvironment localGLEnvironment = this.mFilterContext.getGLEnvironment();
        if ((localGLEnvironment != null) && (!localGLEnvironment.isActive()))
            localGLEnvironment.activate();
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public abstract void close();

    protected void deactivateGlContext()
    {
        GLEnvironment localGLEnvironment = this.mFilterContext.getGLEnvironment();
        if (localGLEnvironment != null)
            localGLEnvironment.deactivate();
    }

    public FilterContext getContext()
    {
        return this.mFilterContext;
    }

    public abstract Exception getError();

    public abstract FilterGraph getGraph();

    public abstract boolean isRunning();

    public abstract void run();

    public abstract void setDoneCallback(OnRunnerDoneListener paramOnRunnerDoneListener);

    public abstract void stop();

    public static abstract interface OnRunnerDoneListener
    {
        public abstract void onRunnerDone(int paramInt);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterfw.core.GraphRunner
 * JD-Core Version:        0.6.2
 */