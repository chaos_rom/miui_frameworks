package android.filterfw.core;

import java.util.Iterator;
import java.util.Random;
import java.util.Set;
import java.util.Vector;

public class RandomScheduler extends Scheduler
{
    private Random mRand = new Random();

    public RandomScheduler(FilterGraph paramFilterGraph)
    {
        super(paramFilterGraph);
    }

    public void reset()
    {
    }

    public Filter scheduleNextNode()
    {
        Vector localVector = new Vector();
        Iterator localIterator = getGraph().getFilters().iterator();
        while (localIterator.hasNext())
        {
            Filter localFilter2 = (Filter)localIterator.next();
            if (localFilter2.canProcess())
                localVector.add(localFilter2);
        }
        if (localVector.size() > 0);
        for (Filter localFilter1 = (Filter)localVector.elementAt(this.mRand.nextInt(localVector.size())); ; localFilter1 = null)
            return localFilter1;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterfw.core.RandomScheduler
 * JD-Core Version:        0.6.2
 */