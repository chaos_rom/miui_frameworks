package android.filterfw.io;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterFactory;
import android.filterfw.core.FilterGraph;
import android.filterfw.core.KeyValueMap;
import android.filterfw.core.ProtocolException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;
import java.util.regex.Pattern;

public class TextGraphReader extends GraphReader
{
    private KeyValueMap mBoundReferences;
    private ArrayList<Command> mCommands = new ArrayList();
    private Filter mCurrentFilter;
    private FilterGraph mCurrentGraph;
    private FilterFactory mFactory;
    private KeyValueMap mSettings;

    private void applySettings()
        throws GraphIOException
    {
        Iterator localIterator = this.mSettings.keySet().iterator();
        while (localIterator.hasNext())
        {
            String str = (String)localIterator.next();
            Object localObject = this.mSettings.get(str);
            if (str.equals("autoBranch"))
            {
                expectSettingClass(str, localObject, String.class);
                if (localObject.equals("synced"))
                    this.mCurrentGraph.setAutoBranchMode(1);
                else if (localObject.equals("unsynced"))
                    this.mCurrentGraph.setAutoBranchMode(2);
                else if (localObject.equals("off"))
                    this.mCurrentGraph.setAutoBranchMode(0);
                else
                    throw new GraphIOException("Unknown autobranch setting: " + localObject + "!");
            }
            else if (str.equals("discardUnconnectedOutputs"))
            {
                expectSettingClass(str, localObject, Boolean.class);
                this.mCurrentGraph.setDiscardUnconnectedOutputs(((Boolean)localObject).booleanValue());
            }
            else
            {
                throw new GraphIOException("Unknown @setting '" + str + "'!");
            }
        }
    }

    private void bindExternal(String paramString)
        throws GraphIOException
    {
        if (this.mReferences.containsKey(paramString))
        {
            Object localObject = this.mReferences.get(paramString);
            this.mBoundReferences.put(paramString, localObject);
            return;
        }
        throw new GraphIOException("Unknown external variable '" + paramString + "'! " + "You must add a reference to this external in the host program using " + "addReference(...)!");
    }

    private void checkReferences()
        throws GraphIOException
    {
        Iterator localIterator = this.mReferences.keySet().iterator();
        while (localIterator.hasNext())
        {
            String str = (String)localIterator.next();
            if (!this.mBoundReferences.containsKey(str))
                throw new GraphIOException("Host program specifies reference to '" + str + "', which is not " + "declared @external in graph file!");
        }
    }

    private void executeCommands()
        throws GraphIOException
    {
        Iterator localIterator = this.mCommands.iterator();
        while (localIterator.hasNext())
            ((Command)localIterator.next()).execute(this);
    }

    private void expectSettingClass(String paramString, Object paramObject, Class paramClass)
        throws GraphIOException
    {
        if (paramObject.getClass() != paramClass)
            throw new GraphIOException("Setting '" + paramString + "' must have a value of type " + paramClass.getSimpleName() + ", but found a value of type " + paramObject.getClass().getSimpleName() + "!");
    }

    private void parseString(String paramString)
        throws GraphIOException
    {
        Pattern localPattern1 = Pattern.compile("@[a-zA-Z]+");
        Pattern localPattern2 = Pattern.compile("\\}");
        Pattern localPattern3 = Pattern.compile("\\{");
        Pattern localPattern4 = Pattern.compile("(\\s+|//[^\\n]*\\n)+");
        Pattern localPattern5 = Pattern.compile("[a-zA-Z\\.]+");
        Pattern localPattern6 = Pattern.compile("[a-zA-Z\\./:]+");
        Pattern localPattern7 = Pattern.compile("\\[[a-zA-Z0-9\\-_]+\\]");
        Pattern localPattern8 = Pattern.compile("=>");
        Pattern localPattern9 = Pattern.compile(";");
        Pattern localPattern10 = Pattern.compile("[a-zA-Z0-9\\-_]+");
        int i = 0;
        PatternScanner localPatternScanner = new PatternScanner(paramString, localPattern4);
        String str1 = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        while (!localPatternScanner.atEnd())
            switch (i)
            {
            default:
                break;
            case 0:
                String str11 = localPatternScanner.eat(localPattern1, "<command>");
                if (str11.equals("@import"))
                    i = 1;
                else if (str11.equals("@library"))
                    i = 2;
                else if (str11.equals("@filter"))
                    i = 3;
                else if (str11.equals("@connect"))
                    i = 8;
                else if (str11.equals("@set"))
                    i = 13;
                else if (str11.equals("@external"))
                    i = 14;
                else if (str11.equals("@setting"))
                    i = 15;
                else
                    throw new GraphIOException("Unknown command '" + str11 + "'!");
                break;
            case 1:
                String str10 = localPatternScanner.eat(localPattern5, "<package-name>");
                this.mCommands.add(new ImportPackageCommand(str10));
                i = 16;
                break;
            case 2:
                String str9 = localPatternScanner.eat(localPattern6, "<library-name>");
                this.mCommands.add(new AddLibraryCommand(str9));
                i = 16;
                break;
            case 3:
                str1 = localPatternScanner.eat(localPattern10, "<class-name>");
                i = 4;
                break;
            case 4:
                String str8 = localPatternScanner.eat(localPattern10, "<filter-name>");
                this.mCommands.add(new AllocateFilterCommand(str1, str8));
                i = 5;
                break;
            case 5:
                localPatternScanner.eat(localPattern3, "{");
                i = 6;
                break;
            case 6:
                KeyValueMap localKeyValueMap3 = readKeyValueAssignments(localPatternScanner, localPattern2);
                this.mCommands.add(new InitFilterCommand(localKeyValueMap3));
                i = 7;
                break;
            case 7:
                localPatternScanner.eat(localPattern2, "}");
                i = 0;
                break;
            case 8:
                str2 = localPatternScanner.eat(localPattern10, "<source-filter-name>");
                i = 9;
                break;
            case 9:
                String str7 = localPatternScanner.eat(localPattern7, "[<source-port-name>]");
                str3 = str7.substring(1, -1 + str7.length());
                i = 10;
                break;
            case 10:
                localPatternScanner.eat(localPattern8, "=>");
                i = 11;
                break;
            case 11:
                str4 = localPatternScanner.eat(localPattern10, "<target-filter-name>");
                i = 12;
                break;
            case 12:
                String str5 = localPatternScanner.eat(localPattern7, "[<target-port-name>]");
                String str6 = str5.substring(1, -1 + str5.length());
                this.mCommands.add(new ConnectCommand(str2, str3, str4, str6));
                i = 16;
                break;
            case 13:
                KeyValueMap localKeyValueMap2 = readKeyValueAssignments(localPatternScanner, localPattern9);
                this.mBoundReferences.putAll(localKeyValueMap2);
                i = 16;
                break;
            case 14:
                bindExternal(localPatternScanner.eat(localPattern10, "<external-identifier>"));
                i = 16;
                break;
            case 15:
                KeyValueMap localKeyValueMap1 = readKeyValueAssignments(localPatternScanner, localPattern9);
                this.mSettings.putAll(localKeyValueMap1);
                i = 16;
                break;
            case 16:
                localPatternScanner.eat(localPattern9, ";");
                i = 0;
            }
        if ((i != 16) && (i != 0))
            throw new GraphIOException("Unexpected end of input!");
    }

    private KeyValueMap readKeyValueAssignments(PatternScanner paramPatternScanner, Pattern paramPattern)
        throws GraphIOException
    {
        Pattern localPattern1 = Pattern.compile("=");
        Pattern localPattern2 = Pattern.compile(";");
        Pattern localPattern3 = Pattern.compile("[a-zA-Z]+[a-zA-Z0-9]*");
        Pattern localPattern4 = Pattern.compile("'[^']*'|\\\"[^\\\"]*\\\"");
        Pattern localPattern5 = Pattern.compile("[0-9]+");
        Pattern localPattern6 = Pattern.compile("[0-9]*\\.[0-9]+f?");
        Pattern localPattern7 = Pattern.compile("\\$[a-zA-Z]+[a-zA-Z0-9]");
        Pattern localPattern8 = Pattern.compile("true|false");
        int i = 0;
        KeyValueMap localKeyValueMap = new KeyValueMap();
        String str1 = null;
        while ((!paramPatternScanner.atEnd()) && ((paramPattern == null) || (!paramPatternScanner.peek(paramPattern))))
            switch (i)
            {
            default:
                break;
            case 0:
                str1 = paramPatternScanner.eat(localPattern3, "<identifier>");
                i = 1;
                break;
            case 1:
                paramPatternScanner.eat(localPattern1, "=");
                i = 2;
                break;
            case 2:
                String str2 = paramPatternScanner.tryEat(localPattern4);
                if (str2 != null)
                    localKeyValueMap.put(str1, str2.substring(1, -1 + str2.length()));
                while (true)
                {
                    i = 3;
                    break;
                    String str3 = paramPatternScanner.tryEat(localPattern7);
                    if (str3 != null)
                    {
                        String str7 = str3.substring(1, str3.length());
                        if (this.mBoundReferences != null);
                        for (Object localObject = this.mBoundReferences.get(str7); localObject == null; localObject = null)
                            throw new GraphIOException("Unknown object reference to '" + str7 + "'!");
                        localKeyValueMap.put(str1, localObject);
                    }
                    else
                    {
                        String str4 = paramPatternScanner.tryEat(localPattern8);
                        if (str4 != null)
                        {
                            localKeyValueMap.put(str1, Boolean.valueOf(Boolean.parseBoolean(str4)));
                        }
                        else
                        {
                            String str5 = paramPatternScanner.tryEat(localPattern6);
                            if (str5 != null)
                            {
                                localKeyValueMap.put(str1, Float.valueOf(Float.parseFloat(str5)));
                            }
                            else
                            {
                                String str6 = paramPatternScanner.tryEat(localPattern5);
                                if (str6 == null)
                                    break label402;
                                localKeyValueMap.put(str1, Integer.valueOf(Integer.parseInt(str6)));
                            }
                        }
                    }
                }
                throw new GraphIOException(paramPatternScanner.unexpectedTokenMessage("<value>"));
            case 3:
                label402: paramPatternScanner.eat(localPattern2, ";");
                i = 0;
            }
        if ((i != 0) && (i != 3))
            throw new GraphIOException("Unexpected end of assignments on line " + paramPatternScanner.lineNo() + "!");
        return localKeyValueMap;
    }

    private void reset()
    {
        this.mCurrentGraph = null;
        this.mCurrentFilter = null;
        this.mCommands.clear();
        this.mBoundReferences = new KeyValueMap();
        this.mSettings = new KeyValueMap();
        this.mFactory = new FilterFactory();
    }

    public FilterGraph readGraphString(String paramString)
        throws GraphIOException
    {
        FilterGraph localFilterGraph = new FilterGraph();
        reset();
        this.mCurrentGraph = localFilterGraph;
        parseString(paramString);
        applySettings();
        executeCommands();
        reset();
        return localFilterGraph;
    }

    public KeyValueMap readKeyValueAssignments(String paramString)
        throws GraphIOException
    {
        return readKeyValueAssignments(new PatternScanner(paramString, Pattern.compile("\\s+")), null);
    }

    private class ConnectCommand
        implements TextGraphReader.Command
    {
        private String mSourceFilter;
        private String mSourcePort;
        private String mTargetFilter;
        private String mTargetName;

        public ConnectCommand(String paramString1, String paramString2, String paramString3, String arg5)
        {
            this.mSourceFilter = paramString1;
            this.mSourcePort = paramString2;
            this.mTargetFilter = paramString3;
            Object localObject;
            this.mTargetName = localObject;
        }

        public void execute(TextGraphReader paramTextGraphReader)
        {
            paramTextGraphReader.mCurrentGraph.connect(this.mSourceFilter, this.mSourcePort, this.mTargetFilter, this.mTargetName);
        }
    }

    private class InitFilterCommand
        implements TextGraphReader.Command
    {
        private KeyValueMap mParams;

        public InitFilterCommand(KeyValueMap arg2)
        {
            Object localObject;
            this.mParams = localObject;
        }

        public void execute(TextGraphReader paramTextGraphReader)
            throws GraphIOException
        {
            Filter localFilter = paramTextGraphReader.mCurrentFilter;
            try
            {
                localFilter.initWithValueMap(this.mParams);
                paramTextGraphReader.mCurrentGraph.addFilter(TextGraphReader.this.mCurrentFilter);
                return;
            }
            catch (ProtocolException localProtocolException)
            {
                throw new GraphIOException(localProtocolException.getMessage());
            }
        }
    }

    private class AllocateFilterCommand
        implements TextGraphReader.Command
    {
        private String mClassName;
        private String mFilterName;

        public AllocateFilterCommand(String paramString1, String arg3)
        {
            this.mClassName = paramString1;
            Object localObject;
            this.mFilterName = localObject;
        }

        public void execute(TextGraphReader paramTextGraphReader)
            throws GraphIOException
        {
            try
            {
                Filter localFilter = paramTextGraphReader.mFactory.createFilterByClassName(this.mClassName, this.mFilterName);
                TextGraphReader.access$102(paramTextGraphReader, localFilter);
                return;
            }
            catch (IllegalArgumentException localIllegalArgumentException)
            {
                throw new GraphIOException(localIllegalArgumentException.getMessage());
            }
        }
    }

    private class AddLibraryCommand
        implements TextGraphReader.Command
    {
        private String mLibraryName;

        public AddLibraryCommand(String arg2)
        {
            Object localObject;
            this.mLibraryName = localObject;
        }

        public void execute(TextGraphReader paramTextGraphReader)
        {
            FilterFactory.addFilterLibrary(this.mLibraryName);
        }
    }

    private class ImportPackageCommand
        implements TextGraphReader.Command
    {
        private String mPackageName;

        public ImportPackageCommand(String arg2)
        {
            Object localObject;
            this.mPackageName = localObject;
        }

        public void execute(TextGraphReader paramTextGraphReader)
            throws GraphIOException
        {
            try
            {
                paramTextGraphReader.mFactory.addPackage(this.mPackageName);
                return;
            }
            catch (IllegalArgumentException localIllegalArgumentException)
            {
                throw new GraphIOException(localIllegalArgumentException.getMessage());
            }
        }
    }

    private static abstract interface Command
    {
        public abstract void execute(TextGraphReader paramTextGraphReader)
            throws GraphIOException;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterfw.io.TextGraphReader
 * JD-Core Version:        0.6.2
 */