package android.filterfw.io;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatternScanner
{
    private Pattern mIgnorePattern;
    private String mInput;
    private int mLineNo = 0;
    private int mOffset = 0;
    private int mStartOfLine = 0;

    public PatternScanner(String paramString)
    {
        this.mInput = paramString;
    }

    public PatternScanner(String paramString, Pattern paramPattern)
    {
        this.mInput = paramString;
        this.mIgnorePattern = paramPattern;
        skip(this.mIgnorePattern);
    }

    public boolean atEnd()
    {
        if (this.mOffset >= this.mInput.length());
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public String eat(Pattern paramPattern, String paramString)
    {
        String str = tryEat(paramPattern);
        if (str == null)
            throw new RuntimeException(unexpectedTokenMessage(paramString));
        return str;
    }

    public int lineNo()
    {
        return this.mLineNo;
    }

    public boolean peek(Pattern paramPattern)
    {
        if (this.mIgnorePattern != null)
            skip(this.mIgnorePattern);
        Matcher localMatcher = paramPattern.matcher(this.mInput);
        localMatcher.region(this.mOffset, this.mInput.length());
        return localMatcher.lookingAt();
    }

    public void skip(Pattern paramPattern)
    {
        Matcher localMatcher = paramPattern.matcher(this.mInput);
        localMatcher.region(this.mOffset, this.mInput.length());
        if (localMatcher.lookingAt())
        {
            updateLineCount(this.mOffset, localMatcher.end());
            this.mOffset = localMatcher.end();
        }
    }

    public String tryEat(Pattern paramPattern)
    {
        if (this.mIgnorePattern != null)
            skip(this.mIgnorePattern);
        Matcher localMatcher = paramPattern.matcher(this.mInput);
        localMatcher.region(this.mOffset, this.mInput.length());
        String str = null;
        if (localMatcher.lookingAt())
        {
            updateLineCount(this.mOffset, localMatcher.end());
            this.mOffset = localMatcher.end();
            str = this.mInput.substring(localMatcher.start(), localMatcher.end());
        }
        if ((str != null) && (this.mIgnorePattern != null))
            skip(this.mIgnorePattern);
        return str;
    }

    public String unexpectedTokenMessage(String paramString)
    {
        String str = this.mInput.substring(this.mStartOfLine, this.mOffset);
        return "Unexpected token on line " + (1 + this.mLineNo) + " after '" + str + "' <- Expected " + paramString + "!";
    }

    public void updateLineCount(int paramInt1, int paramInt2)
    {
        for (int i = paramInt1; i < paramInt2; i++)
            if (this.mInput.charAt(i) == '\n')
            {
                this.mLineNo = (1 + this.mLineNo);
                this.mStartOfLine = (i + 1);
            }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterfw.io.PatternScanner
 * JD-Core Version:        0.6.2
 */