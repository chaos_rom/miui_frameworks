package android.filterfw.io;

import android.content.Context;
import android.content.res.Resources;
import android.filterfw.core.FilterGraph;
import android.filterfw.core.KeyValueMap;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;

public abstract class GraphReader
{
    protected KeyValueMap mReferences = new KeyValueMap();

    public void addReference(String paramString, Object paramObject)
    {
        this.mReferences.put(paramString, paramObject);
    }

    public void addReferencesByKeysAndValues(Object[] paramArrayOfObject)
    {
        this.mReferences.setKeyValues(paramArrayOfObject);
    }

    public void addReferencesByMap(KeyValueMap paramKeyValueMap)
    {
        this.mReferences.putAll(paramKeyValueMap);
    }

    public FilterGraph readGraphResource(Context paramContext, int paramInt)
        throws GraphIOException
    {
        InputStreamReader localInputStreamReader = new InputStreamReader(paramContext.getResources().openRawResource(paramInt));
        StringWriter localStringWriter = new StringWriter();
        char[] arrayOfChar = new char[1024];
        try
        {
            while (true)
            {
                int i = localInputStreamReader.read(arrayOfChar, 0, 1024);
                if (i <= 0)
                    break;
                localStringWriter.write(arrayOfChar, 0, i);
            }
        }
        catch (IOException localIOException)
        {
            throw new RuntimeException("Could not read specified resource file!");
        }
        return readGraphString(localStringWriter.toString());
    }

    public abstract FilterGraph readGraphString(String paramString)
        throws GraphIOException;

    public abstract KeyValueMap readKeyValueAssignments(String paramString)
        throws GraphIOException;
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterfw.io.GraphReader
 * JD-Core Version:        0.6.2
 */