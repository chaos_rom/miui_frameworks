package android.filterfw.format;

import android.filterfw.core.MutableFrameFormat;
import android.filterfw.core.NativeBuffer;

public class ObjectFormat
{
    private static int bytesPerSampleForClass(Class paramClass, int paramInt)
    {
        if (paramInt == 2)
            if (!NativeBuffer.class.isAssignableFrom(paramClass))
                throw new IllegalArgumentException("Native object-based formats must be of a NativeBuffer subclass! (Received class: " + paramClass + ").");
        while (true)
        {
            try
            {
                int j = ((NativeBuffer)paramClass.newInstance()).getElementSize();
                i = j;
                return i;
            }
            catch (Exception localException)
            {
                throw new RuntimeException("Could not determine the size of an element in a native object-based frame of type " + paramClass + "! Perhaps it is missing a " + "default constructor?");
            }
            int i = 1;
        }
    }

    public static MutableFrameFormat fromClass(Class paramClass, int paramInt)
    {
        return fromClass(paramClass, 0, paramInt);
    }

    public static MutableFrameFormat fromClass(Class paramClass, int paramInt1, int paramInt2)
    {
        MutableFrameFormat localMutableFrameFormat = new MutableFrameFormat(8, paramInt2);
        localMutableFrameFormat.setObjectClass(getBoxedClass(paramClass));
        if (paramInt1 != 0)
            localMutableFrameFormat.setDimensions(paramInt1);
        localMutableFrameFormat.setBytesPerSample(bytesPerSampleForClass(paramClass, paramInt2));
        return localMutableFrameFormat;
    }

    public static MutableFrameFormat fromObject(Object paramObject, int paramInt)
    {
        if (paramObject == null);
        for (MutableFrameFormat localMutableFrameFormat = new MutableFrameFormat(8, paramInt); ; localMutableFrameFormat = fromClass(paramObject.getClass(), 0, paramInt))
            return localMutableFrameFormat;
    }

    public static MutableFrameFormat fromObject(Object paramObject, int paramInt1, int paramInt2)
    {
        if (paramObject == null);
        for (MutableFrameFormat localMutableFrameFormat = new MutableFrameFormat(8, paramInt2); ; localMutableFrameFormat = fromClass(paramObject.getClass(), paramInt1, paramInt2))
            return localMutableFrameFormat;
    }

    private static Class getBoxedClass(Class paramClass)
    {
        if (paramClass.isPrimitive())
        {
            if (paramClass != Boolean.TYPE)
                break label19;
            paramClass = Boolean.class;
        }
        while (true)
        {
            return paramClass;
            label19: if (paramClass == Byte.TYPE)
            {
                paramClass = Byte.class;
            }
            else if (paramClass == Character.TYPE)
            {
                paramClass = Character.class;
            }
            else if (paramClass == Short.TYPE)
            {
                paramClass = Short.class;
            }
            else if (paramClass == Integer.TYPE)
            {
                paramClass = Integer.class;
            }
            else if (paramClass == Long.TYPE)
            {
                paramClass = Long.class;
            }
            else if (paramClass == Float.TYPE)
            {
                paramClass = Float.class;
            }
            else
            {
                if (paramClass != Double.TYPE)
                    break;
                paramClass = Double.class;
            }
        }
        throw new IllegalArgumentException("Unknown primitive type: " + paramClass.getSimpleName() + "!");
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         android.filterfw.format.ObjectFormat
 * JD-Core Version:        0.6.2
 */