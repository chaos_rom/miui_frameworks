package com.android.server.sip;

import android.net.sip.ISipSession;
import android.net.sip.ISipSession.Stub;
import android.net.sip.ISipSessionListener;
import android.net.sip.SipProfile;
import android.net.sip.SipProfile.Builder;
import android.net.sip.SipSession.State;
import android.net.sip.SipSessionAdapter;
import android.text.TextUtils;
import android.util.Log;
import gov.nist.javax.sip.clientauthutils.AccountManager;
import gov.nist.javax.sip.clientauthutils.UserCredentials;
import gov.nist.javax.sip.header.ProxyAuthenticate;
import gov.nist.javax.sip.header.StatusLine;
import gov.nist.javax.sip.header.WWWAuthenticate;
import gov.nist.javax.sip.header.extensions.ReferredByHeader;
import gov.nist.javax.sip.header.extensions.ReplacesHeader;
import gov.nist.javax.sip.message.SIPMessage;
import gov.nist.javax.sip.message.SIPResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.util.Collection;
import java.util.EventObject;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.sip.ClientTransaction;
import javax.sip.Dialog;
import javax.sip.DialogTerminatedEvent;
import javax.sip.IOExceptionEvent;
import javax.sip.ObjectInUseException;
import javax.sip.RequestEvent;
import javax.sip.ResponseEvent;
import javax.sip.ServerTransaction;
import javax.sip.SipException;
import javax.sip.SipListener;
import javax.sip.SipStack;
import javax.sip.TimeoutEvent;
import javax.sip.Transaction;
import javax.sip.TransactionTerminatedEvent;
import javax.sip.address.Address;
import javax.sip.address.SipURI;
import javax.sip.header.CSeqHeader;
import javax.sip.header.ContactHeader;
import javax.sip.header.ExpiresHeader;
import javax.sip.header.HeaderAddress;
import javax.sip.header.ReferToHeader;
import javax.sip.header.ViaHeader;
import javax.sip.message.Message;
import javax.sip.message.Request;
import javax.sip.message.Response;

class SipSessionGroup
    implements SipListener
{
    private static final String ANONYMOUS = "anonymous";
    private static final int CANCEL_CALL_TIMER = 3;
    private static final EventObject CONTINUE_CALL = new EventObject("Continue call");
    private static final boolean DEBUG = false;
    private static final boolean DEBUG_PING = false;
    private static final EventObject DEREGISTER = new EventObject("Deregister");
    private static final EventObject END_CALL = new EventObject("End call");
    private static final int END_CALL_TIMER = 3;
    private static final int EXPIRY_TIME = 3600;
    private static final EventObject HOLD_CALL = new EventObject("Hold call");
    private static final int INCALL_KEEPALIVE_INTERVAL = 10;
    private static final int KEEPALIVE_TIMEOUT = 5;
    private static final String TAG = "SipSession";
    private static final String THREAD_POOL_SIZE = "1";
    private static final long WAKE_LOCK_HOLDING_TIME = 500L;
    private SipSessionImpl mCallReceiverSession;
    private String mExternalIp;
    private int mExternalPort;
    private String mLocalIp;
    private final SipProfile mLocalProfile;
    private final String mPassword;
    private Map<String, SipSessionImpl> mSessionMap = new HashMap();
    private SipHelper mSipHelper;
    private SipStack mSipStack;
    private SipWakeLock mWakeLock;
    private SipWakeupTimer mWakeupTimer;

    public SipSessionGroup(SipProfile paramSipProfile, String paramString, SipWakeupTimer paramSipWakeupTimer, SipWakeLock paramSipWakeLock)
        throws SipException
    {
        this.mLocalProfile = paramSipProfile;
        this.mPassword = paramString;
        this.mWakeupTimer = paramSipWakeupTimer;
        this.mWakeLock = paramSipWakeLock;
        reset();
    }

    /** @deprecated */
    private void addSipSession(SipSessionImpl paramSipSessionImpl)
    {
        try
        {
            removeSipSession(paramSipSessionImpl);
            String str1 = paramSipSessionImpl.getCallId();
            this.mSessionMap.put(str1, paramSipSessionImpl);
            if (isLoggable(paramSipSessionImpl))
            {
                Log.d("SipSession", "+++    add a session with key:    '" + str1 + "'");
                Iterator localIterator = this.mSessionMap.keySet().iterator();
                if (localIterator.hasNext())
                {
                    String str2 = (String)localIterator.next();
                    Log.d("SipSession", "    " + str2 + ": " + this.mSessionMap.get(str2));
                }
            }
        }
        finally
        {
        }
    }

    private SipSessionImpl createNewSession(RequestEvent paramRequestEvent, ISipSessionListener paramISipSessionListener, ServerTransaction paramServerTransaction, int paramInt)
        throws SipException
    {
        SipSessionImpl localSipSessionImpl = new SipSessionImpl(paramISipSessionListener);
        localSipSessionImpl.mServerTransaction = paramServerTransaction;
        localSipSessionImpl.mState = paramInt;
        localSipSessionImpl.mDialog = localSipSessionImpl.mServerTransaction.getDialog();
        localSipSessionImpl.mInviteReceived = paramRequestEvent;
        localSipSessionImpl.mPeerProfile = createPeerProfile((HeaderAddress)paramRequestEvent.getRequest().getHeader("From"));
        localSipSessionImpl.mPeerSessionDescription = extractContent(paramRequestEvent.getRequest());
        return localSipSessionImpl;
    }

    private static SipProfile createPeerProfile(HeaderAddress paramHeaderAddress)
        throws SipException
    {
        try
        {
            Address localAddress = paramHeaderAddress.getAddress();
            SipURI localSipURI = (SipURI)localAddress.getURI();
            String str = localSipURI.getUser();
            if (str == null)
                str = "anonymous";
            int i = localSipURI.getPort();
            SipProfile.Builder localBuilder = new SipProfile.Builder(str, localSipURI.getHost()).setDisplayName(localAddress.getDisplayName());
            if (i > 0)
                localBuilder.setPort(i);
            SipProfile localSipProfile = localBuilder.build();
            return localSipProfile;
        }
        catch (IllegalArgumentException localIllegalArgumentException)
        {
            throw new SipException("createPeerProfile()", localIllegalArgumentException);
        }
        catch (ParseException localParseException)
        {
            throw new SipException("createPeerProfile()", localParseException);
        }
    }

    private static boolean expectResponse(int paramInt, String paramString, EventObject paramEventObject)
    {
        Response localResponse;
        if ((paramEventObject instanceof ResponseEvent))
        {
            localResponse = ((ResponseEvent)paramEventObject).getResponse();
            if (localResponse.getStatusCode() != paramInt);
        }
        for (boolean bool = paramString.equalsIgnoreCase(getCseqMethod(localResponse)); ; bool = false)
            return bool;
    }

    private static boolean expectResponse(String paramString, EventObject paramEventObject)
    {
        if ((paramEventObject instanceof ResponseEvent));
        for (boolean bool = paramString.equalsIgnoreCase(getCseqMethod(((ResponseEvent)paramEventObject).getResponse())); ; bool = false)
            return bool;
    }

    private String extractContent(Message paramMessage)
    {
        byte[] arrayOfByte = paramMessage.getRawContent();
        String str;
        if (arrayOfByte != null)
            try
            {
                if ((paramMessage instanceof SIPMessage))
                    str = ((SIPMessage)paramMessage).getMessageContent();
                else
                    str = new String(arrayOfByte, "UTF-8");
            }
            catch (UnsupportedEncodingException localUnsupportedEncodingException)
            {
            }
        else
            str = null;
        return str;
    }

    private void extractExternalAddress(ResponseEvent paramResponseEvent)
    {
        ViaHeader localViaHeader = (ViaHeader)paramResponseEvent.getResponse().getHeader("Via");
        if (localViaHeader == null);
        while (true)
        {
            return;
            int i = localViaHeader.getRPort();
            String str = localViaHeader.getReceived();
            if ((i > 0) && (str != null))
            {
                this.mExternalIp = str;
                this.mExternalPort = i;
            }
        }
    }

    private static String getCseqMethod(Message paramMessage)
    {
        return ((CSeqHeader)paramMessage.getHeader("CSeq")).getMethod();
    }

    private Throwable getRootCause(Throwable paramThrowable)
    {
        for (Throwable localThrowable = paramThrowable.getCause(); localThrowable != null; localThrowable = paramThrowable.getCause())
            paramThrowable = localThrowable;
        return paramThrowable;
    }

    /** @deprecated */
    private SipSessionImpl getSipSession(EventObject paramEventObject)
    {
        SipSessionImpl localSipSessionImpl;
        try
        {
            String str1 = SipHelper.getCallId(paramEventObject);
            localSipSessionImpl = (SipSessionImpl)this.mSessionMap.get(str1);
            if ((localSipSessionImpl != null) && (isLoggable(localSipSessionImpl)))
            {
                Log.d("SipSession", "session key from event: " + str1);
                Log.d("SipSession", "active sessions:");
                Iterator localIterator = this.mSessionMap.keySet().iterator();
                while (localIterator.hasNext())
                {
                    String str2 = (String)localIterator.next();
                    Log.d("SipSession", " ..." + str2 + ": " + this.mSessionMap.get(str2));
                }
            }
        }
        finally
        {
        }
        if (localSipSessionImpl != null);
        while (true)
        {
            return localSipSessionImpl;
            localSipSessionImpl = this.mCallReceiverSession;
        }
    }

    private String getStackName()
    {
        return "stack" + System.currentTimeMillis();
    }

    private static boolean isLoggable(SipSessionImpl paramSipSessionImpl)
    {
        if (paramSipSessionImpl != null)
            switch (paramSipSessionImpl.mState)
            {
            case 9:
            }
        return false;
    }

    // ERROR //
    private static boolean isLoggable(SipSessionImpl paramSipSessionImpl, EventObject paramEventObject)
    {
        // Byte code:
        //     0: aload_0
        //     1: invokestatic 221	com/android/server/sip/SipSessionGroup:isLoggable	(Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;)Z
        //     4: ifne +5 -> 9
        //     7: iconst_0
        //     8: ireturn
        //     9: aload_1
        //     10: ifnull -3 -> 7
        //     13: aload_1
        //     14: instanceof 374
        //     17: ifeq +30 -> 47
        //     20: ldc_w 461
        //     23: aload_1
        //     24: checkcast 374	javax/sip/ResponseEvent
        //     27: invokevirtual 378	javax/sip/ResponseEvent:getResponse	()Ljavax/sip/message/Response;
        //     30: ldc_w 421
        //     33: invokeinterface 411 2 0
        //     38: invokevirtual 465	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     41: ifeq -34 -> 7
        //     44: goto -37 -> 7
        //     47: aload_1
        //     48: instanceof 300
        //     51: ifeq -44 -> 7
        //     54: ldc_w 461
        //     57: aload_1
        //     58: invokestatic 208	com/android/server/sip/SipSessionGroup:isRequestEvent	(Ljava/lang/String;Ljava/util/EventObject;)Z
        //     61: ifeq -54 -> 7
        //     64: goto -57 -> 7
    }

    private static boolean isLoggable(EventObject paramEventObject)
    {
        return isLoggable(null, paramEventObject);
    }

    private static boolean isRequestEvent(String paramString, EventObject paramEventObject)
    {
        try
        {
            if ((paramEventObject instanceof RequestEvent))
            {
                boolean bool2 = paramString.equals(((RequestEvent)paramEventObject).getRequest().getMethod());
                bool1 = bool2;
                return bool1;
            }
        }
        catch (Throwable localThrowable)
        {
            while (true)
                boolean bool1 = false;
        }
    }

    private static String log(EventObject paramEventObject)
    {
        String str;
        if ((paramEventObject instanceof RequestEvent))
            str = ((RequestEvent)paramEventObject).getRequest().toString();
        while (true)
        {
            return str;
            if ((paramEventObject instanceof ResponseEvent))
                str = ((ResponseEvent)paramEventObject).getResponse().toString();
            else
                str = paramEventObject.toString();
        }
    }

    /** @deprecated */
    private void process(EventObject paramEventObject)
    {
        try
        {
            SipSessionImpl localSipSessionImpl = getSipSession(paramEventObject);
            try
            {
                boolean bool = isLoggable(localSipSessionImpl, paramEventObject);
                if ((localSipSessionImpl != null) && (localSipSessionImpl.process(paramEventObject)));
                for (int i = 1; ; i = 0)
                {
                    if ((bool) && (i != 0))
                        Log.d("SipSession", "new state after: " + SipSession.State.toString(localSipSessionImpl.mState));
                    return;
                }
            }
            catch (Throwable localThrowable)
            {
                while (true)
                {
                    Log.w("SipSession", "event process error: " + paramEventObject, getRootCause(localThrowable));
                    localSipSessionImpl.onError(localThrowable);
                }
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    private void removeSipSession(SipSessionImpl paramSipSessionImpl)
    {
        while (true)
        {
            try
            {
                SipSessionImpl localSipSessionImpl1 = this.mCallReceiverSession;
                if (paramSipSessionImpl == localSipSessionImpl1)
                    return;
                String str1 = paramSipSessionImpl.getCallId();
                SipSessionImpl localSipSessionImpl2 = (SipSessionImpl)this.mSessionMap.remove(str1);
                Iterator localIterator1;
                if ((localSipSessionImpl2 != null) && (localSipSessionImpl2 != paramSipSessionImpl))
                {
                    Log.w("SipSession", "session " + paramSipSessionImpl + " is not associated with key '" + str1 + "'");
                    this.mSessionMap.put(str1, localSipSessionImpl2);
                    Iterator localIterator2 = this.mSessionMap.entrySet().iterator();
                    if (localIterator2.hasNext())
                    {
                        Map.Entry localEntry = (Map.Entry)localIterator2.next();
                        if (localEntry.getValue() != localSipSessionImpl2)
                            continue;
                        str1 = (String)localEntry.getKey();
                        this.mSessionMap.remove(str1);
                        continue;
                    }
                }
            }
            finally
            {
            }
            if ((localSipSessionImpl2 != null) && (isLoggable(localSipSessionImpl2)))
            {
                Log.d("SipSession", "remove session " + paramSipSessionImpl + " @key '" + str1 + "'");
                localIterator1 = this.mSessionMap.keySet().iterator();
                while (localIterator1.hasNext())
                {
                    String str2 = (String)localIterator1.next();
                    Log.d("SipSession", "    " + str2 + ": " + this.mSessionMap.get(str2));
                }
            }
        }
    }

    /** @deprecated */
    public void close()
    {
        try
        {
            Log.d("SipSession", " close stack for " + this.mLocalProfile.getUriString());
            onConnectivityChanged();
            this.mSessionMap.clear();
            closeToNotReceiveCalls();
            if (this.mSipStack != null)
            {
                this.mSipStack.stop();
                this.mSipStack = null;
                this.mSipHelper = null;
            }
            resetExternalAddress();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void closeToNotReceiveCalls()
    {
        try
        {
            this.mCallReceiverSession = null;
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    boolean containsSession(String paramString)
    {
        try
        {
            boolean bool = this.mSessionMap.containsKey(paramString);
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public ISipSession createSession(ISipSessionListener paramISipSessionListener)
    {
        if (isClosed());
        for (Object localObject = null; ; localObject = new SipSessionImpl(paramISipSessionListener))
            return localObject;
    }

    public SipProfile getLocalProfile()
    {
        return this.mLocalProfile;
    }

    public String getLocalProfileUri()
    {
        return this.mLocalProfile.getUriString();
    }

    /** @deprecated */
    public boolean isClosed()
    {
        try
        {
            SipStack localSipStack = this.mSipStack;
            if (localSipStack == null)
            {
                bool = true;
                return bool;
            }
            boolean bool = false;
        }
        finally
        {
        }
    }

    /** @deprecated */
    void onConnectivityChanged()
    {
        try
        {
            SipSessionImpl[] arrayOfSipSessionImpl = (SipSessionImpl[])this.mSessionMap.values().toArray(new SipSessionImpl[this.mSessionMap.size()]);
            int i = arrayOfSipSessionImpl.length;
            for (int j = 0; j < i; j++)
                arrayOfSipSessionImpl[j].onError(-10, "data connection lost");
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void openToReceiveCalls(ISipSessionListener paramISipSessionListener)
    {
        try
        {
            if (this.mCallReceiverSession == null)
                this.mCallReceiverSession = new SipSessionCallReceiverImpl(paramISipSessionListener);
            while (true)
            {
                return;
                this.mCallReceiverSession.setListener(paramISipSessionListener);
            }
        }
        finally
        {
        }
    }

    public void processDialogTerminated(DialogTerminatedEvent paramDialogTerminatedEvent)
    {
        process(paramDialogTerminatedEvent);
    }

    public void processIOException(IOExceptionEvent paramIOExceptionEvent)
    {
        process(paramIOExceptionEvent);
    }

    public void processRequest(RequestEvent paramRequestEvent)
    {
        if (isRequestEvent("INVITE", paramRequestEvent))
            this.mWakeLock.acquire(500L);
        process(paramRequestEvent);
    }

    public void processResponse(ResponseEvent paramResponseEvent)
    {
        process(paramResponseEvent);
    }

    public void processTimeout(TimeoutEvent paramTimeoutEvent)
    {
        process(paramTimeoutEvent);
    }

    public void processTransactionTerminated(TransactionTerminatedEvent paramTransactionTerminatedEvent)
    {
        process(paramTransactionTerminatedEvent);
    }

    /** @deprecated */
    // ERROR //
    void reset()
        throws SipException
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: new 605	java/util/Properties
        //     5: dup
        //     6: invokespecial 606	java/util/Properties:<init>	()V
        //     9: astore_1
        //     10: aload_0
        //     11: getfield 112	com/android/server/sip/SipSessionGroup:mLocalProfile	Landroid/net/sip/SipProfile;
        //     14: invokevirtual 609	android/net/sip/SipProfile:getProtocol	()Ljava/lang/String;
        //     17: astore_3
        //     18: aload_0
        //     19: getfield 112	com/android/server/sip/SipSessionGroup:mLocalProfile	Landroid/net/sip/SipProfile;
        //     22: invokevirtual 610	android/net/sip/SipProfile:getPort	()I
        //     25: istore 4
        //     27: aload_0
        //     28: getfield 112	com/android/server/sip/SipSessionGroup:mLocalProfile	Landroid/net/sip/SipProfile;
        //     31: invokevirtual 613	android/net/sip/SipProfile:getProxyAddress	()Ljava/lang/String;
        //     34: astore 5
        //     36: aload 5
        //     38: invokestatic 619	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
        //     41: ifne +175 -> 216
        //     44: aload_1
        //     45: ldc_w 621
        //     48: new 223	java/lang/StringBuilder
        //     51: dup
        //     52: invokespecial 224	java/lang/StringBuilder:<init>	()V
        //     55: aload 5
        //     57: invokevirtual 230	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     60: bipush 58
        //     62: invokevirtual 624	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
        //     65: iload 4
        //     67: invokevirtual 627	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     70: bipush 47
        //     72: invokevirtual 624	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
        //     75: aload_3
        //     76: invokevirtual 230	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     79: invokevirtual 235	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     82: invokevirtual 631	java/util/Properties:setProperty	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
        //     85: pop
        //     86: aload 5
        //     88: ldc_w 633
        //     91: invokevirtual 636	java/lang/String:startsWith	(Ljava/lang/String;)Z
        //     94: ifeq +34 -> 128
        //     97: aload 5
        //     99: ldc_w 638
        //     102: invokevirtual 641	java/lang/String:endsWith	(Ljava/lang/String;)Z
        //     105: ifeq +23 -> 128
        //     108: aload 5
        //     110: iconst_1
        //     111: bipush 255
        //     113: aload 5
        //     115: invokevirtual 644	java/lang/String:length	()I
        //     118: iadd
        //     119: invokevirtual 648	java/lang/String:substring	(II)Ljava/lang/String;
        //     122: astore 20
        //     124: aload 20
        //     126: astore 5
        //     128: aconst_null
        //     129: astore 7
        //     131: aload 5
        //     133: invokestatic 654	java/net/InetAddress:getAllByName	(Ljava/lang/String;)[Ljava/net/InetAddress;
        //     136: astore 15
        //     138: aload 15
        //     140: arraylength
        //     141: istore 16
        //     143: iconst_0
        //     144: istore 17
        //     146: iload 17
        //     148: iload 16
        //     150: if_icmpge +58 -> 208
        //     153: aload 15
        //     155: iload 17
        //     157: aaload
        //     158: astore 18
        //     160: new 656	java/net/DatagramSocket
        //     163: dup
        //     164: invokespecial 657	java/net/DatagramSocket:<init>	()V
        //     167: astore 19
        //     169: aload 19
        //     171: aload 18
        //     173: iload 4
        //     175: invokevirtual 661	java/net/DatagramSocket:connect	(Ljava/net/InetAddress;I)V
        //     178: aload 19
        //     180: invokevirtual 664	java/net/DatagramSocket:isConnected	()Z
        //     183: ifeq +49 -> 232
        //     186: aload 19
        //     188: invokevirtual 668	java/net/DatagramSocket:getLocalAddress	()Ljava/net/InetAddress;
        //     191: invokevirtual 671	java/net/InetAddress:getHostAddress	()Ljava/lang/String;
        //     194: astore 7
        //     196: aload 19
        //     198: invokevirtual 674	java/net/DatagramSocket:getLocalPort	()I
        //     201: istore 4
        //     203: aload 19
        //     205: invokevirtual 676	java/net/DatagramSocket:close	()V
        //     208: aload 7
        //     210: ifnonnull +33 -> 243
        //     213: aload_0
        //     214: monitorexit
        //     215: return
        //     216: aload_0
        //     217: getfield 112	com/android/server/sip/SipSessionGroup:mLocalProfile	Landroid/net/sip/SipProfile;
        //     220: invokevirtual 679	android/net/sip/SipProfile:getSipDomain	()Ljava/lang/String;
        //     223: astore 6
        //     225: aload 6
        //     227: astore 5
        //     229: goto -143 -> 86
        //     232: aload 19
        //     234: invokevirtual 676	java/net/DatagramSocket:close	()V
        //     237: iinc 17 1
        //     240: goto -94 -> 146
        //     243: aload_0
        //     244: invokevirtual 680	com/android/server/sip/SipSessionGroup:close	()V
        //     247: aload_0
        //     248: aload 7
        //     250: putfield 131	com/android/server/sip/SipSessionGroup:mLocalIp	Ljava/lang/String;
        //     253: aload_1
        //     254: ldc_w 682
        //     257: aload_0
        //     258: invokespecial 684	com/android/server/sip/SipSessionGroup:getStackName	()Ljava/lang/String;
        //     261: invokevirtual 631	java/util/Properties:setProperty	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
        //     264: pop
        //     265: aload_1
        //     266: ldc_w 686
        //     269: ldc 53
        //     271: invokevirtual 631	java/util/Properties:setProperty	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
        //     274: pop
        //     275: aload_0
        //     276: invokestatic 692	javax/sip/SipFactory:getInstance	()Ljavax/sip/SipFactory;
        //     279: aload_1
        //     280: invokevirtual 696	javax/sip/SipFactory:createSipStack	(Ljava/util/Properties;)Ljavax/sip/SipStack;
        //     283: putfield 534	com/android/server/sip/SipSessionGroup:mSipStack	Ljavax/sip/SipStack;
        //     286: aload_0
        //     287: getfield 534	com/android/server/sip/SipSessionGroup:mSipStack	Ljavax/sip/SipStack;
        //     290: aload_0
        //     291: getfield 534	com/android/server/sip/SipSessionGroup:mSipStack	Ljavax/sip/SipStack;
        //     294: aload 7
        //     296: iload 4
        //     298: aload_3
        //     299: invokeinterface 700 4 0
        //     304: invokeinterface 704 2 0
        //     309: astore 13
        //     311: aload 13
        //     313: aload_0
        //     314: invokeinterface 710 2 0
        //     319: aload_0
        //     320: new 437	com/android/server/sip/SipHelper
        //     323: dup
        //     324: aload_0
        //     325: getfield 534	com/android/server/sip/SipSessionGroup:mSipStack	Ljavax/sip/SipStack;
        //     328: aload 13
        //     330: invokespecial 713	com/android/server/sip/SipHelper:<init>	(Ljavax/sip/SipStack;Ljavax/sip/SipProvider;)V
        //     333: putfield 185	com/android/server/sip/SipSessionGroup:mSipHelper	Lcom/android/server/sip/SipHelper;
        //     336: ldc 50
        //     338: new 223	java/lang/StringBuilder
        //     341: dup
        //     342: invokespecial 224	java/lang/StringBuilder:<init>	()V
        //     345: ldc_w 715
        //     348: invokevirtual 230	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     351: aload_0
        //     352: getfield 112	com/android/server/sip/SipSessionGroup:mLocalProfile	Landroid/net/sip/SipProfile;
        //     355: invokevirtual 523	android/net/sip/SipProfile:getUriString	()Ljava/lang/String;
        //     358: invokevirtual 230	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     361: invokevirtual 235	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     364: invokestatic 241	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
        //     367: pop
        //     368: aload_0
        //     369: getfield 534	com/android/server/sip/SipSessionGroup:mSipStack	Ljavax/sip/SipStack;
        //     372: invokeinterface 718 1 0
        //     377: goto -164 -> 213
        //     380: astore_2
        //     381: aload_0
        //     382: monitorexit
        //     383: aload_2
        //     384: athrow
        //     385: astore 12
        //     387: aload 12
        //     389: athrow
        //     390: astore 11
        //     392: new 103	javax/sip/SipException
        //     395: dup
        //     396: ldc_w 720
        //     399: aload 11
        //     401: invokespecial 371	javax/sip/SipException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
        //     404: athrow
        //     405: astore 8
        //     407: goto -199 -> 208
        //
        // Exception table:
        //     from	to	target	type
        //     2	124	380	finally
        //     131	208	380	finally
        //     216	225	380	finally
        //     232	237	380	finally
        //     243	286	380	finally
        //     286	336	380	finally
        //     336	377	380	finally
        //     387	405	380	finally
        //     286	336	385	javax/sip/SipException
        //     286	336	390	java/lang/Exception
        //     131	208	405	java/lang/Exception
        //     232	237	405	java/lang/Exception
    }

    /** @deprecated */
    void resetExternalAddress()
    {
        try
        {
            this.mExternalIp = null;
            this.mExternalPort = 0;
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    void setWakeupTimer(SipWakeupTimer paramSipWakeupTimer)
    {
        this.mWakeupTimer = paramSipWakeupTimer;
    }

    static class KeepAliveProcessCallbackProxy
        implements SipSessionGroup.KeepAliveProcessCallback
    {
        private SipSessionGroup.KeepAliveProcessCallback mCallback;

        KeepAliveProcessCallbackProxy(SipSessionGroup.KeepAliveProcessCallback paramKeepAliveProcessCallback)
        {
            this.mCallback = paramKeepAliveProcessCallback;
        }

        private void proxy(Runnable paramRunnable)
        {
            new Thread(paramRunnable, "SIP-KeepAliveProcessCallbackThread").start();
        }

        public void onError(final int paramInt, final String paramString)
        {
            if (this.mCallback == null);
            while (true)
            {
                return;
                proxy(new Runnable()
                {
                    public void run()
                    {
                        try
                        {
                            SipSessionGroup.KeepAliveProcessCallbackProxy.this.mCallback.onError(paramInt, paramString);
                            return;
                        }
                        catch (Throwable localThrowable)
                        {
                            while (true)
                                Log.w("SipSession", "onError", localThrowable);
                        }
                    }
                });
            }
        }

        public void onResponse(final boolean paramBoolean)
        {
            if (this.mCallback == null);
            while (true)
            {
                return;
                proxy(new Runnable()
                {
                    public void run()
                    {
                        try
                        {
                            SipSessionGroup.KeepAliveProcessCallbackProxy.this.mCallback.onResponse(paramBoolean);
                            return;
                        }
                        catch (Throwable localThrowable)
                        {
                            while (true)
                                Log.w("SipSession", "onResponse", localThrowable);
                        }
                    }
                });
            }
        }
    }

    private class MakeCallCommand extends EventObject
    {
        private String mSessionDescription;
        private int mTimeout;

        public MakeCallCommand(SipProfile paramString, String arg3)
        {
            this(paramString, str, -1);
        }

        public MakeCallCommand(SipProfile paramString, String paramInt, int arg4)
        {
            super();
            this.mSessionDescription = paramInt;
            int i;
            this.mTimeout = i;
        }

        public SipProfile getPeerProfile()
        {
            return (SipProfile)getSource();
        }

        public String getSessionDescription()
        {
            return this.mSessionDescription;
        }

        public int getTimeout()
        {
            return this.mTimeout;
        }
    }

    private class RegisterCommand extends EventObject
    {
        private int mDuration;

        public RegisterCommand(int arg2)
        {
            super();
            int i;
            this.mDuration = i;
        }

        public int getDuration()
        {
            return this.mDuration;
        }
    }

    class SipSessionImpl extends ISipSession.Stub
    {
        int mAuthenticationRetryCount;
        ClientTransaction mClientTransaction;
        Dialog mDialog;
        boolean mInCall;
        RequestEvent mInviteReceived;
        private KeepAliveProcess mKeepAliveProcess;
        private SipSessionImpl mKeepAliveSession;
        SipProfile mPeerProfile;
        String mPeerSessionDescription;
        SipSessionListenerProxy mProxy = new SipSessionListenerProxy();
        SipSessionImpl mReferSession;
        ReferredByHeader mReferredBy;
        String mReplaces;
        ServerTransaction mServerTransaction;
        SessionTimer mSessionTimer;
        int mState = 0;

        public SipSessionImpl(ISipSessionListener arg2)
        {
            ISipSessionListener localISipSessionListener;
            setListener(localISipSessionListener);
        }

        private void cancelSessionTimer()
        {
            if (this.mSessionTimer != null)
            {
                this.mSessionTimer.cancel();
                this.mSessionTimer = null;
            }
        }

        private String createErrorMessage(Response paramResponse)
        {
            Object[] arrayOfObject = new Object[2];
            arrayOfObject[0] = paramResponse.getReasonPhrase();
            arrayOfObject[1] = Integer.valueOf(paramResponse.getStatusCode());
            return String.format("%s (%d)", arrayOfObject);
        }

        private boolean crossDomainAuthenticationRequired(Response paramResponse)
        {
            String str = getRealmFromResponse(paramResponse);
            if (str == null)
                str = "";
            if (!SipSessionGroup.this.mLocalProfile.getSipDomain().trim().equals(str.trim()));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        private void doCommandAsync(final EventObject paramEventObject)
        {
            new Thread(new Runnable()
            {
                public void run()
                {
                    try
                    {
                        SipSessionGroup.SipSessionImpl.this.processCommand(paramEventObject);
                        return;
                    }
                    catch (Throwable localThrowable)
                    {
                        while (true)
                        {
                            Log.w("SipSession", "command error: " + paramEventObject + ": " + SipSessionGroup.this.mLocalProfile.getUriString(), SipSessionGroup.this.getRootCause(localThrowable));
                            SipSessionGroup.SipSessionImpl.this.onError(localThrowable);
                        }
                    }
                }
            }
            , "SipSessionAsyncCmdThread").start();
        }

        private void enableKeepAlive()
        {
            if (this.mKeepAliveSession != null)
                this.mKeepAliveSession.stopKeepAliveProcess();
            try
            {
                while (true)
                {
                    this.mKeepAliveSession.startKeepAliveProcess(10, this.mPeerProfile, null);
                    return;
                    this.mKeepAliveSession = duplicate();
                }
            }
            catch (SipException localSipException)
            {
                while (true)
                {
                    Log.w("SipSession", "keepalive cannot be enabled; ignored", localSipException);
                    this.mKeepAliveSession.stopKeepAliveProcess();
                }
            }
        }

        private void endCallNormally()
        {
            reset();
            this.mProxy.onCallEnded(this);
        }

        private void endCallOnBusy()
        {
            reset();
            this.mProxy.onCallBusy(this);
        }

        private void endCallOnError(int paramInt, String paramString)
        {
            reset();
            this.mProxy.onError(this, paramInt, paramString);
        }

        private boolean endingCall(EventObject paramEventObject)
            throws SipException
        {
            boolean bool = true;
            ResponseEvent localResponseEvent;
            if (SipSessionGroup.expectResponse("BYE", paramEventObject))
            {
                localResponseEvent = (ResponseEvent)paramEventObject;
                switch (localResponseEvent.getResponse().getStatusCode())
                {
                default:
                    cancelSessionTimer();
                    reset();
                case 401:
                case 407:
                }
            }
            while (true)
            {
                return bool;
                if (!handleAuthentication(localResponseEvent))
                    break;
                continue;
                bool = false;
            }
        }

        private void establishCall(boolean paramBoolean)
        {
            this.mState = 8;
            cancelSessionTimer();
            if ((!this.mInCall) && (paramBoolean))
                enableKeepAlive();
            this.mInCall = true;
            this.mProxy.onCallEstablished(this, this.mPeerSessionDescription);
        }

        private AccountManager getAccountManager()
        {
            return new AccountManager()
            {
                public UserCredentials getCredentials(ClientTransaction paramAnonymousClientTransaction, String paramAnonymousString)
                {
                    return new UserCredentials()
                    {
                        public String getPassword()
                        {
                            return SipSessionGroup.this.mPassword;
                        }

                        public String getSipDomain()
                        {
                            return SipSessionGroup.this.mLocalProfile.getSipDomain();
                        }

                        public String getUserName()
                        {
                            String str = SipSessionGroup.this.mLocalProfile.getAuthUserName();
                            if (!TextUtils.isEmpty(str));
                            while (true)
                            {
                                return str;
                                str = SipSessionGroup.this.mLocalProfile.getUserName();
                            }
                        }
                    };
                }
            };
        }

        private int getErrorCode(int paramInt)
        {
            int i;
            switch (paramInt)
            {
            default:
                if (paramInt < 500)
                    i = -4;
                break;
            case 403:
            case 404:
            case 406:
            case 410:
            case 480:
            case 488:
            case 414:
            case 484:
            case 485:
            case 408:
            }
            while (true)
            {
                return i;
                i = -7;
                continue;
                i = -6;
                continue;
                i = -5;
                continue;
                i = -2;
            }
        }

        private int getErrorCode(Throwable paramThrowable)
        {
            paramThrowable.getMessage();
            int i;
            if ((paramThrowable instanceof UnknownHostException))
                i = -12;
            while (true)
            {
                return i;
                if ((paramThrowable instanceof IOException))
                    i = -1;
                else
                    i = -4;
            }
        }

        private int getExpiryTime(Response paramResponse)
        {
            int i = -1;
            ContactHeader localContactHeader = (ContactHeader)paramResponse.getHeader("Contact");
            if (localContactHeader != null)
                i = localContactHeader.getExpires();
            ExpiresHeader localExpiresHeader1 = (ExpiresHeader)paramResponse.getHeader("Expires");
            if ((localExpiresHeader1 != null) && ((i < 0) || (i > localExpiresHeader1.getExpires())))
                i = localExpiresHeader1.getExpires();
            if (i <= 0)
                i = 3600;
            ExpiresHeader localExpiresHeader2 = (ExpiresHeader)paramResponse.getHeader("Min-Expires");
            if ((localExpiresHeader2 != null) && (i < localExpiresHeader2.getExpires()))
                i = localExpiresHeader2.getExpires();
            return i;
        }

        private String getNonceFromResponse(Response paramResponse)
        {
            WWWAuthenticate localWWWAuthenticate = (WWWAuthenticate)paramResponse.getHeader("WWW-Authenticate");
            String str;
            if (localWWWAuthenticate != null)
                str = localWWWAuthenticate.getNonce();
            while (true)
            {
                return str;
                ProxyAuthenticate localProxyAuthenticate = (ProxyAuthenticate)paramResponse.getHeader("Proxy-Authenticate");
                if (localProxyAuthenticate == null)
                    str = null;
                else
                    str = localProxyAuthenticate.getNonce();
            }
        }

        private String getRealmFromResponse(Response paramResponse)
        {
            WWWAuthenticate localWWWAuthenticate = (WWWAuthenticate)paramResponse.getHeader("WWW-Authenticate");
            String str;
            if (localWWWAuthenticate != null)
                str = localWWWAuthenticate.getRealm();
            while (true)
            {
                return str;
                ProxyAuthenticate localProxyAuthenticate = (ProxyAuthenticate)paramResponse.getHeader("Proxy-Authenticate");
                if (localProxyAuthenticate == null)
                    str = null;
                else
                    str = localProxyAuthenticate.getRealm();
            }
        }

        private String getResponseString(int paramInt)
        {
            StatusLine localStatusLine = new StatusLine();
            localStatusLine.setStatusCode(paramInt);
            localStatusLine.setReasonPhrase(SIPResponse.getReasonPhrase(paramInt));
            return localStatusLine.encode();
        }

        private Transaction getTransaction()
        {
            Object localObject;
            if (this.mClientTransaction != null)
                localObject = this.mClientTransaction;
            while (true)
            {
                return localObject;
                if (this.mServerTransaction != null)
                    localObject = this.mServerTransaction;
                else
                    localObject = null;
            }
        }

        private boolean handleAuthentication(ResponseEvent paramResponseEvent)
            throws SipException
        {
            boolean bool = false;
            Response localResponse = paramResponseEvent.getResponse();
            if (getNonceFromResponse(localResponse) == null)
                onError(-2, "server does not provide challenge");
            while (true)
            {
                return bool;
                if (this.mAuthenticationRetryCount < 2)
                {
                    this.mClientTransaction = SipSessionGroup.this.mSipHelper.handleChallenge(paramResponseEvent, getAccountManager());
                    this.mDialog = this.mClientTransaction.getDialog();
                    this.mAuthenticationRetryCount = (1 + this.mAuthenticationRetryCount);
                    if (SipSessionGroup.isLoggable(this, paramResponseEvent))
                        Log.d("SipSession", "     authentication retry count=" + this.mAuthenticationRetryCount);
                    bool = true;
                }
                else if (crossDomainAuthenticationRequired(localResponse))
                {
                    onError(-11, getRealmFromResponse(localResponse));
                }
                else
                {
                    onError(-8, "incorrect username or password");
                }
            }
        }

        private boolean inCall(EventObject paramEventObject)
            throws SipException
        {
            boolean bool;
            if (SipSessionGroup.END_CALL == paramEventObject)
            {
                this.mState = 10;
                SipSessionGroup.this.mSipHelper.sendBye(this.mDialog);
                this.mProxy.onCallEnded(this);
                startSessionTimer(3);
                bool = true;
            }
            while (true)
            {
                return bool;
                if (SipSessionGroup.isRequestEvent("INVITE", paramEventObject))
                {
                    this.mState = 3;
                    RequestEvent localRequestEvent = (RequestEvent)paramEventObject;
                    this.mInviteReceived = localRequestEvent;
                    this.mPeerSessionDescription = SipSessionGroup.this.extractContent(localRequestEvent.getRequest());
                    this.mServerTransaction = null;
                    this.mProxy.onRinging(this, this.mPeerProfile, this.mPeerSessionDescription);
                    bool = true;
                }
                else if (SipSessionGroup.isRequestEvent("BYE", paramEventObject))
                {
                    SipSessionGroup.this.mSipHelper.sendResponse((RequestEvent)paramEventObject, 200);
                    endCallNormally();
                    bool = true;
                }
                else if (SipSessionGroup.isRequestEvent("REFER", paramEventObject))
                {
                    bool = processReferRequest((RequestEvent)paramEventObject);
                }
                else if ((paramEventObject instanceof SipSessionGroup.MakeCallCommand))
                {
                    this.mState = 5;
                    this.mClientTransaction = SipSessionGroup.this.mSipHelper.sendReinvite(this.mDialog, ((SipSessionGroup.MakeCallCommand)paramEventObject).getSessionDescription());
                    startSessionTimer(((SipSessionGroup.MakeCallCommand)paramEventObject).getTimeout());
                    bool = true;
                }
                else if (((paramEventObject instanceof ResponseEvent)) && (SipSessionGroup.expectResponse("NOTIFY", paramEventObject)))
                {
                    bool = true;
                }
                else
                {
                    bool = false;
                }
            }
        }

        private boolean incomingCall(EventObject paramEventObject)
            throws SipException
        {
            boolean bool;
            if ((paramEventObject instanceof SipSessionGroup.MakeCallCommand))
            {
                this.mState = 4;
                this.mServerTransaction = SipSessionGroup.this.mSipHelper.sendInviteOk(this.mInviteReceived, SipSessionGroup.this.mLocalProfile, ((SipSessionGroup.MakeCallCommand)paramEventObject).getSessionDescription(), this.mServerTransaction, SipSessionGroup.this.mExternalIp, SipSessionGroup.this.mExternalPort);
                startSessionTimer(((SipSessionGroup.MakeCallCommand)paramEventObject).getTimeout());
                bool = true;
            }
            while (true)
            {
                return bool;
                if (SipSessionGroup.END_CALL == paramEventObject)
                {
                    SipSessionGroup.this.mSipHelper.sendInviteBusyHere(this.mInviteReceived, this.mServerTransaction);
                    endCallNormally();
                    bool = true;
                }
                else if (SipSessionGroup.isRequestEvent("CANCEL", paramEventObject))
                {
                    RequestEvent localRequestEvent = (RequestEvent)paramEventObject;
                    SipSessionGroup.this.mSipHelper.sendResponse(localRequestEvent, 200);
                    SipSessionGroup.this.mSipHelper.sendInviteRequestTerminated(this.mInviteReceived.getRequest(), this.mServerTransaction);
                    endCallNormally();
                    bool = true;
                }
                else
                {
                    bool = false;
                }
            }
        }

        private boolean incomingCallToInCall(EventObject paramEventObject)
            throws SipException
        {
            boolean bool = true;
            if (SipSessionGroup.isRequestEvent("ACK", paramEventObject))
            {
                String str = SipSessionGroup.this.extractContent(((RequestEvent)paramEventObject).getRequest());
                if (str != null)
                    this.mPeerSessionDescription = str;
                if (this.mPeerSessionDescription == null)
                    onError(-4, "peer sdp is empty");
            }
            while (true)
            {
                return bool;
                establishCall(false);
                continue;
                if (!SipSessionGroup.isRequestEvent("CANCEL", paramEventObject))
                    bool = false;
            }
        }

        private boolean isCurrentTransaction(TransactionTerminatedEvent paramTransactionTerminatedEvent)
        {
            boolean bool = true;
            Object localObject1;
            Object localObject2;
            if (paramTransactionTerminatedEvent.isServerTransaction())
            {
                localObject1 = this.mServerTransaction;
                if (!paramTransactionTerminatedEvent.isServerTransaction())
                    break label99;
                localObject2 = paramTransactionTerminatedEvent.getServerTransaction();
                label27: if ((localObject1 == localObject2) || (this.mState == 9))
                    break label108;
                Log.d("SipSession", "not the current transaction; current=" + toString((Transaction)localObject1) + ", target=" + toString((Transaction)localObject2));
                bool = false;
            }
            while (true)
            {
                return bool;
                localObject1 = this.mClientTransaction;
                break;
                label99: localObject2 = paramTransactionTerminatedEvent.getClientTransaction();
                break label27;
                label108: if (localObject1 != null)
                    Log.d("SipSession", "transaction terminated: " + toString((Transaction)localObject1));
            }
        }

        private void onError(int paramInt, String paramString)
        {
            cancelSessionTimer();
            switch (this.mState)
            {
            default:
                endCallOnError(paramInt, paramString);
            case 1:
            case 2:
            }
            while (true)
            {
                return;
                onRegistrationFailed(paramInt, paramString);
            }
        }

        private void onError(Throwable paramThrowable)
        {
            Throwable localThrowable = SipSessionGroup.this.getRootCause(paramThrowable);
            onError(getErrorCode(localThrowable), localThrowable.toString());
        }

        private void onError(Response paramResponse)
        {
            int i = paramResponse.getStatusCode();
            if ((!this.mInCall) && (i == 486))
                endCallOnBusy();
            while (true)
            {
                return;
                onError(getErrorCode(i), createErrorMessage(paramResponse));
            }
        }

        private void onRegistrationDone(int paramInt)
        {
            reset();
            this.mProxy.onRegistrationDone(this, paramInt);
        }

        private void onRegistrationFailed(int paramInt, String paramString)
        {
            reset();
            this.mProxy.onRegistrationFailed(this, paramInt, paramString);
        }

        private void onRegistrationFailed(Throwable paramThrowable)
        {
            Throwable localThrowable = SipSessionGroup.this.getRootCause(paramThrowable);
            onRegistrationFailed(getErrorCode(localThrowable), localThrowable.toString());
        }

        private void onRegistrationFailed(Response paramResponse)
        {
            onRegistrationFailed(getErrorCode(paramResponse.getStatusCode()), createErrorMessage(paramResponse));
        }

        private boolean outgoingCall(EventObject paramEventObject)
            throws SipException
        {
            boolean bool = true;
            ResponseEvent localResponseEvent;
            Response localResponse;
            int i;
            if (SipSessionGroup.expectResponse("INVITE", paramEventObject))
            {
                localResponseEvent = (ResponseEvent)paramEventObject;
                localResponse = localResponseEvent.getResponse();
                i = localResponse.getStatusCode();
                switch (i)
                {
                default:
                    if (this.mReferSession != null)
                        SipSessionGroup.this.mSipHelper.sendReferNotify(this.mReferSession.mDialog, getResponseString(503));
                    if (i >= 400)
                        onError(localResponse);
                    break;
                case 491:
                case 180:
                case 181:
                case 182:
                case 183:
                case 200:
                case 401:
                case 407:
                }
            }
            while (true)
            {
                return bool;
                if (this.mState == 5)
                {
                    this.mState = 6;
                    cancelSessionTimer();
                    this.mProxy.onRingingBack(this);
                    continue;
                    if (this.mReferSession != null)
                    {
                        SipSessionGroup.this.mSipHelper.sendReferNotify(this.mReferSession.mDialog, getResponseString(200));
                        this.mReferSession = null;
                    }
                    SipSessionGroup.this.mSipHelper.sendInviteAck(localResponseEvent, this.mDialog);
                    this.mPeerSessionDescription = SipSessionGroup.this.extractContent(localResponse);
                    establishCall(bool);
                    continue;
                    if (handleAuthentication(localResponseEvent))
                    {
                        SipSessionGroup.this.addSipSession(this);
                        continue;
                        if (i >= 300)
                        {
                            bool = false;
                            continue;
                            if (SipSessionGroup.END_CALL == paramEventObject)
                            {
                                this.mState = 7;
                                SipSessionGroup.this.mSipHelper.sendCancel(this.mClientTransaction);
                                startSessionTimer(3);
                            }
                            else if (SipSessionGroup.isRequestEvent("INVITE", paramEventObject))
                            {
                                RequestEvent localRequestEvent = (RequestEvent)paramEventObject;
                                SipSessionGroup.this.mSipHelper.sendInviteBusyHere(localRequestEvent, localRequestEvent.getServerTransaction());
                            }
                            else
                            {
                                bool = false;
                            }
                        }
                    }
                }
            }
        }

        private boolean outgoingCallToReady(EventObject paramEventObject)
            throws SipException
        {
            boolean bool = true;
            Response localResponse;
            int i;
            if ((paramEventObject instanceof ResponseEvent))
            {
                localResponse = ((ResponseEvent)paramEventObject).getResponse();
                i = localResponse.getStatusCode();
                if (SipSessionGroup.expectResponse("CANCEL", paramEventObject))
                    if (i != 200)
                        break label84;
            }
            while (true)
            {
                return bool;
                if (SipSessionGroup.expectResponse("INVITE", paramEventObject))
                {
                    switch (i)
                    {
                    default:
                        if (i < 400)
                            break label142;
                        onError(localResponse);
                        break;
                    case 200:
                        outgoingCall(paramEventObject);
                        break;
                    case 487:
                        label84: endCallNormally();
                        break;
                    }
                }
                else
                {
                    bool = false;
                    continue;
                    if ((paramEventObject instanceof TransactionTerminatedEvent))
                        onError(new SipException("timed out"));
                    label142: bool = false;
                }
            }
        }

        private void processCommand(EventObject paramEventObject)
            throws SipException
        {
            if (SipSessionGroup.isLoggable(paramEventObject))
                Log.d("SipSession", "process cmd: " + paramEventObject);
            if (!process(paramEventObject))
                onError(-9, "cannot initiate a new transaction to execute: " + paramEventObject);
        }

        private void processDialogTerminated(DialogTerminatedEvent paramDialogTerminatedEvent)
        {
            if (this.mDialog == paramDialogTerminatedEvent.getDialog())
                onError(new SipException("dialog terminated"));
            while (true)
            {
                return;
                Log.d("SipSession", "not the current dialog; current=" + this.mDialog + ", terminated=" + paramDialogTerminatedEvent.getDialog());
            }
        }

        private boolean processExceptions(EventObject paramEventObject)
            throws SipException
        {
            boolean bool;
            if (SipSessionGroup.isRequestEvent("BYE", paramEventObject))
            {
                SipSessionGroup.this.mSipHelper.sendResponse((RequestEvent)paramEventObject, 200);
                endCallNormally();
                bool = true;
            }
            while (true)
            {
                return bool;
                if (SipSessionGroup.isRequestEvent("CANCEL", paramEventObject))
                {
                    SipSessionGroup.this.mSipHelper.sendResponse((RequestEvent)paramEventObject, 481);
                    bool = true;
                }
                else
                {
                    if ((paramEventObject instanceof TransactionTerminatedEvent))
                    {
                        if (isCurrentTransaction((TransactionTerminatedEvent)paramEventObject))
                        {
                            if ((paramEventObject instanceof TimeoutEvent))
                                processTimeout((TimeoutEvent)paramEventObject);
                            while (true)
                            {
                                bool = true;
                                break;
                                processTransactionTerminated((TransactionTerminatedEvent)paramEventObject);
                            }
                        }
                    }
                    else
                    {
                        if (SipSessionGroup.isRequestEvent("OPTIONS", paramEventObject))
                        {
                            SipSessionGroup.this.mSipHelper.sendResponse((RequestEvent)paramEventObject, 200);
                            bool = true;
                            continue;
                        }
                        if ((paramEventObject instanceof DialogTerminatedEvent))
                        {
                            processDialogTerminated((DialogTerminatedEvent)paramEventObject);
                            bool = true;
                            continue;
                        }
                    }
                    bool = false;
                }
            }
        }

        private boolean processReferRequest(RequestEvent paramRequestEvent)
            throws SipException
        {
            boolean bool = false;
            try
            {
                ReferToHeader localReferToHeader = (ReferToHeader)paramRequestEvent.getRequest().getHeader("Refer-To");
                SipURI localSipURI = (SipURI)localReferToHeader.getAddress().getURI();
                String str = localSipURI.getHeader("Replaces");
                if (localSipURI.getUser() == null)
                {
                    SipSessionGroup.this.mSipHelper.sendResponse(paramRequestEvent, 400);
                }
                else
                {
                    SipSessionGroup.this.mSipHelper.sendResponse(paramRequestEvent, 202);
                    SipSessionImpl localSipSessionImpl = SipSessionGroup.this.createNewSession(paramRequestEvent, this.mProxy.getListener(), SipSessionGroup.this.mSipHelper.getServerTransaction(paramRequestEvent), 0);
                    localSipSessionImpl.mReferSession = this;
                    localSipSessionImpl.mReferredBy = ((ReferredByHeader)paramRequestEvent.getRequest().getHeader("Referred-By"));
                    localSipSessionImpl.mReplaces = str;
                    localSipSessionImpl.mPeerProfile = SipSessionGroup.createPeerProfile(localReferToHeader);
                    localSipSessionImpl.mProxy.onCallTransferring(localSipSessionImpl, null);
                    bool = true;
                }
            }
            catch (IllegalArgumentException localIllegalArgumentException)
            {
                throw new SipException("createPeerProfile()", localIllegalArgumentException);
            }
            return bool;
        }

        private void processTimeout(TimeoutEvent paramTimeoutEvent)
        {
            Log.d("SipSession", "processing Timeout...");
            switch (this.mState)
            {
            case 6:
            default:
                Log.d("SipSession", "     do nothing");
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 7:
            }
            while (true)
            {
                return;
                reset();
                this.mProxy.onRegistrationTimeout(this);
                continue;
                onError(-5, paramTimeoutEvent.toString());
            }
        }

        private void processTransactionTerminated(TransactionTerminatedEvent paramTransactionTerminatedEvent)
        {
            switch (this.mState)
            {
            default:
                Log.d("SipSession", "Transaction terminated early: " + this);
                onError(-3, "transaction terminated");
            case 0:
            case 8:
            }
            while (true)
            {
                return;
                Log.d("SipSession", "Transaction terminated; do nothing");
            }
        }

        private boolean readyForCall(EventObject paramEventObject)
            throws SipException
        {
            boolean bool = false;
            if ((paramEventObject instanceof SipSessionGroup.MakeCallCommand))
            {
                this.mState = 5;
                SipSessionGroup.MakeCallCommand localMakeCallCommand = (SipSessionGroup.MakeCallCommand)paramEventObject;
                this.mPeerProfile = localMakeCallCommand.getPeerProfile();
                if (this.mReferSession != null)
                    SipSessionGroup.this.mSipHelper.sendReferNotify(this.mReferSession.mDialog, getResponseString(100));
                this.mClientTransaction = SipSessionGroup.this.mSipHelper.sendInvite(SipSessionGroup.this.mLocalProfile, this.mPeerProfile, localMakeCallCommand.getSessionDescription(), generateTag(), this.mReferredBy, this.mReplaces);
                this.mDialog = this.mClientTransaction.getDialog();
                SipSessionGroup.this.addSipSession(this);
                startSessionTimer(localMakeCallCommand.getTimeout());
                this.mProxy.onCalling(this);
                bool = true;
            }
            while (true)
            {
                return bool;
                if ((paramEventObject instanceof SipSessionGroup.RegisterCommand))
                {
                    this.mState = 1;
                    int i = ((SipSessionGroup.RegisterCommand)paramEventObject).getDuration();
                    this.mClientTransaction = SipSessionGroup.this.mSipHelper.sendRegister(SipSessionGroup.this.mLocalProfile, generateTag(), i);
                    this.mDialog = this.mClientTransaction.getDialog();
                    SipSessionGroup.this.addSipSession(this);
                    this.mProxy.onRegistering(this);
                    bool = true;
                }
                else if (SipSessionGroup.DEREGISTER == paramEventObject)
                {
                    this.mState = 2;
                    this.mClientTransaction = SipSessionGroup.this.mSipHelper.sendRegister(SipSessionGroup.this.mLocalProfile, generateTag(), 0);
                    this.mDialog = this.mClientTransaction.getDialog();
                    SipSessionGroup.this.addSipSession(this);
                    this.mProxy.onRegistering(this);
                    bool = true;
                }
            }
        }

        private boolean registeringToReady(EventObject paramEventObject)
            throws SipException
        {
            ResponseEvent localResponseEvent;
            boolean bool;
            if (SipSessionGroup.expectResponse("REGISTER", paramEventObject))
            {
                localResponseEvent = (ResponseEvent)paramEventObject;
                Response localResponse = localResponseEvent.getResponse();
                int i = localResponse.getStatusCode();
                switch (i)
                {
                default:
                    if (i >= 500)
                    {
                        onRegistrationFailed(localResponse);
                        bool = true;
                    }
                    break;
                case 200:
                case 401:
                case 407:
                }
            }
            while (true)
            {
                return bool;
                if (this.mState == 1);
                for (int j = getExpiryTime(((ResponseEvent)paramEventObject).getResponse()); ; j = -1)
                {
                    onRegistrationDone(j);
                    bool = true;
                    break;
                }
                handleAuthentication(localResponseEvent);
                bool = true;
                continue;
                bool = false;
            }
        }

        private void reset()
        {
            this.mInCall = false;
            SipSessionGroup.this.removeSipSession(this);
            this.mPeerProfile = null;
            this.mState = 0;
            this.mInviteReceived = null;
            this.mPeerSessionDescription = null;
            this.mAuthenticationRetryCount = 0;
            this.mReferSession = null;
            this.mReferredBy = null;
            this.mReplaces = null;
            if (this.mDialog != null)
                this.mDialog.delete();
            this.mDialog = null;
            try
            {
                if (this.mServerTransaction != null)
                    this.mServerTransaction.terminate();
                label90: this.mServerTransaction = null;
                try
                {
                    if (this.mClientTransaction != null)
                        this.mClientTransaction.terminate();
                    label111: this.mClientTransaction = null;
                    cancelSessionTimer();
                    if (this.mKeepAliveSession != null)
                    {
                        this.mKeepAliveSession.stopKeepAliveProcess();
                        this.mKeepAliveSession = null;
                    }
                    return;
                }
                catch (ObjectInUseException localObjectInUseException2)
                {
                    break label111;
                }
            }
            catch (ObjectInUseException localObjectInUseException1)
            {
                break label90;
            }
        }

        private void startSessionTimer(int paramInt)
        {
            if (paramInt > 0)
            {
                this.mSessionTimer = new SessionTimer();
                this.mSessionTimer.start(paramInt);
            }
        }

        private String toString(Transaction paramTransaction)
        {
            String str;
            if (paramTransaction == null)
            {
                str = "null";
                return str;
            }
            Request localRequest = paramTransaction.getRequest();
            Dialog localDialog = paramTransaction.getDialog();
            CSeqHeader localCSeqHeader = (CSeqHeader)localRequest.getHeader("CSeq");
            Object[] arrayOfObject = new Object[4];
            arrayOfObject[0] = localRequest.getMethod();
            arrayOfObject[1] = Long.valueOf(localCSeqHeader.getSeqNumber());
            arrayOfObject[2] = paramTransaction.getState();
            if (localDialog == null);
            for (Object localObject = "-"; ; localObject = localDialog.getState())
            {
                arrayOfObject[3] = localObject;
                str = String.format("req=%s,%s,s=%s,ds=%s,", arrayOfObject);
                break;
            }
        }

        public void answerCall(String paramString, int paramInt)
        {
            synchronized (SipSessionGroup.this)
            {
                if (this.mPeerProfile != null)
                    doCommandAsync(new SipSessionGroup.MakeCallCommand(SipSessionGroup.this, this.mPeerProfile, paramString, paramInt));
            }
        }

        public void changeCall(String paramString, int paramInt)
        {
            synchronized (SipSessionGroup.this)
            {
                if (this.mPeerProfile != null)
                    doCommandAsync(new SipSessionGroup.MakeCallCommand(SipSessionGroup.this, this.mPeerProfile, paramString, paramInt));
            }
        }

        SipSessionImpl duplicate()
        {
            return new SipSessionImpl(SipSessionGroup.this, this.mProxy.getListener());
        }

        public void endCall()
        {
            doCommandAsync(SipSessionGroup.END_CALL);
        }

        protected String generateTag()
        {
            return String.valueOf(()(4294967296.0D * Math.random()));
        }

        public String getCallId()
        {
            return SipHelper.getCallId(getTransaction());
        }

        public String getLocalIp()
        {
            return SipSessionGroup.this.mLocalIp;
        }

        public SipProfile getLocalProfile()
        {
            return SipSessionGroup.this.mLocalProfile;
        }

        public SipProfile getPeerProfile()
        {
            return this.mPeerProfile;
        }

        public int getState()
        {
            return this.mState;
        }

        public boolean isInCall()
        {
            return this.mInCall;
        }

        public void makeCall(SipProfile paramSipProfile, String paramString, int paramInt)
        {
            doCommandAsync(new SipSessionGroup.MakeCallCommand(SipSessionGroup.this, paramSipProfile, paramString, paramInt));
        }

        public boolean process(EventObject paramEventObject)
            throws SipException
        {
            if (SipSessionGroup.isLoggable(this, paramEventObject))
                Log.d("SipSession", " ~~~~~     " + this + ": " + SipSession.State.toString(this.mState) + ": processing " + SipSessionGroup.log(paramEventObject));
            while (true)
            {
                Dialog localDialog;
                boolean bool1;
                synchronized (SipSessionGroup.this)
                {
                    if (SipSessionGroup.this.isClosed())
                    {
                        bool3 = false;
                    }
                    else if ((this.mKeepAliveProcess != null) && (this.mKeepAliveProcess.process(paramEventObject)))
                    {
                        bool3 = true;
                    }
                    else
                    {
                        localDialog = null;
                        if ((paramEventObject instanceof RequestEvent))
                        {
                            localDialog = ((RequestEvent)paramEventObject).getDialog();
                            if (localDialog != null)
                                this.mDialog = localDialog;
                            switch (this.mState)
                            {
                            case 1:
                                if (bool2)
                                    break label363;
                                if (processExceptions(paramEventObject))
                                    break label363;
                                break;
                            case 2:
                            case 0:
                            case 3:
                            case 4:
                            case 5:
                            case 6:
                            case 7:
                            case 8:
                            case 10:
                            case 9:
                            }
                        }
                    }
                }
                boolean bool2 = false;
                continue;
                label363: boolean bool3 = true;
            }
        }

        public void register(int paramInt)
        {
            doCommandAsync(new SipSessionGroup.RegisterCommand(SipSessionGroup.this, paramInt));
        }

        public void setListener(ISipSessionListener paramISipSessionListener)
        {
            SipSessionListenerProxy localSipSessionListenerProxy = this.mProxy;
            if ((paramISipSessionListener instanceof SipSessionListenerProxy))
                paramISipSessionListener = ((SipSessionListenerProxy)paramISipSessionListener).getListener();
            localSipSessionListenerProxy.setListener(paramISipSessionListener);
        }

        // ERROR //
        public void startKeepAliveProcess(int paramInt, SipProfile paramSipProfile, SipSessionGroup.KeepAliveProcessCallback paramKeepAliveProcessCallback)
            throws SipException
        {
            // Byte code:
            //     0: aload_0
            //     1: getfield 52	com/android/server/sip/SipSessionGroup$SipSessionImpl:this$0	Lcom/android/server/sip/SipSessionGroup;
            //     4: astore 4
            //     6: aload 4
            //     8: monitorenter
            //     9: aload_0
            //     10: getfield 799	com/android/server/sip/SipSessionGroup$SipSessionImpl:mKeepAliveProcess	Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$KeepAliveProcess;
            //     13: ifnull +22 -> 35
            //     16: new 81	javax/sip/SipException
            //     19: dup
            //     20: ldc_w 829
            //     23: invokespecial 539	javax/sip/SipException:<init>	(Ljava/lang/String;)V
            //     26: athrow
            //     27: astore 5
            //     29: aload 4
            //     31: monitorexit
            //     32: aload 5
            //     34: athrow
            //     35: aload_0
            //     36: aload_2
            //     37: putfield 177	com/android/server/sip/SipSessionGroup$SipSessionImpl:mPeerProfile	Landroid/net/sip/SipProfile;
            //     40: aload_0
            //     41: new 13	com/android/server/sip/SipSessionGroup$SipSessionImpl$KeepAliveProcess
            //     44: dup
            //     45: aload_0
            //     46: invokespecial 830	com/android/server/sip/SipSessionGroup$SipSessionImpl$KeepAliveProcess:<init>	(Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;)V
            //     49: putfield 799	com/android/server/sip/SipSessionGroup$SipSessionImpl:mKeepAliveProcess	Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$KeepAliveProcess;
            //     52: aload_0
            //     53: getfield 60	com/android/server/sip/SipSessionGroup$SipSessionImpl:mProxy	Lcom/android/server/sip/SipSessionListenerProxy;
            //     56: aload_0
            //     57: getfield 799	com/android/server/sip/SipSessionGroup$SipSessionImpl:mKeepAliveProcess	Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$KeepAliveProcess;
            //     60: invokevirtual 827	com/android/server/sip/SipSessionListenerProxy:setListener	(Landroid/net/sip/ISipSessionListener;)V
            //     63: aload_0
            //     64: getfield 799	com/android/server/sip/SipSessionGroup$SipSessionImpl:mKeepAliveProcess	Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$KeepAliveProcess;
            //     67: iload_1
            //     68: aload_3
            //     69: invokevirtual 833	com/android/server/sip/SipSessionGroup$SipSessionImpl$KeepAliveProcess:start	(ILcom/android/server/sip/SipSessionGroup$KeepAliveProcessCallback;)V
            //     72: aload 4
            //     74: monitorexit
            //     75: return
            //
            // Exception table:
            //     from	to	target	type
            //     9	32	27	finally
            //     35	75	27	finally
        }

        public void startKeepAliveProcess(int paramInt, SipSessionGroup.KeepAliveProcessCallback paramKeepAliveProcessCallback)
            throws SipException
        {
            synchronized (SipSessionGroup.this)
            {
                startKeepAliveProcess(paramInt, SipSessionGroup.this.mLocalProfile, paramKeepAliveProcessCallback);
                return;
            }
        }

        public void stopKeepAliveProcess()
        {
            synchronized (SipSessionGroup.this)
            {
                if (this.mKeepAliveProcess != null)
                {
                    this.mKeepAliveProcess.stop();
                    this.mKeepAliveProcess = null;
                }
                return;
            }
        }

        public String toString()
        {
            try
            {
                String str2 = super.toString();
                String str3 = str2.substring(str2.indexOf("@")) + ":" + SipSession.State.toString(this.mState);
                str1 = str3;
                return str1;
            }
            catch (Throwable localThrowable)
            {
                while (true)
                    String str1 = super.toString();
            }
        }

        public void unregister()
        {
            doCommandAsync(SipSessionGroup.DEREGISTER);
        }

        class KeepAliveProcess extends SipSessionAdapter
            implements Runnable
        {
            private static final String TAG = "SipKeepAlive";
            private SipSessionGroup.KeepAliveProcessCallback mCallback;
            private int mInterval;
            private boolean mPortChanged = false;
            private int mRPort = 0;
            private boolean mRunning = false;

            KeepAliveProcess()
            {
            }

            private int getRPortFromResponse(Response paramResponse)
            {
                ViaHeader localViaHeader = (ViaHeader)paramResponse.getHeader("Via");
                if (localViaHeader == null);
                for (int i = -1; ; i = localViaHeader.getRPort())
                    return i;
            }

            private boolean parseOptionsResult(EventObject paramEventObject)
            {
                boolean bool = true;
                if (SipSessionGroup.expectResponse("OPTIONS", paramEventObject))
                {
                    int i = getRPortFromResponse(((ResponseEvent)paramEventObject).getResponse());
                    if (i != -1)
                    {
                        if (this.mRPort == 0)
                            this.mRPort = i;
                        if (this.mRPort != i)
                        {
                            this.mPortChanged = bool;
                            this.mRPort = i;
                        }
                    }
                }
                while (true)
                {
                    return bool;
                    bool = false;
                }
            }

            private void sendKeepAlive()
                throws SipException, InterruptedException
            {
                synchronized (SipSessionGroup.this)
                {
                    SipSessionGroup.SipSessionImpl.this.mState = 9;
                    SipSessionGroup.SipSessionImpl.this.mClientTransaction = SipSessionGroup.this.mSipHelper.sendOptions(SipSessionGroup.this.mLocalProfile, SipSessionGroup.SipSessionImpl.this.mPeerProfile, SipSessionGroup.SipSessionImpl.this.generateTag());
                    SipSessionGroup.SipSessionImpl.this.mDialog = SipSessionGroup.SipSessionImpl.this.mClientTransaction.getDialog();
                    SipSessionGroup.this.addSipSession(SipSessionGroup.SipSessionImpl.this);
                    SipSessionGroup.SipSessionImpl.this.startSessionTimer(5);
                    return;
                }
            }

            public void onError(ISipSession paramISipSession, int paramInt, String paramString)
            {
                stop();
                this.mCallback.onError(paramInt, paramString);
            }

            boolean process(EventObject paramEventObject)
                throws SipException
            {
                if ((this.mRunning) && (SipSessionGroup.SipSessionImpl.this.mState == 9) && ((paramEventObject instanceof ResponseEvent)) && (parseOptionsResult(paramEventObject)))
                    if (this.mPortChanged)
                    {
                        SipSessionGroup.this.resetExternalAddress();
                        stop();
                        this.mCallback.onResponse(this.mPortChanged);
                    }
                for (boolean bool = true; ; bool = false)
                {
                    return bool;
                    SipSessionGroup.SipSessionImpl.this.cancelSessionTimer();
                    SipSessionGroup.this.removeSipSession(SipSessionGroup.SipSessionImpl.this);
                    break;
                }
            }

            public void run()
            {
                while (true)
                {
                    synchronized (SipSessionGroup.this)
                    {
                        if (!this.mRunning)
                            return;
                    }
                    try
                    {
                        sendKeepAlive();
                        continue;
                        localObject = finally;
                        throw localObject;
                    }
                    catch (Throwable localThrowable)
                    {
                        while (true)
                            if (this.mRunning)
                                SipSessionGroup.SipSessionImpl.this.onError(localThrowable);
                    }
                }
            }

            void start(int paramInt, SipSessionGroup.KeepAliveProcessCallback paramKeepAliveProcessCallback)
            {
                if (this.mRunning);
                while (true)
                {
                    return;
                    this.mRunning = true;
                    this.mInterval = paramInt;
                    this.mCallback = new SipSessionGroup.KeepAliveProcessCallbackProxy(paramKeepAliveProcessCallback);
                    SipSessionGroup.this.mWakeupTimer.set(paramInt * 1000, this);
                    run();
                }
            }

            void stop()
            {
                synchronized (SipSessionGroup.this)
                {
                    this.mRunning = false;
                    SipSessionGroup.this.mWakeupTimer.cancel(this);
                    SipSessionGroup.SipSessionImpl.this.reset();
                    return;
                }
            }
        }

        class SessionTimer
        {
            private boolean mRunning = true;

            SessionTimer()
            {
            }

            /** @deprecated */
            private void sleep(int paramInt)
            {
                long l = paramInt * 1000;
                try
                {
                    wait(l);
                    return;
                }
                catch (InterruptedException localInterruptedException)
                {
                    while (true)
                        Log.e("SipSession", "session timer interrupted!");
                }
                finally
                {
                }
            }

            private void timeout()
            {
                synchronized (SipSessionGroup.this)
                {
                    SipSessionGroup.SipSessionImpl.this.onError(-5, "Session timed out!");
                    return;
                }
            }

            /** @deprecated */
            void cancel()
            {
                try
                {
                    this.mRunning = false;
                    notify();
                    return;
                }
                finally
                {
                    localObject = finally;
                    throw localObject;
                }
            }

            void start(final int paramInt)
            {
                new Thread(new Runnable()
                {
                    public void run()
                    {
                        SipSessionGroup.SipSessionImpl.SessionTimer.this.sleep(paramInt);
                        if (SipSessionGroup.SipSessionImpl.SessionTimer.this.mRunning)
                            SipSessionGroup.SipSessionImpl.SessionTimer.this.timeout();
                    }
                }
                , "SipSessionTimerThread").start();
            }
        }
    }

    static abstract interface KeepAliveProcessCallback
    {
        public abstract void onError(int paramInt, String paramString);

        public abstract void onResponse(boolean paramBoolean);
    }

    private class SipSessionCallReceiverImpl extends SipSessionGroup.SipSessionImpl
    {
        public SipSessionCallReceiverImpl(ISipSessionListener arg2)
        {
            super(localISipSessionListener);
        }

        private int processInviteWithReplaces(RequestEvent paramRequestEvent, ReplacesHeader paramReplacesHeader)
        {
            int i = 481;
            String str = paramReplacesHeader.getCallId();
            SipSessionGroup.SipSessionImpl localSipSessionImpl = (SipSessionGroup.SipSessionImpl)SipSessionGroup.this.mSessionMap.get(str);
            if (localSipSessionImpl == null);
            while (true)
            {
                return i;
                Dialog localDialog = localSipSessionImpl.mDialog;
                if (localDialog == null)
                {
                    i = 603;
                }
                else if ((localDialog.getLocalTag().equals(paramReplacesHeader.getToTag())) && (localDialog.getRemoteTag().equals(paramReplacesHeader.getFromTag())))
                {
                    ReferredByHeader localReferredByHeader = (ReferredByHeader)paramRequestEvent.getRequest().getHeader("Referred-By");
                    if ((localReferredByHeader != null) && (localDialog.getRemoteParty().equals(localReferredByHeader.getAddress())))
                        i = 200;
                }
            }
        }

        private void processNewInviteRequest(RequestEvent paramRequestEvent)
            throws SipException
        {
            ReplacesHeader localReplacesHeader = (ReplacesHeader)paramRequestEvent.getRequest().getHeader("Replaces");
            SipSessionGroup.SipSessionImpl localSipSessionImpl1 = null;
            int i;
            if (localReplacesHeader != null)
            {
                i = processInviteWithReplaces(paramRequestEvent, localReplacesHeader);
                if (i == 200)
                {
                    SipSessionGroup.SipSessionImpl localSipSessionImpl2 = (SipSessionGroup.SipSessionImpl)SipSessionGroup.this.mSessionMap.get(localReplacesHeader.getCallId());
                    localSipSessionImpl1 = SipSessionGroup.this.createNewSession(paramRequestEvent, localSipSessionImpl2.mProxy.getListener(), SipSessionGroup.this.mSipHelper.getServerTransaction(paramRequestEvent), 3);
                    localSipSessionImpl1.mProxy.onCallTransferring(localSipSessionImpl1, localSipSessionImpl1.mPeerSessionDescription);
                }
            }
            while (true)
            {
                if (localSipSessionImpl1 != null)
                    SipSessionGroup.this.addSipSession(localSipSessionImpl1);
                return;
                SipSessionGroup.this.mSipHelper.sendResponse(paramRequestEvent, i);
                continue;
                localSipSessionImpl1 = SipSessionGroup.this.createNewSession(paramRequestEvent, this.mProxy, SipSessionGroup.this.mSipHelper.sendRinging(paramRequestEvent, generateTag()), 3);
                this.mProxy.onRinging(localSipSessionImpl1, localSipSessionImpl1.mPeerProfile, localSipSessionImpl1.mPeerSessionDescription);
            }
        }

        public boolean process(EventObject paramEventObject)
            throws SipException
        {
            boolean bool = true;
            if (SipSessionGroup.isLoggable(this, paramEventObject))
                Log.d("SipSession", " ~~~~~     " + this + ": " + SipSession.State.toString(this.mState) + ": processing " + SipSessionGroup.log(paramEventObject));
            if (SipSessionGroup.isRequestEvent("INVITE", paramEventObject))
                processNewInviteRequest((RequestEvent)paramEventObject);
            while (true)
            {
                return bool;
                if (SipSessionGroup.isRequestEvent("OPTIONS", paramEventObject))
                    SipSessionGroup.this.mSipHelper.sendResponse((RequestEvent)paramEventObject, 200);
                else
                    bool = false;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         com.android.server.sip.SipSessionGroup
 * JD-Core Version:        0.6.2
 */