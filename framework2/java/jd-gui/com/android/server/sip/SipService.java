package com.android.server.sip;

import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.sip.ISipService.Stub;
import android.net.sip.ISipSession;
import android.net.sip.ISipSessionListener;
import android.net.sip.SipManager;
import android.net.sip.SipProfile;
import android.net.sip.SipProfile.Builder;
import android.net.sip.SipSessionAdapter;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.WifiLock;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemClock;
import android.util.Log;
import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Executor;
import javax.sip.SipException;

public final class SipService extends ISipService.Stub
{
    static final boolean DEBUG = false;
    private static final int DEFAULT_KEEPALIVE_INTERVAL = 10;
    private static final int DEFAULT_MAX_KEEPALIVE_INTERVAL = 120;
    private static final int EXPIRY_TIME = 3600;
    private static final int MIN_EXPIRY_TIME = 60;
    private static final int SHORT_EXPIRY_TIME = 10;
    static final String TAG = "SipService";
    private ConnectivityReceiver mConnectivityReceiver;
    private Context mContext;
    private MyExecutor mExecutor = new MyExecutor();
    private IntervalMeasurementProcess mIntervalMeasurementProcess;
    private int mKeepAliveInterval;
    private int mLastGoodKeepAliveInterval = 10;
    private String mLocalIp;
    private SipWakeLock mMyWakeLock;
    private int mNetworkType = -1;
    private Map<String, ISipSession> mPendingSessions = new HashMap();
    private Map<String, SipSessionGroupExt> mSipGroups = new HashMap();
    private boolean mSipOnWifiOnly;
    private SipWakeupTimer mTimer;
    private WifiManager.WifiLock mWifiLock;

    private SipService(Context paramContext)
    {
        this.mContext = paramContext;
        this.mConnectivityReceiver = new ConnectivityReceiver(null);
        this.mWifiLock = ((WifiManager)paramContext.getSystemService("wifi")).createWifiLock(1, "SipService");
        this.mWifiLock.setReferenceCounted(false);
        this.mSipOnWifiOnly = SipManager.isSipWifiOnly(paramContext);
        this.mMyWakeLock = new SipWakeLock((PowerManager)paramContext.getSystemService("power"));
        this.mTimer = new SipWakeupTimer(paramContext, this.mExecutor);
    }

    /** @deprecated */
    private void addPendingSession(ISipSession paramISipSession)
    {
        try
        {
            cleanUpPendingSessions();
            this.mPendingSessions.put(paramISipSession.getCallId(), paramISipSession);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("SipService", "addPendingSession()", localRemoteException);
        }
        finally
        {
        }
    }

    /** @deprecated */
    private boolean callingSelf(SipSessionGroupExt paramSipSessionGroupExt, SipSessionGroup.SipSessionImpl paramSipSessionImpl)
    {
        try
        {
            String str = paramSipSessionImpl.getCallId();
            Iterator localIterator = this.mSipGroups.values().iterator();
            while (localIterator.hasNext())
            {
                SipSessionGroupExt localSipSessionGroupExt = (SipSessionGroupExt)localIterator.next();
                if (localSipSessionGroupExt != paramSipSessionGroupExt)
                {
                    boolean bool2 = localSipSessionGroupExt.containsSession(str);
                    if (bool2)
                    {
                        bool1 = true;
                        return bool1;
                    }
                }
            }
            boolean bool1 = false;
        }
        finally
        {
        }
    }

    private void cleanUpPendingSessions()
        throws RemoteException
    {
        for (Map.Entry localEntry : (Map.Entry[])this.mPendingSessions.entrySet().toArray(new Map.Entry[this.mPendingSessions.size()]))
            if (((ISipSession)localEntry.getValue()).getState() != 3)
                this.mPendingSessions.remove(localEntry.getKey());
    }

    private SipSessionGroupExt createGroup(SipProfile paramSipProfile)
        throws SipException
    {
        String str = paramSipProfile.getUriString();
        SipSessionGroupExt localSipSessionGroupExt = (SipSessionGroupExt)this.mSipGroups.get(str);
        if (localSipSessionGroupExt == null)
        {
            localSipSessionGroupExt = new SipSessionGroupExt(paramSipProfile, null, null);
            this.mSipGroups.put(str, localSipSessionGroupExt);
            notifyProfileAdded(paramSipProfile);
        }
        while (isCallerCreator(localSipSessionGroupExt))
            return localSipSessionGroupExt;
        throw new SipException("only creator can access the profile");
    }

    private SipSessionGroupExt createGroup(SipProfile paramSipProfile, PendingIntent paramPendingIntent, ISipSessionListener paramISipSessionListener)
        throws SipException
    {
        String str = paramSipProfile.getUriString();
        SipSessionGroupExt localSipSessionGroupExt = (SipSessionGroupExt)this.mSipGroups.get(str);
        if (localSipSessionGroupExt != null)
        {
            if (!isCallerCreator(localSipSessionGroupExt))
                throw new SipException("only creator can access the profile");
            localSipSessionGroupExt.setIncomingCallPendingIntent(paramPendingIntent);
            localSipSessionGroupExt.setListener(paramISipSessionListener);
        }
        while (true)
        {
            return localSipSessionGroupExt;
            localSipSessionGroupExt = new SipSessionGroupExt(paramSipProfile, paramPendingIntent, paramISipSessionListener);
            this.mSipGroups.put(str, localSipSessionGroupExt);
            notifyProfileAdded(paramSipProfile);
        }
    }

    private static Looper createLooper()
    {
        HandlerThread localHandlerThread = new HandlerThread("SipService.Executor");
        localHandlerThread.start();
        return localHandlerThread.getLooper();
    }

    private String determineLocalIp()
    {
        try
        {
            DatagramSocket localDatagramSocket = new DatagramSocket();
            localDatagramSocket.connect(InetAddress.getByName("192.168.1.1"), 80);
            String str2 = localDatagramSocket.getLocalAddress().getHostAddress();
            str1 = str2;
            return str1;
        }
        catch (IOException localIOException)
        {
            while (true)
                String str1 = null;
        }
    }

    private int getKeepAliveInterval()
    {
        if (this.mKeepAliveInterval < 0);
        for (int i = this.mLastGoodKeepAliveInterval; ; i = this.mKeepAliveInterval)
            return i;
    }

    private boolean isBehindNAT(String paramString)
    {
        boolean bool = true;
        try
        {
            byte[] arrayOfByte = InetAddress.getByName(paramString).getAddress();
            if ((arrayOfByte[0] != 10) && (((0xFF & arrayOfByte[0]) != 172) || ((0xF0 & arrayOfByte[1]) != 16)))
            {
                if ((0xFF & arrayOfByte[0]) == 192)
                {
                    int i = arrayOfByte[1];
                    if ((i & 0xFF) != 168);
                }
            }
            else
                return bool;
        }
        catch (UnknownHostException localUnknownHostException)
        {
            while (true)
            {
                Log.e("SipService", "isBehindAT()" + paramString, localUnknownHostException);
                bool = false;
            }
        }
    }

    private boolean isCallerCreator(SipSessionGroupExt paramSipSessionGroupExt)
    {
        if (paramSipSessionGroupExt.getLocalProfile().getCallingUid() == Binder.getCallingUid());
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private boolean isCallerCreatorOrRadio(SipSessionGroupExt paramSipSessionGroupExt)
    {
        if ((isCallerRadio()) || (isCallerCreator(paramSipSessionGroupExt)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private boolean isCallerRadio()
    {
        if (Binder.getCallingUid() == 1001);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private void notifyProfileAdded(SipProfile paramSipProfile)
    {
        Intent localIntent = new Intent("com.android.phone.SIP_ADD_PHONE");
        localIntent.putExtra("android:localSipUri", paramSipProfile.getUriString());
        this.mContext.sendBroadcast(localIntent);
        if (this.mSipGroups.size() == 1)
            registerReceivers();
    }

    private void notifyProfileRemoved(SipProfile paramSipProfile)
    {
        Intent localIntent = new Intent("com.android.phone.SIP_REMOVE_PHONE");
        localIntent.putExtra("android:localSipUri", paramSipProfile.getUriString());
        this.mContext.sendBroadcast(localIntent);
        if (this.mSipGroups.size() == 0)
            unregisterReceivers();
    }

    /** @deprecated */
    private void onConnectivityChanged(NetworkInfo paramNetworkInfo)
    {
        if (paramNetworkInfo != null);
        while (true)
        {
            int i;
            try
            {
                if ((paramNetworkInfo.isConnected()) || (paramNetworkInfo.getType() != this.mNetworkType))
                    paramNetworkInfo = ((ConnectivityManager)this.mContext.getSystemService("connectivity")).getActiveNetworkInfo();
                if ((paramNetworkInfo != null) && (paramNetworkInfo.isConnected()))
                {
                    i = paramNetworkInfo.getType();
                    if ((this.mSipOnWifiOnly) && (i != 1))
                        i = -1;
                    int j = this.mNetworkType;
                    if (j != i);
                }
                else
                {
                    i = -1;
                    continue;
                }
                try
                {
                    if (this.mNetworkType == -1)
                        break label175;
                    this.mLocalIp = null;
                    stopPortMappingMeasurement();
                    Iterator localIterator2 = this.mSipGroups.values().iterator();
                    if (!localIterator2.hasNext())
                        break label175;
                    ((SipSessionGroupExt)localIterator2.next()).onConnectivityChanged(false);
                    continue;
                }
                catch (SipException localSipException)
                {
                    Log.e("SipService", "onConnectivityChanged()", localSipException);
                }
                continue;
            }
            finally
            {
            }
            label175: this.mNetworkType = i;
            if (this.mNetworkType != -1)
            {
                this.mLocalIp = determineLocalIp();
                this.mKeepAliveInterval = -1;
                this.mLastGoodKeepAliveInterval = 10;
                Iterator localIterator1 = this.mSipGroups.values().iterator();
                while (localIterator1.hasNext())
                    ((SipSessionGroupExt)localIterator1.next()).onConnectivityChanged(true);
            }
            updateWakeLocks();
        }
    }

    /** @deprecated */
    private void onKeepAliveIntervalChanged()
    {
        try
        {
            Iterator localIterator = this.mSipGroups.values().iterator();
            if (localIterator.hasNext())
                ((SipSessionGroupExt)localIterator.next()).onKeepAliveIntervalChanged();
        }
        finally
        {
        }
    }

    private void registerReceivers()
    {
        this.mContext.registerReceiver(this.mConnectivityReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }

    private void restartPortMappingLifetimeMeasurement(SipProfile paramSipProfile, int paramInt)
    {
        stopPortMappingMeasurement();
        this.mKeepAliveInterval = -1;
        startPortMappingLifetimeMeasurement(paramSipProfile, paramInt);
    }

    public static void start(Context paramContext)
    {
        if (SipManager.isApiSupported(paramContext))
        {
            ServiceManager.addService("sip", new SipService(paramContext));
            paramContext.sendBroadcast(new Intent("android.net.sip.SIP_SERVICE_UP"));
        }
    }

    private void startPortMappingLifetimeMeasurement(SipProfile paramSipProfile)
    {
        startPortMappingLifetimeMeasurement(paramSipProfile, 120);
    }

    private void startPortMappingLifetimeMeasurement(SipProfile paramSipProfile, int paramInt)
    {
        if ((this.mIntervalMeasurementProcess == null) && (this.mKeepAliveInterval == -1) && (isBehindNAT(this.mLocalIp)))
        {
            Log.d("SipService", "start NAT port mapping timeout measurement on " + paramSipProfile.getUriString());
            int i = this.mLastGoodKeepAliveInterval;
            if (i >= paramInt)
            {
                i = 10;
                this.mLastGoodKeepAliveInterval = i;
                Log.d("SipService", "    reset min interval to " + i);
            }
            this.mIntervalMeasurementProcess = new IntervalMeasurementProcess(paramSipProfile, i, paramInt);
            this.mIntervalMeasurementProcess.start();
        }
    }

    private void stopPortMappingMeasurement()
    {
        if (this.mIntervalMeasurementProcess != null)
        {
            this.mIntervalMeasurementProcess.stop();
            this.mIntervalMeasurementProcess = null;
        }
    }

    private void unregisterReceivers()
    {
        this.mContext.unregisterReceiver(this.mConnectivityReceiver);
        this.mWifiLock.release();
        this.mNetworkType = -1;
    }

    private void updateWakeLocks()
    {
        Iterator localIterator = this.mSipGroups.values().iterator();
        while (true)
            if (localIterator.hasNext())
                if (((SipSessionGroupExt)localIterator.next()).isOpenedToReceiveCalls())
                    if ((this.mNetworkType == 1) || (this.mNetworkType == -1))
                        this.mWifiLock.acquire();
        while (true)
        {
            return;
            this.mWifiLock.release();
            continue;
            this.mWifiLock.release();
            this.mMyWakeLock.reset();
        }
    }

    /** @deprecated */
    public void close(String paramString)
    {
        while (true)
        {
            try
            {
                this.mContext.enforceCallingOrSelfPermission("android.permission.USE_SIP", null);
                SipSessionGroupExt localSipSessionGroupExt1 = (SipSessionGroupExt)this.mSipGroups.get(paramString);
                if (localSipSessionGroupExt1 == null)
                    return;
                if (!isCallerCreatorOrRadio(localSipSessionGroupExt1))
                {
                    Log.w("SipService", "only creator or radio can close this profile");
                    continue;
                }
            }
            finally
            {
            }
            SipSessionGroupExt localSipSessionGroupExt2 = (SipSessionGroupExt)this.mSipGroups.remove(paramString);
            notifyProfileRemoved(localSipSessionGroupExt2.getLocalProfile());
            localSipSessionGroupExt2.close();
            updateWakeLocks();
        }
    }

    /** @deprecated */
    public ISipSession createSession(SipProfile paramSipProfile, ISipSessionListener paramISipSessionListener)
    {
        Object localObject1 = null;
        try
        {
            this.mContext.enforceCallingOrSelfPermission("android.permission.USE_SIP", null);
            paramSipProfile.setCallingUid(Binder.getCallingUid());
            int i = this.mNetworkType;
            if (i == -1);
            while (true)
            {
                return localObject1;
                try
                {
                    ISipSession localISipSession = createGroup(paramSipProfile).createSession(paramISipSessionListener);
                    localObject1 = localISipSession;
                }
                catch (SipException localSipException)
                {
                }
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    public SipProfile[] getListOfProfiles()
    {
        ArrayList localArrayList;
        try
        {
            this.mContext.enforceCallingOrSelfPermission("android.permission.USE_SIP", null);
            boolean bool = isCallerRadio();
            localArrayList = new ArrayList();
            Iterator localIterator = this.mSipGroups.values().iterator();
            while (localIterator.hasNext())
            {
                SipSessionGroupExt localSipSessionGroupExt = (SipSessionGroupExt)localIterator.next();
                if ((bool) || (isCallerCreator(localSipSessionGroupExt)))
                    localArrayList.add(localSipSessionGroupExt.getLocalProfile());
            }
        }
        finally
        {
        }
        SipProfile[] arrayOfSipProfile = (SipProfile[])localArrayList.toArray(new SipProfile[localArrayList.size()]);
        return arrayOfSipProfile;
    }

    /** @deprecated */
    public ISipSession getPendingSession(String paramString)
    {
        ISipSession localISipSession = null;
        try
        {
            this.mContext.enforceCallingOrSelfPermission("android.permission.USE_SIP", null);
            if (paramString == null);
            while (true)
            {
                return localISipSession;
                localISipSession = (ISipSession)this.mPendingSessions.get(paramString);
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    public boolean isOpened(String paramString)
    {
        boolean bool = false;
        try
        {
            this.mContext.enforceCallingOrSelfPermission("android.permission.USE_SIP", null);
            SipSessionGroupExt localSipSessionGroupExt = (SipSessionGroupExt)this.mSipGroups.get(paramString);
            if (localSipSessionGroupExt == null);
            while (true)
            {
                return bool;
                if (isCallerCreatorOrRadio(localSipSessionGroupExt))
                    bool = true;
                else
                    Log.w("SipService", "only creator or radio can query on the profile");
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    public boolean isRegistered(String paramString)
    {
        boolean bool = false;
        try
        {
            this.mContext.enforceCallingOrSelfPermission("android.permission.USE_SIP", null);
            SipSessionGroupExt localSipSessionGroupExt = (SipSessionGroupExt)this.mSipGroups.get(paramString);
            if (localSipSessionGroupExt == null);
            while (true)
            {
                return bool;
                if (isCallerCreatorOrRadio(localSipSessionGroupExt))
                    bool = localSipSessionGroupExt.isRegistered();
                else
                    Log.w("SipService", "only creator or radio can query on the profile");
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    public void open(SipProfile paramSipProfile)
    {
        try
        {
            this.mContext.enforceCallingOrSelfPermission("android.permission.USE_SIP", null);
            paramSipProfile.setCallingUid(Binder.getCallingUid());
            try
            {
                createGroup(paramSipProfile);
                return;
            }
            catch (SipException localSipException)
            {
                while (true)
                    Log.e("SipService", "openToMakeCalls()", localSipException);
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    public void open3(SipProfile paramSipProfile, PendingIntent paramPendingIntent, ISipSessionListener paramISipSessionListener)
    {
        try
        {
            this.mContext.enforceCallingOrSelfPermission("android.permission.USE_SIP", null);
            paramSipProfile.setCallingUid(Binder.getCallingUid());
            if (paramPendingIntent == null)
                Log.w("SipService", "incomingCallPendingIntent cannot be null; the profile is not opened");
            while (true)
            {
                return;
                try
                {
                    SipSessionGroupExt localSipSessionGroupExt = createGroup(paramSipProfile, paramPendingIntent, paramISipSessionListener);
                    if (paramSipProfile.getAutoRegistration())
                    {
                        localSipSessionGroupExt.openToReceiveCalls();
                        updateWakeLocks();
                    }
                }
                catch (SipException localSipException)
                {
                    Log.e("SipService", "openToReceiveCalls()", localSipException);
                }
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    public void setRegistrationListener(String paramString, ISipSessionListener paramISipSessionListener)
    {
        while (true)
        {
            try
            {
                this.mContext.enforceCallingOrSelfPermission("android.permission.USE_SIP", null);
                SipSessionGroupExt localSipSessionGroupExt = (SipSessionGroupExt)this.mSipGroups.get(paramString);
                if (localSipSessionGroupExt == null)
                    return;
                if (isCallerCreator(localSipSessionGroupExt))
                {
                    localSipSessionGroupExt.setListener(paramISipSessionListener);
                    continue;
                }
            }
            finally
            {
            }
            Log.w("SipService", "only creator can set listener on the profile");
        }
    }

    private class MyExecutor extends Handler
        implements Executor
    {
        MyExecutor()
        {
            super();
        }

        private void executeInternal(Runnable paramRunnable)
        {
            try
            {
                paramRunnable.run();
                return;
            }
            catch (Throwable localThrowable)
            {
                while (true)
                {
                    Log.e("SipService", "run task: " + paramRunnable, localThrowable);
                    SipService.this.mMyWakeLock.release(paramRunnable);
                }
            }
            finally
            {
                SipService.this.mMyWakeLock.release(paramRunnable);
            }
        }

        public void execute(Runnable paramRunnable)
        {
            SipService.this.mMyWakeLock.acquire(paramRunnable);
            Message.obtain(this, 0, paramRunnable).sendToTarget();
        }

        public void handleMessage(Message paramMessage)
        {
            if ((paramMessage.obj instanceof Runnable))
                executeInternal((Runnable)paramMessage.obj);
            while (true)
            {
                return;
                Log.w("SipService", "can't handle msg: " + paramMessage);
            }
        }
    }

    private class ConnectivityReceiver extends BroadcastReceiver
    {
        private ConnectivityReceiver()
        {
        }

        public void onReceive(Context paramContext, Intent paramIntent)
        {
            Bundle localBundle = paramIntent.getExtras();
            if (localBundle != null)
            {
                final NetworkInfo localNetworkInfo = (NetworkInfo)localBundle.get("networkInfo");
                SipService.this.mExecutor.execute(new Runnable()
                {
                    public void run()
                    {
                        SipService.this.onConnectivityChanged(localNetworkInfo);
                    }
                });
            }
        }
    }

    private class AutoRegistrationProcess extends SipSessionAdapter
        implements Runnable, SipSessionGroup.KeepAliveProcessCallback
    {
        private static final int MIN_KEEPALIVE_SUCCESS_COUNT = 10;
        private String TAG = "SipAutoReg";
        private int mBackoff = 1;
        private int mErrorCode;
        private String mErrorMessage;
        private long mExpiryTime;
        private SipSessionGroup.SipSessionImpl mKeepAliveSession;
        private int mKeepAliveSuccessCount = 0;
        private SipSessionListenerProxy mProxy = new SipSessionListenerProxy();
        private boolean mRegistered;
        private boolean mRunning = false;
        private SipSessionGroup.SipSessionImpl mSession;

        private AutoRegistrationProcess()
        {
        }

        private int backoffDuration()
        {
            int i = 10 * this.mBackoff;
            if (i > 3600)
                i = 3600;
            while (true)
            {
                return i;
                this.mBackoff = (2 * this.mBackoff);
            }
        }

        private String getAction()
        {
            return toString();
        }

        private boolean notCurrentSession(ISipSession paramISipSession)
        {
            boolean bool1 = true;
            if (paramISipSession != this.mSession)
            {
                ((SipSessionGroup.SipSessionImpl)paramISipSession).setListener(null);
                SipService.this.mMyWakeLock.release(paramISipSession);
                return bool1;
            }
            if (!this.mRunning);
            for (boolean bool2 = bool1; ; bool2 = false)
            {
                bool1 = bool2;
                break;
            }
        }

        private void restart(int paramInt)
        {
            Log.d(this.TAG, "Refresh registration " + paramInt + "s later.");
            SipService.this.mTimer.cancel(this);
            SipService.this.mTimer.set(paramInt * 1000, this);
        }

        private void restartLater()
        {
            this.mRegistered = false;
            restart(backoffDuration());
        }

        private void startKeepAliveProcess(int paramInt)
        {
            if (this.mKeepAliveSession == null)
                this.mKeepAliveSession = this.mSession.duplicate();
            try
            {
                while (true)
                {
                    this.mKeepAliveSession.startKeepAliveProcess(paramInt, this);
                    return;
                    this.mKeepAliveSession.stopKeepAliveProcess();
                }
            }
            catch (SipException localSipException)
            {
                while (true)
                    Log.e(this.TAG, "failed to start keepalive w interval=" + paramInt, localSipException);
            }
        }

        private void stopKeepAliveProcess()
        {
            if (this.mKeepAliveSession != null)
            {
                this.mKeepAliveSession.stopKeepAliveProcess();
                this.mKeepAliveSession = null;
            }
            this.mKeepAliveSuccessCount = 0;
        }

        public boolean isRegistered()
        {
            return this.mRegistered;
        }

        public void onError(int paramInt, String paramString)
        {
            onResponse(true);
        }

        public void onKeepAliveIntervalChanged()
        {
            if (this.mKeepAliveSession != null)
            {
                int i = SipService.this.getKeepAliveInterval();
                this.mKeepAliveSuccessCount = 0;
                startKeepAliveProcess(i);
            }
        }

        public void onRegistering(ISipSession paramISipSession)
        {
            synchronized (SipService.this)
            {
                if (!notCurrentSession(paramISipSession))
                {
                    this.mRegistered = false;
                    this.mProxy.onRegistering(paramISipSession);
                }
            }
        }

        // ERROR //
        public void onRegistrationDone(ISipSession paramISipSession, int paramInt)
        {
            // Byte code:
            //     0: aload_0
            //     1: getfield 36	com/android/server/sip/SipService$AutoRegistrationProcess:this$0	Lcom/android/server/sip/SipService;
            //     4: astore_3
            //     5: aload_3
            //     6: monitorenter
            //     7: aload_0
            //     8: aload_1
            //     9: invokespecial 170	com/android/server/sip/SipService$AutoRegistrationProcess:notCurrentSession	(Landroid/net/sip/ISipSession;)Z
            //     12: ifeq +8 -> 20
            //     15: aload_3
            //     16: monitorexit
            //     17: goto +159 -> 176
            //     20: aload_0
            //     21: getfield 48	com/android/server/sip/SipService$AutoRegistrationProcess:mProxy	Lcom/android/server/sip/SipSessionListenerProxy;
            //     24: aload_1
            //     25: iload_2
            //     26: invokevirtual 176	com/android/server/sip/SipSessionListenerProxy:onRegistrationDone	(Landroid/net/sip/ISipSession;I)V
            //     29: iload_2
            //     30: ifle +127 -> 157
            //     33: aload_0
            //     34: invokestatic 182	android/os/SystemClock:elapsedRealtime	()J
            //     37: iload_2
            //     38: sipush 1000
            //     41: imul
            //     42: i2l
            //     43: ladd
            //     44: putfield 184	com/android/server/sip/SipService$AutoRegistrationProcess:mExpiryTime	J
            //     47: aload_0
            //     48: getfield 126	com/android/server/sip/SipService$AutoRegistrationProcess:mRegistered	Z
            //     51: ifne +83 -> 134
            //     54: aload_0
            //     55: iconst_1
            //     56: putfield 126	com/android/server/sip/SipService$AutoRegistrationProcess:mRegistered	Z
            //     59: iload_2
            //     60: bipush 60
            //     62: isub
            //     63: istore 5
            //     65: iload 5
            //     67: bipush 60
            //     69: if_icmpge +7 -> 76
            //     72: bipush 60
            //     74: istore 5
            //     76: aload_0
            //     77: iload 5
            //     79: invokespecial 130	com/android/server/sip/SipService$AutoRegistrationProcess:restart	(I)V
            //     82: aload_0
            //     83: getfield 70	com/android/server/sip/SipService$AutoRegistrationProcess:mSession	Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;
            //     86: invokevirtual 188	com/android/server/sip/SipSessionGroup$SipSessionImpl:getLocalProfile	()Landroid/net/sip/SipProfile;
            //     89: astore 6
            //     91: aload_0
            //     92: getfield 135	com/android/server/sip/SipService$AutoRegistrationProcess:mKeepAliveSession	Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;
            //     95: ifnonnull +39 -> 134
            //     98: aload_0
            //     99: getfield 36	com/android/server/sip/SipService$AutoRegistrationProcess:this$0	Lcom/android/server/sip/SipService;
            //     102: aload_0
            //     103: getfield 36	com/android/server/sip/SipService$AutoRegistrationProcess:this$0	Lcom/android/server/sip/SipService;
            //     106: invokestatic 192	com/android/server/sip/SipService:access$1500	(Lcom/android/server/sip/SipService;)Ljava/lang/String;
            //     109: invokestatic 196	com/android/server/sip/SipService:access$1600	(Lcom/android/server/sip/SipService;Ljava/lang/String;)Z
            //     112: ifne +11 -> 123
            //     115: aload 6
            //     117: invokevirtual 201	android/net/sip/SipProfile:getSendKeepAlive	()Z
            //     120: ifeq +14 -> 134
            //     123: aload_0
            //     124: aload_0
            //     125: getfield 36	com/android/server/sip/SipService$AutoRegistrationProcess:this$0	Lcom/android/server/sip/SipService;
            //     128: invokestatic 164	com/android/server/sip/SipService:access$1200	(Lcom/android/server/sip/SipService;)I
            //     131: invokespecial 166	com/android/server/sip/SipService$AutoRegistrationProcess:startKeepAliveProcess	(I)V
            //     134: aload_0
            //     135: getfield 36	com/android/server/sip/SipService$AutoRegistrationProcess:this$0	Lcom/android/server/sip/SipService;
            //     138: invokestatic 80	com/android/server/sip/SipService:access$300	(Lcom/android/server/sip/SipService;)Lcom/android/server/sip/SipWakeLock;
            //     141: aload_1
            //     142: invokevirtual 86	com/android/server/sip/SipWakeLock:release	(Ljava/lang/Object;)V
            //     145: aload_3
            //     146: monitorexit
            //     147: goto +29 -> 176
            //     150: astore 4
            //     152: aload_3
            //     153: monitorexit
            //     154: aload 4
            //     156: athrow
            //     157: aload_0
            //     158: iconst_0
            //     159: putfield 126	com/android/server/sip/SipService$AutoRegistrationProcess:mRegistered	Z
            //     162: aload_0
            //     163: ldc2_w 202
            //     166: putfield 184	com/android/server/sip/SipService$AutoRegistrationProcess:mExpiryTime	J
            //     169: aload_0
            //     170: invokevirtual 206	com/android/server/sip/SipService$AutoRegistrationProcess:run	()V
            //     173: goto -28 -> 145
            //     176: return
            //
            // Exception table:
            //     from	to	target	type
            //     7	154	150	finally
            //     157	173	150	finally
        }

        public void onRegistrationFailed(ISipSession paramISipSession, int paramInt, String paramString)
        {
            label79: synchronized (SipService.this)
            {
                if (notCurrentSession(paramISipSession))
                {
                    break label79;
                    restartLater();
                    this.mErrorCode = paramInt;
                    this.mErrorMessage = paramString;
                    this.mProxy.onRegistrationFailed(paramISipSession, paramInt, paramString);
                    SipService.this.mMyWakeLock.release(paramISipSession);
                }
            }
            switch (paramInt)
            {
            default:
            case -12:
            case -8:
            }
        }

        public void onRegistrationTimeout(ISipSession paramISipSession)
        {
            synchronized (SipService.this)
            {
                if (!notCurrentSession(paramISipSession))
                {
                    this.mErrorCode = -5;
                    this.mProxy.onRegistrationTimeout(paramISipSession);
                    restartLater();
                    SipService.this.mMyWakeLock.release(paramISipSession);
                }
            }
        }

        // ERROR //
        public void onResponse(boolean paramBoolean)
        {
            // Byte code:
            //     0: aload_0
            //     1: getfield 36	com/android/server/sip/SipService$AutoRegistrationProcess:this$0	Lcom/android/server/sip/SipService;
            //     4: astore_2
            //     5: aload_2
            //     6: monitorenter
            //     7: iload_1
            //     8: ifeq +123 -> 131
            //     11: aload_0
            //     12: getfield 36	com/android/server/sip/SipService$AutoRegistrationProcess:this$0	Lcom/android/server/sip/SipService;
            //     15: invokestatic 164	com/android/server/sip/SipService:access$1200	(Lcom/android/server/sip/SipService;)I
            //     18: istore 4
            //     20: aload_0
            //     21: getfield 54	com/android/server/sip/SipService$AutoRegistrationProcess:mKeepAliveSuccessCount	I
            //     24: bipush 10
            //     26: if_icmpge +87 -> 113
            //     29: aload_0
            //     30: getfield 43	com/android/server/sip/SipService$AutoRegistrationProcess:TAG	Ljava/lang/String;
            //     33: new 90	java/lang/StringBuilder
            //     36: dup
            //     37: invokespecial 91	java/lang/StringBuilder:<init>	()V
            //     40: ldc 224
            //     42: invokevirtual 97	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     45: iload 4
            //     47: invokevirtual 100	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
            //     50: ldc 226
            //     52: invokevirtual 97	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     55: aload_0
            //     56: getfield 54	com/android/server/sip/SipService$AutoRegistrationProcess:mKeepAliveSuccessCount	I
            //     59: invokevirtual 100	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
            //     62: invokevirtual 103	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     65: invokestatic 229	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
            //     68: pop
            //     69: iload 4
            //     71: bipush 10
            //     73: if_icmple +24 -> 97
            //     76: aload_0
            //     77: getfield 36	com/android/server/sip/SipService$AutoRegistrationProcess:this$0	Lcom/android/server/sip/SipService;
            //     80: aload_0
            //     81: getfield 70	com/android/server/sip/SipService$AutoRegistrationProcess:mSession	Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;
            //     84: invokevirtual 188	com/android/server/sip/SipSessionGroup$SipSessionImpl:getLocalProfile	()Landroid/net/sip/SipProfile;
            //     87: iload 4
            //     89: invokestatic 233	com/android/server/sip/SipService:access$1300	(Lcom/android/server/sip/SipService;Landroid/net/sip/SipProfile;I)V
            //     92: aload_0
            //     93: iconst_0
            //     94: putfield 54	com/android/server/sip/SipService$AutoRegistrationProcess:mKeepAliveSuccessCount	I
            //     97: aload_0
            //     98: getfield 52	com/android/server/sip/SipService$AutoRegistrationProcess:mRunning	Z
            //     101: ifeq +7 -> 108
            //     104: iload_1
            //     105: ifne +53 -> 158
            //     108: aload_2
            //     109: monitorexit
            //     110: goto +79 -> 189
            //     113: aload_0
            //     114: aload_0
            //     115: getfield 54	com/android/server/sip/SipService$AutoRegistrationProcess:mKeepAliveSuccessCount	I
            //     118: iconst_2
            //     119: idiv
            //     120: putfield 54	com/android/server/sip/SipService$AutoRegistrationProcess:mKeepAliveSuccessCount	I
            //     123: goto -26 -> 97
            //     126: astore_3
            //     127: aload_2
            //     128: monitorexit
            //     129: aload_3
            //     130: athrow
            //     131: aload_0
            //     132: getfield 36	com/android/server/sip/SipService$AutoRegistrationProcess:this$0	Lcom/android/server/sip/SipService;
            //     135: aload_0
            //     136: getfield 70	com/android/server/sip/SipService$AutoRegistrationProcess:mSession	Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;
            //     139: invokevirtual 188	com/android/server/sip/SipSessionGroup$SipSessionImpl:getLocalProfile	()Landroid/net/sip/SipProfile;
            //     142: invokestatic 237	com/android/server/sip/SipService:access$1400	(Lcom/android/server/sip/SipService;Landroid/net/sip/SipProfile;)V
            //     145: aload_0
            //     146: iconst_1
            //     147: aload_0
            //     148: getfield 54	com/android/server/sip/SipService$AutoRegistrationProcess:mKeepAliveSuccessCount	I
            //     151: iadd
            //     152: putfield 54	com/android/server/sip/SipService$AutoRegistrationProcess:mKeepAliveSuccessCount	I
            //     155: goto -58 -> 97
            //     158: aload_0
            //     159: aconst_null
            //     160: putfield 135	com/android/server/sip/SipService$AutoRegistrationProcess:mKeepAliveSession	Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;
            //     163: aload_0
            //     164: getfield 36	com/android/server/sip/SipService$AutoRegistrationProcess:this$0	Lcom/android/server/sip/SipService;
            //     167: invokestatic 80	com/android/server/sip/SipService:access$300	(Lcom/android/server/sip/SipService;)Lcom/android/server/sip/SipWakeLock;
            //     170: aload_0
            //     171: getfield 70	com/android/server/sip/SipService$AutoRegistrationProcess:mSession	Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;
            //     174: invokevirtual 240	com/android/server/sip/SipWakeLock:acquire	(Ljava/lang/Object;)V
            //     177: aload_0
            //     178: getfield 70	com/android/server/sip/SipService$AutoRegistrationProcess:mSession	Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;
            //     181: sipush 3600
            //     184: invokevirtual 243	com/android/server/sip/SipSessionGroup$SipSessionImpl:register	(I)V
            //     187: aload_2
            //     188: monitorexit
            //     189: return
            //
            // Exception table:
            //     from	to	target	type
            //     11	129	126	finally
            //     131	189	126	finally
        }

        public void run()
        {
            synchronized (SipService.this)
            {
                if (this.mRunning)
                {
                    this.mErrorCode = 0;
                    this.mErrorMessage = null;
                    if (SipService.this.mNetworkType != -1)
                    {
                        SipService.this.mMyWakeLock.acquire(this.mSession);
                        this.mSession.register(3600);
                    }
                }
            }
        }

        public void setListener(ISipSessionListener paramISipSessionListener)
        {
            while (true)
            {
                int i;
                synchronized (SipService.this)
                {
                    this.mProxy.setListener(paramISipSessionListener);
                    try
                    {
                        if (this.mSession == null)
                        {
                            i = 0;
                            break label256;
                            this.mProxy.onRegistering(this.mSession);
                            return;
                        }
                        i = this.mSession.getState();
                        break label256;
                        if (this.mRegistered)
                        {
                            int j = (int)(this.mExpiryTime - SystemClock.elapsedRealtime());
                            this.mProxy.onRegistrationDone(this.mSession, j);
                            continue;
                        }
                    }
                    catch (Throwable localThrowable)
                    {
                        Log.w(this.TAG, "setListener(): " + localThrowable);
                        continue;
                    }
                }
                if (this.mErrorCode != 0)
                {
                    if (this.mErrorCode == -5)
                        this.mProxy.onRegistrationTimeout(this.mSession);
                    else
                        this.mProxy.onRegistrationFailed(this.mSession, this.mErrorCode, this.mErrorMessage);
                }
                else if (SipService.this.mNetworkType == -1)
                {
                    this.mProxy.onRegistrationFailed(this.mSession, -10, "no data connection");
                }
                else if (!this.mRunning)
                {
                    this.mProxy.onRegistrationFailed(this.mSession, -4, "registration not running");
                }
                else
                {
                    this.mProxy.onRegistrationFailed(this.mSession, -9, String.valueOf(i));
                    continue;
                    label256: if (i != 1)
                        if (i != 2);
                }
            }
        }

        public void start(SipSessionGroup paramSipSessionGroup)
        {
            if (!this.mRunning)
            {
                this.mRunning = true;
                this.mBackoff = 1;
                this.mSession = ((SipSessionGroup.SipSessionImpl)paramSipSessionGroup.createSession(this));
                if (this.mSession != null)
                    break label37;
            }
            while (true)
            {
                return;
                label37: SipService.this.mMyWakeLock.acquire(this.mSession);
                this.mSession.unregister();
                this.TAG = ("SipAutoReg:" + this.mSession.getLocalProfile().getUriString());
            }
        }

        public void stop()
        {
            if (!this.mRunning);
            while (true)
            {
                return;
                this.mRunning = false;
                SipService.this.mMyWakeLock.release(this.mSession);
                if (this.mSession != null)
                {
                    this.mSession.setListener(null);
                    if ((SipService.this.mNetworkType != -1) && (this.mRegistered))
                        this.mSession.unregister();
                }
                SipService.this.mTimer.cancel(this);
                stopKeepAliveProcess();
                this.mRegistered = false;
                setListener(this.mProxy.getListener());
            }
        }
    }

    private class IntervalMeasurementProcess
        implements Runnable, SipSessionGroup.KeepAliveProcessCallback
    {
        private static final int MAX_RETRY_COUNT = 5;
        private static final int MIN_INTERVAL = 5;
        private static final int NAT_MEASUREMENT_RETRY_INTERVAL = 120;
        private static final int PASS_THRESHOLD = 10;
        private static final String TAG = "SipKeepAliveInterval";
        private SipService.SipSessionGroupExt mGroup;
        private int mInterval;
        private SipProfile mLocalProfile;
        private int mMaxInterval;
        private int mMinInterval;
        private int mPassCount;
        private SipSessionGroup.SipSessionImpl mSession;

        public IntervalMeasurementProcess(SipProfile paramInt1, int paramInt2, int arg4)
        {
            int i;
            this.mMaxInterval = i;
            this.mMinInterval = paramInt2;
            this.mLocalProfile = paramInt1;
        }

        private boolean checkTermination()
        {
            if (this.mMaxInterval - this.mMinInterval < 5);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        private void restart()
        {
            synchronized (SipService.this)
            {
                if (this.mSession == null)
                    return;
                Log.d("SipKeepAliveInterval", "restart measurement w interval=" + this.mInterval);
            }
            try
            {
                this.mSession.stopKeepAliveProcess();
                this.mPassCount = 0;
                this.mSession.startKeepAliveProcess(this.mInterval, this);
                return;
                localObject = finally;
                throw localObject;
            }
            catch (SipException localSipException)
            {
                while (true)
                    Log.e("SipKeepAliveInterval", "restart()", localSipException);
            }
        }

        private void restartLater()
        {
            synchronized (SipService.this)
            {
                SipService.this.mTimer.cancel(this);
                SipService.this.mTimer.set(120000, this);
                return;
            }
        }

        public void onError(int paramInt, String paramString)
        {
            Log.w("SipKeepAliveInterval", "interval measurement error: " + paramString);
            restartLater();
        }

        // ERROR //
        public void onResponse(boolean paramBoolean)
        {
            // Byte code:
            //     0: aload_0
            //     1: getfield 39	com/android/server/sip/SipService$IntervalMeasurementProcess:this$0	Lcom/android/server/sip/SipService;
            //     4: astore_2
            //     5: aload_2
            //     6: monitorenter
            //     7: iload_1
            //     8: ifne +120 -> 128
            //     11: iconst_1
            //     12: aload_0
            //     13: getfield 86	com/android/server/sip/SipService$IntervalMeasurementProcess:mPassCount	I
            //     16: iadd
            //     17: istore 5
            //     19: aload_0
            //     20: iload 5
            //     22: putfield 86	com/android/server/sip/SipService$IntervalMeasurementProcess:mPassCount	I
            //     25: iload 5
            //     27: bipush 10
            //     29: if_icmpeq +8 -> 37
            //     32: aload_2
            //     33: monitorexit
            //     34: goto +127 -> 161
            //     37: aload_0
            //     38: getfield 39	com/android/server/sip/SipService$IntervalMeasurementProcess:this$0	Lcom/android/server/sip/SipService;
            //     41: invokestatic 127	com/android/server/sip/SipService:access$900	(Lcom/android/server/sip/SipService;)I
            //     44: ifle +18 -> 62
            //     47: aload_0
            //     48: getfield 39	com/android/server/sip/SipService$IntervalMeasurementProcess:this$0	Lcom/android/server/sip/SipService;
            //     51: aload_0
            //     52: getfield 39	com/android/server/sip/SipService$IntervalMeasurementProcess:this$0	Lcom/android/server/sip/SipService;
            //     55: invokestatic 127	com/android/server/sip/SipService:access$900	(Lcom/android/server/sip/SipService;)I
            //     58: invokestatic 131	com/android/server/sip/SipService:access$1002	(Lcom/android/server/sip/SipService;I)I
            //     61: pop
            //     62: aload_0
            //     63: getfield 39	com/android/server/sip/SipService$IntervalMeasurementProcess:this$0	Lcom/android/server/sip/SipService;
            //     66: astore 6
            //     68: aload_0
            //     69: getfield 66	com/android/server/sip/SipService$IntervalMeasurementProcess:mInterval	I
            //     72: istore 7
            //     74: aload_0
            //     75: iload 7
            //     77: putfield 46	com/android/server/sip/SipService$IntervalMeasurementProcess:mMinInterval	I
            //     80: aload 6
            //     82: iload 7
            //     84: invokestatic 134	com/android/server/sip/SipService:access$902	(Lcom/android/server/sip/SipService;I)I
            //     87: pop
            //     88: aload_0
            //     89: getfield 39	com/android/server/sip/SipService$IntervalMeasurementProcess:this$0	Lcom/android/server/sip/SipService;
            //     92: invokestatic 138	com/android/server/sip/SipService:access$1100	(Lcom/android/server/sip/SipService;)V
            //     95: aload_0
            //     96: invokespecial 140	com/android/server/sip/SipService$IntervalMeasurementProcess:checkTermination	()Z
            //     99: ifeq +40 -> 139
            //     102: aload_0
            //     103: invokevirtual 143	com/android/server/sip/SipService$IntervalMeasurementProcess:stop	()V
            //     106: aload_0
            //     107: getfield 39	com/android/server/sip/SipService$IntervalMeasurementProcess:this$0	Lcom/android/server/sip/SipService;
            //     110: aload_0
            //     111: getfield 46	com/android/server/sip/SipService$IntervalMeasurementProcess:mMinInterval	I
            //     114: invokestatic 134	com/android/server/sip/SipService:access$902	(Lcom/android/server/sip/SipService;I)I
            //     117: pop
            //     118: aload_2
            //     119: monitorexit
            //     120: goto +41 -> 161
            //     123: astore_3
            //     124: aload_2
            //     125: monitorexit
            //     126: aload_3
            //     127: athrow
            //     128: aload_0
            //     129: aload_0
            //     130: getfield 66	com/android/server/sip/SipService$IntervalMeasurementProcess:mInterval	I
            //     133: putfield 44	com/android/server/sip/SipService$IntervalMeasurementProcess:mMaxInterval	I
            //     136: goto -41 -> 95
            //     139: aload_0
            //     140: aload_0
            //     141: getfield 44	com/android/server/sip/SipService$IntervalMeasurementProcess:mMaxInterval	I
            //     144: aload_0
            //     145: getfield 46	com/android/server/sip/SipService$IntervalMeasurementProcess:mMinInterval	I
            //     148: iadd
            //     149: iconst_2
            //     150: idiv
            //     151: putfield 66	com/android/server/sip/SipService$IntervalMeasurementProcess:mInterval	I
            //     154: aload_0
            //     155: invokespecial 145	com/android/server/sip/SipService$IntervalMeasurementProcess:restart	()V
            //     158: goto -40 -> 118
            //     161: return
            //
            // Exception table:
            //     from	to	target	type
            //     11	126	123	finally
            //     128	158	123	finally
        }

        public void run()
        {
            SipService.this.mTimer.cancel(this);
            restart();
        }

        // ERROR //
        public void start()
        {
            // Byte code:
            //     0: aload_0
            //     1: getfield 39	com/android/server/sip/SipService$IntervalMeasurementProcess:this$0	Lcom/android/server/sip/SipService;
            //     4: astore_1
            //     5: aload_1
            //     6: monitorenter
            //     7: aload_0
            //     8: getfield 55	com/android/server/sip/SipService$IntervalMeasurementProcess:mSession	Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;
            //     11: ifnull +8 -> 19
            //     14: aload_1
            //     15: monitorexit
            //     16: goto +219 -> 235
            //     19: aload_0
            //     20: aload_0
            //     21: getfield 44	com/android/server/sip/SipService$IntervalMeasurementProcess:mMaxInterval	I
            //     24: aload_0
            //     25: getfield 46	com/android/server/sip/SipService$IntervalMeasurementProcess:mMinInterval	I
            //     28: iadd
            //     29: iconst_2
            //     30: idiv
            //     31: putfield 66	com/android/server/sip/SipService$IntervalMeasurementProcess:mInterval	I
            //     34: aload_0
            //     35: iconst_0
            //     36: putfield 86	com/android/server/sip/SipService$IntervalMeasurementProcess:mPassCount	I
            //     39: aload_0
            //     40: getfield 66	com/android/server/sip/SipService$IntervalMeasurementProcess:mInterval	I
            //     43: bipush 10
            //     45: if_icmplt +10 -> 55
            //     48: aload_0
            //     49: invokespecial 140	com/android/server/sip/SipService$IntervalMeasurementProcess:checkTermination	()Z
            //     52: ifeq +58 -> 110
            //     55: ldc 23
            //     57: new 57	java/lang/StringBuilder
            //     60: dup
            //     61: invokespecial 58	java/lang/StringBuilder:<init>	()V
            //     64: ldc 151
            //     66: invokevirtual 64	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     69: aload_0
            //     70: getfield 46	com/android/server/sip/SipService$IntervalMeasurementProcess:mMinInterval	I
            //     73: invokevirtual 69	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
            //     76: ldc 153
            //     78: invokevirtual 64	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     81: aload_0
            //     82: getfield 44	com/android/server/sip/SipService$IntervalMeasurementProcess:mMaxInterval	I
            //     85: invokevirtual 69	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
            //     88: ldc 155
            //     90: invokevirtual 64	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     93: invokevirtual 73	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     96: invokestatic 119	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
            //     99: pop
            //     100: aload_1
            //     101: monitorexit
            //     102: goto +133 -> 235
            //     105: astore_2
            //     106: aload_1
            //     107: monitorexit
            //     108: aload_2
            //     109: athrow
            //     110: ldc 23
            //     112: new 57	java/lang/StringBuilder
            //     115: dup
            //     116: invokespecial 58	java/lang/StringBuilder:<init>	()V
            //     119: ldc 157
            //     121: invokevirtual 64	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     124: aload_0
            //     125: getfield 66	com/android/server/sip/SipService$IntervalMeasurementProcess:mInterval	I
            //     128: invokevirtual 69	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
            //     131: invokevirtual 73	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     134: invokestatic 79	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
            //     137: pop
            //     138: aload_0
            //     139: new 159	com/android/server/sip/SipService$SipSessionGroupExt
            //     142: dup
            //     143: aload_0
            //     144: getfield 39	com/android/server/sip/SipService$IntervalMeasurementProcess:this$0	Lcom/android/server/sip/SipService;
            //     147: aload_0
            //     148: getfield 48	com/android/server/sip/SipService$IntervalMeasurementProcess:mLocalProfile	Landroid/net/sip/SipProfile;
            //     151: aconst_null
            //     152: aconst_null
            //     153: invokespecial 162	com/android/server/sip/SipService$SipSessionGroupExt:<init>	(Lcom/android/server/sip/SipService;Landroid/net/sip/SipProfile;Landroid/app/PendingIntent;Landroid/net/sip/ISipSessionListener;)V
            //     156: putfield 164	com/android/server/sip/SipService$IntervalMeasurementProcess:mGroup	Lcom/android/server/sip/SipService$SipSessionGroupExt;
            //     159: aload_0
            //     160: getfield 164	com/android/server/sip/SipService$IntervalMeasurementProcess:mGroup	Lcom/android/server/sip/SipService$SipSessionGroupExt;
            //     163: new 103	com/android/server/sip/SipWakeupTimer
            //     166: dup
            //     167: aload_0
            //     168: getfield 39	com/android/server/sip/SipService$IntervalMeasurementProcess:this$0	Lcom/android/server/sip/SipService;
            //     171: invokestatic 168	com/android/server/sip/SipService:access$700	(Lcom/android/server/sip/SipService;)Landroid/content/Context;
            //     174: aload_0
            //     175: getfield 39	com/android/server/sip/SipService$IntervalMeasurementProcess:this$0	Lcom/android/server/sip/SipService;
            //     178: invokestatic 172	com/android/server/sip/SipService:access$800	(Lcom/android/server/sip/SipService;)Lcom/android/server/sip/SipService$MyExecutor;
            //     181: invokespecial 175	com/android/server/sip/SipWakeupTimer:<init>	(Landroid/content/Context;Ljava/util/concurrent/Executor;)V
            //     184: invokevirtual 179	com/android/server/sip/SipService$SipSessionGroupExt:setWakeupTimer	(Lcom/android/server/sip/SipWakeupTimer;)V
            //     187: aload_0
            //     188: aload_0
            //     189: getfield 164	com/android/server/sip/SipService$IntervalMeasurementProcess:mGroup	Lcom/android/server/sip/SipService$SipSessionGroupExt;
            //     192: aconst_null
            //     193: invokevirtual 183	com/android/server/sip/SipService$SipSessionGroupExt:createSession	(Landroid/net/sip/ISipSessionListener;)Landroid/net/sip/ISipSession;
            //     196: checkcast 81	com/android/server/sip/SipSessionGroup$SipSessionImpl
            //     199: putfield 55	com/android/server/sip/SipService$IntervalMeasurementProcess:mSession	Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;
            //     202: aload_0
            //     203: getfield 55	com/android/server/sip/SipService$IntervalMeasurementProcess:mSession	Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;
            //     206: aload_0
            //     207: getfield 66	com/android/server/sip/SipService$IntervalMeasurementProcess:mInterval	I
            //     210: aload_0
            //     211: invokevirtual 90	com/android/server/sip/SipSessionGroup$SipSessionImpl:startKeepAliveProcess	(ILcom/android/server/sip/SipSessionGroup$KeepAliveProcessCallback;)V
            //     214: aload_1
            //     215: monitorexit
            //     216: goto +19 -> 235
            //     219: astore 4
            //     221: aload_0
            //     222: bipush 252
            //     224: aload 4
            //     226: invokevirtual 184	java/lang/Throwable:toString	()Ljava/lang/String;
            //     229: invokevirtual 186	com/android/server/sip/SipService$IntervalMeasurementProcess:onError	(ILjava/lang/String;)V
            //     232: goto -18 -> 214
            //     235: return
            //
            // Exception table:
            //     from	to	target	type
            //     7	108	105	finally
            //     110	214	105	finally
            //     214	232	105	finally
            //     110	214	219	java/lang/Throwable
        }

        public void stop()
        {
            synchronized (SipService.this)
            {
                if (this.mSession != null)
                {
                    this.mSession.stopKeepAliveProcess();
                    this.mSession = null;
                }
                if (this.mGroup != null)
                {
                    this.mGroup.close();
                    this.mGroup = null;
                }
                SipService.this.mTimer.cancel(this);
                return;
            }
        }
    }

    private class SipSessionGroupExt extends SipSessionAdapter
    {
        private SipService.AutoRegistrationProcess mAutoRegistration = new SipService.AutoRegistrationProcess(SipService.this, null);
        private PendingIntent mIncomingCallPendingIntent;
        private boolean mOpenedToReceiveCalls;
        private SipSessionGroup mSipGroup;

        public SipSessionGroupExt(SipProfile paramPendingIntent, PendingIntent paramISipSessionListener, ISipSessionListener arg4)
            throws SipException
        {
            this.mSipGroup = new SipSessionGroup(duplicate(paramPendingIntent), paramPendingIntent.getPassword(), SipService.this.mTimer, SipService.this.mMyWakeLock);
            this.mIncomingCallPendingIntent = paramISipSessionListener;
            ISipSessionListener localISipSessionListener;
            this.mAutoRegistration.setListener(localISipSessionListener);
        }

        private SipProfile duplicate(SipProfile paramSipProfile)
        {
            try
            {
                SipProfile localSipProfile = new SipProfile.Builder(paramSipProfile).setPassword("*").build();
                return localSipProfile;
            }
            catch (Exception localException)
            {
                Log.wtf("SipService", "duplicate()", localException);
                throw new RuntimeException("duplicate profile", localException);
            }
        }

        private String getUri()
        {
            return this.mSipGroup.getLocalProfileUri();
        }

        public void close()
        {
            this.mOpenedToReceiveCalls = false;
            this.mSipGroup.close();
            this.mAutoRegistration.stop();
        }

        public boolean containsSession(String paramString)
        {
            return this.mSipGroup.containsSession(paramString);
        }

        public ISipSession createSession(ISipSessionListener paramISipSessionListener)
        {
            return this.mSipGroup.createSession(paramISipSessionListener);
        }

        public SipProfile getLocalProfile()
        {
            return this.mSipGroup.getLocalProfile();
        }

        public boolean isOpenedToReceiveCalls()
        {
            return this.mOpenedToReceiveCalls;
        }

        public boolean isRegistered()
        {
            return this.mAutoRegistration.isRegistered();
        }

        public void onConnectivityChanged(boolean paramBoolean)
            throws SipException
        {
            this.mSipGroup.onConnectivityChanged();
            if (paramBoolean)
            {
                this.mSipGroup.reset();
                if (this.mOpenedToReceiveCalls)
                    openToReceiveCalls();
            }
            while (true)
            {
                return;
                this.mSipGroup.close();
                this.mAutoRegistration.stop();
            }
        }

        public void onError(ISipSession paramISipSession, int paramInt, String paramString)
        {
        }

        public void onKeepAliveIntervalChanged()
        {
            this.mAutoRegistration.onKeepAliveIntervalChanged();
        }

        public void onRinging(ISipSession paramISipSession, SipProfile paramSipProfile, String paramString)
        {
            SipSessionGroup.SipSessionImpl localSipSessionImpl = (SipSessionGroup.SipSessionImpl)paramISipSession;
            try
            {
                synchronized (SipService.this)
                {
                    if ((!isRegistered()) || (SipService.this.callingSelf(this, localSipSessionImpl)))
                    {
                        localSipSessionImpl.endCall();
                        return;
                    }
                    SipService.this.addPendingSession(localSipSessionImpl);
                    Intent localIntent = SipManager.createIncomingCallBroadcast(localSipSessionImpl.getCallId(), paramString);
                    this.mIncomingCallPendingIntent.send(SipService.this.mContext, 101, localIntent);
                }
            }
            catch (PendingIntent.CanceledException localCanceledException)
            {
                while (true)
                {
                    Log.w("SipService", "pendingIntent is canceled, drop incoming call");
                    localSipSessionImpl.endCall();
                }
            }
        }

        public void openToReceiveCalls()
            throws SipException
        {
            this.mOpenedToReceiveCalls = true;
            if (SipService.this.mNetworkType != -1)
            {
                this.mSipGroup.openToReceiveCalls(this);
                this.mAutoRegistration.start(this.mSipGroup);
            }
        }

        public void setIncomingCallPendingIntent(PendingIntent paramPendingIntent)
        {
            this.mIncomingCallPendingIntent = paramPendingIntent;
        }

        public void setListener(ISipSessionListener paramISipSessionListener)
        {
            this.mAutoRegistration.setListener(paramISipSessionListener);
        }

        void setWakeupTimer(SipWakeupTimer paramSipWakeupTimer)
        {
            this.mSipGroup.setWakeupTimer(paramSipWakeupTimer);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         com.android.server.sip.SipService
 * JD-Core Version:        0.6.2
 */