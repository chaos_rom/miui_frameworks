package com.android.server.sip;

import android.net.sip.ISipSession;
import android.net.sip.ISipSessionListener;
import android.net.sip.ISipSessionListener.Stub;
import android.net.sip.SipProfile;
import android.os.DeadObjectException;
import android.util.Log;

class SipSessionListenerProxy extends ISipSessionListener.Stub
{
    private static final String TAG = "SipSession";
    private ISipSessionListener mListener;

    private void handle(Throwable paramThrowable, String paramString)
    {
        if ((paramThrowable instanceof DeadObjectException))
            this.mListener = null;
        while (true)
        {
            return;
            if (this.mListener != null)
                Log.w("SipSession", paramString, paramThrowable);
        }
    }

    private void proxy(Runnable paramRunnable)
    {
        new Thread(paramRunnable, "SipSessionCallbackThread").start();
    }

    public ISipSessionListener getListener()
    {
        return this.mListener;
    }

    public void onCallBusy(final ISipSession paramISipSession)
    {
        if (this.mListener == null);
        while (true)
        {
            return;
            proxy(new Runnable()
            {
                public void run()
                {
                    try
                    {
                        SipSessionListenerProxy.this.mListener.onCallBusy(paramISipSession);
                        return;
                    }
                    catch (Throwable localThrowable)
                    {
                        while (true)
                            SipSessionListenerProxy.this.handle(localThrowable, "onCallBusy()");
                    }
                }
            });
        }
    }

    public void onCallChangeFailed(final ISipSession paramISipSession, final int paramInt, final String paramString)
    {
        if (this.mListener == null);
        while (true)
        {
            return;
            proxy(new Runnable()
            {
                public void run()
                {
                    try
                    {
                        SipSessionListenerProxy.this.mListener.onCallChangeFailed(paramISipSession, paramInt, paramString);
                        return;
                    }
                    catch (Throwable localThrowable)
                    {
                        while (true)
                            SipSessionListenerProxy.this.handle(localThrowable, "onCallChangeFailed()");
                    }
                }
            });
        }
    }

    public void onCallEnded(final ISipSession paramISipSession)
    {
        if (this.mListener == null);
        while (true)
        {
            return;
            proxy(new Runnable()
            {
                public void run()
                {
                    try
                    {
                        SipSessionListenerProxy.this.mListener.onCallEnded(paramISipSession);
                        return;
                    }
                    catch (Throwable localThrowable)
                    {
                        while (true)
                            SipSessionListenerProxy.this.handle(localThrowable, "onCallEnded()");
                    }
                }
            });
        }
    }

    public void onCallEstablished(final ISipSession paramISipSession, final String paramString)
    {
        if (this.mListener == null);
        while (true)
        {
            return;
            proxy(new Runnable()
            {
                public void run()
                {
                    try
                    {
                        SipSessionListenerProxy.this.mListener.onCallEstablished(paramISipSession, paramString);
                        return;
                    }
                    catch (Throwable localThrowable)
                    {
                        while (true)
                            SipSessionListenerProxy.this.handle(localThrowable, "onCallEstablished()");
                    }
                }
            });
        }
    }

    public void onCallTransferring(final ISipSession paramISipSession, final String paramString)
    {
        if (this.mListener == null);
        while (true)
        {
            return;
            proxy(new Runnable()
            {
                public void run()
                {
                    try
                    {
                        SipSessionListenerProxy.this.mListener.onCallTransferring(paramISipSession, paramString);
                        return;
                    }
                    catch (Throwable localThrowable)
                    {
                        while (true)
                            SipSessionListenerProxy.this.handle(localThrowable, "onCallTransferring()");
                    }
                }
            });
        }
    }

    public void onCalling(final ISipSession paramISipSession)
    {
        if (this.mListener == null);
        while (true)
        {
            return;
            proxy(new Runnable()
            {
                public void run()
                {
                    try
                    {
                        SipSessionListenerProxy.this.mListener.onCalling(paramISipSession);
                        return;
                    }
                    catch (Throwable localThrowable)
                    {
                        while (true)
                            SipSessionListenerProxy.this.handle(localThrowable, "onCalling()");
                    }
                }
            });
        }
    }

    public void onError(final ISipSession paramISipSession, final int paramInt, final String paramString)
    {
        if (this.mListener == null);
        while (true)
        {
            return;
            proxy(new Runnable()
            {
                public void run()
                {
                    try
                    {
                        SipSessionListenerProxy.this.mListener.onError(paramISipSession, paramInt, paramString);
                        return;
                    }
                    catch (Throwable localThrowable)
                    {
                        while (true)
                            SipSessionListenerProxy.this.handle(localThrowable, "onError()");
                    }
                }
            });
        }
    }

    public void onRegistering(final ISipSession paramISipSession)
    {
        if (this.mListener == null);
        while (true)
        {
            return;
            proxy(new Runnable()
            {
                public void run()
                {
                    try
                    {
                        SipSessionListenerProxy.this.mListener.onRegistering(paramISipSession);
                        return;
                    }
                    catch (Throwable localThrowable)
                    {
                        while (true)
                            SipSessionListenerProxy.this.handle(localThrowable, "onRegistering()");
                    }
                }
            });
        }
    }

    public void onRegistrationDone(final ISipSession paramISipSession, final int paramInt)
    {
        if (this.mListener == null);
        while (true)
        {
            return;
            proxy(new Runnable()
            {
                public void run()
                {
                    try
                    {
                        SipSessionListenerProxy.this.mListener.onRegistrationDone(paramISipSession, paramInt);
                        return;
                    }
                    catch (Throwable localThrowable)
                    {
                        while (true)
                            SipSessionListenerProxy.this.handle(localThrowable, "onRegistrationDone()");
                    }
                }
            });
        }
    }

    public void onRegistrationFailed(final ISipSession paramISipSession, final int paramInt, final String paramString)
    {
        if (this.mListener == null);
        while (true)
        {
            return;
            proxy(new Runnable()
            {
                public void run()
                {
                    try
                    {
                        SipSessionListenerProxy.this.mListener.onRegistrationFailed(paramISipSession, paramInt, paramString);
                        return;
                    }
                    catch (Throwable localThrowable)
                    {
                        while (true)
                            SipSessionListenerProxy.this.handle(localThrowable, "onRegistrationFailed()");
                    }
                }
            });
        }
    }

    public void onRegistrationTimeout(final ISipSession paramISipSession)
    {
        if (this.mListener == null);
        while (true)
        {
            return;
            proxy(new Runnable()
            {
                public void run()
                {
                    try
                    {
                        SipSessionListenerProxy.this.mListener.onRegistrationTimeout(paramISipSession);
                        return;
                    }
                    catch (Throwable localThrowable)
                    {
                        while (true)
                            SipSessionListenerProxy.this.handle(localThrowable, "onRegistrationTimeout()");
                    }
                }
            });
        }
    }

    public void onRinging(final ISipSession paramISipSession, final SipProfile paramSipProfile, final String paramString)
    {
        if (this.mListener == null);
        while (true)
        {
            return;
            proxy(new Runnable()
            {
                public void run()
                {
                    try
                    {
                        SipSessionListenerProxy.this.mListener.onRinging(paramISipSession, paramSipProfile, paramString);
                        return;
                    }
                    catch (Throwable localThrowable)
                    {
                        while (true)
                            SipSessionListenerProxy.this.handle(localThrowable, "onRinging()");
                    }
                }
            });
        }
    }

    public void onRingingBack(final ISipSession paramISipSession)
    {
        if (this.mListener == null);
        while (true)
        {
            return;
            proxy(new Runnable()
            {
                public void run()
                {
                    try
                    {
                        SipSessionListenerProxy.this.mListener.onRingingBack(paramISipSession);
                        return;
                    }
                    catch (Throwable localThrowable)
                    {
                        while (true)
                            SipSessionListenerProxy.this.handle(localThrowable, "onRingingBack()");
                    }
                }
            });
        }
    }

    public void setListener(ISipSessionListener paramISipSessionListener)
    {
        this.mListener = paramISipSessionListener;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         com.android.server.sip.SipSessionListenerProxy
 * JD-Core Version:        0.6.2
 */