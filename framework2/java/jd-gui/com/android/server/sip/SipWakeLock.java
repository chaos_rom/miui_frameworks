package com.android.server.sip;

import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import java.util.HashSet;

class SipWakeLock
{
    private static final boolean DEBUG = false;
    private static final String TAG = "SipWakeLock";
    private HashSet<Object> mHolders = new HashSet();
    private PowerManager mPowerManager;
    private PowerManager.WakeLock mTimerWakeLock;
    private PowerManager.WakeLock mWakeLock;

    SipWakeLock(PowerManager paramPowerManager)
    {
        this.mPowerManager = paramPowerManager;
    }

    /** @deprecated */
    void acquire(long paramLong)
    {
        try
        {
            if (this.mTimerWakeLock == null)
            {
                this.mTimerWakeLock = this.mPowerManager.newWakeLock(1, "SipWakeLock.timer");
                this.mTimerWakeLock.setReferenceCounted(true);
            }
            this.mTimerWakeLock.acquire(paramLong);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void acquire(Object paramObject)
    {
        try
        {
            this.mHolders.add(paramObject);
            if (this.mWakeLock == null)
                this.mWakeLock = this.mPowerManager.newWakeLock(1, "SipWakeLock");
            if (!this.mWakeLock.isHeld())
                this.mWakeLock.acquire();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void release(Object paramObject)
    {
        try
        {
            this.mHolders.remove(paramObject);
            if ((this.mWakeLock != null) && (this.mHolders.isEmpty()) && (this.mWakeLock.isHeld()))
                this.mWakeLock.release();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void reset()
    {
        try
        {
            this.mHolders.clear();
            release(null);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         com.android.server.sip.SipWakeLock
 * JD-Core Version:        0.6.2
 */