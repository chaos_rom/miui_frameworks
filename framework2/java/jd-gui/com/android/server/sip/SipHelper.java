package com.android.server.sip;

import android.net.sip.SipProfile;
import gov.nist.javax.sip.SipStackExt;
import gov.nist.javax.sip.clientauthutils.AccountManager;
import gov.nist.javax.sip.clientauthutils.AuthenticationHelper;
import gov.nist.javax.sip.header.extensions.ReferredByHeader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;
import java.util.regex.Pattern;
import javax.sip.ClientTransaction;
import javax.sip.Dialog;
import javax.sip.DialogTerminatedEvent;
import javax.sip.InvalidArgumentException;
import javax.sip.ListeningPoint;
import javax.sip.PeerUnavailableException;
import javax.sip.RequestEvent;
import javax.sip.ResponseEvent;
import javax.sip.ServerTransaction;
import javax.sip.SipException;
import javax.sip.SipFactory;
import javax.sip.SipProvider;
import javax.sip.SipStack;
import javax.sip.Transaction;
import javax.sip.TransactionState;
import javax.sip.TransactionTerminatedEvent;
import javax.sip.address.Address;
import javax.sip.address.AddressFactory;
import javax.sip.address.SipURI;
import javax.sip.header.CSeqHeader;
import javax.sip.header.CallIdHeader;
import javax.sip.header.ContactHeader;
import javax.sip.header.FromHeader;
import javax.sip.header.HeaderFactory;
import javax.sip.header.MaxForwardsHeader;
import javax.sip.header.ToHeader;
import javax.sip.header.ViaHeader;
import javax.sip.message.Message;
import javax.sip.message.MessageFactory;
import javax.sip.message.Request;
import javax.sip.message.Response;

class SipHelper
{
    private static final boolean DEBUG;
    private static final boolean DEBUG_PING;
    private static final String TAG = SipHelper.class.getSimpleName();
    private AddressFactory mAddressFactory;
    private HeaderFactory mHeaderFactory;
    private MessageFactory mMessageFactory;
    private SipProvider mSipProvider;
    private SipStack mSipStack;

    public SipHelper(SipStack paramSipStack, SipProvider paramSipProvider)
        throws PeerUnavailableException
    {
        this.mSipStack = paramSipStack;
        this.mSipProvider = paramSipProvider;
        SipFactory localSipFactory = SipFactory.getInstance();
        this.mAddressFactory = localSipFactory.createAddressFactory();
        this.mHeaderFactory = localSipFactory.createHeaderFactory();
        this.mMessageFactory = localSipFactory.createMessageFactory();
    }

    private CSeqHeader createCSeqHeader(String paramString)
        throws ParseException, InvalidArgumentException
    {
        long l = ()(10000.0D * Math.random());
        return this.mHeaderFactory.createCSeqHeader(l, paramString);
    }

    private CallIdHeader createCallIdHeader()
    {
        return this.mSipProvider.getNewCallId();
    }

    private ContactHeader createContactHeader(SipProfile paramSipProfile)
        throws ParseException, SipException
    {
        return createContactHeader(paramSipProfile, null, 0);
    }

    private ContactHeader createContactHeader(SipProfile paramSipProfile, String paramString, int paramInt)
        throws ParseException, SipException
    {
        if (paramString == null);
        for (SipURI localSipURI = createSipUri(paramSipProfile.getUserName(), paramSipProfile.getProtocol(), getListeningPoint()); ; localSipURI = createSipUri(paramSipProfile.getUserName(), paramSipProfile.getProtocol(), paramString, paramInt))
        {
            Address localAddress = this.mAddressFactory.createAddress(localSipURI);
            localAddress.setDisplayName(paramSipProfile.getDisplayName());
            return this.mHeaderFactory.createContactHeader(localAddress);
        }
    }

    private FromHeader createFromHeader(SipProfile paramSipProfile, String paramString)
        throws ParseException
    {
        return this.mHeaderFactory.createFromHeader(paramSipProfile.getSipAddress(), paramString);
    }

    private MaxForwardsHeader createMaxForwardsHeader()
        throws InvalidArgumentException
    {
        return this.mHeaderFactory.createMaxForwardsHeader(70);
    }

    private MaxForwardsHeader createMaxForwardsHeader(int paramInt)
        throws InvalidArgumentException
    {
        return this.mHeaderFactory.createMaxForwardsHeader(paramInt);
    }

    private Request createRequest(String paramString1, SipProfile paramSipProfile1, SipProfile paramSipProfile2, String paramString2)
        throws ParseException, SipException
    {
        FromHeader localFromHeader = createFromHeader(paramSipProfile1, paramString2);
        ToHeader localToHeader = createToHeader(paramSipProfile2);
        SipURI localSipURI = paramSipProfile2.getUri();
        List localList = createViaHeaders();
        CallIdHeader localCallIdHeader = createCallIdHeader();
        CSeqHeader localCSeqHeader = createCSeqHeader(paramString1);
        MaxForwardsHeader localMaxForwardsHeader = createMaxForwardsHeader();
        Request localRequest = this.mMessageFactory.createRequest(localSipURI, paramString1, localCallIdHeader, localCSeqHeader, localFromHeader, localToHeader, localList, localMaxForwardsHeader);
        localRequest.addHeader(createContactHeader(paramSipProfile1));
        return localRequest;
    }

    private Request createRequest(String paramString1, SipProfile paramSipProfile, String paramString2)
        throws ParseException, SipException
    {
        FromHeader localFromHeader = createFromHeader(paramSipProfile, paramString2);
        ToHeader localToHeader = createToHeader(paramSipProfile);
        String str = Pattern.quote(paramSipProfile.getUserName() + "@");
        SipURI localSipURI = this.mAddressFactory.createSipURI(paramSipProfile.getUriString().replaceFirst(str, ""));
        List localList = createViaHeaders();
        CallIdHeader localCallIdHeader = createCallIdHeader();
        CSeqHeader localCSeqHeader = createCSeqHeader(paramString1);
        MaxForwardsHeader localMaxForwardsHeader = createMaxForwardsHeader();
        Request localRequest = this.mMessageFactory.createRequest(localSipURI, paramString1, localCallIdHeader, localCSeqHeader, localFromHeader, localToHeader, localList, localMaxForwardsHeader);
        localRequest.addHeader(this.mHeaderFactory.createHeader("User-Agent", "SIPAUA/0.1.001"));
        return localRequest;
    }

    private SipURI createSipUri(String paramString1, String paramString2, String paramString3, int paramInt)
        throws ParseException
    {
        SipURI localSipURI = this.mAddressFactory.createSipURI(paramString1, paramString3);
        try
        {
            localSipURI.setPort(paramInt);
            localSipURI.setTransportParam(paramString2);
            return localSipURI;
        }
        catch (InvalidArgumentException localInvalidArgumentException)
        {
            throw new RuntimeException(localInvalidArgumentException);
        }
    }

    private SipURI createSipUri(String paramString1, String paramString2, ListeningPoint paramListeningPoint)
        throws ParseException
    {
        return createSipUri(paramString1, paramString2, paramListeningPoint.getIPAddress(), paramListeningPoint.getPort());
    }

    private ToHeader createToHeader(SipProfile paramSipProfile)
        throws ParseException
    {
        return createToHeader(paramSipProfile, null);
    }

    private ToHeader createToHeader(SipProfile paramSipProfile, String paramString)
        throws ParseException
    {
        return this.mHeaderFactory.createToHeader(paramSipProfile.getSipAddress(), paramString);
    }

    private List<ViaHeader> createViaHeaders()
        throws ParseException, SipException
    {
        ArrayList localArrayList = new ArrayList(1);
        ListeningPoint localListeningPoint = getListeningPoint();
        ViaHeader localViaHeader = this.mHeaderFactory.createViaHeader(localListeningPoint.getIPAddress(), localListeningPoint.getPort(), localListeningPoint.getTransport(), null);
        localViaHeader.setRPort();
        localArrayList.add(localViaHeader);
        return localArrayList;
    }

    private ContactHeader createWildcardContactHeader()
    {
        ContactHeader localContactHeader = this.mHeaderFactory.createContactHeader();
        localContactHeader.setWildCard();
        return localContactHeader;
    }

    public static String getCallId(EventObject paramEventObject)
    {
        String str;
        if (paramEventObject == null)
            str = null;
        while (true)
        {
            return str;
            if ((paramEventObject instanceof RequestEvent))
            {
                str = getCallId(((RequestEvent)paramEventObject).getRequest());
            }
            else if ((paramEventObject instanceof ResponseEvent))
            {
                str = getCallId(((ResponseEvent)paramEventObject).getResponse());
            }
            else if ((paramEventObject instanceof DialogTerminatedEvent))
            {
                ((DialogTerminatedEvent)paramEventObject).getDialog();
                str = getCallId(((DialogTerminatedEvent)paramEventObject).getDialog());
            }
            else
            {
                if ((paramEventObject instanceof TransactionTerminatedEvent))
                {
                    TransactionTerminatedEvent localTransactionTerminatedEvent = (TransactionTerminatedEvent)paramEventObject;
                    if (localTransactionTerminatedEvent.isServerTransaction());
                    for (Object localObject2 = localTransactionTerminatedEvent.getServerTransaction(); ; localObject2 = localTransactionTerminatedEvent.getClientTransaction())
                    {
                        str = getCallId((Transaction)localObject2);
                        break;
                    }
                }
                Object localObject1 = paramEventObject.getSource();
                if ((localObject1 instanceof Transaction))
                    str = getCallId((Transaction)localObject1);
                else if ((localObject1 instanceof Dialog))
                    str = getCallId((Dialog)localObject1);
                else
                    str = "";
            }
        }
    }

    private static String getCallId(Dialog paramDialog)
    {
        return paramDialog.getCallId().getCallId();
    }

    public static String getCallId(Transaction paramTransaction)
    {
        if (paramTransaction != null);
        for (String str = getCallId(paramTransaction.getRequest()); ; str = "")
            return str;
    }

    private static String getCallId(Message paramMessage)
    {
        return ((CallIdHeader)paramMessage.getHeader("Call-ID")).getCallId();
    }

    private ListeningPoint getListeningPoint()
        throws SipException
    {
        ListeningPoint localListeningPoint = this.mSipProvider.getListeningPoint("UDP");
        if (localListeningPoint == null)
            localListeningPoint = this.mSipProvider.getListeningPoint("TCP");
        if (localListeningPoint == null)
        {
            ListeningPoint[] arrayOfListeningPoint = this.mSipProvider.getListeningPoints();
            if ((arrayOfListeningPoint != null) && (arrayOfListeningPoint.length > 0))
                localListeningPoint = arrayOfListeningPoint[0];
        }
        if (localListeningPoint == null)
            throw new SipException("no listening point is available");
        return localListeningPoint;
    }

    public ServerTransaction getServerTransaction(RequestEvent paramRequestEvent)
        throws SipException
    {
        ServerTransaction localServerTransaction = paramRequestEvent.getServerTransaction();
        if (localServerTransaction == null)
        {
            Request localRequest = paramRequestEvent.getRequest();
            localServerTransaction = this.mSipProvider.getNewServerTransaction(localRequest);
        }
        return localServerTransaction;
    }

    public ClientTransaction handleChallenge(ResponseEvent paramResponseEvent, AccountManager paramAccountManager)
        throws SipException
    {
        AuthenticationHelper localAuthenticationHelper = ((SipStackExt)this.mSipStack).getAuthenticationHelper(paramAccountManager, this.mHeaderFactory);
        ClientTransaction localClientTransaction1 = paramResponseEvent.getClientTransaction();
        ClientTransaction localClientTransaction2 = localAuthenticationHelper.handleChallenge(paramResponseEvent.getResponse(), localClientTransaction1, this.mSipProvider, 5);
        localClientTransaction2.sendRequest();
        return localClientTransaction2;
    }

    public void sendBye(Dialog paramDialog)
        throws SipException
    {
        Request localRequest = paramDialog.createRequest("BYE");
        paramDialog.sendRequest(this.mSipProvider.getNewClientTransaction(localRequest));
    }

    public void sendCancel(ClientTransaction paramClientTransaction)
        throws SipException
    {
        Request localRequest = paramClientTransaction.createCancel();
        this.mSipProvider.getNewClientTransaction(localRequest).sendRequest();
    }

    public ClientTransaction sendInvite(SipProfile paramSipProfile1, SipProfile paramSipProfile2, String paramString1, String paramString2, ReferredByHeader paramReferredByHeader, String paramString3)
        throws SipException
    {
        try
        {
            Request localRequest = createRequest("INVITE", paramSipProfile1, paramSipProfile2, paramString2);
            if (paramReferredByHeader != null)
                localRequest.addHeader(paramReferredByHeader);
            if (paramString3 != null)
                localRequest.addHeader(this.mHeaderFactory.createHeader("Replaces", paramString3));
            localRequest.setContent(paramString1, this.mHeaderFactory.createContentTypeHeader("application", "sdp"));
            ClientTransaction localClientTransaction = this.mSipProvider.getNewClientTransaction(localRequest);
            localClientTransaction.sendRequest();
            return localClientTransaction;
        }
        catch (ParseException localParseException)
        {
            throw new SipException("sendInvite()", localParseException);
        }
    }

    public void sendInviteAck(ResponseEvent paramResponseEvent, Dialog paramDialog)
        throws SipException
    {
        paramDialog.sendAck(paramDialog.createAck(((CSeqHeader)paramResponseEvent.getResponse().getHeader("CSeq")).getSeqNumber()));
    }

    public void sendInviteBusyHere(RequestEvent paramRequestEvent, ServerTransaction paramServerTransaction)
        throws SipException
    {
        try
        {
            Request localRequest = paramRequestEvent.getRequest();
            Response localResponse = this.mMessageFactory.createResponse(486, localRequest);
            if (paramServerTransaction == null)
                paramServerTransaction = getServerTransaction(paramRequestEvent);
            if (paramServerTransaction.getState() != TransactionState.COMPLETED)
                paramServerTransaction.sendResponse(localResponse);
            return;
        }
        catch (ParseException localParseException)
        {
            throw new SipException("sendInviteBusyHere()", localParseException);
        }
    }

    public ServerTransaction sendInviteOk(RequestEvent paramRequestEvent, SipProfile paramSipProfile, String paramString1, ServerTransaction paramServerTransaction, String paramString2, int paramInt)
        throws SipException
    {
        try
        {
            Request localRequest = paramRequestEvent.getRequest();
            Response localResponse = this.mMessageFactory.createResponse(200, localRequest);
            localResponse.addHeader(createContactHeader(paramSipProfile, paramString2, paramInt));
            localResponse.setContent(paramString1, this.mHeaderFactory.createContentTypeHeader("application", "sdp"));
            if (paramServerTransaction == null)
                paramServerTransaction = getServerTransaction(paramRequestEvent);
            if (paramServerTransaction.getState() != TransactionState.COMPLETED)
                paramServerTransaction.sendResponse(localResponse);
            return paramServerTransaction;
        }
        catch (ParseException localParseException)
        {
            throw new SipException("sendInviteOk()", localParseException);
        }
    }

    public void sendInviteRequestTerminated(Request paramRequest, ServerTransaction paramServerTransaction)
        throws SipException
    {
        try
        {
            paramServerTransaction.sendResponse(this.mMessageFactory.createResponse(487, paramRequest));
            return;
        }
        catch (ParseException localParseException)
        {
            throw new SipException("sendInviteRequestTerminated()", localParseException);
        }
    }

    public ClientTransaction sendOptions(SipProfile paramSipProfile1, SipProfile paramSipProfile2, String paramString)
        throws SipException
    {
        if (paramSipProfile1 == paramSipProfile2);
        try
        {
            Request localRequest;
            for (Object localObject = createRequest("OPTIONS", paramSipProfile1, paramString); ; localObject = localRequest)
            {
                ClientTransaction localClientTransaction = this.mSipProvider.getNewClientTransaction((Request)localObject);
                localClientTransaction.sendRequest();
                return localClientTransaction;
                localRequest = createRequest("OPTIONS", paramSipProfile1, paramSipProfile2, paramString);
            }
        }
        catch (Exception localException)
        {
            throw new SipException("sendOptions()", localException);
        }
    }

    public void sendReferNotify(Dialog paramDialog, String paramString)
        throws SipException
    {
        try
        {
            Request localRequest = paramDialog.createRequest("NOTIFY");
            localRequest.addHeader(this.mHeaderFactory.createSubscriptionStateHeader("active;expires=60"));
            localRequest.setContent(paramString, this.mHeaderFactory.createContentTypeHeader("message", "sipfrag"));
            localRequest.addHeader(this.mHeaderFactory.createEventHeader("refer"));
            paramDialog.sendRequest(this.mSipProvider.getNewClientTransaction(localRequest));
            return;
        }
        catch (ParseException localParseException)
        {
            throw new SipException("sendReferNotify()", localParseException);
        }
    }

    public ClientTransaction sendRegister(SipProfile paramSipProfile, String paramString, int paramInt)
        throws SipException
    {
        try
        {
            Request localRequest = createRequest("REGISTER", paramSipProfile, paramString);
            if (paramInt == 0)
                localRequest.addHeader(createWildcardContactHeader());
            while (true)
            {
                localRequest.addHeader(this.mHeaderFactory.createExpiresHeader(paramInt));
                ClientTransaction localClientTransaction = this.mSipProvider.getNewClientTransaction(localRequest);
                localClientTransaction.sendRequest();
                return localClientTransaction;
                localRequest.addHeader(createContactHeader(paramSipProfile));
            }
        }
        catch (ParseException localParseException)
        {
            throw new SipException("sendRegister()", localParseException);
        }
    }

    public ClientTransaction sendReinvite(Dialog paramDialog, String paramString)
        throws SipException
    {
        try
        {
            Request localRequest = paramDialog.createRequest("INVITE");
            localRequest.setContent(paramString, this.mHeaderFactory.createContentTypeHeader("application", "sdp"));
            ViaHeader localViaHeader = (ViaHeader)localRequest.getHeader("Via");
            if (localViaHeader != null)
                localViaHeader.setRPort();
            ClientTransaction localClientTransaction = this.mSipProvider.getNewClientTransaction(localRequest);
            paramDialog.sendRequest(localClientTransaction);
            return localClientTransaction;
        }
        catch (ParseException localParseException)
        {
            throw new SipException("sendReinvite()", localParseException);
        }
    }

    public void sendResponse(RequestEvent paramRequestEvent, int paramInt)
        throws SipException
    {
        try
        {
            Request localRequest = paramRequestEvent.getRequest();
            Response localResponse = this.mMessageFactory.createResponse(paramInt, localRequest);
            getServerTransaction(paramRequestEvent).sendResponse(localResponse);
            return;
        }
        catch (ParseException localParseException)
        {
            throw new SipException("sendResponse()", localParseException);
        }
    }

    public ServerTransaction sendRinging(RequestEvent paramRequestEvent, String paramString)
        throws SipException
    {
        try
        {
            Request localRequest = paramRequestEvent.getRequest();
            ServerTransaction localServerTransaction = getServerTransaction(paramRequestEvent);
            Response localResponse = this.mMessageFactory.createResponse(180, localRequest);
            ToHeader localToHeader = (ToHeader)localResponse.getHeader("To");
            localToHeader.setTag(paramString);
            localResponse.addHeader(localToHeader);
            localServerTransaction.sendResponse(localResponse);
            return localServerTransaction;
        }
        catch (ParseException localParseException)
        {
            throw new SipException("sendRinging()", localParseException);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         com.android.server.sip.SipHelper
 * JD-Core Version:        0.6.2
 */