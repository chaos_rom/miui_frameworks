package com.android.server.sip;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.concurrent.Executor;

class SipWakeupTimer extends BroadcastReceiver
{
    private static final boolean DEBUG_TIMER = false;
    private static final String TAG = "_SIP.WkTimer_";
    private static final String TRIGGER_TIME = "TriggerTime";
    private AlarmManager mAlarmManager;
    private Context mContext;
    private TreeSet<MyEvent> mEventQueue = new TreeSet(new MyEventComparator(null));
    private Executor mExecutor;
    private PendingIntent mPendingIntent;

    public SipWakeupTimer(Context paramContext, Executor paramExecutor)
    {
        this.mContext = paramContext;
        this.mAlarmManager = ((AlarmManager)paramContext.getSystemService("alarm"));
        paramContext.registerReceiver(this, new IntentFilter(getAction()));
        this.mExecutor = paramExecutor;
    }

    private void cancelAlarm()
    {
        this.mAlarmManager.cancel(this.mPendingIntent);
        this.mPendingIntent = null;
    }

    private void execute(long paramLong)
    {
        if ((stopped()) || (this.mEventQueue.isEmpty()));
        while (true)
        {
            return;
            Iterator localIterator = this.mEventQueue.iterator();
            while (localIterator.hasNext())
            {
                MyEvent localMyEvent = (MyEvent)localIterator.next();
                if (localMyEvent.mTriggerTime == paramLong)
                {
                    localMyEvent.mLastTriggerTime = paramLong;
                    localMyEvent.mTriggerTime += localMyEvent.mPeriod;
                    this.mExecutor.execute(localMyEvent.mCallback);
                }
            }
            scheduleNext();
        }
    }

    private String getAction()
    {
        return toString();
    }

    private void insertEvent(MyEvent paramMyEvent)
    {
        long l1 = SystemClock.elapsedRealtime();
        if (this.mEventQueue.isEmpty())
        {
            paramMyEvent.mTriggerTime = (l1 + paramMyEvent.mPeriod);
            this.mEventQueue.add(paramMyEvent);
        }
        MyEvent localMyEvent;
        while (true)
        {
            return;
            localMyEvent = (MyEvent)this.mEventQueue.first();
            int i = localMyEvent.mPeriod;
            if (i > paramMyEvent.mMaxPeriod)
                break;
            paramMyEvent.mPeriod = (i * (paramMyEvent.mMaxPeriod / i));
            int j = i * ((paramMyEvent.mMaxPeriod - (int)(localMyEvent.mTriggerTime - l1)) / i);
            localMyEvent.mTriggerTime += j;
            this.mEventQueue.add(paramMyEvent);
        }
        long l2 = l1 + paramMyEvent.mPeriod;
        if (localMyEvent.mTriggerTime < l2)
        {
            paramMyEvent.mTriggerTime = localMyEvent.mTriggerTime;
            paramMyEvent.mLastTriggerTime -= paramMyEvent.mPeriod;
        }
        while (true)
        {
            this.mEventQueue.add(paramMyEvent);
            recalculatePeriods();
            break;
            paramMyEvent.mTriggerTime = l2;
        }
    }

    private void printQueue()
    {
        int i = 0;
        Iterator localIterator = this.mEventQueue.iterator();
        do
        {
            if (!localIterator.hasNext())
                break;
            MyEvent localMyEvent = (MyEvent)localIterator.next();
            Log.d("_SIP.WkTimer_", "         " + localMyEvent + ": scheduled at " + showTime(localMyEvent.mTriggerTime) + ": last at " + showTime(localMyEvent.mLastTriggerTime));
            i++;
        }
        while (i < 5);
        if (this.mEventQueue.size() > i)
            Log.d("_SIP.WkTimer_", "         .....");
        while (true)
        {
            return;
            if (i == 0)
                Log.d("_SIP.WkTimer_", "         <empty>");
        }
    }

    private void recalculatePeriods()
    {
        if (this.mEventQueue.isEmpty());
        while (true)
        {
            return;
            MyEvent localMyEvent1 = (MyEvent)this.mEventQueue.first();
            int i = localMyEvent1.mMaxPeriod;
            long l = localMyEvent1.mTriggerTime;
            Iterator localIterator = this.mEventQueue.iterator();
            while (localIterator.hasNext())
            {
                MyEvent localMyEvent2 = (MyEvent)localIterator.next();
                localMyEvent2.mPeriod = (i * (localMyEvent2.mMaxPeriod / i));
                localMyEvent2.mTriggerTime = (l + i * ((int)(localMyEvent2.mLastTriggerTime + localMyEvent2.mMaxPeriod - l) / i));
            }
            TreeSet localTreeSet = new TreeSet(this.mEventQueue.comparator());
            localTreeSet.addAll(this.mEventQueue);
            this.mEventQueue.clear();
            this.mEventQueue = localTreeSet;
        }
    }

    private void scheduleNext()
    {
        if ((stopped()) || (this.mEventQueue.isEmpty()));
        while (true)
        {
            return;
            if (this.mPendingIntent != null)
                throw new RuntimeException("pendingIntent is not null!");
            MyEvent localMyEvent = (MyEvent)this.mEventQueue.first();
            Intent localIntent = new Intent(getAction());
            localIntent.putExtra("TriggerTime", localMyEvent.mTriggerTime);
            PendingIntent localPendingIntent = PendingIntent.getBroadcast(this.mContext, 0, localIntent, 134217728);
            this.mPendingIntent = localPendingIntent;
            this.mAlarmManager.set(2, localMyEvent.mTriggerTime, localPendingIntent);
        }
    }

    private String showTime(long paramLong)
    {
        int i = (int)(paramLong % 1000L);
        int j = (int)(paramLong / 1000L);
        int k = j / 60;
        int m = j % 60;
        Object[] arrayOfObject = new Object[3];
        arrayOfObject[0] = Integer.valueOf(k);
        arrayOfObject[1] = Integer.valueOf(m);
        arrayOfObject[2] = Integer.valueOf(i);
        return String.format("%d.%d.%d", arrayOfObject);
    }

    private boolean stopped()
    {
        if (this.mEventQueue == null)
            Log.w("_SIP.WkTimer_", "Timer stopped");
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    /** @deprecated */
    public void cancel(Runnable paramRunnable)
    {
        while (true)
        {
            MyEvent localMyEvent1;
            try
            {
                if (!stopped())
                {
                    boolean bool = this.mEventQueue.isEmpty();
                    if (!bool);
                }
                else
                {
                    return;
                }
                localMyEvent1 = (MyEvent)this.mEventQueue.first();
                Iterator localIterator = this.mEventQueue.iterator();
                if (localIterator.hasNext())
                {
                    if (((MyEvent)localIterator.next()).mCallback != paramRunnable)
                        continue;
                    localIterator.remove();
                    continue;
                }
            }
            finally
            {
            }
            if (this.mEventQueue.isEmpty())
            {
                cancelAlarm();
            }
            else if (this.mEventQueue.first() != localMyEvent1)
            {
                cancelAlarm();
                MyEvent localMyEvent2 = (MyEvent)this.mEventQueue.first();
                localMyEvent2.mPeriod = localMyEvent2.mMaxPeriod;
                localMyEvent2.mTriggerTime = (localMyEvent2.mLastTriggerTime + localMyEvent2.mPeriod);
                recalculatePeriods();
                scheduleNext();
            }
        }
    }

    /** @deprecated */
    public void onReceive(Context paramContext, Intent paramIntent)
    {
        try
        {
            String str = paramIntent.getAction();
            if ((getAction().equals(str)) && (paramIntent.getExtras().containsKey("TriggerTime")))
            {
                this.mPendingIntent = null;
                execute(paramIntent.getLongExtra("TriggerTime", -1L));
            }
            while (true)
            {
                return;
                Log.d("_SIP.WkTimer_", "unrecognized intent: " + paramIntent);
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    public void set(int paramInt, Runnable paramRunnable)
    {
        try
        {
            boolean bool = stopped();
            if (bool);
            MyEvent localMyEvent;
            while (true)
            {
                return;
                localMyEvent = new MyEvent(paramInt, paramRunnable, SystemClock.elapsedRealtime());
                insertEvent(localMyEvent);
                if (this.mEventQueue.first() == localMyEvent)
                {
                    if (this.mEventQueue.size() > 1)
                        cancelAlarm();
                    scheduleNext();
                }
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    public void stop()
    {
        try
        {
            this.mContext.unregisterReceiver(this);
            if (this.mPendingIntent != null)
            {
                this.mAlarmManager.cancel(this.mPendingIntent);
                this.mPendingIntent = null;
            }
            this.mEventQueue.clear();
            this.mEventQueue = null;
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private static class MyEventComparator
        implements Comparator<SipWakeupTimer.MyEvent>
    {
        public int compare(SipWakeupTimer.MyEvent paramMyEvent1, SipWakeupTimer.MyEvent paramMyEvent2)
        {
            int i;
            if (paramMyEvent1 == paramMyEvent2)
                i = 0;
            while (true)
            {
                return i;
                i = paramMyEvent1.mMaxPeriod - paramMyEvent2.mMaxPeriod;
                if (i == 0)
                    i = -1;
            }
        }

        public boolean equals(Object paramObject)
        {
            if (this == paramObject);
            for (boolean bool = true; ; bool = false)
                return bool;
        }
    }

    private static class MyEvent
    {
        Runnable mCallback;
        long mLastTriggerTime;
        int mMaxPeriod;
        int mPeriod;
        long mTriggerTime;

        MyEvent(int paramInt, Runnable paramRunnable, long paramLong)
        {
            this.mMaxPeriod = paramInt;
            this.mPeriod = paramInt;
            this.mCallback = paramRunnable;
            this.mLastTriggerTime = paramLong;
        }

        private String toString(Object paramObject)
        {
            String str = paramObject.toString();
            int i = str.indexOf("$");
            if (i > 0)
                str = str.substring(i + 1);
            return str;
        }

        public String toString()
        {
            String str1 = super.toString();
            String str2 = str1.substring(str1.indexOf("@"));
            return str2 + ":" + this.mPeriod / 1000 + ":" + this.mMaxPeriod / 1000 + ":" + toString(this.mCallback);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         com.android.server.sip.SipWakeupTimer
 * JD-Core Version:        0.6.2
 */