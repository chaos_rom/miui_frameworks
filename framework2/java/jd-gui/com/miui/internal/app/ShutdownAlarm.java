package com.miui.internal.app;

import android.content.Context;
import android.text.format.DateFormat;
import android.util.Log;
import android.widget.CheckBox;
import java.util.Calendar;

public class ShutdownAlarm
{
    private static final String TAG = "ShutdownAlarm";
    private static final String WAKEALARM_PATH = "/sys/class/rtc/rtc0/wakealarm";

    public static CheckBox buildShutdownAlarmCheckBox(Context paramContext)
    {
        long l = 1000L * readWakeAlarm();
        int i = getWakeAlarmDeltaDays(l);
        int j;
        CheckBox localCheckBox;
        StringBuilder localStringBuilder;
        if ((i == 0) || (i == 1))
        {
            j = 1;
            localCheckBox = null;
            if (j != 0)
            {
                localCheckBox = new CheckBox(paramContext);
                localCheckBox.setChecked(true);
                localStringBuilder = new StringBuilder();
                if (i != 0)
                    break label134;
                localStringBuilder.append(paramContext.getString(101450193));
            }
        }
        while (true)
        {
            Calendar localCalendar = Calendar.getInstance();
            localCalendar.setTimeInMillis(l);
            localStringBuilder.append(formatTime(paramContext, localCalendar));
            Object[] arrayOfObject = new Object[1];
            arrayOfObject[0] = localStringBuilder.toString();
            localCheckBox.setText(paramContext.getString(101450229, arrayOfObject));
            return localCheckBox;
            j = 0;
            break;
            label134: if (i == 1)
                localStringBuilder.append(paramContext.getString(101450153));
        }
    }

    private static String formatTime(Context paramContext, Calendar paramCalendar)
    {
        String str1;
        if (DateFormat.is24HourFormat(paramContext))
        {
            str1 = "kk:mm";
            if (paramCalendar != null)
                break label29;
        }
        label29: for (String str2 = ""; ; str2 = (String)DateFormat.format(str1, paramCalendar))
        {
            return str2;
            str1 = paramContext.getString(101450230);
            break;
        }
    }

    private static int getWakeAlarmDeltaDays(long paramLong)
    {
        long l = paramLong - System.currentTimeMillis();
        int i = -1;
        if ((l > 0L) && (l < 172800000L))
        {
            Calendar localCalendar = Calendar.getInstance();
            int j = localCalendar.get(7);
            localCalendar.setTimeInMillis(paramLong);
            i = (7 + (localCalendar.get(7) - j)) % 7;
            Log.d("ShutdownAlarm", "wake alarm days: " + i);
        }
        return i;
    }

    // ERROR //
    public static long readWakeAlarm()
    {
        // Byte code:
        //     0: lconst_0
        //     1: lstore_0
        //     2: aconst_null
        //     3: astore_2
        //     4: new 124	java/io/BufferedReader
        //     7: dup
        //     8: new 126	java/io/FileReader
        //     11: dup
        //     12: ldc 11
        //     14: invokespecial 129	java/io/FileReader:<init>	(Ljava/lang/String;)V
        //     17: invokespecial 132	java/io/BufferedReader:<init>	(Ljava/io/Reader;)V
        //     20: astore_3
        //     21: aload_3
        //     22: invokevirtual 135	java/io/BufferedReader:readLine	()Ljava/lang/String;
        //     25: invokestatic 141	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     28: istore 19
        //     30: iload 19
        //     32: i2l
        //     33: lstore_0
        //     34: aload_3
        //     35: ifnull +194 -> 229
        //     38: aload_3
        //     39: invokevirtual 144	java/io/BufferedReader:close	()V
        //     42: lload_0
        //     43: lreturn
        //     44: astore 20
        //     46: ldc 8
        //     48: ldc 146
        //     50: aload 20
        //     52: invokestatic 150	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     55: pop
        //     56: goto -14 -> 42
        //     59: astore 4
        //     61: ldc 8
        //     63: ldc 146
        //     65: aload 4
        //     67: invokestatic 150	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     70: pop
        //     71: aload_2
        //     72: ifnull -30 -> 42
        //     75: aload_2
        //     76: invokevirtual 144	java/io/BufferedReader:close	()V
        //     79: goto -37 -> 42
        //     82: astore 9
        //     84: ldc 8
        //     86: ldc 146
        //     88: aload 9
        //     90: invokestatic 150	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     93: pop
        //     94: goto -52 -> 42
        //     97: astore 11
        //     99: ldc 8
        //     101: ldc 146
        //     103: aload 11
        //     105: invokestatic 150	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     108: pop
        //     109: aload_2
        //     110: ifnull -68 -> 42
        //     113: aload_2
        //     114: invokevirtual 144	java/io/BufferedReader:close	()V
        //     117: goto -75 -> 42
        //     120: astore 13
        //     122: ldc 8
        //     124: ldc 146
        //     126: aload 13
        //     128: invokestatic 150	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     131: pop
        //     132: goto -90 -> 42
        //     135: astore 15
        //     137: ldc 8
        //     139: ldc 146
        //     141: aload 15
        //     143: invokestatic 150	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     146: pop
        //     147: aload_2
        //     148: ifnull -106 -> 42
        //     151: aload_2
        //     152: invokevirtual 144	java/io/BufferedReader:close	()V
        //     155: goto -113 -> 42
        //     158: astore 17
        //     160: ldc 8
        //     162: ldc 146
        //     164: aload 17
        //     166: invokestatic 150	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     169: pop
        //     170: goto -128 -> 42
        //     173: astore 5
        //     175: aload_2
        //     176: ifnull +7 -> 183
        //     179: aload_2
        //     180: invokevirtual 144	java/io/BufferedReader:close	()V
        //     183: aload 5
        //     185: athrow
        //     186: astore 6
        //     188: ldc 8
        //     190: ldc 146
        //     192: aload 6
        //     194: invokestatic 150	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     197: pop
        //     198: goto -15 -> 183
        //     201: astore 5
        //     203: aload_3
        //     204: astore_2
        //     205: goto -30 -> 175
        //     208: astore 15
        //     210: aload_3
        //     211: astore_2
        //     212: goto -75 -> 137
        //     215: astore 11
        //     217: aload_3
        //     218: astore_2
        //     219: goto -120 -> 99
        //     222: astore 4
        //     224: aload_3
        //     225: astore_2
        //     226: goto -165 -> 61
        //     229: goto -187 -> 42
        //
        // Exception table:
        //     from	to	target	type
        //     38	42	44	java/io/IOException
        //     4	21	59	java/io/FileNotFoundException
        //     75	79	82	java/io/IOException
        //     4	21	97	java/lang/NumberFormatException
        //     113	117	120	java/io/IOException
        //     4	21	135	java/io/IOException
        //     151	155	158	java/io/IOException
        //     4	21	173	finally
        //     61	71	173	finally
        //     99	109	173	finally
        //     137	147	173	finally
        //     179	183	186	java/io/IOException
        //     21	30	201	finally
        //     21	30	208	java/io/IOException
        //     21	30	215	java/lang/NumberFormatException
        //     21	30	222	java/io/FileNotFoundException
    }

    // ERROR //
    public static void writeWakeAlarm(long paramLong)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_2
        //     2: new 153	java/io/BufferedWriter
        //     5: dup
        //     6: new 155	java/io/FileWriter
        //     9: dup
        //     10: ldc 11
        //     12: invokespecial 156	java/io/FileWriter:<init>	(Ljava/lang/String;)V
        //     15: invokespecial 159	java/io/BufferedWriter:<init>	(Ljava/io/Writer;)V
        //     18: astore_3
        //     19: aload_3
        //     20: lload_0
        //     21: invokestatic 163	java/lang/String:valueOf	(J)Ljava/lang/String;
        //     24: invokevirtual 166	java/io/BufferedWriter:write	(Ljava/lang/String;)V
        //     27: aload_3
        //     28: ifnull +103 -> 131
        //     31: aload_3
        //     32: invokevirtual 167	java/io/BufferedWriter:close	()V
        //     35: return
        //     36: astore 11
        //     38: ldc 8
        //     40: ldc 169
        //     42: aload 11
        //     44: invokestatic 150	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     47: pop
        //     48: goto -13 -> 35
        //     51: astore 4
        //     53: ldc 8
        //     55: ldc 169
        //     57: aload 4
        //     59: invokestatic 150	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     62: pop
        //     63: aload_2
        //     64: ifnull -29 -> 35
        //     67: aload_2
        //     68: invokevirtual 167	java/io/BufferedWriter:close	()V
        //     71: goto -36 -> 35
        //     74: astore 9
        //     76: ldc 8
        //     78: ldc 169
        //     80: aload 9
        //     82: invokestatic 150	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     85: pop
        //     86: goto -51 -> 35
        //     89: astore 5
        //     91: aload_2
        //     92: ifnull +7 -> 99
        //     95: aload_2
        //     96: invokevirtual 167	java/io/BufferedWriter:close	()V
        //     99: aload 5
        //     101: athrow
        //     102: astore 6
        //     104: ldc 8
        //     106: ldc 169
        //     108: aload 6
        //     110: invokestatic 150	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     113: pop
        //     114: goto -15 -> 99
        //     117: astore 5
        //     119: aload_3
        //     120: astore_2
        //     121: goto -30 -> 91
        //     124: astore 4
        //     126: aload_3
        //     127: astore_2
        //     128: goto -75 -> 53
        //     131: goto -96 -> 35
        //
        // Exception table:
        //     from	to	target	type
        //     31	35	36	java/io/IOException
        //     2	19	51	java/io/IOException
        //     67	71	74	java/io/IOException
        //     2	19	89	finally
        //     53	63	89	finally
        //     95	99	102	java/io/IOException
        //     19	27	117	finally
        //     19	27	124	java/io/IOException
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         com.miui.internal.app.ShutdownAlarm
 * JD-Core Version:        0.6.2
 */