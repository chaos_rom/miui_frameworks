package com.miui.internal.app;

import android.app.ActivityManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings.System;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import com.android.internal.app.AlertActivity;
import com.android.internal.app.AlertController.AlertParams;
import java.util.HashSet;

public class DisclaimerActivity extends AlertActivity
    implements DialogInterface.OnClickListener
{
    public static final String STOP_PACKAGE_NAME = "stop_package_name";
    private static HashSet<String> mPredefinePackages;
    static final String[] sPredefinedPackage;
    public String mStopPackageName;

    static
    {
        String[] arrayOfString1 = new String[4];
        arrayOfString1[0] = "com.miui.player";
        arrayOfString1[1] = "com.xiaomi.market";
        arrayOfString1[2] = "com.miui.backup";
        arrayOfString1[3] = "com.android.updater";
        sPredefinedPackage = arrayOfString1;
        mPredefinePackages = new HashSet(sPredefinedPackage.length);
        for (String str : sPredefinedPackage)
            mPredefinePackages.add(str);
    }

    private void forceStopPackage()
    {
        if (!TextUtils.isEmpty(this.mStopPackageName))
            ((ActivityManager)getSystemService("activity")).forceStopPackage(this.mStopPackageName);
    }

    public static boolean requestApproval(Context paramContext, String paramString)
    {
        int i = 1;
        if ((TextUtils.isEmpty(paramString)) || (!mPredefinePackages.contains(paramString)) || (Settings.System.getInt(paramContext.getContentResolver(), "confirm_miui_disclaimer", 0) == i))
            i = 0;
        return i;
    }

    public void onBackPressed()
    {
        setResult(0);
        forceStopPackage();
        finish();
        super.onBackPressed();
    }

    public void onClick(DialogInterface paramDialogInterface, int paramInt)
    {
        if (paramInt == -1)
        {
            Settings.System.putInt(getContentResolver(), "confirm_miui_disclaimer", 1);
            setResult(1);
        }
        while (true)
        {
            finish();
            return;
            if (paramInt == -2)
            {
                forceStopPackage();
                setResult(0);
            }
        }
    }

    public void onCreate(Bundle paramBundle)
    {
        super.onCreate(paramBundle);
        getWindow().setBackgroundDrawableResource(17170445);
        this.mStopPackageName = getIntent().getStringExtra("stop_package_name");
        String str1 = getString(101449779);
        String str2 = getString(101449780);
        String str3 = getString(101449781);
        String str4 = getString(101449782);
        String str5 = getString(101449783);
        SpannableStringBuilder localSpannableStringBuilder = new SpannableStringBuilder();
        localSpannableStringBuilder.append(str1 + str2 + str3 + str4 + str5);
        localSpannableStringBuilder.setSpan(new IntentSpan(MiuiLicenseActivity.URL_MIUI_USER_AGREEMENT), str1.length(), str1.length() + str2.length(), 33);
        localSpannableStringBuilder.setSpan(new IntentSpan(MiuiLicenseActivity.URL_MIUI_PRIVACY_POLICY), str1.length() + str2.length() + str3.length(), str1.length() + str2.length() + str3.length() + str4.length(), 33);
        AlertController.AlertParams localAlertParams = this.mAlertParams;
        localAlertParams.mIconId = 17301659;
        localAlertParams.mMessage = localSpannableStringBuilder;
        localAlertParams.mPositiveButtonText = getString(17039370);
        localAlertParams.mPositiveButtonListener = this;
        localAlertParams.mNegativeButtonText = getString(17039360);
        localAlertParams.mNegativeButtonListener = this;
        setupAlert();
        ((TextView)getWindow().findViewById(16908299)).setMovementMethod(LinkMovementMethod.getInstance());
    }

    class IntentSpan extends ClickableSpan
    {
        private String mUrl;

        public IntentSpan(String arg2)
        {
            Object localObject;
            this.mUrl = localObject;
        }

        public void onClick(View paramView)
        {
            Intent localIntent = new Intent("android.intent.action.MIUI_LICENSE");
            localIntent.putExtra(MiuiLicenseActivity.EXTRA_MIUI_DOC_URL, this.mUrl);
            DisclaimerActivity.this.startActivity(localIntent);
        }

        public void updateDrawState(TextPaint paramTextPaint)
        {
            paramTextPaint.setUnderlineText(true);
            paramTextPaint.setColor(-16776961);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         com.miui.internal.app.DisclaimerActivity
 * JD-Core Version:        0.6.2
 */