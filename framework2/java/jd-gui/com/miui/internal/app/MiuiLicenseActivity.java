package com.miui.internal.app;

import android.app.ActivityManagerNative;
import android.app.AlertDialog.Builder;
import android.app.IActivityManager;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.RemoteException;
import android.text.TextUtils;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.android.internal.app.AlertActivity;
import com.android.internal.app.AlertController;
import com.android.internal.app.AlertController.AlertParams;
import java.util.Locale;

public class MiuiLicenseActivity extends AlertActivity
    implements DialogInterface.OnCancelListener, DialogInterface.OnClickListener
{
    public static String CACHE_URL_MIUI_COPYRIGHT = "file:///system/etc/license/copyright.html";
    public static String CACHE_URL_MIUI_PRIVACY_POLICY = "file:///system/etc/license/privacy.html";
    public static String CACHE_URL_MIUI_USER_AGREEMENT = "file:///system/etc/license/eula.html";
    public static String CACHE_URL_MIUI_USER_MANUAL = "file:///system/etc/user_manual_%s/index.html";
    public static String EXTRA_MIUI_DOC_URL = "url";
    public static String URL_MIUI_COPYRIGHT = "http://www.miui.com/res/doc/copyright.html";
    public static String URL_MIUI_PRIVACY_POLICY = "http://product.xiaomi.com/policy/privacy.html";
    public static String URL_MIUI_USER_AGREEMENT = "http://product.xiaomi.com/policy/eula.html";
    public static String URL_MIUI_USER_MANUAL = "http://p.www.xiaomi.com/manual/index.html?lang=%s&miphone=%s";
    private String mCachePath;
    private String mTitle;
    private String mUrl;
    private WebView mWebView;

    private void showErrorAndFinish(String paramString)
    {
        this.mWebView.setVisibility(8);
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        Object[] arrayOfObject = new Object[1];
        arrayOfObject[0] = paramString;
        localBuilder.setMessage(getString(101449775, arrayOfObject)).setTitle(this.mTitle).setPositiveButton(17039370, this).setOnCancelListener(this).setCancelable(true).show();
    }

    public boolean isConnectivityActive()
    {
        ConnectivityManager localConnectivityManager = (ConnectivityManager)getSystemService("connectivity");
        if ((localConnectivityManager != null) && (localConnectivityManager.getActiveNetworkInfo() != null));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void onBackPressed()
    {
        if (this.mWebView.canGoBack())
            this.mWebView.goBack();
        while (true)
        {
            return;
            super.onBackPressed();
        }
    }

    public void onCancel(DialogInterface paramDialogInterface)
    {
        finish();
    }

    public void onClick(DialogInterface paramDialogInterface, int paramInt)
    {
        finish();
    }

    protected void onCreate(Bundle paramBundle)
    {
        super.onCreate(paramBundle);
        getWindow().setBackgroundDrawableResource(17170445);
        this.mUrl = getIntent().getStringExtra(EXTRA_MIUI_DOC_URL);
        if (URL_MIUI_COPYRIGHT.equals(this.mUrl))
        {
            this.mTitle = getString(101449776);
            this.mCachePath = CACHE_URL_MIUI_COPYRIGHT;
        }
        while (true)
        {
            this.mWebView = new WebView(this);
            this.mWebView.getSettings().setJavaScriptEnabled(true);
            this.mWebView.setWebViewClient(new WebViewClient()
            {
                public void onPageFinished(WebView paramAnonymousWebView, String paramAnonymousString)
                {
                    MiuiLicenseActivity.this.mAlert.setTitle(MiuiLicenseActivity.this.mTitle);
                }

                public void onReceivedError(WebView paramAnonymousWebView, int paramAnonymousInt, String paramAnonymousString1, String paramAnonymousString2)
                {
                    if (TextUtils.isEmpty(MiuiLicenseActivity.this.mCachePath))
                        MiuiLicenseActivity.this.showErrorAndFinish(paramAnonymousString2);
                    while (true)
                    {
                        return;
                        MiuiLicenseActivity.this.mWebView.loadUrl(MiuiLicenseActivity.this.mCachePath);
                    }
                }
            });
            label114: IActivityManager localIActivityManager;
            Object localObject;
            if (isConnectivityActive())
            {
                this.mWebView.loadUrl(this.mUrl);
                AlertController.AlertParams localAlertParams = this.mAlertParams;
                localAlertParams.mTitle = this.mTitle;
                localAlertParams.mView = this.mWebView;
                localAlertParams.mForceInverseBackground = true;
                setupAlert();
                return;
                if (URL_MIUI_PRIVACY_POLICY.equals(this.mUrl))
                {
                    this.mTitle = getString(101449778);
                    this.mCachePath = CACHE_URL_MIUI_PRIVACY_POLICY;
                    continue;
                }
                if (URL_MIUI_USER_AGREEMENT.equals(this.mUrl))
                {
                    this.mTitle = getString(101449777);
                    this.mCachePath = CACHE_URL_MIUI_USER_AGREEMENT;
                    continue;
                }
                if (!URL_MIUI_USER_MANUAL.equals(this.mUrl))
                    continue;
                this.mTitle = getString(101450240);
                String str1 = CACHE_URL_MIUI_USER_MANUAL;
                Object[] arrayOfObject1 = new Object[1];
                arrayOfObject1[0] = Build.DEVICE;
                this.mCachePath = String.format(str1, arrayOfObject1);
                localIActivityManager = ActivityManagerNative.getDefault();
                localObject = "zh_CN";
            }
            try
            {
                String str3 = localIActivityManager.getConfiguration().locale.toString();
                localObject = str3;
                label291: String str2 = this.mUrl;
                Object[] arrayOfObject2 = new Object[2];
                arrayOfObject2[0] = localObject;
                arrayOfObject2[1] = Build.DEVICE;
                this.mUrl = String.format(str2, arrayOfObject2);
                continue;
                if (TextUtils.isEmpty(this.mCachePath))
                {
                    showErrorAndFinish(this.mUrl);
                    break label114;
                }
                this.mWebView.loadUrl(this.mCachePath);
            }
            catch (RemoteException localRemoteException)
            {
                break label291;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         com.miui.internal.app.MiuiLicenseActivity
 * JD-Core Version:        0.6.2
 */