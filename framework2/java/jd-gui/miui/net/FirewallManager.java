package miui.net;

import android.app.Activity;
import android.app.ActivityManagerNative;
import android.app.IActivityManager;
import android.app.IApplicationThread;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.provider.Settings.Secure;
import android.text.TextUtils;
import com.android.internal.telephony.ApnSetting;

public class FirewallManager
{
    public static final int ALARM_BOOT_COMPLETED_FLAG = 1;
    private static final boolean DEBUG = true;
    public static final int DRIVE_MODE_FLAG = 4;
    private static final String LOG_TAG = "FirewallService";
    public static final String SERVICE_NAME = "miui.Firewall";
    public static final int SIM_CARD_SYNC_COMPLETED_FLAG = 2;
    private static FirewallManager sInstance = new FirewallManager();
    private IFirewall mService;

    private FirewallManager()
    {
        ensureService();
    }

    public static void checkAccessControl(Activity paramActivity, ContentResolver paramContentResolver, String paramString1, PackageManager paramPackageManager, IApplicationThread paramIApplicationThread, IBinder paramIBinder, String paramString2)
    {
        if (paramActivity != null);
        while (true)
        {
            return;
            if (1 != Settings.Secure.getInt(paramContentResolver, "access_control_lock_enabled", 0))
                continue;
            try
            {
                ApplicationInfo localApplicationInfo2 = paramPackageManager.getApplicationInfo(paramString1, 0);
                localApplicationInfo1 = localApplicationInfo2;
                if ((localApplicationInfo1 == null) || ((0x80000000 & localApplicationInfo1.flags) != -2147483648) || (getInstance().checkAccessControlPass(paramString1)))
                    continue;
                Intent localIntent = new Intent("android.app.action.CONFIRM_ACCESS_CONTROL");
                localIntent.putExtra("android.intent.extra.shortcut.NAME", paramString1);
                try
                {
                    ActivityManagerNative.getDefault().startActivity(paramIApplicationThread, localIntent, localIntent.resolveTypeIfNeeded(paramContentResolver), paramIBinder, paramString2, -1, 0, null, null, null);
                }
                catch (RemoteException localRemoteException)
                {
                }
            }
            catch (PackageManager.NameNotFoundException localNameNotFoundException)
            {
                while (true)
                    ApplicationInfo localApplicationInfo1 = null;
            }
        }
    }

    public static ApnSetting decodeApnSetting(String paramString)
    {
        ApnSetting localApnSetting;
        if (TextUtils.isEmpty(paramString))
            localApnSetting = null;
        while (true)
        {
            return localApnSetting;
            String[] arrayOfString = paramString.split("\\s*,\\s*");
            if (arrayOfString.length < 16)
                localApnSetting = null;
            else
                localApnSetting = new ApnSetting(Integer.parseInt(arrayOfString[0]), arrayOfString[1], arrayOfString[2], arrayOfString[3], arrayOfString[4], arrayOfString[5], arrayOfString[6], arrayOfString[7], arrayOfString[8], arrayOfString[9], arrayOfString[10], Integer.parseInt(arrayOfString[11]), arrayOfString[12].split("\\s*\\|\\s*"), arrayOfString[13], arrayOfString[14], Boolean.parseBoolean(arrayOfString[15]), Integer.parseInt(arrayOfString[16]));
        }
    }

    public static String encodeApnSetting(ApnSetting paramApnSetting)
    {
        if (paramApnSetting == null);
        for (String str = null; ; str = paramApnSetting.id + ',' + paramApnSetting.numeric + ',' + paramApnSetting.carrier + ',' + paramApnSetting.apn + ',' + paramApnSetting.proxy + ',' + paramApnSetting.port + ',' + paramApnSetting.mmsc + ',' + paramApnSetting.mmsProxy + ',' + paramApnSetting.mmsPort + ',' + paramApnSetting.user + ',' + paramApnSetting.password + ',' + paramApnSetting.authType + ',' + TextUtils.join("|", paramApnSetting.types) + ',' + paramApnSetting.protocol + ',' + paramApnSetting.roamingProtocol + ',' + paramApnSetting.carrierEnabled + ',' + paramApnSetting.bearer)
            return str;
    }

    private void ensureService()
    {
        if (this.mService == null)
            try
            {
                this.mService = IFirewall.Stub.asInterface(ServiceManager.getService("miui.Firewall"));
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
    }

    public static FirewallManager getInstance()
    {
        return sInstance;
    }

    public static boolean isAccessControlProtected(Context paramContext, String paramString)
    {
        boolean bool = false;
        try
        {
            ApplicationInfo localApplicationInfo = paramContext.getPackageManager().getApplicationInfo(paramString, 0);
            if ((localApplicationInfo != null) && ((0x80000000 & localApplicationInfo.flags) == -2147483648))
                bool = true;
            label33: return bool;
        }
        catch (PackageManager.NameNotFoundException localNameNotFoundException)
        {
            break label33;
        }
    }

    public void addAccessControlPass(String paramString)
    {
        try
        {
            ensureService();
            if (this.mService != null)
                this.mService.addAccessControlPass(paramString);
            label21: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label21;
        }
    }

    public void addOneShotFlag(int paramInt)
    {
        try
        {
            ensureService();
            if ((this.mService != null) && (paramInt > 0))
                this.mService.addOneShotFlag(paramInt);
            label25: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label25;
        }
    }

    public boolean checkAccessControlPass(String paramString)
    {
        boolean bool1 = false;
        try
        {
            ensureService();
            if (this.mService != null)
            {
                boolean bool2 = this.mService.checkAccessControlPass(paramString);
                bool1 = bool2;
            }
            label28: return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            break label28;
        }
    }

    public boolean getOneShotFlag(int paramInt)
    {
        try
        {
            ensureService();
            if ((this.mService != null) && (paramInt > 0))
            {
                boolean bool2 = this.mService.getOneShotFlag(paramInt);
                bool1 = bool2;
                return bool1;
            }
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool1 = false;
        }
    }

    public void onDataConnected(int paramInt, String paramString1, String paramString2)
    {
        try
        {
            if (!TextUtils.isEmpty(paramString1))
            {
                ensureService();
                if (this.mService != null)
                    this.mService.onDataConnected(paramInt, paramString1, paramString2);
            }
            label30: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label30;
        }
    }

    public void onDataDisconnected(int paramInt, String paramString)
    {
        try
        {
            if (!TextUtils.isEmpty(paramString))
            {
                ensureService();
                if (this.mService != null)
                    this.mService.onDataDisconnected(paramInt, paramString);
            }
            label29: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label29;
        }
    }

    public void onStartUsingNetworkFeature(int paramInt1, int paramInt2, int paramInt3)
    {
        try
        {
            ensureService();
            if (this.mService != null)
                this.mService.onStartUsingNetworkFeature(paramInt1, paramInt2, paramInt3);
            label23: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label23;
        }
    }

    public void onStopUsingNetworkFeature(int paramInt1, int paramInt2, int paramInt3)
    {
        try
        {
            ensureService();
            if (this.mService != null)
                this.mService.onStopUsingNetworkFeature(paramInt1, paramInt2, paramInt3);
            label23: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label23;
        }
    }

    public void removeAccessControlPass(String paramString)
    {
        try
        {
            ensureService();
            if (this.mService != null)
                this.mService.removeAccessControlPass(paramString);
            label21: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label21;
        }
    }

    public void removeOneShotFlag(int paramInt)
    {
        try
        {
            ensureService();
            if ((this.mService != null) && (paramInt > 0))
                this.mService.removeOneShotFlag(paramInt);
            label25: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label25;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.net.FirewallManager
 * JD-Core Version:        0.6.2
 */