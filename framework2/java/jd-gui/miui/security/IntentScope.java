package miui.security;

import android.content.Context;
import android.content.Intent;

public class IntentScope
{
    public static final String PACKAGE_NAME_PHONE = "com.android.phone";

    public static Intent processIntentScope(Context paramContext, Intent paramIntent, String paramString)
    {
        if (paramIntent != null)
            paramIntent.setPackage(paramString);
        return paramIntent;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.security.IntentScope
 * JD-Core Version:        0.6.2
 */