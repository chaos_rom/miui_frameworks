package miui.security;

import android.content.Context;
import android.os.Environment;
import android.os.FileObserver;
import android.util.Log;
import com.android.internal.widget.LockPatternUtils;
import com.android.internal.widget.LockPatternView.Cell;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class MiuiLockPatternUtils extends LockPatternUtils
{
    private static final String LOCK_AC_FILE = "access_control.key";
    private static final String SYSTEM_DIRECTORY = "/system/";
    private static final String TAG = "MiuiLockPatternUtils";
    private static final AtomicBoolean sHaveNonZeroACFile = new AtomicBoolean(false);
    private static String sLockACFilename;
    private static FileObserver sPasswordObserver;

    public MiuiLockPatternUtils(Context paramContext)
    {
        super(paramContext);
        String str;
        AtomicBoolean localAtomicBoolean;
        if (sLockACFilename == null)
        {
            str = Environment.getDataDirectory().getAbsolutePath() + "/system/";
            sLockACFilename = str + "access_control.key";
            localAtomicBoolean = sHaveNonZeroACFile;
            if (new File(sLockACFilename).length() <= 0L)
                break label110;
        }
        label110: for (boolean bool = true; ; bool = false)
        {
            localAtomicBoolean.set(bool);
            sPasswordObserver = new PasswordFileObserver(str, 904);
            sPasswordObserver.startWatching();
            return;
        }
    }

    public boolean checkAccessControl(List<LockPatternView.Cell> paramList)
    {
        boolean bool1 = true;
        try
        {
            RandomAccessFile localRandomAccessFile = new RandomAccessFile(sLockACFilename, "r");
            byte[] arrayOfByte = new byte[(int)localRandomAccessFile.length()];
            int i = localRandomAccessFile.read(arrayOfByte, 0, arrayOfByte.length);
            localRandomAccessFile.close();
            if (i > 0)
            {
                boolean bool2 = Arrays.equals(arrayOfByte, LockPatternUtils.callPatternToHash(paramList));
                bool1 = bool2;
            }
        }
        catch (FileNotFoundException localFileNotFoundException)
        {
        }
        catch (IOException localIOException)
        {
        }
        return bool1;
    }

    public void saveACLockPattern(List<LockPatternView.Cell> paramList)
    {
        byte[] arrayOfByte = LockPatternUtils.callPatternToHash(paramList);
        try
        {
            RandomAccessFile localRandomAccessFile = new RandomAccessFile(sLockACFilename, "rw");
            if (paramList == null)
                localRandomAccessFile.setLength(0L);
            while (true)
            {
                localRandomAccessFile.close();
                break;
                localRandomAccessFile.write(arrayOfByte, 0, arrayOfByte.length);
            }
        }
        catch (FileNotFoundException localFileNotFoundException)
        {
            Log.e("MiuiLockPatternUtils", "Unable to save lock pattern to " + sLockACFilename);
        }
        catch (IOException localIOException)
        {
            Log.e("MiuiLockPatternUtils", "Unable to save lock pattern to " + sLockACFilename);
        }
    }

    public boolean savedAccessControlExists()
    {
        return sHaveNonZeroACFile.get();
    }

    private static class PasswordFileObserver extends FileObserver
    {
        public PasswordFileObserver(String paramString, int paramInt)
        {
            super(paramInt);
        }

        public void onEvent(int paramInt, String paramString)
        {
            AtomicBoolean localAtomicBoolean;
            if ("access_control.key".equals(paramString))
            {
                Log.d("MiuiLockPatternUtils", "access control password file changed");
                localAtomicBoolean = MiuiLockPatternUtils.sHaveNonZeroACFile;
                if (new File(MiuiLockPatternUtils.sLockACFilename).length() <= 0L)
                    break label51;
            }
            label51: for (boolean bool = true; ; bool = false)
            {
                localAtomicBoolean.set(bool);
                return;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.security.MiuiLockPatternUtils
 * JD-Core Version:        0.6.2
 */