package miui.provider;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import java.util.ArrayList;
import java.util.List;

public class Userbook
{
    public static final String AUTHORITY = "userbook";
    public static final Uri CONTENT_URI = Uri.parse("content://userbook/feature");
    private static final String SELECTION = "package=? AND feature=?";

    public static int delete(ContentResolver paramContentResolver, String paramString1, String paramString2)
    {
        Uri localUri = CONTENT_URI;
        String[] arrayOfString = new String[2];
        arrayOfString[0] = paramString1;
        arrayOfString[1] = paramString2;
        return paramContentResolver.delete(localUri, "package=? AND feature=?", arrayOfString);
    }

    public static boolean getAchieved(ContentResolver paramContentResolver, String paramString1, String paramString2, boolean paramBoolean)
    {
        return getState(paramContentResolver, paramString1, paramString2, paramBoolean, "achieved");
    }

    public static List<String> getPackageAchievedFeatures(ContentResolver paramContentResolver, String paramString, boolean paramBoolean)
    {
        boolean bool = true;
        StringBuilder localStringBuilder = new StringBuilder().append("achieved=");
        if (paramBoolean == bool);
        while (true)
        {
            return getPackageFeatures(paramContentResolver, paramString, bool);
            bool = false;
        }
    }

    private static List<String> getPackageFeatures(ContentResolver paramContentResolver, String paramString1, String paramString2)
    {
        String str = paramString2 + " AND package='" + paramString1 + "'";
        Uri localUri = CONTENT_URI;
        String[] arrayOfString = new String[1];
        arrayOfString[0] = "feature";
        Cursor localCursor = paramContentResolver.query(localUri, arrayOfString, str, null, null);
        ArrayList localArrayList = new ArrayList();
        if (localCursor != null)
        {
            if (localCursor.moveToFirst())
                do
                    localArrayList.add(localCursor.getString(0));
                while (localCursor.moveToNext());
            localCursor.close();
        }
        return localArrayList;
    }

    public static List<String> getPackageReadFeatures(ContentResolver paramContentResolver, String paramString, boolean paramBoolean)
    {
        boolean bool = true;
        StringBuilder localStringBuilder = new StringBuilder().append("read=");
        if (paramBoolean == bool);
        while (true)
        {
            return getPackageFeatures(paramContentResolver, paramString, bool);
            bool = false;
        }
    }

    public static boolean getRead(ContentResolver paramContentResolver, String paramString1, String paramString2, boolean paramBoolean)
    {
        return getState(paramContentResolver, paramString1, paramString2, paramBoolean, "read");
    }

    private static boolean getState(ContentResolver paramContentResolver, String paramString1, String paramString2, boolean paramBoolean, String paramString3)
    {
        boolean bool = paramBoolean;
        Uri localUri = CONTENT_URI;
        String[] arrayOfString1 = new String[1];
        arrayOfString1[0] = paramString3;
        String[] arrayOfString2 = new String[2];
        arrayOfString2[0] = paramString1;
        arrayOfString2[1] = paramString2;
        Cursor localCursor = paramContentResolver.query(localUri, arrayOfString1, "package=? AND feature=?", arrayOfString2, null);
        if (localCursor != null)
            if (localCursor.moveToFirst())
                if (localCursor.getInt(0) <= 0)
                    break label90;
        label90: for (bool = true; ; bool = false)
        {
            localCursor.close();
            return bool;
        }
    }

    private static void insertOrUpdate(ContentResolver paramContentResolver, String paramString1, String paramString2, boolean paramBoolean, String paramString3)
    {
        Uri localUri1 = CONTENT_URI;
        String[] arrayOfString1 = new String[1];
        arrayOfString1[0] = paramString3;
        String[] arrayOfString2 = new String[2];
        arrayOfString2[0] = paramString1;
        arrayOfString2[1] = paramString2;
        Cursor localCursor = paramContentResolver.query(localUri1, arrayOfString1, "package=? AND feature=?", arrayOfString2, null);
        boolean bool = false;
        if (localCursor != null)
        {
            bool = localCursor.moveToFirst();
            localCursor.close();
        }
        ContentValues localContentValues = new ContentValues();
        int i;
        if (paramBoolean == true)
        {
            i = 1;
            localContentValues.put(paramString3, Integer.valueOf(i));
            if (!bool)
                break label147;
            Uri localUri2 = CONTENT_URI;
            String[] arrayOfString3 = new String[2];
            arrayOfString3[0] = paramString1;
            arrayOfString3[1] = paramString2;
            paramContentResolver.update(localUri2, localContentValues, "package=? AND feature=?", arrayOfString3);
        }
        while (true)
        {
            return;
            i = 0;
            break;
            label147: localContentValues.put("package", paramString1);
            localContentValues.put("feature", paramString2);
            paramContentResolver.insert(CONTENT_URI, localContentValues);
        }
    }

    public static void setAchieved(ContentResolver paramContentResolver, String paramString1, String paramString2, boolean paramBoolean)
    {
        insertOrUpdate(paramContentResolver, paramString1, paramString2, paramBoolean, "achieved");
    }

    public static void setRead(ContentResolver paramContentResolver, String paramString1, String paramString2, boolean paramBoolean)
    {
        insertOrUpdate(paramContentResolver, paramString1, paramString2, paramBoolean, "read");
    }

    public static abstract interface Columns
    {
        public static final String ACHIEVED = "achieved";
        public static final String DATA1 = "data1";
        public static final String DATA2 = "data2";
        public static final String DATA3 = "data3";
        public static final String FEATURE = "feature";
        public static final String ID = "_id";
        public static final String PACKAGE = "package";
        public static final String READ = "read";
        public static final String VERSION = "version";
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.provider.Userbook
 * JD-Core Version:        0.6.2
 */