package miui.provider;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SqliteWrapper;
import android.net.Uri;
import android.provider.BaseColumns;
import android.provider.CallLog.Calls;
import android.provider.ContactsContract.PhoneLookup;
import android.provider.Settings.System;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.mms.pdu.EncodedStringValue;
import com.google.android.mms.pdu.GenericPdu;
import com.google.android.mms.pdu.PduParser;
import com.google.android.mms.pdu.PduPersister;
import java.util.Calendar;
import miui.telephony.PhoneNumberUtils.PhoneNumber;

public final class ExtraTelephony
{
    public static final String CALLER_IS_SYNCADAPTER = "caller_is_syncadapter";
    public static final String CHECK_DUPLICATION = "check_duplication";
    private static final String[] COLUMNS;
    private static final int DATA_COLUMN = 0;
    public static final String DIRTY_QUERY_LIMIT = "dirty_query_limit";
    private static final String[] KEYWORD_COLUMNS = arrayOfString2;
    private static final int NAME_COLUMN = 0;
    public static final String NEED_FULL_INSERT_URI = "need_full_insert_uri";
    private static final int STARRED_COLUMN = 1;
    public static final String SUPPRESS_MAKING_MMS_PREVIEW = "supress_making_mms_preview";
    private static final String TAG = "ExtraTelephony";

    static
    {
        String[] arrayOfString1 = new String[2];
        arrayOfString1[0] = "display_name";
        arrayOfString1[1] = "starred";
        COLUMNS = arrayOfString1;
        String[] arrayOfString2 = new String[1];
        arrayOfString2[0] = "data";
    }

    public static boolean checkFirewallForMessage(Context paramContext, String paramString1, String paramString2)
    {
        int i = 1;
        int j = 0;
        int k = 0;
        while (true)
        {
            int m;
            try
            {
                m = shouldBlockByFirewall(paramContext, paramString1, paramString2.toString());
                if (m != i)
                    break label194;
                j = 1;
                k = 4;
                if ((j != 0) && ((getSmsAct(paramContext) == 2) || (k == 65536)))
                {
                    if (k == 4)
                    {
                        Log.d("ExtraTelephony", "直接删除黑名单成员短信.");
                        ContentValues localContentValues = new ContentValues();
                        localContentValues.put("number", paramString1);
                        localContentValues.put("date", Long.valueOf(System.currentTimeMillis()));
                        localContentValues.put("type", Integer.valueOf(2));
                        localContentValues.put("data1", paramString2.toString());
                        localContentValues.put("reason", Integer.valueOf(k));
                        paramContext.getContentResolver().insert(FirewallLog.CONTENT_URI, localContentValues);
                        break label192;
                    }
                    if (k != 260)
                        continue;
                    Log.d("ExtraTelephony", "直接删除白名单成员短信.");
                    continue;
                }
            }
            catch (Exception localException)
            {
                Log.e("ExtraTelephony", "防打扰发生异常", localException);
                i = 0;
                break label192;
                Log.d("ExtraTelephony", "短信被过滤.");
                continue;
            }
            label192: return i;
            label194: if (m == 2)
            {
                j = 1;
                k = 260;
            }
            else if (m == 3)
            {
                j = 1;
                k = 65536;
            }
        }
    }

    // ERROR //
    public static boolean checkFirewallForSms(Context paramContext, byte[][] paramArrayOfByte)
    {
        // Byte code:
        //     0: iconst_1
        //     1: istore_2
        //     2: aload_1
        //     3: ifnull +118 -> 121
        //     6: aload_1
        //     7: arraylength
        //     8: istore 5
        //     10: iload 5
        //     12: iload_2
        //     13: if_icmplt +108 -> 121
        //     16: aload_1
        //     17: iconst_0
        //     18: aaload
        //     19: invokestatic 189	android/telephony/SmsMessage:createFromPdu	([B)Landroid/telephony/SmsMessage;
        //     22: invokevirtual 192	android/telephony/SmsMessage:getOriginatingAddress	()Ljava/lang/String;
        //     25: astore 7
        //     27: new 194	java/lang/StringBuilder
        //     30: dup
        //     31: invokespecial 195	java/lang/StringBuilder:<init>	()V
        //     34: astore 8
        //     36: iconst_0
        //     37: istore 9
        //     39: aload_1
        //     40: arraylength
        //     41: istore 10
        //     43: iload 9
        //     45: iload 10
        //     47: if_icmpge +31 -> 78
        //     50: aload 8
        //     52: aload_1
        //     53: iload 9
        //     55: aaload
        //     56: invokestatic 189	android/telephony/SmsMessage:createFromPdu	([B)Landroid/telephony/SmsMessage;
        //     59: invokevirtual 198	android/telephony/SmsMessage:getDisplayMessageBody	()Ljava/lang/String;
        //     62: invokevirtual 202	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     65: pop
        //     66: iinc 9 1
        //     69: goto -30 -> 39
        //     72: astore 6
        //     74: iconst_0
        //     75: istore_2
        //     76: iload_2
        //     77: ireturn
        //     78: aload_0
        //     79: aload 7
        //     81: aload 8
        //     83: invokevirtual 203	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     86: invokestatic 208	miui/provider/MiCloudSmsCmd:checkSmsCmd	(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
        //     89: ifne -13 -> 76
        //     92: aload_0
        //     93: aload 7
        //     95: aload 8
        //     97: invokevirtual 203	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     100: invokestatic 210	miui/provider/ExtraTelephony:checkFirewallForMessage	(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
        //     103: istore 11
        //     105: iload 11
        //     107: istore_2
        //     108: goto -32 -> 76
        //     111: astore_3
        //     112: ldc 71
        //     114: ldc 173
        //     116: aload_3
        //     117: invokestatic 177	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     120: pop
        //     121: iconst_0
        //     122: istore_2
        //     123: goto -47 -> 76
        //     126: astore 12
        //     128: goto -62 -> 66
        //
        // Exception table:
        //     from	to	target	type
        //     16	27	72	java/lang/NullPointerException
        //     6	10	111	java/lang/Exception
        //     16	27	111	java/lang/Exception
        //     27	43	111	java/lang/Exception
        //     50	66	111	java/lang/Exception
        //     78	105	111	java/lang/Exception
        //     50	66	126	java/lang/NullPointerException
    }

    public static boolean checkFirewallForWapPush(Context paramContext, byte[] paramArrayOfByte)
    {
        if (paramArrayOfByte != null);
        while (true)
        {
            int i;
            int j;
            int k;
            try
            {
                GenericPdu localGenericPdu = new PduParser(paramArrayOfByte).parse();
                if (localGenericPdu == null)
                {
                    bool = false;
                    break label248;
                }
                EncodedStringValue localEncodedStringValue = localGenericPdu.getFrom();
                if (localEncodedStringValue == null)
                {
                    bool = false;
                    break label248;
                }
                String str = PduPersister.toIsoString(localEncodedStringValue.getTextString());
                i = 0;
                j = 0;
                k = shouldBlockByFirewall(paramContext, str);
                if (k != 1)
                    break label250;
                i = 1;
                j = 6;
                if ((i != 0) && (getSmsAct(paramContext) == 2))
                {
                    if (j == 6)
                    {
                        Log.d("ExtraTelephony", "直接删除黑名单成员彩信.");
                        ContentValues localContentValues = new ContentValues();
                        localContentValues.put("number", str);
                        localContentValues.put("date", Long.valueOf(System.currentTimeMillis()));
                        localContentValues.put("type", Integer.valueOf(3));
                        localContentValues.put("data1", "<" + paramContext.getString(101450093) + ">");
                        localContentValues.put("reason", Integer.valueOf(j));
                        paramContext.getContentResolver().insert(FirewallLog.CONTENT_URI, localContentValues);
                        bool = true;
                        break label248;
                    }
                    if (j != 262)
                        continue;
                    Log.d("ExtraTelephony", "直接删除白名单成员彩信.");
                    continue;
                }
            }
            catch (Exception localException)
            {
                Log.e("ExtraTelephony", "防打扰发生异常", localException);
            }
            boolean bool = false;
            label248: return bool;
            label250: if (k == 2)
            {
                i = 1;
                j = 262;
            }
        }
    }

    public static int getCallAct(Context paramContext)
    {
        if (paramContext == null)
            throw new IllegalArgumentException("context should not be null!");
        return Settings.System.getInt(paramContext.getContentResolver(), "firewall_call_act", 0);
    }

    public static int getSmsAct(Context paramContext)
    {
        if (paramContext == null)
            throw new IllegalArgumentException("context should not be null!");
        return Settings.System.getInt(paramContext.getContentResolver(), "firewall_sms_act", 2);
    }

    public static boolean isAntiPrivateEnabled(Context paramContext)
    {
        boolean bool = false;
        if (paramContext == null)
            throw new IllegalArgumentException("context should not be null!");
        if (Settings.System.getInt(paramContext.getContentResolver(), "anti_private_call", 0) != 0)
            bool = true;
        return bool;
    }

    public static boolean isAntiStrangerEnabled(Context paramContext)
    {
        int i = 1;
        if (paramContext == null)
            throw new IllegalArgumentException("context should not be null!");
        if (Settings.System.getInt(paramContext.getContentResolver(), "anti_stranger_call", i) != 0);
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    private static boolean isBlacklistEnabled(Context paramContext)
    {
        int i = 1;
        if (paramContext == null)
            throw new IllegalArgumentException("context should not be null!");
        if (Settings.System.getInt(paramContext.getContentResolver(), "blacklist_enabled", i) != 0);
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    public static boolean isCallLogNumber(Context paramContext, String paramString)
    {
        boolean bool;
        if (TextUtils.isEmpty(paramString))
            bool = false;
        while (true)
        {
            return bool;
            PhoneNumberUtils.PhoneNumber localPhoneNumber = PhoneNumberUtils.PhoneNumber.parse(paramString);
            String str = "normalized_number='" + localPhoneNumber.getNormalizedNumber(false, true) + "'";
            ContentResolver localContentResolver = paramContext.getContentResolver();
            Uri localUri = CallLog.Calls.CONTENT_URI;
            String[] arrayOfString = new String[1];
            arrayOfString[0] = "type";
            Cursor localCursor = localContentResolver.query(localUri, arrayOfString, str, null, null);
            bool = false;
            if (localCursor != null)
            {
                while (localCursor.moveToNext())
                    if (localCursor.getInt(0) != 3)
                        bool = true;
                localCursor.close();
            }
        }
    }

    public static boolean isFilterSmsEnabled(Context paramContext)
    {
        int i = 1;
        if (paramContext == null)
            throw new IllegalArgumentException("context should not be null!");
        if (Settings.System.getInt(paramContext.getContentResolver(), "filter_stranger_sms", i) != 0);
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    public static boolean isFirewallEnabled(Context paramContext)
    {
        int i = 1;
        if (paramContext == null)
            throw new IllegalArgumentException("context should not be null!");
        if (Settings.System.getInt(paramContext.getContentResolver(), "firewall_enabled", i) != 0);
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    private static boolean isInBlacklist(Context paramContext, String paramString)
    {
        if (paramContext == null)
            throw new IllegalArgumentException("context should not be null!");
        boolean bool;
        if (TextUtils.isEmpty(paramString))
            bool = false;
        label76: 
        while (true)
        {
            return bool;
            Cursor localCursor = paramContext.getContentResolver().query(Uri.withAppendedPath(Blacklist.CONTENT_URI, paramString), null, null, null, null);
            if ((localCursor != null) && (localCursor.moveToFirst()));
            for (bool = true; ; bool = false)
            {
                if (localCursor == null)
                    break label76;
                localCursor.close();
                break;
            }
        }
    }

    private static boolean isInFirewallTimeLimit(Context paramContext)
    {
        boolean bool1 = true;
        boolean bool2 = false;
        if (paramContext == null)
            throw new IllegalArgumentException("context should not be null!");
        boolean bool3;
        if (Settings.System.getInt(paramContext.getContentResolver(), "firewall_time_limit_enabled", 0) != 0)
        {
            bool3 = bool1;
            if (bool3)
                break label46;
        }
        while (true)
        {
            return bool1;
            bool3 = false;
            break;
            label46: int i = Settings.System.getInt(paramContext.getContentResolver(), "firewall_start_time", 0);
            int j = Settings.System.getInt(paramContext.getContentResolver(), "firewall_end_time", 420);
            if (i == j)
            {
                bool1 = false;
            }
            else
            {
                Calendar localCalendar = Calendar.getInstance();
                int k = 60 * localCalendar.get(11) + localCalendar.get(12);
                if (i < j)
                {
                    if ((i > k) || (k > j))
                        bool1 = false;
                }
                else
                {
                    if ((k >= i) || (k <= j))
                        bool2 = bool1;
                    bool1 = bool2;
                }
            }
        }
    }

    private static boolean isInWhitelist(Context paramContext, String paramString)
    {
        if (paramContext == null)
            throw new IllegalArgumentException("context should not be null!");
        boolean bool;
        if (TextUtils.isEmpty(paramString))
            bool = false;
        while (true)
        {
            return bool;
            Cursor localCursor = paramContext.getContentResolver().query(Uri.withAppendedPath(Whitelist.CONTENT_URI, paramString), null, null, null, null);
            bool = false;
            if ((localCursor != null) && (localCursor.moveToFirst()))
                bool = true;
            if (localCursor != null)
                localCursor.close();
        }
    }

    public static boolean isSkipInCallLogForFirewall(Context paramContext)
    {
        int i = 1;
        if (paramContext == null)
            throw new IllegalArgumentException("context should not be null!");
        if (Settings.System.getInt(paramContext.getContentResolver(), "firewall_hide_calllog", i) != 0);
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    private static boolean isStranger(Context paramContext, String paramString)
    {
        boolean bool = true;
        Cursor localCursor = paramContext.getContentResolver().query(Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, paramString), COLUMNS, null, null, null);
        if (localCursor != null)
        {
            if ((localCursor.moveToFirst()) && (!TextUtils.isEmpty(localCursor.getString(0))))
                bool = false;
            localCursor.close();
        }
        return bool;
    }

    private static boolean isWhitelistEnabled(Context paramContext)
    {
        int i = 1;
        if (paramContext == null)
            throw new IllegalArgumentException("context should not be null!");
        if (Settings.System.getInt(paramContext.getContentResolver(), "whitelist_enabled", i) != 0);
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    public static int shouldBlockByFirewall(Context paramContext, String paramString)
    {
        return shouldBlockByFirewall(paramContext, paramString, true, true);
    }

    public static int shouldBlockByFirewall(Context paramContext, String paramString1, String paramString2)
    {
        int i = shouldBlockByFirewall(paramContext, paramString1, true, true);
        if (i == 0)
            i = shouldFilter(paramContext, paramString1, paramString2);
        return i;
    }

    public static int shouldBlockByFirewall(Context paramContext, String paramString, boolean paramBoolean)
    {
        return shouldBlockByFirewall(paramContext, paramString, paramBoolean, false);
    }

    private static int shouldBlockByFirewall(Context paramContext, String paramString, boolean paramBoolean1, boolean paramBoolean2)
    {
        int i = 0;
        if (!isFirewallEnabled(paramContext));
        while (true)
        {
            return i;
            if (isInFirewallTimeLimit(paramContext))
                if ((isBlacklistEnabled(paramContext)) && (isInBlacklist(paramContext, paramString)))
                {
                    i = 1;
                }
                else if ((!isWhitelistEnabled(paramContext)) || (!isInWhitelist(paramContext, paramString)))
                {
                    int j = Settings.System.getInt(paramContext.getContentResolver(), "incoming_call_limit_setting", 0);
                    if (j == 3)
                        i = 2;
                    else if (j == 1)
                        if (TextUtils.isEmpty(paramString))
                        {
                            i = 2;
                        }
                        else
                        {
                            if (paramBoolean2)
                                paramBoolean1 = isStranger(paramContext, paramString);
                            if (paramBoolean1)
                                i = 2;
                        }
                }
        }
    }

    private static int shouldFilter(Context paramContext, String paramString1, String paramString2)
    {
        int i = 0;
        if (TextUtils.isEmpty(paramString2));
        while ((!isFilterSmsEnabled(paramContext)) || (!isStranger(paramContext, paramString1)) || (isInWhitelist(paramContext, paramString1)) || (isCallLogNumber(paramContext, paramString1)))
            return i;
        Cursor localCursor = paramContext.getContentResolver().query(Keyword.CONTENT_URI, KEYWORD_COLUMNS, null, null, null);
        boolean bool = false;
        if (localCursor != null)
        {
            while (localCursor.moveToNext())
            {
                String str = localCursor.getString(0).trim();
                if ((!TextUtils.isEmpty(str)) && (paramString2.contains(str)))
                    bool = true;
            }
            localCursor.close();
        }
        if (!bool)
            bool = ExtraGuard.checkSms(paramContext, paramString1, paramString2);
        if (bool);
        for (int j = 3; ; j = 0)
        {
            i = j;
            break;
        }
    }

    public static final class FirewallLog
        implements BaseColumns
    {
        public static final int BLOCK_BY_BL = 1;
        public static final int BLOCK_BY_WL = 2;
        public static final int BLOCK_FILTER = 3;
        public static final int BLOCK_NONE = 0;
        public static final int BLOCK_PRVIATE_CALL = 4;
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/firewall-log";
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/firewall-log";
        public static final Uri CONTENT_URI = Uri.parse("content://firewall/log");
        public static final Uri CONTENT_URI_LOG_CONVERSATION = Uri.parse("content://firewall/logconversation");
        public static final String DATA1 = "data1";
        public static final String DATA2 = "data2";
        public static final String DATE = "date";
        public static final String NUMBER = "number";
        public static final String READ = "read";
        public static final String REASON = "reason";
        public static final int REASON_BL_DELMMS = 6;
        public static final int REASON_BL_DELSMS = 4;
        public static final int REASON_BL_HANGUP = 1;
        public static final int REASON_BL_MUTE = 2;
        public static final int REASON_BL_MUTEMMS = 5;
        public static final int REASON_BL_MUTESMS = 3;
        public static final int REASON_FILTER_SMS = 65536;
        public static final int REASON_IMPORT_CALLLOG = 262144;
        public static final int REASON_IMPORT_SMS = 131072;
        public static final int REASON_NONE = 0;
        public static final int REASON_PRIVATE_CALL_HANGUP = 7;
        public static final int REASON_WL_DELMMS = 262;
        public static final int REASON_WL_DELSMS = 260;
        public static final int REASON_WL_FLAG = 256;
        public static final int REASON_WL_HANGUP = 257;
        public static final int REASON_WL_MUTE = 258;
        public static final int REASON_WL_MUTEMMS = 261;
        public static final int REASON_WL_MUTESMS = 259;
        public static final String TYPE = "type";
        public static final int TYPE_CALL = 1;
        public static final int TYPE_MMS = 3;
        public static final int TYPE_SMS = 2;
        public static final int TYPE_UNKNOWN;
    }

    public static final class Keyword
        implements BaseColumns
    {
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/firewall-keyword";
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/firewall-keyword";
        public static final Uri CONTENT_URI = Uri.parse("content://firewall/keyword");
        public static final String DATA = "data";
    }

    public static final class Whitelist
        implements BaseColumns
    {
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/firewall-whitelist";
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/firewall-whitelist";
        public static final Uri CONTENT_URI = Uri.parse("content://firewall/whitelist");
        public static final String NOTES = "notes";
    }

    public static final class Blacklist
        implements BaseColumns
    {
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/firewall-blacklist";
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/firewall-blacklist";
        public static final Uri CONTENT_URI = Uri.parse("content://firewall/blacklist");
        public static final String NOTES = "notes";
    }

    public static final class SimCards
    {
        public static final String BIND_ID = "bind_id";
        public static final Uri CONTENT_URI = Uri.parse("content://mms-sms/sim-cards");
        public static final String IMSI = "imsi";
        public static final String MARKER1 = "marker1";
        public static final String MARKER2 = "marker2";
        public static final String MARKER_BASE = "marker_base";
        public static final String NUMBER = "number";
        public static final String SLOT = "slot";
        public static final String _ID = "_id";
    }

    public static final class SmsPhrase
    {
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/smsphrase";
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/smsphrase";
    }

    public static final class MmsSms
    {
        public static final Uri CONTENT_ALL_LOCKED_URI = Uri.parse("content://mms-sms/locked/all");
        public static final Uri CONTENT_EXPIRED_URI = Uri.parse("content://mms-sms/expired");
        public static final Uri CONTENT_PREVIEW_URI = Uri.parse("content://mms-sms/message/preview");
        public static final Uri CONTENT_RECENT_RECIPIENTS_URI = Uri.parse("content://mms-sms/recent-recipients");
        public static final String INSERT_PATH_IGNORED = "ignored";
        public static final String INSERT_PATH_INSERTED = "inserted";
        public static final String INSERT_PATH_RESTORED = "restored";
        public static final String INSERT_PATH_UPDATED = "updated";
        public static final int PREVIEW_ADDRESS_COLUMN_INDEX = 1;
        public static final int PREVIEW_BODY_COLUMN_INDEX = 4;
        public static final int PREVIEW_CHARSET_COLUMN_INDEX = 5;
        public static final int PREVIEW_DATE_COLUMN_INDEX = 2;
        public static final int PREVIEW_ID_COLUMN_INDEX = 0;
        public static final int PREVIEW_THREAD_ID_COLUMN_INDEX = 6;
        public static final int PREVIEW_TYPE_COLUMN_INDEX = 3;
        public static final int SYNC_STATE_DIRTY = 0;
        public static final int SYNC_STATE_ERROR = 3;
        public static final int SYNC_STATE_SYNCED = 2;
        public static final int SYNC_STATE_SYNCING = 1;
    }

    public static final class Threads
        implements ExtraTelephony.ThreadsColumns
    {
        public static final class Intents
        {
            public static final String THREADS_OBSOLETED_ACTION = "android.intent.action.SMS_THREADS_OBSOLETED_ACTION";
        }
    }

    public static abstract interface ThreadsColumns
    {
        public static final String HAS_DRAFT = "has_draft";
        public static final String UNREAD_COUNT = "unread_count";
    }

    public static final class Mx
    {
        public static final int TYPE_COMMON = 0;
        public static final int TYPE_DELIVERED = 17;
        public static final int TYPE_FAILED = 131073;
        public static final int TYPE_INCOMING = 65537;
        public static final int TYPE_PENDING = 1;
        public static final int TYPE_READ = 256;
        public static final int TYPE_SENT = 16;
        public static final int TYPE_WEB = 196609;
    }

    public static final class Mms
    {
        public static final String ACCOUNT = "account";
        public static final String ADDRESSES = "addresses";
        public static final String BIND_ID = "bind_id";
        public static final String DATE_FULL = "date_full";
        public static final String DATE_MS_PART = "date_ms_part";
        public static final String DELETED = "deleted";
        public static final String FILE_ID = "file_id";
        public static final String MARKER = "marker";
        public static final String MX_ID = "mx_id";
        public static final String MX_STATUS = "mx_status";
        public static final String NEED_DOWNLOAD = "need_download";
        public static final String OUT_TIME = "out_time";
        public static final String PREVIEW_DATA = "preview_data";
        public static final String PREVIEW_TYPE = "preview_type";
        public static final String SNIPPET = "snippet";
        public static final String SOURCE = "source";
        public static final String SYNC_STATE = "sync_state";
        public static final String TIMED = "timed";

        public static final class PreviewType
        {
            public static final int AUDIO = 3;
            public static final int IMAGE = 2;
            public static final int NONE = 1;
            public static final int SLIDESHOW = 6;
            public static final int UNKNOWN = 0;
            public static final int VCARD = 5;
            public static final int VIDEO = 4;
        }

        public static final class Intents
        {
            public static final String MAKE_MMS_PREVIEW_ACTION = "android.provider.Telephony.MAKE_MMS_PREVIEW";
        }
    }

    public static final class Sms
        implements ExtraTelephony.TextBasedSmsColumns
    {
        public static final String ACCOUNT = "account";
        public static final String ADDRESSES = "addresses";
        public static final String BIND_ID = "bind_id";
        public static final String DELETED = "deleted";
        public static final String MARKER = "marker";
        public static final String MX_ID = "mx_id";
        public static final String MX_STATUS = "mx_status";
        public static final String OUT_TIME = "out_time";
        public static final String SOURCE = "source";
        public static final String SYNC_STATE = "sync_state";
        public static final String TIMED = "timed";

        public static Uri addMiMessageToUri(ContentResolver paramContentResolver, Uri paramUri, String paramString1, String paramString2, String paramString3, Long paramLong, boolean paramBoolean1, boolean paramBoolean2, long paramLong1)
        {
            ContentValues localContentValues = new ContentValues(10);
            localContentValues.put("address", paramString1);
            if (paramLong != null)
                localContentValues.put("date", paramLong);
            if (paramBoolean1);
            for (Integer localInteger = Integer.valueOf(1); ; localInteger = Integer.valueOf(0))
            {
                localContentValues.put("read", localInteger);
                localContentValues.put("subject", paramString3);
                localContentValues.put("body", paramString2);
                if (paramBoolean2)
                    localContentValues.put("status", Integer.valueOf(32));
                if (paramLong1 != -1L)
                    localContentValues.put("thread_id", Long.valueOf(paramLong1));
                localContentValues.put("mx_status", Integer.valueOf(1));
                return paramContentResolver.insert(paramUri, localContentValues);
            }
        }

        public static boolean moveMessageToFolder(Context paramContext, Uri paramUri, int paramInt1, int paramInt2, Long paramLong, Integer paramInteger, int paramInt3)
        {
            return moveMessageToFolder(paramContext, paramUri, paramInt1, paramInt2, paramLong, paramInteger, paramInt3, null);
        }

        public static boolean moveMessageToFolder(Context paramContext, Uri paramUri, int paramInt1, int paramInt2, Long paramLong1, Integer paramInteger, int paramInt3, Long paramLong2)
        {
            boolean bool;
            if (paramUri == null)
                bool = false;
            while (true)
            {
                return bool;
                int i = 0;
                int j = 0;
                label65: ContentValues localContentValues;
                switch (paramInt1)
                {
                default:
                    bool = false;
                    break;
                case 2:
                case 4:
                    j = 1;
                case 1:
                case 3:
                    localContentValues = new ContentValues();
                    localContentValues.put("type", Integer.valueOf(paramInt1));
                    if (i != 0)
                        localContentValues.put("read", Integer.valueOf(0));
                case 5:
                case 6:
                    while (true)
                    {
                        if (paramLong1 != null)
                            localContentValues.put("out_time", paramLong1);
                        if (paramInteger != null)
                            localContentValues.put("status", paramInteger);
                        localContentValues.put("error_code", Integer.valueOf(paramInt2));
                        localContentValues.put("mx_status", Integer.valueOf(paramInt3));
                        if (paramLong2 != null)
                            localContentValues.put("mx_id", paramLong2);
                        if (1 != SqliteWrapper.update(paramContext, paramContext.getContentResolver(), paramUri, localContentValues, null, null))
                            break label214;
                        bool = true;
                        break;
                        i = 1;
                        break label65;
                        if (j != 0)
                            localContentValues.put("read", Integer.valueOf(1));
                    }
                    label214: bool = false;
                }
            }
        }

        public static final class Intents
        {
            public static final String DISMISS_NEW_MESSAGE_NOTIFICATION_ACTION = "android.provider.Telephony.DISMISS_NEW_MESSAGE_NOTIFICATION";
        }
    }

    public static abstract interface TextBasedSmsColumns
    {
        public static final int MESSAGE_TYPE_INVALID = 7;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.provider.ExtraTelephony
 * JD-Core Version:        0.6.2
 */