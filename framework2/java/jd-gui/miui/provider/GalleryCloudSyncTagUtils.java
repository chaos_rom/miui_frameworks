package miui.provider;

import android.accounts.Account;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

public class GalleryCloudSyncTagUtils
{
    private static final String TAG = "GalleryCloudSyncTagUtils";
    private static final Object sSyncTagLock = new Object();

    // ERROR //
    public static long getSyncTag(Context paramContext, Account paramAccount)
    {
        // Byte code:
        //     0: getstatic 17	miui/provider/GalleryCloudSyncTagUtils:sSyncTagLock	Ljava/lang/Object;
        //     3: astore_2
        //     4: aload_2
        //     5: monitorenter
        //     6: aconst_null
        //     7: astore_3
        //     8: aload_0
        //     9: aload_1
        //     10: invokestatic 23	miui/provider/GalleryCloudSyncTagUtils:getSyncTagCursorByAccount	(Landroid/content/Context;Landroid/accounts/Account;)Landroid/database/Cursor;
        //     13: astore_3
        //     14: aload_3
        //     15: ifnull +40 -> 55
        //     18: aload_3
        //     19: invokeinterface 29 1 0
        //     24: ifeq +31 -> 55
        //     27: aload_3
        //     28: iconst_0
        //     29: invokeinterface 33 2 0
        //     34: lstore 8
        //     36: lload 8
        //     38: lstore 6
        //     40: aload_3
        //     41: ifnull +9 -> 50
        //     44: aload_3
        //     45: invokeinterface 36 1 0
        //     50: aload_2
        //     51: monitorexit
        //     52: goto +43 -> 95
        //     55: lconst_1
        //     56: lstore 6
        //     58: aload_3
        //     59: ifnull +9 -> 68
        //     62: aload_3
        //     63: invokeinterface 36 1 0
        //     68: aload_2
        //     69: monitorexit
        //     70: goto +25 -> 95
        //     73: astore 5
        //     75: aload_2
        //     76: monitorexit
        //     77: aload 5
        //     79: athrow
        //     80: astore 4
        //     82: aload_3
        //     83: ifnull +9 -> 92
        //     86: aload_3
        //     87: invokeinterface 36 1 0
        //     92: aload 4
        //     94: athrow
        //     95: lload 6
        //     97: lreturn
        //
        // Exception table:
        //     from	to	target	type
        //     44	77	73	finally
        //     86	95	73	finally
        //     8	36	80	finally
    }

    private static Cursor getSyncTagCursorByAccount(Context paramContext, Account paramAccount)
    {
        ContentResolver localContentResolver = paramContext.getContentResolver();
        Uri localUri = GalleryCloudUtils.CLOUD_SETTING_URI;
        String[] arrayOfString = new String[1];
        arrayOfString[0] = "syncTag";
        return localContentResolver.query(localUri, arrayOfString, "accountName = '" + paramAccount.name + "' and " + "accountType" + " = '" + paramAccount.type + "'", null, null);
    }

    public static void insertAccountToDB(Context paramContext, Account paramAccount)
    {
        synchronized (sSyncTagLock)
        {
            ContentValues localContentValues = new ContentValues();
            localContentValues.put("accountName", paramAccount.name);
            localContentValues.put("accountType", paramAccount.type);
            internalUpdateAccount(paramContext, paramAccount, localContentValues);
            return;
        }
    }

    private static void internalUpdateAccount(Context paramContext, Account paramAccount, ContentValues paramContentValues)
    {
        ContentResolver localContentResolver = paramContext.getContentResolver();
        Cursor localCursor = null;
        try
        {
            localCursor = getSyncTagCursorByAccount(paramContext, paramAccount);
            if ((localCursor == null) || (!localCursor.moveToNext()))
            {
                paramContentValues.put("syncTag", Integer.valueOf(1));
                localContentResolver.insert(GalleryCloudUtils.CLOUD_SETTING_URI, paramContentValues);
            }
            while (true)
            {
                return;
                boolean bool = paramContentValues.containsKey("syncTag");
                if (!bool)
                {
                    if (localCursor != null)
                        localCursor.close();
                }
                else if (localCursor.getLong(0) != paramContentValues.getAsLong("syncTag").longValue())
                    localContentResolver.update(GalleryCloudUtils.CLOUD_SETTING_URI, paramContentValues, null, null);
            }
        }
        finally
        {
            if (localCursor != null)
                localCursor.close();
        }
    }

    public static void setSyncTag(Context paramContext, Account paramAccount, Long paramLong)
    {
        synchronized (sSyncTagLock)
        {
            ContentValues localContentValues = new ContentValues();
            localContentValues.put("syncTag", paramLong);
            internalUpdateAccount(paramContext, paramAccount, localContentValues);
            return;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.provider.GalleryCloudSyncTagUtils
 * JD-Core Version:        0.6.2
 */