package miui.provider;

import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.OperationApplicationException;
import android.net.Uri;
import android.os.RemoteException;
import android.util.Log;
import java.util.ArrayList;
import java.util.Iterator;

public final class BatchOperation
{
    public static final int BATCH_EXECUTE_SIZE = 100;
    private final String mAuthority;
    private final ArrayList<ContentProviderOperation> mOperations;
    private final ContentResolver mResolver;

    public BatchOperation(ContentResolver paramContentResolver, String paramString)
    {
        this.mResolver = paramContentResolver;
        this.mOperations = new ArrayList();
        this.mAuthority = paramString;
    }

    public void add(ContentProviderOperation paramContentProviderOperation)
    {
        this.mOperations.add(paramContentProviderOperation);
    }

    public Uri execute()
    {
        Uri localUri1 = null;
        Uri localUri2;
        if (this.mOperations.size() == 0)
            localUri2 = null;
        while (true)
        {
            return localUri2;
            try
            {
                ContentProviderResult[] arrayOfContentProviderResult = this.mResolver.applyBatch(this.mAuthority, this.mOperations);
                if ((arrayOfContentProviderResult != null) && (arrayOfContentProviderResult.length > 0))
                    localUri1 = arrayOfContentProviderResult[0].uri;
                this.mOperations.clear();
                localUri2 = localUri1;
            }
            catch (OperationApplicationException localOperationApplicationException)
            {
                while (true)
                    Log.e("BatchOperation", "storing contact data failed", localOperationApplicationException);
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    Log.e("BatchOperation", "storing contact data failed", localRemoteException);
            }
        }
    }

    public ContentResolver getContentResolver()
    {
        return this.mResolver;
    }

    public int size()
    {
        return this.mOperations.size();
    }

    public String toString()
    {
        StringBuilder localStringBuilder = new StringBuilder();
        Iterator localIterator = this.mOperations.iterator();
        while (localIterator.hasNext())
        {
            ContentProviderOperation localContentProviderOperation = (ContentProviderOperation)localIterator.next();
            localStringBuilder.append(localContentProviderOperation.toString() + "\n");
        }
        return localStringBuilder.toString();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.provider.BatchOperation
 * JD-Core Version:        0.6.2
 */