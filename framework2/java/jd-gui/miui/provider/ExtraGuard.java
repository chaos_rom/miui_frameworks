package miui.provider;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.text.TextUtils;
import java.util.List;

public final class ExtraGuard
{
    private static final String CLOUD_SCAN_KEY = "cloud_scan";
    public static final String DEFAULT_PACKAGE_NAME = "com.tencent.tmsprovider";
    private static final int SMS_BLACK = 2;
    private static final int SMS_WHITE = 1;
    private static final int VIRUS_BLACK = 2;
    private static final int VIRUS_GRAY = 3;
    private static final int VIRUS_WHITE = 1;

    public static boolean checkApk(Context paramContext, Uri paramUri)
    {
        boolean bool;
        if (paramUri == null)
            bool = false;
        while (true)
        {
            return bool;
            ContentResolver localContentResolver = paramContext.getContentResolver();
            String str1 = null;
            String str2 = paramUri.getScheme();
            if ("content".equals(str2))
            {
                Cursor localCursor = localContentResolver.query(paramUri, null, null, null, null);
                if (localCursor != null)
                {
                    if (localCursor.moveToFirst())
                        str1 = localCursor.getString(localCursor.getColumnIndex("_data"));
                    localCursor.close();
                }
            }
            while (true)
            {
                if (!TextUtils.isEmpty(str1))
                    break label119;
                bool = false;
                break;
                if ((str2 == null) || ("file".equals(str2)))
                    str1 = paramUri.getPath();
            }
            try
            {
                label119: Uri localUri = getUri(paramContext, "AntiVirusUri");
                if (localUri != null)
                {
                    ContentValues localContentValues = new ContentValues();
                    localContentValues.put("cloud_scan", Boolean.valueOf(false));
                    String[] arrayOfString = new String[1];
                    arrayOfString[0] = str1;
                    int i = localContentResolver.update(localUri, localContentValues, null, arrayOfString);
                    if (i != 2)
                    {
                        bool = true;
                        continue;
                    }
                    bool = false;
                }
            }
            catch (Exception localException)
            {
                bool = true;
            }
        }
    }

    public static boolean checkSms(Context paramContext, String paramString1, String paramString2)
    {
        boolean bool = true;
        try
        {
            ContentResolver localContentResolver = paramContext.getContentResolver();
            Uri localUri = getUri(paramContext, "AntiSmsSpamUri");
            if (localUri != null)
            {
                ContentValues localContentValues = new ContentValues();
                String[] arrayOfString = new String[2];
                arrayOfString[0] = paramString1;
                arrayOfString[1] = paramString2;
                int i = localContentResolver.update(localUri, localContentValues, null, arrayOfString);
                if (i == 2);
                while (true)
                {
                    return bool;
                    bool = false;
                }
            }
        }
        catch (Exception localException)
        {
            while (true)
                bool = false;
        }
    }

    public static Uri getUri(Context paramContext, String paramString)
    {
        String str = Settings.Secure.getString(paramContext.getContentResolver(), "safe_guard_choosed");
        if (TextUtils.isEmpty(str))
            str = "com.tencent.tmsprovider";
        Uri localUri = null;
        PackageManager localPackageManager = paramContext.getPackageManager();
        Intent localIntent = new Intent("miui.intent.action.safeguard");
        localIntent.setPackage(str);
        List localList = localPackageManager.queryBroadcastReceivers(localIntent, 128);
        if ((localList != null) && (localList.size() > 0))
            localUri = Uri.parse(((ResolveInfo)localList.get(0)).activityInfo.metaData.getString(paramString));
        return localUri;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.provider.ExtraGuard
 * JD-Core Version:        0.6.2
 */