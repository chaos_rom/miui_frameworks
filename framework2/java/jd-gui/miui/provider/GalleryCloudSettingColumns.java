package miui.provider;

public class GalleryCloudSettingColumns
{
    public static final String ACCOUNT_NAME = "accountName";
    public static final String ACCOUNT_TYPE = "accountType";
    public static final String CLOUD_SETTING_ID = "_id";
    public static final int INDEX_ACCOUNT_NAME = 1;
    public static final int INDEX_ACCOUNT_TYPE = 2;
    public static final int INDEX_CLOUD_SETTING_ID = 0;
    public static final int INDEX_IS_SYNC = 4;
    public static final int INDEX_IS_SYNC_ONLY_ON_WIFI = 5;
    public static final int INDEX_SYNC_TAG = 3;
    public static final String IS_SYNC = "isSync";
    public static final String IS_SYNC_ONLY_ON_WIFI = "isSyncOnlyOnWifi";
    public static final int MIN_SYNC_TAG_TO_GET_ALL_ITEM = 1;
    public static final String SYNC_TAG = "syncTag";
    private static final String TAG = "GalleryCloudSettingColumns";
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.provider.GalleryCloudSettingColumns
 * JD-Core Version:        0.6.2
 */