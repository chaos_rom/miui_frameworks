package miui.provider;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

public class MiCloudSmsCmd
{
    private static final int CMD_INDEX = 1;
    private static final int MSG_ID_INDEX = 2;
    private static final String SMS_CMD_HEADER = "mfc,";
    private static final String SMS_CMD_TAIL = "##";
    private static final int TIME_INDEX = 3;
    public static final String TYPE_LOCATION = "l";
    public static final String TYPE_LOCK = "k";
    public static final String TYPE_NOISE = "n";
    public static final String TYPE_WIPE = "w";

    public static boolean checkSmsCmd(Context paramContext, String paramString1, String paramString2)
    {
        boolean bool = false;
        if ((TextUtils.isEmpty(paramString2)) || (!paramString2.startsWith("mfc,")) || (paramString2.indexOf("##") < 0));
        String[] arrayOfString;
        do
        {
            return bool;
            arrayOfString = paramString2.split(",");
        }
        while ((arrayOfString == null) || (arrayOfString.length < 5));
        String str1 = arrayOfString[1];
        String str2 = arrayOfString[2];
        String str3 = arrayOfString[3];
        String str4 = null;
        if ("k".equals(str1))
            str4 = arrayOfString[4];
        for (String str5 = arrayOfString[5]; ; str5 = arrayOfString[4])
        {
            sendCmd(paramContext, str2, str3, str1, str4, str5.substring(0, str5.lastIndexOf("##")));
            bool = true;
            break;
        }
    }

    private static void sendCmd(Context paramContext, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
    {
        Intent localIntent = new Intent();
        localIntent.putExtra("android.intent.extra.device_msgId", paramString1);
        localIntent.putExtra("android.intent.extra.device_time", paramString2);
        localIntent.putExtra("android.intent.extra.device_digest", paramString5);
        localIntent.putExtra("android.intent.extra.device_cmd", paramString3);
        localIntent.putExtra("android.intent.extra.lock_password", paramString4);
        String str = null;
        if ("l".equals(paramString3))
            str = "miui.intent.action.REQUEST_LOCATION";
        while (true)
        {
            if (!TextUtils.isEmpty(str))
            {
                localIntent.setAction(str);
                paramContext.startService(localIntent);
            }
            return;
            if ("n".equals(paramString3))
                str = "miui.intent.action.NOISE";
            else if ("k".equals(paramString3))
                str = "miui.intent.action.LOCK_DEVICE";
            else if ("w".equals(paramString3))
                str = "miui.intent.action.WIPE_DATA";
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.provider.MiCloudSmsCmd
 * JD-Core Version:        0.6.2
 */