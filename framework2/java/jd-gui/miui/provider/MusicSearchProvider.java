package miui.provider;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import android.util.Log;
import java.util.Arrays;

public class MusicSearchProvider
{
    public static final int INDEX_ALBUM = 2;
    public static final int INDEX_ARTIST = 1;
    public static final int INDEX_TRACK = 0;
    public static final String KEY_RESULT = "result_key";
    public static final String PATH = "search";
    public static final String QUERY_PARAM_LIMIT = "limit";
    public static final String QUERY_PARAM_TYPE = "type";
    public static final String[] SEARCH_COLS = arrayOfString;
    static final String TAG = MusicSearchProvider.class.getName();
    public static final int TYPE_ALL = 3;
    public static final int TYPE_MEDIA_PROVIDER = 1;
    public static final int TYPE_ONLINE = 2;
    public static final Uri URI = Uri.parse("content://com.miui.player/search");

    static
    {
        String[] arrayOfString = new String[3];
        arrayOfString[0] = "title";
        arrayOfString[1] = "artist";
        arrayOfString[2] = "album";
    }

    public static boolean isValidType(int paramInt)
    {
        if ((paramInt & 0xFFFFFFFC) == 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static int parseIntFromUri(Uri paramUri, String paramString, int paramInt)
    {
        int i = paramInt;
        String str;
        if (paramUri != null)
        {
            str = paramUri.getQueryParameter(paramString);
            if (str == null);
        }
        try
        {
            int j = Integer.valueOf(str).intValue();
            i = j;
            return i;
        }
        catch (NumberFormatException localNumberFormatException)
        {
            while (true)
                Log.e(TAG, "", localNumberFormatException);
        }
    }

    public static int parseLimitFromUri(Uri paramUri)
    {
        return parseIntFromUri(paramUri, "limit", -1);
    }

    public static int parseTypeFromUri(Uri paramUri)
    {
        return parseIntFromUri(paramUri, "type", 3);
    }

    public static MusicSearchResult query(Context paramContext, int paramInt1, String[] paramArrayOfString, int paramInt2)
    {
        if (!isValidType(paramInt1))
            throw new IllegalArgumentException("bad type: " + paramInt1);
        Object localObject1 = null;
        Uri.Builder localBuilder = URI.buildUpon();
        localBuilder.appendQueryParameter("type", String.valueOf(paramInt1)).appendQueryParameter("limit", String.valueOf(paramInt2));
        Cursor localCursor = paramContext.getContentResolver().query(localBuilder.build(), null, null, paramArrayOfString, null);
        if (localCursor != null);
        while (true)
        {
            try
            {
                Bundle localBundle = localCursor.getExtras();
                localObject1 = localBundle;
                localCursor.close();
                if (localObject1 != null)
                {
                    localMusicSearchResult = (MusicSearchResult)localObject1.getParcelable("result_key");
                    return localMusicSearchResult;
                }
            }
            finally
            {
                localCursor.close();
            }
            MusicSearchResult localMusicSearchResult = null;
        }
    }

    public static class MusicMeta
        implements Parcelable
    {
        public static final Parcelable.Creator<MusicMeta> CREATOR = new Parcelable.Creator()
        {
            public MusicSearchProvider.MusicMeta createFromParcel(Parcel paramAnonymousParcel)
            {
                return new MusicSearchProvider.MusicMeta(paramAnonymousParcel);
            }

            public MusicSearchProvider.MusicMeta[] newArray(int paramAnonymousInt)
            {
                return new MusicSearchProvider.MusicMeta[paramAnonymousInt];
            }
        };
        public final String mAlbumName;
        public final String mArtistName;
        public final String mData;
        public final long mDuartion;
        public final int mFlag;
        public final long mLocalId;
        public final String mOnlineId;
        public final String mTitle;

        public MusicMeta(long paramLong1, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, long paramLong2, int paramInt)
        {
            this.mLocalId = paramLong1;
            this.mOnlineId = paramString1;
            this.mTitle = paramString2;
            this.mData = paramString3;
            this.mArtistName = paramString4;
            this.mAlbumName = paramString5;
            this.mDuartion = paramLong2;
            this.mFlag = paramInt;
        }

        MusicMeta(Parcel paramParcel)
        {
            this.mLocalId = paramParcel.readLong();
            this.mOnlineId = paramParcel.readString();
            this.mTitle = paramParcel.readString();
            this.mData = paramParcel.readString();
            this.mArtistName = paramParcel.readString();
            this.mAlbumName = paramParcel.readString();
            this.mDuartion = paramParcel.readLong();
            this.mFlag = paramParcel.readInt();
        }

        public int describeContents()
        {
            return 0;
        }

        public boolean equals(Object paramObject)
        {
            boolean bool = true;
            if (this == paramObject);
            while (true)
            {
                return bool;
                if (!(paramObject instanceof MusicMeta))
                {
                    bool = false;
                }
                else
                {
                    MusicMeta localMusicMeta = (MusicMeta)paramObject;
                    if ((this.mLocalId != localMusicMeta.mLocalId) || (this.mDuartion != localMusicMeta.mDuartion) || (this.mFlag != localMusicMeta.mFlag) || (!TextUtils.equals(this.mOnlineId, localMusicMeta.mOnlineId)) || (!TextUtils.equals(this.mTitle, localMusicMeta.mTitle)) || (!TextUtils.equals(this.mData, localMusicMeta.mData)) || (!TextUtils.equals(this.mArtistName, localMusicMeta.mArtistName)) || (!TextUtils.equals(this.mAlbumName, localMusicMeta.mAlbumName)))
                        bool = false;
                }
            }
        }

        public int hashCode()
        {
            int i = (int)this.mLocalId;
            if (this.mOnlineId != null);
            for (int j = this.mOnlineId.hashCode(); ; j = 0)
                return j + i;
        }

        public boolean isOnline()
        {
            if (this.mOnlineId != null);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            paramParcel.writeLong(this.mLocalId);
            paramParcel.writeString(this.mOnlineId);
            paramParcel.writeString(this.mTitle);
            paramParcel.writeString(this.mData);
            paramParcel.writeString(this.mArtistName);
            paramParcel.writeString(this.mAlbumName);
            paramParcel.writeLong(this.mDuartion);
            paramParcel.writeInt(this.mFlag);
        }

        public static class Builder
        {
            public String mAlbumName = null;
            public String mArtistName = null;
            public String mData = null;
            public long mDuartion = 0L;
            public int mFlag = 0;
            public long mLocalId = -1L;
            public String mOnlineId = null;
            public String mTitle = null;

            public MusicSearchProvider.MusicMeta create()
            {
                return new MusicSearchProvider.MusicMeta(this.mLocalId, this.mOnlineId, this.mTitle, this.mData, this.mArtistName, this.mAlbumName, this.mDuartion, this.mFlag);
            }

            public Builder setAlbumName(String paramString)
            {
                this.mAlbumName = paramString;
                return this;
            }

            public Builder setArtistName(String paramString)
            {
                this.mArtistName = paramString;
                return this;
            }

            public Builder setData(String paramString)
            {
                this.mData = paramString;
                return this;
            }

            public Builder setDuration(long paramLong)
            {
                this.mDuartion = paramLong;
                return this;
            }

            public Builder setFlag(int paramInt)
            {
                this.mFlag = paramInt;
                return this;
            }

            public Builder setLocalId(long paramLong)
            {
                this.mLocalId = paramLong;
                return this;
            }

            public Builder setOnlineId(String paramString)
            {
                this.mOnlineId = paramString;
                return this;
            }

            public Builder setTitle(String paramString)
            {
                this.mTitle = paramString;
                return this;
            }
        }

        public static abstract interface Flag
        {
            public static final int FILTERED = 1;
            public static final int NONE;
        }
    }

    public static class MusicSearchResult
        implements Parcelable
    {
        public static final Parcelable.Creator<MusicSearchResult> CREATOR = new Parcelable.Creator()
        {
            public MusicSearchProvider.MusicSearchResult createFromParcel(Parcel paramAnonymousParcel)
            {
                return new MusicSearchProvider.MusicSearchResult(paramAnonymousParcel);
            }

            public MusicSearchProvider.MusicSearchResult[] newArray(int paramAnonymousInt)
            {
                return new MusicSearchProvider.MusicSearchResult[paramAnonymousInt];
            }
        };
        public final String[] mKeys;
        private final MusicSearchProvider.MusicMeta[] mLocalResult;
        private final MusicSearchProvider.MusicMeta[] mOnlineResult;
        public final int mType;

        MusicSearchResult(Parcel paramParcel)
        {
            this.mLocalResult = ((MusicSearchProvider.MusicMeta[])paramParcel.createTypedArray(MusicSearchProvider.MusicMeta.CREATOR));
            this.mOnlineResult = ((MusicSearchProvider.MusicMeta[])paramParcel.createTypedArray(MusicSearchProvider.MusicMeta.CREATOR));
            this.mKeys = paramParcel.readStringArray();
            this.mType = paramParcel.readInt();
        }

        public MusicSearchResult(MusicSearchProvider.MusicMeta[] paramArrayOfMusicMeta1, MusicSearchProvider.MusicMeta[] paramArrayOfMusicMeta2, String[] paramArrayOfString, int paramInt)
        {
            this.mLocalResult = paramArrayOfMusicMeta1;
            this.mOnlineResult = paramArrayOfMusicMeta2;
            this.mKeys = paramArrayOfString;
            this.mType = paramInt;
        }

        public int describeContents()
        {
            return 0;
        }

        public boolean equals(Object paramObject)
        {
            boolean bool = true;
            if (this == paramObject);
            while (true)
            {
                return bool;
                if (!(paramObject instanceof MusicSearchResult))
                {
                    bool = false;
                }
                else
                {
                    MusicSearchResult localMusicSearchResult = (MusicSearchResult)paramObject;
                    if ((this.mType != localMusicSearchResult.mType) || (!Arrays.equals(this.mKeys, localMusicSearchResult.mKeys)) || (!Arrays.equals(this.mLocalResult, localMusicSearchResult.mLocalResult)) || (!Arrays.equals(this.mOnlineResult, localMusicSearchResult.mOnlineResult)))
                        bool = false;
                }
            }
        }

        public int getCount()
        {
            return localCount() + onlineCount();
        }

        public int hashCode()
        {
            int i = 0;
            if (this.mKeys == null);
            while (true)
            {
                return i;
                int j = 0;
                String[] arrayOfString = this.mKeys;
                int k = arrayOfString.length;
                int m = 0;
                if (m < k)
                {
                    String str = arrayOfString[m];
                    int n = j * 31;
                    if (str == null);
                    for (int i1 = 0; ; i1 = str.hashCode())
                    {
                        j = n + i1;
                        m++;
                        break;
                    }
                }
                i = j;
            }
        }

        public int localCount()
        {
            if (this.mLocalResult != null);
            for (int i = this.mLocalResult.length; ; i = 0)
                return i;
        }

        public MusicSearchProvider.MusicMeta localResult(int paramInt)
        {
            return this.mLocalResult[paramInt];
        }

        public MusicSearchProvider.MusicMeta[] localResult()
        {
            MusicSearchProvider.MusicMeta[] arrayOfMusicMeta = null;
            if (this.mLocalResult != null)
            {
                arrayOfMusicMeta = new MusicSearchProvider.MusicMeta[this.mLocalResult.length];
                System.arraycopy(this.mLocalResult, 0, arrayOfMusicMeta, 0, this.mLocalResult.length);
            }
            return arrayOfMusicMeta;
        }

        public int onlineCount()
        {
            if (this.mOnlineResult != null);
            for (int i = this.mOnlineResult.length; ; i = 0)
                return i;
        }

        public MusicSearchProvider.MusicMeta onlineResult(int paramInt)
        {
            return this.mOnlineResult[paramInt];
        }

        public MusicSearchProvider.MusicMeta[] onlineResult()
        {
            MusicSearchProvider.MusicMeta[] arrayOfMusicMeta = null;
            if (this.mOnlineResult != null)
            {
                arrayOfMusicMeta = new MusicSearchProvider.MusicMeta[this.mOnlineResult.length];
                System.arraycopy(this.mOnlineResult, 0, arrayOfMusicMeta, 0, this.mOnlineResult.length);
            }
            return arrayOfMusicMeta;
        }

        public MusicSearchProvider.MusicMeta[] result()
        {
            MusicSearchProvider.MusicMeta[] arrayOfMusicMeta;
            if ((this.mLocalResult != null) && (this.mOnlineResult != null))
            {
                arrayOfMusicMeta = new MusicSearchProvider.MusicMeta[this.mLocalResult.length + this.mOnlineResult.length];
                System.arraycopy(this.mLocalResult, 0, arrayOfMusicMeta, 0, this.mLocalResult.length);
                System.arraycopy(this.mOnlineResult, 0, arrayOfMusicMeta, this.mLocalResult.length, this.mOnlineResult.length);
            }
            while (true)
            {
                return arrayOfMusicMeta;
                if (this.mOnlineResult != null)
                    arrayOfMusicMeta = onlineResult();
                else if (this.mLocalResult != null)
                    arrayOfMusicMeta = localResult();
                else
                    arrayOfMusicMeta = null;
            }
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            paramParcel.writeTypedArray(this.mLocalResult, paramInt);
            paramParcel.writeTypedArray(this.mOnlineResult, paramInt);
            paramParcel.writeStringArray(this.mKeys);
            paramParcel.writeInt(this.mType);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.provider.MusicSearchProvider
 * JD-Core Version:        0.6.2
 */