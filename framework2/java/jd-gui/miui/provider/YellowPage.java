package miui.provider;

public class YellowPage
{
    public static final String ADDRESS = "addr";
    public static final String AUTHORITY = "yellowpage";
    public static final String CONTENT_SP_TYPE = "vnd.android.cursor.dir/yellowpage.sp";
    public static final String EMAIL = "email";
    public static final String ID = "_id";
    public static final String NAME = "name";
    public static final String PHOTO = "photo";
    public static final String PINYIN = "pinyin";
    public static final String REMARK = "remark";
    public static final String SP_CONTENT_URI = "content://yellowpage/sp";
    public static final String SP_MATCH = "sp";
    public static final String TABLE_NAME = "sp_info";
    public static final String TYPE = "type";
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.provider.YellowPage
 * JD-Core Version:        0.6.2
 */