package miui.provider;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.graphics.BitmapFactory.Options;
import android.location.Location;
import android.media.ExifInterface;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.Bundle;
import android.util.Log;
import java.io.File;
import java.io.IOException;
import miui.os.ExtraFileUtils;
import miui.util.ImageUtils;

public class GalleryCloudUtils
{
    public static final String AUTHORITY = "com.miui.gallery.cloud.provider";
    public static final Uri BASE_URI = Uri.parse("content://com.miui.gallery.cloud.provider");
    public static final Uri CLOUD_SETTING_URI = BASE_URI.buildUpon().appendPath("cloudSetting").build();
    public static final Uri CLOUD_URI = BASE_URI.buildUpon().appendPath("cloud").build();
    public static final String EXTRA_FILE_PATH = "extra_file_path";
    public static final String SAVE_TO_CLOUD_ACTION = "com.miui.gallery.save_to_cloud_action";
    private static final String TAG = "GalleryCloudUtils";

    public static boolean existXiaomiAccount(Context paramContext)
    {
        if (findXiaomiAccount(paramContext) != null);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static Account findXiaomiAccount(Context paramContext)
    {
        Account localAccount = null;
        Account[] arrayOfAccount = AccountManager.get(paramContext).getAccounts();
        if (arrayOfAccount == null);
        label49: 
        while (true)
        {
            return localAccount;
            for (int i = 0; ; i++)
            {
                if (i >= arrayOfAccount.length)
                    break label49;
                if (arrayOfAccount[i].type.equals("com.xiaomi"))
                {
                    localAccount = arrayOfAccount[i];
                    break;
                }
            }
        }
    }

    public static String getMimeTypeByGroupID(int paramInt)
    {
        String str;
        if (paramInt == 1)
            str = "image/jpeg";
        while (true)
        {
            return str;
            if (paramInt == 2)
                str = "image/png";
            else
                str = "";
        }
    }

    public static boolean isGalleryCloudSyncable(Context paramContext)
    {
        boolean bool = false;
        Account localAccount = findXiaomiAccount(paramContext);
        if (localAccount == null);
        while (true)
        {
            return bool;
            paramContext.getContentResolver();
            if ((ContentResolver.getMasterSyncAutomatically()) && (ContentResolver.getSyncAutomatically(localAccount, "com.miui.gallery.cloud.provider")))
                bool = true;
        }
    }

    private static void putExifIntToContentValues(ExifInterface paramExifInterface, String paramString1, ContentValues paramContentValues, String paramString2)
    {
        String str = paramExifInterface.getAttribute(paramString1);
        if (str == null);
        while (true)
        {
            return;
            try
            {
                paramContentValues.put(paramString2, Integer.valueOf(str));
            }
            catch (NumberFormatException localNumberFormatException)
            {
                localNumberFormatException.printStackTrace();
            }
        }
    }

    public static void requestSync(Context paramContext)
    {
        requestSync(paramContext, isGalleryCloudSyncable(paramContext));
    }

    private static void requestSync(Context paramContext, boolean paramBoolean)
    {
        if (!paramBoolean);
        while (true)
        {
            return;
            Account localAccount = findXiaomiAccount(paramContext);
            if (localAccount != null)
                ContentResolver.requestSync(localAccount, "com.miui.gallery.cloud.provider", new Bundle());
        }
    }

    public static void saveToCloudDB(Context paramContext, String paramString, int paramInt1, int paramInt2)
    {
        int i = 1;
        if (isGalleryCloudSyncable(paramContext))
            i = 4;
        saveToCloudDBWithoutSync(paramContext, paramString, paramInt1, paramInt2, i);
        requestSync(paramContext);
    }

    public static void saveToCloudDBWithoutSync(Context paramContext, String paramString, int paramInt1, int paramInt2, int paramInt3)
    {
        File localFile = new File(paramString);
        if (!localFile.exists())
            Log.d("GalleryCloudUtils", "file not exist:" + paramString);
        while (true)
        {
            return;
            saveToCloudDBWithoutSync(paramContext, new SaveToCloudDB(paramString, localFile.length(), getMimeTypeByGroupID(paramInt1), ExtraFileUtils.getFileTitle(localFile), localFile.getName(), localFile.lastModified(), 0, 0, 0, null, paramInt1, paramInt2, paramInt3));
        }
    }

    private static void saveToCloudDBWithoutSync(Context paramContext, SaveToCloudDB paramSaveToCloudDB)
    {
        long l = System.currentTimeMillis();
        if (!existXiaomiAccount(paramContext));
        while (true)
        {
            return;
            try
            {
                ExifInterface localExifInterface = new ExifInterface(paramSaveToCloudDB.path);
                ContentValues localContentValues = new ContentValues();
                localContentValues.put("groupId", Integer.valueOf(paramSaveToCloudDB.groupId));
                localContentValues.put("serverType", Integer.valueOf(paramSaveToCloudDB.serverType));
                localContentValues.put("localFlag", Integer.valueOf(paramSaveToCloudDB.localFlag));
                localContentValues.put("size", Long.valueOf(paramSaveToCloudDB.size));
                localContentValues.put("mimeType", paramSaveToCloudDB.mimeType);
                localContentValues.put("title", paramSaveToCloudDB.title);
                localContentValues.put("fileName", paramSaveToCloudDB.fileName);
                localContentValues.put("dateTaken", Long.valueOf(paramSaveToCloudDB.dateTaken));
                File localFile = new File(paramSaveToCloudDB.path);
                if (localFile.exists())
                    localContentValues.put("dateModified", Long.valueOf(localFile.lastModified()));
                localContentValues.put("localFile", paramSaveToCloudDB.path);
                int i = localExifInterface.getAttributeInt("ImageWidth", 0);
                int j = localExifInterface.getAttributeInt("ImageLength", 0);
                if ((i <= 0) || (j <= 0))
                {
                    BitmapFactory.Options localOptions = ImageUtils.getBitmapSize(paramSaveToCloudDB.path);
                    i = localOptions.outWidth;
                    j = localOptions.outHeight;
                }
                localContentValues.put("exifImageWidth", Integer.valueOf(i));
                localContentValues.put("exifImageLength", Integer.valueOf(j));
                localContentValues.put("exifOrientation", Integer.valueOf(localExifInterface.getAttributeInt("Orientation", paramSaveToCloudDB.orientation)));
                String str1 = localExifInterface.getAttribute("GPSLatitude");
                Double localDouble2;
                label328: String str2;
                if (str1 == null)
                {
                    if (paramSaveToCloudDB.location != null)
                    {
                        localDouble2 = Double.valueOf(paramSaveToCloudDB.location.getLatitude());
                        str1 = String.valueOf(localDouble2);
                    }
                }
                else
                {
                    localContentValues.put("exifGPSLatitude", str1);
                    str2 = localExifInterface.getAttribute("GPSLongitude");
                    if (str2 == null)
                        if (paramSaveToCloudDB.location == null)
                            break label732;
                }
                label732: for (Double localDouble1 = Double.valueOf(paramSaveToCloudDB.location.getLongitude()); ; localDouble1 = null)
                {
                    str2 = String.valueOf(localDouble1);
                    localContentValues.put("exifGPSLongitude", str2);
                    localContentValues.put("exifMake", localExifInterface.getAttribute("Make"));
                    localContentValues.put("exifModel", localExifInterface.getAttribute("Model"));
                    putExifIntToContentValues(localExifInterface, "Flash", localContentValues, "exifFlash");
                    localContentValues.put("exifGPSLatitudeRef", localExifInterface.getAttribute("GPSLatitudeRef"));
                    localContentValues.put("exifGPSLongitudeRef", localExifInterface.getAttribute("GPSLongitudeRef"));
                    localContentValues.put("exifExposureTime", localExifInterface.getAttribute("ExposureTime"));
                    localContentValues.put("exifFNumber", localExifInterface.getAttribute("FNumber"));
                    localContentValues.put("exifISOSpeedRatings", localExifInterface.getAttribute("ISOSpeedRatings"));
                    localContentValues.put("exifGPSAltitude", localExifInterface.getAttribute("GPSAltitude"));
                    putExifIntToContentValues(localExifInterface, "GPSAltitudeRef", localContentValues, "exifGPSAltitudeRef");
                    localContentValues.put("exifGPSTimeStamp", localExifInterface.getAttribute("GPSTimeStamp"));
                    localContentValues.put("exifGPSDateStamp", localExifInterface.getAttribute("GPSDateStamp"));
                    putExifIntToContentValues(localExifInterface, "WhiteBalance", localContentValues, "exifWhiteBalance");
                    localContentValues.put("exifFocalLength", localExifInterface.getAttribute("FocalLength"));
                    localContentValues.put("exifGPSProcessingMethod", localExifInterface.getAttribute("GPSProcessingMethod"));
                    localContentValues.put("exifDateTime", localExifInterface.getAttribute("DateTime"));
                    if (paramContext.getContentResolver().insert(CLOUD_URI, localContentValues) == null)
                        Log.e("GalleryCloudUtils", "saveToCloudDB, insert " + paramSaveToCloudDB.path + " into database error.");
                    Log.d("GalleryCloudUtils", "save to cloud db finished, time:" + (System.currentTimeMillis() - l));
                    break;
                    localDouble2 = null;
                    break label328;
                }
            }
            catch (IOException localIOException)
            {
                while (true)
                    Log.e("GalleryCloudUtils", "saveToCloudDB, create " + paramSaveToCloudDB.path + " ExifInterface error.");
            }
            catch (IllegalArgumentException localIllegalArgumentException)
            {
                while (true)
                    Log.e("GalleryCloudUtils", "saveToCloudDB failed:" + localIllegalArgumentException);
            }
        }
    }

    public static class SaveToCloudDB
    {
        public long dateTaken;
        public String fileName;
        public int groupId;
        public int height;
        public int localFlag;
        public Location location;
        public String mimeType;
        public int orientation;
        public String path;
        public int serverType;
        public long size;
        public String title;
        public int width;

        public SaveToCloudDB(String paramString1, long paramLong1, String paramString2, String paramString3, String paramString4, long paramLong2, int paramInt1, int paramInt2, int paramInt3, Location paramLocation, int paramInt4, int paramInt5, int paramInt6)
        {
            this.path = paramString1;
            this.size = paramLong1;
            this.mimeType = paramString2;
            this.title = paramString3;
            this.fileName = paramString4;
            this.dateTaken = paramLong2;
            this.width = paramInt1;
            this.height = paramInt2;
            this.orientation = paramInt3;
            this.location = paramLocation;
            this.groupId = paramInt4;
            this.serverType = paramInt5;
            this.localFlag = paramInt6;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.provider.GalleryCloudUtils
 * JD-Core Version:        0.6.2
 */