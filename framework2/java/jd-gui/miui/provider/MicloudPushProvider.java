package miui.provider;

import android.accounts.Account;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import java.util.Iterator;
import java.util.List;
import miui.accounts.ExtraAccountManager;

public abstract class MicloudPushProvider extends ContentProvider
{
    public static final String NAME_COLUMNS = "name";
    public static final int NAME_COLUMNS_INDEX = 0;
    public static final String[] PROJECTION = arrayOfString;
    public static final String TYPE_COLUMNS = "type";
    public static final int TYPE_COLUMNS_INDEX = 2;
    public static final String VALUE_COLUMNS = "value";
    public static final int VALUE_COLUMNS_INDEX = 1;
    private static final int WATERMARK_LIST = 1;
    private static final String WATERMARK_LIST_PATH = "watermark_list";

    static
    {
        String[] arrayOfString = new String[3];
        arrayOfString[0] = "name";
        arrayOfString[1] = "value";
        arrayOfString[2] = "type";
    }

    private MatrixCursor getWatermarkListCursor(Account paramAccount)
    {
        MatrixCursor localMatrixCursor = null;
        if (paramAccount != null)
        {
            localMatrixCursor = new MatrixCursor(PROJECTION);
            Iterator localIterator = getWatermarkList(getContext(), paramAccount).iterator();
            while (localIterator.hasNext())
            {
                Watermark localWatermark = (Watermark)localIterator.next();
                Object[] arrayOfObject = new Object[3];
                arrayOfObject[0] = localWatermark.mName;
                arrayOfObject[1] = Long.valueOf(localWatermark.mValue);
                arrayOfObject[2] = localWatermark.mType;
                localMatrixCursor.addRow(arrayOfObject);
            }
        }
        return localMatrixCursor;
    }

    public int delete(Uri paramUri, String paramString, String[] paramArrayOfString)
    {
        return 0;
    }

    protected abstract String getAuthority();

    public String getType(Uri paramUri)
    {
        return null;
    }

    protected abstract List<Watermark> getWatermarkList(Context paramContext, Account paramAccount);

    public Uri insert(Uri paramUri, ContentValues paramContentValues)
    {
        return null;
    }

    public boolean onCreate()
    {
        return true;
    }

    public Cursor query(Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2)
    {
        UriMatcher localUriMatcher = new UriMatcher(-1);
        localUriMatcher.addURI(getAuthority(), "watermark_list", 1);
        Account localAccount = ExtraAccountManager.getXiaomiAccount(getContext());
        switch (localUriMatcher.match(paramUri))
        {
        default:
            throw new IllegalArgumentException("Unknown URI " + paramUri);
        case 1:
        }
        return getWatermarkListCursor(localAccount);
    }

    public int update(Uri paramUri, ContentValues paramContentValues, String paramString, String[] paramArrayOfString)
    {
        return 0;
    }

    public static class Watermark
    {
        public String mName;
        public String mType;
        public long mValue;

        public Watermark(String paramString1, long paramLong, String paramString2)
        {
            this.mName = paramString1;
            this.mValue = paramLong;
            this.mType = paramString2;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.provider.MicloudPushProvider
 * JD-Core Version:        0.6.2
 */