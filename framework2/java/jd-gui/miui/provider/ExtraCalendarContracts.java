package miui.provider;

public class ExtraCalendarContracts
{
    public static final String ACCOUNT_NAME_LOCAL = "account_name_local";
    public static final String ACCOUNT_TYPE_LOCAL = "LOCAL";
    public static final int CALENDAR_ACCESS_LEVEL_LOCAL = 700;
    public static final String CALENDAR_DISPLAYNAME_BIRTHDAY = "calendar_displayname_birthday";
    public static final String CALENDAR_DISPLAYNAME_LOCAL = "calendar_displayname_local";
    public static final String INTENT_EXTRA_KEY_BIRTHDAY_QUERY = "BIRTHDAY_QUERY";
    public static final String INTENT_EXTRA_KEY_DETAIL_VIEW = "DETAIL_VIEW";
    public static final String OWNERACCOUNT_LOCAL = "owner_account_local";
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.provider.ExtraCalendarContracts
 * JD-Core Version:        0.6.2
 */