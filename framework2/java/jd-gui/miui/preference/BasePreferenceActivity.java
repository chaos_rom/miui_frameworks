package miui.preference;

import android.app.ActionBar;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.view.MenuItem;

public class BasePreferenceActivity extends PreferenceActivity
{
    protected void onCreate(Bundle paramBundle)
    {
        super.onCreate(paramBundle);
        ActionBar localActionBar = getActionBar();
        if (localActionBar != null)
            localActionBar.setHomeButtonEnabled(true);
    }

    public boolean onOptionsItemSelected(MenuItem paramMenuItem)
    {
        switch (paramMenuItem.getItemId())
        {
        default:
        case 16908332:
        }
        for (boolean bool = super.onOptionsItemSelected(paramMenuItem); ; bool = true)
        {
            return bool;
            finish();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.preference.BasePreferenceActivity
 * JD-Core Version:        0.6.2
 */