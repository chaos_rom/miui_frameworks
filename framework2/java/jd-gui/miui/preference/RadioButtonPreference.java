package miui.preference;

import android.content.Context;
import android.preference.CheckBoxPreference;
import android.util.AttributeSet;

public class RadioButtonPreference extends CheckBoxPreference
{
    public RadioButtonPreference(Context paramContext)
    {
        this(paramContext, null);
    }

    public RadioButtonPreference(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
        setWidgetLayoutResource(100859956);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.preference.RadioButtonPreference
 * JD-Core Version:        0.6.2
 */