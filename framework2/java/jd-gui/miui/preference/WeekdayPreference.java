package miui.preference;

import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.preference.ListPreference;
import android.text.TextUtils;
import android.util.AttributeSet;
import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;

public class WeekdayPreference extends ListPreference
{
    private DaysOfWeek mDaysOfWeek = new DaysOfWeek(0);
    private boolean mIsSingleChoice;
    private DaysOfWeek mNewDaysOfWeek = new DaysOfWeek(0);

    public WeekdayPreference(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
        String[] arrayOfString1 = new DateFormatSymbols().getWeekdays();
        String[] arrayOfString2 = new String[7];
        arrayOfString2[0] = arrayOfString1[2];
        arrayOfString2[1] = arrayOfString1[3];
        arrayOfString2[2] = arrayOfString1[4];
        arrayOfString2[3] = arrayOfString1[5];
        arrayOfString2[4] = arrayOfString1[6];
        arrayOfString2[5] = arrayOfString1[7];
        arrayOfString2[6] = arrayOfString1[1];
        setEntries(arrayOfString2);
        setEntryValues(arrayOfString2);
    }

    public DaysOfWeek getDaysOfWeek()
    {
        return this.mDaysOfWeek;
    }

    protected void onDialogClosed(boolean paramBoolean)
    {
        if (paramBoolean)
        {
            this.mDaysOfWeek.set(this.mNewDaysOfWeek);
            setSummary(this.mDaysOfWeek.toString(getContext(), true));
            callChangeListener(this.mDaysOfWeek);
        }
        while (true)
        {
            return;
            this.mNewDaysOfWeek.set(this.mDaysOfWeek);
        }
    }

    protected void onPrepareDialogBuilder(AlertDialog.Builder paramBuilder)
    {
        if (this.mIsSingleChoice)
            super.onPrepareDialogBuilder(paramBuilder);
        while (true)
        {
            return;
            paramBuilder.setMultiChoiceItems(getEntries(), this.mDaysOfWeek.getBooleanArray(), new DialogInterface.OnMultiChoiceClickListener()
            {
                public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt, boolean paramAnonymousBoolean)
                {
                    WeekdayPreference.this.mNewDaysOfWeek.set(paramAnonymousInt, paramAnonymousBoolean);
                }
            });
        }
    }

    public void setDaysOfWeek(DaysOfWeek paramDaysOfWeek)
    {
        this.mDaysOfWeek.set(paramDaysOfWeek);
        this.mNewDaysOfWeek.set(paramDaysOfWeek);
        setSummary(paramDaysOfWeek.toString(getContext(), true));
    }

    public void setSingleChoice(boolean paramBoolean)
    {
        this.mIsSingleChoice = paramBoolean;
    }

    public static final class DaysOfWeek
    {
        private static int[] DAY_MAP = arrayOfInt;
        private int mDays;

        static
        {
            int[] arrayOfInt = new int[7];
            arrayOfInt[0] = 2;
            arrayOfInt[1] = 3;
            arrayOfInt[2] = 4;
            arrayOfInt[3] = 5;
            arrayOfInt[4] = 6;
            arrayOfInt[5] = 7;
            arrayOfInt[6] = 1;
        }

        public DaysOfWeek(int paramInt)
        {
            this.mDays = paramInt;
        }

        private boolean isSet(int paramInt)
        {
            int i = 1;
            if ((this.mDays & i << paramInt) > 0);
            while (true)
            {
                return i;
                i = 0;
            }
        }

        public boolean[] getBooleanArray()
        {
            boolean[] arrayOfBoolean = new boolean[7];
            for (int i = 0; i < 7; i++)
                arrayOfBoolean[i] = isSet(i);
            return arrayOfBoolean;
        }

        public int getCoded()
        {
            return this.mDays;
        }

        public int getNextAlarm(Calendar paramCalendar)
        {
            int j;
            if (this.mDays == 0)
                j = -1;
            while (true)
            {
                return j;
                int i = (5 + paramCalendar.get(7)) % 7;
                for (j = 0; (j < 7) && (!isSet((i + j) % 7)); j++);
            }
        }

        public boolean isRepeatSet()
        {
            if (this.mDays != 0);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public void set(int paramInt, boolean paramBoolean)
        {
            if (paramBoolean);
            for (this.mDays |= 1 << paramInt; ; this.mDays &= (0xFFFFFFFF ^ 1 << paramInt))
                return;
        }

        public void set(DaysOfWeek paramDaysOfWeek)
        {
            this.mDays = paramDaysOfWeek.mDays;
        }

        public String toString(Context paramContext, boolean paramBoolean)
        {
            String str;
            if (this.mDays == 0)
                if (paramBoolean)
                    str = paramContext.getText(101450160).toString();
            while (true)
            {
                return str;
                str = "";
                continue;
                if (this.mDays == 127)
                {
                    str = paramContext.getText(101450159).toString();
                }
                else
                {
                    int i = 0;
                    int j = this.mDays;
                    while (j > 0)
                    {
                        if ((j & 0x1) == 1)
                            i++;
                        j >>= 1;
                    }
                    DateFormatSymbols localDateFormatSymbols = new DateFormatSymbols();
                    if (i > 1);
                    ArrayList localArrayList;
                    for (String[] arrayOfString = localDateFormatSymbols.getShortWeekdays(); ; arrayOfString = localDateFormatSymbols.getWeekdays())
                    {
                        localArrayList = new ArrayList();
                        for (int k = 0; k < 7; k++)
                            if ((this.mDays & 1 << k) != 0)
                            {
                                localArrayList.add(arrayOfString[DAY_MAP[k]]);
                                i--;
                            }
                    }
                    str = TextUtils.join(localArrayList).toString();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.preference.WeekdayPreference
 * JD-Core Version:        0.6.2
 */