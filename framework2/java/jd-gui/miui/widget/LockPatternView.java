package miui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Join;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Rect;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.BaseSavedState;
import android.view.View.MeasureSpec;
import android.view.accessibility.AccessibilityManager;
import com.android.internal.widget.LockPatternUtils;
import com.android.internal.widget.LockPatternView.Cell;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import miui.R.styleable;

public class LockPatternView extends View
{
    private static final int ASPECT_LOCK_HEIGHT = 2;
    private static final int ASPECT_LOCK_WIDTH = 1;
    private static final int ASPECT_SQUARE = 0;
    private static final int MILLIS_PER_CIRCLE_ANIMATING = 700;
    private static final boolean PROFILE_DRAWING = false;
    static final int STATUS_BAR_HEIGHT = 25;
    private static final String TAG = "LockPatternView";
    private long mAnimatingPeriodStart;
    private final Matrix mArrowMatrix;
    private int mAspect;
    private Bitmap mBitmapArrowGreenUp;
    private Bitmap mBitmapArrowRedUp;
    private Bitmap mBitmapBtnDefault;
    private Bitmap mBitmapBtnDefaultWithBackground;
    private Bitmap mBitmapBtnTouched;
    private Bitmap mBitmapCircleDefault;
    private Bitmap mBitmapCircleGreen;
    private Bitmap mBitmapCircleRed;
    private int mBitmapHeight;
    private int mBitmapWidth;
    private final Matrix mCircleMatrix;
    private final Path mCurrentPath;
    private float mDiameterFactor;
    private boolean mDrawingProfilingStarted = false;
    private boolean mEnableHapticFeedback;
    private float mHitFactor;
    private float mInProgressX;
    private float mInProgressY;
    private boolean mInStealthMode;
    private boolean mInputEnabled;
    private final Rect mInvalidate;
    private OnPatternListener mOnPatternListener;
    private Paint mPaint = new Paint();
    private Paint mPathPaint = new Paint();
    private ArrayList<LockPatternView.Cell> mPattern = new ArrayList(9);
    private DisplayMode mPatternDisplayMode;
    private boolean[][] mPatternDrawLookup;
    private boolean mPatternInProgress;
    private float mSquareHeight;
    private float mSquareWidth;
    private int mStrokeAlpha;

    public LockPatternView(Context paramContext)
    {
        this(paramContext, null);
    }

    public LockPatternView(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
        int[] arrayOfInt = new int[2];
        arrayOfInt[0] = 3;
        arrayOfInt[1] = 3;
        this.mPatternDrawLookup = ((boolean[][])Array.newInstance(Boolean.TYPE, arrayOfInt));
        this.mInProgressX = -1.0F;
        this.mInProgressY = -1.0F;
        this.mPatternDisplayMode = DisplayMode.Correct;
        this.mInputEnabled = true;
        this.mInStealthMode = false;
        this.mEnableHapticFeedback = true;
        this.mPatternInProgress = false;
        this.mDiameterFactor = 0.1F;
        this.mStrokeAlpha = 64;
        this.mHitFactor = 0.6F;
        this.mCurrentPath = new Path();
        this.mInvalidate = new Rect();
        this.mArrowMatrix = new Matrix();
        this.mCircleMatrix = new Matrix();
        loadAttrs(paramContext, paramAttributeSet);
        setClickable(true);
        this.mPathPaint.setAntiAlias(true);
        this.mPathPaint.setDither(true);
        this.mPathPaint.setAlpha(this.mStrokeAlpha);
        this.mPathPaint.setStyle(Paint.Style.STROKE);
        this.mPathPaint.setStrokeJoin(Paint.Join.ROUND);
        this.mPathPaint.setStrokeCap(Paint.Cap.ROUND);
    }

    private void addCellToPattern(LockPatternView.Cell paramCell)
    {
        this.mPatternDrawLookup[paramCell.getRow()][paramCell.getColumn()] = 1;
        this.mPattern.add(paramCell);
        notifyCellAdded();
    }

    private LockPatternView.Cell checkForNewHit(float paramFloat1, float paramFloat2)
    {
        LockPatternView.Cell localCell = null;
        int i = getRowHit(paramFloat2);
        if (i < 0);
        while (true)
        {
            return localCell;
            int j = getColumnHit(paramFloat1);
            if ((j >= 0) && (this.mPatternDrawLookup[i][j] == 0))
                localCell = LockPatternView.Cell.of(i, j);
        }
    }

    private void clearPatternDrawLookup()
    {
        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
                this.mPatternDrawLookup[i][j] = 0;
    }

    private LockPatternView.Cell detectAndAddHit(float paramFloat1, float paramFloat2)
    {
        int i = -1;
        LockPatternView.Cell localCell1 = checkForNewHit(paramFloat1, paramFloat2);
        int i3;
        if (localCell1 != null)
        {
            LockPatternView.Cell localCell2 = null;
            ArrayList localArrayList = this.mPattern;
            if (!localArrayList.isEmpty())
            {
                LockPatternView.Cell localCell3 = (LockPatternView.Cell)localArrayList.get(-1 + localArrayList.size());
                int j = localCell1.getRow() - localCell3.getRow();
                int k = localCell1.getColumn() - localCell3.getColumn();
                int m = localCell3.getRow();
                int n = localCell3.getColumn();
                if ((Math.abs(j) == 2) && (Math.abs(k) != 1))
                {
                    int i2 = localCell3.getRow();
                    if (j > 0)
                    {
                        i3 = 1;
                        m = i2 + i3;
                    }
                }
                else
                {
                    if ((Math.abs(k) == 2) && (Math.abs(j) != 1))
                    {
                        int i1 = localCell3.getColumn();
                        if (k > 0)
                            i = 1;
                        n = i1 + i;
                    }
                    localCell2 = LockPatternView.Cell.of(m, n);
                }
            }
            else
            {
                if ((localCell2 != null) && (this.mPatternDrawLookup[localCell2.getRow()][localCell2.getColumn()] == 0))
                    addCellToPattern(localCell2);
                addCellToPattern(localCell1);
                if (this.mEnableHapticFeedback)
                    performHapticFeedback(1, 3);
            }
        }
        while (true)
        {
            return localCell1;
            i3 = i;
            break;
            localCell1 = null;
        }
    }

    private void drawArrow(Canvas paramCanvas, float paramFloat1, float paramFloat2, LockPatternView.Cell paramCell1, LockPatternView.Cell paramCell2)
    {
        int i;
        int j;
        int k;
        int m;
        int n;
        int i1;
        int i2;
        if (this.mPatternDisplayMode != DisplayMode.Wrong)
        {
            i = 1;
            j = paramCell2.getRow();
            k = paramCell1.getRow();
            m = paramCell2.getColumn();
            n = paramCell1.getColumn();
            i1 = ((int)this.mSquareWidth - this.mBitmapWidth) / 2;
            i2 = ((int)this.mSquareHeight - this.mBitmapHeight) / 2;
            if (i == 0)
                break label344;
        }
        label344: for (Bitmap localBitmap = this.mBitmapArrowGreenUp; ; localBitmap = this.mBitmapArrowRedUp)
        {
            int i3 = this.mBitmapWidth;
            int i4 = this.mBitmapHeight;
            float f1 = 90.0F + (float)Math.toDegrees((float)Math.atan2(j - k, m - n));
            float f2 = j - k;
            float f3 = m - n;
            float f4 = (float)(this.mSquareHeight * Math.sqrt(f3 * f3 + f2 * f2) - this.mBitmapHeight + localBitmap.getHeight()) / 2.0F;
            float f5 = Math.min(this.mSquareWidth / this.mBitmapWidth, 1.0F);
            float f6 = Math.min(this.mSquareHeight / this.mBitmapHeight, 1.0F);
            this.mArrowMatrix.setTranslate(paramFloat1 + i1, paramFloat2 + i2);
            this.mArrowMatrix.preTranslate(this.mBitmapWidth / 2, this.mBitmapHeight / 2);
            this.mArrowMatrix.preScale(f5, f6);
            this.mArrowMatrix.preTranslate(-this.mBitmapWidth / 2, -this.mBitmapHeight / 2);
            this.mArrowMatrix.preRotate(f1, i3 / 2.0F, i4 / 2.0F);
            this.mArrowMatrix.preTranslate((i3 - localBitmap.getWidth()) / 2.0F, -f4);
            paramCanvas.drawBitmap(localBitmap, this.mArrowMatrix, this.mPaint);
            return;
            i = 0;
            break;
        }
    }

    private void drawCircle(Canvas paramCanvas, int paramInt1, int paramInt2, boolean paramBoolean)
    {
        Bitmap localBitmap1;
        Bitmap localBitmap2;
        if ((!paramBoolean) || ((this.mInStealthMode) && (this.mPatternDisplayMode != DisplayMode.Wrong)))
        {
            localBitmap1 = this.mBitmapCircleDefault;
            localBitmap2 = this.mBitmapBtnDefaultWithBackground;
        }
        while (true)
        {
            int i = this.mBitmapWidth;
            int j = this.mBitmapHeight;
            float f1 = this.mSquareWidth;
            float f2 = this.mSquareHeight;
            int k = (int)((f1 - i) / 2.0F);
            int m = (int)((f2 - j) / 2.0F);
            float f3 = Math.min(this.mSquareWidth / this.mBitmapWidth, 1.0F);
            float f4 = Math.min(this.mSquareHeight / this.mBitmapHeight, 1.0F);
            this.mCircleMatrix.setTranslate(paramInt1 + k, paramInt2 + m);
            this.mCircleMatrix.preTranslate(this.mBitmapWidth / 2, this.mBitmapHeight / 2);
            this.mCircleMatrix.preScale(f3, f4);
            this.mCircleMatrix.preTranslate(-this.mBitmapWidth / 2, -this.mBitmapHeight / 2);
            paramCanvas.drawBitmap(localBitmap1, this.mCircleMatrix, this.mPaint);
            paramCanvas.drawBitmap(localBitmap2, this.mCircleMatrix, this.mPaint);
            return;
            if (this.mPatternInProgress)
            {
                localBitmap1 = this.mBitmapCircleGreen;
                localBitmap2 = this.mBitmapBtnTouched;
            }
            else if (this.mPatternDisplayMode == DisplayMode.Wrong)
            {
                localBitmap1 = this.mBitmapCircleRed;
                localBitmap2 = this.mBitmapBtnDefault;
            }
            else
            {
                if ((this.mPatternDisplayMode != DisplayMode.Correct) && (this.mPatternDisplayMode != DisplayMode.Animate))
                    break;
                localBitmap1 = this.mBitmapCircleGreen;
                localBitmap2 = this.mBitmapBtnDefault;
            }
        }
        throw new IllegalStateException("unknown display mode " + this.mPatternDisplayMode);
    }

    private Bitmap getBitmapFor(int paramInt)
    {
        return BitmapFactory.decodeResource(getContext().getResources(), paramInt);
    }

    private float getCenterXForColumn(int paramInt)
    {
        return this.mPaddingLeft + paramInt * this.mSquareWidth + this.mSquareWidth / 2.0F;
    }

    private float getCenterYForRow(int paramInt)
    {
        return this.mPaddingTop + paramInt * this.mSquareHeight + this.mSquareHeight / 2.0F;
    }

    private int getColumnHit(float paramFloat)
    {
        float f1 = this.mSquareWidth;
        float f2 = f1 * this.mHitFactor;
        float f3 = this.mPaddingLeft + (f1 - f2) / 2.0F;
        int i = 0;
        if (i < 3)
        {
            float f4 = f3 + f1 * i;
            if ((paramFloat < f4) || (paramFloat > f4 + f2));
        }
        while (true)
        {
            return i;
            i++;
            break;
            i = -1;
        }
    }

    private int getRowHit(float paramFloat)
    {
        float f1 = this.mSquareHeight;
        float f2 = f1 * this.mHitFactor;
        float f3 = this.mPaddingTop + (f1 - f2) / 2.0F;
        int i = 0;
        if (i < 3)
        {
            float f4 = f3 + f1 * i;
            if ((paramFloat < f4) || (paramFloat > f4 + f2));
        }
        while (true)
        {
            return i;
            i++;
            break;
            i = -1;
        }
    }

    private void handleActionDown(MotionEvent paramMotionEvent)
    {
        resetPattern();
        float f1 = paramMotionEvent.getX();
        float f2 = paramMotionEvent.getY();
        LockPatternView.Cell localCell = detectAndAddHit(f1, f2);
        if (localCell != null)
        {
            this.mPatternInProgress = true;
            this.mPatternDisplayMode = DisplayMode.Correct;
            notifyPatternStarted();
        }
        while (true)
        {
            if (localCell != null)
            {
                float f3 = getCenterXForColumn(localCell.getColumn());
                float f4 = getCenterYForRow(localCell.getRow());
                float f5 = this.mSquareWidth / 2.0F;
                float f6 = this.mSquareHeight / 2.0F;
                invalidate((int)(f3 - f5), (int)(f4 - f6), (int)(f3 + f5), (int)(f4 + f6));
            }
            this.mInProgressX = f1;
            this.mInProgressY = f2;
            return;
            this.mPatternInProgress = false;
            notifyPatternCleared();
        }
    }

    private void handleActionMove(MotionEvent paramMotionEvent)
    {
        int i = paramMotionEvent.getHistorySize();
        int j = 0;
        if (j < i + 1)
        {
            float f1;
            label26: float f2;
            label38: float f3;
            float f4;
            float f6;
            float f7;
            float f8;
            float f9;
            label234: float f10;
            float f11;
            label250: float f12;
            float f13;
            label295: float f14;
            float f15;
            label311: float f16;
            float f17;
            float f24;
            float f25;
            float f19;
            float f18;
            label431: float f21;
            float f20;
            if (j < i)
            {
                f1 = paramMotionEvent.getHistoricalX(j);
                if (j >= i)
                    break label513;
                f2 = paramMotionEvent.getHistoricalY(j);
                int k = this.mPattern.size();
                LockPatternView.Cell localCell1 = detectAndAddHit(f1, f2);
                int m = this.mPattern.size();
                if ((localCell1 != null) && (m == 1))
                {
                    this.mPatternInProgress = true;
                    notifyPatternStarted();
                }
                if (Math.abs(f1 - this.mInProgressX) + Math.abs(f2 - this.mInProgressY) > 0.01F * this.mSquareWidth)
                {
                    f3 = this.mInProgressX;
                    f4 = this.mInProgressY;
                    this.mInProgressX = f1;
                    this.mInProgressY = f2;
                    if ((!this.mPatternInProgress) || (m <= 0))
                        break label607;
                    ArrayList localArrayList = this.mPattern;
                    float f5 = 0.5F * (this.mSquareWidth * this.mDiameterFactor);
                    LockPatternView.Cell localCell2 = (LockPatternView.Cell)localArrayList.get(m - 1);
                    f6 = getCenterXForColumn(localCell2.getColumn());
                    f7 = getCenterYForRow(localCell2.getRow());
                    Rect localRect = this.mInvalidate;
                    if (f6 >= f1)
                        break label522;
                    f8 = f6;
                    f9 = f1;
                    if (f7 >= f2)
                        break label533;
                    f10 = f7;
                    f11 = f2;
                    localRect.set((int)(f8 - f5), (int)(f10 - f5), (int)(f9 + f5), (int)(f11 + f5));
                    if (f6 >= f3)
                        break label544;
                    f12 = f6;
                    f13 = f3;
                    if (f7 >= f4)
                        break label555;
                    f14 = f7;
                    f15 = f4;
                    localRect.union((int)(f12 - f5), (int)(f14 - f5), (int)(f13 + f5), (int)(f15 + f5));
                    if (localCell1 != null)
                    {
                        f16 = getCenterXForColumn(localCell1.getColumn());
                        f17 = getCenterYForRow(localCell1.getRow());
                        if (m < 2)
                            break label588;
                        LockPatternView.Cell localCell3 = (LockPatternView.Cell)localArrayList.get(m - 1 - (m - k));
                        f24 = getCenterXForColumn(localCell3.getColumn());
                        f25 = getCenterYForRow(localCell3.getRow());
                        if (f16 >= f24)
                            break label566;
                        f19 = f16;
                        f18 = f24;
                        if (f17 >= f25)
                            break label577;
                        f21 = f17;
                        f20 = f25;
                        label447: float f22 = this.mSquareWidth / 2.0F;
                        float f23 = this.mSquareHeight / 2.0F;
                        localRect.set((int)(f19 - f22), (int)(f21 - f23), (int)(f18 + f22), (int)(f20 + f23));
                    }
                    invalidate(localRect);
                }
            }
            while (true)
            {
                j++;
                break;
                f1 = paramMotionEvent.getX();
                break label26;
                label513: f2 = paramMotionEvent.getY();
                break label38;
                label522: f8 = f1;
                f9 = f6;
                break label234;
                label533: f10 = f2;
                f11 = f7;
                break label250;
                label544: f12 = f3;
                f13 = f6;
                break label295;
                label555: f14 = f4;
                f15 = f7;
                break label311;
                label566: f19 = f24;
                f18 = f16;
                break label431;
                label577: f21 = f25;
                f20 = f17;
                break label447;
                label588: f18 = f16;
                f19 = f16;
                f20 = f17;
                f21 = f17;
                break label447;
                label607: invalidate();
            }
        }
    }

    private void handleActionUp(MotionEvent paramMotionEvent)
    {
        if (!this.mPattern.isEmpty())
        {
            this.mPatternInProgress = false;
            notifyPatternDetected();
            invalidate();
        }
    }

    private void loadAttrs(Context paramContext, AttributeSet paramAttributeSet)
    {
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.LockPatternView);
        String str = localTypedArray.getString(0);
        if ("square".equals(str))
            this.mAspect = 0;
        while (true)
        {
            this.mPathPaint.setColor(localTypedArray.getColor(1, -1));
            this.mDiameterFactor = localTypedArray.getFloat(2, 0.1F);
            this.mStrokeAlpha = localTypedArray.getInteger(11, 128);
            this.mBitmapBtnDefaultWithBackground = getBitmapFor(localTypedArray.getResourceId(4, 100794419));
            this.mBitmapBtnDefault = getBitmapFor(localTypedArray.getResourceId(3, 100794453));
            this.mBitmapBtnTouched = getBitmapFor(localTypedArray.getResourceId(5, 100794454));
            this.mBitmapCircleDefault = getBitmapFor(localTypedArray.getResourceId(6, 100794455));
            this.mBitmapCircleGreen = getBitmapFor(localTypedArray.getResourceId(7, 100794456));
            this.mBitmapCircleRed = getBitmapFor(localTypedArray.getResourceId(8, 100794424));
            this.mBitmapArrowGreenUp = getBitmapFor(localTypedArray.getResourceId(9, 100794425));
            this.mBitmapArrowRedUp = getBitmapFor(localTypedArray.getResourceId(10, 100794418));
            Bitmap[] arrayOfBitmap = new Bitmap[5];
            arrayOfBitmap[0] = this.mBitmapBtnDefault;
            arrayOfBitmap[1] = this.mBitmapBtnTouched;
            arrayOfBitmap[2] = this.mBitmapCircleDefault;
            arrayOfBitmap[3] = this.mBitmapCircleGreen;
            arrayOfBitmap[4] = this.mBitmapCircleRed;
            int i = arrayOfBitmap.length;
            for (int j = 0; j < i; j++)
            {
                Bitmap localBitmap = arrayOfBitmap[j];
                this.mBitmapWidth = Math.max(this.mBitmapWidth, localBitmap.getWidth());
                this.mBitmapHeight = Math.max(this.mBitmapHeight, localBitmap.getHeight());
            }
            if ("lock_width".equals(str))
                this.mAspect = 1;
            else if ("lock_height".equals(str))
                this.mAspect = 2;
            else
                this.mAspect = 0;
        }
    }

    private void notifyCellAdded()
    {
        if (this.mOnPatternListener != null)
            this.mOnPatternListener.onPatternCellAdded(this.mPattern);
        sendAccessEvent(101450163);
    }

    private void notifyPatternCleared()
    {
        if (this.mOnPatternListener != null)
            this.mOnPatternListener.onPatternCleared();
        sendAccessEvent(101450162);
    }

    private void notifyPatternDetected()
    {
        if (this.mOnPatternListener != null)
            this.mOnPatternListener.onPatternDetected(this.mPattern);
        sendAccessEvent(101450164);
    }

    private void notifyPatternStarted()
    {
        if (this.mOnPatternListener != null)
            this.mOnPatternListener.onPatternStart();
        sendAccessEvent(101450161);
    }

    private void resetPattern()
    {
        this.mPattern.clear();
        clearPatternDrawLookup();
        this.mPatternDisplayMode = DisplayMode.Correct;
        invalidate();
    }

    private int resolveMeasured(int paramInt1, int paramInt2)
    {
        int i = View.MeasureSpec.getSize(paramInt1);
        int j;
        switch (View.MeasureSpec.getMode(paramInt1))
        {
        default:
            j = i;
        case 0:
        case -2147483648:
        }
        while (true)
        {
            return j;
            j = paramInt2;
            continue;
            j = Math.max(i, paramInt2);
        }
    }

    private void sendAccessEvent(int paramInt)
    {
        setContentDescription(this.mContext.getString(paramInt));
        sendAccessibilityEvent(4);
        setContentDescription(null);
    }

    public void clearPattern()
    {
        resetPattern();
    }

    public void disableInput()
    {
        this.mInputEnabled = false;
    }

    public void enableInput()
    {
        this.mInputEnabled = true;
    }

    protected int getSuggestedMinimumHeight()
    {
        return 3 * this.mBitmapWidth;
    }

    protected int getSuggestedMinimumWidth()
    {
        return 3 * this.mBitmapWidth;
    }

    public boolean isInStealthMode()
    {
        return this.mInStealthMode;
    }

    public boolean isTactileFeedbackEnabled()
    {
        return this.mEnableHapticFeedback;
    }

    protected void onDraw(Canvas paramCanvas)
    {
        ArrayList localArrayList = this.mPattern;
        int i = localArrayList.size();
        boolean[][] arrayOfBoolean = this.mPatternDrawLookup;
        int i10;
        float f1;
        float f2;
        Path localPath;
        int j;
        int k;
        if (this.mPatternDisplayMode == DisplayMode.Animate)
        {
            int i6 = 700 * (i + 1);
            int i7 = (int)(SystemClock.elapsedRealtime() - this.mAnimatingPeriodStart) % i6;
            int i8 = i7 / 700;
            clearPatternDrawLookup();
            for (int i9 = 0; i9 < i8; i9++)
            {
                LockPatternView.Cell localCell6 = (LockPatternView.Cell)localArrayList.get(i9);
                arrayOfBoolean[localCell6.getRow()][localCell6.getColumn()] = 1;
            }
            if ((i8 > 0) && (i8 < i))
            {
                i10 = 1;
                if (i10 != 0)
                {
                    float f7 = i7 % 700 / 700.0F;
                    LockPatternView.Cell localCell4 = (LockPatternView.Cell)localArrayList.get(i8 - 1);
                    float f8 = getCenterXForColumn(localCell4.getColumn());
                    float f9 = getCenterYForRow(localCell4.getRow());
                    LockPatternView.Cell localCell5 = (LockPatternView.Cell)localArrayList.get(i8);
                    float f10 = f7 * (getCenterXForColumn(localCell5.getColumn()) - f8);
                    float f11 = f7 * (getCenterYForRow(localCell5.getRow()) - f9);
                    this.mInProgressX = (f8 + f10);
                    this.mInProgressY = (f9 + f11);
                }
                invalidate();
            }
        }
        else
        {
            f1 = this.mSquareWidth;
            f2 = this.mSquareHeight;
            float f3 = 0.5F * (f1 * this.mDiameterFactor);
            this.mPathPaint.setStrokeWidth(f3);
            localPath = this.mCurrentPath;
            localPath.rewind();
            j = this.mPaddingTop;
            k = this.mPaddingLeft;
        }
        for (int m = 0; ; m++)
        {
            if (m >= 3)
                break label369;
            float f6 = j + f2 * m;
            int i5 = 0;
            while (true)
                if (i5 < 3)
                {
                    drawCircle(paramCanvas, (int)(k + f1 * i5), (int)f6, arrayOfBoolean[m][i5]);
                    i5++;
                    continue;
                    i10 = 0;
                    break;
                }
        }
        label369: int n;
        boolean bool;
        if ((!this.mInStealthMode) || (this.mPatternDisplayMode == DisplayMode.Wrong))
        {
            n = 1;
            if ((0x2 & this.mPaint.getFlags()) == 0)
                break label579;
            bool = true;
            label404: this.mPaint.setFilterBitmap(true);
            if (n == 0);
        }
        int i2;
        LockPatternView.Cell localCell1;
        for (int i3 = 0; ; i3++)
        {
            int i4 = i - 1;
            LockPatternView.Cell localCell2;
            LockPatternView.Cell localCell3;
            if (i3 < i4)
            {
                localCell2 = (LockPatternView.Cell)localArrayList.get(i3);
                localCell3 = (LockPatternView.Cell)localArrayList.get(i3 + 1);
                if (arrayOfBoolean[localCell3.getRow()][localCell3.getColumn()] != 0);
            }
            else
            {
                if (n != 0)
                {
                    i1 = 0;
                    i2 = 0;
                    if (i2 < i)
                    {
                        localCell1 = (LockPatternView.Cell)localArrayList.get(i2);
                        if (arrayOfBoolean[localCell1.getRow()][localCell1.getColumn()] != 0)
                            break label626;
                    }
                    if (((this.mPatternInProgress) || (this.mPatternDisplayMode == DisplayMode.Animate)) && (i1 != 0))
                        localPath.lineTo(this.mInProgressX, this.mInProgressY);
                    paramCanvas.drawPath(localPath, this.mPathPaint);
                }
                this.mPaint.setFilterBitmap(bool);
                return;
                n = 0;
                break;
                label579: bool = false;
                break label404;
            }
            drawArrow(paramCanvas, k + f1 * localCell2.getColumn(), j + f2 * localCell2.getRow(), localCell2, localCell3);
        }
        label626: int i1 = 1;
        float f4 = getCenterXForColumn(localCell1.getColumn());
        float f5 = getCenterYForRow(localCell1.getRow());
        if (i2 == 0)
            localPath.moveTo(f4, f5);
        while (true)
        {
            i2++;
            break;
            localPath.lineTo(f4, f5);
        }
    }

    public boolean onHoverEvent(MotionEvent paramMotionEvent)
    {
        int i;
        if (AccessibilityManager.getInstance(this.mContext).isTouchExplorationEnabled())
        {
            i = paramMotionEvent.getAction();
            switch (i)
            {
            case 8:
            default:
            case 9:
            case 7:
            case 10:
            }
        }
        while (true)
        {
            onTouchEvent(paramMotionEvent);
            paramMotionEvent.setAction(i);
            return super.onHoverEvent(paramMotionEvent);
            paramMotionEvent.setAction(0);
            continue;
            paramMotionEvent.setAction(2);
            continue;
            paramMotionEvent.setAction(1);
        }
    }

    protected void onMeasure(int paramInt1, int paramInt2)
    {
        int i = getSuggestedMinimumWidth();
        int j = getSuggestedMinimumHeight();
        int k = resolveMeasured(paramInt1, i);
        int m = resolveMeasured(paramInt2, j);
        switch (this.mAspect)
        {
        default:
        case 0:
        case 1:
        case 2:
        }
        while (true)
        {
            setMeasuredDimension(k, m);
            return;
            m = Math.min(k, m);
            k = m;
            continue;
            m = Math.min(k, m);
            continue;
            k = Math.min(k, m);
        }
    }

    protected void onRestoreInstanceState(Parcelable paramParcelable)
    {
        SavedState localSavedState = (SavedState)paramParcelable;
        super.onRestoreInstanceState(localSavedState.getSuperState());
        setPattern(DisplayMode.Correct, LockPatternUtils.stringToPattern(localSavedState.getSerializedPattern()));
        this.mPatternDisplayMode = DisplayMode.values()[localSavedState.getDisplayMode()];
        this.mInputEnabled = localSavedState.isInputEnabled();
        this.mInStealthMode = localSavedState.isInStealthMode();
        this.mEnableHapticFeedback = localSavedState.isTactileFeedbackEnabled();
    }

    protected Parcelable onSaveInstanceState()
    {
        return new SavedState(super.onSaveInstanceState(), LockPatternUtils.patternToString(this.mPattern), this.mPatternDisplayMode.ordinal(), this.mInputEnabled, this.mInStealthMode, this.mEnableHapticFeedback, null);
    }

    protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        this.mSquareWidth = ((paramInt1 - this.mPaddingLeft - this.mPaddingRight) / 3.0F);
        this.mSquareHeight = ((paramInt2 - this.mPaddingTop - this.mPaddingBottom) / 3.0F);
    }

    public boolean onTouchEvent(MotionEvent paramMotionEvent)
    {
        boolean bool = true;
        if ((!this.mInputEnabled) || (!isEnabled()))
            bool = false;
        while (true)
        {
            return bool;
            switch (paramMotionEvent.getAction())
            {
            default:
                bool = false;
                break;
            case 0:
                handleActionDown(paramMotionEvent);
                break;
            case 1:
                handleActionUp(paramMotionEvent);
                break;
            case 2:
                handleActionMove(paramMotionEvent);
                break;
            case 3:
                resetPattern();
                this.mPatternInProgress = false;
                notifyPatternCleared();
            }
        }
    }

    public void setDisplayMode(DisplayMode paramDisplayMode)
    {
        this.mPatternDisplayMode = paramDisplayMode;
        if (paramDisplayMode == DisplayMode.Animate)
        {
            if (this.mPattern.size() == 0)
                throw new IllegalStateException("you must have a pattern to animate if you want to set the display mode to animate");
            this.mAnimatingPeriodStart = SystemClock.elapsedRealtime();
            LockPatternView.Cell localCell = (LockPatternView.Cell)this.mPattern.get(0);
            this.mInProgressX = getCenterXForColumn(localCell.getColumn());
            this.mInProgressY = getCenterYForRow(localCell.getRow());
            clearPatternDrawLookup();
        }
        invalidate();
    }

    public void setInStealthMode(boolean paramBoolean)
    {
        this.mInStealthMode = paramBoolean;
    }

    public void setOnPatternListener(OnPatternListener paramOnPatternListener)
    {
        this.mOnPatternListener = paramOnPatternListener;
    }

    public void setPattern(DisplayMode paramDisplayMode, List<LockPatternView.Cell> paramList)
    {
        this.mPattern.clear();
        this.mPattern.addAll(paramList);
        clearPatternDrawLookup();
        Iterator localIterator = paramList.iterator();
        while (localIterator.hasNext())
        {
            LockPatternView.Cell localCell = (LockPatternView.Cell)localIterator.next();
            this.mPatternDrawLookup[localCell.getRow()][localCell.getColumn()] = 1;
        }
        setDisplayMode(paramDisplayMode);
    }

    public void setTactileFeedbackEnabled(boolean paramBoolean)
    {
        this.mEnableHapticFeedback = paramBoolean;
    }

    private static class SavedState extends View.BaseSavedState
    {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator()
        {
            public LockPatternView.SavedState createFromParcel(Parcel paramAnonymousParcel)
            {
                return new LockPatternView.SavedState(paramAnonymousParcel, null);
            }

            public LockPatternView.SavedState[] newArray(int paramAnonymousInt)
            {
                return new LockPatternView.SavedState[paramAnonymousInt];
            }
        };
        private final int mDisplayMode;
        private final boolean mInStealthMode;
        private final boolean mInputEnabled;
        private final String mSerializedPattern;
        private final boolean mTactileFeedbackEnabled;

        private SavedState(Parcel paramParcel)
        {
            super();
            this.mSerializedPattern = paramParcel.readString();
            this.mDisplayMode = paramParcel.readInt();
            this.mInputEnabled = ((Boolean)paramParcel.readValue(null)).booleanValue();
            this.mInStealthMode = ((Boolean)paramParcel.readValue(null)).booleanValue();
            this.mTactileFeedbackEnabled = ((Boolean)paramParcel.readValue(null)).booleanValue();
        }

        private SavedState(Parcelable paramParcelable, String paramString, int paramInt, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
        {
            super();
            this.mSerializedPattern = paramString;
            this.mDisplayMode = paramInt;
            this.mInputEnabled = paramBoolean1;
            this.mInStealthMode = paramBoolean2;
            this.mTactileFeedbackEnabled = paramBoolean3;
        }

        public int getDisplayMode()
        {
            return this.mDisplayMode;
        }

        public String getSerializedPattern()
        {
            return this.mSerializedPattern;
        }

        public boolean isInStealthMode()
        {
            return this.mInStealthMode;
        }

        public boolean isInputEnabled()
        {
            return this.mInputEnabled;
        }

        public boolean isTactileFeedbackEnabled()
        {
            return this.mTactileFeedbackEnabled;
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            super.writeToParcel(paramParcel, paramInt);
            paramParcel.writeString(this.mSerializedPattern);
            paramParcel.writeInt(this.mDisplayMode);
            paramParcel.writeValue(Boolean.valueOf(this.mInputEnabled));
            paramParcel.writeValue(Boolean.valueOf(this.mInStealthMode));
            paramParcel.writeValue(Boolean.valueOf(this.mTactileFeedbackEnabled));
        }
    }

    public static abstract interface OnPatternListener
    {
        public abstract void onPatternCellAdded(List<LockPatternView.Cell> paramList);

        public abstract void onPatternCleared();

        public abstract void onPatternDetected(List<LockPatternView.Cell> paramList);

        public abstract void onPatternStart();
    }

    public static enum DisplayMode
    {
        static
        {
            Animate = new DisplayMode("Animate", 1);
            Wrong = new DisplayMode("Wrong", 2);
            DisplayMode[] arrayOfDisplayMode = new DisplayMode[3];
            arrayOfDisplayMode[0] = Correct;
            arrayOfDisplayMode[1] = Animate;
            arrayOfDisplayMode[2] = Wrong;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.widget.LockPatternView
 * JD-Core Version:        0.6.2
 */