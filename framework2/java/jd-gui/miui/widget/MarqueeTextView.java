package miui.widget;

import android.content.Context;
import android.text.TextUtils.TruncateAt;
import android.util.AttributeSet;
import android.widget.TextView;

public class MarqueeTextView extends TextView
{
    public MarqueeTextView(Context paramContext)
    {
        super(paramContext);
    }

    public MarqueeTextView(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
    }

    public MarqueeTextView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
    }

    public boolean isFocused()
    {
        return true;
    }

    public void setEllipsize(TextUtils.TruncateAt paramTruncateAt)
    {
        super.setEllipsize(TextUtils.TruncateAt.MARQUEE);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.widget.MarqueeTextView
 * JD-Core Version:        0.6.2
 */