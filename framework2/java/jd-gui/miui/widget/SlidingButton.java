package miui.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.ViewParent;
import android.widget.CheckBox;
import com.miui.internal.R.styleable;
import miui.util.UiUtils;

public class SlidingButton extends CheckBox
{
    private static final int ANIMATION_FRAME_DURATION = 16;
    private static final float MAXIMUM_MINOR_VELOCITY = 150.0F;
    private static final int MSG_ANIMATE = 1000;
    private static final int TAP_THRESHOLD = 6;
    private BitmapDrawable mActiveSlider;
    private int[] mAlphaPixels;
    private float mAnimatedVelocity = 150.0F;
    private boolean mAnimating = false;
    private long mAnimationLastTime;
    private float mAnimationPosition;
    private Bitmap mBarBitmap;
    private int[] mBarSlice;
    private long mCurrentAnimationTime;
    private Drawable mFrame;
    private final Handler mHandler = new SlidingHandler(null);
    private int mHeight;
    private int mLastX;
    private BitmapDrawable mOffDisable;
    private OnCheckedChangedListener mOnCheckedChangedListener = null;
    private BitmapDrawable mOnDisable;
    private int mOriginalTouchPointX;
    private BitmapDrawable mPressedSlider;
    private Drawable mSlideMask;
    private BitmapDrawable mSlideOff;
    private BitmapDrawable mSlideOn;
    private BitmapDrawable mSlideState;
    private BitmapDrawable mSlider;
    private boolean mSliderMoved;
    private int mSliderOffset;
    private int mSliderPositionEnd;
    private int mSliderPositionStart;
    private int mSliderWidth;
    private int mTapThreshold;
    private boolean mTracking;
    private int mWidth;

    public SlidingButton(Context paramContext)
    {
        this(paramContext, null);
    }

    public SlidingButton(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 0);
    }

    public SlidingButton(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        initialize(paramContext, paramAttributeSet, paramInt);
    }

    private void animateOff()
    {
        performFling(-150.0F);
        invalidate();
    }

    private void animateOn()
    {
        performFling(150.0F);
        invalidate();
    }

    private void animateToggle()
    {
        if (isChecked())
            animateOff();
        while (true)
        {
            return;
            animateOn();
        }
    }

    private void createBarBitmap(BitmapDrawable paramBitmapDrawable)
    {
        if (paramBitmapDrawable != null)
            this.mBarBitmap = Bitmap.createScaledBitmap(paramBitmapDrawable.getBitmap(), 2 * this.mWidth - this.mSliderWidth, this.mHeight, true);
    }

    private void cutEdge(int paramInt1, int paramInt2, int[] paramArrayOfInt)
    {
        for (int i = -1 + paramInt1 * paramInt2; i >= 0; i--)
            paramArrayOfInt[i] &= 16777215 + ((paramArrayOfInt[i] >>> 24) * (this.mAlphaPixels[i] >>> 24) / 255 << 24);
    }

    private void doAnimation()
    {
        boolean bool = false;
        if (!this.mAnimating);
        while (true)
        {
            return;
            incrementAnimation();
            moveSlider((int)this.mAnimationPosition);
            if ((this.mSliderOffset <= this.mSliderPositionStart) || (this.mSliderOffset >= this.mSliderPositionEnd))
            {
                this.mHandler.removeMessages(1000);
                this.mAnimating = false;
                if (this.mSliderOffset >= this.mSliderPositionEnd)
                    bool = true;
                setChecked(bool);
                if (this.mOnCheckedChangedListener != null)
                    this.mOnCheckedChangedListener.onCheckedChanged(isChecked());
            }
            else
            {
                this.mCurrentAnimationTime = (16L + this.mCurrentAnimationTime);
                this.mHandler.sendMessageAtTime(this.mHandler.obtainMessage(1000), this.mCurrentAnimationTime);
            }
        }
    }

    private void drawSlidingBar(Canvas paramCanvas)
    {
        int i = this.mSliderPositionEnd - this.mSliderOffset;
        this.mBarBitmap.getPixels(this.mBarSlice, 0, this.mWidth, i, 0, this.mWidth, this.mHeight);
        cutEdge(this.mWidth, this.mHeight, this.mBarSlice);
        paramCanvas.drawBitmap(this.mBarSlice, 0, this.mWidth, 0, 0, this.mWidth, this.mHeight, true, null);
    }

    private void incrementAnimation()
    {
        long l = SystemClock.uptimeMillis();
        float f = (float)(l - this.mAnimationLastTime) / 1000.0F;
        this.mAnimationPosition += f * this.mAnimatedVelocity;
        this.mAnimationLastTime = l;
    }

    private void initialize(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        int i = 101515303;
        int j = UiUtils.resolveAttribute(paramContext, 100728963);
        if (j > 0)
            i = j;
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.SlidingButton, paramInt, i);
        setDrawingCacheEnabled(false);
        this.mTapThreshold = ((int)(0.5F + 6.0F * getResources().getDisplayMetrics().density));
        this.mFrame = localTypedArray.getDrawable(0);
        this.mSlider = ((BitmapDrawable)localTypedArray.getDrawable(1));
        this.mPressedSlider = ((BitmapDrawable)localTypedArray.getDrawable(2));
        this.mOnDisable = ((BitmapDrawable)localTypedArray.getDrawable(3));
        this.mOffDisable = ((BitmapDrawable)localTypedArray.getDrawable(4));
        this.mSlideOff = ((BitmapDrawable)localTypedArray.getDrawable(7));
        this.mSlideOn = ((BitmapDrawable)localTypedArray.getDrawable(8));
        this.mSlideMask = localTypedArray.getDrawable(5);
        this.mWidth = this.mFrame.getIntrinsicWidth();
        this.mHeight = this.mFrame.getIntrinsicHeight();
        this.mActiveSlider = this.mSlider;
        this.mSliderWidth = Math.min(this.mWidth, this.mSlider.getIntrinsicWidth());
        this.mSliderPositionStart = 0;
        this.mSliderPositionEnd = (this.mWidth - this.mSliderWidth);
        this.mSliderOffset = this.mSliderPositionStart;
        this.mFrame.setBounds(0, 0, this.mWidth, this.mHeight);
        this.mOnDisable.setBounds(0, 0, this.mWidth, this.mHeight);
        this.mOffDisable.setBounds(0, 0, this.mWidth, this.mHeight);
        this.mSlideOn.setBounds(0, 0, this.mWidth, this.mHeight);
        this.mSlideOff.setBounds(0, 0, this.mWidth, this.mHeight);
        this.mAlphaPixels = new int[this.mWidth * this.mHeight];
        Bitmap localBitmap1 = ((BitmapDrawable)localTypedArray.getDrawable(5)).getBitmap();
        Bitmap localBitmap2 = Bitmap.createScaledBitmap(localBitmap1, this.mWidth, this.mHeight, false);
        localBitmap2.getPixels(this.mAlphaPixels, 0, this.mWidth, 0, 0, this.mWidth, this.mHeight);
        if (localBitmap2 != localBitmap1)
            localBitmap2.recycle();
        this.mBarSlice = new int[this.mWidth * this.mHeight];
        this.mSlideState = this.mSlideOff;
        createBarBitmap(this.mSlideState);
        localTypedArray.recycle();
    }

    private void moveSlider(int paramInt)
    {
        this.mSliderOffset = (paramInt + this.mSliderOffset);
        if (this.mSliderOffset < this.mSliderPositionStart)
            this.mSliderOffset = this.mSliderPositionStart;
        while (true)
        {
            invalidate();
            return;
            if (this.mSliderOffset > this.mSliderPositionEnd)
                this.mSliderOffset = this.mSliderPositionEnd;
        }
    }

    private void performFling(float paramFloat)
    {
        this.mAnimating = true;
        this.mAnimationPosition = 0.0F;
        this.mAnimatedVelocity = paramFloat;
        long l = SystemClock.uptimeMillis();
        this.mAnimationLastTime = l;
        this.mCurrentAnimationTime = (16L + l);
        this.mHandler.removeMessages(1000);
        this.mHandler.sendMessageAtTime(this.mHandler.obtainMessage(1000), this.mCurrentAnimationTime);
    }

    protected void onDraw(Canvas paramCanvas)
    {
        super.onDraw(paramCanvas);
        if (!isEnabled())
            if (isChecked())
                this.mOnDisable.draw(paramCanvas);
        while (true)
        {
            return;
            this.mOffDisable.draw(paramCanvas);
            continue;
            drawSlidingBar(paramCanvas);
            this.mFrame.draw(paramCanvas);
            this.mSlideMask.draw(paramCanvas);
            this.mActiveSlider.setBounds(this.mSliderOffset, 0, this.mSliderWidth + this.mSliderOffset, this.mHeight);
            this.mActiveSlider.draw(paramCanvas);
        }
    }

    protected void onMeasure(int paramInt1, int paramInt2)
    {
        setMeasuredDimension(this.mWidth, this.mHeight);
    }

    public boolean onTouchEvent(MotionEvent paramMotionEvent)
    {
        boolean bool = false;
        if (!isEnabled())
            return bool;
        int i = paramMotionEvent.getAction();
        int j = (int)paramMotionEvent.getX();
        int k = (int)paramMotionEvent.getY();
        Rect localRect = new Rect(this.mSliderOffset, 0, this.mSliderOffset + this.mSliderWidth, this.mHeight);
        switch (i)
        {
        default:
        case 0:
        case 2:
        case 1:
        case 3:
        }
        while (true)
        {
            bool = true;
            break;
            if (localRect.contains(j, k))
            {
                this.mTracking = true;
                this.mActiveSlider = this.mPressedSlider;
                invalidate();
            }
            while (true)
            {
                this.mLastX = j;
                this.mOriginalTouchPointX = j;
                this.mSliderMoved = false;
                break;
                this.mTracking = false;
            }
            if (this.mTracking)
            {
                moveSlider(j - this.mLastX);
                this.mLastX = j;
                if (Math.abs(j - this.mOriginalTouchPointX) >= this.mTapThreshold)
                {
                    this.mSliderMoved = true;
                    getParent().requestDisallowInterceptTouchEvent(true);
                    continue;
                    if (this.mTracking)
                        if (!this.mSliderMoved)
                            animateToggle();
                    while (true)
                    {
                        this.mTracking = false;
                        this.mSliderMoved = false;
                        break;
                        if ((this.mSliderOffset >= this.mSliderPositionStart) && (this.mSliderOffset <= this.mSliderPositionEnd / 2))
                        {
                            animateOff();
                        }
                        else
                        {
                            animateOn();
                            continue;
                            animateToggle();
                        }
                    }
                    this.mTracking = false;
                    this.mSliderMoved = false;
                }
            }
        }
    }

    public void setBarImageResource(int paramInt)
    {
        createBarBitmap((BitmapDrawable)this.mContext.getResources().getDrawable(paramInt));
    }

    public void setButtonDrawable(Drawable paramDrawable)
    {
    }

    public void setChecked(boolean paramBoolean)
    {
        super.setChecked(paramBoolean);
        if (paramBoolean)
        {
            this.mSliderOffset = this.mSliderPositionEnd;
            this.mSlideState = this.mSlideOn;
            createBarBitmap(this.mSlideState);
        }
        while (true)
        {
            this.mActiveSlider = this.mSlider;
            invalidate();
            return;
            this.mSliderOffset = this.mSliderPositionStart;
            this.mSlideState = this.mSlideOff;
            createBarBitmap(this.mSlideState);
        }
    }

    public void setOnCheckedChangedListener(OnCheckedChangedListener paramOnCheckedChangedListener)
    {
        this.mOnCheckedChangedListener = paramOnCheckedChangedListener;
    }

    private class SlidingHandler extends Handler
    {
        private SlidingHandler()
        {
        }

        public void handleMessage(Message paramMessage)
        {
            switch (paramMessage.what)
            {
            default:
            case 1000:
            }
            while (true)
            {
                return;
                SlidingButton.this.doAnimation();
            }
        }
    }

    public static abstract interface OnCheckedChangedListener
    {
        public abstract void onCheckedChanged(boolean paramBoolean);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.widget.SlidingButton
 * JD-Core Version:        0.6.2
 */