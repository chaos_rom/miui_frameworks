package miui.widget;

import android.R.styleable;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.ViewDebug.ExportedProperty;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.Checkable;
import android.widget.TextView;

public class CheckedTextView extends TextView
    implements Checkable
{
    private static final int[] CHECKED_STATE_SET = arrayOfInt;
    private int mCheckMarkResource;
    private boolean mChecked;

    static
    {
        int[] arrayOfInt = new int[1];
        arrayOfInt[0] = 16842912;
    }

    public CheckedTextView(Context paramContext)
    {
        this(paramContext, null);
    }

    public CheckedTextView(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 0);
    }

    public CheckedTextView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.CheckedTextView, paramInt, 0);
        Drawable localDrawable = localTypedArray.getDrawable(1);
        if (localDrawable != null)
            setCheckMarkDrawable(localDrawable);
        setChecked(localTypedArray.getBoolean(0, false));
        localTypedArray.recycle();
    }

    public Drawable getCheckMarkDrawable()
    {
        return getCompoundDrawables()[0];
    }

    @ViewDebug.ExportedProperty
    public boolean isChecked()
    {
        return this.mChecked;
    }

    protected int[] onCreateDrawableState(int paramInt)
    {
        int[] arrayOfInt = super.onCreateDrawableState(paramInt + 1);
        if (isChecked())
            mergeDrawableStates(arrayOfInt, CHECKED_STATE_SET);
        return arrayOfInt;
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
        paramAccessibilityEvent.setClassName(CheckedTextView.class.getName());
        paramAccessibilityEvent.setChecked(this.mChecked);
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(CheckedTextView.class.getName());
        paramAccessibilityNodeInfo.setCheckable(true);
        paramAccessibilityNodeInfo.setChecked(this.mChecked);
    }

    public void setCheckMarkDrawable(int paramInt)
    {
        if ((paramInt != 0) && (paramInt == this.mCheckMarkResource));
        while (true)
        {
            return;
            this.mCheckMarkResource = paramInt;
            Drawable localDrawable = null;
            if (this.mCheckMarkResource != 0)
                localDrawable = getResources().getDrawable(this.mCheckMarkResource);
            setCheckMarkDrawable(localDrawable);
        }
    }

    public void setCheckMarkDrawable(Drawable paramDrawable)
    {
        setCompoundDrawablesWithIntrinsicBounds(paramDrawable, null, null, null);
    }

    public void setChecked(boolean paramBoolean)
    {
        if (this.mChecked != paramBoolean)
        {
            this.mChecked = paramBoolean;
            refreshDrawableState();
            notifyAccessibilityStateChanged();
        }
    }

    public void toggle()
    {
        if (!this.mChecked);
        for (boolean bool = true; ; bool = false)
        {
            setChecked(bool);
            return;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.widget.CheckedTextView
 * JD-Core Version:        0.6.2
 */