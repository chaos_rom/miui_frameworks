package miui.view;

import android.view.WindowManager.LayoutParams;

public class ExtraWindowManager
{
    public static class LayoutParams extends WindowManager.LayoutParams
    {
        public static final int PRIVATE_FLAG_LOCKSCREEN_DISPALY_DESKTOP = 1073741824;
        public static final int PRIVATE_FLAG_TRANSPARENT_STATUS_BAR = -2147483648;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.view.ExtraWindowManager
 * JD-Core Version:        0.6.2
 */