package miui.os;

import android.os.Build.VERSION;
import android.os.SystemProperties;
import android.text.TextUtils;

public class Build extends android.os.Build
{
    public static final boolean IS_DEFY = false;
    public static final boolean IS_DESIRE = false;
    public static final boolean IS_DEVELOPMENT_VERSION = false;
    public static final boolean IS_FAST_GPU_DEVICE = false;
    public static final boolean IS_FINAL_USER_BUILD = false;
    public static final boolean IS_FULL_SOURCE_CODE_DEVICE = false;
    public static final boolean IS_GALAXYS2 = false;
    public static final boolean IS_GALAXYS_NEXUS = false;
    public static final boolean IS_HK_BUILD = false;
    public static final boolean IS_HTC_HD2 = false;
    public static final boolean IS_I9000 = false;
    public static final boolean IS_INTERNATIONAL_BUILD = false;
    public static final boolean IS_LOW_MEMORY_DEVICE = false;
    public static final boolean IS_MI1S = false;
    public static final boolean IS_MILESTONE = false;
    public static final boolean IS_MIONE = false;
    public static final boolean IS_MIONE_CT_CUSTOMIZATION = false;
    public static final boolean IS_MIONE_PLUS_CDMA = false;
    public static final boolean IS_MITWO = false;
    public static final boolean IS_MITWO_CDMA = false;
    private static final boolean IS_MITWO_HK_BUILD = false;
    private static final boolean IS_MITWO_TW_BUILD = false;
    public static final boolean IS_NEED_UNCOMPRESSED_UCS2_SMS_DEVICE = false;
    public static final boolean IS_NEXUS_7 = false;
    public static final boolean IS_NEXUS_ONE = false;
    public static final boolean IS_NEXUS_S = false;
    public static final boolean IS_P990 = false;
    public static final boolean IS_RICH_MEMORY_DEVICE = false;
    public static final boolean IS_STABLE_VERSION = false;
    public static final boolean IS_T959 = false;
    public static final boolean IS_TW_BUILD = false;
    public static final boolean IS_U8860 = false;
    public static final boolean IS_U9200 = false;
    public static final boolean IS_XIAOMI = false;
    private static final String REGULAR_EXPRESSION_FOR_DEVELOPMENT = "\\d+.\\d+.\\d+(.ALPHA)?";
    public static final boolean SHOW_MAGNIFIER_WHEN_INPUT;

    static
    {
        boolean bool1 = false;
        IS_DEFY = "jordan".equals(BOARD);
        IS_DESIRE = "bravo".equals(DEVICE);
        IS_GALAXYS2 = "galaxys2".equals(DEVICE);
        boolean bool2;
        boolean bool3;
        label109: boolean bool4;
        label138: boolean bool5;
        label255: boolean bool6;
        label275: boolean bool7;
        label319: boolean bool8;
        label363: boolean bool9;
        label383: boolean bool10;
        label403: boolean bool11;
        label448: boolean bool12;
        label480: boolean bool13;
        if (("htcleo".equals(DEVICE)) || ("leo".equals(DEVICE)))
        {
            bool2 = true;
            IS_HTC_HD2 = bool2;
            IS_I9000 = "aries".equals(BOARD);
            IS_MILESTONE = "umts_sholes".equals(DEVICE);
            if ((!"mione".equals(DEVICE)) && (!"mione_plus".equals(DEVICE)))
                break label604;
            bool3 = true;
            IS_MIONE = bool3;
            IS_MITWO = "aries".equals(DEVICE);
            if ((!IS_MIONE) && (!IS_MITWO))
                break label609;
            bool4 = true;
            IS_XIAOMI = bool4;
            IS_NEXUS_ONE = "passion".equals(DEVICE);
            IS_NEXUS_S = "crespo".equals(DEVICE);
            IS_NEXUS_7 = "grouper".equals(DEVICE);
            IS_GALAXYS_NEXUS = "maguro".equals(DEVICE);
            IS_P990 = "p990".equals(DEVICE);
            IS_T959 = DEVICE.startsWith("vibrant");
            IS_U8860 = "hwu8860".equals(DEVICE);
            IS_U9200 = "hwu9200".equals(DEVICE);
            if ((!"MI 1S".equals(MODEL)) && (!"MI 1SC".equals(MODEL)))
                break label614;
            bool5 = true;
            IS_MI1S = bool5;
            if ((!IS_MIONE) && (!IS_GALAXYS2))
                break label620;
            bool6 = true;
            IS_RICH_MEMORY_DEVICE = bool6;
            IS_LOW_MEMORY_DEVICE = IS_MILESTONE;
            if ((!IS_MIONE) && (!IS_I9000) && (!IS_P990) && (!IS_NEXUS_S) && (!IS_GALAXYS2))
                break label626;
            bool7 = true;
            IS_FAST_GPU_DEVICE = bool7;
            if ((!IS_MILESTONE) && (!IS_NEXUS_S) && (!IS_I9000) && (!IS_DEFY) && (!IS_GALAXYS2) && (!IS_P990))
                break label632;
            bool8 = true;
            IS_NEED_UNCOMPRESSED_UCS2_SMS_DEVICE = bool8;
            if ((!IS_MIONE) || (!isMsm8660()))
                break label638;
            bool9 = true;
            IS_MIONE_PLUS_CDMA = bool9;
            if ((!IS_MITWO) || (!hasMeid()))
                break label644;
            bool10 = true;
            IS_MITWO_CDMA = bool10;
            IS_MIONE_CT_CUSTOMIZATION = "ct".equals(getString("ro.carrier.name"));
            if ((!IS_XIAOMI) && (!IS_GALAXYS_NEXUS) && (!IS_NEXUS_S) && (!IS_NEXUS_7))
                break label650;
            bool11 = true;
            SHOW_MAGNIFIER_WHEN_INPUT = bool11;
            if ((!IS_XIAOMI) && (!IS_GALAXYS_NEXUS) && (!IS_NEXUS_S) && (!IS_NEXUS_7))
                break label656;
            bool12 = true;
            IS_FULL_SOURCE_CODE_DEVICE = bool12;
            if ((TextUtils.isEmpty(Build.VERSION.INCREMENTAL)) || (!Build.VERSION.INCREMENTAL.matches("\\d+.\\d+.\\d+(.ALPHA)?")))
                break label662;
            bool13 = true;
            label508: IS_DEVELOPMENT_VERSION = bool13;
            if ((!"user".equals(TYPE)) || (IS_DEVELOPMENT_VERSION))
                break label668;
        }
        label644: label650: label656: label662: label668: for (boolean bool14 = true; ; bool14 = false)
        {
            IS_STABLE_VERSION = bool14;
            IS_MITWO_TW_BUILD = "aries_tw".equals(SystemProperties.get("ro.product.mod_device", ""));
            IS_MITWO_HK_BUILD = "aries_hk".equals(SystemProperties.get("ro.product.mod_device", ""));
            IS_TW_BUILD = IS_MITWO_TW_BUILD;
            IS_HK_BUILD = IS_MITWO_HK_BUILD;
            if ((IS_TW_BUILD) || (IS_HK_BUILD))
                bool1 = true;
            IS_INTERNATIONAL_BUILD = bool1;
            return;
            bool2 = false;
            break;
            label604: bool3 = false;
            break label109;
            label609: bool4 = false;
            break label138;
            label614: bool5 = false;
            break label255;
            label620: bool6 = false;
            break label275;
            label626: bool7 = false;
            break label319;
            label632: bool8 = false;
            break label363;
            label638: bool9 = false;
            break label383;
            bool10 = false;
            break label403;
            bool11 = false;
            break label448;
            bool12 = false;
            break label480;
            bool13 = false;
            break label508;
        }
    }

    private static String getString(String paramString)
    {
        return SystemProperties.get(paramString, "unknown");
    }

    public static boolean hasMeid()
    {
        if (!TextUtils.isEmpty(SystemProperties.get("ro.ril.oem.meid")));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static boolean isDevelopmentVersion()
    {
        return IS_DEVELOPMENT_VERSION;
    }

    public static boolean isMsm8660()
    {
        String str = SystemProperties.get("ro.soc.name");
        if ((TextUtils.equals(str, "msm8660")) || (TextUtils.equals(str, "unkown")));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static boolean isOfficialVersion()
    {
        if ((IS_DEVELOPMENT_VERSION) || (IS_STABLE_VERSION));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static boolean isStableVersion()
    {
        return IS_STABLE_VERSION;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.os.Build
 * JD-Core Version:        0.6.2
 */