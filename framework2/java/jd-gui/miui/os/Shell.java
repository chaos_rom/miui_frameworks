package miui.os;

public class Shell
{
    private static final String TAG = "Shell";

    static
    {
        System.loadLibrary("shell_jni");
    }

    public static boolean chmod(String paramString, int paramInt)
    {
        return nativeChmod(paramString, paramInt);
    }

    public static boolean chown(String paramString, int paramInt1, int paramInt2)
    {
        return nativeChown(paramString, paramInt1, paramInt2);
    }

    public static boolean copy(String paramString1, String paramString2)
    {
        return nativeCopy(paramString1, paramString2);
    }

    public static boolean link(String paramString1, String paramString2)
    {
        return nativeLink(paramString1, paramString2);
    }

    public static boolean mkdirs(String paramString)
    {
        return nativeMkdirs(paramString);
    }

    public static boolean move(String paramString1, String paramString2)
    {
        return nativeMove(paramString1, paramString2);
    }

    private static native boolean nativeChmod(String paramString, int paramInt);

    private static native boolean nativeChown(String paramString, int paramInt1, int paramInt2);

    private static native boolean nativeCopy(String paramString1, String paramString2);

    private static native boolean nativeLink(String paramString1, String paramString2);

    private static native boolean nativeMkdirs(String paramString);

    private static native boolean nativeMove(String paramString1, String paramString2);

    private static native boolean nativeRemove(String paramString);

    private static native boolean nativeRun(String paramString);

    public static boolean remove(String paramString)
    {
        return nativeRemove(paramString);
    }

    public static boolean run(String paramString, Object[] paramArrayOfObject)
    {
        if (paramArrayOfObject.length > 0);
        for (String str = String.format(paramString, paramArrayOfObject); ; str = paramString)
            return nativeRun(str);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.os.Shell
 * JD-Core Version:        0.6.2
 */