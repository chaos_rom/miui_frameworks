package miui.os;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.os.Process;
import android.os.StatFs;
import android.os.storage.StorageManager;
import android.os.storage.StorageVolume;
import android.text.TextUtils;
import java.io.File;
import java.util.HashMap;
import miui.provider.ExtraSettings.Secure;

public class Environment extends android.os.Environment
{
    private static final String EXTERNAL_STORAGE_DIRECTORY;
    private static final String EXTERNAL_STORAGE_DIRECTORY_WITH_ENDING_SLASH;
    private static final File EXTERNAL_STORAGE_MIUI_DIRECTORY;
    private static final File INTERNAL_STORAGE_DIRECTORY = new File("/data/sdcard");
    private static final File INTERNAL_STORAGE_MIUI_DIRECTORY;
    private static HashMap<String, Long> sDevice2Memory;
    private static long sTotalMemory;
    private static long sTotalPhysicalMemory;

    static
    {
        EXTERNAL_STORAGE_DIRECTORY = getExternalStorageDirectory().getPath();
        if (EXTERNAL_STORAGE_DIRECTORY.endsWith("/"));
        for (String str = EXTERNAL_STORAGE_DIRECTORY; ; str = EXTERNAL_STORAGE_DIRECTORY.concat("/"))
        {
            EXTERNAL_STORAGE_DIRECTORY_WITH_ENDING_SLASH = str;
            INTERNAL_STORAGE_MIUI_DIRECTORY = new File(INTERNAL_STORAGE_DIRECTORY, "MIUI");
            EXTERNAL_STORAGE_MIUI_DIRECTORY = new File(EXTERNAL_STORAGE_DIRECTORY, "MIUI");
            sDevice2Memory = new HashMap();
            sDevice2Memory.put("hwu9200", Long.valueOf(1048576L));
            sDevice2Memory.put("hwu9500", Long.valueOf(1048576L));
            sDevice2Memory.put("maguro", Long.valueOf(1048576L));
            sDevice2Memory.put("ville", Long.valueOf(1048576L));
            sDevice2Memory.put("LT26i", Long.valueOf(1048576L));
            sDevice2Memory.put("ventana", Long.valueOf(1048576L));
            sDevice2Memory.put("stuttgart", Long.valueOf(1048576L));
            sDevice2Memory.put("t03g", Long.valueOf(2097152L));
            return;
        }
    }

    public static File getInternalStorageDirectory()
    {
        return INTERNAL_STORAGE_DIRECTORY;
    }

    public static File getMIUIExternalStorageDirectory()
    {
        return EXTERNAL_STORAGE_MIUI_DIRECTORY;
    }

    public static File getMIUIInternalStorageDirectory()
    {
        return INTERNAL_STORAGE_MIUI_DIRECTORY;
    }

    public static File getMIUIStorageDirectory()
    {
        if (isExternalStorageMounted());
        for (File localFile = EXTERNAL_STORAGE_MIUI_DIRECTORY; ; localFile = INTERNAL_STORAGE_MIUI_DIRECTORY)
            return localFile;
    }

    public static File getStorageDirectory()
    {
        if (isExternalStorageMounted());
        for (File localFile = getExternalStorageDirectory(); ; localFile = INTERNAL_STORAGE_DIRECTORY)
            return localFile;
    }

    public static long getTotalMemory()
    {
        if (sTotalMemory == 0L);
        try
        {
            String[] arrayOfString = new String[1];
            arrayOfString[0] = "MemTotal:";
            long[] arrayOfLong = new long[arrayOfString.length];
            Process.readProcLines("/proc/meminfo", arrayOfString, arrayOfLong);
            sTotalMemory = arrayOfLong[0];
            label36: return sTotalMemory;
        }
        catch (Exception localException)
        {
            break label36;
        }
    }

    public static long getTotalPhysicalMemory()
    {
        if (sTotalPhysicalMemory == 0L)
            if (!sDevice2Memory.containsKey(Build.DEVICE))
                break label42;
        label42: for (sTotalPhysicalMemory = ((Long)sDevice2Memory.get(Build.DEVICE)).longValue(); ; sTotalPhysicalMemory = 1024L * (256L * (1L + getTotalMemory() / 262144L)))
            return sTotalPhysicalMemory;
    }

    public static void init(File paramFile1, File paramFile2)
    {
        ExtraFileUtils.mkdirs(new File(paramFile1, "customized_icons"), 1023, -1, -1);
        ExtraFileUtils.mkdirs(new File(paramFile2, "sdcard"), 511, -1, -1);
    }

    public static boolean isExternalPath(String paramString)
    {
        if ((paramString != null) && ((paramString.startsWith("/sdcard")) || (paramString.startsWith("/storage")) || (paramString.startsWith(getExternalStorageDirectory().getPath()))));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static boolean isExternalStorageMounted()
    {
        return "mounted".equals(getExternalStorageState());
    }

    public static boolean isExternalStoragePath(String paramString)
    {
        boolean bool = false;
        if (paramString == null);
        while (true)
        {
            return bool;
            String str = paramString.concat("/");
            if ((str.startsWith("/sdcard/")) || (str.startsWith(EXTERNAL_STORAGE_DIRECTORY_WITH_ENDING_SLASH)))
                bool = true;
        }
    }

    public static boolean isExternalStorageStateChanged(Intent paramIntent)
    {
        StorageVolume localStorageVolume = (StorageVolume)paramIntent.getParcelableExtra("storage_volume");
        if (localStorageVolume != null);
        for (boolean bool = isExternalStoragePath(localStorageVolume.getPath()); ; bool = false)
            return bool;
    }

    public static boolean isInternalStorageLow(ContentResolver paramContentResolver)
    {
        boolean bool = true;
        try
        {
            StatFs localStatFs = new StatFs(INTERNAL_STORAGE_DIRECTORY.getPath());
            long l1 = localStatFs.getAvailableBlocks() * localStatFs.getBlockSize();
            long l2 = ExtraSettings.Secure.getStorageThreshold(paramContentResolver);
            if (l1 < l2);
            while (true)
            {
                label43: return bool;
                bool = false;
            }
        }
        catch (Exception localException)
        {
            break label43;
        }
    }

    public static boolean isStorageUsb(Context paramContext, String paramString)
    {
        boolean bool = false;
        if (TextUtils.isEmpty(paramString));
        label81: 
        while (true)
        {
            return bool;
            StorageVolume[] arrayOfStorageVolume = ((StorageManager)paramContext.getSystemService("storage")).getVolumeList();
            if (arrayOfStorageVolume != null)
                for (int i = 0; ; i++)
                {
                    if (i >= arrayOfStorageVolume.length)
                        break label81;
                    if (arrayOfStorageVolume[i].getPath().equals(paramString))
                    {
                        if (!paramContext.getString(101450232).equals(arrayOfStorageVolume[i].getDescription(paramContext)))
                            break;
                        bool = true;
                        break;
                    }
                }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.os.Environment
 * JD-Core Version:        0.6.2
 */