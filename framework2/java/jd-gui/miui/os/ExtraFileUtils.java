package miui.os;

import android.os.FileUtils;
import android.text.TextUtils;
import java.io.File;
import java.io.IOException;

public class ExtraFileUtils
{
    public static void addNoMedia(String paramString)
    {
        File localFile = new File(paramString);
        if (localFile.isDirectory());
        try
        {
            new File(localFile, ".nomedia").createNewFile();
            label30: return;
        }
        catch (IOException localIOException)
        {
            break label30;
        }
    }

    public static boolean deleteDir(File paramFile)
    {
        boolean bool1 = true;
        String[] arrayOfString;
        if (paramFile.isDirectory())
        {
            arrayOfString = paramFile.list();
            if (arrayOfString != null);
        }
        for (boolean bool2 = false; ; bool2 = bool1)
        {
            return bool2;
            for (int i = 0; i < arrayOfString.length; i++)
                if (!deleteDir(new File(paramFile, arrayOfString[i])))
                    bool1 = false;
            if (!paramFile.delete())
                bool1 = false;
        }
    }

    public static String getExtension(File paramFile)
    {
        if (paramFile == null);
        for (String str = ""; ; str = getExtension(paramFile.getName()))
            return str;
    }

    public static String getExtension(String paramString)
    {
        String str;
        if (TextUtils.isEmpty(paramString))
            str = "";
        while (true)
        {
            return str;
            int i = paramString.lastIndexOf(".");
            if (i > -1)
                str = paramString.substring(i + 1);
            else
                str = "";
        }
    }

    public static String getFileName(String paramString)
    {
        if (TextUtils.isEmpty(paramString))
            paramString = "";
        while (true)
        {
            return paramString;
            int i = paramString.lastIndexOf(File.separator);
            if (i > -1)
                paramString = paramString.substring(i + 1);
        }
    }

    public static String getFileTitle(File paramFile)
    {
        if (paramFile == null);
        for (String str = ""; ; str = getFileTitle(paramFile.getName()))
            return str;
    }

    public static String getFileTitle(String paramString)
    {
        if (TextUtils.isEmpty(paramString))
            paramString = "";
        while (true)
        {
            return paramString;
            int i = paramString.lastIndexOf(".");
            if (i > -1)
                paramString = paramString.substring(0, i);
        }
    }

    public static String getParentFolderPath(String paramString)
    {
        if (TextUtils.isEmpty(paramString))
            paramString = "";
        while (true)
        {
            return paramString;
            int i = paramString.lastIndexOf(File.separator);
            if (i > -1)
                paramString = paramString.substring(0, i);
        }
    }

    public static boolean mkdirs(File paramFile, int paramInt1, int paramInt2, int paramInt3)
    {
        boolean bool = false;
        if (paramFile.exists());
        while (true)
        {
            return bool;
            String str = paramFile.getParent();
            if (str != null)
                mkdirs(new File(str), paramInt1, paramInt2, paramInt3);
            if (paramFile.mkdir())
            {
                FileUtils.setPermissions(paramFile.getPath(), paramInt1, paramInt2, paramInt3);
                bool = true;
            }
        }
    }

    public static String standardizeFolderPath(String paramString)
    {
        if (paramString.endsWith(File.separator));
        while (true)
        {
            return paramString;
            paramString = paramString + File.separator;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.os.ExtraFileUtils
 * JD-Core Version:        0.6.2
 */