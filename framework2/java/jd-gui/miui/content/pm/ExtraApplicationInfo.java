package miui.content.pm;

public class ExtraApplicationInfo
{
    public static final int FLAG_ACCESS_CONTROL_HIDE = 33554432;
    public static final int FLAG_ACCESS_CONTROL_PASSWORD = 67108864;
    public static final int FLAG_COMPATIBILITY_MODE = 134217728;
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.content.pm.ExtraApplicationInfo
 * JD-Core Version:        0.6.2
 */