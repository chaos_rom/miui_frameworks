package miui.content.res;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import android.util.Log;
import java.io.File;
import miui.util.ImageUtils;
import miui.util.InputStreamLoader;

public class ThemeResources
{
    public static final String ADVANCE_LOCKSCREEN_NAME = "advance";
    static boolean DBG = false;
    public static final String FRAMEWORK_NAME = "framework-res";
    public static final String FRAMEWORK_PACKAGE = "android";
    public static final String ICONS_NAME = "icons";
    public static final String LANGUAGE_THEME_PATH = "/data/system/language/";
    public static final String LOCKSCREEN_NAME = "lockscreen";
    public static final String LOCKSCREEN_WALLPAPER_NAME = "lock_wallpaper";
    public static final String MIUI_NAME = "framework-miui-res";
    public static final String MIUI_PACKAGE = "miui";
    public static final String SYSTEMUI_NAME = "com.android.systemui";
    public static final String SYSTEM_LANGUAGE_THEME_PATH = "/system/language/";
    public static final String SYSTEM_THEME_PATH = "/system/media/theme/default/";
    public static final String THEME_PATH = "/data/system/theme/";
    public static final MetaData[] THEME_PATHS = arrayOfMetaData;
    public static final String WALLPAPER_NAME = "wallpaper";
    public static final String sAppliedLockstyleConfigPath = "/data/system/theme/" + File.separator + "config.config";
    private static Drawable sLockWallpaperCache;
    private static long sLockWallpaperModifiedTime;
    private static ThemeResourcesSystem sSystem;
    protected boolean mHasValue;
    protected boolean mHasWrapped;
    protected ThemeZipFile mPackageZipFile;
    protected Resources mResources;
    protected boolean mSupportWrapper;
    protected ThemeResources mWrapped;

    static
    {
        MetaData[] arrayOfMetaData = new MetaData[2];
        arrayOfMetaData[0] = new MetaData("/system/media/theme/default/", true, true, true);
        arrayOfMetaData[1] = new MetaData("/data/system/theme/", true, true, true);
    }

    protected ThemeResources(ThemeResources paramThemeResources, Resources paramResources, String paramString, MetaData paramMetaData)
    {
        this.mWrapped = paramThemeResources;
        this.mResources = paramResources;
        this.mPackageZipFile = ThemeZipFile.getThemeZipFile(paramMetaData, paramString, paramResources);
        if (!"icons".equals(paramString));
        for (boolean bool = true; ; bool = false)
        {
            this.mSupportWrapper = bool;
            checkUpdate();
            return;
        }
    }

    public static final void clearLockWallpaperCache()
    {
        sLockWallpaperModifiedTime = 0L;
        sLockWallpaperCache = null;
    }

    public static final Drawable getLockWallpaperCache(Context paramContext)
    {
        Drawable localDrawable = null;
        File localFile = sSystem.getLockscreenWallpaper();
        if ((localFile == null) || (!localFile.exists()));
        while (true)
        {
            return localDrawable;
            if (sLockWallpaperModifiedTime == localFile.lastModified())
            {
                localDrawable = sLockWallpaperCache;
                continue;
            }
            sLockWallpaperModifiedTime = localFile.lastModified();
            sLockWallpaperCache = null;
            try
            {
                DisplayMetrics localDisplayMetrics = Resources.getSystem().getDisplayMetrics();
                int i = localDisplayMetrics.widthPixels;
                int j = localDisplayMetrics.heightPixels;
                if (i > j)
                {
                    Log.e("LockWallpaper", "Wrong display metrics for width = " + i + " and height = " + j);
                    int k = i;
                    i = j;
                    j = k;
                }
                Bitmap localBitmap = ImageUtils.getBitmap(new InputStreamLoader(localFile.getAbsolutePath()), i, j);
                sLockWallpaperCache = new BitmapDrawable(paramContext.getResources(), localBitmap);
                label164: localDrawable = sLockWallpaperCache;
            }
            catch (OutOfMemoryError localOutOfMemoryError)
            {
                break label164;
            }
            catch (Exception localException)
            {
                break label164;
            }
        }
    }

    public static ThemeResources getSystem(Resources paramResources)
    {
        if (sSystem == null)
            sSystem = ThemeResourcesSystem.getTopLevelThemeResources(paramResources);
        return sSystem;
    }

    public static ThemeResourcesSystem getSystem()
    {
        return sSystem;
    }

    public static ThemeResources getTopLevelThemeResources(Resources paramResources, String paramString)
    {
        Object localObject = null;
        int i = 0;
        while (i < THEME_PATHS.length)
        {
            ThemeResources localThemeResources = new ThemeResources((ThemeResources)localObject, paramResources, paramString, THEME_PATHS[i]);
            i++;
            localObject = localThemeResources;
        }
        return localObject;
    }

    public boolean checkUpdate()
    {
        boolean bool1 = this.mPackageZipFile.checkUpdate();
        boolean bool2;
        if ((this.mWrapped != null) && ((this.mSupportWrapper) || (!this.mPackageZipFile.exists())))
        {
            bool2 = true;
            this.mHasWrapped = bool2;
            if (this.mHasWrapped)
                if ((!this.mWrapped.checkUpdate()) && (!bool1))
                    break label77;
        }
        label77: for (bool1 = true; ; bool1 = false)
        {
            this.mHasValue = hasValuesInner();
            return bool1;
            bool2 = false;
            break;
        }
    }

    public boolean containsEntry(String paramString)
    {
        boolean bool = this.mPackageZipFile.containsEntry(paramString);
        if ((!bool) && (!this.mPackageZipFile.exists()) && (this.mWrapped != null))
            bool = this.mWrapped.containsEntry(paramString);
        return bool;
    }

    public CharSequence getThemeCharSequence(int paramInt)
    {
        return getThemeCharSequenceInner(paramInt);
    }

    protected CharSequence getThemeCharSequenceInner(int paramInt)
    {
        CharSequence localCharSequence = this.mPackageZipFile.getThemeCharSequence(paramInt);
        if ((localCharSequence == null) && (this.mHasWrapped))
            localCharSequence = this.mWrapped.getThemeCharSequenceInner(paramInt);
        return localCharSequence;
    }

    public ThemeZipFile.ThemeFileInfo getThemeFileStream(int paramInt, String paramString)
    {
        return getThemeFileStream(paramString);
    }

    public ThemeZipFile.ThemeFileInfo getThemeFileStream(String paramString)
    {
        return getThemeFileStreamInner(paramString);
    }

    protected ThemeZipFile.ThemeFileInfo getThemeFileStreamInner(String paramString)
    {
        ThemeZipFile.ThemeFileInfo localThemeFileInfo = this.mPackageZipFile.getInputStream(paramString);
        if ((localThemeFileInfo == null) && (this.mHasWrapped))
            localThemeFileInfo = this.mWrapped.getThemeFileStreamInner(paramString);
        return localThemeFileInfo;
    }

    public Integer getThemeInt(int paramInt)
    {
        return getThemeIntInner(paramInt);
    }

    protected Integer getThemeIntInner(int paramInt)
    {
        Integer localInteger = this.mPackageZipFile.getThemeInt(paramInt);
        if ((localInteger == null) && (this.mHasWrapped))
            localInteger = this.mWrapped.getThemeIntInner(paramInt);
        return localInteger;
    }

    public boolean hasValues()
    {
        return this.mHasValue;
    }

    protected boolean hasValuesInner()
    {
        if ((this.mPackageZipFile.hasValues()) || ((this.mHasWrapped) && (this.mWrapped.hasValuesInner())));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    protected static final class MetaData
    {
        public boolean supportCharSequence;
        public boolean supportFile;
        public boolean supportInt;
        public String themePath;

        public MetaData(String paramString, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
        {
            this.themePath = paramString;
            this.supportInt = paramBoolean1;
            this.supportCharSequence = paramBoolean2;
            this.supportFile = paramBoolean3;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.content.res.ThemeResources
 * JD-Core Version:        0.6.2
 */