package miui.content.res;

import android.app.MiuiThemeHelper;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseArray;
import com.android.internal.util.XmlUtils;
import java.io.File;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import miui.util.DisplayUtils;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public final class ThemeZipFile
{
    private static final String ALTERNATIVE_THEME_VALUE_FILE = "theme_values%s.xml";
    private static final String ATTR_NAME = "name";
    private static final String ATTR_PACKAGE = "package";
    static boolean DBG = false;
    private static final String FUZZY_SEARCH_ICON_SUFFIX = "#*.png";
    static String TAG = "ThemeZipFile";
    private static final String TAG_BOOLEAN = "bool";
    private static final String TAG_COLOR = "color";
    private static final String TAG_DIMEN = "dimen";
    private static final String TAG_DRAWABLE = "drawable";
    private static final String TAG_INTEGER = "integer";
    private static final String TAG_STRING = "string";
    public static final String THEME_VALUE_FILE = "theme_values.xml";
    private static final String TRUE = "true";
    private static final int[] sDensities = DisplayUtils.getBestDensityOrder(sDensity);
    private static final int sDensity = DisplayMetrics.DENSITY_DEVICE;
    protected static final Map<String, WeakReference<ThemeZipFile>> sThemeZipFiles = new HashMap();
    private SparseArray<CharSequence> mCharSequences = new SparseArray();
    private SparseArray<Integer> mIntegers = new SparseArray();
    private long mLastModifyTime = -1L;
    private ThemeResources.MetaData mMetaData;
    private String mPackageName;
    private String mPath;
    private Resources mResources;
    private ZipFile mZipFile;

    public ThemeZipFile(String paramString1, ThemeResources.MetaData paramMetaData, String paramString2, Resources paramResources)
    {
        if (DBG)
            Log.d(TAG, "create ThemeZipFile for " + paramString1);
        this.mResources = paramResources;
        this.mPackageName = paramString2;
        this.mPath = paramString1;
        this.mMetaData = paramMetaData;
    }

    private void clean()
    {
        if (DBG)
            Log.d(TAG, "clean for " + this.mPath);
        if (this.mZipFile != null);
        try
        {
            this.mZipFile.close();
            label49: this.mZipFile = null;
            this.mIntegers.clear();
            this.mCharSequences.clear();
            return;
        }
        catch (Exception localException)
        {
            break label49;
        }
    }

    private ThemeFileInfo getInputStreamInner(String paramString)
    {
        ThemeFileInfo localThemeFileInfo1 = getZipInputStream(paramString);
        ThemeFileInfo localThemeFileInfo2;
        if (localThemeFileInfo1 != null)
            localThemeFileInfo2 = localThemeFileInfo1;
        while (true)
        {
            return localThemeFileInfo2;
            int i = paramString.indexOf("dpi/");
            if (i > 0)
            {
                String str1 = paramString.substring(i + 3);
                while (paramString.charAt(i) != '-')
                    i--;
                String str2 = paramString.substring(0, i);
                for (int j = 0; ; j++)
                {
                    if (j >= sDensities.length)
                        break label151;
                    Object[] arrayOfObject = new Object[3];
                    arrayOfObject[0] = str2;
                    arrayOfObject[1] = DisplayUtils.getDensitySuffix(sDensities[j]);
                    arrayOfObject[2] = str1;
                    localThemeFileInfo1 = getZipInputStream(String.format("%s%s%s", arrayOfObject));
                    if (localThemeFileInfo1 != null)
                    {
                        if (sDensities[j] > 1)
                            localThemeFileInfo1.mDensity = sDensities[j];
                        localThemeFileInfo2 = localThemeFileInfo1;
                        break;
                    }
                }
            }
            label151: localThemeFileInfo2 = localThemeFileInfo1;
        }
    }

    private static final String getPackageName(String paramString)
    {
        if (("framework-res".equals(paramString)) || ("icons".equals(paramString)));
        for (paramString = "android"; ; paramString = "miui")
            do
                return paramString;
            while ((!"framework-miui-res".equals(paramString)) && (!"lockscreen".equals(paramString)));
    }

    // ERROR //
    protected static ThemeZipFile getThemeZipFile(ThemeResources.MetaData paramMetaData, String paramString, Resources paramResources)
    {
        // Byte code:
        //     0: new 118	java/lang/StringBuilder
        //     3: dup
        //     4: invokespecial 119	java/lang/StringBuilder:<init>	()V
        //     7: aload_0
        //     8: getfield 219	miui/content/res/ThemeResources$MetaData:themePath	Ljava/lang/String;
        //     11: invokevirtual 125	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     14: aload_1
        //     15: invokevirtual 125	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     18: invokevirtual 129	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     21: astore_3
        //     22: getstatic 103	miui/content/res/ThemeZipFile:sThemeZipFiles	Ljava/util/Map;
        //     25: aload_3
        //     26: invokeinterface 225 2 0
        //     31: checkcast 227	java/lang/ref/WeakReference
        //     34: astore 4
        //     36: aload 4
        //     38: ifnull +128 -> 166
        //     41: aload 4
        //     43: invokevirtual 230	java/lang/ref/WeakReference:get	()Ljava/lang/Object;
        //     46: checkcast 2	miui/content/res/ThemeZipFile
        //     49: astore 5
        //     51: aload 5
        //     53: ifnonnull +110 -> 163
        //     56: getstatic 103	miui/content/res/ThemeZipFile:sThemeZipFiles	Ljava/util/Map;
        //     59: astore 6
        //     61: aload 6
        //     63: monitorenter
        //     64: getstatic 103	miui/content/res/ThemeZipFile:sThemeZipFiles	Ljava/util/Map;
        //     67: aload_3
        //     68: invokeinterface 225 2 0
        //     73: checkcast 227	java/lang/ref/WeakReference
        //     76: astore 8
        //     78: aload 8
        //     80: ifnull +92 -> 172
        //     83: aload 8
        //     85: invokevirtual 230	java/lang/ref/WeakReference:get	()Ljava/lang/Object;
        //     88: checkcast 2	miui/content/res/ThemeZipFile
        //     91: astore 5
        //     93: aload 5
        //     95: ifnonnull +46 -> 141
        //     98: new 2	miui/content/res/ThemeZipFile
        //     101: dup
        //     102: aload_3
        //     103: aload_0
        //     104: aload_1
        //     105: invokestatic 232	miui/content/res/ThemeZipFile:getPackageName	(Ljava/lang/String;)Ljava/lang/String;
        //     108: aload_2
        //     109: invokespecial 234	miui/content/res/ThemeZipFile:<init>	(Ljava/lang/String;Lmiui/content/res/ThemeResources$MetaData;Ljava/lang/String;Landroid/content/res/Resources;)V
        //     112: astore 9
        //     114: new 227	java/lang/ref/WeakReference
        //     117: dup
        //     118: aload 9
        //     120: invokespecial 237	java/lang/ref/WeakReference:<init>	(Ljava/lang/Object;)V
        //     123: astore 10
        //     125: getstatic 103	miui/content/res/ThemeZipFile:sThemeZipFiles	Ljava/util/Map;
        //     128: aload_3
        //     129: aload 10
        //     131: invokeinterface 241 3 0
        //     136: pop
        //     137: aload 9
        //     139: astore 5
        //     141: aload 6
        //     143: monitorexit
        //     144: goto +19 -> 163
        //     147: aload 6
        //     149: monitorexit
        //     150: aload 7
        //     152: athrow
        //     153: astore 7
        //     155: goto -8 -> 147
        //     158: astore 7
        //     160: goto -13 -> 147
        //     163: aload 5
        //     165: areturn
        //     166: aconst_null
        //     167: astore 5
        //     169: goto -118 -> 51
        //     172: aconst_null
        //     173: astore 5
        //     175: goto -82 -> 93
        //     178: astore 7
        //     180: goto -33 -> 147
        //
        // Exception table:
        //     from	to	target	type
        //     114	125	153	finally
        //     125	137	158	finally
        //     64	114	178	finally
        //     141	150	178	finally
    }

    private ThemeFileInfo getZipInputStream(String paramString)
    {
        ThemeFileInfo localThemeFileInfo = null;
        if (!isValid());
        while (true)
        {
            return localThemeFileInfo;
            Object localObject = null;
            try
            {
                ZipEntry localZipEntry2;
                if (paramString.endsWith("#*.png"))
                {
                    String str = paramString.substring(0, paramString.length() - "#*.png".length());
                    Enumeration localEnumeration = this.mZipFile.entries();
                    do
                    {
                        if (!localEnumeration.hasMoreElements())
                            break;
                        localZipEntry2 = (ZipEntry)localEnumeration.nextElement();
                    }
                    while ((localZipEntry2.isDirectory()) || (!localZipEntry2.getName().startsWith(str)));
                }
                ZipEntry localZipEntry1;
                for (localObject = localZipEntry2; localObject != null; localObject = localZipEntry1)
                {
                    InputStream localInputStream = this.mZipFile.getInputStream(localObject);
                    if (localInputStream == null)
                        break;
                    localThemeFileInfo = new ThemeFileInfo(localInputStream, localObject.getSize());
                    break;
                    localZipEntry1 = this.mZipFile.getEntry(paramString);
                }
            }
            catch (Exception localException)
            {
            }
        }
    }

    private boolean isValid()
    {
        if (this.mZipFile != null);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    // ERROR //
    private void loadThemeValues()
    {
        // Byte code:
        //     0: getstatic 77	miui/content/res/ThemeZipFile:DBG	Z
        //     3: ifeq +33 -> 36
        //     6: getstatic 81	miui/content/res/ThemeZipFile:TAG	Ljava/lang/String;
        //     9: new 118	java/lang/StringBuilder
        //     12: dup
        //     13: invokespecial 119	java/lang/StringBuilder:<init>	()V
        //     16: ldc_w 298
        //     19: invokevirtual 125	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     22: aload_0
        //     23: getfield 141	miui/content/res/ThemeZipFile:mPath	Ljava/lang/String;
        //     26: invokevirtual 125	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     29: invokevirtual 129	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     32: invokestatic 135	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
        //     35: pop
        //     36: bipush 255
        //     38: getstatic 96	miui/content/res/ThemeZipFile:sDensities	[I
        //     41: arraylength
        //     42: iadd
        //     43: istore_1
        //     44: iload_1
        //     45: iflt +139 -> 184
        //     48: iconst_1
        //     49: anewarray 4	java/lang/Object
        //     52: astore_2
        //     53: aload_2
        //     54: iconst_0
        //     55: getstatic 96	miui/content/res/ThemeZipFile:sDensities	[I
        //     58: iload_1
        //     59: iaload
        //     60: invokestatic 185	miui/util/DisplayUtils:getDensitySuffix	(I)Ljava/lang/String;
        //     63: aastore
        //     64: aload_0
        //     65: ldc 11
        //     67: aload_2
        //     68: invokestatic 191	java/lang/String:format	(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
        //     71: invokespecial 163	miui/content/res/ThemeZipFile:getZipInputStream	(Ljava/lang/String;)Lmiui/content/res/ThemeZipFile$ThemeFileInfo;
        //     74: astore_3
        //     75: aload_3
        //     76: ifnull +52 -> 128
        //     79: aload_3
        //     80: getfield 302	miui/content/res/ThemeZipFile$ThemeFileInfo:mInput	Ljava/io/InputStream;
        //     83: astore 4
        //     85: invokestatic 308	org/xmlpull/v1/XmlPullParserFactory:newInstance	()Lorg/xmlpull/v1/XmlPullParserFactory;
        //     88: invokevirtual 312	org/xmlpull/v1/XmlPullParserFactory:newPullParser	()Lorg/xmlpull/v1/XmlPullParser;
        //     91: astore 9
        //     93: new 314	java/io/BufferedInputStream
        //     96: dup
        //     97: aload 4
        //     99: sipush 8192
        //     102: invokespecial 317	java/io/BufferedInputStream:<init>	(Ljava/io/InputStream;I)V
        //     105: astore 10
        //     107: aload 9
        //     109: aload 10
        //     111: aconst_null
        //     112: invokeinterface 323 3 0
        //     117: aload_0
        //     118: aload 9
        //     120: invokespecial 327	miui/content/res/ThemeZipFile:mergeThemeValues	(Lorg/xmlpull/v1/XmlPullParser;)V
        //     123: aload 10
        //     125: invokevirtual 330	java/io/InputStream:close	()V
        //     128: iinc 1 255
        //     131: goto -87 -> 44
        //     134: astore 12
        //     136: aload 12
        //     138: invokevirtual 333	java/io/IOException:printStackTrace	()V
        //     141: goto -13 -> 128
        //     144: astore 7
        //     146: aload 4
        //     148: invokevirtual 330	java/io/InputStream:close	()V
        //     151: goto -23 -> 128
        //     154: astore 8
        //     156: aload 8
        //     158: invokevirtual 333	java/io/IOException:printStackTrace	()V
        //     161: goto -33 -> 128
        //     164: astore 5
        //     166: aload 4
        //     168: invokevirtual 330	java/io/InputStream:close	()V
        //     171: aload 5
        //     173: athrow
        //     174: astore 6
        //     176: aload 6
        //     178: invokevirtual 333	java/io/IOException:printStackTrace	()V
        //     181: goto -10 -> 171
        //     184: return
        //     185: astore 5
        //     187: aload 10
        //     189: astore 4
        //     191: goto -25 -> 166
        //     194: astore 11
        //     196: aload 10
        //     198: astore 4
        //     200: goto -54 -> 146
        //
        // Exception table:
        //     from	to	target	type
        //     123	128	134	java/io/IOException
        //     85	107	144	org/xmlpull/v1/XmlPullParserException
        //     146	151	154	java/io/IOException
        //     85	107	164	finally
        //     166	171	174	java/io/IOException
        //     107	123	185	finally
        //     107	123	194	org/xmlpull/v1/XmlPullParserException
    }

    private void mergeThemeValues(XmlPullParser paramXmlPullParser)
    {
        try
        {
            int i;
            do
                i = paramXmlPullParser.next();
            while ((i != 2) && (i != 1));
            if (i == 2)
                break label39;
            throw new XmlPullParserException("No start tag found");
        }
        catch (Exception localException)
        {
            localException.printStackTrace();
        }
        return;
        label39: String str1;
        int k;
        label98: String str4;
        int m;
        do
        {
            do
            {
                String str2;
                String str3;
                do
                {
                    int j;
                    do
                        j = paramXmlPullParser.next();
                    while ((j != 2) && (j != 1));
                    if (j == 1)
                        break;
                    str1 = paramXmlPullParser.getName().trim();
                    if (str1 == null)
                        break;
                    str2 = null;
                    str3 = null;
                    k = -1 + paramXmlPullParser.getAttributeCount();
                    if (k >= 0)
                    {
                        String str5 = paramXmlPullParser.getAttributeName(k).trim();
                        if (str5.equals("name"))
                        {
                            str2 = paramXmlPullParser.getAttributeValue(k);
                            break label475;
                        }
                        if (!str5.equals("package"))
                            break label475;
                        str3 = paramXmlPullParser.getAttributeValue(k);
                        break label475;
                    }
                    str4 = paramXmlPullParser.nextText();
                }
                while ((str2 == null) || (str4 == null) || (str4.length() == 0));
                Resources localResources = this.mResources;
                if (str3 == null)
                    str3 = this.mPackageName;
                m = localResources.getIdentifier(str2, str1, str3);
            }
            while (m <= 0);
            if (!str1.equals("bool"))
                break label292;
        }
        while ((!this.mMetaData.supportInt) || (this.mIntegers.indexOfKey(m) >= 0));
        SparseArray localSparseArray = this.mIntegers;
        if ("true".equals(str4.trim()));
        for (int n = 1; ; n = 0)
        {
            localSparseArray.put(m, Integer.valueOf(n));
            break;
            label292: if ((str1.equals("color")) || (str1.equals("integer")) || (str1.equals("drawable")))
            {
                if ((!this.mMetaData.supportInt) || (this.mIntegers.indexOfKey(m) >= 0))
                    break;
                this.mIntegers.put(m, Integer.valueOf(XmlUtils.convertValueToUnsignedInt(str4.trim(), 0)));
                break;
            }
            if (str1.equals("string"))
            {
                if ((!this.mMetaData.supportCharSequence) || (this.mCharSequences.indexOfKey(m) >= 0))
                    break;
                this.mCharSequences.put(m, str4);
                break;
            }
            if ((!str1.equals("dimen")) || (!this.mMetaData.supportInt) || (this.mIntegers.indexOfKey(m) >= 0))
                break;
            Integer localInteger = MiuiThemeHelper.parseDimension(str4.toString());
            if (localInteger == null)
                break;
            this.mIntegers.put(m, localInteger);
            break;
            label475: k--;
            break label98;
        }
    }

    private void openZipFile()
    {
        if (DBG)
            Log.d(TAG, "openZipFile for " + this.mPath);
        File localFile = new File(this.mPath);
        this.mLastModifyTime = localFile.lastModified();
        if (this.mLastModifyTime == 0L);
        while (true)
        {
            return;
            try
            {
                this.mZipFile = new ZipFile(localFile);
            }
            catch (Exception localException)
            {
                this.mZipFile = null;
            }
        }
    }

    public boolean checkUpdate()
    {
        if (DBG)
            Log.d(TAG, "checkUpdate for " + this.mPath);
        long l = new File(this.mPath).lastModified();
        boolean bool;
        if (this.mLastModifyTime != l)
            try
            {
                if (this.mLastModifyTime != l)
                {
                    clean();
                    openZipFile();
                    loadThemeValues();
                    bool = true;
                }
                else
                {
                    break label104;
                }
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        while (true)
        {
            return bool;
            label104: bool = false;
        }
    }

    public boolean containsEntry(String paramString)
    {
        if ((isValid()) && (this.mZipFile.getEntry(paramString) != null));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean exists()
    {
        return new File(this.mPath).exists();
    }

    public ThemeFileInfo getInputStream(String paramString)
    {
        if (this.mMetaData.supportFile);
        for (ThemeFileInfo localThemeFileInfo = getInputStreamInner(paramString); ; localThemeFileInfo = null)
            return localThemeFileInfo;
    }

    public CharSequence getThemeCharSequence(int paramInt)
    {
        return (CharSequence)this.mCharSequences.get(paramInt);
    }

    public Integer getThemeInt(int paramInt)
    {
        return (Integer)this.mIntegers.get(paramInt);
    }

    public boolean hasValues()
    {
        if ((this.mIntegers.size() > 0) || (this.mCharSequences.size() > 0));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public class ThemeFileInfo
    {
        public int mDensity;
        public InputStream mInput;
        public long mSize;

        ThemeFileInfo(InputStream paramLong, long arg3)
        {
            this.mInput = paramLong;
            Object localObject;
            this.mSize = localObject;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.content.res.ThemeZipFile
 * JD-Core Version:        0.6.2
 */