package miui.content.res;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.PaintFlagsDrawFilter;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.PaintDrawable;
import android.os.SystemProperties;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import java.io.File;
import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import miui.os.Shell;
import miui.util.DisplayUtils;

public class IconCustomizer
{
    private static final int sAlphaShift = 24;
    private static HashMap<String, SoftReference<Bitmap>> sCache;
    private static final Canvas sCanvas;
    private static final int sColorShift = 8;
    public static final int sCustomizedIconHeight = 0;
    public static final int sCustomizedIconWidth = 0;
    private static final int[] sDensities;
    private static final int sDensity = 0;
    private static Boolean sExcludeAll;
    private static Set<String> sExcludes;
    private static final int sIconHeight = 0;
    private static Map<String, String> sIconMapping;
    private static final int sIconWidth = 0;
    private static final Rect sOldBounds;
    private static final String sPathPrefix = "/data/system/customized_icons/";
    private static final int sRGBMask = 16777215;
    private static final Resources sSystemResource;

    static
    {
        int i = 1;
        sSystemResource = Resources.getSystem();
        sDensity = sSystemResource.getDisplayMetrics().densityDpi;
        sDensities = DisplayUtils.getBestDensityOrder(sDensity);
        sIconWidth = scalePixel(72);
        sIconHeight = scalePixel(72);
        sCustomizedIconWidth = scalePixel(90);
        sCustomizedIconHeight = scalePixel(90);
        sOldBounds = new Rect();
        sCanvas = new Canvas();
        sCanvas.setDrawFilter(new PaintFlagsDrawFilter(4, 2));
        sCache = new HashMap();
        if (SystemProperties.getInt("sys.ui.app-icon-background", i) != i);
        while (true)
        {
            sExcludeAll = Boolean.valueOf(i);
            sIconMapping = new HashMap();
            sIconMapping.put("com.android.contacts.activities.TwelveKeyDialer.png", "com.android.contacts.TwelveKeyDialer.png");
            sIconMapping.put("com.miui.weather2.png", "com.miui.weather.png");
            sIconMapping.put("com.miui.gallery.png", "com.android.gallery.png");
            sIconMapping.put("com.android.gallery3d.png", "com.cooliris.media.png");
            sIconMapping.put("com.xiaomi.market.png", "com.miui.supermarket.png");
            sIconMapping.put("com.wali.miui.networkassistant.png", "com.android.monitor.png");
            sIconMapping.put("com.android.camera.CameraEntry.png", "com.miui.camera.png");
            sIconMapping.put("com.htc.album.png", "com.miui.gallery.png");
            sIconMapping.put("com.htc.fm.activity.FMRadioMain.png", "com.miui.fmradio.png");
            sIconMapping.put("com.htc.fm.FMRadio.png", "com.miui.fmradio.png");
            sIconMapping.put("com.sec.android.app.camera.Camera.png", "com.miui.camera.png");
            sIconMapping.put("com.sec.android.app.fm.png", "com.miui.fmradio.png");
            sIconMapping.put("com.android.hwcamera.png", "com.miui.camera.png");
            sIconMapping.put("com.huawei.android.FMRadio.png", "com.miui.fmradio.png");
            sIconMapping.put("com.sonyericsson.android.camera.png", "com.miui.camera.png");
            sIconMapping.put("com.sonyericsson.fmradio.png", "com.miui.fmradio.png");
            sIconMapping.put("com.motorola.Camera.Camera.png", "com.miui.camera.png");
            sIconMapping.put("com.lge.camera.png", "com.miui.camera.png");
            sIconMapping.put("com.oppo.camera.OppoCamera.png", "com.miui.camera.png");
            return;
            int j = 0;
        }
    }

    private static int RGBToColor(int[] paramArrayOfInt)
    {
        return ((paramArrayOfInt[0] << 8) + paramArrayOfInt[1] << 8) + paramArrayOfInt[2];
    }

    public static void clearCache()
    {
        synchronized (sExcludeAll)
        {
            sExcludes = null;
            sCache.clear();
            return;
        }
    }

    public static void clearCustomizedIcons(String paramString)
    {
        if (TextUtils.isEmpty(paramString))
        {
            Shell.remove("/data/system/customized_icons/*");
            sCache.clear();
        }
        while (true)
        {
            return;
            Shell.remove("/data/system/customized_icons/" + paramString + "*");
        }
    }

    private static int[] colorToRGB(int paramInt)
    {
        int[] arrayOfInt = new int[3];
        arrayOfInt[0] = ((0xFF0000 & paramInt) >> 16);
        arrayOfInt[1] = ((0xFF00 & paramInt) >> 8);
        arrayOfInt[2] = (paramInt & 0xFF);
        return arrayOfInt;
    }

    private static Bitmap composeIcon(Bitmap paramBitmap)
    {
        int i = paramBitmap.getWidth();
        int j = paramBitmap.getHeight();
        int[] arrayOfInt = new int[i * j];
        paramBitmap.getPixels(arrayOfInt, 0, i, 0, 0, i, j);
        paramBitmap.recycle();
        cutEdge(i, j, arrayOfInt);
        Bitmap localBitmap1 = Bitmap.createBitmap(sCustomizedIconWidth, sCustomizedIconHeight, Bitmap.Config.ARGB_8888);
        Canvas localCanvas = new Canvas(localBitmap1);
        Bitmap localBitmap2 = getCachedThemeIcon("icon_background.png");
        if (localBitmap2 != null)
            drawBackground(localCanvas, localBitmap2, i, j, arrayOfInt);
        Bitmap localBitmap3 = getCachedThemeIcon("icon_pattern.png");
        if (localBitmap3 != null)
            localCanvas.drawBitmap(localBitmap3, 0.0F, 0.0F, null);
        localCanvas.drawBitmap(arrayOfInt, 0, i, (sCustomizedIconWidth - i) / 2, (sCustomizedIconHeight - j) / 2, i, j, true, null);
        Bitmap localBitmap4 = getCachedThemeIcon("icon_border.png");
        if (localBitmap4 != null)
            localCanvas.drawBitmap(localBitmap4, 0.0F, 0.0F, null);
        return localBitmap1;
    }

    private static Bitmap composeShortcutIcon(Bitmap paramBitmap)
    {
        int i = paramBitmap.getWidth();
        int j = paramBitmap.getHeight();
        int[] arrayOfInt = new int[i * j];
        paramBitmap.getPixels(arrayOfInt, 0, i, 0, 0, i, j);
        paramBitmap.recycle();
        cutEdge(i, j, arrayOfInt);
        Bitmap localBitmap1 = Bitmap.createBitmap(sCustomizedIconWidth, sCustomizedIconHeight, Bitmap.Config.ARGB_8888);
        Canvas localCanvas = new Canvas(localBitmap1);
        Bitmap localBitmap2 = getCachedThemeIcon("icon_shortcut.png");
        if (localBitmap2 != null)
            localCanvas.drawBitmap(localBitmap2, 0.0F, 0.0F, null);
        localCanvas.drawBitmap(arrayOfInt, 0, i, (sCustomizedIconWidth - i) / 2, (sCustomizedIconHeight - j) / 2, i, j, true, null);
        Bitmap localBitmap3 = getCachedThemeIcon("icon_shortcut_arrow.png");
        if (localBitmap3 != null)
            localCanvas.drawBitmap(localBitmap3, 0.0F, 0.0F, null);
        return localBitmap1;
    }

    private static void cutEdge(int paramInt1, int paramInt2, int[] paramArrayOfInt)
    {
        Bitmap localBitmap = getCachedThemeIcon("icon_mask.png");
        if (localBitmap == null);
        while (true)
        {
            return;
            int i = localBitmap.getWidth();
            int j = localBitmap.getHeight();
            if ((i >= paramInt1) && (j >= paramInt2))
            {
                int[] arrayOfInt = new int[i * j];
                localBitmap.getPixels(arrayOfInt, 0, paramInt1, (i - paramInt1) / 2, (j - paramInt2) / 2, paramInt1, paramInt2);
                for (int k = -1 + paramInt1 * paramInt2; k >= 0; k--)
                    paramArrayOfInt[k] &= 16777215 + ((paramArrayOfInt[k] >>> 24) * (arrayOfInt[k] >>> 24) / 255 << 24);
            }
        }
    }

    private static void drawBackground(Canvas paramCanvas, Bitmap paramBitmap, int paramInt1, int paramInt2, int[] paramArrayOfInt)
    {
        int i = 0;
        int[] arrayOfInt1 = new int[3];
        arrayOfInt1[0] = 0;
        arrayOfInt1[1] = 0;
        arrayOfInt1[2] = 0;
        for (int j = -1 + paramInt1 * paramInt2; j >= 0; j--)
        {
            int i8 = 0xFFFFFF & paramArrayOfInt[j];
            if (i8 > 0)
            {
                int[] arrayOfInt6 = colorToRGB(i8);
                arrayOfInt1[0] += arrayOfInt6[0];
                arrayOfInt1[1] += arrayOfInt6[1];
                arrayOfInt1[2] += arrayOfInt6[2];
                i++;
            }
        }
        if (i > 0)
        {
            arrayOfInt1[0] /= i;
            arrayOfInt1[1] /= i;
            arrayOfInt1[2] /= i;
        }
        int k = RGBToColor(arrayOfInt1);
        if (getSaturation(k) < 0.02D);
        int i3;
        int i4;
        int[] arrayOfInt5;
        float f;
        for (int i2 = 0; ; i2 = setSaturation(setValue(setHue(k, f), 0.6F), 0.4F))
        {
            int[] arrayOfInt4 = colorToRGB(i2);
            i3 = paramBitmap.getWidth();
            i4 = paramBitmap.getHeight();
            arrayOfInt5 = new int[i3 * i4];
            paramBitmap.getPixels(arrayOfInt5, 0, i3, 0, 0, i3, i4);
            for (int i5 = -1 + i3 * i4; i5 >= 0; i5--)
            {
                int i6 = arrayOfInt5[i5];
                arrayOfInt5[i5] = (0xFF000000 & i6 | 0xFF0000 & (0xFF0000 & i6) * arrayOfInt4[0] >>> 8 | 0xFF00 & (0xFF00 & i6) * arrayOfInt4[1] >>> 8 | 0xFF & (i6 & 0xFF) * arrayOfInt4[2] >>> 8);
            }
            int[][] arrayOfInt = new int[2][];
            int[] arrayOfInt2 = new int[2];
            arrayOfInt2[0] = 100;
            arrayOfInt2[1] = 110;
            arrayOfInt[0] = arrayOfInt2;
            int[] arrayOfInt3 = new int[2];
            arrayOfInt3[0] = 190;
            arrayOfInt3[1] = 275;
            arrayOfInt[1] = arrayOfInt3;
            int m = 0;
            for (int n = 0; n < arrayOfInt.length; n++)
                m += arrayOfInt[n][1] - arrayOfInt[n][0];
            f = getHue(k) * m / 360.0F;
            int i1 = 0;
            while (i1 < arrayOfInt.length)
            {
                int i7 = arrayOfInt[i1][1] - arrayOfInt[i1][0];
                if (f > i7)
                {
                    f -= i7;
                    i1++;
                }
                else
                {
                    f += arrayOfInt[i1][0];
                }
            }
        }
        paramCanvas.drawBitmap(arrayOfInt5, 0, i3, 0, 0, i3, i4, true, null);
    }

    private static Bitmap drawableToBitmap(Drawable paramDrawable)
    {
        while (true)
        {
            int m;
            int n;
            int i1;
            float f;
            synchronized (sCanvas)
            {
                int i = sIconWidth;
                int j = sIconHeight;
                k = i;
                m = j;
                if ((paramDrawable instanceof PaintDrawable))
                {
                    PaintDrawable localPaintDrawable = (PaintDrawable)paramDrawable;
                    localPaintDrawable.setIntrinsicWidth(i);
                    localPaintDrawable.setIntrinsicHeight(j);
                    n = paramDrawable.getIntrinsicWidth();
                    i1 = paramDrawable.getIntrinsicHeight();
                    if ((n > 0) && (n > 0))
                    {
                        if ((k >= n) && (m >= i1))
                            break label257;
                        f = n / i1;
                        if (n > i1)
                            m = (int)(k / f);
                    }
                    else
                    {
                        Bitmap localBitmap = Bitmap.createBitmap(i, j, Bitmap.Config.ARGB_8888);
                        Canvas localCanvas2 = sCanvas;
                        localCanvas2.setBitmap(localBitmap);
                        int i2 = (i - k) / 2;
                        int i3 = (j - m) / 2;
                        sOldBounds.set(paramDrawable.getBounds());
                        paramDrawable.setBounds(i2, i3, i2 + k, i3 + m);
                        paramDrawable.draw(localCanvas2);
                        paramDrawable.setBounds(sOldBounds);
                        return localBitmap;
                    }
                }
                else
                {
                    if (!(paramDrawable instanceof BitmapDrawable))
                        continue;
                    BitmapDrawable localBitmapDrawable = (BitmapDrawable)paramDrawable;
                    if (localBitmapDrawable.getBitmap().getDensity() != 0)
                        continue;
                    localBitmapDrawable.setTargetDensity(sSystemResource.getDisplayMetrics());
                }
            }
            int k = (int)(f * m);
            continue;
            label257: if ((n < k) && (i1 < m))
            {
                k = n;
                m = i1;
            }
        }
    }

    public static BitmapDrawable generateIconDrawable(Drawable paramDrawable)
    {
        return scaleDrawable(composeIcon(drawableToBitmap(paramDrawable)));
    }

    public static BitmapDrawable generateShortcutIconDrawable(Drawable paramDrawable)
    {
        return scaleDrawable(composeShortcutIcon(drawableToBitmap(paramDrawable)));
    }

    public static Bitmap getCachedThemeIcon(String paramString)
    {
        return getCachedThemeIcon(paramString, sCustomizedIconWidth, sCustomizedIconHeight);
    }

    public static Bitmap getCachedThemeIcon(String paramString, int paramInt1, int paramInt2)
    {
        Bitmap localBitmap = null;
        SoftReference localSoftReference = (SoftReference)sCache.get(paramString);
        if (localSoftReference != null)
            localBitmap = (Bitmap)localSoftReference.get();
        if (localBitmap == null)
        {
            localBitmap = getThemeIcon(paramString);
            sCache.put(paramString, new SoftReference(localBitmap));
        }
        return scaleBitmap(localBitmap, paramInt1, paramInt2);
    }

    public static BitmapDrawable getCustomizedIconDrawable(String paramString1, String paramString2)
    {
        String str1 = getFileName(paramString1, paramString2);
        Bitmap localBitmap = getThemeIcon(str1);
        if ((localBitmap == null) && (paramString2 != null) && (!paramString2.startsWith(paramString1)))
        {
            Object[] arrayOfObject = new Object[1];
            arrayOfObject[0] = paramString2;
            localBitmap = getThemeIcon(String.format("%s.png", arrayOfObject));
        }
        if (localBitmap == null)
        {
            String str3 = (String)sIconMapping.get(str1);
            if (str3 != null)
                localBitmap = getThemeIcon(str3);
        }
        if (localBitmap == null)
        {
            String str2 = "/data/system/customized_icons/" + str1;
            File localFile = new File(str2);
            if (localFile.exists())
            {
                localBitmap = BitmapFactory.decodeFile(str2);
                if (localBitmap == null)
                    localFile.delete();
            }
        }
        return scaleDrawable(localBitmap);
    }

    public static String getFileName(String paramString1, String paramString2)
    {
        String str;
        if (paramString2 == null)
        {
            Object[] arrayOfObject3 = new Object[1];
            arrayOfObject3[0] = paramString1;
            str = String.format("%s.png", arrayOfObject3);
        }
        while (true)
        {
            return str;
            if (paramString2.startsWith(paramString1))
            {
                Object[] arrayOfObject2 = new Object[1];
                arrayOfObject2[0] = paramString2;
                str = String.format("%s.png", arrayOfObject2);
            }
            else
            {
                Object[] arrayOfObject1 = new Object[2];
                arrayOfObject1[0] = paramString1;
                arrayOfObject1[1] = paramString2;
                str = String.format("%s#%s.png", arrayOfObject1);
            }
        }
    }

    private static float getHue(int paramInt)
    {
        int[] arrayOfInt = colorToRGB(paramInt);
        int i = Math.min(arrayOfInt[0], Math.min(arrayOfInt[1], arrayOfInt[2]));
        int j = Math.max(arrayOfInt[0], Math.max(arrayOfInt[1], arrayOfInt[2]));
        int k = j - i;
        if (k == 0);
        int m;
        for (float f = 0.0F; ; f = 120 * ((m + 1) % 3) + 60.0F * (arrayOfInt[((m + 2) % 3)] - i) / k + 60.0F * (j - arrayOfInt[((m + 1) % 3)]) / k)
        {
            return f;
            for (m = 0; (m < 2) && (i != arrayOfInt[m]); m++);
        }
    }

    private static float getSaturation(int paramInt)
    {
        int[] arrayOfInt = colorToRGB(paramInt);
        int i = Math.min(arrayOfInt[0], Math.min(arrayOfInt[1], arrayOfInt[2]));
        int j = Math.max(arrayOfInt[0], Math.max(arrayOfInt[1], arrayOfInt[2]));
        if ((j == 0) || (j == i));
        for (float f = paramInt; ; f = 1.0F * (j - i) / j)
            return f;
    }

    private static Bitmap getThemeIcon(String paramString)
    {
        Bitmap localBitmap = null;
        for (int i = 0; ; i++)
            if (i < sDensities.length)
            {
                String str = DisplayUtils.getDrawbleDensityFolder(sDensities[i]) + paramString;
                localBitmap = ThemeResources.getSystem().getIcon(sSystemResource, str);
                if (localBitmap != null)
                    localBitmap.setDensity(sDensities[i]);
            }
            else
            {
                if (localBitmap == null)
                {
                    localBitmap = ThemeResources.getSystem().getIcon(sSystemResource, paramString);
                    if (localBitmap != null)
                        localBitmap.setDensity(240);
                }
                return localBitmap;
            }
    }

    private static float getValue(int paramInt)
    {
        int[] arrayOfInt = colorToRGB(paramInt);
        return 1.0F * Math.max(arrayOfInt[0], Math.max(arrayOfInt[1], arrayOfInt[2])) / 255.0F;
    }

    public static boolean isExclude(String paramString)
    {
        while (true)
        {
            synchronized (sExcludeAll)
            {
                if (sExcludes == null)
                {
                    sExcludes = new HashSet();
                    if (ThemeResources.getSystem().hasIcon("exclude_list.txt"))
                    {
                        sExcludes.add("com.android.browser");
                        sExcludes.add("com.android.calendar");
                        sExcludes.add("com.android.camera");
                        sExcludes.add("com.android.contacts");
                        sExcludes.add("com.android.deskclock");
                        sExcludes.add("com.android.email");
                        sExcludes.add("com.android.fileexplorer");
                        sExcludes.add("com.android.gallery");
                        sExcludes.add("com.android.launcher");
                        sExcludes.add("com.android.mms");
                        sExcludes.add("com.android.monitor");
                        sExcludes.add("com.android.music");
                        sExcludes.add("com.android.phone");
                        sExcludes.add("com.android.providers.contacts");
                        sExcludes.add("com.android.providers.downloads.ui");
                        sExcludes.add("com.android.providers.telephony");
                        sExcludes.add("com.android.quicksearchbox");
                        sExcludes.add("com.android.settings");
                        sExcludes.add("com.android.soundrecorder");
                        sExcludes.add("com.android.spare_parts");
                        sExcludes.add("com.android.stk");
                        sExcludes.add("com.android.thememanager");
                        sExcludes.add("com.android.updater");
                        sExcludes.add("com.miui.antispam");
                        sExcludes.add("com.miui.backup");
                        sExcludes.add("com.miui.bugreport");
                        sExcludes.add("com.miui.camera");
                        sExcludes.add("com.miui.cit");
                        sExcludes.add("com.miui.compass");
                        sExcludes.add("com.miui.fmradio");
                        sExcludes.add("com.miui.lockv4");
                        sExcludes.add("com.miui.notes");
                        sExcludes.add("com.miui.player");
                        sExcludes.add("com.xiaomi.market");
                        sExcludes.add("com.miui.uac");
                        sExcludes.add("com.miui.userbook");
                        sExcludes.add("com.miui.weather2");
                    }
                }
                if (!sExcludeAll.booleanValue())
                {
                    if (!sExcludes.contains(paramString))
                        break label516;
                    break label511;
                    return bool;
                }
            }
            label511: boolean bool = true;
            continue;
            label516: bool = false;
        }
    }

    public static void prepareCustomizedIcons(Context paramContext)
    {
        prepareCustomizedIcons(paramContext, null);
    }

    public static void prepareCustomizedIcons(Context paramContext, CustomizedIconsListener paramCustomizedIconsListener)
    {
        Intent localIntent = new Intent("android.intent.action.MAIN", null);
        localIntent.addCategory("android.intent.category.LAUNCHER");
        PackageManager localPackageManager = paramContext.getPackageManager();
        List localList = localPackageManager.queryIntentActivities(localIntent, 0);
        if (paramCustomizedIconsListener != null)
            paramCustomizedIconsListener.beforePrepareIcon(localList.size());
        int i = 0;
        Iterator localIterator = localList.iterator();
        while (localIterator.hasNext())
        {
            ((ResolveInfo)localIterator.next()).activityInfo.loadIcon(localPackageManager);
            if (paramCustomizedIconsListener != null)
            {
                int j = i + 1;
                paramCustomizedIconsListener.finishPrepareIcon(i);
                i = j;
            }
        }
        if (paramCustomizedIconsListener != null)
            paramCustomizedIconsListener.finishAllIcons();
    }

    // ERROR //
    public static void saveCustomizedIconBitmap(String paramString, Bitmap paramBitmap)
    {
        // Byte code:
        //     0: new 215	java/lang/StringBuilder
        //     3: dup
        //     4: invokespecial 216	java/lang/StringBuilder:<init>	()V
        //     7: ldc 39
        //     9: invokevirtual 220	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     12: aload_0
        //     13: invokevirtual 220	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     16: invokevirtual 226	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     19: astore 4
        //     21: new 429	java/io/File
        //     24: dup
        //     25: aload 4
        //     27: invokespecial 431	java/io/File:<init>	(Ljava/lang/String;)V
        //     30: astore 5
        //     32: aconst_null
        //     33: astore 6
        //     35: new 646	java/io/FileOutputStream
        //     38: dup
        //     39: aload 5
        //     41: invokespecial 649	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
        //     44: astore 7
        //     46: aload 4
        //     48: sipush 436
        //     51: bipush 255
        //     53: bipush 255
        //     55: invokestatic 655	android/os/FileUtils:setPermissions	(Ljava/lang/String;III)I
        //     58: pop
        //     59: aload 7
        //     61: astore 6
        //     63: aload 6
        //     65: ifnonnull +56 -> 121
        //     68: aload 5
        //     70: invokevirtual 659	java/io/File:getParentFile	()Ljava/io/File;
        //     73: astore 10
        //     75: aload 10
        //     77: invokevirtual 662	java/io/File:mkdirs	()Z
        //     80: pop
        //     81: aload 10
        //     83: invokevirtual 665	java/io/File:getPath	()Ljava/lang/String;
        //     86: sipush 1023
        //     89: bipush 255
        //     91: bipush 255
        //     93: invokestatic 655	android/os/FileUtils:setPermissions	(Ljava/lang/String;III)I
        //     96: pop
        //     97: new 646	java/io/FileOutputStream
        //     100: dup
        //     101: aload 5
        //     103: invokespecial 649	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
        //     106: astore 6
        //     108: aload 4
        //     110: sipush 436
        //     113: bipush 255
        //     115: bipush 255
        //     117: invokestatic 655	android/os/FileUtils:setPermissions	(Ljava/lang/String;III)I
        //     120: pop
        //     121: aload_1
        //     122: getstatic 671	android/graphics/Bitmap$CompressFormat:PNG	Landroid/graphics/Bitmap$CompressFormat;
        //     125: bipush 100
        //     127: aload 6
        //     129: invokevirtual 675	android/graphics/Bitmap:compress	(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
        //     132: pop
        //     133: aload 6
        //     135: invokevirtual 678	java/io/FileOutputStream:flush	()V
        //     138: aload 6
        //     140: invokevirtual 681	java/io/FileOutputStream:close	()V
        //     143: return
        //     144: astore_3
        //     145: aload_3
        //     146: invokevirtual 684	java/io/FileNotFoundException:printStackTrace	()V
        //     149: goto -6 -> 143
        //     152: astore_2
        //     153: aload_2
        //     154: invokevirtual 685	java/io/IOException:printStackTrace	()V
        //     157: goto -14 -> 143
        //     160: astore 15
        //     162: goto -99 -> 63
        //     165: astore 8
        //     167: aload 7
        //     169: astore 6
        //     171: goto -108 -> 63
        //
        // Exception table:
        //     from	to	target	type
        //     0	32	144	java/io/FileNotFoundException
        //     68	143	144	java/io/FileNotFoundException
        //     0	32	152	java/io/IOException
        //     35	46	152	java/io/IOException
        //     46	59	152	java/io/IOException
        //     68	143	152	java/io/IOException
        //     35	46	160	java/io/FileNotFoundException
        //     46	59	165	java/io/FileNotFoundException
    }

    private static Bitmap scaleBitmap(Bitmap paramBitmap)
    {
        return scaleBitmap(paramBitmap, sCustomizedIconWidth, sCustomizedIconHeight);
    }

    private static Bitmap scaleBitmap(Bitmap paramBitmap, int paramInt1, int paramInt2)
    {
        Bitmap localBitmap = null;
        if (paramBitmap != null)
            if ((paramBitmap.getWidth() != paramInt1) || (paramBitmap.getHeight() != paramInt2));
        while (true)
        {
            return paramBitmap;
            localBitmap = Bitmap.createScaledBitmap(paramBitmap, paramInt1, paramInt2, true);
            localBitmap.setDensity(sDensity);
            paramBitmap = localBitmap;
        }
    }

    private static BitmapDrawable scaleDrawable(Bitmap paramBitmap)
    {
        BitmapDrawable localBitmapDrawable = null;
        if (paramBitmap != null)
        {
            Bitmap localBitmap = scaleBitmap(paramBitmap);
            localBitmapDrawable = new BitmapDrawable(sSystemResource, localBitmap);
        }
        return localBitmapDrawable;
    }

    private static int scalePixel(int paramInt)
    {
        int i = sDensity;
        if (sDensity == 320)
            i = 360;
        return paramInt * i / 240;
    }

    private static int setHue(int paramInt, float paramFloat)
    {
        int[] arrayOfInt = colorToRGB(paramInt);
        int i = Math.min(arrayOfInt[0], Math.min(arrayOfInt[1], arrayOfInt[2]));
        int j = Math.max(arrayOfInt[0], Math.max(arrayOfInt[1], arrayOfInt[2]));
        int k = j - i;
        if (k == 0);
        while (true)
        {
            return paramInt;
            while (paramFloat < 0.0F)
                paramFloat += 360.0F;
            while (paramFloat > 360.0F)
                paramFloat -= 360.0F;
            int m = (int)Math.floor(paramFloat / 120.0F);
            float f = paramFloat - m * 120;
            int n = (m + 2) % 3;
            arrayOfInt[n] = i;
            arrayOfInt[((n + 2) % 3)] = ((int)(i + k * Math.min(f, 60.0F) / 60.0F));
            arrayOfInt[((n + 1) % 3)] = ((int)(j - k * Math.max(0.0F, f - 60.0F) / 60.0F));
            paramInt = RGBToColor(arrayOfInt);
        }
    }

    private static int setSaturation(int paramInt, float paramFloat)
    {
        int[] arrayOfInt = colorToRGB(paramInt);
        int i = Math.min(arrayOfInt[0], Math.min(arrayOfInt[1], arrayOfInt[2]));
        int j = Math.max(arrayOfInt[0], Math.max(arrayOfInt[1], arrayOfInt[2]));
        if ((j == 0) || (j == i));
        while (true)
        {
            return paramInt;
            float f = 1.0F * (j - i) / j;
            arrayOfInt[0] = ((int)(j - paramFloat * (j - arrayOfInt[0]) / f));
            arrayOfInt[1] = ((int)(j - paramFloat * (j - arrayOfInt[1]) / f));
            arrayOfInt[2] = ((int)(j - paramFloat * (j - arrayOfInt[2]) / f));
            paramInt = RGBToColor(arrayOfInt);
        }
    }

    private static int setValue(int paramInt, float paramFloat)
    {
        int[] arrayOfInt = colorToRGB(paramInt);
        int i = Math.max(arrayOfInt[0], Math.max(arrayOfInt[1], arrayOfInt[2]));
        if (i == 0);
        while (true)
        {
            return paramInt;
            float f = 1.0F * i / 255.0F;
            arrayOfInt[0] = ((int)(paramFloat * arrayOfInt[0] / f));
            arrayOfInt[1] = ((int)(paramFloat * arrayOfInt[1] / f));
            arrayOfInt[2] = ((int)(paramFloat * arrayOfInt[2] / f));
            paramInt = RGBToColor(arrayOfInt);
        }
    }

    public static abstract interface CustomizedIconsListener
    {
        public abstract void beforePrepareIcon(int paramInt);

        public abstract void finishAllIcons();

        public abstract void finishPrepareIcon(int paramInt);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.content.res.IconCustomizer
 * JD-Core Version:        0.6.2
 */