package miui.content.res;

import android.content.res.Resources;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

public final class ThemeResourcesPackage extends ThemeResources
{
    private static final Map<String, WeakReference<ThemeResourcesPackage>> sPackageResources = new HashMap();

    protected ThemeResourcesPackage(ThemeResourcesPackage paramThemeResourcesPackage, Resources paramResources, String paramString, ThemeResources.MetaData paramMetaData)
    {
        super(paramThemeResourcesPackage, paramResources, paramString, paramMetaData);
    }

    public static ThemeResourcesPackage getThemeResources(Resources paramResources, String paramString)
    {
        ThemeResourcesPackage localThemeResourcesPackage = null;
        if (sPackageResources.containsKey(paramString))
            localThemeResourcesPackage = (ThemeResourcesPackage)((WeakReference)sPackageResources.get(paramString)).get();
        if (localThemeResourcesPackage == null)
            synchronized (sPackageResources)
            {
                if (sPackageResources.containsKey(paramString))
                    localThemeResourcesPackage = (ThemeResourcesPackage)((WeakReference)sPackageResources.get(paramString)).get();
                if (localThemeResourcesPackage == null)
                {
                    localThemeResourcesPackage = getTopLevelThemeResources(paramResources, paramString);
                    sPackageResources.put(paramString, new WeakReference(localThemeResourcesPackage));
                }
            }
        return localThemeResourcesPackage;
    }

    public static ThemeResourcesPackage getTopLevelThemeResources(Resources paramResources, String paramString)
    {
        Object localObject = null;
        int i = 0;
        while (i < THEME_PATHS.length)
        {
            ThemeResourcesPackage localThemeResourcesPackage = new ThemeResourcesPackage((ThemeResourcesPackage)localObject, paramResources, paramString, THEME_PATHS[i]);
            i++;
            localObject = localThemeResourcesPackage;
        }
        return localObject;
    }

    public CharSequence getThemeCharSequence(int paramInt)
    {
        CharSequence localCharSequence = super.getThemeCharSequence(paramInt);
        if (localCharSequence == null)
            localCharSequence = getSystem().getThemeCharSequence(paramInt);
        return localCharSequence;
    }

    public ThemeZipFile.ThemeFileInfo getThemeFileStream(int paramInt, String paramString)
    {
        ThemeZipFile.ThemeFileInfo localThemeFileInfo;
        if (1 == paramInt)
        {
            localThemeFileInfo = getThemeFileStream("framework-res/" + paramString);
            if (localThemeFileInfo == null)
                localThemeFileInfo = getSystem().getThemeFileStream(paramInt, paramString);
        }
        while (true)
        {
            return localThemeFileInfo;
            if (2 == paramInt)
            {
                localThemeFileInfo = getThemeFileStream("framework-miui-res/" + paramString);
                if (localThemeFileInfo == null)
                    localThemeFileInfo = getSystem().getThemeFileStream(paramInt, paramString);
            }
            else
            {
                localThemeFileInfo = getThemeFileStream(paramString);
            }
        }
    }

    public Integer getThemeInt(int paramInt)
    {
        Integer localInteger = super.getThemeInt(paramInt);
        if (localInteger == null)
            localInteger = getSystem().getThemeInt(paramInt);
        return localInteger;
    }

    public boolean hasValues()
    {
        if ((super.hasValues()) || (getSystem().hasValues()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.content.res.ThemeResourcesPackage
 * JD-Core Version:        0.6.2
 */