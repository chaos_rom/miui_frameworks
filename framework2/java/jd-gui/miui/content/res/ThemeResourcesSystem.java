package miui.content.res;

import android.content.res.Resources;
import java.io.File;

public final class ThemeResourcesSystem extends ThemeResources
{
    private static ThemeResources sIcons;
    private static ThemeResources sLockscreen;
    private static ThemeResources sMiui;
    private static ThemeResources sSystemUI;
    protected String mThemePath;

    protected ThemeResourcesSystem(ThemeResourcesSystem paramThemeResourcesSystem, Resources paramResources, ThemeResources.MetaData paramMetaData)
    {
        super(paramThemeResourcesSystem, paramResources, "framework-res", paramMetaData);
        this.mThemePath = paramMetaData.themePath;
    }

    private ThemeZipFile.ThemeFileInfo getThemeFileStreamMIUI(String paramString1, String paramString2)
    {
        ThemeZipFile.ThemeFileInfo localThemeFileInfo = null;
        if (paramString2.startsWith("lock_screen_"))
        {
            localThemeFileInfo = sLockscreen.getThemeFileStream(paramString1);
            if (localThemeFileInfo == null)
                localThemeFileInfo = sLockscreen.getThemeFileStream(paramString2);
        }
        while (true)
        {
            if (localThemeFileInfo == null)
                localThemeFileInfo = sMiui.getThemeFileStream(paramString1);
            return localThemeFileInfo;
            if (paramString2.startsWith("status_bar_toggle_"))
            {
                if (sSystemUI == null)
                    sSystemUI = ThemeResources.getTopLevelThemeResources(this.mResources, "com.android.systemui");
                localThemeFileInfo = sSystemUI.getThemeFileStream(paramString1);
            }
        }
    }

    private ThemeZipFile.ThemeFileInfo getThemeFileStreamSystem(String paramString1, String paramString2)
    {
        ThemeZipFile.ThemeFileInfo localThemeFileInfo1 = null;
        if (paramString2.equals("sym_def_app_icon.png"))
        {
            localThemeFileInfo1 = getIconStream(paramString1);
            if (localThemeFileInfo1 == null)
                localThemeFileInfo1 = getThemeFileStreamInner(paramString1);
        }
        for (ThemeZipFile.ThemeFileInfo localThemeFileInfo2 = localThemeFileInfo1; ; localThemeFileInfo2 = null)
        {
            return localThemeFileInfo2;
            if (!paramString2.equals("default_wallpaper.jpg"))
                break;
        }
    }

    public static ThemeResourcesSystem getTopLevelThemeResources(Resources paramResources)
    {
        sIcons = ThemeResources.getTopLevelThemeResources(paramResources, "icons");
        sLockscreen = ThemeResources.getTopLevelThemeResources(paramResources, "lockscreen");
        sMiui = ThemeResources.getTopLevelThemeResources(paramResources, "framework-miui-res");
        Object localObject = null;
        int i = 0;
        while (i < THEME_PATHS.length)
        {
            ThemeResourcesSystem localThemeResourcesSystem = new ThemeResourcesSystem((ThemeResourcesSystem)localObject, paramResources, THEME_PATHS[i]);
            i++;
            localObject = localThemeResourcesSystem;
        }
        return localObject;
    }

    public boolean checkUpdate()
    {
        sIcons.checkUpdate();
        sLockscreen.checkUpdate();
        sMiui.checkUpdate();
        if (sSystemUI != null)
            sSystemUI.checkUpdate();
        return super.checkUpdate();
    }

    public boolean containsAwesomeLockscreenEntry(String paramString)
    {
        return sLockscreen.containsEntry("advance/" + paramString);
    }

    public ThemeZipFile.ThemeFileInfo getAwesomeLockscreenFileStream(String paramString)
    {
        return sLockscreen.getThemeFileStream("advance/" + paramString);
    }

    // ERROR //
    public android.graphics.Bitmap getIcon(Resources paramResources, String paramString)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_3
        //     2: aload_0
        //     3: aload_2
        //     4: invokevirtual 67	miui/content/res/ThemeResourcesSystem:getIconStream	(Ljava/lang/String;)Lmiui/content/res/ThemeZipFile$ThemeFileInfo;
        //     7: astore 4
        //     9: aload 4
        //     11: ifnull +61 -> 72
        //     14: aconst_null
        //     15: astore 5
        //     17: aload 4
        //     19: getfield 123	miui/content/res/ThemeZipFile$ThemeFileInfo:mDensity	I
        //     22: ifle +26 -> 48
        //     25: new 125	android/graphics/BitmapFactory$Options
        //     28: dup
        //     29: invokespecial 126	android/graphics/BitmapFactory$Options:<init>	()V
        //     32: astore 10
        //     34: aload 10
        //     36: aload 4
        //     38: getfield 123	miui/content/res/ThemeZipFile$ThemeFileInfo:mDensity	I
        //     41: putfield 129	android/graphics/BitmapFactory$Options:inDensity	I
        //     44: aload 10
        //     46: astore 5
        //     48: aload 4
        //     50: getfield 133	miui/content/res/ThemeZipFile$ThemeFileInfo:mInput	Ljava/io/InputStream;
        //     53: aconst_null
        //     54: aload 5
        //     56: invokestatic 139	android/graphics/BitmapFactory:decodeStream	(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
        //     59: astore 12
        //     61: aload 12
        //     63: astore_3
        //     64: aload 4
        //     66: getfield 133	miui/content/res/ThemeZipFile$ThemeFileInfo:mInput	Ljava/io/InputStream;
        //     69: invokevirtual 144	java/io/InputStream:close	()V
        //     72: aload_3
        //     73: areturn
        //     74: astore 8
        //     76: aload 4
        //     78: getfield 133	miui/content/res/ThemeZipFile$ThemeFileInfo:mInput	Ljava/io/InputStream;
        //     81: invokevirtual 144	java/io/InputStream:close	()V
        //     84: goto -12 -> 72
        //     87: astore 9
        //     89: goto -17 -> 72
        //     92: astore 6
        //     94: aload 4
        //     96: getfield 133	miui/content/res/ThemeZipFile$ThemeFileInfo:mInput	Ljava/io/InputStream;
        //     99: invokevirtual 144	java/io/InputStream:close	()V
        //     102: aload 6
        //     104: athrow
        //     105: astore 7
        //     107: goto -5 -> 102
        //     110: astore 6
        //     112: goto -18 -> 94
        //     115: astore 11
        //     117: goto -41 -> 76
        //     120: astore 13
        //     122: goto -50 -> 72
        //
        // Exception table:
        //     from	to	target	type
        //     17	34	74	java/lang/OutOfMemoryError
        //     48	61	74	java/lang/OutOfMemoryError
        //     76	84	87	java/io/IOException
        //     17	34	92	finally
        //     48	61	92	finally
        //     94	102	105	java/io/IOException
        //     34	44	110	finally
        //     34	44	115	java/lang/OutOfMemoryError
        //     64	72	120	java/io/IOException
    }

    public ThemeZipFile.ThemeFileInfo getIconStream(String paramString)
    {
        return sIcons.getThemeFileStream(paramString);
    }

    public ThemeZipFile.ThemeFileInfo getLockscreenStream(String paramString)
    {
        return sLockscreen.getThemeFileStream(paramString);
    }

    public File getLockscreenWallpaper()
    {
        File localFile = new File(this.mThemePath + "lock_wallpaper");
        if (((localFile == null) || (!localFile.exists())) && (this.mWrapped != null))
            localFile = ((ThemeResourcesSystem)this.mWrapped).getLockscreenWallpaper();
        return localFile;
    }

    public CharSequence getThemeCharSequence(int paramInt)
    {
        CharSequence localCharSequence = sLockscreen.getThemeCharSequence(paramInt);
        if (localCharSequence == null)
            localCharSequence = sMiui.getThemeCharSequence(paramInt);
        if (localCharSequence == null)
            localCharSequence = getThemeCharSequenceInner(paramInt);
        return localCharSequence;
    }

    public ThemeZipFile.ThemeFileInfo getThemeFileStream(int paramInt, String paramString)
    {
        String str = paramString.substring(1 + paramString.lastIndexOf('/'));
        if (2 == paramInt);
        for (ThemeZipFile.ThemeFileInfo localThemeFileInfo = getThemeFileStreamMIUI(paramString, str); ; localThemeFileInfo = getThemeFileStreamSystem(paramString, str))
            return localThemeFileInfo;
    }

    public Integer getThemeInt(int paramInt)
    {
        Integer localInteger = sLockscreen.getThemeInt(paramInt);
        if (localInteger == null)
            localInteger = sMiui.getThemeInt(paramInt);
        if (localInteger == null)
            localInteger = getThemeIntInner(paramInt);
        return localInteger;
    }

    public boolean hasAwesomeLockscreen()
    {
        return sLockscreen.containsEntry("advance/manifest.xml");
    }

    public boolean hasIcon(String paramString)
    {
        return sIcons.containsEntry(paramString);
    }

    public boolean hasValues()
    {
        if ((super.hasValues()) || (sLockscreen.hasValues()) || (sMiui.hasValues()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void resetIcons()
    {
        sIcons.checkUpdate();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.content.res.ThemeResourcesSystem
 * JD-Core Version:        0.6.2
 */