package miui.accounts;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.MiuiOnAccountsUpdateListener;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.database.SQLException;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import com.google.android.collect.Maps;
import java.util.HashMap;

public class ExtraAccountManager
{
    public static final String EXTRA_ACCOUNT = "extra_account";
    public static final String EXTRA_BUNDLE = "extra_bundle";
    public static final String EXTRA_CLEAR_WHEN_RESET = "extra_clear_when_reset";
    public static final String EXTRA_DISABLE_BACK_KEY = "extra_disable_back_key";
    public static final String EXTRA_SHOW_FIND_DEVICE = "extra_show_find_device";
    public static final String EXTRA_SHOW_SKIP_LOGIN = "extra_show_skip_login";
    public static final String EXTRA_SHOW_SYNC_SETTINGS = "show_detail";
    public static final String EXTRA_UPDATE_TYPE = "extra_update_type";
    public static final String EXTRA_WIPE_DATA = "extra_wipe_data";
    public static final String KEY_SKIPPED = "key_skipped";
    public static final String LOGIN_ACCOUNTS_POST_CHANGED_ACTION = "android.accounts.LOGIN_ACCOUNTS_POST_CHANGED";
    public static final String LOGIN_ACCOUNTS_PRE_CHANGED_ACTION = "android.accounts.LOGIN_ACCOUNTS_PRE_CHANGED";
    private static final String TAG = "ExtraAccountManager";
    public static final int TYPE_ADD = 2;
    public static final int TYPE_REMOVE = 1;
    private static ExtraAccountManager mInstance;
    private AccountManager mAccountManager;
    private final BroadcastReceiver mAccountsChangedBroadcastReceiver = new BroadcastReceiver()
    {
        // ERROR //
        public void onReceive(Context paramAnonymousContext, android.content.Intent paramAnonymousIntent)
        {
            // Byte code:
            //     0: aload_2
            //     1: invokevirtual 23	android/content/Intent:getAction	()Ljava/lang/String;
            //     4: astore_3
            //     5: ldc 25
            //     7: aload_3
            //     8: invokevirtual 31	java/lang/String:equals	(Ljava/lang/Object;)Z
            //     11: ifne +12 -> 23
            //     14: ldc 33
            //     16: aload_3
            //     17: invokevirtual 31	java/lang/String:equals	(Ljava/lang/Object;)Z
            //     20: ifeq +161 -> 181
            //     23: aload_2
            //     24: ldc 35
            //     26: invokevirtual 39	android/content/Intent:getParcelableExtra	(Ljava/lang/String;)Landroid/os/Parcelable;
            //     29: checkcast 41	android/accounts/Account
            //     32: astore 4
            //     34: aload_2
            //     35: ldc 43
            //     37: invokevirtual 39	android/content/Intent:getParcelableExtra	(Ljava/lang/String;)Landroid/os/Parcelable;
            //     40: checkcast 45	android/os/Bundle
            //     43: astore 5
            //     45: aload_2
            //     46: ldc 47
            //     48: bipush 255
            //     50: invokevirtual 51	android/content/Intent:getIntExtra	(Ljava/lang/String;I)I
            //     53: istore 6
            //     55: aload 4
            //     57: ifnull +113 -> 170
            //     60: iload 6
            //     62: ifle +108 -> 170
            //     65: aload_0
            //     66: getfield 12	miui/accounts/ExtraAccountManager$3:this$0	Lmiui/accounts/ExtraAccountManager;
            //     69: invokestatic 55	miui/accounts/ExtraAccountManager:access$000	(Lmiui/accounts/ExtraAccountManager;)Ljava/util/HashMap;
            //     72: astore 8
            //     74: aload 8
            //     76: monitorenter
            //     77: aload_0
            //     78: getfield 12	miui/accounts/ExtraAccountManager$3:this$0	Lmiui/accounts/ExtraAccountManager;
            //     81: invokestatic 55	miui/accounts/ExtraAccountManager:access$000	(Lmiui/accounts/ExtraAccountManager;)Ljava/util/HashMap;
            //     84: invokevirtual 61	java/util/HashMap:entrySet	()Ljava/util/Set;
            //     87: invokeinterface 67 1 0
            //     92: astore 10
            //     94: aload 10
            //     96: invokeinterface 73 1 0
            //     101: ifeq +65 -> 166
            //     104: aload 10
            //     106: invokeinterface 77 1 0
            //     111: checkcast 79	java/util/Map$Entry
            //     114: astore 11
            //     116: aload_0
            //     117: getfield 12	miui/accounts/ExtraAccountManager$3:this$0	Lmiui/accounts/ExtraAccountManager;
            //     120: aload 11
            //     122: invokeinterface 82 1 0
            //     127: checkcast 84	android/os/Handler
            //     130: aload 11
            //     132: invokeinterface 87 1 0
            //     137: checkcast 89	android/accounts/MiuiOnAccountsUpdateListener
            //     140: aload 4
            //     142: iload 6
            //     144: aload 5
            //     146: ldc 25
            //     148: aload_3
            //     149: invokevirtual 31	java/lang/String:equals	(Ljava/lang/Object;)Z
            //     152: invokestatic 93	miui/accounts/ExtraAccountManager:access$100	(Lmiui/accounts/ExtraAccountManager;Landroid/os/Handler;Landroid/accounts/MiuiOnAccountsUpdateListener;Landroid/accounts/Account;ILandroid/os/Bundle;Z)V
            //     155: goto -61 -> 94
            //     158: astore 9
            //     160: aload 8
            //     162: monitorexit
            //     163: aload 9
            //     165: athrow
            //     166: aload 8
            //     168: monitorexit
            //     169: return
            //     170: ldc 95
            //     172: ldc 97
            //     174: invokestatic 103	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
            //     177: pop
            //     178: goto -9 -> 169
            //     181: aload_0
            //     182: getfield 12	miui/accounts/ExtraAccountManager$3:this$0	Lmiui/accounts/ExtraAccountManager;
            //     185: invokestatic 107	miui/accounts/ExtraAccountManager:access$200	(Lmiui/accounts/ExtraAccountManager;)Landroid/accounts/AccountManager;
            //     188: invokevirtual 113	android/accounts/AccountManager:getAccounts	()[Landroid/accounts/Account;
            //     191: astore 12
            //     193: aload_0
            //     194: getfield 12	miui/accounts/ExtraAccountManager$3:this$0	Lmiui/accounts/ExtraAccountManager;
            //     197: invokestatic 55	miui/accounts/ExtraAccountManager:access$000	(Lmiui/accounts/ExtraAccountManager;)Ljava/util/HashMap;
            //     200: astore 13
            //     202: aload 13
            //     204: monitorenter
            //     205: aload_0
            //     206: getfield 12	miui/accounts/ExtraAccountManager$3:this$0	Lmiui/accounts/ExtraAccountManager;
            //     209: invokestatic 55	miui/accounts/ExtraAccountManager:access$000	(Lmiui/accounts/ExtraAccountManager;)Ljava/util/HashMap;
            //     212: invokevirtual 61	java/util/HashMap:entrySet	()Ljava/util/Set;
            //     215: invokeinterface 67 1 0
            //     220: astore 15
            //     222: aload 15
            //     224: invokeinterface 73 1 0
            //     229: ifeq +55 -> 284
            //     232: aload 15
            //     234: invokeinterface 77 1 0
            //     239: checkcast 79	java/util/Map$Entry
            //     242: astore 16
            //     244: aload_0
            //     245: getfield 12	miui/accounts/ExtraAccountManager$3:this$0	Lmiui/accounts/ExtraAccountManager;
            //     248: aload 16
            //     250: invokeinterface 82 1 0
            //     255: checkcast 84	android/os/Handler
            //     258: aload 16
            //     260: invokeinterface 87 1 0
            //     265: checkcast 89	android/accounts/MiuiOnAccountsUpdateListener
            //     268: aload 12
            //     270: invokestatic 117	miui/accounts/ExtraAccountManager:access$300	(Lmiui/accounts/ExtraAccountManager;Landroid/os/Handler;Landroid/accounts/MiuiOnAccountsUpdateListener;[Landroid/accounts/Account;)V
            //     273: goto -51 -> 222
            //     276: astore 14
            //     278: aload 13
            //     280: monitorexit
            //     281: aload 14
            //     283: athrow
            //     284: aload 13
            //     286: monitorexit
            //     287: goto -118 -> 169
            //
            // Exception table:
            //     from	to	target	type
            //     77	163	158	finally
            //     166	169	158	finally
            //     205	281	276	finally
            //     284	287	276	finally
        }
    };
    private Context mContext;
    private Handler mMainHandler;
    private final HashMap<MiuiOnAccountsUpdateListener, Handler> mMiuiAccountsUpdatedListeners = Maps.newHashMap();

    private ExtraAccountManager(Context paramContext)
    {
        this.mContext = paramContext;
        this.mAccountManager = AccountManager.get(this.mContext);
        this.mMainHandler = new Handler(this.mContext.getMainLooper());
    }

    public static ExtraAccountManager getInstance(Context paramContext)
    {
        if (mInstance == null)
            mInstance = new ExtraAccountManager(paramContext);
        return mInstance;
    }

    public static Account getXiaomiAccount(Context paramContext)
    {
        Account localAccount = null;
        Account[] arrayOfAccount = AccountManager.get(paramContext).getAccountsByType("com.xiaomi");
        if (arrayOfAccount.length > 0)
            localAccount = arrayOfAccount[0];
        return localAccount;
    }

    private void postToHandler(Handler paramHandler, final MiuiOnAccountsUpdateListener paramMiuiOnAccountsUpdateListener, final Account paramAccount, final int paramInt, final Bundle paramBundle, final boolean paramBoolean)
    {
        if (paramHandler == null)
            paramHandler = this.mMainHandler;
        paramHandler.post(new Runnable()
        {
            public void run()
            {
                try
                {
                    if (paramBoolean)
                        paramMiuiOnAccountsUpdateListener.onPreAccountUpdated(paramAccount, paramInt, paramBundle);
                    else
                        paramMiuiOnAccountsUpdateListener.onPostAccountUpdated(paramAccount, paramInt, paramBundle);
                }
                catch (SQLException localSQLException)
                {
                    Log.e("ExtraAccountManager", "Can't update accounts", localSQLException);
                }
            }
        });
    }

    private void postToHandler(Handler paramHandler, final MiuiOnAccountsUpdateListener paramMiuiOnAccountsUpdateListener, Account[] paramArrayOfAccount)
    {
        final Account[] arrayOfAccount = new Account[paramArrayOfAccount.length];
        System.arraycopy(paramArrayOfAccount, 0, arrayOfAccount, 0, arrayOfAccount.length);
        if (paramHandler == null)
            paramHandler = this.mMainHandler;
        paramHandler.post(new Runnable()
        {
            public void run()
            {
                try
                {
                    paramMiuiOnAccountsUpdateListener.onAccountsUpdated(arrayOfAccount);
                    return;
                }
                catch (SQLException localSQLException)
                {
                    while (true)
                        Log.e("ExtraAccountManager", "Can't update accounts", localSQLException);
                }
            }
        });
    }

    // ERROR //
    public void addOnAccountsUpdatedListener(MiuiOnAccountsUpdateListener paramMiuiOnAccountsUpdateListener, Handler paramHandler, boolean paramBoolean)
    {
        // Byte code:
        //     0: aload_1
        //     1: ifnonnull +13 -> 14
        //     4: new 160	java/lang/IllegalArgumentException
        //     7: dup
        //     8: ldc 162
        //     10: invokespecial 165	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     13: athrow
        //     14: aload_0
        //     15: getfield 81	miui/accounts/ExtraAccountManager:mMiuiAccountsUpdatedListeners	Ljava/util/HashMap;
        //     18: astore 4
        //     20: aload 4
        //     22: monitorenter
        //     23: aload_0
        //     24: getfield 81	miui/accounts/ExtraAccountManager:mMiuiAccountsUpdatedListeners	Ljava/util/HashMap;
        //     27: aload_1
        //     28: invokevirtual 171	java/util/HashMap:containsKey	(Ljava/lang/Object;)Z
        //     31: ifeq +21 -> 52
        //     34: new 173	java/lang/IllegalStateException
        //     37: dup
        //     38: ldc 175
        //     40: invokespecial 176	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     43: athrow
        //     44: astore 5
        //     46: aload 4
        //     48: monitorexit
        //     49: aload 5
        //     51: athrow
        //     52: aload_0
        //     53: getfield 81	miui/accounts/ExtraAccountManager:mMiuiAccountsUpdatedListeners	Ljava/util/HashMap;
        //     56: invokevirtual 180	java/util/HashMap:isEmpty	()Z
        //     59: istore 6
        //     61: aload_0
        //     62: getfield 81	miui/accounts/ExtraAccountManager:mMiuiAccountsUpdatedListeners	Ljava/util/HashMap;
        //     65: aload_1
        //     66: aload_2
        //     67: invokevirtual 184	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //     70: pop
        //     71: iload 6
        //     73: ifeq +54 -> 127
        //     76: new 186	android/content/IntentFilter
        //     79: dup
        //     80: invokespecial 187	android/content/IntentFilter:<init>	()V
        //     83: astore 8
        //     85: aload 8
        //     87: ldc 189
        //     89: invokevirtual 192	android/content/IntentFilter:addAction	(Ljava/lang/String;)V
        //     92: aload 8
        //     94: ldc 47
        //     96: invokevirtual 192	android/content/IntentFilter:addAction	(Ljava/lang/String;)V
        //     99: aload 8
        //     101: ldc 44
        //     103: invokevirtual 192	android/content/IntentFilter:addAction	(Ljava/lang/String;)V
        //     106: aload 8
        //     108: ldc 194
        //     110: invokevirtual 192	android/content/IntentFilter:addAction	(Ljava/lang/String;)V
        //     113: aload_0
        //     114: getfield 88	miui/accounts/ExtraAccountManager:mContext	Landroid/content/Context;
        //     117: aload_0
        //     118: getfield 86	miui/accounts/ExtraAccountManager:mAccountsChangedBroadcastReceiver	Landroid/content/BroadcastReceiver;
        //     121: aload 8
        //     123: invokevirtual 198	android/content/Context:registerReceiver	(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
        //     126: pop
        //     127: aload 4
        //     129: monitorexit
        //     130: iload_3
        //     131: ifeq +16 -> 147
        //     134: aload_0
        //     135: aload_2
        //     136: aload_1
        //     137: aload_0
        //     138: getfield 96	miui/accounts/ExtraAccountManager:mAccountManager	Landroid/accounts/AccountManager;
        //     141: invokevirtual 202	android/accounts/AccountManager:getAccounts	()[Landroid/accounts/Account;
        //     144: invokespecial 124	miui/accounts/ExtraAccountManager:postToHandler	(Landroid/os/Handler;Landroid/accounts/MiuiOnAccountsUpdateListener;[Landroid/accounts/Account;)V
        //     147: return
        //
        // Exception table:
        //     from	to	target	type
        //     23	49	44	finally
        //     52	130	44	finally
    }

    public void removeOnAccountsUpdatedListener(MiuiOnAccountsUpdateListener paramMiuiOnAccountsUpdateListener)
    {
        if (paramMiuiOnAccountsUpdateListener == null)
            throw new IllegalArgumentException("listener is null");
        synchronized (this.mMiuiAccountsUpdatedListeners)
        {
            if (!this.mMiuiAccountsUpdatedListeners.containsKey(paramMiuiOnAccountsUpdateListener))
            {
                Log.e("ExtraAccountManager", "Listener was not previously added");
            }
            else
            {
                this.mMiuiAccountsUpdatedListeners.remove(paramMiuiOnAccountsUpdateListener);
                if (this.mMiuiAccountsUpdatedListeners.isEmpty())
                    this.mContext.unregisterReceiver(this.mAccountsChangedBroadcastReceiver);
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.accounts.ExtraAccountManager
 * JD-Core Version:        0.6.2
 */