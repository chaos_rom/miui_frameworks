package miui.telephony;

import android.content.AsyncQueryHandler;
import android.content.AsyncQueryHandler.WorkerHandler;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.Settings.System;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.android.internal.telephony.gsm.MiuiSpnOverride;
import java.util.Locale;
import miui.provider.ExtraSettings.System;
import miui.telephony.phonenumber.ChineseAreaCode;
import miui.telephony.phonenumber.ChineseTelocation;
import miui.telephony.phonenumber.CountryCode;
import miui.telephony.phonenumber.Prefix;

public class PhoneNumberUtils extends android.telephony.PhoneNumberUtils
{
    private static final String CHINA_COUNTRY_CODE = "86";
    public static final String CHINA_MCC = "460";
    private static final int CHINA_MOBILE_NUMBER_LENGTH = 11;
    private static final String CHINA_MOBILE_NUMBER_PREFIX = "1";
    private static final String CHINA_REGION_CODE1 = "+86";
    private static final String CHINA_REGION_CODE2 = "0086";
    private static final String[] EMERGENCY_NUMBERS = arrayOfString;
    static final String LOG_TAG = "PhoneNumberUtils";
    public static final int MASK_PHONE_NUMBER_MODE_HEAD = 0;
    public static final int MASK_PHONE_NUMBER_MODE_MIDDLE = 2;
    public static final int MASK_PHONE_NUMBER_MODE_TAIL = 1;

    static
    {
        String[] arrayOfString = new String[6];
        arrayOfString[0] = "110";
        arrayOfString[1] = "112";
        arrayOfString[2] = "119";
        arrayOfString[3] = "120";
        arrayOfString[4] = "122";
        arrayOfString[5] = "911";
    }

    public static void cancelAsyncTelocationQuery(int paramInt)
    {
        TelocationAsyncQueryHandler.getInstance().cancelOperation(paramInt);
    }

    public static String extractNetworkPortion(String paramString)
    {
        return extractNetworkPortion(paramString, 0);
    }

    public static String extractNetworkPortion(String paramString, int paramInt)
    {
        String str;
        if (paramString == null)
            str = null;
        while (true)
        {
            return str;
            if ((paramInt == 3) || (isUriNumber(paramString)))
                str = paramString.substring(0, 1 + indexOfLastNetworkChar(paramString)).trim();
            else
                str = android.telephony.PhoneNumberUtils.extractNetworkPortion(paramString);
        }
    }

    public static String extractNetworkPortionAlt(String paramString)
    {
        return extractNetworkPortionAlt(paramString, 0);
    }

    public static String extractNetworkPortionAlt(String paramString, int paramInt)
    {
        String str;
        if (paramString == null)
            str = null;
        while (true)
        {
            return str;
            if ((paramInt == 3) || (isUriNumber(paramString)))
                str = paramString.substring(0, 1 + indexOfLastNetworkChar(paramString)).trim();
            else
                str = android.telephony.PhoneNumberUtils.extractNetworkPortionAlt(paramString);
        }
    }

    public static String formatNumberWithIp(Context paramContext, String paramString)
    {
        int i;
        String str2;
        if (Settings.System.getInt(paramContext.getContentResolver(), "button_autoip", 0) != 0)
        {
            i = 1;
            if (i != 0)
                break label30;
            str2 = paramString;
        }
        label30: String str1;
        String str3;
        while (true)
        {
            return str2;
            i = 0;
            break;
            str1 = ExtraSettings.System.getString(paramContext.getContentResolver(), "current_areacode", null);
            if ((str1 == null) || (str1.length() < 2))
            {
                str2 = paramString;
            }
            else
            {
                str3 = ExtraSettings.System.getString(paramContext.getContentResolver(), "autoip_prefix", getDefaultIpBySim(paramContext));
                if (TextUtils.isEmpty(str3))
                {
                    str2 = paramString;
                }
                else
                {
                    if (!paramString.startsWith(str3))
                        break label103;
                    str2 = paramString;
                }
            }
        }
        label103: PhoneNumber localPhoneNumber = PhoneNumber.parse(paramString);
        String str5;
        label144: int j;
        label160: int k;
        if (localPhoneNumber.getPrefix().length() == 0)
        {
            String str4 = localPhoneNumber.getLocationAreaCode(paramContext);
            if (!str1.startsWith("0"))
                break label272;
            str5 = str1.substring(1);
            if (Settings.System.getInt(paramContext.getContentResolver(), "button_add_zero_prefix", 0) == 0)
                break label278;
            j = 1;
            if (Settings.System.getInt(paramContext.getContentResolver(), "button_auto_ip_support_local_numbers", 0) == 0)
                break label284;
            k = 1;
            label176: if ((!TextUtils.isEmpty(str4)) && ((k != 0) || (!str5.equals(str4))))
            {
                if (!paramString.startsWith("+86"))
                    break label317;
                if ((localPhoneNumber.getAreaCode().length() <= 0) && ((!localPhoneNumber.isNormalMobileNumber()) || (j == 0)))
                    break label290;
                paramString = str3 + "0" + paramString.substring(3);
            }
        }
        while (true)
        {
            localPhoneNumber.recycle();
            str2 = paramString;
            break;
            label272: str5 = str1;
            break label144;
            label278: j = 0;
            break label160;
            label284: k = 0;
            break label176;
            label290: paramString = str3 + paramString.substring(3);
            continue;
            label317: if (paramString.startsWith("0086"))
            {
                if ((localPhoneNumber.getAreaCode().length() > 0) || ((localPhoneNumber.isNormalMobileNumber()) && (j != 0)))
                    paramString = str3 + "0" + paramString.substring(4);
                else
                    paramString = str3 + paramString.substring(4);
            }
            else if ((localPhoneNumber.isNormalMobileNumber()) && (j != 0))
                paramString = str3 + "0" + paramString;
            else
                paramString = str3 + paramString.replace("+", "00");
        }
    }

    public static String formatNumberWithoutIp(Context paramContext, String paramString)
    {
        int i = 0;
        String str2;
        if (paramString == null)
            str2 = paramString;
        while (true)
        {
            return str2;
            if (Settings.System.getInt(paramContext.getContentResolver(), "button_autoip", 0) != 0)
                i = 1;
            if (i == 0)
            {
                str2 = paramString;
            }
            else
            {
                String str1 = Settings.System.getString(paramContext.getContentResolver(), "autoip_prefix");
                if (str1 == null)
                    str1 = getDefaultIpBySim(paramContext);
                if (TextUtils.isEmpty(str1))
                {
                    str2 = paramString;
                }
                else
                {
                    if (paramString.startsWith(str1))
                        paramString = paramString.substring(str1.length());
                    str2 = paramString;
                }
            }
        }
    }

    public static String getDefaultIpBySim(Context paramContext)
    {
        String str1 = ((TelephonyManager)paramContext.getSystemService("phone")).getSimOperator();
        String str2 = MiuiSpnOverride.getInstance().getEquivalentOperatorNumeric(str1);
        String str3;
        if ("46000".equals(str2))
            str3 = "17951";
        while (true)
        {
            return str3;
            if ("46001".equals(str2))
                str3 = "17911";
            else if ("46003".equals(str2))
                str3 = "17901";
            else
                str3 = "";
        }
    }

    private static int indexOfLastNetworkChar(String paramString)
    {
        int i = paramString.length();
        int j = minPositive(paramString.indexOf(','), paramString.indexOf(';'));
        if (j < 0);
        for (int k = i - 1; ; k = j - 1)
            return k;
    }

    private static boolean isAlnum(char paramChar)
    {
        if (((paramChar >= '0') && (paramChar <= '9')) || ((paramChar >= 'a') && (paramChar <= 'z')) || ((paramChar >= 'A') && (paramChar <= 'Z')));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static boolean isChinaMobileNumber(String paramString)
    {
        boolean bool = false;
        if ((TextUtils.isEmpty(paramString)) || (paramString.length() < 11));
        while (true)
        {
            return bool;
            String str = stripSeparators(paramString);
            if (str.length() >= 11 + "86".length())
                bool = str.substring(-11 + str.length() - "86".length()).startsWith("861");
            else if (str.length() >= 11)
                bool = str.substring(-11 + str.length()).startsWith("1");
        }
    }

    public static boolean isChineseOperator(String paramString)
    {
        if ((!TextUtils.isEmpty(paramString)) && (paramString.startsWith("460")));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static boolean isDialable(String paramString)
    {
        int i = 0;
        int j = paramString.length();
        if (i < j)
            if (isDialable(paramString.charAt(i)));
        for (boolean bool = false; ; bool = true)
        {
            return bool;
            i++;
            break;
        }
    }

    public static boolean isMiuiEmergencyNumber(String paramString, boolean paramBoolean)
    {
        boolean bool = false;
        if (paramString == null);
        label68: 
        while (true)
        {
            return bool;
            String[] arrayOfString = EMERGENCY_NUMBERS;
            int i = arrayOfString.length;
            for (int j = 0; ; j++)
            {
                if (j >= i)
                    break label68;
                String str = arrayOfString[j];
                if (paramBoolean)
                {
                    if (!str.equals(paramString))
                        continue;
                    bool = true;
                    break;
                }
                if (str.startsWith(paramString))
                {
                    bool = true;
                    break;
                }
            }
        }
    }

    public static boolean isServiceNumber(String paramString)
    {
        PhoneNumber localPhoneNumber = PhoneNumber.parse(paramString);
        if ((localPhoneNumber != null) && (localPhoneNumber.isServiceNumber()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static String maskPhoneNumber(String paramString, int paramInt)
    {
        String str;
        if (paramString == null)
            str = "";
        while (true)
        {
            return str;
            int i = 0;
            for (int j = 0; j < paramString.length(); j++)
                if (isAlnum(paramString.charAt(j)))
                    i++;
            if (i < 7)
            {
                str = new String(paramString);
            }
            else
            {
                if (i < 11);
                for (int k = 2; ; k = 3)
                    switch (paramInt)
                    {
                    default:
                        throw new IllegalArgumentException("Invalid cut mode");
                    case 0:
                    case 1:
                    case 2:
                    }
                int m = 0;
                StringBuilder localStringBuilder = new StringBuilder();
                int n = 0;
                int i1 = 0;
                label135: if (i1 < paramString.length())
                {
                    if (isAlnum(paramString.charAt(i1)))
                        if ((n < m) || (k <= 0))
                        {
                            localStringBuilder.append(paramString.charAt(i1));
                            label180: n++;
                        }
                    while (true)
                    {
                        i1++;
                        break label135;
                        m = i - k;
                        break;
                        m = (i - k) / 2;
                        break;
                        localStringBuilder.append('?');
                        k--;
                        break label180;
                        localStringBuilder.append(paramString.charAt(i1));
                    }
                }
                str = localStringBuilder.toString();
            }
        }
    }

    private static int minPositive(int paramInt1, int paramInt2)
    {
        if ((paramInt1 >= 0) && (paramInt2 >= 0))
            if (paramInt1 >= paramInt2);
        while (true)
        {
            return paramInt1;
            paramInt1 = paramInt2;
            continue;
            if (paramInt1 < 0)
                if (paramInt2 >= 0)
                    paramInt1 = paramInt2;
                else
                    paramInt1 = -1;
        }
    }

    public static String parseNumber(String paramString)
    {
        if (TelephonyManager.getDefault().getSimState() == 5)
        {
            PhoneNumber localPhoneNumber = PhoneNumber.parse(paramString);
            if (localPhoneNumber != null)
                paramString = localPhoneNumber.getEffectiveNumber();
        }
        return paramString;
    }

    public static String parseTelocationString(Context paramContext, CharSequence paramCharSequence)
    {
        return TelocationAsyncQueryHandler.queryTelocation(paramContext, paramCharSequence);
    }

    public static void queryTelocationStringAsync(int paramInt, Object paramObject1, Object paramObject2, Object paramObject3, Object paramObject4, TelocationQueryListener paramTelocationQueryListener, Context paramContext, String paramString)
    {
        if (Settings.System.getInt(paramContext.getContentResolver(), "enable_telocation", 1) == 1)
            TelocationAsyncQueryHandler.getInstance().startQueryTelocationString(paramInt, paramObject1, paramObject2, paramObject3, paramObject4, paramTelocationQueryListener, paramContext, paramString);
        while (true)
        {
            return;
            paramTelocationQueryListener.onComplete(paramObject1, paramObject2, paramObject3, paramObject4, null);
        }
    }

    public static void queryTelocationStringAsync(int paramInt, Object paramObject1, Object paramObject2, Object paramObject3, Object paramObject4, TelocationQueryListener paramTelocationQueryListener, Context paramContext, String paramString, boolean paramBoolean)
    {
        if (paramBoolean)
            TelocationAsyncQueryHandler.getInstance().startQueryTelocationString(paramInt, paramObject1, paramObject2, paramObject3, paramObject4, paramTelocationQueryListener, paramContext, paramString);
        while (true)
        {
            return;
            paramTelocationQueryListener.onComplete(paramObject1, paramObject2, paramObject3, paramObject4, null);
        }
    }

    public static String removeDashesAndBlanks(String paramString)
    {
        if (TextUtils.isEmpty(paramString));
        while (true)
        {
            return paramString;
            StringBuilder localStringBuilder = new StringBuilder();
            for (int i = 0; i < paramString.length(); i++)
            {
                char c = paramString.charAt(i);
                if ((c != ' ') && (c != '-'))
                    localStringBuilder.append(c);
            }
            paramString = localStringBuilder.toString();
        }
    }

    public static String[] splitNetworkAndPostDialPortion(String paramString)
    {
        if (paramString == null)
        {
            arrayOfString = null;
            return arrayOfString;
        }
        int i = 1 + indexOfLastNetworkChar(paramString);
        String[] arrayOfString = new String[2];
        arrayOfString[0] = paramString.substring(0, i);
        if (i == paramString.length());
        for (String str = ""; ; str = paramString.substring(i))
        {
            arrayOfString[1] = str;
            break;
        }
    }

    public static String stripSeparatorsAndCountryCode(String paramString)
    {
        String str = stripSeparators(paramString);
        if (str != null)
        {
            if (!str.startsWith("+86"))
                break label30;
            str = str.substring("+86".length());
        }
        while (true)
        {
            return str;
            label30: if (str.startsWith("0086"))
                str = str.substring("0086".length());
        }
    }

    private static class TelocationAsyncQueryHandler extends AsyncQueryHandler
    {
        private static final int EVENT_QUERY_TELOCATION = 10;
        private static TelocationAsyncQueryHandler sInstance;
        private Handler mWorkerHandler;

        private TelocationAsyncQueryHandler()
        {
            super();
        }

        public static TelocationAsyncQueryHandler getInstance()
        {
            if (sInstance == null);
            try
            {
                if (sInstance == null)
                    sInstance = new TelocationAsyncQueryHandler();
                return sInstance;
            }
            finally
            {
            }
        }

        public static String queryTelocation(Context paramContext, CharSequence paramCharSequence)
        {
            return PhoneNumberUtils.PhoneNumber.getLocation(paramContext, paramCharSequence);
        }

        protected Handler createHandler(Looper paramLooper)
        {
            if (this.mWorkerHandler == null)
                this.mWorkerHandler = new TelocationWorkerHandler(paramLooper);
            return this.mWorkerHandler;
        }

        public void handleMessage(Message paramMessage)
        {
            TelocationWorkerArgs localTelocationWorkerArgs = (TelocationWorkerArgs)paramMessage.obj;
            if ((paramMessage.arg1 == 10) && (localTelocationWorkerArgs.listener != null))
                localTelocationWorkerArgs.listener.onComplete(localTelocationWorkerArgs.cookie1, localTelocationWorkerArgs.cookie2, localTelocationWorkerArgs.cookie3, localTelocationWorkerArgs.cookie4, localTelocationWorkerArgs.location);
        }

        public void startQueryTelocationString(int paramInt, Object paramObject1, Object paramObject2, Object paramObject3, Object paramObject4, PhoneNumberUtils.TelocationQueryListener paramTelocationQueryListener, Context paramContext, String paramString)
        {
            TelocationWorkerArgs localTelocationWorkerArgs = new TelocationWorkerArgs();
            localTelocationWorkerArgs.handler = this;
            localTelocationWorkerArgs.context = paramContext;
            localTelocationWorkerArgs.phoneNumber = paramString;
            localTelocationWorkerArgs.cookie1 = paramObject1;
            localTelocationWorkerArgs.cookie2 = paramObject2;
            localTelocationWorkerArgs.cookie3 = paramObject3;
            localTelocationWorkerArgs.cookie4 = paramObject4;
            localTelocationWorkerArgs.listener = paramTelocationQueryListener;
            localTelocationWorkerArgs.location = null;
            Message localMessage = this.mWorkerHandler.obtainMessage(paramInt);
            localMessage.arg1 = 10;
            localMessage.obj = localTelocationWorkerArgs;
            localMessage.sendToTarget();
        }

        protected class TelocationWorkerHandler extends AsyncQueryHandler.WorkerHandler
        {
            public TelocationWorkerHandler(Looper arg2)
            {
                super(localLooper);
            }

            public void handleMessage(Message paramMessage)
            {
                PhoneNumberUtils.TelocationAsyncQueryHandler.TelocationWorkerArgs localTelocationWorkerArgs = (PhoneNumberUtils.TelocationAsyncQueryHandler.TelocationWorkerArgs)paramMessage.obj;
                if (paramMessage.arg1 == 10)
                    localTelocationWorkerArgs.location = PhoneNumberUtils.TelocationAsyncQueryHandler.queryTelocation(localTelocationWorkerArgs.context, localTelocationWorkerArgs.phoneNumber);
                Message localMessage = localTelocationWorkerArgs.handler.obtainMessage(paramMessage.what);
                localMessage.arg1 = paramMessage.arg1;
                localMessage.obj = paramMessage.obj;
                localMessage.sendToTarget();
            }
        }

        protected static final class TelocationWorkerArgs
        {
            public Context context;
            public Object cookie1;
            public Object cookie2;
            public Object cookie3;
            public Object cookie4;
            public Handler handler;
            public PhoneNumberUtils.TelocationQueryListener listener;
            public String location;
            public String phoneNumber;
        }
    }

    public static abstract interface TelocationQueryListener
    {
        public abstract void onComplete(Object paramObject1, Object paramObject2, Object paramObject3, Object paramObject4, String paramString);
    }

    public static class PhoneNumber
    {
        private static final String EMPTY = "";
        private static final char HASH_STRING_INDICATOR = '\001';
        private static final int MAX_NUMBER_LENGTH = 256;
        private static final char MISSING_AREA_CODE_INDICATOR = '\002';
        private static final int POOL_SIZE = 10;
        private static final PhoneNumber[] sPool = new PhoneNumber[10];
        private static int sPoolIndex = -1;
        private String mAreaCode;
        private StringBuffer mBuffer = new StringBuffer(256);
        private String mCountryCode;
        private String mEffectiveNumber;
        private int mEffectiveNumberStart;
        private CharSequence mOriginal;
        private String mPostDialString;
        private int mPostDialStringStart;
        private String mPrefix;

        private PhoneNumber()
        {
            clear();
        }

        public static String addCountryCode(String paramString)
        {
            PhoneNumber localPhoneNumber = parse(paramString);
            boolean bool = TextUtils.isEmpty(localPhoneNumber.getCountryCode());
            String str2;
            if (localPhoneNumber.isChineseNumber())
            {
                if (!TextUtils.isEmpty(localPhoneNumber.getPrefix()))
                    bool = false;
            }
            else
            {
                str1 = paramString;
                if (bool)
                {
                    str2 = CountryCode.getUserDefinedCountryCode();
                    if (TextUtils.isEmpty(str2))
                        str2 = CountryCode.getIccCountryCode();
                    if (!TextUtils.isEmpty(str2))
                        if ((!"39".equals(str2)) && (paramString.charAt(0) == '0'))
                            break label154;
                }
            }
            label154: for (String str1 = "+" + str2 + paramString; ; str1 = "+" + str2 + paramString.substring(1))
            {
                localPhoneNumber.recycle();
                return str1;
                if (localPhoneNumber.isServiceNumber())
                {
                    bool = false;
                    break;
                }
                if (localPhoneNumber.isNormalMobileNumber())
                    break;
                if (!TextUtils.isEmpty(localPhoneNumber.getAreaCode()));
                for (bool = true; ; bool = false)
                    break;
            }
        }

        private static boolean areEqual(CharSequence paramCharSequence1, int paramInt1, CharSequence paramCharSequence2, int paramInt2, int paramInt3)
        {
            boolean bool = false;
            if ((paramCharSequence1 == null) || (paramCharSequence2 == null) || (paramInt1 < 0) || (paramInt2 < 0) || (paramInt3 < 0));
            while (true)
            {
                return bool;
                if ((paramCharSequence1.length() >= paramInt1 + paramInt3) && (paramCharSequence2.length() >= paramInt2 + paramInt3))
                {
                    for (int i = 0; ; i++)
                    {
                        if (i >= paramInt3)
                            break label92;
                        if (paramCharSequence1.charAt(paramInt1 + i) != paramCharSequence2.charAt(paramInt2 + i))
                            break;
                    }
                    label92: bool = true;
                }
            }
        }

        private void attach(CharSequence paramCharSequence)
        {
            if (paramCharSequence == null)
                paramCharSequence = "";
            this.mOriginal = paramCharSequence;
            int i = 0;
            int j = 0;
            int k = paramCharSequence.length();
            if (j < k)
            {
                char c = paramCharSequence.charAt(j);
                if ((i != 0) && (PhoneNumberUtils.isNonSeparator(c)))
                    this.mBuffer.append(c);
                while (true)
                {
                    j++;
                    break;
                    if ((j == 0) && (c == '+'))
                    {
                        this.mBuffer.append(c);
                    }
                    else if ((c >= '0') && (c <= '9'))
                    {
                        this.mBuffer.append(c);
                    }
                    else if ((i == 0) && (PhoneNumberUtils.isStartsPostDial(c)))
                    {
                        this.mPostDialStringStart = this.mBuffer.length();
                        i = 1;
                        this.mBuffer.append(c);
                    }
                }
            }
            if (i == 0)
                this.mPostDialStringStart = this.mBuffer.length();
        }

        private void clear()
        {
            this.mBuffer.setLength(0);
            this.mPrefix = null;
            this.mCountryCode = null;
            this.mAreaCode = null;
            this.mEffectiveNumberStart = 0;
            this.mEffectiveNumber = null;
            this.mPostDialStringStart = 0;
            this.mPostDialString = null;
        }

        public static String getDefaultCountryCode()
        {
            return CountryCode.getIccCountryCode();
        }

        public static String getDialableNumber(String paramString)
        {
            if (TextUtils.isEmpty(paramString))
                paramString = "";
            label47: 
            while (true)
            {
                return paramString;
                int i = paramString.indexOf(2);
                if (i < 0);
                for (int j = 1; ; j = i + 1)
                {
                    if (paramString.charAt(0) != '\001')
                        break label47;
                    paramString = paramString.substring(j);
                    break;
                }
            }
        }

        public static String getHashString(String paramString)
        {
            PhoneNumber localPhoneNumber = parse(paramString);
            String str1;
            String str2;
            if (localPhoneNumber.isSmsPrefix())
            {
                str1 = localPhoneNumber.getPrefix() + localPhoneNumber.getEffectiveNumber();
                if (localPhoneNumber.isChineseNumber())
                    break label103;
                Object[] arrayOfObject6 = new Object[4];
                arrayOfObject6[0] = Character.valueOf('\001');
                arrayOfObject6[1] = localPhoneNumber.getCountryCode();
                arrayOfObject6[2] = str1;
                arrayOfObject6[3] = localPhoneNumber.getPostDialString();
                str2 = String.format("%c(00%s)%s%s", arrayOfObject6);
            }
            while (true)
            {
                localPhoneNumber.recycle();
                return str2;
                str1 = localPhoneNumber.getEffectiveNumber();
                break;
                label103: if (localPhoneNumber.isNormalMobileNumber())
                {
                    Object[] arrayOfObject5 = new Object[4];
                    arrayOfObject5[0] = Character.valueOf('\001');
                    arrayOfObject5[1] = "86";
                    arrayOfObject5[2] = str1;
                    arrayOfObject5[3] = localPhoneNumber.getPostDialString();
                    str2 = String.format("%c(00%s)%s%s", arrayOfObject5);
                }
                else if (!TextUtils.isEmpty(localPhoneNumber.getCountryCode()))
                {
                    if (!TextUtils.isEmpty(localPhoneNumber.getAreaCode()))
                    {
                        Object[] arrayOfObject4 = new Object[5];
                        arrayOfObject4[0] = Character.valueOf('\001');
                        arrayOfObject4[1] = "86";
                        arrayOfObject4[2] = localPhoneNumber.getAreaCode();
                        arrayOfObject4[3] = str1;
                        arrayOfObject4[4] = localPhoneNumber.getPostDialString();
                        str2 = String.format("%c(00%s)%s-%s%s", arrayOfObject4);
                    }
                    else
                    {
                        Object[] arrayOfObject3 = new Object[4];
                        arrayOfObject3[0] = Character.valueOf('\001');
                        arrayOfObject3[1] = "86";
                        arrayOfObject3[2] = str1;
                        arrayOfObject3[3] = localPhoneNumber.getPostDialString();
                        str2 = String.format("%c(00%s)%s%s", arrayOfObject3);
                    }
                }
                else if (!TextUtils.isEmpty(localPhoneNumber.getAreaCode()))
                {
                    Object[] arrayOfObject2 = new Object[5];
                    arrayOfObject2[0] = Character.valueOf('\001');
                    arrayOfObject2[1] = "86";
                    arrayOfObject2[2] = localPhoneNumber.getAreaCode();
                    arrayOfObject2[3] = str1;
                    arrayOfObject2[4] = localPhoneNumber.getPostDialString();
                    str2 = String.format("%c(00%s)%s-%s%s", arrayOfObject2);
                }
                else
                {
                    Object[] arrayOfObject1 = new Object[5];
                    arrayOfObject1[0] = Character.valueOf('\001');
                    arrayOfObject1[1] = "86";
                    arrayOfObject1[2] = Character.valueOf('\002');
                    arrayOfObject1[3] = str1;
                    arrayOfObject1[4] = localPhoneNumber.getPostDialString();
                    str2 = String.format("%c(00%s)%c%s%s", arrayOfObject1);
                }
            }
        }

        public static String getLocation(Context paramContext, CharSequence paramCharSequence)
        {
            PhoneNumber localPhoneNumber = parse(paramCharSequence);
            String str = localPhoneNumber.getLocation(paramContext);
            localPhoneNumber.recycle();
            return str;
        }

        public static String getLocationAreaCode(Context paramContext, String paramString)
        {
            PhoneNumber localPhoneNumber = parse(paramString);
            String str = localPhoneNumber.getLocationAreaCode(paramContext);
            localPhoneNumber.recycle();
            return str;
        }

        public static boolean isChineseOperator()
        {
            return CountryCode.isChinaEnvironment();
        }

        public static boolean isValidCountryCode(String paramString)
        {
            return CountryCode.isValidCountryCode(paramString);
        }

        public static PhoneNumber parse(CharSequence paramCharSequence)
        {
            synchronized (sPool)
            {
                if (sPoolIndex == -1)
                {
                    localPhoneNumber = new PhoneNumber();
                    localPhoneNumber.attach(paramCharSequence);
                    return localPhoneNumber;
                }
                PhoneNumber localPhoneNumber = sPool[sPoolIndex];
                PhoneNumber[] arrayOfPhoneNumber2 = sPool;
                int i = sPoolIndex;
                sPoolIndex = i - 1;
                arrayOfPhoneNumber2[i] = null;
            }
        }

        public static String replaceCdmaInternationalAccessCode(String paramString)
        {
            String str1;
            if ((paramString.startsWith("+86")) && ("86".equals(CountryCode.getNetworkCountryCode())))
            {
                str1 = paramString.substring(3);
                if (!PhoneNumberUtils.isChinaMobileNumber(str1));
            }
            while (true)
            {
                return str1;
                if (str1.charAt(0) != '0')
                {
                    str1 = '0' + str1;
                    continue;
                    if ((!TextUtils.isEmpty(paramString)) && (paramString.charAt(0) == '+'))
                    {
                        String str2 = CountryCode.getIddCode();
                        paramString = str2 + paramString.substring(1);
                    }
                    str1 = paramString;
                }
            }
        }

        public String getAreaCode()
        {
            if (this.mAreaCode == null)
            {
                this.mAreaCode = "";
                if ((isChineseNumber()) && (!Prefix.isSmsPrefix(getPrefix())))
                {
                    int i = 1;
                    if (TextUtils.isEmpty(getCountryCode()))
                    {
                        i = 0;
                        if ((-1 + this.mBuffer.length() > this.mEffectiveNumberStart) && (this.mBuffer.charAt(this.mEffectiveNumberStart) == '0'))
                        {
                            i = 1;
                            this.mEffectiveNumberStart = (1 + this.mEffectiveNumberStart);
                        }
                    }
                    if (i != 0)
                    {
                        this.mAreaCode = ChineseAreaCode.parse(this.mBuffer, this.mEffectiveNumberStart, this.mPostDialStringStart - this.mEffectiveNumberStart);
                        if (this.mAreaCode.length() != 0)
                            break label143;
                    }
                }
            }
            label143: for (this.mEffectiveNumberStart = (-1 + this.mEffectiveNumberStart); ; this.mEffectiveNumberStart += this.mAreaCode.length())
                return this.mAreaCode;
        }

        public String getCountryCode()
        {
            if (this.mCountryCode == null)
            {
                getPrefix();
                String str = "+";
                if (!areEqual(this.mBuffer, this.mEffectiveNumberStart, str, 0, str.length()))
                {
                    str = CountryCode.getIddCode();
                    if (!areEqual(this.mBuffer, this.mEffectiveNumberStart, str, 0, str.length()))
                        str = null;
                }
                if (str == null)
                    break label149;
                this.mEffectiveNumberStart += str.length();
                this.mCountryCode = CountryCode.parse(this.mBuffer, this.mEffectiveNumberStart, this.mPostDialStringStart - this.mEffectiveNumberStart);
                if (this.mCountryCode.length() != 0)
                    break label130;
                this.mEffectiveNumberStart -= str.length();
            }
            while (true)
            {
                return this.mCountryCode;
                label130: this.mEffectiveNumberStart += this.mCountryCode.length();
                continue;
                label149: this.mCountryCode = "";
            }
        }

        public String getEffectiveNumber()
        {
            String str2;
            if (this.mEffectiveNumber == null)
            {
                getAreaCode();
                if (this.mBuffer.length() > this.mEffectiveNumberStart)
                    this.mEffectiveNumber = this.mBuffer.substring(this.mEffectiveNumberStart, this.mPostDialStringStart);
            }
            else
            {
                if (!TextUtils.isEmpty(this.mEffectiveNumber))
                    break label84;
                str2 = this.mOriginal.toString();
                this.mOriginal = str2;
            }
            label84: for (String str1 = str2.toString(); ; str1 = this.mEffectiveNumber)
            {
                return str1;
                this.mEffectiveNumber = "";
                break;
            }
        }

        public String getLocation(Context paramContext)
        {
            Locale localLocale = paramContext.getResources().getConfiguration().locale;
            String str;
            if ((!localLocale.getLanguage().equals(Locale.CHINA.getLanguage())) || (!isChineseNumber()))
            {
                str = ChineseTelocation.getInstance().getExternalLocation(paramContext, getCountryCode(), this.mOriginal, localLocale);
                return str;
            }
            ChineseTelocation localChineseTelocation = ChineseTelocation.getInstance();
            StringBuffer localStringBuffer = this.mBuffer;
            int i = this.mEffectiveNumberStart;
            int j = this.mPostDialStringStart - this.mEffectiveNumberStart;
            if ((isNormalMobileNumber()) || (getAreaCode().length() > 0));
            for (boolean bool = true; ; bool = false)
            {
                str = localChineseTelocation.getLocation(paramContext, localStringBuffer, i, j, bool);
                break;
            }
        }

        public String getLocationAreaCode(Context paramContext)
        {
            String str = "";
            if (isChineseNumber())
                if (!isNormalMobileNumber())
                    break label44;
            label44: for (str = ChineseTelocation.getInstance().getAreaCode(paramContext, this.mBuffer, this.mEffectiveNumberStart, this.mPostDialStringStart - this.mEffectiveNumberStart); ; str = getAreaCode())
                return str;
        }

        public String getNormalizedNumber(boolean paramBoolean1, boolean paramBoolean2)
        {
            int k;
            int m;
            label38: String str1;
            if (!isChineseNumber())
                if (paramBoolean1)
                {
                    k = this.mEffectiveNumberStart - getCountryCode().length();
                    if (!paramBoolean2)
                        break label99;
                    m = this.mBuffer.length();
                    str1 = this.mBuffer.substring(k, m);
                    if ((paramBoolean1) && (getCountryCode().length() > 0))
                        str1 = "+" + str1;
                }
            while (true)
            {
                return str1;
                k = this.mEffectiveNumberStart;
                break;
                label99: m = this.mPostDialStringStart;
                break label38;
                if (isNormalMobileNumber())
                {
                    if (paramBoolean2);
                    for (int j = this.mBuffer.length(); ; j = this.mPostDialStringStart)
                    {
                        str1 = this.mBuffer.substring(this.mEffectiveNumberStart, j);
                        if (!paramBoolean1)
                            break;
                        str1 = "+86" + str1;
                        break;
                    }
                }
                int i;
                if (paramBoolean2)
                    i = this.mBuffer.length();
                while (true)
                    if ((!TextUtils.isEmpty(getAreaCode())) && (!isServiceNumber()))
                    {
                        String str2 = this.mBuffer.substring(this.mEffectiveNumberStart - getAreaCode().length(), i);
                        if (paramBoolean1)
                        {
                            str1 = "+86" + str2;
                            break;
                            i = this.mPostDialStringStart;
                            continue;
                        }
                        str1 = "0" + str2;
                        break;
                    }
                str1 = this.mBuffer.substring(this.mEffectiveNumberStart, i);
            }
        }

        public String getNumberWithoutPrefix(boolean paramBoolean)
        {
            int i = 0;
            if (!TextUtils.isEmpty(getPrefix()))
                i = getPrefix().length();
            if (paramBoolean);
            for (String str = this.mBuffer.substring(i); ; str = this.mBuffer.substring(i, this.mPostDialStringStart))
                return str;
        }

        public String getPostDialString()
        {
            if (this.mPostDialString == null)
                if (this.mBuffer.length() <= this.mPostDialStringStart)
                    break label41;
            label41: for (this.mPostDialString = this.mBuffer.substring(this.mPostDialStringStart); ; this.mPostDialString = "")
                return this.mPostDialString;
        }

        public String getPrefix()
        {
            if (this.mPrefix == null)
            {
                this.mPrefix = Prefix.parse(this.mBuffer, this.mEffectiveNumberStart, this.mPostDialStringStart - this.mEffectiveNumberStart);
                this.mEffectiveNumberStart += this.mPrefix.length();
            }
            return this.mPrefix;
        }

        public boolean isChineseNumber()
        {
            String str = getCountryCode();
            if (!TextUtils.isEmpty(str));
            for (boolean bool = "86".equals(str); ; bool = CountryCode.isChinaEnvironment())
                return bool;
        }

        public boolean isNormalMobileNumber()
        {
            boolean bool1 = true;
            boolean bool2 = false;
            getAreaCode();
            if ((isChineseNumber()) && (this.mPostDialStringStart - this.mEffectiveNumberStart >= 11) && (this.mBuffer.charAt(this.mEffectiveNumberStart) == '1'))
                switch (this.mBuffer.charAt(1 + this.mEffectiveNumberStart))
                {
                case '6':
                default:
                case '3':
                case '7':
                case '4':
                case '5':
                case '8':
                }
            while (true)
            {
                return bool2;
                if ((this.mBuffer.charAt(2 + this.mEffectiveNumberStart) != '8') || (this.mBuffer.charAt(3 + this.mEffectiveNumberStart) != '0') || (this.mBuffer.charAt(4 + this.mEffectiveNumberStart) != '0') || (this.mBuffer.charAt(5 + this.mEffectiveNumberStart) != '1') || (this.mBuffer.charAt(6 + this.mEffectiveNumberStart) != '3') || (this.mBuffer.charAt(7 + this.mEffectiveNumberStart) != '8') || (this.mBuffer.charAt(8 + this.mEffectiveNumberStart) != '0') || (this.mBuffer.charAt(9 + this.mEffectiveNumberStart) != '0') || (this.mBuffer.charAt(10 + this.mEffectiveNumberStart) != '0'))
                {
                    bool2 = bool1;
                    continue;
                    if (this.mBuffer.charAt(2 + this.mEffectiveNumberStart) != '9');
                    while (true)
                    {
                        bool2 = bool1;
                        break;
                        bool1 = false;
                    }
                    bool2 = bool1;
                }
            }
        }

        public boolean isServiceNumber()
        {
            boolean bool = true;
            getAreaCode();
            int i;
            if ((isChineseNumber()) && (this.mPostDialStringStart - this.mEffectiveNumberStart > 2))
            {
                i = this.mBuffer.charAt(this.mEffectiveNumberStart);
                if (i == 49)
                    switch (this.mBuffer.charAt(1 + this.mEffectiveNumberStart))
                    {
                    case '4':
                    case '5':
                    default:
                        bool = false;
                    case '0':
                    case '1':
                    case '2':
                    case '6':
                    case '3':
                    case '7':
                    }
            }
            while (true)
            {
                return bool;
                if ((this.mPostDialStringStart - this.mEffectiveNumberStart < 11) || (this.mBuffer.charAt(2 + this.mEffectiveNumberStart) != '8') || (this.mBuffer.charAt(3 + this.mEffectiveNumberStart) != '0') || (this.mBuffer.charAt(4 + this.mEffectiveNumberStart) != '0') || (this.mBuffer.charAt(5 + this.mEffectiveNumberStart) != '1') || (this.mBuffer.charAt(6 + this.mEffectiveNumberStart) != '3') || (this.mBuffer.charAt(7 + this.mEffectiveNumberStart) != '8') || (this.mBuffer.charAt(8 + this.mEffectiveNumberStart) != '0') || (this.mBuffer.charAt(9 + this.mEffectiveNumberStart) != '0') || (this.mBuffer.charAt(10 + this.mEffectiveNumberStart) != '0'))
                {
                    bool = false;
                    continue;
                    if (this.mBuffer.charAt(2 + this.mEffectiveNumberStart) != '9')
                    {
                        bool = false;
                        continue;
                        if (i != 57)
                            if ((i >= 50) && (i <= 56))
                            {
                                if ((this.mBuffer.charAt(1 + this.mEffectiveNumberStart) != '0') || (this.mBuffer.charAt(2 + this.mEffectiveNumberStart) != '0'))
                                    bool = false;
                            }
                            else
                                bool = false;
                    }
                }
            }
        }

        public boolean isSmsPrefix()
        {
            return Prefix.isSmsPrefix(getPrefix());
        }

        public void recycle()
        {
            clear();
            synchronized (sPool)
            {
                if (sPoolIndex < sPool.length)
                {
                    PhoneNumber[] arrayOfPhoneNumber2 = sPool;
                    int i = 1 + sPoolIndex;
                    sPoolIndex = i;
                    arrayOfPhoneNumber2[i] = this;
                }
                return;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.telephony.PhoneNumberUtils
 * JD-Core Version:        0.6.2
 */