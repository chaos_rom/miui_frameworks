package miui.telephony;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Looper;
import android.os.SystemProperties;
import android.provider.Settings.System;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.android.internal.telephony.Call.State;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.atomic.AtomicReference;
import miui.telephony.exception.IllegalDeviceException;

public class ExtraTelephonyManager
{
    public static final String ACTION_DEVICE_ID_READY = "android.intent.action.DEVICE_ID_READY";
    public static final String ACTION_ENTER_INCALL_SCREEN_DURING_CALL = "android.intent.action.ENTER_INCALL_SCREEN_DURING_CALL";
    public static final String ACTION_LEAVE_INCALL_SCREEN_DURING_CALL = "android.intent.action.LEAVE_INCALL_SCREEN_DURING_CALL";
    public static final String EXTRA_BASE_TIME = "base_time";
    public static final String EXTRA_CALL_STATE = "call_state";
    public static final String EXTRA_CALL_STATE_ACTIVE = Call.State.ACTIVE.toString();
    public static final String EXTRA_CALL_STATE_DIALING;
    public static final String EXTRA_CALL_STATE_HOLDING = Call.State.HOLDING.toString();
    public static final String EXTRA_DEVICE_ID = "device_id";
    private static final AtomicReference<Future<String>> mDeviceIdCache = new AtomicReference();
    private static final AtomicReference<Future<String>> mIccIdCache = new AtomicReference();
    private static final AtomicReference<Future<String>> mImsiCache = new AtomicReference();

    static
    {
        EXTRA_CALL_STATE_DIALING = Call.State.DIALING.toString();
    }

    public static String blockingGetDeviceId(Context paramContext)
        throws IllegalDeviceException
    {
        Future localFuture = (Future)mDeviceIdCache.get();
        if (localFuture == null)
        {
            FutureTask localFutureTask = new FutureTask(new Callable()
            {
                public String call()
                    throws Exception
                {
                    return ExtraTelephonyManager.waitAndGetDeviceId(ExtraTelephonyManager.this);
                }
            });
            if (mDeviceIdCache.compareAndSet(null, localFutureTask))
                localFutureTask.run();
            localFuture = (Future)mDeviceIdCache.get();
        }
        String str = null;
        try
        {
            str = (String)localFuture.get();
            if (TextUtils.isEmpty(str))
                throw new IllegalDeviceException("device id is empty");
        }
        catch (Exception localException)
        {
            while (true)
                localException.printStackTrace();
        }
        return str;
    }

    public static String blockingGetIccId(Context paramContext)
        throws IllegalDeviceException
    {
        Future localFuture = (Future)mIccIdCache.get();
        if (localFuture == null)
        {
            FutureTask localFutureTask = new FutureTask(new Callable()
            {
                public String call()
                    throws Exception
                {
                    return ExtraTelephonyManager.waitAndGetIccid(ExtraTelephonyManager.this);
                }
            });
            if (mIccIdCache.compareAndSet(null, localFutureTask))
                localFutureTask.run();
            localFuture = (Future)mIccIdCache.get();
        }
        String str = null;
        try
        {
            str = (String)localFuture.get();
            if (TextUtils.isEmpty(str))
                throw new IllegalDeviceException("icc id is is empty");
        }
        catch (Exception localException)
        {
            while (true)
                localException.printStackTrace();
        }
        return str;
    }

    public static String blockingGetImsi(Context paramContext)
        throws IllegalDeviceException
    {
        Future localFuture = (Future)mImsiCache.get();
        if (localFuture == null)
        {
            FutureTask localFutureTask = new FutureTask(new Callable()
            {
                public String call()
                    throws Exception
                {
                    return ExtraTelephonyManager.waitAndGetImsi(ExtraTelephonyManager.this);
                }
            });
            if (mImsiCache.compareAndSet(null, localFutureTask))
                localFutureTask.run();
            localFuture = (Future)mImsiCache.get();
        }
        String str = null;
        try
        {
            str = (String)localFuture.get();
            if (TextUtils.isEmpty(str))
                throw new IllegalDeviceException("imsi is is empty");
        }
        catch (Exception localException)
        {
            while (true)
                localException.printStackTrace();
        }
        return str;
    }

    private static void ensureNotOnMainThread(Context paramContext)
    {
        Looper localLooper = Looper.myLooper();
        if ((localLooper != null) && (localLooper == paramContext.getMainLooper()))
            throw new IllegalStateException("calling this from your main thread can lead to deadlock");
    }

    public static String getSimOperator(Context paramContext)
    {
        Object localObject = SystemProperties.get("gsm.sim.operator.numeric");
        if (paramContext == null);
        while (true)
        {
            return localObject;
            if (("com.android.vending".equals(paramContext.getPackageName())) && (PhoneNumberUtils.isChineseOperator((String)localObject)))
            {
                String str = Settings.System.getString(paramContext.getContentResolver(), "fake_mobile_operator_for_vending");
                if (str != null)
                {
                    if (str.length() != 0)
                        localObject = str;
                }
                else
                    localObject = "310410";
            }
        }
    }

    // ERROR //
    private static String waitAndGetDeviceId(Context paramContext)
        throws IllegalDeviceException
    {
        // Byte code:
        //     0: aload_0
        //     1: invokestatic 206	miui/telephony/ExtraTelephonyManager:ensureNotOnMainThread	(Landroid/content/Context;)V
        //     4: aload_0
        //     5: ldc 208
        //     7: invokevirtual 212	android/content/Context:getSystemService	(Ljava/lang/String;)Ljava/lang/Object;
        //     10: checkcast 214	android/telephony/TelephonyManager
        //     13: astore_1
        //     14: new 18	miui/telephony/ExtraTelephonyManager$AsyncFuture
        //     17: dup
        //     18: invokespecial 215	miui/telephony/ExtraTelephonyManager$AsyncFuture:<init>	()V
        //     21: astore_2
        //     22: new 8	miui/telephony/ExtraTelephonyManager$2
        //     25: dup
        //     26: aload_2
        //     27: invokespecial 218	miui/telephony/ExtraTelephonyManager$2:<init>	(Lmiui/telephony/ExtraTelephonyManager$AsyncFuture;)V
        //     30: astore_3
        //     31: aload_0
        //     32: aload_3
        //     33: new 220	android/content/IntentFilter
        //     36: dup
        //     37: ldc 23
        //     39: invokespecial 221	android/content/IntentFilter:<init>	(Ljava/lang/String;)V
        //     42: invokevirtual 225	android/content/Context:registerReceiver	(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
        //     45: pop
        //     46: aload_1
        //     47: invokevirtual 228	android/telephony/TelephonyManager:getDeviceId	()Ljava/lang/String;
        //     50: astore 5
        //     52: aload 5
        //     54: invokestatic 130	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
        //     57: ifne +9 -> 66
        //     60: aload_2
        //     61: aload 5
        //     63: invokevirtual 232	miui/telephony/ExtraTelephonyManager$AsyncFuture:setResult	(Ljava/lang/Object;)V
        //     66: aload_2
        //     67: invokevirtual 233	miui/telephony/ExtraTelephonyManager$AsyncFuture:get	()Ljava/lang/Object;
        //     70: checkcast 124	java/lang/String
        //     73: astore 8
        //     75: aload_0
        //     76: aload_3
        //     77: invokevirtual 237	android/content/Context:unregisterReceiver	(Landroid/content/BroadcastReceiver;)V
        //     80: aload 8
        //     82: areturn
        //     83: astore 7
        //     85: aconst_null
        //     86: astore 8
        //     88: aload_0
        //     89: aload_3
        //     90: invokevirtual 237	android/content/Context:unregisterReceiver	(Landroid/content/BroadcastReceiver;)V
        //     93: goto -13 -> 80
        //     96: astore 6
        //     98: aload_0
        //     99: aload_3
        //     100: invokevirtual 237	android/content/Context:unregisterReceiver	(Landroid/content/BroadcastReceiver;)V
        //     103: aload 6
        //     105: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     66	75	83	java/lang/Exception
        //     66	75	96	finally
    }

    // ERROR //
    private static String waitAndGetIccid(Context paramContext)
    {
        // Byte code:
        //     0: aload_0
        //     1: invokestatic 206	miui/telephony/ExtraTelephonyManager:ensureNotOnMainThread	(Landroid/content/Context;)V
        //     4: aload_0
        //     5: ldc 208
        //     7: invokevirtual 212	android/content/Context:getSystemService	(Ljava/lang/String;)Ljava/lang/Object;
        //     10: checkcast 214	android/telephony/TelephonyManager
        //     13: astore_1
        //     14: new 18	miui/telephony/ExtraTelephonyManager$AsyncFuture
        //     17: dup
        //     18: invokespecial 215	miui/telephony/ExtraTelephonyManager$AsyncFuture:<init>	()V
        //     21: astore_2
        //     22: new 12	miui/telephony/ExtraTelephonyManager$4
        //     25: dup
        //     26: aload_2
        //     27: invokespecial 238	miui/telephony/ExtraTelephonyManager$4:<init>	(Lmiui/telephony/ExtraTelephonyManager$AsyncFuture;)V
        //     30: astore_3
        //     31: aload_0
        //     32: aload_3
        //     33: new 220	android/content/IntentFilter
        //     36: dup
        //     37: ldc 240
        //     39: invokespecial 221	android/content/IntentFilter:<init>	(Ljava/lang/String;)V
        //     42: invokevirtual 225	android/content/Context:registerReceiver	(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
        //     45: pop
        //     46: aload_1
        //     47: invokevirtual 243	android/telephony/TelephonyManager:getSimSerialNumber	()Ljava/lang/String;
        //     50: astore 5
        //     52: aload 5
        //     54: invokestatic 130	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
        //     57: ifne +9 -> 66
        //     60: aload_2
        //     61: aload 5
        //     63: invokevirtual 232	miui/telephony/ExtraTelephonyManager$AsyncFuture:setResult	(Ljava/lang/Object;)V
        //     66: aload_2
        //     67: invokevirtual 233	miui/telephony/ExtraTelephonyManager$AsyncFuture:get	()Ljava/lang/Object;
        //     70: checkcast 124	java/lang/String
        //     73: astore 8
        //     75: aload_0
        //     76: aload_3
        //     77: invokevirtual 237	android/content/Context:unregisterReceiver	(Landroid/content/BroadcastReceiver;)V
        //     80: aload 8
        //     82: areturn
        //     83: astore 7
        //     85: aconst_null
        //     86: astore 8
        //     88: aload_0
        //     89: aload_3
        //     90: invokevirtual 237	android/content/Context:unregisterReceiver	(Landroid/content/BroadcastReceiver;)V
        //     93: goto -13 -> 80
        //     96: astore 6
        //     98: aload_0
        //     99: aload_3
        //     100: invokevirtual 237	android/content/Context:unregisterReceiver	(Landroid/content/BroadcastReceiver;)V
        //     103: aload 6
        //     105: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     66	75	83	java/lang/Exception
        //     66	75	96	finally
    }

    // ERROR //
    private static String waitAndGetImsi(Context paramContext)
    {
        // Byte code:
        //     0: aload_0
        //     1: invokestatic 206	miui/telephony/ExtraTelephonyManager:ensureNotOnMainThread	(Landroid/content/Context;)V
        //     4: aload_0
        //     5: ldc 208
        //     7: invokevirtual 212	android/content/Context:getSystemService	(Ljava/lang/String;)Ljava/lang/Object;
        //     10: checkcast 214	android/telephony/TelephonyManager
        //     13: astore_1
        //     14: new 18	miui/telephony/ExtraTelephonyManager$AsyncFuture
        //     17: dup
        //     18: invokespecial 215	miui/telephony/ExtraTelephonyManager$AsyncFuture:<init>	()V
        //     21: astore_2
        //     22: new 16	miui/telephony/ExtraTelephonyManager$6
        //     25: dup
        //     26: aload_2
        //     27: invokespecial 244	miui/telephony/ExtraTelephonyManager$6:<init>	(Lmiui/telephony/ExtraTelephonyManager$AsyncFuture;)V
        //     30: astore_3
        //     31: aload_0
        //     32: aload_3
        //     33: new 220	android/content/IntentFilter
        //     36: dup
        //     37: ldc 240
        //     39: invokespecial 221	android/content/IntentFilter:<init>	(Ljava/lang/String;)V
        //     42: invokevirtual 225	android/content/Context:registerReceiver	(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
        //     45: pop
        //     46: aload_1
        //     47: invokevirtual 247	android/telephony/TelephonyManager:getSubscriberId	()Ljava/lang/String;
        //     50: astore 5
        //     52: aload 5
        //     54: invokestatic 130	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
        //     57: ifne +9 -> 66
        //     60: aload_2
        //     61: aload 5
        //     63: invokevirtual 232	miui/telephony/ExtraTelephonyManager$AsyncFuture:setResult	(Ljava/lang/Object;)V
        //     66: aload_2
        //     67: invokevirtual 233	miui/telephony/ExtraTelephonyManager$AsyncFuture:get	()Ljava/lang/Object;
        //     70: checkcast 124	java/lang/String
        //     73: astore 8
        //     75: aload_0
        //     76: aload_3
        //     77: invokevirtual 237	android/content/Context:unregisterReceiver	(Landroid/content/BroadcastReceiver;)V
        //     80: aload 8
        //     82: areturn
        //     83: astore 7
        //     85: aconst_null
        //     86: astore 8
        //     88: aload_0
        //     89: aload_3
        //     90: invokevirtual 237	android/content/Context:unregisterReceiver	(Landroid/content/BroadcastReceiver;)V
        //     93: goto -13 -> 80
        //     96: astore 6
        //     98: aload_0
        //     99: aload_3
        //     100: invokevirtual 237	android/content/Context:unregisterReceiver	(Landroid/content/BroadcastReceiver;)V
        //     103: aload 6
        //     105: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     66	75	83	java/lang/Exception
        //     66	75	96	finally
    }

    private static class AsyncFuture<V> extends FutureTask<V>
    {
        public AsyncFuture()
        {
            super();
        }

        public void setResult(V paramV)
        {
            set(paramV);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.telephony.ExtraTelephonyManager
 * JD-Core Version:        0.6.2
 */