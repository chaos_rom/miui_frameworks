package miui.telephony;

import com.android.internal.telephony.gsm.MiuiSpnOverride;
import com.google.android.collect.Maps;
import com.google.android.collect.Sets;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public final class CarrierSelector<T>
{
    private static final Map<CARRIER, String> mCarrier2MccMap = Maps.newHashMap();
    private static final Set<String> mMccHandled;
    private static final Map<STATE, String> mState2MccMap = Maps.newHashMap();
    private final Map<CARRIER, T> mCarrierMap = new HashMap();
    private CARRIER mDefaultCarrier = CARRIER.DEFAULT;
    private final Map<STATE, T> mStateMap = new HashMap();

    static
    {
        mMccHandled = Sets.newHashSet();
        mCarrier2MccMap.put(CARRIER.DEFAULT, "000");
        mCarrier2MccMap.put(CARRIER.CHINA_MOBILE, "460");
        mCarrier2MccMap.put(CARRIER.CHINA_UNICOM, "460");
        mCarrier2MccMap.put(CARRIER.CHINA_TELECOM, "460");
        mState2MccMap.put(STATE.TAIWAN, "466");
    }

    public CarrierSelector()
    {
    }

    public CarrierSelector(CARRIER paramCARRIER)
    {
        this.mDefaultCarrier = paramCARRIER;
    }

    private CARRIER internalSelectCarrier(String paramString, CARRIER paramCARRIER)
    {
        CARRIER localCARRIER = paramCARRIER;
        String str = MiuiSpnOverride.getInstance().getEquivalentOperatorNumeric(paramString);
        if ("46000".equals(str))
            localCARRIER = CARRIER.CHINA_MOBILE;
        while (true)
        {
            return localCARRIER;
            if ("46001".equals(str))
                localCARRIER = CARRIER.CHINA_UNICOM;
            else if ("46003".equals(str))
                localCARRIER = CARRIER.CHINA_TELECOM;
        }
    }

    private STATE internalSelectState(String paramString)
    {
        if ("466".equals(paramString));
        for (STATE localSTATE = STATE.TAIWAN; ; localSTATE = null)
            return localSTATE;
    }

    public void register(CARRIER paramCARRIER, T paramT)
    {
        if (paramCARRIER == null)
            throw new IllegalArgumentException("carrier not nullable");
        String str = (String)mCarrier2MccMap.get(paramCARRIER);
        if (str == null)
            throw new IllegalStateException("mcc for the carrier is unknown, carrier: " + paramCARRIER);
        mMccHandled.add(str);
        this.mCarrierMap.put(paramCARRIER, paramT);
    }

    public void register(STATE paramSTATE, T paramT)
    {
        if (paramSTATE == null)
            throw new IllegalArgumentException("state not nullable");
        String str = (String)mState2MccMap.get(paramSTATE);
        if (str == null)
            throw new IllegalStateException("mcc for the state is unknown, state: " + paramSTATE);
        mMccHandled.add(str);
        this.mStateMap.put(paramSTATE, paramT);
    }

    public CARRIER selectCarrier(String paramString)
    {
        return internalSelectCarrier(paramString, null);
    }

    public T selectValue(String paramString)
    {
        CARRIER localCARRIER = internalSelectCarrier(paramString, this.mDefaultCarrier);
        return this.mCarrierMap.get(localCARRIER);
    }

    public T selectValue(String paramString1, String paramString2)
    {
        return selectValue(paramString1, paramString2, false);
    }

    public T selectValue(String paramString1, String paramString2, boolean paramBoolean)
    {
        if (paramString1 == null)
            throw new IllegalArgumentException("mcc not nullable");
        CARRIER localCARRIER = null;
        if (paramString2 != null)
            localCARRIER = internalSelectCarrier(paramString1 + paramString2, null);
        STATE localSTATE;
        if (localCARRIER == null)
            localSTATE = internalSelectState(paramString1);
        for (Object localObject = this.mStateMap.get(localSTATE); ; localObject = this.mCarrierMap.get(localCARRIER))
        {
            if ((localObject == null) && (paramBoolean) && (!mMccHandled.contains(paramString1)))
                localObject = this.mCarrierMap.get(this.mDefaultCarrier);
            return localObject;
        }
    }

    public T selectValue(String paramString, boolean paramBoolean)
    {
        if (paramBoolean);
        for (CARRIER localCARRIER1 = this.mDefaultCarrier; ; localCARRIER1 = null)
        {
            CARRIER localCARRIER2 = internalSelectCarrier(paramString, localCARRIER1);
            return this.mCarrierMap.get(localCARRIER2);
        }
    }

    public static enum STATE
    {
        static
        {
            STATE[] arrayOfSTATE = new STATE[1];
            arrayOfSTATE[0] = TAIWAN;
        }
    }

    public static enum CARRIER
    {
        static
        {
            CHINA_TELECOM = new CARRIER("CHINA_TELECOM", 2);
            DEFAULT = new CARRIER("DEFAULT", 3);
            CARRIER[] arrayOfCARRIER = new CARRIER[4];
            arrayOfCARRIER[0] = CHINA_MOBILE;
            arrayOfCARRIER[1] = CHINA_UNICOM;
            arrayOfCARRIER[2] = CHINA_TELECOM;
            arrayOfCARRIER[3] = DEFAULT;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.telephony.CarrierSelector
 * JD-Core Version:        0.6.2
 */