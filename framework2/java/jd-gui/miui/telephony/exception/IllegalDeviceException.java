package miui.telephony.exception;

public class IllegalDeviceException extends Exception
{
    public IllegalDeviceException(String paramString)
    {
        super(paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.telephony.exception.IllegalDeviceException
 * JD-Core Version:        0.6.2
 */