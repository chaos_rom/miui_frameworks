package miui.telephony.phonenumber;

import android.text.TextUtils;

public class Prefix
{
    public static final String EMPTY = "";
    public static final String PREFIX_10193 = "10193";
    public static final String PREFIX_10650 = "10650";
    public static final String PREFIX_10651 = "10651";
    public static final String PREFIX_10656 = "10656";
    public static final String PREFIX_10657 = "10657";
    public static final String PREFIX_10659 = "10659";
    public static final String PREFIX_12520 = "12520";
    public static final String PREFIX_12593 = "12593";
    public static final String PREFIX_17901 = "17901";
    public static final String PREFIX_17909 = "17909";
    public static final String PREFIX_17911 = "17911";
    public static final String PREFIX_17951 = "17951";
    public static final String PREFIX_17961 = "17961";
    public static final String PREFIX_600 = "600";
    public static final String[] SMS_PREFIXES = arrayOfString;

    static
    {
        String[] arrayOfString = new String[6];
        arrayOfString[0] = "12520";
        arrayOfString[1] = "10657";
        arrayOfString[2] = "10656";
        arrayOfString[3] = "10650";
        arrayOfString[4] = "10651";
        arrayOfString[5] = "10659";
    }

    public static boolean isSmsPrefix(String paramString)
    {
        int j;
        if (!TextUtils.isEmpty(paramString))
        {
            String[] arrayOfString = SMS_PREFIXES;
            int i = arrayOfString.length;
            j = 0;
            if (j < i)
                if (!arrayOfString[j].equals(paramString));
        }
        for (boolean bool = true; ; bool = false)
        {
            return bool;
            j++;
            break;
        }
    }

    public static String parse(StringBuffer paramStringBuffer, int paramInt1, int paramInt2)
    {
        String str;
        if (!CountryCode.isChinaEnvironment())
            str = "";
        while (true)
        {
            return str;
            if (paramInt2 <= 0)
            {
                str = "";
            }
            else
            {
                switch (paramStringBuffer.charAt(paramInt1))
                {
                default:
                case '1':
                case '6':
                }
                label225: 
                do
                {
                    do
                    {
                        do
                        {
                            do
                            {
                                do
                                {
                                    do
                                    {
                                        while (true)
                                        {
                                            str = "";
                                            break;
                                            if (paramInt2 > 4)
                                                switch (paramStringBuffer.charAt(paramInt1 + 1))
                                                {
                                                default:
                                                    break;
                                                case '0':
                                                    if ((paramStringBuffer.charAt(paramInt1 + 2) != '6') || (paramStringBuffer.charAt(paramInt1 + 3) != '5'))
                                                        break label225;
                                                    switch (paramStringBuffer.charAt(paramInt1 + 4))
                                                    {
                                                    case '2':
                                                    case '3':
                                                    case '4':
                                                    case '5':
                                                    case '8':
                                                    default:
                                                    case '0':
                                                    case '1':
                                                    case '6':
                                                    case '7':
                                                    case '9':
                                                    }
                                                    break;
                                                case '2':
                                                case '7':
                                                }
                                        }
                                        str = "10650";
                                        break;
                                        str = "10651";
                                        break;
                                        str = "10656";
                                        break;
                                        str = "10657";
                                        break;
                                        str = "10659";
                                        break;
                                    }
                                    while ((paramStringBuffer.charAt(paramInt1 + 2) != '1') || (paramStringBuffer.charAt(paramInt1 + 3) != '9') || (paramStringBuffer.charAt(paramInt1 + 4) != '3'));
                                    str = "10193";
                                    break;
                                }
                                while (paramStringBuffer.charAt(paramInt1 + 2) != '5');
                                if ((paramStringBuffer.charAt(paramInt1 + 3) == '9') && (paramStringBuffer.charAt(paramInt1 + 4) == '3'))
                                {
                                    str = "12593";
                                    break;
                                }
                            }
                            while ((paramStringBuffer.charAt(paramInt1 + 3) != '2') || (paramStringBuffer.charAt(paramInt1 + 4) != '0'));
                            str = "12520";
                            break;
                        }
                        while (paramStringBuffer.charAt(paramInt1 + 2) != '9');
                        if ((paramStringBuffer.charAt(paramInt1 + 3) == '0') && (paramStringBuffer.charAt(paramInt1 + 4) == '1'))
                        {
                            str = "17901";
                            break;
                        }
                        if ((paramStringBuffer.charAt(paramInt1 + 3) == '0') && (paramStringBuffer.charAt(paramInt1 + 4) == '9'))
                        {
                            str = "17909";
                            break;
                        }
                        if ((paramStringBuffer.charAt(paramInt1 + 3) == '1') && (paramStringBuffer.charAt(paramInt1 + 4) == '1'))
                        {
                            str = "17911";
                            break;
                        }
                        if ((paramStringBuffer.charAt(paramInt1 + 3) == '5') && (paramStringBuffer.charAt(paramInt1 + 4) == '1'))
                        {
                            str = "17951";
                            break;
                        }
                    }
                    while ((paramStringBuffer.charAt(paramInt1 + 3) != '6') || (paramStringBuffer.charAt(paramInt1 + 4) != '1'));
                    str = "17961";
                    break;
                }
                while ((paramInt2 <= 13) || (paramStringBuffer.charAt(paramInt1 + 1) != '0') || (paramStringBuffer.charAt(paramInt1 + 2) != '0'));
                str = "600";
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.telephony.phonenumber.Prefix
 * JD-Core Version:        0.6.2
 */