package miui.telephony.phonenumber;

import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.provider.Settings.System;
import android.text.TextUtils;
import android.util.Log;
import com.android.i18n.phonenumbers.PhoneNumberUtil;
import com.android.i18n.phonenumbers.geocoding.PhoneNumberOfflineGeocoder;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import miui.provider.Telocation;
import miui.util.DensyIndexFile.Reader;

public class ChineseTelocation
{
    private static final String DATA_PATH = "/etc/telocation.idf";
    private static final String EMPTY = "";
    private static final String TAG = "ChineseTelocation";
    private static final int UNIQUE_ID_NONE;
    private static ChineseTelocation sInstance = new ChineseTelocation();
    private boolean mAllowTelocation;
    private Context mContext;
    private ContentObserver mCustomLocationObserver;
    private DensyIndexFile.Reader mDensyIndexFileReader;
    private HashMap<String, String> mGroupCustomLocations = new HashMap();
    private HashMap<Integer, String> mNormalCustomLocations = new HashMap();
    private ContentObserver mSettingObserver;

    private ChineseTelocation()
    {
        try
        {
            this.mDensyIndexFileReader = new DensyIndexFile.Reader("/etc/telocation.idf");
            return;
        }
        catch (IOException localIOException)
        {
            while (true)
                Log.w("ChineseTelocation", "Can't read /etc/telocation.idf, NO mobile telocation supported!");
        }
    }

    private String findCustomLocation(int paramInt1, CharSequence paramCharSequence, int paramInt2, int paramInt3)
    {
        String str1 = null;
        if (paramInt1 > 0)
            str1 = (String)this.mNormalCustomLocations.get(Integer.valueOf(paramInt1));
        if ((str1 == null) && (this.mGroupCustomLocations.size() > 0))
        {
            Iterator localIterator = this.mGroupCustomLocations.keySet().iterator();
            while (localIterator.hasNext())
            {
                String str2 = (String)localIterator.next();
                if (paramInt3 == str2.length())
                {
                    int i = 1;
                    int j = 0;
                    int k = str2.length();
                    while (true)
                    {
                        if (j < k)
                        {
                            if ((str2.charAt(j) != '#') && (str2.charAt(j) != paramCharSequence.charAt(paramInt2 + j)))
                                i = 0;
                        }
                        else
                        {
                            if (i == 0)
                                break;
                            str1 = (String)this.mGroupCustomLocations.get(str2);
                            break;
                        }
                        j++;
                    }
                }
            }
        }
        if (str1 == null)
            str1 = "";
        return str1;
    }

    public static ChineseTelocation getInstance()
    {
        return sInstance;
    }

    private void initObserver(Context paramContext)
    {
        this.mContext = paramContext.getApplicationContext();
        if (this.mContext == null)
            this.mContext = paramContext;
        updateTelocationSetting();
        updateCustomLocation();
        Handler localHandler = new Handler(this.mContext.getMainLooper());
        this.mSettingObserver = new ContentObserver(localHandler)
        {
            public void onChange(boolean paramAnonymousBoolean)
            {
                super.onChange(paramAnonymousBoolean);
                Log.d("ChineseTelocation", "telocation setting changed, reloading ...");
                ChineseTelocation.this.updateTelocationSetting();
            }
        };
        this.mContext.getContentResolver().registerContentObserver(Settings.System.getUriFor("enable_telocation"), false, this.mSettingObserver);
        this.mCustomLocationObserver = new ContentObserver(localHandler)
        {
            public void onChange(boolean paramAnonymousBoolean)
            {
                super.onChange(paramAnonymousBoolean);
                Log.d("ChineseTelocation", "custom location changed, reloading ...");
                ChineseTelocation.this.updateCustomLocation();
            }
        };
        this.mContext.getContentResolver().registerContentObserver(Telocation.CONTENT_CUSTOM_LOCATION_URI, true, this.mCustomLocationObserver);
    }

    private void updateCustomLocation()
    {
        this.mNormalCustomLocations.clear();
        this.mGroupCustomLocations.clear();
        Cursor localCursor;
        if (this.mAllowTelocation)
            localCursor = null;
        while (true)
        {
            String str1;
            String str2;
            int j;
            try
            {
                ContentResolver localContentResolver = this.mContext.getContentResolver();
                Uri localUri = Telocation.CONTENT_CUSTOM_LOCATION_URI;
                String[] arrayOfString = new String[4];
                arrayOfString[0] = "_id";
                arrayOfString[1] = "number";
                arrayOfString[2] = "location";
                arrayOfString[3] = "type";
                localCursor = localContentResolver.query(localUri, arrayOfString, null, null, null);
                if (localCursor == null)
                    break label247;
                if (!localCursor.moveToNext())
                    break label247;
                int i = localCursor.getInt(3);
                str1 = localCursor.getString(1);
                str2 = localCursor.getString(2);
                switch (i)
                {
                case 0:
                case 1:
                    j = 0;
                    if (str1.startsWith("+86"))
                    {
                        j = 3;
                        int k = getUniqId(str1, j, str1.length(), true);
                        if (k == 0)
                            continue;
                        this.mNormalCustomLocations.put(Integer.valueOf(k), str2);
                        continue;
                    }
                    break;
                case 2:
                }
            }
            finally
            {
                if (localCursor != null)
                    localCursor.close();
            }
            if (str1.startsWith("0086"))
            {
                j = 4;
                continue;
                this.mGroupCustomLocations.put(str1, str2);
                continue;
                label247: if (localCursor != null)
                    localCursor.close();
                return;
            }
        }
    }

    private void updateTelocationSetting()
    {
        int i = 1;
        if (Settings.System.getInt(this.mContext.getContentResolver(), "enable_telocation", i) != 0);
        while (true)
        {
            this.mAllowTelocation = i;
            updateCustomLocation();
            return;
            i = 0;
        }
    }

    protected void finalize()
        throws Throwable
    {
        if (this.mDensyIndexFileReader != null);
        try
        {
            this.mDensyIndexFileReader.close();
            label14: if (this.mSettingObserver != null)
                this.mContext.getContentResolver().unregisterContentObserver(this.mSettingObserver);
            if (this.mCustomLocationObserver != null)
                this.mContext.getContentResolver().unregisterContentObserver(this.mCustomLocationObserver);
            super.finalize();
            return;
        }
        catch (IOException localIOException)
        {
            break label14;
        }
    }

    public String getAreaCode(Context paramContext, CharSequence paramCharSequence, int paramInt1, int paramInt2)
    {
        int i = getUniqId(paramCharSequence, paramInt1, paramInt2, true);
        return (String)this.mDensyIndexFileReader.get(0, i, 1);
    }

    public String getExternalLocation(Context paramContext, String paramString, CharSequence paramCharSequence, Locale paramLocale)
    {
        if (this.mContext == null)
            initObserver(paramContext);
        Object localObject;
        if (!this.mAllowTelocation)
            localObject = null;
        while (true)
        {
            return localObject;
            if (TextUtils.isEmpty(paramString))
            {
                paramString = CountryCode.getUserDefinedCountryCode();
                if (TextUtils.isEmpty(paramString))
                    paramString = CountryCode.getIccCountryCode();
            }
            try
            {
                if (!TextUtils.isEmpty(paramString))
                {
                    String str = PhoneNumberOfflineGeocoder.getInstance().getDescriptionForNumber(PhoneNumberUtil.getInstance().parse(paramCharSequence.toString(), PhoneNumberUtil.getInstance().getRegionCodeForCountryCode(Integer.parseInt(paramString))), paramLocale);
                    localObject = str;
                }
            }
            catch (Exception localException)
            {
                localObject = "";
            }
        }
    }

    public String getLocation(Context paramContext, CharSequence paramCharSequence, int paramInt1, int paramInt2, boolean paramBoolean)
    {
        if (this.mContext == null)
            initObserver(paramContext);
        if (!this.mAllowTelocation);
        int i;
        for (String str = null; ; str = (String)this.mDensyIndexFileReader.get(0, i, 0))
            do
            {
                return str;
                i = -1;
                if (paramBoolean)
                    i = getUniqId(paramCharSequence, paramInt1, paramInt2, true);
                str = findCustomLocation(i, paramCharSequence, paramInt1, paramInt2);
            }
            while ((!TextUtils.isEmpty(str)) || (i <= 0));
    }

    int getUniqId(CharSequence paramCharSequence, int paramInt1, int paramInt2, boolean paramBoolean)
    {
        if ((paramInt2 > 0) && (paramCharSequence.charAt(paramInt1) == '0'))
        {
            paramInt1++;
            paramInt2--;
        }
        int i;
        if (paramInt2 <= 1)
            i = 0;
        while (true)
        {
            return i;
            switch (paramCharSequence.charAt(paramInt1))
            {
            default:
                if (paramInt2 > 2)
                    i = 10 * (10 * ('\0*0' + paramCharSequence.charAt(paramInt1)) + ('\0*0' + paramCharSequence.charAt(paramInt1 + 1))) + ('\0*0' + paramCharSequence.charAt(paramInt1 + 2));
                break;
            case '1':
                if (paramCharSequence.charAt(paramInt1 + 1) == '0')
                {
                    i = 10;
                }
                else if ((paramBoolean) && (paramInt2 > 6))
                {
                    i = 1000000 + 100000 * ('\0*0' + paramCharSequence.charAt(paramInt1 + 1)) + 10000 * ('\0*0' + paramCharSequence.charAt(paramInt1 + 2)) + 1000 * ('\0*0' + paramCharSequence.charAt(paramInt1 + 3)) + 100 * ('\0*0' + paramCharSequence.charAt(paramInt1 + 4)) + 10 * ('\0*0' + paramCharSequence.charAt(paramInt1 + 5)) + ('\0*0' + paramCharSequence.charAt(paramInt1 + 6));
                    if ((i != 1380013) || (paramInt2 <= 10) || (paramCharSequence.charAt(paramInt1 + 7) != '8') || (paramCharSequence.charAt(paramInt1 + 8) != '0') || (paramCharSequence.charAt(paramInt1 + 9) != '0') || (paramCharSequence.charAt(paramInt1 + 10) != '0'))
                        continue;
                    i = 0;
                }
                break;
            case '2':
                i = 20 + ('\0*0' + paramCharSequence.charAt(paramInt1 + 1));
                break;
            case '0':
                i = 0;
            }
        }
    }

    public String parseAreaCode(CharSequence paramCharSequence, int paramInt1, int paramInt2)
    {
        int i = getUniqId(paramCharSequence, paramInt1, paramInt2, false);
        return (String)this.mDensyIndexFileReader.get(0, i, 1);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.telephony.phonenumber.ChineseTelocation
 * JD-Core Version:        0.6.2
 */