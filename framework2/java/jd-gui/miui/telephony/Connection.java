package miui.telephony;

import android.content.res.Resources;

public class Connection
{
    public static int PRESENTATION_ALLOWED = 1;
    public static int PRESENTATION_PAYPHONE = 4;
    public static int PRESENTATION_RESTRICTED = 2;
    public static int PRESENTATION_UNKNOWN = 3;

    public static String getPresentationString(int paramInt)
    {
        String str;
        if (paramInt == PRESENTATION_RESTRICTED)
            str = Resources.getSystem().getString(101449789);
        while (true)
        {
            return str;
            if (paramInt == PRESENTATION_PAYPHONE)
                str = Resources.getSystem().getString(101449790);
            else
                str = Resources.getSystem().getString(101449788);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.telephony.Connection
 * JD-Core Version:        0.6.2
 */