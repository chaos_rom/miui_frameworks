package miui.micloud;

import android.text.TextUtils;

public class MicloudPushType
{
    private final String mContentAuthority;
    private final String mPackageName;
    private final String mPushName;
    private final String mPushType;
    private long mWatermark;
    private final String mWatermarkType;

    public MicloudPushType(String paramString1, String paramString2, String paramString3, String paramString4)
    {
        this(paramString1, paramString2, paramString3, paramString4, "p", 0L);
    }

    public MicloudPushType(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, long paramLong)
    {
        if ((TextUtils.isEmpty(paramString1)) || (TextUtils.isEmpty(paramString2)) || (TextUtils.isEmpty(paramString3)) || (TextUtils.isEmpty(paramString4)))
            throw new IllegalArgumentException("the packageName, contentAuthority, pushType and pushName must not be empty: ");
        this.mPackageName = paramString1;
        this.mContentAuthority = paramString2;
        this.mPushType = paramString3;
        this.mPushName = paramString4;
        this.mWatermarkType = paramString5;
        this.mWatermark = paramLong;
    }

    public boolean equals(Object paramObject)
    {
        boolean bool = true;
        if (paramObject == this);
        while (true)
        {
            return bool;
            if (!(paramObject instanceof MicloudPushType))
            {
                bool = false;
            }
            else
            {
                MicloudPushType localMicloudPushType = (MicloudPushType)paramObject;
                if ((!TextUtils.equals(this.mPackageName, localMicloudPushType.mPackageName)) || (!TextUtils.equals(this.mContentAuthority, localMicloudPushType.mContentAuthority)) || (!TextUtils.equals(this.mPushType, localMicloudPushType.mPushType)) || (!TextUtils.equals(this.mPushName, localMicloudPushType.mPushName)) || (!TextUtils.equals(this.mWatermarkType, localMicloudPushType.mWatermarkType)) || (this.mWatermark != localMicloudPushType.mWatermark))
                    bool = false;
            }
        }
    }

    public String getContentAuthority()
    {
        return this.mContentAuthority;
    }

    public String getPackageName()
    {
        return this.mPackageName;
    }

    public String getPushName()
    {
        return this.mPushName;
    }

    public String getPushType()
    {
        return this.mPushType;
    }

    public long getWatermark()
    {
        return this.mWatermark;
    }

    public String getWatermarkType()
    {
        return this.mWatermarkType;
    }

    public int hashCode()
    {
        return 31 * (31 * (31 * (31 * (31 * (527 + this.mPackageName.hashCode()) + this.mContentAuthority.hashCode()) + this.mPushType.hashCode()) + this.mPushName.hashCode()) + this.mWatermarkType.hashCode()) + (int)(this.mWatermark ^ this.mWatermark >>> 32);
    }

    public String toString()
    {
        return "MicloudPushType {packageName=" + this.mPackageName + ", contentAuthority=" + this.mContentAuthority + ", pushType=" + this.mPushType + ", pushName=" + this.mPushName + ", watermarkType=" + this.mWatermarkType + ", watermark=" + this.mWatermark + "}";
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.micloud.MicloudPushType
 * JD-Core Version:        0.6.2
 */