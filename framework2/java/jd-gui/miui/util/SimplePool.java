package miui.util;

import java.lang.reflect.Array;

public class SimplePool
{
    public static <T> PoolInstance<T> newInsance(Manager<T> paramManager, int paramInt)
    {
        return new PoolInstance(paramManager, paramInt);
    }

    public static class PoolInstance<T>
    {
        private T[] mElements;
        private int mIndex;
        private SimplePool.Manager<T> mManager;

        public PoolInstance(SimplePool.Manager<T> paramManager, int paramInt)
        {
            this.mManager = paramManager;
            this.mElements = ((Object[])Array.newInstance(Object.class, paramInt));
            this.mIndex = -1;
        }

        public T acquire()
        {
            Object localObject1 = null;
            try
            {
                if (this.mIndex >= 0)
                {
                    localObject1 = this.mElements[this.mIndex];
                    Object[] arrayOfObject = this.mElements;
                    int i = this.mIndex;
                    this.mIndex = (i - 1);
                    arrayOfObject[i] = null;
                }
                if (localObject1 == null)
                    localObject1 = this.mManager.createInstance();
                this.mManager.onAcquire(localObject1);
                return localObject1;
            }
            finally
            {
            }
        }

        public void release(T paramT)
        {
            this.mManager.onRelease(paramT);
            try
            {
                if (1 + this.mIndex < this.mElements.length)
                {
                    Object[] arrayOfObject = this.mElements;
                    int i = 1 + this.mIndex;
                    this.mIndex = i;
                    arrayOfObject[i] = paramT;
                }
                return;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }
    }

    public static abstract class Manager<T>
    {
        public abstract T createInstance();

        public void onAcquire(T paramT)
        {
        }

        public void onRelease(T paramT)
        {
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.util.SimplePool
 * JD-Core Version:        0.6.2
 */