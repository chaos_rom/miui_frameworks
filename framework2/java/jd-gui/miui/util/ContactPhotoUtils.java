package miui.util;

import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.provider.Settings.System;
import android.text.TextUtils;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ContactPhotoUtils
{
    private static final String WORD_PHOTO = "word_photo";
    private static Pattern sAsiaLangPattern = Pattern.compile("[一-龥]");
    private static Bitmap sBitmap;
    private static Canvas sCanvas;
    private static final int sPhotoSize = 96;
    private static Paint sPt = null;
    private static Rect sRect = new Rect(0, 0, 96, 96);
    private static String[] suffix;

    static
    {
        sBitmap = null;
        sCanvas = null;
        sBitmap = Bitmap.createBitmap(96, 96, Bitmap.Config.ARGB_8888);
        sCanvas = new Canvas(sBitmap);
        sPt = new Paint();
        sPt.setFilterBitmap(true);
        sPt.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        String[] arrayOfString = new String[40];
        arrayOfString[0] = "老师";
        arrayOfString[1] = "先生";
        arrayOfString[2] = "老板";
        arrayOfString[3] = "仔";
        arrayOfString[4] = "手机";
        arrayOfString[5] = "叔";
        arrayOfString[6] = "阿姨";
        arrayOfString[7] = "宅";
        arrayOfString[8] = "伯";
        arrayOfString[9] = "伯母";
        arrayOfString[10] = "伯父";
        arrayOfString[11] = "哥";
        arrayOfString[12] = "姐";
        arrayOfString[13] = "弟";
        arrayOfString[14] = "妹";
        arrayOfString[15] = "舅";
        arrayOfString[16] = "姑";
        arrayOfString[17] = "父";
        arrayOfString[18] = "主任";
        arrayOfString[19] = "经理";
        arrayOfString[20] = "工作";
        arrayOfString[21] = "同事";
        arrayOfString[22] = "律师";
        arrayOfString[23] = "司机";
        arrayOfString[24] = "师傅";
        arrayOfString[25] = "师父";
        arrayOfString[26] = "爷";
        arrayOfString[27] = "奶";
        arrayOfString[28] = "中介";
        arrayOfString[29] = "董";
        arrayOfString[30] = "总";
        arrayOfString[31] = "太太";
        arrayOfString[32] = "保姆";
        arrayOfString[33] = "某";
        arrayOfString[34] = "秘书";
        arrayOfString[35] = "处长";
        arrayOfString[36] = "局长";
        arrayOfString[37] = "班长";
        arrayOfString[38] = "兄";
        arrayOfString[39] = "助理";
        suffix = arrayOfString;
    }

    public static Bitmap createNameBitmap(Context paramContext, String paramString, int paramInt1, int paramInt2)
    {
        Bitmap localBitmap;
        if (TextUtils.isEmpty(paramString))
            localBitmap = null;
        while (true)
        {
            return localBitmap;
            if (paramString.length() > 1);
            for (String str1 = getWordFromName(paramString.trim()); ; str1 = paramString)
            {
                if (str1 != null)
                    break label47;
                localBitmap = null;
                break;
            }
            label47: String str2 = str1.trim();
            if (TextUtils.isEmpty(str2))
            {
                localBitmap = null;
            }
            else
            {
                localBitmap = Bitmap.createBitmap(96, 96, Bitmap.Config.ARGB_8888);
                Canvas localCanvas = new Canvas(localBitmap);
                Drawable localDrawable = paramContext.getResources().getDrawable(paramInt1);
                localDrawable.setBounds(sRect);
                localDrawable.draw(localCanvas);
                Paint localPaint = new Paint();
                localPaint.setAntiAlias(true);
                localPaint.setColor(paramInt2);
                if (isChinese(paramString))
                {
                    localPaint.setTextSize(67.199997F);
                    localCanvas.drawText(str2, 14.0F, 72.0F, localPaint);
                }
                else
                {
                    String[] arrayOfString = str2.split(" |\\.|-|,|\\(|\\)|（|）|—");
                    if (arrayOfString == null)
                    {
                        localBitmap.recycle();
                        localBitmap = null;
                    }
                    else
                    {
                        String str3 = null;
                        String str4 = null;
                        int i = 0;
                        if (i < arrayOfString.length)
                        {
                            if (!TextUtils.isEmpty(arrayOfString[i].trim()))
                            {
                                if (str3 != null)
                                    break label244;
                                str3 = arrayOfString[i].trim();
                            }
                            while (true)
                            {
                                i++;
                                break;
                                label244: if (str4 != null)
                                    break label262;
                                str4 = arrayOfString[i].trim();
                            }
                        }
                        label262: if (str3 == null)
                        {
                            localBitmap.recycle();
                            localBitmap = null;
                        }
                        else if (str4 == null)
                        {
                            localPaint.setTextSize(67.199997F);
                            localCanvas.drawText(str3, 14.0F, 72.0F, localPaint);
                        }
                        else
                        {
                            localPaint.setTextSize(28.800001F);
                            localCanvas.drawText(str3, 14.0F, 43.0F, localPaint);
                            localCanvas.drawText(str4, 28.0F, 81.0F, localPaint);
                        }
                    }
                }
            }
        }
    }

    public static Bitmap createNameBitmap(Context paramContext, String paramString, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
    {
        Bitmap localBitmap = createNameBitmap(paramContext, paramString, paramInt1, paramInt2);
        return createPhoto(paramContext.getResources(), localBitmap, paramInt3, paramInt4, paramInt5);
    }

    /** @deprecated */
    public static Bitmap createPhoto(Resources paramResources, Bitmap paramBitmap, int paramInt1, int paramInt2, int paramInt3)
    {
        Bitmap localBitmap = null;
        if (paramBitmap == null);
        while (true)
        {
            return localBitmap;
            try
            {
                localBitmap = Bitmap.createBitmap(96, 96, Bitmap.Config.ARGB_8888);
                Drawable localDrawable1 = paramResources.getDrawable(paramInt1);
                localDrawable1.setBounds(sRect);
                Drawable localDrawable2 = paramResources.getDrawable(paramInt3);
                localDrawable2.setBounds(sRect);
                Canvas localCanvas = new Canvas(localBitmap);
                localDrawable1.draw(localCanvas);
                cutBitmap(paramResources, paramBitmap, paramInt2);
                localCanvas.drawBitmap(sBitmap, sRect, sRect, null);
                localDrawable2.draw(localCanvas);
                paramBitmap.recycle();
            }
            finally
            {
            }
        }
    }

    private static void cutBitmap(Resources paramResources, Bitmap paramBitmap, int paramInt)
    {
        sCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
        Drawable localDrawable = paramResources.getDrawable(paramInt);
        localDrawable.setBounds(sRect);
        localDrawable.draw(sCanvas);
        int i = paramBitmap.getWidth();
        int j = paramBitmap.getHeight();
        int k = 0;
        int m = 0;
        if (i > j)
        {
            k = (i - j) / 2;
            i = j;
        }
        while (true)
        {
            sCanvas.drawBitmap(paramBitmap, new Rect(k, m, k + i, m + j), sRect, sPt);
            return;
            if (i < j)
            {
                m = (j - i) / 2;
                j = i;
            }
        }
    }

    private static String getWordFromName(String paramString)
    {
        String str;
        if (isChinese(paramString))
        {
            str = removeSuffix(paramString);
            if (!TextUtils.isEmpty(str))
                break label23;
        }
        label23: int i;
        for (paramString = null; ; paramString = str.substring(i - 1, i))
        {
            return paramString;
            i = str.length();
        }
    }

    private static boolean isChinese(String paramString)
    {
        return sAsiaLangPattern.matcher(paramString).find();
    }

    private static String removeSuffix(String paramString)
    {
        String str1 = paramString;
        String str2;
        if (TextUtils.isEmpty(str1))
        {
            str2 = null;
            return str2;
        }
        label15: int i = 0;
        label230: for (int j = 0; ; j++)
        {
            if (j < suffix.length)
            {
                if (!str1.endsWith(suffix[j]))
                    break label112;
                i = 1;
            }
            for (str1 = str1.substring(0, str1.length() - suffix[j].length()); ; str1 = str1.substring(0, -1 + str1.length()))
            {
                label112: int k;
                do
                {
                    if (!TextUtils.isEmpty(str1))
                        break label230;
                    if ((i != 0) && (!TextUtils.isEmpty(str1)))
                        break label15;
                    if (str1 != null)
                        str1 = str1.trim();
                    if (TextUtils.isEmpty(str1))
                        str1 = paramString.substring(-1 + paramString.length());
                    str2 = str1;
                    break;
                    k = str1.charAt(-1 + str1.length());
                }
                while (((k < 65) || (k > 90)) && ((k < 97) || (k > 122)) && (k != 44) && (k != 46) && (k != 45) && (k != 40) && (k != 41) && (k != 65288) && (k != 65289) && (k != 8212));
                i = 1;
            }
        }
    }

    public static void setUseWordPhoto(Context paramContext, boolean paramBoolean)
    {
        ContentResolver localContentResolver = paramContext.getContentResolver();
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            Settings.System.putInt(localContentResolver, "word_photo", i);
            return;
        }
    }

    public static boolean useWordPhoto(Context paramContext)
    {
        int i = 1;
        if (Settings.System.getInt(paramContext.getContentResolver(), "word_photo", i) == i);
        while (true)
        {
            return i;
            int j = 0;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.util.ContactPhotoUtils
 * JD-Core Version:        0.6.2
 */