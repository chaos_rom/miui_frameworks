package miui.util;

import android.content.Context;
import android.content.res.Resources;
import android.text.format.DateFormat;
import android.text.format.DateUtils;
import android.text.format.Time;
import java.nio.CharBuffer;

public class MiuiDateUtils
{
    private static final int CHAR_BUFFER_CAPACITY = 64;
    private static final SimplePool.PoolInstance<CharBuffer> mCharBufferPool = SimplePool.newInsance(new SimplePool.Manager()
    {
        public CharBuffer createInstance()
        {
            return CharBuffer.allocate(64);
        }

        public void onRelease(CharBuffer paramAnonymousCharBuffer)
        {
            paramAnonymousCharBuffer.clear();
        }
    }
    , 2);
    private static final SimplePool.PoolInstance<Time> mTimePool = SimplePool.newInsance(new SimplePool.Manager()
    {
        public Time createInstance()
        {
            return new Time();
        }
    }
    , 4);
    private static int[] sAmPmIndex;
    private static String[] sMonth;

    @Deprecated
    public static String formatDateTime(Context paramContext, long paramLong, int paramInt)
    {
        CharBuffer localCharBuffer = (CharBuffer)mCharBufferPool.acquire();
        formatDateTime(paramContext, paramLong, paramInt, localCharBuffer);
        String str = new String(localCharBuffer.array(), 0, localCharBuffer.position());
        mCharBufferPool.release(localCharBuffer);
        return str;
    }

    public static void formatDateTime(Context paramContext, long paramLong, int paramInt, CharBuffer paramCharBuffer)
    {
        Time localTime = (Time)mTimePool.acquire();
        localTime.set(paramLong);
        getFormatTime(paramContext, localTime, paramInt, DateFormat.is24HourFormat(paramContext), paramCharBuffer);
        mTimePool.release(localTime);
    }

    private static String getDetailedAmPm(int paramInt)
    {
        if (sAmPmIndex == null)
        {
            sAmPmIndex = new int[24];
            int[] arrayOfInt = Resources.getSystem().getIntArray(101056522);
            int i = 23;
            int j = -1 + arrayOfInt.length;
            while ((i >= 0) && (j >= 0))
            {
                if (i < arrayOfInt[j])
                    j--;
                sAmPmIndex[i] = j;
                i--;
            }
        }
        return Resources.getSystem().getStringArray(101056523)[sAmPmIndex[paramInt]];
    }

    private static void getFormatTime(Context paramContext, Time paramTime, int paramInt, boolean paramBoolean, CharBuffer paramCharBuffer)
    {
        long l1 = paramTime.toMillis(true);
        int i;
        if ((paramInt & 0x1) != 0)
        {
            i = 1;
            if (!paramBoolean)
                break label42;
            paramCharBuffer.append(DateUtils.formatDateRange(paramContext, l1, l1, paramInt));
        }
        while (true)
        {
            return;
            i = 0;
            break;
            label42: if (i != 0)
            {
                if ((paramInt ^ 0x1) != 0)
                {
                    paramCharBuffer.append(DateUtils.formatDateRange(paramContext, l1, l1, paramInt ^ 0x1));
                    paramCharBuffer.append(" ");
                }
                String str1 = getDetailedAmPm(paramTime.hour);
                if (paramTime.hour > 12)
                    paramTime.hour = (-12 + paramTime.hour);
                long l2 = paramTime.toMillis(true);
                String str2 = DateUtils.formatDateRange(paramContext, l2, l2, 129);
                Resources localResources = Resources.getSystem();
                Object[] arrayOfObject = new Object[2];
                arrayOfObject[0] = str2;
                arrayOfObject[1] = str1;
                paramCharBuffer.append(localResources.getString(101450231, arrayOfObject));
            }
            else
            {
                paramCharBuffer.append(DateUtils.formatDateRange(paramContext, l1, l1, paramInt));
            }
        }
    }

    public static void getRelativeTimeSpanString(Context paramContext, long paramLong, boolean paramBoolean, CharBuffer paramCharBuffer)
    {
        long l1 = System.currentTimeMillis();
        Resources localResources = Resources.getSystem();
        String[] arrayOfString = new String[12];
        arrayOfString[0] = localResources.getString(101450174);
        arrayOfString[1] = localResources.getString(101450175);
        arrayOfString[2] = localResources.getString(101450176);
        arrayOfString[3] = localResources.getString(101450177);
        arrayOfString[4] = localResources.getString(101450178);
        arrayOfString[5] = localResources.getString(101450179);
        arrayOfString[6] = localResources.getString(101450180);
        arrayOfString[7] = localResources.getString(101450181);
        arrayOfString[8] = localResources.getString(101450182);
        arrayOfString[9] = localResources.getString(101450183);
        arrayOfString[10] = localResources.getString(101450184);
        arrayOfString[11] = localResources.getString(101450185);
        sMonth = arrayOfString;
        int i;
        Time localTime1;
        Time localTime2;
        boolean bool;
        int j;
        long l3;
        if (l1 >= paramLong)
        {
            i = 1;
            long l2 = Math.abs(l1 - paramLong);
            localTime1 = (Time)mTimePool.acquire();
            localTime2 = (Time)mTimePool.acquire();
            localTime1.set(l1);
            localTime2.set(paramLong);
            bool = DateFormat.is24HourFormat(paramContext);
            j = -1;
            l3 = l2 / 60000L;
            if (l3 > 60L)
                break label423;
            if (i == 0)
                break label370;
            if (l3 != 60L)
                break label333;
            j = 101711876;
        }
        label423: 
        while (true)
        {
            if (j != -1)
            {
                int m = (int)l3;
                String str1 = localResources.getQuantityString(j, m);
                Object[] arrayOfObject1 = new Object[1];
                arrayOfObject1[0] = Long.valueOf(l3);
                paramCharBuffer.append(String.format(str1, arrayOfObject1));
            }
            mTimePool.release(localTime1);
            mTimePool.release(localTime2);
            return;
            i = 0;
            break;
            label333: if (l3 == 30L)
            {
                j = 101711882;
            }
            else if (l3 == 0L)
            {
                j = 101711877;
            }
            else
            {
                j = 101711884;
                continue;
                label370: if (l3 == 60L)
                {
                    j = 101711879;
                }
                else if (l3 == 30L)
                {
                    j = 101711874;
                }
                else if (l3 == 0L)
                {
                    j = 101711883;
                }
                else
                {
                    j = 101711886;
                    continue;
                    if ((localTime1.year == localTime2.year) && (localTime1.yearDay == localTime2.yearDay))
                    {
                        getFormatTime(paramContext, localTime2, 1, bool, paramCharBuffer);
                    }
                    else if ((localTime1.year == localTime2.year) && (Math.abs(localTime1.yearDay - localTime2.yearDay) < 2))
                    {
                        if (i != 0)
                        {
                            paramCharBuffer.append(localResources.getString(101450152));
                            paramCharBuffer.append(" ");
                            getFormatTime(paramContext, localTime2, 1, bool, paramCharBuffer);
                        }
                        else
                        {
                            paramCharBuffer.append(localResources.getString(101450153));
                            paramCharBuffer.append(" ");
                            getFormatTime(paramContext, localTime2, 1, bool, paramCharBuffer);
                        }
                    }
                    else if ((localTime1.year == localTime2.year) && (localTime1.getWeekNumber() == localTime2.getWeekNumber()))
                    {
                        paramCharBuffer.append(DateUtils.formatDateRange(paramContext, paramLong, paramLong, 524290));
                        paramCharBuffer.append(" ");
                        getFormatTime(paramContext, localTime2, 1, bool, paramCharBuffer);
                    }
                    else if (localTime1.year == localTime2.year)
                    {
                        String str2 = sMonth[localTime2.month];
                        Object[] arrayOfObject2 = new Object[1];
                        arrayOfObject2[0] = Integer.valueOf(localTime2.monthDay);
                        String str3 = localResources.getString(101450186, arrayOfObject2);
                        paramCharBuffer.append(str2 + str3);
                        if (paramBoolean)
                        {
                            paramCharBuffer.append(" ");
                            getFormatTime(paramContext, localTime2, 1, bool, paramCharBuffer);
                        }
                    }
                    else
                    {
                        int k = 524308;
                        if (paramBoolean)
                            k |= 1;
                        paramCharBuffer.append(DateUtils.formatDateRange(paramContext, paramLong, paramLong, k));
                    }
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.util.MiuiDateUtils
 * JD-Core Version:        0.6.2
 */