package miui.util;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.PrintStream;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public abstract class DensyIndexFile
{
    private static final boolean DEBUG = true;
    private static final String LOG_TAG = "DensyIndexFile: ";

    static byte getSizeOf(int paramInt)
        throws IOException
    {
        byte b = 0;
        int i = paramInt;
        while (i > 0)
        {
            b = (byte)(b + 1);
            i >>= 8;
        }
        if (b == 3)
            b = 4;
        do
            while (true)
            {
                return b;
                if ((b <= 4) || (b >= 8))
                    break;
                b = 8;
            }
        while (b <= 8);
        throw new IOException("Too many data");
    }

    static void loge(String paramString)
    {
        System.err.println("DensyIndexFile: " + paramString);
    }

    static void logo(String paramString)
    {
        System.out.println("DensyIndexFile: " + paramString);
    }

    static long readAccordingToSize(DataInput paramDataInput, int paramInt)
        throws IOException
    {
        long l;
        switch (paramInt)
        {
        case 3:
        case 5:
        case 6:
        case 7:
        default:
            throw new IOException("Unsupport size " + paramInt);
        case 1:
            l = paramDataInput.readByte();
        case 2:
        case 4:
        case 8:
        }
        while (true)
        {
            return l;
            l = paramDataInput.readShort();
            continue;
            l = paramDataInput.readInt();
            continue;
            l = paramDataInput.readLong();
        }
    }

    static void writeAccordingToSize(DataOutput paramDataOutput, int paramInt, long paramLong)
        throws IOException
    {
        switch (paramInt)
        {
        case 3:
        case 5:
        case 6:
        case 7:
        default:
            throw new IOException("Unsupport size " + paramInt);
        case 1:
            paramDataOutput.writeByte((int)paramLong);
        case 2:
        case 4:
        case 8:
        }
        while (true)
        {
            return;
            paramDataOutput.writeShort((int)paramLong);
            continue;
            paramDataOutput.writeInt((int)paramLong);
            continue;
            paramDataOutput.writeLong(paramLong);
        }
    }

    public static class Builder
    {
        IndexData mCurrentIndexData;
        DensyIndexFile.FileHeader mFileHeader;
        ArrayList<IndexData> mIndexDataList = new ArrayList();

        private void build()
            throws IOException
        {
            int i = this.mIndexDataList.size();
            this.mFileHeader = new DensyIndexFile.FileHeader(i);
            for (int j = 0; j < i; j++)
            {
                IndexData localIndexData = (IndexData)this.mIndexDataList.get(j);
                this.mFileHeader.mDescriptionOffsets[j] = new DensyIndexFile.DescriptionPair(0L, 0L);
                for (int k = 0; ; k++)
                    if ((k >= localIndexData.mIndexDataGroups.size()) || (((ArrayList)localIndexData.mIndexDataGroups.get(k)).size() == 0))
                    {
                        localIndexData.mIndexGroupDescriptions = new DensyIndexFile.IndexGroupDescription[k];
                        for (int m = 0; m < localIndexData.mIndexGroupDescriptions.length; m++)
                        {
                            ArrayList localArrayList = (ArrayList)localIndexData.mIndexDataGroups.get(m);
                            Collections.sort(localArrayList);
                            int n = ((Item)localArrayList.get(0)).mIndex;
                            int i1 = 1 + ((Item)localArrayList.get(-1 + localArrayList.size())).mIndex;
                            localIndexData.mIndexGroupDescriptions[m] = new DensyIndexFile.IndexGroupDescription(n, i1, 0L);
                        }
                    }
            }
            writeAll(null);
        }

        private void checkCurrentIndexingDataKind()
            throws IOException
        {
            if (this.mCurrentIndexData == null)
                throw new IOException("Please add a data kind before adding group");
        }

        private void checkCurrentIndexingGroup()
            throws IOException
        {
            checkCurrentIndexingDataKind();
            if (this.mCurrentIndexData.mIndexDataGroups.size() == 0)
                throw new IOException("Please add a data group before adding data");
        }

        private int writeAll(DataOutput paramDataOutput)
            throws IOException
        {
            int i = 0;
            for (int j = 0; j < this.mFileHeader.mDescriptionOffsets.length; j++)
            {
                int k = i + this.mFileHeader.write(paramDataOutput);
                this.mFileHeader.mDescriptionOffsets[j].mIndexGroupDescriptionOffset = k;
                IndexData localIndexData = (IndexData)this.mIndexDataList.get(j);
                if (paramDataOutput != null)
                    paramDataOutput.writeInt(localIndexData.mIndexGroupDescriptions.length);
                int m = k + 4;
                for (int n = 0; n < localIndexData.mIndexGroupDescriptions.length; n++)
                    m += localIndexData.mIndexGroupDescriptions[n].write(paramDataOutput);
                this.mFileHeader.mDescriptionOffsets[j].mDataItemDescriptionOffset = m;
                if (paramDataOutput != null)
                    paramDataOutput.writeInt(localIndexData.mDataItemDescriptions.length);
                i = m + 4;
                for (int i1 = 0; i1 < localIndexData.mDataItemDescriptions.length; i1++)
                    i += localIndexData.mDataItemDescriptions[i1].write(paramDataOutput);
                for (int i2 = 0; i2 < localIndexData.mDataItemDescriptions.length; i2++)
                {
                    localIndexData.mDataItemDescriptions[i2].mOffset = i;
                    i += localIndexData.mDataItemDescriptions[i2].writeDataItems(paramDataOutput, ((DataItemHolder)localIndexData.mDataItemHolders.get(i2)).getAll());
                }
                int i3 = 0;
                if (i3 < localIndexData.mIndexGroupDescriptions.length)
                {
                    localIndexData.mIndexGroupDescriptions[i3].mOffset = i;
                    if (paramDataOutput == null)
                    {
                        int i6 = 0;
                        for (int i7 = 0; i7 < localIndexData.mDataItemDescriptions.length; i7++)
                            i6 += localIndexData.mDataItemDescriptions[i7].mIndexSize;
                        i += i6 * (localIndexData.mIndexGroupDescriptions[i3].mMaxIndex - localIndexData.mIndexGroupDescriptions[i3].mMinIndex);
                    }
                    while (true)
                    {
                        i3++;
                        break;
                        for (int i4 = localIndexData.mIndexGroupDescriptions[i3].mMinIndex; i4 < localIndexData.mIndexGroupDescriptions[i3].mMaxIndex; i4++)
                        {
                            Item localItem = (Item)localIndexData.mDataMap.get(Integer.valueOf(i4));
                            if (localItem == null)
                                localItem = localIndexData.mDefaultValue;
                            int i5 = 0;
                            if (i5 < localIndexData.mDataItemDescriptions.length)
                            {
                                if (localIndexData.mDataItemDescriptions[i5].mIndexSize == 1)
                                    paramDataOutput.writeByte(((Integer)localItem.mObjects[i5]).intValue());
                                while (true)
                                {
                                    i += localIndexData.mDataItemDescriptions[i5].mIndexSize;
                                    i5++;
                                    break;
                                    if (localIndexData.mDataItemDescriptions[i5].mIndexSize == 2)
                                        paramDataOutput.writeShort(((Integer)localItem.mObjects[i5]).intValue());
                                    else if (localIndexData.mDataItemDescriptions[i5].mIndexSize == 4)
                                        paramDataOutput.writeInt(((Integer)localItem.mObjects[i5]).intValue());
                                    else if (localIndexData.mDataItemDescriptions[i5].mIndexSize == 8)
                                        paramDataOutput.writeLong(((Long)localItem.mObjects[i5]).longValue());
                                }
                            }
                        }
                    }
                }
            }
            return i;
        }

        public void add(int paramInt, Object[] paramArrayOfObject)
            throws IOException
        {
            checkCurrentIndexingGroup();
            if (this.mCurrentIndexData.mDataItemDescriptions.length != paramArrayOfObject.length)
                throw new IOException("Different number of objects inputted");
            int i = 0;
            if (i < paramArrayOfObject.length)
            {
                switch (this.mCurrentIndexData.mDataItemDescriptions[i].mType)
                {
                default:
                    throw new IOException("Unsupported type of objects inputted");
                case 0:
                    if (!(paramArrayOfObject[i] instanceof Byte))
                        throw new IOException("Object[" + i + "] should be byte");
                    break;
                case 1:
                    if (!(paramArrayOfObject[i] instanceof Short))
                        throw new IOException("Object[" + i + "] should be short");
                    break;
                case 2:
                    if (!(paramArrayOfObject[i] instanceof Integer))
                        throw new IOException("Object[" + i + "] should be int");
                    break;
                case 3:
                    if (!(paramArrayOfObject[i] instanceof Long))
                        throw new IOException("Object[" + i + "] should be long");
                    break;
                case 4:
                    if (!(paramArrayOfObject[i] instanceof String))
                        throw new IOException("Object[" + i + "] should be String");
                    paramArrayOfObject[i] = ((DataItemHolder)this.mCurrentIndexData.mDataItemHolders.get(i)).put(paramArrayOfObject[i]);
                    this.mCurrentIndexData.mDataItemDescriptions[i].mIndexSize = DensyIndexFile.getSizeOf(((DataItemHolder)this.mCurrentIndexData.mDataItemHolders.get(i)).size());
                case 5:
                case 6:
                case 7:
                case 8:
                }
                while (true)
                {
                    i++;
                    break;
                    if (!(paramArrayOfObject[i] instanceof byte[]))
                        throw new IOException("Object[" + i + "] should be byte[]");
                    paramArrayOfObject[i] = ((DataItemHolder)this.mCurrentIndexData.mDataItemHolders.get(i)).put(paramArrayOfObject[i]);
                    this.mCurrentIndexData.mDataItemDescriptions[i].mIndexSize = DensyIndexFile.getSizeOf(((DataItemHolder)this.mCurrentIndexData.mDataItemHolders.get(i)).size());
                    continue;
                    if (!(paramArrayOfObject[i] instanceof short[]))
                        throw new IOException("Object[" + i + "] should be short[]");
                    paramArrayOfObject[i] = ((DataItemHolder)this.mCurrentIndexData.mDataItemHolders.get(i)).put(paramArrayOfObject[i]);
                    this.mCurrentIndexData.mDataItemDescriptions[i].mIndexSize = DensyIndexFile.getSizeOf(((DataItemHolder)this.mCurrentIndexData.mDataItemHolders.get(i)).size());
                    continue;
                    if (!(paramArrayOfObject[i] instanceof int[]))
                        throw new IOException("Object[" + i + "] should be int[]");
                    paramArrayOfObject[i] = ((DataItemHolder)this.mCurrentIndexData.mDataItemHolders.get(i)).put(paramArrayOfObject[i]);
                    this.mCurrentIndexData.mDataItemDescriptions[i].mIndexSize = DensyIndexFile.getSizeOf(((DataItemHolder)this.mCurrentIndexData.mDataItemHolders.get(i)).size());
                    continue;
                    if (!(paramArrayOfObject[i] instanceof long[]))
                        throw new IOException("Object[" + i + "] should be long[]");
                    paramArrayOfObject[i] = ((DataItemHolder)this.mCurrentIndexData.mDataItemHolders.get(i)).put(paramArrayOfObject[i]);
                    this.mCurrentIndexData.mDataItemDescriptions[i].mIndexSize = DensyIndexFile.getSizeOf(((DataItemHolder)this.mCurrentIndexData.mDataItemHolders.get(i)).size());
                }
            }
            Item localItem = new Item(paramInt, paramArrayOfObject);
            this.mCurrentIndexData.mDataMap.put(Integer.valueOf(paramInt), localItem);
            ((ArrayList)this.mCurrentIndexData.mIndexDataGroups.get(-1 + this.mCurrentIndexData.mIndexDataGroups.size())).add(localItem);
        }

        public void addGroup(int[] paramArrayOfInt, Object[][] paramArrayOfObject)
            throws IOException
        {
            checkCurrentIndexingDataKind();
            if (paramArrayOfInt.length == paramArrayOfObject.length)
            {
                newGroup();
                for (int i = 0; i < paramArrayOfInt.length; i++)
                    add(paramArrayOfInt[i], paramArrayOfObject[i]);
            }
            throw new IOException("Different number between indexes and objects");
        }

        public void addKind(Object[] paramArrayOfObject)
            throws IOException
        {
            this.mCurrentIndexData = new IndexData(paramArrayOfObject.length);
            this.mIndexDataList.add(this.mCurrentIndexData);
            int i = 0;
            if (i < paramArrayOfObject.length)
            {
                this.mCurrentIndexData.mDataItemHolders.add(new DataItemHolder(null));
                byte b1 = 1;
                byte b2;
                if ((paramArrayOfObject[i] instanceof Byte))
                {
                    b2 = 0;
                    b1 = 1;
                    ((DataItemHolder)this.mCurrentIndexData.mDataItemHolders.get(i)).put(paramArrayOfObject[i]);
                }
                while (true)
                {
                    this.mCurrentIndexData.mDataItemDescriptions[i] = new DensyIndexFile.DataItemDescription(b2, b1, 0, 0, 0L);
                    i++;
                    break;
                    if ((paramArrayOfObject[i] instanceof Short))
                    {
                        b2 = 1;
                        b1 = 2;
                        ((DataItemHolder)this.mCurrentIndexData.mDataItemHolders.get(i)).put(paramArrayOfObject[i]);
                    }
                    else if ((paramArrayOfObject[i] instanceof Integer))
                    {
                        b2 = 2;
                        b1 = 4;
                        ((DataItemHolder)this.mCurrentIndexData.mDataItemHolders.get(i)).put(paramArrayOfObject[i]);
                    }
                    else if ((paramArrayOfObject[i] instanceof Long))
                    {
                        b2 = 3;
                        b1 = 8;
                        ((DataItemHolder)this.mCurrentIndexData.mDataItemHolders.get(i)).put(paramArrayOfObject[i]);
                    }
                    else if ((paramArrayOfObject[i] instanceof String))
                    {
                        b2 = 4;
                        paramArrayOfObject[i] = ((DataItemHolder)this.mCurrentIndexData.mDataItemHolders.get(i)).put(paramArrayOfObject[i]);
                    }
                    else if ((paramArrayOfObject[i] instanceof byte[]))
                    {
                        b2 = 5;
                        paramArrayOfObject[i] = ((DataItemHolder)this.mCurrentIndexData.mDataItemHolders.get(i)).put(paramArrayOfObject[i]);
                    }
                    else if ((paramArrayOfObject[i] instanceof short[]))
                    {
                        b2 = 6;
                        paramArrayOfObject[i] = ((DataItemHolder)this.mCurrentIndexData.mDataItemHolders.get(i)).put(paramArrayOfObject[i]);
                    }
                    else if ((paramArrayOfObject[i] instanceof int[]))
                    {
                        b2 = 7;
                        paramArrayOfObject[i] = ((DataItemHolder)this.mCurrentIndexData.mDataItemHolders.get(i)).put(paramArrayOfObject[i]);
                    }
                    else
                    {
                        if (!(paramArrayOfObject[i] instanceof long[]))
                            break label432;
                        b2 = 8;
                        paramArrayOfObject[i] = ((DataItemHolder)this.mCurrentIndexData.mDataItemHolders.get(i)).put(paramArrayOfObject[i]);
                    }
                }
                label432: throw new IOException("Unsupported type of object[" + i + "] inputted");
            }
            this.mCurrentIndexData.mDefaultValue = new Item(-1, paramArrayOfObject);
        }

        public void newGroup()
        {
            if ((this.mCurrentIndexData.mIndexDataGroups.size() == 0) || (((ArrayList)this.mCurrentIndexData.mIndexDataGroups.get(-1 + this.mCurrentIndexData.mIndexDataGroups.size())).size() != 0))
                this.mCurrentIndexData.mIndexDataGroups.add(new ArrayList());
        }

        // ERROR //
        public void write(String paramString)
            throws IOException
        {
            // Byte code:
            //     0: aload_0
            //     1: invokespecial 289	miui/util/DensyIndexFile$Builder:build	()V
            //     4: aconst_null
            //     5: astore_2
            //     6: new 291	java/io/DataOutputStream
            //     9: dup
            //     10: new 293	java/io/BufferedOutputStream
            //     13: dup
            //     14: new 295	java/io/FileOutputStream
            //     17: dup
            //     18: aload_1
            //     19: invokespecial 296	java/io/FileOutputStream:<init>	(Ljava/lang/String;)V
            //     22: invokespecial 299	java/io/BufferedOutputStream:<init>	(Ljava/io/OutputStream;)V
            //     25: invokespecial 300	java/io/DataOutputStream:<init>	(Ljava/io/OutputStream;)V
            //     28: astore_3
            //     29: aload_0
            //     30: aload_3
            //     31: invokespecial 85	miui/util/DensyIndexFile$Builder:writeAll	(Ljava/io/DataOutput;)I
            //     34: pop
            //     35: aload_3
            //     36: invokevirtual 303	java/io/DataOutputStream:close	()V
            //     39: iconst_1
            //     40: ifne +15 -> 55
            //     43: new 305	java/io/File
            //     46: dup
            //     47: aload_1
            //     48: invokespecial 306	java/io/File:<init>	(Ljava/lang/String;)V
            //     51: invokevirtual 310	java/io/File:delete	()Z
            //     54: pop
            //     55: return
            //     56: astore 4
            //     58: aload_2
            //     59: invokevirtual 303	java/io/DataOutputStream:close	()V
            //     62: iconst_0
            //     63: ifne +15 -> 78
            //     66: new 305	java/io/File
            //     69: dup
            //     70: aload_1
            //     71: invokespecial 306	java/io/File:<init>	(Ljava/lang/String;)V
            //     74: invokevirtual 310	java/io/File:delete	()Z
            //     77: pop
            //     78: aload 4
            //     80: athrow
            //     81: astore 4
            //     83: aload_3
            //     84: astore_2
            //     85: goto -27 -> 58
            //
            // Exception table:
            //     from	to	target	type
            //     6	29	56	finally
            //     29	35	81	finally
        }

        private static class IndexData
        {
            DensyIndexFile.DataItemDescription[] mDataItemDescriptions;
            ArrayList<DensyIndexFile.Builder.DataItemHolder> mDataItemHolders = new ArrayList();
            HashMap<Integer, DensyIndexFile.Builder.Item> mDataMap = new HashMap();
            DensyIndexFile.Builder.Item mDefaultValue;
            ArrayList<ArrayList<DensyIndexFile.Builder.Item>> mIndexDataGroups = new ArrayList();
            DensyIndexFile.IndexGroupDescription[] mIndexGroupDescriptions;

            IndexData(int paramInt)
            {
                this.mDataItemDescriptions = new DensyIndexFile.DataItemDescription[paramInt];
            }
        }

        private class DataItemHolder
        {
            private ArrayList<Object> mList = new ArrayList();
            private HashMap<Object, Integer> mMap = new HashMap();

            private DataItemHolder()
            {
            }

            ArrayList<Object> getAll()
            {
                return this.mList;
            }

            Integer put(Object paramObject)
            {
                Integer localInteger = (Integer)this.mMap.get(paramObject);
                if (localInteger == null)
                {
                    localInteger = Integer.valueOf(this.mList.size());
                    this.mMap.put(paramObject, localInteger);
                    this.mList.add(paramObject);
                }
                return localInteger;
            }

            int size()
            {
                return this.mList.size();
            }
        }

        private class Item
            implements Comparable<Item>
        {
            int mIndex;
            Object[] mObjects;

            public Item(int paramArrayOfObject, Object[] arg3)
            {
                this.mIndex = paramArrayOfObject;
                Object localObject;
                this.mObjects = localObject;
            }

            public int compareTo(Item paramItem)
            {
                return this.mIndex - paramItem.mIndex;
            }

            public boolean equals(Object paramObject)
            {
                boolean bool = false;
                if (paramObject == this)
                    bool = true;
                while (!(paramObject instanceof Item))
                    return bool;
                if (this.mIndex == ((Item)paramObject).mIndex);
                for (bool = true; ; bool = false)
                    break;
            }

            public int hashCode()
            {
                return this.mIndex;
            }
        }
    }

    public static class Reader
    {
        private RandomAccessFile mFile;
        private DensyIndexFile.FileHeader mHeader;
        private IndexData[] mIndexData;

        public Reader(String paramString)
            throws IOException
        {
            long l1 = System.currentTimeMillis();
            try
            {
                this.mFile = new RandomAccessFile(paramString, "r");
                this.mFile.seek(0L);
                this.mHeader = DensyIndexFile.FileHeader.read(this.mFile);
                this.mIndexData = new IndexData[this.mHeader.mDescriptionOffsets.length];
                for (int i = 0; i < this.mHeader.mDescriptionOffsets.length; i++)
                {
                    this.mIndexData[i] = new IndexData(null);
                    this.mFile.seek(this.mHeader.mDescriptionOffsets[i].mIndexGroupDescriptionOffset);
                    int j = this.mFile.readInt();
                    this.mIndexData[i].mIndexGroupDescriptions = new DensyIndexFile.IndexGroupDescription[j];
                    for (int k = 0; k < j; k++)
                        this.mIndexData[i].mIndexGroupDescriptions[k] = DensyIndexFile.IndexGroupDescription.read(this.mFile);
                    this.mFile.seek(this.mHeader.mDescriptionOffsets[i].mDataItemDescriptionOffset);
                    int m = this.mFile.readInt();
                    this.mIndexData[i].mSizeOfItems = 0;
                    this.mIndexData[i].mDataItemDescriptions = new DensyIndexFile.DataItemDescription[m];
                    for (int n = 0; n < m; n++)
                    {
                        this.mIndexData[i].mDataItemDescriptions[n] = DensyIndexFile.DataItemDescription.read(this.mFile);
                        IndexData localIndexData = this.mIndexData[i];
                        localIndexData.mSizeOfItems += this.mIndexData[i].mDataItemDescriptions[n].mIndexSize;
                    }
                    this.mIndexData[i].mDataItems = new Object[m][];
                    for (int i1 = 0; i1 < m; i1++)
                    {
                        this.mFile.seek(this.mIndexData[i].mDataItemDescriptions[i1].mOffset);
                        this.mIndexData[i].mDataItems[i1] = this.mIndexData[i].mDataItemDescriptions[i1].readDataItems(this.mFile);
                    }
                }
            }
            catch (IOException localIOException)
            {
                close();
                throw localIOException;
            }
            long l2 = System.currentTimeMillis() - l1;
            DensyIndexFile.logo(l2 + "ms used to load file " + paramString);
        }

        private long offset(int paramInt1, int paramInt2)
        {
            DensyIndexFile.IndexGroupDescription localIndexGroupDescription = null;
            int i = 0;
            int j = this.mIndexData[paramInt1].mIndexGroupDescriptions.length;
            while (i < j)
            {
                int k = (j + i) / 2;
                if (this.mIndexData[paramInt1].mIndexGroupDescriptions[k].mMinIndex > paramInt2)
                    j = k;
                else if (this.mIndexData[paramInt1].mIndexGroupDescriptions[k].mMaxIndex <= paramInt2)
                    i = k + 1;
                else
                    localIndexGroupDescription = this.mIndexData[paramInt1].mIndexGroupDescriptions[k];
            }
            long l = -1L;
            if (localIndexGroupDescription != null)
                l = localIndexGroupDescription.mOffset + (paramInt2 - localIndexGroupDescription.mMinIndex) * this.mIndexData[paramInt1].mSizeOfItems;
            return l;
        }

        private Object readSingleDataItem(int paramInt1, int paramInt2, int paramInt3)
            throws IOException
        {
            if (this.mIndexData[paramInt1].mDataItems[paramInt2][paramInt3] == null)
            {
                this.mFile.seek(4L + this.mIndexData[paramInt1].mDataItemDescriptions[paramInt2].mOffset);
                this.mIndexData[paramInt1].mDataItems[paramInt2][paramInt3] = this.mIndexData[paramInt1].mDataItemDescriptions[paramInt2].readSingleDataItem(this.mFile, paramInt3);
            }
            return this.mIndexData[paramInt1].mDataItems[paramInt2][paramInt3];
        }

        /** @deprecated */
        public void close()
            throws IOException
        {
            try
            {
                if (this.mFile != null)
                    this.mFile.close();
                this.mFile = null;
                this.mHeader = null;
                this.mIndexData = null;
                return;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        /** @deprecated */
        public Object get(int paramInt1, int paramInt2, int paramInt3)
        {
            Object localObject1 = null;
            long l2;
            Object localObject3;
            while (true)
            {
                try
                {
                    if (this.mFile == null)
                    {
                        DensyIndexFile.loge("Get data from a corrupt file");
                        return localObject1;
                    }
                    if ((paramInt1 < 0) || (paramInt1 >= this.mIndexData.length))
                    {
                        DensyIndexFile.loge("Kind " + paramInt1 + " out of range[0, " + this.mIndexData.length + ")");
                        continue;
                    }
                }
                finally
                {
                }
                if ((paramInt3 < 0) || (paramInt3 >= this.mIndexData[paramInt1].mDataItemDescriptions.length))
                {
                    DensyIndexFile.loge("DataIndex " + paramInt3 + " out of range[0, " + this.mIndexData[paramInt1].mDataItemDescriptions.length + ")");
                }
                else
                {
                    long l1 = System.currentTimeMillis();
                    l2 = offset(paramInt1, paramInt2);
                    localObject3 = null;
                    if (l2 >= 0L)
                        break;
                    localObject1 = this.mIndexData[paramInt1].mDataItems[paramInt3][0];
                    long l3 = System.currentTimeMillis() - l1;
                    DensyIndexFile.logo(l3 + "ms used to get data(" + paramInt1 + ", " + paramInt2 + ", " + paramInt3 + ")");
                }
            }
            while (true)
            {
                int i;
                try
                {
                    this.mFile.seek(l2);
                    i = 0;
                    if (i > paramInt3)
                        break label512;
                    switch (this.mIndexData[paramInt1].mDataItemDescriptions[i].mType)
                    {
                    default:
                        throw new IOException("Unknown type " + this.mIndexData[paramInt1].mDataItemDescriptions[i].mType);
                    case 0:
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                    }
                }
                catch (IOException localIOException1)
                {
                    DensyIndexFile.loge("Seek data from a corrupt file");
                    localObject1 = null;
                }
                break;
                localObject3 = Byte.valueOf(this.mFile.readByte());
                break label519;
                localObject3 = Short.valueOf(this.mFile.readShort());
                break label519;
                localObject3 = Integer.valueOf(this.mFile.readInt());
                break label519;
                Long localLong = Long.valueOf(this.mFile.readLong());
                localObject3 = localLong;
                break label519;
                try
                {
                    int j = (int)DensyIndexFile.readAccordingToSize(this.mFile, this.mIndexData[paramInt1].mDataItemDescriptions[i].mIndexSize);
                    if (i != paramInt3)
                        break label519;
                    Object localObject4 = readSingleDataItem(paramInt1, paramInt3, j);
                    localObject3 = localObject4;
                }
                catch (IOException localIOException2)
                {
                    throw new IOException("File may be corrupt due to invalid data index size", localIOException2);
                }
                label512: localObject1 = localObject3;
                break;
                label519: i++;
            }
        }

        /** @deprecated */
        public Object[] get(int paramInt1, int paramInt2)
        {
            Object[] arrayOfObject = null;
            long l1;
            long l2;
            while (true)
            {
                try
                {
                    if (this.mFile == null)
                    {
                        DensyIndexFile.loge("Get data from a corrupt file");
                        return arrayOfObject;
                    }
                    if ((paramInt1 < 0) || (paramInt1 >= this.mIndexData.length))
                    {
                        DensyIndexFile.loge("Cannot get data kind " + paramInt1);
                        continue;
                    }
                }
                finally
                {
                }
                l1 = System.currentTimeMillis();
                l2 = offset(paramInt1, paramInt2);
                arrayOfObject = new Object[this.mIndexData[paramInt1].mDataItemDescriptions.length];
                if (l2 >= 0L)
                    break;
                for (int k = 0; k < arrayOfObject.length; k++)
                    arrayOfObject[k] = this.mIndexData[paramInt1].mDataItems[k][0];
            }
            while (true)
            {
                int i;
                try
                {
                    this.mFile.seek(l2);
                    i = 0;
                    if (i < arrayOfObject.length)
                        switch (this.mIndexData[paramInt1].mDataItemDescriptions[i].mType)
                        {
                        default:
                            throw new IOException("Unknown type " + this.mIndexData[paramInt1].mDataItemDescriptions[i].mType);
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                        case 6:
                        case 7:
                        case 8:
                        }
                }
                catch (IOException localIOException1)
                {
                    DensyIndexFile.loge("Seek data from a corrupt file");
                    arrayOfObject = null;
                    long l3 = System.currentTimeMillis() - l1;
                    DensyIndexFile.logo(l3 + "ms used to get data(" + paramInt1 + ", " + paramInt2 + ")");
                }
                break;
                arrayOfObject[i] = Byte.valueOf(this.mFile.readByte());
                break label459;
                arrayOfObject[i] = Short.valueOf(this.mFile.readShort());
                break label459;
                arrayOfObject[i] = Integer.valueOf(this.mFile.readInt());
                break label459;
                arrayOfObject[i] = Long.valueOf(this.mFile.readLong());
                break label459;
                try
                {
                    int j = (int)DensyIndexFile.readAccordingToSize(this.mFile, this.mIndexData[paramInt1].mDataItemDescriptions[i].mIndexSize);
                    long l4 = this.mFile.getFilePointer();
                    arrayOfObject[i] = readSingleDataItem(paramInt1, i, j);
                    this.mFile.seek(l4);
                }
                catch (IOException localIOException2)
                {
                    throw new IOException("File may be corrupt due to invalid data index size", localIOException2);
                }
                label459: i++;
            }
        }

        private static class IndexData
        {
            public DensyIndexFile.DataItemDescription[] mDataItemDescriptions;
            public Object[][] mDataItems;
            public DensyIndexFile.IndexGroupDescription[] mIndexGroupDescriptions;
            public int mSizeOfItems;
        }
    }

    private static class FileHeader
    {
        private static final int CURRENT_VERSION = 1;
        private static final byte[] FILE_TAG = arrayOfByte;
        public DensyIndexFile.DescriptionPair[] mDescriptionOffsets;

        static
        {
            byte[] arrayOfByte = new byte[4];
            arrayOfByte[0] = 73;
            arrayOfByte[1] = 68;
            arrayOfByte[2] = 70;
            arrayOfByte[3] = 32;
        }

        public FileHeader(int paramInt)
        {
            this.mDescriptionOffsets = new DensyIndexFile.DescriptionPair[paramInt];
        }

        public static FileHeader read(DataInput paramDataInput)
            throws IOException
        {
            byte[] arrayOfByte = new byte[FILE_TAG.length];
            for (int i = 0; i < arrayOfByte.length; i++)
                arrayOfByte[i] = paramDataInput.readByte();
            if (!Arrays.equals(arrayOfByte, FILE_TAG))
                throw new IOException("File tag unmatched, file may be corrupt");
            if (paramDataInput.readInt() != 1)
                throw new IOException("File version unmatched, please upgrade your reader");
            int j = paramDataInput.readInt();
            FileHeader localFileHeader = new FileHeader(j);
            for (int k = 0; k < j; k++)
                localFileHeader.mDescriptionOffsets[k] = DensyIndexFile.DescriptionPair.read(paramDataInput);
            return localFileHeader;
        }

        public int write(DataOutput paramDataOutput)
            throws IOException
        {
            int i = 4 + (4 + FILE_TAG.length);
            if (paramDataOutput != null)
            {
                paramDataOutput.write(FILE_TAG);
                paramDataOutput.writeInt(1);
                paramDataOutput.writeInt(this.mDescriptionOffsets.length);
            }
            for (int j = 0; j < this.mDescriptionOffsets.length; j++)
                i += this.mDescriptionOffsets[j].write(paramDataOutput);
            return i;
        }
    }

    private static class DescriptionPair
    {
        long mDataItemDescriptionOffset;
        long mIndexGroupDescriptionOffset;

        DescriptionPair(long paramLong1, long paramLong2)
        {
            this.mIndexGroupDescriptionOffset = paramLong1;
            this.mDataItemDescriptionOffset = paramLong2;
        }

        static DescriptionPair read(DataInput paramDataInput)
            throws IOException
        {
            return new DescriptionPair(paramDataInput.readLong(), paramDataInput.readLong());
        }

        int write(DataOutput paramDataOutput)
            throws IOException
        {
            if (paramDataOutput != null)
            {
                paramDataOutput.writeLong(this.mIndexGroupDescriptionOffset);
                paramDataOutput.writeLong(this.mDataItemDescriptionOffset);
            }
            return 16;
        }
    }

    private static class DataItemDescription
    {
        static final byte BYTE = 0;
        static final byte BYTE_ARRAY = 5;
        static final byte INTEGER = 2;
        static final byte INTEGER_ARRAY = 7;
        static final byte LONG = 3;
        static final byte LONG_ARRAY = 8;
        static final byte SHORT = 1;
        static final byte SHORT_ARRAY = 6;
        static final byte STRING = 4;
        static byte[] sBuffer = new byte[1024];
        byte mIndexSize;
        byte mLengthSize;
        long mOffset;
        byte mOffsetSize;
        byte mType;

        DataItemDescription(byte paramByte1, byte paramByte2, byte paramByte3, byte paramByte4, long paramLong)
        {
            this.mType = paramByte1;
            this.mIndexSize = paramByte2;
            this.mLengthSize = paramByte3;
            this.mOffsetSize = paramByte4;
            this.mOffset = paramLong;
        }

        static byte[] aquireBuffer(int paramInt)
        {
            try
            {
                if ((sBuffer == null) || (sBuffer.length < paramInt))
                    sBuffer = new byte[paramInt];
                byte[] arrayOfByte = sBuffer;
                sBuffer = null;
                return arrayOfByte;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        static DataItemDescription read(DataInput paramDataInput)
            throws IOException
        {
            return new DataItemDescription(paramDataInput.readByte(), paramDataInput.readByte(), paramDataInput.readByte(), paramDataInput.readByte(), paramDataInput.readLong());
        }

        static void releaseBuffer(byte[] paramArrayOfByte)
        {
            if (paramArrayOfByte != null);
            try
            {
                if ((sBuffer == null) || (sBuffer.length < paramArrayOfByte.length))
                    sBuffer = paramArrayOfByte;
                return;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        Object[] readDataItems(RandomAccessFile paramRandomAccessFile)
            throws IOException
        {
            Object[] arrayOfObject = null;
            switch (this.mType)
            {
            default:
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            }
            while (true)
            {
                return arrayOfObject;
                arrayOfObject = new Object[1];
                arrayOfObject[0] = Byte.valueOf(paramRandomAccessFile.readByte());
                continue;
                arrayOfObject = new Object[1];
                arrayOfObject[0] = Short.valueOf(paramRandomAccessFile.readShort());
                continue;
                arrayOfObject = new Object[1];
                arrayOfObject[0] = Integer.valueOf(paramRandomAccessFile.readInt());
                continue;
                arrayOfObject = new Object[1];
                arrayOfObject[0] = Long.valueOf(paramRandomAccessFile.readLong());
                continue;
                arrayOfObject = new Object[paramRandomAccessFile.readInt()];
                arrayOfObject[0] = readSingleDataItem(paramRandomAccessFile, 0);
            }
        }

        Object readSingleDataItem(RandomAccessFile paramRandomAccessFile, int paramInt)
            throws IOException
        {
            Object localObject = null;
            byte[] arrayOfByte1 = null;
            long l = paramRandomAccessFile.getFilePointer();
            if (paramInt != 0)
                paramRandomAccessFile.seek(l + paramInt * this.mOffsetSize);
            paramRandomAccessFile.seek(l + DensyIndexFile.readAccordingToSize(paramRandomAccessFile, this.mOffsetSize));
            switch (this.mType)
            {
            default:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            }
            while (true)
            {
                releaseBuffer(arrayOfByte1);
                return localObject;
                int m = (int)DensyIndexFile.readAccordingToSize(paramRandomAccessFile, this.mLengthSize);
                arrayOfByte1 = aquireBuffer(m);
                paramRandomAccessFile.readFully(arrayOfByte1, 0, m);
                localObject = new String(arrayOfByte1, 0, m);
                continue;
                byte[] arrayOfByte2 = new byte[(int)DensyIndexFile.readAccordingToSize(paramRandomAccessFile, this.mLengthSize)];
                paramRandomAccessFile.readFully(arrayOfByte2);
                localObject = arrayOfByte2;
                arrayOfByte1 = null;
                continue;
                short[] arrayOfShort = new short[(int)DensyIndexFile.readAccordingToSize(paramRandomAccessFile, this.mLengthSize)];
                localObject = arrayOfShort;
                for (int k = 0; k < arrayOfShort.length; k++)
                    arrayOfShort[k] = paramRandomAccessFile.readShort();
                int[] arrayOfInt = new int[(int)DensyIndexFile.readAccordingToSize(paramRandomAccessFile, this.mLengthSize)];
                localObject = arrayOfInt;
                for (int j = 0; j < arrayOfInt.length; j++)
                    arrayOfInt[j] = paramRandomAccessFile.readInt();
                long[] arrayOfLong = new long[(int)DensyIndexFile.readAccordingToSize(paramRandomAccessFile, this.mLengthSize)];
                localObject = arrayOfLong;
                for (int i = 0; i < arrayOfLong.length; i++)
                    arrayOfLong[i] = paramRandomAccessFile.readLong();
            }
        }

        int write(DataOutput paramDataOutput)
            throws IOException
        {
            if (paramDataOutput != null)
            {
                paramDataOutput.writeByte(this.mType);
                paramDataOutput.writeByte(this.mIndexSize);
                paramDataOutput.writeByte(this.mLengthSize);
                paramDataOutput.writeByte(this.mOffsetSize);
                paramDataOutput.writeLong(this.mOffset);
            }
            return 12;
        }

        int writeDataItems(DataOutput paramDataOutput, List<Object> paramList)
            throws IOException
        {
            int i = 0;
            switch (this.mType)
            {
            default:
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            }
            while (true)
            {
                return i;
                if (paramDataOutput != null)
                    paramDataOutput.writeByte(((Byte)paramList.get(0)).byteValue());
                i = 0 + 1;
                continue;
                if (paramDataOutput != null)
                    paramDataOutput.writeShort(((Short)paramList.get(0)).shortValue());
                i = 0 + 2;
                continue;
                if (paramDataOutput != null)
                    paramDataOutput.writeInt(((Integer)paramList.get(0)).intValue());
                i = 0 + 4;
                continue;
                if (paramDataOutput != null)
                    paramDataOutput.writeLong(((Long)paramList.get(0)).longValue());
                i = 0 + 8;
                continue;
                if (paramDataOutput != null)
                    paramDataOutput.writeInt(paramList.size());
                (0 + 4);
                i = 4 + writeOffsets(paramDataOutput, paramList);
                for (int i4 = 0; i4 < paramList.size(); i4++)
                {
                    byte[] arrayOfByte2 = ((String)paramList.get(i4)).getBytes();
                    if (paramDataOutput != null)
                    {
                        DensyIndexFile.writeAccordingToSize(paramDataOutput, this.mLengthSize, arrayOfByte2.length);
                        for (int i5 = 0; i5 < arrayOfByte2.length; i5++)
                            paramDataOutput.writeByte(arrayOfByte2[i5]);
                    }
                    i += this.mLengthSize + arrayOfByte2.length;
                }
                if (paramDataOutput != null)
                    paramDataOutput.writeInt(paramList.size());
                (0 + 4);
                i = 4 + writeOffsets(paramDataOutput, paramList);
                for (int i3 = 0; i3 < paramList.size(); i3++)
                {
                    byte[] arrayOfByte1 = (byte[])paramList.get(i3);
                    if (paramDataOutput != null)
                    {
                        DensyIndexFile.writeAccordingToSize(paramDataOutput, this.mLengthSize, arrayOfByte1.length);
                        paramDataOutput.write(arrayOfByte1);
                    }
                    i += this.mLengthSize + arrayOfByte1.length;
                }
                if (paramDataOutput != null)
                    paramDataOutput.writeInt(paramList.size());
                (0 + 4);
                i = 4 + writeOffsets(paramDataOutput, paramList);
                for (int i1 = 0; i1 < paramList.size(); i1++)
                {
                    short[] arrayOfShort = (short[])paramList.get(i1);
                    if (paramDataOutput != null)
                    {
                        DensyIndexFile.writeAccordingToSize(paramDataOutput, this.mLengthSize, arrayOfShort.length);
                        for (int i2 = 0; i2 < arrayOfShort.length; i2++)
                            paramDataOutput.writeShort(arrayOfShort[i2]);
                    }
                    i += this.mLengthSize + 2 * arrayOfShort.length;
                }
                if (paramDataOutput != null)
                    paramDataOutput.writeInt(paramList.size());
                (0 + 4);
                i = 4 + writeOffsets(paramDataOutput, paramList);
                for (int m = 0; m < paramList.size(); m++)
                {
                    int[] arrayOfInt = (int[])paramList.get(m);
                    if (paramDataOutput != null)
                    {
                        DensyIndexFile.writeAccordingToSize(paramDataOutput, this.mLengthSize, arrayOfInt.length);
                        for (int n = 0; n < arrayOfInt.length; n++)
                            paramDataOutput.writeInt(arrayOfInt[n]);
                    }
                    i += this.mLengthSize + 4 * arrayOfInt.length;
                }
                if (paramDataOutput != null)
                    paramDataOutput.writeInt(paramList.size());
                (0 + 4);
                i = 4 + writeOffsets(paramDataOutput, paramList);
                for (int j = 0; j < paramList.size(); j++)
                {
                    long[] arrayOfLong = (long[])paramList.get(j);
                    if (paramDataOutput != null)
                    {
                        DensyIndexFile.writeAccordingToSize(paramDataOutput, this.mLengthSize, arrayOfLong.length);
                        for (int k = 0; k < arrayOfLong.length; k++)
                            paramDataOutput.writeLong(arrayOfLong[k]);
                    }
                    i += this.mLengthSize + 8 * arrayOfLong.length;
                }
            }
        }

        int writeOffsets(DataOutput paramDataOutput, List<Object> paramList)
            throws IOException
        {
            if ((paramDataOutput == null) || (this.mOffsetSize == 0) || (this.mLengthSize == 0))
            {
                int i = 0;
                int j = 4 * paramList.size();
                int k = 0;
                if (k < paramList.size())
                {
                    int i2 = 0;
                    switch (this.mType)
                    {
                    default:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                    }
                    while (true)
                    {
                        if (i < i2)
                            i = i2;
                        k++;
                        break;
                        i2 = ((String)paramList.get(k)).getBytes().length;
                        j += i2;
                        continue;
                        i2 = ((byte[])paramList.get(k)).length;
                        j += i2;
                        continue;
                        i2 = ((short[])paramList.get(k)).length;
                        j += i2 * 2;
                        continue;
                        i2 = ((int[])paramList.get(k)).length;
                        j += i2 * 4;
                        continue;
                        i2 = ((long[])paramList.get(k)).length;
                        j += i2 * 8;
                    }
                }
                this.mLengthSize = DensyIndexFile.getSizeOf(i);
                this.mOffsetSize = DensyIndexFile.getSizeOf(j + this.mLengthSize * paramList.size());
            }
            int m = this.mOffsetSize * paramList.size();
            if (paramDataOutput != null)
            {
                int n = m;
                int i1 = 0;
                if (i1 < paramList.size())
                {
                    DensyIndexFile.writeAccordingToSize(paramDataOutput, this.mOffsetSize, n);
                    switch (this.mType)
                    {
                    default:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                    }
                    while (true)
                    {
                        i1++;
                        break;
                        n += this.mLengthSize + ((String)paramList.get(i1)).getBytes().length;
                        continue;
                        n += this.mLengthSize + ((byte[])paramList.get(i1)).length;
                        continue;
                        n += this.mLengthSize + 2 * ((short[])paramList.get(i1)).length;
                        continue;
                        n += this.mLengthSize + 4 * ((int[])paramList.get(i1)).length;
                        continue;
                        n += this.mLengthSize + 8 * ((long[])paramList.get(i1)).length;
                    }
                }
            }
            return m;
        }
    }

    private static class IndexGroupDescription
    {
        int mMaxIndex;
        int mMinIndex;
        long mOffset;

        IndexGroupDescription(int paramInt1, int paramInt2, long paramLong)
        {
            this.mMinIndex = paramInt1;
            this.mMaxIndex = paramInt2;
            this.mOffset = paramLong;
        }

        static IndexGroupDescription read(DataInput paramDataInput)
            throws IOException
        {
            return new IndexGroupDescription(paramDataInput.readInt(), paramDataInput.readInt(), paramDataInput.readLong());
        }

        int write(DataOutput paramDataOutput)
            throws IOException
        {
            if (paramDataOutput != null)
            {
                paramDataOutput.writeInt(this.mMinIndex);
                paramDataOutput.writeInt(this.mMaxIndex);
                paramDataOutput.writeLong(this.mOffset);
            }
            return 16;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.util.DensyIndexFile
 * JD-Core Version:        0.6.2
 */