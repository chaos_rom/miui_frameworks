package miui.util;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class DecodeGifImageHelper
{
    public static final int MESSAGE_WHAT_DECODE_FRAMES = 1;
    public Handler mDecodeFrameHandler;
    private DecodeGifFrames mDecodeGifFrames;
    public boolean mDecodedAllFrames = false;
    public List<GifFrame> mFrames = new ArrayList();
    public InputStreamLoader mGifSource;
    public long mMaxDecodeSize = 1048576L;
    private int mMaxFrames = 0;
    public int mRealFrameCount = 0;

    private int calcFrameIndex(int paramInt)
    {
        if (this.mRealFrameCount == 0);
        while (true)
        {
            return paramInt;
            paramInt %= this.mRealFrameCount;
        }
    }

    public static GifDecodeResult decode(InputStreamLoader paramInputStreamLoader, long paramLong, int paramInt)
    {
        boolean bool = false;
        GifDecodeResult localGifDecodeResult = new GifDecodeResult();
        localGifDecodeResult.mGifDecoder = null;
        localGifDecodeResult.mIsDecodeOk = false;
        InputStream localInputStream = paramInputStreamLoader.get();
        if (localInputStream != null)
        {
            localGifDecodeResult.mGifDecoder = new GifDecoder();
            GifDecoder localGifDecoder = localGifDecodeResult.mGifDecoder;
            localGifDecoder.setStartFrame(paramInt);
            localGifDecoder.setMaxDecodeSize(paramLong);
            if (localGifDecoder.read(localInputStream) == 0)
                bool = true;
            localGifDecodeResult.mIsDecodeOk = bool;
        }
        paramInputStreamLoader.close();
        return localGifDecodeResult;
    }

    private int getLastFrameIndex()
    {
        return ((GifFrame)this.mFrames.get(-1 + this.mFrames.size())).mIndex;
    }

    public GifDecodeResult decodeFrom(int paramInt)
    {
        return decode(this.mGifSource, this.mMaxDecodeSize, paramInt);
    }

    public void decodeNextFrames()
    {
        int i = this.mFrames.size();
        if (this.mMaxFrames <= 3)
        {
            if (i <= 2);
            for (j = 1; ; j = 0)
            {
                if (j != 0)
                {
                    int k = calcFrameIndex(1 + getLastFrameIndex());
                    this.mDecodeGifFrames.decode(k);
                }
                return;
            }
        }
        if (i <= this.mMaxFrames / 2);
        for (int j = 1; ; j = 0)
            break;
    }

    public void destroy()
    {
        if (this.mDecodeGifFrames != null)
            this.mDecodeGifFrames.destroy();
    }

    public void firstDecodeNextFrames()
    {
        this.mDecodeFrameHandler = new Handler(Looper.getMainLooper())
        {
            public void handleMessage(Message paramAnonymousMessage)
            {
                switch (paramAnonymousMessage.what)
                {
                default:
                case 1:
                }
                while (true)
                {
                    return;
                    if (DecodeGifImageHelper.this.handleDecodeFramesResult(DecodeGifImageHelper.this.mDecodeGifFrames.getAndClearDecodeResult()))
                        DecodeGifImageHelper.this.decodeNextFrames();
                }
            }
        };
        this.mDecodeGifFrames = DecodeGifFrames.createInstance(this.mGifSource, this.mMaxDecodeSize, this.mDecodeFrameHandler);
        this.mMaxFrames = this.mFrames.size();
        decodeNextFrames();
    }

    public boolean handleDecodeFramesResult(GifDecodeResult paramGifDecodeResult)
    {
        boolean bool = true;
        if ((!paramGifDecodeResult.mIsDecodeOk) || (paramGifDecodeResult.mGifDecoder == null))
            bool = false;
        while (true)
        {
            return bool;
            GifDecoder localGifDecoder = paramGifDecodeResult.mGifDecoder;
            Object[] arrayOfObject = new Object[4];
            arrayOfObject[0] = Long.valueOf(Thread.currentThread().getId());
            arrayOfObject[bool] = Integer.valueOf(paramGifDecodeResult.mGifDecoder.getFrameCount());
            arrayOfObject[2] = Boolean.valueOf(paramGifDecodeResult.mIsDecodeOk);
            arrayOfObject[3] = Integer.valueOf(this.mRealFrameCount);
            Log.d("dumpFrameIndex", String.format("Thread#%d: decoded %d frames [%s] [%d]", arrayOfObject));
            if (localGifDecoder.isDecodeToTheEnd())
                this.mRealFrameCount = localGifDecoder.getRealFrameCount();
            int i = localGifDecoder.getFrameCount();
            if (i > 0)
            {
                int j = getLastFrameIndex();
                for (int k = 0; k < i; k++)
                {
                    Bitmap localBitmap = localGifDecoder.getFrame(k);
                    int m = localGifDecoder.getDelay(k);
                    int n = calcFrameIndex(k + (j + 1));
                    this.mFrames.add(new GifFrame(localBitmap, m, n));
                }
            }
        }
    }

    public static class GifFrame
    {
        public int mDuration;
        public Bitmap mImage;
        public int mIndex;

        public GifFrame(Bitmap paramBitmap, int paramInt1, int paramInt2)
        {
            this.mImage = paramBitmap;
            this.mDuration = paramInt1;
            this.mIndex = paramInt2;
        }
    }

    public static class GifDecodeResult
    {
        public GifDecoder mGifDecoder;
        public boolean mIsDecodeOk;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.util.DecodeGifImageHelper
 * JD-Core Version:        0.6.2
 */