package miui.util;

public class DisplayUtils
{
    private static final int[] DENSITIES = arrayOfInt;
    public static final int DENSITY_NONE = 1;

    static
    {
        int[] arrayOfInt = new int[6];
        arrayOfInt[0] = 320;
        arrayOfInt[1] = 240;
        arrayOfInt[2] = 160;
        arrayOfInt[3] = 120;
        arrayOfInt[4] = 1;
        arrayOfInt[5] = 0;
    }

    public static int[] getBestDensityOrder(int paramInt)
    {
        int i = 1;
        int[] arrayOfInt;
        for (int j = 0; ; j++)
            if (j < DENSITIES.length)
            {
                if (paramInt == DENSITIES[j])
                    i = 0;
            }
            else
            {
                arrayOfInt = new int[i + DENSITIES.length];
                arrayOfInt[0] = paramInt;
                int k = i;
                int m = 1;
                while (k < DENSITIES.length)
                {
                    if (paramInt != DENSITIES[k])
                    {
                        int n = m + 1;
                        arrayOfInt[m] = DENSITIES[k];
                        m = n;
                    }
                    k++;
                }
            }
        return arrayOfInt;
    }

    public static String getDensityName(int paramInt)
    {
        int i;
        int j;
        String str;
        switch (paramInt)
        {
        default:
            i = -1 + DENSITIES.length;
            j = i - 1;
        case 120:
            while (j > 0)
            {
                if (Math.abs(DENSITIES[j] - paramInt) < Math.abs(DENSITIES[i] - paramInt))
                    i = j;
                j--;
                continue;
                str = "ldpi";
            }
        case 160:
        case 240:
        case 320:
        case 1:
        case 0:
        }
        while (true)
        {
            return str;
            str = "mdpi";
            continue;
            str = "hdpi";
            continue;
            str = "xhdpi";
            continue;
            str = "nodpi";
            continue;
            str = "";
            continue;
            str = getDensityName(DENSITIES[i]);
        }
    }

    public static String getDensitySuffix(int paramInt)
    {
        String str = getDensityName(paramInt);
        if (!str.equals(""))
            str = "-" + str;
        return str;
    }

    public static String getDrawbleDensityFolder(int paramInt)
    {
        return "res/" + getDrawbleDensityName(paramInt) + "/";
    }

    public static String getDrawbleDensityName(int paramInt)
    {
        return "drawable" + getDensitySuffix(paramInt);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.util.DisplayUtils
 * JD-Core Version:        0.6.2
 */