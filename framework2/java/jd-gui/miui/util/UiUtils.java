package miui.util;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;

public class UiUtils
{
    static SimplePool.PoolInstance<TypedValue> sTypedValuePool = SimplePool.newInsance(new SimplePool.Manager()
    {
        public TypedValue createInstance()
        {
            return new TypedValue();
        }
    }
    , 4);

    public static boolean getBoolean(Context paramContext, int paramInt)
    {
        boolean bool = true;
        TypedValue localTypedValue = (TypedValue)sTypedValuePool.acquire();
        paramContext.getTheme().resolveAttribute(paramInt, localTypedValue, bool);
        if (localTypedValue.data != 0);
        while (true)
        {
            sTypedValuePool.release(localTypedValue);
            return bool;
            bool = false;
        }
    }

    public static int getColor(Context paramContext, int paramInt)
    {
        return paramContext.getResources().getColor(resolveAttribute(paramContext, paramInt));
    }

    public static Drawable getDrawable(Context paramContext, int paramInt)
    {
        int i = resolveAttribute(paramContext, paramInt);
        if (i > 0);
        for (Drawable localDrawable = paramContext.getResources().getDrawable(i); ; localDrawable = null)
            return localDrawable;
    }

    public static int resolveAttribute(Context paramContext, int paramInt)
    {
        TypedValue localTypedValue = (TypedValue)sTypedValuePool.acquire();
        int i = -1;
        if (paramContext.getTheme().resolveAttribute(paramInt, localTypedValue, true))
            i = localTypedValue.resourceId;
        sTypedValuePool.release(localTypedValue);
        return i;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.util.UiUtils
 * JD-Core Version:        0.6.2
 */