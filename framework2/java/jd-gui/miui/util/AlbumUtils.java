package miui.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.Uri;

public class AlbumUtils
{
    public static Bitmap clipRoundCornerBitmap(Bitmap paramBitmap, float paramFloat, int paramInt)
    {
        Bitmap localBitmap;
        if (paramBitmap == null)
            localBitmap = null;
        while (true)
        {
            return localBitmap;
            localBitmap = Bitmap.createBitmap(paramBitmap.getWidth(), paramBitmap.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas localCanvas = new Canvas(localBitmap);
            Paint localPaint = new Paint();
            Rect localRect = new Rect(0, 0, paramBitmap.getWidth(), paramBitmap.getHeight());
            RectF localRectF = new RectF(localRect);
            localPaint.setColor(paramInt);
            localPaint.setFlags(7);
            localCanvas.drawARGB(0, 0, 0, 0);
            localCanvas.drawRoundRect(localRectF, paramFloat, paramFloat, localPaint);
            localPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
            localCanvas.drawBitmap(paramBitmap, localRect, localRect, localPaint);
        }
    }

    public static Bitmap loadAlbum(Context paramContext, Uri paramUri, String paramString, int paramInt1, int paramInt2)
    {
        Bitmap localBitmap = null;
        if (paramUri != null)
            localBitmap = ImageUtils.getBitmap(new InputStreamLoader(paramContext, paramUri), paramInt1, paramInt2);
        if ((localBitmap == null) && (paramString != null))
            localBitmap = ImageUtils.getBitmap(new InputStreamLoader(paramString), paramInt1, paramInt2);
        return localBitmap;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.util.AlbumUtils
 * JD-Core Version:        0.6.2
 */