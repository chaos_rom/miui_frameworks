package miui.util;

import android.content.res.Resources;
import android.text.TextUtils;
import android.text.format.Time;
import java.util.Calendar;
import java.util.GregorianCalendar;
import miui.os.Build;

public class LunarDate
{
    public static final int MAX_LUNAR_YEAR = 2050;
    public static final int MIN_LUNAR_YEAR = 1900;
    private static final char[] iSolarLunarOffsetTable;
    private static final long[] luYearData;
    private static int[] lunarHolidays;
    private static int[] lunarHolidaysTable;
    private static int[] solarHolidays;
    private static int[] solarHolidaysTable;
    private static int[] solarHolidaysTable_TW;
    private static int[] solarHolidays_TW;
    private static int[] solarTerms;
    private static char[] solarTermsTable = arrayOfChar2;

    static
    {
        long[] arrayOfLong = new long[''];
        arrayOfLong[0] = 19416L;
        arrayOfLong[1] = 19168L;
        arrayOfLong[2] = 42352L;
        arrayOfLong[3] = 21717L;
        arrayOfLong[4] = 53856L;
        arrayOfLong[5] = 55632L;
        arrayOfLong[6] = 91476L;
        arrayOfLong[7] = 22176L;
        arrayOfLong[8] = 39632L;
        arrayOfLong[9] = 21970L;
        arrayOfLong[10] = 19168L;
        arrayOfLong[11] = 42422L;
        arrayOfLong[12] = 42192L;
        arrayOfLong[13] = 53840L;
        arrayOfLong[14] = 119381L;
        arrayOfLong[15] = 46400L;
        arrayOfLong[16] = 54944L;
        arrayOfLong[17] = 44450L;
        arrayOfLong[18] = 38320L;
        arrayOfLong[19] = 84343L;
        arrayOfLong[20] = 18800L;
        arrayOfLong[21] = 42160L;
        arrayOfLong[22] = 46261L;
        arrayOfLong[23] = 27216L;
        arrayOfLong[24] = 27968L;
        arrayOfLong[25] = 109396L;
        arrayOfLong[26] = 11104L;
        arrayOfLong[27] = 38256L;
        arrayOfLong[28] = 21234L;
        arrayOfLong[29] = 18800L;
        arrayOfLong[30] = 25958L;
        arrayOfLong[31] = 54432L;
        arrayOfLong[32] = 59984L;
        arrayOfLong[33] = 28309L;
        arrayOfLong[34] = 23248L;
        arrayOfLong[35] = 11104L;
        arrayOfLong[36] = 100067L;
        arrayOfLong[37] = 37600L;
        arrayOfLong[38] = 116951L;
        arrayOfLong[39] = 51536L;
        arrayOfLong[40] = 54432L;
        arrayOfLong[41] = 120998L;
        arrayOfLong[42] = 46416L;
        arrayOfLong[43] = 22176L;
        arrayOfLong[44] = 107956L;
        arrayOfLong[45] = 9680L;
        arrayOfLong[46] = 37584L;
        arrayOfLong[47] = 53938L;
        arrayOfLong[48] = 43344L;
        arrayOfLong[49] = 46423L;
        arrayOfLong[50] = 27808L;
        arrayOfLong[51] = 46416L;
        arrayOfLong[52] = 86869L;
        arrayOfLong[53] = 19872L;
        arrayOfLong[54] = 42448L;
        arrayOfLong[55] = 83315L;
        arrayOfLong[56] = 21200L;
        arrayOfLong[57] = 43432L;
        arrayOfLong[58] = 59728L;
        arrayOfLong[59] = 27296L;
        arrayOfLong[60] = 44710L;
        arrayOfLong[61] = 43856L;
        arrayOfLong[62] = 19296L;
        arrayOfLong[63] = 43748L;
        arrayOfLong[64] = 42352L;
        arrayOfLong[65] = 21088L;
        arrayOfLong[66] = 62051L;
        arrayOfLong[67] = 55632L;
        arrayOfLong[68] = 23383L;
        arrayOfLong[69] = 22176L;
        arrayOfLong[70] = 38608L;
        arrayOfLong[71] = 19925L;
        arrayOfLong[72] = 19152L;
        arrayOfLong[73] = 42192L;
        arrayOfLong[74] = 54484L;
        arrayOfLong[75] = 53840L;
        arrayOfLong[76] = 54616L;
        arrayOfLong[77] = 46400L;
        arrayOfLong[78] = 46496L;
        arrayOfLong[79] = 103846L;
        arrayOfLong[80] = 38320L;
        arrayOfLong[81] = 18864L;
        arrayOfLong[82] = 43380L;
        arrayOfLong[83] = 42160L;
        arrayOfLong[84] = 45690L;
        arrayOfLong[85] = 27216L;
        arrayOfLong[86] = 27968L;
        arrayOfLong[87] = 44870L;
        arrayOfLong[88] = 43872L;
        arrayOfLong[89] = 38256L;
        arrayOfLong[90] = 19189L;
        arrayOfLong[91] = 18800L;
        arrayOfLong[92] = 25776L;
        arrayOfLong[93] = 29859L;
        arrayOfLong[94] = 59984L;
        arrayOfLong[95] = 27480L;
        arrayOfLong[96] = 21952L;
        arrayOfLong[97] = 43872L;
        arrayOfLong[98] = 38613L;
        arrayOfLong[99] = 37600L;
        arrayOfLong[100] = 51552L;
        arrayOfLong[101] = 55636L;
        arrayOfLong[102] = 54432L;
        arrayOfLong[103] = 55888L;
        arrayOfLong[104] = 30034L;
        arrayOfLong[105] = 22176L;
        arrayOfLong[106] = 43959L;
        arrayOfLong[107] = 9680L;
        arrayOfLong[108] = 37584L;
        arrayOfLong[109] = 51893L;
        arrayOfLong[110] = 43344L;
        arrayOfLong[111] = 46240L;
        arrayOfLong[112] = 47780L;
        arrayOfLong[113] = 44368L;
        arrayOfLong[114] = 21977L;
        arrayOfLong[115] = 19360L;
        arrayOfLong[116] = 42416L;
        arrayOfLong[117] = 86390L;
        arrayOfLong[118] = 21168L;
        arrayOfLong[119] = 43312L;
        arrayOfLong[120] = 31060L;
        arrayOfLong[121] = 27296L;
        arrayOfLong[122] = 44368L;
        arrayOfLong[123] = 23378L;
        arrayOfLong[124] = 19296L;
        arrayOfLong[125] = 42726L;
        arrayOfLong[126] = 42208L;
        arrayOfLong[127] = 53856L;
        arrayOfLong[''] = 60005L;
        arrayOfLong[''] = 54576L;
        arrayOfLong[''] = 23200L;
        arrayOfLong[''] = 30371L;
        arrayOfLong[''] = 38608L;
        arrayOfLong[''] = 19415L;
        arrayOfLong[''] = 19152L;
        arrayOfLong[''] = 42192L;
        arrayOfLong[''] = 118966L;
        arrayOfLong[''] = 53840L;
        arrayOfLong[''] = 54560L;
        arrayOfLong[''] = 56645L;
        arrayOfLong[''] = 46496L;
        arrayOfLong[''] = 22224L;
        arrayOfLong[''] = 21938L;
        arrayOfLong[''] = 18864L;
        arrayOfLong[''] = 42359L;
        arrayOfLong[''] = 42160L;
        arrayOfLong[''] = 43600L;
        arrayOfLong[''] = 111189L;
        arrayOfLong[''] = 27936L;
        arrayOfLong[''] = 44448L;
        luYearData = arrayOfLong;
        char[] arrayOfChar1 = new char[''];
        arrayOfChar1[0] = 49;
        arrayOfChar1[1] = 38;
        arrayOfChar1[2] = 28;
        arrayOfChar1[3] = 46;
        arrayOfChar1[4] = 34;
        arrayOfChar1[5] = 24;
        arrayOfChar1[6] = 43;
        arrayOfChar1[7] = 32;
        arrayOfChar1[8] = 21;
        arrayOfChar1[9] = 40;
        arrayOfChar1[10] = 29;
        arrayOfChar1[11] = 48;
        arrayOfChar1[12] = 36;
        arrayOfChar1[13] = 25;
        arrayOfChar1[14] = 44;
        arrayOfChar1[15] = 33;
        arrayOfChar1[16] = 22;
        arrayOfChar1[17] = 41;
        arrayOfChar1[18] = 31;
        arrayOfChar1[19] = 50;
        arrayOfChar1[20] = 38;
        arrayOfChar1[21] = 27;
        arrayOfChar1[22] = 46;
        arrayOfChar1[23] = 35;
        arrayOfChar1[24] = 23;
        arrayOfChar1[25] = 43;
        arrayOfChar1[26] = 32;
        arrayOfChar1[27] = 22;
        arrayOfChar1[28] = 40;
        arrayOfChar1[29] = 29;
        arrayOfChar1[30] = 47;
        arrayOfChar1[31] = 36;
        arrayOfChar1[32] = 25;
        arrayOfChar1[33] = 44;
        arrayOfChar1[34] = 34;
        arrayOfChar1[35] = 23;
        arrayOfChar1[36] = 41;
        arrayOfChar1[37] = 30;
        arrayOfChar1[38] = 49;
        arrayOfChar1[39] = 38;
        arrayOfChar1[40] = 26;
        arrayOfChar1[41] = 45;
        arrayOfChar1[42] = 35;
        arrayOfChar1[43] = 24;
        arrayOfChar1[44] = 43;
        arrayOfChar1[45] = 32;
        arrayOfChar1[46] = 21;
        arrayOfChar1[47] = 40;
        arrayOfChar1[48] = 28;
        arrayOfChar1[49] = 47;
        arrayOfChar1[50] = 36;
        arrayOfChar1[51] = 26;
        arrayOfChar1[52] = 44;
        arrayOfChar1[53] = 33;
        arrayOfChar1[54] = 23;
        arrayOfChar1[55] = 42;
        arrayOfChar1[56] = 30;
        arrayOfChar1[57] = 48;
        arrayOfChar1[58] = 38;
        arrayOfChar1[59] = 27;
        arrayOfChar1[60] = 45;
        arrayOfChar1[61] = 35;
        arrayOfChar1[62] = 24;
        arrayOfChar1[63] = 43;
        arrayOfChar1[64] = 32;
        arrayOfChar1[65] = 20;
        arrayOfChar1[66] = 39;
        arrayOfChar1[67] = 29;
        arrayOfChar1[68] = 47;
        arrayOfChar1[69] = 36;
        arrayOfChar1[70] = 26;
        arrayOfChar1[71] = 45;
        arrayOfChar1[72] = 33;
        arrayOfChar1[73] = 22;
        arrayOfChar1[74] = 41;
        arrayOfChar1[75] = 30;
        arrayOfChar1[76] = 48;
        arrayOfChar1[77] = 37;
        arrayOfChar1[78] = 27;
        arrayOfChar1[79] = 46;
        arrayOfChar1[80] = 35;
        arrayOfChar1[81] = 24;
        arrayOfChar1[82] = 43;
        arrayOfChar1[83] = 32;
        arrayOfChar1[84] = 50;
        arrayOfChar1[85] = 39;
        arrayOfChar1[86] = 28;
        arrayOfChar1[87] = 47;
        arrayOfChar1[88] = 36;
        arrayOfChar1[89] = 26;
        arrayOfChar1[90] = 45;
        arrayOfChar1[91] = 34;
        arrayOfChar1[92] = 22;
        arrayOfChar1[93] = 40;
        arrayOfChar1[94] = 30;
        arrayOfChar1[95] = 49;
        arrayOfChar1[96] = 37;
        arrayOfChar1[97] = 27;
        arrayOfChar1[98] = 46;
        arrayOfChar1[99] = 35;
        arrayOfChar1[100] = 23;
        arrayOfChar1[101] = 42;
        arrayOfChar1[102] = 31;
        arrayOfChar1[103] = 21;
        arrayOfChar1[104] = 39;
        arrayOfChar1[105] = 28;
        arrayOfChar1[106] = 48;
        arrayOfChar1[107] = 37;
        arrayOfChar1[108] = 25;
        arrayOfChar1[109] = 44;
        arrayOfChar1[110] = 33;
        arrayOfChar1[111] = 22;
        arrayOfChar1[112] = 40;
        arrayOfChar1[113] = 30;
        arrayOfChar1[114] = 49;
        arrayOfChar1[115] = 38;
        arrayOfChar1[116] = 27;
        arrayOfChar1[117] = 46;
        arrayOfChar1[118] = 35;
        arrayOfChar1[119] = 24;
        arrayOfChar1[120] = 42;
        arrayOfChar1[121] = 31;
        arrayOfChar1[122] = 21;
        arrayOfChar1[123] = 40;
        arrayOfChar1[124] = 28;
        arrayOfChar1[125] = 47;
        arrayOfChar1[126] = 36;
        arrayOfChar1[127] = 25;
        arrayOfChar1[''] = 43;
        arrayOfChar1[''] = 33;
        arrayOfChar1[''] = 22;
        arrayOfChar1[''] = 41;
        arrayOfChar1[''] = 30;
        arrayOfChar1[''] = 49;
        arrayOfChar1[''] = 38;
        arrayOfChar1[''] = 27;
        arrayOfChar1[''] = 45;
        arrayOfChar1[''] = 34;
        arrayOfChar1[''] = 23;
        arrayOfChar1[''] = 42;
        arrayOfChar1[''] = 31;
        arrayOfChar1[''] = 21;
        arrayOfChar1[''] = 40;
        arrayOfChar1[''] = 29;
        arrayOfChar1[''] = 47;
        arrayOfChar1[''] = 36;
        arrayOfChar1[''] = 25;
        arrayOfChar1[''] = 44;
        arrayOfChar1[''] = 32;
        arrayOfChar1[''] = 22;
        iSolarLunarOffsetTable = arrayOfChar1;
        int[] arrayOfInt1 = new int[7];
        arrayOfInt1[0] = 101;
        arrayOfInt1[1] = 115;
        arrayOfInt1[2] = 505;
        arrayOfInt1[3] = 707;
        arrayOfInt1[4] = 815;
        arrayOfInt1[5] = 909;
        arrayOfInt1[6] = 1208;
        lunarHolidaysTable = arrayOfInt1;
        int[] arrayOfInt2 = new int[13];
        arrayOfInt2[0] = 101;
        arrayOfInt2[1] = 214;
        arrayOfInt2[2] = 308;
        arrayOfInt2[3] = 312;
        arrayOfInt2[4] = 401;
        arrayOfInt2[5] = 501;
        arrayOfInt2[6] = 504;
        arrayOfInt2[7] = 601;
        arrayOfInt2[8] = 701;
        arrayOfInt2[9] = 801;
        arrayOfInt2[10] = 910;
        arrayOfInt2[11] = 1001;
        arrayOfInt2[12] = 1225;
        solarHolidaysTable = arrayOfInt2;
        int[] arrayOfInt3 = new int[10];
        arrayOfInt3[0] = 101;
        arrayOfInt3[1] = 214;
        arrayOfInt3[2] = 228;
        arrayOfInt3[3] = 308;
        arrayOfInt3[4] = 312;
        arrayOfInt3[5] = 501;
        arrayOfInt3[6] = 928;
        arrayOfInt3[7] = 1010;
        arrayOfInt3[8] = 1112;
        arrayOfInt3[9] = 1225;
        solarHolidaysTable_TW = arrayOfInt3;
        int[] arrayOfInt4 = new int[7];
        arrayOfInt4[0] = 101449816;
        arrayOfInt4[1] = 101449817;
        arrayOfInt4[2] = 101449818;
        arrayOfInt4[3] = 101449819;
        arrayOfInt4[4] = 101449820;
        arrayOfInt4[5] = 101449821;
        arrayOfInt4[6] = 101449822;
        lunarHolidays = arrayOfInt4;
        int[] arrayOfInt5 = new int[13];
        arrayOfInt5[0] = 101449823;
        arrayOfInt5[1] = 101449824;
        arrayOfInt5[2] = 101449825;
        arrayOfInt5[3] = 101449826;
        arrayOfInt5[4] = 101449827;
        arrayOfInt5[5] = 101449828;
        arrayOfInt5[6] = 101449829;
        arrayOfInt5[7] = 101449830;
        arrayOfInt5[8] = 101449831;
        arrayOfInt5[9] = 101449832;
        arrayOfInt5[10] = 101449833;
        arrayOfInt5[11] = 101449834;
        arrayOfInt5[12] = 101449835;
        solarHolidays = arrayOfInt5;
        int[] arrayOfInt6 = new int[10];
        arrayOfInt6[0] = 101449823;
        arrayOfInt6[1] = 101449824;
        arrayOfInt6[2] = 101450305;
        arrayOfInt6[3] = 101449825;
        arrayOfInt6[4] = 101449826;
        arrayOfInt6[5] = 101449828;
        arrayOfInt6[6] = 101449833;
        arrayOfInt6[7] = 101449834;
        arrayOfInt6[8] = 101450306;
        arrayOfInt6[9] = 101449835;
        solarHolidays_TW = arrayOfInt6;
        int[] arrayOfInt7 = new int[24];
        arrayOfInt7[0] = 101449836;
        arrayOfInt7[1] = 101449837;
        arrayOfInt7[2] = 101449838;
        arrayOfInt7[3] = 101449839;
        arrayOfInt7[4] = 101449840;
        arrayOfInt7[5] = 101449841;
        arrayOfInt7[6] = 101449842;
        arrayOfInt7[7] = 101449843;
        arrayOfInt7[8] = 101449844;
        arrayOfInt7[9] = 101449845;
        arrayOfInt7[10] = 101449846;
        arrayOfInt7[11] = 101449847;
        arrayOfInt7[12] = 101449848;
        arrayOfInt7[13] = 101449849;
        arrayOfInt7[14] = 101449850;
        arrayOfInt7[15] = 101449851;
        arrayOfInt7[16] = 101449852;
        arrayOfInt7[17] = 101449853;
        arrayOfInt7[18] = 101449854;
        arrayOfInt7[19] = 101449855;
        arrayOfInt7[20] = 101449856;
        arrayOfInt7[21] = 101449857;
        arrayOfInt7[22] = 101449858;
        arrayOfInt7[23] = 101449859;
        solarTerms = arrayOfInt7;
        char[] arrayOfChar2 = new char[1800];
        arrayOfChar2[0] = 150;
        arrayOfChar2[1] = 180;
        arrayOfChar2[2] = 150;
        arrayOfChar2[3] = 166;
        arrayOfChar2[4] = 151;
        arrayOfChar2[5] = 151;
        arrayOfChar2[6] = 120;
        arrayOfChar2[7] = 121;
        arrayOfChar2[8] = 121;
        arrayOfChar2[9] = 105;
        arrayOfChar2[10] = 120;
        arrayOfChar2[11] = 119;
        arrayOfChar2[12] = 150;
        arrayOfChar2[13] = 164;
        arrayOfChar2[14] = 150;
        arrayOfChar2[15] = 150;
        arrayOfChar2[16] = 151;
        arrayOfChar2[17] = 135;
        arrayOfChar2[18] = 121;
        arrayOfChar2[19] = 121;
        arrayOfChar2[20] = 121;
        arrayOfChar2[21] = 105;
        arrayOfChar2[22] = 120;
        arrayOfChar2[23] = 120;
        arrayOfChar2[24] = 150;
        arrayOfChar2[25] = 165;
        arrayOfChar2[26] = 135;
        arrayOfChar2[27] = 150;
        arrayOfChar2[28] = 135;
        arrayOfChar2[29] = 135;
        arrayOfChar2[30] = 121;
        arrayOfChar2[31] = 105;
        arrayOfChar2[32] = 105;
        arrayOfChar2[33] = 105;
        arrayOfChar2[34] = 120;
        arrayOfChar2[35] = 120;
        arrayOfChar2[36] = 134;
        arrayOfChar2[37] = 165;
        arrayOfChar2[38] = 150;
        arrayOfChar2[39] = 165;
        arrayOfChar2[40] = 150;
        arrayOfChar2[41] = 151;
        arrayOfChar2[42] = 136;
        arrayOfChar2[43] = 120;
        arrayOfChar2[44] = 120;
        arrayOfChar2[45] = 121;
        arrayOfChar2[46] = 120;
        arrayOfChar2[47] = 135;
        arrayOfChar2[48] = 150;
        arrayOfChar2[49] = 180;
        arrayOfChar2[50] = 150;
        arrayOfChar2[51] = 166;
        arrayOfChar2[52] = 151;
        arrayOfChar2[53] = 151;
        arrayOfChar2[54] = 120;
        arrayOfChar2[55] = 121;
        arrayOfChar2[56] = 121;
        arrayOfChar2[57] = 105;
        arrayOfChar2[58] = 120;
        arrayOfChar2[59] = 119;
        arrayOfChar2[60] = 150;
        arrayOfChar2[61] = 164;
        arrayOfChar2[62] = 150;
        arrayOfChar2[63] = 150;
        arrayOfChar2[64] = 151;
        arrayOfChar2[65] = 151;
        arrayOfChar2[66] = 121;
        arrayOfChar2[67] = 121;
        arrayOfChar2[68] = 121;
        arrayOfChar2[69] = 105;
        arrayOfChar2[70] = 120;
        arrayOfChar2[71] = 120;
        arrayOfChar2[72] = 150;
        arrayOfChar2[73] = 165;
        arrayOfChar2[74] = 135;
        arrayOfChar2[75] = 150;
        arrayOfChar2[76] = 135;
        arrayOfChar2[77] = 135;
        arrayOfChar2[78] = 121;
        arrayOfChar2[79] = 105;
        arrayOfChar2[80] = 105;
        arrayOfChar2[81] = 105;
        arrayOfChar2[82] = 120;
        arrayOfChar2[83] = 120;
        arrayOfChar2[84] = 134;
        arrayOfChar2[85] = 165;
        arrayOfChar2[86] = 150;
        arrayOfChar2[87] = 165;
        arrayOfChar2[88] = 150;
        arrayOfChar2[89] = 151;
        arrayOfChar2[90] = 136;
        arrayOfChar2[91] = 120;
        arrayOfChar2[92] = 120;
        arrayOfChar2[93] = 105;
        arrayOfChar2[94] = 120;
        arrayOfChar2[95] = 135;
        arrayOfChar2[96] = 150;
        arrayOfChar2[97] = 180;
        arrayOfChar2[98] = 150;
        arrayOfChar2[99] = 166;
        arrayOfChar2[100] = 151;
        arrayOfChar2[101] = 151;
        arrayOfChar2[102] = 120;
        arrayOfChar2[103] = 121;
        arrayOfChar2[104] = 121;
        arrayOfChar2[105] = 105;
        arrayOfChar2[106] = 120;
        arrayOfChar2[107] = 119;
        arrayOfChar2[108] = 150;
        arrayOfChar2[109] = 164;
        arrayOfChar2[110] = 150;
        arrayOfChar2[111] = 150;
        arrayOfChar2[112] = 151;
        arrayOfChar2[113] = 151;
        arrayOfChar2[114] = 121;
        arrayOfChar2[115] = 121;
        arrayOfChar2[116] = 121;
        arrayOfChar2[117] = 105;
        arrayOfChar2[118] = 120;
        arrayOfChar2[119] = 120;
        arrayOfChar2[120] = 150;
        arrayOfChar2[121] = 165;
        arrayOfChar2[122] = 135;
        arrayOfChar2[123] = 150;
        arrayOfChar2[124] = 135;
        arrayOfChar2[125] = 135;
        arrayOfChar2[126] = 121;
        arrayOfChar2[127] = 105;
        arrayOfChar2[''] = 105;
        arrayOfChar2[''] = 105;
        arrayOfChar2[''] = 120;
        arrayOfChar2[''] = 120;
        arrayOfChar2[''] = 134;
        arrayOfChar2[''] = 165;
        arrayOfChar2[''] = 150;
        arrayOfChar2[''] = 165;
        arrayOfChar2[''] = 150;
        arrayOfChar2[''] = 151;
        arrayOfChar2[''] = 136;
        arrayOfChar2[''] = 120;
        arrayOfChar2[''] = 120;
        arrayOfChar2[''] = 105;
        arrayOfChar2[''] = 120;
        arrayOfChar2[''] = 135;
        arrayOfChar2[''] = 149;
        arrayOfChar2[''] = 180;
        arrayOfChar2[''] = 150;
        arrayOfChar2[''] = 166;
        arrayOfChar2[''] = 151;
        arrayOfChar2[''] = 151;
        arrayOfChar2[''] = 120;
        arrayOfChar2[''] = 121;
        arrayOfChar2[''] = 121;
        arrayOfChar2[''] = 105;
        arrayOfChar2[''] = 120;
        arrayOfChar2[''] = 119;
        arrayOfChar2[''] = 150;
        arrayOfChar2[''] = 180;
        arrayOfChar2[''] = 150;
        arrayOfChar2[''] = 166;
        arrayOfChar2[' '] = 151;
        arrayOfChar2['¡'] = 151;
        arrayOfChar2['¢'] = 121;
        arrayOfChar2['£'] = 121;
        arrayOfChar2['¤'] = 121;
        arrayOfChar2['¥'] = 105;
        arrayOfChar2['¦'] = 120;
        arrayOfChar2['§'] = 120;
        arrayOfChar2['¨'] = 150;
        arrayOfChar2['©'] = 165;
        arrayOfChar2['ª'] = 151;
        arrayOfChar2['«'] = 150;
        arrayOfChar2['¬'] = 151;
        arrayOfChar2['­'] = 135;
        arrayOfChar2['®'] = 121;
        arrayOfChar2['¯'] = 121;
        arrayOfChar2['°'] = 105;
        arrayOfChar2['±'] = 105;
        arrayOfChar2['²'] = 120;
        arrayOfChar2['³'] = 120;
        arrayOfChar2['´'] = 150;
        arrayOfChar2['µ'] = 165;
        arrayOfChar2['¶'] = 150;
        arrayOfChar2['·'] = 165;
        arrayOfChar2['¸'] = 150;
        arrayOfChar2['¹'] = 151;
        arrayOfChar2['º'] = 136;
        arrayOfChar2['»'] = 120;
        arrayOfChar2['¼'] = 120;
        arrayOfChar2['½'] = 121;
        arrayOfChar2['¾'] = 119;
        arrayOfChar2['¿'] = 135;
        arrayOfChar2['À'] = 149;
        arrayOfChar2['Á'] = 180;
        arrayOfChar2['Â'] = 150;
        arrayOfChar2['Ã'] = 166;
        arrayOfChar2['Ä'] = 150;
        arrayOfChar2['Å'] = 151;
        arrayOfChar2['Æ'] = 120;
        arrayOfChar2['Ç'] = 121;
        arrayOfChar2['È'] = 120;
        arrayOfChar2['É'] = 105;
        arrayOfChar2['Ê'] = 120;
        arrayOfChar2['Ë'] = 135;
        arrayOfChar2['Ì'] = 150;
        arrayOfChar2['Í'] = 180;
        arrayOfChar2['Î'] = 150;
        arrayOfChar2['Ï'] = 166;
        arrayOfChar2['Ð'] = 151;
        arrayOfChar2['Ñ'] = 151;
        arrayOfChar2['Ò'] = 121;
        arrayOfChar2['Ó'] = 121;
        arrayOfChar2['Ô'] = 121;
        arrayOfChar2['Õ'] = 105;
        arrayOfChar2['Ö'] = 120;
        arrayOfChar2['×'] = 119;
        arrayOfChar2['Ø'] = 150;
        arrayOfChar2['Ù'] = 165;
        arrayOfChar2['Ú'] = 151;
        arrayOfChar2['Û'] = 150;
        arrayOfChar2['Ü'] = 151;
        arrayOfChar2['Ý'] = 135;
        arrayOfChar2['Þ'] = 121;
        arrayOfChar2['ß'] = 121;
        arrayOfChar2['à'] = 105;
        arrayOfChar2['á'] = 105;
        arrayOfChar2['â'] = 120;
        arrayOfChar2['ã'] = 120;
        arrayOfChar2['ä'] = 150;
        arrayOfChar2['å'] = 165;
        arrayOfChar2['æ'] = 150;
        arrayOfChar2['ç'] = 165;
        arrayOfChar2['è'] = 150;
        arrayOfChar2['é'] = 151;
        arrayOfChar2['ê'] = 136;
        arrayOfChar2['ë'] = 120;
        arrayOfChar2['ì'] = 120;
        arrayOfChar2['í'] = 121;
        arrayOfChar2['î'] = 119;
        arrayOfChar2['ï'] = 135;
        arrayOfChar2['ð'] = 149;
        arrayOfChar2['ñ'] = 180;
        arrayOfChar2['ò'] = 150;
        arrayOfChar2['ó'] = 165;
        arrayOfChar2['ô'] = 150;
        arrayOfChar2['õ'] = 151;
        arrayOfChar2['ö'] = 120;
        arrayOfChar2['÷'] = 121;
        arrayOfChar2['ø'] = 120;
        arrayOfChar2['ù'] = 105;
        arrayOfChar2['ú'] = 120;
        arrayOfChar2['û'] = 135;
        arrayOfChar2['ü'] = 150;
        arrayOfChar2['ý'] = 180;
        arrayOfChar2['þ'] = 150;
        arrayOfChar2['ÿ'] = 166;
        arrayOfChar2[256] = 151;
        arrayOfChar2[257] = 151;
        arrayOfChar2[258] = 121;
        arrayOfChar2[259] = 121;
        arrayOfChar2[260] = 121;
        arrayOfChar2[261] = 105;
        arrayOfChar2[262] = 120;
        arrayOfChar2[263] = 119;
        arrayOfChar2[264] = 150;
        arrayOfChar2[265] = 164;
        arrayOfChar2[266] = 150;
        arrayOfChar2[267] = 150;
        arrayOfChar2[268] = 151;
        arrayOfChar2[269] = 135;
        arrayOfChar2[270] = 121;
        arrayOfChar2[271] = 121;
        arrayOfChar2[272] = 105;
        arrayOfChar2[273] = 105;
        arrayOfChar2[274] = 120;
        arrayOfChar2[275] = 120;
        arrayOfChar2[276] = 150;
        arrayOfChar2[277] = 165;
        arrayOfChar2[278] = 150;
        arrayOfChar2[279] = 165;
        arrayOfChar2[280] = 150;
        arrayOfChar2[281] = 151;
        arrayOfChar2[282] = 136;
        arrayOfChar2[283] = 120;
        arrayOfChar2[284] = 120;
        arrayOfChar2[285] = 121;
        arrayOfChar2[286] = 119;
        arrayOfChar2[287] = 135;
        arrayOfChar2[288] = 149;
        arrayOfChar2[289] = 180;
        arrayOfChar2[290] = 150;
        arrayOfChar2[291] = 165;
        arrayOfChar2[292] = 150;
        arrayOfChar2[293] = 151;
        arrayOfChar2[294] = 120;
        arrayOfChar2[295] = 121;
        arrayOfChar2[296] = 120;
        arrayOfChar2[297] = 105;
        arrayOfChar2[298] = 120;
        arrayOfChar2[299] = 135;
        arrayOfChar2[300] = 150;
        arrayOfChar2[301] = 180;
        arrayOfChar2[302] = 150;
        arrayOfChar2[303] = 166;
        arrayOfChar2[304] = 151;
        arrayOfChar2[305] = 151;
        arrayOfChar2[306] = 120;
        arrayOfChar2[307] = 121;
        arrayOfChar2[308] = 121;
        arrayOfChar2[309] = 105;
        arrayOfChar2[310] = 120;
        arrayOfChar2[311] = 119;
        arrayOfChar2[312] = 150;
        arrayOfChar2[313] = 164;
        arrayOfChar2[314] = 150;
        arrayOfChar2[315] = 150;
        arrayOfChar2[316] = 151;
        arrayOfChar2[317] = 135;
        arrayOfChar2[318] = 121;
        arrayOfChar2[319] = 121;
        arrayOfChar2[320] = 121;
        arrayOfChar2[321] = 105;
        arrayOfChar2[322] = 120;
        arrayOfChar2[323] = 120;
        arrayOfChar2[324] = 150;
        arrayOfChar2[325] = 165;
        arrayOfChar2[326] = 150;
        arrayOfChar2[327] = 165;
        arrayOfChar2[328] = 150;
        arrayOfChar2[329] = 150;
        arrayOfChar2[330] = 136;
        arrayOfChar2[331] = 120;
        arrayOfChar2[332] = 120;
        arrayOfChar2[333] = 120;
        arrayOfChar2[334] = 135;
        arrayOfChar2[335] = 135;
        arrayOfChar2[336] = 149;
        arrayOfChar2[337] = 180;
        arrayOfChar2[338] = 150;
        arrayOfChar2[339] = 165;
        arrayOfChar2[340] = 150;
        arrayOfChar2[341] = 151;
        arrayOfChar2[342] = 136;
        arrayOfChar2[343] = 120;
        arrayOfChar2[344] = 120;
        arrayOfChar2[345] = 121;
        arrayOfChar2[346] = 119;
        arrayOfChar2[347] = 135;
        arrayOfChar2[348] = 150;
        arrayOfChar2[349] = 180;
        arrayOfChar2[350] = 150;
        arrayOfChar2[351] = 166;
        arrayOfChar2[352] = 151;
        arrayOfChar2[353] = 151;
        arrayOfChar2[354] = 120;
        arrayOfChar2[355] = 121;
        arrayOfChar2[356] = 121;
        arrayOfChar2[357] = 105;
        arrayOfChar2[358] = 120;
        arrayOfChar2[359] = 119;
        arrayOfChar2[360] = 150;
        arrayOfChar2[361] = 164;
        arrayOfChar2[362] = 150;
        arrayOfChar2[363] = 150;
        arrayOfChar2[364] = 151;
        arrayOfChar2[365] = 135;
        arrayOfChar2[366] = 121;
        arrayOfChar2[367] = 121;
        arrayOfChar2[368] = 121;
        arrayOfChar2[369] = 105;
        arrayOfChar2[370] = 120;
        arrayOfChar2[371] = 120;
        arrayOfChar2[372] = 150;
        arrayOfChar2[373] = 165;
        arrayOfChar2[374] = 150;
        arrayOfChar2[375] = 165;
        arrayOfChar2[376] = 150;
        arrayOfChar2[377] = 150;
        arrayOfChar2[378] = 136;
        arrayOfChar2[379] = 120;
        arrayOfChar2[380] = 120;
        arrayOfChar2[381] = 120;
        arrayOfChar2[382] = 135;
        arrayOfChar2[383] = 135;
        arrayOfChar2[384] = 149;
        arrayOfChar2[385] = 180;
        arrayOfChar2[386] = 150;
        arrayOfChar2[387] = 165;
        arrayOfChar2[388] = 150;
        arrayOfChar2[389] = 151;
        arrayOfChar2[390] = 136;
        arrayOfChar2[391] = 120;
        arrayOfChar2[392] = 120;
        arrayOfChar2[393] = 105;
        arrayOfChar2[394] = 120;
        arrayOfChar2[395] = 135;
        arrayOfChar2[396] = 150;
        arrayOfChar2[397] = 180;
        arrayOfChar2[398] = 150;
        arrayOfChar2[399] = 166;
        arrayOfChar2[400] = 151;
        arrayOfChar2[401] = 151;
        arrayOfChar2[402] = 120;
        arrayOfChar2[403] = 121;
        arrayOfChar2[404] = 121;
        arrayOfChar2[405] = 105;
        arrayOfChar2[406] = 120;
        arrayOfChar2[407] = 119;
        arrayOfChar2[408] = 150;
        arrayOfChar2[409] = 164;
        arrayOfChar2[410] = 150;
        arrayOfChar2[411] = 150;
        arrayOfChar2[412] = 151;
        arrayOfChar2[413] = 151;
        arrayOfChar2[414] = 121;
        arrayOfChar2[415] = 121;
        arrayOfChar2[416] = 121;
        arrayOfChar2[417] = 105;
        arrayOfChar2[418] = 120;
        arrayOfChar2[419] = 120;
        arrayOfChar2[420] = 150;
        arrayOfChar2[421] = 165;
        arrayOfChar2[422] = 150;
        arrayOfChar2[423] = 165;
        arrayOfChar2[424] = 150;
        arrayOfChar2[425] = 150;
        arrayOfChar2[426] = 136;
        arrayOfChar2[427] = 120;
        arrayOfChar2[428] = 120;
        arrayOfChar2[429] = 120;
        arrayOfChar2[430] = 135;
        arrayOfChar2[431] = 135;
        arrayOfChar2[432] = 149;
        arrayOfChar2[433] = 180;
        arrayOfChar2[434] = 150;
        arrayOfChar2[435] = 165;
        arrayOfChar2[436] = 150;
        arrayOfChar2[437] = 151;
        arrayOfChar2[438] = 136;
        arrayOfChar2[439] = 120;
        arrayOfChar2[440] = 120;
        arrayOfChar2[441] = 105;
        arrayOfChar2[442] = 120;
        arrayOfChar2[443] = 135;
        arrayOfChar2[444] = 150;
        arrayOfChar2[445] = 180;
        arrayOfChar2[446] = 150;
        arrayOfChar2[447] = 166;
        arrayOfChar2[448] = 151;
        arrayOfChar2[449] = 151;
        arrayOfChar2[450] = 120;
        arrayOfChar2[451] = 121;
        arrayOfChar2[452] = 121;
        arrayOfChar2[453] = 105;
        arrayOfChar2[454] = 120;
        arrayOfChar2[455] = 119;
        arrayOfChar2[456] = 150;
        arrayOfChar2[457] = 164;
        arrayOfChar2[458] = 150;
        arrayOfChar2[459] = 150;
        arrayOfChar2[460] = 151;
        arrayOfChar2[461] = 151;
        arrayOfChar2[462] = 121;
        arrayOfChar2[463] = 121;
        arrayOfChar2[464] = 121;
        arrayOfChar2[465] = 105;
        arrayOfChar2[466] = 120;
        arrayOfChar2[467] = 120;
        arrayOfChar2[468] = 150;
        arrayOfChar2[469] = 165;
        arrayOfChar2[470] = 150;
        arrayOfChar2[471] = 165;
        arrayOfChar2[472] = 150;
        arrayOfChar2[473] = 150;
        arrayOfChar2[474] = 136;
        arrayOfChar2[475] = 120;
        arrayOfChar2[476] = 120;
        arrayOfChar2[477] = 120;
        arrayOfChar2[478] = 135;
        arrayOfChar2[479] = 135;
        arrayOfChar2[480] = 149;
        arrayOfChar2[481] = 180;
        arrayOfChar2[482] = 150;
        arrayOfChar2[483] = 165;
        arrayOfChar2[484] = 150;
        arrayOfChar2[485] = 151;
        arrayOfChar2[486] = 136;
        arrayOfChar2[487] = 120;
        arrayOfChar2[488] = 120;
        arrayOfChar2[489] = 105;
        arrayOfChar2[490] = 120;
        arrayOfChar2[491] = 135;
        arrayOfChar2[492] = 150;
        arrayOfChar2[493] = 180;
        arrayOfChar2[494] = 150;
        arrayOfChar2[495] = 166;
        arrayOfChar2[496] = 151;
        arrayOfChar2[497] = 151;
        arrayOfChar2[498] = 120;
        arrayOfChar2[499] = 121;
        arrayOfChar2[500] = 121;
        arrayOfChar2[501] = 105;
        arrayOfChar2[502] = 120;
        arrayOfChar2[503] = 119;
        arrayOfChar2[504] = 150;
        arrayOfChar2[505] = 164;
        arrayOfChar2[506] = 150;
        arrayOfChar2[507] = 150;
        arrayOfChar2[508] = 151;
        arrayOfChar2[509] = 151;
        arrayOfChar2[510] = 121;
        arrayOfChar2[511] = 121;
        arrayOfChar2[512] = 121;
        arrayOfChar2[513] = 105;
        arrayOfChar2[514] = 120;
        arrayOfChar2[515] = 120;
        arrayOfChar2[516] = 150;
        arrayOfChar2[517] = 165;
        arrayOfChar2[518] = 150;
        arrayOfChar2[519] = 165;
        arrayOfChar2[520] = 166;
        arrayOfChar2[521] = 150;
        arrayOfChar2[522] = 136;
        arrayOfChar2[523] = 120;
        arrayOfChar2[524] = 120;
        arrayOfChar2[525] = 120;
        arrayOfChar2[526] = 135;
        arrayOfChar2[527] = 135;
        arrayOfChar2[528] = 149;
        arrayOfChar2[529] = 180;
        arrayOfChar2[530] = 150;
        arrayOfChar2[531] = 165;
        arrayOfChar2[532] = 150;
        arrayOfChar2[533] = 151;
        arrayOfChar2[534] = 136;
        arrayOfChar2[535] = 120;
        arrayOfChar2[536] = 120;
        arrayOfChar2[537] = 121;
        arrayOfChar2[538] = 119;
        arrayOfChar2[539] = 135;
        arrayOfChar2[540] = 149;
        arrayOfChar2[541] = 180;
        arrayOfChar2[542] = 150;
        arrayOfChar2[543] = 166;
        arrayOfChar2[544] = 151;
        arrayOfChar2[545] = 151;
        arrayOfChar2[546] = 120;
        arrayOfChar2[547] = 121;
        arrayOfChar2[548] = 120;
        arrayOfChar2[549] = 105;
        arrayOfChar2[550] = 120;
        arrayOfChar2[551] = 119;
        arrayOfChar2[552] = 150;
        arrayOfChar2[553] = 180;
        arrayOfChar2[554] = 150;
        arrayOfChar2[555] = 166;
        arrayOfChar2[556] = 151;
        arrayOfChar2[557] = 151;
        arrayOfChar2[558] = 121;
        arrayOfChar2[559] = 121;
        arrayOfChar2[560] = 121;
        arrayOfChar2[561] = 105;
        arrayOfChar2[562] = 120;
        arrayOfChar2[563] = 120;
        arrayOfChar2[564] = 150;
        arrayOfChar2[565] = 165;
        arrayOfChar2[566] = 166;
        arrayOfChar2[567] = 165;
        arrayOfChar2[568] = 166;
        arrayOfChar2[569] = 150;
        arrayOfChar2[570] = 136;
        arrayOfChar2[571] = 136;
        arrayOfChar2[572] = 120;
        arrayOfChar2[573] = 120;
        arrayOfChar2[574] = 135;
        arrayOfChar2[575] = 135;
        arrayOfChar2[576] = 165;
        arrayOfChar2[577] = 180;
        arrayOfChar2[578] = 150;
        arrayOfChar2[579] = 165;
        arrayOfChar2[580] = 150;
        arrayOfChar2[581] = 151;
        arrayOfChar2[582] = 136;
        arrayOfChar2[583] = 121;
        arrayOfChar2[584] = 120;
        arrayOfChar2[585] = 121;
        arrayOfChar2[586] = 119;
        arrayOfChar2[587] = 135;
        arrayOfChar2[588] = 149;
        arrayOfChar2[589] = 180;
        arrayOfChar2[590] = 150;
        arrayOfChar2[591] = 165;
        arrayOfChar2[592] = 150;
        arrayOfChar2[593] = 151;
        arrayOfChar2[594] = 120;
        arrayOfChar2[595] = 121;
        arrayOfChar2[596] = 120;
        arrayOfChar2[597] = 105;
        arrayOfChar2[598] = 120;
        arrayOfChar2[599] = 119;
        arrayOfChar2[600] = 150;
        arrayOfChar2[601] = 180;
        arrayOfChar2[602] = 150;
        arrayOfChar2[603] = 166;
        arrayOfChar2[604] = 151;
        arrayOfChar2[605] = 151;
        arrayOfChar2[606] = 121;
        arrayOfChar2[607] = 121;
        arrayOfChar2[608] = 121;
        arrayOfChar2[609] = 105;
        arrayOfChar2[610] = 120;
        arrayOfChar2[611] = 120;
        arrayOfChar2[612] = 150;
        arrayOfChar2[613] = 165;
        arrayOfChar2[614] = 166;
        arrayOfChar2[615] = 165;
        arrayOfChar2[616] = 166;
        arrayOfChar2[617] = 150;
        arrayOfChar2[618] = 136;
        arrayOfChar2[619] = 136;
        arrayOfChar2[620] = 120;
        arrayOfChar2[621] = 120;
        arrayOfChar2[622] = 135;
        arrayOfChar2[623] = 135;
        arrayOfChar2[624] = 165;
        arrayOfChar2[625] = 180;
        arrayOfChar2[626] = 150;
        arrayOfChar2[627] = 165;
        arrayOfChar2[628] = 150;
        arrayOfChar2[629] = 151;
        arrayOfChar2[630] = 136;
        arrayOfChar2[631] = 120;
        arrayOfChar2[632] = 120;
        arrayOfChar2[633] = 121;
        arrayOfChar2[634] = 119;
        arrayOfChar2[635] = 135;
        arrayOfChar2[636] = 149;
        arrayOfChar2[637] = 180;
        arrayOfChar2[638] = 150;
        arrayOfChar2[639] = 165;
        arrayOfChar2[640] = 150;
        arrayOfChar2[641] = 151;
        arrayOfChar2[642] = 120;
        arrayOfChar2[643] = 121;
        arrayOfChar2[644] = 120;
        arrayOfChar2[645] = 104;
        arrayOfChar2[646] = 120;
        arrayOfChar2[647] = 135;
        arrayOfChar2[648] = 150;
        arrayOfChar2[649] = 180;
        arrayOfChar2[650] = 150;
        arrayOfChar2[651] = 166;
        arrayOfChar2[652] = 151;
        arrayOfChar2[653] = 151;
        arrayOfChar2[654] = 120;
        arrayOfChar2[655] = 121;
        arrayOfChar2[656] = 121;
        arrayOfChar2[657] = 105;
        arrayOfChar2[658] = 120;
        arrayOfChar2[659] = 119;
        arrayOfChar2[660] = 150;
        arrayOfChar2[661] = 165;
        arrayOfChar2[662] = 165;
        arrayOfChar2[663] = 165;
        arrayOfChar2[664] = 166;
        arrayOfChar2[665] = 150;
        arrayOfChar2[666] = 136;
        arrayOfChar2[667] = 136;
        arrayOfChar2[668] = 120;
        arrayOfChar2[669] = 120;
        arrayOfChar2[670] = 135;
        arrayOfChar2[671] = 135;
        arrayOfChar2[672] = 165;
        arrayOfChar2[673] = 180;
        arrayOfChar2[674] = 150;
        arrayOfChar2[675] = 165;
        arrayOfChar2[676] = 150;
        arrayOfChar2[677] = 151;
        arrayOfChar2[678] = 136;
        arrayOfChar2[679] = 120;
        arrayOfChar2[680] = 120;
        arrayOfChar2[681] = 121;
        arrayOfChar2[682] = 119;
        arrayOfChar2[683] = 135;
        arrayOfChar2[684] = 149;
        arrayOfChar2[685] = 180;
        arrayOfChar2[686] = 150;
        arrayOfChar2[687] = 165;
        arrayOfChar2[688] = 150;
        arrayOfChar2[689] = 151;
        arrayOfChar2[690] = 136;
        arrayOfChar2[691] = 120;
        arrayOfChar2[692] = 120;
        arrayOfChar2[693] = 105;
        arrayOfChar2[694] = 120;
        arrayOfChar2[695] = 135;
        arrayOfChar2[696] = 150;
        arrayOfChar2[697] = 180;
        arrayOfChar2[698] = 150;
        arrayOfChar2[699] = 166;
        arrayOfChar2[700] = 151;
        arrayOfChar2[701] = 151;
        arrayOfChar2[702] = 120;
        arrayOfChar2[703] = 121;
        arrayOfChar2[704] = 121;
        arrayOfChar2[705] = 105;
        arrayOfChar2[706] = 120;
        arrayOfChar2[707] = 119;
        arrayOfChar2[708] = 150;
        arrayOfChar2[709] = 164;
        arrayOfChar2[710] = 165;
        arrayOfChar2[711] = 165;
        arrayOfChar2[712] = 166;
        arrayOfChar2[713] = 150;
        arrayOfChar2[714] = 136;
        arrayOfChar2[715] = 136;
        arrayOfChar2[716] = 136;
        arrayOfChar2[717] = 120;
        arrayOfChar2[718] = 135;
        arrayOfChar2[719] = 135;
        arrayOfChar2[720] = 165;
        arrayOfChar2[721] = 180;
        arrayOfChar2[722] = 150;
        arrayOfChar2[723] = 165;
        arrayOfChar2[724] = 150;
        arrayOfChar2[725] = 150;
        arrayOfChar2[726] = 136;
        arrayOfChar2[727] = 120;
        arrayOfChar2[728] = 120;
        arrayOfChar2[729] = 120;
        arrayOfChar2[730] = 135;
        arrayOfChar2[731] = 135;
        arrayOfChar2[732] = 150;
        arrayOfChar2[733] = 180;
        arrayOfChar2[734] = 150;
        arrayOfChar2[735] = 165;
        arrayOfChar2[736] = 150;
        arrayOfChar2[737] = 151;
        arrayOfChar2[738] = 136;
        arrayOfChar2[739] = 120;
        arrayOfChar2[740] = 120;
        arrayOfChar2[741] = 105;
        arrayOfChar2[742] = 120;
        arrayOfChar2[743] = 135;
        arrayOfChar2[744] = 150;
        arrayOfChar2[745] = 180;
        arrayOfChar2[746] = 150;
        arrayOfChar2[747] = 166;
        arrayOfChar2[748] = 151;
        arrayOfChar2[749] = 151;
        arrayOfChar2[750] = 120;
        arrayOfChar2[751] = 121;
        arrayOfChar2[752] = 121;
        arrayOfChar2[753] = 105;
        arrayOfChar2[754] = 120;
        arrayOfChar2[755] = 119;
        arrayOfChar2[756] = 150;
        arrayOfChar2[757] = 164;
        arrayOfChar2[758] = 165;
        arrayOfChar2[759] = 165;
        arrayOfChar2[760] = 166;
        arrayOfChar2[761] = 150;
        arrayOfChar2[762] = 136;
        arrayOfChar2[763] = 136;
        arrayOfChar2[764] = 136;
        arrayOfChar2[765] = 120;
        arrayOfChar2[766] = 135;
        arrayOfChar2[767] = 135;
        arrayOfChar2[768] = 165;
        arrayOfChar2[769] = 180;
        arrayOfChar2[770] = 150;
        arrayOfChar2[771] = 165;
        arrayOfChar2[772] = 150;
        arrayOfChar2[773] = 150;
        arrayOfChar2[774] = 136;
        arrayOfChar2[775] = 120;
        arrayOfChar2[776] = 120;
        arrayOfChar2[777] = 120;
        arrayOfChar2[778] = 135;
        arrayOfChar2[779] = 135;
        arrayOfChar2[780] = 149;
        arrayOfChar2[781] = 180;
        arrayOfChar2[782] = 150;
        arrayOfChar2[783] = 165;
        arrayOfChar2[784] = 150;
        arrayOfChar2[785] = 151;
        arrayOfChar2[786] = 136;
        arrayOfChar2[787] = 120;
        arrayOfChar2[788] = 120;
        arrayOfChar2[789] = 105;
        arrayOfChar2[790] = 120;
        arrayOfChar2[791] = 135;
        arrayOfChar2[792] = 150;
        arrayOfChar2[793] = 180;
        arrayOfChar2[794] = 150;
        arrayOfChar2[795] = 166;
        arrayOfChar2[796] = 151;
        arrayOfChar2[797] = 151;
        arrayOfChar2[798] = 120;
        arrayOfChar2[799] = 121;
        arrayOfChar2[800] = 121;
        arrayOfChar2[801] = 105;
        arrayOfChar2[802] = 120;
        arrayOfChar2[803] = 119;
        arrayOfChar2[804] = 150;
        arrayOfChar2[805] = 164;
        arrayOfChar2[806] = 165;
        arrayOfChar2[807] = 165;
        arrayOfChar2[808] = 166;
        arrayOfChar2[809] = 166;
        arrayOfChar2[810] = 136;
        arrayOfChar2[811] = 136;
        arrayOfChar2[812] = 136;
        arrayOfChar2[813] = 120;
        arrayOfChar2[814] = 135;
        arrayOfChar2[815] = 135;
        arrayOfChar2[816] = 165;
        arrayOfChar2[817] = 180;
        arrayOfChar2[818] = 150;
        arrayOfChar2[819] = 165;
        arrayOfChar2[820] = 150;
        arrayOfChar2[821] = 150;
        arrayOfChar2[822] = 136;
        arrayOfChar2[823] = 120;
        arrayOfChar2[824] = 120;
        arrayOfChar2[825] = 120;
        arrayOfChar2[826] = 135;
        arrayOfChar2[827] = 135;
        arrayOfChar2[828] = 149;
        arrayOfChar2[829] = 180;
        arrayOfChar2[830] = 150;
        arrayOfChar2[831] = 165;
        arrayOfChar2[832] = 150;
        arrayOfChar2[833] = 151;
        arrayOfChar2[834] = 136;
        arrayOfChar2[835] = 120;
        arrayOfChar2[836] = 120;
        arrayOfChar2[837] = 105;
        arrayOfChar2[838] = 120;
        arrayOfChar2[839] = 135;
        arrayOfChar2[840] = 150;
        arrayOfChar2[841] = 180;
        arrayOfChar2[842] = 150;
        arrayOfChar2[843] = 166;
        arrayOfChar2[844] = 151;
        arrayOfChar2[845] = 151;
        arrayOfChar2[846] = 120;
        arrayOfChar2[847] = 121;
        arrayOfChar2[848] = 121;
        arrayOfChar2[849] = 105;
        arrayOfChar2[850] = 120;
        arrayOfChar2[851] = 119;
        arrayOfChar2[852] = 150;
        arrayOfChar2[853] = 164;
        arrayOfChar2[854] = 165;
        arrayOfChar2[855] = 165;
        arrayOfChar2[856] = 166;
        arrayOfChar2[857] = 166;
        arrayOfChar2[858] = 136;
        arrayOfChar2[859] = 136;
        arrayOfChar2[860] = 136;
        arrayOfChar2[861] = 120;
        arrayOfChar2[862] = 135;
        arrayOfChar2[863] = 135;
        arrayOfChar2[864] = 165;
        arrayOfChar2[865] = 181;
        arrayOfChar2[866] = 150;
        arrayOfChar2[867] = 165;
        arrayOfChar2[868] = 166;
        arrayOfChar2[869] = 150;
        arrayOfChar2[870] = 136;
        arrayOfChar2[871] = 120;
        arrayOfChar2[872] = 120;
        arrayOfChar2[873] = 120;
        arrayOfChar2[874] = 135;
        arrayOfChar2[875] = 135;
        arrayOfChar2[876] = 149;
        arrayOfChar2[877] = 180;
        arrayOfChar2[878] = 150;
        arrayOfChar2[879] = 165;
        arrayOfChar2[880] = 150;
        arrayOfChar2[881] = 151;
        arrayOfChar2[882] = 136;
        arrayOfChar2[883] = 120;
        arrayOfChar2[884] = 120;
        arrayOfChar2[885] = 105;
        arrayOfChar2[886] = 120;
        arrayOfChar2[887] = 135;
        arrayOfChar2[888] = 150;
        arrayOfChar2[889] = 180;
        arrayOfChar2[890] = 150;
        arrayOfChar2[891] = 166;
        arrayOfChar2[892] = 151;
        arrayOfChar2[893] = 151;
        arrayOfChar2[894] = 120;
        arrayOfChar2[895] = 121;
        arrayOfChar2[896] = 120;
        arrayOfChar2[897] = 105;
        arrayOfChar2[898] = 120;
        arrayOfChar2[899] = 119;
        arrayOfChar2[900] = 150;
        arrayOfChar2[901] = 164;
        arrayOfChar2[902] = 165;
        arrayOfChar2[903] = 181;
        arrayOfChar2[904] = 166;
        arrayOfChar2[905] = 166;
        arrayOfChar2[906] = 136;
        arrayOfChar2[907] = 137;
        arrayOfChar2[908] = 136;
        arrayOfChar2[909] = 120;
        arrayOfChar2[910] = 135;
        arrayOfChar2[911] = 135;
        arrayOfChar2[912] = 165;
        arrayOfChar2[913] = 180;
        arrayOfChar2[914] = 150;
        arrayOfChar2[915] = 165;
        arrayOfChar2[916] = 150;
        arrayOfChar2[917] = 150;
        arrayOfChar2[918] = 136;
        arrayOfChar2[919] = 136;
        arrayOfChar2[920] = 120;
        arrayOfChar2[921] = 120;
        arrayOfChar2[922] = 135;
        arrayOfChar2[923] = 135;
        arrayOfChar2[924] = 149;
        arrayOfChar2[925] = 180;
        arrayOfChar2[926] = 150;
        arrayOfChar2[927] = 165;
        arrayOfChar2[928] = 150;
        arrayOfChar2[929] = 151;
        arrayOfChar2[930] = 136;
        arrayOfChar2[931] = 120;
        arrayOfChar2[932] = 120;
        arrayOfChar2[933] = 121;
        arrayOfChar2[934] = 120;
        arrayOfChar2[935] = 135;
        arrayOfChar2[936] = 150;
        arrayOfChar2[937] = 180;
        arrayOfChar2[938] = 150;
        arrayOfChar2[939] = 166;
        arrayOfChar2[940] = 150;
        arrayOfChar2[941] = 151;
        arrayOfChar2[942] = 120;
        arrayOfChar2[943] = 121;
        arrayOfChar2[944] = 120;
        arrayOfChar2[945] = 105;
        arrayOfChar2[946] = 120;
        arrayOfChar2[947] = 119;
        arrayOfChar2[948] = 150;
        arrayOfChar2[949] = 164;
        arrayOfChar2[950] = 165;
        arrayOfChar2[951] = 181;
        arrayOfChar2[952] = 166;
        arrayOfChar2[953] = 166;
        arrayOfChar2[954] = 136;
        arrayOfChar2[955] = 136;
        arrayOfChar2[956] = 136;
        arrayOfChar2[957] = 120;
        arrayOfChar2[958] = 135;
        arrayOfChar2[959] = 135;
        arrayOfChar2[960] = 165;
        arrayOfChar2[961] = 180;
        arrayOfChar2[962] = 150;
        arrayOfChar2[963] = 165;
        arrayOfChar2[964] = 166;
        arrayOfChar2[965] = 150;
        arrayOfChar2[966] = 136;
        arrayOfChar2[967] = 136;
        arrayOfChar2[968] = 120;
        arrayOfChar2[969] = 120;
        arrayOfChar2[970] = 119;
        arrayOfChar2[971] = 135;
        arrayOfChar2[972] = 149;
        arrayOfChar2[973] = 180;
        arrayOfChar2[974] = 150;
        arrayOfChar2[975] = 165;
        arrayOfChar2[976] = 150;
        arrayOfChar2[977] = 151;
        arrayOfChar2[978] = 136;
        arrayOfChar2[979] = 120;
        arrayOfChar2[980] = 120;
        arrayOfChar2[981] = 121;
        arrayOfChar2[982] = 119;
        arrayOfChar2[983] = 135;
        arrayOfChar2[984] = 149;
        arrayOfChar2[985] = 180;
        arrayOfChar2[986] = 150;
        arrayOfChar2[987] = 165;
        arrayOfChar2[988] = 150;
        arrayOfChar2[989] = 151;
        arrayOfChar2[990] = 120;
        arrayOfChar2[991] = 121;
        arrayOfChar2[992] = 120;
        arrayOfChar2[993] = 105;
        arrayOfChar2[994] = 120;
        arrayOfChar2[995] = 119;
        arrayOfChar2[996] = 150;
        arrayOfChar2[997] = 180;
        arrayOfChar2[998] = 165;
        arrayOfChar2[999] = 181;
        arrayOfChar2[1000] = 166;
        arrayOfChar2[1001] = 166;
        arrayOfChar2[1002] = 135;
        arrayOfChar2[1003] = 136;
        arrayOfChar2[1004] = 136;
        arrayOfChar2[1005] = 120;
        arrayOfChar2[1006] = 135;
        arrayOfChar2[1007] = 135;
        arrayOfChar2[1008] = 165;
        arrayOfChar2[1009] = 180;
        arrayOfChar2[1010] = 166;
        arrayOfChar2[1011] = 165;
        arrayOfChar2[1012] = 166;
        arrayOfChar2[1013] = 150;
        arrayOfChar2[1014] = 136;
        arrayOfChar2[1015] = 136;
        arrayOfChar2[1016] = 120;
        arrayOfChar2[1017] = 120;
        arrayOfChar2[1018] = 135;
        arrayOfChar2[1019] = 135;
        arrayOfChar2[1020] = 165;
        arrayOfChar2[1021] = 180;
        arrayOfChar2[1022] = 150;
        arrayOfChar2[1023] = 165;
        arrayOfChar2[1024] = 150;
        arrayOfChar2[1025] = 151;
        arrayOfChar2[1026] = 136;
        arrayOfChar2[1027] = 120;
        arrayOfChar2[1028] = 120;
        arrayOfChar2[1029] = 121;
        arrayOfChar2[1030] = 119;
        arrayOfChar2[1031] = 135;
        arrayOfChar2[1032] = 149;
        arrayOfChar2[1033] = 180;
        arrayOfChar2[1034] = 150;
        arrayOfChar2[1035] = 165;
        arrayOfChar2[1036] = 150;
        arrayOfChar2[1037] = 151;
        arrayOfChar2[1038] = 136;
        arrayOfChar2[1039] = 121;
        arrayOfChar2[1040] = 120;
        arrayOfChar2[1041] = 105;
        arrayOfChar2[1042] = 120;
        arrayOfChar2[1043] = 135;
        arrayOfChar2[1044] = 150;
        arrayOfChar2[1045] = 180;
        arrayOfChar2[1046] = 165;
        arrayOfChar2[1047] = 181;
        arrayOfChar2[1048] = 166;
        arrayOfChar2[1049] = 166;
        arrayOfChar2[1050] = 135;
        arrayOfChar2[1051] = 136;
        arrayOfChar2[1052] = 136;
        arrayOfChar2[1053] = 120;
        arrayOfChar2[1054] = 135;
        arrayOfChar2[1055] = 134;
        arrayOfChar2[1056] = 165;
        arrayOfChar2[1057] = 180;
        arrayOfChar2[1058] = 165;
        arrayOfChar2[1059] = 165;
        arrayOfChar2[1060] = 166;
        arrayOfChar2[1061] = 150;
        arrayOfChar2[1062] = 136;
        arrayOfChar2[1063] = 136;
        arrayOfChar2[1064] = 136;
        arrayOfChar2[1065] = 120;
        arrayOfChar2[1066] = 135;
        arrayOfChar2[1067] = 135;
        arrayOfChar2[1068] = 165;
        arrayOfChar2[1069] = 180;
        arrayOfChar2[1070] = 150;
        arrayOfChar2[1071] = 165;
        arrayOfChar2[1072] = 150;
        arrayOfChar2[1073] = 150;
        arrayOfChar2[1074] = 136;
        arrayOfChar2[1075] = 120;
        arrayOfChar2[1076] = 120;
        arrayOfChar2[1077] = 121;
        arrayOfChar2[1078] = 119;
        arrayOfChar2[1079] = 135;
        arrayOfChar2[1080] = 149;
        arrayOfChar2[1081] = 180;
        arrayOfChar2[1082] = 150;
        arrayOfChar2[1083] = 165;
        arrayOfChar2[1084] = 134;
        arrayOfChar2[1085] = 151;
        arrayOfChar2[1086] = 136;
        arrayOfChar2[1087] = 120;
        arrayOfChar2[1088] = 120;
        arrayOfChar2[1089] = 105;
        arrayOfChar2[1090] = 120;
        arrayOfChar2[1091] = 135;
        arrayOfChar2[1092] = 150;
        arrayOfChar2[1093] = 180;
        arrayOfChar2[1094] = 165;
        arrayOfChar2[1095] = 181;
        arrayOfChar2[1096] = 166;
        arrayOfChar2[1097] = 166;
        arrayOfChar2[1098] = 135;
        arrayOfChar2[1099] = 136;
        arrayOfChar2[1100] = 136;
        arrayOfChar2[1101] = 120;
        arrayOfChar2[1102] = 135;
        arrayOfChar2[1103] = 134;
        arrayOfChar2[1104] = 165;
        arrayOfChar2[1105] = 179;
        arrayOfChar2[1106] = 165;
        arrayOfChar2[1107] = 165;
        arrayOfChar2[1108] = 166;
        arrayOfChar2[1109] = 150;
        arrayOfChar2[1110] = 136;
        arrayOfChar2[1111] = 136;
        arrayOfChar2[1112] = 136;
        arrayOfChar2[1113] = 120;
        arrayOfChar2[1114] = 135;
        arrayOfChar2[1115] = 135;
        arrayOfChar2[1116] = 165;
        arrayOfChar2[1117] = 180;
        arrayOfChar2[1118] = 150;
        arrayOfChar2[1119] = 165;
        arrayOfChar2[1120] = 150;
        arrayOfChar2[1121] = 150;
        arrayOfChar2[1122] = 136;
        arrayOfChar2[1123] = 120;
        arrayOfChar2[1124] = 120;
        arrayOfChar2[1125] = 120;
        arrayOfChar2[1126] = 135;
        arrayOfChar2[1127] = 135;
        arrayOfChar2[1128] = 149;
        arrayOfChar2[1129] = 180;
        arrayOfChar2[1130] = 150;
        arrayOfChar2[1131] = 165;
        arrayOfChar2[1132] = 150;
        arrayOfChar2[1133] = 151;
        arrayOfChar2[1134] = 136;
        arrayOfChar2[1135] = 118;
        arrayOfChar2[1136] = 120;
        arrayOfChar2[1137] = 105;
        arrayOfChar2[1138] = 120;
        arrayOfChar2[1139] = 135;
        arrayOfChar2[1140] = 150;
        arrayOfChar2[1141] = 180;
        arrayOfChar2[1142] = 165;
        arrayOfChar2[1143] = 181;
        arrayOfChar2[1144] = 166;
        arrayOfChar2[1145] = 166;
        arrayOfChar2[1146] = 135;
        arrayOfChar2[1147] = 136;
        arrayOfChar2[1148] = 136;
        arrayOfChar2[1149] = 120;
        arrayOfChar2[1150] = 135;
        arrayOfChar2[1151] = 134;
        arrayOfChar2[1152] = 165;
        arrayOfChar2[1153] = 179;
        arrayOfChar2[1154] = 165;
        arrayOfChar2[1155] = 165;
        arrayOfChar2[1156] = 166;
        arrayOfChar2[1157] = 166;
        arrayOfChar2[1158] = 136;
        arrayOfChar2[1159] = 136;
        arrayOfChar2[1160] = 136;
        arrayOfChar2[1161] = 120;
        arrayOfChar2[1162] = 135;
        arrayOfChar2[1163] = 135;
        arrayOfChar2[1164] = 165;
        arrayOfChar2[1165] = 180;
        arrayOfChar2[1166] = 150;
        arrayOfChar2[1167] = 165;
        arrayOfChar2[1168] = 150;
        arrayOfChar2[1169] = 150;
        arrayOfChar2[1170] = 136;
        arrayOfChar2[1171] = 120;
        arrayOfChar2[1172] = 120;
        arrayOfChar2[1173] = 120;
        arrayOfChar2[1174] = 135;
        arrayOfChar2[1175] = 135;
        arrayOfChar2[1176] = 149;
        arrayOfChar2[1177] = 180;
        arrayOfChar2[1178] = 150;
        arrayOfChar2[1179] = 165;
        arrayOfChar2[1180] = 150;
        arrayOfChar2[1181] = 151;
        arrayOfChar2[1182] = 136;
        arrayOfChar2[1183] = 120;
        arrayOfChar2[1184] = 120;
        arrayOfChar2[1185] = 105;
        arrayOfChar2[1186] = 120;
        arrayOfChar2[1187] = 135;
        arrayOfChar2[1188] = 150;
        arrayOfChar2[1189] = 180;
        arrayOfChar2[1190] = 165;
        arrayOfChar2[1191] = 181;
        arrayOfChar2[1192] = 166;
        arrayOfChar2[1193] = 166;
        arrayOfChar2[1194] = 135;
        arrayOfChar2[1195] = 136;
        arrayOfChar2[1196] = 136;
        arrayOfChar2[1197] = 120;
        arrayOfChar2[1198] = 135;
        arrayOfChar2[1199] = 134;
        arrayOfChar2[1200] = 165;
        arrayOfChar2[1201] = 179;
        arrayOfChar2[1202] = 165;
        arrayOfChar2[1203] = 165;
        arrayOfChar2[1204] = 166;
        arrayOfChar2[1205] = 166;
        arrayOfChar2[1206] = 136;
        arrayOfChar2[1207] = 136;
        arrayOfChar2[1208] = 136;
        arrayOfChar2[1209] = 120;
        arrayOfChar2[1210] = 135;
        arrayOfChar2[1211] = 135;
        arrayOfChar2[1212] = 165;
        arrayOfChar2[1213] = 180;
        arrayOfChar2[1214] = 150;
        arrayOfChar2[1215] = 165;
        arrayOfChar2[1216] = 150;
        arrayOfChar2[1217] = 150;
        arrayOfChar2[1218] = 136;
        arrayOfChar2[1219] = 120;
        arrayOfChar2[1220] = 120;
        arrayOfChar2[1221] = 120;
        arrayOfChar2[1222] = 135;
        arrayOfChar2[1223] = 135;
        arrayOfChar2[1224] = 149;
        arrayOfChar2[1225] = 180;
        arrayOfChar2[1226] = 150;
        arrayOfChar2[1227] = 165;
        arrayOfChar2[1228] = 150;
        arrayOfChar2[1229] = 151;
        arrayOfChar2[1230] = 136;
        arrayOfChar2[1231] = 120;
        arrayOfChar2[1232] = 120;
        arrayOfChar2[1233] = 105;
        arrayOfChar2[1234] = 120;
        arrayOfChar2[1235] = 135;
        arrayOfChar2[1236] = 150;
        arrayOfChar2[1237] = 180;
        arrayOfChar2[1238] = 165;
        arrayOfChar2[1239] = 181;
        arrayOfChar2[1240] = 166;
        arrayOfChar2[1241] = 166;
        arrayOfChar2[1242] = 135;
        arrayOfChar2[1243] = 136;
        arrayOfChar2[1244] = 136;
        arrayOfChar2[1245] = 120;
        arrayOfChar2[1246] = 135;
        arrayOfChar2[1247] = 134;
        arrayOfChar2[1248] = 165;
        arrayOfChar2[1249] = 179;
        arrayOfChar2[1250] = 165;
        arrayOfChar2[1251] = 165;
        arrayOfChar2[1252] = 166;
        arrayOfChar2[1253] = 166;
        arrayOfChar2[1254] = 136;
        arrayOfChar2[1255] = 136;
        arrayOfChar2[1256] = 136;
        arrayOfChar2[1257] = 120;
        arrayOfChar2[1258] = 135;
        arrayOfChar2[1259] = 135;
        arrayOfChar2[1260] = 165;
        arrayOfChar2[1261] = 180;
        arrayOfChar2[1262] = 150;
        arrayOfChar2[1263] = 165;
        arrayOfChar2[1264] = 166;
        arrayOfChar2[1265] = 150;
        arrayOfChar2[1266] = 136;
        arrayOfChar2[1267] = 136;
        arrayOfChar2[1268] = 120;
        arrayOfChar2[1269] = 120;
        arrayOfChar2[1270] = 135;
        arrayOfChar2[1271] = 135;
        arrayOfChar2[1272] = 149;
        arrayOfChar2[1273] = 180;
        arrayOfChar2[1274] = 150;
        arrayOfChar2[1275] = 165;
        arrayOfChar2[1276] = 150;
        arrayOfChar2[1277] = 151;
        arrayOfChar2[1278] = 136;
        arrayOfChar2[1279] = 120;
        arrayOfChar2[1280] = 120;
        arrayOfChar2[1281] = 105;
        arrayOfChar2[1282] = 120;
        arrayOfChar2[1283] = 135;
        arrayOfChar2[1284] = 150;
        arrayOfChar2[1285] = 180;
        arrayOfChar2[1286] = 165;
        arrayOfChar2[1287] = 181;
        arrayOfChar2[1288] = 166;
        arrayOfChar2[1289] = 166;
        arrayOfChar2[1290] = 135;
        arrayOfChar2[1291] = 136;
        arrayOfChar2[1292] = 135;
        arrayOfChar2[1293] = 120;
        arrayOfChar2[1294] = 135;
        arrayOfChar2[1295] = 134;
        arrayOfChar2[1296] = 165;
        arrayOfChar2[1297] = 179;
        arrayOfChar2[1298] = 165;
        arrayOfChar2[1299] = 181;
        arrayOfChar2[1300] = 166;
        arrayOfChar2[1301] = 166;
        arrayOfChar2[1302] = 136;
        arrayOfChar2[1303] = 136;
        arrayOfChar2[1304] = 136;
        arrayOfChar2[1305] = 120;
        arrayOfChar2[1306] = 135;
        arrayOfChar2[1307] = 135;
        arrayOfChar2[1308] = 165;
        arrayOfChar2[1309] = 180;
        arrayOfChar2[1310] = 150;
        arrayOfChar2[1311] = 165;
        arrayOfChar2[1312] = 166;
        arrayOfChar2[1313] = 150;
        arrayOfChar2[1314] = 136;
        arrayOfChar2[1315] = 136;
        arrayOfChar2[1316] = 120;
        arrayOfChar2[1317] = 120;
        arrayOfChar2[1318] = 135;
        arrayOfChar2[1319] = 135;
        arrayOfChar2[1320] = 149;
        arrayOfChar2[1321] = 180;
        arrayOfChar2[1322] = 150;
        arrayOfChar2[1323] = 165;
        arrayOfChar2[1324] = 150;
        arrayOfChar2[1325] = 151;
        arrayOfChar2[1326] = 136;
        arrayOfChar2[1327] = 120;
        arrayOfChar2[1328] = 120;
        arrayOfChar2[1329] = 121;
        arrayOfChar2[1330] = 120;
        arrayOfChar2[1331] = 135;
        arrayOfChar2[1332] = 150;
        arrayOfChar2[1333] = 180;
        arrayOfChar2[1334] = 165;
        arrayOfChar2[1335] = 181;
        arrayOfChar2[1336] = 165;
        arrayOfChar2[1337] = 166;
        arrayOfChar2[1338] = 135;
        arrayOfChar2[1339] = 136;
        arrayOfChar2[1340] = 135;
        arrayOfChar2[1341] = 120;
        arrayOfChar2[1342] = 135;
        arrayOfChar2[1343] = 134;
        arrayOfChar2[1344] = 165;
        arrayOfChar2[1345] = 179;
        arrayOfChar2[1346] = 165;
        arrayOfChar2[1347] = 181;
        arrayOfChar2[1348] = 166;
        arrayOfChar2[1349] = 166;
        arrayOfChar2[1350] = 135;
        arrayOfChar2[1351] = 136;
        arrayOfChar2[1352] = 136;
        arrayOfChar2[1353] = 120;
        arrayOfChar2[1354] = 135;
        arrayOfChar2[1355] = 135;
        arrayOfChar2[1356] = 165;
        arrayOfChar2[1357] = 180;
        arrayOfChar2[1358] = 150;
        arrayOfChar2[1359] = 165;
        arrayOfChar2[1360] = 166;
        arrayOfChar2[1361] = 150;
        arrayOfChar2[1362] = 136;
        arrayOfChar2[1363] = 136;
        arrayOfChar2[1364] = 120;
        arrayOfChar2[1365] = 120;
        arrayOfChar2[1366] = 135;
        arrayOfChar2[1367] = 135;
        arrayOfChar2[1368] = 149;
        arrayOfChar2[1369] = 180;
        arrayOfChar2[1370] = 150;
        arrayOfChar2[1371] = 165;
        arrayOfChar2[1372] = 150;
        arrayOfChar2[1373] = 151;
        arrayOfChar2[1374] = 136;
        arrayOfChar2[1375] = 120;
        arrayOfChar2[1376] = 120;
        arrayOfChar2[1377] = 121;
        arrayOfChar2[1378] = 119;
        arrayOfChar2[1379] = 135;
        arrayOfChar2[1380] = 149;
        arrayOfChar2[1381] = 180;
        arrayOfChar2[1382] = 165;
        arrayOfChar2[1383] = 180;
        arrayOfChar2[1384] = 165;
        arrayOfChar2[1385] = 166;
        arrayOfChar2[1386] = 135;
        arrayOfChar2[1387] = 136;
        arrayOfChar2[1388] = 135;
        arrayOfChar2[1389] = 120;
        arrayOfChar2[1390] = 135;
        arrayOfChar2[1391] = 134;
        arrayOfChar2[1392] = 165;
        arrayOfChar2[1393] = 195;
        arrayOfChar2[1394] = 165;
        arrayOfChar2[1395] = 181;
        arrayOfChar2[1396] = 166;
        arrayOfChar2[1397] = 166;
        arrayOfChar2[1398] = 135;
        arrayOfChar2[1399] = 136;
        arrayOfChar2[1400] = 136;
        arrayOfChar2[1401] = 120;
        arrayOfChar2[1402] = 135;
        arrayOfChar2[1403] = 135;
        arrayOfChar2[1404] = 165;
        arrayOfChar2[1405] = 180;
        arrayOfChar2[1406] = 166;
        arrayOfChar2[1407] = 165;
        arrayOfChar2[1408] = 166;
        arrayOfChar2[1409] = 150;
        arrayOfChar2[1410] = 136;
        arrayOfChar2[1411] = 136;
        arrayOfChar2[1412] = 120;
        arrayOfChar2[1413] = 120;
        arrayOfChar2[1414] = 135;
        arrayOfChar2[1415] = 135;
        arrayOfChar2[1416] = 165;
        arrayOfChar2[1417] = 180;
        arrayOfChar2[1418] = 150;
        arrayOfChar2[1419] = 165;
        arrayOfChar2[1420] = 150;
        arrayOfChar2[1421] = 150;
        arrayOfChar2[1422] = 136;
        arrayOfChar2[1423] = 120;
        arrayOfChar2[1424] = 120;
        arrayOfChar2[1425] = 121;
        arrayOfChar2[1426] = 119;
        arrayOfChar2[1427] = 135;
        arrayOfChar2[1428] = 149;
        arrayOfChar2[1429] = 180;
        arrayOfChar2[1430] = 165;
        arrayOfChar2[1431] = 180;
        arrayOfChar2[1432] = 165;
        arrayOfChar2[1433] = 166;
        arrayOfChar2[1434] = 151;
        arrayOfChar2[1435] = 135;
        arrayOfChar2[1436] = 135;
        arrayOfChar2[1437] = 120;
        arrayOfChar2[1438] = 135;
        arrayOfChar2[1439] = 134;
        arrayOfChar2[1440] = 165;
        arrayOfChar2[1441] = 195;
        arrayOfChar2[1442] = 165;
        arrayOfChar2[1443] = 181;
        arrayOfChar2[1444] = 166;
        arrayOfChar2[1445] = 166;
        arrayOfChar2[1446] = 135;
        arrayOfChar2[1447] = 136;
        arrayOfChar2[1448] = 136;
        arrayOfChar2[1449] = 120;
        arrayOfChar2[1450] = 135;
        arrayOfChar2[1451] = 134;
        arrayOfChar2[1452] = 165;
        arrayOfChar2[1453] = 180;
        arrayOfChar2[1454] = 165;
        arrayOfChar2[1455] = 165;
        arrayOfChar2[1456] = 166;
        arrayOfChar2[1457] = 150;
        arrayOfChar2[1458] = 136;
        arrayOfChar2[1459] = 136;
        arrayOfChar2[1460] = 136;
        arrayOfChar2[1461] = 120;
        arrayOfChar2[1462] = 135;
        arrayOfChar2[1463] = 135;
        arrayOfChar2[1464] = 165;
        arrayOfChar2[1465] = 180;
        arrayOfChar2[1466] = 150;
        arrayOfChar2[1467] = 165;
        arrayOfChar2[1468] = 150;
        arrayOfChar2[1469] = 150;
        arrayOfChar2[1470] = 136;
        arrayOfChar2[1471] = 120;
        arrayOfChar2[1472] = 120;
        arrayOfChar2[1473] = 121;
        arrayOfChar2[1474] = 119;
        arrayOfChar2[1475] = 135;
        arrayOfChar2[1476] = 149;
        arrayOfChar2[1477] = 180;
        arrayOfChar2[1478] = 165;
        arrayOfChar2[1479] = 180;
        arrayOfChar2[1480] = 165;
        arrayOfChar2[1481] = 166;
        arrayOfChar2[1482] = 151;
        arrayOfChar2[1483] = 135;
        arrayOfChar2[1484] = 135;
        arrayOfChar2[1485] = 120;
        arrayOfChar2[1486] = 135;
        arrayOfChar2[1487] = 150;
        arrayOfChar2[1488] = 165;
        arrayOfChar2[1489] = 195;
        arrayOfChar2[1490] = 165;
        arrayOfChar2[1491] = 181;
        arrayOfChar2[1492] = 166;
        arrayOfChar2[1493] = 166;
        arrayOfChar2[1494] = 135;
        arrayOfChar2[1495] = 136;
        arrayOfChar2[1496] = 136;
        arrayOfChar2[1497] = 120;
        arrayOfChar2[1498] = 135;
        arrayOfChar2[1499] = 134;
        arrayOfChar2[1500] = 165;
        arrayOfChar2[1501] = 179;
        arrayOfChar2[1502] = 165;
        arrayOfChar2[1503] = 165;
        arrayOfChar2[1504] = 166;
        arrayOfChar2[1505] = 166;
        arrayOfChar2[1506] = 136;
        arrayOfChar2[1507] = 136;
        arrayOfChar2[1508] = 136;
        arrayOfChar2[1509] = 120;
        arrayOfChar2[1510] = 135;
        arrayOfChar2[1511] = 135;
        arrayOfChar2[1512] = 165;
        arrayOfChar2[1513] = 180;
        arrayOfChar2[1514] = 150;
        arrayOfChar2[1515] = 165;
        arrayOfChar2[1516] = 150;
        arrayOfChar2[1517] = 150;
        arrayOfChar2[1518] = 136;
        arrayOfChar2[1519] = 120;
        arrayOfChar2[1520] = 120;
        arrayOfChar2[1521] = 120;
        arrayOfChar2[1522] = 135;
        arrayOfChar2[1523] = 135;
        arrayOfChar2[1524] = 149;
        arrayOfChar2[1525] = 180;
        arrayOfChar2[1526] = 165;
        arrayOfChar2[1527] = 180;
        arrayOfChar2[1528] = 165;
        arrayOfChar2[1529] = 166;
        arrayOfChar2[1530] = 151;
        arrayOfChar2[1531] = 135;
        arrayOfChar2[1532] = 135;
        arrayOfChar2[1533] = 120;
        arrayOfChar2[1534] = 135;
        arrayOfChar2[1535] = 150;
        arrayOfChar2[1536] = 165;
        arrayOfChar2[1537] = 195;
        arrayOfChar2[1538] = 165;
        arrayOfChar2[1539] = 181;
        arrayOfChar2[1540] = 166;
        arrayOfChar2[1541] = 166;
        arrayOfChar2[1542] = 135;
        arrayOfChar2[1543] = 136;
        arrayOfChar2[1544] = 136;
        arrayOfChar2[1545] = 120;
        arrayOfChar2[1546] = 135;
        arrayOfChar2[1547] = 134;
        arrayOfChar2[1548] = 165;
        arrayOfChar2[1549] = 179;
        arrayOfChar2[1550] = 165;
        arrayOfChar2[1551] = 165;
        arrayOfChar2[1552] = 166;
        arrayOfChar2[1553] = 166;
        arrayOfChar2[1554] = 136;
        arrayOfChar2[1555] = 136;
        arrayOfChar2[1556] = 136;
        arrayOfChar2[1557] = 120;
        arrayOfChar2[1558] = 135;
        arrayOfChar2[1559] = 135;
        arrayOfChar2[1560] = 165;
        arrayOfChar2[1561] = 180;
        arrayOfChar2[1562] = 150;
        arrayOfChar2[1563] = 165;
        arrayOfChar2[1564] = 150;
        arrayOfChar2[1565] = 150;
        arrayOfChar2[1566] = 136;
        arrayOfChar2[1567] = 120;
        arrayOfChar2[1568] = 120;
        arrayOfChar2[1569] = 120;
        arrayOfChar2[1570] = 135;
        arrayOfChar2[1571] = 135;
        arrayOfChar2[1572] = 149;
        arrayOfChar2[1573] = 180;
        arrayOfChar2[1574] = 165;
        arrayOfChar2[1575] = 180;
        arrayOfChar2[1576] = 165;
        arrayOfChar2[1577] = 166;
        arrayOfChar2[1578] = 151;
        arrayOfChar2[1579] = 135;
        arrayOfChar2[1580] = 135;
        arrayOfChar2[1581] = 120;
        arrayOfChar2[1582] = 135;
        arrayOfChar2[1583] = 150;
        arrayOfChar2[1584] = 165;
        arrayOfChar2[1585] = 195;
        arrayOfChar2[1586] = 165;
        arrayOfChar2[1587] = 181;
        arrayOfChar2[1588] = 166;
        arrayOfChar2[1589] = 166;
        arrayOfChar2[1590] = 136;
        arrayOfChar2[1591] = 136;
        arrayOfChar2[1592] = 136;
        arrayOfChar2[1593] = 120;
        arrayOfChar2[1594] = 135;
        arrayOfChar2[1595] = 134;
        arrayOfChar2[1596] = 165;
        arrayOfChar2[1597] = 179;
        arrayOfChar2[1598] = 165;
        arrayOfChar2[1599] = 165;
        arrayOfChar2[1600] = 166;
        arrayOfChar2[1601] = 166;
        arrayOfChar2[1602] = 136;
        arrayOfChar2[1603] = 120;
        arrayOfChar2[1604] = 136;
        arrayOfChar2[1605] = 120;
        arrayOfChar2[1606] = 135;
        arrayOfChar2[1607] = 135;
        arrayOfChar2[1608] = 165;
        arrayOfChar2[1609] = 180;
        arrayOfChar2[1610] = 150;
        arrayOfChar2[1611] = 165;
        arrayOfChar2[1612] = 166;
        arrayOfChar2[1613] = 150;
        arrayOfChar2[1614] = 136;
        arrayOfChar2[1615] = 136;
        arrayOfChar2[1616] = 120;
        arrayOfChar2[1617] = 120;
        arrayOfChar2[1618] = 135;
        arrayOfChar2[1619] = 135;
        arrayOfChar2[1620] = 149;
        arrayOfChar2[1621] = 180;
        arrayOfChar2[1622] = 165;
        arrayOfChar2[1623] = 180;
        arrayOfChar2[1624] = 165;
        arrayOfChar2[1625] = 166;
        arrayOfChar2[1626] = 151;
        arrayOfChar2[1627] = 135;
        arrayOfChar2[1628] = 135;
        arrayOfChar2[1629] = 120;
        arrayOfChar2[1630] = 135;
        arrayOfChar2[1631] = 150;
        arrayOfChar2[1632] = 165;
        arrayOfChar2[1633] = 195;
        arrayOfChar2[1634] = 165;
        arrayOfChar2[1635] = 181;
        arrayOfChar2[1636] = 166;
        arrayOfChar2[1637] = 166;
        arrayOfChar2[1638] = 135;
        arrayOfChar2[1639] = 136;
        arrayOfChar2[1640] = 136;
        arrayOfChar2[1641] = 120;
        arrayOfChar2[1642] = 135;
        arrayOfChar2[1643] = 134;
        arrayOfChar2[1644] = 165;
        arrayOfChar2[1645] = 179;
        arrayOfChar2[1646] = 165;
        arrayOfChar2[1647] = 165;
        arrayOfChar2[1648] = 166;
        arrayOfChar2[1649] = 166;
        arrayOfChar2[1650] = 136;
        arrayOfChar2[1651] = 136;
        arrayOfChar2[1652] = 136;
        arrayOfChar2[1653] = 120;
        arrayOfChar2[1654] = 135;
        arrayOfChar2[1655] = 135;
        arrayOfChar2[1656] = 165;
        arrayOfChar2[1657] = 180;
        arrayOfChar2[1658] = 150;
        arrayOfChar2[1659] = 165;
        arrayOfChar2[1660] = 166;
        arrayOfChar2[1661] = 150;
        arrayOfChar2[1662] = 136;
        arrayOfChar2[1663] = 136;
        arrayOfChar2[1664] = 120;
        arrayOfChar2[1665] = 120;
        arrayOfChar2[1666] = 135;
        arrayOfChar2[1667] = 135;
        arrayOfChar2[1668] = 149;
        arrayOfChar2[1669] = 180;
        arrayOfChar2[1670] = 165;
        arrayOfChar2[1671] = 180;
        arrayOfChar2[1672] = 165;
        arrayOfChar2[1673] = 166;
        arrayOfChar2[1674] = 151;
        arrayOfChar2[1675] = 135;
        arrayOfChar2[1676] = 135;
        arrayOfChar2[1677] = 120;
        arrayOfChar2[1678] = 135;
        arrayOfChar2[1679] = 150;
        arrayOfChar2[1680] = 165;
        arrayOfChar2[1681] = 195;
        arrayOfChar2[1682] = 165;
        arrayOfChar2[1683] = 181;
        arrayOfChar2[1684] = 165;
        arrayOfChar2[1685] = 166;
        arrayOfChar2[1686] = 135;
        arrayOfChar2[1687] = 136;
        arrayOfChar2[1688] = 135;
        arrayOfChar2[1689] = 120;
        arrayOfChar2[1690] = 135;
        arrayOfChar2[1691] = 134;
        arrayOfChar2[1692] = 165;
        arrayOfChar2[1693] = 179;
        arrayOfChar2[1694] = 165;
        arrayOfChar2[1695] = 181;
        arrayOfChar2[1696] = 166;
        arrayOfChar2[1697] = 166;
        arrayOfChar2[1698] = 136;
        arrayOfChar2[1699] = 136;
        arrayOfChar2[1700] = 136;
        arrayOfChar2[1701] = 120;
        arrayOfChar2[1702] = 135;
        arrayOfChar2[1703] = 135;
        arrayOfChar2[1704] = 165;
        arrayOfChar2[1705] = 180;
        arrayOfChar2[1706] = 150;
        arrayOfChar2[1707] = 165;
        arrayOfChar2[1708] = 166;
        arrayOfChar2[1709] = 150;
        arrayOfChar2[1710] = 136;
        arrayOfChar2[1711] = 136;
        arrayOfChar2[1712] = 120;
        arrayOfChar2[1713] = 120;
        arrayOfChar2[1714] = 135;
        arrayOfChar2[1715] = 135;
        arrayOfChar2[1716] = 149;
        arrayOfChar2[1717] = 180;
        arrayOfChar2[1718] = 165;
        arrayOfChar2[1719] = 180;
        arrayOfChar2[1720] = 165;
        arrayOfChar2[1721] = 166;
        arrayOfChar2[1722] = 151;
        arrayOfChar2[1723] = 135;
        arrayOfChar2[1724] = 135;
        arrayOfChar2[1725] = 136;
        arrayOfChar2[1726] = 135;
        arrayOfChar2[1727] = 150;
        arrayOfChar2[1728] = 165;
        arrayOfChar2[1729] = 195;
        arrayOfChar2[1730] = 165;
        arrayOfChar2[1731] = 180;
        arrayOfChar2[1732] = 165;
        arrayOfChar2[1733] = 166;
        arrayOfChar2[1734] = 135;
        arrayOfChar2[1735] = 136;
        arrayOfChar2[1736] = 135;
        arrayOfChar2[1737] = 120;
        arrayOfChar2[1738] = 135;
        arrayOfChar2[1739] = 134;
        arrayOfChar2[1740] = 165;
        arrayOfChar2[1741] = 179;
        arrayOfChar2[1742] = 165;
        arrayOfChar2[1743] = 181;
        arrayOfChar2[1744] = 166;
        arrayOfChar2[1745] = 166;
        arrayOfChar2[1746] = 135;
        arrayOfChar2[1747] = 136;
        arrayOfChar2[1748] = 136;
        arrayOfChar2[1749] = 120;
        arrayOfChar2[1750] = 135;
        arrayOfChar2[1751] = 135;
        arrayOfChar2[1752] = 165;
        arrayOfChar2[1753] = 180;
        arrayOfChar2[1754] = 150;
        arrayOfChar2[1755] = 165;
        arrayOfChar2[1756] = 166;
        arrayOfChar2[1757] = 150;
        arrayOfChar2[1758] = 136;
        arrayOfChar2[1759] = 136;
        arrayOfChar2[1760] = 120;
        arrayOfChar2[1761] = 120;
        arrayOfChar2[1762] = 135;
        arrayOfChar2[1763] = 135;
        arrayOfChar2[1764] = 149;
        arrayOfChar2[1765] = 180;
        arrayOfChar2[1766] = 165;
        arrayOfChar2[1767] = 180;
        arrayOfChar2[1768] = 165;
        arrayOfChar2[1769] = 165;
        arrayOfChar2[1770] = 151;
        arrayOfChar2[1771] = 135;
        arrayOfChar2[1772] = 135;
        arrayOfChar2[1773] = 136;
        arrayOfChar2[1774] = 134;
        arrayOfChar2[1775] = 150;
        arrayOfChar2[1776] = 164;
        arrayOfChar2[1777] = 195;
        arrayOfChar2[1778] = 165;
        arrayOfChar2[1779] = 165;
        arrayOfChar2[1780] = 165;
        arrayOfChar2[1781] = 166;
        arrayOfChar2[1782] = 151;
        arrayOfChar2[1783] = 135;
        arrayOfChar2[1784] = 135;
        arrayOfChar2[1785] = 120;
        arrayOfChar2[1786] = 135;
        arrayOfChar2[1787] = 134;
        arrayOfChar2[1788] = 165;
        arrayOfChar2[1789] = 195;
        arrayOfChar2[1790] = 165;
        arrayOfChar2[1791] = 181;
        arrayOfChar2[1792] = 166;
        arrayOfChar2[1793] = 166;
        arrayOfChar2[1794] = 135;
        arrayOfChar2[1795] = 136;
        arrayOfChar2[1796] = 120;
        arrayOfChar2[1797] = 120;
        arrayOfChar2[1798] = 135;
        arrayOfChar2[1799] = 135;
    }

    public static final long[] calLunar(int paramInt1, int paramInt2, int paramInt3)
    {
        long[] arrayOfLong = new long[7];
        int i = 0;
        long l = getDayOffset(paramInt1, paramInt2, paramInt3);
        arrayOfLong[5] = (40L + l);
        arrayOfLong[4] = 14L;
        for (int j = 1900; (j < 2050) && (l > 0L); j++)
        {
            i = yrDays(j);
            l -= i;
            arrayOfLong[4] = (12L + arrayOfLong[4]);
        }
        if (l < 0L)
        {
            l += i;
            j--;
            arrayOfLong[4] -= 12L;
        }
        arrayOfLong[0] = j;
        arrayOfLong[3] = (j - 1864);
        int k = rMonth(j);
        arrayOfLong[6] = 0L;
        int m = 1;
        if ((m < 13) && (l > 0L))
        {
            if ((k > 0) && (m == k + 1) && (arrayOfLong[6] == 0L))
            {
                m--;
                arrayOfLong[6] = 1L;
            }
            for (i = rMthDays((int)arrayOfLong[0]); ; i = mthDays((int)arrayOfLong[0], m))
            {
                if ((arrayOfLong[6] == 1L) && (m == k + 1))
                    arrayOfLong[6] = 0L;
                l -= i;
                if (arrayOfLong[6] == 0L)
                    arrayOfLong[4] = (1L + arrayOfLong[4]);
                m++;
                break;
            }
        }
        if ((l == 0L) && (k > 0) && (m == k + 1))
        {
            if (arrayOfLong[6] != 1L)
                break label340;
            arrayOfLong[6] = 0L;
        }
        while (true)
        {
            if (l < 0L)
            {
                l += i;
                m--;
                arrayOfLong[4] -= 1L;
            }
            arrayOfLong[1] = m;
            arrayOfLong[2] = (1L + l);
            return arrayOfLong;
            label340: arrayOfLong[6] = 1L;
            m--;
            arrayOfLong[4] -= 1L;
        }
    }

    public static String formatLunarDate(int paramInt1, int paramInt2, int paramInt3)
    {
        StringBuilder localStringBuilder = new StringBuilder();
        if (paramInt1 > 0)
        {
            localStringBuilder.append(paramInt1);
            localStringBuilder.append("-");
        }
        localStringBuilder.append(paramInt2 + 1);
        localStringBuilder.append("-");
        localStringBuilder.append(paramInt3);
        return localStringBuilder.toString();
    }

    private static final int getDayOffset(int paramInt1, int paramInt2, int paramInt3)
    {
        int i = 0;
        GregorianCalendar localGregorianCalendar = (GregorianCalendar)Calendar.getInstance();
        localGregorianCalendar.clear();
        int j = 1900;
        if (j < paramInt1)
        {
            if (localGregorianCalendar.isLeapYear(j))
                i += 366;
            while (true)
            {
                j++;
                break;
                i += 365;
            }
        }
        localGregorianCalendar.set(paramInt1, paramInt2, paramInt3);
        int k = i + localGregorianCalendar.get(6);
        localGregorianCalendar.set(1900, 0, 31);
        return k - localGregorianCalendar.get(6);
    }

    public static final String getDayString(Resources paramResources, int paramInt)
    {
        String str1 = "";
        String str2;
        if (paramInt == 10)
            str2 = paramResources.getString(101449809);
        while (true)
        {
            return str2;
            if (paramInt == 20)
            {
                str2 = paramResources.getString(101449810);
            }
            else
            {
                if (paramInt != 30)
                    break;
                str2 = paramResources.getString(101449811);
            }
        }
        int i = paramInt / 10;
        if (i == 0)
            str1 = paramResources.getString(101449812);
        if (i == 1)
            str1 = paramResources.getString(101449806);
        if (i == 2)
            str1 = paramResources.getString(101449813);
        if (i == 3)
            str1 = paramResources.getString(101449799);
        switch (paramInt % 10)
        {
        default:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
        }
        while (true)
        {
            str2 = str1;
            break;
            str1 = str1 + paramResources.getString(101449797);
            continue;
            str1 = str1 + paramResources.getString(101449798);
            continue;
            str1 = str1 + paramResources.getString(101449799);
            continue;
            str1 = str1 + paramResources.getString(101449800);
            continue;
            str1 = str1 + paramResources.getString(101449801);
            continue;
            str1 = str1 + paramResources.getString(101449802);
            continue;
            str1 = str1 + paramResources.getString(101449803);
            continue;
            str1 = str1 + paramResources.getString(101449804);
            continue;
            str1 = str1 + paramResources.getString(101449805);
        }
    }

    private static String getDigitString(Resources paramResources, int paramInt)
    {
        String str;
        switch (paramInt)
        {
        default:
            str = "";
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
        }
        while (true)
        {
            return str;
            str = paramResources.getString(101449796);
            continue;
            str = paramResources.getString(101449797);
            continue;
            str = paramResources.getString(101449798);
            continue;
            str = paramResources.getString(101449799);
            continue;
            str = paramResources.getString(101449800);
            continue;
            str = paramResources.getString(101449801);
            continue;
            str = paramResources.getString(101449802);
            continue;
            str = paramResources.getString(101449803);
            continue;
            str = paramResources.getString(101449804);
            continue;
            str = paramResources.getString(101449805);
        }
    }

    public static String getHoliday(Resources paramResources, long[] paramArrayOfLong, Calendar paramCalendar, String paramString)
    {
        while (true)
        {
            int m;
            try
            {
                int i = 1 + paramCalendar.get(2);
                int j = paramCalendar.get(5);
                int[] arrayOfInt1;
                int[] arrayOfInt2;
                if (Build.IS_TW_BUILD)
                {
                    arrayOfInt1 = solarHolidaysTable_TW;
                    arrayOfInt2 = solarHolidays_TW;
                    int k = arrayOfInt1.length;
                    m = 0;
                    if (m < k)
                    {
                        if ((arrayOfInt1[m] / 100 != i) || (arrayOfInt1[m] % 100 != j))
                            break label207;
                        localObject2 = paramResources.getString(arrayOfInt2[m]);
                        break label204;
                    }
                }
                else
                {
                    arrayOfInt1 = solarHolidaysTable;
                    arrayOfInt2 = solarHolidays;
                    continue;
                }
                if (paramArrayOfLong[6] == 1L)
                {
                    localObject2 = null;
                    break label204;
                }
                int n = (int)paramArrayOfLong[1];
                int i1 = (int)paramArrayOfLong[2];
                int i2 = lunarHolidaysTable.length;
                int i3 = 0;
                if (i3 < i2)
                {
                    if ((lunarHolidaysTable[i3] / 100 == n) && (lunarHolidaysTable[i3] % 100 == i1))
                    {
                        String str = paramResources.getString(lunarHolidays[i3]);
                        localObject2 = str;
                        break label204;
                    }
                    i3++;
                }
            }
            finally
            {
            }
            Object localObject2 = null;
            label204: return localObject2;
            label207: m++;
        }
    }

    public static int[][] getLunarBirthdays(int paramInt1, int paramInt2, int paramInt3)
    {
        int i = 0;
        int j = paramInt2 + 1;
        int[][] arrayOfInt;
        int i1;
        label105: label108: int m;
        int[] arrayOfInt2;
        if (j > 12)
            if (j - 12 == rMonth(paramInt1))
            {
                arrayOfInt = new int[2][];
                int n = Math.min(rMthDays(paramInt1), paramInt3);
                int[] arrayOfInt3 = lunarToSolar(paramInt1, j, n);
                int[] arrayOfInt4 = new int[4];
                arrayOfInt4[i] = arrayOfInt3[0];
                arrayOfInt4[1] = (-1 + arrayOfInt3[1]);
                arrayOfInt4[2] = arrayOfInt3[2];
                if (n == paramInt3)
                {
                    i1 = 0;
                    arrayOfInt4[3] = i1;
                    arrayOfInt[i] = arrayOfInt4;
                    j -= 12;
                    int k = Math.min(mthDays(paramInt1, j), paramInt3);
                    int[] arrayOfInt1 = lunarToSolar(paramInt1, j, k);
                    m = -1 + arrayOfInt.length;
                    arrayOfInt2 = new int[4];
                    arrayOfInt2[i] = arrayOfInt1[0];
                    arrayOfInt2[1] = (-1 + arrayOfInt1[1]);
                    arrayOfInt2[2] = arrayOfInt1[2];
                    if (k != paramInt3)
                        break label215;
                }
            }
        while (true)
        {
            arrayOfInt2[3] = i;
            arrayOfInt[m] = arrayOfInt2;
            return arrayOfInt;
            i1 = 1;
            break;
            arrayOfInt = new int[1][];
            break label105;
            arrayOfInt = new int[1][];
            break label108;
            label215: i = 1;
        }
    }

    private static int getLunarNewYearOffsetDays(int paramInt1, int paramInt2, int paramInt3)
    {
        int i = 0;
        int j = rMonth(paramInt1);
        if ((j > 0) && (j == paramInt2 - 12))
        {
            paramInt2 = j;
            i = 0 + mthDays(paramInt1, paramInt2);
        }
        for (int k = 1; k < paramInt2; k++)
        {
            i += mthDays(paramInt1, k);
            if (k == j)
                i += rMthDays(paramInt1);
        }
        return i + (paramInt3 - 1);
    }

    public static String getLunarString(Resources paramResources, int paramInt1, int paramInt2, int paramInt3)
    {
        StringBuilder localStringBuilder = new StringBuilder();
        if (paramInt1 > 0)
            localStringBuilder.append(Integer.toString(paramInt1)).append(paramResources.getString(101449815));
        if (paramInt2 >= 12)
        {
            localStringBuilder.append(paramResources.getString(101449795));
            paramInt2 -= 12;
        }
        localStringBuilder.append(getMonthString(paramResources, paramInt2 + 1));
        localStringBuilder.append(paramResources.getString(101449814));
        localStringBuilder.append(getDayString(paramResources, paramInt3));
        return localStringBuilder.toString();
    }

    public static String getMonthString(Resources paramResources, int paramInt)
    {
        String str = null;
        if (paramInt > 12);
        while (true)
        {
            return str;
            switch (paramInt)
            {
            default:
                break;
            case 0:
                str = "";
                break;
            case 1:
                str = paramResources.getString(101449794);
                break;
            case 2:
                str = paramResources.getString(101449798);
                break;
            case 3:
                str = paramResources.getString(101449799);
                break;
            case 4:
                str = paramResources.getString(101449800);
                break;
            case 5:
                str = paramResources.getString(101449801);
                break;
            case 6:
                str = paramResources.getString(101449802);
                break;
            case 7:
                str = paramResources.getString(101449803);
                break;
            case 8:
                str = paramResources.getString(101449804);
                break;
            case 9:
                str = paramResources.getString(101449805);
                break;
            case 10:
                str = paramResources.getString(101449806);
                break;
            case 11:
                str = paramResources.getString(101449807);
                break;
            case 12:
                str = paramResources.getString(101449808);
            }
        }
    }

    public static long getNextLunarBirthday(int paramInt1, int paramInt2)
    {
        Time localTime = new Time();
        localTime.setToNow();
        int i = (int)calLunar(localTime.year, localTime.month, localTime.monthDay)[0];
        localTime.second = 0;
        localTime.minute = 0;
        localTime.hour = 0;
        long l1 = localTime.normalize(false);
        long l2 = 9223372036854775807L;
        while ((9223372036854775807L == l2) && (i >= 1900) && (i < 2050))
        {
            for (int[] arrayOfInt1 : getLunarBirthdays(i, paramInt1, paramInt2))
            {
                localTime.set(arrayOfInt1[2], arrayOfInt1[1], arrayOfInt1[0]);
                long l3 = localTime.normalize(false);
                if (l3 >= l1)
                    l2 = Math.min(l2, l3);
            }
            i++;
        }
        if (9223372036854775807L == l2)
            l2 = 0L;
        return l2;
    }

    public static String getSolarTerm(Resources paramResources, Calendar paramCalendar)
    {
        int i = paramCalendar.get(1);
        int j = paramCalendar.get(2);
        int k = paramCalendar.get(5);
        int m = solarTermsTable[(j + 12 * (i - 1901))];
        String str;
        if (k == 15 + m % 16)
            str = paramResources.getString(solarTerms[(1 + j * 2)]);
        while (true)
        {
            return str;
            if (k == 15 - m / 16)
                str = paramResources.getString(solarTerms[(j * 2)]);
            else
                str = null;
        }
    }

    static int getSolarYearMonthDays(int paramInt1, int paramInt2)
    {
        int i;
        if ((paramInt2 == 1) || (paramInt2 == 3) || (paramInt2 == 5) || (paramInt2 == 7) || (paramInt2 == 8) || (paramInt2 == 10) || (paramInt2 == 12))
            i = 31;
        while (true)
        {
            return i;
            if ((paramInt2 == 4) || (paramInt2 == 6) || (paramInt2 == 9) || (paramInt2 == 11))
                i = 30;
            else if (paramInt2 == 2)
            {
                if (isSolarLeapYear(paramInt1))
                    i = 29;
                else
                    i = 28;
            }
            else
                i = 0;
        }
    }

    public static String getString(Resources paramResources, Calendar paramCalendar)
    {
        return solar2lunar(paramResources, paramCalendar.get(1), paramCalendar.get(2), paramCalendar.get(5));
    }

    public static String getYearString(Resources paramResources, int paramInt)
    {
        StringBuffer localStringBuffer = new StringBuffer();
        int i = paramInt;
        do
        {
            int j = i % 10;
            i /= 10;
            localStringBuffer.insert(0, getDigitString(paramResources, j));
        }
        while (i > 0);
        return localStringBuffer.toString();
    }

    static boolean isSolarLeapYear(int paramInt)
    {
        if (((paramInt % 4 == 0) && (paramInt % 100 != 0)) || (paramInt % 400 == 0));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static int[] lunarToSolar(int paramInt1, int paramInt2, int paramInt3)
    {
        int[] arrayOfInt = new int[3];
        int i = getLunarNewYearOffsetDays(paramInt1, paramInt2, paramInt3) + iSolarLunarOffsetTable[(paramInt1 - 1901)];
        int j;
        int k;
        if (isSolarLeapYear(paramInt1))
        {
            j = 366;
            if (i < j)
                break label99;
            k = paramInt1 + 1;
            i -= j;
        }
        int m;
        int n;
        while (true)
        {
            m = i + 1;
            for (n = 1; i >= 0; n++)
            {
                m = i + 1;
                i -= getSolarYearMonthDays(k, n);
            }
            j = 365;
            break;
            label99: k = paramInt1;
        }
        int i1 = n - 1;
        arrayOfInt[0] = k;
        arrayOfInt[1] = i1;
        arrayOfInt[2] = m;
        return arrayOfInt;
    }

    public static final int mthDays(int paramInt1, int paramInt2)
    {
        if ((luYearData[(paramInt1 - 1900)] & 65536 >> paramInt2) == 0L);
        for (int i = 29; ; i = 30)
            return i;
    }

    public static int[] parseLunarDate(String paramString)
    {
        int[] arrayOfInt;
        if (TextUtils.isEmpty(paramString))
            arrayOfInt = null;
        while (true)
        {
            return arrayOfInt;
            arrayOfInt = new int[3];
            String[] arrayOfString;
            try
            {
                arrayOfString = paramString.split("-");
                if (arrayOfString.length != 2)
                    break label73;
                arrayOfInt[0] = Integer.parseInt(arrayOfString[1].trim());
                arrayOfInt[1] = (-1 + Integer.parseInt(arrayOfString[0].trim()));
                arrayOfInt[2] = 0;
            }
            catch (NumberFormatException localNumberFormatException)
            {
                localNumberFormatException.printStackTrace();
                arrayOfInt = null;
            }
            continue;
            label73: if (arrayOfString.length == 3)
            {
                arrayOfInt[0] = Integer.parseInt(arrayOfString[2].trim());
                arrayOfInt[1] = (-1 + Integer.parseInt(arrayOfString[1].trim()));
                arrayOfInt[2] = Integer.parseInt(arrayOfString[0].trim());
            }
            else
            {
                arrayOfInt = null;
            }
        }
    }

    public static final int rMonth(int paramInt)
    {
        return (int)(0xF & luYearData[(paramInt - 1900)]);
    }

    public static final int rMthDays(int paramInt)
    {
        int i;
        if (rMonth(paramInt) != 0)
            if ((0x10000 & luYearData[(paramInt - 1900)]) != 0L)
                i = 30;
        while (true)
        {
            return i;
            i = 29;
            continue;
            i = 0;
        }
    }

    public static String solar2lunar(Resources paramResources, int paramInt1, int paramInt2, int paramInt3)
    {
        long[] arrayOfLong = calLunar(paramInt1, paramInt2, paramInt3);
        StringBuffer localStringBuffer = new StringBuffer();
        try
        {
            if (arrayOfLong[6] == 1L)
                localStringBuffer.append(paramResources.getString(101449795));
            localStringBuffer.append(getMonthString(paramResources, (int)arrayOfLong[1]));
            localStringBuffer.append(paramResources.getString(101449814));
            localStringBuffer.append(getDayString(paramResources, (int)arrayOfLong[2]));
            String str = localStringBuffer.toString();
            return str;
        }
        finally
        {
        }
    }

    private static final int yrDays(int paramInt)
    {
        int i = 348;
        int j = 32768;
        while (j > 8)
        {
            if ((luYearData[(paramInt - 1900)] & j) != 0L)
                i++;
            j >>= 1;
        }
        return i + rMthDays(paramInt);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.util.LunarDate
 * JD-Core Version:        0.6.2
 */