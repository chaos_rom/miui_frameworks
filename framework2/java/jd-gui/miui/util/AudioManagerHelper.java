package miui.util;

import android.content.ContentResolver;
import android.content.Context;
import android.media.AudioManager;
import android.provider.Settings.System;
import miui.provider.ExtraSettings.System;

public class AudioManagerHelper
{
    public static boolean isSilentEnabled(Context paramContext)
    {
        if (((AudioManager)paramContext.getSystemService("audio")).getRingerMode() != 2);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static boolean isVibrateEnabled(Context paramContext)
    {
        int i = 1;
        int j = ((AudioManager)paramContext.getSystemService("audio")).getRingerMode();
        if (2 != j)
            if (i != j);
        while (true)
        {
            return i;
            i = 0;
            continue;
            i = ExtraSettings.System.getBoolean(paramContext.getContentResolver(), "vibrate_in_normal", false);
        }
    }

    public static void saveLastAudibleRingVolume(Context paramContext, int paramInt)
    {
        if (paramInt > 0)
            Settings.System.putInt(paramContext.getContentResolver(), "last_audible_ring_volume", paramInt);
    }

    public static void setVibrateSetting(Context paramContext, boolean paramBoolean1, boolean paramBoolean2)
    {
        ContentResolver localContentResolver = paramContext.getContentResolver();
        if (paramBoolean2);
        for (String str = "vibrate_in_silent"; ; str = "vibrate_in_normal")
        {
            ExtraSettings.System.putBoolean(localContentResolver, str, paramBoolean1);
            validateRingerMode(paramContext);
            return;
        }
    }

    public static void toggleSilent(Context paramContext, int paramInt)
    {
        AudioManager localAudioManager = (AudioManager)paramContext.getSystemService("audio");
        if (2 == localAudioManager.getRingerMode());
        for (int i = 0; ; i = 2)
        {
            localAudioManager.setRingerMode(i);
            if (2 == i)
            {
                int j = Settings.System.getInt(paramContext.getContentResolver(), "last_audible_ring_volume", 0);
                if (j > 0)
                    localAudioManager.setStreamVolume(2, j, 0);
            }
            validateRingerMode(paramContext);
            if (paramInt != 0)
                localAudioManager.adjustStreamVolume(2, 0, paramInt);
            return;
        }
    }

    public static void toggleVibrateSetting(Context paramContext)
    {
        if (!isVibrateEnabled(paramContext));
        for (boolean bool = true; ; bool = false)
        {
            setVibrateSetting(paramContext, bool, isSilentEnabled(paramContext));
            return;
        }
    }

    private static void validateRingerMode(Context paramContext)
    {
        AudioManager localAudioManager = (AudioManager)paramContext.getSystemService("audio");
        ContentResolver localContentResolver = paramContext.getContentResolver();
        int i = localAudioManager.getRingerMode();
        if (2 == i)
            ExtraSettings.System.getBoolean(localContentResolver, "vibrate_in_normal", false);
        while (true)
        {
            return;
            boolean bool = ExtraSettings.System.getBoolean(localContentResolver, "vibrate_in_silent", true);
            if (i == 0)
            {
                if (bool)
                    localAudioManager.setRingerMode(1);
            }
            else if (!bool)
                localAudioManager.setRingerMode(0);
        }
    }

    public static void validateVibrateSettings(Context paramContext)
    {
        AudioManager localAudioManager = (AudioManager)paramContext.getSystemService("audio");
        ContentResolver localContentResolver = paramContext.getContentResolver();
        if (2 == localAudioManager.getRingerMode());
        for (String str = "vibrate_in_normal"; ; str = "vibrate_in_silent")
        {
            ExtraSettings.System.putBoolean(localContentResolver, str, isVibrateEnabled(paramContext));
            return;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.util.AudioManagerHelper
 * JD-Core Version:        0.6.2
 */