package miui.util;

import com.android.internal.app.LocalePicker.LocaleInfo;
import java.util.Locale;

public class ExtraLocalePicker
{
    public static void adjustLocaleOrder(LocalePicker.LocaleInfo[] paramArrayOfLocaleInfo)
    {
        moveLocaleToFirst(paramArrayOfLocaleInfo, "en_US");
        moveLocaleToFirst(paramArrayOfLocaleInfo, "zh_TW");
        moveLocaleToFirst(paramArrayOfLocaleInfo, "zh_CN");
    }

    private static void moveLocaleToFirst(LocalePicker.LocaleInfo[] paramArrayOfLocaleInfo, String paramString)
    {
        for (int i = 0; ; i++)
        {
            if (i < paramArrayOfLocaleInfo.length)
            {
                if (!paramString.equals(paramArrayOfLocaleInfo[i].getLocale().toString()))
                    continue;
                if (i != 0)
                    break label29;
            }
            while (true)
            {
                return;
                label29: LocalePicker.LocaleInfo localLocaleInfo = paramArrayOfLocaleInfo[i];
                for (int j = i; j > 0; j--)
                    paramArrayOfLocaleInfo[j] = paramArrayOfLocaleInfo[(j - 1)];
                paramArrayOfLocaleInfo[0] = localLocaleInfo;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.util.ExtraLocalePicker
 * JD-Core Version:        0.6.2
 */