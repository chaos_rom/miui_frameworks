package miui.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.provider.Settings.Secure;
import android.util.Log;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import miui.os.Build;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONObject;

public class ErrorReportUtils
{
    public static final int ANR_MAX_LINE_NUMBER = 300;
    public static final String ERROR_TYPE_ANR = "anr";
    public static final String ERROR_TYPE_FC = "fc";
    public static final String JSON_ANR_ACTIVITY = "anr_activity";
    public static final String JSON_ANR_CAUSE = "anr_cause";
    public static final String JSON_APP_VESION = "app_version";
    public static final String JSON_BUILD_VERSION = "build_version";
    public static final String JSON_DEVICE = "device";
    public static final String JSON_ERROR_TYPE = "error_type";
    public static final String JSON_EXCEPTION_CLASS = "exception_class";
    public static final String JSON_EXCEPTION_SOURCE_METHOD = "exception_source_method";
    public static final String JSON_IMEI = "imei";
    public static final String JSON_NETWORK = "network";
    public static final String JSON_PACKAGE_NAME = "package_name";
    public static final String JSON_PLATFORM = "platform";
    public static final String JSON_STACK_TRACK = "stack_track";
    private static final String SALT_P1 = "8007236f-";
    private static final String SALT_P2 = "a2d6-4847-ac83-";
    private static final String SALT_P3 = "c49395ad6d65";

    private static String getKeyFromParams(List<NameValuePair> paramList)
    {
        Collections.sort(paramList, new Comparator()
        {
            public int compare(NameValuePair paramAnonymousNameValuePair1, NameValuePair paramAnonymousNameValuePair2)
            {
                return paramAnonymousNameValuePair1.getName().compareTo(paramAnonymousNameValuePair2.getName());
            }
        });
        StringBuilder localStringBuilder = new StringBuilder();
        int i = 1;
        Iterator localIterator = paramList.iterator();
        while (localIterator.hasNext())
        {
            NameValuePair localNameValuePair = (NameValuePair)localIterator.next();
            if (i == 0)
                localStringBuilder.append("&");
            localStringBuilder.append(localNameValuePair.getName()).append("=").append(localNameValuePair.getValue());
            i = 0;
        }
        localStringBuilder.append("&").append("8007236f-");
        localStringBuilder.append("a2d6-4847-ac83-");
        localStringBuilder.append("c49395ad6d65");
        return CommonUtils.getMd5Digest(new String(Base64Coder.encode(CommonUtils.getBytes(localStringBuilder.toString()))));
    }

    public static boolean isNetworkAvailable(Context paramContext)
    {
        NetworkInfo localNetworkInfo = ((ConnectivityManager)paramContext.getSystemService("connectivity")).getActiveNetworkInfo();
        if ((localNetworkInfo != null) && (localNetworkInfo.isAvailable()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static boolean isUserAllowed(Context paramContext)
    {
        int i = 1;
        if (Build.isDevelopmentVersion())
        {
            int j = i;
            if (Settings.Secure.getInt(paramContext.getContentResolver(), "upload_log_pref", j) == 0)
                break label30;
        }
        while (true)
        {
            return i;
            int k = 0;
            break;
            label30: i = 0;
        }
    }

    public static boolean isWifiConnected(Context paramContext)
    {
        int i = 1;
        NetworkInfo localNetworkInfo = ((ConnectivityManager)paramContext.getSystemService("connectivity")).getActiveNetworkInfo();
        if ((localNetworkInfo != null) && (localNetworkInfo.getType() == i));
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    public static boolean postErrorReport(Context paramContext, JSONObject paramJSONObject)
    {
        if (isNetworkAvailable(paramContext))
            new ReportErrorTask(paramJSONObject).execute((Void[])null);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static class Base64Coder
    {
        private static char[] map1 = new char[64];
        private static byte[] map2;

        static
        {
            int i = 65;
            int i5;
            for (int j = 0; i <= 90; j = i5)
            {
                char[] arrayOfChar5 = map1;
                i5 = j + 1;
                arrayOfChar5[j] = i;
                i = (char)(i + 1);
            }
            int k = 97;
            while (k <= 122)
            {
                char[] arrayOfChar4 = map1;
                int i4 = j + 1;
                arrayOfChar4[j] = k;
                k = (char)(k + 1);
                j = i4;
            }
            int m = 48;
            while (m <= 57)
            {
                char[] arrayOfChar3 = map1;
                int i3 = j + 1;
                arrayOfChar3[j] = m;
                m = (char)(m + 1);
                j = i3;
            }
            char[] arrayOfChar1 = map1;
            int n = j + 1;
            arrayOfChar1[j] = '+';
            char[] arrayOfChar2 = map1;
            (n + 1);
            arrayOfChar2[n] = '/';
            map2 = new byte[''];
            for (int i1 = 0; i1 < map2.length; i1++)
                map2[i1] = -1;
            for (int i2 = 0; i2 < 64; i2++)
                map2[map1[i2]] = ((byte)i2);
        }

        public static char[] encode(byte[] paramArrayOfByte)
        {
            return encode(paramArrayOfByte, 0, paramArrayOfByte.length);
        }

        public static char[] encode(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
        {
            int i = (2 + paramInt2 * 4) / 3;
            char[] arrayOfChar = new char[4 * ((paramInt2 + 2) / 3)];
            int j = paramInt1 + paramInt2;
            int k = 0;
            int m = paramInt1;
            if (m < j)
            {
                int n = m + 1;
                int i1 = 0xFF & paramArrayOfByte[m];
                int i3;
                int i2;
                label76: int i5;
                int i4;
                label99: int i9;
                int i12;
                label188: int i13;
                if (n < j)
                {
                    i3 = n + 1;
                    i2 = 0xFF & paramArrayOfByte[n];
                    if (i3 >= j)
                        break label245;
                    i5 = i3 + 1;
                    i4 = 0xFF & paramArrayOfByte[i3];
                    int i6 = i1 >>> 2;
                    int i7 = (i1 & 0x3) << 4 | i2 >>> 4;
                    int i8 = (i2 & 0xF) << 2 | i4 >>> 6;
                    i9 = i4 & 0x3F;
                    int i10 = k + 1;
                    arrayOfChar[k] = map1[i6];
                    int i11 = i10 + 1;
                    arrayOfChar[i10] = map1[i7];
                    if (i11 >= i)
                        break label255;
                    i12 = map1[i8];
                    arrayOfChar[i11] = i12;
                    i13 = i11 + 1;
                    if (i13 >= i)
                        break label262;
                }
                label262: for (int i14 = map1[i9]; ; i14 = 61)
                {
                    arrayOfChar[i13] = i14;
                    k = i13 + 1;
                    m = i5;
                    break;
                    i2 = 0;
                    i3 = n;
                    break label76;
                    label245: i4 = 0;
                    i5 = i3;
                    break label99;
                    label255: i12 = 61;
                    break label188;
                }
            }
            return arrayOfChar;
        }
    }

    private static class CommonUtils
    {
        public static byte[] getBytes(String paramString)
        {
            try
            {
                byte[] arrayOfByte2 = paramString.getBytes("UTF-8");
                arrayOfByte1 = arrayOfByte2;
                return arrayOfByte1;
            }
            catch (UnsupportedEncodingException localUnsupportedEncodingException)
            {
                while (true)
                    byte[] arrayOfByte1 = paramString.getBytes();
            }
        }

        public static String getMd5Digest(String paramString)
        {
            try
            {
                MessageDigest localMessageDigest = MessageDigest.getInstance("MD5");
                localMessageDigest.update(getBytes(paramString));
                BigInteger localBigInteger = new BigInteger(1, localMessageDigest.digest());
                Object[] arrayOfObject = new Object[1];
                arrayOfObject[0] = localBigInteger;
                String str = String.format("%1$032X", arrayOfObject);
                return str;
            }
            catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
            {
                throw new RuntimeException(localNoSuchAlgorithmException);
            }
        }
    }

    public static class ReportErrorTask extends AsyncTask<Void, Void, Void>
    {
        private JSONObject mJsPost;

        public ReportErrorTask(JSONObject paramJSONObject)
        {
            this.mJsPost = paramJSONObject;
        }

        protected Void doInBackground(Void[] paramArrayOfVoid)
        {
            try
            {
                HttpPost localHttpPost = new HttpPost("http://api.chat.xiaomi.net/v2/miui/feedback");
                localHttpPost.setHeader("Content-Type", "application/x-www-form-urlencoded;charset=utf-8");
                LinkedList localLinkedList = new LinkedList();
                localLinkedList.add(new BasicNameValuePair("error", this.mJsPost.toString()));
                localLinkedList.add(new BasicNameValuePair("s", ErrorReportUtils.getKeyFromParams(localLinkedList)));
                localHttpPost.setEntity(new UrlEncodedFormEntity(localLinkedList, "UTF-8"));
                BasicHttpParams localBasicHttpParams = new BasicHttpParams();
                HttpConnectionParams.setConnectionTimeout(localBasicHttpParams, 3000);
                HttpConnectionParams.setSoTimeout(localBasicHttpParams, 5000);
                int i = new DefaultHttpClient(localBasicHttpParams).execute(localHttpPost).getStatusLine().getStatusCode();
                if (i != 200)
                    Log.w(ReportErrorTask.class.getSimpleName(), "failed to report errors to miui. status code:" + i);
                return null;
            }
            catch (UnsupportedEncodingException localUnsupportedEncodingException)
            {
                while (true)
                    localUnsupportedEncodingException.printStackTrace();
            }
            catch (ClientProtocolException localClientProtocolException)
            {
                while (true)
                    localClientProtocolException.printStackTrace();
            }
            catch (IOException localIOException)
            {
                while (true)
                    localIOException.printStackTrace();
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.util.ErrorReportUtils
 * JD-Core Version:        0.6.2
 */