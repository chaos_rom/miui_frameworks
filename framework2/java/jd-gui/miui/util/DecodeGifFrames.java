package miui.util;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;

class DecodeGifFrames extends Handler
{
    private static final int MESSAGE_WHAT_START = 1;
    protected static final String TAG = "DecodeGifFrames";
    private Handler mCallerHandler;
    DecodeGifImageHelper.GifDecodeResult mDecodeResult = null;
    private InputStreamLoader mGifSource;
    HandlerThread mHandlerThread;
    private long mMaxDecodeSize;

    public DecodeGifFrames(HandlerThread paramHandlerThread, InputStreamLoader paramInputStreamLoader, long paramLong, Handler paramHandler)
    {
        super(paramHandlerThread.getLooper());
        this.mHandlerThread = paramHandlerThread;
        this.mMaxDecodeSize = paramLong;
        this.mGifSource = paramInputStreamLoader;
        this.mCallerHandler = paramHandler;
    }

    public static DecodeGifFrames createInstance(InputStreamLoader paramInputStreamLoader, long paramLong, Handler paramHandler)
    {
        HandlerThread localHandlerThread = new HandlerThread("handler thread to decode GIF frames");
        localHandlerThread.start();
        return new DecodeGifFrames(localHandlerThread, paramInputStreamLoader, paramLong, paramHandler);
    }

    public void decode(int paramInt)
    {
        if (this.mDecodeResult != null);
        while (true)
        {
            return;
            this.mDecodeResult = new DecodeGifImageHelper.GifDecodeResult();
            sendMessage(obtainMessage(1, paramInt, 0));
        }
    }

    public void destroy()
    {
        this.mHandlerThread.quit();
    }

    protected void finalize()
        throws Throwable
    {
        this.mHandlerThread.quit();
        super.finalize();
    }

    public DecodeGifImageHelper.GifDecodeResult getAndClearDecodeResult()
    {
        DecodeGifImageHelper.GifDecodeResult localGifDecodeResult = this.mDecodeResult;
        this.mDecodeResult = null;
        return localGifDecodeResult;
    }

    public void handleMessage(Message paramMessage)
    {
        if (paramMessage.what == 1)
        {
            int i = paramMessage.arg1;
            DecodeGifImageHelper.GifDecodeResult localGifDecodeResult = DecodeGifImageHelper.decode(this.mGifSource, this.mMaxDecodeSize, i);
            this.mDecodeResult.mGifDecoder = localGifDecodeResult.mGifDecoder;
            this.mDecodeResult.mIsDecodeOk = localGifDecodeResult.mIsDecodeOk;
            this.mCallerHandler.sendEmptyMessage(1);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.util.DecodeGifFrames
 * JD-Core Version:        0.6.2
 */