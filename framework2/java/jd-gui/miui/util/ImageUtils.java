package miui.util;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Paint;
import java.io.FileOutputStream;
import libcore.io.Streams;

public class ImageUtils
{
    private static byte[] PNG_HEAD_FORMAT = arrayOfByte;

    static
    {
        byte[] arrayOfByte = new byte[8];
        arrayOfByte[0] = -119;
        arrayOfByte[1] = 80;
        arrayOfByte[2] = 78;
        arrayOfByte[3] = 71;
        arrayOfByte[4] = 13;
        arrayOfByte[5] = 10;
        arrayOfByte[6] = 26;
        arrayOfByte[7] = 10;
    }

    public static int computeSampleSize(InputStreamLoader paramInputStreamLoader, int paramInt)
    {
        int i = 1;
        if (paramInt > 0)
        {
            BitmapFactory.Options localOptions = getBitmapSize(paramInputStreamLoader);
            double d = Math.sqrt(localOptions.outWidth * localOptions.outHeight / paramInt);
            while (i * 2 <= d)
                i <<= 1;
        }
        return i;
    }

    public static boolean cropBitmapToAnother(Bitmap paramBitmap1, Bitmap paramBitmap2, boolean paramBoolean)
    {
        boolean bool = true;
        if ((paramBitmap1 != null) && (paramBitmap2 != null))
        {
            int i = paramBitmap1.getWidth();
            int j = paramBitmap1.getHeight();
            int k = paramBitmap2.getWidth();
            int m = paramBitmap2.getHeight();
            float f = Math.max(1.0F * k / i, 1.0F * m / j);
            Paint localPaint = new Paint();
            localPaint.setFilterBitmap(bool);
            localPaint.setAntiAlias(bool);
            localPaint.setDither(bool);
            Canvas localCanvas = new Canvas(paramBitmap2);
            localCanvas.translate((k - f * i) / 2.0F, (m - f * j) / 2.0F);
            localCanvas.scale(f, f);
            localCanvas.drawBitmap(paramBitmap1, 0.0F, 0.0F, localPaint);
            if (paramBoolean)
                paramBitmap1.recycle();
        }
        while (true)
        {
            return bool;
            bool = false;
        }
    }

    public static final Bitmap getBitmap(InputStreamLoader paramInputStreamLoader, int paramInt)
    {
        BitmapFactory.Options localOptions = getDefaultOptions();
        localOptions.inSampleSize = computeSampleSize(paramInputStreamLoader, paramInt);
        Object localObject1 = null;
        int i = 0;
        while (true)
        {
            int j = i + 1;
            if (i < 3);
            try
            {
                Bitmap localBitmap = BitmapFactory.decodeStream(paramInputStreamLoader.get(), null, localOptions);
                localObject1 = localBitmap;
                return localObject1;
            }
            catch (OutOfMemoryError localOutOfMemoryError)
            {
                System.gc();
                localOptions.inSampleSize = (2 * localOptions.inSampleSize);
                paramInputStreamLoader.close();
                i = j;
            }
            catch (Exception localException)
            {
                while (true)
                    paramInputStreamLoader.close();
            }
            finally
            {
                paramInputStreamLoader.close();
            }
        }
    }

    public static Bitmap getBitmap(InputStreamLoader paramInputStreamLoader, int paramInt1, int paramInt2)
    {
        int i = 2 * (paramInt1 * paramInt2);
        if ((paramInt1 <= 0) || (paramInt2 <= 0))
            i = -1;
        Bitmap localBitmap = getBitmap(paramInputStreamLoader, i);
        if (i > 0)
            localBitmap = scaleBitmapToDesire(localBitmap, paramInt1, paramInt2, true);
        return localBitmap;
    }

    // ERROR //
    public static Bitmap getBitmap(InputStreamLoader paramInputStreamLoader, int paramInt1, int paramInt2, Bitmap paramBitmap)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore 4
        //     3: aload_3
        //     4: ifnull +86 -> 90
        //     7: aload_3
        //     8: invokevirtual 135	android/graphics/Bitmap:isRecycled	()Z
        //     11: ifne +79 -> 90
        //     14: aload_0
        //     15: invokestatic 26	miui/util/ImageUtils:getBitmapSize	(Lmiui/util/InputStreamLoader;)Landroid/graphics/BitmapFactory$Options;
        //     18: astore 6
        //     20: aload 6
        //     22: getfield 32	android/graphics/BitmapFactory$Options:outWidth	I
        //     25: aload_3
        //     26: invokevirtual 49	android/graphics/Bitmap:getWidth	()I
        //     29: if_icmpne +52 -> 81
        //     32: aload 6
        //     34: getfield 35	android/graphics/BitmapFactory$Options:outHeight	I
        //     37: aload_3
        //     38: invokevirtual 52	android/graphics/Bitmap:getHeight	()I
        //     41: if_icmpne +40 -> 81
        //     44: invokestatic 98	miui/util/ImageUtils:getDefaultOptions	()Landroid/graphics/BitmapFactory$Options;
        //     47: astore 9
        //     49: aload 9
        //     51: aload_3
        //     52: putfield 139	android/graphics/BitmapFactory$Options:inBitmap	Landroid/graphics/Bitmap;
        //     55: aload 9
        //     57: iconst_1
        //     58: putfield 103	android/graphics/BitmapFactory$Options:inSampleSize	I
        //     61: aload_0
        //     62: invokevirtual 109	miui/util/InputStreamLoader:get	()Ljava/io/InputStream;
        //     65: aconst_null
        //     66: aload 9
        //     68: invokestatic 115	android/graphics/BitmapFactory:decodeStream	(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
        //     71: astore 10
        //     73: aload 10
        //     75: astore 4
        //     77: aload_0
        //     78: invokevirtual 118	miui/util/InputStreamLoader:close	()V
        //     81: aload 4
        //     83: ifnonnull +7 -> 90
        //     86: aload_3
        //     87: invokevirtual 88	android/graphics/Bitmap:recycle	()V
        //     90: aload 4
        //     92: astore 5
        //     94: aload 5
        //     96: ifnull +42 -> 138
        //     99: iload_1
        //     100: ifle +17 -> 117
        //     103: iload_2
        //     104: ifle +13 -> 117
        //     107: aload 5
        //     109: iload_1
        //     110: iload_2
        //     111: iconst_1
        //     112: invokestatic 130	miui/util/ImageUtils:scaleBitmapToDesire	(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
        //     115: astore 5
        //     117: aload 5
        //     119: areturn
        //     120: astore 8
        //     122: aload_0
        //     123: invokevirtual 118	miui/util/InputStreamLoader:close	()V
        //     126: goto -45 -> 81
        //     129: astore 7
        //     131: aload_0
        //     132: invokevirtual 118	miui/util/InputStreamLoader:close	()V
        //     135: aload 7
        //     137: athrow
        //     138: aload_0
        //     139: iload_1
        //     140: iload_2
        //     141: invokestatic 141	miui/util/ImageUtils:getBitmap	(Lmiui/util/InputStreamLoader;II)Landroid/graphics/Bitmap;
        //     144: astore 5
        //     146: goto -29 -> 117
        //
        // Exception table:
        //     from	to	target	type
        //     44	73	120	java/lang/Exception
        //     44	73	129	finally
    }

    public static final BitmapFactory.Options getBitmapSize(String paramString)
    {
        return getBitmapSize(new InputStreamLoader(paramString));
    }

    // ERROR //
    public static final BitmapFactory.Options getBitmapSize(InputStreamLoader paramInputStreamLoader)
    {
        // Byte code:
        //     0: new 28	android/graphics/BitmapFactory$Options
        //     3: dup
        //     4: invokespecial 146	android/graphics/BitmapFactory$Options:<init>	()V
        //     7: astore_1
        //     8: aload_1
        //     9: iconst_1
        //     10: putfield 150	android/graphics/BitmapFactory$Options:inJustDecodeBounds	Z
        //     13: aload_0
        //     14: invokevirtual 109	miui/util/InputStreamLoader:get	()Ljava/io/InputStream;
        //     17: aconst_null
        //     18: aload_1
        //     19: invokestatic 115	android/graphics/BitmapFactory:decodeStream	(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
        //     22: pop
        //     23: aload_0
        //     24: invokevirtual 118	miui/util/InputStreamLoader:close	()V
        //     27: aload_1
        //     28: areturn
        //     29: astore_3
        //     30: aload_0
        //     31: invokevirtual 118	miui/util/InputStreamLoader:close	()V
        //     34: goto -7 -> 27
        //     37: astore_2
        //     38: aload_0
        //     39: invokevirtual 118	miui/util/InputStreamLoader:close	()V
        //     42: aload_2
        //     43: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     8	23	29	java/lang/Exception
        //     8	23	37	finally
    }

    public static BitmapFactory.Options getDefaultOptions()
    {
        BitmapFactory.Options localOptions = new BitmapFactory.Options();
        localOptions.inDither = false;
        localOptions.inJustDecodeBounds = false;
        localOptions.inSampleSize = 1;
        localOptions.inScaled = false;
        return localOptions;
    }

    // ERROR //
    public static boolean isPngFormat(InputStreamLoader paramInputStreamLoader)
    {
        // Byte code:
        //     0: iconst_0
        //     1: istore_1
        //     2: aload_0
        //     3: invokevirtual 109	miui/util/InputStreamLoader:get	()Ljava/io/InputStream;
        //     6: astore 4
        //     8: getstatic 17	miui/util/ImageUtils:PNG_HEAD_FORMAT	[B
        //     11: arraylength
        //     12: newarray byte
        //     14: astore 5
        //     16: aload 4
        //     18: aload 5
        //     20: invokevirtual 164	java/io/InputStream:read	([B)I
        //     23: aload 5
        //     25: arraylength
        //     26: if_icmplt +13 -> 39
        //     29: aload 5
        //     31: invokestatic 167	miui/util/ImageUtils:isPngFormat	([B)Z
        //     34: istore 6
        //     36: iload 6
        //     38: istore_1
        //     39: aload_0
        //     40: ifnull +7 -> 47
        //     43: aload_0
        //     44: invokevirtual 118	miui/util/InputStreamLoader:close	()V
        //     47: iload_1
        //     48: ireturn
        //     49: astore_3
        //     50: aload_0
        //     51: ifnull -4 -> 47
        //     54: aload_0
        //     55: invokevirtual 118	miui/util/InputStreamLoader:close	()V
        //     58: goto -11 -> 47
        //     61: astore_2
        //     62: aload_0
        //     63: ifnull +7 -> 70
        //     66: aload_0
        //     67: invokevirtual 118	miui/util/InputStreamLoader:close	()V
        //     70: aload_2
        //     71: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     2	36	49	java/lang/Exception
        //     2	36	61	finally
    }

    public static boolean isPngFormat(byte[] paramArrayOfByte)
    {
        boolean bool = false;
        if ((paramArrayOfByte == null) || (paramArrayOfByte.length < PNG_HEAD_FORMAT.length));
        while (true)
        {
            return bool;
            for (int i = 0; ; i++)
            {
                if (i >= PNG_HEAD_FORMAT.length)
                    break label44;
                if (paramArrayOfByte[i] != PNG_HEAD_FORMAT[i])
                    break;
            }
            label44: bool = true;
        }
    }

    public static boolean saveBitmapToLocal(InputStreamLoader paramInputStreamLoader, String paramString, int paramInt1, int paramInt2)
    {
        boolean bool;
        if ((paramInputStreamLoader == null) || (paramString == null) || (paramInt1 < 1) || (paramInt2 < 1))
            bool = false;
        while (true)
        {
            return bool;
            bool = false;
            BitmapFactory.Options localOptions = getBitmapSize(paramInputStreamLoader);
            if ((localOptions.outWidth > 0) && (localOptions.outHeight > 0))
                if ((localOptions.outWidth == paramInt1) && (localOptions.outHeight == paramInt2))
                {
                    bool = saveToFile(paramInputStreamLoader, paramString);
                }
                else
                {
                    Bitmap localBitmap = getBitmap(paramInputStreamLoader, paramInt1, paramInt2);
                    if (localBitmap != null)
                    {
                        bool = saveToFile(localBitmap, paramString, isPngFormat(paramInputStreamLoader));
                        localBitmap.recycle();
                    }
                }
        }
    }

    public static boolean saveToFile(Bitmap paramBitmap, String paramString)
    {
        return saveToFile(paramBitmap, paramString, false);
    }

    public static boolean saveToFile(Bitmap paramBitmap, String paramString, boolean paramBoolean)
    {
        boolean bool;
        if (paramBitmap != null)
            try
            {
                FileOutputStream localFileOutputStream = new FileOutputStream(paramString);
                if (paramBoolean);
                for (Bitmap.CompressFormat localCompressFormat = Bitmap.CompressFormat.PNG; ; localCompressFormat = Bitmap.CompressFormat.JPEG)
                {
                    paramBitmap.compress(localCompressFormat, 100, localFileOutputStream);
                    localFileOutputStream.close();
                    bool = true;
                    break;
                }
            }
            catch (Exception localException)
            {
            }
        else
            bool = false;
        return bool;
    }

    private static boolean saveToFile(InputStreamLoader paramInputStreamLoader, String paramString)
    {
        boolean bool = false;
        try
        {
            FileOutputStream localFileOutputStream = new FileOutputStream(paramString);
            Streams.copy(paramInputStreamLoader.get(), localFileOutputStream);
            localFileOutputStream.close();
            paramInputStreamLoader.close();
            bool = true;
            label30: return bool;
        }
        catch (Exception localException)
        {
            break label30;
        }
    }

    public static Bitmap scaleBitmapToDesire(Bitmap paramBitmap, int paramInt1, int paramInt2, boolean paramBoolean)
    {
        Bitmap localBitmap = null;
        try
        {
            int i = paramBitmap.getWidth();
            int j = paramBitmap.getHeight();
            if ((i == paramInt1) && (j == paramInt2))
            {
                localBitmap = paramBitmap;
            }
            else
            {
                Bitmap.Config localConfig = Bitmap.Config.ARGB_8888;
                if (paramBitmap.getConfig() != null)
                    localConfig = paramBitmap.getConfig();
                localBitmap = Bitmap.createBitmap(paramInt1, paramInt2, localConfig);
                cropBitmapToAnother(paramBitmap, localBitmap, paramBoolean);
            }
        }
        catch (Exception localException)
        {
        }
        catch (OutOfMemoryError localOutOfMemoryError)
        {
        }
        return localBitmap;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.util.ImageUtils
 * JD-Core Version:        0.6.2
 */