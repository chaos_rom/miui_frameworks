package miui.util;

import android.content.ComponentName;
import android.content.Context;
import android.os.RemoteException;
import android.os.ServiceManager;
import com.android.internal.app.IUsageStats;
import com.android.internal.app.IUsageStats.Stub;

public final class Jlog
{
    private static final int LOG_ID_MIUI = 4;
    private static IUsageStats sUsageStatsService;

    public static int d(String paramString1, String paramString2)
    {
        return 0;
    }

    public static int d(String paramString1, String paramString2, Throwable paramThrowable)
    {
        return 0;
    }

    public static int e(String paramString1, String paramString2)
    {
        return 0;
    }

    public static int e(String paramString1, String paramString2, Throwable paramThrowable)
    {
        return 0;
    }

    public static int i(String paramString1, String paramString2)
    {
        return 0;
    }

    public static int i(String paramString1, String paramString2, Throwable paramThrowable)
    {
        return 0;
    }

    public static int println(int paramInt, String paramString1, String paramString2)
    {
        return 0;
    }

    public static void usage(Context paramContext, String paramString)
    {
        if (sUsageStatsService == null)
            sUsageStatsService = IUsageStats.Stub.asInterface(ServiceManager.getService("usagestats"));
        ComponentName localComponentName = new ComponentName(paramContext.getPackageName(), "#" + paramString + "#" + System.currentTimeMillis());
        try
        {
            sUsageStatsService.noteLaunchTime(localComponentName, 0);
            label69: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label69;
        }
    }

    public static int w(String paramString1, String paramString2)
    {
        return 0;
    }

    public static int w(String paramString1, String paramString2, Throwable paramThrowable)
    {
        return 0;
    }

    public static int w(String paramString, Throwable paramThrowable)
    {
        return 0;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.util.Jlog
 * JD-Core Version:        0.6.2
 */