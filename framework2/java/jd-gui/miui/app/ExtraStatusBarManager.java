package miui.app;

import android.content.ContentResolver;
import android.content.Context;
import android.provider.Settings.System;

public class ExtraStatusBarManager
{
    public static final String ACTION_ENTER_DRIVE_MODE = "com.miui.app.ExtraStatusBarManager.action_enter_drive_mode";
    public static final String ACTION_EXIT_FULLSCREEN = "com.miui.app.ExtraStatusBarManager.EXIT_FULLSCREEN";
    public static final String ACTION_EXPAND_NOTIFICATIONS_TAB = "com.miui.app.ExtraStatusBarManager.EXPAND_NOTIFICATIONS_TAB";
    public static final String ACTION_EXPAND_TOGGLES_TAB = "com.miui.app.ExtraStatusBarManager.EXPAND_TOGGLE_TAB";
    public static final String ACTION_LEAVE_DRIVE_MODE = "com.miui.app.ExtraStatusBarManager.action_leave_drive_mode";
    public static final String ACTION_PICK_TOGGLE_INTENT = "com.miui.app.ExtraStatusBarManager.action_PICK_TOGGLE_INTENT";
    public static final String ACTION_REQUEST_RESTART = "com.miui.app.ExtraStatusBarManager.REQUEST_RESTART";
    public static final String ACTION_STATUSBAR_LOADED = "com.miui.app.ExtraStatusBarManager.LOADED";
    public static final String ACTION_STATUSBAR_UNLOADED = "com.miui.app.ExtraStatusBarManager.UNLOADED";
    public static final String ACTION_TRIGGER_CAMERA_KEY = "com.miui.app.ExtraStatusBarManager.TRIGGER_CAMERA_KEY";
    public static final String ACTION_TRIGGER_TOGGLE = "com.miui.app.ExtraStatusBarManager.action_TRIGGER_TOGGLE";
    public static final String ACTION_TRIGGER_TOGGLE_LOCK = "com.miui.app.ExtraStatusBarManager.TRIGGER_TOGGLE_LOCK";
    public static final String ACTION_TRIGGER_TOGGLE_SCREEN_BUTTONS = "com.miui.app.ExtraStatusBarManager.TRIGGER_TOGGLE_SCREEN_BUTTONS";
    public static final int DISABLE_BACKGROUND = 1073741824;
    public static final int DISABLE_FOR_KEYGUARD = -2147483648;
    public static final int DISABLE_FOR_MUSIC = 134217728;
    public static final int DISABLE_FULLSCREEN = 536870912;
    public static final int DISABLE_SIMPLE_STATUS_BAR = 268435456;
    public static final String EXTRA_TOGGLE_ID = "com.miui.app.ExtraStatusBarManager.extra_TOGGLE_ID";

    public static void enableScreenshotNotification(Context paramContext, boolean paramBoolean)
    {
        ContentResolver localContentResolver = paramContext.getContentResolver();
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            Settings.System.putInt(localContentResolver, "screenshot_notification_enabled", i);
            return;
        }
    }

    public static boolean isExpandableUnderFullscreen(Context paramContext)
    {
        int i = 1;
        if (Settings.System.getInt(paramContext.getContentResolver(), "status_bar_expandable_under_fullscreen", i) != 0);
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    public static boolean isExpandableUnderKeyguard(Context paramContext)
    {
        boolean bool = false;
        if (Settings.System.getInt(paramContext.getContentResolver(), "status_bar_expandable_under_keyguard", 0) != 0)
            bool = true;
        return bool;
    }

    public static boolean isScreenshotNotificationEnabled(Context paramContext)
    {
        int i = 1;
        if (Settings.System.getInt(paramContext.getContentResolver(), "screenshot_notification_enabled", i) != 0);
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    public static boolean isShowFlowInfo(Context paramContext)
    {
        boolean bool = false;
        if (Settings.System.getInt(paramContext.getContentResolver(), "status_bar_show_network_assistant", 0) != 0)
            bool = true;
        return bool;
    }

    public static boolean isShowNetworkSpeed(Context paramContext)
    {
        boolean bool = false;
        if (Settings.System.getInt(paramContext.getContentResolver(), "status_bar_show_network_speed", 0) != 0)
            bool = true;
        return bool;
    }

    public static void setExpandableUnderFullscreen(Context paramContext, boolean paramBoolean)
    {
        ContentResolver localContentResolver = paramContext.getContentResolver();
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            Settings.System.putInt(localContentResolver, "status_bar_expandable_under_fullscreen", i);
            return;
        }
    }

    public static void setExpandableUnderKeyguard(Context paramContext, boolean paramBoolean)
    {
        ContentResolver localContentResolver = paramContext.getContentResolver();
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            Settings.System.putInt(localContentResolver, "status_bar_expandable_under_keyguard", i);
            return;
        }
    }

    public static void setShowFlowInfo(Context paramContext, boolean paramBoolean)
    {
        ContentResolver localContentResolver = paramContext.getContentResolver();
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            Settings.System.putInt(localContentResolver, "status_bar_show_network_assistant", i);
            return;
        }
    }

    public static void setShowNetworkSpeed(Context paramContext, boolean paramBoolean)
    {
        ContentResolver localContentResolver = paramContext.getContentResolver();
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            Settings.System.putInt(localContentResolver, "status_bar_show_network_speed", i);
            return;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.app.ExtraStatusBarManager
 * JD-Core Version:        0.6.2
 */