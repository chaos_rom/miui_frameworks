package miui.text.util;

import android.text.Spannable;
import android.text.style.URLSpan;
import android.text.util.Linkify.LinkSpec;
import android.text.util.Linkify.MatchFilter;
import android.text.util.Linkify.TransformFilter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.regex.Pattern;

public class Linkify
{
    public static final Linkify.MatchFilter sPhoneNumberMatchFilter = android.text.util.Linkify.sPhoneNumberMatchFilter;
    public static final Linkify.TransformFilter sPhoneNumberTransformFilter = android.text.util.Linkify.sPhoneNumberTransformFilter;
    public static final Linkify.MatchFilter sUrlMatchFilter = android.text.util.Linkify.sUrlMatchFilter;

    public static final boolean addLinks(Spannable paramSpannable, int paramInt)
    {
        boolean bool;
        if (paramInt == 0)
            bool = false;
        while (true)
        {
            return bool;
            URLSpan[] arrayOfURLSpan = (URLSpan[])paramSpannable.getSpans(0, paramSpannable.length(), URLSpan.class);
            for (int i = -1 + arrayOfURLSpan.length; i >= 0; i--)
                paramSpannable.removeSpan(arrayOfURLSpan[i]);
            ArrayList localArrayList = new ArrayList();
            if ((paramInt & 0x1) != 0)
            {
                Pattern localPattern3 = miui.util.Patterns.WEB_URL;
                String[] arrayOfString3 = new String[3];
                arrayOfString3[0] = "http://";
                arrayOfString3[1] = "https://";
                arrayOfString3[2] = "rtsp://";
                gatherLinks(localArrayList, paramSpannable, localPattern3, arrayOfString3, sUrlMatchFilter, null);
            }
            if ((paramInt & 0x2) != 0)
            {
                Pattern localPattern2 = android.util.Patterns.EMAIL_ADDRESS;
                String[] arrayOfString2 = new String[1];
                arrayOfString2[0] = "mailto:";
                gatherLinks(localArrayList, paramSpannable, localPattern2, arrayOfString2, null, null);
            }
            if ((paramInt & 0x4) != 0)
            {
                Pattern localPattern1 = android.util.Patterns.PHONE;
                String[] arrayOfString1 = new String[1];
                arrayOfString1[0] = "tel:";
                gatherLinks(localArrayList, paramSpannable, localPattern1, arrayOfString1, sPhoneNumberMatchFilter, sPhoneNumberTransformFilter);
            }
            if ((paramInt & 0x8) != 0)
                gatherMapLinks(localArrayList, paramSpannable);
            pruneOverlaps(localArrayList);
            if (localArrayList.size() == 0)
            {
                bool = false;
            }
            else
            {
                Iterator localIterator = localArrayList.iterator();
                while (localIterator.hasNext())
                {
                    Linkify.LinkSpec localLinkSpec = (Linkify.LinkSpec)localIterator.next();
                    applyLink(localLinkSpec.url, localLinkSpec.start, localLinkSpec.end, paramSpannable);
                }
                bool = true;
            }
        }
    }

    private static final void applyLink(String paramString, int paramInt1, int paramInt2, Spannable paramSpannable)
    {
        android.text.util.Linkify.applyLink(paramString, paramInt1, paramInt2, paramSpannable);
    }

    private static final void gatherLinks(ArrayList<Linkify.LinkSpec> paramArrayList, Spannable paramSpannable, Pattern paramPattern, String[] paramArrayOfString, Linkify.MatchFilter paramMatchFilter, Linkify.TransformFilter paramTransformFilter)
    {
        android.text.util.Linkify.gatherLinks(paramArrayList, paramSpannable, paramPattern, paramArrayOfString, paramMatchFilter, paramTransformFilter);
    }

    private static final void gatherMapLinks(ArrayList<Linkify.LinkSpec> paramArrayList, Spannable paramSpannable)
    {
        android.text.util.Linkify.gatherMapLinks(paramArrayList, paramSpannable);
    }

    private static final void pruneOverlaps(ArrayList<Linkify.LinkSpec> paramArrayList)
    {
        android.text.util.Linkify.pruneOverlaps(paramArrayList);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.text.util.Linkify
 * JD-Core Version:        0.6.2
 */