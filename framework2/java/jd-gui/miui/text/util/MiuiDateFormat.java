package miui.text.util;

import android.content.res.Resources;
import java.text.DateFormatSymbols;
import java.text.FieldPosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class MiuiDateFormat extends SimpleDateFormat
{
    private static final long serialVersionUID = 1L;

    public MiuiDateFormat()
    {
    }

    public MiuiDateFormat(String paramString)
    {
        super(paramString);
    }

    public MiuiDateFormat(String paramString, DateFormatSymbols paramDateFormatSymbols)
    {
        super(paramString, paramDateFormatSymbols);
    }

    public MiuiDateFormat(String paramString, Locale paramLocale)
    {
        super(paramString, paramLocale);
    }

    private String getDetailedAmPm(int paramInt)
    {
        int[] arrayOfInt = Resources.getSystem().getIntArray(101056522);
        String[] arrayOfString = Resources.getSystem().getStringArray(101056523);
        int i = -1 + arrayOfInt.length;
        if (i >= 0)
            if (paramInt < arrayOfInt[i]);
        for (String str = arrayOfString[i]; ; str = null)
        {
            return str;
            i--;
            break;
        }
    }

    public StringBuffer format(Date paramDate, StringBuffer paramStringBuffer, FieldPosition paramFieldPosition)
    {
        DateFormatSymbols localDateFormatSymbols = getDateFormatSymbols();
        this.calendar.setTime(paramDate);
        String[] arrayOfString = localDateFormatSymbols.getAmPmStrings();
        arrayOfString[this.calendar.get(9)] = getDetailedAmPm(this.calendar.get(11));
        localDateFormatSymbols.setAmPmStrings(arrayOfString);
        setDateFormatSymbols(localDateFormatSymbols);
        return super.format(paramDate, paramStringBuffer, paramFieldPosition);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.text.util.MiuiDateFormat
 * JD-Core Version:        0.6.2
 */