package miui.text.util;

import android.content.Context;
import android.content.res.Resources;

public final class MiuiFormatter
{
    public static final long GB = 1000000000L;
    public static final long KB = 1000L;
    public static final long MB = 1000000L;
    private static final long UNIT = 1000L;

    public static String formatFileSize(Context paramContext, long paramLong)
    {
        return formatFileSize(paramContext, paramLong, false);
    }

    private static String formatFileSize(Context paramContext, long paramLong, boolean paramBoolean)
    {
        String str2;
        if (paramContext == null)
        {
            str2 = "";
            return str2;
        }
        float f = (float)paramLong;
        int i = 101450206;
        if (f > 900.0F)
        {
            i = 101450107;
            f /= 1000.0F;
        }
        if (f > 900.0F)
        {
            i = 101450108;
            f /= 1000.0F;
        }
        if (f > 900.0F)
        {
            i = 101450139;
            f /= 1000.0F;
        }
        if (f > 900.0F)
        {
            i = 101450207;
            f /= 1000.0F;
        }
        if (f > 900.0F)
        {
            i = 101450208;
            f /= 1000.0F;
        }
        String str1;
        if (f < 1.0F)
        {
            Object[] arrayOfObject7 = new Object[1];
            arrayOfObject7[0] = Float.valueOf(f);
            str1 = String.format("%.2f", arrayOfObject7);
        }
        while (true)
        {
            Resources localResources = paramContext.getResources();
            Object[] arrayOfObject2 = new Object[2];
            arrayOfObject2[0] = str1;
            arrayOfObject2[1] = paramContext.getString(i);
            str2 = localResources.getString(101450209, arrayOfObject2);
            break;
            if (f < 10.0F)
            {
                if (paramBoolean)
                {
                    Object[] arrayOfObject6 = new Object[1];
                    arrayOfObject6[0] = Float.valueOf(f);
                    str1 = String.format("%.1f", arrayOfObject6);
                }
                else
                {
                    Object[] arrayOfObject5 = new Object[1];
                    arrayOfObject5[0] = Float.valueOf(f);
                    str1 = String.format("%.2f", arrayOfObject5);
                }
            }
            else if (f < 100.0F)
            {
                if (paramBoolean)
                {
                    Object[] arrayOfObject4 = new Object[1];
                    arrayOfObject4[0] = Float.valueOf(f);
                    str1 = String.format("%.0f", arrayOfObject4);
                }
                else
                {
                    Object[] arrayOfObject3 = new Object[1];
                    arrayOfObject3[0] = Float.valueOf(f);
                    str1 = String.format("%.2f", arrayOfObject3);
                }
            }
            else
            {
                Object[] arrayOfObject1 = new Object[1];
                arrayOfObject1[0] = Float.valueOf(f);
                str1 = String.format("%.0f", arrayOfObject1);
            }
        }
    }

    public static String formatShortFileSize(Context paramContext, long paramLong)
    {
        return formatFileSize(paramContext, paramLong, true);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework2_dex2jar.jar
 * Qualified Name:         miui.text.util.MiuiFormatter
 * JD-Core Version:        0.6.2
 */