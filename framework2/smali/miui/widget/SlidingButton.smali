.class public Lmiui/widget/SlidingButton;
.super Landroid/widget/CheckBox;
.source "SlidingButton.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/widget/SlidingButton$1;,
        Lmiui/widget/SlidingButton$SlidingHandler;,
        Lmiui/widget/SlidingButton$OnCheckedChangedListener;
    }
.end annotation


# static fields
.field private static final ANIMATION_FRAME_DURATION:I = 0x10

.field private static final MAXIMUM_MINOR_VELOCITY:F = 150.0f

.field private static final MSG_ANIMATE:I = 0x3e8

.field private static final TAP_THRESHOLD:I = 0x6


# instance fields
.field private mActiveSlider:Landroid/graphics/drawable/BitmapDrawable;

.field private mAlphaPixels:[I

.field private mAnimatedVelocity:F

.field private mAnimating:Z

.field private mAnimationLastTime:J

.field private mAnimationPosition:F

.field private mBarBitmap:Landroid/graphics/Bitmap;

.field private mBarSlice:[I

.field private mCurrentAnimationTime:J

.field private mFrame:Landroid/graphics/drawable/Drawable;

.field private final mHandler:Landroid/os/Handler;

.field private mHeight:I

.field private mLastX:I

.field private mOffDisable:Landroid/graphics/drawable/BitmapDrawable;

.field private mOnCheckedChangedListener:Lmiui/widget/SlidingButton$OnCheckedChangedListener;

.field private mOnDisable:Landroid/graphics/drawable/BitmapDrawable;

.field private mOriginalTouchPointX:I

.field private mPressedSlider:Landroid/graphics/drawable/BitmapDrawable;

.field private mSlideMask:Landroid/graphics/drawable/Drawable;

.field private mSlideOff:Landroid/graphics/drawable/BitmapDrawable;

.field private mSlideOn:Landroid/graphics/drawable/BitmapDrawable;

.field private mSlideState:Landroid/graphics/drawable/BitmapDrawable;

.field private mSlider:Landroid/graphics/drawable/BitmapDrawable;

.field private mSliderMoved:Z

.field private mSliderOffset:I

.field private mSliderPositionEnd:I

.field private mSliderPositionStart:I

.field private mSliderWidth:I

.field private mTapThreshold:I

.field private mTracking:Z

.field private mWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    .line 81
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lmiui/widget/SlidingButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 82
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 90
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lmiui/widget/SlidingButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 91
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 6
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    const/4 v1, 0x0

    .line 85
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/CheckBox;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 55
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiui/widget/SlidingButton;->mAnimating:Z

    .line 62
    new-instance v0, Lmiui/widget/SlidingButton$SlidingHandler;

    invoke-direct {v0, p0, v1}, Lmiui/widget/SlidingButton$SlidingHandler;-><init>(Lmiui/widget/SlidingButton;Lmiui/widget/SlidingButton$1;)V

    iput-object v0, p0, Lmiui/widget/SlidingButton;->mHandler:Landroid/os/Handler;

    .line 67
    const/high16 v0, 0x4316

    iput v0, p0, Lmiui/widget/SlidingButton;->mAnimatedVelocity:F

    .line 74
    iput-object v1, p0, Lmiui/widget/SlidingButton;->mOnCheckedChangedListener:Lmiui/widget/SlidingButton$OnCheckedChangedListener;

    .line 86
    invoke-direct {p0, p1, p2, p3}, Lmiui/widget/SlidingButton;->initialize(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 87
    return-void
.end method

.method static synthetic access$100(Lmiui/widget/SlidingButton;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 28
    invoke-direct {p0}, Lmiui/widget/SlidingButton;->doAnimation()V

    return-void
.end method

.method private animateOff()V
    .registers 2

    .prologue
    .line 275
    const/high16 v0, -0x3cea

    invoke-direct {p0, v0}, Lmiui/widget/SlidingButton;->performFling(F)V

    .line 276
    invoke-virtual {p0}, Lmiui/widget/SlidingButton;->invalidate()V

    .line 277
    return-void
.end method

.method private animateOn()V
    .registers 2

    .prologue
    .line 270
    const/high16 v0, 0x4316

    invoke-direct {p0, v0}, Lmiui/widget/SlidingButton;->performFling(F)V

    .line 271
    invoke-virtual {p0}, Lmiui/widget/SlidingButton;->invalidate()V

    .line 272
    return-void
.end method

.method private animateToggle()V
    .registers 2

    .prologue
    .line 262
    invoke-virtual {p0}, Lmiui/widget/SlidingButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 263
    invoke-direct {p0}, Lmiui/widget/SlidingButton;->animateOff()V

    .line 267
    :goto_9
    return-void

    .line 265
    :cond_a
    invoke-direct {p0}, Lmiui/widget/SlidingButton;->animateOn()V

    goto :goto_9
.end method

.method private createBarBitmap(Landroid/graphics/drawable/BitmapDrawable;)V
    .registers 6
    .parameter "drawable"

    .prologue
    .line 99
    if-eqz p1, :cond_16

    .line 100
    invoke-virtual {p1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iget v1, p0, Lmiui/widget/SlidingButton;->mWidth:I

    mul-int/lit8 v1, v1, 0x2

    iget v2, p0, Lmiui/widget/SlidingButton;->mSliderWidth:I

    sub-int/2addr v1, v2

    iget v2, p0, Lmiui/widget/SlidingButton;->mHeight:I

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lmiui/widget/SlidingButton;->mBarBitmap:Landroid/graphics/Bitmap;

    .line 106
    :cond_16
    return-void
.end method

.method private cutEdge(II[I)V
    .registers 10
    .parameter "baseWidth"
    .parameter "baseHeight"
    .parameter "basePixels"

    .prologue
    .line 366
    const v2, 0xffffff

    .line 367
    .local v2, sRGBMask:I
    const/16 v1, 0x18

    .line 369
    .local v1, sAlphaShift:I
    mul-int v3, p1, p2

    add-int/lit8 v0, v3, -0x1

    .local v0, i:I
    :goto_9
    if-ltz v0, :cond_20

    .line 370
    aget v3, p3, v0

    aget v4, p3, v0

    ushr-int/2addr v4, v1

    iget-object v5, p0, Lmiui/widget/SlidingButton;->mAlphaPixels:[I

    aget v5, v5, v0

    ushr-int/2addr v5, v1

    mul-int/2addr v4, v5

    div-int/lit16 v4, v4, 0xff

    shl-int/2addr v4, v1

    add-int/2addr v4, v2

    and-int/2addr v3, v4

    aput v3, p3, v0

    .line 369
    add-int/lit8 v0, v0, -0x1

    goto :goto_9

    .line 374
    :cond_20
    return-void
.end method

.method private doAnimation()V
    .registers 6

    .prologue
    const/16 v4, 0x3e8

    const/4 v0, 0x0

    .line 302
    iget-boolean v1, p0, Lmiui/widget/SlidingButton;->mAnimating:Z

    if-nez v1, :cond_8

    .line 318
    :cond_7
    :goto_7
    return-void

    .line 305
    :cond_8
    invoke-direct {p0}, Lmiui/widget/SlidingButton;->incrementAnimation()V

    .line 306
    iget v1, p0, Lmiui/widget/SlidingButton;->mAnimationPosition:F

    float-to-int v1, v1

    invoke-direct {p0, v1}, Lmiui/widget/SlidingButton;->moveSlider(I)V

    .line 307
    iget v1, p0, Lmiui/widget/SlidingButton;->mSliderOffset:I

    iget v2, p0, Lmiui/widget/SlidingButton;->mSliderPositionStart:I

    if-le v1, v2, :cond_1d

    iget v1, p0, Lmiui/widget/SlidingButton;->mSliderOffset:I

    iget v2, p0, Lmiui/widget/SlidingButton;->mSliderPositionEnd:I

    if-lt v1, v2, :cond_3c

    .line 308
    :cond_1d
    iget-object v1, p0, Lmiui/widget/SlidingButton;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 309
    iput-boolean v0, p0, Lmiui/widget/SlidingButton;->mAnimating:Z

    .line 310
    iget v1, p0, Lmiui/widget/SlidingButton;->mSliderOffset:I

    iget v2, p0, Lmiui/widget/SlidingButton;->mSliderPositionEnd:I

    if-lt v1, v2, :cond_2b

    const/4 v0, 0x1

    :cond_2b
    invoke-virtual {p0, v0}, Lmiui/widget/SlidingButton;->setChecked(Z)V

    .line 311
    iget-object v0, p0, Lmiui/widget/SlidingButton;->mOnCheckedChangedListener:Lmiui/widget/SlidingButton$OnCheckedChangedListener;

    if-eqz v0, :cond_7

    .line 312
    iget-object v0, p0, Lmiui/widget/SlidingButton;->mOnCheckedChangedListener:Lmiui/widget/SlidingButton$OnCheckedChangedListener;

    invoke-virtual {p0}, Lmiui/widget/SlidingButton;->isChecked()Z

    move-result v1

    invoke-interface {v0, v1}, Lmiui/widget/SlidingButton$OnCheckedChangedListener;->onCheckedChanged(Z)V

    goto :goto_7

    .line 315
    :cond_3c
    iget-wide v0, p0, Lmiui/widget/SlidingButton;->mCurrentAnimationTime:J

    const-wide/16 v2, 0x10

    add-long/2addr v0, v2

    iput-wide v0, p0, Lmiui/widget/SlidingButton;->mCurrentAnimationTime:J

    .line 316
    iget-object v0, p0, Lmiui/widget/SlidingButton;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lmiui/widget/SlidingButton;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    iget-wide v2, p0, Lmiui/widget/SlidingButton;->mCurrentAnimationTime:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z

    goto :goto_7
.end method

.method private drawSlidingBar(Landroid/graphics/Canvas;)V
    .registers 17
    .parameter "canvas"

    .prologue
    .line 356
    iget v0, p0, Lmiui/widget/SlidingButton;->mSliderPositionEnd:I

    iget v1, p0, Lmiui/widget/SlidingButton;->mSliderOffset:I

    sub-int v4, v0, v1

    .line 357
    .local v4, barOffset:I
    iget-object v0, p0, Lmiui/widget/SlidingButton;->mBarBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lmiui/widget/SlidingButton;->mBarSlice:[I

    const/4 v2, 0x0

    iget v3, p0, Lmiui/widget/SlidingButton;->mWidth:I

    const/4 v5, 0x0

    iget v6, p0, Lmiui/widget/SlidingButton;->mWidth:I

    iget v7, p0, Lmiui/widget/SlidingButton;->mHeight:I

    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 360
    iget v0, p0, Lmiui/widget/SlidingButton;->mWidth:I

    iget v1, p0, Lmiui/widget/SlidingButton;->mHeight:I

    iget-object v2, p0, Lmiui/widget/SlidingButton;->mBarSlice:[I

    invoke-direct {p0, v0, v1, v2}, Lmiui/widget/SlidingButton;->cutEdge(II[I)V

    .line 361
    iget-object v6, p0, Lmiui/widget/SlidingButton;->mBarSlice:[I

    const/4 v7, 0x0

    iget v8, p0, Lmiui/widget/SlidingButton;->mWidth:I

    const/4 v9, 0x0

    const/4 v10, 0x0

    iget v11, p0, Lmiui/widget/SlidingButton;->mWidth:I

    iget v12, p0, Lmiui/widget/SlidingButton;->mHeight:I

    const/4 v13, 0x1

    const/4 v14, 0x0

    move-object/from16 v5, p1

    invoke-virtual/range {v5 .. v14}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    .line 362
    return-void
.end method

.method private incrementAnimation()V
    .registers 7

    .prologue
    .line 321
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 322
    .local v0, now:J
    iget-wide v4, p0, Lmiui/widget/SlidingButton;->mAnimationLastTime:J

    sub-long v4, v0, v4

    long-to-float v4, v4

    const/high16 v5, 0x447a

    div-float v3, v4, v5

    .line 323
    .local v3, t:F
    iget v2, p0, Lmiui/widget/SlidingButton;->mAnimationPosition:F

    .line 324
    .local v2, position:F
    iget v4, p0, Lmiui/widget/SlidingButton;->mAnimatedVelocity:F

    mul-float/2addr v4, v3

    add-float/2addr v4, v2

    iput v4, p0, Lmiui/widget/SlidingButton;->mAnimationPosition:F

    .line 325
    iput-wide v0, p0, Lmiui/widget/SlidingButton;->mAnimationLastTime:J

    .line 326
    return-void
.end method

.method private initialize(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 20
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 108
    const v12, 0x60d0027

    .line 110
    .local v12, defaultStyleId:I
    const v4, 0x6010083

    move-object/from16 v0, p1

    invoke-static {v0, v4}, Lmiui/util/UiUtils;->resolveAttribute(Landroid/content/Context;I)I

    move-result v14

    .line 111
    .local v14, newId:I
    if-lez v14, :cond_f

    .line 112
    move v12, v14

    .line 114
    :cond_f
    sget-object v4, Lcom/miui/internal/R$styleable;->SlidingButton:[I

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move/from16 v2, p3

    invoke-virtual {v0, v1, v4, v2, v12}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v11

    .line 118
    .local v11, a:Landroid/content/res/TypedArray;
    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lmiui/widget/SlidingButton;->setDrawingCacheEnabled(Z)V

    .line 119
    invoke-virtual/range {p0 .. p0}, Lmiui/widget/SlidingButton;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v13, v4, Landroid/util/DisplayMetrics;->density:F

    .line 120
    .local v13, density:F
    const/high16 v4, 0x40c0

    mul-float/2addr v4, v13

    const/high16 v5, 0x3f00

    add-float/2addr v4, v5

    float-to-int v4, v4

    move-object/from16 v0, p0

    iput v4, v0, Lmiui/widget/SlidingButton;->mTapThreshold:I

    .line 123
    const/4 v4, 0x0

    invoke-virtual {v11, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lmiui/widget/SlidingButton;->mFrame:Landroid/graphics/drawable/Drawable;

    .line 124
    const/4 v4, 0x1

    invoke-virtual {v11, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    check-cast v4, Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, p0

    iput-object v4, v0, Lmiui/widget/SlidingButton;->mSlider:Landroid/graphics/drawable/BitmapDrawable;

    .line 125
    const/4 v4, 0x2

    invoke-virtual {v11, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    check-cast v4, Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, p0

    iput-object v4, v0, Lmiui/widget/SlidingButton;->mPressedSlider:Landroid/graphics/drawable/BitmapDrawable;

    .line 126
    const/4 v4, 0x3

    invoke-virtual {v11, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    check-cast v4, Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, p0

    iput-object v4, v0, Lmiui/widget/SlidingButton;->mOnDisable:Landroid/graphics/drawable/BitmapDrawable;

    .line 127
    const/4 v4, 0x4

    invoke-virtual {v11, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    check-cast v4, Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, p0

    iput-object v4, v0, Lmiui/widget/SlidingButton;->mOffDisable:Landroid/graphics/drawable/BitmapDrawable;

    .line 129
    const/4 v4, 0x7

    invoke-virtual {v11, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    check-cast v4, Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, p0

    iput-object v4, v0, Lmiui/widget/SlidingButton;->mSlideOff:Landroid/graphics/drawable/BitmapDrawable;

    .line 130
    const/16 v4, 0x8

    invoke-virtual {v11, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    check-cast v4, Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, p0

    iput-object v4, v0, Lmiui/widget/SlidingButton;->mSlideOn:Landroid/graphics/drawable/BitmapDrawable;

    .line 131
    const/4 v4, 0x5

    invoke-virtual {v11, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lmiui/widget/SlidingButton;->mSlideMask:Landroid/graphics/drawable/Drawable;

    .line 134
    move-object/from16 v0, p0

    iget-object v4, v0, Lmiui/widget/SlidingButton;->mFrame:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lmiui/widget/SlidingButton;->mWidth:I

    .line 135
    move-object/from16 v0, p0

    iget-object v4, v0, Lmiui/widget/SlidingButton;->mFrame:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lmiui/widget/SlidingButton;->mHeight:I

    .line 137
    move-object/from16 v0, p0

    iget-object v4, v0, Lmiui/widget/SlidingButton;->mSlider:Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, p0

    iput-object v4, v0, Lmiui/widget/SlidingButton;->mActiveSlider:Landroid/graphics/drawable/BitmapDrawable;

    .line 138
    move-object/from16 v0, p0

    iget v4, v0, Lmiui/widget/SlidingButton;->mWidth:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lmiui/widget/SlidingButton;->mSlider:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicWidth()I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lmiui/widget/SlidingButton;->mSliderWidth:I

    .line 139
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lmiui/widget/SlidingButton;->mSliderPositionStart:I

    .line 140
    move-object/from16 v0, p0

    iget v4, v0, Lmiui/widget/SlidingButton;->mWidth:I

    move-object/from16 v0, p0

    iget v5, v0, Lmiui/widget/SlidingButton;->mSliderWidth:I

    sub-int/2addr v4, v5

    move-object/from16 v0, p0

    iput v4, v0, Lmiui/widget/SlidingButton;->mSliderPositionEnd:I

    .line 142
    move-object/from16 v0, p0

    iget v4, v0, Lmiui/widget/SlidingButton;->mSliderPositionStart:I

    move-object/from16 v0, p0

    iput v4, v0, Lmiui/widget/SlidingButton;->mSliderOffset:I

    .line 144
    move-object/from16 v0, p0

    iget-object v4, v0, Lmiui/widget/SlidingButton;->mFrame:Landroid/graphics/drawable/Drawable;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget v7, v0, Lmiui/widget/SlidingButton;->mWidth:I

    move-object/from16 v0, p0

    iget v8, v0, Lmiui/widget/SlidingButton;->mHeight:I

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 145
    move-object/from16 v0, p0

    iget-object v4, v0, Lmiui/widget/SlidingButton;->mOnDisable:Landroid/graphics/drawable/BitmapDrawable;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget v7, v0, Lmiui/widget/SlidingButton;->mWidth:I

    move-object/from16 v0, p0

    iget v8, v0, Lmiui/widget/SlidingButton;->mHeight:I

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(IIII)V

    .line 146
    move-object/from16 v0, p0

    iget-object v4, v0, Lmiui/widget/SlidingButton;->mOffDisable:Landroid/graphics/drawable/BitmapDrawable;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget v7, v0, Lmiui/widget/SlidingButton;->mWidth:I

    move-object/from16 v0, p0

    iget v8, v0, Lmiui/widget/SlidingButton;->mHeight:I

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(IIII)V

    .line 147
    move-object/from16 v0, p0

    iget-object v4, v0, Lmiui/widget/SlidingButton;->mSlideOn:Landroid/graphics/drawable/BitmapDrawable;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget v7, v0, Lmiui/widget/SlidingButton;->mWidth:I

    move-object/from16 v0, p0

    iget v8, v0, Lmiui/widget/SlidingButton;->mHeight:I

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(IIII)V

    .line 148
    move-object/from16 v0, p0

    iget-object v4, v0, Lmiui/widget/SlidingButton;->mSlideOff:Landroid/graphics/drawable/BitmapDrawable;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget v7, v0, Lmiui/widget/SlidingButton;->mWidth:I

    move-object/from16 v0, p0

    iget v8, v0, Lmiui/widget/SlidingButton;->mHeight:I

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(IIII)V

    .line 151
    move-object/from16 v0, p0

    iget v4, v0, Lmiui/widget/SlidingButton;->mWidth:I

    move-object/from16 v0, p0

    iget v5, v0, Lmiui/widget/SlidingButton;->mHeight:I

    mul-int/2addr v4, v5

    new-array v4, v4, [I

    move-object/from16 v0, p0

    iput-object v4, v0, Lmiui/widget/SlidingButton;->mAlphaPixels:[I

    .line 152
    const/4 v4, 0x5

    invoke-virtual {v11, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    check-cast v4, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v15

    .line 153
    .local v15, source:Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget v4, v0, Lmiui/widget/SlidingButton;->mWidth:I

    move-object/from16 v0, p0

    iget v5, v0, Lmiui/widget/SlidingButton;->mHeight:I

    const/4 v6, 0x0

    invoke-static {v15, v4, v5, v6}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 158
    .local v3, alphaCutter:Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v4, v0, Lmiui/widget/SlidingButton;->mAlphaPixels:[I

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget v6, v0, Lmiui/widget/SlidingButton;->mWidth:I

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget v9, v0, Lmiui/widget/SlidingButton;->mWidth:I

    move-object/from16 v0, p0

    iget v10, v0, Lmiui/widget/SlidingButton;->mHeight:I

    invoke-virtual/range {v3 .. v10}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 159
    if-eq v3, v15, :cond_170

    .line 160
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 164
    :cond_170
    move-object/from16 v0, p0

    iget v4, v0, Lmiui/widget/SlidingButton;->mWidth:I

    move-object/from16 v0, p0

    iget v5, v0, Lmiui/widget/SlidingButton;->mHeight:I

    mul-int/2addr v4, v5

    new-array v4, v4, [I

    move-object/from16 v0, p0

    iput-object v4, v0, Lmiui/widget/SlidingButton;->mBarSlice:[I

    .line 165
    move-object/from16 v0, p0

    iget-object v4, v0, Lmiui/widget/SlidingButton;->mSlideOff:Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, p0

    iput-object v4, v0, Lmiui/widget/SlidingButton;->mSlideState:Landroid/graphics/drawable/BitmapDrawable;

    .line 166
    move-object/from16 v0, p0

    iget-object v4, v0, Lmiui/widget/SlidingButton;->mSlideState:Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lmiui/widget/SlidingButton;->createBarBitmap(Landroid/graphics/drawable/BitmapDrawable;)V

    .line 168
    invoke-virtual {v11}, Landroid/content/res/TypedArray;->recycle()V

    .line 169
    return-void
.end method

.method private moveSlider(I)V
    .registers 4
    .parameter "offsetX"

    .prologue
    .line 281
    iget v0, p0, Lmiui/widget/SlidingButton;->mSliderOffset:I

    add-int/2addr v0, p1

    iput v0, p0, Lmiui/widget/SlidingButton;->mSliderOffset:I

    .line 282
    iget v0, p0, Lmiui/widget/SlidingButton;->mSliderOffset:I

    iget v1, p0, Lmiui/widget/SlidingButton;->mSliderPositionStart:I

    if-ge v0, v1, :cond_13

    .line 283
    iget v0, p0, Lmiui/widget/SlidingButton;->mSliderPositionStart:I

    iput v0, p0, Lmiui/widget/SlidingButton;->mSliderOffset:I

    .line 287
    :cond_f
    :goto_f
    invoke-virtual {p0}, Lmiui/widget/SlidingButton;->invalidate()V

    .line 288
    return-void

    .line 284
    :cond_13
    iget v0, p0, Lmiui/widget/SlidingButton;->mSliderOffset:I

    iget v1, p0, Lmiui/widget/SlidingButton;->mSliderPositionEnd:I

    if-le v0, v1, :cond_f

    .line 285
    iget v0, p0, Lmiui/widget/SlidingButton;->mSliderPositionEnd:I

    iput v0, p0, Lmiui/widget/SlidingButton;->mSliderOffset:I

    goto :goto_f
.end method

.method private performFling(F)V
    .registers 8
    .parameter "velocity"

    .prologue
    const/16 v4, 0x3e8

    .line 291
    const/4 v2, 0x1

    iput-boolean v2, p0, Lmiui/widget/SlidingButton;->mAnimating:Z

    .line 292
    const/4 v2, 0x0

    iput v2, p0, Lmiui/widget/SlidingButton;->mAnimationPosition:F

    .line 293
    iput p1, p0, Lmiui/widget/SlidingButton;->mAnimatedVelocity:F

    .line 294
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 295
    .local v0, now:J
    iput-wide v0, p0, Lmiui/widget/SlidingButton;->mAnimationLastTime:J

    .line 296
    const-wide/16 v2, 0x10

    add-long/2addr v2, v0

    iput-wide v2, p0, Lmiui/widget/SlidingButton;->mCurrentAnimationTime:J

    .line 297
    iget-object v2, p0, Lmiui/widget/SlidingButton;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 298
    iget-object v2, p0, Lmiui/widget/SlidingButton;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lmiui/widget/SlidingButton;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    iget-wide v4, p0, Lmiui/widget/SlidingButton;->mCurrentAnimationTime:J

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z

    .line 299
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 7
    .parameter "canvas"

    .prologue
    .line 330
    invoke-super {p0, p1}, Landroid/widget/CheckBox;->onDraw(Landroid/graphics/Canvas;)V

    .line 332
    invoke-virtual {p0}, Lmiui/widget/SlidingButton;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_1b

    .line 333
    invoke-virtual {p0}, Lmiui/widget/SlidingButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 334
    iget-object v0, p0, Lmiui/widget/SlidingButton;->mOnDisable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 351
    :goto_14
    return-void

    .line 336
    :cond_15
    iget-object v0, p0, Lmiui/widget/SlidingButton;->mOffDisable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_14

    .line 341
    :cond_1b
    invoke-direct {p0, p1}, Lmiui/widget/SlidingButton;->drawSlidingBar(Landroid/graphics/Canvas;)V

    .line 343
    iget-object v0, p0, Lmiui/widget/SlidingButton;->mFrame:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 345
    iget-object v0, p0, Lmiui/widget/SlidingButton;->mSlideMask:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 347
    iget-object v0, p0, Lmiui/widget/SlidingButton;->mActiveSlider:Landroid/graphics/drawable/BitmapDrawable;

    iget v1, p0, Lmiui/widget/SlidingButton;->mSliderOffset:I

    const/4 v2, 0x0

    iget v3, p0, Lmiui/widget/SlidingButton;->mSliderWidth:I

    iget v4, p0, Lmiui/widget/SlidingButton;->mSliderOffset:I

    add-int/2addr v3, v4

    iget v4, p0, Lmiui/widget/SlidingButton;->mHeight:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(IIII)V

    .line 348
    iget-object v0, p0, Lmiui/widget/SlidingButton;->mActiveSlider:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_14
.end method

.method protected onMeasure(II)V
    .registers 5
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    .prologue
    .line 195
    iget v0, p0, Lmiui/widget/SlidingButton;->mWidth:I

    iget v1, p0, Lmiui/widget/SlidingButton;->mHeight:I

    invoke-virtual {p0, v0, v1}, Lmiui/widget/SlidingButton;->setMeasuredDimension(II)V

    .line 196
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 11
    .parameter "event"

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 201
    invoke-virtual {p0}, Lmiui/widget/SlidingButton;->isEnabled()Z

    move-result v6

    if-nez v6, :cond_9

    .line 258
    :goto_8
    return v4

    .line 204
    :cond_9
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 205
    .local v0, action:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    float-to-int v2, v6

    .line 206
    .local v2, x:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    float-to-int v3, v6

    .line 207
    .local v3, y:I
    new-instance v1, Landroid/graphics/Rect;

    iget v6, p0, Lmiui/widget/SlidingButton;->mSliderOffset:I

    iget v7, p0, Lmiui/widget/SlidingButton;->mSliderOffset:I

    iget v8, p0, Lmiui/widget/SlidingButton;->mSliderWidth:I

    add-int/2addr v7, v8

    iget v8, p0, Lmiui/widget/SlidingButton;->mHeight:I

    invoke-direct {v1, v6, v4, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 209
    .local v1, sliderFrame:Landroid/graphics/Rect;
    packed-switch v0, :pswitch_data_96

    :cond_28
    :goto_28
    move v4, v5

    .line 258
    goto :goto_8

    .line 211
    :pswitch_2a
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-eqz v6, :cond_40

    .line 212
    iput-boolean v5, p0, Lmiui/widget/SlidingButton;->mTracking:Z

    .line 213
    iget-object v6, p0, Lmiui/widget/SlidingButton;->mPressedSlider:Landroid/graphics/drawable/BitmapDrawable;

    iput-object v6, p0, Lmiui/widget/SlidingButton;->mActiveSlider:Landroid/graphics/drawable/BitmapDrawable;

    .line 214
    invoke-virtual {p0}, Lmiui/widget/SlidingButton;->invalidate()V

    .line 218
    :goto_39
    iput v2, p0, Lmiui/widget/SlidingButton;->mLastX:I

    .line 219
    iput v2, p0, Lmiui/widget/SlidingButton;->mOriginalTouchPointX:I

    .line 220
    iput-boolean v4, p0, Lmiui/widget/SlidingButton;->mSliderMoved:Z

    goto :goto_28

    .line 216
    :cond_40
    iput-boolean v4, p0, Lmiui/widget/SlidingButton;->mTracking:Z

    goto :goto_39

    .line 224
    :pswitch_43
    iget-boolean v4, p0, Lmiui/widget/SlidingButton;->mTracking:Z

    if-eqz v4, :cond_28

    .line 225
    iget v4, p0, Lmiui/widget/SlidingButton;->mLastX:I

    sub-int v4, v2, v4

    invoke-direct {p0, v4}, Lmiui/widget/SlidingButton;->moveSlider(I)V

    .line 226
    iput v2, p0, Lmiui/widget/SlidingButton;->mLastX:I

    .line 227
    iget v4, p0, Lmiui/widget/SlidingButton;->mOriginalTouchPointX:I

    sub-int v4, v2, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    iget v6, p0, Lmiui/widget/SlidingButton;->mTapThreshold:I

    if-lt v4, v6, :cond_28

    .line 228
    iput-boolean v5, p0, Lmiui/widget/SlidingButton;->mSliderMoved:Z

    .line 229
    invoke-virtual {p0}, Lmiui/widget/SlidingButton;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    invoke-interface {v4, v5}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_28

    .line 235
    :pswitch_66
    iget-boolean v6, p0, Lmiui/widget/SlidingButton;->mTracking:Z

    if-eqz v6, :cond_8c

    .line 236
    iget-boolean v6, p0, Lmiui/widget/SlidingButton;->mSliderMoved:Z

    if-nez v6, :cond_76

    .line 237
    invoke-direct {p0}, Lmiui/widget/SlidingButton;->animateToggle()V

    .line 248
    :goto_71
    iput-boolean v4, p0, Lmiui/widget/SlidingButton;->mTracking:Z

    .line 249
    iput-boolean v4, p0, Lmiui/widget/SlidingButton;->mSliderMoved:Z

    goto :goto_28

    .line 239
    :cond_76
    iget v6, p0, Lmiui/widget/SlidingButton;->mSliderOffset:I

    iget v7, p0, Lmiui/widget/SlidingButton;->mSliderPositionStart:I

    if-lt v6, v7, :cond_88

    iget v6, p0, Lmiui/widget/SlidingButton;->mSliderOffset:I

    iget v7, p0, Lmiui/widget/SlidingButton;->mSliderPositionEnd:I

    div-int/lit8 v7, v7, 0x2

    if-gt v6, v7, :cond_88

    .line 240
    invoke-direct {p0}, Lmiui/widget/SlidingButton;->animateOff()V

    goto :goto_71

    .line 242
    :cond_88
    invoke-direct {p0}, Lmiui/widget/SlidingButton;->animateOn()V

    goto :goto_71

    .line 246
    :cond_8c
    invoke-direct {p0}, Lmiui/widget/SlidingButton;->animateToggle()V

    goto :goto_71

    .line 253
    :pswitch_90
    iput-boolean v4, p0, Lmiui/widget/SlidingButton;->mTracking:Z

    .line 254
    iput-boolean v4, p0, Lmiui/widget/SlidingButton;->mSliderMoved:Z

    goto :goto_28

    .line 209
    nop

    :pswitch_data_96
    .packed-switch 0x0
        :pswitch_2a
        :pswitch_66
        :pswitch_43
        :pswitch_90
    .end packed-switch
.end method

.method public setBarImageResource(I)V
    .registers 4
    .parameter "resId"

    .prologue
    .line 94
    iget-object v1, p0, Lmiui/widget/SlidingButton;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .line 95
    .local v0, drawable:Landroid/graphics/drawable/BitmapDrawable;
    invoke-direct {p0, v0}, Lmiui/widget/SlidingButton;->createBarBitmap(Landroid/graphics/drawable/BitmapDrawable;)V

    .line 96
    return-void
.end method

.method public setButtonDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 2
    .parameter "d"

    .prologue
    .line 190
    return-void
.end method

.method public setChecked(Z)V
    .registers 3
    .parameter "checked"

    .prologue
    .line 173
    invoke-super {p0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 174
    if-eqz p1, :cond_1a

    .line 175
    iget v0, p0, Lmiui/widget/SlidingButton;->mSliderPositionEnd:I

    iput v0, p0, Lmiui/widget/SlidingButton;->mSliderOffset:I

    .line 176
    iget-object v0, p0, Lmiui/widget/SlidingButton;->mSlideOn:Landroid/graphics/drawable/BitmapDrawable;

    iput-object v0, p0, Lmiui/widget/SlidingButton;->mSlideState:Landroid/graphics/drawable/BitmapDrawable;

    .line 177
    iget-object v0, p0, Lmiui/widget/SlidingButton;->mSlideState:Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {p0, v0}, Lmiui/widget/SlidingButton;->createBarBitmap(Landroid/graphics/drawable/BitmapDrawable;)V

    .line 183
    :goto_12
    iget-object v0, p0, Lmiui/widget/SlidingButton;->mSlider:Landroid/graphics/drawable/BitmapDrawable;

    iput-object v0, p0, Lmiui/widget/SlidingButton;->mActiveSlider:Landroid/graphics/drawable/BitmapDrawable;

    .line 184
    invoke-virtual {p0}, Lmiui/widget/SlidingButton;->invalidate()V

    .line 185
    return-void

    .line 179
    :cond_1a
    iget v0, p0, Lmiui/widget/SlidingButton;->mSliderPositionStart:I

    iput v0, p0, Lmiui/widget/SlidingButton;->mSliderOffset:I

    .line 180
    iget-object v0, p0, Lmiui/widget/SlidingButton;->mSlideOff:Landroid/graphics/drawable/BitmapDrawable;

    iput-object v0, p0, Lmiui/widget/SlidingButton;->mSlideState:Landroid/graphics/drawable/BitmapDrawable;

    .line 181
    iget-object v0, p0, Lmiui/widget/SlidingButton;->mSlideState:Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {p0, v0}, Lmiui/widget/SlidingButton;->createBarBitmap(Landroid/graphics/drawable/BitmapDrawable;)V

    goto :goto_12
.end method

.method public setOnCheckedChangedListener(Lmiui/widget/SlidingButton$OnCheckedChangedListener;)V
    .registers 2
    .parameter "listener"

    .prologue
    .line 77
    iput-object p1, p0, Lmiui/widget/SlidingButton;->mOnCheckedChangedListener:Lmiui/widget/SlidingButton$OnCheckedChangedListener;

    .line 78
    return-void
.end method
