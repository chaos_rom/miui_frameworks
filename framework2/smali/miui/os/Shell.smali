.class public Lmiui/os/Shell;
.super Ljava/lang/Object;
.source "Shell.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "Shell"


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 6
    const-string v0, "shell_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 7
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 3
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static chmod(Ljava/lang/String;I)Z
    .registers 3
    .parameter "path"
    .parameter "mode"

    .prologue
    .line 10
    invoke-static {p0, p1}, Lmiui/os/Shell;->nativeChmod(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public static chown(Ljava/lang/String;II)Z
    .registers 4
    .parameter "path"
    .parameter "owner"
    .parameter "group"

    .prologue
    .line 14
    invoke-static {p0, p1, p2}, Lmiui/os/Shell;->nativeChown(Ljava/lang/String;II)Z

    move-result v0

    return v0
.end method

.method public static copy(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 3
    .parameter "source"
    .parameter "dest"

    .prologue
    .line 18
    invoke-static {p0, p1}, Lmiui/os/Shell;->nativeCopy(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static link(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 3
    .parameter "oldPath"
    .parameter "newPath"

    .prologue
    .line 22
    invoke-static {p0, p1}, Lmiui/os/Shell;->nativeLink(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static mkdirs(Ljava/lang/String;)Z
    .registers 2
    .parameter "path"

    .prologue
    .line 26
    invoke-static {p0}, Lmiui/os/Shell;->nativeMkdirs(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static move(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 3
    .parameter "oldPath"
    .parameter "newPath"

    .prologue
    .line 30
    invoke-static {p0, p1}, Lmiui/os/Shell;->nativeMove(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static native nativeChmod(Ljava/lang/String;I)Z
.end method

.method private static native nativeChown(Ljava/lang/String;II)Z
.end method

.method private static native nativeCopy(Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method private static native nativeLink(Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method private static native nativeMkdirs(Ljava/lang/String;)Z
.end method

.method private static native nativeMove(Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method private static native nativeRemove(Ljava/lang/String;)Z
.end method

.method private static native nativeRun(Ljava/lang/String;)Z
.end method

.method public static remove(Ljava/lang/String;)Z
    .registers 2
    .parameter "path"

    .prologue
    .line 34
    invoke-static {p0}, Lmiui/os/Shell;->nativeRemove(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static varargs run(Ljava/lang/String;[Ljava/lang/Object;)Z
    .registers 4
    .parameter "command"
    .parameter "args"

    .prologue
    .line 38
    array-length v1, p1

    if-lez v1, :cond_c

    invoke-static {p0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 39
    .local v0, cmd:Ljava/lang/String;
    :goto_7
    invoke-static {v0}, Lmiui/os/Shell;->nativeRun(Ljava/lang/String;)Z

    move-result v1

    return v1

    .end local v0           #cmd:Ljava/lang/String;
    :cond_c
    move-object v0, p0

    .line 38
    goto :goto_7
.end method
