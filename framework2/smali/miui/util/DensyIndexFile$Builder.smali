.class public Lmiui/util/DensyIndexFile$Builder;
.super Ljava/lang/Object;
.source "DensyIndexFile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/util/DensyIndexFile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/util/DensyIndexFile$Builder$IndexData;,
        Lmiui/util/DensyIndexFile$Builder$DataItemHolder;,
        Lmiui/util/DensyIndexFile$Builder$Item;
    }
.end annotation


# instance fields
.field mCurrentIndexData:Lmiui/util/DensyIndexFile$Builder$IndexData;

.field mFileHeader:Lmiui/util/DensyIndexFile$FileHeader;

.field mIndexDataList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lmiui/util/DensyIndexFile$Builder$IndexData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 764
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 765
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/util/DensyIndexFile$Builder;->mIndexDataList:Ljava/util/ArrayList;

    .line 766
    return-void
.end method

.method private build()V
    .registers 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v10, 0x0

    .line 943
    iget-object v8, p0, Lmiui/util/DensyIndexFile$Builder;->mIndexDataList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 944
    .local v0, dataCount:I
    new-instance v8, Lmiui/util/DensyIndexFile$FileHeader;

    invoke-direct {v8, v0}, Lmiui/util/DensyIndexFile$FileHeader;-><init>(I)V

    iput-object v8, p0, Lmiui/util/DensyIndexFile$Builder;->mFileHeader:Lmiui/util/DensyIndexFile$FileHeader;

    .line 946
    const/4 v4, 0x0

    .local v4, k:I
    :goto_10
    if-ge v4, v0, :cond_7d

    .line 947
    iget-object v8, p0, Lmiui/util/DensyIndexFile$Builder;->mIndexDataList:Ljava/util/ArrayList;

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/util/DensyIndexFile$Builder$IndexData;

    .line 948
    .local v2, idk:Lmiui/util/DensyIndexFile$Builder$IndexData;
    iget-object v8, p0, Lmiui/util/DensyIndexFile$Builder;->mFileHeader:Lmiui/util/DensyIndexFile$FileHeader;

    iget-object v8, v8, Lmiui/util/DensyIndexFile$FileHeader;->mDescriptionOffsets:[Lmiui/util/DensyIndexFile$DescriptionPair;

    new-instance v9, Lmiui/util/DensyIndexFile$DescriptionPair;

    invoke-direct {v9, v10, v11, v10, v11}, Lmiui/util/DensyIndexFile$DescriptionPair;-><init>(JJ)V

    aput-object v9, v8, v4

    .line 950
    const/4 v5, 0x0

    .line 951
    .local v5, length:I
    const/4 v5, 0x0

    :goto_27
    iget-object v8, v2, Lmiui/util/DensyIndexFile$Builder$IndexData;->mIndexDataGroups:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v5, v8, :cond_3d

    .line 952
    iget-object v8, v2, Lmiui/util/DensyIndexFile$Builder$IndexData;->mIndexDataGroups:Ljava/util/ArrayList;

    invoke-virtual {v8, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-nez v8, :cond_77

    .line 957
    :cond_3d
    new-array v8, v5, [Lmiui/util/DensyIndexFile$IndexGroupDescription;

    iput-object v8, v2, Lmiui/util/DensyIndexFile$Builder$IndexData;->mIndexGroupDescriptions:[Lmiui/util/DensyIndexFile$IndexGroupDescription;

    .line 958
    const/4 v1, 0x0

    .local v1, i:I
    :goto_42
    iget-object v8, v2, Lmiui/util/DensyIndexFile$Builder$IndexData;->mIndexGroupDescriptions:[Lmiui/util/DensyIndexFile$IndexGroupDescription;

    array-length v8, v8

    if-ge v1, v8, :cond_7a

    .line 959
    iget-object v8, v2, Lmiui/util/DensyIndexFile$Builder$IndexData;->mIndexDataGroups:Ljava/util/ArrayList;

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/ArrayList;

    .line 960
    .local v3, items:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lmiui/util/DensyIndexFile$Builder$Item;>;"
    invoke-static {v3}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 962
    const/4 v8, 0x0

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lmiui/util/DensyIndexFile$Builder$Item;

    iget v7, v8, Lmiui/util/DensyIndexFile$Builder$Item;->mIndex:I

    .line 963
    .local v7, minIndex:I
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lmiui/util/DensyIndexFile$Builder$Item;

    iget v8, v8, Lmiui/util/DensyIndexFile$Builder$Item;->mIndex:I

    add-int/lit8 v6, v8, 0x1

    .line 964
    .local v6, maxIndex:I
    iget-object v8, v2, Lmiui/util/DensyIndexFile$Builder$IndexData;->mIndexGroupDescriptions:[Lmiui/util/DensyIndexFile$IndexGroupDescription;

    new-instance v9, Lmiui/util/DensyIndexFile$IndexGroupDescription;

    invoke-direct {v9, v7, v6, v10, v11}, Lmiui/util/DensyIndexFile$IndexGroupDescription;-><init>(IIJ)V

    aput-object v9, v8, v1

    .line 958
    add-int/lit8 v1, v1, 0x1

    goto :goto_42

    .line 951
    .end local v1           #i:I
    .end local v3           #items:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lmiui/util/DensyIndexFile$Builder$Item;>;"
    .end local v6           #maxIndex:I
    .end local v7           #minIndex:I
    :cond_77
    add-int/lit8 v5, v5, 0x1

    goto :goto_27

    .line 946
    .restart local v1       #i:I
    :cond_7a
    add-int/lit8 v4, v4, 0x1

    goto :goto_10

    .line 968
    .end local v1           #i:I
    .end local v2           #idk:Lmiui/util/DensyIndexFile$Builder$IndexData;
    .end local v5           #length:I
    :cond_7d
    const/4 v8, 0x0

    invoke-direct {p0, v8}, Lmiui/util/DensyIndexFile$Builder;->writeAll(Ljava/io/DataOutput;)I

    .line 969
    return-void
.end method

.method private checkCurrentIndexingDataKind()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 930
    iget-object v0, p0, Lmiui/util/DensyIndexFile$Builder;->mCurrentIndexData:Lmiui/util/DensyIndexFile$Builder$IndexData;

    if-nez v0, :cond_c

    .line 931
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Please add a data kind before adding group"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 933
    :cond_c
    return-void
.end method

.method private checkCurrentIndexingGroup()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 936
    invoke-direct {p0}, Lmiui/util/DensyIndexFile$Builder;->checkCurrentIndexingDataKind()V

    .line 937
    iget-object v0, p0, Lmiui/util/DensyIndexFile$Builder;->mCurrentIndexData:Lmiui/util/DensyIndexFile$Builder$IndexData;

    iget-object v0, v0, Lmiui/util/DensyIndexFile$Builder$IndexData;->mIndexDataGroups:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_15

    .line 938
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Please add a data group before adding data"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 940
    :cond_15
    return-void
.end method

.method private writeAll(Ljava/io/DataOutput;)I
    .registers 13
    .parameter "o"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 972
    const/4 v7, 0x0

    .line 973
    .local v7, written:I
    const/4 v4, 0x0

    .local v4, k:I
    :goto_2
    iget-object v8, p0, Lmiui/util/DensyIndexFile$Builder;->mFileHeader:Lmiui/util/DensyIndexFile$FileHeader;

    iget-object v8, v8, Lmiui/util/DensyIndexFile$FileHeader;->mDescriptionOffsets:[Lmiui/util/DensyIndexFile$DescriptionPair;

    array-length v8, v8

    if-ge v4, v8, :cond_14b

    .line 974
    iget-object v8, p0, Lmiui/util/DensyIndexFile$Builder;->mFileHeader:Lmiui/util/DensyIndexFile$FileHeader;

    invoke-virtual {v8, p1}, Lmiui/util/DensyIndexFile$FileHeader;->write(Ljava/io/DataOutput;)I

    move-result v8

    add-int/2addr v7, v8

    .line 975
    iget-object v8, p0, Lmiui/util/DensyIndexFile$Builder;->mFileHeader:Lmiui/util/DensyIndexFile$FileHeader;

    iget-object v8, v8, Lmiui/util/DensyIndexFile$FileHeader;->mDescriptionOffsets:[Lmiui/util/DensyIndexFile$DescriptionPair;

    aget-object v8, v8, v4

    int-to-long v9, v7

    iput-wide v9, v8, Lmiui/util/DensyIndexFile$DescriptionPair;->mIndexGroupDescriptionOffset:J

    .line 977
    iget-object v8, p0, Lmiui/util/DensyIndexFile$Builder;->mIndexDataList:Ljava/util/ArrayList;

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/util/DensyIndexFile$Builder$IndexData;

    .line 978
    .local v1, idk:Lmiui/util/DensyIndexFile$Builder$IndexData;
    if-eqz p1, :cond_29

    .line 979
    iget-object v8, v1, Lmiui/util/DensyIndexFile$Builder$IndexData;->mIndexGroupDescriptions:[Lmiui/util/DensyIndexFile$IndexGroupDescription;

    array-length v8, v8

    invoke-interface {p1, v8}, Ljava/io/DataOutput;->writeInt(I)V

    .line 981
    :cond_29
    add-int/lit8 v7, v7, 0x4

    .line 982
    const/4 v0, 0x0

    .local v0, i:I
    :goto_2c
    iget-object v8, v1, Lmiui/util/DensyIndexFile$Builder$IndexData;->mIndexGroupDescriptions:[Lmiui/util/DensyIndexFile$IndexGroupDescription;

    array-length v8, v8

    if-ge v0, v8, :cond_3d

    .line 983
    iget-object v8, v1, Lmiui/util/DensyIndexFile$Builder$IndexData;->mIndexGroupDescriptions:[Lmiui/util/DensyIndexFile$IndexGroupDescription;

    aget-object v8, v8, v0

    invoke-virtual {v8, p1}, Lmiui/util/DensyIndexFile$IndexGroupDescription;->write(Ljava/io/DataOutput;)I

    move-result v8

    add-int/2addr v7, v8

    .line 982
    add-int/lit8 v0, v0, 0x1

    goto :goto_2c

    .line 986
    :cond_3d
    iget-object v8, p0, Lmiui/util/DensyIndexFile$Builder;->mFileHeader:Lmiui/util/DensyIndexFile$FileHeader;

    iget-object v8, v8, Lmiui/util/DensyIndexFile$FileHeader;->mDescriptionOffsets:[Lmiui/util/DensyIndexFile$DescriptionPair;

    aget-object v8, v8, v4

    int-to-long v9, v7

    iput-wide v9, v8, Lmiui/util/DensyIndexFile$DescriptionPair;->mDataItemDescriptionOffset:J

    .line 987
    if-eqz p1, :cond_4e

    .line 988
    iget-object v8, v1, Lmiui/util/DensyIndexFile$Builder$IndexData;->mDataItemDescriptions:[Lmiui/util/DensyIndexFile$DataItemDescription;

    array-length v8, v8

    invoke-interface {p1, v8}, Ljava/io/DataOutput;->writeInt(I)V

    .line 990
    :cond_4e
    add-int/lit8 v7, v7, 0x4

    .line 991
    const/4 v0, 0x0

    :goto_51
    iget-object v8, v1, Lmiui/util/DensyIndexFile$Builder$IndexData;->mDataItemDescriptions:[Lmiui/util/DensyIndexFile$DataItemDescription;

    array-length v8, v8

    if-ge v0, v8, :cond_62

    .line 992
    iget-object v8, v1, Lmiui/util/DensyIndexFile$Builder$IndexData;->mDataItemDescriptions:[Lmiui/util/DensyIndexFile$DataItemDescription;

    aget-object v8, v8, v0

    invoke-virtual {v8, p1}, Lmiui/util/DensyIndexFile$DataItemDescription;->write(Ljava/io/DataOutput;)I

    move-result v8

    add-int/2addr v7, v8

    .line 991
    add-int/lit8 v0, v0, 0x1

    goto :goto_51

    .line 995
    :cond_62
    const/4 v0, 0x0

    :goto_63
    iget-object v8, v1, Lmiui/util/DensyIndexFile$Builder$IndexData;->mDataItemDescriptions:[Lmiui/util/DensyIndexFile$DataItemDescription;

    array-length v8, v8

    if-ge v0, v8, :cond_87

    .line 996
    iget-object v8, v1, Lmiui/util/DensyIndexFile$Builder$IndexData;->mDataItemDescriptions:[Lmiui/util/DensyIndexFile$DataItemDescription;

    aget-object v8, v8, v0

    int-to-long v9, v7

    iput-wide v9, v8, Lmiui/util/DensyIndexFile$DataItemDescription;->mOffset:J

    .line 997
    iget-object v8, v1, Lmiui/util/DensyIndexFile$Builder$IndexData;->mDataItemDescriptions:[Lmiui/util/DensyIndexFile$DataItemDescription;

    aget-object v9, v8, v0

    iget-object v8, v1, Lmiui/util/DensyIndexFile$Builder$IndexData;->mDataItemHolders:Ljava/util/ArrayList;

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lmiui/util/DensyIndexFile$Builder$DataItemHolder;

    invoke-virtual {v8}, Lmiui/util/DensyIndexFile$Builder$DataItemHolder;->getAll()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v9, p1, v8}, Lmiui/util/DensyIndexFile$DataItemDescription;->writeDataItems(Ljava/io/DataOutput;Ljava/util/List;)I

    move-result v8

    add-int/2addr v7, v8

    .line 995
    add-int/lit8 v0, v0, 0x1

    goto :goto_63

    .line 1000
    :cond_87
    const/4 v0, 0x0

    :goto_88
    iget-object v8, v1, Lmiui/util/DensyIndexFile$Builder$IndexData;->mIndexGroupDescriptions:[Lmiui/util/DensyIndexFile$IndexGroupDescription;

    array-length v8, v8

    if-ge v0, v8, :cond_147

    .line 1001
    iget-object v8, v1, Lmiui/util/DensyIndexFile$Builder$IndexData;->mIndexGroupDescriptions:[Lmiui/util/DensyIndexFile$IndexGroupDescription;

    aget-object v8, v8, v0

    int-to-long v9, v7

    iput-wide v9, v8, Lmiui/util/DensyIndexFile$IndexGroupDescription;->mOffset:J

    .line 1002
    if-nez p1, :cond_b9

    .line 1003
    const/4 v6, 0x0

    .line 1004
    .local v6, sizeOfItem:I
    const/4 v5, 0x0

    .local v5, m:I
    :goto_98
    iget-object v8, v1, Lmiui/util/DensyIndexFile$Builder$IndexData;->mDataItemDescriptions:[Lmiui/util/DensyIndexFile$DataItemDescription;

    array-length v8, v8

    if-ge v5, v8, :cond_a7

    .line 1005
    iget-object v8, v1, Lmiui/util/DensyIndexFile$Builder$IndexData;->mDataItemDescriptions:[Lmiui/util/DensyIndexFile$DataItemDescription;

    aget-object v8, v8, v5

    iget-byte v8, v8, Lmiui/util/DensyIndexFile$DataItemDescription;->mIndexSize:B

    add-int/2addr v6, v8

    .line 1004
    add-int/lit8 v5, v5, 0x1

    goto :goto_98

    .line 1007
    :cond_a7
    iget-object v8, v1, Lmiui/util/DensyIndexFile$Builder$IndexData;->mIndexGroupDescriptions:[Lmiui/util/DensyIndexFile$IndexGroupDescription;

    aget-object v8, v8, v0

    iget v8, v8, Lmiui/util/DensyIndexFile$IndexGroupDescription;->mMaxIndex:I

    iget-object v9, v1, Lmiui/util/DensyIndexFile$Builder$IndexData;->mIndexGroupDescriptions:[Lmiui/util/DensyIndexFile$IndexGroupDescription;

    aget-object v9, v9, v0

    iget v9, v9, Lmiui/util/DensyIndexFile$IndexGroupDescription;->mMinIndex:I

    sub-int/2addr v8, v9

    mul-int/2addr v8, v6

    add-int/2addr v7, v8

    .line 1000
    .end local v5           #m:I
    .end local v6           #sizeOfItem:I
    :cond_b6
    add-int/lit8 v0, v0, 0x1

    goto :goto_88

    .line 1009
    :cond_b9
    iget-object v8, v1, Lmiui/util/DensyIndexFile$Builder$IndexData;->mIndexGroupDescriptions:[Lmiui/util/DensyIndexFile$IndexGroupDescription;

    aget-object v8, v8, v0

    iget v3, v8, Lmiui/util/DensyIndexFile$IndexGroupDescription;->mMinIndex:I

    .local v3, j:I
    :goto_bf
    iget-object v8, v1, Lmiui/util/DensyIndexFile$Builder$IndexData;->mIndexGroupDescriptions:[Lmiui/util/DensyIndexFile$IndexGroupDescription;

    aget-object v8, v8, v0

    iget v8, v8, Lmiui/util/DensyIndexFile$IndexGroupDescription;->mMaxIndex:I

    if-ge v3, v8, :cond_b6

    .line 1010
    iget-object v8, v1, Lmiui/util/DensyIndexFile$Builder$IndexData;->mDataMap:Ljava/util/HashMap;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/util/DensyIndexFile$Builder$Item;

    .line 1011
    .local v2, item:Lmiui/util/DensyIndexFile$Builder$Item;
    if-nez v2, :cond_d7

    .line 1012
    iget-object v2, v1, Lmiui/util/DensyIndexFile$Builder$IndexData;->mDefaultValue:Lmiui/util/DensyIndexFile$Builder$Item;

    .line 1014
    :cond_d7
    const/4 v5, 0x0

    .restart local v5       #m:I
    :goto_d8
    iget-object v8, v1, Lmiui/util/DensyIndexFile$Builder$IndexData;->mDataItemDescriptions:[Lmiui/util/DensyIndexFile$DataItemDescription;

    array-length v8, v8

    if-ge v5, v8, :cond_143

    .line 1015
    iget-object v8, v1, Lmiui/util/DensyIndexFile$Builder$IndexData;->mDataItemDescriptions:[Lmiui/util/DensyIndexFile$DataItemDescription;

    aget-object v8, v8, v5

    iget-byte v8, v8, Lmiui/util/DensyIndexFile$DataItemDescription;->mIndexSize:B

    const/4 v9, 0x1

    if-ne v8, v9, :cond_fd

    .line 1016
    iget-object v8, v2, Lmiui/util/DensyIndexFile$Builder$Item;->mObjects:[Ljava/lang/Object;

    aget-object v8, v8, v5

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-interface {p1, v8}, Ljava/io/DataOutput;->writeByte(I)V

    .line 1024
    :cond_f3
    :goto_f3
    iget-object v8, v1, Lmiui/util/DensyIndexFile$Builder$IndexData;->mDataItemDescriptions:[Lmiui/util/DensyIndexFile$DataItemDescription;

    aget-object v8, v8, v5

    iget-byte v8, v8, Lmiui/util/DensyIndexFile$DataItemDescription;->mIndexSize:B

    add-int/2addr v7, v8

    .line 1014
    add-int/lit8 v5, v5, 0x1

    goto :goto_d8

    .line 1017
    :cond_fd
    iget-object v8, v1, Lmiui/util/DensyIndexFile$Builder$IndexData;->mDataItemDescriptions:[Lmiui/util/DensyIndexFile$DataItemDescription;

    aget-object v8, v8, v5

    iget-byte v8, v8, Lmiui/util/DensyIndexFile$DataItemDescription;->mIndexSize:B

    const/4 v9, 0x2

    if-ne v8, v9, :cond_114

    .line 1018
    iget-object v8, v2, Lmiui/util/DensyIndexFile$Builder$Item;->mObjects:[Ljava/lang/Object;

    aget-object v8, v8, v5

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-interface {p1, v8}, Ljava/io/DataOutput;->writeShort(I)V

    goto :goto_f3

    .line 1019
    :cond_114
    iget-object v8, v1, Lmiui/util/DensyIndexFile$Builder$IndexData;->mDataItemDescriptions:[Lmiui/util/DensyIndexFile$DataItemDescription;

    aget-object v8, v8, v5

    iget-byte v8, v8, Lmiui/util/DensyIndexFile$DataItemDescription;->mIndexSize:B

    const/4 v9, 0x4

    if-ne v8, v9, :cond_12b

    .line 1020
    iget-object v8, v2, Lmiui/util/DensyIndexFile$Builder$Item;->mObjects:[Ljava/lang/Object;

    aget-object v8, v8, v5

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-interface {p1, v8}, Ljava/io/DataOutput;->writeInt(I)V

    goto :goto_f3

    .line 1021
    :cond_12b
    iget-object v8, v1, Lmiui/util/DensyIndexFile$Builder$IndexData;->mDataItemDescriptions:[Lmiui/util/DensyIndexFile$DataItemDescription;

    aget-object v8, v8, v5

    iget-byte v8, v8, Lmiui/util/DensyIndexFile$DataItemDescription;->mIndexSize:B

    const/16 v9, 0x8

    if-ne v8, v9, :cond_f3

    .line 1022
    iget-object v8, v2, Lmiui/util/DensyIndexFile$Builder$Item;->mObjects:[Ljava/lang/Object;

    aget-object v8, v8, v5

    check-cast v8, Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-interface {p1, v8, v9}, Ljava/io/DataOutput;->writeLong(J)V

    goto :goto_f3

    .line 1009
    :cond_143
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_bf

    .line 973
    .end local v2           #item:Lmiui/util/DensyIndexFile$Builder$Item;
    .end local v3           #j:I
    .end local v5           #m:I
    :cond_147
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_2

    .line 1030
    .end local v0           #i:I
    .end local v1           #idk:Lmiui/util/DensyIndexFile$Builder$IndexData;
    :cond_14b
    return v7
.end method


# virtual methods
.method public varargs add(I[Ljava/lang/Object;)V
    .registers 8
    .parameter "index"
    .parameter "objects"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 839
    invoke-direct {p0}, Lmiui/util/DensyIndexFile$Builder;->checkCurrentIndexingGroup()V

    .line 841
    iget-object v2, p0, Lmiui/util/DensyIndexFile$Builder;->mCurrentIndexData:Lmiui/util/DensyIndexFile$Builder$IndexData;

    iget-object v2, v2, Lmiui/util/DensyIndexFile$Builder$IndexData;->mDataItemDescriptions:[Lmiui/util/DensyIndexFile$DataItemDescription;

    array-length v2, v2

    array-length v3, p2

    if-eq v2, v3, :cond_13

    .line 842
    new-instance v2, Ljava/io/IOException;

    const-string v3, "Different number of objects inputted"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 845
    :cond_13
    const/4 v0, 0x0

    .local v0, i:I
    :goto_14
    array-length v2, p2

    if-ge v0, v2, :cond_25e

    .line 846
    iget-object v2, p0, Lmiui/util/DensyIndexFile$Builder;->mCurrentIndexData:Lmiui/util/DensyIndexFile$Builder$IndexData;

    iget-object v2, v2, Lmiui/util/DensyIndexFile$Builder$IndexData;->mDataItemDescriptions:[Lmiui/util/DensyIndexFile$DataItemDescription;

    aget-object v2, v2, v0

    iget-byte v2, v2, Lmiui/util/DensyIndexFile$DataItemDescription;->mType:B

    packed-switch v2, :pswitch_data_286

    .line 903
    new-instance v2, Ljava/io/IOException;

    const-string v3, "Unsupported type of objects inputted"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 848
    :pswitch_2a
    aget-object v2, p2, v0

    instance-of v2, v2, Ljava/lang/Byte;

    if-nez v2, :cond_10f

    .line 849
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Object["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] should be byte"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 853
    :pswitch_4f
    aget-object v2, p2, v0

    instance-of v2, v2, Ljava/lang/Short;

    if-nez v2, :cond_10f

    .line 854
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Object["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] should be short"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 858
    :pswitch_74
    aget-object v2, p2, v0

    instance-of v2, v2, Ljava/lang/Integer;

    if-nez v2, :cond_10f

    .line 859
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Object["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] should be int"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 863
    :pswitch_99
    aget-object v2, p2, v0

    instance-of v2, v2, Ljava/lang/Long;

    if-nez v2, :cond_10f

    .line 864
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Object["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] should be long"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 868
    :pswitch_be
    aget-object v2, p2, v0

    instance-of v2, v2, Ljava/lang/String;

    if-nez v2, :cond_e3

    .line 869
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Object["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] should be String"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 871
    :cond_e3
    iget-object v2, p0, Lmiui/util/DensyIndexFile$Builder;->mCurrentIndexData:Lmiui/util/DensyIndexFile$Builder$IndexData;

    iget-object v2, v2, Lmiui/util/DensyIndexFile$Builder$IndexData;->mDataItemHolders:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/util/DensyIndexFile$Builder$DataItemHolder;

    aget-object v3, p2, v0

    invoke-virtual {v2, v3}, Lmiui/util/DensyIndexFile$Builder$DataItemHolder;->put(Ljava/lang/Object;)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, p2, v0

    .line 872
    iget-object v2, p0, Lmiui/util/DensyIndexFile$Builder;->mCurrentIndexData:Lmiui/util/DensyIndexFile$Builder$IndexData;

    iget-object v2, v2, Lmiui/util/DensyIndexFile$Builder$IndexData;->mDataItemDescriptions:[Lmiui/util/DensyIndexFile$DataItemDescription;

    aget-object v3, v2, v0

    iget-object v2, p0, Lmiui/util/DensyIndexFile$Builder;->mCurrentIndexData:Lmiui/util/DensyIndexFile$Builder$IndexData;

    iget-object v2, v2, Lmiui/util/DensyIndexFile$Builder$IndexData;->mDataItemHolders:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/util/DensyIndexFile$Builder$DataItemHolder;

    invoke-virtual {v2}, Lmiui/util/DensyIndexFile$Builder$DataItemHolder;->size()I

    move-result v2

    invoke-static {v2}, Lmiui/util/DensyIndexFile;->getSizeOf(I)B

    move-result v2

    iput-byte v2, v3, Lmiui/util/DensyIndexFile$DataItemDescription;->mIndexSize:B

    .line 845
    :cond_10f
    :goto_10f
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_14

    .line 875
    :pswitch_113
    aget-object v2, p2, v0

    instance-of v2, v2, [B

    if-nez v2, :cond_138

    .line 876
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Object["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] should be byte[]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 878
    :cond_138
    iget-object v2, p0, Lmiui/util/DensyIndexFile$Builder;->mCurrentIndexData:Lmiui/util/DensyIndexFile$Builder$IndexData;

    iget-object v2, v2, Lmiui/util/DensyIndexFile$Builder$IndexData;->mDataItemHolders:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/util/DensyIndexFile$Builder$DataItemHolder;

    aget-object v3, p2, v0

    invoke-virtual {v2, v3}, Lmiui/util/DensyIndexFile$Builder$DataItemHolder;->put(Ljava/lang/Object;)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, p2, v0

    .line 879
    iget-object v2, p0, Lmiui/util/DensyIndexFile$Builder;->mCurrentIndexData:Lmiui/util/DensyIndexFile$Builder$IndexData;

    iget-object v2, v2, Lmiui/util/DensyIndexFile$Builder$IndexData;->mDataItemDescriptions:[Lmiui/util/DensyIndexFile$DataItemDescription;

    aget-object v3, v2, v0

    iget-object v2, p0, Lmiui/util/DensyIndexFile$Builder;->mCurrentIndexData:Lmiui/util/DensyIndexFile$Builder$IndexData;

    iget-object v2, v2, Lmiui/util/DensyIndexFile$Builder$IndexData;->mDataItemHolders:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/util/DensyIndexFile$Builder$DataItemHolder;

    invoke-virtual {v2}, Lmiui/util/DensyIndexFile$Builder$DataItemHolder;->size()I

    move-result v2

    invoke-static {v2}, Lmiui/util/DensyIndexFile;->getSizeOf(I)B

    move-result v2

    iput-byte v2, v3, Lmiui/util/DensyIndexFile$DataItemDescription;->mIndexSize:B

    goto :goto_10f

    .line 882
    :pswitch_165
    aget-object v2, p2, v0

    instance-of v2, v2, [S

    if-nez v2, :cond_18a

    .line 883
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Object["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] should be short[]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 885
    :cond_18a
    iget-object v2, p0, Lmiui/util/DensyIndexFile$Builder;->mCurrentIndexData:Lmiui/util/DensyIndexFile$Builder$IndexData;

    iget-object v2, v2, Lmiui/util/DensyIndexFile$Builder$IndexData;->mDataItemHolders:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/util/DensyIndexFile$Builder$DataItemHolder;

    aget-object v3, p2, v0

    invoke-virtual {v2, v3}, Lmiui/util/DensyIndexFile$Builder$DataItemHolder;->put(Ljava/lang/Object;)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, p2, v0

    .line 886
    iget-object v2, p0, Lmiui/util/DensyIndexFile$Builder;->mCurrentIndexData:Lmiui/util/DensyIndexFile$Builder$IndexData;

    iget-object v2, v2, Lmiui/util/DensyIndexFile$Builder$IndexData;->mDataItemDescriptions:[Lmiui/util/DensyIndexFile$DataItemDescription;

    aget-object v3, v2, v0

    iget-object v2, p0, Lmiui/util/DensyIndexFile$Builder;->mCurrentIndexData:Lmiui/util/DensyIndexFile$Builder$IndexData;

    iget-object v2, v2, Lmiui/util/DensyIndexFile$Builder$IndexData;->mDataItemHolders:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/util/DensyIndexFile$Builder$DataItemHolder;

    invoke-virtual {v2}, Lmiui/util/DensyIndexFile$Builder$DataItemHolder;->size()I

    move-result v2

    invoke-static {v2}, Lmiui/util/DensyIndexFile;->getSizeOf(I)B

    move-result v2

    iput-byte v2, v3, Lmiui/util/DensyIndexFile$DataItemDescription;->mIndexSize:B

    goto/16 :goto_10f

    .line 889
    :pswitch_1b8
    aget-object v2, p2, v0

    instance-of v2, v2, [I

    if-nez v2, :cond_1dd

    .line 890
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Object["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] should be int[]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 892
    :cond_1dd
    iget-object v2, p0, Lmiui/util/DensyIndexFile$Builder;->mCurrentIndexData:Lmiui/util/DensyIndexFile$Builder$IndexData;

    iget-object v2, v2, Lmiui/util/DensyIndexFile$Builder$IndexData;->mDataItemHolders:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/util/DensyIndexFile$Builder$DataItemHolder;

    aget-object v3, p2, v0

    invoke-virtual {v2, v3}, Lmiui/util/DensyIndexFile$Builder$DataItemHolder;->put(Ljava/lang/Object;)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, p2, v0

    .line 893
    iget-object v2, p0, Lmiui/util/DensyIndexFile$Builder;->mCurrentIndexData:Lmiui/util/DensyIndexFile$Builder$IndexData;

    iget-object v2, v2, Lmiui/util/DensyIndexFile$Builder$IndexData;->mDataItemDescriptions:[Lmiui/util/DensyIndexFile$DataItemDescription;

    aget-object v3, v2, v0

    iget-object v2, p0, Lmiui/util/DensyIndexFile$Builder;->mCurrentIndexData:Lmiui/util/DensyIndexFile$Builder$IndexData;

    iget-object v2, v2, Lmiui/util/DensyIndexFile$Builder$IndexData;->mDataItemHolders:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/util/DensyIndexFile$Builder$DataItemHolder;

    invoke-virtual {v2}, Lmiui/util/DensyIndexFile$Builder$DataItemHolder;->size()I

    move-result v2

    invoke-static {v2}, Lmiui/util/DensyIndexFile;->getSizeOf(I)B

    move-result v2

    iput-byte v2, v3, Lmiui/util/DensyIndexFile$DataItemDescription;->mIndexSize:B

    goto/16 :goto_10f

    .line 896
    :pswitch_20b
    aget-object v2, p2, v0

    instance-of v2, v2, [J

    if-nez v2, :cond_230

    .line 897
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Object["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] should be long[]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 899
    :cond_230
    iget-object v2, p0, Lmiui/util/DensyIndexFile$Builder;->mCurrentIndexData:Lmiui/util/DensyIndexFile$Builder$IndexData;

    iget-object v2, v2, Lmiui/util/DensyIndexFile$Builder$IndexData;->mDataItemHolders:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/util/DensyIndexFile$Builder$DataItemHolder;

    aget-object v3, p2, v0

    invoke-virtual {v2, v3}, Lmiui/util/DensyIndexFile$Builder$DataItemHolder;->put(Ljava/lang/Object;)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, p2, v0

    .line 900
    iget-object v2, p0, Lmiui/util/DensyIndexFile$Builder;->mCurrentIndexData:Lmiui/util/DensyIndexFile$Builder$IndexData;

    iget-object v2, v2, Lmiui/util/DensyIndexFile$Builder$IndexData;->mDataItemDescriptions:[Lmiui/util/DensyIndexFile$DataItemDescription;

    aget-object v3, v2, v0

    iget-object v2, p0, Lmiui/util/DensyIndexFile$Builder;->mCurrentIndexData:Lmiui/util/DensyIndexFile$Builder$IndexData;

    iget-object v2, v2, Lmiui/util/DensyIndexFile$Builder$IndexData;->mDataItemHolders:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/util/DensyIndexFile$Builder$DataItemHolder;

    invoke-virtual {v2}, Lmiui/util/DensyIndexFile$Builder$DataItemHolder;->size()I

    move-result v2

    invoke-static {v2}, Lmiui/util/DensyIndexFile;->getSizeOf(I)B

    move-result v2

    iput-byte v2, v3, Lmiui/util/DensyIndexFile$DataItemDescription;->mIndexSize:B

    goto/16 :goto_10f

    .line 907
    :cond_25e
    new-instance v1, Lmiui/util/DensyIndexFile$Builder$Item;

    invoke-direct {v1, p0, p1, p2}, Lmiui/util/DensyIndexFile$Builder$Item;-><init>(Lmiui/util/DensyIndexFile$Builder;I[Ljava/lang/Object;)V

    .line 908
    .local v1, item:Lmiui/util/DensyIndexFile$Builder$Item;
    iget-object v2, p0, Lmiui/util/DensyIndexFile$Builder;->mCurrentIndexData:Lmiui/util/DensyIndexFile$Builder$IndexData;

    iget-object v2, v2, Lmiui/util/DensyIndexFile$Builder$IndexData;->mDataMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 909
    iget-object v2, p0, Lmiui/util/DensyIndexFile$Builder;->mCurrentIndexData:Lmiui/util/DensyIndexFile$Builder$IndexData;

    iget-object v2, v2, Lmiui/util/DensyIndexFile$Builder$IndexData;->mIndexDataGroups:Ljava/util/ArrayList;

    iget-object v3, p0, Lmiui/util/DensyIndexFile$Builder;->mCurrentIndexData:Lmiui/util/DensyIndexFile$Builder$IndexData;

    iget-object v3, v3, Lmiui/util/DensyIndexFile$Builder$IndexData;->mIndexDataGroups:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 910
    return-void

    .line 846
    :pswitch_data_286
    .packed-switch 0x0
        :pswitch_2a
        :pswitch_4f
        :pswitch_74
        :pswitch_99
        :pswitch_be
        :pswitch_113
        :pswitch_165
        :pswitch_1b8
        :pswitch_20b
    .end packed-switch
.end method

.method public addGroup([I[[Ljava/lang/Object;)V
    .registers 6
    .parameter "indexes"
    .parameter "objects"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 826
    invoke-direct {p0}, Lmiui/util/DensyIndexFile$Builder;->checkCurrentIndexingDataKind()V

    .line 828
    array-length v1, p1

    array-length v2, p2

    if-ne v1, v2, :cond_18

    .line 829
    invoke-virtual {p0}, Lmiui/util/DensyIndexFile$Builder;->newGroup()V

    .line 830
    const/4 v0, 0x0

    .local v0, i:I
    :goto_b
    array-length v1, p1

    if-ge v0, v1, :cond_20

    .line 831
    aget v1, p1, v0

    aget-object v2, p2, v0

    invoke-virtual {p0, v1, v2}, Lmiui/util/DensyIndexFile$Builder;->add(I[Ljava/lang/Object;)V

    .line 830
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    .line 834
    .end local v0           #i:I
    :cond_18
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Different number between indexes and objects"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 836
    .restart local v0       #i:I
    :cond_20
    return-void
.end method

.method public varargs addKind([Ljava/lang/Object;)V
    .registers 11
    .parameter "objects"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 769
    new-instance v0, Lmiui/util/DensyIndexFile$Builder$IndexData;

    array-length v4, p1

    invoke-direct {v0, v4}, Lmiui/util/DensyIndexFile$Builder$IndexData;-><init>(I)V

    iput-object v0, p0, Lmiui/util/DensyIndexFile$Builder;->mCurrentIndexData:Lmiui/util/DensyIndexFile$Builder$IndexData;

    .line 770
    iget-object v0, p0, Lmiui/util/DensyIndexFile$Builder;->mIndexDataList:Ljava/util/ArrayList;

    iget-object v4, p0, Lmiui/util/DensyIndexFile$Builder;->mCurrentIndexData:Lmiui/util/DensyIndexFile$Builder$IndexData;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 772
    const/4 v7, 0x0

    .local v7, i:I
    :goto_11
    array-length v0, p1

    if-ge v7, v0, :cond_13a

    .line 773
    iget-object v0, p0, Lmiui/util/DensyIndexFile$Builder;->mCurrentIndexData:Lmiui/util/DensyIndexFile$Builder$IndexData;

    iget-object v0, v0, Lmiui/util/DensyIndexFile$Builder$IndexData;->mDataItemHolders:Ljava/util/ArrayList;

    new-instance v4, Lmiui/util/DensyIndexFile$Builder$DataItemHolder;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5}, Lmiui/util/DensyIndexFile$Builder$DataItemHolder;-><init>(Lmiui/util/DensyIndexFile$Builder;Lmiui/util/DensyIndexFile$1;)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 775
    const/4 v1, 0x0

    .line 776
    .local v1, type:B
    const/4 v2, 0x1

    .line 777
    .local v2, indexSize:B
    aget-object v0, p1, v7

    instance-of v0, v0, Ljava/lang/Byte;

    if-eqz v0, :cond_4b

    .line 778
    const/4 v1, 0x0

    .line 779
    const/4 v2, 0x1

    .line 780
    iget-object v0, p0, Lmiui/util/DensyIndexFile$Builder;->mCurrentIndexData:Lmiui/util/DensyIndexFile$Builder$IndexData;

    iget-object v0, v0, Lmiui/util/DensyIndexFile$Builder$IndexData;->mDataItemHolders:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/util/DensyIndexFile$Builder$DataItemHolder;

    aget-object v4, p1, v7

    invoke-virtual {v0, v4}, Lmiui/util/DensyIndexFile$Builder$DataItemHolder;->put(Ljava/lang/Object;)Ljava/lang/Integer;

    .line 812
    :goto_3a
    iget-object v0, p0, Lmiui/util/DensyIndexFile$Builder;->mCurrentIndexData:Lmiui/util/DensyIndexFile$Builder$IndexData;

    iget-object v8, v0, Lmiui/util/DensyIndexFile$Builder$IndexData;->mDataItemDescriptions:[Lmiui/util/DensyIndexFile$DataItemDescription;

    new-instance v0, Lmiui/util/DensyIndexFile$DataItemDescription;

    const-wide/16 v5, 0x0

    move v4, v3

    invoke-direct/range {v0 .. v6}, Lmiui/util/DensyIndexFile$DataItemDescription;-><init>(BBBBJ)V

    aput-object v0, v8, v7

    .line 772
    add-int/lit8 v7, v7, 0x1

    goto :goto_11

    .line 781
    :cond_4b
    aget-object v0, p1, v7

    instance-of v0, v0, Ljava/lang/Short;

    if-eqz v0, :cond_63

    .line 782
    const/4 v1, 0x1

    .line 783
    const/4 v2, 0x2

    .line 784
    iget-object v0, p0, Lmiui/util/DensyIndexFile$Builder;->mCurrentIndexData:Lmiui/util/DensyIndexFile$Builder$IndexData;

    iget-object v0, v0, Lmiui/util/DensyIndexFile$Builder$IndexData;->mDataItemHolders:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/util/DensyIndexFile$Builder$DataItemHolder;

    aget-object v4, p1, v7

    invoke-virtual {v0, v4}, Lmiui/util/DensyIndexFile$Builder$DataItemHolder;->put(Ljava/lang/Object;)Ljava/lang/Integer;

    goto :goto_3a

    .line 785
    :cond_63
    aget-object v0, p1, v7

    instance-of v0, v0, Ljava/lang/Integer;

    if-eqz v0, :cond_7b

    .line 786
    const/4 v1, 0x2

    .line 787
    const/4 v2, 0x4

    .line 788
    iget-object v0, p0, Lmiui/util/DensyIndexFile$Builder;->mCurrentIndexData:Lmiui/util/DensyIndexFile$Builder$IndexData;

    iget-object v0, v0, Lmiui/util/DensyIndexFile$Builder$IndexData;->mDataItemHolders:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/util/DensyIndexFile$Builder$DataItemHolder;

    aget-object v4, p1, v7

    invoke-virtual {v0, v4}, Lmiui/util/DensyIndexFile$Builder$DataItemHolder;->put(Ljava/lang/Object;)Ljava/lang/Integer;

    goto :goto_3a

    .line 789
    :cond_7b
    aget-object v0, p1, v7

    instance-of v0, v0, Ljava/lang/Long;

    if-eqz v0, :cond_94

    .line 790
    const/4 v1, 0x3

    .line 791
    const/16 v2, 0x8

    .line 792
    iget-object v0, p0, Lmiui/util/DensyIndexFile$Builder;->mCurrentIndexData:Lmiui/util/DensyIndexFile$Builder$IndexData;

    iget-object v0, v0, Lmiui/util/DensyIndexFile$Builder$IndexData;->mDataItemHolders:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/util/DensyIndexFile$Builder$DataItemHolder;

    aget-object v4, p1, v7

    invoke-virtual {v0, v4}, Lmiui/util/DensyIndexFile$Builder$DataItemHolder;->put(Ljava/lang/Object;)Ljava/lang/Integer;

    goto :goto_3a

    .line 793
    :cond_94
    aget-object v0, p1, v7

    instance-of v0, v0, Ljava/lang/String;

    if-eqz v0, :cond_ae

    .line 794
    const/4 v1, 0x4

    .line 795
    iget-object v0, p0, Lmiui/util/DensyIndexFile$Builder;->mCurrentIndexData:Lmiui/util/DensyIndexFile$Builder$IndexData;

    iget-object v0, v0, Lmiui/util/DensyIndexFile$Builder$IndexData;->mDataItemHolders:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/util/DensyIndexFile$Builder$DataItemHolder;

    aget-object v4, p1, v7

    invoke-virtual {v0, v4}, Lmiui/util/DensyIndexFile$Builder$DataItemHolder;->put(Ljava/lang/Object;)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p1, v7

    goto :goto_3a

    .line 796
    :cond_ae
    aget-object v0, p1, v7

    instance-of v0, v0, [B

    if-eqz v0, :cond_c9

    .line 797
    const/4 v1, 0x5

    .line 798
    iget-object v0, p0, Lmiui/util/DensyIndexFile$Builder;->mCurrentIndexData:Lmiui/util/DensyIndexFile$Builder$IndexData;

    iget-object v0, v0, Lmiui/util/DensyIndexFile$Builder$IndexData;->mDataItemHolders:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/util/DensyIndexFile$Builder$DataItemHolder;

    aget-object v4, p1, v7

    invoke-virtual {v0, v4}, Lmiui/util/DensyIndexFile$Builder$DataItemHolder;->put(Ljava/lang/Object;)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p1, v7

    goto/16 :goto_3a

    .line 799
    :cond_c9
    aget-object v0, p1, v7

    instance-of v0, v0, [S

    if-eqz v0, :cond_e4

    .line 800
    const/4 v1, 0x6

    .line 801
    iget-object v0, p0, Lmiui/util/DensyIndexFile$Builder;->mCurrentIndexData:Lmiui/util/DensyIndexFile$Builder$IndexData;

    iget-object v0, v0, Lmiui/util/DensyIndexFile$Builder$IndexData;->mDataItemHolders:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/util/DensyIndexFile$Builder$DataItemHolder;

    aget-object v4, p1, v7

    invoke-virtual {v0, v4}, Lmiui/util/DensyIndexFile$Builder$DataItemHolder;->put(Ljava/lang/Object;)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p1, v7

    goto/16 :goto_3a

    .line 802
    :cond_e4
    aget-object v0, p1, v7

    instance-of v0, v0, [I

    if-eqz v0, :cond_ff

    .line 803
    const/4 v1, 0x7

    .line 804
    iget-object v0, p0, Lmiui/util/DensyIndexFile$Builder;->mCurrentIndexData:Lmiui/util/DensyIndexFile$Builder$IndexData;

    iget-object v0, v0, Lmiui/util/DensyIndexFile$Builder$IndexData;->mDataItemHolders:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/util/DensyIndexFile$Builder$DataItemHolder;

    aget-object v4, p1, v7

    invoke-virtual {v0, v4}, Lmiui/util/DensyIndexFile$Builder$DataItemHolder;->put(Ljava/lang/Object;)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p1, v7

    goto/16 :goto_3a

    .line 805
    :cond_ff
    aget-object v0, p1, v7

    instance-of v0, v0, [J

    if-eqz v0, :cond_11b

    .line 806
    const/16 v1, 0x8

    .line 807
    iget-object v0, p0, Lmiui/util/DensyIndexFile$Builder;->mCurrentIndexData:Lmiui/util/DensyIndexFile$Builder$IndexData;

    iget-object v0, v0, Lmiui/util/DensyIndexFile$Builder$IndexData;->mDataItemHolders:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/util/DensyIndexFile$Builder$DataItemHolder;

    aget-object v4, p1, v7

    invoke-virtual {v0, v4}, Lmiui/util/DensyIndexFile$Builder$DataItemHolder;->put(Ljava/lang/Object;)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p1, v7

    goto/16 :goto_3a

    .line 809
    :cond_11b
    new-instance v0, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unsupported type of object["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] inputted"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 815
    .end local v1           #type:B
    .end local v2           #indexSize:B
    :cond_13a
    iget-object v0, p0, Lmiui/util/DensyIndexFile$Builder;->mCurrentIndexData:Lmiui/util/DensyIndexFile$Builder$IndexData;

    new-instance v3, Lmiui/util/DensyIndexFile$Builder$Item;

    const/4 v4, -0x1

    invoke-direct {v3, p0, v4, p1}, Lmiui/util/DensyIndexFile$Builder$Item;-><init>(Lmiui/util/DensyIndexFile$Builder;I[Ljava/lang/Object;)V

    iput-object v3, v0, Lmiui/util/DensyIndexFile$Builder$IndexData;->mDefaultValue:Lmiui/util/DensyIndexFile$Builder$Item;

    .line 816
    return-void
.end method

.method public newGroup()V
    .registers 3

    .prologue
    .line 819
    iget-object v0, p0, Lmiui/util/DensyIndexFile$Builder;->mCurrentIndexData:Lmiui/util/DensyIndexFile$Builder$IndexData;

    iget-object v0, v0, Lmiui/util/DensyIndexFile$Builder$IndexData;->mIndexDataGroups:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_24

    iget-object v0, p0, Lmiui/util/DensyIndexFile$Builder;->mCurrentIndexData:Lmiui/util/DensyIndexFile$Builder$IndexData;

    iget-object v0, v0, Lmiui/util/DensyIndexFile$Builder$IndexData;->mIndexDataGroups:Ljava/util/ArrayList;

    iget-object v1, p0, Lmiui/util/DensyIndexFile$Builder;->mCurrentIndexData:Lmiui/util/DensyIndexFile$Builder$IndexData;

    iget-object v1, v1, Lmiui/util/DensyIndexFile$Builder$IndexData;->mIndexDataGroups:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_30

    .line 821
    :cond_24
    iget-object v0, p0, Lmiui/util/DensyIndexFile$Builder;->mCurrentIndexData:Lmiui/util/DensyIndexFile$Builder$IndexData;

    iget-object v0, v0, Lmiui/util/DensyIndexFile$Builder$IndexData;->mIndexDataGroups:Ljava/util/ArrayList;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 823
    :cond_30
    return-void
.end method

.method public write(Ljava/lang/String;)V
    .registers 7
    .parameter "file"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 913
    invoke-direct {p0}, Lmiui/util/DensyIndexFile$Builder;->build()V

    .line 915
    const/4 v1, 0x0

    .line 916
    .local v1, dos:Ljava/io/DataOutputStream;
    const/4 v0, 0x0

    .line 918
    .local v0, done:Z
    :try_start_5
    new-instance v2, Ljava/io/DataOutputStream;

    new-instance v3, Ljava/io/BufferedOutputStream;

    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v2, v3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_14
    .catchall {:try_start_5 .. :try_end_14} :catchall_26

    .line 919
    .end local v1           #dos:Ljava/io/DataOutputStream;
    .local v2, dos:Ljava/io/DataOutputStream;
    :try_start_14
    invoke-direct {p0, v2}, Lmiui/util/DensyIndexFile$Builder;->writeAll(Ljava/io/DataOutput;)I
    :try_end_17
    .catchall {:try_start_14 .. :try_end_17} :catchall_35

    .line 920
    const/4 v0, 0x1

    .line 922
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V

    .line 923
    if-nez v0, :cond_25

    .line 924
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 927
    :cond_25
    return-void

    .line 922
    .end local v2           #dos:Ljava/io/DataOutputStream;
    .restart local v1       #dos:Ljava/io/DataOutputStream;
    :catchall_26
    move-exception v3

    :goto_27
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V

    .line 923
    if-nez v0, :cond_34

    .line 924
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    :cond_34
    throw v3

    .line 922
    .end local v1           #dos:Ljava/io/DataOutputStream;
    .restart local v2       #dos:Ljava/io/DataOutputStream;
    :catchall_35
    move-exception v3

    move-object v1, v2

    .end local v2           #dos:Ljava/io/DataOutputStream;
    .restart local v1       #dos:Ljava/io/DataOutputStream;
    goto :goto_27
.end method
