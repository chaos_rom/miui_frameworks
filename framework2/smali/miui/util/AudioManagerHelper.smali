.class public Lmiui/util/AudioManagerHelper;
.super Ljava/lang/Object;
.source "AudioManagerHelper.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 12
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isSilentEnabled(Landroid/content/Context;)Z
    .registers 4
    .parameter "context"

    .prologue
    .line 120
    const-string v1, "audio"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 121
    .local v0, am:Landroid/media/AudioManager;
    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_11

    const/4 v1, 0x1

    :goto_10
    return v1

    :cond_11
    const/4 v1, 0x0

    goto :goto_10
.end method

.method public static isVibrateEnabled(Landroid/content/Context;)Z
    .registers 6
    .parameter "context"

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 37
    const-string v4, "audio"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 38
    .local v0, am:Landroid/media/AudioManager;
    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v1

    .line 39
    .local v1, mode:I
    const/4 v4, 0x2

    if-eq v4, v1, :cond_16

    .line 40
    if-ne v2, v1, :cond_14

    .line 42
    :goto_13
    return v2

    :cond_14
    move v2, v3

    .line 40
    goto :goto_13

    .line 42
    :cond_16
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v4, "vibrate_in_normal"

    invoke-static {v2, v4, v3}, Lmiui/provider/ExtraSettings$System;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v2

    goto :goto_13
.end method

.method public static saveLastAudibleRingVolume(Landroid/content/Context;I)V
    .registers 4
    .parameter "context"
    .parameter "volume"

    .prologue
    .line 125
    if-lez p1, :cond_b

    .line 126
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "last_audible_ring_volume"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 130
    :cond_b
    return-void
.end method

.method public static setVibrateSetting(Landroid/content/Context;ZZ)V
    .registers 5
    .parameter "context"
    .parameter "enable"
    .parameter "forSilient"

    .prologue
    .line 24
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    if-eqz p2, :cond_f

    const-string v0, "vibrate_in_silent"

    :goto_8
    invoke-static {v1, v0, p1}, Lmiui/provider/ExtraSettings$System;->putBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    .line 30
    invoke-static {p0}, Lmiui/util/AudioManagerHelper;->validateRingerMode(Landroid/content/Context;)V

    .line 31
    return-void

    .line 24
    :cond_f
    const-string v0, "vibrate_in_normal"

    goto :goto_8
.end method

.method public static toggleSilent(Landroid/content/Context;I)V
    .registers 10
    .parameter "context"
    .parameter "flag"

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x2

    .line 93
    const-string v6, "audio"

    invoke-virtual {p0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 94
    .local v0, am:Landroid/media/AudioManager;
    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v1

    .line 95
    .local v1, mode:I
    if-ne v5, v1, :cond_2e

    move v2, v4

    .line 98
    .local v2, newMode:I
    :goto_11
    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->setRingerMode(I)V

    .line 100
    if-ne v5, v2, :cond_25

    .line 101
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "last_audible_ring_volume"

    invoke-static {v6, v7, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    .line 104
    .local v3, volume:I
    if-lez v3, :cond_25

    .line 105
    invoke-virtual {v0, v5, v3, v4}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 109
    .end local v3           #volume:I
    :cond_25
    invoke-static {p0}, Lmiui/util/AudioManagerHelper;->validateRingerMode(Landroid/content/Context;)V

    .line 111
    if-eqz p1, :cond_2d

    .line 112
    invoke-virtual {v0, v5, v4, p1}, Landroid/media/AudioManager;->adjustStreamVolume(III)V

    .line 114
    :cond_2d
    return-void

    .end local v2           #newMode:I
    :cond_2e
    move v2, v5

    .line 95
    goto :goto_11
.end method

.method public static toggleVibrateSetting(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    .line 17
    invoke-static {p0}, Lmiui/util/AudioManagerHelper;->isVibrateEnabled(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_f

    const/4 v0, 0x1

    :goto_7
    invoke-static {p0}, Lmiui/util/AudioManagerHelper;->isSilentEnabled(Landroid/content/Context;)Z

    move-result v1

    invoke-static {p0, v0, v1}, Lmiui/util/AudioManagerHelper;->setVibrateSetting(Landroid/content/Context;ZZ)V

    .line 18
    return-void

    .line 17
    :cond_f
    const/4 v0, 0x0

    goto :goto_7
.end method

.method private static validateRingerMode(Landroid/content/Context;)V
    .registers 8
    .parameter "context"

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 65
    const-string v4, "audio"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 66
    .local v0, am:Landroid/media/AudioManager;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 67
    .local v2, resolver:Landroid/content/ContentResolver;
    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v1

    .line 68
    .local v1, mode:I
    const/4 v3, 0x0

    .line 69
    .local v3, vibrate:Z
    const/4 v4, 0x2

    if-ne v4, v1, :cond_1d

    .line 70
    const-string v4, "vibrate_in_normal"

    invoke-static {v2, v4, v5}, Lmiui/provider/ExtraSettings$System;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v3

    .line 87
    :cond_1c
    :goto_1c
    return-void

    .line 75
    :cond_1d
    const-string v4, "vibrate_in_silent"

    invoke-static {v2, v4, v6}, Lmiui/provider/ExtraSettings$System;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v3

    .line 76
    if-nez v1, :cond_2b

    .line 77
    if-eqz v3, :cond_1c

    .line 78
    invoke-virtual {v0, v6}, Landroid/media/AudioManager;->setRingerMode(I)V

    goto :goto_1c

    .line 82
    :cond_2b
    if-nez v3, :cond_1c

    .line 83
    invoke-virtual {v0, v5}, Landroid/media/AudioManager;->setRingerMode(I)V

    goto :goto_1c
.end method

.method public static validateVibrateSettings(Landroid/content/Context;)V
    .registers 5
    .parameter "context"

    .prologue
    .line 52
    const-string v1, "audio"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 53
    .local v0, am:Landroid/media/AudioManager;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v1, 0x2

    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v3

    if-ne v1, v3, :cond_1d

    const-string v1, "vibrate_in_normal"

    :goto_15
    invoke-static {p0}, Lmiui/util/AudioManagerHelper;->isVibrateEnabled(Landroid/content/Context;)Z

    move-result v3

    invoke-static {v2, v1, v3}, Lmiui/provider/ExtraSettings$System;->putBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    .line 59
    return-void

    .line 53
    :cond_1d
    const-string v1, "vibrate_in_silent"

    goto :goto_15
.end method
