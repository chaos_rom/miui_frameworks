.class public abstract Lmiui/util/DensyIndexFile;
.super Ljava/lang/Object;
.source "DensyIndexFile.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/util/DensyIndexFile$1;,
        Lmiui/util/DensyIndexFile$Builder;,
        Lmiui/util/DensyIndexFile$Reader;,
        Lmiui/util/DensyIndexFile$FileHeader;,
        Lmiui/util/DensyIndexFile$DescriptionPair;,
        Lmiui/util/DensyIndexFile$DataItemDescription;,
        Lmiui/util/DensyIndexFile$IndexGroupDescription;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = true

.field private static final LOG_TAG:Ljava/lang/String; = "DensyIndexFile: "


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 17
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 687
    return-void
.end method

.method static getSizeOf(I)B
    .registers 5
    .parameter "length"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v3, 0x8

    .line 1075
    const/4 v1, 0x0

    .line 1076
    .local v1, sizeOf:B
    move v0, p0

    .local v0, size:I
    :goto_4
    if-lez v0, :cond_c

    .line 1077
    add-int/lit8 v2, v1, 0x1

    int-to-byte v1, v2

    .line 1076
    shr-int/lit8 v0, v0, 0x8

    goto :goto_4

    .line 1080
    :cond_c
    const/4 v2, 0x3

    if-ne v1, v2, :cond_11

    .line 1081
    const/4 v1, 0x4

    .line 1088
    :cond_10
    :goto_10
    return v1

    .line 1082
    :cond_11
    const/4 v2, 0x4

    if-le v1, v2, :cond_19

    if-ge v1, v3, :cond_19

    .line 1083
    const/16 v1, 0x8

    goto :goto_10

    .line 1084
    :cond_19
    if-le v1, v3, :cond_10

    .line 1085
    new-instance v2, Ljava/io/IOException;

    const-string v3, "Too many data"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method static loge(Ljava/lang/String;)V
    .registers 4
    .parameter "msg"

    .prologue
    .line 1092
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DensyIndexFile: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1093
    return-void
.end method

.method static logo(Ljava/lang/String;)V
    .registers 4
    .parameter "msg"

    .prologue
    .line 1096
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DensyIndexFile: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1097
    return-void
.end method

.method static readAccordingToSize(Ljava/io/DataInput;I)J
    .registers 7
    .parameter "i"
    .parameter "size"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1055
    packed-switch p1, :pswitch_data_34

    .line 1069
    :pswitch_3
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unsupport size "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1057
    :pswitch_1c
    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v2

    int-to-long v0, v2

    .line 1071
    .local v0, data:J
    :goto_21
    return-wide v0

    .line 1060
    .end local v0           #data:J
    :pswitch_22
    invoke-interface {p0}, Ljava/io/DataInput;->readShort()S

    move-result v2

    int-to-long v0, v2

    .line 1061
    .restart local v0       #data:J
    goto :goto_21

    .line 1063
    .end local v0           #data:J
    :pswitch_28
    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v2

    int-to-long v0, v2

    .line 1064
    .restart local v0       #data:J
    goto :goto_21

    .line 1066
    .end local v0           #data:J
    :pswitch_2e
    invoke-interface {p0}, Ljava/io/DataInput;->readLong()J

    move-result-wide v0

    .line 1067
    .restart local v0       #data:J
    goto :goto_21

    .line 1055
    nop

    :pswitch_data_34
    .packed-switch 0x1
        :pswitch_1c
        :pswitch_22
        :pswitch_3
        :pswitch_28
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_2e
    .end packed-switch
.end method

.method static writeAccordingToSize(Ljava/io/DataOutput;IJ)V
    .registers 7
    .parameter "o"
    .parameter "size"
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1035
    packed-switch p1, :pswitch_data_30

    .line 1049
    :pswitch_3
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupport size "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1037
    :pswitch_1c
    long-to-int v0, p2

    invoke-interface {p0, v0}, Ljava/io/DataOutput;->writeByte(I)V

    .line 1051
    :goto_20
    return-void

    .line 1040
    :pswitch_21
    long-to-int v0, p2

    invoke-interface {p0, v0}, Ljava/io/DataOutput;->writeShort(I)V

    goto :goto_20

    .line 1043
    :pswitch_26
    long-to-int v0, p2

    invoke-interface {p0, v0}, Ljava/io/DataOutput;->writeInt(I)V

    goto :goto_20

    .line 1046
    :pswitch_2b
    invoke-interface {p0, p2, p3}, Ljava/io/DataOutput;->writeLong(J)V

    goto :goto_20

    .line 1035
    nop

    :pswitch_data_30
    .packed-switch 0x1
        :pswitch_1c
        :pswitch_21
        :pswitch_3
        :pswitch_26
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_2b
    .end packed-switch
.end method
