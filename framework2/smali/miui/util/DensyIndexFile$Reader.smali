.class public Lmiui/util/DensyIndexFile$Reader;
.super Ljava/lang/Object;
.source "DensyIndexFile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/util/DensyIndexFile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Reader"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/util/DensyIndexFile$Reader$IndexData;
    }
.end annotation


# instance fields
.field private mFile:Ljava/io/RandomAccessFile;

.field private mHeader:Lmiui/util/DensyIndexFile$FileHeader;

.field private mIndexData:[Lmiui/util/DensyIndexFile$Reader$IndexData;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 12
    .parameter "file"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 468
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 469
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    .line 471
    .local v5, time:J
    :try_start_7
    new-instance v7, Ljava/io/RandomAccessFile;

    const-string v8, "r"

    invoke-direct {v7, p1, v8}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v7, p0, Lmiui/util/DensyIndexFile$Reader;->mFile:Ljava/io/RandomAccessFile;

    .line 472
    iget-object v7, p0, Lmiui/util/DensyIndexFile$Reader;->mFile:Ljava/io/RandomAccessFile;

    const-wide/16 v8, 0x0

    invoke-virtual {v7, v8, v9}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 473
    iget-object v7, p0, Lmiui/util/DensyIndexFile$Reader;->mFile:Ljava/io/RandomAccessFile;

    invoke-static {v7}, Lmiui/util/DensyIndexFile$FileHeader;->read(Ljava/io/DataInput;)Lmiui/util/DensyIndexFile$FileHeader;

    move-result-object v7

    iput-object v7, p0, Lmiui/util/DensyIndexFile$Reader;->mHeader:Lmiui/util/DensyIndexFile$FileHeader;

    .line 475
    iget-object v7, p0, Lmiui/util/DensyIndexFile$Reader;->mHeader:Lmiui/util/DensyIndexFile$FileHeader;

    iget-object v7, v7, Lmiui/util/DensyIndexFile$FileHeader;->mDescriptionOffsets:[Lmiui/util/DensyIndexFile$DescriptionPair;

    array-length v7, v7

    new-array v7, v7, [Lmiui/util/DensyIndexFile$Reader$IndexData;

    iput-object v7, p0, Lmiui/util/DensyIndexFile$Reader;->mIndexData:[Lmiui/util/DensyIndexFile$Reader$IndexData;

    .line 476
    const/4 v4, 0x0

    .local v4, k:I
    :goto_29
    iget-object v7, p0, Lmiui/util/DensyIndexFile$Reader;->mHeader:Lmiui/util/DensyIndexFile$FileHeader;

    iget-object v7, v7, Lmiui/util/DensyIndexFile$FileHeader;->mDescriptionOffsets:[Lmiui/util/DensyIndexFile$DescriptionPair;

    array-length v7, v7

    if-ge v4, v7, :cond_ee

    .line 477
    iget-object v7, p0, Lmiui/util/DensyIndexFile$Reader;->mIndexData:[Lmiui/util/DensyIndexFile$Reader$IndexData;

    new-instance v8, Lmiui/util/DensyIndexFile$Reader$IndexData;

    const/4 v9, 0x0

    invoke-direct {v8, v9}, Lmiui/util/DensyIndexFile$Reader$IndexData;-><init>(Lmiui/util/DensyIndexFile$1;)V

    aput-object v8, v7, v4

    .line 478
    iget-object v7, p0, Lmiui/util/DensyIndexFile$Reader;->mFile:Ljava/io/RandomAccessFile;

    iget-object v8, p0, Lmiui/util/DensyIndexFile$Reader;->mHeader:Lmiui/util/DensyIndexFile$FileHeader;

    iget-object v8, v8, Lmiui/util/DensyIndexFile$FileHeader;->mDescriptionOffsets:[Lmiui/util/DensyIndexFile$DescriptionPair;

    aget-object v8, v8, v4

    iget-wide v8, v8, Lmiui/util/DensyIndexFile$DescriptionPair;->mIndexGroupDescriptionOffset:J

    invoke-virtual {v7, v8, v9}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 479
    iget-object v7, p0, Lmiui/util/DensyIndexFile$Reader;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v7}, Ljava/io/RandomAccessFile;->readInt()I

    move-result v0

    .line 480
    .local v0, IndexGroupDescriptionCount:I
    iget-object v7, p0, Lmiui/util/DensyIndexFile$Reader;->mIndexData:[Lmiui/util/DensyIndexFile$Reader$IndexData;

    aget-object v7, v7, v4

    new-array v8, v0, [Lmiui/util/DensyIndexFile$IndexGroupDescription;

    iput-object v8, v7, Lmiui/util/DensyIndexFile$Reader$IndexData;->mIndexGroupDescriptions:[Lmiui/util/DensyIndexFile$IndexGroupDescription;

    .line 481
    const/4 v3, 0x0

    .local v3, i:I
    :goto_56
    if-ge v3, v0, :cond_69

    .line 482
    iget-object v7, p0, Lmiui/util/DensyIndexFile$Reader;->mIndexData:[Lmiui/util/DensyIndexFile$Reader$IndexData;

    aget-object v7, v7, v4

    iget-object v7, v7, Lmiui/util/DensyIndexFile$Reader$IndexData;->mIndexGroupDescriptions:[Lmiui/util/DensyIndexFile$IndexGroupDescription;

    iget-object v8, p0, Lmiui/util/DensyIndexFile$Reader;->mFile:Ljava/io/RandomAccessFile;

    invoke-static {v8}, Lmiui/util/DensyIndexFile$IndexGroupDescription;->read(Ljava/io/DataInput;)Lmiui/util/DensyIndexFile$IndexGroupDescription;

    move-result-object v8

    aput-object v8, v7, v3

    .line 481
    add-int/lit8 v3, v3, 0x1

    goto :goto_56

    .line 485
    :cond_69
    iget-object v7, p0, Lmiui/util/DensyIndexFile$Reader;->mFile:Ljava/io/RandomAccessFile;

    iget-object v8, p0, Lmiui/util/DensyIndexFile$Reader;->mHeader:Lmiui/util/DensyIndexFile$FileHeader;

    iget-object v8, v8, Lmiui/util/DensyIndexFile$FileHeader;->mDescriptionOffsets:[Lmiui/util/DensyIndexFile$DescriptionPair;

    aget-object v8, v8, v4

    iget-wide v8, v8, Lmiui/util/DensyIndexFile$DescriptionPair;->mDataItemDescriptionOffset:J

    invoke-virtual {v7, v8, v9}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 486
    iget-object v7, p0, Lmiui/util/DensyIndexFile$Reader;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v7}, Ljava/io/RandomAccessFile;->readInt()I

    move-result v1

    .line 487
    .local v1, dataItemDescriptionCount:I
    iget-object v7, p0, Lmiui/util/DensyIndexFile$Reader;->mIndexData:[Lmiui/util/DensyIndexFile$Reader$IndexData;

    aget-object v7, v7, v4

    const/4 v8, 0x0

    iput v8, v7, Lmiui/util/DensyIndexFile$Reader$IndexData;->mSizeOfItems:I

    .line 488
    iget-object v7, p0, Lmiui/util/DensyIndexFile$Reader;->mIndexData:[Lmiui/util/DensyIndexFile$Reader$IndexData;

    aget-object v7, v7, v4

    new-array v8, v1, [Lmiui/util/DensyIndexFile$DataItemDescription;

    iput-object v8, v7, Lmiui/util/DensyIndexFile$Reader$IndexData;->mDataItemDescriptions:[Lmiui/util/DensyIndexFile$DataItemDescription;

    .line 489
    const/4 v3, 0x0

    :goto_8c
    if-ge v3, v1, :cond_b2

    .line 490
    iget-object v7, p0, Lmiui/util/DensyIndexFile$Reader;->mIndexData:[Lmiui/util/DensyIndexFile$Reader$IndexData;

    aget-object v7, v7, v4

    iget-object v7, v7, Lmiui/util/DensyIndexFile$Reader$IndexData;->mDataItemDescriptions:[Lmiui/util/DensyIndexFile$DataItemDescription;

    iget-object v8, p0, Lmiui/util/DensyIndexFile$Reader;->mFile:Ljava/io/RandomAccessFile;

    invoke-static {v8}, Lmiui/util/DensyIndexFile$DataItemDescription;->read(Ljava/io/DataInput;)Lmiui/util/DensyIndexFile$DataItemDescription;

    move-result-object v8

    aput-object v8, v7, v3

    .line 491
    iget-object v7, p0, Lmiui/util/DensyIndexFile$Reader;->mIndexData:[Lmiui/util/DensyIndexFile$Reader$IndexData;

    aget-object v7, v7, v4

    iget v8, v7, Lmiui/util/DensyIndexFile$Reader$IndexData;->mSizeOfItems:I

    iget-object v9, p0, Lmiui/util/DensyIndexFile$Reader;->mIndexData:[Lmiui/util/DensyIndexFile$Reader$IndexData;

    aget-object v9, v9, v4

    iget-object v9, v9, Lmiui/util/DensyIndexFile$Reader$IndexData;->mDataItemDescriptions:[Lmiui/util/DensyIndexFile$DataItemDescription;

    aget-object v9, v9, v3

    iget-byte v9, v9, Lmiui/util/DensyIndexFile$DataItemDescription;->mIndexSize:B

    add-int/2addr v8, v9

    iput v8, v7, Lmiui/util/DensyIndexFile$Reader$IndexData;->mSizeOfItems:I

    .line 489
    add-int/lit8 v3, v3, 0x1

    goto :goto_8c

    .line 494
    :cond_b2
    iget-object v7, p0, Lmiui/util/DensyIndexFile$Reader;->mIndexData:[Lmiui/util/DensyIndexFile$Reader$IndexData;

    aget-object v7, v7, v4

    new-array v8, v1, [[Ljava/lang/Object;

    iput-object v8, v7, Lmiui/util/DensyIndexFile$Reader$IndexData;->mDataItems:[[Ljava/lang/Object;

    .line 495
    const/4 v3, 0x0

    :goto_bb
    if-ge v3, v1, :cond_e5

    .line 496
    iget-object v7, p0, Lmiui/util/DensyIndexFile$Reader;->mFile:Ljava/io/RandomAccessFile;

    iget-object v8, p0, Lmiui/util/DensyIndexFile$Reader;->mIndexData:[Lmiui/util/DensyIndexFile$Reader$IndexData;

    aget-object v8, v8, v4

    iget-object v8, v8, Lmiui/util/DensyIndexFile$Reader$IndexData;->mDataItemDescriptions:[Lmiui/util/DensyIndexFile$DataItemDescription;

    aget-object v8, v8, v3

    iget-wide v8, v8, Lmiui/util/DensyIndexFile$DataItemDescription;->mOffset:J

    invoke-virtual {v7, v8, v9}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 497
    iget-object v7, p0, Lmiui/util/DensyIndexFile$Reader;->mIndexData:[Lmiui/util/DensyIndexFile$Reader$IndexData;

    aget-object v7, v7, v4

    iget-object v7, v7, Lmiui/util/DensyIndexFile$Reader$IndexData;->mDataItems:[[Ljava/lang/Object;

    iget-object v8, p0, Lmiui/util/DensyIndexFile$Reader;->mIndexData:[Lmiui/util/DensyIndexFile$Reader$IndexData;

    aget-object v8, v8, v4

    iget-object v8, v8, Lmiui/util/DensyIndexFile$Reader$IndexData;->mDataItemDescriptions:[Lmiui/util/DensyIndexFile$DataItemDescription;

    aget-object v8, v8, v3

    iget-object v9, p0, Lmiui/util/DensyIndexFile$Reader;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v8, v9}, Lmiui/util/DensyIndexFile$DataItemDescription;->readDataItems(Ljava/io/RandomAccessFile;)[Ljava/lang/Object;

    move-result-object v8

    aput-object v8, v7, v3
    :try_end_e2
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_e2} :catch_e9

    .line 495
    add-int/lit8 v3, v3, 0x1

    goto :goto_bb

    .line 476
    :cond_e5
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_29

    .line 500
    .end local v0           #IndexGroupDescriptionCount:I
    .end local v1           #dataItemDescriptionCount:I
    .end local v3           #i:I
    .end local v4           #k:I
    :catch_e9
    move-exception v2

    .line 501
    .local v2, e:Ljava/io/IOException;
    invoke-virtual {p0}, Lmiui/util/DensyIndexFile$Reader;->close()V

    .line 502
    throw v2

    .line 505
    .end local v2           #e:Ljava/io/IOException;
    .restart local v4       #k:I
    :cond_ee
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    sub-long v5, v7, v5

    .line 506
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "ms used to load file "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lmiui/util/DensyIndexFile;->logo(Ljava/lang/String;)V

    .line 508
    return-void
.end method

.method private offset(II)J
    .registers 13
    .parameter "kind"
    .parameter "index"

    .prologue
    .line 665
    const/4 v1, 0x0

    .line 666
    .local v1, id:Lmiui/util/DensyIndexFile$IndexGroupDescription;
    const/4 v3, 0x0

    .local v3, min:I
    iget-object v6, p0, Lmiui/util/DensyIndexFile$Reader;->mIndexData:[Lmiui/util/DensyIndexFile$Reader$IndexData;

    aget-object v6, v6, p1

    iget-object v6, v6, Lmiui/util/DensyIndexFile$Reader$IndexData;->mIndexGroupDescriptions:[Lmiui/util/DensyIndexFile$IndexGroupDescription;

    array-length v2, v6

    .line 667
    .local v2, max:I
    :goto_9
    if-ge v3, v2, :cond_34

    .line 668
    add-int v6, v2, v3

    div-int/lit8 v0, v6, 0x2

    .line 669
    .local v0, found:I
    iget-object v6, p0, Lmiui/util/DensyIndexFile$Reader;->mIndexData:[Lmiui/util/DensyIndexFile$Reader$IndexData;

    aget-object v6, v6, p1

    iget-object v6, v6, Lmiui/util/DensyIndexFile$Reader$IndexData;->mIndexGroupDescriptions:[Lmiui/util/DensyIndexFile$IndexGroupDescription;

    aget-object v6, v6, v0

    iget v6, v6, Lmiui/util/DensyIndexFile$IndexGroupDescription;->mMinIndex:I

    if-le v6, p2, :cond_1d

    .line 670
    move v2, v0

    goto :goto_9

    .line 671
    :cond_1d
    iget-object v6, p0, Lmiui/util/DensyIndexFile$Reader;->mIndexData:[Lmiui/util/DensyIndexFile$Reader$IndexData;

    aget-object v6, v6, p1

    iget-object v6, v6, Lmiui/util/DensyIndexFile$Reader$IndexData;->mIndexGroupDescriptions:[Lmiui/util/DensyIndexFile$IndexGroupDescription;

    aget-object v6, v6, v0

    iget v6, v6, Lmiui/util/DensyIndexFile$IndexGroupDescription;->mMaxIndex:I

    if-gt v6, p2, :cond_2c

    .line 672
    add-int/lit8 v3, v0, 0x1

    goto :goto_9

    .line 674
    :cond_2c
    iget-object v6, p0, Lmiui/util/DensyIndexFile$Reader;->mIndexData:[Lmiui/util/DensyIndexFile$Reader$IndexData;

    aget-object v6, v6, p1

    iget-object v6, v6, Lmiui/util/DensyIndexFile$Reader$IndexData;->mIndexGroupDescriptions:[Lmiui/util/DensyIndexFile$IndexGroupDescription;

    aget-object v1, v6, v0

    .line 679
    .end local v0           #found:I
    :cond_34
    const-wide/16 v4, -0x1

    .line 680
    .local v4, offset:J
    if-eqz v1, :cond_48

    .line 681
    iget-wide v6, v1, Lmiui/util/DensyIndexFile$IndexGroupDescription;->mOffset:J

    iget v8, v1, Lmiui/util/DensyIndexFile$IndexGroupDescription;->mMinIndex:I

    sub-int v8, p2, v8

    iget-object v9, p0, Lmiui/util/DensyIndexFile$Reader;->mIndexData:[Lmiui/util/DensyIndexFile$Reader$IndexData;

    aget-object v9, v9, p1

    iget v9, v9, Lmiui/util/DensyIndexFile$Reader$IndexData;->mSizeOfItems:I

    mul-int/2addr v8, v9

    int-to-long v8, v8

    add-long v4, v6, v8

    .line 683
    :cond_48
    return-wide v4
.end method

.method private readSingleDataItem(III)Ljava/lang/Object;
    .registers 9
    .parameter "kind"
    .parameter "dataIndex"
    .parameter "dataItemIndex"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 657
    iget-object v0, p0, Lmiui/util/DensyIndexFile$Reader;->mIndexData:[Lmiui/util/DensyIndexFile$Reader$IndexData;

    aget-object v0, v0, p1

    iget-object v0, v0, Lmiui/util/DensyIndexFile$Reader$IndexData;->mDataItems:[[Ljava/lang/Object;

    aget-object v0, v0, p2

    aget-object v0, v0, p3

    if-nez v0, :cond_36

    .line 658
    iget-object v0, p0, Lmiui/util/DensyIndexFile$Reader;->mFile:Ljava/io/RandomAccessFile;

    iget-object v1, p0, Lmiui/util/DensyIndexFile$Reader;->mIndexData:[Lmiui/util/DensyIndexFile$Reader$IndexData;

    aget-object v1, v1, p1

    iget-object v1, v1, Lmiui/util/DensyIndexFile$Reader$IndexData;->mDataItemDescriptions:[Lmiui/util/DensyIndexFile$DataItemDescription;

    aget-object v1, v1, p2

    iget-wide v1, v1, Lmiui/util/DensyIndexFile$DataItemDescription;->mOffset:J

    const-wide/16 v3, 0x4

    add-long/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 659
    iget-object v0, p0, Lmiui/util/DensyIndexFile$Reader;->mIndexData:[Lmiui/util/DensyIndexFile$Reader$IndexData;

    aget-object v0, v0, p1

    iget-object v0, v0, Lmiui/util/DensyIndexFile$Reader$IndexData;->mDataItems:[[Ljava/lang/Object;

    aget-object v0, v0, p2

    iget-object v1, p0, Lmiui/util/DensyIndexFile$Reader;->mIndexData:[Lmiui/util/DensyIndexFile$Reader$IndexData;

    aget-object v1, v1, p1

    iget-object v1, v1, Lmiui/util/DensyIndexFile$Reader$IndexData;->mDataItemDescriptions:[Lmiui/util/DensyIndexFile$DataItemDescription;

    aget-object v1, v1, p2

    iget-object v2, p0, Lmiui/util/DensyIndexFile$Reader;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v1, v2, p3}, Lmiui/util/DensyIndexFile$DataItemDescription;->readSingleDataItem(Ljava/io/RandomAccessFile;I)Ljava/lang/Object;

    move-result-object v1

    aput-object v1, v0, p3

    .line 661
    :cond_36
    iget-object v0, p0, Lmiui/util/DensyIndexFile$Reader;->mIndexData:[Lmiui/util/DensyIndexFile$Reader$IndexData;

    aget-object v0, v0, p1

    iget-object v0, v0, Lmiui/util/DensyIndexFile$Reader$IndexData;->mDataItems:[[Ljava/lang/Object;

    aget-object v0, v0, p2

    aget-object v0, v0, p3

    return-object v0
.end method


# virtual methods
.method public declared-synchronized close()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 647
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lmiui/util/DensyIndexFile$Reader;->mFile:Ljava/io/RandomAccessFile;

    if-eqz v0, :cond_a

    .line 648
    iget-object v0, p0, Lmiui/util/DensyIndexFile$Reader;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V

    .line 651
    :cond_a
    const/4 v0, 0x0

    iput-object v0, p0, Lmiui/util/DensyIndexFile$Reader;->mFile:Ljava/io/RandomAccessFile;

    .line 652
    const/4 v0, 0x0

    iput-object v0, p0, Lmiui/util/DensyIndexFile$Reader;->mHeader:Lmiui/util/DensyIndexFile$FileHeader;

    .line 653
    const/4 v0, 0x0

    iput-object v0, p0, Lmiui/util/DensyIndexFile$Reader;->mIndexData:[Lmiui/util/DensyIndexFile$Reader$IndexData;
    :try_end_13
    .catchall {:try_start_1 .. :try_end_13} :catchall_15

    .line 654
    monitor-exit p0

    return-void

    .line 647
    :catchall_15
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized get(III)Ljava/lang/Object;
    .registers 15
    .parameter "kind"
    .parameter "index"
    .parameter "dataIndex"

    .prologue
    const/4 v8, 0x0

    .line 511
    monitor-enter p0

    :try_start_2
    iget-object v9, p0, Lmiui/util/DensyIndexFile$Reader;->mFile:Ljava/io/RandomAccessFile;

    if-nez v9, :cond_d

    .line 512
    const-string v9, "Get data from a corrupt file"

    invoke-static {v9}, Lmiui/util/DensyIndexFile;->loge(Ljava/lang/String;)V
    :try_end_b
    .catchall {:try_start_2 .. :try_end_b} :catchall_3e

    .line 576
    :goto_b
    monitor-exit p0

    return-object v8

    .line 515
    :cond_d
    if-ltz p1, :cond_14

    :try_start_f
    iget-object v9, p0, Lmiui/util/DensyIndexFile$Reader;->mIndexData:[Lmiui/util/DensyIndexFile$Reader$IndexData;

    array-length v9, v9

    if-lt p1, v9, :cond_41

    .line 516
    :cond_14
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Kind "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " out of range[0, "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lmiui/util/DensyIndexFile$Reader;->mIndexData:[Lmiui/util/DensyIndexFile$Reader$IndexData;

    array-length v10, v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ")"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lmiui/util/DensyIndexFile;->loge(Ljava/lang/String;)V
    :try_end_3d
    .catchall {:try_start_f .. :try_end_3d} :catchall_3e

    goto :goto_b

    .line 511
    :catchall_3e
    move-exception v8

    monitor-exit p0

    throw v8

    .line 519
    :cond_41
    if-ltz p3, :cond_4c

    :try_start_43
    iget-object v9, p0, Lmiui/util/DensyIndexFile$Reader;->mIndexData:[Lmiui/util/DensyIndexFile$Reader$IndexData;

    aget-object v9, v9, p1

    iget-object v9, v9, Lmiui/util/DensyIndexFile$Reader$IndexData;->mDataItemDescriptions:[Lmiui/util/DensyIndexFile$DataItemDescription;

    array-length v9, v9

    if-lt p3, v9, :cond_7a

    .line 520
    :cond_4c
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "DataIndex "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " out of range[0, "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lmiui/util/DensyIndexFile$Reader;->mIndexData:[Lmiui/util/DensyIndexFile$Reader$IndexData;

    aget-object v10, v10, p1

    iget-object v10, v10, Lmiui/util/DensyIndexFile$Reader$IndexData;->mDataItemDescriptions:[Lmiui/util/DensyIndexFile$DataItemDescription;

    array-length v10, v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ")"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lmiui/util/DensyIndexFile;->loge(Ljava/lang/String;)V

    goto :goto_b

    .line 524
    :cond_7a
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 526
    .local v6, time:J
    invoke-direct {p0, p1, p2}, Lmiui/util/DensyIndexFile$Reader;->offset(II)J

    move-result-wide v3

    .line 527
    .local v3, offset:J
    const/4 v5, 0x0

    .line 528
    .local v5, ret:Ljava/lang/Object;
    const-wide/16 v8, 0x0

    cmp-long v8, v3, v8

    if-gez v8, :cond_d1

    .line 529
    iget-object v8, p0, Lmiui/util/DensyIndexFile$Reader;->mIndexData:[Lmiui/util/DensyIndexFile$Reader$IndexData;

    aget-object v8, v8, p1

    iget-object v8, v8, Lmiui/util/DensyIndexFile$Reader$IndexData;->mDataItems:[[Ljava/lang/Object;

    aget-object v8, v8, p3

    const/4 v9, 0x0

    aget-object v5, v8, v9

    move-object v8, v5

    .line 573
    .end local v5           #ret:Ljava/lang/Object;
    :goto_95
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    sub-long v6, v9, v6

    .line 574
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "ms used to get data("

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ")"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lmiui/util/DensyIndexFile;->logo(Ljava/lang/String;)V
    :try_end_cf
    .catchall {:try_start_43 .. :try_end_cf} :catchall_3e

    goto/16 :goto_b

    .line 532
    .restart local v5       #ret:Ljava/lang/Object;
    :cond_d1
    :try_start_d1
    iget-object v8, p0, Lmiui/util/DensyIndexFile$Reader;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v8, v3, v4}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 533
    const/4 v2, 0x0

    .end local v5           #ret:Ljava/lang/Object;
    .local v2, i:I
    :goto_d7
    if-gt v2, p3, :cond_161

    .line 534
    iget-object v8, p0, Lmiui/util/DensyIndexFile$Reader;->mIndexData:[Lmiui/util/DensyIndexFile$Reader$IndexData;

    aget-object v8, v8, p1

    iget-object v8, v8, Lmiui/util/DensyIndexFile$Reader$IndexData;->mDataItemDescriptions:[Lmiui/util/DensyIndexFile$DataItemDescription;

    aget-object v8, v8, v2

    iget-byte v8, v8, Lmiui/util/DensyIndexFile$DataItemDescription;->mType:B

    packed-switch v8, :pswitch_data_164

    .line 562
    new-instance v8, Ljava/io/IOException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Unknown type "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lmiui/util/DensyIndexFile$Reader;->mIndexData:[Lmiui/util/DensyIndexFile$Reader$IndexData;

    aget-object v10, v10, p1

    iget-object v10, v10, Lmiui/util/DensyIndexFile$Reader$IndexData;->mDataItemDescriptions:[Lmiui/util/DensyIndexFile$DataItemDescription;

    aget-object v10, v10, v2

    iget-byte v10, v10, Lmiui/util/DensyIndexFile$DataItemDescription;->mType:B

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v8
    :try_end_109
    .catchall {:try_start_d1 .. :try_end_109} :catchall_3e
    .catch Ljava/io/IOException; {:try_start_d1 .. :try_end_109} :catch_109

    .line 566
    .end local v2           #i:I
    :catch_109
    move-exception v1

    .line 567
    .local v1, e:Ljava/io/IOException;
    :try_start_10a
    const-string v8, "Seek data from a corrupt file"

    invoke-static {v8}, Lmiui/util/DensyIndexFile;->loge(Ljava/lang/String;)V
    :try_end_10f
    .catchall {:try_start_10a .. :try_end_10f} :catchall_3e

    .line 568
    const/4 v5, 0x0

    .restart local v5       #ret:Ljava/lang/Object;
    move-object v8, v5

    goto :goto_95

    .line 536
    .end local v1           #e:Ljava/io/IOException;
    .end local v5           #ret:Ljava/lang/Object;
    .restart local v2       #i:I
    :pswitch_112
    :try_start_112
    iget-object v8, p0, Lmiui/util/DensyIndexFile$Reader;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->readByte()B

    move-result v8

    invoke-static {v8}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v5

    .line 533
    :cond_11c
    :goto_11c
    add-int/lit8 v2, v2, 0x1

    goto :goto_d7

    .line 539
    :pswitch_11f
    iget-object v8, p0, Lmiui/util/DensyIndexFile$Reader;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->readShort()S

    move-result v8

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    .line 540
    .local v5, ret:Ljava/lang/Short;
    goto :goto_11c

    .line 542
    .end local v5           #ret:Ljava/lang/Short;
    :pswitch_12a
    iget-object v8, p0, Lmiui/util/DensyIndexFile$Reader;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->readInt()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 543
    .local v5, ret:Ljava/lang/Integer;
    goto :goto_11c

    .line 545
    .end local v5           #ret:Ljava/lang/Integer;
    :pswitch_135
    iget-object v8, p0, Lmiui/util/DensyIndexFile$Reader;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->readLong()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_13e
    .catchall {:try_start_112 .. :try_end_13e} :catchall_3e
    .catch Ljava/io/IOException; {:try_start_112 .. :try_end_13e} :catch_109

    move-result-object v5

    .line 546
    .local v5, ret:Ljava/lang/Long;
    goto :goto_11c

    .line 553
    .end local v5           #ret:Ljava/lang/Long;
    :pswitch_140
    :try_start_140
    iget-object v8, p0, Lmiui/util/DensyIndexFile$Reader;->mFile:Ljava/io/RandomAccessFile;

    iget-object v9, p0, Lmiui/util/DensyIndexFile$Reader;->mIndexData:[Lmiui/util/DensyIndexFile$Reader$IndexData;

    aget-object v9, v9, p1

    iget-object v9, v9, Lmiui/util/DensyIndexFile$Reader$IndexData;->mDataItemDescriptions:[Lmiui/util/DensyIndexFile$DataItemDescription;

    aget-object v9, v9, v2

    iget-byte v9, v9, Lmiui/util/DensyIndexFile$DataItemDescription;->mIndexSize:B

    invoke-static {v8, v9}, Lmiui/util/DensyIndexFile;->readAccordingToSize(Ljava/io/DataInput;I)J

    move-result-wide v8

    long-to-int v0, v8

    .line 554
    .local v0, dataItemIndex:I
    if-ne v2, p3, :cond_11c

    .line 555
    invoke-direct {p0, p1, p3, v0}, Lmiui/util/DensyIndexFile$Reader;->readSingleDataItem(III)Ljava/lang/Object;
    :try_end_156
    .catchall {:try_start_140 .. :try_end_156} :catchall_3e
    .catch Ljava/io/IOException; {:try_start_140 .. :try_end_156} :catch_158

    move-result-object v5

    .local v5, ret:Ljava/lang/Object;
    goto :goto_11c

    .line 557
    .end local v0           #dataItemIndex:I
    .end local v5           #ret:Ljava/lang/Object;
    :catch_158
    move-exception v1

    .line 558
    .restart local v1       #e:Ljava/io/IOException;
    :try_start_159
    new-instance v8, Ljava/io/IOException;

    const-string v9, "File may be corrupt due to invalid data index size"

    invoke-direct {v8, v9, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v8
    :try_end_161
    .catchall {:try_start_159 .. :try_end_161} :catchall_3e
    .catch Ljava/io/IOException; {:try_start_159 .. :try_end_161} :catch_109

    .end local v1           #e:Ljava/io/IOException;
    :cond_161
    move-object v8, v5

    .line 569
    goto/16 :goto_95

    .line 534
    :pswitch_data_164
    .packed-switch 0x0
        :pswitch_112
        :pswitch_11f
        :pswitch_12a
        :pswitch_135
        :pswitch_140
        :pswitch_140
        :pswitch_140
        :pswitch_140
        :pswitch_140
    .end packed-switch
.end method

.method public declared-synchronized get(II)[Ljava/lang/Object;
    .registers 16
    .parameter "kind"
    .parameter "index"

    .prologue
    const/4 v7, 0x0

    .line 580
    monitor-enter p0

    :try_start_2
    iget-object v10, p0, Lmiui/util/DensyIndexFile$Reader;->mFile:Ljava/io/RandomAccessFile;

    if-nez v10, :cond_d

    .line 581
    const-string v10, "Get data from a corrupt file"

    invoke-static {v10}, Lmiui/util/DensyIndexFile;->loge(Ljava/lang/String;)V
    :try_end_b
    .catchall {:try_start_2 .. :try_end_b} :catchall_2b

    .line 643
    :cond_b
    :goto_b
    monitor-exit p0

    return-object v7

    .line 584
    :cond_d
    if-ltz p1, :cond_14

    :try_start_f
    iget-object v10, p0, Lmiui/util/DensyIndexFile$Reader;->mIndexData:[Lmiui/util/DensyIndexFile$Reader$IndexData;

    array-length v10, v10

    if-lt p1, v10, :cond_2e

    .line 585
    :cond_14
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Cannot get data kind "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lmiui/util/DensyIndexFile;->loge(Ljava/lang/String;)V
    :try_end_2a
    .catchall {:try_start_f .. :try_end_2a} :catchall_2b

    goto :goto_b

    .line 580
    :catchall_2b
    move-exception v10

    monitor-exit p0

    throw v10

    .line 589
    :cond_2e
    :try_start_2e
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 591
    .local v8, time:J
    invoke-direct {p0, p1, p2}, Lmiui/util/DensyIndexFile$Reader;->offset(II)J

    move-result-wide v5

    .line 592
    .local v5, offset:J
    iget-object v10, p0, Lmiui/util/DensyIndexFile$Reader;->mIndexData:[Lmiui/util/DensyIndexFile$Reader$IndexData;

    aget-object v10, v10, p1

    iget-object v10, v10, Lmiui/util/DensyIndexFile$Reader$IndexData;->mDataItemDescriptions:[Lmiui/util/DensyIndexFile$DataItemDescription;

    array-length v10, v10

    new-array v7, v10, [Ljava/lang/Object;

    .line 593
    .local v7, ret:[Ljava/lang/Object;
    const-wide/16 v10, 0x0

    cmp-long v10, v5, v10

    if-gez v10, :cond_59

    .line 594
    const/4 v4, 0x0

    .local v4, i:I
    :goto_46
    array-length v10, v7

    if-ge v4, v10, :cond_b

    .line 595
    iget-object v10, p0, Lmiui/util/DensyIndexFile$Reader;->mIndexData:[Lmiui/util/DensyIndexFile$Reader$IndexData;

    aget-object v10, v10, p1

    iget-object v10, v10, Lmiui/util/DensyIndexFile$Reader$IndexData;->mDataItems:[[Ljava/lang/Object;

    aget-object v10, v10, v4

    const/4 v11, 0x0

    aget-object v10, v10, v11

    aput-object v10, v7, v4
    :try_end_56
    .catchall {:try_start_2e .. :try_end_56} :catchall_2b

    .line 594
    add-int/lit8 v4, v4, 0x1

    goto :goto_46

    .line 600
    .end local v4           #i:I
    :cond_59
    :try_start_59
    iget-object v10, p0, Lmiui/util/DensyIndexFile$Reader;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v10, v5, v6}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 601
    const/4 v4, 0x0

    .restart local v4       #i:I
    :goto_5f
    array-length v10, v7

    if-ge v4, v10, :cond_99

    .line 602
    iget-object v10, p0, Lmiui/util/DensyIndexFile$Reader;->mIndexData:[Lmiui/util/DensyIndexFile$Reader$IndexData;

    aget-object v10, v10, p1

    iget-object v10, v10, Lmiui/util/DensyIndexFile$Reader$IndexData;->mDataItemDescriptions:[Lmiui/util/DensyIndexFile$DataItemDescription;

    aget-object v10, v10, v4

    iget-byte v10, v10, Lmiui/util/DensyIndexFile$DataItemDescription;->mType:B

    packed-switch v10, :pswitch_data_12e

    .line 630
    new-instance v10, Ljava/io/IOException;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Unknown type "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lmiui/util/DensyIndexFile$Reader;->mIndexData:[Lmiui/util/DensyIndexFile$Reader$IndexData;

    aget-object v12, v12, p1

    iget-object v12, v12, Lmiui/util/DensyIndexFile$Reader$IndexData;->mDataItemDescriptions:[Lmiui/util/DensyIndexFile$DataItemDescription;

    aget-object v12, v12, v4

    iget-byte v12, v12, Lmiui/util/DensyIndexFile$DataItemDescription;->mType:B

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v10
    :try_end_92
    .catchall {:try_start_59 .. :try_end_92} :catchall_2b
    .catch Ljava/io/IOException; {:try_start_59 .. :try_end_92} :catch_92

    .line 633
    .end local v4           #i:I
    :catch_92
    move-exception v3

    .line 634
    .local v3, e:Ljava/io/IOException;
    :try_start_93
    const-string v10, "Seek data from a corrupt file"

    invoke-static {v10}, Lmiui/util/DensyIndexFile;->loge(Ljava/lang/String;)V

    .line 635
    const/4 v7, 0x0

    .line 640
    .end local v3           #e:Ljava/io/IOException;
    :cond_99
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    sub-long v8, v10, v8

    .line 641
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "ms used to get data("

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ")"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lmiui/util/DensyIndexFile;->logo(Ljava/lang/String;)V
    :try_end_c9
    .catchall {:try_start_93 .. :try_end_c9} :catchall_2b

    goto/16 :goto_b

    .line 604
    .restart local v4       #i:I
    :pswitch_cb
    :try_start_cb
    iget-object v10, p0, Lmiui/util/DensyIndexFile$Reader;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v10}, Ljava/io/RandomAccessFile;->readByte()B

    move-result v10

    invoke-static {v10}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v10

    aput-object v10, v7, v4

    .line 601
    :goto_d7
    add-int/lit8 v4, v4, 0x1

    goto :goto_5f

    .line 607
    :pswitch_da
    iget-object v10, p0, Lmiui/util/DensyIndexFile$Reader;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v10}, Ljava/io/RandomAccessFile;->readShort()S

    move-result v10

    invoke-static {v10}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v10

    aput-object v10, v7, v4

    goto :goto_d7

    .line 610
    :pswitch_e7
    iget-object v10, p0, Lmiui/util/DensyIndexFile$Reader;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v10}, Ljava/io/RandomAccessFile;->readInt()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v7, v4

    goto :goto_d7

    .line 613
    :pswitch_f4
    iget-object v10, p0, Lmiui/util/DensyIndexFile$Reader;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v10}, Ljava/io/RandomAccessFile;->readLong()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v7, v4
    :try_end_100
    .catchall {:try_start_cb .. :try_end_100} :catchall_2b
    .catch Ljava/io/IOException; {:try_start_cb .. :try_end_100} :catch_92

    goto :goto_d7

    .line 621
    :pswitch_101
    :try_start_101
    iget-object v10, p0, Lmiui/util/DensyIndexFile$Reader;->mFile:Ljava/io/RandomAccessFile;

    iget-object v11, p0, Lmiui/util/DensyIndexFile$Reader;->mIndexData:[Lmiui/util/DensyIndexFile$Reader$IndexData;

    aget-object v11, v11, p1

    iget-object v11, v11, Lmiui/util/DensyIndexFile$Reader$IndexData;->mDataItemDescriptions:[Lmiui/util/DensyIndexFile$DataItemDescription;

    aget-object v11, v11, v4

    iget-byte v11, v11, Lmiui/util/DensyIndexFile$DataItemDescription;->mIndexSize:B

    invoke-static {v10, v11}, Lmiui/util/DensyIndexFile;->readAccordingToSize(Ljava/io/DataInput;I)J

    move-result-wide v10

    long-to-int v2, v10

    .line 622
    .local v2, dataItemIndex:I
    iget-object v10, p0, Lmiui/util/DensyIndexFile$Reader;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v10}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v0

    .line 623
    .local v0, curPos:J
    invoke-direct {p0, p1, v4, v2}, Lmiui/util/DensyIndexFile$Reader;->readSingleDataItem(III)Ljava/lang/Object;

    move-result-object v10

    aput-object v10, v7, v4

    .line 624
    iget-object v10, p0, Lmiui/util/DensyIndexFile$Reader;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v10, v0, v1}, Ljava/io/RandomAccessFile;->seek(J)V
    :try_end_123
    .catchall {:try_start_101 .. :try_end_123} :catchall_2b
    .catch Ljava/io/IOException; {:try_start_101 .. :try_end_123} :catch_124

    goto :goto_d7

    .line 625
    .end local v0           #curPos:J
    .end local v2           #dataItemIndex:I
    :catch_124
    move-exception v3

    .line 626
    .restart local v3       #e:Ljava/io/IOException;
    :try_start_125
    new-instance v10, Ljava/io/IOException;

    const-string v11, "File may be corrupt due to invalid data index size"

    invoke-direct {v10, v11, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v10
    :try_end_12d
    .catchall {:try_start_125 .. :try_end_12d} :catchall_2b
    .catch Ljava/io/IOException; {:try_start_125 .. :try_end_12d} :catch_92

    .line 602
    nop

    :pswitch_data_12e
    .packed-switch 0x0
        :pswitch_cb
        :pswitch_da
        :pswitch_e7
        :pswitch_f4
        :pswitch_101
        :pswitch_101
        :pswitch_101
        :pswitch_101
        :pswitch_101
    .end packed-switch
.end method
