.class public Lmiui/util/UiUtils;
.super Ljava/lang/Object;
.source "UiUtils.java"


# static fields
.field static sTypedValuePool:Lmiui/util/SimplePool$PoolInstance;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lmiui/util/SimplePool$PoolInstance",
            "<",
            "Landroid/util/TypedValue;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 8
    new-instance v0, Lmiui/util/UiUtils$1;

    invoke-direct {v0}, Lmiui/util/UiUtils$1;-><init>()V

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lmiui/util/SimplePool;->newInsance(Lmiui/util/SimplePool$Manager;I)Lmiui/util/SimplePool$PoolInstance;

    move-result-object v0

    sput-object v0, Lmiui/util/UiUtils;->sTypedValuePool:Lmiui/util/SimplePool$PoolInstance;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 7
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getBoolean(Landroid/content/Context;I)Z
    .registers 5
    .parameter "context"
    .parameter "attrId"

    .prologue
    const/4 v0, 0x1

    .line 36
    sget-object v2, Lmiui/util/UiUtils;->sTypedValuePool:Lmiui/util/SimplePool$PoolInstance;

    invoke-virtual {v2}, Lmiui/util/SimplePool$PoolInstance;->acquire()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/TypedValue;

    .line 37
    .local v1, typedValue:Landroid/util/TypedValue;
    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    invoke-virtual {v2, p1, v1, v0}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 38
    iget v2, v1, Landroid/util/TypedValue;->data:I

    if-eqz v2, :cond_1a

    .line 39
    .local v0, ret:Z
    :goto_14
    sget-object v2, Lmiui/util/UiUtils;->sTypedValuePool:Lmiui/util/SimplePool$PoolInstance;

    invoke-virtual {v2, v1}, Lmiui/util/SimplePool$PoolInstance;->release(Ljava/lang/Object;)V

    .line 40
    return v0

    .line 38
    .end local v0           #ret:Z
    :cond_1a
    const/4 v0, 0x0

    goto :goto_14
.end method

.method public static getColor(Landroid/content/Context;I)I
    .registers 4
    .parameter "context"
    .parameter "attrId"

    .prologue
    .line 32
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {p0, p1}, Lmiui/util/UiUtils;->resolveAttribute(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    return v0
.end method

.method public static getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
    .registers 4
    .parameter "context"
    .parameter "attrId"

    .prologue
    .line 27
    invoke-static {p0, p1}, Lmiui/util/UiUtils;->resolveAttribute(Landroid/content/Context;I)I

    move-result v0

    .line 28
    .local v0, id:I
    if-lez v0, :cond_f

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    :goto_e
    return-object v1

    :cond_f
    const/4 v1, 0x0

    goto :goto_e
.end method

.method public static resolveAttribute(Landroid/content/Context;I)I
    .registers 6
    .parameter "context"
    .parameter "attrId"

    .prologue
    .line 17
    sget-object v2, Lmiui/util/UiUtils;->sTypedValuePool:Lmiui/util/SimplePool$PoolInstance;

    invoke-virtual {v2}, Lmiui/util/SimplePool$PoolInstance;->acquire()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/TypedValue;

    .line 18
    .local v1, typedValue:Landroid/util/TypedValue;
    const/4 v0, -0x1

    .line 19
    .local v0, ret:I
    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, p1, v1, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v2

    if-eqz v2, :cond_16

    .line 20
    iget v0, v1, Landroid/util/TypedValue;->resourceId:I

    .line 22
    :cond_16
    sget-object v2, Lmiui/util/UiUtils;->sTypedValuePool:Lmiui/util/SimplePool$PoolInstance;

    invoke-virtual {v2, v1}, Lmiui/util/SimplePool$PoolInstance;->release(Ljava/lang/Object;)V

    .line 23
    return v0
.end method
