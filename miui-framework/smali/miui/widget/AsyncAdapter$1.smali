.class final Lmiui/widget/AsyncAdapter$1;
.super Lmiui/cache/DataCache;
.source "AsyncAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/widget/AsyncAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmiui/cache/DataCache",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method constructor <init>(I)V
    .registers 2
    .parameter "x0"

    .prologue
    invoke-direct {p0, p1}, Lmiui/cache/DataCache;-><init>(I)V

    return-void
.end method


# virtual methods
.method protected removeEldestEntry(Ljava/util/Map$Entry;)Z
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, eldest:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/Object;Ljava/lang/Object;>;"
    invoke-super {p0, p1}, Lmiui/cache/DataCache;->removeEldestEntry(Ljava/util/Map$Entry;)Z

    move-result v2

    .local v2, ret:Z
    if-eqz v2, :cond_15

    :try_start_6
    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Bitmap;

    move-object v0, v3

    check-cast v0, Landroid/graphics/Bitmap;

    move-object v1, v0

    .local v1, b:Landroid/graphics/Bitmap;
    if-eqz v1, :cond_15

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_15} :catch_16

    .end local v1           #b:Landroid/graphics/Bitmap;
    :cond_15
    :goto_15
    return v2

    :catch_16
    move-exception v3

    goto :goto_15
.end method
