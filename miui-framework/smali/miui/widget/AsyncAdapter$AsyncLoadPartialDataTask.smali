.class public abstract Lmiui/widget/AsyncAdapter$AsyncLoadPartialDataTask;
.super Lmiui/os/DaemonAsyncTask;
.source "AsyncAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/widget/AsyncAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401
    name = "AsyncLoadPartialDataTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmiui/os/DaemonAsyncTask",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field mDoingJobs:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lmiui/widget/AsyncAdapter;


# direct methods
.method public constructor <init>(Lmiui/widget/AsyncAdapter;)V
    .registers 3
    .parameter

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter$AsyncLoadPartialDataTask;,"Lmiui/widget/AsyncAdapter<TT;>.AsyncLoadPartialDataTask;"
    iput-object p1, p0, Lmiui/widget/AsyncAdapter$AsyncLoadPartialDataTask;->this$0:Lmiui/widget/AsyncAdapter;

    invoke-direct {p0}, Lmiui/os/DaemonAsyncTask;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lmiui/widget/AsyncAdapter$AsyncLoadPartialDataTask;->mDoingJobs:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public containJob(Ljava/lang/Object;)Z
    .registers 3
    .parameter "job"

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter$AsyncLoadPartialDataTask;,"Lmiui/widget/AsyncAdapter<TT;>.AsyncLoadPartialDataTask;"
    iget-object v0, p0, Lmiui/widget/AsyncAdapter$AsyncLoadPartialDataTask;->mDoingJobs:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected needsDoJob(Ljava/lang/Object;)Z
    .registers 4
    .parameter "job"

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter$AsyncLoadPartialDataTask;,"Lmiui/widget/AsyncAdapter<TT;>.AsyncLoadPartialDataTask;"
    invoke-static {}, Lmiui/widget/AsyncAdapter;->access$400()Lmiui/cache/DataCache;

    move-result-object v1

    invoke-virtual {v1, p1}, Lmiui/cache/DataCache;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1b

    iget-object v1, p0, Lmiui/widget/AsyncAdapter$AsyncLoadPartialDataTask;->mDoingJobs:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1b

    const/4 v0, 0x1

    .local v0, ret:Z
    :goto_13
    if-eqz v0, :cond_1a

    iget-object v1, p0, Lmiui/widget/AsyncAdapter$AsyncLoadPartialDataTask;->mDoingJobs:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_1a
    return v0

    .end local v0           #ret:Z
    :cond_1b
    const/4 v0, 0x0

    goto :goto_13
.end method

.method protected varargs onProgressUpdate([Landroid/util/Pair;)V
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter$AsyncLoadPartialDataTask;,"Lmiui/widget/AsyncAdapter<TT;>.AsyncLoadPartialDataTask;"
    .local p1, values:[Landroid/util/Pair;,"[Landroid/util/Pair<Ljava/lang/Object;Ljava/lang/Object;>;"
    const/4 v3, 0x0

    if-eqz p1, :cond_6

    array-length v2, p1

    if-nez v2, :cond_7

    :cond_6
    :goto_6
    return-void

    :cond_7
    aget-object v2, p1, v3

    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    .local v0, key:Ljava/lang/Object;
    aget-object v2, p1, v3

    iget-object v1, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    .local v1, value:Ljava/lang/Object;
    if-eqz v1, :cond_1d

    invoke-static {}, Lmiui/widget/AsyncAdapter;->access$400()Lmiui/cache/DataCache;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lmiui/cache/DataCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lmiui/widget/AsyncAdapter$AsyncLoadPartialDataTask;->this$0:Lmiui/widget/AsyncAdapter;

    invoke-virtual {v2}, Lmiui/widget/AsyncAdapter;->notifyDataSetChanged()V

    :cond_1d
    iget-object v2, p0, Lmiui/widget/AsyncAdapter$AsyncLoadPartialDataTask;->mDoingJobs:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    invoke-super {p0, p1}, Lmiui/os/DaemonAsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    goto :goto_6
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .registers 2
    .parameter "x0"

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter$AsyncLoadPartialDataTask;,"Lmiui/widget/AsyncAdapter<TT;>.AsyncLoadPartialDataTask;"
    check-cast p1, [Landroid/util/Pair;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/widget/AsyncAdapter$AsyncLoadPartialDataTask;->onProgressUpdate([Landroid/util/Pair;)V

    return-void
.end method
