.class public abstract Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;
.super Lmiui/os/UniqueAsyncTask;
.source "AsyncAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/widget/AsyncAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401
    name = "AsyncLoadDataTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmiui/os/UniqueAsyncTask",
        "<",
        "Ljava/lang/Void;",
        "TT;",
        "Ljava/util/List",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field private mFirstTimeLoad:Z

.field private mGroup:I

.field private mResultDataSet:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end field

.field private mTempDataSet:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end field

.field private mVisitors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lmiui/widget/AsyncAdapter$AsyncLoadDataVisitor",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lmiui/widget/AsyncAdapter;


# direct methods
.method public constructor <init>(Lmiui/widget/AsyncAdapter;)V
    .registers 3
    .parameter

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;,"Lmiui/widget/AsyncAdapter<TT;>.AsyncLoadDataTask;"
    iput-object p1, p0, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->this$0:Lmiui/widget/AsyncAdapter;

    invoke-direct {p0}, Lmiui/os/UniqueAsyncTask;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->mFirstTimeLoad:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->mResultDataSet:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->mTempDataSet:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->mVisitors:Ljava/util/List;

    return-void
.end method

.method private realNeedExecuteTask()Z
    .registers 3

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;,"Lmiui/widget/AsyncAdapter<TT;>.AsyncLoadDataTask;"
    const/4 v0, 0x0

    .local v0, i:I
    :goto_1
    iget-object v1, p0, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->mVisitors:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1c

    iget-object v1, p0, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->mVisitors:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/widget/AsyncAdapter$AsyncLoadDataVisitor;

    invoke-interface {v1}, Lmiui/widget/AsyncAdapter$AsyncLoadDataVisitor;->dataChanged()Z

    move-result v1

    if-eqz v1, :cond_19

    const/4 v1, 0x1

    :goto_18
    return v1

    :cond_19
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1c
    const/4 v1, 0x0

    goto :goto_18
.end method


# virtual methods
.method public addVisitor(Lmiui/widget/AsyncAdapter$AsyncLoadDataVisitor;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiui/widget/AsyncAdapter$AsyncLoadDataVisitor",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;,"Lmiui/widget/AsyncAdapter<TT;>.AsyncLoadDataTask;"
    .local p1, visitor:Lmiui/widget/AsyncAdapter$AsyncLoadDataVisitor;,"Lmiui/widget/AsyncAdapter$AsyncLoadDataVisitor<TT;>;"
    iget-object v0, p0, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->mVisitors:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;,"Lmiui/widget/AsyncAdapter<TT;>.AsyncLoadDataTask;"
    check-cast p1, [Ljava/lang/Void;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->doInBackground([Ljava/lang/Void;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/util/List;
    .registers 6
    .parameter "params"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;,"Lmiui/widget/AsyncAdapter<TT;>.AsyncLoadDataTask;"
    iget-object v3, p0, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->mVisitors:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_1f

    const/4 v0, 0x0

    .local v0, i:I
    :goto_9
    iget-object v3, p0, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->mVisitors:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_31

    iget-object v3, p0, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->mVisitors:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmiui/widget/AsyncAdapter$AsyncLoadDataVisitor;

    invoke-interface {v3, p0}, Lmiui/widget/AsyncAdapter$AsyncLoadDataVisitor;->loadData(Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .end local v0           #i:I
    :cond_1f
    const/4 v1, 0x0

    .local v1, index:I
    :goto_20
    invoke-virtual {p0, v1}, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->loadData(I)[Ljava/lang/Object;

    move-result-object v2

    .local v2, partialDataSet:[Ljava/lang/Object;,"[TT;"
    if-eqz v2, :cond_31

    iget-object v3, p0, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->mResultDataSet:Ljava/util/List;

    invoke-static {v3, v2}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    invoke-virtual {p0, v2}, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->publishProgress([Ljava/lang/Object;)V

    array-length v3, v2

    add-int/2addr v1, v3

    goto :goto_20

    .end local v1           #index:I
    .end local v2           #partialDataSet:[Ljava/lang/Object;,"[TT;"
    :cond_31
    iget-object v3, p0, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->mResultDataSet:Ljava/util/List;

    return-object v3
.end method

.method protected hasEquivalentRunningTasks()Z
    .registers 2

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;,"Lmiui/widget/AsyncAdapter<TT;>.AsyncLoadDataTask;"
    const/4 v0, 0x0

    return v0
.end method

.method protected abstract loadData(I)[Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)[TT;"
        }
    .end annotation
.end method

.method public varargs onLoadData([Ljava/lang/Object;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TT;)V"
        }
    .end annotation

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;,"Lmiui/widget/AsyncAdapter<TT;>.AsyncLoadDataTask;"
    .local p1, partialDataSet:[Ljava/lang/Object;,"[TT;"
    iget-object v0, p0, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->mResultDataSet:Ljava/util/List;

    invoke-static {v0, p1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    invoke-virtual {p0, p1}, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->publishProgress([Ljava/lang/Object;)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 2
    .parameter "x0"

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;,"Lmiui/widget/AsyncAdapter<TT;>.AsyncLoadDataTask;"
    check-cast p1, Ljava/util/List;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;,"Lmiui/widget/AsyncAdapter<TT;>.AsyncLoadDataTask;"
    .local p1, result:Ljava/util/List;,"Ljava/util/List<TT;>;"
    iget-object v0, p0, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->this$0:Lmiui/widget/AsyncAdapter;

    #getter for: Lmiui/widget/AsyncAdapter;->mLoadUsingCache:Z
    invoke-static {v0}, Lmiui/widget/AsyncAdapter;->access$300(Lmiui/widget/AsyncAdapter;)Z

    move-result v0

    if-eqz v0, :cond_29

    iget-boolean v0, p0, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->mFirstTimeLoad:Z

    if-nez v0, :cond_29

    iget-object v0, p0, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->this$0:Lmiui/widget/AsyncAdapter;

    iget v1, p0, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->mGroup:I

    #calls: Lmiui/widget/AsyncAdapter;->getDataGroup(I)Lmiui/widget/DataGroup;
    invoke-static {v0, v1}, Lmiui/widget/AsyncAdapter;->access$200(Lmiui/widget/AsyncAdapter;I)Lmiui/widget/DataGroup;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/widget/DataGroup;->clear()V

    iget-object v0, p0, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->this$0:Lmiui/widget/AsyncAdapter;

    iget v1, p0, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->mGroup:I

    #calls: Lmiui/widget/AsyncAdapter;->getDataGroup(I)Lmiui/widget/DataGroup;
    invoke-static {v0, v1}, Lmiui/widget/AsyncAdapter;->access$200(Lmiui/widget/AsyncAdapter;I)Lmiui/widget/DataGroup;

    move-result-object v0

    iget-object v1, p0, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->mTempDataSet:Ljava/util/List;

    invoke-virtual {v0, v1}, Lmiui/widget/DataGroup;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->this$0:Lmiui/widget/AsyncAdapter;

    invoke-virtual {v0}, Lmiui/widget/AsyncAdapter;->notifyDataSetChanged()V

    :cond_29
    iget-object v0, p0, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->this$0:Lmiui/widget/AsyncAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmiui/widget/AsyncAdapter;->setForceToLoadData(Z)V

    invoke-super {p0, p1}, Lmiui/os/UniqueAsyncTask;->onPostExecute(Ljava/lang/Object;)V

    iget-object v0, p0, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->this$0:Lmiui/widget/AsyncAdapter;

    #calls: Lmiui/widget/AsyncAdapter;->postExecuteTask(Lmiui/os/UniqueAsyncTask;)V
    invoke-static {v0, p0}, Lmiui/widget/AsyncAdapter;->access$100(Lmiui/widget/AsyncAdapter;Lmiui/os/UniqueAsyncTask;)V

    iget-object v0, p0, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->this$0:Lmiui/widget/AsyncAdapter;

    invoke-virtual {v0, p1}, Lmiui/widget/AsyncAdapter;->postLoadData(Ljava/util/List;)V

    return-void
.end method

.method protected onPreExecute()V
    .registers 4

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;,"Lmiui/widget/AsyncAdapter<TT;>.AsyncLoadDataTask;"
    const/4 v0, 0x0

    iget-object v1, p0, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->this$0:Lmiui/widget/AsyncAdapter;

    #getter for: Lmiui/widget/AsyncAdapter;->mForceToLoadData:Z
    invoke-static {v1}, Lmiui/widget/AsyncAdapter;->access$000(Lmiui/widget/AsyncAdapter;)Z

    move-result v1

    if-nez v1, :cond_18

    invoke-direct {p0}, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->realNeedExecuteTask()Z

    move-result v1

    if-nez v1, :cond_18

    invoke-virtual {p0, v0}, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->cancel(Z)Z

    iget-object v0, p0, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->this$0:Lmiui/widget/AsyncAdapter;

    #calls: Lmiui/widget/AsyncAdapter;->postExecuteTask(Lmiui/os/UniqueAsyncTask;)V
    invoke-static {v0, p0}, Lmiui/widget/AsyncAdapter;->access$100(Lmiui/widget/AsyncAdapter;Lmiui/os/UniqueAsyncTask;)V

    :goto_17
    return-void

    :cond_18
    iget-object v1, p0, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->this$0:Lmiui/widget/AsyncAdapter;

    iget v2, p0, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->mGroup:I

    #calls: Lmiui/widget/AsyncAdapter;->getDataGroup(I)Lmiui/widget/DataGroup;
    invoke-static {v1, v2}, Lmiui/widget/AsyncAdapter;->access$200(Lmiui/widget/AsyncAdapter;I)Lmiui/widget/DataGroup;

    move-result-object v1

    invoke-virtual {v1}, Lmiui/widget/DataGroup;->size()I

    move-result v1

    if-nez v1, :cond_27

    const/4 v0, 0x1

    :cond_27
    iput-boolean v0, p0, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->mFirstTimeLoad:Z

    iget-object v0, p0, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->this$0:Lmiui/widget/AsyncAdapter;

    #getter for: Lmiui/widget/AsyncAdapter;->mLoadUsingCache:Z
    invoke-static {v0}, Lmiui/widget/AsyncAdapter;->access$300(Lmiui/widget/AsyncAdapter;)Z

    move-result v0

    if-eqz v0, :cond_35

    iget-boolean v0, p0, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->mFirstTimeLoad:Z

    if-eqz v0, :cond_44

    :cond_35
    iget-object v0, p0, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->this$0:Lmiui/widget/AsyncAdapter;

    iget v1, p0, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->mGroup:I

    #calls: Lmiui/widget/AsyncAdapter;->getDataGroup(I)Lmiui/widget/DataGroup;
    invoke-static {v0, v1}, Lmiui/widget/AsyncAdapter;->access$200(Lmiui/widget/AsyncAdapter;I)Lmiui/widget/DataGroup;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/widget/DataGroup;->clear()V

    :goto_40
    invoke-super {p0}, Lmiui/os/UniqueAsyncTask;->onPreExecute()V

    goto :goto_17

    :cond_44
    iget-object v0, p0, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->mTempDataSet:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto :goto_40
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Object;)V
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TT;)V"
        }
    .end annotation

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;,"Lmiui/widget/AsyncAdapter<TT;>.AsyncLoadDataTask;"
    .local p1, values:[Ljava/lang/Object;,"[TT;"
    const/4 v0, 0x0

    .local v0, i:I
    :goto_1
    array-length v1, p1

    if-ge v0, v1, :cond_28

    iget-object v1, p0, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->this$0:Lmiui/widget/AsyncAdapter;

    #getter for: Lmiui/widget/AsyncAdapter;->mLoadUsingCache:Z
    invoke-static {v1}, Lmiui/widget/AsyncAdapter;->access$300(Lmiui/widget/AsyncAdapter;)Z

    move-result v1

    if-eqz v1, :cond_10

    iget-boolean v1, p0, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->mFirstTimeLoad:Z

    if-eqz v1, :cond_20

    :cond_10
    iget-object v1, p0, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->this$0:Lmiui/widget/AsyncAdapter;

    iget v2, p0, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->mGroup:I

    #calls: Lmiui/widget/AsyncAdapter;->getDataGroup(I)Lmiui/widget/DataGroup;
    invoke-static {v1, v2}, Lmiui/widget/AsyncAdapter;->access$200(Lmiui/widget/AsyncAdapter;I)Lmiui/widget/DataGroup;

    move-result-object v1

    aget-object v2, p1, v0

    invoke-virtual {v1, v2}, Lmiui/widget/DataGroup;->add(Ljava/lang/Object;)Z

    :goto_1d
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_20
    iget-object v1, p0, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->mTempDataSet:Ljava/util/List;

    aget-object v2, p1, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1d

    :cond_28
    iget-object v1, p0, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->this$0:Lmiui/widget/AsyncAdapter;

    #getter for: Lmiui/widget/AsyncAdapter;->mLoadUsingCache:Z
    invoke-static {v1}, Lmiui/widget/AsyncAdapter;->access$300(Lmiui/widget/AsyncAdapter;)Z

    move-result v1

    if-eqz v1, :cond_34

    iget-boolean v1, p0, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->mFirstTimeLoad:Z

    if-eqz v1, :cond_39

    :cond_34
    iget-object v1, p0, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->this$0:Lmiui/widget/AsyncAdapter;

    invoke-virtual {v1}, Lmiui/widget/AsyncAdapter;->notifyDataSetChanged()V

    :cond_39
    invoke-super {p0, p1}, Lmiui/os/UniqueAsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    return-void
.end method

.method public setGroup(I)V
    .registers 2
    .parameter "group"

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;,"Lmiui/widget/AsyncAdapter<TT;>.AsyncLoadDataTask;"
    iput p1, p0, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->mGroup:I

    return-void
.end method
