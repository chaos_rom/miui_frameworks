.class Lmiui/widget/MiCloudAdvancedSettingsBase$5;
.super Ljava/lang/Object;
.source "MiCloudAdvancedSettingsBase.java"

# interfaces
.implements Lmiui/widget/MiCloudAdvancedSettingsBase$CheckPhoneTask$CheckPhoneCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiui/widget/MiCloudAdvancedSettingsBase;->handleSyncPrefClick(Lmiui/widget/SyncStatePreference;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/widget/MiCloudAdvancedSettingsBase;


# direct methods
.method constructor <init>(Lmiui/widget/MiCloudAdvancedSettingsBase;)V
    .registers 2
    .parameter

    .prologue
    iput-object p1, p0, Lmiui/widget/MiCloudAdvancedSettingsBase$5;->this$0:Lmiui/widget/MiCloudAdvancedSettingsBase;

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckPhoneResult(Landroid/accounts/Account;Ljava/lang/String;Lmiui/widget/MiCloudAdvancedSettingsBase$CheckPhoneTask$CheckResult;)V
    .registers 8
    .parameter "checkedAccount"
    .parameter "checkedAuthority"
    .parameter "result"

    .prologue
    const/4 v3, 0x0

    sget-object v0, Lmiui/widget/MiCloudAdvancedSettingsBase$CheckPhoneTask$CheckResult;->RESULT_OK:Lmiui/widget/MiCloudAdvancedSettingsBase$CheckPhoneTask$CheckResult;

    if-ne p3, v0, :cond_11

    iget-object v0, p0, Lmiui/widget/MiCloudAdvancedSettingsBase$5;->this$0:Lmiui/widget/MiCloudAdvancedSettingsBase;

    const/4 v1, 0x1

    #calls: Lmiui/widget/MiCloudAdvancedSettingsBase;->enableSync(Landroid/accounts/Account;Ljava/lang/String;Z)V
    invoke-static {v0, p1, p2, v1}, Lmiui/widget/MiCloudAdvancedSettingsBase;->access$400(Lmiui/widget/MiCloudAdvancedSettingsBase;Landroid/accounts/Account;Ljava/lang/String;Z)V

    iget-object v0, p0, Lmiui/widget/MiCloudAdvancedSettingsBase$5;->this$0:Lmiui/widget/MiCloudAdvancedSettingsBase;

    #calls: Lmiui/widget/MiCloudAdvancedSettingsBase;->updateSyncState(Ljava/lang/Boolean;)V
    invoke-static {v0, v3}, Lmiui/widget/MiCloudAdvancedSettingsBase;->access$200(Lmiui/widget/MiCloudAdvancedSettingsBase;Ljava/lang/Boolean;)V

    :cond_10
    :goto_10
    return-void

    :cond_11
    sget-object v0, Lmiui/widget/MiCloudAdvancedSettingsBase$CheckPhoneTask$CheckResult;->RESULT_NOT_ACTIVATED:Lmiui/widget/MiCloudAdvancedSettingsBase$CheckPhoneTask$CheckResult;

    if-ne p3, v0, :cond_25

    iget-object v0, p0, Lmiui/widget/MiCloudAdvancedSettingsBase$5;->this$0:Lmiui/widget/MiCloudAdvancedSettingsBase;

    new-instance v1, Lmiui/widget/MiCloudAdvancedSettingsBase$5$1;

    invoke-direct {v1, p0, p1, p2}, Lmiui/widget/MiCloudAdvancedSettingsBase$5$1;-><init>(Lmiui/widget/MiCloudAdvancedSettingsBase$5;Landroid/accounts/Account;Ljava/lang/String;)V

    new-instance v2, Lmiui/widget/MiCloudAdvancedSettingsBase$5$2;

    invoke-direct {v2, p0}, Lmiui/widget/MiCloudAdvancedSettingsBase$5$2;-><init>(Lmiui/widget/MiCloudAdvancedSettingsBase$5;)V

    #calls: Lmiui/widget/MiCloudAdvancedSettingsBase;->showSendSmsForSyncDialog(Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnDismissListener;)V
    invoke-static {v0, v1, v2}, Lmiui/widget/MiCloudAdvancedSettingsBase;->access$500(Lmiui/widget/MiCloudAdvancedSettingsBase;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnDismissListener;)V

    goto :goto_10

    :cond_25
    sget-object v0, Lmiui/widget/MiCloudAdvancedSettingsBase$CheckPhoneTask$CheckResult;->RESULT_IO_ERROR:Lmiui/widget/MiCloudAdvancedSettingsBase$CheckPhoneTask$CheckResult;

    if-ne p3, v0, :cond_10

    iget-object v0, p0, Lmiui/widget/MiCloudAdvancedSettingsBase$5;->this$0:Lmiui/widget/MiCloudAdvancedSettingsBase;

    const v1, 0x60c01e6

    const v2, 0x60c01e7

    #calls: Lmiui/widget/MiCloudAdvancedSettingsBase;->showAlertDialog(II)V
    invoke-static {v0, v1, v2}, Lmiui/widget/MiCloudAdvancedSettingsBase;->access$600(Lmiui/widget/MiCloudAdvancedSettingsBase;II)V

    iget-object v0, p0, Lmiui/widget/MiCloudAdvancedSettingsBase$5;->this$0:Lmiui/widget/MiCloudAdvancedSettingsBase;

    #calls: Lmiui/widget/MiCloudAdvancedSettingsBase;->updateSyncState(Ljava/lang/Boolean;)V
    invoke-static {v0, v3}, Lmiui/widget/MiCloudAdvancedSettingsBase;->access$200(Lmiui/widget/MiCloudAdvancedSettingsBase;Ljava/lang/Boolean;)V

    goto :goto_10
.end method
