.class public abstract Lmiui/widget/AsyncAdapter$AsyncLoadMoreDataTask;
.super Lmiui/os/UniqueAsyncTask;
.source "AsyncAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/widget/AsyncAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401
    name = "AsyncLoadMoreDataTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmiui/os/UniqueAsyncTask",
        "<",
        "Ljava/lang/Void;",
        "TT;",
        "Ljava/util/List",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field private mClearData:Z

.field private mGroup:I

.field private mLoadParams:Lmiui/widget/AsyncAdapter$AsyncLoadMoreParams;

.field final synthetic this$0:Lmiui/widget/AsyncAdapter;


# direct methods
.method public constructor <init>(Lmiui/widget/AsyncAdapter;)V
    .registers 2
    .parameter

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter$AsyncLoadMoreDataTask;,"Lmiui/widget/AsyncAdapter<TT;>.AsyncLoadMoreDataTask;"
    iput-object p1, p0, Lmiui/widget/AsyncAdapter$AsyncLoadMoreDataTask;->this$0:Lmiui/widget/AsyncAdapter;

    invoke-direct {p0}, Lmiui/os/UniqueAsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter$AsyncLoadMoreDataTask;,"Lmiui/widget/AsyncAdapter<TT;>.AsyncLoadMoreDataTask;"
    check-cast p1, [Ljava/lang/Void;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/widget/AsyncAdapter$AsyncLoadMoreDataTask;->doInBackground([Ljava/lang/Void;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/util/List;
    .registers 7
    .parameter "params"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter$AsyncLoadMoreDataTask;,"Lmiui/widget/AsyncAdapter<TT;>.AsyncLoadMoreDataTask;"
    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v3, p0, Lmiui/widget/AsyncAdapter$AsyncLoadMoreDataTask;->mLoadParams:Lmiui/widget/AsyncAdapter$AsyncLoadMoreParams;

    if-nez v3, :cond_8

    const/4 v0, 0x0

    :goto_7
    return-object v0

    :cond_8
    const/4 v0, 0x0

    .local v0, dataSet:Ljava/util/List;,"Ljava/util/List<TT;>;"
    iget-object v3, p0, Lmiui/widget/AsyncAdapter$AsyncLoadMoreDataTask;->mLoadParams:Lmiui/widget/AsyncAdapter$AsyncLoadMoreParams;

    invoke-virtual {p0, v3}, Lmiui/widget/AsyncAdapter$AsyncLoadMoreDataTask;->loadMoreData(Lmiui/widget/AsyncAdapter$AsyncLoadMoreParams;)Ljava/util/List;

    move-result-object v0

    iget-object v3, p0, Lmiui/widget/AsyncAdapter$AsyncLoadMoreDataTask;->mLoadParams:Lmiui/widget/AsyncAdapter$AsyncLoadMoreParams;

    iget-boolean v3, v3, Lmiui/widget/AsyncAdapter$AsyncLoadMoreParams;->upwards:Z

    if-eqz v3, :cond_23

    iget-object v3, p0, Lmiui/widget/AsyncAdapter$AsyncLoadMoreDataTask;->this$0:Lmiui/widget/AsyncAdapter;

    if-eqz v0, :cond_1f

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_20

    :cond_1f
    move v1, v2

    :cond_20
    iput-boolean v1, v3, Lmiui/widget/AsyncAdapter;->mReachTop:Z

    goto :goto_7

    :cond_23
    iget-object v3, p0, Lmiui/widget/AsyncAdapter$AsyncLoadMoreDataTask;->this$0:Lmiui/widget/AsyncAdapter;

    if-eqz v0, :cond_2d

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_2e

    :cond_2d
    move v1, v2

    :cond_2e
    iput-boolean v1, v3, Lmiui/widget/AsyncAdapter;->mReachBottom:Z

    goto :goto_7
.end method

.method protected hasEquivalentRunningTasks()Z
    .registers 2

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter$AsyncLoadMoreDataTask;,"Lmiui/widget/AsyncAdapter<TT;>.AsyncLoadMoreDataTask;"
    const/4 v0, 0x0

    return v0
.end method

.method protected abstract loadMoreData(Lmiui/widget/AsyncAdapter$AsyncLoadMoreParams;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiui/widget/AsyncAdapter$AsyncLoadMoreParams;",
            ")",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 2
    .parameter "x0"

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter$AsyncLoadMoreDataTask;,"Lmiui/widget/AsyncAdapter<TT;>.AsyncLoadMoreDataTask;"
    check-cast p1, Ljava/util/List;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/widget/AsyncAdapter$AsyncLoadMoreDataTask;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter$AsyncLoadMoreDataTask;,"Lmiui/widget/AsyncAdapter<TT;>.AsyncLoadMoreDataTask;"
    .local p1, result:Ljava/util/List;,"Ljava/util/List<TT;>;"
    iget-boolean v1, p0, Lmiui/widget/AsyncAdapter$AsyncLoadMoreDataTask;->mClearData:Z

    if-eqz v1, :cond_11

    if-eqz p1, :cond_11

    iget-object v1, p0, Lmiui/widget/AsyncAdapter$AsyncLoadMoreDataTask;->this$0:Lmiui/widget/AsyncAdapter;

    iget v2, p0, Lmiui/widget/AsyncAdapter$AsyncLoadMoreDataTask;->mGroup:I

    #calls: Lmiui/widget/AsyncAdapter;->getDataGroup(I)Lmiui/widget/DataGroup;
    invoke-static {v1, v2}, Lmiui/widget/AsyncAdapter;->access$200(Lmiui/widget/AsyncAdapter;I)Lmiui/widget/DataGroup;

    move-result-object v1

    invoke-virtual {v1}, Lmiui/widget/DataGroup;->clear()V

    :cond_11
    if-eqz p1, :cond_2c

    const/4 v0, 0x0

    .local v0, i:I
    :goto_14
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_2c

    iget-object v1, p0, Lmiui/widget/AsyncAdapter$AsyncLoadMoreDataTask;->this$0:Lmiui/widget/AsyncAdapter;

    iget v2, p0, Lmiui/widget/AsyncAdapter$AsyncLoadMoreDataTask;->mGroup:I

    #calls: Lmiui/widget/AsyncAdapter;->getDataGroup(I)Lmiui/widget/DataGroup;
    invoke-static {v1, v2}, Lmiui/widget/AsyncAdapter;->access$200(Lmiui/widget/AsyncAdapter;I)Lmiui/widget/DataGroup;

    move-result-object v1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmiui/widget/DataGroup;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_14

    .end local v0           #i:I
    :cond_2c
    iget-object v1, p0, Lmiui/widget/AsyncAdapter$AsyncLoadMoreDataTask;->this$0:Lmiui/widget/AsyncAdapter;

    invoke-virtual {v1}, Lmiui/widget/AsyncAdapter;->notifyDataSetChanged()V

    invoke-super {p0, p1}, Lmiui/os/UniqueAsyncTask;->onPostExecute(Ljava/lang/Object;)V

    iget-object v1, p0, Lmiui/widget/AsyncAdapter$AsyncLoadMoreDataTask;->this$0:Lmiui/widget/AsyncAdapter;

    #calls: Lmiui/widget/AsyncAdapter;->postExecuteTask(Lmiui/os/UniqueAsyncTask;)V
    invoke-static {v1, p0}, Lmiui/widget/AsyncAdapter;->access$100(Lmiui/widget/AsyncAdapter;Lmiui/os/UniqueAsyncTask;)V

    iget-object v1, p0, Lmiui/widget/AsyncAdapter$AsyncLoadMoreDataTask;->this$0:Lmiui/widget/AsyncAdapter;

    invoke-virtual {v1, p1}, Lmiui/widget/AsyncAdapter;->postLoadMoreData(Ljava/util/List;)V

    return-void
.end method

.method public setClearData(Z)V
    .registers 2
    .parameter "clearData"

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter$AsyncLoadMoreDataTask;,"Lmiui/widget/AsyncAdapter<TT;>.AsyncLoadMoreDataTask;"
    iput-boolean p1, p0, Lmiui/widget/AsyncAdapter$AsyncLoadMoreDataTask;->mClearData:Z

    return-void
.end method

.method public setGroup(I)V
    .registers 2
    .parameter "group"

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter$AsyncLoadMoreDataTask;,"Lmiui/widget/AsyncAdapter<TT;>.AsyncLoadMoreDataTask;"
    iput p1, p0, Lmiui/widget/AsyncAdapter$AsyncLoadMoreDataTask;->mGroup:I

    return-void
.end method

.method public setLoadParams(Lmiui/widget/AsyncAdapter$AsyncLoadMoreParams;)V
    .registers 2
    .parameter "loadParams"

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter$AsyncLoadMoreDataTask;,"Lmiui/widget/AsyncAdapter<TT;>.AsyncLoadMoreDataTask;"
    iput-object p1, p0, Lmiui/widget/AsyncAdapter$AsyncLoadMoreDataTask;->mLoadParams:Lmiui/widget/AsyncAdapter$AsyncLoadMoreParams;

    return-void
.end method
