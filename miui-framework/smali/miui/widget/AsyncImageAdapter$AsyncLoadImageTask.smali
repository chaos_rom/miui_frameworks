.class public Lmiui/widget/AsyncImageAdapter$AsyncLoadImageTask;
.super Lmiui/widget/AsyncAdapter$AsyncLoadPartialDataTask;
.source "AsyncImageAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/widget/AsyncImageAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "AsyncLoadImageTask"
.end annotation


# instance fields
.field private mScaled:Z

.field private mTargetHeight:I

.field private mTargetWidth:I

.field final synthetic this$0:Lmiui/widget/AsyncImageAdapter;


# direct methods
.method public constructor <init>(Lmiui/widget/AsyncImageAdapter;)V
    .registers 2
    .parameter

    .prologue
    .local p0, this:Lmiui/widget/AsyncImageAdapter$AsyncLoadImageTask;,"Lmiui/widget/AsyncImageAdapter<TT;>.AsyncLoadImageTask;"
    iput-object p1, p0, Lmiui/widget/AsyncImageAdapter$AsyncLoadImageTask;->this$0:Lmiui/widget/AsyncImageAdapter;

    invoke-direct {p0, p1}, Lmiui/widget/AsyncAdapter$AsyncLoadPartialDataTask;-><init>(Lmiui/widget/AsyncAdapter;)V

    return-void
.end method


# virtual methods
.method protected doJob(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 19
    .parameter "key"

    .prologue
    .local p0, this:Lmiui/widget/AsyncImageAdapter$AsyncLoadImageTask;,"Lmiui/widget/AsyncImageAdapter<TT;>.AsyncLoadImageTask;"
    move-object/from16 v5, p1

    check-cast v5, Ljava/lang/String;

    .local v5, imagePath:Ljava/lang/String;
    invoke-static {v5}, Lmiui/util/ImageUtils;->getBitmapSize(Ljava/lang/String;)Landroid/graphics/BitmapFactory$Options;

    move-result-object v1

    .local v1, boundsOptions:Landroid/graphics/BitmapFactory$Options;
    iget v12, v1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .local v12, width:I
    iget v4, v1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .local v4, height:I
    move-object/from16 v0, p0

    iget v15, v0, Lmiui/widget/AsyncImageAdapter$AsyncLoadImageTask;->mTargetWidth:I

    if-lez v15, :cond_18

    move-object/from16 v0, p0

    iget v15, v0, Lmiui/widget/AsyncImageAdapter$AsyncLoadImageTask;->mTargetHeight:I

    if-gtz v15, :cond_27

    :cond_18
    const-string v15, "ResourceBrowser"

    const-string v16, "AsyncImageAdapter does not set valid parameters for target size."

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iput v12, v0, Lmiui/widget/AsyncImageAdapter$AsyncLoadImageTask;->mTargetWidth:I

    move-object/from16 v0, p0

    iput v4, v0, Lmiui/widget/AsyncImageAdapter$AsyncLoadImageTask;->mTargetHeight:I

    :cond_27
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .local v2, decodeOptions:Landroid/graphics/BitmapFactory$Options;
    move-object/from16 v0, p0

    iget-object v15, v0, Lmiui/widget/AsyncImageAdapter$AsyncLoadImageTask;->this$0:Lmiui/widget/AsyncImageAdapter;

    invoke-virtual {v15}, Lmiui/widget/AsyncImageAdapter;->useLowQualityDecoding()Z

    move-result v15

    if-eqz v15, :cond_3a

    sget-object v15, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v15, v2, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    :cond_3a
    const/4 v9, 0x0

    .local v9, scaledBitmap:Landroid/graphics/Bitmap;
    const/4 v8, 0x0

    .local v8, originalBitmap:Landroid/graphics/Bitmap;
    :try_start_3c
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lmiui/widget/AsyncImageAdapter$AsyncLoadImageTask;->mScaled:Z

    if-nez v15, :cond_6a

    move-object/from16 v0, p0

    iget v15, v0, Lmiui/widget/AsyncImageAdapter$AsyncLoadImageTask;->mTargetWidth:I

    invoke-static {v12, v15}, Ljava/lang/Math;->min(II)I

    move-result v7

    .local v7, minWidth:I
    move-object/from16 v0, p0

    iget v15, v0, Lmiui/widget/AsyncImageAdapter$AsyncLoadImageTask;->mTargetHeight:I

    invoke-static {v4, v15}, Ljava/lang/Math;->min(II)I

    move-result v6

    .local v6, minHeight:I
    sub-int v15, v12, v7

    div-int/lit8 v13, v15, 0x2

    .local v13, x:I
    sub-int v15, v4, v6

    div-int/lit8 v14, v15, 0x2

    .local v14, y:I
    invoke-static {v5, v2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v8

    invoke-static {v8, v13, v14, v7, v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;
    :try_end_61
    .catch Ljava/lang/Exception; {:try_start_3c .. :try_end_61} :catch_99
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3c .. :try_end_61} :catch_94

    move-result-object v9

    .end local v6           #minHeight:I
    .end local v7           #minWidth:I
    .end local v13           #x:I
    .end local v14           #y:I
    :goto_62
    if-eq v9, v8, :cond_69

    if-eqz v8, :cond_69

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->recycle()V

    :cond_69
    return-object v9

    :cond_6a
    :try_start_6a
    move-object/from16 v0, p0

    iget v11, v0, Lmiui/widget/AsyncImageAdapter$AsyncLoadImageTask;->mTargetWidth:I

    .local v11, scaledWidth:I
    move-object/from16 v0, p0

    iget v10, v0, Lmiui/widget/AsyncImageAdapter$AsyncLoadImageTask;->mTargetHeight:I

    .local v10, scaledHeight:I
    move-object/from16 v0, p0

    iget v15, v0, Lmiui/widget/AsyncImageAdapter$AsyncLoadImageTask;->mTargetWidth:I

    if-ge v15, v12, :cond_7e

    move-object/from16 v0, p0

    iget v15, v0, Lmiui/widget/AsyncImageAdapter$AsyncLoadImageTask;->mTargetHeight:I

    if-lt v15, v4, :cond_80

    :cond_7e
    move v11, v12

    move v10, v4

    :cond_80
    div-int v15, v12, v11

    div-int v16, v4, v10

    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->min(II)I

    move-result v15

    iput v15, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    invoke-static {v5, v2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v8

    const/4 v15, 0x0

    invoke-static {v8, v11, v10, v15}, Lmiui/util/ImageUtils;->scaleBitmapToDesire(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    :try_end_92
    .catch Ljava/lang/Exception; {:try_start_6a .. :try_end_92} :catch_99
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6a .. :try_end_92} :catch_94

    move-result-object v9

    goto :goto_62

    .end local v10           #scaledHeight:I
    .end local v11           #scaledWidth:I
    :catch_94
    move-exception v3

    .local v3, e:Ljava/lang/OutOfMemoryError;
    invoke-virtual {v3}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto :goto_62

    .end local v3           #e:Ljava/lang/OutOfMemoryError;
    :catch_99
    move-exception v15

    goto :goto_62
.end method

.method public setScaled(Z)V
    .registers 2
    .parameter "scaled"

    .prologue
    .local p0, this:Lmiui/widget/AsyncImageAdapter$AsyncLoadImageTask;,"Lmiui/widget/AsyncImageAdapter<TT;>.AsyncLoadImageTask;"
    iput-boolean p1, p0, Lmiui/widget/AsyncImageAdapter$AsyncLoadImageTask;->mScaled:Z

    return-void
.end method

.method public setTargetSize(II)V
    .registers 3
    .parameter "targetWidth"
    .parameter "targetHeight"

    .prologue
    .local p0, this:Lmiui/widget/AsyncImageAdapter$AsyncLoadImageTask;,"Lmiui/widget/AsyncImageAdapter<TT;>.AsyncLoadImageTask;"
    iput p1, p0, Lmiui/widget/AsyncImageAdapter$AsyncLoadImageTask;->mTargetWidth:I

    iput p2, p0, Lmiui/widget/AsyncImageAdapter$AsyncLoadImageTask;->mTargetHeight:I

    return-void
.end method
