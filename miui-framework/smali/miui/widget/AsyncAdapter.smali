.class public abstract Lmiui/widget/AsyncAdapter;
.super Landroid/widget/BaseAdapter;
.source "AsyncAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/widget/AsyncAdapter$AsyncLoadPartialDataTask;,
        Lmiui/widget/AsyncAdapter$AsyncLoadMoreDataTask;,
        Lmiui/widget/AsyncAdapter$AsyncLoadMoreParams;,
        Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;,
        Lmiui/widget/AsyncAdapter$AsyncLoadDataVisitor;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/widget/BaseAdapter;"
    }
.end annotation


# static fields
.field public static final BOTH:I = 0x3

.field public static final DOWNWARDS:I = 0x2

.field public static final NONE:I = 0x0

.field public static final UPWARDS:I = 0x1

.field private static sPartialDataCache:Lmiui/cache/DataCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lmiui/cache/DataCache",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAutoLoadDownwardsMore:Z

.field private mAutoLoadUpwardsMore:Z

.field private mDataPerLine:I

.field private mDataSet:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lmiui/widget/DataGroup",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field private mForceToLoadData:Z

.field private mLoadPartialDataTask:Lmiui/widget/AsyncAdapter$AsyncLoadPartialDataTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lmiui/widget/AsyncAdapter",
            "<TT;>.Async",
            "LoadPartialDataTask;"
        }
    .end annotation
.end field

.field private mLoadUsingCache:Z

.field private mPreloadOffset:I

.field protected mReachBottom:Z

.field protected mReachTop:Z

.field private mTaskLocker:[B

.field private mTaskSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lmiui/os/UniqueAsyncTask",
            "<***>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    new-instance v0, Lmiui/widget/AsyncAdapter$1;

    const/16 v1, 0x64

    invoke-direct {v0, v1}, Lmiui/widget/AsyncAdapter$1;-><init>(I)V

    sput-object v0, Lmiui/widget/AsyncAdapter;->sPartialDataCache:Lmiui/cache/DataCache;

    return-void
.end method

.method public constructor <init>()V
    .registers 3

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter;,"Lmiui/widget/AsyncAdapter<TT;>;"
    const/4 v1, 0x1

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/widget/AsyncAdapter;->mDataSet:Ljava/util/List;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lmiui/widget/AsyncAdapter;->mTaskSet:Ljava/util/Set;

    const/4 v0, 0x0

    new-array v0, v0, [B

    iput-object v0, p0, Lmiui/widget/AsyncAdapter;->mTaskLocker:[B

    iput-boolean v1, p0, Lmiui/widget/AsyncAdapter;->mLoadUsingCache:Z

    iput v1, p0, Lmiui/widget/AsyncAdapter;->mDataPerLine:I

    return-void
.end method

.method static synthetic access$000(Lmiui/widget/AsyncAdapter;)Z
    .registers 2
    .parameter "x0"

    .prologue
    iget-boolean v0, p0, Lmiui/widget/AsyncAdapter;->mForceToLoadData:Z

    return v0
.end method

.method static synthetic access$100(Lmiui/widget/AsyncAdapter;Lmiui/os/UniqueAsyncTask;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    invoke-direct {p0, p1}, Lmiui/widget/AsyncAdapter;->postExecuteTask(Lmiui/os/UniqueAsyncTask;)V

    return-void
.end method

.method static synthetic access$200(Lmiui/widget/AsyncAdapter;I)Lmiui/widget/DataGroup;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    invoke-direct {p0, p1}, Lmiui/widget/AsyncAdapter;->getDataGroup(I)Lmiui/widget/DataGroup;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lmiui/widget/AsyncAdapter;)Z
    .registers 2
    .parameter "x0"

    .prologue
    iget-boolean v0, p0, Lmiui/widget/AsyncAdapter;->mLoadUsingCache:Z

    return v0
.end method

.method static synthetic access$400()Lmiui/cache/DataCache;
    .registers 1

    .prologue
    sget-object v0, Lmiui/widget/AsyncAdapter;->sPartialDataCache:Lmiui/cache/DataCache;

    return-object v0
.end method

.method private getDataGroup(I)Lmiui/widget/DataGroup;
    .registers 4
    .parameter "group"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lmiui/widget/DataGroup",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter;,"Lmiui/widget/AsyncAdapter<TT;>;"
    iget-object v0, p0, Lmiui/widget/AsyncAdapter;->mDataSet:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, p1, :cond_1c

    monitor-enter p0

    :try_start_9
    iget-object v0, p0, Lmiui/widget/AsyncAdapter;->mDataSet:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, p1, :cond_1b

    iget-object v0, p0, Lmiui/widget/AsyncAdapter;->mDataSet:Ljava/util/List;

    new-instance v1, Lmiui/widget/DataGroup;

    invoke-direct {v1}, Lmiui/widget/DataGroup;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1b
    monitor-exit p0
    :try_end_1c
    .catchall {:try_start_9 .. :try_end_1c} :catchall_25

    :cond_1c
    iget-object v0, p0, Lmiui/widget/AsyncAdapter;->mDataSet:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/widget/DataGroup;

    return-object v0

    :catchall_25
    move-exception v0

    :try_start_26
    monitor-exit p0
    :try_end_27
    .catchall {:try_start_26 .. :try_end_27} :catchall_25

    throw v0
.end method

.method private loadData(ILmiui/widget/AsyncAdapter$AsyncLoadDataTask;)V
    .registers 5
    .parameter "group"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lmiui/widget/AsyncAdapter",
            "<TT;>.Async",
            "LoadDataTask;",
            ")V"
        }
    .end annotation

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter;,"Lmiui/widget/AsyncAdapter<TT;>;"
    .local p2, task:Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;,"Lmiui/widget/AsyncAdapter<TT;>.AsyncLoadDataTask;"
    if-nez p2, :cond_3

    :cond_2
    :goto_2
    return-void

    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "loadData-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->setId(Ljava/lang/String;)V

    invoke-virtual {p2, p1}, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->setGroup(I)V

    invoke-direct {p0, p2}, Lmiui/widget/AsyncAdapter;->preExecuteTask(Lmiui/os/UniqueAsyncTask;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    :try_start_23
    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {p2, v0}, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_28
    .catch Ljava/lang/IllegalStateException; {:try_start_23 .. :try_end_28} :catch_29

    goto :goto_2

    :catch_29
    move-exception v0

    goto :goto_2
.end method

.method private loadMoreData(ZZILmiui/widget/AsyncAdapter$AsyncLoadMoreDataTask;)V
    .registers 10
    .parameter "upwards"
    .parameter "usingCache"
    .parameter "group"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZI",
            "Lmiui/widget/AsyncAdapter",
            "<TT;>.Async",
            "LoadMoreDataTask;",
            ")V"
        }
    .end annotation

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter;,"Lmiui/widget/AsyncAdapter<TT;>;"
    .local p4, task:Lmiui/widget/AsyncAdapter$AsyncLoadMoreDataTask;,"Lmiui/widget/AsyncAdapter<TT;>.AsyncLoadMoreDataTask;"
    const/4 v4, 0x0

    if-nez p4, :cond_4

    :cond_3
    :goto_3
    return-void

    :cond_4
    new-instance v1, Lmiui/widget/AsyncAdapter$AsyncLoadMoreParams;

    invoke-direct {v1}, Lmiui/widget/AsyncAdapter$AsyncLoadMoreParams;-><init>()V

    .local v1, params:Lmiui/widget/AsyncAdapter$AsyncLoadMoreParams;
    iput-boolean p1, v1, Lmiui/widget/AsyncAdapter$AsyncLoadMoreParams;->upwards:Z

    if-nez p1, :cond_13

    invoke-virtual {p0, p3}, Lmiui/widget/AsyncAdapter;->getDataCount(I)I

    move-result v3

    if-nez v3, :cond_32

    :cond_13
    iput v4, v1, Lmiui/widget/AsyncAdapter$AsyncLoadMoreParams;->cursor:I

    :goto_15
    if-eqz p2, :cond_3d

    invoke-virtual {p0, v1}, Lmiui/widget/AsyncAdapter;->loadCacheData(Lmiui/widget/AsyncAdapter$AsyncLoadMoreParams;)Ljava/util/List;

    move-result-object v2

    .local v2, result:Ljava/util/List;,"Ljava/util/List<TT;>;"
    if-eqz v2, :cond_39

    const/4 v0, 0x0

    .local v0, j:I
    :goto_1e
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_39

    invoke-direct {p0, p3}, Lmiui/widget/AsyncAdapter;->getDataGroup(I)Lmiui/widget/DataGroup;

    move-result-object v3

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Lmiui/widget/DataGroup;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1e

    .end local v0           #j:I
    .end local v2           #result:Ljava/util/List;,"Ljava/util/List<TT;>;"
    :cond_32
    invoke-virtual {p0, p3}, Lmiui/widget/AsyncAdapter;->getDataCount(I)I

    move-result v3

    iput v3, v1, Lmiui/widget/AsyncAdapter$AsyncLoadMoreParams;->cursor:I

    goto :goto_15

    .restart local v2       #result:Ljava/util/List;,"Ljava/util/List<TT;>;"
    :cond_39
    const/4 v3, 0x1

    invoke-virtual {p4, v3}, Lmiui/widget/AsyncAdapter$AsyncLoadMoreDataTask;->setClearData(Z)V

    .end local v2           #result:Ljava/util/List;,"Ljava/util/List<TT;>;"
    :cond_3d
    invoke-virtual {p4, v1}, Lmiui/widget/AsyncAdapter$AsyncLoadMoreDataTask;->setLoadParams(Lmiui/widget/AsyncAdapter$AsyncLoadMoreParams;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "loadMoreData-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p4, v3}, Lmiui/widget/AsyncAdapter$AsyncLoadMoreDataTask;->setId(Ljava/lang/String;)V

    invoke-virtual {p4, p3}, Lmiui/widget/AsyncAdapter$AsyncLoadMoreDataTask;->setGroup(I)V

    invoke-direct {p0, p4}, Lmiui/widget/AsyncAdapter;->preExecuteTask(Lmiui/os/UniqueAsyncTask;)Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x0

    :try_start_60
    new-array v3, v3, [Ljava/lang/Void;

    invoke-virtual {p4, v3}, Lmiui/widget/AsyncAdapter$AsyncLoadMoreDataTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_65
    .catch Ljava/lang/IllegalStateException; {:try_start_60 .. :try_end_65} :catch_66

    goto :goto_3

    :catch_66
    move-exception v3

    goto :goto_3
.end method

.method private postExecuteTask(Lmiui/os/UniqueAsyncTask;)V
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiui/os/UniqueAsyncTask",
            "<***>;)V"
        }
    .end annotation

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter;,"Lmiui/widget/AsyncAdapter<TT;>;"
    .local p1, task:Lmiui/os/UniqueAsyncTask;,"Lmiui/os/UniqueAsyncTask<***>;"
    iget-object v1, p0, Lmiui/widget/AsyncAdapter;->mTaskLocker:[B

    monitor-enter v1

    :try_start_3
    iget-object v0, p0, Lmiui/widget/AsyncAdapter;->mTaskSet:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    monitor-exit v1

    return-void

    :catchall_a
    move-exception v0

    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    throw v0
.end method

.method private preExecuteTask(Lmiui/os/UniqueAsyncTask;)Z
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiui/os/UniqueAsyncTask",
            "<***>;)Z"
        }
    .end annotation

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter;,"Lmiui/widget/AsyncAdapter<TT;>;"
    .local p1, task:Lmiui/os/UniqueAsyncTask;,"Lmiui/os/UniqueAsyncTask<***>;"
    iget-object v1, p0, Lmiui/widget/AsyncAdapter;->mTaskLocker:[B

    monitor-enter v1

    :try_start_3
    iget-object v0, p0, Lmiui/widget/AsyncAdapter;->mTaskSet:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x0

    monitor-exit v1

    :goto_d
    return v0

    :cond_e
    iget-object v0, p0, Lmiui/widget/AsyncAdapter;->mTaskSet:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    monitor-exit v1

    goto :goto_d

    :catchall_16
    move-exception v0

    monitor-exit v1
    :try_end_18
    .catchall {:try_start_3 .. :try_end_18} :catchall_16

    throw v0
.end method


# virtual methods
.method protected abstract bindContentView(Landroid/view/View;Ljava/util/List;III)Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/List",
            "<TT;>;III)",
            "Landroid/view/View;"
        }
    .end annotation
.end method

.method protected abstract bindPartialContentView(Landroid/view/View;Ljava/lang/Object;ILjava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "TT;I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation
.end method

.method protected abstract getCacheKeys(Ljava/lang/Object;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method public getCount()I
    .registers 4

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter;,"Lmiui/widget/AsyncAdapter<TT;>;"
    const/4 v1, 0x0

    .local v1, total:I
    const/4 v0, 0x0

    .local v0, i:I
    :goto_2
    iget-object v2, p0, Lmiui/widget/AsyncAdapter;->mDataSet:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_12

    invoke-virtual {p0, v0}, Lmiui/widget/AsyncAdapter;->getCount(I)I

    move-result v2

    add-int/2addr v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_12
    return v1
.end method

.method protected getCount(I)I
    .registers 5
    .parameter "group"

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter;,"Lmiui/widget/AsyncAdapter<TT;>;"
    invoke-virtual {p0, p1}, Lmiui/widget/AsyncAdapter;->getDataCount(I)I

    move-result v0

    .local v0, total:I
    if-nez v0, :cond_8

    const/4 v1, 0x0

    :goto_7
    return v1

    :cond_8
    add-int/lit8 v1, v0, -0x1

    iget v2, p0, Lmiui/widget/AsyncAdapter;->mDataPerLine:I

    div-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x1

    goto :goto_7
.end method

.method public getDataCount(I)I
    .registers 3
    .parameter "group"

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter;,"Lmiui/widget/AsyncAdapter<TT;>;"
    invoke-direct {p0, p1}, Lmiui/widget/AsyncAdapter;->getDataGroup(I)Lmiui/widget/DataGroup;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/widget/DataGroup;->size()I

    move-result v0

    return v0
.end method

.method public getDataItem(II)Ljava/lang/Object;
    .registers 4
    .parameter "index"
    .parameter "group"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)TT;"
        }
    .end annotation

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter;,"Lmiui/widget/AsyncAdapter<TT;>;"
    invoke-direct {p0, p2}, Lmiui/widget/AsyncAdapter;->getDataGroup(I)Lmiui/widget/DataGroup;

    move-result-object v0

    invoke-virtual {v0, p1}, Lmiui/widget/DataGroup;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getDataPerLine()I
    .registers 2

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter;,"Lmiui/widget/AsyncAdapter<TT;>;"
    iget v0, p0, Lmiui/widget/AsyncAdapter;->mDataPerLine:I

    return v0
.end method

.method public getGroupPosition(I)Landroid/util/Pair;
    .registers 9
    .parameter "position"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter;,"Lmiui/widget/AsyncAdapter<TT;>;"
    const/4 v3, 0x0

    .local v3, total:I
    const/4 v2, 0x0

    .local v2, i:I
    :goto_2
    iget-object v4, p0, Lmiui/widget/AsyncAdapter;->mDataSet:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v2, v4, :cond_26

    invoke-virtual {p0, v2}, Lmiui/widget/AsyncAdapter;->getCount(I)I

    move-result v0

    .local v0, count:I
    add-int v4, v3, v0

    if-ge p1, v4, :cond_22

    sub-int v1, p1, v3

    .local v1, groupPos:I
    new-instance v4, Landroid/util/Pair;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .end local v0           #count:I
    .end local v1           #groupPos:I
    :goto_21
    return-object v4

    .restart local v0       #count:I
    :cond_22
    add-int/2addr v3, v0

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .end local v0           #count:I
    :cond_26
    const/4 v4, 0x0

    goto :goto_21
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter;,"Lmiui/widget/AsyncAdapter<TT;>;"
    invoke-virtual {p0, p1}, Lmiui/widget/AsyncAdapter;->getItem(I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getItem(I)Ljava/util/List;
    .registers 5
    .parameter "position"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter;,"Lmiui/widget/AsyncAdapter<TT;>;"
    invoke-virtual {p0, p1}, Lmiui/widget/AsyncAdapter;->getGroupPosition(I)Landroid/util/Pair;

    move-result-object v0

    .local v0, groupPosition:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v2, v1}, Lmiui/widget/AsyncAdapter;->getItem(II)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method protected getItem(II)Ljava/util/List;
    .registers 9
    .parameter "groupPos"
    .parameter "group"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter;,"Lmiui/widget/AsyncAdapter<TT;>;"
    iget v4, p0, Lmiui/widget/AsyncAdapter;->mDataPerLine:I

    mul-int v2, p1, v4

    .local v2, index:I
    iget v4, p0, Lmiui/widget/AsyncAdapter;->mDataPerLine:I

    invoke-virtual {p0, p2}, Lmiui/widget/AsyncAdapter;->getDataCount(I)I

    move-result v5

    sub-int/2addr v5, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v3

    .local v3, length:I
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .local v0, data:Ljava/util/List;,"Ljava/util/List<TT;>;"
    const/4 v1, 0x0

    .local v1, i:I
    :goto_15
    if-ge v1, v3, :cond_23

    add-int v4, v2, v1

    invoke-virtual {p0, v4, p2}, Lmiui/widget/AsyncAdapter;->getDataItem(II)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_15

    :cond_23
    return-object v0
.end method

.method public getItemId(I)J
    .registers 4
    .parameter "position"

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter;,"Lmiui/widget/AsyncAdapter<TT;>;"
    int-to-long v0, p1

    return-wide v0
.end method

.method protected getLoadDataTask()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lmiui/widget/AsyncAdapter",
            "<TT;>.Async",
            "LoadDataTask;",
            ">;"
        }
    .end annotation

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter;,"Lmiui/widget/AsyncAdapter<TT;>;"
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getLoadMoreDataTask()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lmiui/widget/AsyncAdapter",
            "<TT;>.Async",
            "LoadMoreDataTask;",
            ">;"
        }
    .end annotation

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter;,"Lmiui/widget/AsyncAdapter<TT;>;"
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getLoadPartialDataTask()Lmiui/widget/AsyncAdapter$AsyncLoadPartialDataTask;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lmiui/widget/AsyncAdapter",
            "<TT;>.Async",
            "LoadPartialDataTask;"
        }
    .end annotation

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter;,"Lmiui/widget/AsyncAdapter<TT;>;"
    const/4 v0, 0x0

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 13
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter;,"Lmiui/widget/AsyncAdapter<TT;>;"
    const/4 v8, 0x0

    invoke-virtual {p0}, Lmiui/widget/AsyncAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_9

    const/4 v0, 0x0

    :goto_8
    return-object v0

    :cond_9
    invoke-virtual {p0, p1}, Lmiui/widget/AsyncAdapter;->getGroupPosition(I)Landroid/util/Pair;

    move-result-object v6

    .local v6, groupPosition:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    iget-object v0, v6, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v0, v6, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lmiui/widget/AsyncAdapter;->getItem(II)Ljava/util/List;

    move-result-object v2

    .local v2, bindData:Ljava/util/List;,"Ljava/util/List<TT;>;"
    iget-object v0, v6, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget-object v0, v6, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    move-object v0, p0

    move-object v1, p2

    move v3, p1

    invoke-virtual/range {v0 .. v5}, Lmiui/widget/AsyncAdapter;->bindContentView(Landroid/view/View;Ljava/util/List;III)Landroid/view/View;

    move-result-object p2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v7, v0, -0x1

    .local v7, i:I
    :goto_3e
    if-ltz v7, :cond_4a

    invoke-interface {v2, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, p2, v0, v7}, Lmiui/widget/AsyncAdapter;->loadPartialData(Landroid/view/View;Ljava/lang/Object;I)V

    add-int/lit8 v7, v7, -0x1

    goto :goto_3e

    :cond_4a
    iget-boolean v0, p0, Lmiui/widget/AsyncAdapter;->mReachTop:Z

    if-nez v0, :cond_5c

    iget v0, p0, Lmiui/widget/AsyncAdapter;->mPreloadOffset:I

    if-ne p1, v0, :cond_5c

    iget-boolean v0, p0, Lmiui/widget/AsyncAdapter;->mAutoLoadUpwardsMore:Z

    if-eqz v0, :cond_5c

    const/4 v0, 0x1

    invoke-virtual {p0, v0, v8}, Lmiui/widget/AsyncAdapter;->loadMoreData(ZZ)V

    :cond_5a
    :goto_5a
    move-object v0, p2

    goto :goto_8

    :cond_5c
    iget-boolean v0, p0, Lmiui/widget/AsyncAdapter;->mReachBottom:Z

    if-nez v0, :cond_5a

    invoke-virtual {p0}, Lmiui/widget/AsyncAdapter;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Lmiui/widget/AsyncAdapter;->mPreloadOffset:I

    sub-int/2addr v0, v1

    if-ne p1, v0, :cond_5a

    iget-boolean v0, p0, Lmiui/widget/AsyncAdapter;->mAutoLoadDownwardsMore:Z

    if-eqz v0, :cond_5a

    invoke-virtual {p0, v8, v8}, Lmiui/widget/AsyncAdapter;->loadMoreData(ZZ)V

    goto :goto_5a
.end method

.method protected isValidKey(Ljava/lang/Object;Ljava/lang/Object;I)Z
    .registers 5
    .parameter "key"
    .parameter
    .parameter "index"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "TT;I)Z"
        }
    .end annotation

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter;,"Lmiui/widget/AsyncAdapter<TT;>;"
    .local p2, data:Ljava/lang/Object;,"TT;"
    sget-object v0, Lmiui/widget/AsyncAdapter;->sPartialDataCache:Lmiui/cache/DataCache;

    invoke-virtual {v0, p1}, Lmiui/cache/DataCache;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected loadCacheData(Lmiui/widget/AsyncAdapter$AsyncLoadMoreParams;)Ljava/util/List;
    .registers 3
    .parameter "params"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiui/widget/AsyncAdapter$AsyncLoadMoreParams;",
            ")",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter;,"Lmiui/widget/AsyncAdapter<TT;>;"
    const/4 v0, 0x0

    return-object v0
.end method

.method public loadData()V
    .registers 4

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter;,"Lmiui/widget/AsyncAdapter<TT;>;"
    invoke-virtual {p0}, Lmiui/widget/AsyncAdapter;->getLoadDataTask()Ljava/util/List;

    move-result-object v1

    .local v1, tasks:Ljava/util/List;,"Ljava/util/List<Lmiui/widget/AsyncAdapter<TT;>.AsyncLoadDataTask;>;"
    if-nez v1, :cond_7

    :cond_6
    return-void

    :cond_7
    const/4 v0, 0x0

    .local v0, i:I
    :goto_8
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_6

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;

    invoke-direct {p0, v0, v2}, Lmiui/widget/AsyncAdapter;->loadData(ILmiui/widget/AsyncAdapter$AsyncLoadDataTask;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_8
.end method

.method public loadData(I)V
    .registers 4
    .parameter "group"

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter;,"Lmiui/widget/AsyncAdapter<TT;>;"
    invoke-virtual {p0}, Lmiui/widget/AsyncAdapter;->getLoadDataTask()Ljava/util/List;

    move-result-object v0

    .local v0, tasks:Ljava/util/List;,"Ljava/util/List<Lmiui/widget/AsyncAdapter<TT;>.AsyncLoadDataTask;>;"
    if-eqz v0, :cond_c

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-gt v1, p1, :cond_d

    :cond_c
    :goto_c
    return-void

    :cond_d
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;

    invoke-direct {p0, p1, v1}, Lmiui/widget/AsyncAdapter;->loadData(ILmiui/widget/AsyncAdapter$AsyncLoadDataTask;)V

    goto :goto_c
.end method

.method public loadMoreData(ZZ)V
    .registers 6
    .parameter "upwards"
    .parameter "usingCache"

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter;,"Lmiui/widget/AsyncAdapter<TT;>;"
    invoke-virtual {p0}, Lmiui/widget/AsyncAdapter;->getLoadMoreDataTask()Ljava/util/List;

    move-result-object v1

    .local v1, tasks:Ljava/util/List;,"Ljava/util/List<Lmiui/widget/AsyncAdapter<TT;>.AsyncLoadMoreDataTask;>;"
    if-nez v1, :cond_7

    :cond_6
    return-void

    :cond_7
    const/4 v0, 0x0

    .local v0, i:I
    :goto_8
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_6

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/widget/AsyncAdapter$AsyncLoadMoreDataTask;

    invoke-direct {p0, p1, p2, v0, v2}, Lmiui/widget/AsyncAdapter;->loadMoreData(ZZILmiui/widget/AsyncAdapter$AsyncLoadMoreDataTask;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_8
.end method

.method public loadMoreData(ZZI)V
    .registers 6
    .parameter "upwards"
    .parameter "usingCache"
    .parameter "group"

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter;,"Lmiui/widget/AsyncAdapter<TT;>;"
    invoke-virtual {p0}, Lmiui/widget/AsyncAdapter;->getLoadMoreDataTask()Ljava/util/List;

    move-result-object v0

    .local v0, tasks:Ljava/util/List;,"Ljava/util/List<Lmiui/widget/AsyncAdapter<TT;>.AsyncLoadMoreDataTask;>;"
    if-eqz v0, :cond_c

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-gt v1, p3, :cond_d

    :cond_c
    :goto_c
    return-void

    :cond_d
    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/widget/AsyncAdapter$AsyncLoadMoreDataTask;

    invoke-direct {p0, p1, p2, p3, v1}, Lmiui/widget/AsyncAdapter;->loadMoreData(ZZILmiui/widget/AsyncAdapter$AsyncLoadMoreDataTask;)V

    goto :goto_c
.end method

.method protected loadPartialData(Landroid/view/View;Ljava/lang/Object;I)V
    .registers 10
    .parameter "view"
    .parameter
    .parameter "offset"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "TT;I)V"
        }
    .end annotation

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter;,"Lmiui/widget/AsyncAdapter<TT;>;"
    .local p2, data:Ljava/lang/Object;,"TT;"
    iget-object v4, p0, Lmiui/widget/AsyncAdapter;->mLoadPartialDataTask:Lmiui/widget/AsyncAdapter$AsyncLoadPartialDataTask;

    if-eqz v4, :cond_e

    iget-object v4, p0, Lmiui/widget/AsyncAdapter;->mLoadPartialDataTask:Lmiui/widget/AsyncAdapter$AsyncLoadPartialDataTask;

    invoke-virtual {v4}, Lmiui/widget/AsyncAdapter$AsyncLoadPartialDataTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v4

    sget-object v5, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-ne v4, v5, :cond_19

    :cond_e
    invoke-virtual {p0}, Lmiui/widget/AsyncAdapter;->getLoadPartialDataTask()Lmiui/widget/AsyncAdapter$AsyncLoadPartialDataTask;

    move-result-object v4

    iput-object v4, p0, Lmiui/widget/AsyncAdapter;->mLoadPartialDataTask:Lmiui/widget/AsyncAdapter$AsyncLoadPartialDataTask;

    iget-object v4, p0, Lmiui/widget/AsyncAdapter;->mLoadPartialDataTask:Lmiui/widget/AsyncAdapter$AsyncLoadPartialDataTask;

    if-nez v4, :cond_19

    :goto_18
    return-void

    :cond_19
    iget-object v4, p0, Lmiui/widget/AsyncAdapter;->mLoadPartialDataTask:Lmiui/widget/AsyncAdapter$AsyncLoadPartialDataTask;

    invoke-virtual {v4}, Lmiui/widget/AsyncAdapter$AsyncLoadPartialDataTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v4

    sget-object v5, Landroid/os/AsyncTask$Status;->PENDING:Landroid/os/AsyncTask$Status;

    if-ne v4, v5, :cond_2b

    :try_start_23
    iget-object v4, p0, Lmiui/widget/AsyncAdapter;->mLoadPartialDataTask:Lmiui/widget/AsyncAdapter$AsyncLoadPartialDataTask;

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Void;

    invoke-virtual {v4, v5}, Lmiui/widget/AsyncAdapter$AsyncLoadPartialDataTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_2b
    .catch Ljava/lang/IllegalStateException; {:try_start_23 .. :try_end_2b} :catch_63

    :cond_2b
    :goto_2b
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .local v3, partialData:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Object;>;"
    invoke-virtual {p0, p2}, Lmiui/widget/AsyncAdapter;->getCacheKeys(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .local v2, keys:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Object;>;"
    const/4 v0, 0x0

    .local v0, i:I
    :goto_35
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_5f

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    .local v1, key:Ljava/lang/Object;
    invoke-virtual {p0, v1, p2, v0}, Lmiui/widget/AsyncAdapter;->isValidKey(Ljava/lang/Object;Ljava/lang/Object;I)Z

    move-result v4

    if-eqz v4, :cond_51

    sget-object v4, Lmiui/widget/AsyncAdapter;->sPartialDataCache:Lmiui/cache/DataCache;

    invoke-virtual {v4, v1}, Lmiui/cache/DataCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4e
    :goto_4e
    add-int/lit8 v0, v0, 0x1

    goto :goto_35

    :cond_51
    iget-object v4, p0, Lmiui/widget/AsyncAdapter;->mLoadPartialDataTask:Lmiui/widget/AsyncAdapter$AsyncLoadPartialDataTask;

    invoke-virtual {v4, v1}, Lmiui/widget/AsyncAdapter$AsyncLoadPartialDataTask;->containJob(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4e

    iget-object v4, p0, Lmiui/widget/AsyncAdapter;->mLoadPartialDataTask:Lmiui/widget/AsyncAdapter$AsyncLoadPartialDataTask;

    invoke-virtual {v4, v1}, Lmiui/widget/AsyncAdapter$AsyncLoadPartialDataTask;->addJob(Ljava/lang/Object;)V

    goto :goto_4e

    .end local v1           #key:Ljava/lang/Object;
    :cond_5f
    invoke-virtual {p0, p1, p2, p3, v3}, Lmiui/widget/AsyncAdapter;->bindPartialContentView(Landroid/view/View;Ljava/lang/Object;ILjava/util/List;)V

    goto :goto_18

    .end local v0           #i:I
    .end local v2           #keys:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Object;>;"
    .end local v3           #partialData:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Object;>;"
    :catch_63
    move-exception v4

    goto :goto_2b
.end method

.method public loadingData()Z
    .registers 2

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter;,"Lmiui/widget/AsyncAdapter<TT;>;"
    iget-object v0, p0, Lmiui/widget/AsyncAdapter;->mTaskSet:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public onStop()V
    .registers 2

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter;,"Lmiui/widget/AsyncAdapter<TT;>;"
    iget-object v0, p0, Lmiui/widget/AsyncAdapter;->mLoadPartialDataTask:Lmiui/widget/AsyncAdapter$AsyncLoadPartialDataTask;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lmiui/widget/AsyncAdapter;->mLoadPartialDataTask:Lmiui/widget/AsyncAdapter$AsyncLoadPartialDataTask;

    invoke-virtual {v0}, Lmiui/widget/AsyncAdapter$AsyncLoadPartialDataTask;->stop()V

    :cond_9
    return-void
.end method

.method protected postLoadData(Ljava/util/List;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter;,"Lmiui/widget/AsyncAdapter<TT;>;"
    .local p1, result:Ljava/util/List;,"Ljava/util/List<TT;>;"
    return-void
.end method

.method protected postLoadMoreData(Ljava/util/List;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter;,"Lmiui/widget/AsyncAdapter<TT;>;"
    .local p1, result:Ljava/util/List;,"Ljava/util/List<TT;>;"
    return-void
.end method

.method protected postLoadPartialData(Ljava/util/List;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter;,"Lmiui/widget/AsyncAdapter<TT;>;"
    .local p1, result:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Object;>;"
    return-void
.end method

.method public setAutoLoadMoreStyle(I)V
    .registers 5
    .parameter "autoLoadMoreStyle"

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter;,"Lmiui/widget/AsyncAdapter<TT;>;"
    const/4 v1, 0x1

    const/4 v2, 0x0

    and-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_10

    move v0, v1

    :goto_7
    iput-boolean v0, p0, Lmiui/widget/AsyncAdapter;->mAutoLoadUpwardsMore:Z

    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_12

    :goto_d
    iput-boolean v1, p0, Lmiui/widget/AsyncAdapter;->mAutoLoadDownwardsMore:Z

    return-void

    :cond_10
    move v0, v2

    goto :goto_7

    :cond_12
    move v1, v2

    goto :goto_d
.end method

.method public setDataPerLine(I)V
    .registers 2
    .parameter "dataPerLine"

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter;,"Lmiui/widget/AsyncAdapter<TT;>;"
    iput p1, p0, Lmiui/widget/AsyncAdapter;->mDataPerLine:I

    return-void
.end method

.method public setDataSet(Ljava/util/List;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lmiui/widget/DataGroup",
            "<TT;>;>;)V"
        }
    .end annotation

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter;,"Lmiui/widget/AsyncAdapter<TT;>;"
    .local p1, dataSet:Ljava/util/List;,"Ljava/util/List<Lmiui/widget/DataGroup<TT;>;>;"
    iput-object p1, p0, Lmiui/widget/AsyncAdapter;->mDataSet:Ljava/util/List;

    invoke-virtual {p0}, Lmiui/widget/AsyncAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public setForceToLoadData(Z)V
    .registers 2
    .parameter "isForceToLoadData"

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter;,"Lmiui/widget/AsyncAdapter<TT;>;"
    iput-boolean p1, p0, Lmiui/widget/AsyncAdapter;->mForceToLoadData:Z

    return-void
.end method

.method public setLoadUsingCache(Z)V
    .registers 2
    .parameter "usingCache"

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter;,"Lmiui/widget/AsyncAdapter<TT;>;"
    iput-boolean p1, p0, Lmiui/widget/AsyncAdapter;->mLoadUsingCache:Z

    return-void
.end method

.method public setPreloadOffset(I)V
    .registers 2
    .parameter "preloadOffset"

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter;,"Lmiui/widget/AsyncAdapter<TT;>;"
    iput p1, p0, Lmiui/widget/AsyncAdapter;->mPreloadOffset:I

    return-void
.end method

.method public stopAllAsynLoadTask()V
    .registers 5

    .prologue
    .local p0, this:Lmiui/widget/AsyncAdapter;,"Lmiui/widget/AsyncAdapter<TT;>;"
    invoke-virtual {p0}, Lmiui/widget/AsyncAdapter;->onStop()V

    iget-object v3, p0, Lmiui/widget/AsyncAdapter;->mTaskLocker:[B

    monitor-enter v3

    :try_start_6
    iget-object v2, p0, Lmiui/widget/AsyncAdapter;->mTaskSet:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_c
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_20

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/os/UniqueAsyncTask;

    .local v1, task:Lmiui/os/ObservableAsyncTask;,"Lmiui/os/ObservableAsyncTask<***>;"
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lmiui/os/UniqueAsyncTask;->cancel(Z)Z

    goto :goto_c

    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #task:Lmiui/os/ObservableAsyncTask;,"Lmiui/os/ObservableAsyncTask<***>;"
    :catchall_1d
    move-exception v2

    monitor-exit v3
    :try_end_1f
    .catchall {:try_start_6 .. :try_end_1f} :catchall_1d

    throw v2

    .restart local v0       #i$:Ljava/util/Iterator;
    :cond_20
    :try_start_20
    iget-object v2, p0, Lmiui/widget/AsyncAdapter;->mTaskSet:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    monitor-exit v3
    :try_end_26
    .catchall {:try_start_20 .. :try_end_26} :catchall_1d

    return-void
.end method
