.class Lmiui/widget/MiCloudAdvancedSettingsBase$CheckPhoneTask;
.super Landroid/os/AsyncTask;
.source "MiCloudAdvancedSettingsBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/widget/MiCloudAdvancedSettingsBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CheckPhoneTask"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/widget/MiCloudAdvancedSettingsBase$CheckPhoneTask$CheckResult;,
        Lmiui/widget/MiCloudAdvancedSettingsBase$CheckPhoneTask$CheckPhoneCallback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lmiui/widget/MiCloudAdvancedSettingsBase$CheckPhoneTask$CheckResult;",
        ">;"
    }
.end annotation


# instance fields
.field private final mAccount:Landroid/accounts/Account;

.field private final mActivity:Landroid/app/Activity;

.field private final mAuthority:Ljava/lang/String;

.field private final mCheckCallback:Lmiui/widget/MiCloudAdvancedSettingsBase$CheckPhoneTask$CheckPhoneCallback;

.field private mProgressDialog:Lmiui/widget/SimpleDialogFragment;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/accounts/Account;Ljava/lang/String;Lmiui/widget/MiCloudAdvancedSettingsBase$CheckPhoneTask$CheckPhoneCallback;)V
    .registers 5
    .parameter "activity"
    .parameter "account"
    .parameter "authority"
    .parameter "checkResultCallable"

    .prologue
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p1, p0, Lmiui/widget/MiCloudAdvancedSettingsBase$CheckPhoneTask;->mActivity:Landroid/app/Activity;

    iput-object p2, p0, Lmiui/widget/MiCloudAdvancedSettingsBase$CheckPhoneTask;->mAccount:Landroid/accounts/Account;

    iput-object p3, p0, Lmiui/widget/MiCloudAdvancedSettingsBase$CheckPhoneTask;->mAuthority:Ljava/lang/String;

    iput-object p4, p0, Lmiui/widget/MiCloudAdvancedSettingsBase$CheckPhoneTask;->mCheckCallback:Lmiui/widget/MiCloudAdvancedSettingsBase$CheckPhoneTask$CheckPhoneCallback;

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    .prologue
    check-cast p1, [Ljava/lang/Void;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/widget/MiCloudAdvancedSettingsBase$CheckPhoneTask;->doInBackground([Ljava/lang/Void;)Lmiui/widget/MiCloudAdvancedSettingsBase$CheckPhoneTask$CheckResult;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Lmiui/widget/MiCloudAdvancedSettingsBase$CheckPhoneTask$CheckResult;
    .registers 10
    .parameter "params"

    .prologue
    iget-object v6, p0, Lmiui/widget/MiCloudAdvancedSettingsBase$CheckPhoneTask;->mActivity:Landroid/app/Activity;

    const-string v7, "phone"

    invoke-virtual {v6, v7}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/telephony/TelephonyManager;

    .local v5, tm:Landroid/telephony/TelephonyManager;
    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    move-result-object v3

    .local v3, imsi:Ljava/lang/String;
    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    .local v0, deviceId:Ljava/lang/String;
    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getSimSerialNumber()Ljava/lang/String;

    move-result-object v2

    .local v2, iccId:Ljava/lang/String;
    :try_start_16
    #calls: Lmiui/widget/MiCloudAdvancedSettingsBase;->queryPhone(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0, v2}, Lmiui/widget/MiCloudAdvancedSettingsBase;->access$800(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .local v4, phone:Ljava/lang/String;
    if-nez v4, :cond_20

    #calls: Lmiui/widget/MiCloudAdvancedSettingsBase;->queryPhone(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0, v3}, Lmiui/widget/MiCloudAdvancedSettingsBase;->access$800(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1f
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_1f} :catch_25
    .catch Lmiui/net/exception/InvalidResponseException; {:try_start_16 .. :try_end_1f} :catch_29

    move-result-object v4

    :cond_20
    if-eqz v4, :cond_2d

    sget-object v6, Lmiui/widget/MiCloudAdvancedSettingsBase$CheckPhoneTask$CheckResult;->RESULT_OK:Lmiui/widget/MiCloudAdvancedSettingsBase$CheckPhoneTask$CheckResult;

    .end local v4           #phone:Ljava/lang/String;
    :goto_24
    return-object v6

    :catch_25
    move-exception v1

    .local v1, e:Ljava/io/IOException;
    sget-object v6, Lmiui/widget/MiCloudAdvancedSettingsBase$CheckPhoneTask$CheckResult;->RESULT_IO_ERROR:Lmiui/widget/MiCloudAdvancedSettingsBase$CheckPhoneTask$CheckResult;

    goto :goto_24

    .end local v1           #e:Ljava/io/IOException;
    :catch_29
    move-exception v1

    .local v1, e:Lmiui/net/exception/InvalidResponseException;
    sget-object v6, Lmiui/widget/MiCloudAdvancedSettingsBase$CheckPhoneTask$CheckResult;->RESULT_NOT_ACTIVATED:Lmiui/widget/MiCloudAdvancedSettingsBase$CheckPhoneTask$CheckResult;

    goto :goto_24

    .end local v1           #e:Lmiui/net/exception/InvalidResponseException;
    .restart local v4       #phone:Ljava/lang/String;
    :cond_2d
    sget-object v6, Lmiui/widget/MiCloudAdvancedSettingsBase$CheckPhoneTask$CheckResult;->RESULT_NOT_ACTIVATED:Lmiui/widget/MiCloudAdvancedSettingsBase$CheckPhoneTask$CheckResult;

    goto :goto_24
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 2
    .parameter "x0"

    .prologue
    check-cast p1, Lmiui/widget/MiCloudAdvancedSettingsBase$CheckPhoneTask$CheckResult;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/widget/MiCloudAdvancedSettingsBase$CheckPhoneTask;->onPostExecute(Lmiui/widget/MiCloudAdvancedSettingsBase$CheckPhoneTask$CheckResult;)V

    return-void
.end method

.method protected onPostExecute(Lmiui/widget/MiCloudAdvancedSettingsBase$CheckPhoneTask$CheckResult;)V
    .registers 5
    .parameter "result"

    .prologue
    iget-object v0, p0, Lmiui/widget/MiCloudAdvancedSettingsBase$CheckPhoneTask;->mProgressDialog:Lmiui/widget/SimpleDialogFragment;

    invoke-virtual {v0}, Lmiui/widget/SimpleDialogFragment;->dismiss()V

    iget-object v0, p0, Lmiui/widget/MiCloudAdvancedSettingsBase$CheckPhoneTask;->mCheckCallback:Lmiui/widget/MiCloudAdvancedSettingsBase$CheckPhoneTask$CheckPhoneCallback;

    if-eqz v0, :cond_12

    iget-object v0, p0, Lmiui/widget/MiCloudAdvancedSettingsBase$CheckPhoneTask;->mCheckCallback:Lmiui/widget/MiCloudAdvancedSettingsBase$CheckPhoneTask$CheckPhoneCallback;

    iget-object v1, p0, Lmiui/widget/MiCloudAdvancedSettingsBase$CheckPhoneTask;->mAccount:Landroid/accounts/Account;

    iget-object v2, p0, Lmiui/widget/MiCloudAdvancedSettingsBase$CheckPhoneTask;->mAuthority:Ljava/lang/String;

    invoke-interface {v0, v1, v2, p1}, Lmiui/widget/MiCloudAdvancedSettingsBase$CheckPhoneTask$CheckPhoneCallback;->onCheckPhoneResult(Landroid/accounts/Account;Ljava/lang/String;Lmiui/widget/MiCloudAdvancedSettingsBase$CheckPhoneTask$CheckResult;)V

    :cond_12
    return-void
.end method

.method protected onPreExecute()V
    .registers 4

    .prologue
    new-instance v0, Lmiui/widget/SimpleDialogFragment$AlertDialogFragmentBuilder;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lmiui/widget/SimpleDialogFragment$AlertDialogFragmentBuilder;-><init>(I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmiui/widget/SimpleDialogFragment$AlertDialogFragmentBuilder;->setCancelable(Z)Lmiui/widget/SimpleDialogFragment$AlertDialogFragmentBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/widget/SimpleDialogFragment$AlertDialogFragmentBuilder;->create()Lmiui/widget/SimpleDialogFragment;

    move-result-object v0

    iput-object v0, p0, Lmiui/widget/MiCloudAdvancedSettingsBase$CheckPhoneTask;->mProgressDialog:Lmiui/widget/SimpleDialogFragment;

    iget-object v0, p0, Lmiui/widget/MiCloudAdvancedSettingsBase$CheckPhoneTask;->mProgressDialog:Lmiui/widget/SimpleDialogFragment;

    iget-object v1, p0, Lmiui/widget/MiCloudAdvancedSettingsBase$CheckPhoneTask;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "CheckPhoneProgress"

    invoke-virtual {v0, v1, v2}, Lmiui/widget/SimpleDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method
