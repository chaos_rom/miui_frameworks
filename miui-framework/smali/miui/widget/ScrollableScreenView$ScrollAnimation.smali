.class Lmiui/widget/ScrollableScreenView$ScrollAnimation;
.super Landroid/view/animation/Animation;
.source "ScrollableScreenView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/widget/ScrollableScreenView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ScrollAnimation"
.end annotation


# instance fields
.field private mFromY:I

.field private mToY:I

.field final synthetic this$0:Lmiui/widget/ScrollableScreenView;


# direct methods
.method public constructor <init>(Lmiui/widget/ScrollableScreenView;II)V
    .registers 6
    .parameter
    .parameter "fromY"
    .parameter "toY"

    .prologue
    const/4 v0, 0x0

    iput-object p1, p0, Lmiui/widget/ScrollableScreenView$ScrollAnimation;->this$0:Lmiui/widget/ScrollableScreenView;

    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    iput v0, p0, Lmiui/widget/ScrollableScreenView$ScrollAnimation;->mFromY:I

    iput v0, p0, Lmiui/widget/ScrollableScreenView$ScrollAnimation;->mToY:I

    iput p2, p0, Lmiui/widget/ScrollableScreenView$ScrollAnimation;->mFromY:I

    iput p3, p0, Lmiui/widget/ScrollableScreenView$ScrollAnimation;->mToY:I

    const-wide/16 v0, 0x1f4

    invoke-virtual {p0, v0, v1}, Lmiui/widget/ScrollableScreenView$ScrollAnimation;->setDuration(J)V

    return-void
.end method


# virtual methods
.method protected applyTransformation(FLandroid/view/animation/Transformation;)V
    .registers 7
    .parameter "interpolatedTime"
    .parameter "t"

    .prologue
    iget v1, p0, Lmiui/widget/ScrollableScreenView$ScrollAnimation;->mToY:I

    iget v2, p0, Lmiui/widget/ScrollableScreenView$ScrollAnimation;->mFromY:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v0, v1

    .local v0, offset:I
    iget-object v1, p0, Lmiui/widget/ScrollableScreenView$ScrollAnimation;->this$0:Lmiui/widget/ScrollableScreenView;

    #getter for: Lmiui/widget/ScrollableScreenView;->mScrollView:Landroid/widget/ScrollView;
    invoke-static {v1}, Lmiui/widget/ScrollableScreenView;->access$000(Lmiui/widget/ScrollableScreenView;)Landroid/widget/ScrollView;

    move-result-object v1

    iget-object v2, p0, Lmiui/widget/ScrollableScreenView$ScrollAnimation;->this$0:Lmiui/widget/ScrollableScreenView;

    #getter for: Lmiui/widget/ScrollableScreenView;->mScrollView:Landroid/widget/ScrollView;
    invoke-static {v2}, Lmiui/widget/ScrollableScreenView;->access$000(Lmiui/widget/ScrollableScreenView;)Landroid/widget/ScrollView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ScrollView;->getScrollX()I

    move-result v2

    iget v3, p0, Lmiui/widget/ScrollableScreenView$ScrollAnimation;->mFromY:I

    add-int/2addr v3, v0

    invoke-virtual {v1, v2, v3}, Landroid/widget/ScrollView;->scrollTo(II)V

    return-void
.end method
