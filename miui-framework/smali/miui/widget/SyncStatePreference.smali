.class public Lmiui/widget/SyncStatePreference;
.super Landroid/preference/CheckBoxPreference;
.source "SyncStatePreference.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SyncStatePreference"


# instance fields
.field private mAccount:Landroid/accounts/Account;

.field private mActivating:Z

.field private mAuthority:Ljava/lang/String;

.field private mFailed:Z

.field private mIsActive:Z

.field private mIsPending:Z

.field private mNoSim:Z

.field private mOneTimeSyncMode:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/accounts/Account;Ljava/lang/String;)V
    .registers 6
    .parameter "context"
    .parameter "account"
    .parameter "authority"

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-boolean v1, p0, Lmiui/widget/SyncStatePreference;->mIsActive:Z

    iput-boolean v1, p0, Lmiui/widget/SyncStatePreference;->mIsPending:Z

    iput-boolean v1, p0, Lmiui/widget/SyncStatePreference;->mFailed:Z

    iput-boolean v1, p0, Lmiui/widget/SyncStatePreference;->mNoSim:Z

    iput-boolean v1, p0, Lmiui/widget/SyncStatePreference;->mActivating:Z

    iput-boolean v1, p0, Lmiui/widget/SyncStatePreference;->mOneTimeSyncMode:Z

    iput-object p2, p0, Lmiui/widget/SyncStatePreference;->mAccount:Landroid/accounts/Account;

    iput-object p3, p0, Lmiui/widget/SyncStatePreference;->mAuthority:Ljava/lang/String;

    const v0, 0x603003d

    invoke-virtual {p0, v0}, Lmiui/widget/SyncStatePreference;->setLayoutResource(I)V

    const v0, 0x603003c

    invoke-virtual {p0, v0}, Lmiui/widget/SyncStatePreference;->setWidgetLayoutResource(I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 5
    .parameter "context"
    .parameter "attrs"

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-boolean v0, p0, Lmiui/widget/SyncStatePreference;->mIsActive:Z

    iput-boolean v0, p0, Lmiui/widget/SyncStatePreference;->mIsPending:Z

    iput-boolean v0, p0, Lmiui/widget/SyncStatePreference;->mFailed:Z

    iput-boolean v0, p0, Lmiui/widget/SyncStatePreference;->mNoSim:Z

    iput-boolean v0, p0, Lmiui/widget/SyncStatePreference;->mActivating:Z

    iput-boolean v0, p0, Lmiui/widget/SyncStatePreference;->mOneTimeSyncMode:Z

    const v0, 0x603003d

    invoke-virtual {p0, v0}, Lmiui/widget/SyncStatePreference;->setLayoutResource(I)V

    const v0, 0x603003c

    invoke-virtual {p0, v0}, Lmiui/widget/SyncStatePreference;->setWidgetLayoutResource(I)V

    iput-object v1, p0, Lmiui/widget/SyncStatePreference;->mAccount:Landroid/accounts/Account;

    iput-object v1, p0, Lmiui/widget/SyncStatePreference;->mAuthority:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getAccount()Landroid/accounts/Account;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/widget/SyncStatePreference;->mAccount:Landroid/accounts/Account;

    return-object v0
.end method

.method public getAuthority()Ljava/lang/String;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/widget/SyncStatePreference;->mAuthority:Ljava/lang/String;

    return-object v0
.end method

.method public isOneTimeSyncMode()Z
    .registers 2

    .prologue
    iget-boolean v0, p0, Lmiui/widget/SyncStatePreference;->mOneTimeSyncMode:Z

    return v0
.end method

.method public onBindView(Landroid/view/View;)V
    .registers 16
    .parameter "view"

    .prologue
    const v13, 0x1020010

    const v12, 0x1020001

    const/16 v9, 0x8

    const/4 v10, 0x1

    const/4 v8, 0x0

    invoke-super {p0, p1}, Landroid/preference/CheckBoxPreference;->onBindView(Landroid/view/View;)V

    const v7, 0x60b0081

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lmiui/widget/AnimatedImageView;

    .local v5, syncActiveView:Lmiui/widget/AnimatedImageView;
    const v7, 0x60b0082

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .local v6, syncFailedView:Landroid/view/View;
    iget-boolean v7, p0, Lmiui/widget/SyncStatePreference;->mIsActive:Z

    if-nez v7, :cond_25

    iget-boolean v7, p0, Lmiui/widget/SyncStatePreference;->mIsPending:Z

    if-eqz v7, :cond_99

    :cond_25
    move v0, v10

    .local v0, activeVisible:Z
    :goto_26
    if-eqz v0, :cond_9b

    move v7, v8

    :goto_29
    invoke-virtual {v5, v7}, Lmiui/widget/AnimatedImageView;->setVisibility(I)V

    iget-boolean v7, p0, Lmiui/widget/SyncStatePreference;->mIsActive:Z

    invoke-virtual {v5, v7}, Lmiui/widget/AnimatedImageView;->setAnimating(Z)V

    iget-boolean v7, p0, Lmiui/widget/SyncStatePreference;->mFailed:Z

    if-eqz v7, :cond_9d

    if-nez v0, :cond_9d

    move v3, v10

    .local v3, failedVisible:Z
    :goto_38
    if-eqz v3, :cond_9f

    move v7, v8

    :goto_3b
    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p1, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .local v1, checkBox:Landroid/view/View;
    iget-boolean v7, p0, Lmiui/widget/SyncStatePreference;->mOneTimeSyncMode:Z

    if-eqz v7, :cond_a1

    invoke-virtual {v1, v9}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p1, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .local v4, summary:Landroid/widget/TextView;
    invoke-virtual {p0}, Lmiui/widget/SyncStatePreference;->getContext()Landroid/content/Context;

    move-result-object v7

    const v9, 0x60c01db

    new-array v10, v10, [Ljava/lang/Object;

    invoke-virtual {p0}, Lmiui/widget/SyncStatePreference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v11

    aput-object v11, v10, v8

    invoke-virtual {v7, v9, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .end local v4           #summary:Landroid/widget/TextView;
    :goto_65
    iget-boolean v7, p0, Lmiui/widget/SyncStatePreference;->mNoSim:Z

    if-nez v7, :cond_6d

    iget-boolean v7, p0, Lmiui/widget/SyncStatePreference;->mActivating:Z

    if-eqz v7, :cond_85

    :cond_6d
    invoke-virtual {p1, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .restart local v4       #summary:Landroid/widget/TextView;
    iget-boolean v7, p0, Lmiui/widget/SyncStatePreference;->mNoSim:Z

    if-eqz v7, :cond_a5

    invoke-virtual {p0}, Lmiui/widget/SyncStatePreference;->getContext()Landroid/content/Context;

    move-result-object v7

    const v8, 0x60c01dc

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .end local v4           #summary:Landroid/widget/TextView;
    :cond_85
    :goto_85
    invoke-virtual {p1, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .local v2, checkableView:Landroid/view/View;
    if-eqz v2, :cond_98

    instance-of v7, v2, Landroid/widget/Checkable;

    if-eqz v7, :cond_98

    check-cast v2, Landroid/widget/Checkable;

    .end local v2           #checkableView:Landroid/view/View;
    invoke-virtual {p0}, Lmiui/widget/SyncStatePreference;->isChecked()Z

    move-result v7

    invoke-interface {v2, v7}, Landroid/widget/Checkable;->setChecked(Z)V

    :cond_98
    return-void

    .end local v0           #activeVisible:Z
    .end local v1           #checkBox:Landroid/view/View;
    .end local v3           #failedVisible:Z
    :cond_99
    move v0, v8

    goto :goto_26

    .restart local v0       #activeVisible:Z
    :cond_9b
    move v7, v9

    goto :goto_29

    :cond_9d
    move v3, v8

    goto :goto_38

    .restart local v3       #failedVisible:Z
    :cond_9f
    move v7, v9

    goto :goto_3b

    .restart local v1       #checkBox:Landroid/view/View;
    :cond_a1
    invoke-virtual {v1, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_65

    .restart local v4       #summary:Landroid/widget/TextView;
    :cond_a5
    iget-boolean v7, p0, Lmiui/widget/SyncStatePreference;->mActivating:Z

    if-eqz v7, :cond_85

    invoke-virtual {p0}, Lmiui/widget/SyncStatePreference;->getContext()Landroid/content/Context;

    move-result-object v7

    const v8, 0x60c01dd

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_85
.end method

.method protected onClick()V
    .registers 2

    .prologue
    iget-boolean v0, p0, Lmiui/widget/SyncStatePreference;->mOneTimeSyncMode:Z

    if-nez v0, :cond_7

    invoke-super {p0}, Landroid/preference/CheckBoxPreference;->onClick()V

    :cond_7
    return-void
.end method

.method public setAccount(Landroid/accounts/Account;)V
    .registers 2
    .parameter "account"

    .prologue
    iput-object p1, p0, Lmiui/widget/SyncStatePreference;->mAccount:Landroid/accounts/Account;

    return-void
.end method

.method public setActivating(Z)V
    .registers 2
    .parameter "activating"

    .prologue
    iput-boolean p1, p0, Lmiui/widget/SyncStatePreference;->mActivating:Z

    return-void
.end method

.method public setActive(Z)V
    .registers 2
    .parameter "isActive"

    .prologue
    iput-boolean p1, p0, Lmiui/widget/SyncStatePreference;->mIsActive:Z

    invoke-virtual {p0}, Lmiui/widget/SyncStatePreference;->notifyChanged()V

    return-void
.end method

.method public setAuthority(Ljava/lang/String;)V
    .registers 2
    .parameter "authority"

    .prologue
    iput-object p1, p0, Lmiui/widget/SyncStatePreference;->mAuthority:Ljava/lang/String;

    return-void
.end method

.method public setFailed(Z)V
    .registers 2
    .parameter "failed"

    .prologue
    iput-boolean p1, p0, Lmiui/widget/SyncStatePreference;->mFailed:Z

    invoke-virtual {p0}, Lmiui/widget/SyncStatePreference;->notifyChanged()V

    return-void
.end method

.method public setNoSim(Z)V
    .registers 2
    .parameter "noSim"

    .prologue
    iput-boolean p1, p0, Lmiui/widget/SyncStatePreference;->mNoSim:Z

    return-void
.end method

.method public setOneTimeSyncMode(Z)V
    .registers 2
    .parameter "oneTimeSyncMode"

    .prologue
    iput-boolean p1, p0, Lmiui/widget/SyncStatePreference;->mOneTimeSyncMode:Z

    invoke-virtual {p0}, Lmiui/widget/SyncStatePreference;->notifyChanged()V

    return-void
.end method

.method public setPending(Z)V
    .registers 2
    .parameter "isPending"

    .prologue
    iput-boolean p1, p0, Lmiui/widget/SyncStatePreference;->mIsPending:Z

    invoke-virtual {p0}, Lmiui/widget/SyncStatePreference;->notifyChanged()V

    return-void
.end method
