.class Lmiui/widget/DateTimePicker$2;
.super Ljava/lang/Object;
.source "DateTimePicker.java"

# interfaces
.implements Landroid/widget/NumberPicker$OnValueChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/widget/DateTimePicker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/widget/DateTimePicker;


# direct methods
.method constructor <init>(Lmiui/widget/DateTimePicker;)V
    .registers 2
    .parameter

    .prologue
    iput-object p1, p0, Lmiui/widget/DateTimePicker$2;->this$0:Lmiui/widget/DateTimePicker;

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onValueChange(Landroid/widget/NumberPicker;II)V
    .registers 15
    .parameter "picker"
    .parameter "oldVal"
    .parameter "newVal"

    .prologue
    const/4 v10, -0x1

    const/4 v9, 0x6

    const/4 v4, 0x1

    const/16 v5, 0xc

    const/16 v8, 0xb

    const/4 v1, 0x0

    .local v1, isDateChanged:Z
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .local v0, cal:Ljava/util/Calendar;
    iget-object v3, p0, Lmiui/widget/DateTimePicker$2;->this$0:Lmiui/widget/DateTimePicker;

    #getter for: Lmiui/widget/DateTimePicker;->mIs24HourView:Z
    invoke-static {v3}, Lmiui/widget/DateTimePicker;->access$300(Lmiui/widget/DateTimePicker;)Z

    move-result v3

    if-nez v3, :cond_b3

    iget-object v3, p0, Lmiui/widget/DateTimePicker$2;->this$0:Lmiui/widget/DateTimePicker;

    #getter for: Lmiui/widget/DateTimePicker;->mIsAm:Z
    invoke-static {v3}, Lmiui/widget/DateTimePicker;->access$400(Lmiui/widget/DateTimePicker;)Z

    move-result v3

    if-nez v3, :cond_91

    if-ne p2, v8, :cond_91

    if-ne p3, v5, :cond_91

    iget-object v3, p0, Lmiui/widget/DateTimePicker$2;->this$0:Lmiui/widget/DateTimePicker;

    #getter for: Lmiui/widget/DateTimePicker;->mDate:Ljava/util/Calendar;
    invoke-static {v3}, Lmiui/widget/DateTimePicker;->access$000(Lmiui/widget/DateTimePicker;)Ljava/util/Calendar;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, Ljava/util/Calendar;->setTimeInMillis(J)V

    invoke-virtual {v0, v9, v4}, Ljava/util/Calendar;->add(II)V

    const/4 v1, 0x1

    :cond_31
    :goto_31
    if-ne p2, v8, :cond_35

    if-eq p3, v5, :cond_39

    :cond_35
    if-ne p2, v5, :cond_4c

    if-ne p3, v8, :cond_4c

    :cond_39
    iget-object v6, p0, Lmiui/widget/DateTimePicker$2;->this$0:Lmiui/widget/DateTimePicker;

    iget-object v3, p0, Lmiui/widget/DateTimePicker$2;->this$0:Lmiui/widget/DateTimePicker;

    #getter for: Lmiui/widget/DateTimePicker;->mIsAm:Z
    invoke-static {v3}, Lmiui/widget/DateTimePicker;->access$400(Lmiui/widget/DateTimePicker;)Z

    move-result v3

    if-nez v3, :cond_af

    move v3, v4

    :goto_44
    #setter for: Lmiui/widget/DateTimePicker;->mIsAm:Z
    invoke-static {v6, v3}, Lmiui/widget/DateTimePicker;->access$402(Lmiui/widget/DateTimePicker;Z)Z

    iget-object v3, p0, Lmiui/widget/DateTimePicker$2;->this$0:Lmiui/widget/DateTimePicker;

    #calls: Lmiui/widget/DateTimePicker;->updateAmPmControl()V
    invoke-static {v3}, Lmiui/widget/DateTimePicker;->access$500(Lmiui/widget/DateTimePicker;)V

    :cond_4c
    iget-object v3, p0, Lmiui/widget/DateTimePicker$2;->this$0:Lmiui/widget/DateTimePicker;

    #getter for: Lmiui/widget/DateTimePicker;->mHourSpinner:Landroid/widget/NumberPicker;
    invoke-static {v3}, Lmiui/widget/DateTimePicker;->access$600(Lmiui/widget/DateTimePicker;)Landroid/widget/NumberPicker;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/NumberPicker;->getValue()I

    move-result v3

    rem-int/lit8 v6, v3, 0xc

    iget-object v3, p0, Lmiui/widget/DateTimePicker$2;->this$0:Lmiui/widget/DateTimePicker;

    #getter for: Lmiui/widget/DateTimePicker;->mIsAm:Z
    invoke-static {v3}, Lmiui/widget/DateTimePicker;->access$400(Lmiui/widget/DateTimePicker;)Z

    move-result v3

    if-eqz v3, :cond_b1

    const/4 v3, 0x0

    :goto_61
    add-int v2, v6, v3

    .local v2, newHour:I
    :goto_63
    iget-object v3, p0, Lmiui/widget/DateTimePicker$2;->this$0:Lmiui/widget/DateTimePicker;

    #getter for: Lmiui/widget/DateTimePicker;->mDate:Ljava/util/Calendar;
    invoke-static {v3}, Lmiui/widget/DateTimePicker;->access$000(Lmiui/widget/DateTimePicker;)Ljava/util/Calendar;

    move-result-object v3

    invoke-virtual {v3, v8, v2}, Ljava/util/Calendar;->set(II)V

    iget-object v3, p0, Lmiui/widget/DateTimePicker$2;->this$0:Lmiui/widget/DateTimePicker;

    #calls: Lmiui/widget/DateTimePicker;->onDateTimeChanged()V
    invoke-static {v3}, Lmiui/widget/DateTimePicker;->access$200(Lmiui/widget/DateTimePicker;)V

    if-eqz v1, :cond_90

    iget-object v3, p0, Lmiui/widget/DateTimePicker$2;->this$0:Lmiui/widget/DateTimePicker;

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-virtual {v3, v4}, Lmiui/widget/DateTimePicker;->setCurrentYear(I)V

    iget-object v3, p0, Lmiui/widget/DateTimePicker$2;->this$0:Lmiui/widget/DateTimePicker;

    const/4 v4, 0x2

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-virtual {v3, v4}, Lmiui/widget/DateTimePicker;->setCurrentMonth(I)V

    iget-object v3, p0, Lmiui/widget/DateTimePicker$2;->this$0:Lmiui/widget/DateTimePicker;

    const/4 v4, 0x5

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-virtual {v3, v4}, Lmiui/widget/DateTimePicker;->setCurrentDay(I)V

    :cond_90
    return-void

    .end local v2           #newHour:I
    :cond_91
    iget-object v3, p0, Lmiui/widget/DateTimePicker$2;->this$0:Lmiui/widget/DateTimePicker;

    #getter for: Lmiui/widget/DateTimePicker;->mIsAm:Z
    invoke-static {v3}, Lmiui/widget/DateTimePicker;->access$400(Lmiui/widget/DateTimePicker;)Z

    move-result v3

    if-eqz v3, :cond_31

    if-ne p2, v5, :cond_31

    if-ne p3, v8, :cond_31

    iget-object v3, p0, Lmiui/widget/DateTimePicker$2;->this$0:Lmiui/widget/DateTimePicker;

    #getter for: Lmiui/widget/DateTimePicker;->mDate:Ljava/util/Calendar;
    invoke-static {v3}, Lmiui/widget/DateTimePicker;->access$000(Lmiui/widget/DateTimePicker;)Ljava/util/Calendar;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, Ljava/util/Calendar;->setTimeInMillis(J)V

    invoke-virtual {v0, v9, v10}, Ljava/util/Calendar;->add(II)V

    const/4 v1, 0x1

    goto :goto_31

    :cond_af
    const/4 v3, 0x0

    goto :goto_44

    :cond_b1
    move v3, v5

    goto :goto_61

    :cond_b3
    const/16 v3, 0x17

    if-ne p2, v3, :cond_d5

    if-nez p3, :cond_d5

    iget-object v3, p0, Lmiui/widget/DateTimePicker$2;->this$0:Lmiui/widget/DateTimePicker;

    #getter for: Lmiui/widget/DateTimePicker;->mDate:Ljava/util/Calendar;
    invoke-static {v3}, Lmiui/widget/DateTimePicker;->access$000(Lmiui/widget/DateTimePicker;)Ljava/util/Calendar;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v5

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->setTimeInMillis(J)V

    invoke-virtual {v0, v9, v4}, Ljava/util/Calendar;->add(II)V

    const/4 v1, 0x1

    :cond_ca
    :goto_ca
    iget-object v3, p0, Lmiui/widget/DateTimePicker$2;->this$0:Lmiui/widget/DateTimePicker;

    #getter for: Lmiui/widget/DateTimePicker;->mHourSpinner:Landroid/widget/NumberPicker;
    invoke-static {v3}, Lmiui/widget/DateTimePicker;->access$600(Lmiui/widget/DateTimePicker;)Landroid/widget/NumberPicker;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/NumberPicker;->getValue()I

    move-result v2

    .restart local v2       #newHour:I
    goto :goto_63

    .end local v2           #newHour:I
    :cond_d5
    if-nez p2, :cond_ca

    const/16 v3, 0x17

    if-ne p3, v3, :cond_ca

    iget-object v3, p0, Lmiui/widget/DateTimePicker$2;->this$0:Lmiui/widget/DateTimePicker;

    #getter for: Lmiui/widget/DateTimePicker;->mDate:Ljava/util/Calendar;
    invoke-static {v3}, Lmiui/widget/DateTimePicker;->access$000(Lmiui/widget/DateTimePicker;)Ljava/util/Calendar;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v5

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->setTimeInMillis(J)V

    invoke-virtual {v0, v9, v10}, Ljava/util/Calendar;->add(II)V

    const/4 v1, 0x1

    goto :goto_ca
.end method
