.class public Lmiui/widget/DataGroup;
.super Ljava/util/ArrayList;
.source "DataGroup.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/ArrayList",
        "<TT;>;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .local p0, this:Lmiui/widget/DataGroup;,"Lmiui/widget/DataGroup<TT;>;"
    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    return-void
.end method


# virtual methods
.method public getTitle()Ljava/lang/String;
    .registers 2

    .prologue
    .local p0, this:Lmiui/widget/DataGroup;,"Lmiui/widget/DataGroup<TT;>;"
    iget-object v0, p0, Lmiui/widget/DataGroup;->title:Ljava/lang/String;

    return-object v0
.end method

.method public setTitle(Ljava/lang/String;)V
    .registers 2
    .parameter "title"

    .prologue
    .local p0, this:Lmiui/widget/DataGroup;,"Lmiui/widget/DataGroup<TT;>;"
    iput-object p1, p0, Lmiui/widget/DataGroup;->title:Ljava/lang/String;

    return-void
.end method
