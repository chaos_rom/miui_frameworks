.class public Lmiui/widget/ScrollableScreenView;
.super Lmiui/widget/ScreenView;
.source "ScrollableScreenView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/widget/ScrollableScreenView$ScrollAnimation;,
        Lmiui/widget/ScrollableScreenView$OnScrollOutListener;
    }
.end annotation


# static fields
.field private static final DEFAULT_SCREEN_SNAP_DURATION:I = 0x64


# instance fields
.field private mScrollOutListener:Lmiui/widget/ScrollableScreenView$OnScrollOutListener;

.field private mScrollView:Landroid/widget/ScrollView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    .prologue
    invoke-direct {p0, p1}, Lmiui/widget/ScreenView;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .parameter "context"
    .parameter "attrs"

    .prologue
    invoke-direct {p0, p1, p2}, Lmiui/widget/ScreenView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    invoke-direct {p0, p1, p2, p3}, Lmiui/widget/ScreenView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method static synthetic access$000(Lmiui/widget/ScrollableScreenView;)Landroid/widget/ScrollView;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/widget/ScrollableScreenView;->mScrollView:Landroid/widget/ScrollView;

    return-object v0
.end method

.method private makeWholeViewVisiable()V
    .registers 6

    .prologue
    iget-object v2, p0, Lmiui/widget/ScrollableScreenView;->mScrollView:Landroid/widget/ScrollView;

    if-nez v2, :cond_5

    :cond_4
    :goto_4
    return-void

    :cond_5
    invoke-virtual {p0}, Lmiui/widget/ScrollableScreenView;->getTop()I

    move-result v0

    .local v0, disToScrollParent:I
    invoke-virtual {p0}, Lmiui/widget/ScrollableScreenView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .local v1, pare:Landroid/view/ViewParent;
    :goto_d
    if-eqz v1, :cond_26

    iget-object v2, p0, Lmiui/widget/ScrollableScreenView;->mScrollView:Landroid/widget/ScrollView;

    if-eq v1, v2, :cond_26

    instance-of v2, v1, Landroid/view/View;

    if-eqz v2, :cond_24

    move-object v2, v1

    check-cast v2, Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    add-int/2addr v0, v2

    invoke-interface {v1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    goto :goto_d

    :cond_24
    const/4 v1, 0x0

    goto :goto_d

    :cond_26
    iget-object v2, p0, Lmiui/widget/ScrollableScreenView;->mScrollView:Landroid/widget/ScrollView;

    if-ne v1, v2, :cond_4

    iget-object v2, p0, Lmiui/widget/ScrollableScreenView;->mScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v2}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v2

    if-le v2, v0, :cond_4

    iget-object v2, p0, Lmiui/widget/ScrollableScreenView;->mScrollView:Landroid/widget/ScrollView;

    new-instance v3, Lmiui/widget/ScrollableScreenView$ScrollAnimation;

    iget-object v4, p0, Lmiui/widget/ScrollableScreenView;->mScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v4}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v4

    invoke-direct {v3, p0, v4, v0}, Lmiui/widget/ScrollableScreenView$ScrollAnimation;-><init>(Lmiui/widget/ScrollableScreenView;II)V

    invoke-virtual {v2, v3}, Landroid/widget/ScrollView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_4
.end method


# virtual methods
.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 4
    .parameter "ev"

    .prologue
    const/4 v0, 0x2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    and-int/lit16 v1, v1, 0xff

    if-ne v0, v1, :cond_d

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lmiui/widget/ScrollableScreenView;->requestDisallowInterceptTouchEvent(Z)V

    :cond_d
    invoke-super {p0, p1}, Lmiui/widget/ScreenView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public setOnScrollOutListener(Lmiui/widget/ScrollableScreenView$OnScrollOutListener;)V
    .registers 2
    .parameter "listener"

    .prologue
    iput-object p1, p0, Lmiui/widget/ScrollableScreenView;->mScrollOutListener:Lmiui/widget/ScrollableScreenView$OnScrollOutListener;

    return-void
.end method

.method public setParentScrollView(Landroid/widget/ScrollView;)V
    .registers 3
    .parameter "view"

    .prologue
    iput-object p1, p0, Lmiui/widget/ScrollableScreenView;->mScrollView:Landroid/widget/ScrollView;

    iget-object v0, p0, Lmiui/widget/ScrollableScreenView;->mScrollView:Landroid/widget/ScrollView;

    if-eqz v0, :cond_b

    const/16 v0, 0x64

    invoke-virtual {p0, v0}, Lmiui/widget/ScrollableScreenView;->setScreenSnapDuration(I)V

    :cond_b
    return-void
.end method

.method protected snapToScreen(IIZ)V
    .registers 15
    .parameter "whichScreen"
    .parameter "velocity"
    .parameter "settle"

    .prologue
    const-wide v9, 0x3fc999999999999aL

    const/4 v8, 0x0

    invoke-virtual {p0}, Lmiui/widget/ScrollableScreenView;->getScreenCount()I

    move-result v2

    iget v3, p0, Lmiui/widget/ScreenView;->mVisibleRange:I

    sub-int/2addr v2, v3

    invoke-static {p1, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v8, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    iput v2, p0, Lmiui/widget/ScreenView;->mNextScreen:I

    iget v2, p0, Lmiui/widget/ScreenView;->mNextScreen:I

    iget v3, p0, Lmiui/widget/ScreenView;->mCurrentScreen:I

    if-ne v2, v3, :cond_72

    iget-object v2, p0, Lmiui/widget/ScrollableScreenView;->mScrollOutListener:Lmiui/widget/ScrollableScreenView$OnScrollOutListener;

    if-eqz v2, :cond_72

    const-wide v0, 0x3fc999999999999aL

    .local v0, RATIO:D
    iget v2, p0, Lmiui/widget/ScreenView;->mScrollX:I

    int-to-double v2, v2

    iget v4, p0, Lmiui/widget/ScreenView;->mScrollLeftBound:I

    int-to-double v4, v4

    iget v6, p0, Lmiui/widget/ScreenView;->mChildScreenWidth:I

    int-to-float v6, v6

    iget v7, p0, Lmiui/widget/ScreenView;->mOverScrollRatio:F

    mul-float/2addr v6, v7

    float-to-double v6, v6

    mul-double/2addr v6, v9

    add-double/2addr v4, v6

    cmpg-double v2, v2, v4

    if-gez v2, :cond_4e

    iget-object v2, p0, Lmiui/widget/ScrollableScreenView;->mScrollOutListener:Lmiui/widget/ScrollableScreenView$OnScrollOutListener;

    invoke-interface {v2, p0, v8}, Lmiui/widget/ScrollableScreenView$OnScrollOutListener;->onScrollOut(Landroid/view/View;I)Z

    move-result v2

    if-eqz v2, :cond_4e

    invoke-virtual {p0}, Lmiui/widget/ScrollableScreenView;->getScreenCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p0, v2}, Lmiui/widget/ScrollableScreenView;->setCurrentScreen(I)V

    .end local v0           #RATIO:D
    :goto_4a
    invoke-direct {p0}, Lmiui/widget/ScrollableScreenView;->makeWholeViewVisiable()V

    return-void

    .restart local v0       #RATIO:D
    :cond_4e
    iget v2, p0, Lmiui/widget/ScreenView;->mScrollX:I

    int-to-double v2, v2

    iget v4, p0, Lmiui/widget/ScreenView;->mScrollRightBound:I

    int-to-double v4, v4

    iget v6, p0, Lmiui/widget/ScreenView;->mChildScreenWidth:I

    int-to-float v6, v6

    iget v7, p0, Lmiui/widget/ScreenView;->mOverScrollRatio:F

    mul-float/2addr v6, v7

    float-to-double v6, v6

    mul-double/2addr v6, v9

    sub-double/2addr v4, v6

    cmpl-double v2, v2, v4

    if-lez v2, :cond_6e

    iget-object v2, p0, Lmiui/widget/ScrollableScreenView;->mScrollOutListener:Lmiui/widget/ScrollableScreenView$OnScrollOutListener;

    const/4 v3, 0x1

    invoke-interface {v2, p0, v3}, Lmiui/widget/ScrollableScreenView$OnScrollOutListener;->onScrollOut(Landroid/view/View;I)Z

    move-result v2

    if-eqz v2, :cond_6e

    invoke-virtual {p0, v8}, Lmiui/widget/ScrollableScreenView;->setCurrentScreen(I)V

    goto :goto_4a

    :cond_6e
    invoke-super {p0, p1, p2, p3}, Lmiui/widget/ScreenView;->snapToScreen(IIZ)V

    goto :goto_4a

    .end local v0           #RATIO:D
    :cond_72
    invoke-super {p0, p1, p2, p3}, Lmiui/widget/ScreenView;->snapToScreen(IIZ)V

    goto :goto_4a
.end method
