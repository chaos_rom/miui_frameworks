.class Lmiui/analytics/PersistenceHelper$RunThread$1;
.super Landroid/os/Handler;
.source "PersistenceHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiui/analytics/PersistenceHelper$RunThread;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lmiui/analytics/PersistenceHelper$RunThread;


# direct methods
.method constructor <init>(Lmiui/analytics/PersistenceHelper$RunThread;)V
    .registers 2
    .parameter

    .prologue
    iput-object p1, p0, Lmiui/analytics/PersistenceHelper$RunThread$1;->this$1:Lmiui/analytics/PersistenceHelper$RunThread;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 4
    .parameter "msg"

    .prologue
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_3a

    :goto_5
    return-void

    :pswitch_6
    iget-object v0, p0, Lmiui/analytics/PersistenceHelper$RunThread$1;->this$1:Lmiui/analytics/PersistenceHelper$RunThread;

    iget-object v0, v0, Lmiui/analytics/PersistenceHelper$RunThread;->this$0:Lmiui/analytics/PersistenceHelper;

    #calls: Lmiui/analytics/PersistenceHelper;->writeOpenImp()V
    invoke-static {v0}, Lmiui/analytics/PersistenceHelper;->access$200(Lmiui/analytics/PersistenceHelper;)V

    goto :goto_5

    :pswitch_e
    iget-object v0, p0, Lmiui/analytics/PersistenceHelper$RunThread$1;->this$1:Lmiui/analytics/PersistenceHelper$RunThread;

    iget-object v1, v0, Lmiui/analytics/PersistenceHelper$RunThread;->this$0:Lmiui/analytics/PersistenceHelper;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lmiui/analytics/Event;

    #calls: Lmiui/analytics/PersistenceHelper;->writeEventImp(Lmiui/analytics/Event;)V
    invoke-static {v1, v0}, Lmiui/analytics/PersistenceHelper;->access$300(Lmiui/analytics/PersistenceHelper;Lmiui/analytics/Event;)V

    goto :goto_5

    :pswitch_1a
    iget-object v0, p0, Lmiui/analytics/PersistenceHelper$RunThread$1;->this$1:Lmiui/analytics/PersistenceHelper$RunThread;

    iget-object v0, v0, Lmiui/analytics/PersistenceHelper$RunThread;->this$0:Lmiui/analytics/PersistenceHelper;

    #calls: Lmiui/analytics/PersistenceHelper;->closeImp()V
    invoke-static {v0}, Lmiui/analytics/PersistenceHelper;->access$400(Lmiui/analytics/PersistenceHelper;)V

    iget-object v0, p0, Lmiui/analytics/PersistenceHelper$RunThread$1;->this$1:Lmiui/analytics/PersistenceHelper$RunThread;

    iget-object v0, v0, Lmiui/analytics/PersistenceHelper$RunThread;->this$0:Lmiui/analytics/PersistenceHelper;

    #getter for: Lmiui/analytics/PersistenceHelper;->mWorkHandler:Landroid/os/Handler;
    invoke-static {v0}, Lmiui/analytics/PersistenceHelper;->access$100(Lmiui/analytics/PersistenceHelper;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    iget-object v0, p0, Lmiui/analytics/PersistenceHelper$RunThread$1;->this$1:Lmiui/analytics/PersistenceHelper$RunThread;

    iget-object v0, v0, Lmiui/analytics/PersistenceHelper$RunThread;->this$0:Lmiui/analytics/PersistenceHelper;

    const/4 v1, 0x0

    #setter for: Lmiui/analytics/PersistenceHelper;->mWorkHandler:Landroid/os/Handler;
    invoke-static {v0, v1}, Lmiui/analytics/PersistenceHelper;->access$102(Lmiui/analytics/PersistenceHelper;Landroid/os/Handler;)Landroid/os/Handler;

    goto :goto_5

    nop

    :pswitch_data_3a
    .packed-switch 0x1
        :pswitch_6
        :pswitch_e
        :pswitch_1a
    .end packed-switch
.end method
