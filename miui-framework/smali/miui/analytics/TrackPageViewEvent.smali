.class public Lmiui/analytics/TrackPageViewEvent;
.super Lmiui/analytics/Event;
.source "TrackPageViewEvent.java"


# static fields
.field private static final PAGEVIEW_EVENT:Ljava/lang/String; = "_pageview_event_"


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    invoke-direct {p0}, Lmiui/analytics/Event;-><init>()V

    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmiui/analytics/Event;->mType:Ljava/lang/Integer;

    const-string v0, "_pageview_event_"

    iput-object v0, p0, Lmiui/analytics/Event;->mEventId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public dispatch()V
    .registers 4

    .prologue
    sget-object v2, Lmiui/analytics/TrackPageViewEvent;->sDispatcher:Ljava/util/List;

    if-eqz v2, :cond_1a

    sget-object v2, Lmiui/analytics/TrackPageViewEvent;->sDispatcher:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/analytics/Dispatchable;

    .local v0, disp:Lmiui/analytics/Dispatchable;
    invoke-interface {v0, p0}, Lmiui/analytics/Dispatchable;->dispatchPageView(Lmiui/analytics/TrackPageViewEvent;)V

    goto :goto_a

    .end local v0           #disp:Lmiui/analytics/Dispatchable;
    .end local v1           #i$:Ljava/util/Iterator;
    :cond_1a
    return-void
.end method

.method public writeEvent(Lmiui/analytics/Storable;)V
    .registers 8
    .parameter "store"

    .prologue
    if-eqz p1, :cond_26

    iget-object v1, p0, Lmiui/analytics/Event;->mType:Ljava/lang/Integer;

    iget-object v2, p0, Lmiui/analytics/Event;->mEventId:Ljava/lang/String;

    const-string v3, ""

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v4, p0, Lmiui/analytics/Event;->mTrackTime:J

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ""

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v5

    move-object v0, p1

    invoke-interface/range {v0 .. v5}, Lmiui/analytics/Storable;->writeData(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_26
    return-void
.end method
