.class public Lmiui/analytics/EventReader;
.super Ljava/lang/Object;
.source "EventReader.java"


# instance fields
.field private mPersistenceHelper:Lmiui/analytics/PersistenceHelper;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lmiui/analytics/PersistenceHelper;

    invoke-direct {v0}, Lmiui/analytics/PersistenceHelper;-><init>()V

    iput-object v0, p0, Lmiui/analytics/EventReader;->mPersistenceHelper:Lmiui/analytics/PersistenceHelper;

    return-void
.end method


# virtual methods
.method public close()V
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/analytics/EventReader;->mPersistenceHelper:Lmiui/analytics/PersistenceHelper;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lmiui/analytics/EventReader;->mPersistenceHelper:Lmiui/analytics/PersistenceHelper;

    invoke-virtual {v0}, Lmiui/analytics/PersistenceHelper;->close()V

    :cond_9
    return-void
.end method

.method public open(Landroid/content/Context;Ljava/lang/String;)V
    .registers 4
    .parameter "context"
    .parameter "databaseName"

    .prologue
    iget-object v0, p0, Lmiui/analytics/EventReader;->mPersistenceHelper:Lmiui/analytics/PersistenceHelper;

    invoke-virtual {v0, p1, p2}, Lmiui/analytics/PersistenceHelper;->readOpen(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method public readEvents(Ljava/lang/String;Ljava/util/Map;)Ljava/util/List;
    .registers 4
    .parameter "selection"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/util/regex/Pattern;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lmiui/analytics/Event;",
            ">;"
        }
    .end annotation

    .prologue
    .local p2, itemsReg:Ljava/util/Map;,"Ljava/util/Map<Ljava/util/regex/Pattern;Ljava/lang/String;>;"
    iget-object v0, p0, Lmiui/analytics/EventReader;->mPersistenceHelper:Lmiui/analytics/PersistenceHelper;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lmiui/analytics/EventReader;->mPersistenceHelper:Lmiui/analytics/PersistenceHelper;

    invoke-virtual {v0, p1, p2}, Lmiui/analytics/PersistenceHelper;->readEvents(Ljava/lang/String;Ljava/util/Map;)Ljava/util/List;

    move-result-object v0

    :goto_a
    return-object v0

    :cond_b
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_a
.end method
