.class public Lmiui/util/FileIconHelper;
.super Ljava/lang/Object;
.source "FileIconHelper.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "FileIconHelper"

.field private static final TYPE_APK:Ljava/lang/String; = "apk"

.field private static sFileExtToIcons:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lmiui/util/FileIconHelper;->sFileExtToIcons:Ljava/util/HashMap;

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "mp3"

    aput-object v1, v0, v3

    const v1, 0x602000b

    invoke-static {v0, v1}, Lmiui/util/FileIconHelper;->addItem([Ljava/lang/String;I)V

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "wma"

    aput-object v1, v0, v3

    const v1, 0x602000c

    invoke-static {v0, v1}, Lmiui/util/FileIconHelper;->addItem([Ljava/lang/String;I)V

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "wav"

    aput-object v1, v0, v3

    const v1, 0x602000d

    invoke-static {v0, v1}, Lmiui/util/FileIconHelper;->addItem([Ljava/lang/String;I)V

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "mid"

    aput-object v1, v0, v3

    const v1, 0x602000e

    invoke-static {v0, v1}, Lmiui/util/FileIconHelper;->addItem([Ljava/lang/String;I)V

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "mp4"

    aput-object v1, v0, v3

    const-string v1, "wmv"

    aput-object v1, v0, v4

    const-string v1, "mpeg"

    aput-object v1, v0, v5

    const-string v1, "m4v"

    aput-object v1, v0, v6

    const-string v1, "3gp"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "3gpp"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "3g2"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "3gpp2"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "asf"

    aput-object v2, v0, v1

    const v1, 0x602000f

    invoke-static {v0, v1}, Lmiui/util/FileIconHelper;->addItem([Ljava/lang/String;I)V

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "jpg"

    aput-object v1, v0, v3

    const-string v1, "jpeg"

    aput-object v1, v0, v4

    const-string v1, "gif"

    aput-object v1, v0, v5

    const-string v1, "png"

    aput-object v1, v0, v6

    const-string v1, "bmp"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "wbmp"

    aput-object v2, v0, v1

    const v1, 0x6020010

    invoke-static {v0, v1}, Lmiui/util/FileIconHelper;->addItem([Ljava/lang/String;I)V

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "txt"

    aput-object v1, v0, v3

    const-string v1, "log"

    aput-object v1, v0, v4

    const-string v1, "xml"

    aput-object v1, v0, v5

    const-string v1, "ini"

    aput-object v1, v0, v6

    const-string v1, "lrc"

    aput-object v1, v0, v7

    const v1, 0x6020011

    invoke-static {v0, v1}, Lmiui/util/FileIconHelper;->addItem([Ljava/lang/String;I)V

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "doc"

    aput-object v1, v0, v3

    const-string v1, "ppt"

    aput-object v1, v0, v4

    const-string v1, "docx"

    aput-object v1, v0, v5

    const-string v1, "pptx"

    aput-object v1, v0, v6

    const-string v1, "xsl"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "xslx"

    aput-object v2, v0, v1

    const v1, 0x6020012

    invoke-static {v0, v1}, Lmiui/util/FileIconHelper;->addItem([Ljava/lang/String;I)V

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "pdf"

    aput-object v1, v0, v3

    const v1, 0x6020013

    invoke-static {v0, v1}, Lmiui/util/FileIconHelper;->addItem([Ljava/lang/String;I)V

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "zip"

    aput-object v1, v0, v3

    const v1, 0x6020014

    invoke-static {v0, v1}, Lmiui/util/FileIconHelper;->addItem([Ljava/lang/String;I)V

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "mtz"

    aput-object v1, v0, v3

    const v1, 0x6020015

    invoke-static {v0, v1}, Lmiui/util/FileIconHelper;->addItem([Ljava/lang/String;I)V

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "rar"

    aput-object v1, v0, v3

    const v1, 0x6020016

    invoke-static {v0, v1}, Lmiui/util/FileIconHelper;->addItem([Ljava/lang/String;I)V

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static addItem([Ljava/lang/String;I)V
    .registers 9
    .parameter "exts"
    .parameter "resId"

    .prologue
    if-eqz p0, :cond_19

    move-object v0, p0

    .local v0, arr$:[Ljava/lang/String;
    array-length v3, v0

    .local v3, len$:I
    const/4 v2, 0x0

    .local v2, i$:I
    :goto_5
    if-ge v2, v3, :cond_19

    aget-object v1, v0, v2

    .local v1, ext:Ljava/lang/String;
    sget-object v4, Lmiui/util/FileIconHelper;->sFileExtToIcons:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #ext:Ljava/lang/String;
    .end local v2           #i$:I
    .end local v3           #len$:I
    :cond_19
    return-void
.end method

.method private static getApkIcon(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .registers 8
    .parameter "context"
    .parameter "path"

    .prologue
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .local v3, pm:Landroid/content/pm/PackageManager;
    const/4 v4, 0x1

    invoke-virtual {v3, p1, v4}, Landroid/content/pm/PackageManager;->getPackageArchiveInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .local v2, info:Landroid/content/pm/PackageInfo;
    if-eqz v2, :cond_20

    iget-object v0, v2, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    .local v0, appInfo:Landroid/content/pm/ApplicationInfo;
    if-eqz v0, :cond_20

    :try_start_f
    iput-object p1, v0, Landroid/content/pm/ApplicationInfo;->publicSourceDir:Ljava/lang/String;

    invoke-virtual {v3, v0}, Landroid/content/pm/PackageManager;->getApplicationIcon(Landroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;
    :try_end_14
    .catch Ljava/lang/OutOfMemoryError; {:try_start_f .. :try_end_14} :catch_16

    move-result-object v4

    .end local v0           #appInfo:Landroid/content/pm/ApplicationInfo;
    :goto_15
    return-object v4

    .restart local v0       #appInfo:Landroid/content/pm/ApplicationInfo;
    :catch_16
    move-exception v1

    .local v1, e:Ljava/lang/OutOfMemoryError;
    const-string v4, "FileIconHelper"

    invoke-virtual {v1}, Ljava/lang/OutOfMemoryError;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .end local v0           #appInfo:Landroid/content/pm/ApplicationInfo;
    .end local v1           #e:Ljava/lang/OutOfMemoryError;
    :cond_20
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x6020017

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    goto :goto_15
.end method

.method private static getExtFromFilename(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "filename"

    .prologue
    const/16 v1, 0x2e

    invoke-virtual {p0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .local v0, dotPosition:I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_14

    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    :goto_13
    return-object v1

    :cond_14
    const-string v1, ""

    goto :goto_13
.end method

.method public static getFileIcon(Ljava/lang/String;)I
    .registers 5
    .parameter "ext"

    .prologue
    sget-object v2, Lmiui/util/FileIconHelper;->sFileExtToIcons:Ljava/util/HashMap;

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .local v0, i:Ljava/lang/Integer;
    const v1, 0x6020017

    .local v1, resId:I
    if-eqz v0, :cond_15

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :cond_15
    return v1
.end method

.method public static getFileIcon(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .registers 6
    .parameter "context"
    .parameter "fileFullPath"

    .prologue
    invoke-static {p1}, Lmiui/util/FileIconHelper;->getExtFromFilename(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .local v0, ext:Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, icon:Landroid/graphics/drawable/Drawable;
    const-string v2, "apk"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12

    invoke-static {p0, p1}, Lmiui/util/FileIconHelper;->getApkIcon(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    :goto_11
    return-object v1

    :cond_12
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v0}, Lmiui/util/FileIconHelper;->getFileIcon(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto :goto_11
.end method
