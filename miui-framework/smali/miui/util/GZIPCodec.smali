.class public final Lmiui/util/GZIPCodec;
.super Ljava/lang/Object;
.source "GZIPCodec.java"


# static fields
.field private static final BUFFER_SIZE:I = 0x200


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static decode([B)[B
    .registers 8
    .parameter "compressed"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .local v1, byteIn:Ljava/io/ByteArrayInputStream;
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .local v2, byteOut:Ljava/io/ByteArrayOutputStream;
    const/4 v3, 0x0

    .local v3, gzIn:Ljava/util/zip/GZIPInputStream;
    :try_start_b
    new-instance v4, Ljava/util/zip/GZIPInputStream;

    invoke-direct {v4, v1}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_10
    .catchall {:try_start_b .. :try_end_10} :catchall_2e

    .end local v3           #gzIn:Ljava/util/zip/GZIPInputStream;
    .local v4, gzIn:Ljava/util/zip/GZIPInputStream;
    const/16 v6, 0x200

    :try_start_12
    new-array v0, v6, [B

    .local v0, buffer:[B
    :cond_14
    invoke-virtual {v4, v0}, Ljava/util/zip/GZIPInputStream;->read([B)I

    move-result v5

    .local v5, read:I
    if-lez v5, :cond_1e

    const/4 v6, 0x0

    invoke-virtual {v2, v0, v6, v5}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_1e
    .catchall {:try_start_12 .. :try_end_1e} :catchall_39

    :cond_1e
    if-gez v5, :cond_14

    invoke-static {v4}, Lmiui/util/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    invoke-static {v1}, Lmiui/util/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    invoke-static {v2}, Lmiui/util/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    return-object v6

    .end local v0           #buffer:[B
    .end local v4           #gzIn:Ljava/util/zip/GZIPInputStream;
    .end local v5           #read:I
    .restart local v3       #gzIn:Ljava/util/zip/GZIPInputStream;
    :catchall_2e
    move-exception v6

    :goto_2f
    invoke-static {v3}, Lmiui/util/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    invoke-static {v1}, Lmiui/util/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    invoke-static {v2}, Lmiui/util/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    throw v6

    .end local v3           #gzIn:Ljava/util/zip/GZIPInputStream;
    .restart local v4       #gzIn:Ljava/util/zip/GZIPInputStream;
    :catchall_39
    move-exception v6

    move-object v3, v4

    .end local v4           #gzIn:Ljava/util/zip/GZIPInputStream;
    .restart local v3       #gzIn:Ljava/util/zip/GZIPInputStream;
    goto :goto_2f
.end method

.method public static encode([B)[B
    .registers 5
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .local v0, byteOut:Ljava/io/ByteArrayOutputStream;
    const/4 v1, 0x0

    .local v1, gzOut:Ljava/util/zip/GZIPOutputStream;
    :try_start_6
    new-instance v2, Ljava/util/zip/GZIPOutputStream;

    invoke-direct {v2, v0}, Ljava/util/zip/GZIPOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_b
    .catchall {:try_start_6 .. :try_end_b} :catchall_19

    .end local v1           #gzOut:Ljava/util/zip/GZIPOutputStream;
    .local v2, gzOut:Ljava/util/zip/GZIPOutputStream;
    :try_start_b
    invoke-virtual {v2, p0}, Ljava/util/zip/GZIPOutputStream;->write([B)V
    :try_end_e
    .catchall {:try_start_b .. :try_end_e} :catchall_21

    invoke-static {v2}, Lmiui/util/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    invoke-static {v0}, Lmiui/util/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    return-object v3

    .end local v2           #gzOut:Ljava/util/zip/GZIPOutputStream;
    .restart local v1       #gzOut:Ljava/util/zip/GZIPOutputStream;
    :catchall_19
    move-exception v3

    :goto_1a
    invoke-static {v1}, Lmiui/util/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    invoke-static {v0}, Lmiui/util/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    throw v3

    .end local v1           #gzOut:Ljava/util/zip/GZIPOutputStream;
    .restart local v2       #gzOut:Ljava/util/zip/GZIPOutputStream;
    :catchall_21
    move-exception v3

    move-object v1, v2

    .end local v2           #gzOut:Ljava/util/zip/GZIPOutputStream;
    .restart local v1       #gzOut:Ljava/util/zip/GZIPOutputStream;
    goto :goto_1a
.end method

.method public static getID()Ljava/lang/String;
    .registers 1

    .prologue
    const-string v0, "gzip"

    return-object v0
.end method
