.class public Lmiui/util/HanziToPinyinHelper;
.super Ljava/lang/Object;
.source "HanziToPinyinHelper.java"


# static fields
#the value of this static final field might be set in the static constructor
.field private static final FILE_HEADER_SIZE:I = 0x0

.field private static final FILE_VERSION:I = 0x1

.field private static final TAG:Ljava/lang/String; = "HanziToPinyinHelper"

.field public static final UNICODE_2_PINYIN_RESOURCE_NAME:Ljava/lang/String; = "etc/unicode_py_index.td"

.field private static sSingleton:Lmiui/util/HanziToPinyinHelper;


# instance fields
.field private mFile:Ljava/io/RandomAccessFile;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    sget-object v0, Lmiui/util/AllPinyinConstants;->FILE_TAG:[B

    array-length v0, v0

    add-int/lit8 v0, v0, 0x4

    const-string v1, "26d188162454f2c17cf3194641c1e0fca7c2b72d"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    array-length v1, v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x0

    sput v0, Lmiui/util/HanziToPinyinHelper;->FILE_HEADER_SIZE:I

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    invoke-direct {p0}, Lmiui/util/HanziToPinyinHelper;->initResource()V

    return-void
.end method

.method public static declared-synchronized getIntance()Lmiui/util/HanziToPinyinHelper;
    .registers 2

    .prologue
    const-class v1, Lmiui/util/HanziToPinyinHelper;

    monitor-enter v1

    :try_start_3
    sget-object v0, Lmiui/util/HanziToPinyinHelper;->sSingleton:Lmiui/util/HanziToPinyinHelper;

    if-nez v0, :cond_e

    new-instance v0, Lmiui/util/HanziToPinyinHelper;

    invoke-direct {v0}, Lmiui/util/HanziToPinyinHelper;-><init>()V

    sput-object v0, Lmiui/util/HanziToPinyinHelper;->sSingleton:Lmiui/util/HanziToPinyinHelper;

    :cond_e
    sget-object v0, Lmiui/util/HanziToPinyinHelper;->sSingleton:Lmiui/util/HanziToPinyinHelper;
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_12

    monitor-exit v1

    return-object v0

    :catchall_12
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private initResource()V
    .registers 15

    .prologue
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getRootDirectory()Ljava/io/File;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget-object v11, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "etc/unicode_py_index.td"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .local v5, path:Ljava/lang/String;
    const/4 v6, 0x1

    .local v6, success:Z
    :try_start_1e
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    .local v7, times:J
    const-string v10, "HanziToPinyinHelper"

    const-string v11, "Read"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .local v3, file:Ljava/io/File;
    new-instance v10, Ljava/io/RandomAccessFile;

    const-string v11, "r"

    invoke-direct {v10, v3, v11}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v10, p0, Lmiui/util/HanziToPinyinHelper;->mFile:Ljava/io/RandomAccessFile;

    sget-object v10, Lmiui/util/AllPinyinConstants;->FILE_TAG:[B

    array-length v10, v10

    new-array v4, v10, [B

    .local v4, fileTag:[B
    iget-object v10, p0, Lmiui/util/HanziToPinyinHelper;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v10, v4}, Ljava/io/RandomAccessFile;->read([B)I

    sget-object v10, Lmiui/util/AllPinyinConstants;->FILE_TAG:[B

    invoke-static {v4, v10}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v10

    if-nez v10, :cond_7d

    const-string v10, "HanziToPinyinHelper"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "File tag not right "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_61
    .catchall {:try_start_1e .. :try_end_61} :catchall_1b6
    .catch Ljava/io/FileNotFoundException; {:try_start_1e .. :try_end_61} :catch_146
    .catch Ljava/io/IOException; {:try_start_1e .. :try_end_61} :catch_17b

    const/4 v6, 0x0

    if-nez v6, :cond_1d3

    iget-object v10, p0, Lmiui/util/HanziToPinyinHelper;->mFile:Ljava/io/RandomAccessFile;

    if-eqz v10, :cond_1d3

    :try_start_68
    iget-object v10, p0, Lmiui/util/HanziToPinyinHelper;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v10}, Ljava/io/RandomAccessFile;->close()V

    const/4 v10, 0x0

    iput-object v10, p0, Lmiui/util/HanziToPinyinHelper;->mFile:Ljava/io/RandomAccessFile;
    :try_end_70
    .catch Ljava/lang/Exception; {:try_start_68 .. :try_end_70} :catch_78

    :goto_70
    new-instance v10, Ljava/lang/RuntimeException;

    const-string v11, "Please update the data file: unicode_py_index.td"

    invoke-direct {v10, v11}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v10

    :catch_78
    move-exception v2

    .local v2, e2:Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_70

    .end local v2           #e2:Ljava/lang/Exception;
    :cond_7d
    :try_start_7d
    iget-object v10, p0, Lmiui/util/HanziToPinyinHelper;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v10}, Ljava/io/RandomAccessFile;->readInt()I

    move-result v10

    new-array v0, v10, [B

    .local v0, digestBytes:[B
    iget-object v10, p0, Lmiui/util/HanziToPinyinHelper;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v10, v0}, Ljava/io/RandomAccessFile;->read([B)I

    const-string v10, "26d188162454f2c17cf3194641c1e0fca7c2b72d"

    new-instance v11, Ljava/lang/String;

    invoke-direct {v11, v0}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_cb

    const-string v10, "HanziToPinyinHelper"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Unmatched digest for "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_af
    .catchall {:try_start_7d .. :try_end_af} :catchall_1b6
    .catch Ljava/io/FileNotFoundException; {:try_start_7d .. :try_end_af} :catch_146
    .catch Ljava/io/IOException; {:try_start_7d .. :try_end_af} :catch_17b

    const/4 v6, 0x0

    if-nez v6, :cond_1d3

    iget-object v10, p0, Lmiui/util/HanziToPinyinHelper;->mFile:Ljava/io/RandomAccessFile;

    if-eqz v10, :cond_1d3

    :try_start_b6
    iget-object v10, p0, Lmiui/util/HanziToPinyinHelper;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v10}, Ljava/io/RandomAccessFile;->close()V

    const/4 v10, 0x0

    iput-object v10, p0, Lmiui/util/HanziToPinyinHelper;->mFile:Ljava/io/RandomAccessFile;
    :try_end_be
    .catch Ljava/lang/Exception; {:try_start_b6 .. :try_end_be} :catch_c6

    :goto_be
    new-instance v10, Ljava/lang/RuntimeException;

    const-string v11, "Please update the data file: unicode_py_index.td"

    invoke-direct {v10, v11}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v10

    :catch_c6
    move-exception v2

    .restart local v2       #e2:Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_be

    .end local v2           #e2:Ljava/lang/Exception;
    :cond_cb
    :try_start_cb
    iget-object v10, p0, Lmiui/util/HanziToPinyinHelper;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v10}, Ljava/io/RandomAccessFile;->readInt()I

    move-result v9

    .local v9, version:I
    const/4 v10, 0x1

    if-eq v10, v9, :cond_108

    const-string v10, "HanziToPinyinHelper"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Unmatched version for "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_ec
    .catchall {:try_start_cb .. :try_end_ec} :catchall_1b6
    .catch Ljava/io/FileNotFoundException; {:try_start_cb .. :try_end_ec} :catch_146
    .catch Ljava/io/IOException; {:try_start_cb .. :try_end_ec} :catch_17b

    const/4 v6, 0x0

    if-nez v6, :cond_1d3

    iget-object v10, p0, Lmiui/util/HanziToPinyinHelper;->mFile:Ljava/io/RandomAccessFile;

    if-eqz v10, :cond_1d3

    :try_start_f3
    iget-object v10, p0, Lmiui/util/HanziToPinyinHelper;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v10}, Ljava/io/RandomAccessFile;->close()V

    const/4 v10, 0x0

    iput-object v10, p0, Lmiui/util/HanziToPinyinHelper;->mFile:Ljava/io/RandomAccessFile;
    :try_end_fb
    .catch Ljava/lang/Exception; {:try_start_f3 .. :try_end_fb} :catch_103

    :goto_fb
    new-instance v10, Ljava/lang/RuntimeException;

    const-string v11, "Please update the data file: unicode_py_index.td"

    invoke-direct {v10, v11}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v10

    :catch_103
    move-exception v2

    .restart local v2       #e2:Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_fb

    .end local v2           #e2:Ljava/lang/Exception;
    :cond_108
    :try_start_108
    const-string v10, "HanziToPinyinHelper"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    sub-long/2addr v12, v7

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "]ms to load data file"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_12b
    .catchall {:try_start_108 .. :try_end_12b} :catchall_1b6
    .catch Ljava/io/FileNotFoundException; {:try_start_108 .. :try_end_12b} :catch_146
    .catch Ljava/io/IOException; {:try_start_108 .. :try_end_12b} :catch_17b

    if-nez v6, :cond_1d3

    iget-object v10, p0, Lmiui/util/HanziToPinyinHelper;->mFile:Ljava/io/RandomAccessFile;

    if-eqz v10, :cond_1d3

    :try_start_131
    iget-object v10, p0, Lmiui/util/HanziToPinyinHelper;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v10}, Ljava/io/RandomAccessFile;->close()V

    const/4 v10, 0x0

    iput-object v10, p0, Lmiui/util/HanziToPinyinHelper;->mFile:Ljava/io/RandomAccessFile;
    :try_end_139
    .catch Ljava/lang/Exception; {:try_start_131 .. :try_end_139} :catch_141

    :goto_139
    new-instance v10, Ljava/lang/RuntimeException;

    const-string v11, "Please update the data file: unicode_py_index.td"

    invoke-direct {v10, v11}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v10

    :catch_141
    move-exception v2

    .restart local v2       #e2:Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_139

    .end local v0           #digestBytes:[B
    .end local v2           #e2:Ljava/lang/Exception;
    .end local v3           #file:Ljava/io/File;
    .end local v4           #fileTag:[B
    .end local v7           #times:J
    .end local v9           #version:I
    :catch_146
    move-exception v1

    .local v1, e:Ljava/io/FileNotFoundException;
    :try_start_147
    const-string v10, "HanziToPinyinHelper"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Can\'t find "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_15f
    .catchall {:try_start_147 .. :try_end_15f} :catchall_1b6

    const/4 v6, 0x0

    if-nez v6, :cond_1d3

    iget-object v10, p0, Lmiui/util/HanziToPinyinHelper;->mFile:Ljava/io/RandomAccessFile;

    if-eqz v10, :cond_1d3

    :try_start_166
    iget-object v10, p0, Lmiui/util/HanziToPinyinHelper;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v10}, Ljava/io/RandomAccessFile;->close()V

    const/4 v10, 0x0

    iput-object v10, p0, Lmiui/util/HanziToPinyinHelper;->mFile:Ljava/io/RandomAccessFile;
    :try_end_16e
    .catch Ljava/lang/Exception; {:try_start_166 .. :try_end_16e} :catch_176

    :goto_16e
    new-instance v10, Ljava/lang/RuntimeException;

    const-string v11, "Please update the data file: unicode_py_index.td"

    invoke-direct {v10, v11}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v10

    :catch_176
    move-exception v2

    .restart local v2       #e2:Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_16e

    .end local v1           #e:Ljava/io/FileNotFoundException;
    .end local v2           #e2:Ljava/lang/Exception;
    :catch_17b
    move-exception v1

    .local v1, e:Ljava/io/IOException;
    :try_start_17c
    const-string v10, "HanziToPinyinHelper"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Can\'t read "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "IOException"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_19a
    .catchall {:try_start_17c .. :try_end_19a} :catchall_1b6

    const/4 v6, 0x0

    if-nez v6, :cond_1d3

    iget-object v10, p0, Lmiui/util/HanziToPinyinHelper;->mFile:Ljava/io/RandomAccessFile;

    if-eqz v10, :cond_1d3

    :try_start_1a1
    iget-object v10, p0, Lmiui/util/HanziToPinyinHelper;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v10}, Ljava/io/RandomAccessFile;->close()V

    const/4 v10, 0x0

    iput-object v10, p0, Lmiui/util/HanziToPinyinHelper;->mFile:Ljava/io/RandomAccessFile;
    :try_end_1a9
    .catch Ljava/lang/Exception; {:try_start_1a1 .. :try_end_1a9} :catch_1b1

    :goto_1a9
    new-instance v10, Ljava/lang/RuntimeException;

    const-string v11, "Please update the data file: unicode_py_index.td"

    invoke-direct {v10, v11}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v10

    :catch_1b1
    move-exception v2

    .restart local v2       #e2:Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1a9

    .end local v1           #e:Ljava/io/IOException;
    .end local v2           #e2:Ljava/lang/Exception;
    :catchall_1b6
    move-exception v10

    if-nez v6, :cond_1d2

    iget-object v11, p0, Lmiui/util/HanziToPinyinHelper;->mFile:Ljava/io/RandomAccessFile;

    if-eqz v11, :cond_1d2

    :try_start_1bd
    iget-object v10, p0, Lmiui/util/HanziToPinyinHelper;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v10}, Ljava/io/RandomAccessFile;->close()V

    const/4 v10, 0x0

    iput-object v10, p0, Lmiui/util/HanziToPinyinHelper;->mFile:Ljava/io/RandomAccessFile;
    :try_end_1c5
    .catch Ljava/lang/Exception; {:try_start_1bd .. :try_end_1c5} :catch_1cd

    :goto_1c5
    new-instance v10, Ljava/lang/RuntimeException;

    const-string v11, "Please update the data file: unicode_py_index.td"

    invoke-direct {v10, v11}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v10

    :catch_1cd
    move-exception v2

    .restart local v2       #e2:Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1c5

    .end local v2           #e2:Ljava/lang/Exception;
    :cond_1d2
    throw v10

    :cond_1d3
    return-void
.end method


# virtual methods
.method protected finalize()V
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    iget-object v1, p0, Lmiui/util/HanziToPinyinHelper;->mFile:Ljava/io/RandomAccessFile;

    if-eqz v1, :cond_9

    :try_start_4
    iget-object v1, p0, Lmiui/util/HanziToPinyinHelper;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_9} :catch_d

    :cond_9
    :goto_9
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    return-void

    :catch_d
    move-exception v0

    .local v0, e:Ljava/io/IOException;
    const-string v1, "HanziToPinyinHelper"

    const-string v2, "finalize IOException"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_9
.end method

.method public getPinyinString(C)[Ljava/lang/String;
    .registers 11
    .parameter "ch"

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    .local v2, pyString:Ljava/lang/String;
    add-int/lit16 v6, p1, -0x4e00

    mul-int/lit8 v6, v6, 0x2

    :try_start_6
    sget v7, Lmiui/util/HanziToPinyinHelper;->FILE_HEADER_SIZE:I

    add-int/2addr v6, v7

    int-to-long v3, v6

    .local v3, sp:J
    iget-object v6, p0, Lmiui/util/HanziToPinyinHelper;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v6, v3, v4}, Ljava/io/RandomAccessFile;->seek(J)V

    iget-object v6, p0, Lmiui/util/HanziToPinyinHelper;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->readShort()S

    move-result v1

    .local v1, offset:I
    if-nez v1, :cond_18

    .end local v1           #offset:I
    .end local v3           #sp:J
    :goto_17
    return-object v5

    .restart local v1       #offset:I
    .restart local v3       #sp:J
    :cond_18
    sget-object v6, Lmiui/util/AllPinyinConstants;->ALL_PINYIN:[Ljava/lang/String;

    aget-object v2, v6, v1

    const-string v6, ","

    invoke-virtual {v2, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_21
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_21} :catch_23
    .catch Ljava/lang/NullPointerException; {:try_start_6 .. :try_end_21} :catch_31

    move-result-object v5

    goto :goto_17

    .end local v1           #offset:I
    .end local v3           #sp:J
    :catch_23
    move-exception v0

    .local v0, e:Ljava/io/IOException;
    const-string v6, "HanziToPinyinHelper"

    const-string v7, "IO:"

    new-instance v8, Ljava/lang/Exception;

    invoke-direct {v8}, Ljava/lang/Exception;-><init>()V

    invoke-static {v6, v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_17

    .end local v0           #e:Ljava/io/IOException;
    :catch_31
    move-exception v0

    .local v0, e:Ljava/lang/NullPointerException;
    const-string v6, "HanziToPinyinHelper"

    const-string v7, "Unmatch Error:Please rebuild the system to regenerate the data file: unicode_py_index.td"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_17
.end method
