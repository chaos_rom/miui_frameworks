.class public Lmiui/util/LocaleUtils;
.super Ljava/lang/Object;
.source "LocaleUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/util/LocaleUtils$1;,
        Lmiui/util/LocaleUtils$NameSplitter;,
        Lmiui/util/LocaleUtils$ChineseLocaleUtils;,
        Lmiui/util/LocaleUtils$NameStyle;,
        Lmiui/util/LocaleUtils$LocaleUtilsBase;
    }
.end annotation


# static fields
.field private static final JAPANESE_LANGUAGE:Ljava/lang/String;

.field private static final KOREAN_LANGUAGE:Ljava/lang/String;

.field private static sSingleton:Lmiui/util/LocaleUtils;


# instance fields
.field private mBase:Lmiui/util/LocaleUtils$LocaleUtilsBase;

.field private mLanguage:Ljava/lang/String;

.field private mUtils:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lmiui/util/LocaleUtils$LocaleUtilsBase;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    sget-object v0, Ljava/util/Locale;->JAPANESE:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lmiui/util/LocaleUtils;->JAPANESE_LANGUAGE:Ljava/lang/String;

    sget-object v0, Ljava/util/Locale;->KOREAN:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lmiui/util/LocaleUtils;->KOREAN_LANGUAGE:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lmiui/util/LocaleUtils;->mUtils:Ljava/util/HashMap;

    new-instance v0, Lmiui/util/LocaleUtils$LocaleUtilsBase;

    invoke-direct {v0, p0, v1}, Lmiui/util/LocaleUtils$LocaleUtilsBase;-><init>(Lmiui/util/LocaleUtils;Lmiui/util/LocaleUtils$1;)V

    iput-object v0, p0, Lmiui/util/LocaleUtils;->mBase:Lmiui/util/LocaleUtils$LocaleUtilsBase;

    invoke-direct {p0, v1}, Lmiui/util/LocaleUtils;->setLocale(Ljava/util/Locale;)V

    return-void
.end method

.method private declared-synchronized get(Ljava/lang/Integer;)Lmiui/util/LocaleUtils$LocaleUtilsBase;
    .registers 5
    .parameter "nameStyle"

    .prologue
    monitor-enter p0

    :try_start_1
    iget-object v1, p0, Lmiui/util/LocaleUtils;->mUtils:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/util/LocaleUtils$LocaleUtilsBase;

    .local v0, utils:Lmiui/util/LocaleUtils$LocaleUtilsBase;
    if-nez v0, :cond_1d

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1d

    new-instance v0, Lmiui/util/LocaleUtils$ChineseLocaleUtils;

    .end local v0           #utils:Lmiui/util/LocaleUtils$LocaleUtilsBase;
    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lmiui/util/LocaleUtils$ChineseLocaleUtils;-><init>(Lmiui/util/LocaleUtils;Lmiui/util/LocaleUtils$1;)V

    .restart local v0       #utils:Lmiui/util/LocaleUtils$LocaleUtilsBase;
    iget-object v1, p0, Lmiui/util/LocaleUtils;->mUtils:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1d
    if-nez v0, :cond_21

    iget-object v0, p0, Lmiui/util/LocaleUtils;->mBase:Lmiui/util/LocaleUtils$LocaleUtilsBase;
    :try_end_21
    .catchall {:try_start_1 .. :try_end_21} :catchall_23

    .end local v0           #utils:Lmiui/util/LocaleUtils$LocaleUtilsBase;
    :cond_21
    monitor-exit p0

    return-object v0

    :catchall_23
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private getAdjustedStyle(I)I
    .registers 4
    .parameter "nameStyle"

    .prologue
    const/4 v0, 0x2

    if-ne p1, v0, :cond_18

    sget-object v0, Lmiui/util/LocaleUtils;->JAPANESE_LANGUAGE:Ljava/lang/String;

    iget-object v1, p0, Lmiui/util/LocaleUtils;->mLanguage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_18

    sget-object v0, Lmiui/util/LocaleUtils;->KOREAN_LANGUAGE:Ljava/lang/String;

    iget-object v1, p0, Lmiui/util/LocaleUtils;->mLanguage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_18

    const/4 p1, 0x3

    .end local p1
    :cond_18
    return p1
.end method

.method private getForSort(Ljava/lang/Integer;)Lmiui/util/LocaleUtils$LocaleUtilsBase;
    .registers 3
    .parameter "nameStyle"

    .prologue
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lmiui/util/LocaleUtils;->getAdjustedStyle(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, v0}, Lmiui/util/LocaleUtils;->get(Ljava/lang/Integer;)Lmiui/util/LocaleUtils$LocaleUtilsBase;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized getIntance()Lmiui/util/LocaleUtils;
    .registers 2

    .prologue
    const-class v1, Lmiui/util/LocaleUtils;

    monitor-enter v1

    :try_start_3
    sget-object v0, Lmiui/util/LocaleUtils;->sSingleton:Lmiui/util/LocaleUtils;

    if-nez v0, :cond_e

    new-instance v0, Lmiui/util/LocaleUtils;

    invoke-direct {v0}, Lmiui/util/LocaleUtils;-><init>()V

    sput-object v0, Lmiui/util/LocaleUtils;->sSingleton:Lmiui/util/LocaleUtils;

    :cond_e
    sget-object v0, Lmiui/util/LocaleUtils;->sSingleton:Lmiui/util/LocaleUtils;
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_12

    monitor-exit v1

    return-object v0

    :catchall_12
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private setLocale(Ljava/util/Locale;)V
    .registers 3
    .parameter "currentLocale"

    .prologue
    if-nez p1, :cond_11

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmiui/util/LocaleUtils;->mLanguage:Ljava/lang/String;

    :goto_10
    return-void

    :cond_11
    invoke-virtual {p1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmiui/util/LocaleUtils;->mLanguage:Ljava/lang/String;

    goto :goto_10
.end method


# virtual methods
.method public getSortKey(Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter "displayName"

    .prologue
    invoke-static {p1}, Lmiui/util/LocaleUtils$NameSplitter;->guessFullNameStyle(Ljava/lang/String;)I

    move-result v0

    .local v0, nameStyle:I
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {p0, v2}, Lmiui/util/LocaleUtils;->getForSort(Ljava/lang/Integer;)Lmiui/util/LocaleUtils$LocaleUtilsBase;

    move-result-object v2

    invoke-virtual {v2, p1}, Lmiui/util/LocaleUtils$LocaleUtilsBase;->getSortKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .local v1, s:Ljava/lang/String;
    return-object v1
.end method
