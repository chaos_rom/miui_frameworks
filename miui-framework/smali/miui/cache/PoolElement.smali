.class public Lmiui/cache/PoolElement;
.super Ljava/lang/Object;
.source "PoolElement.java"

# interfaces
.implements Landroid/util/Poolable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<D:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Landroid/util/Poolable",
        "<TD;>;"
    }
.end annotation


# instance fields
.field private mNext:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TD;"
        }
    .end annotation
.end field

.field private mPooled:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .local p0, this:Lmiui/cache/PoolElement;,"Lmiui/cache/PoolElement<TD;>;"
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiui/cache/PoolElement;->mPooled:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lmiui/cache/PoolElement;->mNext:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public getNextPoolable()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TD;"
        }
    .end annotation

    .prologue
    .local p0, this:Lmiui/cache/PoolElement;,"Lmiui/cache/PoolElement<TD;>;"
    iget-object v0, p0, Lmiui/cache/PoolElement;->mNext:Ljava/lang/Object;

    return-object v0
.end method

.method public isPooled()Z
    .registers 2

    .prologue
    .local p0, this:Lmiui/cache/PoolElement;,"Lmiui/cache/PoolElement<TD;>;"
    iget-boolean v0, p0, Lmiui/cache/PoolElement;->mPooled:Z

    return v0
.end method

.method public setNextPoolable(Ljava/lang/Object;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TD;)V"
        }
    .end annotation

    .prologue
    .local p0, this:Lmiui/cache/PoolElement;,"Lmiui/cache/PoolElement<TD;>;"
    .local p1, element:Ljava/lang/Object;,"TD;"
    iput-object p1, p0, Lmiui/cache/PoolElement;->mNext:Ljava/lang/Object;

    return-void
.end method

.method public setPooled(Z)V
    .registers 2
    .parameter "isPooled"

    .prologue
    .local p0, this:Lmiui/cache/PoolElement;,"Lmiui/cache/PoolElement<TD;>;"
    iput-boolean p1, p0, Lmiui/cache/PoolElement;->mPooled:Z

    return-void
.end method
