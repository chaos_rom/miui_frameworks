.class Lmiui/cache/AsyncLIFORequestManager$Worker$LoopComputer;
.super Ljava/lang/Object;
.source "AsyncLIFORequestManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/cache/AsyncLIFORequestManager$Worker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "LoopComputer"
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/cache/AsyncLIFORequestManager$Worker;


# direct methods
.method constructor <init>(Lmiui/cache/AsyncLIFORequestManager$Worker;)V
    .registers 2
    .parameter

    .prologue
    .local p0, this:Lmiui/cache/AsyncLIFORequestManager$Worker$LoopComputer;,"Lmiui/cache/AsyncLIFORequestManager$Worker<TK;TV;TR;>.LoopComputer;"
    iput-object p1, p0, Lmiui/cache/AsyncLIFORequestManager$Worker$LoopComputer;->this$0:Lmiui/cache/AsyncLIFORequestManager$Worker;

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 5

    .prologue
    .local p0, this:Lmiui/cache/AsyncLIFORequestManager$Worker$LoopComputer;,"Lmiui/cache/AsyncLIFORequestManager$Worker<TK;TV;TR;>.LoopComputer;"
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->toString()Ljava/lang/String;

    move-result-object v0

    .local v0, name:Ljava/lang/String;
    :cond_8
    :goto_8
    :try_start_8
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v2

    if-nez v2, :cond_2b

    iget-object v2, p0, Lmiui/cache/AsyncLIFORequestManager$Worker$LoopComputer;->this$0:Lmiui/cache/AsyncLIFORequestManager$Worker;

    iget-boolean v2, v2, Lmiui/cache/AsyncLIFORequestManager$Worker;->mActive:Z

    if-nez v2, :cond_2d

    iget-object v2, p0, Lmiui/cache/AsyncLIFORequestManager$Worker$LoopComputer;->this$0:Lmiui/cache/AsyncLIFORequestManager$Worker;

    iget-object v3, v2, Lmiui/cache/AsyncLIFORequestManager$Worker;->mLock:Ljava/lang/Object;

    monitor-enter v3
    :try_end_19
    .catchall {:try_start_8 .. :try_end_19} :catchall_4b
    .catch Ljava/lang/InterruptedException; {:try_start_8 .. :try_end_19} :catch_2a

    :goto_19
    :try_start_19
    iget-object v2, p0, Lmiui/cache/AsyncLIFORequestManager$Worker$LoopComputer;->this$0:Lmiui/cache/AsyncLIFORequestManager$Worker;

    iget-boolean v2, v2, Lmiui/cache/AsyncLIFORequestManager$Worker;->mActive:Z

    if-nez v2, :cond_2c

    iget-object v2, p0, Lmiui/cache/AsyncLIFORequestManager$Worker$LoopComputer;->this$0:Lmiui/cache/AsyncLIFORequestManager$Worker;

    iget-object v2, v2, Lmiui/cache/AsyncLIFORequestManager$Worker;->mLock:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->wait()V

    goto :goto_19

    :catchall_27
    move-exception v2

    monitor-exit v3
    :try_end_29
    .catchall {:try_start_19 .. :try_end_29} :catchall_27

    :try_start_29
    throw v2
    :try_end_2a
    .catchall {:try_start_29 .. :try_end_2a} :catchall_4b
    .catch Ljava/lang/InterruptedException; {:try_start_29 .. :try_end_2a} :catch_2a

    :catch_2a
    move-exception v2

    :cond_2b
    return-void

    :cond_2c
    :try_start_2c
    monitor-exit v3
    :try_end_2d
    .catchall {:try_start_2c .. :try_end_2d} :catchall_27

    :cond_2d
    :try_start_2d
    iget-object v2, p0, Lmiui/cache/AsyncLIFORequestManager$Worker$LoopComputer;->this$0:Lmiui/cache/AsyncLIFORequestManager$Worker;

    iget-object v2, v2, Lmiui/cache/AsyncLIFORequestManager$Worker;->mDeque:Ljava/util/concurrent/BlockingDeque;

    invoke-interface {v2}, Ljava/util/concurrent/BlockingDeque;->take()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/cache/RequestManager$Request;

    .local v1, r:Lmiui/cache/RequestManager$Request;,"Lmiui/cache/RequestManager$Request<TK;TV;TR;>;"
    if-eqz v1, :cond_8

    iget-object v2, p0, Lmiui/cache/AsyncLIFORequestManager$Worker$LoopComputer;->this$0:Lmiui/cache/AsyncLIFORequestManager$Worker;

    iget-boolean v2, v2, Lmiui/cache/AsyncLIFORequestManager$Worker;->mActive:Z

    if-eqz v2, :cond_4d

    iget-object v2, p0, Lmiui/cache/AsyncLIFORequestManager$Worker$LoopComputer;->this$0:Lmiui/cache/AsyncLIFORequestManager$Worker;

    iget-object v2, v2, Lmiui/cache/AsyncLIFORequestManager$Worker;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lmiui/cache/AsyncLIFORequestManager$Worker$LoopComputer;->this$0:Lmiui/cache/AsyncLIFORequestManager$Worker;

    iget-object v3, v3, Lmiui/cache/AsyncLIFORequestManager$Worker;->mPool:Landroid/util/Pool;

    invoke-static {v2, v1, v3}, Lmiui/cache/AsyncLIFORequestManager;->onComputeAsync(Landroid/os/Handler;Lmiui/cache/RequestManager$Request;Landroid/util/Pool;)V
    :try_end_4a
    .catchall {:try_start_2d .. :try_end_4a} :catchall_4b
    .catch Ljava/lang/InterruptedException; {:try_start_2d .. :try_end_4a} :catch_2a

    goto :goto_8

    .end local v1           #r:Lmiui/cache/RequestManager$Request;,"Lmiui/cache/RequestManager$Request<TK;TV;TR;>;"
    :catchall_4b
    move-exception v2

    throw v2

    .restart local v1       #r:Lmiui/cache/RequestManager$Request;,"Lmiui/cache/RequestManager$Request<TK;TV;TR;>;"
    :cond_4d
    :try_start_4d
    iget-object v2, p0, Lmiui/cache/AsyncLIFORequestManager$Worker$LoopComputer;->this$0:Lmiui/cache/AsyncLIFORequestManager$Worker;

    iget-object v2, v2, Lmiui/cache/AsyncLIFORequestManager$Worker;->mDeque:Ljava/util/concurrent/BlockingDeque;

    invoke-interface {v2, v1}, Ljava/util/concurrent/BlockingDeque;->addFirst(Ljava/lang/Object;)V
    :try_end_54
    .catchall {:try_start_4d .. :try_end_54} :catchall_4b
    .catch Ljava/lang/InterruptedException; {:try_start_4d .. :try_end_54} :catch_2a

    goto :goto_8
.end method
