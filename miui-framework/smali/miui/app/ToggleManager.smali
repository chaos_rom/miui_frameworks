.class public Lmiui/app/ToggleManager;
.super Ljava/lang/Object;
.source "ToggleManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/app/ToggleManager$WifiStateTracker;,
        Lmiui/app/ToggleManager$StateTracker;,
        Lmiui/app/ToggleManager$OnToggleOrderChangedListener;,
        Lmiui/app/ToggleManager$OnToggleChangedListener;
    }
.end annotation


# static fields
.field public static final ALPHA_DEFAULT:I = 0xff

.field public static final ALPHA_HALF:I = 0x7d

.field public static final DEFAULT_BACKLIGHT:I = 0x66

.field private static final DIVIDER_FIXED_POSITION_IN_PAGE_STYLE:I = 0xb

.field public static final EXTRA_LOCATE_DIVIDER:Ljava/lang/String; = "com.android.systemui.settings.EXTRA_LOCATE_DIVIDER"

.field public static final KEY_TOGGLE_COLLAPSE_AFTER_CLICKED:Ljava/lang/String; = "toggle_collapse_after_clicked"

.field public static final MAXIMUM_BACKLIGHT:I = 0xff

#the value of this static final field might be set in the static constructor
.field public static final MINIMUM_BACKLIGHT:I = 0x0

.field private static final MINIMUM_COUNT_OF_TOGGLES_IN_STATUS_BAR:I = 0x4

#the value of this static final field might be set in the static constructor
.field public static final RANGE:I = 0x0

.field private static final STATE_DISABLED:I = 0x0

.field private static final STATE_ENABLED:I = 0x1

.field private static final STATE_INTERMEDIATE:I = 0x5

.field private static final STATE_TURNING_OFF:I = 0x3

.field private static final STATE_TURNING_ON:I = 0x2

.field private static final STATE_UNKNOWN:I = 0x4

.field static final TAG:Ljava/lang/String; = "ToggleManager"

.field public static final TOGGLE_ACCESS_CONTROL:I = 0x10

.field public static final TOGGLE_ADVANCED_SYNC:I = 0x13

.field public static final TOGGLE_AUTO_BRIGHTNESS:I = 0x16

.field public static final TOGGLE_BLUETOOTH:I = 0x2

.field public static final TOGGLE_BRIGHTNESS:I = 0x4

.field public static final TOGGLE_COUNT:I = 0x18

.field public static final TOGGLE_DATA:I = 0x1

.field public static final TOGGLE_DIVIDER:I = 0x0

.field public static final TOGGLE_DRIVE_MODE:I = 0x15

.field public static final TOGGLE_FLIGHT_MODE:I = 0x9

.field public static final TOGGLE_GPS:I = 0x7

.field public static final TOGGLE_LOCK:I = 0xa

.field public static final TOGGLE_NETWORK_TYPE:I = 0x11

.field public static final TOGGLE_POWER_MODE:I = 0x17

.field public static final TOGGLE_PRIVACY_MODE:I = 0xe

.field public static final TOGGLE_REBOOT:I = 0xc

.field public static final TOGGLE_RINGER:I = 0x5

.field public static final TOGGLE_ROTATE:I = 0x3

.field public static final TOGGLE_SCREENSHOT:I = 0x12

.field public static final TOGGLE_SCREEN_BUTTON:I = 0x14

.field public static final TOGGLE_SHUTDOWN:I = 0xd

.field public static final TOGGLE_SYNC:I = 0x8

.field public static final TOGGLE_TORCH:I = 0xb

.field public static final TOGGLE_VIBRATE:I = 0x6

.field public static final TOGGLE_WIFI:I = 0xf

.field private static sContext:Landroid/content/Context;

.field private static sLongClickActions:[I

.field private static sTextColorDisable:I

.field private static sTextColorEnable:I

.field private static sToggleAlpha:[I

.field private static sToggleGeneralImages:[I

.field private static sToggleImages:[I

.field private static sToggleManager:Lmiui/app/ToggleManager;

.field private static sToggleNames:[I

.field private static sToggleStatus:[Z


# instance fields
.field private mAccelerometer:Z

.field private final mAccelerometerRotationObserver:Landroid/database/ContentObserver;

.field private mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mBluetoothEnable:Z

.field private mBluetoothEnabling:Z

.field private mBrightnessAutoAvailable:Z

.field private mBrightnessAutoLevel:F

.field private mBrightnessAutoMode:Z

.field private mBrightnessManualLevel:I

.field private final mBrightnessObserver:Landroid/database/ContentObserver;

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mDriveMode:Z

.field private final mDriveModeObserver:Landroid/database/ContentObserver;

.field private mEnableNetType:Z

.field private mFlightMode:Z

.field private final mFlightModeObserver:Landroid/database/ContentObserver;

.field private mGpsEnable:Z

.field private final mHandler:Landroid/os/Handler;

.field private final mLocationAllowedObserver:Landroid/database/ContentObserver;

.field private mMobileDataEnable:Z

.field private final mMobileDataEnableObserver:Landroid/database/ContentObserver;

.field private mMobilePolicyEnable:Z

.field private final mMobilePolicyEnableObserver:Landroid/database/ContentObserver;

.field private mPowerMode:Ljava/lang/String;

.field private final mPowerModeObserver:Landroid/database/ContentObserver;

.field private mPrivacyMode:Z

.field private final mPrivacyModeObserver:Landroid/database/ContentObserver;

.field private final mResolver:Landroid/content/ContentResolver;

.field private final mResource:Landroid/content/res/Resources;

.field private mScreenButtonDisabled:Z

.field private final mScreenButtonStateObserver:Landroid/database/ContentObserver;

.field private final mSecurityHelper:Lmiui/security/ChooseLockSettingsHelper;

.field private mStatusChangeListenerHandle:Ljava/lang/Object;

.field private final mSyncStatusObserver:Landroid/content/SyncStatusObserver;

.field private final mTogglOrderObserver:Landroid/database/ContentObserver;

.field private mToggleChangedListener:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lmiui/app/ToggleManager$OnToggleChangedListener;",
            ">;>;"
        }
    .end annotation
.end field

.field private mToggleOrderChangedListener:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lmiui/app/ToggleManager$OnToggleOrderChangedListener;",
            ">;"
        }
    .end annotation
.end field

.field private mTorchEnable:Z

.field private final mTorchEnableObserver:Landroid/database/ContentObserver;

.field private final mUpdateSyncStateRunnable:Ljava/lang/Runnable;

.field private final mVibrateEnableObserver:Landroid/database/ContentObserver;

.field private final mWifiState:Lmiui/app/ToggleManager$StateTracker;


# direct methods
.method static constructor <clinit>()V
    .registers 50

    .prologue
    const/16 v1, 0x18

    new-array v1, v1, [I

    sput-object v1, Lmiui/app/ToggleManager;->sToggleNames:[I

    sget-object v1, Lmiui/app/ToggleManager;->sToggleNames:[I

    const/16 v2, 0x10

    const v3, 0x60c0201

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sToggleNames:[I

    const/16 v2, 0x13

    const v3, 0x60c0211

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sToggleNames:[I

    const/16 v2, 0x16

    const v3, 0x60c0251

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sToggleNames:[I

    const/4 v2, 0x2

    const v3, 0x60c0202

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sToggleNames:[I

    const/4 v2, 0x4

    const v3, 0x60c0203

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sToggleNames:[I

    const/4 v2, 0x1

    const v3, 0x60c0204

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sToggleNames:[I

    const/4 v2, 0x0

    const v3, 0x60c0205

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sToggleNames:[I

    const/16 v2, 0x15

    const v3, 0x60c024f

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sToggleNames:[I

    const/16 v2, 0x9

    const v3, 0x60c0206

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sToggleNames:[I

    const/4 v2, 0x7

    const v3, 0x60c0207

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sToggleNames:[I

    const/16 v2, 0xa

    const v3, 0x60c0208

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sToggleNames:[I

    const/4 v2, 0x5

    const v3, 0x60c0209

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sToggleNames:[I

    const/16 v2, 0x11

    const v3, 0x60c020a

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sToggleNames:[I

    const/16 v2, 0x17

    const v3, 0x60c0248

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sToggleNames:[I

    const/16 v2, 0xe

    const v3, 0x60c020b

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sToggleNames:[I

    const/16 v2, 0xc

    const v3, 0x60c020c

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sToggleNames:[I

    const/4 v2, 0x3

    const v3, 0x60c020d

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sToggleNames:[I

    const/16 v2, 0x14

    const v3, 0x60c020e

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sToggleNames:[I

    const/16 v2, 0x12

    const v3, 0x60c020f

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sToggleNames:[I

    const/16 v2, 0xd

    const v3, 0x60c0210

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sToggleNames:[I

    const/16 v2, 0x8

    const v3, 0x60c0211

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sToggleNames:[I

    const/16 v2, 0xb

    const v3, 0x60c0212

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sToggleNames:[I

    const/4 v2, 0x6

    const v3, 0x60c0213

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sToggleNames:[I

    const/16 v2, 0xf

    const v3, 0x60c0214

    aput v3, v1, v2

    const/16 v1, 0x18

    new-array v1, v1, [I

    sput-object v1, Lmiui/app/ToggleManager;->sLongClickActions:[I

    sget-object v1, Lmiui/app/ToggleManager;->sLongClickActions:[I

    const/16 v2, 0x13

    const v3, 0x60c021a

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sLongClickActions:[I

    const/16 v2, 0x16

    const v3, 0x60c021e

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sLongClickActions:[I

    const/4 v2, 0x1

    const v3, 0x60c021b

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sLongClickActions:[I

    const/4 v2, 0x0

    const v3, 0x60c021c

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sLongClickActions:[I

    const/16 v2, 0x15

    const v3, 0x60c0250

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sLongClickActions:[I

    const/4 v2, 0x2

    const v3, 0x60c021d

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sLongClickActions:[I

    const/4 v2, 0x4

    const v3, 0x60c021e

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sLongClickActions:[I

    const/16 v2, 0x9

    const v3, 0x60c021f

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sLongClickActions:[I

    const/4 v2, 0x7

    const v3, 0x60c0220

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sLongClickActions:[I

    const/4 v2, 0x5

    const v3, 0x60c0221

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sLongClickActions:[I

    const/16 v2, 0x11

    const v3, 0x60c0222

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sLongClickActions:[I

    const/16 v2, 0x17

    const v3, 0x60c0247

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sLongClickActions:[I

    const/4 v2, 0x3

    const v3, 0x60c0223

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sLongClickActions:[I

    const/16 v2, 0x8

    const v3, 0x60c0224

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sLongClickActions:[I

    const/4 v2, 0x6

    const v3, 0x60c0225

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sLongClickActions:[I

    const/16 v2, 0xf

    const v3, 0x60c0226

    aput v3, v1, v2

    const/16 v1, 0x18

    new-array v1, v1, [I

    sput-object v1, Lmiui/app/ToggleManager;->sToggleImages:[I

    sget-object v1, Lmiui/app/ToggleManager;->sToggleImages:[I

    const/16 v2, 0x10

    const v3, 0x60201c9

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sToggleImages:[I

    const/16 v2, 0x13

    const v3, 0x60201fa

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sToggleImages:[I

    const/16 v2, 0x16

    const v3, 0x60201ce

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sToggleImages:[I

    const/4 v2, 0x2

    const v3, 0x60201cd

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sToggleImages:[I

    const/4 v2, 0x4

    const v3, 0x60201d2

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sToggleImages:[I

    const/4 v2, 0x1

    const v3, 0x60201d4

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sToggleImages:[I

    const/4 v2, 0x0

    const v3, 0x60201d5

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sToggleImages:[I

    const/16 v2, 0x15

    const v3, 0x60202d4

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sToggleImages:[I

    const/16 v2, 0x9

    const v3, 0x60201d9

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sToggleImages:[I

    const/4 v2, 0x7

    const v3, 0x60201db

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sToggleImages:[I

    const/16 v2, 0xa

    const v3, 0x60201dc

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sToggleImages:[I

    const/4 v2, 0x5

    const v3, 0x60201de

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sToggleImages:[I

    const/16 v2, 0x11

    const v3, 0x60201df

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sToggleImages:[I

    const/16 v2, 0x17

    const v3, 0x60201ec

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sToggleImages:[I

    const/16 v2, 0xe

    const v3, 0x60201e1

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sToggleImages:[I

    const/16 v2, 0xc

    const v3, 0x60201e2

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sToggleImages:[I

    const/4 v2, 0x3

    const v3, 0x60201e5

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sToggleImages:[I

    const/16 v2, 0x14

    const v3, 0x60201e6

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sToggleImages:[I

    const/16 v2, 0x12

    const v3, 0x60201e8

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sToggleImages:[I

    const/16 v2, 0xd

    const v3, 0x60201ea

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sToggleImages:[I

    const/16 v2, 0x8

    const v3, 0x60201fa

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sToggleImages:[I

    const/16 v2, 0xb

    const v3, 0x60201d6

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sToggleImages:[I

    const/4 v2, 0x6

    const v3, 0x60201e9

    aput v3, v1, v2

    sget-object v1, Lmiui/app/ToggleManager;->sToggleImages:[I

    const/16 v2, 0xf

    const v3, 0x60201ec

    aput v3, v1, v2

    const/16 v1, 0x18

    new-array v1, v1, [I

    sput-object v1, Lmiui/app/ToggleManager;->sToggleGeneralImages:[I

    const/4 v0, 0x0

    .local v0, i:I
    :goto_241
    const/16 v1, 0x18

    if-ge v0, v1, :cond_250

    sget-object v1, Lmiui/app/ToggleManager;->sToggleGeneralImages:[I

    sget-object v2, Lmiui/app/ToggleManager;->sToggleImages:[I

    aget v2, v2, v0

    aput v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_241

    :cond_250
    const/16 v1, 0x18

    new-array v1, v1, [I

    sput-object v1, Lmiui/app/ToggleManager;->sToggleAlpha:[I

    sget-object v1, Lmiui/app/ToggleManager;->sToggleAlpha:[I

    const/16 v2, 0x10

    sget-object v3, Lmiui/app/ToggleManager;->sToggleAlpha:[I

    const/16 v4, 0x13

    sget-object v5, Lmiui/app/ToggleManager;->sToggleAlpha:[I

    const/16 v6, 0x16

    sget-object v7, Lmiui/app/ToggleManager;->sToggleAlpha:[I

    const/4 v8, 0x2

    sget-object v9, Lmiui/app/ToggleManager;->sToggleAlpha:[I

    const/4 v10, 0x4

    sget-object v11, Lmiui/app/ToggleManager;->sToggleAlpha:[I

    const/4 v12, 0x1

    sget-object v13, Lmiui/app/ToggleManager;->sToggleAlpha:[I

    const/4 v14, 0x0

    sget-object v15, Lmiui/app/ToggleManager;->sToggleAlpha:[I

    const/16 v16, 0x15

    sget-object v17, Lmiui/app/ToggleManager;->sToggleAlpha:[I

    const/16 v18, 0x9

    sget-object v19, Lmiui/app/ToggleManager;->sToggleAlpha:[I

    const/16 v20, 0x7

    sget-object v21, Lmiui/app/ToggleManager;->sToggleAlpha:[I

    const/16 v22, 0xa

    sget-object v23, Lmiui/app/ToggleManager;->sToggleAlpha:[I

    const/16 v24, 0x5

    sget-object v25, Lmiui/app/ToggleManager;->sToggleAlpha:[I

    const/16 v26, 0x11

    sget-object v27, Lmiui/app/ToggleManager;->sToggleAlpha:[I

    const/16 v28, 0x17

    sget-object v29, Lmiui/app/ToggleManager;->sToggleAlpha:[I

    const/16 v30, 0xe

    sget-object v31, Lmiui/app/ToggleManager;->sToggleAlpha:[I

    const/16 v32, 0xc

    sget-object v33, Lmiui/app/ToggleManager;->sToggleAlpha:[I

    const/16 v34, 0x3

    sget-object v35, Lmiui/app/ToggleManager;->sToggleAlpha:[I

    const/16 v36, 0x14

    sget-object v37, Lmiui/app/ToggleManager;->sToggleAlpha:[I

    const/16 v38, 0x12

    sget-object v39, Lmiui/app/ToggleManager;->sToggleAlpha:[I

    const/16 v40, 0xd

    sget-object v41, Lmiui/app/ToggleManager;->sToggleAlpha:[I

    const/16 v42, 0x8

    sget-object v43, Lmiui/app/ToggleManager;->sToggleAlpha:[I

    const/16 v44, 0xb

    sget-object v45, Lmiui/app/ToggleManager;->sToggleAlpha:[I

    const/16 v46, 0x6

    sget-object v47, Lmiui/app/ToggleManager;->sToggleAlpha:[I

    const/16 v48, 0xf

    const/16 v49, 0xff

    aput v49, v47, v48

    aput v49, v45, v46

    aput v49, v43, v44

    aput v49, v41, v42

    aput v49, v39, v40

    aput v49, v37, v38

    aput v49, v35, v36

    aput v49, v33, v34

    aput v49, v31, v32

    aput v49, v29, v30

    aput v49, v27, v28

    aput v49, v25, v26

    aput v49, v23, v24

    aput v49, v21, v22

    aput v49, v19, v20

    aput v49, v17, v18

    aput v49, v15, v16

    aput v49, v13, v14

    aput v49, v11, v12

    aput v49, v9, v10

    aput v49, v7, v8

    aput v49, v5, v6

    aput v49, v3, v4

    aput v49, v1, v2

    const/16 v1, 0x18

    new-array v1, v1, [Z

    sput-object v1, Lmiui/app/ToggleManager;->sToggleStatus:[Z

    const/4 v1, 0x0

    sput-object v1, Lmiui/app/ToggleManager;->sToggleManager:Lmiui/app/ToggleManager;

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x6080005

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lmiui/app/ToggleManager;->MINIMUM_BACKLIGHT:I

    sget v1, Lmiui/app/ToggleManager;->MINIMUM_BACKLIGHT:I

    rsub-int v1, v1, 0xff

    sput v1, Lmiui/app/ToggleManager;->RANGE:I

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .registers 9
    .parameter "context"

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    iput-object v2, p0, Lmiui/app/ToggleManager;->mHandler:Landroid/os/Handler;

    iput-boolean v6, p0, Lmiui/app/ToggleManager;->mEnableNetType:Z

    iput-object v3, p0, Lmiui/app/ToggleManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    new-instance v2, Lmiui/app/ToggleManager$WifiStateTracker;

    invoke-direct {v2, v3}, Lmiui/app/ToggleManager$WifiStateTracker;-><init>(Lmiui/app/ToggleManager$1;)V

    iput-object v2, p0, Lmiui/app/ToggleManager;->mWifiState:Lmiui/app/ToggleManager$StateTracker;

    new-instance v2, Lmiui/app/ToggleManager$1;

    invoke-direct {v2, p0}, Lmiui/app/ToggleManager$1;-><init>(Lmiui/app/ToggleManager;)V

    iput-object v2, p0, Lmiui/app/ToggleManager;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v2, Lmiui/app/ToggleManager$2;

    iget-object v3, p0, Lmiui/app/ToggleManager;->mHandler:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, Lmiui/app/ToggleManager$2;-><init>(Lmiui/app/ToggleManager;Landroid/os/Handler;)V

    iput-object v2, p0, Lmiui/app/ToggleManager;->mTogglOrderObserver:Landroid/database/ContentObserver;

    new-instance v2, Lmiui/app/ToggleManager$3;

    iget-object v3, p0, Lmiui/app/ToggleManager;->mHandler:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, Lmiui/app/ToggleManager$3;-><init>(Lmiui/app/ToggleManager;Landroid/os/Handler;)V

    iput-object v2, p0, Lmiui/app/ToggleManager;->mFlightModeObserver:Landroid/database/ContentObserver;

    new-instance v2, Lmiui/app/ToggleManager$4;

    iget-object v3, p0, Lmiui/app/ToggleManager;->mHandler:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, Lmiui/app/ToggleManager$4;-><init>(Lmiui/app/ToggleManager;Landroid/os/Handler;)V

    iput-object v2, p0, Lmiui/app/ToggleManager;->mDriveModeObserver:Landroid/database/ContentObserver;

    new-instance v2, Lmiui/app/ToggleManager$5;

    iget-object v3, p0, Lmiui/app/ToggleManager;->mHandler:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, Lmiui/app/ToggleManager$5;-><init>(Lmiui/app/ToggleManager;Landroid/os/Handler;)V

    iput-object v2, p0, Lmiui/app/ToggleManager;->mMobileDataEnableObserver:Landroid/database/ContentObserver;

    new-instance v2, Lmiui/app/ToggleManager$6;

    iget-object v3, p0, Lmiui/app/ToggleManager;->mHandler:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, Lmiui/app/ToggleManager$6;-><init>(Lmiui/app/ToggleManager;Landroid/os/Handler;)V

    iput-object v2, p0, Lmiui/app/ToggleManager;->mMobilePolicyEnableObserver:Landroid/database/ContentObserver;

    new-instance v2, Lmiui/app/ToggleManager$7;

    iget-object v3, p0, Lmiui/app/ToggleManager;->mHandler:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, Lmiui/app/ToggleManager$7;-><init>(Lmiui/app/ToggleManager;Landroid/os/Handler;)V

    iput-object v2, p0, Lmiui/app/ToggleManager;->mTorchEnableObserver:Landroid/database/ContentObserver;

    new-instance v2, Lmiui/app/ToggleManager$8;

    iget-object v3, p0, Lmiui/app/ToggleManager;->mHandler:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, Lmiui/app/ToggleManager$8;-><init>(Lmiui/app/ToggleManager;Landroid/os/Handler;)V

    iput-object v2, p0, Lmiui/app/ToggleManager;->mPrivacyModeObserver:Landroid/database/ContentObserver;

    new-instance v2, Lmiui/app/ToggleManager$9;

    iget-object v3, p0, Lmiui/app/ToggleManager;->mHandler:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, Lmiui/app/ToggleManager$9;-><init>(Lmiui/app/ToggleManager;Landroid/os/Handler;)V

    iput-object v2, p0, Lmiui/app/ToggleManager;->mScreenButtonStateObserver:Landroid/database/ContentObserver;

    new-instance v2, Lmiui/app/ToggleManager$10;

    iget-object v3, p0, Lmiui/app/ToggleManager;->mHandler:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, Lmiui/app/ToggleManager$10;-><init>(Lmiui/app/ToggleManager;Landroid/os/Handler;)V

    iput-object v2, p0, Lmiui/app/ToggleManager;->mLocationAllowedObserver:Landroid/database/ContentObserver;

    new-instance v2, Lmiui/app/ToggleManager$11;

    iget-object v3, p0, Lmiui/app/ToggleManager;->mHandler:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, Lmiui/app/ToggleManager$11;-><init>(Lmiui/app/ToggleManager;Landroid/os/Handler;)V

    iput-object v2, p0, Lmiui/app/ToggleManager;->mAccelerometerRotationObserver:Landroid/database/ContentObserver;

    new-instance v2, Lmiui/app/ToggleManager$12;

    iget-object v3, p0, Lmiui/app/ToggleManager;->mHandler:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, Lmiui/app/ToggleManager$12;-><init>(Lmiui/app/ToggleManager;Landroid/os/Handler;)V

    iput-object v2, p0, Lmiui/app/ToggleManager;->mBrightnessObserver:Landroid/database/ContentObserver;

    new-instance v2, Lmiui/app/ToggleManager$13;

    iget-object v3, p0, Lmiui/app/ToggleManager;->mHandler:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, Lmiui/app/ToggleManager$13;-><init>(Lmiui/app/ToggleManager;Landroid/os/Handler;)V

    iput-object v2, p0, Lmiui/app/ToggleManager;->mPowerModeObserver:Landroid/database/ContentObserver;

    new-instance v2, Lmiui/app/ToggleManager$14;

    iget-object v3, p0, Lmiui/app/ToggleManager;->mHandler:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, Lmiui/app/ToggleManager$14;-><init>(Lmiui/app/ToggleManager;Landroid/os/Handler;)V

    iput-object v2, p0, Lmiui/app/ToggleManager;->mVibrateEnableObserver:Landroid/database/ContentObserver;

    new-instance v2, Lmiui/app/ToggleManager$15;

    invoke-direct {v2, p0}, Lmiui/app/ToggleManager$15;-><init>(Lmiui/app/ToggleManager;)V

    iput-object v2, p0, Lmiui/app/ToggleManager;->mUpdateSyncStateRunnable:Ljava/lang/Runnable;

    new-instance v2, Lmiui/app/ToggleManager$16;

    invoke-direct {v2, p0}, Lmiui/app/ToggleManager$16;-><init>(Lmiui/app/ToggleManager;)V

    iput-object v2, p0, Lmiui/app/ToggleManager;->mSyncStatusObserver:Landroid/content/SyncStatusObserver;

    sput-object p1, Lmiui/app/ToggleManager;->sContext:Landroid/content/Context;

    sget-object v2, Lmiui/app/ToggleManager;->sContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iput-object v2, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    sget-object v2, Lmiui/app/ToggleManager;->sContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iput-object v2, p0, Lmiui/app/ToggleManager;->mResource:Landroid/content/res/Resources;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmiui/app/ToggleManager;->mToggleChangedListener:Ljava/util/List;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmiui/app/ToggleManager;->mToggleOrderChangedListener:Ljava/util/List;

    iget-object v2, p0, Lmiui/app/ToggleManager;->mResource:Landroid/content/res/Resources;

    const v3, 0x6090007

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    iput-boolean v2, p0, Lmiui/app/ToggleManager;->mBrightnessAutoAvailable:Z

    invoke-direct {p0}, Lmiui/app/ToggleManager;->queryBrightnessStatus()V

    :try_start_d0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "com.android.phone"

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v1

    .local v1, phoneResources:Landroid/content/res/Resources;
    const-string v2, "config_preferred_network_type_when_data_enabled"

    const-string v3, "bool"

    const-string v4, "com.android.phone"

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    iput-boolean v2, p0, Lmiui/app/ToggleManager;->mEnableNetType:Z
    :try_end_ea
    .catch Ljava/lang/Exception; {:try_start_d0 .. :try_end_ea} :catch_238

    .end local v1           #phoneResources:Landroid/content/res/Resources;
    :goto_ea
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .local v0, filter:Landroid/content/IntentFilter;
    const-string v2, "android.media.RINGER_MODE_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.net.wifi.SCAN_RESULTS"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    sget-object v2, Lmiui/app/ToggleManager;->sContext:Landroid/content/Context;

    iget-object v3, p0, Lmiui/app/ToggleManager;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-virtual {p0}, Lmiui/app/ToggleManager;->updateBluetoothToggle()V

    invoke-virtual {p0}, Lmiui/app/ToggleManager;->updateRingerToggle()V

    invoke-virtual {p0}, Lmiui/app/ToggleManager;->updateWifiToggle()V

    iget-object v2, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    const-string v3, "status_bar_toggle_list"

    invoke-static {v3}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iget-object v4, p0, Lmiui/app/ToggleManager;->mTogglOrderObserver:Landroid/database/ContentObserver;

    invoke-virtual {v2, v3, v5, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v2, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    const-string v3, "status_bar_toggle_page"

    invoke-static {v3}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iget-object v4, p0, Lmiui/app/ToggleManager;->mTogglOrderObserver:Landroid/database/ContentObserver;

    invoke-virtual {v2, v3, v5, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    const v2, 0x7fffffff

    iget-object v3, p0, Lmiui/app/ToggleManager;->mSyncStatusObserver:Landroid/content/SyncStatusObserver;

    invoke-static {v2, v3}, Landroid/content/ContentResolver;->addStatusChangeListener(ILandroid/content/SyncStatusObserver;)Ljava/lang/Object;

    move-result-object v2

    iput-object v2, p0, Lmiui/app/ToggleManager;->mStatusChangeListenerHandle:Ljava/lang/Object;

    invoke-direct {p0}, Lmiui/app/ToggleManager;->updateAdvancedSyncToggle()V

    invoke-direct {p0}, Lmiui/app/ToggleManager;->updateSyncToggle()V

    iget-object v2, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    const-string v3, "accelerometer_rotation"

    invoke-static {v3}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iget-object v4, p0, Lmiui/app/ToggleManager;->mAccelerometerRotationObserver:Landroid/database/ContentObserver;

    invoke-virtual {v2, v3, v5, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    invoke-direct {p0}, Lmiui/app/ToggleManager;->updateAccelerometerToggle()V

    iget-object v2, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    const-string v3, "airplane_mode_on"

    invoke-static {v3}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iget-object v4, p0, Lmiui/app/ToggleManager;->mFlightModeObserver:Landroid/database/ContentObserver;

    invoke-virtual {v2, v3, v5, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    invoke-direct {p0}, Lmiui/app/ToggleManager;->updateFlightModeToggle()V

    iget-object v2, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    const-string v3, "drive_mode_drive_mode"

    invoke-static {v3}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iget-object v4, p0, Lmiui/app/ToggleManager;->mDriveModeObserver:Landroid/database/ContentObserver;

    invoke-virtual {v2, v3, v5, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    invoke-direct {p0}, Lmiui/app/ToggleManager;->updateDriveModeToggle()V

    iget-object v2, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    const-string v3, "location_providers_allowed"

    invoke-static {v3}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iget-object v4, p0, Lmiui/app/ToggleManager;->mLocationAllowedObserver:Landroid/database/ContentObserver;

    invoke-virtual {v2, v3, v5, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    invoke-direct {p0}, Lmiui/app/ToggleManager;->updateGpsToggle()V

    iget-object v2, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    const-string v3, "mobile_data"

    invoke-static {v3}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iget-object v4, p0, Lmiui/app/ToggleManager;->mMobileDataEnableObserver:Landroid/database/ContentObserver;

    invoke-virtual {v2, v3, v5, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v2, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    const-string v3, "mobile_policy"

    invoke-static {v3}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iget-object v4, p0, Lmiui/app/ToggleManager;->mMobilePolicyEnableObserver:Landroid/database/ContentObserver;

    invoke-virtual {v2, v3, v5, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v2, p0, Lmiui/app/ToggleManager;->mMobilePolicyEnableObserver:Landroid/database/ContentObserver;

    invoke-virtual {v2, v6}, Landroid/database/ContentObserver;->onChange(Z)V

    iget-object v2, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    const-string v3, "privacy_mode_enabled"

    invoke-static {v3}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iget-object v4, p0, Lmiui/app/ToggleManager;->mPrivacyModeObserver:Landroid/database/ContentObserver;

    invoke-virtual {v2, v3, v5, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    new-instance v2, Lmiui/security/ChooseLockSettingsHelper;

    sget-object v3, Lmiui/app/ToggleManager;->sContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lmiui/security/ChooseLockSettingsHelper;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lmiui/app/ToggleManager;->mSecurityHelper:Lmiui/security/ChooseLockSettingsHelper;

    invoke-direct {p0}, Lmiui/app/ToggleManager;->updatePrivacyModeToggle()V

    iget-object v2, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    const-string v3, "screen_brightness"

    invoke-static {v3}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iget-object v4, p0, Lmiui/app/ToggleManager;->mBrightnessObserver:Landroid/database/ContentObserver;

    invoke-virtual {v2, v3, v5, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v2, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    const-string v3, "screen_auto_brightness_adj"

    invoke-static {v3}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iget-object v4, p0, Lmiui/app/ToggleManager;->mBrightnessObserver:Landroid/database/ContentObserver;

    invoke-virtual {v2, v3, v5, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v2, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    const-string v3, "screen_brightness_mode"

    invoke-static {v3}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iget-object v4, p0, Lmiui/app/ToggleManager;->mBrightnessObserver:Landroid/database/ContentObserver;

    invoke-virtual {v2, v3, v5, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    invoke-direct {p0}, Lmiui/app/ToggleManager;->updateBrightnessToggle()V

    iget-object v2, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    const-string v3, "screen_buttons_state"

    invoke-static {v3}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iget-object v4, p0, Lmiui/app/ToggleManager;->mScreenButtonStateObserver:Landroid/database/ContentObserver;

    invoke-virtual {v2, v3, v5, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    invoke-direct {p0}, Lmiui/app/ToggleManager;->updateScreenButtonState()V

    iget-object v2, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    const-string v3, "torch_state"

    invoke-static {v3}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iget-object v4, p0, Lmiui/app/ToggleManager;->mTorchEnableObserver:Landroid/database/ContentObserver;

    invoke-virtual {v2, v3, v5, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    invoke-direct {p0}, Lmiui/app/ToggleManager;->updateTorchToggle()V

    iget-object v2, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    const-string v3, "vibrate_in_silent"

    invoke-static {v3}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iget-object v4, p0, Lmiui/app/ToggleManager;->mVibrateEnableObserver:Landroid/database/ContentObserver;

    invoke-virtual {v2, v3, v5, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v2, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    const-string v3, "vibrate_in_normal"

    invoke-static {v3}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iget-object v4, p0, Lmiui/app/ToggleManager;->mVibrateEnableObserver:Landroid/database/ContentObserver;

    invoke-virtual {v2, v3, v5, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    invoke-virtual {p0}, Lmiui/app/ToggleManager;->updateVibrateToggle()V

    iget-object v2, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    const-string v3, "power_mode"

    invoke-static {v3}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iget-object v4, p0, Lmiui/app/ToggleManager;->mPowerModeObserver:Landroid/database/ContentObserver;

    invoke-virtual {v2, v3, v5, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    invoke-direct {p0}, Lmiui/app/ToggleManager;->updatePowerModeToggle()V

    return-void

    .end local v0           #filter:Landroid/content/IntentFilter;
    :catch_238
    move-exception v2

    goto/16 :goto_ea
.end method

.method static synthetic access$100(Lmiui/app/ToggleManager;)Lmiui/app/ToggleManager$StateTracker;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/app/ToggleManager;->mWifiState:Lmiui/app/ToggleManager$StateTracker;

    return-object v0
.end method

.method static synthetic access$1000(Lmiui/app/ToggleManager;)V
    .registers 1
    .parameter "x0"

    .prologue
    invoke-direct {p0}, Lmiui/app/ToggleManager;->updateScreenButtonState()V

    return-void
.end method

.method static synthetic access$1100(Lmiui/app/ToggleManager;)V
    .registers 1
    .parameter "x0"

    .prologue
    invoke-direct {p0}, Lmiui/app/ToggleManager;->updateGpsToggle()V

    return-void
.end method

.method static synthetic access$1200(Lmiui/app/ToggleManager;)V
    .registers 1
    .parameter "x0"

    .prologue
    invoke-direct {p0}, Lmiui/app/ToggleManager;->updateAccelerometerToggle()V

    return-void
.end method

.method static synthetic access$1300(Lmiui/app/ToggleManager;)V
    .registers 1
    .parameter "x0"

    .prologue
    invoke-direct {p0}, Lmiui/app/ToggleManager;->queryBrightnessStatus()V

    return-void
.end method

.method static synthetic access$1400(Lmiui/app/ToggleManager;)V
    .registers 1
    .parameter "x0"

    .prologue
    invoke-direct {p0}, Lmiui/app/ToggleManager;->updateBrightnessToggle()V

    return-void
.end method

.method static synthetic access$1500(Lmiui/app/ToggleManager;)V
    .registers 1
    .parameter "x0"

    .prologue
    invoke-direct {p0}, Lmiui/app/ToggleManager;->updatePowerModeToggle()V

    return-void
.end method

.method static synthetic access$1600(Lmiui/app/ToggleManager;)V
    .registers 1
    .parameter "x0"

    .prologue
    invoke-direct {p0}, Lmiui/app/ToggleManager;->updateAdvancedSyncToggle()V

    return-void
.end method

.method static synthetic access$1700(Lmiui/app/ToggleManager;)V
    .registers 1
    .parameter "x0"

    .prologue
    invoke-direct {p0}, Lmiui/app/ToggleManager;->updateSyncToggle()V

    return-void
.end method

.method static synthetic access$1800(Lmiui/app/ToggleManager;)Ljava/lang/Runnable;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/app/ToggleManager;->mUpdateSyncStateRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$1900(Lmiui/app/ToggleManager;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/app/ToggleManager;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lmiui/app/ToggleManager;)V
    .registers 1
    .parameter "x0"

    .prologue
    invoke-direct {p0}, Lmiui/app/ToggleManager;->updateFlightModeToggle()V

    return-void
.end method

.method static synthetic access$2000(Lmiui/app/ToggleManager;)Lmiui/security/ChooseLockSettingsHelper;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/app/ToggleManager;->mSecurityHelper:Lmiui/security/ChooseLockSettingsHelper;

    return-object v0
.end method

.method static synthetic access$2100(Lmiui/app/ToggleManager;)Z
    .registers 2
    .parameter "x0"

    .prologue
    iget-boolean v0, p0, Lmiui/app/ToggleManager;->mPrivacyMode:Z

    return v0
.end method

.method static synthetic access$2102(Lmiui/app/ToggleManager;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    iput-boolean p1, p0, Lmiui/app/ToggleManager;->mPrivacyMode:Z

    return p1
.end method

.method static synthetic access$2200()Landroid/content/Context;
    .registers 1

    .prologue
    sget-object v0, Lmiui/app/ToggleManager;->sContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lmiui/app/ToggleManager;)Ljava/util/List;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/app/ToggleManager;->mToggleOrderChangedListener:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$400(Lmiui/app/ToggleManager;)V
    .registers 1
    .parameter "x0"

    .prologue
    invoke-direct {p0}, Lmiui/app/ToggleManager;->updateDriveModeToggle()V

    return-void
.end method

.method static synthetic access$500(Lmiui/app/ToggleManager;)V
    .registers 1
    .parameter "x0"

    .prologue
    invoke-direct {p0}, Lmiui/app/ToggleManager;->updateDataToggle()V

    return-void
.end method

.method static synthetic access$602(Lmiui/app/ToggleManager;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    iput-boolean p1, p0, Lmiui/app/ToggleManager;->mMobilePolicyEnable:Z

    return p1
.end method

.method static synthetic access$700(Lmiui/app/ToggleManager;)Landroid/content/ContentResolver;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    return-object v0
.end method

.method static synthetic access$800(Lmiui/app/ToggleManager;)V
    .registers 1
    .parameter "x0"

    .prologue
    invoke-direct {p0}, Lmiui/app/ToggleManager;->updateTorchToggle()V

    return-void
.end method

.method static synthetic access$900(Lmiui/app/ToggleManager;)V
    .registers 1
    .parameter "x0"

    .prologue
    invoke-direct {p0}, Lmiui/app/ToggleManager;->updatePrivacyModeToggle()V

    return-void
.end method

.method private static addIfUnselected(Ljava/util/ArrayList;I)V
    .registers 3
    .parameter
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .local p0, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_11

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_11
    return-void
.end method

.method public static createInstance(Landroid/content/Context;)Lmiui/app/ToggleManager;
    .registers 2
    .parameter "context"

    .prologue
    sget-object v0, Lmiui/app/ToggleManager;->sToggleManager:Lmiui/app/ToggleManager;

    if-nez v0, :cond_b

    new-instance v0, Lmiui/app/ToggleManager;

    invoke-direct {v0, p0}, Lmiui/app/ToggleManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lmiui/app/ToggleManager;->sToggleManager:Lmiui/app/ToggleManager;

    :cond_b
    sget-object v0, Lmiui/app/ToggleManager;->sToggleManager:Lmiui/app/ToggleManager;

    return-object v0
.end method

.method private ensureBluetoothAdapter()Z
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/ToggleManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-nez v0, :cond_a

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iput-object v0, p0, Lmiui/app/ToggleManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    :cond_a
    iget-object v0, p0, Lmiui/app/ToggleManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public static getAllToggles()Ljava/util/ArrayList;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x17

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .local v0, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/16 v1, 0x9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v1, 0x13

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v1, 0x12

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v1, 0xf

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v1, 0x16

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v1, 0x10

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v1, 0x11

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v1, 0xe

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v1, 0xb

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v1, 0x14

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v1, 0xa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v1, 0x15

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v1, 0xc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v1, 0xd

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method public static getDividerFixedPosition(Landroid/content/Context;)I
    .registers 2
    .parameter "context"

    .prologue
    invoke-static {p0}, Lmiui/app/ToggleManager;->isListStyle(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_8

    const/4 v0, 0x0

    :goto_7
    return v0

    :cond_8
    const/16 v0, 0xb

    goto :goto_7
.end method

.method public static getGeneralImage(I)I
    .registers 2
    .parameter "id"

    .prologue
    sget-object v0, Lmiui/app/ToggleManager;->sToggleGeneralImages:[I

    aget v0, v0, p0

    return v0
.end method

.method private static getImage(I)I
    .registers 2
    .parameter "id"

    .prologue
    sget-object v0, Lmiui/app/ToggleManager;->sToggleImages:[I

    aget v0, v0, p0

    return v0
.end method

.method public static getImageDrawable(I)Landroid/graphics/drawable/Drawable;
    .registers 4
    .parameter "id"

    .prologue
    sget-object v1, Lmiui/app/ToggleManager;->sContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {p0}, Lmiui/app/ToggleManager;->getImage(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .local v0, drawable:Landroid/graphics/drawable/Drawable;
    sget-object v1, Lmiui/app/ToggleManager;->sToggleAlpha:[I

    aget v1, v1, p0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    return-object v0
.end method

.method public static getName(I)I
    .registers 2
    .parameter "id"

    .prologue
    sget-object v0, Lmiui/app/ToggleManager;->sToggleNames:[I

    aget v0, v0, p0

    return v0
.end method

.method public static getStatus(I)Z
    .registers 2
    .parameter "id"

    .prologue
    sget-object v0, Lmiui/app/ToggleManager;->sToggleStatus:[Z

    aget-boolean v0, v0, p0

    return v0
.end method

.method private static getTextColor(I)I
    .registers 2
    .parameter "id"

    .prologue
    invoke-static {p0}, Lmiui/app/ToggleManager;->getStatus(I)Z

    move-result v0

    if-eqz v0, :cond_9

    sget v0, Lmiui/app/ToggleManager;->sTextColorEnable:I

    :goto_8
    return v0

    :cond_9
    sget v0, Lmiui/app/ToggleManager;->sTextColorDisable:I

    goto :goto_8
.end method

.method private static getToggleOrderSettingID(Landroid/content/Context;)Ljava/lang/String;
    .registers 2
    .parameter "context"

    .prologue
    invoke-static {p0}, Lmiui/app/ToggleManager;->isListStyle(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "status_bar_toggle_list"

    :goto_8
    return-object v0

    :cond_9
    const-string v0, "status_bar_toggle_page"

    goto :goto_8
.end method

.method public static getUserSelectedToggleOrder(Landroid/content/Context;)Ljava/util/ArrayList;
    .registers 9
    .parameter "context"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .local v3, result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-static {p0}, Lmiui/app/ToggleManager;->getToggleOrderSettingID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .local v4, toggleList:Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3f

    const-string v6, " "

    invoke-virtual {v4, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .local v5, toggles:[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, i:I
    :goto_1e
    array-length v6, v5

    if-ge v1, v6, :cond_3f

    :try_start_21
    aget-object v6, v5, v1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .local v2, id:I
    invoke-static {v2}, Lmiui/app/ToggleManager;->getName(I)I

    move-result v6

    if-eqz v6, :cond_38

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_38
    .catch Ljava/lang/Exception; {:try_start_21 .. :try_end_38} :catch_3b

    :cond_38
    add-int/lit8 v1, v1, 0x1

    goto :goto_1e

    .end local v2           #id:I
    :catch_3b
    move-exception v0

    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .end local v0           #e:Ljava/lang/Exception;
    .end local v1           #i:I
    .end local v5           #toggles:[Ljava/lang/String;
    :cond_3f
    invoke-static {p0, v3}, Lmiui/app/ToggleManager;->validateToggleOrder(Landroid/content/Context;Ljava/util/ArrayList;)V

    return-object v3
.end method

.method public static initDrawable(ILandroid/graphics/drawable/Drawable;)V
    .registers 3
    .parameter "id"
    .parameter "drawable"

    .prologue
    const/16 v0, 0x13

    if-ne v0, p0, :cond_d

    instance-of v0, p1, Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v0, :cond_d

    check-cast p1, Landroid/graphics/drawable/AnimationDrawable;

    .end local p1
    invoke-virtual {p1}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    :cond_d
    return-void
.end method

.method private isAutoSync()Z
    .registers 2

    .prologue
    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v0

    return v0
.end method

.method public static isListStyle(Landroid/content/Context;)Z
    .registers 4
    .parameter "context"

    .prologue
    const/4 v0, 0x1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "status_bar_style"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-nez v1, :cond_e

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method private longClickScreenshot()Z
    .registers 12

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v2, 0x0

    .local v2, path:Ljava/lang/String;
    :try_start_3
    const-string v6, "root"

    const-string v7, "busybox ls -t %s/Screenshots/*.png"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    sget-object v10, Lmiui/os/Environment;->DIRECTORY_PICTURES:Ljava/lang/String;

    invoke-static {v10}, Lmiui/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v10

    invoke-virtual {v10}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v6, v7, v8}, Lmiui/util/CommandLineUtils;->runAndOutput(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/InputStream;

    move-result-object v0

    .local v0, in:Ljava/io/InputStream;
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v6, Ljava/io/InputStreamReader;

    invoke-direct {v6, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v3, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .local v3, reader:Ljava/io/BufferedReader;
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_2c
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_2c} :catch_54

    .end local v0           #in:Ljava/io/InputStream;
    .end local v3           #reader:Ljava/io/BufferedReader;
    :goto_2c
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_33

    :goto_32
    return v4

    :cond_33
    new-instance v1, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .local v1, intent:Landroid/content/Intent;
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v4

    const-string v6, "image/*"

    invoke-virtual {v1, v4, v6}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v4, 0x1000

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    sget-object v4, Lmiui/app/ToggleManager;->sContext:Landroid/content/Context;

    invoke-virtual {v4, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    move v4, v5

    goto :goto_32

    .end local v1           #intent:Landroid/content/Intent;
    :catch_54
    move-exception v6

    goto :goto_2c
.end method

.method private queryBrightnessStatus()V
    .registers 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-boolean v2, p0, Lmiui/app/ToggleManager;->mBrightnessAutoAvailable:Z

    if-eqz v2, :cond_36

    iget-object v2, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    const-string v3, "screen_brightness_mode"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v0, v2, :cond_34

    :goto_10
    iput-boolean v0, p0, Lmiui/app/ToggleManager;->mBrightnessAutoMode:Z

    iget-object v0, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "screen_brightness"

    const/16 v2, 0x66

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lmiui/app/ToggleManager;->mBrightnessManualLevel:I

    iget-object v0, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "screen_auto_brightness_adj"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)F

    move-result v0

    const/high16 v1, 0x3f80

    add-float/2addr v0, v1

    sget v1, Lmiui/app/ToggleManager;->RANGE:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    const/high16 v1, 0x4000

    div-float/2addr v0, v1

    iput v0, p0, Lmiui/app/ToggleManager;->mBrightnessAutoLevel:F

    return-void

    :cond_34
    move v0, v1

    goto :goto_10

    :cond_36
    move v0, v1

    goto :goto_10
.end method

.method public static setTextColor(II)V
    .registers 2
    .parameter "enableColor"
    .parameter "disableColor"

    .prologue
    sput p0, Lmiui/app/ToggleManager;->sTextColorEnable:I

    sput p1, Lmiui/app/ToggleManager;->sTextColorDisable:I

    return-void
.end method

.method private showToast(II)V
    .registers 4
    .parameter "resId"
    .parameter "length"

    .prologue
    sget-object v0, Lmiui/app/ToggleManager;->sContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lmiui/app/ToggleManager;->showToast(Ljava/lang/CharSequence;I)V

    return-void
.end method

.method private showToast(Ljava/lang/CharSequence;I)V
    .registers 5
    .parameter "msg"
    .parameter "length"

    .prologue
    sget-object v1, Lmiui/app/ToggleManager;->sContext:Landroid/content/Context;

    invoke-static {v1, p1, p2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .local v0, toast:Landroid/widget/Toast;
    const/16 v1, 0x7d6

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setType(I)V

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method private shutdown(Z)V
    .registers 8
    .parameter "isReboot"

    .prologue
    if-eqz p1, :cond_45

    const v2, 0x60c018c

    .local v2, titleId:I
    :goto_5
    if-eqz p1, :cond_49

    const/high16 v1, 0x60c

    .local v1, messageId:I
    :goto_9
    new-instance v3, Landroid/app/AlertDialog$Builder;

    sget-object v4, Lmiui/app/ToggleManager;->sContext:Landroid/content/Context;

    const/4 v5, 0x3

    invoke-direct {v3, v4, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v4, 0x1010355

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x1040013

    new-instance v5, Lmiui/app/ToggleManager$19;

    invoke-direct {v5, p0, p1}, Lmiui/app/ToggleManager$19;-><init>(Lmiui/app/ToggleManager;Z)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x1040009

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .local v0, dialog:Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    const/16 v4, 0x7da

    invoke-virtual {v3, v4}, Landroid/view/Window;->setType(I)V

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void

    .end local v0           #dialog:Landroid/app/AlertDialog;
    .end local v1           #messageId:I
    .end local v2           #titleId:I
    :cond_45
    const v2, 0x60c0191

    goto :goto_5

    .restart local v2       #titleId:I
    :cond_49
    const v1, 0x60c0192

    goto :goto_9
.end method

.method private toggleAccelerometer()V
    .registers 4

    .prologue
    :try_start_0
    const-string v1, "window"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v0

    .local v0, wm:Landroid/view/IWindowManager;
    iget-boolean v1, p0, Lmiui/app/ToggleManager;->mAccelerometer:Z

    if-nez v1, :cond_12

    invoke-interface {v0}, Landroid/view/IWindowManager;->thawRotation()V

    .end local v0           #wm:Landroid/view/IWindowManager;
    :goto_11
    return-void

    .restart local v0       #wm:Landroid/view/IWindowManager;
    :cond_12
    invoke-interface {v0}, Landroid/view/IWindowManager;->getRotation()I

    move-result v1

    if-eqz v1, :cond_1f

    const v1, 0x60c0215

    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Lmiui/app/ToggleManager;->showToast(II)V

    :cond_1f
    const/4 v1, -0x1

    invoke-interface {v0, v1}, Landroid/view/IWindowManager;->freezeRotation(I)V
    :try_end_23
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_23} :catch_24

    goto :goto_11

    .end local v0           #wm:Landroid/view/IWindowManager;
    :catch_24
    move-exception v1

    goto :goto_11
.end method

.method private toggleAdvancedSync()V
    .registers 7

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    invoke-static {}, Landroid/content/ContentResolver;->getCurrentSyncs()Ljava/util/List;

    move-result-object v2

    .local v2, list:Ljava/util/List;,"Ljava/util/List<Landroid/content/SyncInfo;>;"
    if-eqz v2, :cond_28

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_28

    move v1, v3

    .local v1, isActive:Z
    :goto_f
    if-nez v1, :cond_2a

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .local v0, extras:Landroid/os/Bundle;
    invoke-direct {p0}, Lmiui/app/ToggleManager;->isAutoSync()Z

    move-result v4

    if-nez v4, :cond_21

    const-string v4, "force"

    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_21
    invoke-static {v5, v5, v0}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .end local v0           #extras:Landroid/os/Bundle;
    :goto_24
    invoke-direct {p0}, Lmiui/app/ToggleManager;->updateAdvancedSyncToggle()V

    return-void

    .end local v1           #isActive:Z
    :cond_28
    const/4 v1, 0x0

    goto :goto_f

    .restart local v1       #isActive:Z
    :cond_2a
    invoke-static {v5, v5}, Landroid/content/ContentResolver;->cancelSync(Landroid/accounts/Account;Ljava/lang/String;)V

    goto :goto_24
.end method

.method private toggleAutoBrightness()V
    .registers 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-boolean v2, p0, Lmiui/app/ToggleManager;->mBrightnessAutoMode:Z

    if-eqz v2, :cond_10

    iput-boolean v0, p0, Lmiui/app/ToggleManager;->mBrightnessAutoMode:Z

    :goto_8
    invoke-virtual {p0}, Lmiui/app/ToggleManager;->getCurBrightness()I

    move-result v0

    invoke-virtual {p0, v0, v1}, Lmiui/app/ToggleManager;->applyBrightness(IZ)V

    return-void

    :cond_10
    iget-boolean v2, p0, Lmiui/app/ToggleManager;->mBrightnessAutoAvailable:Z

    if-eqz v2, :cond_15

    move v0, v1

    :cond_15
    iput-boolean v0, p0, Lmiui/app/ToggleManager;->mBrightnessAutoMode:Z

    goto :goto_8
.end method

.method private toggleBluetooth()V
    .registers 2

    .prologue
    invoke-direct {p0}, Lmiui/app/ToggleManager;->ensureBluetoothAdapter()Z

    move-result v0

    if-eqz v0, :cond_12

    iget-boolean v0, p0, Lmiui/app/ToggleManager;->mBluetoothEnable:Z

    if-eqz v0, :cond_13

    iget-object v0, p0, Lmiui/app/ToggleManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->disable()Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiui/app/ToggleManager;->mBluetoothEnabling:Z

    :cond_12
    :goto_12
    return-void

    :cond_13
    iget-object v0, p0, Lmiui/app/ToggleManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->enable()Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiui/app/ToggleManager;->mBluetoothEnabling:Z

    goto :goto_12
.end method

.method private toggleBrightness()V
    .registers 5

    .prologue
    const/16 v3, 0xff

    const/4 v2, 0x1

    iget-boolean v0, p0, Lmiui/app/ToggleManager;->mBrightnessAutoMode:Z

    if-eqz v0, :cond_16

    sget v0, Lmiui/app/ToggleManager;->MINIMUM_BACKLIGHT:I

    iput v0, p0, Lmiui/app/ToggleManager;->mBrightnessManualLevel:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiui/app/ToggleManager;->mBrightnessAutoMode:Z

    :cond_e
    :goto_e
    invoke-virtual {p0}, Lmiui/app/ToggleManager;->getCurBrightness()I

    move-result v0

    invoke-virtual {p0, v0, v2}, Lmiui/app/ToggleManager;->applyBrightness(IZ)V

    return-void

    :cond_16
    iget v0, p0, Lmiui/app/ToggleManager;->mBrightnessManualLevel:I

    sget v1, Lmiui/app/ToggleManager;->MINIMUM_BACKLIGHT:I

    if-gt v0, v1, :cond_21

    const/16 v0, 0x66

    iput v0, p0, Lmiui/app/ToggleManager;->mBrightnessManualLevel:I

    goto :goto_e

    :cond_21
    iget v0, p0, Lmiui/app/ToggleManager;->mBrightnessManualLevel:I

    if-ge v0, v3, :cond_28

    iput v3, p0, Lmiui/app/ToggleManager;->mBrightnessManualLevel:I

    goto :goto_e

    :cond_28
    iget-boolean v0, p0, Lmiui/app/ToggleManager;->mBrightnessAutoAvailable:Z

    if-eqz v0, :cond_e

    iput-boolean v2, p0, Lmiui/app/ToggleManager;->mBrightnessAutoMode:Z

    sget v0, Lmiui/app/ToggleManager;->MINIMUM_BACKLIGHT:I

    iput v0, p0, Lmiui/app/ToggleManager;->mBrightnessManualLevel:I

    goto :goto_e
.end method

.method private toggleData()Z
    .registers 10

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .local v2, result:Z
    sget-object v7, Lmiui/app/ToggleManager;->sContext:Landroid/content/Context;

    const-string v8, "connectivity"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .local v0, cm:Landroid/net/ConnectivityManager;
    iget-boolean v7, p0, Lmiui/app/ToggleManager;->mMobilePolicyEnable:Z

    if-eqz v7, :cond_1a

    iget-boolean v7, p0, Lmiui/app/ToggleManager;->mMobileDataEnable:Z

    if-nez v7, :cond_18

    :goto_14
    invoke-virtual {v0, v6}, Landroid/net/ConnectivityManager;->setMobileDataEnabled(Z)V

    :cond_17
    :goto_17
    return v2

    :cond_18
    const/4 v6, 0x0

    goto :goto_14

    :cond_1a
    invoke-virtual {v0, v6}, Landroid/net/ConnectivityManager;->setMobileDataEnabled(Z)V

    sget-object v6, Lmiui/app/ToggleManager;->sContext:Landroid/content/Context;

    const-string v7, "phone"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/telephony/TelephonyManager;

    .local v4, telephony:Landroid/telephony/TelephonyManager;
    invoke-virtual {v4}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    move-result-object v3

    .local v3, subscriberId:Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_17

    invoke-static {v3}, Landroid/net/NetworkTemplate;->buildTemplateMobileAll(Ljava/lang/String;)Landroid/net/NetworkTemplate;

    move-result-object v5

    .local v5, template:Landroid/net/NetworkTemplate;
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .local v1, intent:Landroid/content/Intent;
    new-instance v6, Landroid/content/ComponentName;

    const-string v7, "com.android.systemui"

    const-string v8, "com.android.systemui.net.NetworkOverLimitActivity"

    invoke-direct {v6, v7, v8}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const/high16 v6, 0x1000

    invoke-virtual {v1, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v6, "android.net.NETWORK_TEMPLATE"

    invoke-virtual {v1, v6, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    sget-object v6, Lmiui/app/ToggleManager;->sContext:Landroid/content/Context;

    invoke-virtual {v6, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    const/4 v2, 0x1

    goto :goto_17
.end method

.method private toggleDriveMode()V
    .registers 7

    .prologue
    const/4 v4, 0x4

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-boolean v1, p0, Lmiui/app/ToggleManager;->mDriveMode:Z

    if-nez v1, :cond_31

    move v1, v2

    :goto_8
    iput-boolean v1, p0, Lmiui/app/ToggleManager;->mDriveMode:Z

    iget-boolean v1, p0, Lmiui/app/ToggleManager;->mDriveMode:Z

    if-eqz v1, :cond_33

    invoke-static {}, Lmiui/net/FirewallManager;->getInstance()Lmiui/net/FirewallManager;

    move-result-object v1

    invoke-virtual {v1, v4}, Lmiui/net/FirewallManager;->addOneShotFlag(I)V

    :goto_15
    iget-object v1, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "drive_mode_drive_mode"

    iget-boolean v5, p0, Lmiui/app/ToggleManager;->mDriveMode:Z

    if-eqz v5, :cond_3b

    :goto_1d
    invoke-static {v1, v4, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    iget-boolean v1, p0, Lmiui/app/ToggleManager;->mDriveMode:Z

    if-eqz v1, :cond_3d

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.miui.app.ExtraStatusBarManager.action_enter_drive_mode"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .local v0, intent:Landroid/content/Intent;
    sget-object v1, Lmiui/app/ToggleManager;->sContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    :goto_30
    return-void

    .end local v0           #intent:Landroid/content/Intent;
    :cond_31
    move v1, v3

    goto :goto_8

    :cond_33
    invoke-static {}, Lmiui/net/FirewallManager;->getInstance()Lmiui/net/FirewallManager;

    move-result-object v1

    invoke-virtual {v1, v4}, Lmiui/net/FirewallManager;->removeOneShotFlag(I)V

    goto :goto_15

    :cond_3b
    move v2, v3

    goto :goto_1d

    :cond_3d
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.miui.app.ExtraStatusBarManager.action_leave_drive_mode"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .restart local v0       #intent:Landroid/content/Intent;
    sget-object v1, Lmiui/app/ToggleManager;->sContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_30
.end method

.method private toggleFlightMode()V
    .registers 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-boolean v1, p0, Lmiui/app/ToggleManager;->mFlightMode:Z

    if-nez v1, :cond_2d

    move v1, v2

    :goto_7
    iput-boolean v1, p0, Lmiui/app/ToggleManager;->mFlightMode:Z

    iget-object v1, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "airplane_mode_on"

    iget-boolean v5, p0, Lmiui/app/ToggleManager;->mFlightMode:Z

    if-eqz v5, :cond_2f

    :goto_11
    invoke-static {v1, v4, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.AIRPLANE_MODE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .local v0, intent:Landroid/content/Intent;
    const/high16 v1, 0x2000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v1, "state"

    iget-boolean v2, p0, Lmiui/app/ToggleManager;->mFlightMode:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    sget-object v1, Lmiui/app/ToggleManager;->sContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void

    .end local v0           #intent:Landroid/content/Intent;
    :cond_2d
    move v1, v3

    goto :goto_7

    :cond_2f
    move v2, v3

    goto :goto_11
.end method

.method private toggleGps()V
    .registers 4

    .prologue
    iget-object v1, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    const-string v2, "gps"

    iget-boolean v0, p0, Lmiui/app/ToggleManager;->mGpsEnable:Z

    if-nez v0, :cond_d

    const/4 v0, 0x1

    :goto_9
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    return-void

    :cond_d
    const/4 v0, 0x0

    goto :goto_9
.end method

.method private togglePowerMode()V
    .registers 4

    .prologue
    const-string v0, "low"

    iget-object v1, p0, Lmiui/app/ToggleManager;->mPowerMode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1f

    const-string v0, "middle"

    iput-object v0, p0, Lmiui/app/ToggleManager;->mPowerMode:Ljava/lang/String;

    :goto_e
    const-string v0, "persist.sys.aries.power_profile"

    iget-object v1, p0, Lmiui/app/ToggleManager;->mPowerMode:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "power_mode"

    iget-object v2, p0, Lmiui/app/ToggleManager;->mPowerMode:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    return-void

    :cond_1f
    const-string v0, "middle"

    iget-object v1, p0, Lmiui/app/ToggleManager;->mPowerMode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2e

    const-string v0, "high"

    iput-object v0, p0, Lmiui/app/ToggleManager;->mPowerMode:Ljava/lang/String;

    goto :goto_e

    :cond_2e
    const-string v0, "low"

    iput-object v0, p0, Lmiui/app/ToggleManager;->mPowerMode:Ljava/lang/String;

    goto :goto_e
.end method

.method private togglePrivacyMode()Z
    .registers 7

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .local v3, result:Z
    iget-boolean v4, p0, Lmiui/app/ToggleManager;->mPrivacyMode:Z

    if-nez v4, :cond_47

    const/4 v4, 0x1

    :goto_7
    iput-boolean v4, p0, Lmiui/app/ToggleManager;->mPrivacyMode:Z

    iget-boolean v4, p0, Lmiui/app/ToggleManager;->mPrivacyMode:Z

    if-eqz v4, :cond_49

    new-instance v0, Landroid/app/AlertDialog$Builder;

    sget-object v4, Lmiui/app/ToggleManager;->sContext:Landroid/content/Context;

    const/4 v5, 0x3

    invoke-direct {v0, v4, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .local v0, ab:Landroid/app/AlertDialog$Builder;
    const v4, 0x60c0171

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v4, 0x60c0174

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    const v4, 0x104000a

    new-instance v5, Lmiui/app/ToggleManager$17;

    invoke-direct {v5, p0}, Lmiui/app/ToggleManager$17;-><init>(Lmiui/app/ToggleManager;)V

    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const/high16 v4, 0x104

    new-instance v5, Lmiui/app/ToggleManager$18;

    invoke-direct {v5, p0}, Lmiui/app/ToggleManager$18;-><init>(Lmiui/app/ToggleManager;)V

    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .local v1, dialog:Landroid/app/AlertDialog;
    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v4

    const/16 v5, 0x7da

    invoke-virtual {v4, v5}, Landroid/view/Window;->setType(I)V

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .end local v0           #ab:Landroid/app/AlertDialog$Builder;
    .end local v1           #dialog:Landroid/app/AlertDialog;
    :goto_46
    return v3

    :cond_47
    move v4, v5

    goto :goto_7

    :cond_49
    iget-object v4, p0, Lmiui/app/ToggleManager;->mSecurityHelper:Lmiui/security/ChooseLockSettingsHelper;

    invoke-virtual {v4}, Lmiui/security/ChooseLockSettingsHelper;->isACLockEnabled()Z

    move-result v4

    if-eqz v4, :cond_7c

    iget-object v4, p0, Lmiui/app/ToggleManager;->mSecurityHelper:Lmiui/security/ChooseLockSettingsHelper;

    invoke-virtual {v4}, Lmiui/security/ChooseLockSettingsHelper;->isPasswordForPrivacyModeEnabled()Z

    move-result v4

    if-eqz v4, :cond_7c

    new-instance v2, Landroid/content/Intent;

    const-string v4, "android.app.action.CONFIRM_ACCESS_CONTROL"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .local v2, intent:Landroid/content/Intent;
    const-string v4, "confirm_purpose"

    const/4 v5, 0x4

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/high16 v4, 0x1000

    invoke-virtual {v2, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const/high16 v4, 0x400

    invoke-virtual {v2, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const/high16 v4, 0x2000

    invoke-virtual {v2, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    sget-object v4, Lmiui/app/ToggleManager;->sContext:Landroid/content/Context;

    invoke-virtual {v4, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    const/4 v3, 0x1

    goto :goto_46

    .end local v2           #intent:Landroid/content/Intent;
    :cond_7c
    iget-object v4, p0, Lmiui/app/ToggleManager;->mSecurityHelper:Lmiui/security/ChooseLockSettingsHelper;

    invoke-virtual {v4, v5}, Lmiui/security/ChooseLockSettingsHelper;->setPrivacyModeEnabled(Z)V

    goto :goto_46
.end method

.method private toggleRinger()V
    .registers 3

    .prologue
    sget-object v0, Lmiui/app/ToggleManager;->sContext:Landroid/content/Context;

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lmiui/util/AudioManagerHelper;->toggleSilent(Landroid/content/Context;I)V

    return-void
.end method

.method private toggleScreenButtonState()V
    .registers 8

    .prologue
    const v4, 0x60c0216

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-boolean v1, p0, Lmiui/app/ToggleManager;->mScreenButtonDisabled:Z

    if-nez v1, :cond_4d

    move v1, v2

    :goto_a
    iput-boolean v1, p0, Lmiui/app/ToggleManager;->mScreenButtonDisabled:Z

    iget-object v1, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    const-string v5, "screen_buttons_has_been_disabled"

    invoke-static {v1, v5, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-nez v1, :cond_4f

    iget-object v1, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    const-string v5, "screen_buttons_has_been_disabled"

    invoke-static {v1, v5, v2}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    new-instance v1, Landroid/app/AlertDialog$Builder;

    sget-object v5, Lmiui/app/ToggleManager;->sContext:Landroid/content/Context;

    const/4 v6, 0x3

    invoke-direct {v1, v5, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v4, 0x104000a

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .local v0, dialog:Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v4, 0x7da

    invoke-virtual {v1, v4}, Landroid/view/Window;->setType(I)V

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .end local v0           #dialog:Landroid/app/AlertDialog;
    :goto_41
    iget-object v1, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "screen_buttons_state"

    iget-boolean v5, p0, Lmiui/app/ToggleManager;->mScreenButtonDisabled:Z

    if-eqz v5, :cond_5c

    :goto_49
    invoke-static {v1, v4, v2}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void

    :cond_4d
    move v1, v3

    goto :goto_a

    :cond_4f
    iget-boolean v1, p0, Lmiui/app/ToggleManager;->mScreenButtonDisabled:Z

    if-eqz v1, :cond_58

    move v1, v4

    :goto_54
    invoke-direct {p0, v1, v3}, Lmiui/app/ToggleManager;->showToast(II)V

    goto :goto_41

    :cond_58
    const v1, 0x60c0217

    goto :goto_54

    :cond_5c
    move v2, v3

    goto :goto_49
.end method

.method private toggleScreenshot()V
    .registers 4

    .prologue
    sget-object v0, Lmiui/app/ToggleManager;->sContext:Landroid/content/Context;

    const-string v1, "statusbar"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/StatusBarManager;

    invoke-virtual {v0}, Landroid/app/StatusBarManager;->collapse()V

    sget-object v0, Lmiui/app/ToggleManager;->sContext:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.CAPTURE_SCREENSHOT"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method private toggleSync()V
    .registers 2

    .prologue
    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v0

    if-nez v0, :cond_b

    const/4 v0, 0x1

    :goto_7
    invoke-static {v0}, Landroid/content/ContentResolver;->setMasterSyncAutomatically(Z)V

    return-void

    :cond_b
    const/4 v0, 0x0

    goto :goto_7
.end method

.method private toggleTorch()V
    .registers 4

    .prologue
    new-instance v0, Landroid/content/Intent;

    const-string v1, "miui.intent.action.TOGGLE_TORCH"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .local v0, intent:Landroid/content/Intent;
    const-string v1, "miui.intent.extra.IS_TOGGLE"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    sget-object v1, Lmiui/app/ToggleManager;->sContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method private toggleVibrate()V
    .registers 2

    .prologue
    sget-object v0, Lmiui/app/ToggleManager;->sContext:Landroid/content/Context;

    invoke-static {v0}, Lmiui/util/AudioManagerHelper;->toggleVibrateSetting(Landroid/content/Context;)V

    return-void
.end method

.method private updateAccelerometerToggle()V
    .registers 6

    .prologue
    const/4 v4, 0x3

    const/4 v2, 0x0

    iget-object v1, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    const-string v3, "accelerometer_rotation"

    invoke-static {v1, v3, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_1f

    const/4 v1, 0x1

    :goto_d
    iput-boolean v1, p0, Lmiui/app/ToggleManager;->mAccelerometer:Z

    iget-boolean v1, p0, Lmiui/app/ToggleManager;->mAccelerometer:Z

    invoke-static {v4, v1}, Lmiui/app/ToggleManager;->updateToggleStatus(IZ)V

    iget-boolean v1, p0, Lmiui/app/ToggleManager;->mAccelerometer:Z

    if-eqz v1, :cond_21

    const v1, 0x60201e5

    invoke-direct {p0, v4, v1}, Lmiui/app/ToggleManager;->updateToggleImage(II)V

    :goto_1e
    return-void

    :cond_1f
    move v1, v2

    goto :goto_d

    :cond_21
    iget-object v1, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    const-string v3, "user_rotation"

    invoke-static {v1, v3, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .local v0, rotation:I
    if-eqz v0, :cond_2e

    const/4 v1, 0x2

    if-ne v1, v0, :cond_35

    :cond_2e
    const v1, 0x60201e3

    :goto_31
    invoke-direct {p0, v4, v1}, Lmiui/app/ToggleManager;->updateToggleImage(II)V

    goto :goto_1e

    :cond_35
    const v1, 0x60201e4

    goto :goto_31
.end method

.method private updateAdvancedSyncToggle()V
    .registers 4

    .prologue
    invoke-static {}, Landroid/content/ContentResolver;->getCurrentSyncs()Ljava/util/List;

    move-result-object v1

    .local v1, list:Ljava/util/List;,"Ljava/util/List<Landroid/content/SyncInfo;>;"
    if-eqz v1, :cond_15

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_15

    const/4 v0, 0x1

    .local v0, isActive:Z
    :goto_d
    invoke-direct {p0}, Lmiui/app/ToggleManager;->isAutoSync()Z

    move-result v2

    invoke-direct {p0, v2, v0}, Lmiui/app/ToggleManager;->updateAdvancedSyncToggle(ZZ)V

    return-void

    .end local v0           #isActive:Z
    :cond_15
    const/4 v0, 0x0

    goto :goto_d
.end method

.method private updateAdvancedSyncToggle(ZZ)V
    .registers 5
    .parameter "isAuto"
    .parameter "isActive"

    .prologue
    const/16 v1, 0x13

    const/4 v0, 0x0

    .local v0, iconId:I
    if-eqz p1, :cond_15

    if-eqz p2, :cond_11

    const v0, 0x60201ee

    :goto_a
    invoke-static {v1, p2}, Lmiui/app/ToggleManager;->updateToggleStatus(IZ)V

    invoke-direct {p0, v1, v0}, Lmiui/app/ToggleManager;->updateToggleImage(II)V

    return-void

    :cond_11
    const v0, 0x60201f1

    goto :goto_a

    :cond_15
    if-eqz p2, :cond_1b

    const v0, 0x60201ef

    :goto_1a
    goto :goto_a

    :cond_1b
    const v0, 0x60201f9

    goto :goto_1a
.end method

.method private updateBrightnessToggle()V
    .registers 7

    .prologue
    const/16 v5, 0x16

    const/4 v4, 0x4

    const/4 v1, 0x0

    .local v1, brightnessResId:I
    const/4 v0, 0x0

    .local v0, autoResId:I
    iget-boolean v2, p0, Lmiui/app/ToggleManager;->mBrightnessAutoMode:Z

    if-eqz v2, :cond_1e

    const v1, 0x60201ce

    move v0, v1

    :goto_d
    iget-boolean v2, p0, Lmiui/app/ToggleManager;->mBrightnessAutoMode:Z

    invoke-static {v4, v2}, Lmiui/app/ToggleManager;->updateToggleStatus(IZ)V

    iget-boolean v2, p0, Lmiui/app/ToggleManager;->mBrightnessAutoMode:Z

    invoke-static {v5, v2}, Lmiui/app/ToggleManager;->updateToggleStatus(IZ)V

    invoke-direct {p0, v4, v1}, Lmiui/app/ToggleManager;->updateToggleImage(II)V

    invoke-direct {p0, v5, v0}, Lmiui/app/ToggleManager;->updateToggleImage(II)V

    return-void

    :cond_1e
    const v0, 0x60201d0

    const v1, 0x60201d1

    iget v2, p0, Lmiui/app/ToggleManager;->mBrightnessManualLevel:I

    sget v3, Lmiui/app/ToggleManager;->MINIMUM_BACKLIGHT:I

    if-gt v2, v3, :cond_2e

    const v1, 0x60201d1

    goto :goto_d

    :cond_2e
    iget v2, p0, Lmiui/app/ToggleManager;->mBrightnessManualLevel:I

    const/16 v3, 0xff

    if-ge v2, v3, :cond_38

    const v1, 0x60201cf

    goto :goto_d

    :cond_38
    const v1, 0x60201d2

    goto :goto_d
.end method

.method private updateDataToggle()V
    .registers 9

    .prologue
    const v7, 0x60201df

    const/16 v5, 0x7d

    const/16 v6, 0x11

    const/4 v2, 0x1

    sget-object v3, Lmiui/app/ToggleManager;->sContext:Landroid/content/Context;

    const-string v4, "connectivity"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .local v0, cm:Landroid/net/ConnectivityManager;
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getMobileDataEnabled()Z

    move-result v3

    iput-boolean v3, p0, Lmiui/app/ToggleManager;->mMobileDataEnable:Z

    iget-boolean v3, p0, Lmiui/app/ToggleManager;->mMobileDataEnable:Z

    if-eqz v3, :cond_3b

    iget-boolean v3, p0, Lmiui/app/ToggleManager;->mMobilePolicyEnable:Z

    if-eqz v3, :cond_3b

    move v1, v2

    .local v1, isDataEnabled:Z
    :goto_21
    invoke-static {v2, v1}, Lmiui/app/ToggleManager;->updateToggleStatus(IZ)V

    if-eqz v1, :cond_3d

    const v3, 0x60201d4

    :goto_29
    iget-boolean v4, p0, Lmiui/app/ToggleManager;->mFlightMode:Z

    if-eqz v4, :cond_41

    move v4, v5

    :goto_2e
    invoke-direct {p0, v2, v3, v4}, Lmiui/app/ToggleManager;->updateToggleImage(III)V

    iget-boolean v2, p0, Lmiui/app/ToggleManager;->mEnableNetType:Z

    if-nez v2, :cond_44

    if-eqz v1, :cond_44

    invoke-direct {p0, v6, v7, v5}, Lmiui/app/ToggleManager;->updateToggleImage(III)V

    :goto_3a
    return-void

    .end local v1           #isDataEnabled:Z
    :cond_3b
    const/4 v1, 0x0

    goto :goto_21

    .restart local v1       #isDataEnabled:Z
    :cond_3d
    const v3, 0x60201d3

    goto :goto_29

    :cond_41
    const/16 v4, 0xff

    goto :goto_2e

    :cond_44
    invoke-direct {p0, v6, v7}, Lmiui/app/ToggleManager;->updateToggleImage(II)V

    goto :goto_3a
.end method

.method private updateDriveModeToggle()V
    .registers 6

    .prologue
    const/16 v4, 0x15

    invoke-static {}, Lmiui/net/FirewallManager;->getInstance()Lmiui/net/FirewallManager;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lmiui/net/FirewallManager;->getOneShotFlag(I)Z

    move-result v1

    iput-boolean v1, p0, Lmiui/app/ToggleManager;->mDriveMode:Z

    iget-object v2, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    const-string v3, "drive_mode_drive_mode"

    iget-boolean v1, p0, Lmiui/app/ToggleManager;->mDriveMode:Z

    if-eqz v1, :cond_3c

    const/4 v1, 0x1

    :goto_16
    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    iget-boolean v1, p0, Lmiui/app/ToggleManager;->mDriveMode:Z

    if-eqz v1, :cond_3e

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.miui.app.ExtraStatusBarManager.action_enter_drive_mode"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .local v0, intent:Landroid/content/Intent;
    sget-object v1, Lmiui/app/ToggleManager;->sContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    :goto_29
    iget-boolean v1, p0, Lmiui/app/ToggleManager;->mDriveMode:Z

    invoke-static {v4, v1}, Lmiui/app/ToggleManager;->updateToggleStatus(IZ)V

    iget-boolean v1, p0, Lmiui/app/ToggleManager;->mDriveMode:Z

    if-eqz v1, :cond_4b

    const v1, 0x60202d4

    :goto_35
    invoke-direct {p0, v4, v1}, Lmiui/app/ToggleManager;->updateToggleImage(II)V

    invoke-direct {p0}, Lmiui/app/ToggleManager;->updateDataToggle()V

    return-void

    .end local v0           #intent:Landroid/content/Intent;
    :cond_3c
    const/4 v1, 0x0

    goto :goto_16

    :cond_3e
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.miui.app.ExtraStatusBarManager.action_leave_drive_mode"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .restart local v0       #intent:Landroid/content/Intent;
    sget-object v1, Lmiui/app/ToggleManager;->sContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_29

    :cond_4b
    const v1, 0x60202d5

    goto :goto_35
.end method

.method private updateFlightModeToggle()V
    .registers 5

    .prologue
    const/16 v3, 0x9

    const/4 v0, 0x0

    iget-object v1, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    const-string v2, "airplane_mode_on"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_e

    const/4 v0, 0x1

    :cond_e
    iput-boolean v0, p0, Lmiui/app/ToggleManager;->mFlightMode:Z

    iget-boolean v0, p0, Lmiui/app/ToggleManager;->mFlightMode:Z

    invoke-static {v3, v0}, Lmiui/app/ToggleManager;->updateToggleStatus(IZ)V

    iget-boolean v0, p0, Lmiui/app/ToggleManager;->mFlightMode:Z

    if-eqz v0, :cond_23

    const v0, 0x60201d9

    :goto_1c
    invoke-direct {p0, v3, v0}, Lmiui/app/ToggleManager;->updateToggleImage(II)V

    invoke-direct {p0}, Lmiui/app/ToggleManager;->updateDataToggle()V

    return-void

    :cond_23
    const v0, 0x60201d8

    goto :goto_1c
.end method

.method private updateGpsToggle()V
    .registers 4

    .prologue
    const/4 v2, 0x7

    iget-object v0, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "gps"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->isLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lmiui/app/ToggleManager;->mGpsEnable:Z

    iget-boolean v0, p0, Lmiui/app/ToggleManager;->mGpsEnable:Z

    invoke-static {v2, v0}, Lmiui/app/ToggleManager;->updateToggleStatus(IZ)V

    iget-boolean v0, p0, Lmiui/app/ToggleManager;->mGpsEnable:Z

    if-eqz v0, :cond_1b

    const v0, 0x60201db

    :goto_17
    invoke-direct {p0, v2, v0}, Lmiui/app/ToggleManager;->updateToggleImage(II)V

    return-void

    :cond_1b
    const v0, 0x60201da

    goto :goto_17
.end method

.method public static updateImageView(ILandroid/widget/ImageView;)V
    .registers 3
    .parameter "id"
    .parameter "imageView"

    .prologue
    invoke-static {p0}, Lmiui/app/ToggleManager;->getImageDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .local v0, drawable:Landroid/graphics/drawable/Drawable;
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-static {p0, v0}, Lmiui/app/ToggleManager;->initDrawable(ILandroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method private updatePowerModeToggle()V
    .registers 4

    .prologue
    const/4 v0, 0x0

    .local v0, resId:I
    iget-object v1, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    const-string v2, "power_mode"

    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lmiui/app/ToggleManager;->mPowerMode:Ljava/lang/String;

    iget-object v1, p0, Lmiui/app/ToggleManager;->mPowerMode:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_17

    const-string v1, "middle"

    iput-object v1, p0, Lmiui/app/ToggleManager;->mPowerMode:Ljava/lang/String;

    :cond_17
    const-string v1, "low"

    iget-object v2, p0, Lmiui/app/ToggleManager;->mPowerMode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2a

    const v0, 0x602022b

    :goto_24
    const/16 v1, 0x17

    invoke-direct {p0, v1, v0}, Lmiui/app/ToggleManager;->updateToggleImage(II)V

    return-void

    :cond_2a
    const-string v1, "middle"

    iget-object v2, p0, Lmiui/app/ToggleManager;->mPowerMode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_38

    const v0, 0x602022c

    goto :goto_24

    :cond_38
    const v0, 0x602022a

    goto :goto_24
.end method

.method private updatePrivacyModeToggle()V
    .registers 3

    .prologue
    const/16 v1, 0xe

    iget-object v0, p0, Lmiui/app/ToggleManager;->mSecurityHelper:Lmiui/security/ChooseLockSettingsHelper;

    invoke-virtual {v0}, Lmiui/security/ChooseLockSettingsHelper;->isPrivacyModeEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lmiui/app/ToggleManager;->mPrivacyMode:Z

    iget-boolean v0, p0, Lmiui/app/ToggleManager;->mPrivacyMode:Z

    invoke-static {v1, v0}, Lmiui/app/ToggleManager;->updateToggleStatus(IZ)V

    iget-boolean v0, p0, Lmiui/app/ToggleManager;->mPrivacyMode:Z

    if-eqz v0, :cond_1a

    const v0, 0x60201e1

    :goto_16
    invoke-direct {p0, v1, v0}, Lmiui/app/ToggleManager;->updateToggleImage(II)V

    return-void

    :cond_1a
    const v0, 0x60201e0

    goto :goto_16
.end method

.method private updateScreenButtonState()V
    .registers 5

    .prologue
    const/16 v3, 0x14

    const/4 v0, 0x0

    iget-object v1, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    const-string v2, "screen_buttons_state"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_e

    const/4 v0, 0x1

    :cond_e
    iput-boolean v0, p0, Lmiui/app/ToggleManager;->mScreenButtonDisabled:Z

    iget-boolean v0, p0, Lmiui/app/ToggleManager;->mScreenButtonDisabled:Z

    invoke-static {v3, v0}, Lmiui/app/ToggleManager;->updateToggleStatus(IZ)V

    iget-boolean v0, p0, Lmiui/app/ToggleManager;->mScreenButtonDisabled:Z

    if-eqz v0, :cond_20

    const v0, 0x60201e6

    :goto_1c
    invoke-direct {p0, v3, v0}, Lmiui/app/ToggleManager;->updateToggleImage(II)V

    return-void

    :cond_20
    const v0, 0x60201e7

    goto :goto_1c
.end method

.method private updateSyncToggle()V
    .registers 3

    .prologue
    const/16 v1, 0x8

    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v0

    if-eqz v0, :cond_f

    const v0, 0x60201fa

    :goto_b
    invoke-direct {p0, v1, v0}, Lmiui/app/ToggleManager;->updateToggleImage(II)V

    return-void

    :cond_f
    const v0, 0x60201f9

    goto :goto_b
.end method

.method public static updateTextView(ILandroid/widget/TextView;)V
    .registers 3
    .parameter "id"
    .parameter "textView"

    .prologue
    invoke-static {p0}, Lmiui/app/ToggleManager;->getName(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    invoke-static {p0}, Lmiui/app/ToggleManager;->getTextColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method private updateToggleImage(II)V
    .registers 4
    .parameter "toggleId"
    .parameter "resId"

    .prologue
    const/16 v0, 0xff

    invoke-direct {p0, p1, p2, v0}, Lmiui/app/ToggleManager;->updateToggleImage(III)V

    return-void
.end method

.method private updateToggleImage(III)V
    .registers 8
    .parameter "toggleId"
    .parameter "resId"
    .parameter "alpha"

    .prologue
    sget-object v3, Lmiui/app/ToggleManager;->sToggleImages:[I

    aput p2, v3, p1

    sget-object v3, Lmiui/app/ToggleManager;->sToggleAlpha:[I

    aput p3, v3, p1

    iget-object v3, p0, Lmiui/app/ToggleManager;->mToggleChangedListener:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_36

    iget-object v3, p0, Lmiui/app/ToggleManager;->mToggleChangedListener:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v0, v3, -0x1

    .local v0, i:I
    :goto_18
    if-ltz v0, :cond_36

    iget-object v3, p0, Lmiui/app/ToggleManager;->mToggleChangedListener:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    .local v1, item:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Lmiui/app/ToggleManager$OnToggleChangedListener;>;"
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/app/ToggleManager$OnToggleChangedListener;

    .local v2, l:Lmiui/app/ToggleManager$OnToggleChangedListener;
    if-nez v2, :cond_32

    iget-object v3, p0, Lmiui/app/ToggleManager;->mToggleChangedListener:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :goto_2f
    add-int/lit8 v0, v0, -0x1

    goto :goto_18

    :cond_32
    invoke-interface {v2, p1}, Lmiui/app/ToggleManager$OnToggleChangedListener;->OnToggleChanged(I)V

    goto :goto_2f

    .end local v0           #i:I
    .end local v1           #item:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Lmiui/app/ToggleManager$OnToggleChangedListener;>;"
    .end local v2           #l:Lmiui/app/ToggleManager$OnToggleChangedListener;
    :cond_36
    return-void
.end method

.method private static updateToggleStatus(IZ)V
    .registers 3
    .parameter "id"
    .parameter "isOpen"

    .prologue
    sget-object v0, Lmiui/app/ToggleManager;->sToggleStatus:[Z

    aput-boolean p1, v0, p0

    return-void
.end method

.method private updateTorchToggle()V
    .registers 6

    .prologue
    const/16 v4, 0xb

    const/4 v0, 0x1

    const/4 v1, 0x0

    sget-object v2, Lmiui/app/ToggleManager;->sContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "torch_state"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_24

    :goto_12
    iput-boolean v0, p0, Lmiui/app/ToggleManager;->mTorchEnable:Z

    iget-boolean v0, p0, Lmiui/app/ToggleManager;->mTorchEnable:Z

    invoke-static {v4, v0}, Lmiui/app/ToggleManager;->updateToggleStatus(IZ)V

    iget-boolean v0, p0, Lmiui/app/ToggleManager;->mTorchEnable:Z

    if-eqz v0, :cond_26

    const v0, 0x60201d6

    :goto_20
    invoke-direct {p0, v4, v0}, Lmiui/app/ToggleManager;->updateToggleImage(II)V

    return-void

    :cond_24
    move v0, v1

    goto :goto_12

    :cond_26
    const v0, 0x60201c8

    goto :goto_20
.end method

.method private static validateToggleList(Ljava/util/ArrayList;)V
    .registers 9
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p0, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/16 v7, 0x17

    const/16 v6, 0x15

    const/4 v5, 0x5

    const/4 v4, 0x4

    const/4 v3, 0x0

    const/16 v2, 0xf

    invoke-static {p0, v2}, Lmiui/app/ToggleManager;->addIfUnselected(Ljava/util/ArrayList;I)V

    const/4 v2, 0x1

    invoke-static {p0, v2}, Lmiui/app/ToggleManager;->addIfUnselected(Ljava/util/ArrayList;I)V

    const/4 v2, 0x7

    invoke-static {p0, v2}, Lmiui/app/ToggleManager;->addIfUnselected(Ljava/util/ArrayList;I)V

    invoke-static {p0, v4}, Lmiui/app/ToggleManager;->addIfUnselected(Ljava/util/ArrayList;I)V

    invoke-static {p0, v5}, Lmiui/app/ToggleManager;->addIfUnselected(Ljava/util/ArrayList;I)V

    const/4 v2, 0x6

    invoke-static {p0, v2}, Lmiui/app/ToggleManager;->addIfUnselected(Ljava/util/ArrayList;I)V

    const/4 v2, 0x3

    invoke-static {p0, v2}, Lmiui/app/ToggleManager;->addIfUnselected(Ljava/util/ArrayList;I)V

    const/16 v2, 0x12

    invoke-static {p0, v2}, Lmiui/app/ToggleManager;->addIfUnselected(Ljava/util/ArrayList;I)V

    const/16 v2, 0x9

    invoke-static {p0, v2}, Lmiui/app/ToggleManager;->addIfUnselected(Ljava/util/ArrayList;I)V

    invoke-static {p0, v3}, Lmiui/app/ToggleManager;->addIfUnselected(Ljava/util/ArrayList;I)V

    const/4 v2, 0x2

    invoke-static {p0, v2}, Lmiui/app/ToggleManager;->addIfUnselected(Ljava/util/ArrayList;I)V

    const/16 v2, 0x13

    invoke-static {p0, v2}, Lmiui/app/ToggleManager;->addIfUnselected(Ljava/util/ArrayList;I)V

    const/16 v2, 0x16

    invoke-static {p0, v2}, Lmiui/app/ToggleManager;->addIfUnselected(Ljava/util/ArrayList;I)V

    const/16 v2, 0x10

    invoke-static {p0, v2}, Lmiui/app/ToggleManager;->addIfUnselected(Ljava/util/ArrayList;I)V

    const/16 v2, 0x11

    invoke-static {p0, v2}, Lmiui/app/ToggleManager;->addIfUnselected(Ljava/util/ArrayList;I)V

    const/16 v2, 0xe

    invoke-static {p0, v2}, Lmiui/app/ToggleManager;->addIfUnselected(Ljava/util/ArrayList;I)V

    const/16 v2, 0xb

    invoke-static {p0, v2}, Lmiui/app/ToggleManager;->addIfUnselected(Ljava/util/ArrayList;I)V

    const/16 v2, 0x14

    invoke-static {p0, v2}, Lmiui/app/ToggleManager;->addIfUnselected(Ljava/util/ArrayList;I)V

    const/16 v2, 0xa

    invoke-static {p0, v2}, Lmiui/app/ToggleManager;->addIfUnselected(Ljava/util/ArrayList;I)V

    invoke-static {p0, v6}, Lmiui/app/ToggleManager;->addIfUnselected(Ljava/util/ArrayList;I)V

    const/16 v2, 0xc

    invoke-static {p0, v2}, Lmiui/app/ToggleManager;->addIfUnselected(Ljava/util/ArrayList;I)V

    const/16 v2, 0xd

    invoke-static {p0, v2}, Lmiui/app/ToggleManager;->addIfUnselected(Ljava/util/ArrayList;I)V

    sget-boolean v2, Lmiui/os/Build;->IS_MITWO:Z

    if-eqz v2, :cond_99

    invoke-static {p0, v7}, Lmiui/app/ToggleManager;->addIfUnselected(Ljava/util/ArrayList;I)V

    :goto_6f
    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .local v0, dividerIndex:I
    if-ge v0, v4, :cond_8c

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p0, v5, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_8c
    :try_start_8c
    sget-object v2, Lmiui/app/ToggleManager;->sContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "com.miui.voiceassist"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_98
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_8c .. :try_end_98} :catch_a1

    :goto_98
    return-void

    .end local v0           #dividerIndex:I
    :cond_99
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_6f

    .restart local v0       #dividerIndex:I
    :catch_a1
    move-exception v1

    .local v1, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_98
.end method

.method private static validateToggleOrder(Landroid/content/Context;Ljava/util/ArrayList;)V
    .registers 3
    .parameter "context"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-static {p0}, Lmiui/app/ToggleManager;->isListStyle(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-static {p1}, Lmiui/app/ToggleManager;->validateToggleList(Ljava/util/ArrayList;)V

    :goto_9
    return-void

    :cond_a
    invoke-static {p1}, Lmiui/app/ToggleManager;->validateTogglePage(Ljava/util/ArrayList;)V

    goto :goto_9
.end method

.method private static validateTogglePage(Ljava/util/ArrayList;)V
    .registers 9
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p0, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/16 v7, 0x15

    const/16 v6, 0xb

    const/16 v4, 0x16

    const/16 v3, 0xa

    const/4 v5, 0x0

    const/16 v0, 0xa

    .local v0, autoBrightnessDefaultPosition:I
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v2, v3, :cond_22

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_22

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p0, v3, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    :cond_22
    const/16 v2, 0x9

    invoke-static {p0, v2}, Lmiui/app/ToggleManager;->addIfUnselected(Ljava/util/ArrayList;I)V

    const/4 v2, 0x2

    invoke-static {p0, v2}, Lmiui/app/ToggleManager;->addIfUnselected(Ljava/util/ArrayList;I)V

    const/16 v2, 0x13

    invoke-static {p0, v2}, Lmiui/app/ToggleManager;->addIfUnselected(Ljava/util/ArrayList;I)V

    const/16 v2, 0x12

    invoke-static {p0, v2}, Lmiui/app/ToggleManager;->addIfUnselected(Ljava/util/ArrayList;I)V

    const/4 v2, 0x3

    invoke-static {p0, v2}, Lmiui/app/ToggleManager;->addIfUnselected(Ljava/util/ArrayList;I)V

    const/4 v2, 0x6

    invoke-static {p0, v2}, Lmiui/app/ToggleManager;->addIfUnselected(Ljava/util/ArrayList;I)V

    const/4 v2, 0x5

    invoke-static {p0, v2}, Lmiui/app/ToggleManager;->addIfUnselected(Ljava/util/ArrayList;I)V

    const/4 v2, 0x7

    invoke-static {p0, v2}, Lmiui/app/ToggleManager;->addIfUnselected(Ljava/util/ArrayList;I)V

    const/16 v2, 0xf

    invoke-static {p0, v2}, Lmiui/app/ToggleManager;->addIfUnselected(Ljava/util/ArrayList;I)V

    const/4 v2, 0x1

    invoke-static {p0, v2}, Lmiui/app/ToggleManager;->addIfUnselected(Ljava/util/ArrayList;I)V

    invoke-static {p0, v4}, Lmiui/app/ToggleManager;->addIfUnselected(Ljava/util/ArrayList;I)V

    const/4 v2, 0x4

    invoke-static {p0, v2}, Lmiui/app/ToggleManager;->addIfUnselected(Ljava/util/ArrayList;I)V

    const/16 v2, 0x10

    invoke-static {p0, v2}, Lmiui/app/ToggleManager;->addIfUnselected(Ljava/util/ArrayList;I)V

    const/16 v2, 0x11

    invoke-static {p0, v2}, Lmiui/app/ToggleManager;->addIfUnselected(Ljava/util/ArrayList;I)V

    const/16 v2, 0xe

    invoke-static {p0, v2}, Lmiui/app/ToggleManager;->addIfUnselected(Ljava/util/ArrayList;I)V

    invoke-static {p0, v6}, Lmiui/app/ToggleManager;->addIfUnselected(Ljava/util/ArrayList;I)V

    const/16 v2, 0x14

    invoke-static {p0, v2}, Lmiui/app/ToggleManager;->addIfUnselected(Ljava/util/ArrayList;I)V

    invoke-static {p0, v3}, Lmiui/app/ToggleManager;->addIfUnselected(Ljava/util/ArrayList;I)V

    invoke-static {p0, v7}, Lmiui/app/ToggleManager;->addIfUnselected(Ljava/util/ArrayList;I)V

    const/16 v2, 0xc

    invoke-static {p0, v2}, Lmiui/app/ToggleManager;->addIfUnselected(Ljava/util/ArrayList;I)V

    const/16 v2, 0xd

    invoke-static {p0, v2}, Lmiui/app/ToggleManager;->addIfUnselected(Ljava/util/ArrayList;I)V

    sget-boolean v2, Lmiui/os/Build;->IS_MITWO:Z

    if-eqz v2, :cond_a9

    const/16 v2, 0x17

    invoke-static {p0, v2}, Lmiui/app/ToggleManager;->addIfUnselected(Ljava/util/ArrayList;I)V

    :goto_85
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :try_start_95
    sget-object v2, Lmiui/app/ToggleManager;->sContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "com.miui.voiceassist"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_a1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_95 .. :try_end_a1} :catch_b3

    :goto_a1
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p0, v6, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    return-void

    :cond_a9
    const/16 v2, 0x17

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_85

    :catch_b3
    move-exception v1

    .local v1, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_a1
.end method


# virtual methods
.method public applyBrightness(IZ)V
    .registers 10
    .parameter "brightness"
    .parameter "write"

    .prologue
    const/4 v3, 0x0

    .local v3, valf:F
    const/4 v0, 0x0

    .local v0, brightnessValue:I
    :try_start_2
    const-string v4, "power"

    invoke-static {v4}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v4

    invoke-static {v4}, Landroid/os/IPowerManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/IPowerManager;

    move-result-object v2

    .local v2, power:Landroid/os/IPowerManager;
    if-eqz v2, :cond_32

    iget-boolean v4, p0, Lmiui/app/ToggleManager;->mBrightnessAutoMode:Z

    if-eqz v4, :cond_33

    int-to-float v4, p1

    const/high16 v5, 0x4000

    mul-float/2addr v4, v5

    sget v5, Lmiui/app/ToggleManager;->RANGE:I

    int-to-float v5, v5

    div-float/2addr v4, v5

    const/high16 v5, 0x3f80

    sub-float v3, v4, v5

    invoke-interface {v2, v3}, Landroid/os/IPowerManager;->setAutoBrightnessAdjustment(F)V

    if-eqz p2, :cond_32

    iget-object v4, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    const-string v5, "screen_brightness_mode"

    const/4 v6, 0x1

    invoke-static {v4, v5, v6}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    iget-object v4, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    const-string v5, "screen_auto_brightness_adj"

    invoke-static {v4, v5, v3}, Landroid/provider/Settings$System;->putFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)Z

    .end local v2           #power:Landroid/os/IPowerManager;
    :cond_32
    :goto_32
    return-void

    .restart local v2       #power:Landroid/os/IPowerManager;
    :cond_33
    sget v4, Lmiui/app/ToggleManager;->MINIMUM_BACKLIGHT:I

    add-int v0, p1, v4

    invoke-interface {v2, p1}, Landroid/os/IPowerManager;->setBacklightBrightness(I)V

    if-eqz p2, :cond_32

    iget-object v4, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    const-string v5, "screen_brightness_mode"

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    iget-object v4, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    const-string v5, "screen_brightness"

    invoke-static {v4, v5, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_4b
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_4b} :catch_4c

    goto :goto_32

    .end local v2           #power:Landroid/os/IPowerManager;
    :catch_4c
    move-exception v1

    .local v1, e:Landroid/os/RemoteException;
    const-string v4, "ToggleManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setBrightness: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_32
.end method

.method public getCurBrightness()I
    .registers 3

    .prologue
    iget-boolean v0, p0, Lmiui/app/ToggleManager;->mBrightnessAutoMode:Z

    if-eqz v0, :cond_8

    iget v0, p0, Lmiui/app/ToggleManager;->mBrightnessAutoLevel:F

    :goto_6
    float-to-int v0, v0

    return v0

    :cond_8
    iget v0, p0, Lmiui/app/ToggleManager;->mBrightnessManualLevel:I

    sget v1, Lmiui/app/ToggleManager;->MINIMUM_BACKLIGHT:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    goto :goto_6
.end method

.method public onDestroy()V
    .registers 3

    .prologue
    sget-object v0, Lmiui/app/ToggleManager;->sContext:Landroid/content/Context;

    iget-object v1, p0, Lmiui/app/ToggleManager;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lmiui/app/ToggleManager;->mTogglOrderObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iget-object v0, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lmiui/app/ToggleManager;->mMobileDataEnableObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iget-object v0, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lmiui/app/ToggleManager;->mMobilePolicyEnableObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iget-object v0, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lmiui/app/ToggleManager;->mTorchEnableObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iget-object v0, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lmiui/app/ToggleManager;->mPrivacyModeObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iget-object v0, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lmiui/app/ToggleManager;->mScreenButtonStateObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iget-object v0, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lmiui/app/ToggleManager;->mLocationAllowedObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iget-object v0, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lmiui/app/ToggleManager;->mAccelerometerRotationObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iget-object v0, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lmiui/app/ToggleManager;->mBrightnessObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iget-object v0, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lmiui/app/ToggleManager;->mVibrateEnableObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iget-object v0, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lmiui/app/ToggleManager;->mPowerModeObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iget-object v0, p0, Lmiui/app/ToggleManager;->mResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lmiui/app/ToggleManager;->mDriveModeObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iget-object v0, p0, Lmiui/app/ToggleManager;->mStatusChangeListenerHandle:Ljava/lang/Object;

    invoke-static {v0}, Landroid/content/ContentResolver;->removeStatusChangeListener(Ljava/lang/Object;)V

    iget-object v0, p0, Lmiui/app/ToggleManager;->mToggleChangedListener:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lmiui/app/ToggleManager;->mToggleOrderChangedListener:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method public performToggle(I)Z
    .registers 10
    .parameter "id"

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    sget-object v3, Lmiui/app/ToggleManager;->sContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v4, "toggle_collapse_after_clicked"

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .local v2, result:Z
    packed-switch p1, :pswitch_data_de

    :cond_11
    :goto_11
    return v2

    :pswitch_12
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.MAIN"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .local v1, intent:Landroid/content/Intent;
    const-string v3, "com.android.settings"

    const-string v4, "com.android.settings.AccessControlSetApp"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v3, 0x1000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    sget-object v3, Lmiui/app/ToggleManager;->sContext:Landroid/content/Context;

    invoke-virtual {v3, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    const/4 v2, 0x1

    goto :goto_11

    .end local v1           #intent:Landroid/content/Intent;
    :pswitch_2c
    invoke-direct {p0}, Lmiui/app/ToggleManager;->toggleAdvancedSync()V

    goto :goto_11

    :pswitch_30
    invoke-direct {p0}, Lmiui/app/ToggleManager;->toggleAutoBrightness()V

    const/4 v2, 0x0

    goto :goto_11

    :pswitch_35
    invoke-direct {p0}, Lmiui/app/ToggleManager;->toggleBluetooth()V

    goto :goto_11

    :pswitch_39
    invoke-direct {p0}, Lmiui/app/ToggleManager;->toggleBrightness()V

    const/4 v2, 0x0

    goto :goto_11

    :pswitch_3e
    iget-boolean v3, p0, Lmiui/app/ToggleManager;->mFlightMode:Z

    if-nez v3, :cond_11

    invoke-direct {p0}, Lmiui/app/ToggleManager;->toggleData()Z

    move-result v0

    .local v0, dataResult:Z
    or-int/2addr v2, v0

    goto :goto_11

    .end local v0           #dataResult:Z
    :pswitch_48
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.MAIN"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .restart local v1       #intent:Landroid/content/Intent;
    const-string v3, "com.android.systemui"

    const-string v4, "com.android.systemui.settings.ToggleArrangement"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v3, 0x1400

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v3, "com.android.systemui.settings.EXTRA_LOCATE_DIVIDER"

    invoke-virtual {v1, v3, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    sget-object v3, Lmiui/app/ToggleManager;->sContext:Landroid/content/Context;

    invoke-virtual {v3, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    const/4 v2, 0x1

    goto :goto_11

    .end local v1           #intent:Landroid/content/Intent;
    :pswitch_67
    invoke-direct {p0}, Lmiui/app/ToggleManager;->toggleFlightMode()V

    goto :goto_11

    :pswitch_6b
    invoke-direct {p0}, Lmiui/app/ToggleManager;->toggleGps()V

    goto :goto_11

    :pswitch_6f
    sget-object v3, Lmiui/app/ToggleManager;->sContext:Landroid/content/Context;

    const-string v4, "power"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/PowerManager;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Landroid/os/PowerManager;->goToSleep(J)V

    goto :goto_11

    :pswitch_84
    invoke-direct {p0}, Lmiui/app/ToggleManager;->toggleRinger()V

    goto :goto_11

    :pswitch_88
    iget-boolean v3, p0, Lmiui/app/ToggleManager;->mMobileDataEnable:Z

    if-eqz v3, :cond_94

    iget-boolean v3, p0, Lmiui/app/ToggleManager;->mMobilePolicyEnable:Z

    if-eqz v3, :cond_94

    iget-boolean v3, p0, Lmiui/app/ToggleManager;->mEnableNetType:Z

    if-eqz v3, :cond_11

    :cond_94
    const/16 v3, 0x11

    invoke-virtual {p0, v3}, Lmiui/app/ToggleManager;->startLongClickAction(I)Z

    const/4 v2, 0x1

    goto/16 :goto_11

    :pswitch_9c
    invoke-direct {p0}, Lmiui/app/ToggleManager;->togglePrivacyMode()Z

    move-result v2

    goto/16 :goto_11

    :pswitch_a2
    invoke-direct {p0, v6}, Lmiui/app/ToggleManager;->shutdown(Z)V

    goto/16 :goto_11

    :pswitch_a7
    invoke-direct {p0}, Lmiui/app/ToggleManager;->toggleAccelerometer()V

    goto/16 :goto_11

    :pswitch_ac
    invoke-direct {p0}, Lmiui/app/ToggleManager;->toggleScreenButtonState()V

    goto/16 :goto_11

    :pswitch_b1
    invoke-direct {p0}, Lmiui/app/ToggleManager;->toggleScreenshot()V

    const/4 v2, 0x1

    goto/16 :goto_11

    :pswitch_b7
    invoke-direct {p0, v5}, Lmiui/app/ToggleManager;->shutdown(Z)V

    goto/16 :goto_11

    :pswitch_bc
    invoke-direct {p0}, Lmiui/app/ToggleManager;->toggleSync()V

    goto/16 :goto_11

    :pswitch_c1
    invoke-direct {p0}, Lmiui/app/ToggleManager;->toggleTorch()V

    goto/16 :goto_11

    :pswitch_c6
    invoke-direct {p0}, Lmiui/app/ToggleManager;->toggleVibrate()V

    goto/16 :goto_11

    :pswitch_cb
    iget-object v3, p0, Lmiui/app/ToggleManager;->mWifiState:Lmiui/app/ToggleManager$StateTracker;

    sget-object v4, Lmiui/app/ToggleManager;->sContext:Landroid/content/Context;

    invoke-virtual {v3, v4}, Lmiui/app/ToggleManager$StateTracker;->toggleState(Landroid/content/Context;)V

    goto/16 :goto_11

    :pswitch_d4
    invoke-direct {p0}, Lmiui/app/ToggleManager;->togglePowerMode()V

    goto/16 :goto_11

    :pswitch_d9
    invoke-direct {p0}, Lmiui/app/ToggleManager;->toggleDriveMode()V

    goto/16 :goto_11

    :pswitch_data_de
    .packed-switch 0x0
        :pswitch_48
        :pswitch_3e
        :pswitch_35
        :pswitch_a7
        :pswitch_39
        :pswitch_84
        :pswitch_c6
        :pswitch_6b
        :pswitch_bc
        :pswitch_67
        :pswitch_6f
        :pswitch_c1
        :pswitch_a2
        :pswitch_b7
        :pswitch_9c
        :pswitch_cb
        :pswitch_12
        :pswitch_88
        :pswitch_b1
        :pswitch_2c
        :pswitch_ac
        :pswitch_d9
        :pswitch_30
        :pswitch_d4
    .end packed-switch
.end method

.method public removeToggleChangedListener(Lmiui/app/ToggleManager$OnToggleChangedListener;)V
    .registers 5
    .parameter "l"

    .prologue
    iget-object v2, p0, Lmiui/app/ToggleManager;->mToggleChangedListener:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .local v0, i:I
    :goto_8
    if-ltz v0, :cond_2a

    iget-object v2, p0, Lmiui/app/ToggleManager;->mToggleChangedListener:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    .local v1, item:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Lmiui/app/ToggleManager$OnToggleChangedListener;>;"
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_22

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_27

    :cond_22
    iget-object v2, p0, Lmiui/app/ToggleManager;->mToggleChangedListener:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_27
    add-int/lit8 v0, v0, -0x1

    goto :goto_8

    .end local v1           #item:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Lmiui/app/ToggleManager$OnToggleChangedListener;>;"
    :cond_2a
    return-void
.end method

.method public removeToggleOrderChangeListener(Lmiui/app/ToggleManager$OnToggleOrderChangedListener;)V
    .registers 3
    .parameter "l"

    .prologue
    iget-object v0, p0, Lmiui/app/ToggleManager;->mToggleOrderChangedListener:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public setOnToggleChangedListener(Lmiui/app/ToggleManager$OnToggleChangedListener;)V
    .registers 4
    .parameter "l"

    .prologue
    iget-object v0, p0, Lmiui/app/ToggleManager;->mToggleChangedListener:Ljava/util/List;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public setOnToggleOrderChangeListener(Lmiui/app/ToggleManager$OnToggleOrderChangedListener;)V
    .registers 3
    .parameter "l"

    .prologue
    iget-object v0, p0, Lmiui/app/ToggleManager;->mToggleOrderChangedListener:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public setUserSelectedToggleOrder(Ljava/util/ArrayList;)V
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    sget-object v2, Lmiui/app/ToggleManager;->sContext:Landroid/content/Context;

    invoke-static {v2, p1}, Lmiui/app/ToggleManager;->validateToggleOrder(Landroid/content/Context;Ljava/util/ArrayList;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x48

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .local v1, toggleList:Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .local v0, i:I
    :goto_d
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_28

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    :cond_28
    sget-object v2, Lmiui/app/ToggleManager;->sContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lmiui/app/ToggleManager;->sContext:Landroid/content/Context;

    invoke-static {v3}, Lmiui/app/ToggleManager;->getToggleOrderSettingID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    return-void
.end method

.method public startLongClickAction(I)Z
    .registers 9
    .parameter "id"

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/16 v6, 0x12

    if-ne v6, p1, :cond_b

    invoke-direct {p0}, Lmiui/app/ToggleManager;->longClickScreenshot()Z

    move-result v4

    :cond_a
    :goto_a
    return v4

    :cond_b
    if-ne v5, p1, :cond_11

    iget-boolean v6, p0, Lmiui/app/ToggleManager;->mFlightMode:Z

    if-nez v6, :cond_a

    :cond_11
    sget-object v6, Lmiui/app/ToggleManager;->sLongClickActions:[I

    aget v3, v6, p1

    .local v3, resId:I
    if-eqz v3, :cond_a

    sget-object v6, Lmiui/app/ToggleManager;->sContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .local v0, action:Ljava/lang/String;
    if-eqz v0, :cond_a

    invoke-static {v0}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v1

    .local v1, component:Landroid/content/ComponentName;
    if-eqz v1, :cond_a

    new-instance v2, Landroid/content/Intent;

    const-string v4, "android.intent.action.MAIN"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .local v2, intent:Landroid/content/Intent;
    invoke-virtual {v2, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    if-nez p1, :cond_4c

    const/high16 v4, 0x1400

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    :goto_3a
    const/16 v4, 0x15

    if-ne v4, p1, :cond_45

    const-string v4, "com.android.settings.FRAGMENT_CLASS"

    const-string v6, "com.android.settings.MiuiDriveModeSettings"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_45
    sget-object v4, Lmiui/app/ToggleManager;->sContext:Landroid/content/Context;

    invoke-virtual {v4, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    move v4, v5

    goto :goto_a

    :cond_4c
    const/high16 v4, 0x1000

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto :goto_3a
.end method

.method updateBluetoothToggle()V
    .registers 5

    .prologue
    const v3, 0x60201cd

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-direct {p0}, Lmiui/app/ToggleManager;->ensureBluetoothAdapter()Z

    move-result v0

    if-eqz v0, :cond_13

    iget-object v0, p0, Lmiui/app/ToggleManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lmiui/app/ToggleManager;->mBluetoothEnable:Z

    :cond_13
    iget-boolean v0, p0, Lmiui/app/ToggleManager;->mBluetoothEnable:Z

    if-nez v0, :cond_1b

    iget-boolean v0, p0, Lmiui/app/ToggleManager;->mBluetoothEnabling:Z

    if-eqz v0, :cond_29

    :cond_1b
    const/4 v0, 0x1

    :goto_1c
    invoke-static {v2, v0}, Lmiui/app/ToggleManager;->updateToggleStatus(IZ)V

    iget-boolean v0, p0, Lmiui/app/ToggleManager;->mBluetoothEnable:Z

    if-eqz v0, :cond_2b

    iput-boolean v1, p0, Lmiui/app/ToggleManager;->mBluetoothEnabling:Z

    invoke-direct {p0, v2, v3}, Lmiui/app/ToggleManager;->updateToggleImage(II)V

    :goto_28
    return-void

    :cond_29
    move v0, v1

    goto :goto_1c

    :cond_2b
    iget-boolean v0, p0, Lmiui/app/ToggleManager;->mBluetoothEnabling:Z

    if-eqz v0, :cond_35

    const/16 v0, 0x7d

    invoke-direct {p0, v2, v3, v0}, Lmiui/app/ToggleManager;->updateToggleImage(III)V

    goto :goto_28

    :cond_35
    const v0, 0x60201cc

    invoke-direct {p0, v2, v0}, Lmiui/app/ToggleManager;->updateToggleImage(II)V

    goto :goto_28
.end method

.method public updateRingerToggle()V
    .registers 4

    .prologue
    const/4 v2, 0x5

    sget-object v1, Lmiui/app/ToggleManager;->sContext:Landroid/content/Context;

    invoke-static {v1}, Lmiui/util/AudioManagerHelper;->isSilentEnabled(Landroid/content/Context;)Z

    move-result v0

    .local v0, isSilentEnabled:Z
    invoke-static {v2, v0}, Lmiui/app/ToggleManager;->updateToggleStatus(IZ)V

    sget-object v1, Lmiui/app/ToggleManager;->sContext:Landroid/content/Context;

    invoke-static {v1}, Lmiui/util/AudioManagerHelper;->isSilentEnabled(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_19

    const v1, 0x60201de

    :goto_15
    invoke-direct {p0, v2, v1}, Lmiui/app/ToggleManager;->updateToggleImage(II)V

    return-void

    :cond_19
    const v1, 0x60201dd

    goto :goto_15
.end method

.method public updateVibrateToggle()V
    .registers 4

    .prologue
    const/4 v2, 0x6

    sget-object v1, Lmiui/app/ToggleManager;->sContext:Landroid/content/Context;

    invoke-static {v1}, Lmiui/util/AudioManagerHelper;->isVibrateEnabled(Landroid/content/Context;)Z

    move-result v0

    .local v0, isVibrateEnabled:Z
    invoke-static {v2, v0}, Lmiui/app/ToggleManager;->updateToggleStatus(IZ)V

    if-eqz v0, :cond_13

    const v1, 0x60201e9

    :goto_f
    invoke-direct {p0, v2, v1}, Lmiui/app/ToggleManager;->updateToggleImage(II)V

    return-void

    :cond_13
    const v1, 0x60201d7

    goto :goto_f
.end method

.method updateWifiToggle()V
    .registers 6

    .prologue
    const v1, 0x60201ec

    const v4, 0x60201eb

    const/16 v3, 0xf

    iget-object v0, p0, Lmiui/app/ToggleManager;->mWifiState:Lmiui/app/ToggleManager$StateTracker;

    sget-object v2, Lmiui/app/ToggleManager;->sContext:Landroid/content/Context;

    invoke-virtual {v0, v2}, Lmiui/app/ToggleManager$StateTracker;->getTriState(Landroid/content/Context;)I

    move-result v0

    packed-switch v0, :pswitch_data_52

    :goto_13
    :pswitch_13
    return-void

    :pswitch_14
    const/4 v0, 0x0

    invoke-static {v3, v0}, Lmiui/app/ToggleManager;->updateToggleStatus(IZ)V

    invoke-direct {p0, v3, v4}, Lmiui/app/ToggleManager;->updateToggleImage(II)V

    goto :goto_13

    :pswitch_1c
    iget-object v0, p0, Lmiui/app/ToggleManager;->mWifiState:Lmiui/app/ToggleManager$StateTracker;

    check-cast v0, Lmiui/app/ToggleManager$WifiStateTracker;

    iget-boolean v0, v0, Lmiui/app/ToggleManager$WifiStateTracker;->zConnected:Z

    invoke-static {v3, v0}, Lmiui/app/ToggleManager;->updateToggleStatus(IZ)V

    iget-object v0, p0, Lmiui/app/ToggleManager;->mWifiState:Lmiui/app/ToggleManager$StateTracker;

    check-cast v0, Lmiui/app/ToggleManager$WifiStateTracker;

    iget-boolean v0, v0, Lmiui/app/ToggleManager$WifiStateTracker;->zConnected:Z

    if-eqz v0, :cond_32

    move v0, v1

    :goto_2e
    invoke-direct {p0, v3, v0}, Lmiui/app/ToggleManager;->updateToggleImage(II)V

    goto :goto_13

    :cond_32
    const v0, 0x60201ed

    goto :goto_2e

    :pswitch_36
    iget-object v0, p0, Lmiui/app/ToggleManager;->mWifiState:Lmiui/app/ToggleManager$StateTracker;

    invoke-virtual {v0}, Lmiui/app/ToggleManager$StateTracker;->isTurningOn()Z

    move-result v0

    invoke-static {v3, v0}, Lmiui/app/ToggleManager;->updateToggleStatus(IZ)V

    iget-object v0, p0, Lmiui/app/ToggleManager;->mWifiState:Lmiui/app/ToggleManager$StateTracker;

    invoke-virtual {v0}, Lmiui/app/ToggleManager$StateTracker;->isTurningOn()Z

    move-result v0

    if-eqz v0, :cond_4d

    const/16 v0, 0x7d

    invoke-direct {p0, v3, v1, v0}, Lmiui/app/ToggleManager;->updateToggleImage(III)V

    goto :goto_13

    :cond_4d
    invoke-direct {p0, v3, v4}, Lmiui/app/ToggleManager;->updateToggleImage(II)V

    goto :goto_13

    nop

    :pswitch_data_52
    .packed-switch 0x0
        :pswitch_14
        :pswitch_1c
        :pswitch_13
        :pswitch_13
        :pswitch_13
        :pswitch_36
    .end packed-switch
.end method
