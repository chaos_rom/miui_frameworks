.class public Lmiui/app/resourcebrowser/resource/ResourceCategory;
.super Ljava/lang/Object;
.source "ResourceCategory.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lmiui/app/resourcebrowser/resource/ResourceCategory;",
        ">;"
    }
.end annotation


# instance fields
.field private mCode:Ljava/lang/String;

.field private mName:Ljava/lang/String;

.field private mType:J


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .registers 3
    .parameter "x0"

    .prologue
    check-cast p1, Lmiui/app/resourcebrowser/resource/ResourceCategory;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/app/resourcebrowser/resource/ResourceCategory;->compareTo(Lmiui/app/resourcebrowser/resource/ResourceCategory;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lmiui/app/resourcebrowser/resource/ResourceCategory;)I
    .registers 4
    .parameter "another"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/resource/ResourceCategory;->mCode:Ljava/lang/String;

    invoke-virtual {p1}, Lmiui/app/resourcebrowser/resource/ResourceCategory;->getCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getCode()Ljava/lang/String;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/resource/ResourceCategory;->mCode:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/resource/ResourceCategory;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getType()J
    .registers 3

    .prologue
    iget-wide v0, p0, Lmiui/app/resourcebrowser/resource/ResourceCategory;->mType:J

    return-wide v0
.end method

.method public setCode(Ljava/lang/String;)V
    .registers 2
    .parameter "code"

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/resource/ResourceCategory;->mCode:Ljava/lang/String;

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .registers 2
    .parameter "name"

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/resource/ResourceCategory;->mName:Ljava/lang/String;

    return-void
.end method

.method public setType(J)V
    .registers 3
    .parameter "type"

    .prologue
    iput-wide p1, p0, Lmiui/app/resourcebrowser/resource/ResourceCategory;->mType:J

    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/resource/ResourceCategory;->mName:Ljava/lang/String;

    return-object v0
.end method
