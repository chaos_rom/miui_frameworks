.class public Lmiui/app/resourcebrowser/resource/ListMetaData;
.super Ljava/lang/Object;
.source "ListMetaData.java"


# instance fields
.field private mCategory:Ljava/lang/String;

.field private mDescription:Ljava/lang/String;

.field private mName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCategory()Ljava/lang/String;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/resource/ListMetaData;->mCategory:Ljava/lang/String;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/resource/ListMetaData;->mDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/resource/ListMetaData;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public setCategory(Ljava/lang/String;)V
    .registers 2
    .parameter "category"

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/resource/ListMetaData;->mCategory:Ljava/lang/String;

    return-void
.end method

.method public setDescription(Ljava/lang/String;)V
    .registers 2
    .parameter "description"

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/resource/ListMetaData;->mDescription:Ljava/lang/String;

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .registers 2
    .parameter "name"

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/resource/ListMetaData;->mName:Ljava/lang/String;

    return-void
.end method
