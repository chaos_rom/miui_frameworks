.class public Lmiui/app/resourcebrowser/resource/Resource;
.super Ljava/lang/Object;
.source "Resource.java"


# static fields
.field public static final AUTHOR:Ljava/lang/String; = "AUTHOR"

.field public static final CREATED_TIME:Ljava/lang/String; = "CREATED_TIME"

.field public static final DESCRIPTION:Ljava/lang/String; = "DESCRIPTION"

.field public static final DESIGNER:Ljava/lang/String; = "DESIGNER"

.field public static final DOWNLOAD_COUNT:Ljava/lang/String; = "DOWNLOAD_COUNT"

.field public static final ID:Ljava/lang/String; = "ID"

.field public static final LOCAL_PATH:Ljava/lang/String; = "LOCAL_PATH"

.field public static final LOCAL_PREVIEWS:Ljava/lang/String; = "LOCAL_PREVIEW"

.field public static final LOCAL_THUMBNAILS:Ljava/lang/String; = "LOCAL_THUMBNAIL"

.field public static final MODIFIED_TIME:Ljava/lang/String; = "MODIFIED_TIME"

.field public static final NAME:Ljava/lang/String; = "NAME"

.field public static final NVP:Ljava/lang/String; = "NVP"

.field public static final ONLINE_PATH:Ljava/lang/String; = "ONLINE_PATH"

.field public static final ONLINE_PREVIEWS:Ljava/lang/String; = "ONLINE_PREVIEW"

.field public static final ONLINE_THUMBNAILS:Ljava/lang/String; = "ONLINE_THUMBNAIL"

.field public static final PLATFORM_VERSION:Ljava/lang/String; = "PLATFORM_VERSION"

.field public static final RESOURCE_LATEST_VERSION:I = 0x0

.field public static final RESOURCE_NOT_EXIST:I = 0x2

.field public static final RESOURCE_OLD_VERSION:I = 0x1

.field public static final SIZE:Ljava/lang/String; = "SIZE"

.field public static final STATUS:Ljava/lang/String; = "STATUS"

.field public static final VERSION:Ljava/lang/String; = "VERSION"


# instance fields
.field private mDescription:Ljava/lang/String;

.field private mDividerTitle:Ljava/lang/String;

.field private mFileHash:Ljava/lang/String;

.field private mFileModifiedTime:J

.field private mFileSize:J

.field private mId:Ljava/lang/String;

.field private mInformation:Landroid/os/Bundle;

.field private mLocalPath:Ljava/lang/String;

.field private mLocalPreviews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mLocalThumbnails:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mOnlinePath:Ljava/lang/String;

.field private mOnlinePreviews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mOnlineThumbnails:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mPlatformVersion:I

.field private mStatus:I

.field private mTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/app/resourcebrowser/resource/Resource;->mLocalThumbnails:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/app/resourcebrowser/resource/Resource;->mLocalPreviews:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/app/resourcebrowser/resource/Resource;->mOnlineThumbnails:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/app/resourcebrowser/resource/Resource;->mOnlinePreviews:Ljava/util/List;

    return-void
.end method

.method private getIntValue(Landroid/os/Bundle;Ljava/lang/String;)I
    .registers 5
    .parameter "information"
    .parameter "key"

    .prologue
    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .local v0, strValue:Ljava/lang/String;
    if-eqz v0, :cond_c

    :try_start_6
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_9} :catch_b

    move-result v1

    :goto_a
    return v1

    :catch_b
    move-exception v1

    :cond_c
    const/4 v1, 0x0

    goto :goto_a
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .parameter "o"

    .prologue
    instance-of v1, p1, Lmiui/app/resourcebrowser/resource/Resource;

    if-nez v1, :cond_6

    const/4 v1, 0x0

    :goto_5
    return v1

    :cond_6
    move-object v0, p1

    check-cast v0, Lmiui/app/resourcebrowser/resource/Resource;

    .local v0, r:Lmiui/app/resourcebrowser/resource/Resource;
    iget-object v1, p0, Lmiui/app/resourcebrowser/resource/Resource;->mLocalPath:Ljava/lang/String;

    iget-object v2, v0, Lmiui/app/resourcebrowser/resource/Resource;->mLocalPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_5
.end method

.method public getDescription()Ljava/lang/String;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/resource/Resource;->mDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getDividerTitle()Ljava/lang/String;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/resource/Resource;->mDividerTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getFileHash()Ljava/lang/String;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/resource/Resource;->mFileHash:Ljava/lang/String;

    return-object v0
.end method

.method public getFileModifiedTime()J
    .registers 3

    .prologue
    iget-wide v0, p0, Lmiui/app/resourcebrowser/resource/Resource;->mFileModifiedTime:J

    return-wide v0
.end method

.method public getFileSize()J
    .registers 3

    .prologue
    iget-wide v0, p0, Lmiui/app/resourcebrowser/resource/Resource;->mFileSize:J

    return-wide v0
.end method

.method public getId()Ljava/lang/String;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/resource/Resource;->mId:Ljava/lang/String;

    return-object v0
.end method

.method public getInformation()Landroid/os/Bundle;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/resource/Resource;->mInformation:Landroid/os/Bundle;

    return-object v0
.end method

.method public getLocalPath()Ljava/lang/String;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/resource/Resource;->mLocalPath:Ljava/lang/String;

    return-object v0
.end method

.method public getLocalPreview(I)Ljava/lang/String;
    .registers 3
    .parameter "index"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/resource/Resource;->mLocalPreviews:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_a

    const/4 v0, 0x0

    :goto_9
    return-object v0

    :cond_a
    iget-object v0, p0, Lmiui/app/resourcebrowser/resource/Resource;->mLocalPreviews:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_9
.end method

.method public getLocalPreviews()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/resource/Resource;->mLocalPreviews:Ljava/util/List;

    return-object v0
.end method

.method public getLocalThumbnail(I)Ljava/lang/String;
    .registers 3
    .parameter "index"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/resource/Resource;->mLocalThumbnails:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_a

    const/4 v0, 0x0

    :goto_9
    return-object v0

    :cond_a
    iget-object v0, p0, Lmiui/app/resourcebrowser/resource/Resource;->mLocalThumbnails:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_9
.end method

.method public getLocalThumbnails()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/resource/Resource;->mLocalThumbnails:Ljava/util/List;

    return-object v0
.end method

.method public getOnlinePath()Ljava/lang/String;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/resource/Resource;->mOnlinePath:Ljava/lang/String;

    return-object v0
.end method

.method public getOnlinePreview(I)Ljava/lang/String;
    .registers 3
    .parameter "index"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/resource/Resource;->mOnlinePreviews:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_a

    const/4 v0, 0x0

    :goto_9
    return-object v0

    :cond_a
    iget-object v0, p0, Lmiui/app/resourcebrowser/resource/Resource;->mOnlinePreviews:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_9
.end method

.method public getOnlinePreviews()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/resource/Resource;->mOnlinePreviews:Ljava/util/List;

    return-object v0
.end method

.method public getOnlineThumbnail(I)Ljava/lang/String;
    .registers 3
    .parameter "index"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/resource/Resource;->mOnlineThumbnails:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_a

    const/4 v0, 0x0

    :goto_9
    return-object v0

    :cond_a
    iget-object v0, p0, Lmiui/app/resourcebrowser/resource/Resource;->mOnlineThumbnails:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_9
.end method

.method public getOnlineThumbnails()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/resource/Resource;->mOnlineThumbnails:Ljava/util/List;

    return-object v0
.end method

.method public getPlatformVersion()I
    .registers 2

    .prologue
    iget v0, p0, Lmiui/app/resourcebrowser/resource/Resource;->mPlatformVersion:I

    return v0
.end method

.method public getStatus()I
    .registers 2

    .prologue
    iget v0, p0, Lmiui/app/resourcebrowser/resource/Resource;->mStatus:I

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/resource/Resource;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/resource/Resource;->mLocalPath:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public setDescription(Ljava/lang/String;)V
    .registers 2
    .parameter "mDescription"

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/resource/Resource;->mDescription:Ljava/lang/String;

    return-void
.end method

.method public setDividerTitle(Ljava/lang/String;)V
    .registers 2
    .parameter "dividerTitle"

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/resource/Resource;->mDividerTitle:Ljava/lang/String;

    return-void
.end method

.method public setFileHash(Ljava/lang/String;)V
    .registers 2
    .parameter "fileHash"

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/resource/Resource;->mFileHash:Ljava/lang/String;

    return-void
.end method

.method public setFileModifiedTime(J)V
    .registers 3
    .parameter "modifiedTime"

    .prologue
    iput-wide p1, p0, Lmiui/app/resourcebrowser/resource/Resource;->mFileModifiedTime:J

    return-void
.end method

.method public setFileSize(J)V
    .registers 3
    .parameter "fileSize"

    .prologue
    iput-wide p1, p0, Lmiui/app/resourcebrowser/resource/Resource;->mFileSize:J

    return-void
.end method

.method public setInformation(Landroid/os/Bundle;)V
    .registers 8
    .parameter "information"

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/resource/Resource;->mInformation:Landroid/os/Bundle;

    const-string v4, "ID"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lmiui/app/resourcebrowser/resource/Resource;->mId:Ljava/lang/String;

    const-string v4, "NAME"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lmiui/app/resourcebrowser/resource/Resource;->mTitle:Ljava/lang/String;

    const-string v4, "DESCRIPTION"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lmiui/app/resourcebrowser/resource/Resource;->mDescription:Ljava/lang/String;

    const-string v4, "LOCAL_PATH"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lmiui/app/resourcebrowser/resource/Resource;->mLocalPath:Ljava/lang/String;

    const-string v4, "ONLINE_PATH"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lmiui/app/resourcebrowser/resource/Resource;->mOnlinePath:Ljava/lang/String;

    const-string v4, "SIZE"

    invoke-direct {p0, p1, v4}, Lmiui/app/resourcebrowser/resource/Resource;->getIntValue(Landroid/os/Bundle;Ljava/lang/String;)I

    move-result v4

    int-to-long v4, v4

    iput-wide v4, p0, Lmiui/app/resourcebrowser/resource/Resource;->mFileSize:J

    const-string v4, "LOCAL_THUMBNAIL"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .local v1, localThumbnails:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v1, :cond_3d

    iput-object v1, p0, Lmiui/app/resourcebrowser/resource/Resource;->mLocalThumbnails:Ljava/util/List;

    :cond_3d
    const-string v4, "LOCAL_PREVIEW"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .local v0, localPreviews:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v0, :cond_47

    iput-object v0, p0, Lmiui/app/resourcebrowser/resource/Resource;->mLocalPreviews:Ljava/util/List;

    :cond_47
    const-string v4, "ONLINE_THUMBNAIL"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .local v3, onlineThumbnails:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v3, :cond_51

    iput-object v3, p0, Lmiui/app/resourcebrowser/resource/Resource;->mOnlineThumbnails:Ljava/util/List;

    :cond_51
    const-string v4, "ONLINE_PREVIEW"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .local v2, onlinePreviews:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v2, :cond_5b

    iput-object v2, p0, Lmiui/app/resourcebrowser/resource/Resource;->mOnlinePreviews:Ljava/util/List;

    :cond_5b
    const-string v4, "STATUS"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lmiui/app/resourcebrowser/resource/Resource;->mStatus:I

    const-string v4, "PLATFORM_VERSION"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lmiui/app/resourcebrowser/resource/Resource;->mPlatformVersion:I

    return-void
.end method

.method public updateOnlinePath(Ljava/lang/String;)V
    .registers 4
    .parameter "onlinePath"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/resource/Resource;->mInformation:Landroid/os/Bundle;

    const-string v1, "ONLINE_PATH"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iput-object p1, p0, Lmiui/app/resourcebrowser/resource/Resource;->mOnlinePath:Ljava/lang/String;

    return-void
.end method

.method public updateStatus(I)V
    .registers 4
    .parameter "status"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/resource/Resource;->mInformation:Landroid/os/Bundle;

    const-string v1, "STATUS"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iput p1, p0, Lmiui/app/resourcebrowser/resource/Resource;->mStatus:I

    return-void
.end method
