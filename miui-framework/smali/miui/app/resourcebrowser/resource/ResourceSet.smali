.class public Lmiui/app/resourcebrowser/resource/ResourceSet;
.super Ljava/util/ArrayList;
.source "ResourceSet.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/ArrayList",
        "<",
        "Lmiui/widget/DataGroup",
        "<",
        "Lmiui/app/resourcebrowser/resource/Resource;",
        ">;>;"
    }
.end annotation


# static fields
.field private static instances:Ljava/util/Map; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lmiui/app/resourcebrowser/resource/ResourceSet;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private mMetaData:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lmiui/app/resourcebrowser/resource/ResourceSet;->instances:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>()V
    .registers 2

    .prologue
    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lmiui/app/resourcebrowser/resource/ResourceSet;->mMetaData:Ljava/util/Map;

    return-void
.end method

.method public static getInstance(Ljava/lang/String;)Lmiui/app/resourcebrowser/resource/ResourceSet;
    .registers 6
    .parameter "id"

    .prologue
    sget-object v3, Lmiui/app/resourcebrowser/resource/ResourceSet;->instances:Ljava/util/Map;

    invoke-interface {v3, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/app/resourcebrowser/resource/ResourceSet;

    .local v1, instance:Lmiui/app/resourcebrowser/resource/ResourceSet;
    if-nez v1, :cond_25

    const-class v4, Lmiui/app/resourcebrowser/resource/ResourceSet;

    monitor-enter v4

    :try_start_d
    sget-object v3, Lmiui/app/resourcebrowser/resource/ResourceSet;->instances:Ljava/util/Map;

    invoke-interface {v3, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lmiui/app/resourcebrowser/resource/ResourceSet;

    move-object v1, v0

    if-nez v1, :cond_24

    new-instance v2, Lmiui/app/resourcebrowser/resource/ResourceSet;

    invoke-direct {v2}, Lmiui/app/resourcebrowser/resource/ResourceSet;-><init>()V
    :try_end_1e
    .catchall {:try_start_d .. :try_end_1e} :catchall_26

    .end local v1           #instance:Lmiui/app/resourcebrowser/resource/ResourceSet;
    .local v2, instance:Lmiui/app/resourcebrowser/resource/ResourceSet;
    :try_start_1e
    sget-object v3, Lmiui/app/resourcebrowser/resource/ResourceSet;->instances:Ljava/util/Map;

    invoke-interface {v3, p0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_23
    .catchall {:try_start_1e .. :try_end_23} :catchall_29

    move-object v1, v2

    .end local v2           #instance:Lmiui/app/resourcebrowser/resource/ResourceSet;
    .restart local v1       #instance:Lmiui/app/resourcebrowser/resource/ResourceSet;
    :cond_24
    :try_start_24
    monitor-exit v4

    :cond_25
    return-object v1

    :catchall_26
    move-exception v3

    :goto_27
    monitor-exit v4
    :try_end_28
    .catchall {:try_start_24 .. :try_end_28} :catchall_26

    throw v3

    .end local v1           #instance:Lmiui/app/resourcebrowser/resource/ResourceSet;
    .restart local v2       #instance:Lmiui/app/resourcebrowser/resource/ResourceSet;
    :catchall_29
    move-exception v3

    move-object v1, v2

    .end local v2           #instance:Lmiui/app/resourcebrowser/resource/ResourceSet;
    .restart local v1       #instance:Lmiui/app/resourcebrowser/resource/ResourceSet;
    goto :goto_27
.end method


# virtual methods
.method public getMetaData(Ljava/lang/String;)Ljava/lang/Object;
    .registers 3
    .parameter "key"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/resource/ResourceSet;->mMetaData:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public set(Ljava/util/List;I)V
    .registers 4
    .parameter
    .parameter "group"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lmiui/app/resourcebrowser/resource/Resource;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .local p1, resourceSet:Ljava/util/List;,"Ljava/util/List<Lmiui/app/resourcebrowser/resource/Resource;>;"
    invoke-virtual {p0, p2}, Lmiui/app/resourcebrowser/resource/ResourceSet;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/widget/DataGroup;

    .local v0, groups:Lmiui/widget/DataGroup;,"Lmiui/widget/DataGroup<Lmiui/app/resourcebrowser/resource/Resource;>;"
    invoke-virtual {v0}, Lmiui/widget/DataGroup;->clear()V

    invoke-virtual {v0, p1}, Lmiui/widget/DataGroup;->addAll(Ljava/util/Collection;)Z

    return-void
.end method

.method public setMetaData(Ljava/lang/String;Ljava/lang/Object;)V
    .registers 4
    .parameter "key"
    .parameter "value"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/resource/ResourceSet;->mMetaData:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
