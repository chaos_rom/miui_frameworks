.class public interface abstract Lmiui/app/resourcebrowser/IntentConstants;
.super Ljava/lang/Object;
.source "IntentConstants.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/app/resourcebrowser/IntentConstants$ThemeWidget;
    }
.end annotation


# static fields
.field public static final ACTION_PICK_RESOURCE:Ljava/lang/String; = "android.intent.action.PICK_RESOURCE"

.field public static final AUDIO_RESOURCE:I = 0x2

.field public static final CACHE_LIST_FOLDER:Ljava/lang/String; = "miui.app.resourcebrowser.CACHE_LIST_FOLDER"

.field public static final CATEGORY_SUPPORTED:Ljava/lang/String; = "miui.app.resourcebrowser.CATEGORY_SUPPORTED"

.field public static final CURRENT_USING_PATH:Ljava/lang/String; = "miui.app.resourcebrowser.CURRENT_USING_PATH"

.field public static final CUSTOM_FLAG:Ljava/lang/String; = "miui.app.resourcebrowser.CUSTOM_FLAG"

.field public static final DETAIL_ACTIVITY_CLASS:Ljava/lang/String; = "miui.app.resourcebrowser.DETAIL_ACTIVITY_CLASS"

.field public static final DETAIL_ACTIVITY_PACKAGE:Ljava/lang/String; = "miui.app.resourcebrowser.DETAIL_ACTIVITY_PACKAGE"

.field public static final DISPLAY_DOUBLE_FLAT:I = 0x6

.field public static final DISPLAY_DOUBLE_FLAT_FONT:I = 0xb

.field public static final DISPLAY_DOUBLE_FLAT_ICON:I = 0xa

.field public static final DISPLAY_SINGLE:I = 0x1

.field public static final DISPLAY_SINGLE_DETAIL:I = 0x3

.field public static final DISPLAY_SINGLE_GALLERY:I = 0x4

.field public static final DISPLAY_SINGLE_MUSIC:I = 0x5

.field public static final DISPLAY_SINGLE_SMALL:I = 0x2

.field public static final DISPLAY_TRIPLE:I = 0x7

.field public static final DISPLAY_TRIPLE_FLAT:I = 0x8

.field public static final DISPLAY_TRIPLE_TEXT:I = 0x9

.field public static final DISPLAY_TYPE:Ljava/lang/String; = "miui.app.resourcebrowser.DISPLAY_TYPE"

.field public static final DOWNLOAD_FOLDER:Ljava/lang/String; = "miui.app.resourcebrowser.DOWNLOAD_FOLDER"

.field public static final DOWNLOAD_FOLDER_EXTRA:Ljava/lang/String; = "miui.app.resourcebrowser.DOWNLOAD_FOLDER_EXTRA"

.field public static final IMAGE_RESOURCE:I = 0x1

.field public static final IS_RECOMMENDATION_LIST:Ljava/lang/String; = "miui.app.resourcebrowser.IS_RECOMMENDATION_LIST"

.field public static final LIST_URL:Ljava/lang/String; = "miui.app.resourcebrowser.LIST_URL"

.field public static final LOCAL_LIST_ACTIVITY_CLASS:Ljava/lang/String; = "miui.app.resourcebrowser.LOCAL_LIST_ACTIVITY_CLASS"

.field public static final LOCAL_LIST_ACTIVITY_PACKAGE:Ljava/lang/String; = "miui.app.resourcebrowser.LOCAL_LIST_ACTIVITY_PACKAGE"

.field public static final META_DATA:Ljava/lang/String; = "META_DATA"

.field public static final META_DATA_FOR_LOCAL:Ljava/lang/String; = "META_DATA_FOR_LOCAL"

.field public static final META_DATA_FOR_ONLINE:Ljava/lang/String; = "META_DATA_FOR_ONLINE"

.field public static final ONLINE_LIST_ACTIVITY_CLASS:Ljava/lang/String; = "miui.app.resourcebrowser.ONLINE_LIST_ACTIVITY_CLASS"

.field public static final ONLINE_LIST_ACTIVITY_PACKAGE:Ljava/lang/String; = "miui.app.resourcebrowser.ONLINE_LIST_ACTIVITY_PACKAGE"

.field public static final PICKED_RESOURCE:Ljava/lang/String; = "miui.app.resourcebrowser.PICKED_RESOURCE"

.field public static final PLATFORM_VERSION_END:Ljava/lang/String; = "miui.app.resourcebrowser.PLATFORM_VERSION_END"

.field public static final PLATFORM_VERSION_START:Ljava/lang/String; = "miui.app.resourcebrowser.PLATFORM_VERSION_START"

.field public static final PLATFORM_VERSION_SUPPORTED:Ljava/lang/String; = "miui.app.resourcebrowser.PLATFORM_VERSION_SUPPORTED"

.field public static final PREVIEW_PREFIX:Ljava/lang/String; = "miui.app.resourcebrowser.PREVIEW_PREFIX"

.field public static final PREVIEW_PREFIX_INDICATOR:Ljava/lang/String; = "miui.app.resourcebrowser.PREVIEW_PREFIX_INDICATOR"

.field public static final PREVIEW_WIDTH:Ljava/lang/String; = "miui.app.resourcebrowser.PREVIEW_WIDTH"

.field public static final RECOMMENDATION_ID:Ljava/lang/String; = "miui.app.resourcebrowser.RECOMMENDATION_ID"

.field public static final RECOMMENDATION_LIST_ACTIVITY_CLASS:Ljava/lang/String; = "miui.app.resourcebrowser.RECOMMENDATION_LIST_ACTIVITY_CLASS"

.field public static final RECOMMENDATION_LIST_ACTIVITY_PACKAGE:Ljava/lang/String; = "miui.app.resourcebrowser.RECOMMENDATION_LIST_ACTIVITY_PACKAGE"

.field public static final RECOMMENDATION_WIDTH:Ljava/lang/String; = "miui.app.resourcebrowser.RECOMMENDATION_WIDTH"

.field public static final REQUEST_CODE:I = 0x1

.field public static final RESOURCE_FILE_EXTENSION:Ljava/lang/String; = "miui.app.resourcebrowser.RESOURCE_FILE_EXTENSION"

.field public static final RESOURCE_GROUP:Ljava/lang/String; = "miui.app.resourcebrowser.RESOURCE_GROUP"

.field public static final RESOURCE_INDEX:Ljava/lang/String; = "miui.app.resourcebrowser.RESOURCE_INDEX"

.field public static final RESOURCE_SET_CATEGORY:Ljava/lang/String; = "miui.app.resourcebrowser.RESOURCE_SET_CATEGORY"

.field public static final RESOURCE_SET_CODE:Ljava/lang/String; = "miui.app.resourcebrowser.RESOURCE_SET_CODE"

.field public static final RESOURCE_SET_NAME:Ljava/lang/String; = "miui.app.resourcebrowser.RESOURCE_SET_NAME"

.field public static final RESOURCE_SET_PACKAGE:Ljava/lang/String; = "miui.app.resourcebrowser.RESOURCE_SET_PACKAGE"

.field public static final RESOURCE_SET_SUBPACKAGE:Ljava/lang/String; = "miui.app.resourcebrowser.RESOURCE_SET_SUBPACKAGE"

.field public static final RESOURCE_SET_SUBPACKAGE_LOCAL:Ljava/lang/String; = ".local"

.field public static final RESOURCE_SET_SUBPACKAGE_ONLINE:Ljava/lang/String; = ".online"

.field public static final RESOURCE_SET_SUBPACKAGE_SEARCH:Ljava/lang/String; = ".search"

.field public static final RESOURCE_SET_SUBPACKAGE_SINGLE:Ljava/lang/String; = ".single"

.field public static final RESOURCE_TYPE_PARAMETER:Ljava/lang/String; = "miui.app.resourcebrowser.RESOURCE_TYPE_PARAMETER"

.field public static final RINGTONE_MAX_DURATION_LIMIT:Ljava/lang/String; = "miui.app.resourcebrowser.RINGTONE_MAX_DURATION_LIMIT"

.field public static final RINGTONE_MIN_DURATION_LIMIT:Ljava/lang/String; = "miui.app.resourcebrowser.RINGTONE_MIN_DURATION_LIMIT"

.field public static final SHOW_RINGTONE_NAME:Ljava/lang/String; = "miui.app.resourcebrowser.SHOW_RINGTONE_NAME"

.field public static final SOURCE_FOLDERS:Ljava/lang/String; = "miui.app.resourcebrowser.SOURCE_FOLDERS"

.field public static final SUB_RECOMMENDS:Ljava/lang/String; = "miui.app.resourcebrowser.SUB_RECOMMENDATIONS"

.field public static final THUMBNAIL_PREFIX:Ljava/lang/String; = "miui.app.resourcebrowser.THUMBNAIL_PREFIX"

.field public static final THUMBNAIL_PREFIX_INDICATOR:Ljava/lang/String; = "miui.app.resourcebrowser.THUMBNAIL_PREFIX_INDICATOR"

.field public static final THUMBNAIL_WIDTH:Ljava/lang/String; = "miui.app.resourcebrowser.THUMBNAIL_WIDTH"

.field public static final TRACK_ID:Ljava/lang/String; = "miui.app.resourcebrowser.TRACK_ID"

.field public static final USING_PICKER:Ljava/lang/String; = "miui.app.resourcebrowser.USING_PICKER"

.field public static final VERSION_SUPPORTED:Ljava/lang/String; = "miui.app.resourcebrowser.VERSION_SUPPORTED"

.field public static final ZIP_RESOURCE:I
