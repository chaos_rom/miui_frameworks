.class Lmiui/app/resourcebrowser/ResourceDetailActivity$11;
.super Lmiui/app/resourcebrowser/view/ResourceOperationHandler;
.source "ResourceDetailActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiui/app/resourcebrowser/ResourceDetailActivity;->getOperationHandler(Lmiui/app/resourcebrowser/view/ResourceOperationView;)Lmiui/app/resourcebrowser/view/ResourceOperationHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;


# direct methods
.method constructor <init>(Lmiui/app/resourcebrowser/ResourceDetailActivity;Landroid/app/Activity;Lmiui/app/resourcebrowser/view/ResourceOperationView;Lmiui/app/resourcebrowser/view/ResourceState;)V
    .registers 5
    .parameter
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$11;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    invoke-direct {p0, p2, p3, p4}, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;-><init>(Landroid/app/Activity;Lmiui/app/resourcebrowser/view/ResourceOperationView;Lmiui/app/resourcebrowser/view/ResourceState;)V

    return-void
.end method


# virtual methods
.method public handleApplyEvent()V
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$11;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->apply()V

    invoke-super {p0}, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->handleApplyEvent()V

    return-void
.end method

.method public handleDeleteEvent()V
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$11;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->delete()V

    invoke-super {p0}, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->handleDeleteEvent()V

    return-void
.end method

.method public handleDownloadEvent()V
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$11;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->download()V

    invoke-super {p0}, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->handleDownloadEvent()V

    return-void
.end method

.method public handlePickEvent()V
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$11;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->pick()V

    invoke-super {p0}, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->handlePickEvent()V

    return-void
.end method

.method public handleResourceDownloadFailedEvent(Ljava/lang/String;)V
    .registers 3
    .parameter "downloadFilePath"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$11;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    invoke-virtual {v0, p1}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->downloadFailed(Ljava/lang/String;)V

    invoke-super {p0, p1}, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->handleResourceDownloadFailedEvent(Ljava/lang/String;)V

    return-void
.end method

.method public handleResourceDownloadSuccessedEvent(Ljava/lang/String;)V
    .registers 3
    .parameter "downloadFilePath"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$11;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    invoke-virtual {v0, p1}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->downloadSuccess(Ljava/lang/String;)V

    invoke-super {p0, p1}, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->handleResourceDownloadSuccessedEvent(Ljava/lang/String;)V

    return-void
.end method

.method public handleUpdateEvent()V
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$11;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->update()V

    invoke-super {p0}, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->handleUpdateEvent()V

    return-void
.end method
