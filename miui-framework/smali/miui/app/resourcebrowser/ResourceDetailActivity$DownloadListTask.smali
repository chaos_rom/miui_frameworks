.class public Lmiui/app/resourcebrowser/ResourceDetailActivity$DownloadListTask;
.super Lmiui/app/resourcebrowser/util/DownloadFileTask;
.source "ResourceDetailActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/app/resourcebrowser/ResourceDetailActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "DownloadListTask"
.end annotation


# instance fields
.field private offset:I

.field final synthetic this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;


# direct methods
.method public constructor <init>(Lmiui/app/resourcebrowser/ResourceDetailActivity;)V
    .registers 3
    .parameter

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$DownloadListTask;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    invoke-direct {p0}, Lmiui/app/resourcebrowser/util/DownloadFileTask;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$DownloadListTask;->offset:I

    return-void
.end method


# virtual methods
.method public getOffset()I
    .registers 2

    .prologue
    iget v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$DownloadListTask;->offset:I

    return v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 2
    .parameter "x0"

    .prologue
    check-cast p1, Ljava/util/List;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/app/resourcebrowser/ResourceDetailActivity$DownloadListTask;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .registers 9
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, result:Ljava/util/List;,"Ljava/util/List<Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;>;"
    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$DownloadListTask;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    invoke-virtual {v3}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->isFinishing()Z

    move-result v3

    if-eqz v3, :cond_b

    :goto_a
    return-void

    :cond_b
    const/4 v1, 0x0

    .local v1, filePath:Ljava/lang/String;
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_1c

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;

    invoke-virtual {v3}, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->getPath()Ljava/lang/String;

    move-result-object v1

    :cond_1c
    if-nez v1, :cond_36

    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$DownloadListTask;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    iput-boolean v6, v3, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mReachBottom:Z

    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$DownloadListTask;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    const v4, 0x60c0020

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    :goto_2e
    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$DownloadListTask;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    iget-object v3, v3, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mDownloadSet:Ljava/util/Set;

    invoke-interface {v3, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_a

    :cond_36
    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$DownloadListTask;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    iget-object v3, v3, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mParser:Lmiui/app/resourcebrowser/service/ResourceDataParser;

    iget-object v4, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$DownloadListTask;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    iget-object v4, v4, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mMetaData:Landroid/os/Bundle;

    invoke-virtual {v3, v1, v4}, Lmiui/app/resourcebrowser/service/ResourceDataParser;->readResources(Ljava/lang/String;Landroid/os/Bundle;)Ljava/util/List;

    move-result-object v2

    .local v2, resources:Ljava/util/List;,"Ljava/util/List<Lmiui/app/resourcebrowser/resource/Resource;>;"
    if-eqz v2, :cond_4a

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_4f

    :cond_4a
    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$DownloadListTask;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    iput-boolean v6, v3, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mReachBottom:Z

    goto :goto_2e

    :cond_4f
    iget v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$DownloadListTask;->offset:I

    if-nez v3, :cond_64

    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$DownloadListTask;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    iget-object v3, v3, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceSet:Lmiui/app/resourcebrowser/resource/ResourceSet;

    iget-object v4, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$DownloadListTask;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    iget v4, v4, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceGroup:I

    invoke-virtual {v3, v2, v4}, Lmiui/app/resourcebrowser/resource/ResourceSet;->set(Ljava/util/List;I)V

    :cond_5e
    :goto_5e
    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$DownloadListTask;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    invoke-virtual {v3}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->updateNavigationState()V

    goto :goto_2e

    :cond_64
    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$DownloadListTask;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    iget-object v3, v3, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceSet:Lmiui/app/resourcebrowser/resource/ResourceSet;

    iget-object v4, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$DownloadListTask;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    iget v4, v4, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceGroup:I

    invoke-virtual {v3, v4}, Lmiui/app/resourcebrowser/resource/ResourceSet;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmiui/widget/DataGroup;

    invoke-virtual {v3}, Lmiui/widget/DataGroup;->size()I

    move-result v0

    .local v0, count:I
    iget v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$DownloadListTask;->offset:I

    if-ne v3, v0, :cond_5e

    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$DownloadListTask;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    iget-object v3, v3, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceSet:Lmiui/app/resourcebrowser/resource/ResourceSet;

    iget-object v4, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$DownloadListTask;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    iget v4, v4, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceGroup:I

    invoke-virtual {v3, v4}, Lmiui/app/resourcebrowser/resource/ResourceSet;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmiui/widget/DataGroup;

    invoke-virtual {v3, v2}, Lmiui/widget/DataGroup;->addAll(Ljava/util/Collection;)Z

    goto :goto_5e
.end method

.method public setOffset(I)V
    .registers 2
    .parameter "offset"

    .prologue
    iput p1, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$DownloadListTask;->offset:I

    return-void
.end method
