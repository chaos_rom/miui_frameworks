.class public Lmiui/app/resourcebrowser/ResourceDetailActivity;
.super Landroid/app/Activity;
.source "ResourceDetailActivity.java"

# interfaces
.implements Lmiui/app/SDCardMonitor$SDCardStatusListener;
.implements Lmiui/app/resourcebrowser/IntentConstants;
.implements Lmiui/app/resourcebrowser/service/online/OnlineProtocolConstants;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/app/resourcebrowser/ResourceDetailActivity$DownloadDetailTask;,
        Lmiui/app/resourcebrowser/ResourceDetailActivity$DownloadListTask;,
        Lmiui/app/resourcebrowser/ResourceDetailActivity$PreviewInfo;
    }
.end annotation


# instance fields
.field private final PREVIEW_INDEX:Ljava/lang/String;

.field protected mBatchPlayer:Lmiui/app/resourcebrowser/util/BatchMediaPlayer;

.field protected mCoverView:Landroid/widget/ImageView;

.field protected mDetailFolder:Ljava/lang/String;

.field protected mDownloadSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lmiui/app/resourcebrowser/util/DownloadFileTask;",
            ">;"
        }
    .end annotation
.end field

.field protected mFullScreen:Z

.field private mFullScreenParams:Landroid/view/ViewGroup$LayoutParams;

.field protected mHandler:Landroid/os/Handler;

.field protected mHasActionBar:Z

.field private mImageCacheDecoder:Lmiui/app/resourcebrowser/util/ImageCacheDecoder;

.field private mImageParams:Landroid/view/ViewGroup$LayoutParams;

.field protected mInflater:Landroid/view/LayoutInflater;

.field protected mInfo:Landroid/widget/FrameLayout;

.field protected mIsOnlineResourceSet:Z

.field protected mIsSingleResourceSet:Z

.field protected mListFolder:Ljava/lang/String;

.field protected mLocalPath:Ljava/lang/String;

.field protected mMetaData:Landroid/os/Bundle;

.field protected mNextItem:Landroid/view/View;

.field private mNormalParams:Landroid/view/ViewGroup$LayoutParams;

.field protected mOnlinePath:Ljava/lang/String;

.field protected mOperationHandler:Lmiui/app/resourcebrowser/view/ResourceOperationHandler;

.field protected mOperationView:Lmiui/app/resourcebrowser/view/ResourceOperationView;

.field protected mParser:Lmiui/app/resourcebrowser/service/ResourceDataParser;

.field protected mPlayButton:Landroid/widget/ImageView;

.field protected mPreview:Lmiui/app/resourcebrowser/util/ResourceScreenView;

.field private mPreviewInfos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lmiui/app/resourcebrowser/ResourceDetailActivity$PreviewInfo;",
            ">;"
        }
    .end annotation
.end field

.field protected mPreviewOffset:I

.field protected mPreviewWidth:I

.field protected mPreviousItem:Landroid/view/View;

.field protected mReachBottom:Z

.field protected mResourceGroup:I

.field protected mResourceIndex:I

.field protected mResourceSet:Lmiui/app/resourcebrowser/resource/ResourceSet;

.field protected mResourceSetCategory:I

.field protected mResourceSetCode:Ljava/lang/String;

.field protected mResourceSetPackage:Ljava/lang/String;

.field protected mResourceSetSubpackage:Ljava/lang/String;

.field protected mRingtoneName:Landroid/widget/TextView;

.field protected mSDCardMonitor:Lmiui/app/SDCardMonitor;

.field private mScreenViewBackgroudId:I

.field private mScreenViewNeedBackgroud:Z

.field protected mScrollOutListener:Lmiui/widget/ScrollableScreenView$OnScrollOutListener;

.field protected mService:Lmiui/app/resourcebrowser/service/online/OnlineService;

.field protected mTitle:Ljava/lang/String;

.field protected mToFullScreenModeAnimator:Landroid/animation/ObjectAnimator;

.field protected mToNormalModeAnimator:Landroid/animation/ObjectAnimator;

.field protected mUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, -0x1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const-string v0, "PREVIEW_INDEX"

    iput-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->PREVIEW_INDEX:Ljava/lang/String;

    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mFullScreenParams:Landroid/view/ViewGroup$LayoutParams;

    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mImageParams:Landroid/view/ViewGroup$LayoutParams;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mDownloadSet:Ljava/util/Set;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mHandler:Landroid/os/Handler;

    invoke-static {}, Lmiui/app/resourcebrowser/service/online/OnlineService;->getInstance()Lmiui/app/resourcebrowser/service/online/OnlineService;

    move-result-object v0

    iput-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mService:Lmiui/app/resourcebrowser/service/online/OnlineService;

    invoke-static {}, Lmiui/app/resourcebrowser/service/ResourceJSONDataParser;->getInstance()Lmiui/app/resourcebrowser/service/ResourceJSONDataParser;

    move-result-object v0

    iput-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mParser:Lmiui/app/resourcebrowser/service/ResourceDataParser;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreviewInfos:Ljava/util/List;

    return-void
.end method

.method static synthetic access$000(Lmiui/app/resourcebrowser/ResourceDetailActivity;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    invoke-direct {p0, p1}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->initImageForScreenView(I)V

    return-void
.end method

.method static synthetic access$1000(Lmiui/app/resourcebrowser/ResourceDetailActivity;)V
    .registers 1
    .parameter "x0"

    .prologue
    invoke-direct {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->onEndEnterFullScreenMode()V

    return-void
.end method

.method static synthetic access$1100(Lmiui/app/resourcebrowser/ResourceDetailActivity;)V
    .registers 1
    .parameter "x0"

    .prologue
    invoke-direct {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->enterFullScreenMode()V

    return-void
.end method

.method static synthetic access$1200(Lmiui/app/resourcebrowser/ResourceDetailActivity;)V
    .registers 1
    .parameter "x0"

    .prologue
    invoke-direct {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->enterNormalMode()V

    return-void
.end method

.method static synthetic access$1300(Lmiui/app/resourcebrowser/ResourceDetailActivity;)V
    .registers 1
    .parameter "x0"

    .prologue
    invoke-direct {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->initPlayer()V

    return-void
.end method

.method static synthetic access$200(Lmiui/app/resourcebrowser/ResourceDetailActivity;Ljava/lang/String;)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    invoke-direct {p0, p1}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->getScreenViewIndexForPreviewImage(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lmiui/app/resourcebrowser/ResourceDetailActivity;I)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    invoke-direct {p0, p1}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->isVisiableScreen(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lmiui/app/resourcebrowser/ResourceDetailActivity;)Lmiui/app/resourcebrowser/util/ImageCacheDecoder;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mImageCacheDecoder:Lmiui/app/resourcebrowser/util/ImageCacheDecoder;

    return-object v0
.end method

.method static synthetic access$500(Lmiui/app/resourcebrowser/ResourceDetailActivity;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    invoke-direct {p0, p1}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->decodeImageForScreenView(I)V

    return-void
.end method

.method static synthetic access$600(Lmiui/app/resourcebrowser/ResourceDetailActivity;)Ljava/util/List;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreviewInfos:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$700(Lmiui/app/resourcebrowser/ResourceDetailActivity;)V
    .registers 1
    .parameter "x0"

    .prologue
    invoke-direct {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->onBeginEnterNormalMode()V

    return-void
.end method

.method static synthetic access$800(Lmiui/app/resourcebrowser/ResourceDetailActivity;)V
    .registers 1
    .parameter "x0"

    .prologue
    invoke-direct {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->onEndEnterNormalMode()V

    return-void
.end method

.method static synthetic access$900(Lmiui/app/resourcebrowser/ResourceDetailActivity;)V
    .registers 1
    .parameter "x0"

    .prologue
    invoke-direct {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->onBeginEnterFullScreenMode()V

    return-void
.end method

.method private addImageView(Landroid/widget/ImageView;)V
    .registers 5
    .parameter "imageView"

    .prologue
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .local v0, imageContainer:Landroid/widget/FrameLayout;
    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    invoke-direct {p0, v0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->enterNormalMode(Landroid/view/View;)V

    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreview:Lmiui/app/resourcebrowser/util/ResourceScreenView;

    iget-object v2, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mNormalParams:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v1, v0, v2}, Lmiui/app/resourcebrowser/util/ResourceScreenView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private bindScreenImageView()V
    .registers 18

    .prologue
    invoke-direct/range {p0 .. p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->initImageDecoder()V

    move-object/from16 v0, p0

    iget-object v14, v0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreviewInfos:Ljava/util/List;

    invoke-interface {v14}, Ljava/util/List;->clear()V

    move-object/from16 v0, p0

    iget v14, v0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceSetCategory:I

    if-nez v14, :cond_32

    move-object/from16 v0, p0

    iget-object v14, v0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreview:Lmiui/app/resourcebrowser/util/ResourceScreenView;

    move-object/from16 v0, p0

    iget-object v15, v0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mInfo:Landroid/widget/FrameLayout;

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Lmiui/app/resourcebrowser/util/ResourceScreenView;->addView(Landroid/view/View;I)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreviewInfos:Ljava/util/List;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-interface/range {v14 .. v16}, Ljava/util/List;->add(ILjava/lang/Object;)V

    move-object/from16 v0, p0

    iget v14, v0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreviewOffset:I

    if-nez v14, :cond_32

    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput v14, v0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreviewOffset:I

    :cond_32
    invoke-virtual/range {p0 .. p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->getCurrentResourceInformation()Landroid/os/Bundle;

    move-result-object v1

    .local v1, bundle:Landroid/os/Bundle;
    const-string v14, "LOCAL_PREVIEW"

    invoke-virtual {v1, v14}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v9

    .local v9, localPreview:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const-string v14, "ONLINE_PREVIEW"

    invoke-virtual {v1, v14}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v10

    .local v10, onlinePreview:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    if-nez v9, :cond_45

    :goto_44
    return-void

    :cond_45
    invoke-virtual/range {p0 .. p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->getCurrentResource()Lmiui/app/resourcebrowser/resource/Resource;

    move-result-object v14

    invoke-virtual {v14}, Lmiui/app/resourcebrowser/resource/Resource;->getStatus()I

    move-result v14

    const/4 v15, 0x2

    if-ne v14, v15, :cond_dc

    const/4 v13, 0x1

    .local v13, updatePreview:Z
    :goto_51
    const-string v14, "MODIFIED_TIME"

    invoke-virtual {v1, v14}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    .local v6, lastUpdate:J
    const/16 v14, 0xf

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v15

    invoke-static {v14, v15}, Ljava/lang/Math;->min(II)I

    move-result v2

    .local v2, count:I
    const/4 v4, 0x0

    .local v4, i:I
    :goto_62
    if-ge v4, v2, :cond_e1

    new-instance v5, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .local v5, imageView:Landroid/widget/ImageView;
    sget-object v14, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v5, v14}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    const v14, 0x60200c3

    invoke-virtual {v5, v14}, Landroid/widget/ImageView;->setImageResource(I)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mImageParams:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v5, v14}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mScreenViewNeedBackgroud:Z

    if-eqz v14, :cond_99

    move-object/from16 v0, p0

    iget v14, v0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mScreenViewBackgroudId:I

    invoke-virtual {v5, v14}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    invoke-virtual/range {p0 .. p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x60a0029

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v14

    float-to-int v12, v14

    .local v12, padding:I
    invoke-virtual {v5, v12, v12, v12, v12}, Landroid/widget/ImageView;->setPadding(IIII)V

    .end local v12           #padding:I
    :cond_99
    invoke-interface {v9, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .local v8, localPath:Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .local v3, file:Ljava/io/File;
    if-eqz v10, :cond_b9

    if-eqz v13, :cond_b9

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v14

    if-eqz v14, :cond_b9

    invoke-virtual {v3}, Ljava/io/File;->lastModified()J

    move-result-wide v14

    cmp-long v14, v14, v6

    if-gez v14, :cond_b9

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    :cond_b9
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->addImageView(Landroid/widget/ImageView;)V

    new-instance v11, Lmiui/app/resourcebrowser/ResourceDetailActivity$PreviewInfo;

    const/4 v14, 0x0

    move-object/from16 v0, p0

    invoke-direct {v11, v0, v14}, Lmiui/app/resourcebrowser/ResourceDetailActivity$PreviewInfo;-><init>(Lmiui/app/resourcebrowser/ResourceDetailActivity;Lmiui/app/resourcebrowser/ResourceDetailActivity$1;)V

    .local v11, pInfo:Lmiui/app/resourcebrowser/ResourceDetailActivity$PreviewInfo;
    iput-object v8, v11, Lmiui/app/resourcebrowser/ResourceDetailActivity$PreviewInfo;->localPath:Ljava/lang/String;

    if-eqz v10, :cond_df

    invoke-interface {v10, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    :goto_d0
    iput-object v14, v11, Lmiui/app/resourcebrowser/ResourceDetailActivity$PreviewInfo;->onlinePath:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v14, v0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreviewInfos:Ljava/util/List;

    invoke-interface {v14, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_62

    .end local v2           #count:I
    .end local v3           #file:Ljava/io/File;
    .end local v4           #i:I
    .end local v5           #imageView:Landroid/widget/ImageView;
    .end local v6           #lastUpdate:J
    .end local v8           #localPath:Ljava/lang/String;
    .end local v11           #pInfo:Lmiui/app/resourcebrowser/ResourceDetailActivity$PreviewInfo;
    .end local v13           #updatePreview:Z
    :cond_dc
    const/4 v13, 0x0

    goto/16 :goto_51

    .restart local v2       #count:I
    .restart local v3       #file:Ljava/io/File;
    .restart local v4       #i:I
    .restart local v5       #imageView:Landroid/widget/ImageView;
    .restart local v6       #lastUpdate:J
    .restart local v8       #localPath:Ljava/lang/String;
    .restart local v11       #pInfo:Lmiui/app/resourcebrowser/ResourceDetailActivity$PreviewInfo;
    .restart local v13       #updatePreview:Z
    :cond_df
    const/4 v14, 0x0

    goto :goto_d0

    .end local v3           #file:Ljava/io/File;
    .end local v5           #imageView:Landroid/widget/ImageView;
    .end local v8           #localPath:Ljava/lang/String;
    .end local v11           #pInfo:Lmiui/app/resourcebrowser/ResourceDetailActivity$PreviewInfo;
    :cond_e1
    if-nez v2, :cond_107

    new-instance v5, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .restart local v5       #imageView:Landroid/widget/ImageView;
    sget-object v14, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v5, v14}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    const v14, 0x60201ba

    invoke-virtual {v5, v14}, Landroid/widget/ImageView;->setImageResource(I)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mImageParams:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v5, v14}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->addImageView(Landroid/widget/ImageView;)V

    const/4 v14, 0x0

    invoke-virtual {v5, v14}, Landroid/widget/ImageView;->setClickable(Z)V

    goto/16 :goto_44

    .end local v5           #imageView:Landroid/widget/ImageView;
    :cond_107
    move-object/from16 v0, p0

    iget v14, v0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreviewOffset:I

    add-int/lit8 v14, v14, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->initImageForScreenView(I)V

    goto/16 :goto_44
.end method

.method private buildModeChangeAnimator()V
    .registers 15

    .prologue
    const-wide/16 v12, 0xc8

    const/4 v11, 0x3

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v8, 0x2

    const-string v6, "scaleX"

    new-array v7, v8, [F

    fill-array-data v7, :array_86

    invoke-static {v6, v7}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v2

    .local v2, pvhScaleX:Landroid/animation/PropertyValuesHolder;
    const-string v6, "scaleY"

    new-array v7, v8, [F

    fill-array-data v7, :array_8e

    invoke-static {v6, v7}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v4

    .local v4, pvhScaleY:Landroid/animation/PropertyValuesHolder;
    const-string v6, "alpha"

    new-array v7, v8, [F

    fill-array-data v7, :array_96

    invoke-static {v6, v7}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    .local v0, pvhAlpha:Landroid/animation/PropertyValuesHolder;
    iget-object v6, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mCoverView:Landroid/widget/ImageView;

    new-array v7, v11, [Landroid/animation/PropertyValuesHolder;

    aput-object v2, v7, v9

    aput-object v4, v7, v10

    aput-object v0, v7, v8

    invoke-static {v6, v7}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v6

    invoke-virtual {v6, v12, v13}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v6

    iput-object v6, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mToNormalModeAnimator:Landroid/animation/ObjectAnimator;

    iget-object v6, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mToNormalModeAnimator:Landroid/animation/ObjectAnimator;

    new-instance v7, Lmiui/app/resourcebrowser/ResourceDetailActivity$5;

    invoke-direct {v7, p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity$5;-><init>(Lmiui/app/resourcebrowser/ResourceDetailActivity;)V

    invoke-virtual {v6, v7}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    const-string v6, "scaleX"

    new-array v7, v8, [F

    fill-array-data v7, :array_9e

    invoke-static {v6, v7}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v3

    .local v3, pvhScaleX2:Landroid/animation/PropertyValuesHolder;
    const-string v6, "scaleY"

    new-array v7, v8, [F

    fill-array-data v7, :array_a6

    invoke-static {v6, v7}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    .local v5, pvhScaleY2:Landroid/animation/PropertyValuesHolder;
    const-string v6, "alpha"

    new-array v7, v8, [F

    fill-array-data v7, :array_ae

    invoke-static {v6, v7}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    .local v1, pvhAlpha2:Landroid/animation/PropertyValuesHolder;
    iget-object v6, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mCoverView:Landroid/widget/ImageView;

    new-array v7, v11, [Landroid/animation/PropertyValuesHolder;

    aput-object v3, v7, v9

    aput-object v5, v7, v10

    aput-object v1, v7, v8

    invoke-static {v6, v7}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v6

    invoke-virtual {v6, v12, v13}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v6

    iput-object v6, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mToFullScreenModeAnimator:Landroid/animation/ObjectAnimator;

    iget-object v6, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mToFullScreenModeAnimator:Landroid/animation/ObjectAnimator;

    new-instance v7, Lmiui/app/resourcebrowser/ResourceDetailActivity$6;

    invoke-direct {v7, p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity$6;-><init>(Lmiui/app/resourcebrowser/ResourceDetailActivity;)V

    invoke-virtual {v6, v7}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    return-void

    nop

    :array_86
    .array-data 0x4
        0x0t 0x0t 0x80t 0x3ft
        0x33t 0x33t 0x33t 0x3ft
    .end array-data

    :array_8e
    .array-data 0x4
        0x0t 0x0t 0x80t 0x3ft
        0x33t 0x33t 0x33t 0x3ft
    .end array-data

    :array_96
    .array-data 0x4
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
    .end array-data

    :array_9e
    .array-data 0x4
        0x33t 0x33t 0x33t 0x3ft
        0x0t 0x0t 0x80t 0x3ft
    .end array-data

    :array_a6
    .array-data 0x4
        0x33t 0x33t 0x33t 0x3ft
        0x0t 0x0t 0x80t 0x3ft
    .end array-data

    :array_ae
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
    .end array-data
.end method

.method private decodeImageForScreenView(I)V
    .registers 9
    .parameter "screenViewIndex"

    .prologue
    if-ltz p1, :cond_12

    iget-object v5, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreviewInfos:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge p1, v5, :cond_12

    iget-object v5, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreviewInfos:Ljava/util/List;

    invoke-interface {v5, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    if-nez v5, :cond_13

    :cond_12
    :goto_12
    return-void

    :cond_13
    iget-object v5, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreviewInfos:Ljava/util/List;

    invoke-interface {v5, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lmiui/app/resourcebrowser/ResourceDetailActivity$PreviewInfo;

    .local v4, pInfo:Lmiui/app/resourcebrowser/ResourceDetailActivity$PreviewInfo;
    iget-object v2, v4, Lmiui/app/resourcebrowser/ResourceDetailActivity$PreviewInfo;->localPath:Ljava/lang/String;

    .local v2, localPath:Ljava/lang/String;
    iget-object v3, v4, Lmiui/app/resourcebrowser/ResourceDetailActivity$PreviewInfo;->onlinePath:Ljava/lang/String;

    .local v3, onlinePath:Ljava/lang/String;
    iget-object v5, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mImageCacheDecoder:Lmiui/app/resourcebrowser/util/ImageCacheDecoder;

    invoke-virtual {v5, v2}, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .local v0, bitmap:Landroid/graphics/Bitmap;
    if-eqz v0, :cond_3a

    iget-object v5, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreview:Lmiui/app/resourcebrowser/util/ResourceScreenView;

    invoke-virtual {v5, p1}, Lmiui/app/resourcebrowser/util/ResourceScreenView;->getScreen(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .local v1, imageView:Landroid/widget/ImageView;
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_12

    .end local v1           #imageView:Landroid/widget/ImageView;
    :cond_3a
    iget-object v5, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mImageCacheDecoder:Lmiui/app/resourcebrowser/util/ImageCacheDecoder;

    invoke-virtual {v5, v2, v3, p1}, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->decodeImageAsync(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_12
.end method

.method private enterFullScreenMode()V
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreview:Lmiui/app/resourcebrowser/util/ResourceScreenView;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/util/ResourceScreenView;->getCurrentScreenIndex()I

    move-result v0

    if-nez v0, :cond_c

    iget v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceSetCategory:I

    if-eqz v0, :cond_11

    :cond_c
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mToFullScreenModeAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    :cond_11
    return-void
.end method

.method private enterFullScreenMode(Landroid/view/View;)V
    .registers 5
    .parameter "imageContainer"

    .prologue
    const/4 v2, 0x0

    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mFullScreenParams:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p1, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p1, v2, v2, v2, v2}, Landroid/view/View;->setPadding(IIII)V

    const/high16 v1, -0x100

    invoke-virtual {p1, v1}, Landroid/view/View;->setBackgroundColor(I)V

    check-cast p1, Landroid/view/ViewGroup;

    .end local p1
    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .local v0, imageView:Landroid/widget/ImageView;
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setAdjustViewBounds(Z)V

    new-instance v1, Lmiui/app/resourcebrowser/ResourceDetailActivity$8;

    invoke-direct {v1, p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity$8;-><init>(Lmiui/app/resourcebrowser/ResourceDetailActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private enterNormalMode()V
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mToNormalModeAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    return-void
.end method

.method private enterNormalMode(Landroid/view/View;)V
    .registers 6
    .parameter "imageContainer"

    .prologue
    const/4 v3, 0x6

    const/4 v2, 0x0

    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mNormalParams:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p1, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/16 v1, 0x3c

    invoke-virtual {p1, v3, v2, v3, v1}, Landroid/view/View;->setPadding(IIII)V

    invoke-virtual {p1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    check-cast p1, Landroid/view/ViewGroup;

    .end local p1
    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .local v0, imageView:Landroid/widget/ImageView;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAdjustViewBounds(Z)V

    new-instance v1, Lmiui/app/resourcebrowser/ResourceDetailActivity$7;

    invoke-direct {v1, p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity$7;-><init>(Lmiui/app/resourcebrowser/ResourceDetailActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private getScreenViewIndexForPreviewImage(Ljava/lang/String;)I
    .registers 5
    .parameter "imageLocalPath"

    .prologue
    const/4 v0, 0x0

    .local v0, i:I
    :goto_1
    iget-object v2, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreviewInfos:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_1f

    iget-object v2, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreviewInfos:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/app/resourcebrowser/ResourceDetailActivity$PreviewInfo;

    .local v1, pInfo:Lmiui/app/resourcebrowser/ResourceDetailActivity$PreviewInfo;
    if-eqz v1, :cond_1c

    iget-object v2, v1, Lmiui/app/resourcebrowser/ResourceDetailActivity$PreviewInfo;->localPath:Ljava/lang/String;

    invoke-static {v2, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1c

    .end local v0           #i:I
    .end local v1           #pInfo:Lmiui/app/resourcebrowser/ResourceDetailActivity$PreviewInfo;
    :goto_1b
    return v0

    .restart local v0       #i:I
    .restart local v1       #pInfo:Lmiui/app/resourcebrowser/ResourceDetailActivity$PreviewInfo;
    :cond_1c
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .end local v1           #pInfo:Lmiui/app/resourcebrowser/ResourceDetailActivity$PreviewInfo;
    :cond_1f
    const/4 v0, -0x1

    goto :goto_1b
.end method

.method private initImageDecoder()V
    .registers 3

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mImageCacheDecoder:Lmiui/app/resourcebrowser/util/ImageCacheDecoder;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mImageCacheDecoder:Lmiui/app/resourcebrowser/util/ImageCacheDecoder;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->clean(Z)V

    :cond_a
    new-instance v0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;-><init>(I)V

    iput-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mImageCacheDecoder:Lmiui/app/resourcebrowser/util/ImageCacheDecoder;

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mImageCacheDecoder:Lmiui/app/resourcebrowser/util/ImageCacheDecoder;

    new-instance v1, Lmiui/app/resourcebrowser/ResourceDetailActivity$4;

    invoke-direct {v1, p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity$4;-><init>(Lmiui/app/resourcebrowser/ResourceDetailActivity;)V

    invoke-virtual {v0, v1}, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->regeisterListener(Lmiui/app/resourcebrowser/util/ImageCacheDecoder$ImageDecodingListener;)V

    return-void
.end method

.method private initImageForScreenView(I)V
    .registers 3
    .parameter "currentScreen"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mImageCacheDecoder:Lmiui/app/resourcebrowser/util/ImageCacheDecoder;

    invoke-virtual {v0, p1}, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->setCurrentUseBitmapIndex(I)V

    add-int/lit8 v0, p1, 0x0

    invoke-direct {p0, v0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->decodeImageForScreenView(I)V

    add-int/lit8 v0, p1, 0x1

    invoke-direct {p0, v0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->decodeImageForScreenView(I)V

    add-int/lit8 v0, p1, -0x1

    invoke-direct {p0, v0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->decodeImageForScreenView(I)V

    return-void
.end method

.method private initPlayer()V
    .registers 3

    .prologue
    new-instance v0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;

    invoke-direct {v0, p0}, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mBatchPlayer:Lmiui/app/resourcebrowser/util/BatchMediaPlayer;

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mBatchPlayer:Lmiui/app/resourcebrowser/util/BatchMediaPlayer;

    new-instance v1, Lmiui/app/resourcebrowser/ResourceDetailActivity$9;

    invoke-direct {v1, p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity$9;-><init>(Lmiui/app/resourcebrowser/ResourceDetailActivity;)V

    invoke-virtual {v0, v1}, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->setListener(Lmiui/app/resourcebrowser/util/BatchMediaPlayer$BatchPlayerListener;)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mBatchPlayer:Lmiui/app/resourcebrowser/util/BatchMediaPlayer;

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->getCurrentResource()Lmiui/app/resourcebrowser/resource/Resource;

    move-result-object v1

    invoke-virtual {p0, v1}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->getMusicPlayList(Lmiui/app/resourcebrowser/resource/Resource;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->setPlayList(Ljava/util/List;)V

    return-void
.end method

.method private isVisiableScreen(I)Z
    .registers 4
    .parameter "screen"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mImageCacheDecoder:Lmiui/app/resourcebrowser/util/ImageCacheDecoder;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->getCurrentUseBitmapIndex()I

    move-result v0

    sub-int v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_11

    const/4 v0, 0x1

    :goto_10
    return v0

    :cond_11
    const/4 v0, 0x0

    goto :goto_10
.end method

.method private onBeginEnterFullScreenMode()V
    .registers 3

    .prologue
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreview:Lmiui/app/resourcebrowser/util/ResourceScreenView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmiui/app/resourcebrowser/util/ResourceScreenView;->setClickable(Z)V

    return-void
.end method

.method private onBeginEnterNormalMode()V
    .registers 8

    .prologue
    const/4 v4, 0x0

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    const/16 v5, 0x400

    invoke-virtual {v3, v5}, Landroid/view/Window;->clearFlags(I)V

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/ActionBar;->show()V

    const/4 v2, 0x0

    .local v2, i:I
    :goto_12
    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreview:Lmiui/app/resourcebrowser/util/ResourceScreenView;

    invoke-virtual {v3}, Lmiui/app/resourcebrowser/util/ResourceScreenView;->getScreenCount()I

    move-result v3

    if-ge v2, v3, :cond_26

    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreview:Lmiui/app/resourcebrowser/util/ResourceScreenView;

    invoke-virtual {v3, v2}, Lmiui/app/resourcebrowser/util/ResourceScreenView;->getScreen(I)Landroid/view/View;

    move-result-object v3

    invoke-direct {p0, v3}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->enterNormalMode(Landroid/view/View;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_12

    :cond_26
    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreview:Lmiui/app/resourcebrowser/util/ResourceScreenView;

    invoke-virtual {v3}, Lmiui/app/resourcebrowser/util/ResourceScreenView;->getCurrentScreenIndex()I

    move-result v1

    .local v1, current:I
    iget v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceSetCategory:I

    if-nez v3, :cond_44

    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreview:Lmiui/app/resourcebrowser/util/ResourceScreenView;

    iget-object v5, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mInfo:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v5, v4}, Lmiui/app/resourcebrowser/util/ResourceScreenView;->addView(Landroid/view/View;I)V

    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreviewInfos:Ljava/util/List;

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, Ljava/util/List;->add(ILjava/lang/Object;)V

    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreview:Lmiui/app/resourcebrowser/util/ResourceScreenView;

    add-int/lit8 v5, v1, 0x1

    invoke-virtual {v3, v5}, Lmiui/app/resourcebrowser/util/ResourceScreenView;->setCurrentScreen(I)V

    :cond_44
    iget-boolean v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mHasActionBar:Z

    if-eqz v3, :cond_5c

    const v3, 0x60b007e

    invoke-virtual {p0, v3}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .local v0, childRoot:Landroid/widget/LinearLayout;
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/ActionBar;->getHeight()I

    move-result v3

    invoke-virtual {v0, v4, v3, v4, v4}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .end local v0           #childRoot:Landroid/widget/LinearLayout;
    :cond_5c
    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mOperationView:Lmiui/app/resourcebrowser/view/ResourceOperationView;

    invoke-virtual {v3, v4}, Lmiui/app/resourcebrowser/view/ResourceOperationView;->setVisibility(I)V

    iget-object v5, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreview:Lmiui/app/resourcebrowser/util/ResourceScreenView;

    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreview:Lmiui/app/resourcebrowser/util/ResourceScreenView;

    invoke-virtual {v3}, Lmiui/app/resourcebrowser/util/ResourceScreenView;->getScreenCount()I

    move-result v3

    const/4 v6, 0x1

    if-le v3, v6, :cond_7d

    move v3, v4

    :goto_6d
    invoke-virtual {v5, v3}, Lmiui/app/resourcebrowser/util/ResourceScreenView;->setSeekBarVisibility(I)V

    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreview:Lmiui/app/resourcebrowser/util/ResourceScreenView;

    invoke-virtual {v3, v4}, Lmiui/app/resourcebrowser/util/ResourceScreenView;->setBackgroundResource(I)V

    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreview:Lmiui/app/resourcebrowser/util/ResourceScreenView;

    invoke-virtual {v3, v4}, Lmiui/app/resourcebrowser/util/ResourceScreenView;->setClickable(Z)V

    iput-boolean v4, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mFullScreen:Z

    return-void

    :cond_7d
    const/16 v3, 0x8

    goto :goto_6d
.end method

.method private onEndEnterFullScreenMode()V
    .registers 9

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreview:Lmiui/app/resourcebrowser/util/ResourceScreenView;

    invoke-virtual {v3}, Lmiui/app/resourcebrowser/util/ResourceScreenView;->getCurrentScreenIndex()I

    move-result v1

    .local v1, current:I
    iget v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceSetCategory:I

    if-nez v3, :cond_18

    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreview:Lmiui/app/resourcebrowser/util/ResourceScreenView;

    invoke-virtual {v3, v5}, Lmiui/app/resourcebrowser/util/ResourceScreenView;->removeScreen(I)V

    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreviewInfos:Ljava/util/List;

    invoke-interface {v3, v5}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_18
    const/4 v2, 0x0

    .local v2, i:I
    :goto_19
    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreview:Lmiui/app/resourcebrowser/util/ResourceScreenView;

    invoke-virtual {v3}, Lmiui/app/resourcebrowser/util/ResourceScreenView;->getScreenCount()I

    move-result v3

    if-ge v2, v3, :cond_2d

    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreview:Lmiui/app/resourcebrowser/util/ResourceScreenView;

    invoke-virtual {v3, v2}, Lmiui/app/resourcebrowser/util/ResourceScreenView;->getScreen(I)Landroid/view/View;

    move-result-object v3

    invoke-direct {p0, v3}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->enterFullScreenMode(Landroid/view/View;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_19

    :cond_2d
    iget v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceSetCategory:I

    if-nez v3, :cond_38

    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreview:Lmiui/app/resourcebrowser/util/ResourceScreenView;

    add-int/lit8 v4, v1, -0x1

    invoke-virtual {v3, v4}, Lmiui/app/resourcebrowser/util/ResourceScreenView;->setCurrentScreen(I)V

    :cond_38
    iget-boolean v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mHasActionBar:Z

    if-eqz v3, :cond_48

    const v3, 0x60b007e

    invoke-virtual {p0, v3}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .local v0, childRoot:Landroid/widget/LinearLayout;
    invoke-virtual {v0, v5, v5, v5, v5}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .end local v0           #childRoot:Landroid/widget/LinearLayout;
    :cond_48
    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mOperationView:Lmiui/app/resourcebrowser/view/ResourceOperationView;

    invoke-virtual {v3, v7}, Lmiui/app/resourcebrowser/view/ResourceOperationView;->setVisibility(I)V

    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreview:Lmiui/app/resourcebrowser/util/ResourceScreenView;

    invoke-virtual {v3, v7}, Lmiui/app/resourcebrowser/util/ResourceScreenView;->setSeekBarVisibility(I)V

    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreview:Lmiui/app/resourcebrowser/util/ResourceScreenView;

    const/high16 v4, -0x100

    invoke-virtual {v3, v4}, Lmiui/app/resourcebrowser/util/ResourceScreenView;->setBackgroundColor(I)V

    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreview:Lmiui/app/resourcebrowser/util/ResourceScreenView;

    invoke-virtual {v3}, Lmiui/app/resourcebrowser/util/ResourceScreenView;->requestFocus()Z

    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreview:Lmiui/app/resourcebrowser/util/ResourceScreenView;

    invoke-virtual {v3, v6}, Lmiui/app/resourcebrowser/util/ResourceScreenView;->setClickable(Z)V

    iput-boolean v6, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mFullScreen:Z

    return-void
.end method

.method private onEndEnterNormalMode()V
    .registers 3

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreview:Lmiui/app/resourcebrowser/util/ResourceScreenView;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/util/ResourceScreenView;->requestFocus()Z

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreview:Lmiui/app/resourcebrowser/util/ResourceScreenView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmiui/app/resourcebrowser/util/ResourceScreenView;->setClickable(Z)V

    return-void
.end method

.method private requestResources(II)V
    .registers 16
    .parameter "start"
    .parameter "length"

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    const/4 v4, 0x1

    .local v4, resourcesPerLine:I
    mul-int/2addr p1, v4

    iget-object v7, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceSet:Lmiui/app/resourcebrowser/resource/ResourceSet;

    iget v8, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceGroup:I

    invoke-virtual {v7, v8}, Lmiui/app/resourcebrowser/resource/ResourceSet;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lmiui/widget/DataGroup;

    invoke-virtual {v7}, Lmiui/widget/DataGroup;->size()I

    move-result v7

    sub-int v7, p1, v7

    div-int/2addr v7, v4

    if-eqz v7, :cond_18

    :cond_17
    :goto_17
    return-void

    :cond_18
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mUrl:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "&start=%s&count=%s"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v11

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v12

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .local v6, url:Ljava/lang/String;
    iget-object v7, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mListFolder:Ljava/lang/String;

    invoke-static {v7, v6}, Lmiui/app/resourcebrowser/util/ResourceHelper;->getFilePathByURL(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .local v2, path:Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .local v1, file:Ljava/io/File;
    if-nez p1, :cond_6a

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_6a

    iget-object v7, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mParser:Lmiui/app/resourcebrowser/service/ResourceDataParser;

    iget-object v8, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mMetaData:Landroid/os/Bundle;

    invoke-virtual {v7, v2, v8}, Lmiui/app/resourcebrowser/service/ResourceDataParser;->readResources(Ljava/lang/String;Landroid/os/Bundle;)Ljava/util/List;

    move-result-object v3

    .local v3, resources:Ljava/util/List;,"Ljava/util/List<Lmiui/app/resourcebrowser/resource/Resource;>;"
    if-eqz v3, :cond_64

    iget-object v7, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceSet:Lmiui/app/resourcebrowser/resource/ResourceSet;

    iget v8, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceGroup:I

    invoke-virtual {v7, v3, v8}, Lmiui/app/resourcebrowser/resource/ResourceSet;->set(Ljava/util/List;I)V

    :cond_64
    invoke-static {v1}, Lmiui/app/resourcebrowser/util/ResourceHelper;->isCacheValid(Ljava/io/File;)Z

    move-result v7

    if-nez v7, :cond_17

    .end local v3           #resources:Ljava/util/List;,"Ljava/util/List<Lmiui/app/resourcebrowser/resource/Resource;>;"
    :cond_6a
    new-instance v5, Lmiui/app/resourcebrowser/ResourceDetailActivity$DownloadListTask;

    invoke-direct {v5, p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity$DownloadListTask;-><init>(Lmiui/app/resourcebrowser/ResourceDetailActivity;)V

    .local v5, task:Lmiui/app/resourcebrowser/ResourceDetailActivity$DownloadListTask;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "list_"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lmiui/app/resourcebrowser/ResourceDetailActivity$DownloadListTask;->setId(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Lmiui/app/resourcebrowser/ResourceDetailActivity$DownloadListTask;->setOffset(I)V

    iget-object v7, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mDownloadSet:Ljava/util/Set;

    invoke-interface {v7, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_17

    new-instance v0, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;

    invoke-direct {v0}, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;-><init>()V

    .local v0, entry:Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;
    invoke-virtual {v0, v6}, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->setUrl(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->setPath(Ljava/lang/String;)V

    iget-object v7, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mDownloadSet:Ljava/util/Set;

    invoke-interface {v7, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-array v7, v12, [Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;

    aput-object v0, v7, v11

    invoke-virtual {v5, v7}, Lmiui/app/resourcebrowser/ResourceDetailActivity$DownloadListTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_17
.end method

.method private stopMusic()V
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mBatchPlayer:Lmiui/app/resourcebrowser/util/BatchMediaPlayer;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mBatchPlayer:Lmiui/app/resourcebrowser/util/BatchMediaPlayer;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->stop()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mBatchPlayer:Lmiui/app/resourcebrowser/util/BatchMediaPlayer;

    :cond_c
    return-void
.end method


# virtual methods
.method protected apply()V
    .registers 1

    .prologue
    return-void
.end method

.method protected bindScreenRingtoneView()V
    .registers 8

    .prologue
    const/4 v6, 0x0

    const/4 v5, -0x2

    iget-object v2, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mInflater:Landroid/view/LayoutInflater;

    const v3, 0x603004d

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .local v1, ringtoneView:Landroid/view/View;
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v2, 0x11

    invoke-direct {v0, v5, v5, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .local v0, ringtoneParams:Landroid/widget/FrameLayout$LayoutParams;
    iget-object v2, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreview:Lmiui/app/resourcebrowser/util/ResourceScreenView;

    invoke-virtual {v2, v1, v0}, Lmiui/app/resourcebrowser/util/ResourceScreenView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    const v2, 0x60b0057

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPlayButton:Landroid/widget/ImageView;

    iget-object v2, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPlayButton:Landroid/widget/ImageView;

    new-instance v3, Lmiui/app/resourcebrowser/ResourceDetailActivity$10;

    invoke-direct {v3, p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity$10;-><init>(Lmiui/app/resourcebrowser/ResourceDetailActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mLocalPath:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3b

    iget-object v2, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPlayButton:Landroid/widget/ImageView;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_3b
    iget-object v2, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mMetaData:Landroid/os/Bundle;

    const-string v3, "miui.app.resourcebrowser.SHOW_RINGTONE_NAME"

    invoke-virtual {v2, v3, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_5e

    const v2, 0x60b0058

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mRingtoneName:Landroid/widget/TextView;

    iget-object v2, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mRingtoneName:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v2, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mRingtoneName:Landroid/widget/TextView;

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->getFormatTitleBeforePlayingRingtone()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_5e
    return-void
.end method

.method protected bindScreenView()V
    .registers 4

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreview:Lmiui/app/resourcebrowser/util/ResourceScreenView;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/util/ResourceScreenView;->removeAllScreens()V

    iget v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceSetCategory:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_27

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->bindScreenRingtoneView()V

    invoke-direct {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->stopMusic()V

    :goto_10
    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreview:Lmiui/app/resourcebrowser/util/ResourceScreenView;

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreview:Lmiui/app/resourcebrowser/util/ResourceScreenView;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/util/ResourceScreenView;->getScreenCount()I

    move-result v0

    const/4 v2, 0x1

    if-le v0, v2, :cond_2b

    const/4 v0, 0x0

    :goto_1c
    invoke-virtual {v1, v0}, Lmiui/app/resourcebrowser/util/ResourceScreenView;->setSeekBarVisibility(I)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreview:Lmiui/app/resourcebrowser/util/ResourceScreenView;

    iget v1, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreviewOffset:I

    invoke-virtual {v0, v1}, Lmiui/app/resourcebrowser/util/ResourceScreenView;->setCurrentScreen(I)V

    return-void

    :cond_27
    invoke-direct {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->bindScreenImageView()V

    goto :goto_10

    :cond_2b
    const/16 v0, 0x8

    goto :goto_1c
.end method

.method protected buildDefaultMetaData(Landroid/os/Bundle;Ljava/lang/String;)Landroid/os/Bundle;
    .registers 4
    .parameter "metaData"
    .parameter "action"

    .prologue
    invoke-static {p1, p2, p0}, Lmiui/app/resourcebrowser/util/ResourceHelper;->buildDefaultMetaData(Landroid/os/Bundle;Ljava/lang/String;Landroid/content/Context;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method protected changeCurrentResource()V
    .registers 2

    .prologue
    iget v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceIndex:I

    invoke-virtual {p0, v0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->requestResourceDetail(I)V

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->setResourceInfo()V

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->setResourceStatus()V

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->bindScreenView()V

    return-void
.end method

.method protected delete()V
    .registers 5

    .prologue
    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mLocalPath:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3d

    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mLocalPath:Ljava/lang/String;

    const/16 v2, 0x2f

    const/16 v3, 0x5f

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    .local v0, endcodedPath:Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lmiui/app/resourcebrowser/ResourceConstants;->PREVIEW_PATH:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lmiui/os/Shell;->remove(Ljava/lang/String;)Z

    iget-boolean v1, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mIsOnlineResourceSet:Z

    if-eqz v1, :cond_3e

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->getCurrentResource()Lmiui/app/resourcebrowser/resource/Resource;

    move-result-object v1

    invoke-virtual {v1}, Lmiui/app/resourcebrowser/resource/Resource;->getInformation()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "STATUS"

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->setResourceStatus()V

    .end local v0           #endcodedPath:Ljava/lang/String;
    :cond_3d
    :goto_3d
    return-void

    .restart local v0       #endcodedPath:Ljava/lang/String;
    :cond_3e
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->finish()V

    goto :goto_3d
.end method

.method protected download()V
    .registers 1

    .prologue
    return-void
.end method

.method protected downloadFailed(Ljava/lang/String;)V
    .registers 2
    .parameter "downloadFilePath"

    .prologue
    return-void
.end method

.method protected downloadSuccess(Ljava/lang/String;)V
    .registers 5
    .parameter "downloadFilePath"

    .prologue
    const/4 v2, 0x0

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceSet:Lmiui/app/resourcebrowser/resource/ResourceSet;

    iget v1, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceGroup:I

    invoke-virtual {v0, v1}, Lmiui/app/resourcebrowser/resource/ResourceSet;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/widget/DataGroup;

    invoke-static {p1, v0, v2}, Lmiui/app/resourcebrowser/util/ResourceHelper;->setResourceStatus(Ljava/lang/String;Lmiui/widget/DataGroup;I)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mOperationHandler:Lmiui/app/resourcebrowser/view/ResourceOperationHandler;

    invoke-virtual {v0, p1}, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->isCurrentResourceDownloadSavePath(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_21

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mOperationHandler:Lmiui/app/resourcebrowser/view/ResourceOperationHandler;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->getResourceState()Lmiui/app/resourcebrowser/view/ResourceState;

    move-result-object v0

    iput-boolean v2, v0, Lmiui/app/resourcebrowser/view/ResourceState;->hasUpdate:Z

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->setResourceInfo()V

    :cond_21
    return-void
.end method

.method protected getConfirmedDownloadUrl(Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "originDowloadUrl"

    .prologue
    return-object p1
.end method

.method protected getCurrentResource()Lmiui/app/resourcebrowser/resource/Resource;
    .registers 2

    .prologue
    iget v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceIndex:I

    invoke-virtual {p0, v0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->getCurrentResource(I)Lmiui/app/resourcebrowser/resource/Resource;

    move-result-object v0

    return-object v0
.end method

.method protected getCurrentResource(I)Lmiui/app/resourcebrowser/resource/Resource;
    .registers 5
    .parameter "resIndex"

    .prologue
    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceSet:Lmiui/app/resourcebrowser/resource/ResourceSet;

    iget v2, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceGroup:I

    invoke-virtual {v1, v2}, Lmiui/app/resourcebrowser/resource/ResourceSet;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .local v0, list:Ljava/util/List;,"Ljava/util/List<Lmiui/app/resourcebrowser/resource/Resource;>;"
    if-ltz p1, :cond_19

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ge p1, v1, :cond_19

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/app/resourcebrowser/resource/Resource;

    :goto_18
    return-object v1

    :cond_19
    const/4 v1, 0x0

    goto :goto_18
.end method

.method protected getCurrentResourceInformation()Landroid/os/Bundle;
    .registers 3

    .prologue
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->getCurrentResource()Lmiui/app/resourcebrowser/resource/Resource;

    move-result-object v0

    .local v0, resource:Lmiui/app/resourcebrowser/resource/Resource;
    if-nez v0, :cond_8

    const/4 v1, 0x0

    :goto_7
    return-object v1

    :cond_8
    invoke-virtual {v0}, Lmiui/app/resourcebrowser/resource/Resource;->getInformation()Landroid/os/Bundle;

    move-result-object v1

    goto :goto_7
.end method

.method protected getFormatPlayingRingtoneName(Ljava/lang/String;II)Ljava/lang/String;
    .registers 5
    .parameter "ringtonePath"
    .parameter "current"
    .parameter "total"

    .prologue
    invoke-static {p1, p2, p3}, Lmiui/app/resourcebrowser/util/ResourceHelper;->getDefaultFormatPlayingRingtoneName(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getFormatTitleBeforePlayingRingtone()Ljava/lang/String;
    .registers 2

    .prologue
    const-string v0, ""

    return-object v0
.end method

.method protected getMusicPlayList(Lmiui/app/resourcebrowser/resource/Resource;)Ljava/util/List;
    .registers 3
    .parameter "resourceItem"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiui/app/resourcebrowser/resource/Resource;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    invoke-static {p0, p1}, Lmiui/app/resourcebrowser/util/ResourceHelper;->getDefaultMusicPlayList(Landroid/content/Context;Lmiui/app/resourcebrowser/resource/Resource;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected getOperationHandler(Lmiui/app/resourcebrowser/view/ResourceOperationView;)Lmiui/app/resourcebrowser/view/ResourceOperationHandler;
    .registers 4
    .parameter "v"

    .prologue
    new-instance v0, Lmiui/app/resourcebrowser/ResourceDetailActivity$11;

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->getRealResourceState()Lmiui/app/resourcebrowser/view/ResourceState;

    move-result-object v1

    invoke-direct {v0, p0, p0, p1, v1}, Lmiui/app/resourcebrowser/ResourceDetailActivity$11;-><init>(Lmiui/app/resourcebrowser/ResourceDetailActivity;Landroid/app/Activity;Lmiui/app/resourcebrowser/view/ResourceOperationView;Lmiui/app/resourcebrowser/view/ResourceState;)V

    return-object v0
.end method

.method protected getOperatorViewUIParameter()Lmiui/app/resourcebrowser/view/ResourceOperationView$UIParameter;
    .registers 2

    .prologue
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getRealResourceState()Lmiui/app/resourcebrowser/view/ResourceState;
    .registers 7

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    new-instance v0, Lmiui/app/resourcebrowser/view/ResourceState;

    invoke-direct {v0}, Lmiui/app/resourcebrowser/view/ResourceState;-><init>()V

    .local v0, ret:Lmiui/app/resourcebrowser/view/ResourceState;
    iget-boolean v2, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mIsOnlineResourceSet:Z

    if-nez v2, :cond_50

    move v2, v3

    :goto_c
    iput-boolean v2, v0, Lmiui/app/resourcebrowser/view/ResourceState;->inLocalPage:Z

    iget-object v2, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mLocalPath:Ljava/lang/String;

    iput-object v2, v0, Lmiui/app/resourcebrowser/view/ResourceState;->localPath:Ljava/lang/String;

    iget-object v2, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mOnlinePath:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->getConfirmedDownloadUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lmiui/app/resourcebrowser/view/ResourceState;->downloadUrl:Ljava/lang/String;

    iget-object v2, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mLocalPath:Ljava/lang/String;

    iput-object v2, v0, Lmiui/app/resourcebrowser/view/ResourceState;->downloadSavePath:Ljava/lang/String;

    iget-object v2, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mTitle:Ljava/lang/String;

    iput-object v2, v0, Lmiui/app/resourcebrowser/view/ResourceState;->title:Ljava/lang/String;

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->getCurrentResource()Lmiui/app/resourcebrowser/resource/Resource;

    move-result-object v2

    invoke-virtual {v2}, Lmiui/app/resourcebrowser/resource/Resource;->getPlatformVersion()I

    move-result v2

    iput v2, v0, Lmiui/app/resourcebrowser/view/ResourceState;->uiVersion:I

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->getCurrentResource()Lmiui/app/resourcebrowser/resource/Resource;

    move-result-object v2

    invoke-virtual {v2}, Lmiui/app/resourcebrowser/resource/Resource;->getStatus()I

    move-result v1

    .local v1, status:I
    if-ne v1, v3, :cond_52

    move v2, v3

    :goto_37
    iput-boolean v2, v0, Lmiui/app/resourcebrowser/view/ResourceState;->hasUpdate:Z

    iget-object v2, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mLocalPath:Ljava/lang/String;

    const-string v5, "/system"

    invoke-virtual {v2, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_54

    :goto_43
    iput-boolean v3, v0, Lmiui/app/resourcebrowser/view/ResourceState;->allowDelete:Z

    iget-object v2, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mMetaData:Landroid/os/Bundle;

    const-string v3, "miui.app.resourcebrowser.USING_PICKER"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, v0, Lmiui/app/resourcebrowser/view/ResourceState;->isPicker:Z

    return-object v0

    .end local v1           #status:I
    :cond_50
    move v2, v4

    goto :goto_c

    .restart local v1       #status:I
    :cond_52
    move v2, v4

    goto :goto_37

    :cond_54
    move v3, v4

    goto :goto_43
.end method

.method protected initNavigationState()V
    .registers 3

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreviousItem:Landroid/view/View;

    new-instance v1, Lmiui/app/resourcebrowser/ResourceDetailActivity$2;

    invoke-direct {v1, p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity$2;-><init>(Lmiui/app/resourcebrowser/ResourceDetailActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mNextItem:Landroid/view/View;

    new-instance v1, Lmiui/app/resourcebrowser/ResourceDetailActivity$3;

    invoke-direct {v1, p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity$3;-><init>(Lmiui/app/resourcebrowser/ResourceDetailActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->updateNavigationState()V

    return-void
.end method

.method protected navigateToNextResource()V
    .registers 5

    .prologue
    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceSet:Lmiui/app/resourcebrowser/resource/ResourceSet;

    iget v2, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceGroup:I

    invoke-virtual {v1, v2}, Lmiui/app/resourcebrowser/resource/ResourceSet;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/widget/DataGroup;

    invoke-virtual {v1}, Lmiui/widget/DataGroup;->size()I

    move-result v0

    .local v0, resourceTotal:I
    iget v1, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceIndex:I

    add-int/lit8 v2, v0, -0x1

    if-ge v1, v2, :cond_29

    iget v1, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceIndex:I

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->changeCurrentResource()V

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->updateNavigationState()V

    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mMetaData:Landroid/os/Bundle;

    const-string v2, "miui.app.resourcebrowser.RESOURCE_INDEX"

    iget v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceIndex:I

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_29
    return-void
.end method

.method protected navigateToPreviousResource()V
    .registers 4

    .prologue
    iget v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceIndex:I

    if-lez v0, :cond_19

    iget v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceIndex:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceIndex:I

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->changeCurrentResource()V

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->updateNavigationState()V

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mMetaData:Landroid/os/Bundle;

    const-string v1, "miui.app.resourcebrowser.RESOURCE_INDEX"

    iget v2, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceIndex:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_19
    return-void
.end method

.method protected needRealRequestDetailInfo(Ljava/io/File;)Z
    .registers 3
    .parameter "cacheFile"

    .prologue
    invoke-static {p1}, Lmiui/app/resourcebrowser/util/ResourceHelper;->isCacheValid(Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public onBackPressed()V
    .registers 2

    .prologue
    iget-boolean v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mFullScreen:Z

    if-eqz v0, :cond_8

    invoke-direct {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->enterNormalMode()V

    :goto_7
    return-void

    :cond_8
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_7
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 11
    .parameter "savedInstanceState"

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/16 v6, 0x9

    invoke-virtual {p0, v6}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->requestWindowFeature(I)Z

    const/16 v6, 0xa

    invoke-virtual {p0, v6}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->requestWindowFeature(I)Z

    const v6, 0x603004e

    invoke-virtual {p0, v6}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->setContentView(I)V

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .local v1, intent:Landroid/content/Intent;
    if-eqz v1, :cond_23

    const-string v6, "META_DATA"

    invoke-virtual {v1, v6}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v6

    iput-object v6, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mMetaData:Landroid/os/Bundle;

    :cond_23
    iget-object v6, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mMetaData:Landroid/os/Bundle;

    if-nez v6, :cond_3d

    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v6, v7}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->buildDefaultMetaData(Landroid/os/Bundle;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v6

    iput-object v6, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mMetaData:Landroid/os/Bundle;

    const-string v6, "META_DATA"

    iget-object v7, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mMetaData:Landroid/os/Bundle;

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    :cond_3d
    iget-object v6, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mMetaData:Landroid/os/Bundle;

    invoke-virtual {p0, v6}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->pickMetaData(Landroid/os/Bundle;)V

    const-string v6, "android.intent.action.VIEW"

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7a

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->responseToViewAction()Lmiui/app/resourcebrowser/resource/Resource;

    move-result-object v2

    .local v2, resource:Lmiui/app/resourcebrowser/resource/Resource;
    if-nez v2, :cond_58

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->finish()V

    .end local v2           #resource:Lmiui/app/resourcebrowser/resource/Resource;
    :goto_57
    return-void

    .restart local v2       #resource:Lmiui/app/resourcebrowser/resource/Resource;
    :cond_58
    iget-object v6, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceSet:Lmiui/app/resourcebrowser/resource/ResourceSet;

    invoke-virtual {v6}, Lmiui/app/resourcebrowser/resource/ResourceSet;->clear()V

    new-instance v0, Lmiui/widget/DataGroup;

    invoke-direct {v0}, Lmiui/widget/DataGroup;-><init>()V

    .local v0, group:Lmiui/widget/DataGroup;,"Lmiui/widget/DataGroup<Lmiui/app/resourcebrowser/resource/Resource;>;"
    invoke-virtual {v0, v2}, Lmiui/widget/DataGroup;->add(Ljava/lang/Object;)Z

    iget-object v6, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceSet:Lmiui/app/resourcebrowser/resource/ResourceSet;

    invoke-virtual {v6, v0}, Lmiui/app/resourcebrowser/resource/ResourceSet;->add(Ljava/lang/Object;)Z

    iput v4, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceGroup:I

    iput v4, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceIndex:I

    .end local v0           #group:Lmiui/widget/DataGroup;,"Lmiui/widget/DataGroup<Lmiui/app/resourcebrowser/resource/Resource;>;"
    .end local v2           #resource:Lmiui/app/resourcebrowser/resource/Resource;
    :goto_6e
    iget-object v6, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceSet:Lmiui/app/resourcebrowser/resource/ResourceSet;

    invoke-virtual {v6}, Lmiui/app/resourcebrowser/resource/ResourceSet;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_8f

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->finish()V

    goto :goto_57

    :cond_7a
    iget-object v6, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mMetaData:Landroid/os/Bundle;

    const-string v7, "miui.app.resourcebrowser.RESOURCE_GROUP"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    iput v6, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceGroup:I

    iget-object v6, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mMetaData:Landroid/os/Bundle;

    const-string v7, "miui.app.resourcebrowser.RESOURCE_INDEX"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    iput v6, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceIndex:I

    goto :goto_6e

    :cond_8f
    iget-object v6, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mMetaData:Landroid/os/Bundle;

    const-string v7, "android.intent.extra.ringtone.TYPE"

    const/4 v8, -0x1

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    .local v3, ringtoneType:I
    if-ltz v3, :cond_9d

    invoke-static {p0, v3}, Lmiui/app/resourcebrowser/util/ResourceHelper;->setMusicVolumeType(Landroid/app/Activity;I)V

    :cond_9d
    invoke-static {p0}, Lmiui/app/SDCardMonitor;->getSDCardMonitor(Landroid/content/Context;)Lmiui/app/SDCardMonitor;

    move-result-object v6

    iput-object v6, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mSDCardMonitor:Lmiui/app/SDCardMonitor;

    iget-object v6, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mSDCardMonitor:Lmiui/app/SDCardMonitor;

    invoke-virtual {v6, p0}, Lmiui/app/SDCardMonitor;->addListener(Lmiui/app/SDCardMonitor$SDCardStatusListener;)V

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mMetaData:Landroid/os/Bundle;

    const-string v8, "miui.app.resourcebrowser.CACHE_LIST_FOLDER"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lmiui/os/ExtraFileUtils;->standardizeFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceSetCode:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lmiui/os/ExtraFileUtils;->standardizeFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mListFolder:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lmiui/app/resourcebrowser/ResourceConstants;->DETAIL_PATH:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceSetCode:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lmiui/os/ExtraFileUtils;->standardizeFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mDetailFolder:Ljava/lang/String;

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v6

    if-eqz v6, :cond_ef

    move v4, v5

    :cond_ef
    iput-boolean v4, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mHasActionBar:Z

    iget-boolean v4, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mHasActionBar:Z

    if-eqz v4, :cond_fc

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    :cond_fc
    if-eqz p1, :cond_106

    const-string v4, "PREVIEW_INDEX"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreviewOffset:I

    :cond_106
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->setupUI()V

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->setupNavigationButton()V

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->changeCurrentResource()V

    goto/16 :goto_57
.end method

.method protected onDestroy()V
    .registers 3

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mSDCardMonitor:Lmiui/app/SDCardMonitor;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mSDCardMonitor:Lmiui/app/SDCardMonitor;

    invoke-virtual {v0, p0}, Lmiui/app/SDCardMonitor;->removeListener(Lmiui/app/SDCardMonitor$SDCardStatusListener;)V

    :cond_9
    invoke-direct {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->stopMusic()V

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mImageCacheDecoder:Lmiui/app/resourcebrowser/util/ImageCacheDecoder;

    if-eqz v0, :cond_16

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mImageCacheDecoder:Lmiui/app/resourcebrowser/util/ImageCacheDecoder;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->clean(Z)V

    :cond_16
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mOperationHandler:Lmiui/app/resourcebrowser/view/ResourceOperationHandler;

    if-eqz v0, :cond_1f

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mOperationHandler:Lmiui/app/resourcebrowser/view/ResourceOperationHandler;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->onActivityOnDestroyEvent()V

    :cond_1f
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 4
    .parameter "item"

    .prologue
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_c

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->finish()V

    :cond_c
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method protected onPause()V
    .registers 2

    .prologue
    invoke-direct {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->stopMusic()V

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mOperationHandler:Lmiui/app/resourcebrowser/view/ResourceOperationHandler;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mOperationHandler:Lmiui/app/resourcebrowser/view/ResourceOperationHandler;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->onActivityOnPauseEvent()V

    :cond_c
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreview:Lmiui/app/resourcebrowser/util/ResourceScreenView;

    if-eqz v0, :cond_15

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreview:Lmiui/app/resourcebrowser/util/ResourceScreenView;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/util/ResourceScreenView;->onPause()V

    :cond_15
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter "outState"

    .prologue
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreview:Lmiui/app/resourcebrowser/util/ResourceScreenView;

    if-eqz v0, :cond_12

    const-string v0, "PREVIEW_INDEX"

    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreview:Lmiui/app/resourcebrowser/util/ResourceScreenView;

    invoke-virtual {v1}, Lmiui/app/resourcebrowser/util/ResourceScreenView;->getCurrentScreenIndex()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_12
    return-void
.end method

.method public onStatusChanged(Z)V
    .registers 2
    .parameter "mount"

    .prologue
    invoke-static {p0}, Lmiui/app/resourcebrowser/util/ResourceHelper;->exit(Landroid/app/Activity;)V

    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .registers 5
    .parameter "hasFocus"

    .prologue
    const/4 v2, 0x0

    if-eqz p1, :cond_1f

    iget-boolean v1, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mHasActionBar:Z

    if-eqz v1, :cond_1f

    iget-boolean v1, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mFullScreen:Z

    if-nez v1, :cond_1f

    const v1, 0x60b007e

    invoke-virtual {p0, v1}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .local v0, childRoot:Landroid/widget/LinearLayout;
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ActionBar;->getHeight()I

    move-result v1

    invoke-virtual {v0, v2, v1, v2, v2}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .end local v0           #childRoot:Landroid/widget/LinearLayout;
    :cond_1f
    invoke-super {p0, p1}, Landroid/app/Activity;->onWindowFocusChanged(Z)V

    return-void
.end method

.method protected pick()V
    .registers 5

    .prologue
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .local v0, resultIntent:Landroid/content/Intent;
    const-string v1, "miui.app.resourcebrowser.PICKED_RESOURCE"

    iget-object v2, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mLocalPath:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "miui.app.resourcebrowser.TRACK_ID"

    iget-object v2, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mMetaData:Landroid/os/Bundle;

    const-string v3, "miui.app.resourcebrowser.TRACK_ID"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget v1, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceSetCategory:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_29

    const-string v1, "android.intent.extra.ringtone.PICKED_URI"

    iget-object v2, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mLocalPath:Ljava/lang/String;

    invoke-static {v2}, Lmiui/app/resourcebrowser/util/ResourceHelper;->getUriByPath(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_29
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->finish()V

    return-void
.end method

.method protected pickMetaData(Landroid/os/Bundle;)V
    .registers 4
    .parameter "metaData"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mMetaData:Landroid/os/Bundle;

    const-string v1, "miui.app.resourcebrowser.RESOURCE_SET_PACKAGE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceSetPackage:Ljava/lang/String;

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mMetaData:Landroid/os/Bundle;

    const-string v1, "miui.app.resourcebrowser.RESOURCE_SET_SUBPACKAGE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceSetSubpackage:Ljava/lang/String;

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mMetaData:Landroid/os/Bundle;

    const-string v1, "miui.app.resourcebrowser.RESOURCE_SET_CODE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceSetCode:Ljava/lang/String;

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mMetaData:Landroid/os/Bundle;

    const-string v1, "miui.app.resourcebrowser.RESOURCE_SET_CATEGORY"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceSetCategory:I

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceSetSubpackage:Ljava/lang/String;

    if-nez v0, :cond_30

    const-string v0, ".single"

    iput-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceSetSubpackage:Ljava/lang/String;

    :cond_30
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceSetPackage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceSetSubpackage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lmiui/app/resourcebrowser/resource/ResourceSet;->getInstance(Ljava/lang/String;)Lmiui/app/resourcebrowser/resource/ResourceSet;

    move-result-object v0

    iput-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceSet:Lmiui/app/resourcebrowser/resource/ResourceSet;

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mMetaData:Landroid/os/Bundle;

    const-string v1, "miui.app.resourcebrowser.LIST_URL"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mUrl:Ljava/lang/String;

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mUrl:Ljava/lang/String;

    if-eqz v0, :cond_5d

    const/4 v0, 0x1

    :goto_5a
    iput-boolean v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mIsOnlineResourceSet:Z

    return-void

    :cond_5d
    const/4 v0, 0x0

    goto :goto_5a
.end method

.method protected requestResourceDetail(I)V
    .registers 12
    .parameter "resIndex"

    .prologue
    invoke-virtual {p0, p1}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->getCurrentResource(I)Lmiui/app/resourcebrowser/resource/Resource;

    move-result-object v4

    .local v4, res:Lmiui/app/resourcebrowser/resource/Resource;
    if-eqz v4, :cond_15

    invoke-virtual {v4}, Lmiui/app/resourcebrowser/resource/Resource;->getId()Ljava/lang/String;

    move-result-object v2

    .local v2, id:Ljava/lang/String;
    :goto_a
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_14

    iget-boolean v8, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mIsOnlineResourceSet:Z

    if-nez v8, :cond_17

    :cond_14
    :goto_14
    return-void

    .end local v2           #id:Ljava/lang/String;
    :cond_15
    const/4 v2, 0x0

    goto :goto_a

    .restart local v2       #id:Ljava/lang/String;
    :cond_17
    iget-object v8, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mService:Lmiui/app/resourcebrowser/service/online/OnlineService;

    invoke-virtual {v8, v2}, Lmiui/app/resourcebrowser/service/online/OnlineService;->getDetailUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .local v7, url:Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mDetailFolder:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .local v3, path:Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .local v1, file:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_4c

    iget-object v8, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mParser:Lmiui/app/resourcebrowser/service/ResourceDataParser;

    iget-object v9, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mMetaData:Landroid/os/Bundle;

    invoke-virtual {v8, v3, v9}, Lmiui/app/resourcebrowser/service/ResourceDataParser;->readResource(Ljava/lang/String;Landroid/os/Bundle;)Lmiui/app/resourcebrowser/resource/Resource;

    move-result-object v5

    .local v5, resource:Lmiui/app/resourcebrowser/resource/Resource;
    if-eqz v5, :cond_4c

    invoke-virtual {v5}, Lmiui/app/resourcebrowser/resource/Resource;->getInformation()Landroid/os/Bundle;

    move-result-object v8

    invoke-virtual {v4, v8}, Lmiui/app/resourcebrowser/resource/Resource;->setInformation(Landroid/os/Bundle;)V

    .end local v5           #resource:Lmiui/app/resourcebrowser/resource/Resource;
    :cond_4c
    invoke-virtual {p0, v1}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->needRealRequestDetailInfo(Ljava/io/File;)Z

    move-result v8

    if-eqz v8, :cond_14

    new-instance v6, Lmiui/app/resourcebrowser/ResourceDetailActivity$DownloadDetailTask;

    invoke-direct {v6, p0, p1}, Lmiui/app/resourcebrowser/ResourceDetailActivity$DownloadDetailTask;-><init>(Lmiui/app/resourcebrowser/ResourceDetailActivity;I)V

    .local v6, task:Lmiui/app/resourcebrowser/ResourceDetailActivity$DownloadDetailTask;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "detail_"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lmiui/app/resourcebrowser/ResourceDetailActivity$DownloadDetailTask;->setId(Ljava/lang/String;)V

    iget-object v8, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mDownloadSet:Ljava/util/Set;

    invoke-interface {v8, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_14

    iget-object v8, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mDownloadSet:Ljava/util/Set;

    invoke-interface {v8, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v0, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;

    invoke-direct {v0}, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;-><init>()V

    .local v0, entry:Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;
    invoke-virtual {v0, v7}, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->setUrl(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->setPath(Ljava/lang/String;)V

    const/4 v8, 0x1

    new-array v8, v8, [Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;

    const/4 v9, 0x0

    aput-object v0, v8, v9

    invoke-virtual {v6, v8}, Lmiui/app/resourcebrowser/ResourceDetailActivity$DownloadDetailTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_14
.end method

.method protected responseToViewAction()Lmiui/app/resourcebrowser/resource/Resource;
    .registers 5

    .prologue
    const/4 v0, 0x0

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mLocalPath:Ljava/lang/String;

    iget-object v2, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mLocalPath:Ljava/lang/String;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p0, v2, v0, v3}, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->createZipResourceInfo(Landroid/content/Context;Ljava/lang/String;Lmiui/app/resourcebrowser/service/local/ZipResourceCache;[Ljava/lang/Object;)Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;

    move-result-object v1

    .local v1, zipResourceInfo:Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;
    if-nez v1, :cond_1b

    :goto_1a
    return-object v0

    :cond_1b
    new-instance v0, Lmiui/app/resourcebrowser/resource/Resource;

    invoke-direct {v0}, Lmiui/app/resourcebrowser/resource/Resource;-><init>()V

    .local v0, resource:Lmiui/app/resourcebrowser/resource/Resource;
    invoke-virtual {v1}, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->getInformation()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v2}, Lmiui/app/resourcebrowser/resource/Resource;->setInformation(Landroid/os/Bundle;)V

    goto :goto_1a
.end method

.method protected setResourceInfo()V
    .registers 21

    .prologue
    invoke-virtual/range {p0 .. p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->getCurrentResourceInformation()Landroid/os/Bundle;

    move-result-object v4

    .local v4, bundle:Landroid/os/Bundle;
    if-nez v4, :cond_7

    :cond_6
    :goto_6
    return-void

    :cond_7
    const-string v18, "ONLINE_PATH"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mOnlinePath:Ljava/lang/String;

    const-string v18, "LOCAL_PATH"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mLocalPath:Ljava/lang/String;

    const-string v18, "NAME"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mTitle:Ljava/lang/String;

    sget-boolean v18, Lmiui/app/resourcebrowser/util/ResourceDebug;->DEBUG:Z

    if-eqz v18, :cond_5e

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget v0, v0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceIndex:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "-"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mTitle:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mTitle:Ljava/lang/String;

    :cond_5e
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mHasActionBar:Z

    move/from16 v18, v0

    if-eqz v18, :cond_73

    invoke-virtual/range {p0 .. p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mTitle:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    :cond_73
    move-object/from16 v0, p0

    iget v0, v0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceSetCategory:I

    move/from16 v18, v0

    if-nez v18, :cond_6

    const-string v18, "AUTHOR"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .local v2, author:Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_94

    const v18, 0x60c000c

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    :cond_94
    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mInfo:Landroid/widget/FrameLayout;

    move-object/from16 v18, v0

    const v19, 0x60b0053

    invoke-virtual/range {v18 .. v19}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .local v3, authorView:Landroid/widget/TextView;
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v18, "DESIGNER"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .local v5, designer:Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_bf

    const v18, 0x60c000c

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    :cond_bf
    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mInfo:Landroid/widget/FrameLayout;

    move-object/from16 v18, v0

    const v19, 0x60b0054

    invoke-virtual/range {v18 .. v19}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .local v6, designerView:Landroid/widget/TextView;
    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v18, "MODIFIED_TIME"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v13

    .local v13, time:J
    const-string v9, ""

    .local v9, modifiedTime:Ljava/lang/String;
    const-wide/16 v18, 0x0

    cmp-long v18, v13, v18

    if-eqz v18, :cond_ed

    invoke-static {}, Ljava/text/DateFormat;->getDateInstance()Ljava/text/DateFormat;

    move-result-object v18

    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    :cond_ed
    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mInfo:Landroid/widget/FrameLayout;

    move-object/from16 v18, v0

    const v19, 0x60b0055

    invoke-virtual/range {v18 .. v19}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .local v10, modifiedTimeView:Landroid/widget/TextView;
    invoke-virtual {v10, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mIsOnlineResourceSet:Z

    move/from16 v18, v0

    if-nez v18, :cond_11e

    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mInfo:Landroid/widget/FrameLayout;

    move-object/from16 v18, v0

    const v19, 0x60b006f

    invoke-virtual/range {v18 .. v19}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/TextView;

    .local v15, timeTitle:Landroid/widget/TextView;
    const v18, 0x60c01f9

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Landroid/widget/TextView;->setText(I)V

    .end local v15           #timeTitle:Landroid/widget/TextView;
    :cond_11e
    const-string v18, "VERSION"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .local v16, version:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mInfo:Landroid/widget/FrameLayout;

    move-object/from16 v18, v0

    const v19, 0x60b0056

    invoke-virtual/range {v18 .. v19}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    .local v17, versionView:Landroid/widget/TextView;
    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v18, "SIZE"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .local v11, size:Ljava/lang/String;
    :try_start_144
    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v18

    move/from16 v0, v18

    int-to-long v0, v0

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Lmiui/app/resourcebrowser/util/ResourceHelper;->getFormattedSize(J)Ljava/lang/String;
    :try_end_150
    .catch Ljava/lang/Exception; {:try_start_144 .. :try_end_150} :catch_17f

    move-result-object v11

    :goto_151
    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mInfo:Landroid/widget/FrameLayout;

    move-object/from16 v18, v0

    const v19, 0x60b0065

    invoke-virtual/range {v18 .. v19}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    .local v12, sizeView:Landroid/widget/TextView;
    invoke-virtual {v12, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v18, "DOWNLOAD_COUNT"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .local v7, downloadTimes:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mInfo:Landroid/widget/FrameLayout;

    move-object/from16 v18, v0

    const v19, 0x60b0064

    invoke-virtual/range {v18 .. v19}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .local v8, downloadTimesView:Landroid/widget/TextView;
    invoke-virtual {v8, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_6

    .end local v7           #downloadTimes:Ljava/lang/String;
    .end local v8           #downloadTimesView:Landroid/widget/TextView;
    .end local v12           #sizeView:Landroid/widget/TextView;
    :catch_17f
    move-exception v18

    goto :goto_151
.end method

.method protected setResourceStatus()V
    .registers 4

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mOperationHandler:Lmiui/app/resourcebrowser/view/ResourceOperationHandler;

    if-nez v0, :cond_22

    const v0, 0x60b0059

    invoke-virtual {p0, v0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiui/app/resourcebrowser/view/ResourceOperationView;

    iput-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mOperationView:Lmiui/app/resourcebrowser/view/ResourceOperationView;

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mOperationView:Lmiui/app/resourcebrowser/view/ResourceOperationView;

    invoke-virtual {p0, v0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->getOperationHandler(Lmiui/app/resourcebrowser/view/ResourceOperationView;)Lmiui/app/resourcebrowser/view/ResourceOperationHandler;

    move-result-object v0

    iput-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mOperationHandler:Lmiui/app/resourcebrowser/view/ResourceOperationHandler;

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mOperationView:Lmiui/app/resourcebrowser/view/ResourceOperationView;

    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mOperationHandler:Lmiui/app/resourcebrowser/view/ResourceOperationHandler;

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->getOperatorViewUIParameter()Lmiui/app/resourcebrowser/view/ResourceOperationView$UIParameter;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmiui/app/resourcebrowser/view/ResourceOperationView;->init(Lmiui/app/resourcebrowser/view/ResourceOperationHandler;Lmiui/app/resourcebrowser/view/ResourceOperationView$UIParameter;)V

    :cond_22
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mOperationHandler:Lmiui/app/resourcebrowser/view/ResourceOperationHandler;

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->getRealResourceState()Lmiui/app/resourcebrowser/view/ResourceState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->refreshUI(Lmiui/app/resourcebrowser/view/ResourceState;)V

    return-void
.end method

.method protected setScreenViewBackground(I)V
    .registers 3
    .parameter "resId"

    .prologue
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mScreenViewNeedBackgroud:Z

    iput p1, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mScreenViewBackgroudId:I

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->bindScreenView()V

    return-void
.end method

.method protected setupNavigationButton()V
    .registers 9

    .prologue
    const/4 v7, 0x1

    const/4 v5, 0x0

    const/4 v6, -0x2

    iget-boolean v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mHasActionBar:Z

    if-nez v3, :cond_8

    :goto_7
    return-void

    :cond_8
    new-instance v1, Landroid/widget/LinearLayout;

    invoke-direct {v1, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .local v1, custom:Landroid/widget/LinearLayout;
    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setOrientation(I)V

    const/high16 v3, 0x4120

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    invoke-static {v7, v3, v4}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    float-to-int v2, v3

    .local v2, rightPadding:I
    invoke-virtual {v1, v5, v5, v2, v5}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .local v0, child:Landroid/widget/ImageView;
    const v3, 0x60200c8

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v3, v6, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v0, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iput-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreviousItem:Landroid/view/View;

    new-instance v0, Landroid/widget/ImageView;

    .end local v0           #child:Landroid/widget/ImageView;
    invoke-direct {v0, p0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .restart local v0       #child:Landroid/widget/ImageView;
    const v3, 0x60200cc

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v3, v6, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v0, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iput-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mNextItem:Landroid/view/View;

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    new-instance v4, Landroid/app/ActionBar$LayoutParams;

    const/16 v5, 0x15

    invoke-direct {v4, v6, v6, v5}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    invoke-virtual {v3, v1, v4}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->initNavigationState()V

    goto :goto_7
.end method

.method protected setupUI()V
    .registers 9

    .prologue
    const/4 v7, 0x6

    const/4 v5, -0x2

    const/4 v6, 0x0

    const-string v3, "layout_inflater"

    invoke-virtual {p0, v3}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    iput-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mInflater:Landroid/view/LayoutInflater;

    const v3, 0x60b007f

    invoke-virtual {p0, v3}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mCoverView:Landroid/widget/ImageView;

    invoke-direct {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->buildModeChangeAnimator()V

    const v3, 0x60b0052

    invoke-virtual {p0, v3}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lmiui/app/resourcebrowser/util/ResourceScreenView;

    iput-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreview:Lmiui/app/resourcebrowser/util/ResourceScreenView;

    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreview:Lmiui/app/resourcebrowser/util/ResourceScreenView;

    const v4, 0x3e4ccccd

    invoke-virtual {v3, v4}, Lmiui/app/resourcebrowser/util/ResourceScreenView;->setOverScrollRatio(F)V

    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreview:Lmiui/app/resourcebrowser/util/ResourceScreenView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lmiui/app/resourcebrowser/util/ResourceScreenView;->setOvershootTension(F)V

    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreview:Lmiui/app/resourcebrowser/util/ResourceScreenView;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lmiui/app/resourcebrowser/util/ResourceScreenView;->setScreenAlignment(I)V

    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreview:Lmiui/app/resourcebrowser/util/ResourceScreenView;

    new-instance v4, Lmiui/app/resourcebrowser/ResourceDetailActivity$1;

    invoke-direct {v4, p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity$1;-><init>(Lmiui/app/resourcebrowser/ResourceDetailActivity;)V

    invoke-virtual {v3, v4}, Lmiui/app/resourcebrowser/util/ResourceScreenView;->setScreenChangeListener(Lmiui/app/resourcebrowser/util/ResourceScreenView$ScreenChangeListener;)V

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v3, 0x51

    invoke-direct {v2, v5, v5, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .local v2, params:Landroid/widget/FrameLayout$LayoutParams;
    const/16 v3, 0x1e

    invoke-virtual {v2, v6, v6, v6, v3}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreview:Lmiui/app/resourcebrowser/util/ResourceScreenView;

    invoke-virtual {v3, v2}, Lmiui/app/resourcebrowser/util/ResourceScreenView;->setSeekBarPosition(Landroid/widget/FrameLayout$LayoutParams;)V

    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .local v1, p:Landroid/graphics/Point;
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    iget v3, v1, Landroid/graphics/Point;->x:I

    invoke-static {v3}, Lmiui/app/resourcebrowser/util/ResourceHelper;->getPreviewWidth(I)I

    move-result v3

    iput v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreviewWidth:I

    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mMetaData:Landroid/os/Bundle;

    const-string v4, "miui.app.resourcebrowser.PREVIEW_WIDTH"

    iget v5, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreviewWidth:I

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    iget v4, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreviewWidth:I

    const/4 v5, -0x1

    invoke-direct {v3, v4, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    iput-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mNormalParams:Landroid/view/ViewGroup$LayoutParams;

    iget v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceSetCategory:I

    if-nez v3, :cond_aa

    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mInflater:Landroid/view/LayoutInflater;

    const v4, 0x603001b

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .local v0, infoView:Landroid/view/View;
    new-instance v3, Landroid/widget/FrameLayout;

    invoke-direct {v3, p0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mInfo:Landroid/widget/FrameLayout;

    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mInfo:Landroid/widget/FrameLayout;

    const/16 v4, 0x3c

    invoke-virtual {v3, v7, v6, v7, v4}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mInfo:Landroid/widget/FrameLayout;

    iget-object v4, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mImageParams:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v3, v0, v4}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mInfo:Landroid/widget/FrameLayout;

    iget-object v4, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mNormalParams:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v3, v4}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .end local v0           #infoView:Landroid/view/View;
    :cond_aa
    return-void
.end method

.method protected update()V
    .registers 1

    .prologue
    return-void
.end method

.method protected updateNavigationState()V
    .registers 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceSet:Lmiui/app/resourcebrowser/resource/ResourceSet;

    iget v4, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceGroup:I

    invoke-virtual {v1, v4}, Lmiui/app/resourcebrowser/resource/ResourceSet;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/widget/DataGroup;

    invoke-virtual {v1}, Lmiui/widget/DataGroup;->size()I

    move-result v0

    .local v0, resourceTotal:I
    iget-object v4, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreviousItem:Landroid/view/View;

    iget v1, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceIndex:I

    if-lez v1, :cond_3d

    move v1, v2

    :goto_17
    invoke-virtual {v4, v1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mNextItem:Landroid/view/View;

    iget v4, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceIndex:I

    add-int/lit8 v5, v0, -0x1

    if-ge v4, v5, :cond_3f

    :goto_22
    invoke-virtual {v1, v2}, Landroid/view/View;->setEnabled(Z)V

    iget v1, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceIndex:I

    add-int/lit8 v2, v0, -0x5

    if-lt v1, v2, :cond_3c

    iget-boolean v1, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mReachBottom:Z

    if-nez v1, :cond_3c

    iget-boolean v1, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mIsOnlineResourceSet:Z

    if-eqz v1, :cond_3c

    iget-boolean v1, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mIsSingleResourceSet:Z

    if-nez v1, :cond_3c

    const/16 v1, 0x1e

    invoke-direct {p0, v0, v1}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->requestResources(II)V

    :cond_3c
    return-void

    :cond_3d
    move v1, v3

    goto :goto_17

    :cond_3f
    move v2, v3

    goto :goto_22
.end method
