.class Lmiui/app/resourcebrowser/ResourceDetailActivity$10;
.super Ljava/lang/Object;
.source "ResourceDetailActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiui/app/resourcebrowser/ResourceDetailActivity;->bindScreenRingtoneView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;


# direct methods
.method constructor <init>(Lmiui/app/resourcebrowser/ResourceDetailActivity;)V
    .registers 2
    .parameter

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$10;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 4
    .parameter "v"

    .prologue
    const v1, 0x60200d2

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$10;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    iget-object v0, v0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mBatchPlayer:Lmiui/app/resourcebrowser/util/BatchMediaPlayer;

    if-nez v0, :cond_1d

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$10;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    iget-object v0, v0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPlayButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$10;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    #calls: Lmiui/app/resourcebrowser/ResourceDetailActivity;->initPlayer()V
    invoke-static {v0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->access$1300(Lmiui/app/resourcebrowser/ResourceDetailActivity;)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$10;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    iget-object v0, v0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mBatchPlayer:Lmiui/app/resourcebrowser/util/BatchMediaPlayer;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->start()V

    :goto_1c
    return-void

    :cond_1d
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$10;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    iget-object v0, v0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mBatchPlayer:Lmiui/app/resourcebrowser/util/BatchMediaPlayer;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_39

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$10;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    iget-object v0, v0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPlayButton:Landroid/widget/ImageView;

    const v1, 0x60200d5

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$10;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    iget-object v0, v0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mBatchPlayer:Lmiui/app/resourcebrowser/util/BatchMediaPlayer;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->pause()V

    goto :goto_1c

    :cond_39
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$10;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    iget-object v0, v0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPlayButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$10;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    iget-object v0, v0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mBatchPlayer:Lmiui/app/resourcebrowser/util/BatchMediaPlayer;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->start()V

    goto :goto_1c
.end method
