.class public Lmiui/app/resourcebrowser/util/CleanCacheTask$ResourcePreviewCleanPolicy;
.super Lmiui/app/resourcebrowser/util/CleanCachePolicy;
.source "CleanCacheTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/app/resourcebrowser/util/CleanCacheTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ResourcePreviewCleanPolicy"
.end annotation


# static fields
.field private static final sDecodedDownloadBasePath:Ljava/lang/String;

.field private static final sDecodedSystemBasePath:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    const/16 v2, 0x5f

    const/16 v1, 0x2f

    const-string v0, "/system/media/"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lmiui/app/resourcebrowser/util/CleanCacheTask$ResourcePreviewCleanPolicy;->sDecodedSystemBasePath:Ljava/lang/String;

    sget-object v0, Lmiui/app/resourcebrowser/ResourceConstants;->DOWNLOADED_RESOURCE_BASE_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lmiui/app/resourcebrowser/util/CleanCacheTask$ResourcePreviewCleanPolicy;->sDecodedDownloadBasePath:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    invoke-direct {p0}, Lmiui/app/resourcebrowser/util/CleanCachePolicy;-><init>()V

    return-void
.end method

.method private existResource(Ljava/lang/String;)Z
    .registers 9
    .parameter "encodedName"

    .prologue
    const/16 v6, 0x5f

    const/4 v2, 0x0

    const/4 v0, -0x1

    .local v0, index:I
    sget-object v3, Lmiui/app/resourcebrowser/util/CleanCacheTask$ResourcePreviewCleanPolicy;->sDecodedSystemBasePath:Ljava/lang/String;

    invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_48

    sget-object v3, Lmiui/app/resourcebrowser/util/CleanCacheTask$ResourcePreviewCleanPolicy;->sDecodedSystemBasePath:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    :cond_12
    :goto_12
    if-lez v0, :cond_47

    :cond_14
    invoke-virtual {p1, v6, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    if-lez v0, :cond_47

    add-int/lit8 v0, v0, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x2f

    invoke-virtual {v4, v6, v5}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .local v1, path:Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_14

    const/4 v2, 0x1

    .end local v1           #path:Ljava/lang/String;
    :cond_47
    return v2

    :cond_48
    sget-object v3, Lmiui/app/resourcebrowser/util/CleanCacheTask$ResourcePreviewCleanPolicy;->sDecodedDownloadBasePath:Ljava/lang/String;

    invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_12

    sget-object v3, Lmiui/app/resourcebrowser/util/CleanCacheTask$ResourcePreviewCleanPolicy;->sDecodedDownloadBasePath:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_12
.end method


# virtual methods
.method protected onCleanCacheFiles(Ljava/util/List;)I
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lmiui/app/resourcebrowser/util/CleanCachePolicy$FileEntry;",
            ">;)I"
        }
    .end annotation

    .prologue
    .local p1, entryList:Ljava/util/List;,"Ljava/util/List<Lmiui/app/resourcebrowser/util/CleanCachePolicy$FileEntry;>;"
    const/4 v3, 0x0

    .local v3, ret:I
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, i$:Ljava/util/Iterator;
    :cond_5
    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_28

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/app/resourcebrowser/util/CleanCachePolicy$FileEntry;

    .local v1, entry:Lmiui/app/resourcebrowser/util/CleanCachePolicy$FileEntry;
    new-instance v0, Ljava/io/File;

    iget-object v4, v1, Lmiui/app/resourcebrowser/util/CleanCachePolicy$FileEntry;->filePath:Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .local v0, cacheFile:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lmiui/app/resourcebrowser/util/CleanCacheTask$ResourcePreviewCleanPolicy;->existResource(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_5

    invoke-static {v0}, Lmiui/app/resourcebrowser/util/CleanCacheTask$ResourcePreviewCleanPolicy;->deleteFile(Ljava/io/File;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .end local v0           #cacheFile:Ljava/io/File;
    .end local v1           #entry:Lmiui/app/resourcebrowser/util/CleanCachePolicy$FileEntry;
    :cond_28
    return v3
.end method
