.class public Lmiui/app/resourcebrowser/util/CleanCacheTask$ResourceCacheCleanPolicy;
.super Lmiui/app/resourcebrowser/util/CleanCachePolicy$LRUCleanCacheByAccessTime;
.source "CleanCacheTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/app/resourcebrowser/util/CleanCacheTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ResourceCacheCleanPolicy"
.end annotation


# instance fields
.field mSubDirCacheClean:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lmiui/app/resourcebrowser/util/CleanCacheRunnable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(I)V
    .registers 3
    .parameter "remainNumber"

    .prologue
    invoke-direct {p0, p1}, Lmiui/app/resourcebrowser/util/CleanCachePolicy$LRUCleanCacheByAccessTime;-><init>(I)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/app/resourcebrowser/util/CleanCacheTask$ResourceCacheCleanPolicy;->mSubDirCacheClean:Ljava/util/List;

    return-void
.end method


# virtual methods
.method protected onCleanCacheFiles(Ljava/util/List;)I
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lmiui/app/resourcebrowser/util/CleanCachePolicy$FileEntry;",
            ">;)I"
        }
    .end annotation

    .prologue
    .local p1, entryList:Ljava/util/List;,"Ljava/util/List<Lmiui/app/resourcebrowser/util/CleanCachePolicy$FileEntry;>;"
    invoke-super {p0, p1}, Lmiui/app/resourcebrowser/util/CleanCachePolicy$LRUCleanCacheByAccessTime;->onCleanCacheFiles(Ljava/util/List;)I

    move-result v2

    .local v2, ret:I
    iget-object v3, p0, Lmiui/app/resourcebrowser/util/CleanCacheTask$ResourceCacheCleanPolicy;->mSubDirCacheClean:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/app/resourcebrowser/util/CleanCacheRunnable;

    .local v0, clean:Lmiui/app/resourcebrowser/util/CleanCacheRunnable;
    invoke-virtual {v0}, Lmiui/app/resourcebrowser/util/CleanCacheRunnable;->run()V

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/util/CleanCacheRunnable;->getCleanFilesNumber()I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_a

    .end local v0           #clean:Lmiui/app/resourcebrowser/util/CleanCacheRunnable;
    :cond_1f
    return v2
.end method

.method protected onScanCacheFiles(Ljava/lang/String;)Ljava/util/List;
    .registers 8
    .parameter "dirPath"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lmiui/app/resourcebrowser/util/CleanCachePolicy$FileEntry;",
            ">;"
        }
    .end annotation

    .prologue
    invoke-super {p0, p1}, Lmiui/app/resourcebrowser/util/CleanCachePolicy$LRUCleanCacheByAccessTime;->onScanCacheFiles(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .local v2, list:Ljava/util/List;,"Ljava/util/List<Lmiui/app/resourcebrowser/util/CleanCachePolicy$FileEntry;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v1, v4, -0x1

    .local v1, i:I
    :goto_a
    if-ltz v1, :cond_3d

    new-instance v0, Ljava/io/File;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lmiui/app/resourcebrowser/util/CleanCachePolicy$FileEntry;

    iget-object v4, v4, Lmiui/app/resourcebrowser/util/CleanCachePolicy$FileEntry;->filePath:Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .local v0, file:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_3a

    invoke-interface {v2, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    new-instance v3, Lmiui/app/resourcebrowser/util/CleanCacheRunnable;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lmiui/app/resourcebrowser/util/CleanCacheRunnable;-><init>(Ljava/lang/String;)V

    .local v3, subClean:Lmiui/app/resourcebrowser/util/CleanCacheRunnable;
    new-instance v4, Lmiui/app/resourcebrowser/util/CleanCachePolicy$LRUCleanCacheByAccessTime;

    iget v5, p0, Lmiui/app/resourcebrowser/util/CleanCachePolicy$LRUCleanCacheByAccessTime;->mRemainNumber:I

    invoke-direct {v4, v5}, Lmiui/app/resourcebrowser/util/CleanCachePolicy$LRUCleanCacheByAccessTime;-><init>(I)V

    invoke-virtual {v3, v4}, Lmiui/app/resourcebrowser/util/CleanCacheRunnable;->setCleanPolicy(Lmiui/app/resourcebrowser/util/CleanCachePolicy;)V

    iget-object v4, p0, Lmiui/app/resourcebrowser/util/CleanCacheTask$ResourceCacheCleanPolicy;->mSubDirCacheClean:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .end local v3           #subClean:Lmiui/app/resourcebrowser/util/CleanCacheRunnable;
    :cond_3a
    add-int/lit8 v1, v1, -0x1

    goto :goto_a

    .end local v0           #file:Ljava/io/File;
    :cond_3d
    return-object v2
.end method
