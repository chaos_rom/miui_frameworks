.class public Lmiui/app/resourcebrowser/util/ZipFileHelper;
.super Ljava/lang/Object;
.source "ZipFileHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/app/resourcebrowser/util/ZipFileHelper$1;,
        Lmiui/app/resourcebrowser/util/ZipFileHelper$DescriptionSAXHandler;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getZipResourceDescribeInfo(Ljava/lang/String;Ljava/lang/String;)Ljava/util/HashMap;
    .registers 5
    .parameter "zipfilePath"
    .parameter "xmlEntryName"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .local v0, ret:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :try_start_1
    new-instance v1, Ljava/util/zip/ZipFile;

    invoke-direct {v1, p0}, Ljava/util/zip/ZipFile;-><init>(Ljava/lang/String;)V

    .local v1, zipfile:Ljava/util/zip/ZipFile;
    invoke-static {v1, p1}, Lmiui/app/resourcebrowser/util/ZipFileHelper;->getZipResourceDescribeInfo(Ljava/util/zip/ZipFile;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v1}, Ljava/util/zip/ZipFile;->close()V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_d} :catch_e

    .end local v1           #zipfile:Ljava/util/zip/ZipFile;
    :goto_d
    return-object v0

    :catch_e
    move-exception v2

    goto :goto_d
.end method

.method public static getZipResourceDescribeInfo(Ljava/util/zip/ZipFile;Ljava/lang/String;)Ljava/util/HashMap;
    .registers 10
    .parameter "zipfile"
    .parameter "xmlEntryName"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/zip/ZipFile;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x0

    .local v1, handler:Lmiui/app/resourcebrowser/util/ZipFileHelper$DescriptionSAXHandler;
    :try_start_2
    invoke-virtual {p0, p1}, Ljava/util/zip/ZipFile;->getEntry(Ljava/lang/String;)Ljava/util/zip/ZipEntry;

    move-result-object v0

    .local v0, entry:Ljava/util/zip/ZipEntry;
    if-eqz v0, :cond_2a

    invoke-virtual {p0, v0}, Ljava/util/zip/ZipFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;

    move-result-object v3

    .local v3, is:Ljava/io/InputStream;
    :goto_c
    if-eqz v3, :cond_23

    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v5

    .local v5, spf:Ljavax/xml/parsers/SAXParserFactory;
    invoke-virtual {v5}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v4

    .local v4, sp:Ljavax/xml/parsers/SAXParser;
    new-instance v2, Lmiui/app/resourcebrowser/util/ZipFileHelper$DescriptionSAXHandler;

    const/4 v7, 0x0

    invoke-direct {v2, v7}, Lmiui/app/resourcebrowser/util/ZipFileHelper$DescriptionSAXHandler;-><init>(Lmiui/app/resourcebrowser/util/ZipFileHelper$1;)V
    :try_end_1c
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_1c} :catch_2c

    .end local v1           #handler:Lmiui/app/resourcebrowser/util/ZipFileHelper$DescriptionSAXHandler;
    .local v2, handler:Lmiui/app/resourcebrowser/util/ZipFileHelper$DescriptionSAXHandler;
    :try_start_1c
    invoke-virtual {v4, v3, v2}, Ljavax/xml/parsers/SAXParser;->parse(Ljava/io/InputStream;Lorg/xml/sax/helpers/DefaultHandler;)V

    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_22
    .catch Ljava/lang/Exception; {:try_start_1c .. :try_end_22} :catch_2e

    move-object v1, v2

    .end local v0           #entry:Ljava/util/zip/ZipEntry;
    .end local v2           #handler:Lmiui/app/resourcebrowser/util/ZipFileHelper$DescriptionSAXHandler;
    .end local v3           #is:Ljava/io/InputStream;
    .end local v4           #sp:Ljavax/xml/parsers/SAXParser;
    .end local v5           #spf:Ljavax/xml/parsers/SAXParserFactory;
    .restart local v1       #handler:Lmiui/app/resourcebrowser/util/ZipFileHelper$DescriptionSAXHandler;
    :cond_23
    :goto_23
    if-eqz v1, :cond_29

    invoke-virtual {v1}, Lmiui/app/resourcebrowser/util/ZipFileHelper$DescriptionSAXHandler;->getElementEntries()Ljava/util/HashMap;

    move-result-object v6

    :cond_29
    return-object v6

    .restart local v0       #entry:Ljava/util/zip/ZipEntry;
    :cond_2a
    move-object v3, v6

    goto :goto_c

    .end local v0           #entry:Ljava/util/zip/ZipEntry;
    :catch_2c
    move-exception v7

    goto :goto_23

    .end local v1           #handler:Lmiui/app/resourcebrowser/util/ZipFileHelper$DescriptionSAXHandler;
    .restart local v0       #entry:Ljava/util/zip/ZipEntry;
    .restart local v2       #handler:Lmiui/app/resourcebrowser/util/ZipFileHelper$DescriptionSAXHandler;
    .restart local v3       #is:Ljava/io/InputStream;
    .restart local v4       #sp:Ljavax/xml/parsers/SAXParser;
    .restart local v5       #spf:Ljavax/xml/parsers/SAXParserFactory;
    :catch_2e
    move-exception v7

    move-object v1, v2

    .end local v2           #handler:Lmiui/app/resourcebrowser/util/ZipFileHelper$DescriptionSAXHandler;
    .restart local v1       #handler:Lmiui/app/resourcebrowser/util/ZipFileHelper$DescriptionSAXHandler;
    goto :goto_23
.end method
