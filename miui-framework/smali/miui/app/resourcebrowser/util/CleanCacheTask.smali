.class public Lmiui/app/resourcebrowser/util/CleanCacheTask;
.super Ljava/lang/Object;
.source "CleanCacheTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/app/resourcebrowser/util/CleanCacheTask$ResourcePreviewCleanPolicy;,
        Lmiui/app/resourcebrowser/util/CleanCacheTask$ResourceCacheCleanPolicy;
    }
.end annotation


# static fields
.field private static sCleanTask:Lmiui/app/resourcebrowser/util/CleanCacheTask;


# instance fields
.field protected mCleanRunnable:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lmiui/app/resourcebrowser/util/CleanCacheRunnable;",
            ">;"
        }
    .end annotation
.end field

.field private mIdleHandler:Landroid/os/MessageQueue$IdleHandler;


# direct methods
.method private constructor <init>()V
    .registers 2

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/app/resourcebrowser/util/CleanCacheTask;->mCleanRunnable:Ljava/util/List;

    return-void
.end method

.method static synthetic access$000(Lmiui/app/resourcebrowser/util/CleanCacheTask;)Landroid/os/MessageQueue$IdleHandler;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/CleanCacheTask;->mIdleHandler:Landroid/os/MessageQueue$IdleHandler;

    return-object v0
.end method

.method public static getInstance()Lmiui/app/resourcebrowser/util/CleanCacheTask;
    .registers 2

    .prologue
    sget-object v0, Lmiui/app/resourcebrowser/util/CleanCacheTask;->sCleanTask:Lmiui/app/resourcebrowser/util/CleanCacheTask;

    if-nez v0, :cond_13

    const-class v1, Lmiui/app/resourcebrowser/util/CleanCacheTask;

    monitor-enter v1

    :try_start_7
    sget-object v0, Lmiui/app/resourcebrowser/util/CleanCacheTask;->sCleanTask:Lmiui/app/resourcebrowser/util/CleanCacheTask;

    if-nez v0, :cond_12

    new-instance v0, Lmiui/app/resourcebrowser/util/CleanCacheTask;

    invoke-direct {v0}, Lmiui/app/resourcebrowser/util/CleanCacheTask;-><init>()V

    sput-object v0, Lmiui/app/resourcebrowser/util/CleanCacheTask;->sCleanTask:Lmiui/app/resourcebrowser/util/CleanCacheTask;

    :cond_12
    monitor-exit v1
    :try_end_13
    .catchall {:try_start_7 .. :try_end_13} :catchall_16

    :cond_13
    sget-object v0, Lmiui/app/resourcebrowser/util/CleanCacheTask;->sCleanTask:Lmiui/app/resourcebrowser/util/CleanCacheTask;

    return-object v0

    :catchall_16
    move-exception v0

    :try_start_17
    monitor-exit v1
    :try_end_18
    .catchall {:try_start_17 .. :try_end_18} :catchall_16

    throw v0
.end method


# virtual methods
.method public addCleanRunnable(Lmiui/app/resourcebrowser/util/CleanCacheRunnable;)V
    .registers 3
    .parameter "clean"

    .prologue
    if-eqz p1, :cond_7

    iget-object v0, p0, Lmiui/app/resourcebrowser/util/CleanCacheTask;->mCleanRunnable:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_7
    return-void
.end method

.method protected initCleanTask()V
    .registers 7

    .prologue
    sget-object v0, Lmiui/app/resourcebrowser/ResourceConstants;->CACHE_FILE_PATHS:[Ljava/lang/String;

    .local v0, cacheDir:[Ljava/lang/String;
    sget-object v1, Lmiui/app/resourcebrowser/ResourceConstants;->CACHE_FILE_MAX_NUMBER:[I

    .local v1, cacheNumber:[I
    const/4 v3, 0x0

    .local v3, i:I
    :goto_5
    array-length v4, v0

    if-ge v3, v4, :cond_32

    new-instance v2, Lmiui/app/resourcebrowser/util/CleanCacheRunnable;

    aget-object v4, v0, v3

    invoke-direct {v2, v4}, Lmiui/app/resourcebrowser/util/CleanCacheRunnable;-><init>(Ljava/lang/String;)V

    .local v2, clean:Lmiui/app/resourcebrowser/util/CleanCacheRunnable;
    aget-object v4, v0, v3

    sget-object v5, Lmiui/app/resourcebrowser/ResourceConstants;->PREVIEW_PATH:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_27

    new-instance v4, Lmiui/app/resourcebrowser/util/CleanCacheTask$ResourcePreviewCleanPolicy;

    invoke-direct {v4}, Lmiui/app/resourcebrowser/util/CleanCacheTask$ResourcePreviewCleanPolicy;-><init>()V

    invoke-virtual {v2, v4}, Lmiui/app/resourcebrowser/util/CleanCacheRunnable;->setCleanPolicy(Lmiui/app/resourcebrowser/util/CleanCachePolicy;)V

    :goto_21
    invoke-virtual {p0, v2}, Lmiui/app/resourcebrowser/util/CleanCacheTask;->addCleanRunnable(Lmiui/app/resourcebrowser/util/CleanCacheRunnable;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    :cond_27
    new-instance v4, Lmiui/app/resourcebrowser/util/CleanCacheTask$ResourceCacheCleanPolicy;

    aget v5, v1, v3

    invoke-direct {v4, v5}, Lmiui/app/resourcebrowser/util/CleanCacheTask$ResourceCacheCleanPolicy;-><init>(I)V

    invoke-virtual {v2, v4}, Lmiui/app/resourcebrowser/util/CleanCacheRunnable;->setCleanPolicy(Lmiui/app/resourcebrowser/util/CleanCachePolicy;)V

    goto :goto_21

    .end local v2           #clean:Lmiui/app/resourcebrowser/util/CleanCacheRunnable;
    :cond_32
    return-void
.end method

.method protected initScheduler()V
    .registers 3

    .prologue
    new-instance v0, Lmiui/app/resourcebrowser/util/CleanCacheTask$1;

    invoke-direct {v0, p0}, Lmiui/app/resourcebrowser/util/CleanCacheTask$1;-><init>(Lmiui/app/resourcebrowser/util/CleanCacheTask;)V

    iput-object v0, p0, Lmiui/app/resourcebrowser/util/CleanCacheTask;->mIdleHandler:Landroid/os/MessageQueue$IdleHandler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getQueue()Landroid/os/MessageQueue;

    move-result-object v0

    iget-object v1, p0, Lmiui/app/resourcebrowser/util/CleanCacheTask;->mIdleHandler:Landroid/os/MessageQueue$IdleHandler;

    invoke-virtual {v0, v1}, Landroid/os/MessageQueue;->addIdleHandler(Landroid/os/MessageQueue$IdleHandler;)V

    return-void
.end method

.method public run()V
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/CleanCacheTask;->mIdleHandler:Landroid/os/MessageQueue$IdleHandler;

    if-nez v0, :cond_a

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/util/CleanCacheTask;->initCleanTask()V

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/util/CleanCacheTask;->initScheduler()V

    :cond_a
    return-void
.end method
