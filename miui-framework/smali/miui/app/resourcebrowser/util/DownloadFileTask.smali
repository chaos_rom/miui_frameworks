.class public Lmiui/app/resourcebrowser/util/DownloadFileTask;
.super Landroid/os/AsyncTask;
.source "DownloadFileTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;",
        "Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;",
        "Ljava/util/List",
        "<",
        "Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;",
        ">;>;"
    }
.end annotation


# static fields
.field private static final BUFFER_SIZE:I = 0x400

.field private static final SUFFIX:Ljava/lang/String; = ".temp"


# instance fields
.field private mId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmiui/app/resourcebrowser/util/DownloadFileTask;->mId:Ljava/lang/String;

    return-void
.end method

.method private handleDownloadFile(Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;)Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;
    .registers 9
    .parameter "entry"

    .prologue
    invoke-virtual {p1}, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->getUrl()Ljava/lang/String;

    move-result-object v4

    .local v4, url:Ljava/lang/String;
    invoke-virtual {p1}, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->getPath()Ljava/lang/String;

    move-result-object v2

    .local v2, path:Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .local v1, file:Ljava/io/File;
    new-instance v3, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".temp"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .local v3, tempFile:Ljava/io/File;
    :try_start_25
    invoke-direct {p0, v4, v3}, Lmiui/app/resourcebrowser/util/DownloadFileTask;->writeToFile(Ljava/lang/String;Ljava/io/File;)V

    invoke-virtual {v3, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    invoke-virtual {p1, v2}, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->setPath(Ljava/lang/String;)V
    :try_end_2e
    .catch Ljava/lang/Exception; {:try_start_25 .. :try_end_2e} :catch_2f

    :cond_2e
    :goto_2e
    return-object p1

    :catch_2f
    move-exception v0

    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const/4 p1, 0x0

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_2e

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    goto :goto_2e
.end method

.method private writeToFile(Ljava/lang/String;Ljava/io/File;)V
    .registers 5
    .parameter "url"
    .parameter "file"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    invoke-static {}, Lmiui/app/resourcebrowser/service/online/OnlineService;->getInstance()Lmiui/app/resourcebrowser/service/online/OnlineService;

    move-result-object v1

    invoke-virtual {v1, p1}, Lmiui/app/resourcebrowser/service/online/OnlineService;->getUrlInputStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    .local v0, is:Ljava/io/InputStream;
    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lmiui/app/resourcebrowser/util/ResourceHelper;->writeTo(Ljava/io/InputStream;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    .prologue
    check-cast p1, [Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/app/resourcebrowser/util/DownloadFileTask;->doInBackground([Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;)Ljava/util/List;
    .registers 4
    .parameter "entries"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;",
            ")",
            "Ljava/util/List",
            "<",
            "Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;",
            ">;"
        }
    .end annotation

    .prologue
    invoke-virtual {p0, p1}, Lmiui/app/resourcebrowser/util/DownloadFileTask;->downloadFiles([Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;)Ljava/util/List;

    move-result-object v0

    .local v0, entryList:Ljava/util/List;,"Ljava/util/List<Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;>;"
    const/4 v1, 0x0

    new-array v1, v1, [Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v1}, Lmiui/app/resourcebrowser/util/DownloadFileTask;->publishProgress([Ljava/lang/Object;)V

    return-object v0
.end method

.method public varargs doInForeground([Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;)Ljava/util/List;
    .registers 3
    .parameter "entries"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;",
            ")",
            "Ljava/util/List",
            "<",
            "Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;",
            ">;"
        }
    .end annotation

    .prologue
    invoke-virtual {p0, p1}, Lmiui/app/resourcebrowser/util/DownloadFileTask;->downloadFiles([Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;)Ljava/util/List;

    move-result-object v0

    .local v0, entryList:Ljava/util/List;,"Ljava/util/List<Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;>;"
    return-object v0
.end method

.method protected varargs downloadFiles([Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;)Ljava/util/List;
    .registers 6
    .parameter "entries"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;",
            ")",
            "Ljava/util/List",
            "<",
            "Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;",
            ">;"
        }
    .end annotation

    .prologue
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .local v1, entryList:Ljava/util/List;,"Ljava/util/List<Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;>;"
    const/4 v2, 0x0

    .local v2, i:I
    :goto_6
    array-length v3, p1

    if-ge v2, v3, :cond_1a

    aget-object v0, p1, v2

    .local v0, entry:Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;
    invoke-direct {p0, v0}, Lmiui/app/resourcebrowser/util/DownloadFileTask;->handleDownloadFile(Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;)Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;

    move-result-object v0

    if-eqz v0, :cond_17

    invoke-virtual {v0, v2}, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->setIndex(I)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_17
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .end local v0           #entry:Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;
    :cond_1a
    return-object v1
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .parameter "o"

    .prologue
    if-eqz p1, :cond_6

    instance-of v1, p1, Lmiui/app/resourcebrowser/util/DownloadFileTask;

    if-nez v1, :cond_8

    :cond_6
    const/4 v1, 0x0

    :goto_7
    return v1

    :cond_8
    move-object v0, p1

    check-cast v0, Lmiui/app/resourcebrowser/util/DownloadFileTask;

    .local v0, d:Lmiui/app/resourcebrowser/util/DownloadFileTask;
    invoke-virtual {v0}, Lmiui/app/resourcebrowser/util/DownloadFileTask;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/util/DownloadFileTask;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_7
.end method

.method public getId()Ljava/lang/String;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/DownloadFileTask;->mId:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .registers 2

    .prologue
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/util/DownloadFileTask;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public setId(Ljava/lang/String;)V
    .registers 2
    .parameter "id"

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/util/DownloadFileTask;->mId:Ljava/lang/String;

    return-void
.end method
