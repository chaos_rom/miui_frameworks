.class public Lmiui/app/resourcebrowser/util/ImageCacheDecoder;
.super Ljava/lang/Object;
.source "ImageCacheDecoder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;,
        Lmiui/app/resourcebrowser/util/ImageCacheDecoder$ImageDecodingListener;,
        Lmiui/app/resourcebrowser/util/ImageCacheDecoder$JobRun;
    }
.end annotation


# static fields
.field private static final MSG_ASYNC_DECODING_FINISH:I = 0x0

.field private static final MSG_ASYNC_DOWNLOAD_FINISH:I = 0x1


# instance fields
.field protected mBitmapCache:Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;

.field private mDecodeExecutorService:Ljava/util/concurrent/ExecutorService;

.field protected mDecodedHeight:I

.field protected mDecodedWidth:I

.field private mDoingJob:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mDownloadExecutorService:Ljava/util/concurrent/ExecutorService;

.field private mFailToDownloadTime:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mHandler:Landroid/os/Handler;

.field private mListener:Lmiui/app/resourcebrowser/util/ImageCacheDecoder$ImageDecodingListener;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;-><init>(I)V

    return-void
.end method

.method public constructor <init>(I)V
    .registers 3
    .parameter "cacheSize"

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->mDoingJob:Ljava/util/HashSet;

    const/4 v0, 0x2

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->mDecodeExecutorService:Ljava/util/concurrent/ExecutorService;

    const/4 v0, 0x4

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->mDownloadExecutorService:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->mFailToDownloadTime:Ljava/util/HashMap;

    const/4 v0, 0x0

    iput-object v0, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->mListener:Lmiui/app/resourcebrowser/util/ImageCacheDecoder$ImageDecodingListener;

    new-instance v0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$1;

    invoke-direct {v0, p0}, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$1;-><init>(Lmiui/app/resourcebrowser/util/ImageCacheDecoder;)V

    iput-object v0, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->mHandler:Landroid/os/Handler;

    new-instance v0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;

    invoke-direct {v0, p0, p1}, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;-><init>(Lmiui/app/resourcebrowser/util/ImageCacheDecoder;I)V

    iput-object v0, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->mBitmapCache:Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;

    return-void
.end method

.method static synthetic access$000(Lmiui/app/resourcebrowser/util/ImageCacheDecoder;)Lmiui/app/resourcebrowser/util/ImageCacheDecoder$ImageDecodingListener;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->mListener:Lmiui/app/resourcebrowser/util/ImageCacheDecoder$ImageDecodingListener;

    return-object v0
.end method

.method static synthetic access$100(Lmiui/app/resourcebrowser/util/ImageCacheDecoder;)Ljava/util/concurrent/ExecutorService;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->mDownloadExecutorService:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method static synthetic access$200(Lmiui/app/resourcebrowser/util/ImageCacheDecoder;)Ljava/util/concurrent/ExecutorService;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->mDecodeExecutorService:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method static synthetic access$300(Lmiui/app/resourcebrowser/util/ImageCacheDecoder;)Ljava/util/HashMap;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->mFailToDownloadTime:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$400(Lmiui/app/resourcebrowser/util/ImageCacheDecoder;)Ljava/util/HashSet;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->mDoingJob:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic access$500(Lmiui/app/resourcebrowser/util/ImageCacheDecoder;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->mHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public clean(Z)V
    .registers 4
    .parameter "stopBgThread"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->mBitmapCache:Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->clean()V

    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->mFailToDownloadTime:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    if-eqz p1, :cond_22

    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->mDecodeExecutorService:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->mDownloadExecutorService:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    :cond_22
    return-void
.end method

.method public decodeImageAsync(Ljava/lang/String;Ljava/lang/String;I)V
    .registers 9
    .parameter "imageLocalPath"
    .parameter "imageOnlinePath"
    .parameter "useIndex"

    .prologue
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1e

    iget-object v1, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->mBitmapCache:Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;

    invoke-virtual {v1, p1}, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    if-nez v1, :cond_1e

    iget-object v1, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->mDoingJob:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1e

    iget-object v1, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->mDecodeExecutorService:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1}, Ljava/util/concurrent/ExecutorService;->isShutdown()Z

    move-result v1

    if-eqz v1, :cond_1f

    :cond_1e
    :goto_1e
    return-void

    :cond_1f
    iget-object v1, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->mFailToDownloadTime:Ljava/util/HashMap;

    invoke-virtual {v1, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .local v0, lastFailToDownloadTime:Ljava/lang/Long;
    if-eqz v0, :cond_38

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    sub-long/2addr v1, v3

    const-wide/16 v3, 0x1388

    cmp-long v1, v1, v3

    if-ltz v1, :cond_1e

    :cond_38
    iget-object v1, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->mDoingJob:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    new-instance v1, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$JobRun;

    invoke-direct {v1, p0, p1, p2, p3}, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$JobRun;-><init>(Lmiui/app/resourcebrowser/util/ImageCacheDecoder;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v1}, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$JobRun;->dispatchJob()V

    goto :goto_1e
.end method

.method public decodeLocalImage(Ljava/lang/String;IZ)Landroid/graphics/Bitmap;
    .registers 10
    .parameter "imagePath"
    .parameter "useIndex"
    .parameter "addIntoCache"

    .prologue
    invoke-virtual {p0, p1}, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .local v0, b:Landroid/graphics/Bitmap;
    if-nez v0, :cond_23

    new-instance v1, Lmiui/util/InputStreamLoader;

    invoke-direct {v1, p1}, Lmiui/util/InputStreamLoader;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->mDecodedWidth:I

    iget v3, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->mDecodedHeight:I

    iget-object v4, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->mBitmapCache:Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->removeIdleCache(Z)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lmiui/util/ImageUtils;->getBitmap(Lmiui/util/InputStreamLoader;IILandroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_23

    if-eqz p3, :cond_23

    iget-object v1, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->mBitmapCache:Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;

    invoke-virtual {v1, p1, v0, p2}, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->add(Ljava/lang/String;Landroid/graphics/Bitmap;I)V

    :cond_23
    return-object v0
.end method

.method public getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .registers 3
    .parameter "imagePath"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->mBitmapCache:Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;

    invoke-virtual {v0, p1}, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentUseBitmapIndex()I
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->mBitmapCache:Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->getCurrentUseIndex()I

    move-result v0

    return v0
.end method

.method public regeisterListener(Lmiui/app/resourcebrowser/util/ImageCacheDecoder$ImageDecodingListener;)V
    .registers 2
    .parameter "l"

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->mListener:Lmiui/app/resourcebrowser/util/ImageCacheDecoder$ImageDecodingListener;

    return-void
.end method

.method public setCacheCapacity(I)V
    .registers 3
    .parameter "cacheCapacity"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->mBitmapCache:Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;

    invoke-virtual {v0, p1}, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->setCacheSize(I)V

    return-void
.end method

.method public setCurrentUseBitmapIndex(I)V
    .registers 3
    .parameter "index"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->mBitmapCache:Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;

    invoke-virtual {v0, p1}, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->setCurrentUseIndex(I)V

    return-void
.end method

.method public setScaledSize(II)V
    .registers 3
    .parameter "scaledWidth"
    .parameter "scaledHeight"

    .prologue
    iput p1, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->mDecodedWidth:I

    iput p2, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->mDecodedHeight:I

    return-void
.end method
