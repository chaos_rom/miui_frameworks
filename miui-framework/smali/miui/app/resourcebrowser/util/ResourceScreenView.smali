.class public Lmiui/app/resourcebrowser/util/ResourceScreenView;
.super Lmiui/widget/ScreenView;
.source "ResourceScreenView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/app/resourcebrowser/util/ResourceScreenView$ScreenChangeListener;
    }
.end annotation


# instance fields
.field private mScreenChangeListener:Lmiui/app/resourcebrowser/util/ResourceScreenView$ScreenChangeListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    .prologue
    invoke-direct {p0, p1}, Lmiui/widget/ScreenView;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .parameter "context"
    .parameter "attrs"

    .prologue
    invoke-direct {p0, p1, p2}, Lmiui/widget/ScreenView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    invoke-direct {p0, p1, p2, p3}, Lmiui/widget/ScreenView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method public setScreenChangeListener(Lmiui/app/resourcebrowser/util/ResourceScreenView$ScreenChangeListener;)V
    .registers 2
    .parameter "l"

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/util/ResourceScreenView;->mScreenChangeListener:Lmiui/app/resourcebrowser/util/ResourceScreenView$ScreenChangeListener;

    return-void
.end method

.method protected snapToScreen(IIZ)V
    .registers 5
    .parameter "whichScreen"
    .parameter "velocity"
    .parameter "settle"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ResourceScreenView;->mScreenChangeListener:Lmiui/app/resourcebrowser/util/ResourceScreenView$ScreenChangeListener;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ResourceScreenView;->mScreenChangeListener:Lmiui/app/resourcebrowser/util/ResourceScreenView$ScreenChangeListener;

    invoke-interface {v0, p1}, Lmiui/app/resourcebrowser/util/ResourceScreenView$ScreenChangeListener;->snapToScreen(I)V

    :cond_9
    invoke-super {p0, p1, p2, p3}, Lmiui/widget/ScreenView;->snapToScreen(IIZ)V

    return-void
.end method
