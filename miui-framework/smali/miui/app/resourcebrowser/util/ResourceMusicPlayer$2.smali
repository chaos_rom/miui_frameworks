.class Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$2;
.super Ljava/lang/Object;
.source "ResourceMusicPlayer.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->initPlayButtonIfNeed(Landroid/widget/ImageView;Lmiui/app/resourcebrowser/resource/Resource;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;

.field final synthetic val$data:Lmiui/app/resourcebrowser/resource/Resource;

.field final synthetic val$view:Landroid/widget/ImageView;


# direct methods
.method constructor <init>(Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;Lmiui/app/resourcebrowser/resource/Resource;Landroid/widget/ImageView;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$2;->this$0:Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;

    iput-object p2, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$2;->val$data:Lmiui/app/resourcebrowser/resource/Resource;

    iput-object p3, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$2;->val$view:Landroid/widget/ImageView;

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 6
    .parameter "v"

    .prologue
    iget-object v1, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$2;->val$data:Lmiui/app/resourcebrowser/resource/Resource;

    iget-object v2, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$2;->this$0:Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;

    #getter for: Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->mCurrentPlayingResource:Lmiui/app/resourcebrowser/resource/Resource;
    invoke-static {v2}, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->access$300(Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;)Lmiui/app/resourcebrowser/resource/Resource;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmiui/app/resourcebrowser/resource/Resource;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2c

    const/4 v0, 0x1

    .local v0, isGoingToPlay:Z
    :goto_f
    iget-object v1, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$2;->this$0:Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;

    invoke-virtual {v1}, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->stopMusic()V

    if-eqz v0, :cond_2b

    iget-object v1, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$2;->val$data:Lmiui/app/resourcebrowser/resource/Resource;

    invoke-virtual {v1}, Lmiui/app/resourcebrowser/resource/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2b

    iget-object v1, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$2;->this$0:Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;

    iget-object v2, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$2;->val$view:Landroid/widget/ImageView;

    iget-object v3, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$2;->val$data:Lmiui/app/resourcebrowser/resource/Resource;

    invoke-virtual {v1, v2, v3}, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->playMusic(Landroid/widget/ImageView;Lmiui/app/resourcebrowser/resource/Resource;)V

    :cond_2b
    return-void

    .end local v0           #isGoingToPlay:Z
    :cond_2c
    const/4 v0, 0x0

    goto :goto_f
.end method
