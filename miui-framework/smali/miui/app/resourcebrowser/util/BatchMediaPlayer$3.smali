.class Lmiui/app/resourcebrowser/util/BatchMediaPlayer$3;
.super Ljava/lang/Object;
.source "BatchMediaPlayer.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->start()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/app/resourcebrowser/util/BatchMediaPlayer;


# direct methods
.method constructor <init>(Lmiui/app/resourcebrowser/util/BatchMediaPlayer;)V
    .registers 2
    .parameter

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer$3;->this$0:Lmiui/app/resourcebrowser/util/BatchMediaPlayer;

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompletion(Landroid/media/MediaPlayer;)V
    .registers 6
    .parameter "mp"

    .prologue
    const/16 v2, 0x3e8

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v3

    rsub-int v3, v3, 0x7d0

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    int-to-long v0, v2

    .local v0, delay:J
    const-wide/16 v2, 0x1f4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iget-object v2, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer$3;->this$0:Lmiui/app/resourcebrowser/util/BatchMediaPlayer;

    #getter for: Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mhandler:Landroid/os/Handler;
    invoke-static {v2}, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->access$200(Lmiui/app/resourcebrowser/util/BatchMediaPlayer;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer$3;->this$0:Lmiui/app/resourcebrowser/util/BatchMediaPlayer;

    #getter for: Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mBatchPlayRun:Ljava/lang/Runnable;
    invoke-static {v3}, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->access$100(Lmiui/app/resourcebrowser/util/BatchMediaPlayer;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v2, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer$3;->this$0:Lmiui/app/resourcebrowser/util/BatchMediaPlayer;

    #getter for: Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mhandler:Landroid/os/Handler;
    invoke-static {v2}, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->access$200(Lmiui/app/resourcebrowser/util/BatchMediaPlayer;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer$3;->this$0:Lmiui/app/resourcebrowser/util/BatchMediaPlayer;

    #getter for: Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mBatchPlayRun:Ljava/lang/Runnable;
    invoke-static {v3}, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->access$100(Lmiui/app/resourcebrowser/util/BatchMediaPlayer;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method
