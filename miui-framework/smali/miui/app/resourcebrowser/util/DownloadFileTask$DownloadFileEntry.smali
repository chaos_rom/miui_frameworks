.class public Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;
.super Ljava/lang/Object;
.source "DownloadFileTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/app/resourcebrowser/util/DownloadFileTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DownloadFileEntry"
.end annotation


# instance fields
.field private index:I

.field private path:Ljava/lang/String;

.field private url:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 7
    .parameter "obj"

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne p0, p1, :cond_5

    :cond_4
    :goto_4
    return v1

    :cond_5
    if-nez p1, :cond_9

    move v1, v2

    goto :goto_4

    :cond_9
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_15

    move v1, v2

    goto :goto_4

    :cond_15
    move-object v0, p1

    check-cast v0, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;

    .local v0, other:Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;
    iget-object v3, p0, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->path:Ljava/lang/String;

    if-nez v3, :cond_22

    iget-object v3, v0, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->path:Ljava/lang/String;

    if-eqz v3, :cond_2e

    move v1, v2

    goto :goto_4

    :cond_22
    iget-object v3, p0, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->path:Ljava/lang/String;

    iget-object v4, v0, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->path:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2e

    move v1, v2

    goto :goto_4

    :cond_2e
    iget-object v3, p0, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->url:Ljava/lang/String;

    if-nez v3, :cond_38

    iget-object v3, v0, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->url:Ljava/lang/String;

    if-eqz v3, :cond_4

    move v1, v2

    goto :goto_4

    :cond_38
    iget-object v3, p0, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->url:Ljava/lang/String;

    iget-object v4, v0, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->url:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    goto :goto_4
.end method

.method public getIndex()I
    .registers 2

    .prologue
    iget v0, p0, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->index:I

    return v0
.end method

.method public getPath()Ljava/lang/String;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->path:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->url:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .registers 6

    .prologue
    const/4 v3, 0x0

    const/16 v0, 0x1f

    .local v0, prime:I
    const/4 v1, 0x1

    .local v1, result:I
    iget-object v2, p0, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->path:Ljava/lang/String;

    if-nez v2, :cond_14

    move v2, v3

    :goto_9
    add-int/lit8 v1, v2, 0x1f

    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->url:Ljava/lang/String;

    if-nez v4, :cond_1b

    :goto_11
    add-int v1, v2, v3

    return v1

    :cond_14
    iget-object v2, p0, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->path:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_9

    :cond_1b
    iget-object v3, p0, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->url:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    goto :goto_11
.end method

.method public setIndex(I)V
    .registers 2
    .parameter "index"

    .prologue
    iput p1, p0, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->index:I

    return-void
.end method

.method public setPath(Ljava/lang/String;)V
    .registers 2
    .parameter "path"

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->path:Ljava/lang/String;

    return-void
.end method

.method public setUrl(Ljava/lang/String;)V
    .registers 2
    .parameter "url"

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->url:Ljava/lang/String;

    return-void
.end method
