.class Lmiui/app/resourcebrowser/util/ResourceDownloadHandler$1;
.super Landroid/content/BroadcastReceiver;
.source "ResourceDownloadHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;


# direct methods
.method constructor <init>(Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;)V
    .registers 2
    .parameter

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler$1;->this$0:Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 15
    .parameter "context"
    .parameter "intent"

    .prologue
    new-instance v5, Landroid/app/MiuiDownloadManager$Query;

    invoke-direct {v5}, Landroid/app/MiuiDownloadManager$Query;-><init>()V

    .local v5, query:Landroid/app/MiuiDownloadManager$Query;
    const-string v9, "extra_download_id"

    const-wide/16 v10, -0x1

    invoke-virtual {p2, v9, v10, v11}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    .local v3, id:J
    const-wide/16 v9, -0x1

    cmp-long v9, v3, v9

    if-eqz v9, :cond_60

    const/4 v9, 0x1

    new-array v9, v9, [J

    const/4 v10, 0x0

    aput-wide v3, v9, v10

    invoke-virtual {v5, v9}, Landroid/app/MiuiDownloadManager$Query;->setFilterById([J)Landroid/app/DownloadManager$Query;

    iget-object v9, p0, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler$1;->this$0:Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;

    #getter for: Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;->mDownloadManager:Landroid/app/DownloadManager;
    invoke-static {v9}, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;->access$000(Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;)Landroid/app/DownloadManager;

    move-result-object v9

    invoke-virtual {v9, v5}, Landroid/app/DownloadManager;->query(Landroid/app/DownloadManager$Query;)Landroid/database/Cursor;

    move-result-object v1

    .local v1, cursor:Landroid/database/Cursor;
    if-eqz v1, :cond_5b

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v9

    if-eqz v9, :cond_5b

    const-string v9, "local_uri"

    invoke-interface {v1, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .local v7, storedPath:Ljava/lang/String;
    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v9}, Landroid/net/Uri;->getEncodedPath()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .local v0, action:Ljava/lang/String;
    const-string v9, "android.intent.action.DOWNLOAD_COMPLETE"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_73

    invoke-static {v1}, Landroid/app/MiuiDownloadManager;->isDownloadSuccess(Landroid/database/Cursor;)Z

    move-result v9

    if-eqz v9, :cond_61

    iget-object v9, p0, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler$1;->this$0:Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;

    invoke-virtual {v9, v7}, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;->handleDownloadSuccessed(Ljava/lang/String;)V

    .end local v0           #action:Ljava/lang/String;
    .end local v7           #storedPath:Ljava/lang/String;
    :cond_5b
    :goto_5b
    if-eqz v1, :cond_60

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .end local v1           #cursor:Landroid/database/Cursor;
    :cond_60
    return-void

    .restart local v0       #action:Ljava/lang/String;
    .restart local v1       #cursor:Landroid/database/Cursor;
    .restart local v7       #storedPath:Ljava/lang/String;
    :cond_61
    const-string v9, "extra_download_status"

    const/16 v10, 0x10

    invoke-virtual {p2, v9, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    .local v6, status:I
    const/16 v9, 0x10

    if-ne v6, v9, :cond_5b

    iget-object v9, p0, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler$1;->this$0:Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;

    invoke-virtual {v9, v7}, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;->handleDownloadFailed(Ljava/lang/String;)V

    goto :goto_5b

    .end local v6           #status:I
    :cond_73
    const-string v9, "android.intent.action.DOWNLOAD_UPDATED"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5b

    const-string v9, "extra_download_current_bytes"

    const-wide/16 v10, 0x0

    invoke-virtual {p2, v9, v10, v11}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v9

    long-to-int v2, v9

    .local v2, downloadBytes:I
    const-string v9, "extra_download_total_bytes"

    const-wide/16 v10, 0x1

    invoke-virtual {p2, v9, v10, v11}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v9

    long-to-int v8, v9

    .local v8, totalBytes:I
    iget-object v9, p0, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler$1;->this$0:Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;

    invoke-virtual {v9, v7, v2, v8}, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;->handleDownloadProgress(Ljava/lang/String;II)V

    goto :goto_5b
.end method
