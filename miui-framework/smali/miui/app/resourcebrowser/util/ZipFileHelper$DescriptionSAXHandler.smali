.class Lmiui/app/resourcebrowser/util/ZipFileHelper$DescriptionSAXHandler;
.super Lorg/xml/sax/helpers/DefaultHandler;
.source "ZipFileHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/app/resourcebrowser/util/ZipFileHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DescriptionSAXHandler"
.end annotation


# instance fields
.field private nvp:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private value:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .registers 2

    .prologue
    invoke-direct {p0}, Lorg/xml/sax/helpers/DefaultHandler;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lmiui/app/resourcebrowser/util/ZipFileHelper$DescriptionSAXHandler;->nvp:Ljava/util/HashMap;

    return-void
.end method

.method synthetic constructor <init>(Lmiui/app/resourcebrowser/util/ZipFileHelper$1;)V
    .registers 2
    .parameter "x0"

    .prologue
    invoke-direct {p0}, Lmiui/app/resourcebrowser/util/ZipFileHelper$DescriptionSAXHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public characters([CII)V
    .registers 5
    .parameter "ch"
    .parameter "start"
    .parameter "length"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1, p2, p3}, Ljava/lang/String;-><init>([CII)V

    iput-object v0, p0, Lmiui/app/resourcebrowser/util/ZipFileHelper$DescriptionSAXHandler;->value:Ljava/lang/String;

    return-void
.end method

.method public endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "uri"
    .parameter "localName"
    .parameter "qName"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ZipFileHelper$DescriptionSAXHandler;->nvp:Ljava/util/HashMap;

    iget-object v1, p0, Lmiui/app/resourcebrowser/util/ZipFileHelper$DescriptionSAXHandler;->value:Ljava/lang/String;

    invoke-virtual {v0, p2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public getElementEntries()Ljava/util/HashMap;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ZipFileHelper$DescriptionSAXHandler;->nvp:Ljava/util/HashMap;

    return-object v0
.end method
