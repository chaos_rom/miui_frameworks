.class public Lmiui/app/resourcebrowser/util/CleanCachePolicy$LRUCleanCacheByAccessTime;
.super Lmiui/app/resourcebrowser/util/CleanCachePolicy;
.source "CleanCachePolicy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/app/resourcebrowser/util/CleanCachePolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LRUCleanCacheByAccessTime"
.end annotation


# instance fields
.field mMaxDeleteDirNum:I

.field mMaxDeleteFileNum:I

.field mRemainNumber:I


# direct methods
.method public constructor <init>(I)V
    .registers 3
    .parameter "remainNumber"

    .prologue
    invoke-direct {p0}, Lmiui/app/resourcebrowser/util/CleanCachePolicy;-><init>()V

    const/16 v0, 0x1e

    iput v0, p0, Lmiui/app/resourcebrowser/util/CleanCachePolicy$LRUCleanCacheByAccessTime;->mMaxDeleteDirNum:I

    const/16 v0, 0x64

    iput v0, p0, Lmiui/app/resourcebrowser/util/CleanCachePolicy$LRUCleanCacheByAccessTime;->mMaxDeleteFileNum:I

    iput p1, p0, Lmiui/app/resourcebrowser/util/CleanCachePolicy$LRUCleanCacheByAccessTime;->mRemainNumber:I

    return-void
.end method


# virtual methods
.method protected onCleanCacheFiles(Ljava/util/List;)I
    .registers 9
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lmiui/app/resourcebrowser/util/CleanCachePolicy$FileEntry;",
            ">;)I"
        }
    .end annotation

    .prologue
    .local p1, entryList:Ljava/util/List;,"Ljava/util/List<Lmiui/app/resourcebrowser/util/CleanCachePolicy$FileEntry;>;"
    iget v5, p0, Lmiui/app/resourcebrowser/util/CleanCachePolicy$LRUCleanCacheByAccessTime;->mRemainNumber:I

    if-lez v5, :cond_14

    iget v5, p0, Lmiui/app/resourcebrowser/util/CleanCachePolicy$LRUCleanCacheByAccessTime;->mRemainNumber:I

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    if-ge v5, v6, :cond_14

    new-instance v5, Lmiui/app/resourcebrowser/util/CleanCachePolicy$LRUCleanCacheByAccessTime$1;

    invoke-direct {v5, p0}, Lmiui/app/resourcebrowser/util/CleanCachePolicy$LRUCleanCacheByAccessTime$1;-><init>(Lmiui/app/resourcebrowser/util/CleanCachePolicy$LRUCleanCacheByAccessTime;)V

    invoke-static {p1, v5}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    :cond_14
    const/4 v0, 0x0

    .local v0, deleteDirCnt:I
    const/4 v1, 0x0

    .local v1, deleteFileCnt:I
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    iget v6, p0, Lmiui/app/resourcebrowser/util/CleanCachePolicy$LRUCleanCacheByAccessTime;->mRemainNumber:I

    sub-int v2, v5, v6

    .local v2, end:I
    const/4 v4, 0x0

    .local v4, i:I
    :goto_1f
    if-ge v4, v2, :cond_41

    new-instance v3, Ljava/io/File;

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lmiui/app/resourcebrowser/util/CleanCachePolicy$FileEntry;

    iget-object v5, v5, Lmiui/app/resourcebrowser/util/CleanCachePolicy$FileEntry;->filePath:Ljava/lang/String;

    invoke-direct {v3, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .local v3, f:Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_44

    add-int/lit8 v0, v0, 0x1

    :goto_36
    invoke-static {v3}, Lmiui/app/resourcebrowser/util/CleanCachePolicy$LRUCleanCacheByAccessTime;->deleteFile(Ljava/io/File;)Z

    iget v5, p0, Lmiui/app/resourcebrowser/util/CleanCachePolicy$LRUCleanCacheByAccessTime;->mMaxDeleteDirNum:I

    if-ge v0, v5, :cond_41

    iget v5, p0, Lmiui/app/resourcebrowser/util/CleanCachePolicy$LRUCleanCacheByAccessTime;->mMaxDeleteFileNum:I

    if-lt v1, v5, :cond_47

    .end local v3           #f:Ljava/io/File;
    :cond_41
    add-int v5, v0, v1

    return v5

    .restart local v3       #f:Ljava/io/File;
    :cond_44
    add-int/lit8 v1, v1, 0x1

    goto :goto_36

    :cond_47
    add-int/lit8 v4, v4, 0x1

    goto :goto_1f
.end method

.method public setMaxDeleteDirNumber(I)V
    .registers 2
    .parameter "max"

    .prologue
    iput p1, p0, Lmiui/app/resourcebrowser/util/CleanCachePolicy$LRUCleanCacheByAccessTime;->mMaxDeleteDirNum:I

    return-void
.end method

.method public setMaxDeleteFileNumber(I)V
    .registers 2
    .parameter "max"

    .prologue
    iput p1, p0, Lmiui/app/resourcebrowser/util/CleanCachePolicy$LRUCleanCacheByAccessTime;->mMaxDeleteFileNum:I

    return-void
.end method
