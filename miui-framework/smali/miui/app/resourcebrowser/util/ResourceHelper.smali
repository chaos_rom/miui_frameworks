.class public Lmiui/app/resourcebrowser/util/ResourceHelper;
.super Ljava/lang/Object;
.source "ResourceHelper.java"

# interfaces
.implements Lmiui/app/resourcebrowser/IntentConstants;


# static fields
.field private static mCacheFileNameMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static mFolderInfoCache:Lmiui/cache/FolderCache;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    new-instance v0, Lmiui/cache/FolderCache;

    invoke-direct {v0}, Lmiui/cache/FolderCache;-><init>()V

    sput-object v0, Lmiui/app/resourcebrowser/util/ResourceHelper;->mFolderInfoCache:Lmiui/cache/FolderCache;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lmiui/app/resourcebrowser/util/ResourceHelper;->mCacheFileNameMap:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static buildDefaultMetaData(Landroid/os/Bundle;Ljava/lang/String;Landroid/content/Context;)Landroid/os/Bundle;
    .registers 14
    .parameter "metaData"
    .parameter "action"
    .parameter "context"

    .prologue
    const/4 v10, 0x7

    const/4 v9, -0x1

    const/4 v6, 0x1

    const/4 v7, 0x0

    const-string v5, "android.intent.action.RINGTONE_PICKER"

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_25d

    const-string v5, "miui.app.resourcebrowser.DISPLAY_TYPE"

    const/4 v8, 0x5

    invoke-virtual {p0, v5, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v5, "miui.app.resourcebrowser.CATEGORY_SUPPORTED"

    invoke-virtual {p0, v5, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v5, "miui.app.resourcebrowser.RESOURCE_SET_NAME"

    const-string v8, "android.intent.extra.ringtone.TITLE"

    invoke-virtual {p0, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v5, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "android.intent.extra.ringtone.EXISTING_URI"

    invoke-virtual {p0, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    .local v1, currentUri:Landroid/net/Uri;
    const-string v5, "android.intent.extra.ringtone.DEFAULT_URI"

    invoke-virtual {p0, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    .local v2, defaultUri:Landroid/net/Uri;
    if-eqz v1, :cond_44

    const-string v8, "miui.app.resourcebrowser.CURRENT_USING_PATH"

    invoke-virtual {v1, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_216

    move v5, v6

    :goto_3d
    invoke-static {p2, v1, v5}, Lmiui/app/resourcebrowser/util/ResourceHelper;->getPathByUri(Landroid/content/Context;Landroid/net/Uri;Z)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v8, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_44
    const-string v5, "miui.app.resourcebrowser.SOURCE_FOLDERS"

    invoke-virtual {p0, v5}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_67

    new-instance v3, Ljava/util/LinkedHashSet;

    invoke-direct {v3}, Ljava/util/LinkedHashSet;-><init>()V

    .local v3, sourceFolders:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    const-string v5, "android.intent.extra.ringtone.TYPE"

    invoke-virtual {p0, v5, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    packed-switch v5, :pswitch_data_346

    :goto_5a
    :pswitch_5a
    const-string v8, "miui.app.resourcebrowser.SOURCE_FOLDERS"

    new-array v5, v7, [Ljava/lang/String;

    invoke-interface {v3, v5}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/String;

    invoke-virtual {p0, v8, v5}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .end local v1           #currentUri:Landroid/net/Uri;
    .end local v2           #defaultUri:Landroid/net/Uri;
    .end local v3           #sourceFolders:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    :cond_67
    :goto_67
    const-string v5, "miui.app.resourcebrowser.DISPLAY_TYPE"

    invoke-virtual {p0, v5, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    if-ne v5, v9, :cond_74

    const-string v5, "miui.app.resourcebrowser.DISPLAY_TYPE"

    invoke-virtual {p0, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_74
    const-string v5, "miui.app.resourcebrowser.RESOURCE_SET_PACKAGE"

    invoke-virtual {p0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_85

    const-string v5, "miui.app.resourcebrowser.RESOURCE_SET_PACKAGE"

    invoke-virtual {p2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v5, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_85
    const-string v5, "miui.app.resourcebrowser.RESOURCE_SET_NAME"

    invoke-virtual {p0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_99

    const-string v5, "miui.app.resourcebrowser.RESOURCE_SET_NAME"

    const v8, 0x60c0028

    invoke-virtual {p2, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v5, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_99
    const-string v5, "miui.app.resourcebrowser.RESOURCE_SET_CATEGORY"

    invoke-virtual {p0, v5, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    if-ne v5, v9, :cond_a6

    const-string v5, "miui.app.resourcebrowser.RESOURCE_SET_CATEGORY"

    invoke-virtual {p0, v5, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_a6
    const-string v5, "miui.app.resourcebrowser.RESOURCE_SET_CODE"

    invoke-virtual {p0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_b7

    const-string v5, "miui.app.resourcebrowser.RESOURCE_SET_CATEGORY"

    invoke-virtual {p0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    packed-switch v5, :pswitch_data_358

    :cond_b7
    :goto_b7
    const-string v5, "miui.app.resourcebrowser.PREVIEW_PREFIX"

    invoke-virtual {p0, v5}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_e7

    const-string v5, "miui.app.resourcebrowser.PREVIEW_PREFIX"

    new-array v8, v6, [Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "preview_"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "miui.app.resourcebrowser.RESOURCE_SET_CODE"

    invoke-virtual {p0, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "_"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v7

    invoke-virtual {p0, v5, v8}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    :cond_e7
    const-string v5, "miui.app.resourcebrowser.PREVIEW_PREFIX_INDICATOR"

    invoke-virtual {p0, v5}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_fa

    const-string v5, "miui.app.resourcebrowser.PREVIEW_PREFIX_INDICATOR"

    const-string v8, "miui.app.resourcebrowser.PREVIEW_PREFIX"

    invoke-virtual {p0, v8}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v5, v8}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    :cond_fa
    const-string v5, "miui.app.resourcebrowser.THUMBNAIL_PREFIX"

    invoke-virtual {p0, v5}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_10d

    const-string v5, "miui.app.resourcebrowser.THUMBNAIL_PREFIX"

    const-string v8, "miui.app.resourcebrowser.PREVIEW_PREFIX"

    invoke-virtual {p0, v8}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v5, v8}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    :cond_10d
    const-string v5, "miui.app.resourcebrowser.THUMBNAIL_PREFIX_INDICATOR"

    invoke-virtual {p0, v5}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_120

    const-string v5, "miui.app.resourcebrowser.THUMBNAIL_PREFIX_INDICATOR"

    const-string v8, "miui.app.resourcebrowser.THUMBNAIL_PREFIX"

    invoke-virtual {p0, v8}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v5, v8}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    :cond_120
    const-string v5, "miui.app.resourcebrowser.SOURCE_FOLDERS"

    invoke-virtual {p0, v5}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_14a

    const-string v5, "miui.app.resourcebrowser.SOURCE_FOLDERS"

    new-array v6, v6, [Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lmiui/app/resourcebrowser/ResourceConstants;->MIUI_PATH:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "miui.app.resourcebrowser.RESOURCE_SET_CODE"

    invoke-virtual {p0, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {p0, v5, v6}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    :cond_14a
    const-string v5, "miui.app.resourcebrowser.DOWNLOAD_FOLDER"

    invoke-virtual {p0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_170

    const-string v5, "miui.app.resourcebrowser.DOWNLOAD_FOLDER"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lmiui/app/resourcebrowser/ResourceConstants;->MIUI_PATH:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "miui.app.resourcebrowser.RESOURCE_SET_CODE"

    invoke-virtual {p0, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_170
    const-string v5, "miui.app.resourcebrowser.CACHE_LIST_FOLDER"

    invoke-virtual {p0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_18d

    sget-object v0, Lmiui/app/resourcebrowser/ResourceConstants;->LIST_PATH:Ljava/lang/String;

    .local v0, cachePath:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v5

    if-eqz v5, :cond_188

    invoke-virtual {p2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    :cond_188
    const-string v5, "miui.app.resourcebrowser.CACHE_LIST_FOLDER"

    invoke-virtual {p0, v5, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .end local v0           #cachePath:Ljava/lang/String;
    :cond_18d
    const-string v5, "miui.app.resourcebrowser.RESOURCE_TYPE_PARAMETER"

    invoke-virtual {p0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .local v4, typeParam:Ljava/lang/String;
    if-nez v4, :cond_19e

    const-string v5, "miui.app.resourcebrowser.RESOURCE_SET_CATEGORY"

    invoke-virtual {p0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    packed-switch v5, :pswitch_data_362

    :cond_19e
    :goto_19e
    const-string v5, "miui.app.resourcebrowser.RESOURCE_FILE_EXTENSION"

    invoke-virtual {p0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_1af

    const-string v5, "miui.app.resourcebrowser.RESOURCE_SET_CATEGORY"

    invoke-virtual {p0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    packed-switch v5, :pswitch_data_36c

    :cond_1af
    :goto_1af
    const-string v5, "miui.app.resourcebrowser.LOCAL_LIST_ACTIVITY_CLASS"

    invoke-virtual {p0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_30c

    const-string v5, "miui.app.resourcebrowser.LOCAL_LIST_ACTIVITY_CLASS"

    const-class v6, Lmiui/app/resourcebrowser/LocalResourceListFragment;

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "miui.app.resourcebrowser.LOCAL_LIST_ACTIVITY_PACKAGE"

    invoke-virtual {p0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_1d1

    const-string v5, "miui.app.resourcebrowser.LOCAL_LIST_ACTIVITY_PACKAGE"

    const-string v6, "android"

    invoke-virtual {p0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1d1
    :goto_1d1
    const-string v5, "miui.app.resourcebrowser.ONLINE_LIST_ACTIVITY_CLASS"

    invoke-virtual {p0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_31f

    const-string v5, "miui.app.resourcebrowser.ONLINE_LIST_ACTIVITY_CLASS"

    const-class v6, Lmiui/app/resourcebrowser/OnlineResourceListFragment;

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "miui.app.resourcebrowser.ONLINE_LIST_ACTIVITY_PACKAGE"

    invoke-virtual {p0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_1f3

    const-string v5, "miui.app.resourcebrowser.ONLINE_LIST_ACTIVITY_PACKAGE"

    const-string v6, "android"

    invoke-virtual {p0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1f3
    :goto_1f3
    const-string v5, "miui.app.resourcebrowser.DETAIL_ACTIVITY_CLASS"

    invoke-virtual {p0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_332

    const-string v5, "miui.app.resourcebrowser.DETAIL_ACTIVITY_CLASS"

    const-class v6, Lmiui/app/resourcebrowser/ResourceDetailActivity;

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "miui.app.resourcebrowser.DETAIL_ACTIVITY_PACKAGE"

    invoke-virtual {p0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_215

    const-string v5, "miui.app.resourcebrowser.DETAIL_ACTIVITY_PACKAGE"

    const-string v6, "android"

    invoke-virtual {p0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_215
    :goto_215
    return-object p0

    .end local v4           #typeParam:Ljava/lang/String;
    .restart local v1       #currentUri:Landroid/net/Uri;
    .restart local v2       #defaultUri:Landroid/net/Uri;
    :cond_216
    move v5, v7

    goto/16 :goto_3d

    .restart local v3       #sourceFolders:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    :pswitch_219
    sget-object v5, Lmiui/app/resourcebrowser/ResourceConstants;->DOWNLOADED_RINGTONE_PATH:Ljava/lang/String;

    invoke-interface {v3, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v5, "/system/media/audio/ringtones"

    invoke-interface {v3, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_5a

    :pswitch_225
    sget-object v5, Lmiui/app/resourcebrowser/ResourceConstants;->DOWNLOADED_NOTIFICATION_PATH:Ljava/lang/String;

    invoke-interface {v3, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v5, "/system/media/audio/notifications"

    invoke-interface {v3, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_5a

    :pswitch_231
    sget-object v5, Lmiui/app/resourcebrowser/ResourceConstants;->DOWNLOADED_ALARM_PATH:Ljava/lang/String;

    invoke-interface {v3, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v5, "/system/media/audio/alarms"

    invoke-interface {v3, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_5a

    :pswitch_23d
    sget-object v5, Lmiui/app/resourcebrowser/ResourceConstants;->DOWNLOADED_RINGTONE_PATH:Ljava/lang/String;

    invoke-interface {v3, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v5, Lmiui/app/resourcebrowser/ResourceConstants;->DOWNLOADED_NOTIFICATION_PATH:Ljava/lang/String;

    invoke-interface {v3, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v5, Lmiui/app/resourcebrowser/ResourceConstants;->DOWNLOADED_ALARM_PATH:Ljava/lang/String;

    invoke-interface {v3, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v5, "/system/media/audio/ringtones"

    invoke-interface {v3, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v5, "/system/media/audio/notifications"

    invoke-interface {v3, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v5, "/system/media/audio/alarms"

    invoke-interface {v3, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_5a

    .end local v1           #currentUri:Landroid/net/Uri;
    .end local v2           #defaultUri:Landroid/net/Uri;
    .end local v3           #sourceFolders:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    :cond_25d
    const-string v5, "android.intent.action.SET_WALLPAPER"

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_277

    const-string v5, "miui.app.resourcebrowser.USING_PICKER"

    invoke-virtual {p0, v5, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v5, "miui.app.resourcebrowser.DISPLAY_TYPE"

    const/4 v8, 0x6

    invoke-virtual {p0, v5, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v5, "miui.app.resourcebrowser.CATEGORY_SUPPORTED"

    invoke-virtual {p0, v5, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_67

    :cond_277
    const-string v5, "android.intent.action.SET_LOCKSCREEN_WALLPAPER"

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_290

    const-string v5, "miui.app.resourcebrowser.USING_PICKER"

    invoke-virtual {p0, v5, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v5, "miui.app.resourcebrowser.DISPLAY_TYPE"

    invoke-virtual {p0, v5, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v5, "miui.app.resourcebrowser.CATEGORY_SUPPORTED"

    invoke-virtual {p0, v5, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_67

    :cond_290
    const-string v5, "android.intent.action.PICK_RESOURCE"

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_29f

    const-string v5, "miui.app.resourcebrowser.USING_PICKER"

    invoke-virtual {p0, v5, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_67

    :cond_29f
    const-string v5, "miui.app.resourcebrowser.USING_PICKER"

    invoke-virtual {p0, v5, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_67

    :pswitch_2a6
    const-string v5, "miui.app.resourcebrowser.RESOURCE_SET_CODE"

    const-string v8, "theme"

    invoke-virtual {p0, v5, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_b7

    :pswitch_2af
    const-string v5, "miui.app.resourcebrowser.RESOURCE_SET_CODE"

    const-string v8, "wallpaper"

    invoke-virtual {p0, v5, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_b7

    :pswitch_2b8
    const-string v5, "miui.app.resourcebrowser.RESOURCE_SET_CODE"

    const-string v8, "ringtone"

    invoke-virtual {p0, v5, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_b7

    .restart local v4       #typeParam:Ljava/lang/String;
    :pswitch_2c1
    const-string v5, "miui.app.resourcebrowser.RESOURCE_TYPE_PARAMETER"

    const-string v6, "compound"

    invoke-virtual {p0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "miui.app.resourcebrowser.RESOURCE_FILE_EXTENSION"

    const-string v6, ".mtz"

    invoke-virtual {p0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_19e

    :pswitch_2d1
    const-string v5, "miui.app.resourcebrowser.RESOURCE_TYPE_PARAMETER"

    const-string v6, "wallpaper"

    invoke-virtual {p0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "miui.app.resourcebrowser.RESOURCE_FILE_EXTENSION"

    const-string v6, ".jpg"

    invoke-virtual {p0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_19e

    :pswitch_2e1
    const-string v5, "miui.app.resourcebrowser.RESOURCE_TYPE_PARAMETER"

    const-string v6, "ringtone"

    invoke-virtual {p0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "miui.app.resourcebrowser.RESOURCE_FILE_EXTENSION"

    const-string v6, ".mp3"

    invoke-virtual {p0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_19e

    :pswitch_2f1
    const-string v5, "miui.app.resourcebrowser.RESOURCE_FILE_EXTENSION"

    const-string v6, ".mtz"

    invoke-virtual {p0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1af

    :pswitch_2fa
    const-string v5, "miui.app.resourcebrowser.RESOURCE_FILE_EXTENSION"

    const-string v6, ".jpg"

    invoke-virtual {p0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1af

    :pswitch_303
    const-string v5, "miui.app.resourcebrowser.RESOURCE_FILE_EXTENSION"

    const-string v6, ".mp3"

    invoke-virtual {p0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1af

    :cond_30c
    const-string v5, "miui.app.resourcebrowser.LOCAL_LIST_ACTIVITY_PACKAGE"

    invoke-virtual {p0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_1d1

    const-string v5, "miui.app.resourcebrowser.LOCAL_LIST_ACTIVITY_PACKAGE"

    invoke-virtual {p2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1d1

    :cond_31f
    const-string v5, "miui.app.resourcebrowser.ONLINE_LIST_ACTIVITY_PACKAGE"

    invoke-virtual {p0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_1f3

    const-string v5, "miui.app.resourcebrowser.ONLINE_LIST_ACTIVITY_PACKAGE"

    invoke-virtual {p2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1f3

    :cond_332
    const-string v5, "miui.app.resourcebrowser.DETAIL_ACTIVITY_PACKAGE"

    invoke-virtual {p0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_215

    const-string v5, "miui.app.resourcebrowser.DETAIL_ACTIVITY_PACKAGE"

    invoke-virtual {p2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_215

    nop

    :pswitch_data_346
    .packed-switch 0x1
        :pswitch_219
        :pswitch_225
        :pswitch_5a
        :pswitch_231
        :pswitch_5a
        :pswitch_5a
        :pswitch_23d
    .end packed-switch

    :pswitch_data_358
    .packed-switch 0x0
        :pswitch_2a6
        :pswitch_2af
        :pswitch_2b8
    .end packed-switch

    :pswitch_data_362
    .packed-switch 0x0
        :pswitch_2c1
        :pswitch_2d1
        :pswitch_2e1
    .end packed-switch

    :pswitch_data_36c
    .packed-switch 0x0
        :pswitch_2f1
        :pswitch_2fa
        :pswitch_303
    .end packed-switch
.end method

.method public static copyIntData(Landroid/os/Bundle;Landroid/os/Bundle;Ljava/lang/String;)V
    .registers 4
    .parameter "source"
    .parameter "target"
    .parameter "key"

    .prologue
    invoke-virtual {p0, p2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, p2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method public static copyLongData(Landroid/os/Bundle;Landroid/os/Bundle;Ljava/lang/String;)V
    .registers 5
    .parameter "source"
    .parameter "target"
    .parameter "key"

    .prologue
    invoke-virtual {p0, p2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-virtual {p1, p2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    return-void
.end method

.method public static copyResourceInformation(Lmiui/app/resourcebrowser/resource/Resource;Lmiui/app/resourcebrowser/resource/Resource;)V
    .registers 5
    .parameter "source"
    .parameter "target"

    .prologue
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/resource/Resource;->getInformation()Landroid/os/Bundle;

    move-result-object v0

    .local v0, sourceInfo:Landroid/os/Bundle;
    invoke-virtual {p1}, Lmiui/app/resourcebrowser/resource/Resource;->getInformation()Landroid/os/Bundle;

    move-result-object v1

    .local v1, targetInfo:Landroid/os/Bundle;
    const-string v2, "ONLINE_PATH"

    invoke-static {v0, v1, v2}, Lmiui/app/resourcebrowser/util/ResourceHelper;->copyStringData(Landroid/os/Bundle;Landroid/os/Bundle;Ljava/lang/String;)V

    const-string v2, "VERSION"

    invoke-static {v0, v1, v2}, Lmiui/app/resourcebrowser/util/ResourceHelper;->copyStringData(Landroid/os/Bundle;Landroid/os/Bundle;Ljava/lang/String;)V

    const-string v2, "PLATFORM_VERSION"

    invoke-static {v0, v1, v2}, Lmiui/app/resourcebrowser/util/ResourceHelper;->copyIntData(Landroid/os/Bundle;Landroid/os/Bundle;Ljava/lang/String;)V

    const-string v2, "MODIFIED_TIME"

    invoke-static {v0, v1, v2}, Lmiui/app/resourcebrowser/util/ResourceHelper;->copyLongData(Landroid/os/Bundle;Landroid/os/Bundle;Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Lmiui/app/resourcebrowser/resource/Resource;->setInformation(Landroid/os/Bundle;)V

    return-void
.end method

.method public static copyStringData(Landroid/os/Bundle;Landroid/os/Bundle;Ljava/lang/String;)V
    .registers 4
    .parameter "source"
    .parameter "target"
    .parameter "key"

    .prologue
    invoke-virtual {p0, p2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, p2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static exit(Landroid/app/Activity;)V
    .registers 5
    .parameter "activity"

    .prologue
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x1010355

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x60c0003

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x60c0004

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x104000a

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .local v0, dialog:Landroid/app/AlertDialog;
    new-instance v1, Lmiui/app/resourcebrowser/util/ResourceHelper$1;

    invoke-direct {v1, p0}, Lmiui/app/resourcebrowser/util/ResourceHelper$1;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method public static getAndInsertMediaEntryByPath(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;
    .registers 6
    .parameter "context"
    .parameter "path"

    .prologue
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8

    const/4 v0, 0x0

    :cond_7
    :goto_7
    return-object v0

    :cond_8
    invoke-static {p0, p1}, Lmiui/app/resourcebrowser/util/ResourceHelper;->getMediaEntryByPath(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .local v0, ret:Landroid/net/Uri;
    if-nez v0, :cond_7

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .local v1, values:Landroid/content/ContentValues;
    const-string v2, "_data"

    invoke-virtual {v1, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "is_music"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {p1}, Landroid/provider/MediaStore$Audio$Media;->getContentUriForPath(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_7
.end method

.method public static getContentColumnValue(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;
    .registers 12
    .parameter "context"
    .parameter "uri"
    .parameter "column"

    .prologue
    const/4 v8, 0x0

    .local v8, value:Ljava/lang/String;
    :try_start_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p2, v2, v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .local v6, cursor:Landroid/database/Cursor;
    if-eqz v6, :cond_23

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_20

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    :cond_20
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_23
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_23} :catch_26

    .end local v6           #cursor:Landroid/database/Cursor;
    :cond_23
    :goto_23
    if-eqz v8, :cond_2b

    .end local v8           #value:Ljava/lang/String;
    :goto_25
    return-object v8

    .restart local v8       #value:Ljava/lang/String;
    :catch_26
    move-exception v7

    .local v7, e:Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_23

    .end local v7           #e:Ljava/lang/Exception;
    :cond_2b
    const-string v8, ""

    goto :goto_25
.end method

.method public static getDataPerLine(I)I
    .registers 2
    .parameter "displayType"

    .prologue
    const/4 v0, 0x1

    .local v0, dataPerLine:I
    packed-switch p0, :pswitch_data_c

    :goto_4
    return v0

    :pswitch_5
    const/4 v0, 0x1

    goto :goto_4

    :pswitch_7
    const/4 v0, 0x2

    goto :goto_4

    :pswitch_9
    const/4 v0, 0x3

    goto :goto_4

    nop

    :pswitch_data_c
    .packed-switch 0x1
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_7
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_7
        :pswitch_7
    .end packed-switch
.end method

.method public static getDefaultFormatPlayingRingtoneName(Ljava/lang/String;II)Ljava/lang/String;
    .registers 9
    .parameter "ringtonePath"
    .parameter "current"
    .parameter "total"

    .prologue
    const/16 v2, 0x2e

    invoke-virtual {p0, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .local v0, dotIndex:I
    if-gez v0, :cond_c

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    :cond_c
    sget-char v2, Ljava/io/File;->separatorChar:C

    invoke-virtual {p0, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p0, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .local v1, name:Ljava/lang/String;
    const-string v2, "%s (%d/%d)"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    const/4 v4, 0x1

    add-int/lit8 v5, p1, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static getDefaultMusicPlayList(Landroid/content/Context;Lmiui/app/resourcebrowser/resource/Resource;)Ljava/util/List;
    .registers 8
    .parameter "context"
    .parameter "resourceItem"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lmiui/app/resourcebrowser/resource/Resource;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .local v3, ret:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p1}, Lmiui/app/resourcebrowser/resource/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v0

    .local v0, localPath:Ljava/lang/String;
    invoke-static {v0}, Lmiui/app/resourcebrowser/util/ResourceHelper;->getUriByPath(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .local v4, uri:Landroid/net/Uri;
    invoke-static {p0, v4}, Lmiui/app/resourcebrowser/util/ResourceHelper;->getPathByUri(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    .local v2, path:Ljava/lang/String;
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_20

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1f
    :goto_1f
    return-object v3

    :cond_20
    const/4 v5, 0x0

    invoke-virtual {p1, v5}, Lmiui/app/resourcebrowser/resource/Resource;->getOnlineThumbnail(I)Ljava/lang/String;

    move-result-object v1

    .local v1, onlinePath:Ljava/lang/String;
    if-eqz v1, :cond_1f

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1f
.end method

.method public static getFileHash(Ljava/lang/String;)Ljava/lang/String;
    .registers 10
    .parameter "path"

    .prologue
    const/4 v5, 0x0

    .local v5, hash:Ljava/lang/String;
    const/4 v6, 0x0

    .local v6, in:Ljava/io/BufferedInputStream;
    :try_start_2
    new-instance v7, Ljava/io/BufferedInputStream;

    new-instance v8, Ljava/io/FileInputStream;

    invoke-direct {v8, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v7, v8}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_c
    .catchall {:try_start_2 .. :try_end_c} :catchall_64
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_c} :catch_79
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_c} :catch_46
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_c} :catch_55

    .end local v6           #in:Ljava/io/BufferedInputStream;
    .local v7, in:Ljava/io/BufferedInputStream;
    :try_start_c
    const-string v8, "SHA1"

    invoke-static {v8}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v3

    .local v3, digester:Ljava/security/MessageDigest;
    const/16 v8, 0x2000

    new-array v1, v8, [B

    .local v1, bytes:[B
    :goto_16
    invoke-virtual {v7, v1}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v0

    .local v0, byteCount:I
    if-lez v0, :cond_2c

    const/4 v8, 0x0

    invoke-virtual {v3, v1, v8, v0}, Ljava/security/MessageDigest;->update([BII)V
    :try_end_20
    .catchall {:try_start_c .. :try_end_20} :catchall_70
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_c .. :try_end_20} :catch_21
    .catch Ljava/io/FileNotFoundException; {:try_start_c .. :try_end_20} :catch_76
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_20} :catch_73

    goto :goto_16

    .end local v0           #byteCount:I
    .end local v1           #bytes:[B
    .end local v3           #digester:Ljava/security/MessageDigest;
    :catch_21
    move-exception v4

    move-object v6, v7

    .end local v7           #in:Ljava/io/BufferedInputStream;
    .local v4, e:Ljava/security/NoSuchAlgorithmException;
    .restart local v6       #in:Ljava/io/BufferedInputStream;
    :goto_23
    :try_start_23
    invoke-virtual {v4}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V
    :try_end_26
    .catchall {:try_start_23 .. :try_end_26} :catchall_64

    if-eqz v6, :cond_2b

    :try_start_28
    invoke-virtual {v6}, Ljava/io/BufferedInputStream;->close()V
    :try_end_2b
    .catch Ljava/io/IOException; {:try_start_28 .. :try_end_2b} :catch_41

    .end local v4           #e:Ljava/security/NoSuchAlgorithmException;
    :cond_2b
    :goto_2b
    return-object v5

    .end local v6           #in:Ljava/io/BufferedInputStream;
    .restart local v0       #byteCount:I
    .restart local v1       #bytes:[B
    .restart local v3       #digester:Ljava/security/MessageDigest;
    .restart local v7       #in:Ljava/io/BufferedInputStream;
    :cond_2c
    :try_start_2c
    invoke-virtual {v3}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v2

    .local v2, digest:[B
    invoke-static {v2}, Lmiui/app/resourcebrowser/util/ResourceHelper;->toHexString([B)Ljava/lang/String;
    :try_end_33
    .catchall {:try_start_2c .. :try_end_33} :catchall_70
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2c .. :try_end_33} :catch_21
    .catch Ljava/io/FileNotFoundException; {:try_start_2c .. :try_end_33} :catch_76
    .catch Ljava/io/IOException; {:try_start_2c .. :try_end_33} :catch_73

    move-result-object v5

    if-eqz v7, :cond_7b

    :try_start_36
    invoke-virtual {v7}, Ljava/io/BufferedInputStream;->close()V
    :try_end_39
    .catch Ljava/io/IOException; {:try_start_36 .. :try_end_39} :catch_3b

    move-object v6, v7

    .end local v7           #in:Ljava/io/BufferedInputStream;
    .restart local v6       #in:Ljava/io/BufferedInputStream;
    goto :goto_2b

    .end local v6           #in:Ljava/io/BufferedInputStream;
    .restart local v7       #in:Ljava/io/BufferedInputStream;
    :catch_3b
    move-exception v4

    .local v4, e:Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    move-object v6, v7

    .end local v7           #in:Ljava/io/BufferedInputStream;
    .restart local v6       #in:Ljava/io/BufferedInputStream;
    goto :goto_2b

    .end local v0           #byteCount:I
    .end local v1           #bytes:[B
    .end local v2           #digest:[B
    .end local v3           #digester:Ljava/security/MessageDigest;
    .local v4, e:Ljava/security/NoSuchAlgorithmException;
    :catch_41
    move-exception v4

    .local v4, e:Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2b

    .end local v4           #e:Ljava/io/IOException;
    :catch_46
    move-exception v4

    .local v4, e:Ljava/io/FileNotFoundException;
    :goto_47
    :try_start_47
    invoke-virtual {v4}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_4a
    .catchall {:try_start_47 .. :try_end_4a} :catchall_64

    if-eqz v6, :cond_2b

    :try_start_4c
    invoke-virtual {v6}, Ljava/io/BufferedInputStream;->close()V
    :try_end_4f
    .catch Ljava/io/IOException; {:try_start_4c .. :try_end_4f} :catch_50

    goto :goto_2b

    :catch_50
    move-exception v4

    .local v4, e:Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2b

    .end local v4           #e:Ljava/io/IOException;
    :catch_55
    move-exception v4

    .restart local v4       #e:Ljava/io/IOException;
    :goto_56
    :try_start_56
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V
    :try_end_59
    .catchall {:try_start_56 .. :try_end_59} :catchall_64

    if-eqz v6, :cond_2b

    :try_start_5b
    invoke-virtual {v6}, Ljava/io/BufferedInputStream;->close()V
    :try_end_5e
    .catch Ljava/io/IOException; {:try_start_5b .. :try_end_5e} :catch_5f

    goto :goto_2b

    :catch_5f
    move-exception v4

    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2b

    .end local v4           #e:Ljava/io/IOException;
    :catchall_64
    move-exception v8

    :goto_65
    if-eqz v6, :cond_6a

    :try_start_67
    invoke-virtual {v6}, Ljava/io/BufferedInputStream;->close()V
    :try_end_6a
    .catch Ljava/io/IOException; {:try_start_67 .. :try_end_6a} :catch_6b

    :cond_6a
    :goto_6a
    throw v8

    :catch_6b
    move-exception v4

    .restart local v4       #e:Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6a

    .end local v4           #e:Ljava/io/IOException;
    .end local v6           #in:Ljava/io/BufferedInputStream;
    .restart local v7       #in:Ljava/io/BufferedInputStream;
    :catchall_70
    move-exception v8

    move-object v6, v7

    .end local v7           #in:Ljava/io/BufferedInputStream;
    .restart local v6       #in:Ljava/io/BufferedInputStream;
    goto :goto_65

    .end local v6           #in:Ljava/io/BufferedInputStream;
    .restart local v7       #in:Ljava/io/BufferedInputStream;
    :catch_73
    move-exception v4

    move-object v6, v7

    .end local v7           #in:Ljava/io/BufferedInputStream;
    .restart local v6       #in:Ljava/io/BufferedInputStream;
    goto :goto_56

    .end local v6           #in:Ljava/io/BufferedInputStream;
    .restart local v7       #in:Ljava/io/BufferedInputStream;
    :catch_76
    move-exception v4

    move-object v6, v7

    .end local v7           #in:Ljava/io/BufferedInputStream;
    .restart local v6       #in:Ljava/io/BufferedInputStream;
    goto :goto_47

    :catch_79
    move-exception v4

    goto :goto_23

    .end local v6           #in:Ljava/io/BufferedInputStream;
    .restart local v0       #byteCount:I
    .restart local v1       #bytes:[B
    .restart local v2       #digest:[B
    .restart local v3       #digester:Ljava/security/MessageDigest;
    .restart local v7       #in:Ljava/io/BufferedInputStream;
    :cond_7b
    move-object v6, v7

    .end local v7           #in:Ljava/io/BufferedInputStream;
    .restart local v6       #in:Ljava/io/BufferedInputStream;
    goto :goto_2b
.end method

.method public static getFileName(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "onlinePath"

    .prologue
    const-string v1, "/"

    invoke-virtual {p0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .local v0, pieces:[Ljava/lang/String;
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-object v1, v0, v1

    return-object v1
.end method

.method public static getFilePathByURL(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 7
    .parameter "directory"
    .parameter "url"

    .prologue
    const/4 v4, -0x1

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/16 v3, 0x1ff

    invoke-static {v2, v3, v4, v4}, Lmiui/os/ExtraFileUtils;->mkdirs(Ljava/io/File;III)Z

    sget-object v2, Lmiui/app/resourcebrowser/util/ResourceHelper;->mCacheFileNameMap:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .local v0, cacheFileName:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5a

    const-string v2, "/"

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .local v1, pieces:[Ljava/lang/String;
    array-length v2, v1

    add-int/lit8 v2, v2, -0x1

    aget-object v0, v1, v2

    :goto_24
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0x80

    if-le v2, v3, :cond_4d

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_24

    :cond_4d
    const-string v2, "\\?"

    const-string v3, "_"

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lmiui/app/resourcebrowser/util/ResourceHelper;->mCacheFileNameMap:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .end local v1           #pieces:[Ljava/lang/String;
    :cond_5a
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Lmiui/os/ExtraFileUtils;->standardizeFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static getFolderInfoCache(Ljava/lang/String;)Lmiui/cache/FolderCache$FolderInfo;
    .registers 2
    .parameter "directory"

    .prologue
    sget-object v0, Lmiui/app/resourcebrowser/util/ResourceHelper;->mFolderInfoCache:Lmiui/cache/FolderCache;

    invoke-virtual {v0, p0}, Lmiui/cache/FolderCache;->get(Ljava/lang/String;)Lmiui/cache/FolderCache$FolderInfo;

    move-result-object v0

    return-object v0
.end method

.method public static getFormattedSize(J)Ljava/lang/String;
    .registers 13
    .parameter "size"

    .prologue
    const/4 v6, 0x1

    const/4 v10, 0x0

    const-wide/high16 v8, 0x4130

    const-wide/high16 v0, 0x4090

    .local v0, oneKilo:D
    const-wide/high16 v2, 0x4130

    .local v2, oneMega:D
    long-to-double v4, p0

    cmpg-double v4, v4, v8

    if-gez v4, :cond_20

    const-string v4, "%.0fK"

    new-array v5, v6, [Ljava/lang/Object;

    long-to-double v6, p0

    const-wide/high16 v8, 0x4090

    div-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v5, v10

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    :goto_1f
    return-object v4

    :cond_20
    const-string v4, "%.1fM"

    new-array v5, v6, [Ljava/lang/Object;

    long-to-double v6, p0

    div-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v5, v10

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_1f
.end method

.method public static getMediaEntryByPath(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;
    .registers 13
    .parameter "context"
    .parameter "path"

    .prologue
    const/4 v4, 0x1

    const/4 v10, 0x0

    const/4 v9, 0x0

    .local v9, uri:Landroid/net/Uri;
    invoke-static {p1}, Landroid/provider/MediaStore$Audio$Media;->getContentUriForPath(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .local v1, contentUri:Landroid/net/Uri;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v2, v10

    const-string v3, "_data=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v10

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .local v6, cursor:Landroid/database/Cursor;
    if-eqz v6, :cond_2f

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2c

    invoke-interface {v6, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    .local v7, id:J
    invoke-static {v1, v7, v8}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v9

    .end local v7           #id:J
    :cond_2c
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2f
    return-object v9
.end method

.method public static getPathByUri(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    .registers 3
    .parameter "context"
    .parameter "uri"

    .prologue
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lmiui/app/resourcebrowser/util/ResourceHelper;->getPathByUri(Landroid/content/Context;Landroid/net/Uri;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getPathByUri(Landroid/content/Context;Landroid/net/Uri;Z)Ljava/lang/String;
    .registers 8
    .parameter "context"
    .parameter "uri"
    .parameter "recursive"

    .prologue
    if-nez p1, :cond_5

    const-string v2, ""

    :cond_4
    :goto_4
    return-object v2

    :cond_5
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .local v2, path:Ljava/lang/String;
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    .local v3, scheme:Ljava/lang/String;
    const-string v4, "content"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4e

    if-eqz p2, :cond_4e

    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    .local v0, authority:Ljava/lang/String;
    const-string v4, "settings"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3f

    const-string v4, "value"

    invoke-static {p0, p1, v4}, Lmiui/app/resourcebrowser/util/ResourceHelper;->getContentColumnValue(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :cond_29
    :goto_29
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .local v1, nextUri:Landroid/net/Uri;
    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {v1, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    const/4 v4, 0x1

    invoke-static {p0, v1, v4}, Lmiui/app/resourcebrowser/util/ResourceHelper;->getPathByUri(Landroid/content/Context;Landroid/net/Uri;Z)Ljava/lang/String;

    move-result-object v2

    goto :goto_4

    .end local v1           #nextUri:Landroid/net/Uri;
    :cond_3f
    const-string v4, "media"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_29

    const-string v4, "_data"

    invoke-static {p0, p1, v4}, Lmiui/app/resourcebrowser/util/ResourceHelper;->getContentColumnValue(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_29

    .end local v0           #authority:Ljava/lang/String;
    :cond_4e
    const-string v4, "file"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    goto :goto_4
.end method

.method public static getPreviewWidth(I)I
    .registers 2
    .parameter "total"

    .prologue
    mul-int/lit8 v0, p0, 0x3

    div-int/lit8 v0, v0, 0x4

    return v0
.end method

.method public static getThumbnailGap(Landroid/content/Context;)I
    .registers 3
    .parameter "context"

    .prologue
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x60a000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method public static getThumbnailSize(Landroid/app/Activity;II)Landroid/util/Pair;
    .registers 13
    .parameter "activity"
    .parameter "displayType"
    .parameter "horizontalPadding"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "II)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .local v2, itemCntPerLine:I
    const/4 v1, -0x1

    .local v1, heightVsWidthRatioId:I
    packed-switch p1, :pswitch_data_5e

    :goto_5
    const/4 v6, 0x0

    .local v6, width:I
    const/4 v0, 0x0

    .local v0, height:I
    if-lez v1, :cond_55

    new-instance v4, Landroid/graphics/Point;

    invoke-direct {v4}, Landroid/graphics/Point;-><init>()V

    .local v4, p:Landroid/graphics/Point;
    invoke-virtual {p0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v7

    invoke-interface {v7}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v7

    invoke-virtual {v7, v4}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    iget v5, v4, Landroid/graphics/Point;->x:I

    .local v5, screenWidth:I
    invoke-static {p0}, Lmiui/app/resourcebrowser/util/ResourceHelper;->getThumbnailGap(Landroid/content/Context;)I

    move-result v3

    .local v3, itemGap:I
    sub-int v7, v5, p2

    add-int/lit8 v8, v2, -0x1

    mul-int/2addr v8, v3

    sub-int/2addr v7, v8

    div-int v6, v7, v2

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v1, v6, v6}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v7

    float-to-int v0, v7

    .end local v3           #itemGap:I
    .end local v4           #p:Landroid/graphics/Point;
    .end local v5           #screenWidth:I
    :goto_30
    new-instance v7, Landroid/util/Pair;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v7

    .end local v0           #height:I
    .end local v6           #width:I
    :pswitch_3e
    const/4 v1, -0x1

    const/4 v2, 0x1

    goto :goto_5

    :pswitch_41
    const v1, 0x6110004

    const/4 v2, 0x2

    goto :goto_5

    :pswitch_46
    const v1, 0x6110003

    const/4 v2, 0x3

    goto :goto_5

    :pswitch_4b
    const v1, 0x6110002

    const/4 v2, 0x3

    goto :goto_5

    :pswitch_50
    const v1, 0x6110001

    const/4 v2, 0x2

    goto :goto_5

    .restart local v0       #height:I
    .restart local v6       #width:I
    :cond_55
    const/4 v7, -0x1

    if-ne v1, v7, :cond_5b

    const/4 v6, -0x1

    const/4 v0, -0x1

    goto :goto_30

    :cond_5b
    const/4 v6, -0x2

    const/4 v0, -0x2

    goto :goto_30

    :pswitch_data_5e
    .packed-switch 0x1
        :pswitch_3e
        :pswitch_3e
        :pswitch_3e
        :pswitch_3e
        :pswitch_3e
        :pswitch_41
        :pswitch_4b
        :pswitch_46
        :pswitch_4b
        :pswitch_41
        :pswitch_50
    .end packed-switch
.end method

.method public static getThumbnailViewResource(I)I
    .registers 2
    .parameter "displayType"

    .prologue
    const/4 v0, 0x0

    .local v0, id:I
    packed-switch p0, :pswitch_data_32

    :goto_4
    return v0

    :pswitch_5
    const v0, 0x6030009

    goto :goto_4

    :pswitch_9
    const v0, 0x603000a

    goto :goto_4

    :pswitch_d
    const v0, 0x603000b

    goto :goto_4

    :pswitch_11
    const v0, 0x603000c

    goto :goto_4

    :pswitch_15
    const v0, 0x603000d

    goto :goto_4

    :pswitch_19
    const v0, 0x6030004

    goto :goto_4

    :pswitch_1d
    const v0, 0x6030003

    goto :goto_4

    :pswitch_21
    const v0, 0x6030004

    goto :goto_4

    :pswitch_25
    const v0, 0x6030008

    goto :goto_4

    :pswitch_29
    const v0, 0x6030006

    goto :goto_4

    :pswitch_2d
    const v0, 0x6030007

    goto :goto_4

    nop

    :pswitch_data_32
    .packed-switch 0x1
        :pswitch_5
        :pswitch_9
        :pswitch_d
        :pswitch_11
        :pswitch_15
        :pswitch_19
        :pswitch_1d
        :pswitch_21
        :pswitch_25
        :pswitch_29
        :pswitch_2d
    .end packed-switch
.end method

.method public static getUriByPath(Ljava/lang/String;)Landroid/net/Uri;
    .registers 4
    .parameter "path"

    .prologue
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8

    const/4 v1, 0x0

    :cond_7
    :goto_7
    return-object v1

    :cond_8
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .local v1, uri:Landroid/net/Uri;
    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    .local v0, scheme:Ljava/lang/String;
    if-nez v0, :cond_7

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_7
.end method

.method public static isCacheValid(Ljava/io/File;)Z
    .registers 2
    .parameter "file"

    .prologue
    const v0, 0x927c0

    invoke-static {p0, v0}, Lmiui/app/resourcebrowser/util/ResourceHelper;->isCacheValid(Ljava/io/File;I)Z

    move-result v0

    return v0
.end method

.method public static isCacheValid(Ljava/io/File;I)Z
    .registers 10
    .parameter "file"
    .parameter "expiredLastTime"

    .prologue
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .local v2, now:J
    invoke-virtual {p0}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    .local v0, modifiedTime:J
    sub-long v4, v2, v0

    int-to-long v6, p1

    cmp-long v4, v4, v6

    if-gez v4, :cond_11

    const/4 v4, 0x1

    :goto_10
    return v4

    :cond_11
    const/4 v4, 0x0

    goto :goto_10
.end method

.method public static isCombineView(I)Z
    .registers 2
    .parameter "displayType"

    .prologue
    const/16 v0, 0x8

    if-eq p0, v0, :cond_b

    const/4 v0, 0x7

    if-eq p0, v0, :cond_b

    const/16 v0, 0x9

    if-ne p0, v0, :cond_d

    :cond_b
    const/4 v0, 0x1

    :goto_c
    return v0

    :cond_d
    const/4 v0, 0x0

    goto :goto_c
.end method

.method public static isCompatiblePlatformVersion(III)Z
    .registers 4
    .parameter "platformVersion"
    .parameter "startPlatformVersion"
    .parameter "endPlatformVersion"

    .prologue
    if-gt p1, p0, :cond_6

    if-gt p0, p2, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public static isDataResource(Ljava/lang/String;)Z
    .registers 2
    .parameter "path"

    .prologue
    if-eqz p0, :cond_c

    const-string v0, "/data"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public static isMultipleView(I)Z
    .registers 2
    .parameter "displayType"

    .prologue
    const/4 v0, 0x4

    if-ne p0, v0, :cond_5

    const/4 v0, 0x1

    :goto_4
    return v0

    :cond_5
    const/4 v0, 0x0

    goto :goto_4
.end method

.method public static isSingleView(I)Z
    .registers 3
    .parameter "displayType"

    .prologue
    const/4 v0, 0x1

    if-eq p0, v0, :cond_f

    const/4 v1, 0x6

    if-eq p0, v1, :cond_f

    const/4 v1, 0x2

    if-eq p0, v1, :cond_f

    const/4 v1, 0x3

    if-eq p0, v1, :cond_f

    const/4 v1, 0x5

    if-ne p0, v1, :cond_10

    :cond_f
    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public static isSystemResource(Ljava/lang/String;)Z
    .registers 2
    .parameter "path"

    .prologue
    if-eqz p0, :cond_c

    const-string v0, "/system"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public static isZipResource(Ljava/lang/String;)Z
    .registers 4
    .parameter "path"

    .prologue
    const/4 v1, 0x0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8

    :cond_7
    :goto_7
    return v1

    :cond_8
    const-string v2, ".zip"

    invoke-virtual {p0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_18

    const-string v2, ".mtz"

    invoke-virtual {p0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    :cond_18
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .local v0, file:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-nez v2, :cond_7

    const/4 v1, 0x1

    goto :goto_7
.end method

.method public static moveUpdatedResourceInCacheDir(Ljava/lang/String;)Ljava/lang/String;
    .registers 9
    .parameter "resPathInCacheDir"

    .prologue
    move-object v3, p0

    .local v3, ret:Ljava/lang/String;
    sget-object v5, Lmiui/app/resourcebrowser/ResourceConstants;->CACHE_PATH:Ljava/lang/String;

    invoke-virtual {p0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_65

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .local v4, srcNewFile:Ljava/io/File;
    new-instance v0, Ljava/io/File;

    sget-object v5, Lmiui/app/resourcebrowser/ResourceConstants;->CACHE_PATH:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {p0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .local v0, destOldFile:Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_61

    :try_start_25
    sget-boolean v5, Lmiui/app/resourcebrowser/util/ResourceDebug;->DEBUG:Z

    if-eqz v5, :cond_45

    const-string v5, "Theme"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Move updated resource in data partition: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_45
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    new-instance v2, Ljava/io/BufferedInputStream;

    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const/16 v6, 0x2000

    invoke-direct {v2, v5, v6}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    .local v2, is:Ljava/io/BufferedInputStream;
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lmiui/app/resourcebrowser/util/ResourceHelper;->writeTo(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/BufferedInputStream;->close()V

    invoke-virtual {v4}, Ljava/io/File;->delete()Z
    :try_end_61
    .catch Ljava/lang/Exception; {:try_start_25 .. :try_end_61} :catch_66

    .end local v2           #is:Ljava/io/BufferedInputStream;
    :cond_61
    :goto_61
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    .end local v0           #destOldFile:Ljava/io/File;
    .end local v4           #srcNewFile:Ljava/io/File;
    :cond_65
    return-object v3

    .restart local v0       #destOldFile:Ljava/io/File;
    .restart local v4       #srcNewFile:Ljava/io/File;
    :catch_66
    move-exception v1

    .local v1, e:Ljava/lang/Exception;
    sget-boolean v5, Lmiui/app/resourcebrowser/util/ResourceDebug;->DEBUG:Z

    if-eqz v5, :cond_61

    const-string v5, "Theme"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Move occur error: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_61
.end method

.method public static resolveResourceStatus(Ljava/lang/String;)I
    .registers 3
    .parameter "path"

    .prologue
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .local v0, file:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_d

    const/4 v1, 0x2

    :goto_c
    return v1

    :cond_d
    const/4 v1, 0x0

    goto :goto_c
.end method

.method public static setMusicVolumeType(Landroid/app/Activity;I)V
    .registers 4
    .parameter "context"
    .parameter "ringtoneType"

    .prologue
    const/4 v0, -0x1

    .local v0, streamType:I
    const/4 v1, 0x1

    if-ne p1, v1, :cond_9

    const/4 v0, 0x2

    :goto_5
    invoke-virtual {p0, v0}, Landroid/app/Activity;->setVolumeControlStream(I)V

    return-void

    :cond_9
    const/4 v1, 0x2

    if-ne p1, v1, :cond_e

    const/4 v0, 0x5

    goto :goto_5

    :cond_e
    const/4 v1, 0x4

    if-ne p1, v1, :cond_13

    const/4 v0, 0x4

    goto :goto_5

    :cond_13
    const/16 v1, 0x20

    if-ne p1, v1, :cond_19

    const/4 v0, 0x3

    goto :goto_5

    :cond_19
    const/4 v0, 0x2

    goto :goto_5
.end method

.method public static setResourceStatus(Ljava/lang/String;Lmiui/widget/DataGroup;I)V
    .registers 6
    .parameter "path"
    .parameter
    .parameter "status"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lmiui/widget/DataGroup",
            "<",
            "Lmiui/app/resourcebrowser/resource/Resource;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .local p1, resourceSet:Lmiui/widget/DataGroup;,"Lmiui/widget/DataGroup<Lmiui/app/resourcebrowser/resource/Resource;>;"
    if-eqz p0, :cond_23

    invoke-static {p0}, Lmiui/app/resourcebrowser/util/ResourceHelper;->moveUpdatedResourceInCacheDir(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    const/4 v0, 0x0

    .local v0, i:I
    :goto_7
    invoke-virtual {p1}, Lmiui/widget/DataGroup;->size()I

    move-result v2

    if-ge v0, v2, :cond_23

    invoke-virtual {p1, v0}, Lmiui/widget/DataGroup;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/app/resourcebrowser/resource/Resource;

    .local v1, resource:Lmiui/app/resourcebrowser/resource/Resource;
    invoke-virtual {v1}, Lmiui/app/resourcebrowser/resource/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_20

    invoke-virtual {v1, p2}, Lmiui/app/resourcebrowser/resource/Resource;->updateStatus(I)V

    :cond_20
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .end local v0           #i:I
    .end local v1           #resource:Lmiui/app/resourcebrowser/resource/Resource;
    :cond_23
    return-void
.end method

.method public static toHexString([B)Ljava/lang/String;
    .registers 7
    .parameter "bytes"

    .prologue
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .local v0, hexString:Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, i:I
    :goto_6
    array-length v2, p0

    if-ge v1, v2, :cond_23

    const-string v2, "%02x"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aget-byte v5, p0, v1

    and-int/lit16 v5, v5, 0xff

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :cond_23
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static writeTo(Ljava/io/InputStream;Ljava/lang/String;)V
    .registers 9
    .parameter "is"
    .parameter "filename"

    .prologue
    :try_start_0
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v3

    const/16 v4, 0x1fd

    const/4 v5, -0x1

    const/4 v6, -0x1

    invoke-static {v3, v4, v5, v6}, Lmiui/os/ExtraFileUtils;->mkdirs(Ljava/io/File;III)Z

    new-instance v1, Lorg/apache/http/entity/InputStreamEntity;

    new-instance v3, Ljava/io/BufferedInputStream;

    invoke-direct {v3, p0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    const-wide/16 v4, -0x1

    invoke-direct {v1, v3, v4, v5}, Lorg/apache/http/entity/InputStreamEntity;-><init>(Ljava/io/InputStream;J)V

    .local v1, inputEntity:Lorg/apache/http/entity/InputStreamEntity;
    new-instance v2, Ljava/io/BufferedOutputStream;

    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .local v2, out:Ljava/io/BufferedOutputStream;
    const/16 v3, 0x1fd

    const/4 v4, -0x1

    const/4 v5, -0x1

    invoke-static {p1, v3, v4, v5}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    invoke-virtual {v1, v2}, Lorg/apache/http/entity/InputStreamEntity;->writeTo(Ljava/io/OutputStream;)V

    invoke-virtual {v2}, Ljava/io/BufferedOutputStream;->close()V

    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/io/File;->setLastModified(J)Z
    :try_end_42
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_42} :catch_43

    .end local v1           #inputEntity:Lorg/apache/http/entity/InputStreamEntity;
    .end local v2           #out:Ljava/io/BufferedOutputStream;
    :goto_42
    return-void

    :catch_43
    move-exception v0

    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_42
.end method
