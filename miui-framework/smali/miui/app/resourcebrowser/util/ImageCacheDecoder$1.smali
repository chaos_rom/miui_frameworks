.class Lmiui/app/resourcebrowser/util/ImageCacheDecoder$1;
.super Landroid/os/Handler;
.source "ImageCacheDecoder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/app/resourcebrowser/util/ImageCacheDecoder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/app/resourcebrowser/util/ImageCacheDecoder;


# direct methods
.method constructor <init>(Lmiui/app/resourcebrowser/util/ImageCacheDecoder;)V
    .registers 2
    .parameter

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$1;->this$0:Lmiui/app/resourcebrowser/util/ImageCacheDecoder;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 7
    .parameter "msg"

    .prologue
    const/4 v2, 0x1

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/util/Pair;

    .local v0, imagePath:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    iget v3, p1, Landroid/os/Message;->arg1:I

    if-nez v3, :cond_28

    move v1, v2

    .local v1, result:Z
    :goto_a
    iget v3, p1, Landroid/os/Message;->what:I

    if-nez v3, :cond_2a

    iget-object v2, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$1;->this$0:Lmiui/app/resourcebrowser/util/ImageCacheDecoder;

    #getter for: Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->mListener:Lmiui/app/resourcebrowser/util/ImageCacheDecoder$ImageDecodingListener;
    invoke-static {v2}, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->access$000(Lmiui/app/resourcebrowser/util/ImageCacheDecoder;)Lmiui/app/resourcebrowser/util/ImageCacheDecoder$ImageDecodingListener;

    move-result-object v2

    if-eqz v2, :cond_27

    iget-object v2, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$1;->this$0:Lmiui/app/resourcebrowser/util/ImageCacheDecoder;

    #getter for: Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->mListener:Lmiui/app/resourcebrowser/util/ImageCacheDecoder$ImageDecodingListener;
    invoke-static {v2}, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->access$000(Lmiui/app/resourcebrowser/util/ImageCacheDecoder;)Lmiui/app/resourcebrowser/util/ImageCacheDecoder$ImageDecodingListener;

    move-result-object v4

    iget-object v2, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    iget-object v3, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    invoke-interface {v4, v1, v2, v3}, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$ImageDecodingListener;->handleDecodingResult(ZLjava/lang/String;Ljava/lang/String;)V

    :cond_27
    :goto_27
    return-void

    .end local v1           #result:Z
    :cond_28
    const/4 v1, 0x0

    goto :goto_a

    .restart local v1       #result:Z
    :cond_2a
    iget v3, p1, Landroid/os/Message;->what:I

    if-ne v3, v2, :cond_27

    iget-object v2, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$1;->this$0:Lmiui/app/resourcebrowser/util/ImageCacheDecoder;

    #getter for: Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->mListener:Lmiui/app/resourcebrowser/util/ImageCacheDecoder$ImageDecodingListener;
    invoke-static {v2}, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->access$000(Lmiui/app/resourcebrowser/util/ImageCacheDecoder;)Lmiui/app/resourcebrowser/util/ImageCacheDecoder$ImageDecodingListener;

    move-result-object v2

    if-eqz v2, :cond_27

    iget-object v2, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$1;->this$0:Lmiui/app/resourcebrowser/util/ImageCacheDecoder;

    #getter for: Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->mListener:Lmiui/app/resourcebrowser/util/ImageCacheDecoder$ImageDecodingListener;
    invoke-static {v2}, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->access$000(Lmiui/app/resourcebrowser/util/ImageCacheDecoder;)Lmiui/app/resourcebrowser/util/ImageCacheDecoder$ImageDecodingListener;

    move-result-object v4

    iget-object v2, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    iget-object v3, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    invoke-interface {v4, v1, v2, v3}, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$ImageDecodingListener;->handleDownloadResult(ZLjava/lang/String;Ljava/lang/String;)V

    goto :goto_27
.end method
