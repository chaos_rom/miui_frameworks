.class Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$3;
.super Ljava/lang/Object;
.source "ResourceMusicPlayer.java"

# interfaces
.implements Lmiui/app/resourcebrowser/util/BatchMediaPlayer$BatchPlayerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->initPlayer()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;


# direct methods
.method constructor <init>(Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;)V
    .registers 2
    .parameter

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$3;->this$0:Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public finish(Z)V
    .registers 6
    .parameter "hasError"

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$3;->this$0:Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;

    #setter for: Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->mUserPlaying:Z
    invoke-static {v0, v2}, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->access$402(Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;Z)Z

    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$3;->this$0:Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;

    #setter for: Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->mCurrentPlayingResource:Lmiui/app/resourcebrowser/resource/Resource;
    invoke-static {v0, v3}, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->access$302(Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;Lmiui/app/resourcebrowser/resource/Resource;)Lmiui/app/resourcebrowser/resource/Resource;

    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$3;->this$0:Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;

    iget-object v0, v0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->mCurrentPlayingButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$3;->this$0:Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;

    iget-object v0, v0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->mCurrentPlayingButton:Landroid/widget/ImageView;

    const v1, 0x60200d9

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_1c
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$3;->this$0:Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;

    iput-object v3, v0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->mCurrentPlayingButton:Landroid/widget/ImageView;

    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$3;->this$0:Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;

    #getter for: Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->mProgressListener:Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$PlayProgressListener;
    invoke-static {v0}, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->access$100(Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;)Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$PlayProgressListener;

    move-result-object v0

    if-eqz v0, :cond_31

    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$3;->this$0:Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;

    #getter for: Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->mProgressListener:Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$PlayProgressListener;
    invoke-static {v0}, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->access$100(Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;)Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$PlayProgressListener;

    move-result-object v0

    invoke-interface {v0}, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$PlayProgressListener;->onStopPlaying()V

    :cond_31
    if-eqz p1, :cond_43

    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$3;->this$0:Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;

    #getter for: Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->access$500(Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x60c0020

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_43
    return-void
.end method

.method public play(Ljava/lang/String;II)V
    .registers 8
    .parameter "ringtonePath"
    .parameter "current"
    .parameter "total"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$3;->this$0:Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;

    #getter for: Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->mProgressListener:Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$PlayProgressListener;
    invoke-static {v0}, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->access$100(Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;)Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$PlayProgressListener;

    move-result-object v0

    if-eqz v0, :cond_1d

    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$3;->this$0:Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;

    #getter for: Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->mProgressListener:Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$PlayProgressListener;
    invoke-static {v0}, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->access$100(Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;)Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$PlayProgressListener;

    move-result-object v0

    invoke-interface {v0}, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$PlayProgressListener;->onStartPlaying()V

    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$3;->this$0:Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;

    #getter for: Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->access$200(Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x0

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_1d
    return-void
.end method
