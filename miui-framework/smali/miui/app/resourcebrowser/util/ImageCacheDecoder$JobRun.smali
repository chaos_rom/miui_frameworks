.class Lmiui/app/resourcebrowser/util/ImageCacheDecoder$JobRun;
.super Ljava/lang/Object;
.source "ImageCacheDecoder.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/app/resourcebrowser/util/ImageCacheDecoder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "JobRun"
.end annotation


# instance fields
.field private final downloadingJob:Z

.field private imageLocalPath:Ljava/lang/String;

.field private imageOnlinePath:Ljava/lang/String;

.field private submitTimeForDebug:J

.field final synthetic this$0:Lmiui/app/resourcebrowser/util/ImageCacheDecoder;

.field private useIndex:I


# direct methods
.method public constructor <init>(Lmiui/app/resourcebrowser/util/ImageCacheDecoder;Ljava/lang/String;Ljava/lang/String;I)V
    .registers 9
    .parameter
    .parameter "imageLocalPath"
    .parameter "imageOnlinePath"
    .parameter "useIndex"

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$JobRun;->this$0:Lmiui/app/resourcebrowser/util/ImageCacheDecoder;

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$JobRun;->imageLocalPath:Ljava/lang/String;

    iput-object p3, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$JobRun;->imageOnlinePath:Ljava/lang/String;

    iput p4, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$JobRun;->useIndex:I

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_57

    const/4 v0, 0x1

    :goto_17
    iput-boolean v0, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$JobRun;->downloadingJob:Z

    sget-boolean v0, Lmiui/app/resourcebrowser/util/ResourceDebug;->DEBUG:Z

    if-eqz v0, :cond_56

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$JobRun;->submitTimeForDebug:J

    const-string v1, "Theme"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "submit loading wallpaper: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v0, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$JobRun;->downloadingJob:Z

    if-eqz v0, :cond_59

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " online "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_4b
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_56
    return-void

    :cond_57
    const/4 v0, 0x0

    goto :goto_17

    :cond_59
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " local "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_4b
.end method


# virtual methods
.method public dispatchJob()V
    .registers 2

    .prologue
    iget-boolean v0, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$JobRun;->downloadingJob:Z

    if-eqz v0, :cond_e

    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$JobRun;->this$0:Lmiui/app/resourcebrowser/util/ImageCacheDecoder;

    #getter for: Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->mDownloadExecutorService:Ljava/util/concurrent/ExecutorService;
    invoke-static {v0}, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->access$100(Lmiui/app/resourcebrowser/util/ImageCacheDecoder;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    :goto_d
    return-void

    :cond_e
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$JobRun;->this$0:Lmiui/app/resourcebrowser/util/ImageCacheDecoder;

    #getter for: Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->mDecodeExecutorService:Ljava/util/concurrent/ExecutorService;
    invoke-static {v0}, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->access$200(Lmiui/app/resourcebrowser/util/ImageCacheDecoder;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    goto :goto_d
.end method

.method public run()V
    .registers 21

    .prologue
    const/4 v12, 0x0

    .local v12, result:Z
    move-object/from16 v0, p0

    iget-object v15, v0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$JobRun;->this$0:Lmiui/app/resourcebrowser/util/ImageCacheDecoder;

    iget-object v15, v15, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->mBitmapCache:Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;

    move-object/from16 v0, p0

    iget v0, v0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$JobRun;->useIndex:I

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->inCacheScope(I)Z

    move-result v15

    if-eqz v15, :cond_1f4

    sget-boolean v15, Lmiui/app/resourcebrowser/util/ResourceDebug;->DEBUG:Z

    if-eqz v15, :cond_15b

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .local v8, jobStartTime:J
    :goto_1b
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$JobRun;->downloadingJob:Z

    if-eqz v15, :cond_1cf

    move-object/from16 v0, p0

    iget-object v15, v0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$JobRun;->imageOnlinePath:Ljava/lang/String;

    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_15f

    const/4 v12, 0x0

    :cond_2c
    :goto_2c
    sget-boolean v15, Lmiui/app/resourcebrowser/util/ResourceDebug;->DEBUG:Z

    if-eqz v15, :cond_ff

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .local v6, jobEndTime:J
    new-instance v15, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$JobRun;->imageLocalPath:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-direct/range {v15 .. v16}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15}, Ljava/io/File;->length()J

    move-result-wide v4

    .local v4, fileSize:J
    const-string v13, ""

    .local v13, speed:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$JobRun;->downloadingJob:Z

    if-eqz v15, :cond_6f

    const-string v15, "%.1fKB/s"

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    sub-long v18, v6, v8

    div-long v18, v4, v18

    move-wide/from16 v0, v18

    long-to-float v0, v0

    move/from16 v18, v0

    const v19, 0x3f79db23

    mul-float v18, v18, v19

    invoke-static/range {v18 .. v18}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-static/range {v15 .. v16}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    :cond_6f
    const-string v16, "Theme"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "finish loading wallpaper: "

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget v0, v0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$JobRun;->useIndex:I

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v17, " "

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    if-eqz v12, :cond_1ec

    const-string v15, "success"

    :goto_96
    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v17, " "

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$JobRun;->downloadingJob:Z

    if-eqz v15, :cond_1f0

    const-string v15, "online"

    :goto_ac
    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v17, " "

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-static {v4, v5}, Lmiui/app/resourcebrowser/util/ResourceHelper;->getFormattedSize(J)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v17, " "

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-wide v0, v0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$JobRun;->submitTimeForDebug:J

    move-wide/from16 v17, v0

    sub-long v17, v6, v17

    move-wide/from16 v0, v17

    invoke-virtual {v15, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v17, " "

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sub-long v17, v6, v8

    move-wide/from16 v0, v17

    invoke-virtual {v15, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v17, " "

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, v16

    invoke-static {v0, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .end local v4           #fileSize:J
    .end local v6           #jobEndTime:J
    .end local v8           #jobStartTime:J
    .end local v13           #speed:Ljava/lang/String;
    :cond_ff
    :goto_ff
    move-object/from16 v0, p0

    iget-object v15, v0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$JobRun;->this$0:Lmiui/app/resourcebrowser/util/ImageCacheDecoder;

    #getter for: Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->mDoingJob:Ljava/util/HashSet;
    invoke-static {v15}, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->access$400(Lmiui/app/resourcebrowser/util/ImageCacheDecoder;)Ljava/util/HashSet;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$JobRun;->imageLocalPath:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v15, v0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$JobRun;->this$0:Lmiui/app/resourcebrowser/util/ImageCacheDecoder;

    iget-object v15, v15, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->mBitmapCache:Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;

    move-object/from16 v0, p0

    iget v0, v0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$JobRun;->useIndex:I

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->inCacheScope(I)Z

    move-result v15

    if-eqz v15, :cond_15a

    move-object/from16 v0, p0

    iget-object v15, v0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$JobRun;->this$0:Lmiui/app/resourcebrowser/util/ImageCacheDecoder;

    #getter for: Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->mHandler:Landroid/os/Handler;
    invoke-static {v15}, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->access$500(Lmiui/app/resourcebrowser/util/ImageCacheDecoder;)Landroid/os/Handler;

    move-result-object v15

    invoke-virtual {v15}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v11

    .local v11, msg:Landroid/os/Message;
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$JobRun;->downloadingJob:Z

    if-eqz v15, :cond_22f

    const/4 v15, 0x1

    :goto_135
    iput v15, v11, Landroid/os/Message;->what:I

    if-eqz v12, :cond_232

    const/4 v15, 0x0

    :goto_13a
    iput v15, v11, Landroid/os/Message;->arg1:I

    new-instance v15, Landroid/util/Pair;

    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$JobRun;->imageLocalPath:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$JobRun;->imageOnlinePath:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-direct/range {v15 .. v17}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v15, v11, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v15, v0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$JobRun;->this$0:Lmiui/app/resourcebrowser/util/ImageCacheDecoder;

    #getter for: Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->mHandler:Landroid/os/Handler;
    invoke-static {v15}, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->access$500(Lmiui/app/resourcebrowser/util/ImageCacheDecoder;)Landroid/os/Handler;

    move-result-object v15

    invoke-virtual {v15, v11}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .end local v11           #msg:Landroid/os/Message;
    :cond_15a
    return-void

    :cond_15b
    const-wide/16 v8, 0x0

    goto/16 :goto_1b

    .restart local v8       #jobStartTime:J
    :cond_15f
    new-instance v14, Lmiui/app/resourcebrowser/util/DownloadFileTask;

    invoke-direct {v14}, Lmiui/app/resourcebrowser/util/DownloadFileTask;-><init>()V

    .local v14, task:Lmiui/app/resourcebrowser/util/DownloadFileTask;
    new-instance v3, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;

    invoke-direct {v3}, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;-><init>()V

    .local v3, entry:Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;
    move-object/from16 v0, p0

    iget-object v15, v0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$JobRun;->imageLocalPath:Ljava/lang/String;

    invoke-virtual {v3, v15}, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->setPath(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v15, v0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$JobRun;->imageOnlinePath:Ljava/lang/String;

    invoke-virtual {v3, v15}, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->setUrl(Ljava/lang/String;)V

    const/4 v15, 0x1

    new-array v15, v15, [Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;

    const/16 v16, 0x0

    aput-object v3, v15, v16

    invoke-virtual {v14, v15}, Lmiui/app/resourcebrowser/util/DownloadFileTask;->doInForeground([Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;)Ljava/util/List;

    new-instance v10, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v15, v0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$JobRun;->imageLocalPath:Ljava/lang/String;

    invoke-direct {v10, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .local v10, localFile:Ljava/io/File;
    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v15

    if-eqz v15, :cond_19d

    invoke-virtual {v10}, Ljava/io/File;->length()J

    move-result-wide v15

    const-wide/16 v17, 0x400

    cmp-long v15, v15, v17

    if-gez v15, :cond_19d

    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    :cond_19d
    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v12

    move-object/from16 v0, p0

    iget-object v15, v0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$JobRun;->this$0:Lmiui/app/resourcebrowser/util/ImageCacheDecoder;

    #getter for: Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->mFailToDownloadTime:Ljava/util/HashMap;
    invoke-static {v15}, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->access$300(Lmiui/app/resourcebrowser/util/ImageCacheDecoder;)Ljava/util/HashMap;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$JobRun;->imageOnlinePath:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    if-nez v12, :cond_2c

    move-object/from16 v0, p0

    iget-object v15, v0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$JobRun;->this$0:Lmiui/app/resourcebrowser/util/ImageCacheDecoder;

    #getter for: Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->mFailToDownloadTime:Ljava/util/HashMap;
    invoke-static {v15}, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->access$300(Lmiui/app/resourcebrowser/util/ImageCacheDecoder;)Ljava/util/HashMap;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$JobRun;->imageOnlinePath:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v17

    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    invoke-virtual/range {v15 .. v17}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2c

    .end local v3           #entry:Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;
    .end local v10           #localFile:Ljava/io/File;
    .end local v14           #task:Lmiui/app/resourcebrowser/util/DownloadFileTask;
    :cond_1cf
    move-object/from16 v0, p0

    iget-object v15, v0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$JobRun;->this$0:Lmiui/app/resourcebrowser/util/ImageCacheDecoder;

    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$JobRun;->imageLocalPath:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$JobRun;->useIndex:I

    move/from16 v17, v0

    const/16 v18, 0x1

    invoke-virtual/range {v15 .. v18}, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->decodeLocalImage(Ljava/lang/String;IZ)Landroid/graphics/Bitmap;

    move-result-object v2

    .local v2, b:Landroid/graphics/Bitmap;
    if-eqz v2, :cond_1ea

    const/4 v12, 0x1

    :goto_1e8
    goto/16 :goto_2c

    :cond_1ea
    const/4 v12, 0x0

    goto :goto_1e8

    .end local v2           #b:Landroid/graphics/Bitmap;
    .restart local v4       #fileSize:J
    .restart local v6       #jobEndTime:J
    .restart local v13       #speed:Ljava/lang/String;
    :cond_1ec
    const-string v15, "fail   "

    goto/16 :goto_96

    :cond_1f0
    const-string v15, "local "

    goto/16 :goto_ac

    .end local v4           #fileSize:J
    .end local v6           #jobEndTime:J
    .end local v8           #jobStartTime:J
    .end local v13           #speed:Ljava/lang/String;
    :cond_1f4
    sget-boolean v15, Lmiui/app/resourcebrowser/util/ResourceDebug;->DEBUG:Z

    if-eqz v15, :cond_ff

    const-string v16, "Theme"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "ingore loading wallpaper: "

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget v0, v0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$JobRun;->useIndex:I

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$JobRun;->downloadingJob:Z

    if-eqz v15, :cond_22c

    const-string v15, " online "

    :goto_21b
    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, v16

    invoke-static {v0, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_ff

    :cond_22c
    const-string v15, " local"

    goto :goto_21b

    .restart local v11       #msg:Landroid/os/Message;
    :cond_22f
    const/4 v15, 0x0

    goto/16 :goto_135

    :cond_232
    const/4 v15, 0x1

    goto/16 :goto_13a
.end method
