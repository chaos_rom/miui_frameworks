.class public Lmiui/app/resourcebrowser/util/ResourceDebug;
.super Ljava/lang/Object;
.source "ResourceDebug.java"


# static fields
#the value of this static final field might be set in the static constructor
.field public static final DEBUG:Z = false

.field private static final FLAG_FILE:Ljava/lang/String; = "/data/system/theme_debug"

.field public static final TAG:Ljava/lang/String; = "Theme"

.field public static final sMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static sMaxThumbnailDownloadTaskNumber:I


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    new-instance v3, Ljava/io/File;

    const-string v4, "/data/system/theme_debug"

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    sput-boolean v3, Lmiui/app/resourcebrowser/util/ResourceDebug;->DEBUG:Z

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    sput-object v3, Lmiui/app/resourcebrowser/util/ResourceDebug;->sMap:Ljava/util/HashMap;

    const/4 v3, 0x3

    sput v3, Lmiui/app/resourcebrowser/util/ResourceDebug;->sMaxThumbnailDownloadTaskNumber:I

    sget-boolean v3, Lmiui/app/resourcebrowser/util/ResourceDebug;->DEBUG:Z

    if-eqz v3, :cond_3e

    :try_start_1b
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/FileReader;

    const-string v4, "/data/system/theme_debug"

    invoke-direct {v3, v4}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .local v0, is:Ljava/io/BufferedReader;
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .local v1, line:Ljava/lang/String;
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .local v2, number:I
    if-lez v2, :cond_3b

    const/16 v3, 0xa

    if-ge v2, v3, :cond_3b

    sput v2, Lmiui/app/resourcebrowser/util/ResourceDebug;->sMaxThumbnailDownloadTaskNumber:I

    :cond_3b
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_3e
    .catch Ljava/lang/Exception; {:try_start_1b .. :try_end_3e} :catch_3f

    .end local v1           #line:Ljava/lang/String;
    .end local v2           #number:I
    :cond_3e
    :goto_3e
    return-void

    :catch_3f
    move-exception v3

    goto :goto_3e
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getMaxThumbnailDownloadTaskNumber()I
    .registers 1

    .prologue
    sget v0, Lmiui/app/resourcebrowser/util/ResourceDebug;->sMaxThumbnailDownloadTaskNumber:I

    return v0
.end method
