.class public abstract Lmiui/app/resourcebrowser/util/CleanCachePolicy;
.super Ljava/lang/Object;
.source "CleanCachePolicy.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/app/resourcebrowser/util/CleanCachePolicy$LRUCleanCacheByAccessTime;,
        Lmiui/app/resourcebrowser/util/CleanCachePolicy$FileEntry;
    }
.end annotation


# instance fields
.field private mLeastKeepTime:I


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x3f480

    iput v0, p0, Lmiui/app/resourcebrowser/util/CleanCachePolicy;->mLeastKeepTime:I

    return-void
.end method

.method protected static deleteFile(Ljava/io/File;)Z
    .registers 8
    .parameter "file"

    .prologue
    const/4 v5, 0x1

    if-nez p0, :cond_4

    :cond_3
    :goto_3
    return v5

    :cond_4
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_1d

    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v4

    .local v4, tmpFiles:[Ljava/io/File;
    if-eqz v4, :cond_1d

    move-object v0, v4

    .local v0, arr$:[Ljava/io/File;
    array-length v2, v0

    .local v2, len$:I
    const/4 v1, 0x0

    .local v1, i$:I
    :goto_13
    if-ge v1, v2, :cond_1d

    aget-object v3, v0, v1

    .local v3, tmp:Ljava/io/File;
    invoke-static {v3}, Lmiui/app/resourcebrowser/util/CleanCachePolicy;->deleteFile(Ljava/io/File;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_13

    .end local v0           #arr$:[Ljava/io/File;
    .end local v1           #i$:I
    .end local v2           #len$:I
    .end local v3           #tmp:Ljava/io/File;
    .end local v4           #tmpFiles:[Ljava/io/File;
    :cond_1d
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_3

    const/4 v5, 0x0

    goto :goto_3
.end method

.method protected static deleteFile(Ljava/lang/String;)Z
    .registers 2
    .parameter "path"

    .prologue
    if-eqz p0, :cond_c

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lmiui/app/resourcebrowser/util/CleanCachePolicy;->deleteFile(Ljava/io/File;)Z

    move-result v0

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method


# virtual methods
.method public final cleanCache(Ljava/lang/String;)I
    .registers 4
    .parameter "cacheDirPath"

    .prologue
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/util/CleanCachePolicy;->preClean()V

    invoke-virtual {p0, p1}, Lmiui/app/resourcebrowser/util/CleanCachePolicy;->onScanCacheFiles(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v1}, Lmiui/app/resourcebrowser/util/CleanCachePolicy;->onCleanCacheFiles(Ljava/util/List;)I

    move-result v0

    .local v0, ret:I
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/util/CleanCachePolicy;->postClean()V

    return v0
.end method

.method protected abstract onCleanCacheFiles(Ljava/util/List;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lmiui/app/resourcebrowser/util/CleanCachePolicy$FileEntry;",
            ">;)I"
        }
    .end annotation
.end method

.method protected onScanCacheFiles(Ljava/lang/String;)Ljava/util/List;
    .registers 15
    .parameter "dirPath"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lmiui/app/resourcebrowser/util/CleanCachePolicy$FileEntry;",
            ">;"
        }
    .end annotation

    .prologue
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .local v4, fileEntries:Ljava/util/List;,"Ljava/util/List<Lmiui/app/resourcebrowser/util/CleanCachePolicy$FileEntry;>;"
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_64

    new-instance v9, Ljava/io/File;

    invoke-direct {v9, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v5

    .local v5, fnames:[Ljava/lang/String;
    if-eqz v5, :cond_64

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    const-wide/16 v11, 0x3e8

    div-long v1, v9, v11

    .local v1, currentTime:J
    move-object v0, v5

    .local v0, arr$:[Ljava/lang/String;
    array-length v7, v0

    .local v7, len$:I
    const/4 v6, 0x0

    .local v6, i$:I
    :goto_21
    if-ge v6, v7, :cond_64

    aget-object v8, v0, v6

    .local v8, name:Ljava/lang/String;
    new-instance v3, Lmiui/app/resourcebrowser/util/CleanCachePolicy$FileEntry;

    invoke-direct {v3}, Lmiui/app/resourcebrowser/util/CleanCachePolicy$FileEntry;-><init>()V

    .local v3, entry:Lmiui/app/resourcebrowser/util/CleanCachePolicy$FileEntry;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-object v10, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    iput-object v9, v3, Lmiui/app/resourcebrowser/util/CleanCachePolicy$FileEntry;->filePath:Ljava/lang/String;

    new-instance v9, Landroid/os/FileUtils$FileStatus;

    invoke-direct {v9}, Landroid/os/FileUtils$FileStatus;-><init>()V

    iput-object v9, v3, Lmiui/app/resourcebrowser/util/CleanCachePolicy$FileEntry;->fileStatus:Landroid/os/FileUtils$FileStatus;

    iget-object v9, v3, Lmiui/app/resourcebrowser/util/CleanCachePolicy$FileEntry;->filePath:Ljava/lang/String;

    iget-object v10, v3, Lmiui/app/resourcebrowser/util/CleanCachePolicy$FileEntry;->fileStatus:Landroid/os/FileUtils$FileStatus;

    invoke-static {v9, v10}, Landroid/os/FileUtils;->getFileStatus(Ljava/lang/String;Landroid/os/FileUtils$FileStatus;)Z

    iget-object v9, v3, Lmiui/app/resourcebrowser/util/CleanCachePolicy$FileEntry;->fileStatus:Landroid/os/FileUtils$FileStatus;

    iget-wide v9, v9, Landroid/os/FileUtils$FileStatus;->mtime:J

    sub-long v9, v1, v9

    iget v11, p0, Lmiui/app/resourcebrowser/util/CleanCachePolicy;->mLeastKeepTime:I

    int-to-long v11, v11

    cmp-long v9, v9, v11

    if-lez v9, :cond_61

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_61
    add-int/lit8 v6, v6, 0x1

    goto :goto_21

    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #currentTime:J
    .end local v3           #entry:Lmiui/app/resourcebrowser/util/CleanCachePolicy$FileEntry;
    .end local v5           #fnames:[Ljava/lang/String;
    .end local v6           #i$:I
    .end local v7           #len$:I
    .end local v8           #name:Ljava/lang/String;
    :cond_64
    return-object v4
.end method

.method protected postClean()V
    .registers 1

    .prologue
    return-void
.end method

.method protected preClean()V
    .registers 1

    .prologue
    return-void
.end method

.method public setLeastKeepTime(I)V
    .registers 2
    .parameter "keep"

    .prologue
    iput p1, p0, Lmiui/app/resourcebrowser/util/CleanCachePolicy;->mLeastKeepTime:I

    return-void
.end method
