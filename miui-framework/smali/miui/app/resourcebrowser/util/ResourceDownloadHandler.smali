.class public Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;
.super Ljava/lang/Object;
.source "ResourceDownloadHandler.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDownloadManager:Landroid/app/DownloadManager;

.field private mDownloadReceiver:Landroid/content/BroadcastReceiver;

.field private mHasRegistDownloadReceiver:Z

.field private mLastQueryFilePath:Ljava/lang/String;

.field private mLastQueryResult:Z

.field private mLastQueryTime:Ljava/lang/Long;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;->mHasRegistDownloadReceiver:Z

    new-instance v0, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler$1;

    invoke-direct {v0, p0}, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler$1;-><init>(Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;)V

    iput-object v0, p0, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;->mDownloadReceiver:Landroid/content/BroadcastReceiver;

    iput-object p1, p0, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;->mContext:Landroid/content/Context;

    const-string v1, "download"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/DownloadManager;

    iput-object v0, p0, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;->mDownloadManager:Landroid/app/DownloadManager;

    return-void
.end method

.method static synthetic access$000(Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;)Landroid/app/DownloadManager;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;->mDownloadManager:Landroid/app/DownloadManager;

    return-object v0
.end method

.method public static downloadResource(Landroid/app/DownloadManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J
    .registers 12
    .parameter "dmgr"
    .parameter "downloadUrl"
    .parameter "savePath"
    .parameter "notificationTitle"

    .prologue
    const/4 v7, -0x1

    invoke-static {p1}, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;->getUriFromUrl(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v3

    .local v3, uri:Ljava/net/URI;
    if-nez v3, :cond_a

    const-wide/16 v4, -0x1

    :goto_9
    return-wide v4

    :cond_a
    sget-boolean v4, Lmiui/app/resourcebrowser/util/ResourceDebug;->DEBUG:Z

    if-eqz v4, :cond_30

    const-string v4, "Theme"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Download resource url= "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " savePath="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_30
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .local v1, targetFile:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v4

    const/16 v5, 0x1ff

    invoke-static {v4, v5, v7, v7}, Lmiui/os/ExtraFileUtils;->mkdirs(Ljava/io/File;III)Z

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".temp"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .local v2, tmpDownloadPath:Ljava/lang/String;
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    new-instance v0, Landroid/app/MiuiDownloadManager$Request;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v0, v4}, Landroid/app/MiuiDownloadManager$Request;-><init>(Landroid/net/Uri;)V

    .local v0, request:Landroid/app/MiuiDownloadManager$Request;
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/app/MiuiDownloadManager$Request;->setNotificationVisibility(I)Landroid/app/DownloadManager$Request;

    invoke-static {p1}, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;->getMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/app/MiuiDownloadManager$Request;->setMimeType(Ljava/lang/String;)Landroid/app/DownloadManager$Request;

    invoke-virtual {v0, p3}, Landroid/app/MiuiDownloadManager$Request;->setTitle(Ljava/lang/CharSequence;)Landroid/app/DownloadManager$Request;

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/app/MiuiDownloadManager$Request;->setDestinationUri(Landroid/net/Uri;)Landroid/app/DownloadManager$Request;

    invoke-virtual {v0, v2}, Landroid/app/MiuiDownloadManager$Request;->setAppointName(Ljava/lang/String;)Landroid/app/MiuiDownloadManager$Request;

    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Landroid/app/MiuiDownloadManager$Request;->setVisibleInDownloadsUi(Z)Landroid/app/DownloadManager$Request;

    invoke-virtual {v0, p2}, Landroid/app/MiuiDownloadManager$Request;->setAppData(Ljava/lang/String;)Landroid/app/MiuiDownloadManager$Request;

    invoke-virtual {p0, v0}, Landroid/app/DownloadManager;->enqueue(Landroid/app/DownloadManager$Request;)J

    move-result-wide v4

    goto :goto_9
.end method

.method private static getMimeType(Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .parameter "url"

    .prologue
    const/4 v4, -0x1

    move-object v2, p0

    .local v2, name:Ljava/lang/String;
    const-string v0, ""

    .local v0, extension:Ljava/lang/String;
    const/16 v3, 0x2f

    invoke-virtual {p0, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .local v1, index:I
    if-eq v4, v1, :cond_12

    add-int/lit8 v3, v1, 0x1

    invoke-virtual {p0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    :cond_12
    const/16 v3, 0x2e

    invoke-virtual {v2, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    if-eq v4, v1, :cond_20

    add-int/lit8 v3, v1, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :cond_20
    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private static getUriFromUrl(Ljava/lang/String;)Ljava/net/URI;
    .registers 13
    .parameter "url"

    .prologue
    const/4 v2, -0x1

    const/4 v10, 0x0

    .local v10, uri:Ljava/net/URI;
    :try_start_2
    new-instance v9, Ljava/lang/String;

    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-static {v1}, Landroid/webkit/URLUtil;->decode([B)[B

    move-result-object v1

    invoke-direct {v9, v1}, Ljava/lang/String;-><init>([B)V

    .local v9, newUrl:Ljava/lang/String;
    new-instance v11, Landroid/net/WebAddress;

    invoke-direct {v11, v9}, Landroid/net/WebAddress;-><init>(Ljava/lang/String;)V

    .local v11, w:Landroid/net/WebAddress;
    const/4 v7, 0x0

    .local v7, frag:Ljava/lang/String;
    const/4 v6, 0x0

    .local v6, query:Ljava/lang/String;
    invoke-virtual {v11}, Landroid/net/WebAddress;->getPath()Ljava/lang/String;

    move-result-object v5

    .local v5, path:Ljava/lang/String;
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_46

    const/16 v1, 0x23

    invoke-virtual {v5, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v8

    .local v8, idx:I
    if-eq v8, v2, :cond_33

    add-int/lit8 v1, v8, 0x1

    invoke-virtual {v5, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    const/4 v1, 0x0

    invoke-virtual {v5, v1, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    :cond_33
    const/16 v1, 0x3f

    invoke-virtual {v5, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v8

    if-eq v8, v2, :cond_46

    add-int/lit8 v1, v8, 0x1

    invoke-virtual {v5, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    const/4 v1, 0x0

    invoke-virtual {v5, v1, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .end local v8           #idx:I
    :cond_46
    new-instance v0, Ljava/net/URI;

    invoke-virtual {v11}, Landroid/net/WebAddress;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11}, Landroid/net/WebAddress;->getAuthInfo()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v11}, Landroid/net/WebAddress;->getHost()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v11}, Landroid/net/WebAddress;->getPort()I

    move-result v4

    invoke-direct/range {v0 .. v7}, Ljava/net/URI;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5b
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_5b} :catch_5c

    .end local v5           #path:Ljava/lang/String;
    .end local v6           #query:Ljava/lang/String;
    .end local v7           #frag:Ljava/lang/String;
    .end local v9           #newUrl:Ljava/lang/String;
    .end local v10           #uri:Ljava/net/URI;
    .end local v11           #w:Landroid/net/WebAddress;
    .local v0, uri:Ljava/net/URI;
    :goto_5b
    return-object v0

    .end local v0           #uri:Ljava/net/URI;
    .restart local v10       #uri:Ljava/net/URI;
    :catch_5c
    move-exception v1

    move-object v0, v10

    .end local v10           #uri:Ljava/net/URI;
    .restart local v0       #uri:Ljava/net/URI;
    goto :goto_5b
.end method


# virtual methods
.method public cancelDownload(Ljava/lang/String;)Z
    .registers 12
    .parameter "downloadSavePath"

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    new-instance v4, Landroid/app/MiuiDownloadManager$Query;

    invoke-direct {v4}, Landroid/app/MiuiDownloadManager$Query;-><init>()V

    .local v4, query:Landroid/app/MiuiDownloadManager$Query;
    const-string v8, "\'"

    const-string v9, "\'\'"

    invoke-virtual {p1, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .local v3, encodedFilePath:Ljava/lang/String;
    invoke-virtual {v4, v3}, Landroid/app/MiuiDownloadManager$Query;->setFilterByAppData(Ljava/lang/String;)Landroid/app/MiuiDownloadManager$Query;

    iget-object v8, p0, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;->mDownloadManager:Landroid/app/DownloadManager;

    invoke-virtual {v8, v4}, Landroid/app/DownloadManager;->query(Landroid/app/DownloadManager$Query;)Landroid/database/Cursor;

    move-result-object v0

    .local v0, cursor:Landroid/database/Cursor;
    const/4 v5, 0x0

    .local v5, ret:I
    if-eqz v0, :cond_38

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v8

    if-eqz v8, :cond_35

    const-string v8, "_id"

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    .local v1, downloadId:J
    iget-object v8, p0, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;->mDownloadManager:Landroid/app/DownloadManager;

    new-array v9, v6, [J

    aput-wide v1, v9, v7

    invoke-virtual {v8, v9}, Landroid/app/DownloadManager;->remove([J)I

    move-result v5

    .end local v1           #downloadId:J
    :cond_35
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_38
    if-ne v5, v6, :cond_3b

    :goto_3a
    return v6

    :cond_3b
    move v6, v7

    goto :goto_3a
.end method

.method public downloadResource(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 5
    .parameter "downloadUrl"
    .parameter "savePath"
    .parameter "notificationTitle"

    .prologue
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;->downloadResource(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public downloadResource(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Z
    .registers 14
    .parameter "downloadUrl"
    .parameter "savePath"
    .parameter "notificationTitle"
    .parameter "listenDownloadProgress"

    .prologue
    const-wide/16 v7, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v4, p0, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;->mDownloadManager:Landroid/app/DownloadManager;

    invoke-static {v4, p1, p2, p3}, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;->downloadResource(Landroid/app/DownloadManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    .local v0, id:J
    cmp-long v4, v0, v7

    if-ltz v4, :cond_1a

    if-eqz p4, :cond_1a

    iget-object v4, p0, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;->mContext:Landroid/content/Context;

    new-array v5, v2, [J

    aput-wide v0, v5, v3

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Landroid/app/MiuiDownloadManager;->operateDownloadsNeedToUpdateProgress(Landroid/content/Context;[J[J)V

    :cond_1a
    cmp-long v4, v0, v7

    if-ltz v4, :cond_1f

    :goto_1e
    return v2

    :cond_1f
    move v2, v3

    goto :goto_1e
.end method

.method public handleDownloadFailed(Ljava/lang/String;)V
    .registers 2
    .parameter "downloadFilePath"

    .prologue
    return-void
.end method

.method public handleDownloadProgress(Ljava/lang/String;II)V
    .registers 4
    .parameter "downloadFilePath"
    .parameter "downloadBytes"
    .parameter "totalBytes"

    .prologue
    return-void
.end method

.method public handleDownloadSuccessed(Ljava/lang/String;)V
    .registers 2
    .parameter "downloadFilePath"

    .prologue
    return-void
.end method

.method public isResourceDownloading(Ljava/lang/String;)Z
    .registers 10
    .parameter "downloadSavePath"

    .prologue
    iget-object v4, p0, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;->mLastQueryFilePath:Ljava/lang/String;

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1c

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-object v6, p0, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;->mLastQueryTime:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sub-long/2addr v4, v6

    const-wide/16 v6, 0x1f4

    cmp-long v4, v4, v6

    if-gez v4, :cond_1c

    iget-boolean v3, p0, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;->mLastQueryResult:Z

    :goto_1b
    return v3

    :cond_1c
    new-instance v2, Landroid/app/MiuiDownloadManager$Query;

    invoke-direct {v2}, Landroid/app/MiuiDownloadManager$Query;-><init>()V

    .local v2, query:Landroid/app/MiuiDownloadManager$Query;
    const-string v4, "\'"

    const-string v5, "\'\'"

    invoke-virtual {p1, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .local v1, encodedFilePath:Ljava/lang/String;
    invoke-virtual {v2, v1}, Landroid/app/MiuiDownloadManager$Query;->setFilterByAppData(Ljava/lang/String;)Landroid/app/MiuiDownloadManager$Query;

    iget-object v4, p0, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;->mDownloadManager:Landroid/app/DownloadManager;

    invoke-virtual {v4, v2}, Landroid/app/DownloadManager;->query(Landroid/app/DownloadManager$Query;)Landroid/database/Cursor;

    move-result-object v0

    .local v0, cursor:Landroid/database/Cursor;
    const/4 v3, 0x0

    .local v3, result:Z
    if-eqz v0, :cond_45

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_54

    invoke-static {v0}, Landroid/app/MiuiDownloadManager;->isDownloading(Landroid/database/Cursor;)Z

    move-result v4

    if-eqz v4, :cond_54

    const/4 v3, 0x1

    :goto_42
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_45
    iput-boolean v3, p0, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;->mLastQueryResult:Z

    iput-object p1, p0, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;->mLastQueryFilePath:Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iput-object v4, p0, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;->mLastQueryTime:Ljava/lang/Long;

    goto :goto_1b

    :cond_54
    const/4 v3, 0x0

    goto :goto_42
.end method

.method public registerDownloadReceiver()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;->registerDownloadReceiver(Z)V

    return-void
.end method

.method public registerDownloadReceiver(Z)V
    .registers 5
    .parameter "listenDownloadProgress"

    .prologue
    iget-boolean v1, p0, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;->mHasRegistDownloadReceiver:Z

    if-nez v1, :cond_1f

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .local v0, f:Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.DOWNLOAD_COMPLETE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    if-eqz p1, :cond_15

    const-string v1, "android.intent.action.DOWNLOAD_UPDATED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    :cond_15
    iget-object v1, p0, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;->mDownloadReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;->mHasRegistDownloadReceiver:Z

    .end local v0           #f:Landroid/content/IntentFilter;
    :cond_1f
    return-void
.end method

.method public unregisterDownloadReceiver()V
    .registers 3

    .prologue
    iget-boolean v0, p0, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;->mHasRegistDownloadReceiver:Z

    if-eqz v0, :cond_e

    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;->mDownloadReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;->mHasRegistDownloadReceiver:Z

    :cond_e
    return-void
.end method
