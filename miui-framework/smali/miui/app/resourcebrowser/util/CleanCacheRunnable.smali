.class public Lmiui/app/resourcebrowser/util/CleanCacheRunnable;
.super Ljava/lang/Object;
.source "CleanCacheRunnable.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field protected mCacheDirPath:Ljava/lang/String;

.field protected mPolicy:Lmiui/app/resourcebrowser/util/CleanCachePolicy;

.field private mStatDeleteNumber:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 2
    .parameter "cacheDirPath"

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0, p1}, Lmiui/app/resourcebrowser/util/CleanCacheRunnable;->setCacheDirPath(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getCleanFilesNumber()I
    .registers 2

    .prologue
    iget v0, p0, Lmiui/app/resourcebrowser/util/CleanCacheRunnable;->mStatDeleteNumber:I

    return v0
.end method

.method public run()V
    .registers 3

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/CleanCacheRunnable;->mPolicy:Lmiui/app/resourcebrowser/util/CleanCachePolicy;

    if-nez v0, :cond_d

    new-instance v0, Lmiui/app/resourcebrowser/util/CleanCachePolicy$LRUCleanCacheByAccessTime;

    const/16 v1, 0x32

    invoke-direct {v0, v1}, Lmiui/app/resourcebrowser/util/CleanCachePolicy$LRUCleanCacheByAccessTime;-><init>(I)V

    iput-object v0, p0, Lmiui/app/resourcebrowser/util/CleanCacheRunnable;->mPolicy:Lmiui/app/resourcebrowser/util/CleanCachePolicy;

    :cond_d
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/CleanCacheRunnable;->mPolicy:Lmiui/app/resourcebrowser/util/CleanCachePolicy;

    iget-object v1, p0, Lmiui/app/resourcebrowser/util/CleanCacheRunnable;->mCacheDirPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lmiui/app/resourcebrowser/util/CleanCachePolicy;->cleanCache(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lmiui/app/resourcebrowser/util/CleanCacheRunnable;->mStatDeleteNumber:I

    return-void
.end method

.method public setCacheDirPath(Ljava/lang/String;)V
    .registers 2
    .parameter "cacheDirPath"

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/util/CleanCacheRunnable;->mCacheDirPath:Ljava/lang/String;

    return-void
.end method

.method public setCleanPolicy(Lmiui/app/resourcebrowser/util/CleanCachePolicy;)V
    .registers 2
    .parameter "policy"

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/util/CleanCacheRunnable;->mPolicy:Lmiui/app/resourcebrowser/util/CleanCachePolicy;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .prologue
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Total delete "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lmiui/app/resourcebrowser/util/CleanCacheRunnable;->mStatDeleteNumber:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " files of "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmiui/app/resourcebrowser/util/CleanCacheRunnable;->mCacheDirPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
