.class Lmiui/app/resourcebrowser/util/CleanCacheTask$1$1;
.super Ljava/lang/Thread;
.source "CleanCacheTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiui/app/resourcebrowser/util/CleanCacheTask$1;->queueIdle()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lmiui/app/resourcebrowser/util/CleanCacheTask$1;

.field final synthetic val$clean:Lmiui/app/resourcebrowser/util/CleanCacheRunnable;


# direct methods
.method constructor <init>(Lmiui/app/resourcebrowser/util/CleanCacheTask$1;Lmiui/app/resourcebrowser/util/CleanCacheRunnable;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/util/CleanCacheTask$1$1;->this$1:Lmiui/app/resourcebrowser/util/CleanCacheTask$1;

    iput-object p2, p0, Lmiui/app/resourcebrowser/util/CleanCacheTask$1$1;->val$clean:Lmiui/app/resourcebrowser/util/CleanCacheRunnable;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 4

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/CleanCacheTask$1$1;->val$clean:Lmiui/app/resourcebrowser/util/CleanCacheRunnable;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/util/CleanCacheRunnable;->run()V

    const-string v0, "clean"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "clean cache: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lmiui/app/resourcebrowser/util/CleanCacheTask$1$1;->val$clean:Lmiui/app/resourcebrowser/util/CleanCacheRunnable;

    invoke-virtual {v2}, Lmiui/app/resourcebrowser/util/CleanCacheRunnable;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lmiui/app/resourcebrowser/util/CleanCacheTask$1$1;->this$1:Lmiui/app/resourcebrowser/util/CleanCacheTask$1;

    iget-object v0, v0, Lmiui/app/resourcebrowser/util/CleanCacheTask$1;->this$0:Lmiui/app/resourcebrowser/util/CleanCacheTask;

    iget-object v0, v0, Lmiui/app/resourcebrowser/util/CleanCacheTask;->mCleanRunnable:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_42

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getQueue()Landroid/os/MessageQueue;

    move-result-object v0

    iget-object v1, p0, Lmiui/app/resourcebrowser/util/CleanCacheTask$1$1;->this$1:Lmiui/app/resourcebrowser/util/CleanCacheTask$1;

    iget-object v1, v1, Lmiui/app/resourcebrowser/util/CleanCacheTask$1;->this$0:Lmiui/app/resourcebrowser/util/CleanCacheTask;

    #getter for: Lmiui/app/resourcebrowser/util/CleanCacheTask;->mIdleHandler:Landroid/os/MessageQueue$IdleHandler;
    invoke-static {v1}, Lmiui/app/resourcebrowser/util/CleanCacheTask;->access$000(Lmiui/app/resourcebrowser/util/CleanCacheTask;)Landroid/os/MessageQueue$IdleHandler;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/MessageQueue;->addIdleHandler(Landroid/os/MessageQueue$IdleHandler;)V

    :cond_42
    return-void
.end method
