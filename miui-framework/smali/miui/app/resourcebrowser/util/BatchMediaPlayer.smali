.class public Lmiui/app/resourcebrowser/util/BatchMediaPlayer;
.super Ljava/lang/Object;
.source "BatchMediaPlayer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/app/resourcebrowser/util/BatchMediaPlayer$BatchPlayerListener;,
        Lmiui/app/resourcebrowser/util/BatchMediaPlayer$PlayState;
    }
.end annotation


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private final mBatchPlayRun:Ljava/lang/Runnable;

.field private mCurrentItem:I

.field private mListener:Lmiui/app/resourcebrowser/util/BatchMediaPlayer$BatchPlayerListener;

.field private mPlayList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mPlayer:Landroid/media/MediaPlayer;

.field private mState:Lmiui/app/resourcebrowser/util/BatchMediaPlayer$PlayState;

.field private mhandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .registers 4
    .parameter "activity"

    .prologue
    const/4 v0, 0x0

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mActivity:Landroid/app/Activity;

    iput-object v0, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mPlayer:Landroid/media/MediaPlayer;

    iput-object v0, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mListener:Lmiui/app/resourcebrowser/util/BatchMediaPlayer$BatchPlayerListener;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mPlayList:Ljava/util/ArrayList;

    const/4 v0, -0x1

    iput v0, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mCurrentItem:I

    sget-object v0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer$PlayState;->UNDEFINED:Lmiui/app/resourcebrowser/util/BatchMediaPlayer$PlayState;

    iput-object v0, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mState:Lmiui/app/resourcebrowser/util/BatchMediaPlayer$PlayState;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mhandler:Landroid/os/Handler;

    new-instance v0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer$1;

    invoke-direct {v0, p0}, Lmiui/app/resourcebrowser/util/BatchMediaPlayer$1;-><init>(Lmiui/app/resourcebrowser/util/BatchMediaPlayer;)V

    iput-object v0, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mBatchPlayRun:Ljava/lang/Runnable;

    if-nez p1, :cond_30

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "activity cann\'t be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_30
    iput-object p1, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mActivity:Landroid/app/Activity;

    return-void
.end method

.method static synthetic access$000(Lmiui/app/resourcebrowser/util/BatchMediaPlayer;)V
    .registers 1
    .parameter "x0"

    .prologue
    invoke-direct {p0}, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->setPlayerDataSource()V

    return-void
.end method

.method static synthetic access$100(Lmiui/app/resourcebrowser/util/BatchMediaPlayer;)Ljava/lang/Runnable;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mBatchPlayRun:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$200(Lmiui/app/resourcebrowser/util/BatchMediaPlayer;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mhandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lmiui/app/resourcebrowser/util/BatchMediaPlayer;)Landroid/app/Activity;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$400(Lmiui/app/resourcebrowser/util/BatchMediaPlayer;)V
    .registers 1
    .parameter "x0"

    .prologue
    invoke-direct {p0}, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->realPlay()V

    return-void
.end method

.method private realPlay()V
    .registers 5

    .prologue
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->isPaused()Z

    move-result v0

    if-nez v0, :cond_2c

    iget-object v0, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_2c

    iget-object v0, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mListener:Lmiui/app/resourcebrowser/util/BatchMediaPlayer$BatchPlayerListener;

    if-eqz v0, :cond_23

    iget-object v1, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mListener:Lmiui/app/resourcebrowser/util/BatchMediaPlayer$BatchPlayerListener;

    iget-object v0, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mPlayList:Ljava/util/ArrayList;

    iget v2, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mCurrentItem:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget v2, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mCurrentItem:I

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->size()I

    move-result v3

    invoke-interface {v1, v0, v2, v3}, Lmiui/app/resourcebrowser/util/BatchMediaPlayer$BatchPlayerListener;->play(Ljava/lang/String;II)V

    :cond_23
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    sget-object v0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer$PlayState;->PLAYING:Lmiui/app/resourcebrowser/util/BatchMediaPlayer$PlayState;

    iput-object v0, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mState:Lmiui/app/resourcebrowser/util/BatchMediaPlayer$PlayState;

    :cond_2c
    return-void
.end method

.method private setPlayerDataSource()V
    .registers 5

    .prologue
    :try_start_0
    iget v0, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mCurrentItem:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mCurrentItem:I

    iget-object v1, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mPlayList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_2e

    iget-object v0, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    iget-object v1, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mActivity:Landroid/app/Activity;

    iget-object v0, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mPlayList:Ljava/util/ArrayList;

    iget v3, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mCurrentItem:I

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lmiui/app/resourcebrowser/util/ResourceHelper;->getUriByPath(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepareAsync()V

    :goto_2d
    return-void

    :cond_2e
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->stop(Z)V
    :try_end_32
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_32} :catch_33

    goto :goto_2d

    :catch_33
    move-exception v0

    goto :goto_2d
.end method


# virtual methods
.method public addPlayUri(Ljava/lang/String;)V
    .registers 3
    .parameter "uriPath"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mPlayList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public getPlayedDuration()I
    .registers 2

    .prologue
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    :goto_c
    return v0

    :cond_d
    const/4 v0, 0x0

    goto :goto_c
.end method

.method public getTotalDuration()I
    .registers 2

    .prologue
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v0

    :goto_c
    return v0

    :cond_d
    const/4 v0, -0x1

    goto :goto_c
.end method

.method public isPaused()Z
    .registers 3

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mState:Lmiui/app/resourcebrowser/util/BatchMediaPlayer$PlayState;

    sget-object v1, Lmiui/app/resourcebrowser/util/BatchMediaPlayer$PlayState;->PAUSED:Lmiui/app/resourcebrowser/util/BatchMediaPlayer$PlayState;

    if-ne v0, v1, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public isPlaying()Z
    .registers 3

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mState:Lmiui/app/resourcebrowser/util/BatchMediaPlayer$PlayState;

    sget-object v1, Lmiui/app/resourcebrowser/util/BatchMediaPlayer$PlayState;->PLAYING:Lmiui/app/resourcebrowser/util/BatchMediaPlayer$PlayState;

    if-ne v0, v1, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public pause()V
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    :cond_9
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_13

    sget-object v0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer$PlayState;->PAUSED:Lmiui/app/resourcebrowser/util/BatchMediaPlayer$PlayState;

    iput-object v0, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mState:Lmiui/app/resourcebrowser/util/BatchMediaPlayer$PlayState;

    :cond_13
    return-void
.end method

.method public setListener(Lmiui/app/resourcebrowser/util/BatchMediaPlayer$BatchPlayerListener;)V
    .registers 2
    .parameter "l"

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mListener:Lmiui/app/resourcebrowser/util/BatchMediaPlayer$BatchPlayerListener;

    return-void
.end method

.method public setPlayList(Ljava/util/List;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, list:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mPlayList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    if-eqz p1, :cond_c

    iget-object v0, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mPlayList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_c
    return-void
.end method

.method public size()I
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mPlayList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public start()V
    .registers 3

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_c

    sget-object v0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer$PlayState;->PLAYING:Lmiui/app/resourcebrowser/util/BatchMediaPlayer$PlayState;

    iput-object v0, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mState:Lmiui/app/resourcebrowser/util/BatchMediaPlayer$PlayState;

    invoke-direct {p0}, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->realPlay()V

    :goto_b
    return-void

    :cond_c
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mPlayer:Landroid/media/MediaPlayer;

    iget-object v0, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mPlayer:Landroid/media/MediaPlayer;

    new-instance v1, Lmiui/app/resourcebrowser/util/BatchMediaPlayer$2;

    invoke-direct {v1, p0}, Lmiui/app/resourcebrowser/util/BatchMediaPlayer$2;-><init>(Lmiui/app/resourcebrowser/util/BatchMediaPlayer;)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mPlayer:Landroid/media/MediaPlayer;

    new-instance v1, Lmiui/app/resourcebrowser/util/BatchMediaPlayer$3;

    invoke-direct {v1, p0}, Lmiui/app/resourcebrowser/util/BatchMediaPlayer$3;-><init>(Lmiui/app/resourcebrowser/util/BatchMediaPlayer;)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mPlayer:Landroid/media/MediaPlayer;

    new-instance v1, Lmiui/app/resourcebrowser/util/BatchMediaPlayer$4;

    invoke-direct {v1, p0}, Lmiui/app/resourcebrowser/util/BatchMediaPlayer$4;-><init>(Lmiui/app/resourcebrowser/util/BatchMediaPlayer;)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mPlayer:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getVolumeControlStream()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    invoke-direct {p0}, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->setPlayerDataSource()V

    goto :goto_b
.end method

.method public stop()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->stop(Z)V

    return-void
.end method

.method public stop(Z)V
    .registers 4
    .parameter "hasError"

    .prologue
    const/4 v1, 0x0

    iget-object v0, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_27

    iget-object v0, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_17

    iget-object v0, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    :cond_17
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    iput-object v1, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mPlayer:Landroid/media/MediaPlayer;

    iget-object v0, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mListener:Lmiui/app/resourcebrowser/util/BatchMediaPlayer$BatchPlayerListener;

    if-eqz v0, :cond_27

    iget-object v0, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mListener:Lmiui/app/resourcebrowser/util/BatchMediaPlayer$BatchPlayerListener;

    invoke-interface {v0, p1}, Lmiui/app/resourcebrowser/util/BatchMediaPlayer$BatchPlayerListener;->finish(Z)V

    :cond_27
    const/4 v0, -0x1

    iput v0, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mCurrentItem:I

    sget-object v0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer$PlayState;->UNDEFINED:Lmiui/app/resourcebrowser/util/BatchMediaPlayer$PlayState;

    iput-object v0, p0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->mState:Lmiui/app/resourcebrowser/util/BatchMediaPlayer$PlayState;

    return-void
.end method
