.class public Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;
.super Ljava/lang/Object;
.source "ImageCacheDecoder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/app/resourcebrowser/util/ImageCacheDecoder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "BitmapCache"
.end annotation


# static fields
.field private static final MAGIC_INDEX:I = -0x3e7


# instance fields
.field private mCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mCacheCapacity:I

.field private mCurrentIndex:I

.field mIdList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mIndexList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lmiui/app/resourcebrowser/util/ImageCacheDecoder;


# direct methods
.method public constructor <init>(Lmiui/app/resourcebrowser/util/ImageCacheDecoder;I)V
    .registers 5
    .parameter
    .parameter "cacheCapacity"

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->this$0:Lmiui/app/resourcebrowser/util/ImageCacheDecoder;

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x3

    invoke-static {v0, p2}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->setCacheSize(I)V

    new-instance v0, Ljava/util/HashMap;

    iget v1, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mCacheCapacity:I

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mCache:Ljava/util/Map;

    new-instance v0, Ljava/util/ArrayList;

    iget v1, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mCacheCapacity:I

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mIdList:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    iget v1, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mCacheCapacity:I

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mIndexList:Ljava/util/ArrayList;

    return-void
.end method

.method private getNextRemoveCachePosition(I)I
    .registers 7
    .parameter "forIndex"

    .prologue
    iget-object v2, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mIndexList:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .local v1, selectIndex:I
    if-gez v1, :cond_58

    iget v2, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mCurrentIndex:I

    if-eq p1, v2, :cond_16

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->isFull()Z

    move-result v2

    if-eqz v2, :cond_58

    :cond_16
    iget-object v2, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mIndexList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_56

    const/4 v1, -0x1

    :goto_1f
    const/4 v0, 0x1

    .local v0, i:I
    :goto_20
    iget-object v2, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mIndexList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_58

    iget v3, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mCurrentIndex:I

    iget-object v2, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mIndexList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    sub-int v2, v3, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v3

    iget v4, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mCurrentIndex:I

    iget-object v2, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mIndexList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    sub-int v2, v4, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    if-le v3, v2, :cond_53

    move v1, v0

    :cond_53
    add-int/lit8 v0, v0, 0x1

    goto :goto_20

    .end local v0           #i:I
    :cond_56
    const/4 v1, 0x0

    goto :goto_1f

    :cond_58
    return v1
.end method


# virtual methods
.method public add(Ljava/lang/String;Landroid/graphics/Bitmap;I)V
    .registers 6
    .parameter "id"
    .parameter "b"
    .parameter "useIndex"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mCache:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    :goto_8
    return-void

    :cond_9
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->removeIdleCache(Z)Landroid/graphics/Bitmap;

    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mCache:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mIdList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mIndexList:Ljava/util/ArrayList;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_8
.end method

.method public clean()V
    .registers 5

    .prologue
    iget-object v3, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mIdList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :cond_6
    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_20

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .local v2, id:Ljava/lang/String;
    iget-object v3, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mCache:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .local v0, b:Landroid/graphics/Bitmap;
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_6

    .end local v0           #b:Landroid/graphics/Bitmap;
    .end local v2           #id:Ljava/lang/String;
    :cond_20
    iget-object v3, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mCache:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->clear()V

    iget-object v3, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mIdList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    iget-object v3, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mIndexList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method public getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .registers 3
    .parameter "id"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mCache:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getCurrentUseIndex()I
    .registers 2

    .prologue
    iget v0, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mCurrentIndex:I

    return v0
.end method

.method public inCacheScope(I)Z
    .registers 4
    .parameter "index"

    .prologue
    iget v0, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mCurrentIndex:I

    sub-int v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v1, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mCacheCapacity:I

    div-int/lit8 v1, v1, 0x2

    if-gt v0, v1, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public isFull()Z
    .registers 3

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mCache:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    iget v1, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mCacheCapacity:I

    if-lt v0, v1, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public removeIdleCache(Z)Landroid/graphics/Bitmap;
    .registers 3
    .parameter "returnBitmap"

    .prologue
    const/16 v0, -0x3e7

    invoke-virtual {p0, p1, v0}, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->removeIdleCache(ZI)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public removeIdleCache(ZI)Landroid/graphics/Bitmap;
    .registers 8
    .parameter "returnBitmap"
    .parameter "forIndex"

    .prologue
    const/4 v1, 0x0

    .local v1, ret:Landroid/graphics/Bitmap;
    invoke-direct {p0, p2}, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->getNextRemoveCachePosition(I)I

    move-result v2

    .local v2, selectIndex:I
    if-ltz v2, :cond_2c

    iget-object v3, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mIndexList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_2c

    iget-object v3, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mCache:Ljava/util/Map;

    iget-object v4, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mIdList:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .local v0, b:Landroid/graphics/Bitmap;
    if-eqz v0, :cond_22

    if-eqz p1, :cond_2d

    move-object v1, v0

    :cond_22
    :goto_22
    iget-object v3, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mIdList:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    iget-object v3, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mIndexList:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .end local v0           #b:Landroid/graphics/Bitmap;
    :cond_2c
    return-object v1

    .restart local v0       #b:Landroid/graphics/Bitmap;
    :cond_2d
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_22
.end method

.method public setCacheSize(I)V
    .registers 3
    .parameter "newSize"

    .prologue
    const/4 v0, 0x1

    if-le p1, v0, :cond_f

    iget v0, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mCacheCapacity:I

    if-eq p1, v0, :cond_f

    and-int/lit8 v0, p1, 0x1

    if-nez v0, :cond_d

    add-int/lit8 p1, p1, 0x1

    :cond_d
    iput p1, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mCacheCapacity:I

    :cond_f
    return-void
.end method

.method public setCurrentUseIndex(I)V
    .registers 2
    .parameter "index"

    .prologue
    iput p1, p0, Lmiui/app/resourcebrowser/util/ImageCacheDecoder$BitmapCache;->mCurrentIndex:I

    return-void
.end method
