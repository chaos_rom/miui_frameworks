.class public Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;
.super Ljava/lang/Object;
.source "ResourceMusicPlayer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$PlayProgressListener;
    }
.end annotation


# static fields
.field private static final INTERVAL_UPDATE_PLAY_PROGRESS:I = 0x32

.field private static final MSG_UPDATE_PLAY_PROGRESS:I


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mBatchPlayer:Lmiui/app/resourcebrowser/util/BatchMediaPlayer;

.field protected mCurrentPlayingButton:Landroid/widget/ImageView;

.field private mCurrentPlayingResource:Lmiui/app/resourcebrowser/resource/Resource;

.field private mHandler:Landroid/os/Handler;

.field private mProgressListener:Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$PlayProgressListener;

.field private mShowPlayButton:Z

.field private mUserPlaying:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;Z)V
    .registers 4
    .parameter "activity"
    .parameter "showPlayButton"

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$1;

    invoke-direct {v0, p0}, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$1;-><init>(Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;)V

    iput-object v0, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->mHandler:Landroid/os/Handler;

    iput-object p1, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->mActivity:Landroid/app/Activity;

    iput-boolean p2, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->mShowPlayButton:Z

    return-void
.end method

.method static synthetic access$000(Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;)Lmiui/app/resourcebrowser/util/BatchMediaPlayer;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->mBatchPlayer:Lmiui/app/resourcebrowser/util/BatchMediaPlayer;

    return-object v0
.end method

.method static synthetic access$100(Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;)Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$PlayProgressListener;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->mProgressListener:Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$PlayProgressListener;

    return-object v0
.end method

.method static synthetic access$200(Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;)Lmiui/app/resourcebrowser/resource/Resource;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->mCurrentPlayingResource:Lmiui/app/resourcebrowser/resource/Resource;

    return-object v0
.end method

.method static synthetic access$302(Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;Lmiui/app/resourcebrowser/resource/Resource;)Lmiui/app/resourcebrowser/resource/Resource;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->mCurrentPlayingResource:Lmiui/app/resourcebrowser/resource/Resource;

    return-object p1
.end method

.method static synthetic access$402(Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    iput-boolean p1, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->mUserPlaying:Z

    return p1
.end method

.method static synthetic access$500(Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;)Landroid/app/Activity;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method private initPlayer()V
    .registers 3

    .prologue
    new-instance v0, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;

    iget-object v1, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->mActivity:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->mBatchPlayer:Lmiui/app/resourcebrowser/util/BatchMediaPlayer;

    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->mBatchPlayer:Lmiui/app/resourcebrowser/util/BatchMediaPlayer;

    new-instance v1, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$3;

    invoke-direct {v1, p0}, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$3;-><init>(Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;)V

    invoke-virtual {v0, v1}, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->setListener(Lmiui/app/resourcebrowser/util/BatchMediaPlayer$BatchPlayerListener;)V

    return-void
.end method


# virtual methods
.method public canPlay(Lmiui/app/resourcebrowser/resource/Resource;)Z
    .registers 3
    .parameter "r"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->mCurrentPlayingResource:Lmiui/app/resourcebrowser/resource/Resource;

    invoke-virtual {p1, v0}, Lmiui/app/resourcebrowser/resource/Resource;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_14

    invoke-virtual {p1}, Lmiui/app/resourcebrowser/resource/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_14

    const/4 v0, 0x1

    :goto_13
    return v0

    :cond_14
    const/4 v0, 0x0

    goto :goto_13
.end method

.method protected getMusicPlayList(Lmiui/app/resourcebrowser/resource/Resource;)Ljava/util/List;
    .registers 3
    .parameter "resourceItem"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiui/app/resourcebrowser/resource/Resource;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->mActivity:Landroid/app/Activity;

    invoke-static {v0, p1}, Lmiui/app/resourcebrowser/util/ResourceHelper;->getDefaultMusicPlayList(Landroid/content/Context;Lmiui/app/resourcebrowser/resource/Resource;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public initPlayButtonIfNeed(Landroid/widget/ImageView;Lmiui/app/resourcebrowser/resource/Resource;)V
    .registers 4
    .parameter "view"
    .parameter "data"

    .prologue
    iget-boolean v0, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->mShowPlayButton:Z

    if-eqz v0, :cond_6

    if-nez p1, :cond_7

    :cond_6
    :goto_6
    return-void

    :cond_7
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->mCurrentPlayingResource:Lmiui/app/resourcebrowser/resource/Resource;

    invoke-virtual {p2, v0}, Lmiui/app/resourcebrowser/resource/Resource;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1e

    const v0, 0x60200dc

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_15
    new-instance v0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$2;

    invoke-direct {v0, p0, p2, p1}, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$2;-><init>(Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;Lmiui/app/resourcebrowser/resource/Resource;Landroid/widget/ImageView;)V

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_6

    :cond_1e
    const v0, 0x60200d9

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_15
.end method

.method public isPlaying()Z
    .registers 2

    .prologue
    iget-boolean v0, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->mUserPlaying:Z

    return v0
.end method

.method public playMusic(Landroid/widget/ImageView;Lmiui/app/resourcebrowser/resource/Resource;)V
    .registers 5
    .parameter "v"
    .parameter "r"

    .prologue
    iget-boolean v0, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->mShowPlayButton:Z

    if-nez v0, :cond_c

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "ResourceMusicPlayer does not support playing button."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_c
    const v0, 0x60200dc

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    iput-object p1, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->mCurrentPlayingButton:Landroid/widget/ImageView;

    invoke-virtual {p0, p2}, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->playMusic(Lmiui/app/resourcebrowser/resource/Resource;)V

    return-void
.end method

.method public playMusic(Lmiui/app/resourcebrowser/resource/Resource;)V
    .registers 4
    .parameter "r"

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->mCurrentPlayingResource:Lmiui/app/resourcebrowser/resource/Resource;

    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->mBatchPlayer:Lmiui/app/resourcebrowser/util/BatchMediaPlayer;

    if-nez v0, :cond_9

    invoke-direct {p0}, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->initPlayer()V

    :cond_9
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->mBatchPlayer:Lmiui/app/resourcebrowser/util/BatchMediaPlayer;

    invoke-virtual {p0, p1}, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->getMusicPlayList(Lmiui/app/resourcebrowser/resource/Resource;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->setPlayList(Ljava/util/List;)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->mBatchPlayer:Lmiui/app/resourcebrowser/util/BatchMediaPlayer;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->start()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->mUserPlaying:Z

    return-void
.end method

.method public regeistePlayProgressListener(Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$PlayProgressListener;)V
    .registers 2
    .parameter "l"

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->mProgressListener:Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$PlayProgressListener;

    return-void
.end method

.method public stopMusic()V
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->mBatchPlayer:Lmiui/app/resourcebrowser/util/BatchMediaPlayer;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->mBatchPlayer:Lmiui/app/resourcebrowser/util/BatchMediaPlayer;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/util/BatchMediaPlayer;->stop()V

    :cond_9
    const/4 v0, 0x0

    iput-object v0, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->mCurrentPlayingResource:Lmiui/app/resourcebrowser/resource/Resource;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->mUserPlaying:Z

    return-void
.end method
