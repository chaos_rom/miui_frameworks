.class Lmiui/app/resourcebrowser/OnlineResourceAdapter$DThumbnailStatNode;
.super Ljava/lang/Object;
.source "OnlineResourceAdapter.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/app/resourcebrowser/OnlineResourceAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DThumbnailStatNode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lmiui/app/resourcebrowser/OnlineResourceAdapter$DThumbnailStatNode;",
        ">;"
    }
.end annotation


# instance fields
.field eTime:J

.field sTime:J

.field final synthetic this$0:Lmiui/app/resourcebrowser/OnlineResourceAdapter;

.field url:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lmiui/app/resourcebrowser/OnlineResourceAdapter;)V
    .registers 2
    .parameter

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter$DThumbnailStatNode;->this$0:Lmiui/app/resourcebrowser/OnlineResourceAdapter;

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lmiui/app/resourcebrowser/OnlineResourceAdapter;Lmiui/app/resourcebrowser/OnlineResourceAdapter$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    invoke-direct {p0, p1}, Lmiui/app/resourcebrowser/OnlineResourceAdapter$DThumbnailStatNode;-><init>(Lmiui/app/resourcebrowser/OnlineResourceAdapter;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .registers 3
    .parameter "x0"

    .prologue
    check-cast p1, Lmiui/app/resourcebrowser/OnlineResourceAdapter$DThumbnailStatNode;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/app/resourcebrowser/OnlineResourceAdapter$DThumbnailStatNode;->compareTo(Lmiui/app/resourcebrowser/OnlineResourceAdapter$DThumbnailStatNode;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lmiui/app/resourcebrowser/OnlineResourceAdapter$DThumbnailStatNode;)I
    .registers 6
    .parameter "another"

    .prologue
    iget-wide v0, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter$DThumbnailStatNode;->sTime:J

    iget-wide v2, p1, Lmiui/app/resourcebrowser/OnlineResourceAdapter$DThumbnailStatNode;->sTime:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_f

    iget-wide v0, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter$DThumbnailStatNode;->sTime:J

    iget-wide v2, p1, Lmiui/app/resourcebrowser/OnlineResourceAdapter$DThumbnailStatNode;->sTime:J

    sub-long/2addr v0, v2

    :goto_d
    long-to-int v0, v0

    return v0

    :cond_f
    iget-wide v0, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter$DThumbnailStatNode;->eTime:J

    iget-wide v2, p1, Lmiui/app/resourcebrowser/OnlineResourceAdapter$DThumbnailStatNode;->eTime:J

    sub-long/2addr v0, v2

    goto :goto_d
.end method
