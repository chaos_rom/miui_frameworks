.class public Lmiui/app/resourcebrowser/OnlineResourceAdapter$DownloadThumbnailTask;
.super Lmiui/app/resourcebrowser/util/DownloadFileTask;
.source "OnlineResourceAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/app/resourcebrowser/OnlineResourceAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "DownloadThumbnailTask"
.end annotation


# instance fields
.field mDownloadEntry:Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;

.field final synthetic this$0:Lmiui/app/resourcebrowser/OnlineResourceAdapter;


# direct methods
.method public constructor <init>(Lmiui/app/resourcebrowser/OnlineResourceAdapter;Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;)V
    .registers 3
    .parameter
    .parameter "entry"

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter$DownloadThumbnailTask;->this$0:Lmiui/app/resourcebrowser/OnlineResourceAdapter;

    invoke-direct {p0}, Lmiui/app/resourcebrowser/util/DownloadFileTask;-><init>()V

    iput-object p2, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter$DownloadThumbnailTask;->mDownloadEntry:Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    .prologue
    check-cast p1, [Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/app/resourcebrowser/OnlineResourceAdapter$DownloadThumbnailTask;->doInBackground([Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;)Ljava/util/List;
    .registers 13
    .parameter "entries"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;",
            ")",
            "Ljava/util/List",
            "<",
            "Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    aget-object v7, p1, v0

    .local v7, entry:Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;
    iget-object v0, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter$DownloadThumbnailTask;->mDownloadEntry:Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;

    if-eq v7, v0, :cond_f

    new-instance v0, Ljava/security/InvalidParameterException;

    const-string v1, "DownloadThumbnailTask parameter error."

    invoke-direct {v0, v1}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_f
    sget-boolean v0, Lmiui/app/resourcebrowser/util/ResourceDebug;->DEBUG:Z

    if-eqz v0, :cond_d5

    iget-object v0, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter$DownloadThumbnailTask;->this$0:Lmiui/app/resourcebrowser/OnlineResourceAdapter;

    invoke-static {v0}, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->access$304(Lmiui/app/resourcebrowser/OnlineResourceAdapter;)I

    const-string v0, "Theme"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Start downloading thumbnail: "

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->getUrl()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v9, " parallelTask="

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v9, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter$DownloadThumbnailTask;->this$0:Lmiui/app/resourcebrowser/OnlineResourceAdapter;

    #getter for: Lmiui/app/resourcebrowser/OnlineResourceAdapter;->dTaskParallelNumber:I
    invoke-static {v9}, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->access$300(Lmiui/app/resourcebrowser/OnlineResourceAdapter;)I

    move-result v9

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .local v2, sTime:J
    invoke-super {p0, p1}, Lmiui/app/resourcebrowser/util/DownloadFileTask;->doInBackground([Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;)Ljava/util/List;

    move-result-object v8

    .local v8, ret:Ljava/util/List;,"Ljava/util/List<Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;>;"
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .local v4, eTime:J
    iget-object v0, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter$DownloadThumbnailTask;->this$0:Lmiui/app/resourcebrowser/OnlineResourceAdapter;

    invoke-virtual {v7}, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->getUrl()Ljava/lang/String;

    move-result-object v1

    #calls: Lmiui/app/resourcebrowser/OnlineResourceAdapter;->dAddIntoStat(Ljava/lang/String;JJ)V
    invoke-static/range {v0 .. v5}, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->access$400(Lmiui/app/resourcebrowser/OnlineResourceAdapter;Ljava/lang/String;JJ)V

    sub-long v0, v4, v2

    long-to-int v6, v0

    .local v6, costTime:I
    sget-object v0, Lmiui/app/resourcebrowser/util/ResourceDebug;->sMap:Ljava/util/HashMap;

    invoke-virtual {v7}, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->getUrl()Ljava/lang/String;

    move-result-object v1

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "ms-"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter$DownloadThumbnailTask;->this$0:Lmiui/app/resourcebrowser/OnlineResourceAdapter;

    #getter for: Lmiui/app/resourcebrowser/OnlineResourceAdapter;->dTaskParallelNumber:I
    invoke-static {v10}, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->access$300(Lmiui/app/resourcebrowser/OnlineResourceAdapter;)I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v1, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "Theme"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "End downloading thumbnail: "

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->getUrl()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v9, "  fileSize: "

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v9, Ljava/io/File;

    invoke-virtual {v7}, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->getPath()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v9

    invoke-virtual {v1, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v9, " costTime: "

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v9, " parallelTask: "

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v9, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter$DownloadThumbnailTask;->this$0:Lmiui/app/resourcebrowser/OnlineResourceAdapter;

    #getter for: Lmiui/app/resourcebrowser/OnlineResourceAdapter;->dTaskParallelNumber:I
    invoke-static {v9}, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->access$300(Lmiui/app/resourcebrowser/OnlineResourceAdapter;)I

    move-result v9

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter$DownloadThumbnailTask;->this$0:Lmiui/app/resourcebrowser/OnlineResourceAdapter;

    invoke-static {v0}, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->access$306(Lmiui/app/resourcebrowser/OnlineResourceAdapter;)I

    .end local v2           #sTime:J
    .end local v4           #eTime:J
    .end local v6           #costTime:I
    .end local v8           #ret:Ljava/util/List;,"Ljava/util/List<Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;>;"
    :goto_d4
    return-object v8

    :cond_d5
    invoke-super {p0, p1}, Lmiui/app/resourcebrowser/util/DownloadFileTask;->doInBackground([Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;)Ljava/util/List;

    move-result-object v8

    goto :goto_d4
.end method

.method protected onCancelled()V
    .registers 3

    .prologue
    invoke-super {p0}, Lmiui/app/resourcebrowser/util/DownloadFileTask;->onCancelled()V

    new-instance v0, Ljava/security/InvalidParameterException;

    const-string v1, "downloading thumbnail is canceled, but we don\'t handle it."

    invoke-direct {v0, v1}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 2
    .parameter "x0"

    .prologue
    check-cast p1, Ljava/util/List;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/app/resourcebrowser/OnlineResourceAdapter$DownloadThumbnailTask;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, result:Ljava/util/List;,"Ljava/util/List<Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;>;"
    if-eqz p1, :cond_d

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_d

    iget-object v2, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter$DownloadThumbnailTask;->this$0:Lmiui/app/resourcebrowser/OnlineResourceAdapter;

    invoke-virtual {v2}, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->notifyDataSetChanged()V

    :cond_d
    iget-object v0, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter$DownloadThumbnailTask;->mDownloadEntry:Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;

    .local v0, entry:Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;
    iget-object v2, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter$DownloadThumbnailTask;->this$0:Lmiui/app/resourcebrowser/OnlineResourceAdapter;

    #getter for: Lmiui/app/resourcebrowser/OnlineResourceAdapter;->mDownloadedCount:Ljava/util/HashMap;
    invoke-static {v2}, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->access$500(Lmiui/app/resourcebrowser/OnlineResourceAdapter;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .local v1, previousCount:Ljava/lang/Integer;
    if-nez v1, :cond_22

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    :cond_22
    iget-object v2, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter$DownloadThumbnailTask;->this$0:Lmiui/app/resourcebrowser/OnlineResourceAdapter;

    #getter for: Lmiui/app/resourcebrowser/OnlineResourceAdapter;->mDownloadedCount:Ljava/util/HashMap;
    invoke-static {v2}, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->access$500(Lmiui/app/resourcebrowser/OnlineResourceAdapter;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter$DownloadThumbnailTask;->this$0:Lmiui/app/resourcebrowser/OnlineResourceAdapter;

    #getter for: Lmiui/app/resourcebrowser/OnlineResourceAdapter;->mThumbnailDownloadingSet:Ljava/util/HashSet;
    invoke-static {v2}, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->access$600(Lmiui/app/resourcebrowser/OnlineResourceAdapter;)Ljava/util/HashSet;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    iget-object v2, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter$DownloadThumbnailTask;->this$0:Lmiui/app/resourcebrowser/OnlineResourceAdapter;

    #getter for: Lmiui/app/resourcebrowser/OnlineResourceAdapter;->mThumbnailDownloadTaskList:Ljava/util/ArrayList;
    invoke-static {v2}, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->access$700(Lmiui/app/resourcebrowser/OnlineResourceAdapter;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v2, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter$DownloadThumbnailTask;->this$0:Lmiui/app/resourcebrowser/OnlineResourceAdapter;

    #calls: Lmiui/app/resourcebrowser/OnlineResourceAdapter;->startOneDownloadThumbnailTask()V
    invoke-static {v2}, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->access$800(Lmiui/app/resourcebrowser/OnlineResourceAdapter;)V

    return-void
.end method
