.class Lmiui/app/resourcebrowser/OnlineResourceListFragment$2;
.super Ljava/lang/Object;
.source "OnlineResourceListFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiui/app/resourcebrowser/OnlineResourceListFragment;->setupUI()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/app/resourcebrowser/OnlineResourceListFragment;


# direct methods
.method constructor <init>(Lmiui/app/resourcebrowser/OnlineResourceListFragment;)V
    .registers 2
    .parameter

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment$2;->this$0:Lmiui/app/resourcebrowser/OnlineResourceListFragment;

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 12
    .parameter
    .parameter "view"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    iget-object v3, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment$2;->this$0:Lmiui/app/resourcebrowser/OnlineResourceListFragment;

    #getter for: Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mCategoryAdapter:Landroid/widget/ArrayAdapter;
    invoke-static {v3}, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->access$400(Lmiui/app/resourcebrowser/OnlineResourceListFragment;)Landroid/widget/ArrayAdapter;

    move-result-object v3

    invoke-virtual {v3, p3}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmiui/app/resourcebrowser/resource/ResourceCategory;

    invoke-virtual {v3}, Lmiui/app/resourcebrowser/resource/ResourceCategory;->getCode()Ljava/lang/String;

    move-result-object v0

    .local v0, categoryCode:Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, subRecommendId:Ljava/lang/String;
    iget-object v3, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment$2;->this$0:Lmiui/app/resourcebrowser/OnlineResourceListFragment;

    #getter for: Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mSubRecommendList:Landroid/widget/Spinner;
    invoke-static {v3}, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->access$500(Lmiui/app/resourcebrowser/OnlineResourceListFragment;)Landroid/widget/Spinner;

    move-result-object v3

    if-eqz v3, :cond_29

    iget-object v3, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment$2;->this$0:Lmiui/app/resourcebrowser/OnlineResourceListFragment;

    #getter for: Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mSubRecommendList:Landroid/widget/Spinner;
    invoke-static {v3}, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->access$500(Lmiui/app/resourcebrowser/OnlineResourceListFragment;)Landroid/widget/Spinner;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/app/resourcebrowser/recommended/RecommendItemData;

    .local v1, subRecommend:Lmiui/app/resourcebrowser/recommended/RecommendItemData;
    if-eqz v1, :cond_4c

    iget-object v2, v1, Lmiui/app/resourcebrowser/recommended/RecommendItemData;->itemId:Ljava/lang/String;

    .end local v1           #subRecommend:Lmiui/app/resourcebrowser/recommended/RecommendItemData;
    :cond_29
    :goto_29
    iget-object v3, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment$2;->this$0:Lmiui/app/resourcebrowser/OnlineResourceListFragment;

    iget-object v4, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment$2;->this$0:Lmiui/app/resourcebrowser/OnlineResourceListFragment;

    #getter for: Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mService:Lmiui/app/resourcebrowser/service/online/OnlineService;
    invoke-static {v4}, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->access$300(Lmiui/app/resourcebrowser/OnlineResourceListFragment;)Lmiui/app/resourcebrowser/service/online/OnlineService;

    move-result-object v4

    iget-object v5, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment$2;->this$0:Lmiui/app/resourcebrowser/OnlineResourceListFragment;

    iget-object v5, v5, Lmiui/app/resourcebrowser/ResourceListFragment;->mMetaData:Landroid/os/Bundle;

    invoke-virtual {v4, v5, v2, v0}, Lmiui/app/resourcebrowser/service/online/OnlineService;->getRecommendationListUrl(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    #setter for: Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mUrl:Ljava/lang/String;
    invoke-static {v3, v4}, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->access$202(Lmiui/app/resourcebrowser/OnlineResourceListFragment;Ljava/lang/String;)Ljava/lang/String;

    iget-object v3, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment$2;->this$0:Lmiui/app/resourcebrowser/OnlineResourceListFragment;

    iget-object v3, v3, Lmiui/app/resourcebrowser/ResourceListFragment;->mAdapter:Lmiui/app/resourcebrowser/ResourceAdapter;

    check-cast v3, Lmiui/app/resourcebrowser/OnlineResourceAdapter;

    iget-object v4, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment$2;->this$0:Lmiui/app/resourcebrowser/OnlineResourceListFragment;

    #getter for: Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mUrl:Ljava/lang/String;
    invoke-static {v4}, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->access$200(Lmiui/app/resourcebrowser/OnlineResourceListFragment;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->setUrl(Ljava/lang/String;)V

    return-void

    .restart local v1       #subRecommend:Lmiui/app/resourcebrowser/recommended/RecommendItemData;
    :cond_4c
    const/4 v2, 0x0

    goto :goto_29
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    return-void
.end method
