.class public Lmiui/app/resourcebrowser/OnlineResourceAdapter$AsyncLoadMoreResourceTask;
.super Lmiui/widget/AsyncAdapter$AsyncLoadMoreDataTask;
.source "OnlineResourceAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/app/resourcebrowser/OnlineResourceAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "AsyncLoadMoreResourceTask"
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/app/resourcebrowser/OnlineResourceAdapter;


# direct methods
.method public constructor <init>(Lmiui/app/resourcebrowser/OnlineResourceAdapter;)V
    .registers 2
    .parameter

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter$AsyncLoadMoreResourceTask;->this$0:Lmiui/app/resourcebrowser/OnlineResourceAdapter;

    invoke-direct {p0, p1}, Lmiui/widget/AsyncAdapter$AsyncLoadMoreDataTask;-><init>(Lmiui/widget/AsyncAdapter;)V

    return-void
.end method


# virtual methods
.method protected loadMoreData(Lmiui/widget/AsyncAdapter$AsyncLoadMoreParams;)Ljava/util/List;
    .registers 14
    .parameter "params"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiui/widget/AsyncAdapter$AsyncLoadMoreParams;",
            ")",
            "Ljava/util/List",
            "<",
            "Lmiui/app/resourcebrowser/resource/Resource;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    iget-boolean v6, p1, Lmiui/widget/AsyncAdapter$AsyncLoadMoreParams;->upwards:Z

    if-eqz v6, :cond_8

    const/4 v2, 0x0

    :cond_7
    :goto_7
    return-object v2

    :cond_8
    const/4 v2, 0x0

    .local v2, resources:Ljava/util/List;,"Ljava/util/List<Lmiui/app/resourcebrowser/resource/Resource;>;"
    new-instance v4, Lmiui/app/resourcebrowser/util/DownloadFileTask;

    invoke-direct {v4}, Lmiui/app/resourcebrowser/util/DownloadFileTask;-><init>()V

    .local v4, task:Lmiui/app/resourcebrowser/util/DownloadFileTask;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter$AsyncLoadMoreResourceTask;->this$0:Lmiui/app/resourcebrowser/OnlineResourceAdapter;

    #getter for: Lmiui/app/resourcebrowser/OnlineResourceAdapter;->mUrl:Ljava/lang/String;
    invoke-static {v7}, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->access$000(Lmiui/app/resourcebrowser/OnlineResourceAdapter;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "&start=%s&count=%s"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    iget v9, p1, Lmiui/widget/AsyncAdapter$AsyncLoadMoreParams;->cursor:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    const/16 v9, 0x1e

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v11

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .local v5, url:Ljava/lang/String;
    sget-boolean v6, Lmiui/app/resourcebrowser/util/ResourceDebug;->DEBUG:Z

    if-eqz v6, :cond_5a

    const-string v6, "Theme"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Online list page request more data: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5a
    new-instance v0, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;

    invoke-direct {v0}, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;-><init>()V

    .local v0, entry:Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;
    invoke-virtual {v0, v5}, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->setUrl(Ljava/lang/String;)V

    iget-object v6, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter$AsyncLoadMoreResourceTask;->this$0:Lmiui/app/resourcebrowser/OnlineResourceAdapter;

    #getter for: Lmiui/app/resourcebrowser/OnlineResourceAdapter;->mListFolder:Ljava/lang/String;
    invoke-static {v6}, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->access$100(Lmiui/app/resourcebrowser/OnlineResourceAdapter;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v5}, Lmiui/app/resourcebrowser/util/ResourceHelper;->getFilePathByURL(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->setPath(Ljava/lang/String;)V

    new-array v6, v11, [Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;

    aput-object v0, v6, v10

    invoke-virtual {v4, v6}, Lmiui/app/resourcebrowser/util/DownloadFileTask;->doInForeground([Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;)Ljava/util/List;

    move-result-object v3

    .local v3, result:Ljava/util/List;,"Ljava/util/List<Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;>;"
    const/4 v1, 0x0

    .local v1, filePath:Ljava/lang/String;
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    if-lez v6, :cond_88

    invoke-interface {v3, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;

    invoke-virtual {v6}, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->getPath()Ljava/lang/String;

    move-result-object v1

    :cond_88
    if-eqz v1, :cond_7

    iget-object v6, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter$AsyncLoadMoreResourceTask;->this$0:Lmiui/app/resourcebrowser/OnlineResourceAdapter;

    #getter for: Lmiui/app/resourcebrowser/OnlineResourceAdapter;->mParser:Lmiui/app/resourcebrowser/service/ResourceDataParser;
    invoke-static {v6}, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->access$200(Lmiui/app/resourcebrowser/OnlineResourceAdapter;)Lmiui/app/resourcebrowser/service/ResourceDataParser;

    move-result-object v6

    iget-object v7, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter$AsyncLoadMoreResourceTask;->this$0:Lmiui/app/resourcebrowser/OnlineResourceAdapter;

    iget-object v7, v7, Lmiui/app/resourcebrowser/ResourceAdapter;->mMetaData:Landroid/os/Bundle;

    invoke-virtual {v6, v1, v7}, Lmiui/app/resourcebrowser/service/ResourceDataParser;->readResources(Ljava/lang/String;Landroid/os/Bundle;)Ljava/util/List;

    move-result-object v2

    goto/16 :goto_7
.end method
