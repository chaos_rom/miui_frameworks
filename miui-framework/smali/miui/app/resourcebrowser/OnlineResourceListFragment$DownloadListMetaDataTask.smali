.class public Lmiui/app/resourcebrowser/OnlineResourceListFragment$DownloadListMetaDataTask;
.super Lmiui/app/resourcebrowser/util/DownloadFileTask;
.source "OnlineResourceListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/app/resourcebrowser/OnlineResourceListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "DownloadListMetaDataTask"
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/app/resourcebrowser/OnlineResourceListFragment;


# direct methods
.method public constructor <init>(Lmiui/app/resourcebrowser/OnlineResourceListFragment;)V
    .registers 2
    .parameter

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment$DownloadListMetaDataTask;->this$0:Lmiui/app/resourcebrowser/OnlineResourceListFragment;

    invoke-direct {p0}, Lmiui/app/resourcebrowser/util/DownloadFileTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 2
    .parameter "x0"

    .prologue
    check-cast p1, Ljava/util/List;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/app/resourcebrowser/OnlineResourceListFragment$DownloadListMetaDataTask;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, result:Ljava/util/List;,"Ljava/util/List<Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;>;"
    const/4 v0, 0x0

    .local v0, filePath:Ljava/lang/String;
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_12

    const/4 v1, 0x0

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;

    invoke-virtual {v1}, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->getPath()Ljava/lang/String;

    move-result-object v0

    :cond_12
    if-eqz v0, :cond_2f

    iget-object v1, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment$DownloadListMetaDataTask;->this$0:Lmiui/app/resourcebrowser/OnlineResourceListFragment;

    invoke-virtual {v1}, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_2f

    iget-object v1, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment$DownloadListMetaDataTask;->this$0:Lmiui/app/resourcebrowser/OnlineResourceListFragment;

    iget-object v2, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment$DownloadListMetaDataTask;->this$0:Lmiui/app/resourcebrowser/OnlineResourceListFragment;

    #getter for: Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mParser:Lmiui/app/resourcebrowser/service/ResourceDataParser;
    invoke-static {v2}, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->access$600(Lmiui/app/resourcebrowser/OnlineResourceListFragment;)Lmiui/app/resourcebrowser/service/ResourceDataParser;

    move-result-object v2

    iget-object v3, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment$DownloadListMetaDataTask;->this$0:Lmiui/app/resourcebrowser/OnlineResourceListFragment;

    iget-object v3, v3, Lmiui/app/resourcebrowser/ResourceListFragment;->mMetaData:Landroid/os/Bundle;

    invoke-virtual {v2, v0, v3}, Lmiui/app/resourcebrowser/service/ResourceDataParser;->readListMetaData(Ljava/lang/String;Landroid/os/Bundle;)Lmiui/app/resourcebrowser/resource/ListMetaData;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->setListMetaData(Lmiui/app/resourcebrowser/resource/ListMetaData;)V

    :cond_2f
    iget-object v1, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment$DownloadListMetaDataTask;->this$0:Lmiui/app/resourcebrowser/OnlineResourceListFragment;

    #getter for: Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mDownloadSet:Ljava/util/Set;
    invoke-static {v1}, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->access$700(Lmiui/app/resourcebrowser/OnlineResourceListFragment;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method
