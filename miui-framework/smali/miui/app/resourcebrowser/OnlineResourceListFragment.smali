.class public Lmiui/app/resourcebrowser/OnlineResourceListFragment;
.super Lmiui/app/resourcebrowser/ResourceListFragment;
.source "OnlineResourceListFragment.java"

# interfaces
.implements Lmiui/os/AsyncTaskObserver;
.implements Lmiui/app/resourcebrowser/service/online/OnlineProtocolConstants;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/app/resourcebrowser/OnlineResourceListFragment$DownloadListMetaDataTask;,
        Lmiui/app/resourcebrowser/OnlineResourceListFragment$DownloadRecommendationListTask;,
        Lmiui/app/resourcebrowser/OnlineResourceListFragment$DownloadCategoryListTask;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmiui/app/resourcebrowser/ResourceListFragment;",
        "Lmiui/os/AsyncTaskObserver",
        "<",
        "Ljava/lang/Void;",
        "Lmiui/app/resourcebrowser/resource/Resource;",
        "Ljava/util/List",
        "<",
        "Lmiui/app/resourcebrowser/resource/Resource;",
        ">;>;",
        "Lmiui/app/resourcebrowser/service/online/OnlineProtocolConstants;"
    }
.end annotation


# static fields
.field private static final CONFIRM_MIUI_DISCLAIMER_REQUEST:I = 0x3e8


# instance fields
.field private mCategoryAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Lmiui/app/resourcebrowser/resource/ResourceCategory;",
            ">;"
        }
    .end annotation
.end field

.field private mCategoryFolder:Ljava/lang/String;

.field private mCategoryList:Landroid/widget/Spinner;

.field private mDownloadSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lmiui/app/resourcebrowser/util/DownloadFileTask;",
            ">;"
        }
    .end annotation
.end field

.field private mFirstVisiable:Z

.field private mHasSubRecommends:Z

.field private mIsRecommendList:Z

.field private mListFolder:Ljava/lang/String;

.field private mParser:Lmiui/app/resourcebrowser/service/ResourceDataParser;

.field private mRecommendationFolder:Ljava/lang/String;

.field private mService:Lmiui/app/resourcebrowser/service/online/OnlineService;

.field private mSubRecommendAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Lmiui/app/resourcebrowser/recommended/RecommendItemData;",
            ">;"
        }
    .end annotation
.end field

.field private mSubRecommendList:Landroid/widget/Spinner;

.field private mTextView:Landroid/widget/TextView;

.field private mUnevenGrid:Lmiui/app/resourcebrowser/recommended/UnevenGrid;

.field private mUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    invoke-direct {p0}, Lmiui/app/resourcebrowser/ResourceListFragment;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mDownloadSet:Ljava/util/Set;

    invoke-static {}, Lmiui/app/resourcebrowser/service/online/OnlineService;->getInstance()Lmiui/app/resourcebrowser/service/online/OnlineService;

    move-result-object v0

    iput-object v0, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mService:Lmiui/app/resourcebrowser/service/online/OnlineService;

    invoke-static {}, Lmiui/app/resourcebrowser/service/ResourceJSONDataParser;->getInstance()Lmiui/app/resourcebrowser/service/ResourceJSONDataParser;

    move-result-object v0

    iput-object v0, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mParser:Lmiui/app/resourcebrowser/service/ResourceDataParser;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mFirstVisiable:Z

    return-void
.end method

.method static synthetic access$000(Lmiui/app/resourcebrowser/OnlineResourceListFragment;)Landroid/widget/ArrayAdapter;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mSubRecommendAdapter:Landroid/widget/ArrayAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lmiui/app/resourcebrowser/OnlineResourceListFragment;)Landroid/widget/Spinner;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mCategoryList:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic access$200(Lmiui/app/resourcebrowser/OnlineResourceListFragment;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$202(Lmiui/app/resourcebrowser/OnlineResourceListFragment;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mUrl:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lmiui/app/resourcebrowser/OnlineResourceListFragment;)Lmiui/app/resourcebrowser/service/online/OnlineService;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mService:Lmiui/app/resourcebrowser/service/online/OnlineService;

    return-object v0
.end method

.method static synthetic access$400(Lmiui/app/resourcebrowser/OnlineResourceListFragment;)Landroid/widget/ArrayAdapter;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mCategoryAdapter:Landroid/widget/ArrayAdapter;

    return-object v0
.end method

.method static synthetic access$500(Lmiui/app/resourcebrowser/OnlineResourceListFragment;)Landroid/widget/Spinner;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mSubRecommendList:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic access$600(Lmiui/app/resourcebrowser/OnlineResourceListFragment;)Lmiui/app/resourcebrowser/service/ResourceDataParser;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mParser:Lmiui/app/resourcebrowser/service/ResourceDataParser;

    return-object v0
.end method

.method static synthetic access$700(Lmiui/app/resourcebrowser/OnlineResourceListFragment;)Ljava/util/Set;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mDownloadSet:Ljava/util/Set;

    return-object v0
.end method

.method private getCategoryHeader()Lmiui/app/resourcebrowser/resource/ResourceCategory;
    .registers 3

    .prologue
    new-instance v0, Lmiui/app/resourcebrowser/resource/ResourceCategory;

    invoke-direct {v0}, Lmiui/app/resourcebrowser/resource/ResourceCategory;-><init>()V

    .local v0, category:Lmiui/app/resourcebrowser/resource/ResourceCategory;
    const v1, 0x60c0027

    invoke-virtual {p0, v1}, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/app/resourcebrowser/resource/ResourceCategory;->setName(Ljava/lang/String;)V

    return-object v0
.end method

.method private onlyShowSeeMoreTextView(Z)V
    .registers 5
    .parameter "yes"

    .prologue
    const/4 v1, 0x0

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v2, 0x60b0050

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz p1, :cond_24

    move v0, v1

    :goto_f
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v2, 0x60b004e

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz p1, :cond_20

    const/4 v1, 0x4

    :cond_20
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_24
    const/16 v0, 0x8

    goto :goto_f
.end method


# virtual methods
.method protected addMetaData(Landroid/os/Bundle;)V
    .registers 6
    .parameter "metaData"

    .prologue
    invoke-super {p0, p1}, Lmiui/app/resourcebrowser/ResourceListFragment;->addMetaData(Landroid/os/Bundle;)V

    const-string v0, ""

    .local v0, id:Ljava/lang/String;
    iget-boolean v1, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mIsRecommendList:Z

    if-eqz v1, :cond_11

    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mMetaData:Landroid/os/Bundle;

    const-string v2, "miui.app.resourcebrowser.RECOMMENDATION_ID"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_11
    const-string v1, "miui.app.resourcebrowser.RESOURCE_SET_SUBPACKAGE"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ".online"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected computeOnlineResourceStatus()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    iget-object v2, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mAdapter:Lmiui/app/resourcebrowser/ResourceAdapter;

    invoke-virtual {v2}, Lmiui/app/resourcebrowser/ResourceAdapter;->getResourceSet()Lmiui/app/resourcebrowser/resource/ResourceSet;

    move-result-object v2

    invoke-virtual {v2}, Lmiui/app/resourcebrowser/resource/ResourceSet;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_e

    :cond_d
    return-void

    :cond_e
    iget-object v2, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mAdapter:Lmiui/app/resourcebrowser/ResourceAdapter;

    invoke-virtual {v2}, Lmiui/app/resourcebrowser/ResourceAdapter;->getResourceSet()Lmiui/app/resourcebrowser/resource/ResourceSet;

    move-result-object v2

    invoke-virtual {v2, v4}, Lmiui/app/resourcebrowser/resource/ResourceSet;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/widget/DataGroup;

    invoke-virtual {v2}, Lmiui/widget/DataGroup;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_1e
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_d

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/app/resourcebrowser/resource/Resource;

    .local v1, r:Lmiui/app/resourcebrowser/resource/Resource;
    new-instance v2, Ljava/io/File;

    invoke-virtual {v1}, Lmiui/app/resourcebrowser/resource/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_3d

    invoke-virtual {v1, v4}, Lmiui/app/resourcebrowser/resource/Resource;->updateStatus(I)V

    goto :goto_1e

    :cond_3d
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lmiui/app/resourcebrowser/resource/Resource;->updateStatus(I)V

    goto :goto_1e
.end method

.method protected getAdapter()Lmiui/app/resourcebrowser/ResourceAdapter;
    .registers 3

    .prologue
    new-instance v0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;

    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mMetaData:Landroid/os/Bundle;

    invoke-direct {v0, p0, v1}, Lmiui/app/resourcebrowser/OnlineResourceAdapter;-><init>(Lmiui/app/resourcebrowser/BaseFragment;Landroid/os/Bundle;)V

    return-object v0
.end method

.method protected getContentView()I
    .registers 2

    .prologue
    const v0, 0x6030018

    return v0
.end method

.method protected getHeaderView()Landroid/view/View;
    .registers 2

    .prologue
    iget-boolean v0, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mIsRecommendList:Z

    if-nez v0, :cond_7

    iget-object v0, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mUnevenGrid:Lmiui/app/resourcebrowser/recommended/UnevenGrid;

    :goto_6
    return-object v0

    :cond_7
    iget-boolean v0, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mHasSubRecommends:Z

    if-nez v0, :cond_e

    iget-object v0, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mTextView:Landroid/widget/TextView;

    goto :goto_6

    :cond_e
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .registers 6
    .parameter "savedInstanceState"

    .prologue
    invoke-super {p0, p1}, Lmiui/app/resourcebrowser/ResourceListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "confirm_miui_disclaimer"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1f

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MIUI_DISCLAIMER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .local v0, i:Landroid/content/Intent;
    const/16 v1, 0x3e8

    invoke-virtual {p0, v0, v1}, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .end local v0           #i:Landroid/content/Intent;
    :cond_1f
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .registers 5
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    const/16 v0, 0x3e8

    if-ne p1, v0, :cond_c

    if-nez p2, :cond_c

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :goto_b
    return-void

    :cond_c
    invoke-super {p0, p1, p2, p3}, Lmiui/app/resourcebrowser/ResourceListFragment;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_b
.end method

.method public onResume()V
    .registers 2

    .prologue
    invoke-super {p0}, Lmiui/app/resourcebrowser/ResourceListFragment;->onResume()V

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mMetaData:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->addMetaData(Landroid/os/Bundle;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->onlyShowSeeMoreTextView(Z)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mAdapter:Lmiui/app/resourcebrowser/ResourceAdapter;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/ResourceAdapter;->refreshDataSet()V

    return-void
.end method

.method protected onVisiableChanged(Z)V
    .registers 4
    .parameter "visiableForUser"

    .prologue
    const/4 v1, 0x0

    invoke-super {p0, p1}, Lmiui/app/resourcebrowser/ResourceListFragment;->onVisiableChanged(Z)V

    if-eqz p1, :cond_18

    iget-boolean v0, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mFirstVisiable:Z

    if-eqz v0, :cond_19

    iput-boolean v1, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mFirstVisiable:Z

    invoke-direct {p0, v1}, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->onlyShowSeeMoreTextView(Z)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mAdapter:Lmiui/app/resourcebrowser/ResourceAdapter;

    check-cast v0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;

    iget-object v1, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->setUrl(Ljava/lang/String;)V

    :cond_18
    :goto_18
    return-void

    :cond_19
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->computeOnlineResourceStatus()V

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mAdapter:Lmiui/app/resourcebrowser/ResourceAdapter;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/ResourceAdapter;->notifyDataSetChanged()V

    goto :goto_18
.end method

.method protected pickMetaData(Landroid/os/Bundle;)V
    .registers 4
    .parameter "metaData"

    .prologue
    invoke-super {p0, p1}, Lmiui/app/resourcebrowser/ResourceListFragment;->pickMetaData(Landroid/os/Bundle;)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mMetaData:Landroid/os/Bundle;

    const-string v1, "miui.app.resourcebrowser.IS_RECOMMENDATION_LIST"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mIsRecommendList:Z

    return-void
.end method

.method protected requestCategories()V
    .registers 8

    .prologue
    iget-object v5, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mService:Lmiui/app/resourcebrowser/service/online/OnlineService;

    iget-object v6, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mMetaData:Landroid/os/Bundle;

    invoke-virtual {v5, v6}, Lmiui/app/resourcebrowser/service/online/OnlineService;->getCategoryUrl(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v4

    .local v4, url:Ljava/lang/String;
    iget-object v5, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mCategoryFolder:Ljava/lang/String;

    invoke-static {v5, v4}, Lmiui/app/resourcebrowser/util/ResourceHelper;->getFilePathByURL(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .local v2, filePath:Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .local v1, file:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_2b

    iget-object v5, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mParser:Lmiui/app/resourcebrowser/service/ResourceDataParser;

    iget-object v6, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mMetaData:Landroid/os/Bundle;

    invoke-virtual {v5, v2, v6}, Lmiui/app/resourcebrowser/service/ResourceDataParser;->readCategories(Ljava/lang/String;Landroid/os/Bundle;)Ljava/util/List;

    move-result-object v5

    invoke-virtual {p0, v5}, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->setCategories(Ljava/util/List;)V

    invoke-static {v1}, Lmiui/app/resourcebrowser/util/ResourceHelper;->isCacheValid(Ljava/io/File;)Z

    move-result v5

    if-eqz v5, :cond_2b

    :cond_2a
    :goto_2a
    return-void

    :cond_2b
    new-instance v3, Lmiui/app/resourcebrowser/OnlineResourceListFragment$DownloadCategoryListTask;

    invoke-direct {v3, p0}, Lmiui/app/resourcebrowser/OnlineResourceListFragment$DownloadCategoryListTask;-><init>(Lmiui/app/resourcebrowser/OnlineResourceListFragment;)V

    .local v3, task:Lmiui/app/resourcebrowser/OnlineResourceListFragment$DownloadCategoryListTask;
    const-string v5, "category"

    invoke-virtual {v3, v5}, Lmiui/app/resourcebrowser/OnlineResourceListFragment$DownloadCategoryListTask;->setId(Ljava/lang/String;)V

    new-instance v0, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;

    invoke-direct {v0}, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;-><init>()V

    .local v0, entry:Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;
    invoke-virtual {v0, v4}, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->setUrl(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->setPath(Ljava/lang/String;)V

    iget-object v5, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mDownloadSet:Ljava/util/Set;

    invoke-interface {v5, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2a

    iget-object v5, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mDownloadSet:Ljava/util/Set;

    invoke-interface {v5, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const/4 v5, 0x1

    new-array v5, v5, [Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-virtual {v3, v5}, Lmiui/app/resourcebrowser/OnlineResourceListFragment$DownloadCategoryListTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_2a
.end method

.method protected requestListMetaData(Ljava/lang/String;)V
    .registers 9
    .parameter "id"

    .prologue
    iget-object v5, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mService:Lmiui/app/resourcebrowser/service/online/OnlineService;

    iget-object v6, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mMetaData:Landroid/os/Bundle;

    invoke-virtual {v5, v6, p1}, Lmiui/app/resourcebrowser/service/online/OnlineService;->getListMetaDataUrl(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .local v4, url:Ljava/lang/String;
    iget-object v5, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mListFolder:Ljava/lang/String;

    invoke-static {v5, v4}, Lmiui/app/resourcebrowser/util/ResourceHelper;->getFilePathByURL(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .local v2, filePath:Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .local v1, file:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_2b

    iget-object v5, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mParser:Lmiui/app/resourcebrowser/service/ResourceDataParser;

    iget-object v6, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mMetaData:Landroid/os/Bundle;

    invoke-virtual {v5, v2, v6}, Lmiui/app/resourcebrowser/service/ResourceDataParser;->readListMetaData(Ljava/lang/String;Landroid/os/Bundle;)Lmiui/app/resourcebrowser/resource/ListMetaData;

    move-result-object v5

    invoke-virtual {p0, v5}, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->setListMetaData(Lmiui/app/resourcebrowser/resource/ListMetaData;)V

    invoke-static {v1}, Lmiui/app/resourcebrowser/util/ResourceHelper;->isCacheValid(Ljava/io/File;)Z

    move-result v5

    if-eqz v5, :cond_2b

    :cond_2a
    :goto_2a
    return-void

    :cond_2b
    new-instance v3, Lmiui/app/resourcebrowser/OnlineResourceListFragment$DownloadListMetaDataTask;

    invoke-direct {v3, p0}, Lmiui/app/resourcebrowser/OnlineResourceListFragment$DownloadListMetaDataTask;-><init>(Lmiui/app/resourcebrowser/OnlineResourceListFragment;)V

    .local v3, task:Lmiui/app/resourcebrowser/OnlineResourceListFragment$DownloadListMetaDataTask;
    const-string v5, "listmetadata"

    invoke-virtual {v3, v5}, Lmiui/app/resourcebrowser/OnlineResourceListFragment$DownloadListMetaDataTask;->setId(Ljava/lang/String;)V

    new-instance v0, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;

    invoke-direct {v0}, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;-><init>()V

    .local v0, entry:Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;
    invoke-virtual {v0, v4}, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->setUrl(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->setPath(Ljava/lang/String;)V

    iget-object v5, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mDownloadSet:Ljava/util/Set;

    invoke-interface {v5, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2a

    iget-object v5, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mDownloadSet:Ljava/util/Set;

    invoke-interface {v5, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const/4 v5, 0x1

    new-array v5, v5, [Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-virtual {v3, v5}, Lmiui/app/resourcebrowser/OnlineResourceListFragment$DownloadListMetaDataTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_2a
.end method

.method protected requestRecommendations()V
    .registers 8

    .prologue
    iget-object v5, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mService:Lmiui/app/resourcebrowser/service/online/OnlineService;

    iget-object v6, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mMetaData:Landroid/os/Bundle;

    invoke-virtual {v5, v6}, Lmiui/app/resourcebrowser/service/online/OnlineService;->getRecommendationUrl(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v4

    .local v4, url:Ljava/lang/String;
    iget-object v5, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mRecommendationFolder:Ljava/lang/String;

    invoke-static {v5, v4}, Lmiui/app/resourcebrowser/util/ResourceHelper;->getFilePathByURL(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .local v2, filePath:Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .local v1, file:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_2b

    iget-object v5, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mParser:Lmiui/app/resourcebrowser/service/ResourceDataParser;

    iget-object v6, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mMetaData:Landroid/os/Bundle;

    invoke-virtual {v5, v2, v6}, Lmiui/app/resourcebrowser/service/ResourceDataParser;->readRecommendations(Ljava/lang/String;Landroid/os/Bundle;)Ljava/util/List;

    move-result-object v5

    invoke-virtual {p0, v5}, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->setRecommendations(Ljava/util/List;)V

    invoke-static {v1}, Lmiui/app/resourcebrowser/util/ResourceHelper;->isCacheValid(Ljava/io/File;)Z

    move-result v5

    if-eqz v5, :cond_2b

    :cond_2a
    :goto_2a
    return-void

    :cond_2b
    new-instance v3, Lmiui/app/resourcebrowser/OnlineResourceListFragment$DownloadRecommendationListTask;

    invoke-direct {v3, p0}, Lmiui/app/resourcebrowser/OnlineResourceListFragment$DownloadRecommendationListTask;-><init>(Lmiui/app/resourcebrowser/OnlineResourceListFragment;)V

    .local v3, task:Lmiui/app/resourcebrowser/OnlineResourceListFragment$DownloadRecommendationListTask;
    const-string v5, "recommendation"

    invoke-virtual {v3, v5}, Lmiui/app/resourcebrowser/OnlineResourceListFragment$DownloadRecommendationListTask;->setId(Ljava/lang/String;)V

    new-instance v0, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;

    invoke-direct {v0}, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;-><init>()V

    .local v0, entry:Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;
    invoke-virtual {v0, v4}, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->setUrl(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->setPath(Ljava/lang/String;)V

    iget-object v5, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mDownloadSet:Ljava/util/Set;

    invoke-interface {v5, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2a

    iget-object v5, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mDownloadSet:Ljava/util/Set;

    invoke-interface {v5, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const/4 v5, 0x1

    new-array v5, v5, [Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-virtual {v3, v5}, Lmiui/app/resourcebrowser/OnlineResourceListFragment$DownloadRecommendationListTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_2a
.end method

.method protected setCategories(Ljava/util/List;)V
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lmiui/app/resourcebrowser/resource/ResourceCategory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, categories:Ljava/util/List;,"Ljava/util/List<Lmiui/app/resourcebrowser/resource/ResourceCategory;>;"
    if-eqz p1, :cond_23

    iget-object v1, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mCategoryAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1}, Landroid/widget/ArrayAdapter;->clear()V

    iget-object v1, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mCategoryAdapter:Landroid/widget/ArrayAdapter;

    invoke-direct {p0}, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->getCategoryHeader()Lmiui/app/resourcebrowser/resource/ResourceCategory;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    const/4 v0, 0x0

    .local v0, i:I
    :goto_11
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_23

    iget-object v1, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mCategoryAdapter:Landroid/widget/ArrayAdapter;

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_11

    .end local v0           #i:I
    :cond_23
    return-void
.end method

.method protected setListMetaData(Lmiui/app/resourcebrowser/resource/ListMetaData;)V
    .registers 4
    .parameter "listMetaData"

    .prologue
    if-eqz p1, :cond_12

    iget-object v0, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mTextView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lmiui/app/resourcebrowser/resource/ListMetaData;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mTextView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_11
    return-void

    :cond_12
    iget-object v0, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mTextView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_11
.end method

.method protected setRecommendations(Ljava/util/List;)V
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lmiui/app/resourcebrowser/recommended/RecommendItemData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, recommendations:Ljava/util/List;,"Ljava/util/List<Lmiui/app/resourcebrowser/recommended/RecommendItemData;>;"
    if-eqz p1, :cond_e

    iget-object v0, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mUnevenGrid:Lmiui/app/resourcebrowser/recommended/UnevenGrid;

    invoke-virtual {v0, p1}, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->updateData(Ljava/util/List;)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mUnevenGrid:Lmiui/app/resourcebrowser/recommended/UnevenGrid;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->setVisibility(I)V

    :goto_d
    return-void

    :cond_e
    iget-object v0, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mUnevenGrid:Lmiui/app/resourcebrowser/recommended/UnevenGrid;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->setVisibility(I)V

    goto :goto_d
.end method

.method protected setSubRecommendations(Ljava/util/List;)V
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lmiui/app/resourcebrowser/recommended/RecommendItemData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, recommends:Ljava/util/List;,"Ljava/util/List<Lmiui/app/resourcebrowser/recommended/RecommendItemData;>;"
    if-eqz p1, :cond_1a

    iget-object v1, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mSubRecommendAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1}, Landroid/widget/ArrayAdapter;->clear()V

    const/4 v0, 0x0

    .local v0, i:I
    :goto_8
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1a

    iget-object v1, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mSubRecommendAdapter:Landroid/widget/ArrayAdapter;

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .end local v0           #i:I
    :cond_1a
    return-void
.end method

.method protected setupUI()V
    .registers 14

    .prologue
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v10, Lmiui/app/resourcebrowser/ResourceConstants;->CATEGORY_PATH:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mResourceSetCode:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lmiui/os/ExtraFileUtils;->standardizeFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mCategoryFolder:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v10, Lmiui/app/resourcebrowser/ResourceConstants;->RECOMMENDATION_PATH:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mResourceSetCode:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lmiui/os/ExtraFileUtils;->standardizeFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mRecommendationFolder:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mMetaData:Landroid/os/Bundle;

    const-string v11, "miui.app.resourcebrowser.CACHE_LIST_FOLDER"

    invoke-virtual {v10, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lmiui/os/ExtraFileUtils;->standardizeFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mResourceSetCode:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lmiui/os/ExtraFileUtils;->standardizeFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mListFolder:Ljava/lang/String;

    iget-object v9, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mMetaData:Landroid/os/Bundle;

    const-string v10, "miui.app.resourcebrowser.SUB_RECOMMENDATIONS"

    invoke-virtual {v9, v10}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v8

    check-cast v8, Ljava/util/List;

    .local v8, subRecommends:Ljava/util/List;,"Ljava/util/List<Lmiui/app/resourcebrowser/recommended/RecommendItemData;>;"
    iget-boolean v9, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mIsRecommendList:Z

    if-eqz v9, :cond_124

    if-eqz v8, :cond_124

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v9

    if-lez v9, :cond_124

    const/4 v9, 0x1

    :goto_72
    iput-boolean v9, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mHasSubRecommends:Z

    iget-boolean v9, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mIsRecommendList:Z

    if-nez v9, :cond_127

    new-instance v9, Lmiui/app/resourcebrowser/recommended/UnevenGrid;

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    invoke-direct {v9, v10}, Lmiui/app/resourcebrowser/recommended/UnevenGrid;-><init>(Landroid/content/Context;)V

    iput-object v9, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mUnevenGrid:Lmiui/app/resourcebrowser/recommended/UnevenGrid;

    iget-object v9, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mUnevenGrid:Lmiui/app/resourcebrowser/recommended/UnevenGrid;

    new-instance v10, Lmiui/app/resourcebrowser/recommended/RecommendGridItemFactory;

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v11

    iget-object v12, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mMetaData:Landroid/os/Bundle;

    invoke-direct {v10, v11, v12}, Lmiui/app/resourcebrowser/recommended/RecommendGridItemFactory;-><init>(Landroid/content/Context;Landroid/os/Bundle;)V

    invoke-virtual {v9, v10}, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->setGridItemFactory(Lmiui/app/resourcebrowser/recommended/UnevenGrid$GridItemFactory;)V

    iget-object v9, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mUnevenGrid:Lmiui/app/resourcebrowser/recommended/UnevenGrid;

    const v10, 0x602018e

    invoke-virtual {v9, v10}, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->setBackgroundResource(I)V

    const/4 v0, 0x2

    .local v0, columnCount:I
    iget-object v9, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v9}, Lmiui/app/resourcebrowser/util/ResourceHelper;->getThumbnailGap(Landroid/content/Context;)I

    move-result v3

    .local v3, gridGap:I
    mul-int/lit8 v2, v3, 0x2

    .local v2, gridBottomMargin:I
    iget-object v9, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mUnevenGrid:Lmiui/app/resourcebrowser/recommended/UnevenGrid;

    iget-object v10, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mUnevenGrid:Lmiui/app/resourcebrowser/recommended/UnevenGrid;

    invoke-virtual {v10}, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->getPaddingLeft()I

    move-result v10

    iget-object v11, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mUnevenGrid:Lmiui/app/resourcebrowser/recommended/UnevenGrid;

    invoke-virtual {v11}, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->getPaddingRight()I

    move-result v11

    invoke-virtual {v9, v10, v3, v11, v2}, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->setPadding(IIII)V

    iget-object v9, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mUnevenGrid:Lmiui/app/resourcebrowser/recommended/UnevenGrid;

    const/16 v10, 0xda

    const/16 v11, 0x84

    invoke-virtual {v9, v10, v11}, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->setGridItemRatio(II)V

    iget-object v9, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mUnevenGrid:Lmiui/app/resourcebrowser/recommended/UnevenGrid;

    invoke-virtual {v9, v3}, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->setGridItemGap(I)V

    iget-object v9, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mUnevenGrid:Lmiui/app/resourcebrowser/recommended/UnevenGrid;

    invoke-virtual {v9, v0}, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->setColumnCount(I)V

    new-instance v4, Landroid/graphics/Point;

    invoke-direct {v4}, Landroid/graphics/Point;-><init>()V

    .local v4, p:Landroid/graphics/Point;
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    invoke-virtual {v9}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v9

    invoke-interface {v9}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v9

    invoke-virtual {v9, v4}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    iget v9, v4, Landroid/graphics/Point;->x:I

    iget-object v10, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mUnevenGrid:Lmiui/app/resourcebrowser/recommended/UnevenGrid;

    invoke-virtual {v10}, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->getPaddingLeft()I

    move-result v10

    sub-int/2addr v9, v10

    iget-object v10, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mUnevenGrid:Lmiui/app/resourcebrowser/recommended/UnevenGrid;

    invoke-virtual {v10}, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->getPaddingRight()I

    move-result v10

    sub-int/2addr v9, v10

    mul-int/lit8 v10, v3, 0x1

    sub-int/2addr v9, v10

    div-int v7, v9, v0

    .local v7, recommendationWidth:I
    iget-object v9, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mMetaData:Landroid/os/Bundle;

    const-string v10, "miui.app.resourcebrowser.RECOMMENDATION_WIDTH"

    invoke-virtual {v9, v10, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .end local v0           #columnCount:I
    .end local v2           #gridBottomMargin:I
    .end local v3           #gridGap:I
    .end local v4           #p:Landroid/graphics/Point;
    .end local v7           #recommendationWidth:I
    :cond_f8
    :goto_f8
    invoke-super {p0}, Lmiui/app/resourcebrowser/ResourceListFragment;->setupUI()V

    const/4 v9, 0x1

    invoke-direct {p0, v9}, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->onlyShowSeeMoreTextView(Z)V

    iget-boolean v9, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mIsRecommendList:Z

    if-nez v9, :cond_146

    iget-object v9, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mService:Lmiui/app/resourcebrowser/service/online/OnlineService;

    iget-object v10, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mMetaData:Landroid/os/Bundle;

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Lmiui/app/resourcebrowser/service/online/OnlineService;->getCommonListUrl(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mUrl:Ljava/lang/String;

    iget-object v9, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mMetaData:Landroid/os/Bundle;

    const-string v10, "miui.app.resourcebrowser.LIST_URL"

    iget-object v11, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mUrl:Ljava/lang/String;

    invoke-virtual {v9, v10, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->requestRecommendations()V

    :cond_11a
    :goto_11a
    iget-object v9, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mAdapter:Lmiui/app/resourcebrowser/ResourceAdapter;

    check-cast v9, Lmiui/app/resourcebrowser/OnlineResourceAdapter;

    iget-object v10, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mUrl:Ljava/lang/String;

    invoke-virtual {v9, v10}, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->setUrl(Ljava/lang/String;)V

    return-void

    :cond_124
    const/4 v9, 0x0

    goto/16 :goto_72

    :cond_127
    iget-boolean v9, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mHasSubRecommends:Z

    if-nez v9, :cond_f8

    new-instance v9, Landroid/widget/TextView;

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    invoke-direct {v9, v10}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v9, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mTextView:Landroid/widget/TextView;

    iget-object v9, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mTextView:Landroid/widget/TextView;

    const/16 v10, 0x13

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v9, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mTextView:Landroid/widget/TextView;

    const v10, 0x602018e

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setBackgroundResource(I)V

    goto :goto_f8

    :cond_146
    iget-boolean v9, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mHasSubRecommends:Z

    if-nez v9, :cond_161

    iget-object v9, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mMetaData:Landroid/os/Bundle;

    const-string v10, "miui.app.resourcebrowser.RECOMMENDATION_ID"

    invoke-virtual {v9, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .local v6, recommendationId:Ljava/lang/String;
    iget-object v9, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mService:Lmiui/app/resourcebrowser/service/online/OnlineService;

    iget-object v10, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mMetaData:Landroid/os/Bundle;

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v6, v11}, Lmiui/app/resourcebrowser/service/online/OnlineService;->getRecommendationListUrl(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mUrl:Ljava/lang/String;

    invoke-virtual {p0, v6}, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->requestListMetaData(Ljava/lang/String;)V

    goto :goto_11a

    .end local v6           #recommendationId:Ljava/lang/String;
    :cond_161
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->getView()Landroid/view/View;

    move-result-object v9

    const v10, 0x60b0051

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .local v1, filters:Landroid/view/View;
    const/4 v9, 0x0

    invoke-virtual {v1, v9}, Landroid/view/View;->setVisibility(I)V

    iget-object v9, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mMetaData:Landroid/os/Bundle;

    const-string v10, "miui.app.resourcebrowser.RECOMMENDATION_ID"

    invoke-virtual {v9, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .restart local v6       #recommendationId:Ljava/lang/String;
    iget-object v9, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mService:Lmiui/app/resourcebrowser/service/online/OnlineService;

    iget-object v10, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mMetaData:Landroid/os/Bundle;

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v6, v11}, Lmiui/app/resourcebrowser/service/online/OnlineService;->getRecommendationListUrl(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mUrl:Ljava/lang/String;

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->getView()Landroid/view/View;

    move-result-object v9

    const v10, 0x60b0080

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/Spinner;

    iput-object v9, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mSubRecommendList:Landroid/widget/Spinner;

    new-instance v9, Landroid/widget/ArrayAdapter;

    iget-object v10, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mActivity:Landroid/app/Activity;

    const v11, 0x1090008

    invoke-direct {v9, v10, v11}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v9, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mSubRecommendAdapter:Landroid/widget/ArrayAdapter;

    iget-object v9, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mSubRecommendAdapter:Landroid/widget/ArrayAdapter;

    const v10, 0x1090009

    invoke-virtual {v9, v10}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v9, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mSubRecommendList:Landroid/widget/Spinner;

    iget-object v10, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mSubRecommendAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v9, v10}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v9, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mSubRecommendList:Landroid/widget/Spinner;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/Spinner;->setVisibility(I)V

    iget-object v9, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mSubRecommendList:Landroid/widget/Spinner;

    new-instance v10, Lmiui/app/resourcebrowser/OnlineResourceListFragment$1;

    invoke-direct {v10, p0}, Lmiui/app/resourcebrowser/OnlineResourceListFragment$1;-><init>(Lmiui/app/resourcebrowser/OnlineResourceListFragment;)V

    invoke-virtual {v9, v10}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    invoke-virtual {p0, v8}, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->setSubRecommendations(Ljava/util/List;)V

    iget-object v9, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mSubRecommendList:Landroid/widget/Spinner;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/Spinner;->setSelection(I)V

    iget-object v9, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mMetaData:Landroid/os/Bundle;

    const-string v10, "miui.app.resourcebrowser.CATEGORY_SUPPORTED"

    invoke-virtual {v9, v10}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_11a

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->getView()Landroid/view/View;

    move-result-object v9

    const v10, 0x60b0069

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/Spinner;

    iput-object v9, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mCategoryList:Landroid/widget/Spinner;

    new-instance v9, Landroid/widget/ArrayAdapter;

    iget-object v10, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mActivity:Landroid/app/Activity;

    const v11, 0x1090008

    invoke-direct {v9, v10, v11}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v9, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mCategoryAdapter:Landroid/widget/ArrayAdapter;

    iget-object v9, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mCategoryAdapter:Landroid/widget/ArrayAdapter;

    const v10, 0x1090009

    invoke-virtual {v9, v10}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v9, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mCategoryList:Landroid/widget/Spinner;

    iget-object v10, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mCategoryAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v9, v10}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v9, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v9}, Lmiui/app/resourcebrowser/util/ResourceHelper;->getThumbnailGap(Landroid/content/Context;)I

    move-result v5

    .local v5, recommendGap:I
    iget-object v9, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mCategoryList:Landroid/widget/Spinner;

    invoke-virtual {v9}, Landroid/widget/Spinner;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    check-cast v9, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v9, v5, v10, v11, v12}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    iget-object v9, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mCategoryList:Landroid/widget/Spinner;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/Spinner;->setVisibility(I)V

    iget-object v9, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mCategoryList:Landroid/widget/Spinner;

    new-instance v10, Lmiui/app/resourcebrowser/OnlineResourceListFragment$2;

    invoke-direct {v10, p0}, Lmiui/app/resourcebrowser/OnlineResourceListFragment$2;-><init>(Lmiui/app/resourcebrowser/OnlineResourceListFragment;)V

    invoke-virtual {v9, v10}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->requestCategories()V

    goto/16 :goto_11a
.end method

.method public startDetailActivityForResource(Landroid/util/Pair;)V
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, position:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mMetaData:Landroid/os/Bundle;

    const-string v1, "miui.app.resourcebrowser.LIST_URL"

    iget-object v2, p0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;->mUrl:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lmiui/app/resourcebrowser/ResourceListFragment;->startDetailActivityForResource(Landroid/util/Pair;)V

    return-void
.end method
