.class public Lmiui/app/resourcebrowser/ResourceDetailActivity$DownloadDetailTask;
.super Lmiui/app/resourcebrowser/util/DownloadFileTask;
.source "ResourceDetailActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/app/resourcebrowser/ResourceDetailActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "DownloadDetailTask"
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

.field private validIndex:I


# direct methods
.method public constructor <init>(Lmiui/app/resourcebrowser/ResourceDetailActivity;I)V
    .registers 3
    .parameter
    .parameter "resIndex"

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$DownloadDetailTask;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    invoke-direct {p0}, Lmiui/app/resourcebrowser/util/DownloadFileTask;-><init>()V

    iput p2, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$DownloadDetailTask;->validIndex:I

    return-void
.end method


# virtual methods
.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 2
    .parameter "x0"

    .prologue
    check-cast p1, Ljava/util/List;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/app/resourcebrowser/ResourceDetailActivity$DownloadDetailTask;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, result:Ljava/util/List;,"Ljava/util/List<Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;>;"
    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$DownloadDetailTask;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    invoke-virtual {v3}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->isFinishing()Z

    move-result v3

    if-nez v3, :cond_a

    if-nez p1, :cond_b

    :cond_a
    :goto_a
    return-void

    :cond_b
    const/4 v0, 0x0

    .local v0, filePath:Ljava/lang/String;
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_1d

    const/4 v3, 0x0

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;

    invoke-virtual {v3}, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->getPath()Ljava/lang/String;

    move-result-object v0

    :cond_1d
    if-eqz v0, :cond_a

    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$DownloadDetailTask;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    iget-object v3, v3, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mParser:Lmiui/app/resourcebrowser/service/ResourceDataParser;

    iget-object v4, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$DownloadDetailTask;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    iget-object v4, v4, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mMetaData:Landroid/os/Bundle;

    invoke-virtual {v3, v0, v4}, Lmiui/app/resourcebrowser/service/ResourceDataParser;->readResource(Ljava/lang/String;Landroid/os/Bundle;)Lmiui/app/resourcebrowser/resource/Resource;

    move-result-object v1

    .local v1, newRes:Lmiui/app/resourcebrowser/resource/Resource;
    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$DownloadDetailTask;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    iget v4, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$DownloadDetailTask;->validIndex:I

    invoke-virtual {v3, v4}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->getCurrentResource(I)Lmiui/app/resourcebrowser/resource/Resource;

    move-result-object v2

    .local v2, oldRes:Lmiui/app/resourcebrowser/resource/Resource;
    if-eqz v1, :cond_a

    if-eqz v2, :cond_a

    invoke-virtual {v1}, Lmiui/app/resourcebrowser/resource/Resource;->getInformation()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v2, v3}, Lmiui/app/resourcebrowser/resource/Resource;->setInformation(Landroid/os/Bundle;)V

    iget v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$DownloadDetailTask;->validIndex:I

    iget-object v4, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$DownloadDetailTask;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    iget v4, v4, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mResourceIndex:I

    if-ne v3, v4, :cond_a

    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$DownloadDetailTask;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    invoke-virtual {v3}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->setResourceInfo()V

    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$DownloadDetailTask;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    invoke-virtual {v3}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->bindScreenView()V

    goto :goto_a
.end method
