.class public abstract Lmiui/app/resourcebrowser/ResourceListFragment;
.super Lmiui/app/resourcebrowser/BaseFragment;
.source "ResourceListFragment.java"

# interfaces
.implements Lmiui/os/AsyncTaskObserver;
.implements Lmiui/app/SDCardMonitor$SDCardStatusListener;
.implements Lmiui/app/resourcebrowser/IntentConstants;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmiui/app/resourcebrowser/BaseFragment;",
        "Lmiui/os/AsyncTaskObserver",
        "<",
        "Ljava/lang/Void;",
        "Lmiui/app/resourcebrowser/resource/Resource;",
        "Ljava/util/List",
        "<",
        "Lmiui/app/resourcebrowser/resource/Resource;",
        ">;>;",
        "Lmiui/app/SDCardMonitor$SDCardStatusListener;",
        "Lmiui/app/resourcebrowser/IntentConstants;"
    }
.end annotation


# instance fields
.field protected mActivity:Landroid/app/Activity;

.field protected mAdapter:Lmiui/app/resourcebrowser/ResourceAdapter;

.field protected mBatchHandler:Lmiui/app/resourcebrowser/view/BatchResourceHandler;

.field protected mDisplayType:I

.field protected mListView:Landroid/widget/ListView;

.field protected mMetaData:Landroid/os/Bundle;

.field protected mProgressBar:Landroid/view/View;

.field protected mResourceSetCategory:I

.field protected mResourceSetCode:Ljava/lang/String;

.field protected mResourceSetName:Ljava/lang/String;

.field protected mResourceSetPackage:Ljava/lang/String;

.field protected mSDCardMonitor:Lmiui/app/SDCardMonitor;

.field protected mUsingPicker:Z


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    invoke-direct {p0}, Lmiui/app/resourcebrowser/BaseFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected addMetaData(Landroid/os/Bundle;)V
    .registers 2
    .parameter "metaData"

    .prologue
    return-void
.end method

.method protected abstract getAdapter()Lmiui/app/resourcebrowser/ResourceAdapter;
.end method

.method protected getBatchOperationHandler()Lmiui/app/resourcebrowser/view/BatchResourceHandler;
    .registers 3

    .prologue
    iget v0, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mResourceSetCategory:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_d

    new-instance v0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;

    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mAdapter:Lmiui/app/resourcebrowser/ResourceAdapter;

    invoke-direct {v0, p0, v1}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;-><init>(Lmiui/app/resourcebrowser/ResourceListFragment;Lmiui/app/resourcebrowser/ResourceAdapter;)V

    :goto_c
    return-object v0

    :cond_d
    new-instance v0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;

    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mAdapter:Lmiui/app/resourcebrowser/ResourceAdapter;

    invoke-direct {v0, p0, v1}, Lmiui/app/resourcebrowser/view/BatchResourceHandler;-><init>(Lmiui/app/resourcebrowser/ResourceListFragment;Lmiui/app/resourcebrowser/ResourceAdapter;)V

    goto :goto_c
.end method

.method protected abstract getContentView()I
.end method

.method protected getHeaderView()Landroid/view/View;
    .registers 2

    .prologue
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getResourceDetailActivity()Landroid/util/Pair;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    new-instance v0, Landroid/util/Pair;

    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mMetaData:Landroid/os/Bundle;

    const-string v2, "miui.app.resourcebrowser.DETAIL_ACTIVITY_PACKAGE"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mMetaData:Landroid/os/Bundle;

    const-string v3, "miui.app.resourcebrowser.DETAIL_ACTIVITY_CLASS"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method public isPicker()Z
    .registers 3

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mMetaData:Landroid/os/Bundle;

    const-string v1, "miui.app.resourcebrowser.USING_PICKER"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .registers 6
    .parameter "savedInstanceState"

    .prologue
    invoke-super {p0, p1}, Lmiui/app/resourcebrowser/BaseFragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iput-object v1, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .local v0, intent:Landroid/content/Intent;
    if-eqz v0, :cond_1d

    instance-of v1, p0, Lmiui/app/resourcebrowser/LocalResourceListFragment;

    if-eqz v1, :cond_42

    const-string v1, "META_DATA_FOR_LOCAL"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    iput-object v1, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mMetaData:Landroid/os/Bundle;

    :cond_1d
    :goto_1d
    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mMetaData:Landroid/os/Bundle;

    if-nez v1, :cond_4b

    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "meta-data can not be null. fragment: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_42
    const-string v1, "META_DATA_FOR_ONLINE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    iput-object v1, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mMetaData:Landroid/os/Bundle;

    goto :goto_1d

    :cond_4b
    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mMetaData:Landroid/os/Bundle;

    invoke-virtual {p0, v1}, Lmiui/app/resourcebrowser/ResourceListFragment;->pickMetaData(Landroid/os/Bundle;)V

    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mMetaData:Landroid/os/Bundle;

    invoke-virtual {p0, v1}, Lmiui/app/resourcebrowser/ResourceListFragment;->addMetaData(Landroid/os/Bundle;)V

    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v1}, Lmiui/app/SDCardMonitor;->getSDCardMonitor(Landroid/content/Context;)Lmiui/app/SDCardMonitor;

    move-result-object v1

    iput-object v1, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mSDCardMonitor:Lmiui/app/SDCardMonitor;

    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mSDCardMonitor:Lmiui/app/SDCardMonitor;

    invoke-virtual {v1, p0}, Lmiui/app/SDCardMonitor;->addListener(Lmiui/app/SDCardMonitor$SDCardStatusListener;)V

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceListFragment;->setupUI()V

    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .registers 5
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceListFragment;->isPicker()Z

    move-result v0

    if-eqz v0, :cond_12

    if-eqz p3, :cond_12

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0, p2, p3}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :cond_12
    return-void
.end method

.method public onCancelled()V
    .registers 3

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mProgressBar:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 6
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceListFragment;->getContentView()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .registers 3

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mBatchHandler:Lmiui/app/resourcebrowser/view/BatchResourceHandler;

    sget-object v1, Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;->DESTYOY:Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;

    invoke-virtual {v0, v1}, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->viewStateChanged(Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mSDCardMonitor:Lmiui/app/SDCardMonitor;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mSDCardMonitor:Lmiui/app/SDCardMonitor;

    invoke-virtual {v0, p0}, Lmiui/app/SDCardMonitor;->removeListener(Lmiui/app/SDCardMonitor$SDCardStatusListener;)V

    :cond_10
    invoke-super {p0}, Lmiui/app/resourcebrowser/BaseFragment;->onDestroy()V

    return-void
.end method

.method public onPause()V
    .registers 3

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mBatchHandler:Lmiui/app/resourcebrowser/view/BatchResourceHandler;

    sget-object v1, Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;->PAUSE:Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;

    invoke-virtual {v0, v1}, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->viewStateChanged(Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;)V

    invoke-super {p0}, Lmiui/app/resourcebrowser/BaseFragment;->onPause()V

    return-void
.end method

.method public bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 2
    .parameter "x0"

    .prologue
    check-cast p1, Ljava/util/List;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/app/resourcebrowser/ResourceListFragment;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method public onPostExecute(Ljava/util/List;)V
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lmiui/app/resourcebrowser/resource/Resource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, result:Ljava/util/List;,"Ljava/util/List<Lmiui/app/resourcebrowser/resource/Resource;>;"
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mProgressBar:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public onPreExecute()V
    .registers 3

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mProgressBar:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .registers 2
    .parameter "x0"

    .prologue
    check-cast p1, [Lmiui/app/resourcebrowser/resource/Resource;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/app/resourcebrowser/ResourceListFragment;->onProgressUpdate([Lmiui/app/resourcebrowser/resource/Resource;)V

    return-void
.end method

.method public varargs onProgressUpdate([Lmiui/app/resourcebrowser/resource/Resource;)V
    .registers 2
    .parameter "values"

    .prologue
    return-void
.end method

.method public onResume()V
    .registers 6

    .prologue
    invoke-super {p0}, Lmiui/app/resourcebrowser/BaseFragment;->onResume()V

    iget-object v2, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mAdapter:Lmiui/app/resourcebrowser/ResourceAdapter;

    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mMetaData:Landroid/os/Bundle;

    const-string v4, "miui.app.resourcebrowser.CURRENT_USING_PATH"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lmiui/app/resourcebrowser/ResourceAdapter;->updateCurrentUsingPath(Ljava/lang/String;)Z

    iget-object v2, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mAdapter:Lmiui/app/resourcebrowser/ResourceAdapter;

    invoke-virtual {v2}, Lmiui/app/resourcebrowser/ResourceAdapter;->notifyDataSetChanged()V

    iget-object v2, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mMetaData:Landroid/os/Bundle;

    const-string v3, "miui.app.resourcebrowser.SOURCE_FOLDERS"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .local v0, folders:[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, i:I
    :goto_1e
    array-length v2, v0

    if-ge v1, v2, :cond_29

    aget-object v2, v0, v1

    invoke-static {v2}, Lmiui/app/resourcebrowser/util/ResourceHelper;->getFolderInfoCache(Ljava/lang/String;)Lmiui/cache/FolderCache$FolderInfo;

    add-int/lit8 v1, v1, 0x1

    goto :goto_1e

    :cond_29
    return-void
.end method

.method public onStatusChanged(Z)V
    .registers 3
    .parameter "mount"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lmiui/app/resourcebrowser/util/ResourceHelper;->exit(Landroid/app/Activity;)V

    return-void
.end method

.method public onStop()V
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mAdapter:Lmiui/app/resourcebrowser/ResourceAdapter;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/ResourceAdapter;->onStop()V

    invoke-super {p0}, Lmiui/app/resourcebrowser/BaseFragment;->onStop()V

    return-void
.end method

.method protected onVisiableChanged(Z)V
    .registers 4
    .parameter "visiableForUser"

    .prologue
    invoke-super {p0, p1}, Lmiui/app/resourcebrowser/BaseFragment;->onVisiableChanged(Z)V

    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mBatchHandler:Lmiui/app/resourcebrowser/view/BatchResourceHandler;

    if-eqz p1, :cond_d

    sget-object v0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;->VISIABLE:Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;

    :goto_9
    invoke-virtual {v1, v0}, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->viewStateChanged(Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;)V

    return-void

    :cond_d
    sget-object v0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;->INVISIABLE:Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;

    goto :goto_9
.end method

.method protected pickMetaData(Landroid/os/Bundle;)V
    .registers 3
    .parameter "metaData"

    .prologue
    const-string v0, "miui.app.resourcebrowser.RESOURCE_SET_PACKAGE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mResourceSetPackage:Ljava/lang/String;

    const-string v0, "miui.app.resourcebrowser.RESOURCE_SET_CODE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mResourceSetCode:Ljava/lang/String;

    const-string v0, "miui.app.resourcebrowser.RESOURCE_SET_NAME"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mResourceSetName:Ljava/lang/String;

    const-string v0, "miui.app.resourcebrowser.RESOURCE_SET_CATEGORY"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mResourceSetCategory:I

    const-string v0, "miui.app.resourcebrowser.DISPLAY_TYPE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mDisplayType:I

    const-string v0, "miui.app.resourcebrowser.USING_PICKER"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mUsingPicker:Z

    return-void
.end method

.method protected setupUI()V
    .registers 4

    .prologue
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceListFragment;->getView()Landroid/view/View;

    move-result-object v1

    const v2, 0x60b004e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceListFragment;->getHeaderView()Landroid/view/View;

    move-result-object v0

    .local v0, header:Landroid/view/View;
    if-eqz v0, :cond_1a

    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    :cond_1a
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceListFragment;->getAdapter()Lmiui/app/resourcebrowser/ResourceAdapter;

    move-result-object v1

    iput-object v1, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mAdapter:Lmiui/app/resourcebrowser/ResourceAdapter;

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceListFragment;->getBatchOperationHandler()Lmiui/app/resourcebrowser/view/BatchResourceHandler;

    move-result-object v1

    iput-object v1, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mBatchHandler:Lmiui/app/resourcebrowser/view/BatchResourceHandler;

    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mAdapter:Lmiui/app/resourcebrowser/ResourceAdapter;

    iget-object v2, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mBatchHandler:Lmiui/app/resourcebrowser/view/BatchResourceHandler;

    invoke-virtual {v1, v2}, Lmiui/app/resourcebrowser/ResourceAdapter;->setResourceBatchHandler(Lmiui/app/resourcebrowser/view/BatchResourceHandler;)V

    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mListView:Landroid/widget/ListView;

    iget-object v2, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mAdapter:Lmiui/app/resourcebrowser/ResourceAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mListView:Landroid/widget/ListView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mListView:Landroid/widget/ListView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setDividerHeight(I)V

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceListFragment;->getView()Landroid/view/View;

    move-result-object v1

    const v2, 0x60b004f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mProgressBar:Landroid/view/View;

    return-void
.end method

.method public startDetailActivityForResource(Landroid/util/Pair;)V
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, position:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .local v1, intent:Landroid/content/Intent;
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceListFragment;->getResourceDetailActivity()Landroid/util/Pair;

    move-result-object v0

    .local v0, activityClass:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v2, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    iget-object v3, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v2, 0x400

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mMetaData:Landroid/os/Bundle;

    const-string v4, "miui.app.resourcebrowser.RESOURCE_INDEX"

    iget-object v2, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mMetaData:Landroid/os/Bundle;

    const-string v4, "miui.app.resourcebrowser.RESOURCE_GROUP"

    iget-object v2, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "META_DATA"

    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mMetaData:Landroid/os/Bundle;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, Lmiui/app/resourcebrowser/ResourceListFragment;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method
