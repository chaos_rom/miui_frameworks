.class Lmiui/app/resourcebrowser/ResourceDetailActivity$5;
.super Landroid/animation/AnimatorListenerAdapter;
.source "ResourceDetailActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiui/app/resourcebrowser/ResourceDetailActivity;->buildModeChangeAnimator()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;


# direct methods
.method constructor <init>(Lmiui/app/resourcebrowser/ResourceDetailActivity;)V
    .registers 2
    .parameter

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$5;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .registers 4
    .parameter "animation"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$5;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    iget-object v0, v0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mCoverView:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$5;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    #calls: Lmiui/app/resourcebrowser/ResourceDetailActivity;->onEndEnterNormalMode()V
    invoke-static {v0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->access$800(Lmiui/app/resourcebrowser/ResourceDetailActivity;)V

    invoke-super {p0, p1}, Landroid/animation/AnimatorListenerAdapter;->onAnimationEnd(Landroid/animation/Animator;)V

    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .registers 7
    .parameter "animation"

    .prologue
    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$5;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    #getter for: Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreviewInfos:Ljava/util/List;
    invoke-static {v3}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->access$600(Lmiui/app/resourcebrowser/ResourceDetailActivity;)Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$5;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    iget-object v4, v4, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPreview:Lmiui/app/resourcebrowser/util/ResourceScreenView;

    invoke-virtual {v4}, Lmiui/app/resourcebrowser/util/ResourceScreenView;->getCurrentScreenIndex()I

    move-result v4

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/app/resourcebrowser/ResourceDetailActivity$PreviewInfo;

    .local v2, pInfo:Lmiui/app/resourcebrowser/ResourceDetailActivity$PreviewInfo;
    iget-object v1, v2, Lmiui/app/resourcebrowser/ResourceDetailActivity$PreviewInfo;->localPath:Ljava/lang/String;

    .local v1, localPath:Ljava/lang/String;
    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$5;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    #getter for: Lmiui/app/resourcebrowser/ResourceDetailActivity;->mImageCacheDecoder:Lmiui/app/resourcebrowser/util/ImageCacheDecoder;
    invoke-static {v3}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->access$400(Lmiui/app/resourcebrowser/ResourceDetailActivity;)Lmiui/app/resourcebrowser/util/ImageCacheDecoder;

    move-result-object v3

    invoke-virtual {v3, v1}, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .local v0, bitmap:Landroid/graphics/Bitmap;
    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$5;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    iget-object v3, v3, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mCoverView:Landroid/widget/ImageView;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$5;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    iget-object v3, v3, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mCoverView:Landroid/widget/ImageView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$5;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    #calls: Lmiui/app/resourcebrowser/ResourceDetailActivity;->onBeginEnterNormalMode()V
    invoke-static {v3}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->access$700(Lmiui/app/resourcebrowser/ResourceDetailActivity;)V

    invoke-super {p0, p1}, Landroid/animation/AnimatorListenerAdapter;->onAnimationStart(Landroid/animation/Animator;)V

    return-void
.end method
