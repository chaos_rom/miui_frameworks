.class Lmiui/app/resourcebrowser/ResourceDetailActivity$9;
.super Ljava/lang/Object;
.source "ResourceDetailActivity.java"

# interfaces
.implements Lmiui/app/resourcebrowser/util/BatchMediaPlayer$BatchPlayerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiui/app/resourcebrowser/ResourceDetailActivity;->initPlayer()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;


# direct methods
.method constructor <init>(Lmiui/app/resourcebrowser/ResourceDetailActivity;)V
    .registers 2
    .parameter

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$9;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public finish(Z)V
    .registers 5
    .parameter "hasError"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$9;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    iget-object v0, v0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mPlayButton:Landroid/widget/ImageView;

    const v1, 0x60200d5

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$9;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    iget-object v0, v0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mRingtoneName:Landroid/widget/TextView;

    if-eqz v0, :cond_1d

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$9;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    iget-object v0, v0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mRingtoneName:Landroid/widget/TextView;

    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$9;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    invoke-virtual {v1}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->getFormatTitleBeforePlayingRingtone()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1d
    if-eqz p1, :cond_2c

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$9;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    const v1, 0x60c0020

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_2c
    return-void
.end method

.method public play(Ljava/lang/String;II)V
    .registers 6
    .parameter "ringtonePath"
    .parameter "current"
    .parameter "total"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$9;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    iget-object v0, v0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mRingtoneName:Landroid/widget/TextView;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$9;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    iget-object v0, v0, Lmiui/app/resourcebrowser/ResourceDetailActivity;->mRingtoneName:Landroid/widget/TextView;

    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$9;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    invoke-virtual {v1, p1, p2, p3}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->getFormatPlayingRingtoneName(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_13
    return-void
.end method
