.class public abstract Lmiui/app/resourcebrowser/BaseTabActivity;
.super Landroid/app/Activity;
.source "BaseTabActivity.java"

# interfaces
.implements Landroid/app/ActionBar$TabListener;
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/app/resourcebrowser/BaseTabActivity$ResourcePagerAdapter;
    }
.end annotation


# static fields
.field private static final VIEW_PAGE_ID:I = 0x1


# instance fields
.field protected mActionBar:Landroid/app/ActionBar;

.field protected mCurrentPagePosition:I

.field protected mFragmentsMenuId:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field protected mPageAdapter:Lmiui/app/resourcebrowser/BaseTabActivity$ResourcePagerAdapter;

.field protected mTabFragments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lmiui/app/resourcebrowser/BaseFragment;",
            ">;"
        }
    .end annotation
.end field

.field protected mViewPager:Landroid/support/v4/view/ViewPager;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    invoke-static {v0}, Landroid/os/AsyncTask;->setDefaultExecutor(Ljava/util/concurrent/Executor;)V

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/app/resourcebrowser/BaseTabActivity;->mTabFragments:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/app/resourcebrowser/BaseTabActivity;->mFragmentsMenuId:Ljava/util/List;

    return-void
.end method

.method private createActionBar()V
    .registers 5

    .prologue
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/BaseTabActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    iput-object v2, p0, Lmiui/app/resourcebrowser/BaseTabActivity;->mActionBar:Landroid/app/ActionBar;

    iget-object v2, p0, Lmiui/app/resourcebrowser/BaseTabActivity;->mActionBar:Landroid/app/ActionBar;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setNavigationMode(I)V

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/BaseTabActivity;->getActionBarTabs()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_14
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_29

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActionBar$Tab;

    .local v1, tab:Landroid/app/ActionBar$Tab;
    invoke-virtual {v1, p0}, Landroid/app/ActionBar$Tab;->setTabListener(Landroid/app/ActionBar$TabListener;)Landroid/app/ActionBar$Tab;

    iget-object v2, p0, Lmiui/app/resourcebrowser/BaseTabActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v2, v1}, Landroid/app/ActionBar;->addTab(Landroid/app/ActionBar$Tab;)V

    goto :goto_14

    .end local v1           #tab:Landroid/app/ActionBar$Tab;
    :cond_29
    return-void
.end method

.method private createPagerAdapter()V
    .registers 3

    .prologue
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/BaseTabActivity;->getResourcePagerAdapter()Lmiui/app/resourcebrowser/BaseTabActivity$ResourcePagerAdapter;

    move-result-object v0

    iput-object v0, p0, Lmiui/app/resourcebrowser/BaseTabActivity;->mPageAdapter:Lmiui/app/resourcebrowser/BaseTabActivity$ResourcePagerAdapter;

    iget-object v0, p0, Lmiui/app/resourcebrowser/BaseTabActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lmiui/app/resourcebrowser/BaseTabActivity;->mPageAdapter:Lmiui/app/resourcebrowser/BaseTabActivity$ResourcePagerAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/BaseTabActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    return-void
.end method

.method private createTabFragments()V
    .registers 8

    .prologue
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/BaseTabActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .local v1, fragmentManager:Landroid/app/FragmentManager;
    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v4

    .local v4, transaction:Landroid/app/FragmentTransaction;
    iget-object v5, p0, Lmiui/app/resourcebrowser/BaseTabActivity;->mTabFragments:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->clear()V

    const/4 v2, 0x0

    .local v2, i:I
    :goto_e
    iget-object v5, p0, Lmiui/app/resourcebrowser/BaseTabActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v5}, Landroid/app/ActionBar;->getTabCount()I

    move-result v5

    if-ge v2, v5, :cond_44

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "tag-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .local v3, tag:Ljava/lang/String;
    invoke-virtual {v1, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .local v0, frag:Landroid/app/Fragment;
    if-nez v0, :cond_3a

    invoke-virtual {p0, v2}, Lmiui/app/resourcebrowser/BaseTabActivity;->initTabFragment(I)Lmiui/app/resourcebrowser/BaseFragment;

    move-result-object v0

    const/4 v5, 0x1

    invoke-virtual {v4, v5, v0, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    invoke-virtual {v4, v0}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    :cond_3a
    iget-object v5, p0, Lmiui/app/resourcebrowser/BaseTabActivity;->mTabFragments:Ljava/util/List;

    check-cast v0, Lmiui/app/resourcebrowser/BaseFragment;

    .end local v0           #frag:Landroid/app/Fragment;
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_e

    .end local v3           #tag:Ljava/lang/String;
    :cond_44
    invoke-virtual {v4}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    invoke-virtual {v1}, Landroid/app/FragmentManager;->executePendingTransactions()Z

    return-void
.end method

.method private selectTab(IZ)V
    .registers 6
    .parameter "position"
    .parameter "updateFragment"

    .prologue
    const/4 v2, 0x1

    iget v0, p0, Lmiui/app/resourcebrowser/BaseTabActivity;->mCurrentPagePosition:I

    if-ne p1, v0, :cond_6

    :goto_5
    return-void

    :cond_6
    iget-object v0, p0, Lmiui/app/resourcebrowser/BaseTabActivity;->mTabFragments:Ljava/util/List;

    iget v1, p0, Lmiui/app/resourcebrowser/BaseTabActivity;->mCurrentPagePosition:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/app/resourcebrowser/BaseFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmiui/app/resourcebrowser/BaseFragment;->onVisiableChanged(Z)V

    iput p1, p0, Lmiui/app/resourcebrowser/BaseTabActivity;->mCurrentPagePosition:I

    iget-object v0, p0, Lmiui/app/resourcebrowser/BaseTabActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    if-eqz p2, :cond_22

    iget-object v0, p0, Lmiui/app/resourcebrowser/BaseTabActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p1, v2}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    :cond_22
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/BaseTabActivity;->invalidateOptionsMenu()V

    iget-object v0, p0, Lmiui/app/resourcebrowser/BaseTabActivity;->mTabFragments:Ljava/util/List;

    iget v1, p0, Lmiui/app/resourcebrowser/BaseTabActivity;->mCurrentPagePosition:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/app/resourcebrowser/BaseFragment;

    invoke-virtual {v0, v2}, Lmiui/app/resourcebrowser/BaseFragment;->onVisiableChanged(Z)V

    goto :goto_5
.end method


# virtual methods
.method protected abstract getActionBarTabs()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/ActionBar$Tab;",
            ">;"
        }
    .end annotation
.end method

.method protected getResourcePagerAdapter()Lmiui/app/resourcebrowser/BaseTabActivity$ResourcePagerAdapter;
    .registers 2

    .prologue
    new-instance v0, Lmiui/app/resourcebrowser/BaseTabActivity$ResourcePagerAdapter;

    invoke-direct {v0, p0}, Lmiui/app/resourcebrowser/BaseTabActivity$ResourcePagerAdapter;-><init>(Lmiui/app/resourcebrowser/BaseTabActivity;)V

    return-object v0
.end method

.method protected abstract initTabFragment(I)Lmiui/app/resourcebrowser/BaseFragment;
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 4
    .parameter "savedInstanceState"

    .prologue
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    new-instance v0, Landroid/support/v4/view/ViewPager;

    invoke-direct {v0, p0}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lmiui/app/resourcebrowser/BaseTabActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v0, p0, Lmiui/app/resourcebrowser/BaseTabActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setId(I)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/BaseTabActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {p0, v0}, Lmiui/app/resourcebrowser/BaseTabActivity;->setContentView(Landroid/view/View;)V

    invoke-direct {p0}, Lmiui/app/resourcebrowser/BaseTabActivity;->createPagerAdapter()V

    invoke-direct {p0}, Lmiui/app/resourcebrowser/BaseTabActivity;->createActionBar()V

    invoke-direct {p0}, Lmiui/app/resourcebrowser/BaseTabActivity;->createTabFragments()V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 6
    .parameter "menu"

    .prologue
    iget-object v3, p0, Lmiui/app/resourcebrowser/BaseTabActivity;->mFragmentsMenuId:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    iget-object v3, p0, Lmiui/app/resourcebrowser/BaseTabActivity;->mTabFragments:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_21

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/app/resourcebrowser/BaseFragment;

    .local v0, f:Lmiui/app/resourcebrowser/BaseFragment;
    invoke-virtual {v0, p1}, Lmiui/app/resourcebrowser/BaseFragment;->onFragmentCreateOptionsMenu(Landroid/view/Menu;)Ljava/util/List;

    move-result-object v2

    .local v2, menuId:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Integer;>;"
    iget-object v3, p0, Lmiui/app/resourcebrowser/BaseTabActivity;->mFragmentsMenuId:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_b

    .end local v0           #f:Lmiui/app/resourcebrowser/BaseFragment;
    .end local v2           #menuId:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Integer;>;"
    :cond_21
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v3

    return v3
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 5
    .parameter "item"

    .prologue
    const/4 v0, 0x0

    .local v0, i:I
    :goto_1
    iget-object v1, p0, Lmiui/app/resourcebrowser/BaseTabActivity;->mFragmentsMenuId:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_2d

    iget-object v1, p0, Lmiui/app/resourcebrowser/BaseTabActivity;->mFragmentsMenuId:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2a

    iget-object v1, p0, Lmiui/app/resourcebrowser/BaseTabActivity;->mTabFragments:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/app/resourcebrowser/BaseFragment;

    invoke-virtual {v1, p1}, Lmiui/app/resourcebrowser/BaseFragment;->onFragmentOptionsItemSelected(Landroid/view/MenuItem;)V

    :cond_2a
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2d
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    return v1
.end method

.method public onPageScrollStateChanged(I)V
    .registers 2
    .parameter "state"

    .prologue
    return-void
.end method

.method public onPageScrolled(IFI)V
    .registers 4
    .parameter "position"
    .parameter "positionOffset"
    .parameter "positionOffsetPixels"

    .prologue
    return-void
.end method

.method public onPageSelected(I)V
    .registers 3
    .parameter "position"

    .prologue
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lmiui/app/resourcebrowser/BaseTabActivity;->selectTab(IZ)V

    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .registers 9
    .parameter "menu"

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    .local v2, i:I
    :goto_2
    iget-object v5, p0, Lmiui/app/resourcebrowser/BaseTabActivity;->mTabFragments:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v2, v5, :cond_45

    iget-object v5, p0, Lmiui/app/resourcebrowser/BaseTabActivity;->mTabFragments:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/app/resourcebrowser/BaseFragment;

    .local v0, f:Lmiui/app/resourcebrowser/BaseFragment;
    iget v5, p0, Lmiui/app/resourcebrowser/BaseTabActivity;->mCurrentPagePosition:I

    if-ne v5, v2, :cond_40

    const/4 v1, 0x1

    .local v1, goingVisiable:Z
    :goto_17
    invoke-virtual {v0, p1, v1}, Lmiui/app/resourcebrowser/BaseFragment;->onFragmentPrepareOptionsMenu(Landroid/view/Menu;Z)V

    if-nez v1, :cond_42

    iget-object v5, p0, Lmiui/app/resourcebrowser/BaseTabActivity;->mFragmentsMenuId:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, i$:Ljava/util/Iterator;
    :goto_28
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_42

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .local v4, menuId:I
    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    invoke-interface {v5, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_28

    .end local v1           #goingVisiable:Z
    .end local v3           #i$:Ljava/util/Iterator;
    .end local v4           #menuId:I
    :cond_40
    move v1, v6

    goto :goto_17

    .restart local v1       #goingVisiable:Z
    :cond_42
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .end local v0           #f:Lmiui/app/resourcebrowser/BaseFragment;
    .end local v1           #goingVisiable:Z
    :cond_45
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v5

    return v5
.end method

.method public onTabReselected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .registers 3
    .parameter "tab"
    .parameter "ft"

    .prologue
    return-void
.end method

.method public onTabSelected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .registers 5
    .parameter "tab"
    .parameter "ft"

    .prologue
    invoke-virtual {p1}, Landroid/app/ActionBar$Tab;->getPosition()I

    move-result v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lmiui/app/resourcebrowser/BaseTabActivity;->selectTab(IZ)V

    return-void
.end method

.method public onTabUnselected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .registers 3
    .parameter "tab"
    .parameter "ft"

    .prologue
    return-void
.end method
