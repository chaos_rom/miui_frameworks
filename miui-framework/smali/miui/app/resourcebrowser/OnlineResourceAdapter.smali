.class public Lmiui/app/resourcebrowser/OnlineResourceAdapter;
.super Lmiui/app/resourcebrowser/ResourceAdapter;
.source "OnlineResourceAdapter.java"

# interfaces
.implements Lmiui/app/resourcebrowser/service/online/OnlineProtocolConstants;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/app/resourcebrowser/OnlineResourceAdapter$1;,
        Lmiui/app/resourcebrowser/OnlineResourceAdapter$DThumbnailStatNode;,
        Lmiui/app/resourcebrowser/OnlineResourceAdapter$DownloadThumbnailTask;,
        Lmiui/app/resourcebrowser/OnlineResourceAdapter$AsyncLoadMoreResourceTask;
    }
.end annotation


# static fields
.field private static final MAX_THUMBNAIL_DOWNLOAD_TASK:I


# instance fields
.field private dTaskParallelNumber:I

.field private dThumbnailTaskStatList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lmiui/app/resourcebrowser/OnlineResourceAdapter$DThumbnailStatNode;",
            ">;"
        }
    .end annotation
.end field

.field private mDownloadedCount:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mListFolder:Ljava/lang/String;

.field private mParser:Lmiui/app/resourcebrowser/service/ResourceDataParser;

.field private mThumbnailDownloadQueue:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private mThumbnailDownloadTaskList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lmiui/app/resourcebrowser/OnlineResourceAdapter$DownloadThumbnailTask;",
            ">;"
        }
    .end annotation
.end field

.field private mThumbnailDownloadingSet:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;",
            ">;"
        }
    .end annotation
.end field

.field private mUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    sget-boolean v0, Lmiui/app/resourcebrowser/util/ResourceDebug;->DEBUG:Z

    if-eqz v0, :cond_b

    invoke-static {}, Lmiui/app/resourcebrowser/util/ResourceDebug;->getMaxThumbnailDownloadTaskNumber()I

    move-result v0

    :goto_8
    sput v0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->MAX_THUMBNAIL_DOWNLOAD_TASK:I

    return-void

    :cond_b
    const/4 v0, 0x3

    goto :goto_8
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Bundle;)V
    .registers 7
    .parameter "activity"
    .parameter "metaData"

    .prologue
    const/4 v3, 0x0

    invoke-direct {p0, p1, p2}, Lmiui/app/resourcebrowser/ResourceAdapter;-><init>(Landroid/content/Context;Landroid/os/Bundle;)V

    new-instance v0, Ljava/util/LinkedHashMap;

    const/high16 v1, 0x3f40

    const/4 v2, 0x1

    invoke-direct {v0, v3, v1, v2}, Ljava/util/LinkedHashMap;-><init>(IFZ)V

    iput-object v0, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->mThumbnailDownloadQueue:Ljava/util/LinkedHashMap;

    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->mThumbnailDownloadingSet:Ljava/util/HashSet;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->mThumbnailDownloadTaskList:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->mDownloadedCount:Ljava/util/HashMap;

    invoke-static {}, Lmiui/app/resourcebrowser/service/ResourceJSONDataParser;->getInstance()Lmiui/app/resourcebrowser/service/ResourceJSONDataParser;

    move-result-object v0

    iput-object v0, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->mParser:Lmiui/app/resourcebrowser/service/ResourceDataParser;

    const/4 v0, 0x0

    iput-object v0, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->dThumbnailTaskStatList:Ljava/util/ArrayList;

    sget-boolean v0, Lmiui/app/resourcebrowser/util/ResourceDebug;->DEBUG:Z

    if-eqz v0, :cond_39

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->dThumbnailTaskStatList:Ljava/util/ArrayList;

    iput v3, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->dTaskParallelNumber:I

    :cond_39
    invoke-direct {p0}, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->initParams()V

    return-void
.end method

.method public constructor <init>(Lmiui/app/resourcebrowser/BaseFragment;Landroid/os/Bundle;)V
    .registers 7
    .parameter "fragment"
    .parameter "metaData"

    .prologue
    const/4 v3, 0x0

    invoke-direct {p0, p1, p2}, Lmiui/app/resourcebrowser/ResourceAdapter;-><init>(Lmiui/app/resourcebrowser/BaseFragment;Landroid/os/Bundle;)V

    new-instance v0, Ljava/util/LinkedHashMap;

    const/high16 v1, 0x3f40

    const/4 v2, 0x1

    invoke-direct {v0, v3, v1, v2}, Ljava/util/LinkedHashMap;-><init>(IFZ)V

    iput-object v0, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->mThumbnailDownloadQueue:Ljava/util/LinkedHashMap;

    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->mThumbnailDownloadingSet:Ljava/util/HashSet;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->mThumbnailDownloadTaskList:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->mDownloadedCount:Ljava/util/HashMap;

    invoke-static {}, Lmiui/app/resourcebrowser/service/ResourceJSONDataParser;->getInstance()Lmiui/app/resourcebrowser/service/ResourceJSONDataParser;

    move-result-object v0

    iput-object v0, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->mParser:Lmiui/app/resourcebrowser/service/ResourceDataParser;

    const/4 v0, 0x0

    iput-object v0, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->dThumbnailTaskStatList:Ljava/util/ArrayList;

    sget-boolean v0, Lmiui/app/resourcebrowser/util/ResourceDebug;->DEBUG:Z

    if-eqz v0, :cond_39

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->dThumbnailTaskStatList:Ljava/util/ArrayList;

    iput v3, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->dTaskParallelNumber:I

    :cond_39
    invoke-direct {p0}, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->initParams()V

    return-void
.end method

.method static synthetic access$000(Lmiui/app/resourcebrowser/OnlineResourceAdapter;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lmiui/app/resourcebrowser/OnlineResourceAdapter;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->mListFolder:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lmiui/app/resourcebrowser/OnlineResourceAdapter;)Lmiui/app/resourcebrowser/service/ResourceDataParser;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->mParser:Lmiui/app/resourcebrowser/service/ResourceDataParser;

    return-object v0
.end method

.method static synthetic access$300(Lmiui/app/resourcebrowser/OnlineResourceAdapter;)I
    .registers 2
    .parameter "x0"

    .prologue
    iget v0, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->dTaskParallelNumber:I

    return v0
.end method

.method static synthetic access$304(Lmiui/app/resourcebrowser/OnlineResourceAdapter;)I
    .registers 2
    .parameter "x0"

    .prologue
    iget v0, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->dTaskParallelNumber:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->dTaskParallelNumber:I

    return v0
.end method

.method static synthetic access$306(Lmiui/app/resourcebrowser/OnlineResourceAdapter;)I
    .registers 2
    .parameter "x0"

    .prologue
    iget v0, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->dTaskParallelNumber:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->dTaskParallelNumber:I

    return v0
.end method

.method static synthetic access$400(Lmiui/app/resourcebrowser/OnlineResourceAdapter;Ljava/lang/String;JJ)V
    .registers 6
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    .prologue
    invoke-direct/range {p0 .. p5}, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->dAddIntoStat(Ljava/lang/String;JJ)V

    return-void
.end method

.method static synthetic access$500(Lmiui/app/resourcebrowser/OnlineResourceAdapter;)Ljava/util/HashMap;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->mDownloadedCount:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$600(Lmiui/app/resourcebrowser/OnlineResourceAdapter;)Ljava/util/HashSet;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->mThumbnailDownloadingSet:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic access$700(Lmiui/app/resourcebrowser/OnlineResourceAdapter;)Ljava/util/ArrayList;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->mThumbnailDownloadTaskList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$800(Lmiui/app/resourcebrowser/OnlineResourceAdapter;)V
    .registers 1
    .parameter "x0"

    .prologue
    invoke-direct {p0}, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->startOneDownloadThumbnailTask()V

    return-void
.end method

.method private dAddIntoStat(Ljava/lang/String;JJ)V
    .registers 9
    .parameter "url"
    .parameter "sTime"
    .parameter "eTime"

    .prologue
    sget-boolean v1, Lmiui/app/resourcebrowser/util/ResourceDebug;->DEBUG:Z

    if-nez v1, :cond_5

    :goto_4
    return-void

    :cond_5
    new-instance v0, Lmiui/app/resourcebrowser/OnlineResourceAdapter$DThumbnailStatNode;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lmiui/app/resourcebrowser/OnlineResourceAdapter$DThumbnailStatNode;-><init>(Lmiui/app/resourcebrowser/OnlineResourceAdapter;Lmiui/app/resourcebrowser/OnlineResourceAdapter$1;)V

    .local v0, node:Lmiui/app/resourcebrowser/OnlineResourceAdapter$DThumbnailStatNode;
    iput-wide p2, v0, Lmiui/app/resourcebrowser/OnlineResourceAdapter$DThumbnailStatNode;->sTime:J

    iput-wide p4, v0, Lmiui/app/resourcebrowser/OnlineResourceAdapter$DThumbnailStatNode;->eTime:J

    iput-object p1, v0, Lmiui/app/resourcebrowser/OnlineResourceAdapter$DThumbnailStatNode;->url:Ljava/lang/String;

    iget-object v2, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->dThumbnailTaskStatList:Ljava/util/ArrayList;

    monitor-enter v2

    :try_start_14
    iget-object v1, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->dThumbnailTaskStatList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v2

    goto :goto_4

    :catchall_1b
    move-exception v1

    monitor-exit v2
    :try_end_1d
    .catchall {:try_start_14 .. :try_end_1d} :catchall_1b

    throw v1
.end method

.method private dStatDownloadThumbnailSpeedInfo()V
    .registers 16

    .prologue
    sget-boolean v7, Lmiui/app/resourcebrowser/util/ResourceDebug;->DEBUG:Z

    if-eqz v7, :cond_14

    iget-object v7, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->mThumbnailDownloadingSet:Ljava/util/HashSet;

    invoke-virtual {v7}, Ljava/util/HashSet;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_14

    iget-object v7, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->dThumbnailTaskStatList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_15

    :cond_14
    :goto_14
    return-void

    :cond_15
    iget-object v8, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->dThumbnailTaskStatList:Ljava/util/ArrayList;

    monitor-enter v8

    const/4 v6, 0x0

    .local v6, taskRelativeSumTime:I
    const-wide/16 v4, 0x0

    .local v4, taskMaxEndTime:J
    :try_start_1b
    iget-object v7, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->dThumbnailTaskStatList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v3

    .local v3, size:I
    const/4 v1, 0x0

    .local v1, i:I
    :goto_22
    if-ge v1, v3, :cond_3d

    iget-object v7, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->dThumbnailTaskStatList:Ljava/util/ArrayList;

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/app/resourcebrowser/OnlineResourceAdapter$DThumbnailStatNode;

    .local v2, node:Lmiui/app/resourcebrowser/OnlineResourceAdapter$DThumbnailStatNode;
    int-to-long v9, v6

    iget-wide v11, v2, Lmiui/app/resourcebrowser/OnlineResourceAdapter$DThumbnailStatNode;->eTime:J

    iget-wide v13, v2, Lmiui/app/resourcebrowser/OnlineResourceAdapter$DThumbnailStatNode;->sTime:J

    sub-long/2addr v11, v13

    add-long/2addr v9, v11

    long-to-int v6, v9

    iget-wide v9, v2, Lmiui/app/resourcebrowser/OnlineResourceAdapter$DThumbnailStatNode;->eTime:J

    invoke-static {v4, v5, v9, v10}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_22

    .end local v2           #node:Lmiui/app/resourcebrowser/OnlineResourceAdapter$DThumbnailStatNode;
    :cond_3d
    iget-object v7, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->dThumbnailTaskStatList:Ljava/util/ArrayList;

    const/4 v9, 0x0

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lmiui/app/resourcebrowser/OnlineResourceAdapter$DThumbnailStatNode;

    iget-wide v9, v7, Lmiui/app/resourcebrowser/OnlineResourceAdapter$DThumbnailStatNode;->sTime:J

    sub-long v9, v4, v9

    long-to-int v0, v9

    .local v0, actualTime:I
    const-string v7, "Theme"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Real Stat Result: ContinuousDownloadTaskNum="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " AvgTaskRelativeTime="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    div-int v10, v6, v3

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " AvgActualTime="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    div-int v10, v0, v3

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v7, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->dThumbnailTaskStatList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->clear()V

    monitor-exit v8

    goto :goto_14

    .end local v0           #actualTime:I
    .end local v1           #i:I
    .end local v3           #size:I
    :catchall_82
    move-exception v7

    monitor-exit v8
    :try_end_84
    .catchall {:try_start_1b .. :try_end_84} :catchall_82

    throw v7
.end method

.method private downloadThumbnail(Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;)V
    .registers 5
    .parameter "entry"

    .prologue
    const/4 v2, 0x5

    if-eqz p1, :cond_28

    iget v1, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mDisplayType:I

    if-eq v1, v2, :cond_28

    iget-object v1, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->mThumbnailDownloadingSet:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_28

    iget-object v1, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->mDownloadedCount:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .local v0, previousCount:Ljava/lang/Integer;
    if-eqz v0, :cond_1f

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ge v1, v2, :cond_28

    :cond_1f
    iget-object v1, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->mThumbnailDownloadQueue:Ljava/util/LinkedHashMap;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->startOneDownloadThumbnailTask()V

    .end local v0           #previousCount:Ljava/lang/Integer;
    :cond_28
    return-void
.end method

.method private initParams()V
    .registers 4

    .prologue
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mMetaData:Landroid/os/Bundle;

    const-string v2, "miui.app.resourcebrowser.CACHE_LIST_FOLDER"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lmiui/os/ExtraFileUtils;->standardizeFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mResourceSetCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lmiui/os/ExtraFileUtils;->standardizeFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->mListFolder:Ljava/lang/String;

    return-void
.end method

.method private resetResources()V
    .registers 3

    .prologue
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->clearResourceSet()V

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->notifyDataSetInvalidated()V

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->loadMoreData(ZZ)V

    return-void
.end method

.method private startOneDownloadThumbnailTask()V
    .registers 7

    .prologue
    iget-object v3, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->mThumbnailDownloadQueue:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_77

    iget-object v3, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->mThumbnailDownloadTaskList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    sget v4, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->MAX_THUMBNAIL_DOWNLOAD_TASK:I

    if-ge v3, v4, :cond_77

    const/4 v0, 0x0

    .local v0, entry:Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;
    iget-object v3, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->mThumbnailDownloadQueue:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, iterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;>;"
    :goto_1d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .end local v0           #entry:Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;
    check-cast v0, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;

    .restart local v0       #entry:Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;
    goto :goto_1d

    :cond_2a
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    new-instance v2, Lmiui/app/resourcebrowser/OnlineResourceAdapter$DownloadThumbnailTask;

    invoke-direct {v2, p0, v0}, Lmiui/app/resourcebrowser/OnlineResourceAdapter$DownloadThumbnailTask;-><init>(Lmiui/app/resourcebrowser/OnlineResourceAdapter;Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;)V

    .local v2, t:Lmiui/app/resourcebrowser/OnlineResourceAdapter$DownloadThumbnailTask;
    iget-object v3, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->mThumbnailDownloadingSet:Ljava/util/HashSet;

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->mThumbnailDownloadTaskList:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v3, 0x1

    new-array v3, v3, [Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v2, v3}, Lmiui/app/resourcebrowser/OnlineResourceAdapter$DownloadThumbnailTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    sget-boolean v3, Lmiui/app/resourcebrowser/util/ResourceDebug;->DEBUG:Z

    if-eqz v3, :cond_77

    const-string v3, "Theme"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "remainTaskNumber="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->mThumbnailDownloadQueue:Ljava/util/LinkedHashMap;

    invoke-virtual {v5}, Ljava/util/LinkedHashMap;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " executingTestNumber="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->mThumbnailDownloadTaskList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .end local v0           #entry:Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;
    .end local v1           #iterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;>;"
    .end local v2           #t:Lmiui/app/resourcebrowser/OnlineResourceAdapter$DownloadThumbnailTask;
    :cond_77
    invoke-direct {p0}, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->dStatDownloadThumbnailSpeedInfo()V

    return-void
.end method


# virtual methods
.method protected getLoadMoreDataTask()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lmiui/widget/AsyncAdapter",
            "<",
            "Lmiui/app/resourcebrowser/resource/Resource;",
            ">.Async",
            "LoadMoreDataTask;",
            ">;"
        }
    .end annotation

    .prologue
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .local v1, tasks:Ljava/util/List;,"Ljava/util/List<Lmiui/widget/AsyncAdapter<Lmiui/app/resourcebrowser/resource/Resource;>.AsyncLoadMoreDataTask;>;"
    new-instance v0, Lmiui/app/resourcebrowser/OnlineResourceAdapter$AsyncLoadMoreResourceTask;

    invoke-direct {v0, p0}, Lmiui/app/resourcebrowser/OnlineResourceAdapter$AsyncLoadMoreResourceTask;-><init>(Lmiui/app/resourcebrowser/OnlineResourceAdapter;)V

    .local v0, task:Lmiui/app/resourcebrowser/OnlineResourceAdapter$AsyncLoadMoreResourceTask;
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->getRegisterAsyncTaskObserver()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/os/AsyncTaskObserver;

    invoke-virtual {v0, v2}, Lmiui/app/resourcebrowser/OnlineResourceAdapter$AsyncLoadMoreResourceTask;->addObserver(Lmiui/os/AsyncTaskObserver;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v1
.end method

.method protected bridge synthetic isValidKey(Ljava/lang/Object;Ljava/lang/Object;I)Z
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    check-cast p2, Lmiui/app/resourcebrowser/resource/Resource;

    .end local p2
    invoke-virtual {p0, p1, p2, p3}, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->isValidKey(Ljava/lang/Object;Lmiui/app/resourcebrowser/resource/Resource;I)Z

    move-result v0

    return v0
.end method

.method protected isValidKey(Ljava/lang/Object;Lmiui/app/resourcebrowser/resource/Resource;I)Z
    .registers 13
    .parameter "key"
    .parameter "data"
    .parameter "position"

    .prologue
    const/4 v4, 0x1

    move-object v2, p1

    check-cast v2, Ljava/lang/String;

    .local v2, localPath:Ljava/lang/String;
    invoke-virtual {p2, p3}, Lmiui/app/resourcebrowser/resource/Resource;->getOnlineThumbnail(I)Ljava/lang/String;

    move-result-object v3

    .local v3, onlinePath:Ljava/lang/String;
    new-instance v0, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;

    invoke-direct {v0}, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;-><init>()V

    .local v0, entry:Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;
    invoke-virtual {v0, v2}, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->setPath(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->setUrl(Ljava/lang/String;)V

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .local v1, file:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_22

    invoke-direct {p0, v0}, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->downloadThumbnail(Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;)V

    :goto_21
    return v4

    :cond_22
    invoke-virtual {v1}, Ljava/io/File;->lastModified()J

    move-result-wide v5

    invoke-virtual {p2}, Lmiui/app/resourcebrowser/resource/Resource;->getFileModifiedTime()J

    move-result-wide v7

    cmp-long v5, v5, v7

    if-gez v5, :cond_35

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    invoke-direct {p0, v0}, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->downloadThumbnail(Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;)V

    goto :goto_21

    :cond_35
    invoke-super {p0, p1, p2, p3}, Lmiui/app/resourcebrowser/ResourceAdapter;->isValidKey(Ljava/lang/Object;Lmiui/app/resourcebrowser/resource/Resource;I)Z

    move-result v4

    goto :goto_21
.end method

.method protected loadCacheData(Lmiui/widget/AsyncAdapter$AsyncLoadMoreParams;)Ljava/util/List;
    .registers 11
    .parameter "params"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiui/widget/AsyncAdapter$AsyncLoadMoreParams;",
            ")",
            "Ljava/util/List",
            "<",
            "Lmiui/app/resourcebrowser/resource/Resource;",
            ">;"
        }
    .end annotation

    .prologue
    iget-boolean v4, p1, Lmiui/widget/AsyncAdapter$AsyncLoadMoreParams;->upwards:Z

    if-nez v4, :cond_8

    iget v4, p1, Lmiui/widget/AsyncAdapter$AsyncLoadMoreParams;->cursor:I

    if-eqz v4, :cond_a

    :cond_8
    const/4 v2, 0x0

    :cond_9
    :goto_9
    return-object v2

    :cond_a
    const/4 v2, 0x0

    .local v2, resources:Ljava/util/List;,"Ljava/util/List<Lmiui/app/resourcebrowser/resource/Resource;>;"
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->mUrl:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "&start=%s&count=%s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget v8, p1, Lmiui/widget/AsyncAdapter$AsyncLoadMoreParams;->cursor:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const/16 v8, 0x1e

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .local v3, url:Ljava/lang/String;
    iget-object v4, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->mListFolder:Ljava/lang/String;

    invoke-static {v4, v3}, Lmiui/app/resourcebrowser/util/ResourceHelper;->getFilePathByURL(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .local v1, filePath:Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .local v0, file:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_9

    iget-object v4, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->mParser:Lmiui/app/resourcebrowser/service/ResourceDataParser;

    iget-object v5, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mMetaData:Landroid/os/Bundle;

    invoke-virtual {v4, v1, v5}, Lmiui/app/resourcebrowser/service/ResourceDataParser;->readResources(Ljava/lang/String;Landroid/os/Bundle;)Ljava/util/List;

    move-result-object v2

    goto :goto_9
.end method

.method protected postLoadMoreData(Ljava/util/List;)V
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lmiui/app/resourcebrowser/resource/Resource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, result:Ljava/util/List;,"Ljava/util/List<Lmiui/app/resourcebrowser/resource/Resource;>;"
    if-nez p1, :cond_1b

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mFragment:Lmiui/app/resourcebrowser/BaseFragment;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mFragment:Lmiui/app/resourcebrowser/BaseFragment;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/BaseFragment;->isVisiableForUser()Z

    move-result v0

    if-eqz v0, :cond_1b

    :cond_e
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mContext:Landroid/app/Activity;

    const v1, 0x60c0020

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_1b
    return-void
.end method

.method public setUrl(Ljava/lang/String;)V
    .registers 2
    .parameter "url"

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->mUrl:Ljava/lang/String;

    invoke-direct {p0}, Lmiui/app/resourcebrowser/OnlineResourceAdapter;->resetResources()V

    return-void
.end method
