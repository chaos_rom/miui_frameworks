.class public Lmiui/app/resourcebrowser/BaseFragment;
.super Landroid/app/Fragment;
.source "BaseFragment.java"


# instance fields
.field private mVisiableForUser:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiui/app/resourcebrowser/BaseFragment;->mVisiableForUser:Z

    return-void
.end method


# virtual methods
.method public isVisiableForUser()Z
    .registers 2

    .prologue
    iget-boolean v0, p0, Lmiui/app/resourcebrowser/BaseFragment;->mVisiableForUser:Z

    return v0
.end method

.method public onFragmentCreateOptionsMenu(Landroid/view/Menu;)Ljava/util/List;
    .registers 3
    .parameter "menu"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Menu;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method public onFragmentOptionsItemSelected(Landroid/view/MenuItem;)V
    .registers 2
    .parameter "item"

    .prologue
    return-void
.end method

.method public onFragmentPrepareOptionsMenu(Landroid/view/Menu;Z)V
    .registers 3
    .parameter "menu"
    .parameter "goingVisiable"

    .prologue
    return-void
.end method

.method protected onVisiableChanged(Z)V
    .registers 2
    .parameter "visiableForUser"

    .prologue
    iput-boolean p1, p0, Lmiui/app/resourcebrowser/BaseFragment;->mVisiableForUser:Z

    return-void
.end method
