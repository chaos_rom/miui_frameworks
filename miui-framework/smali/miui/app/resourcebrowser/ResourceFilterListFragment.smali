.class public Lmiui/app/resourcebrowser/ResourceFilterListFragment;
.super Lmiui/app/resourcebrowser/LocalResourceListFragment;
.source "ResourceFilterListFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/app/resourcebrowser/ResourceFilterListFragment$FilterAdapter;
    }
.end annotation


# static fields
.field public static final FILTER:Ljava/lang/String; = "type"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    invoke-direct {p0}, Lmiui/app/resourcebrowser/LocalResourceListFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected addMetaData(Landroid/os/Bundle;)V
    .registers 5
    .parameter "metaData"

    .prologue
    invoke-super {p0, p1}, Lmiui/app/resourcebrowser/LocalResourceListFragment;->addMetaData(Landroid/os/Bundle;)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mMetaData:Landroid/os/Bundle;

    const-string v1, "miui.app.resourcebrowser.RESOURCE_SET_SUBPACKAGE"

    const-string v2, ".local"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected getAdapter()Lmiui/app/resourcebrowser/ResourceAdapter;
    .registers 6

    .prologue
    new-instance v0, Lmiui/app/resourcebrowser/ResourceFilterListFragment$FilterAdapter;

    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mMetaData:Landroid/os/Bundle;

    iget-object v2, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "type"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    invoke-direct {v0, p0, v1, v2}, Lmiui/app/resourcebrowser/ResourceFilterListFragment$FilterAdapter;-><init>(Lmiui/app/resourcebrowser/BaseFragment;Landroid/os/Bundle;I)V

    return-object v0
.end method
