.class public Lmiui/app/resourcebrowser/recommended/UnevenGrid;
.super Landroid/view/ViewGroup;
.source "UnevenGrid.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/app/resourcebrowser/recommended/UnevenGrid$GridItemFactory;,
        Lmiui/app/resourcebrowser/recommended/UnevenGrid$GridItemData;,
        Lmiui/app/resourcebrowser/recommended/UnevenGrid$LayoutParams;
    }
.end annotation


# instance fields
.field private mColumnCount:I

.field private mEmptyGrids:Ljava/util/TreeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mEmptyRow:I

.field private mGridHeight:I

.field private mGridItemFactory:Lmiui/app/resourcebrowser/recommended/UnevenGrid$GridItemFactory;

.field private mGridItemGap:I

.field private mGridItemPositions:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mGridItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mGridMaxHeight:I

.field private mGridMaxWidth:I

.field private mGridWidth:I

.field private mHeightRatio:I

.field private mWidthRatio:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mGridItems:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mGridItemPositions:Ljava/util/HashMap;

    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    iput-object v0, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mEmptyGrids:Ljava/util/TreeSet;

    const/4 v0, 0x0

    iput v0, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mEmptyRow:I

    invoke-direct {p0}, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->initialize()V

    return-void
.end method

.method private addGridItem(Landroid/view/View;II)V
    .registers 6
    .parameter "view"
    .parameter "widthCount"
    .parameter "heightCount"

    .prologue
    const/4 v1, 0x1

    if-nez p1, :cond_4

    :goto_3
    return-void

    :cond_4
    iget v0, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mGridMaxWidth:I

    if-le p2, v0, :cond_1d

    iget p2, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mGridMaxWidth:I

    :cond_a
    :goto_a
    iget v0, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mGridMaxHeight:I

    if-le p3, v0, :cond_21

    iget p3, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mGridMaxHeight:I

    :cond_10
    :goto_10
    invoke-static {p2, p3}, Lmiui/app/resourcebrowser/recommended/UnevenGrid$LayoutParams;->create(II)Lmiui/app/resourcebrowser/recommended/UnevenGrid$LayoutParams;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mGridItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_1d
    if-ge p2, v1, :cond_a

    const/4 p2, 0x1

    goto :goto_a

    :cond_21
    if-ge p3, v1, :cond_10

    const/4 p3, 0x1

    goto :goto_10
.end method

.method private computeAndPlace(II)I
    .registers 7
    .parameter "widthCount"
    .parameter "heightCount"

    .prologue
    iget-object v2, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mEmptyGrids:Ljava/util/TreeSet;

    invoke-virtual {v2}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_28

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .local v0, emptyPos:Ljava/lang/Integer;
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {p0, v2, p1, p2}, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->isPlaceable(III)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {p0, v2, p1, p2}, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->place(III)V

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .end local v0           #emptyPos:Ljava/lang/Integer;
    :goto_27
    return v0

    :cond_28
    iget v2, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mEmptyRow:I

    iget v3, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mColumnCount:I

    mul-int v0, v2, v3

    .local v0, emptyPos:I
    invoke-direct {p0, v0, p1, p2}, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->place(III)V

    goto :goto_27
.end method

.method private getChildBottom(II)I
    .registers 4
    .parameter "position"
    .parameter "height"

    .prologue
    invoke-direct {p0, p1}, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->getChildTop(I)I

    move-result v0

    add-int/2addr v0, p2

    return v0
.end method

.method private getChildLeft(I)I
    .registers 5
    .parameter "position"

    .prologue
    iget v0, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mColumnCount:I

    rem-int v0, p1, v0

    iget v1, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mGridWidth:I

    iget v2, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mGridItemGap:I

    add-int/2addr v1, v2

    mul-int/2addr v0, v1

    return v0
.end method

.method private getChildRight(II)I
    .registers 4
    .parameter "position"
    .parameter "width"

    .prologue
    invoke-direct {p0, p1}, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->getChildLeft(I)I

    move-result v0

    add-int/2addr v0, p2

    return v0
.end method

.method private getChildTop(I)I
    .registers 5
    .parameter "position"

    .prologue
    iget v0, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mColumnCount:I

    div-int v0, p1, v0

    iget v1, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mGridHeight:I

    iget v2, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mGridItemGap:I

    add-int/2addr v1, v2

    mul-int/2addr v0, v1

    return v0
.end method

.method private initialize()V
    .registers 3

    .prologue
    const/4 v1, 0x2

    iput v1, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mColumnCount:I

    const/4 v0, 0x0

    iput v0, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mGridItemGap:I

    iput v1, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mGridMaxWidth:I

    iput v1, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mGridMaxHeight:I

    const/4 v0, 0x5

    iput v0, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mWidthRatio:I

    const/4 v0, 0x3

    iput v0, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mHeightRatio:I

    return-void
.end method

.method private isPlaceable(III)Z
    .registers 11
    .parameter "position"
    .parameter "widthCount"
    .parameter "heightCount"

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    iget v5, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mColumnCount:I

    iget v6, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mColumnCount:I

    rem-int v6, p1, v6

    sub-int/2addr v5, v6

    if-ge v5, p2, :cond_c

    :cond_b
    :goto_b
    return v3

    :cond_c
    const/4 v0, 0x0

    .local v0, i:I
    :goto_d
    if-ge v0, p3, :cond_34

    const/4 v1, 0x0

    .local v1, j:I
    :goto_10
    if-ge v1, p2, :cond_31

    iget v5, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mColumnCount:I

    mul-int/2addr v5, v0

    add-int/2addr v5, p1

    add-int v2, v5, v1

    .local v2, needPos:I
    iget v5, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mColumnCount:I

    div-int v5, v2, v5

    iget v6, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mEmptyRow:I

    if-lt v5, v6, :cond_22

    move v3, v4

    goto :goto_b

    :cond_22
    iget-object v5, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mEmptyGrids:Ljava/util/TreeSet;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/TreeSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_b

    add-int/lit8 v1, v1, 0x1

    goto :goto_10

    .end local v2           #needPos:I
    :cond_31
    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    .end local v1           #j:I
    :cond_34
    move v3, v4

    goto :goto_b
.end method

.method private layoutItems()V
    .registers 9

    .prologue
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->removeAllViews()V

    iget-object v6, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mGridItemPositions:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->clear()V

    iget-object v6, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mEmptyGrids:Ljava/util/TreeSet;

    invoke-virtual {v6}, Ljava/util/TreeSet;->clear()V

    const/4 v6, 0x0

    iput v6, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mEmptyRow:I

    iget-object v6, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mGridItems:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_16
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    .local v4, view:Landroid/view/View;
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Lmiui/app/resourcebrowser/recommended/UnevenGrid$LayoutParams;

    .local v2, params:Lmiui/app/resourcebrowser/recommended/UnevenGrid$LayoutParams;
    iget v5, v2, Lmiui/app/resourcebrowser/recommended/UnevenGrid$LayoutParams;->widthCount:I

    .local v5, widthCount:I
    iget v0, v2, Lmiui/app/resourcebrowser/recommended/UnevenGrid$LayoutParams;->heightCount:I

    .local v0, heightCount:I
    invoke-direct {p0, v5, v0}, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->computeAndPlace(II)I

    move-result v3

    .local v3, pos:I
    iget-object v6, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mGridItemPositions:Ljava/util/HashMap;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v4, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0, v4}, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->addView(Landroid/view/View;)V

    goto :goto_16

    .end local v0           #heightCount:I
    .end local v2           #params:Lmiui/app/resourcebrowser/recommended/UnevenGrid$LayoutParams;
    .end local v3           #pos:I
    .end local v4           #view:Landroid/view/View;
    .end local v5           #widthCount:I
    :cond_3d
    return-void
.end method

.method private place(III)V
    .registers 11
    .parameter "position"
    .parameter "widthCount"
    .parameter "heightCount"

    .prologue
    iget v5, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mColumnCount:I

    div-int v5, p1, v5

    add-int/2addr v5, p3

    add-int/lit8 v2, v5, -0x1

    .local v2, maxRow:I
    iget v0, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mEmptyRow:I

    .local v0, i:I
    :goto_9
    if-gt v0, v2, :cond_24

    const/4 v1, 0x0

    .local v1, j:I
    :goto_c
    iget v5, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mColumnCount:I

    if-ge v1, v5, :cond_21

    iget v5, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mColumnCount:I

    mul-int/2addr v5, v0

    add-int v4, v5, v1

    .local v4, pos:I
    iget-object v5, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mEmptyGrids:Ljava/util/TreeSet;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_c

    .end local v4           #pos:I
    :cond_21
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .end local v1           #j:I
    :cond_24
    iget v5, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mEmptyRow:I

    if-gt v5, v2, :cond_2c

    add-int/lit8 v5, v2, 0x1

    iput v5, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mEmptyRow:I

    :cond_2c
    const/4 v0, 0x0

    :goto_2d
    if-ge v0, p3, :cond_47

    const/4 v1, 0x0

    .restart local v1       #j:I
    :goto_30
    if-ge v1, p2, :cond_44

    iget v5, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mColumnCount:I

    mul-int/2addr v5, v0

    add-int/2addr v5, p1

    add-int v3, v5, v1

    .local v3, needPos:I
    iget-object v5, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mEmptyGrids:Ljava/util/TreeSet;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_30

    .end local v3           #needPos:I
    :cond_44
    add-int/lit8 v0, v0, 0x1

    goto :goto_2d

    .end local v1           #j:I
    :cond_47
    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .registers 16
    .parameter "changed"
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    .prologue
    const/4 v4, 0x0

    .local v4, i:I
    :goto_1
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->getChildCount()I

    move-result v5

    if-ge v4, v5, :cond_41

    invoke-virtual {p0, v4}, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .local v0, child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    .local v3, childWidth:I
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    .local v1, childHeight:I
    iget-object v5, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mGridItemPositions:Ljava/util/HashMap;

    invoke-virtual {v5, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .local v2, childPos:I
    iget v5, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mPaddingLeft:I

    invoke-direct {p0, v2}, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->getChildLeft(I)I

    move-result v6

    add-int/2addr v5, v6

    iget v6, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mPaddingTop:I

    invoke-direct {p0, v2}, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->getChildTop(I)I

    move-result v7

    add-int/2addr v6, v7

    iget v7, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mPaddingLeft:I

    invoke-direct {p0, v2, v3}, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->getChildRight(II)I

    move-result v8

    add-int/2addr v7, v8

    iget v8, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mPaddingTop:I

    invoke-direct {p0, v2, v1}, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->getChildBottom(II)I

    move-result v9

    add-int/2addr v8, v9

    invoke-virtual {v0, v5, v6, v7, v8}, Landroid/view/View;->layout(IIII)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .end local v0           #child:Landroid/view/View;
    .end local v1           #childHeight:I
    .end local v2           #childPos:I
    .end local v3           #childWidth:I
    :cond_41
    return-void
.end method

.method protected onMeasure(II)V
    .registers 16
    .parameter "widthSpec"
    .parameter "heightSpec"

    .prologue
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v8

    .local v8, width:I
    iget v10, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mPaddingLeft:I

    sub-int v10, v8, v10

    iget v11, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mPaddingRight:I

    sub-int/2addr v10, v11

    iget v11, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mColumnCount:I

    add-int/lit8 v11, v11, -0x1

    iget v12, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mGridItemGap:I

    mul-int/2addr v11, v12

    sub-int/2addr v10, v11

    iget v11, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mColumnCount:I

    div-int/2addr v10, v11

    iput v10, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mGridWidth:I

    iget v10, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mGridWidth:I

    iget v11, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mHeightRatio:I

    mul-int/2addr v10, v11

    iget v11, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mWidthRatio:I

    div-int/2addr v10, v11

    iput v10, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mGridHeight:I

    const/4 v0, 0x0

    .local v0, bottom:I
    const/4 v6, 0x0

    .local v6, i:I
    :goto_24
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->getChildCount()I

    move-result v10

    if-ge v6, v10, :cond_72

    invoke-virtual {p0, v6}, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .local v1, child:Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Lmiui/app/resourcebrowser/recommended/UnevenGrid$LayoutParams;

    .local v7, param:Lmiui/app/resourcebrowser/recommended/UnevenGrid$LayoutParams;
    iget v9, v7, Lmiui/app/resourcebrowser/recommended/UnevenGrid$LayoutParams;->widthCount:I

    .local v9, widthCount:I
    iget v5, v7, Lmiui/app/resourcebrowser/recommended/UnevenGrid$LayoutParams;->heightCount:I

    .local v5, heightCount:I
    iget v10, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mGridWidth:I

    mul-int/2addr v10, v9

    add-int/lit8 v11, v9, -0x1

    iget v12, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mGridItemGap:I

    mul-int/2addr v11, v12

    add-int v4, v10, v11

    .local v4, childWidth:I
    iget v10, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mGridHeight:I

    mul-int/2addr v10, v5

    add-int/lit8 v11, v5, -0x1

    iget v12, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mGridItemGap:I

    mul-int/2addr v11, v12

    add-int v2, v10, v11

    .local v2, childHeight:I
    iget-object v10, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mGridItemPositions:Ljava/util/HashMap;

    invoke-virtual {v10, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .local v3, childPos:I
    const/high16 v10, 0x4000

    invoke-static {v4, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    const/high16 v11, 0x4000

    invoke-static {v2, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v11

    invoke-virtual {v1, v10, v11}, Landroid/view/View;->measure(II)V

    invoke-direct {p0, v3, v2}, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->getChildBottom(II)I

    move-result v10

    invoke-static {v0, v10}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/lit8 v6, v6, 0x1

    goto :goto_24

    .end local v1           #child:Landroid/view/View;
    .end local v2           #childHeight:I
    .end local v3           #childPos:I
    .end local v4           #childWidth:I
    .end local v5           #heightCount:I
    .end local v7           #param:Lmiui/app/resourcebrowser/recommended/UnevenGrid$LayoutParams;
    .end local v9           #widthCount:I
    :cond_72
    iget v10, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mPaddingTop:I

    add-int/2addr v10, v0

    iget v11, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mPaddingBottom:I

    add-int/2addr v10, v11

    invoke-virtual {p0, v8, v10}, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->setMeasuredDimension(II)V

    return-void
.end method

.method public relayout()V
    .registers 1

    .prologue
    invoke-direct {p0}, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->layoutItems()V

    return-void
.end method

.method public setColumnCount(I)V
    .registers 2
    .parameter "columnCount"

    .prologue
    if-lez p1, :cond_4

    iput p1, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mColumnCount:I

    :cond_4
    return-void
.end method

.method public setGridItemFactory(Lmiui/app/resourcebrowser/recommended/UnevenGrid$GridItemFactory;)V
    .registers 2
    .parameter "gridItemFactory"

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mGridItemFactory:Lmiui/app/resourcebrowser/recommended/UnevenGrid$GridItemFactory;

    return-void
.end method

.method public setGridItemGap(I)V
    .registers 2
    .parameter "gridItemGap"

    .prologue
    if-ltz p1, :cond_4

    iput p1, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mGridItemGap:I

    :cond_4
    return-void
.end method

.method public setGridItemMaxSize(II)V
    .registers 3
    .parameter "maxWidth"
    .parameter "maxHeight"

    .prologue
    if-lez p1, :cond_8

    if-lez p2, :cond_8

    iput p1, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mGridMaxWidth:I

    iput p2, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mGridMaxHeight:I

    :cond_8
    return-void
.end method

.method public setGridItemRatio(II)V
    .registers 3
    .parameter "width"
    .parameter "height"

    .prologue
    if-lez p1, :cond_8

    if-lez p2, :cond_8

    iput p1, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mWidthRatio:I

    iput p2, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mHeightRatio:I

    :cond_8
    return-void
.end method

.method public updateData(Ljava/util/List;)V
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lmiui/app/resourcebrowser/recommended/UnevenGrid$GridItemData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, datas:Ljava/util/List;,"Ljava/util/List<+Lmiui/app/resourcebrowser/recommended/UnevenGrid$GridItemData;>;"
    if-eqz p1, :cond_c

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_c

    iget-object v3, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mGridItemFactory:Lmiui/app/resourcebrowser/recommended/UnevenGrid$GridItemFactory;

    if-nez v3, :cond_d

    :cond_c
    :goto_c
    return-void

    :cond_d
    iget-object v3, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mGridItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :cond_16
    :goto_16
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_32

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/app/resourcebrowser/recommended/UnevenGrid$GridItemData;

    .local v0, data:Lmiui/app/resourcebrowser/recommended/UnevenGrid$GridItemData;
    iget-object v3, p0, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->mGridItemFactory:Lmiui/app/resourcebrowser/recommended/UnevenGrid$GridItemFactory;

    invoke-interface {v3, v0}, Lmiui/app/resourcebrowser/recommended/UnevenGrid$GridItemFactory;->createGridItem(Lmiui/app/resourcebrowser/recommended/UnevenGrid$GridItemData;)Landroid/view/View;

    move-result-object v2

    .local v2, view:Landroid/view/View;
    if-eqz v2, :cond_16

    iget v3, v0, Lmiui/app/resourcebrowser/recommended/UnevenGrid$GridItemData;->widthCount:I

    iget v4, v0, Lmiui/app/resourcebrowser/recommended/UnevenGrid$GridItemData;->heightCount:I

    invoke-direct {p0, v2, v3, v4}, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->addGridItem(Landroid/view/View;II)V

    goto :goto_16

    .end local v0           #data:Lmiui/app/resourcebrowser/recommended/UnevenGrid$GridItemData;
    .end local v2           #view:Landroid/view/View;
    :cond_32
    invoke-direct {p0}, Lmiui/app/resourcebrowser/recommended/UnevenGrid;->layoutItems()V

    goto :goto_c
.end method
