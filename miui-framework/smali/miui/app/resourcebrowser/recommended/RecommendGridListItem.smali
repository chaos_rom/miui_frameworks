.class public Lmiui/app/resourcebrowser/recommended/RecommendGridListItem;
.super Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem;
.source "RecommendGridListItem.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .parameter "context"
    .parameter "attrs"

    .prologue
    invoke-direct {p0, p1, p2}, Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method static synthetic access$000(Lmiui/app/resourcebrowser/recommended/RecommendGridListItem;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method protected getOnClickListener(Landroid/os/Bundle;)Landroid/view/View$OnClickListener;
    .registers 3
    .parameter "metaData"

    .prologue
    new-instance v0, Lmiui/app/resourcebrowser/recommended/RecommendGridListItem$1;

    invoke-direct {v0, p0, p1}, Lmiui/app/resourcebrowser/recommended/RecommendGridListItem$1;-><init>(Lmiui/app/resourcebrowser/recommended/RecommendGridListItem;Landroid/os/Bundle;)V

    return-object v0
.end method

.method protected getRecommendedResourceListActivity(Landroid/os/Bundle;)Landroid/util/Pair;
    .registers 5
    .parameter "metaData"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    new-instance v0, Landroid/util/Pair;

    const-string v1, "miui.app.resourcebrowser.RECOMMENDATION_LIST_ACTIVITY_PACKAGE"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "miui.app.resourcebrowser.RECOMMENDATION_LIST_ACTIVITY_CLASS"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method
