.class public Lmiui/app/resourcebrowser/recommended/RecommendGridItemFactory;
.super Ljava/lang/Object;
.source "RecommendGridItemFactory.java"

# interfaces
.implements Lmiui/app/resourcebrowser/recommended/UnevenGrid$GridItemFactory;


# static fields
.field private static sGridTypeLayoutMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mContext:Landroid/content/Context;

.field private mMetaData:Landroid/os/Bundle;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lmiui/app/resourcebrowser/recommended/RecommendGridItemFactory;->sGridTypeLayoutMap:Ljava/util/Map;

    sget-object v0, Lmiui/app/resourcebrowser/recommended/RecommendGridItemFactory;->sGridTypeLayoutMap:Ljava/util/Map;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x6030036

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/app/resourcebrowser/recommended/RecommendGridItemFactory;->sGridTypeLayoutMap:Ljava/util/Map;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x6030035

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Bundle;)V
    .registers 3
    .parameter "context"
    .parameter "metaData"

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmiui/app/resourcebrowser/recommended/RecommendGridItemFactory;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lmiui/app/resourcebrowser/recommended/RecommendGridItemFactory;->mMetaData:Landroid/os/Bundle;

    return-void
.end method


# virtual methods
.method public createGridItem(Lmiui/app/resourcebrowser/recommended/UnevenGrid$GridItemData;)Landroid/view/View;
    .registers 8
    .parameter "gridItemData"

    .prologue
    const/4 v1, 0x0

    if-nez p1, :cond_4

    :cond_3
    :goto_3
    return-object v1

    :cond_4
    move-object v0, p1

    check-cast v0, Lmiui/app/resourcebrowser/recommended/RecommendItemData;

    .local v0, data:Lmiui/app/resourcebrowser/recommended/RecommendItemData;
    iget v2, v0, Lmiui/app/resourcebrowser/recommended/RecommendItemData;->itemType:I

    .local v2, type:I
    if-eqz v2, :cond_3

    iget-object v3, p0, Lmiui/app/resourcebrowser/recommended/RecommendGridItemFactory;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    sget-object v3, Lmiui/app/resourcebrowser/recommended/RecommendGridItemFactory;->sGridTypeLayoutMap:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v5, 0x0

    invoke-virtual {v4, v3, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem;

    .local v1, item:Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem;
    iget-object v3, p0, Lmiui/app/resourcebrowser/recommended/RecommendGridItemFactory;->mMetaData:Landroid/os/Bundle;

    invoke-virtual {v1, v0, v3}, Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem;->bind(Lmiui/app/resourcebrowser/recommended/RecommendItemData;Landroid/os/Bundle;)V

    goto :goto_3
.end method
