.class public abstract Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem;
.super Landroid/widget/FrameLayout;
.source "RecommendGridBaseItem.java"

# interfaces
.implements Lmiui/app/resourcebrowser/IntentConstants;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem$DownloadThumbnailTask;
    }
.end annotation


# instance fields
.field private mImageView:Landroid/widget/ImageView;

.field protected mRecommendItemData:Lmiui/app/resourcebrowser/recommended/RecommendItemData;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .parameter "context"
    .parameter "attrs"

    .prologue
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method static synthetic access$000(Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem;)Landroid/widget/ImageView;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem;->mImageView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$100(Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public bind(Lmiui/app/resourcebrowser/recommended/RecommendItemData;Landroid/os/Bundle;)V
    .registers 9
    .parameter "gridItemData"
    .parameter "metaData"

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem;->mRecommendItemData:Lmiui/app/resourcebrowser/recommended/RecommendItemData;

    iget-object v4, p0, Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {p0, p2}, Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem;->getOnClickListener(Landroid/os/Bundle;)Landroid/view/View$OnClickListener;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v0, 0x0

    .local v0, bitmap:Landroid/graphics/Bitmap;
    new-instance v2, Ljava/io/File;

    iget-object v4, p1, Lmiui/app/resourcebrowser/recommended/RecommendItemData;->localThumbnail:Ljava/lang/String;

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .local v2, file:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_2d

    new-instance v4, Lmiui/util/InputStreamLoader;

    iget-object v5, p1, Lmiui/app/resourcebrowser/recommended/RecommendItemData;->localThumbnail:Ljava/lang/String;

    invoke-direct {v4, v5}, Lmiui/util/InputStreamLoader;-><init>(Ljava/lang/String;)V

    const/4 v5, -0x1

    invoke-static {v4, v5}, Lmiui/util/ImageUtils;->getBitmap(Lmiui/util/InputStreamLoader;I)Landroid/graphics/Bitmap;

    move-result-object v0

    :cond_25
    :goto_25
    if-eqz v0, :cond_2c

    iget-object v4, p0, Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_2c
    return-void

    :cond_2d
    iget-object v4, p1, Lmiui/app/resourcebrowser/recommended/RecommendItemData;->onlineThumbnail:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_25

    new-instance v3, Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem$DownloadThumbnailTask;

    invoke-direct {v3, p0}, Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem$DownloadThumbnailTask;-><init>(Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem;)V

    .local v3, task:Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem$DownloadThumbnailTask;
    new-instance v1, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;

    invoke-direct {v1}, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;-><init>()V

    .local v1, entry:Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;
    iget-object v4, p1, Lmiui/app/resourcebrowser/recommended/RecommendItemData;->localThumbnail:Ljava/lang/String;

    invoke-virtual {v1, v4}, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->setPath(Ljava/lang/String;)V

    iget-object v4, p1, Lmiui/app/resourcebrowser/recommended/RecommendItemData;->onlineThumbnail:Ljava/lang/String;

    invoke-virtual {v1, v4}, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->setUrl(Ljava/lang/String;)V

    const/4 v4, 0x1

    new-array v4, v4, [Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-virtual {v3, v4}, Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem$DownloadThumbnailTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_25
.end method

.method public getItemData()Lmiui/app/resourcebrowser/recommended/RecommendItemData;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem;->mRecommendItemData:Lmiui/app/resourcebrowser/recommended/RecommendItemData;

    return-object v0
.end method

.method protected abstract getOnClickListener(Landroid/os/Bundle;)Landroid/view/View$OnClickListener;
.end method

.method protected onFinishInflate()V
    .registers 2

    .prologue
    const v0, 0x60b006a

    invoke-virtual {p0, v0}, Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem;->mImageView:Landroid/widget/ImageView;

    return-void
.end method
