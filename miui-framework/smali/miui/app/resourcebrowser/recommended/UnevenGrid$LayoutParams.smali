.class public Lmiui/app/resourcebrowser/recommended/UnevenGrid$LayoutParams;
.super Landroid/view/ViewGroup$MarginLayoutParams;
.source "UnevenGrid.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/app/resourcebrowser/recommended/UnevenGrid;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LayoutParams"
.end annotation


# instance fields
.field heightCount:I

.field widthCount:I


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    const/4 v0, -0x1

    invoke-direct {p0, v0, v0}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .parameter "c"
    .parameter "attrs"

    .prologue
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public static create(II)Lmiui/app/resourcebrowser/recommended/UnevenGrid$LayoutParams;
    .registers 3
    .parameter "widthCount"
    .parameter "heightCount"

    .prologue
    new-instance v0, Lmiui/app/resourcebrowser/recommended/UnevenGrid$LayoutParams;

    invoke-direct {v0}, Lmiui/app/resourcebrowser/recommended/UnevenGrid$LayoutParams;-><init>()V

    .local v0, param:Lmiui/app/resourcebrowser/recommended/UnevenGrid$LayoutParams;
    iput p0, v0, Lmiui/app/resourcebrowser/recommended/UnevenGrid$LayoutParams;->widthCount:I

    iput p1, v0, Lmiui/app/resourcebrowser/recommended/UnevenGrid$LayoutParams;->heightCount:I

    return-object v0
.end method
