.class public Lmiui/app/resourcebrowser/recommended/RecommendItemData;
.super Lmiui/app/resourcebrowser/recommended/UnevenGrid$GridItemData;
.source "RecommendItemData.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final TYPE_APP:I = 0x1

.field public static final TYPE_LIST:I = 0x2

.field public static final TYPE_UNKNOWN:I = 0x0

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public itemId:Ljava/lang/String;

.field public itemType:I

.field public localThumbnail:Ljava/lang/String;

.field public onlineThumbnail:Ljava/lang/String;

.field public subItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lmiui/app/resourcebrowser/recommended/RecommendItemData;",
            ">;"
        }
    .end annotation
.end field

.field public title:Ljava/lang/String;

.field public type:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    invoke-direct {p0}, Lmiui/app/resourcebrowser/recommended/UnevenGrid$GridItemData;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/app/resourcebrowser/recommended/RecommendItemData;->subItems:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/recommended/RecommendItemData;->title:Ljava/lang/String;

    return-object v0
.end method
