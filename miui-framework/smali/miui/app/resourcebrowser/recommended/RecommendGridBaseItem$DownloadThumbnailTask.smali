.class public Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem$DownloadThumbnailTask;
.super Lmiui/app/resourcebrowser/util/DownloadFileTask;
.source "RecommendGridBaseItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "DownloadThumbnailTask"
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem;


# direct methods
.method public constructor <init>(Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem;)V
    .registers 2
    .parameter

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem$DownloadThumbnailTask;->this$0:Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem;

    invoke-direct {p0}, Lmiui/app/resourcebrowser/util/DownloadFileTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .registers 2
    .parameter "x0"

    .prologue
    check-cast p1, [Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem$DownloadThumbnailTask;->onProgressUpdate([Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;)V
    .registers 8
    .parameter "values"

    .prologue
    const/4 v5, 0x0

    if-eqz p1, :cond_1b

    array-length v3, p1

    if-lez v3, :cond_1b

    aget-object v2, p1, v5

    .local v2, entry:Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;
    if-eqz v2, :cond_21

    :try_start_a
    invoke-virtual {v2}, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .local v0, bitmap:Landroid/graphics/Bitmap;
    iget-object v3, p0, Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem$DownloadThumbnailTask;->this$0:Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem;

    #getter for: Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem;->mImageView:Landroid/widget/ImageView;
    invoke-static {v3}, Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem;->access$000(Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem;)Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V
    :try_end_1b
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_1b} :catch_1c

    .end local v0           #bitmap:Landroid/graphics/Bitmap;
    .end local v2           #entry:Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;
    :cond_1b
    :goto_1b
    return-void

    .restart local v2       #entry:Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;
    :catch_1c
    move-exception v1

    .local v1, e:Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1b

    .end local v1           #e:Ljava/lang/Exception;
    :cond_21
    iget-object v3, p0, Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem$DownloadThumbnailTask;->this$0:Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem;

    #getter for: Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem;->access$100(Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x60c0024

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_1b
.end method
