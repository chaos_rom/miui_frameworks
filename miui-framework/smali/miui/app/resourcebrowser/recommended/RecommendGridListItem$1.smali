.class Lmiui/app/resourcebrowser/recommended/RecommendGridListItem$1;
.super Ljava/lang/Object;
.source "RecommendGridListItem.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiui/app/resourcebrowser/recommended/RecommendGridListItem;->getOnClickListener(Landroid/os/Bundle;)Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/app/resourcebrowser/recommended/RecommendGridListItem;

.field final synthetic val$metaData:Landroid/os/Bundle;


# direct methods
.method constructor <init>(Lmiui/app/resourcebrowser/recommended/RecommendGridListItem;Landroid/os/Bundle;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/recommended/RecommendGridListItem$1;->this$0:Lmiui/app/resourcebrowser/recommended/RecommendGridListItem;

    iput-object p2, p0, Lmiui/app/resourcebrowser/recommended/RecommendGridListItem$1;->val$metaData:Landroid/os/Bundle;

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 8
    .parameter "v"

    .prologue
    const/4 v5, 0x1

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .local v1, intent:Landroid/content/Intent;
    iget-object v2, p0, Lmiui/app/resourcebrowser/recommended/RecommendGridListItem$1;->this$0:Lmiui/app/resourcebrowser/recommended/RecommendGridListItem;

    iget-object v3, p0, Lmiui/app/resourcebrowser/recommended/RecommendGridListItem$1;->val$metaData:Landroid/os/Bundle;

    invoke-virtual {v2, v3}, Lmiui/app/resourcebrowser/recommended/RecommendGridListItem;->getRecommendedResourceListActivity(Landroid/os/Bundle;)Landroid/util/Pair;

    move-result-object v0

    .local v0, activityClass:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v2, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    iget-object v3, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v2, 0x400

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget-object v2, p0, Lmiui/app/resourcebrowser/recommended/RecommendGridListItem$1;->val$metaData:Landroid/os/Bundle;

    const-string v3, "miui.app.resourcebrowser.IS_RECOMMENDATION_LIST"

    invoke-virtual {v2, v3, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v2, p0, Lmiui/app/resourcebrowser/recommended/RecommendGridListItem$1;->val$metaData:Landroid/os/Bundle;

    const-string v3, "miui.app.resourcebrowser.RECOMMENDATION_ID"

    iget-object v4, p0, Lmiui/app/resourcebrowser/recommended/RecommendGridListItem$1;->this$0:Lmiui/app/resourcebrowser/recommended/RecommendGridListItem;

    iget-object v4, v4, Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem;->mRecommendItemData:Lmiui/app/resourcebrowser/recommended/RecommendItemData;

    iget-object v4, v4, Lmiui/app/resourcebrowser/recommended/RecommendItemData;->itemId:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lmiui/app/resourcebrowser/recommended/RecommendGridListItem$1;->val$metaData:Landroid/os/Bundle;

    const-string v4, "miui.app.resourcebrowser.SUB_RECOMMENDATIONS"

    iget-object v2, p0, Lmiui/app/resourcebrowser/recommended/RecommendGridListItem$1;->this$0:Lmiui/app/resourcebrowser/recommended/RecommendGridListItem;

    iget-object v2, v2, Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem;->mRecommendItemData:Lmiui/app/resourcebrowser/recommended/RecommendItemData;

    iget-object v2, v2, Lmiui/app/resourcebrowser/recommended/RecommendItemData;->subItems:Ljava/util/List;

    check-cast v2, Ljava/io/Serializable;

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    const-string v2, "META_DATA_FOR_ONLINE"

    iget-object v3, p0, Lmiui/app/resourcebrowser/recommended/RecommendGridListItem$1;->val$metaData:Landroid/os/Bundle;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    iget-object v2, p0, Lmiui/app/resourcebrowser/recommended/RecommendGridListItem$1;->this$0:Lmiui/app/resourcebrowser/recommended/RecommendGridListItem;

    #getter for: Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lmiui/app/resourcebrowser/recommended/RecommendGridListItem;->access$000(Lmiui/app/resourcebrowser/recommended/RecommendGridListItem;)Landroid/content/Context;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2, v1, v5}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method
