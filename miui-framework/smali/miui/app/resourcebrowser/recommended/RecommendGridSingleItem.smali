.class public Lmiui/app/resourcebrowser/recommended/RecommendGridSingleItem;
.super Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem;
.source "RecommendGridSingleItem.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .parameter "context"
    .parameter "attrs"

    .prologue
    invoke-direct {p0, p1, p2}, Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method static synthetic access$000(Lmiui/app/resourcebrowser/recommended/RecommendGridSingleItem;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method protected buildResourceSet(Landroid/os/Bundle;)V
    .registers 16
    .parameter "metaData"

    .prologue
    const/4 v13, 0x0

    new-instance v8, Lmiui/app/resourcebrowser/resource/Resource;

    invoke-direct {v8}, Lmiui/app/resourcebrowser/resource/Resource;-><init>()V

    .local v8, resource:Lmiui/app/resourcebrowser/resource/Resource;
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .local v5, info:Landroid/os/Bundle;
    iget-object v11, p0, Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem;->mRecommendItemData:Lmiui/app/resourcebrowser/recommended/RecommendItemData;

    iget-object v4, v11, Lmiui/app/resourcebrowser/recommended/RecommendItemData;->itemId:Ljava/lang/String;

    .local v4, id:Ljava/lang/String;
    const-string v11, "ID"

    invoke-virtual {v5, v11, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v11, p0, Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem;->mRecommendItemData:Lmiui/app/resourcebrowser/recommended/RecommendItemData;

    iget-object v10, v11, Lmiui/app/resourcebrowser/recommended/RecommendItemData;->title:Ljava/lang/String;

    .local v10, title:Ljava/lang/String;
    const-string v11, "NAME"

    invoke-virtual {v5, v11, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lmiui/app/resourcebrowser/service/online/OnlineService;->getInstance()Lmiui/app/resourcebrowser/service/online/OnlineService;

    move-result-object v11

    iget-object v12, p0, Lmiui/app/resourcebrowser/recommended/RecommendGridBaseItem;->mRecommendItemData:Lmiui/app/resourcebrowser/recommended/RecommendItemData;

    iget-object v12, v12, Lmiui/app/resourcebrowser/recommended/RecommendItemData;->itemId:Ljava/lang/String;

    invoke-virtual {v11, v12}, Lmiui/app/resourcebrowser/service/online/OnlineService;->getDownloadUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .local v7, onlinePath:Ljava/lang/String;
    const-string v11, "ONLINE_PATH"

    invoke-virtual {v5, v11, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v11, "miui.app.resourcebrowser.RESOURCE_FILE_EXTENSION"

    invoke-virtual {p1, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .local v2, extension:Ljava/lang/String;
    const-string v11, "miui.app.resourcebrowser.DOWNLOAD_FOLDER"

    invoke-virtual {p1, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lmiui/os/ExtraFileUtils;->standardizeFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .local v0, downloadFolder:Ljava/lang/String;
    const-string v11, "miui.app.resourcebrowser.DOWNLOAD_FOLDER_EXTRA"

    invoke-virtual {p1, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lmiui/os/ExtraFileUtils;->standardizeFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .local v1, downloadFolderExtra:Ljava/lang/String;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .local v6, localPath:Ljava/lang/String;
    const-string v11, ".mp3"

    invoke-virtual {v2, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_75

    const-string v11, ".jpg"

    invoke-virtual {v2, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_75

    const-string v11, ".ogg"

    invoke-virtual {v2, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_d5

    :cond_75
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_90

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    :cond_90
    :goto_90
    const-string v11, "LOCAL_PATH"

    invoke-virtual {v5, v11, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v5}, Lmiui/app/resourcebrowser/resource/Resource;->setInformation(Landroid/os/Bundle;)V

    const-string v11, "miui.app.resourcebrowser.RESOURCE_SET_SUBPACKAGE"

    const-string v12, ".single"

    invoke-virtual {p1, v11, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v11, "miui.app.resourcebrowser.RESOURCE_GROUP"

    invoke-virtual {p1, v11, v13}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v11, "miui.app.resourcebrowser.RESOURCE_INDEX"

    invoke-virtual {p1, v11, v13}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "miui.app.resourcebrowser.RESOURCE_SET_PACKAGE"

    invoke-virtual {p1, v12}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ".single"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lmiui/app/resourcebrowser/resource/ResourceSet;->getInstance(Ljava/lang/String;)Lmiui/app/resourcebrowser/resource/ResourceSet;

    move-result-object v9

    .local v9, resourceSet:Lmiui/app/resourcebrowser/resource/ResourceSet;
    invoke-virtual {v9}, Lmiui/app/resourcebrowser/resource/ResourceSet;->clear()V

    new-instance v3, Lmiui/widget/DataGroup;

    invoke-direct {v3}, Lmiui/widget/DataGroup;-><init>()V

    .local v3, group:Lmiui/widget/DataGroup;,"Lmiui/widget/DataGroup<Lmiui/app/resourcebrowser/resource/Resource;>;"
    invoke-virtual {v3, v8}, Lmiui/widget/DataGroup;->add(Ljava/lang/Object;)Z

    invoke-virtual {v9, v3}, Lmiui/app/resourcebrowser/resource/ResourceSet;->add(Ljava/lang/Object;)Z

    return-void

    .end local v3           #group:Lmiui/widget/DataGroup;,"Lmiui/widget/DataGroup<Lmiui/app/resourcebrowser/resource/Resource;>;"
    .end local v9           #resourceSet:Lmiui/app/resourcebrowser/resource/ResourceSet;
    :cond_d5
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_90

    new-instance v11, Ljava/io/File;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_90

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_90
.end method

.method protected getOnClickListener(Landroid/os/Bundle;)Landroid/view/View$OnClickListener;
    .registers 3
    .parameter "metaData"

    .prologue
    new-instance v0, Lmiui/app/resourcebrowser/recommended/RecommendGridSingleItem$1;

    invoke-direct {v0, p0, p1}, Lmiui/app/resourcebrowser/recommended/RecommendGridSingleItem$1;-><init>(Lmiui/app/resourcebrowser/recommended/RecommendGridSingleItem;Landroid/os/Bundle;)V

    return-object v0
.end method

.method protected getResourceDetailActivity(Landroid/os/Bundle;)Landroid/util/Pair;
    .registers 5
    .parameter "metaData"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    new-instance v0, Landroid/util/Pair;

    const-string v1, "miui.app.resourcebrowser.DETAIL_ACTIVITY_PACKAGE"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "miui.app.resourcebrowser.DETAIL_ACTIVITY_CLASS"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method
