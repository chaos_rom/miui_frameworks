.class public Lmiui/app/resourcebrowser/LocalResourceAdapter;
.super Lmiui/app/resourcebrowser/ResourceAdapter;
.source "LocalResourceAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/app/resourcebrowser/LocalResourceAdapter$DownloadVersionTask;,
        Lmiui/app/resourcebrowser/LocalResourceAdapter$AsyncLoadResourceTask;
    }
.end annotation


# instance fields
.field protected mFileHashMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lmiui/app/resourcebrowser/resource/Resource;",
            ">;"
        }
    .end annotation
.end field

.field protected mKeyword:Ljava/lang/String;

.field private mParser:Lmiui/app/resourcebrowser/service/ResourceDataParser;

.field private mService:Lmiui/app/resourcebrowser/service/online/OnlineService;

.field private mVersionFolder:Ljava/lang/String;

.field protected mVisitors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lmiui/widget/AsyncAdapter$AsyncLoadDataVisitor",
            "<",
            "Lmiui/app/resourcebrowser/resource/Resource;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Bundle;)V
    .registers 4
    .parameter "context"
    .parameter "metaData"

    .prologue
    invoke-direct {p0, p1, p2}, Lmiui/app/resourcebrowser/ResourceAdapter;-><init>(Landroid/content/Context;Landroid/os/Bundle;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lmiui/app/resourcebrowser/LocalResourceAdapter;->mVisitors:Ljava/util/List;

    invoke-static {}, Lmiui/app/resourcebrowser/service/online/OnlineService;->getInstance()Lmiui/app/resourcebrowser/service/online/OnlineService;

    move-result-object v0

    iput-object v0, p0, Lmiui/app/resourcebrowser/LocalResourceAdapter;->mService:Lmiui/app/resourcebrowser/service/online/OnlineService;

    invoke-static {}, Lmiui/app/resourcebrowser/service/ResourceJSONDataParser;->getInstance()Lmiui/app/resourcebrowser/service/ResourceJSONDataParser;

    move-result-object v0

    iput-object v0, p0, Lmiui/app/resourcebrowser/LocalResourceAdapter;->mParser:Lmiui/app/resourcebrowser/service/ResourceDataParser;

    invoke-direct {p0}, Lmiui/app/resourcebrowser/LocalResourceAdapter;->initParams()V

    return-void
.end method

.method public constructor <init>(Lmiui/app/resourcebrowser/BaseFragment;Landroid/os/Bundle;)V
    .registers 4
    .parameter "fragment"
    .parameter "metaData"

    .prologue
    invoke-direct {p0, p1, p2}, Lmiui/app/resourcebrowser/ResourceAdapter;-><init>(Lmiui/app/resourcebrowser/BaseFragment;Landroid/os/Bundle;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lmiui/app/resourcebrowser/LocalResourceAdapter;->mVisitors:Ljava/util/List;

    invoke-static {}, Lmiui/app/resourcebrowser/service/online/OnlineService;->getInstance()Lmiui/app/resourcebrowser/service/online/OnlineService;

    move-result-object v0

    iput-object v0, p0, Lmiui/app/resourcebrowser/LocalResourceAdapter;->mService:Lmiui/app/resourcebrowser/service/online/OnlineService;

    invoke-static {}, Lmiui/app/resourcebrowser/service/ResourceJSONDataParser;->getInstance()Lmiui/app/resourcebrowser/service/ResourceJSONDataParser;

    move-result-object v0

    iput-object v0, p0, Lmiui/app/resourcebrowser/LocalResourceAdapter;->mParser:Lmiui/app/resourcebrowser/service/ResourceDataParser;

    invoke-direct {p0}, Lmiui/app/resourcebrowser/LocalResourceAdapter;->initParams()V

    return-void
.end method

.method static synthetic access$000(Lmiui/app/resourcebrowser/LocalResourceAdapter;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    invoke-direct {p0, p1}, Lmiui/app/resourcebrowser/LocalResourceAdapter;->checkVersionStatus(Ljava/lang/String;)V

    return-void
.end method

.method private checkVersionStatus(Ljava/lang/String;)V
    .registers 15
    .parameter "path"

    .prologue
    iget-object v10, p0, Lmiui/app/resourcebrowser/LocalResourceAdapter;->mParser:Lmiui/app/resourcebrowser/service/ResourceDataParser;

    iget-object v11, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mMetaData:Landroid/os/Bundle;

    invoke-virtual {v10, p1, v11}, Lmiui/app/resourcebrowser/service/ResourceDataParser;->readUpdatableResources(Ljava/lang/String;Landroid/os/Bundle;)Ljava/util/List;

    move-result-object v9

    .local v9, updatableResources:Ljava/util/List;,"Ljava/util/List<Lmiui/app/resourcebrowser/resource/Resource;>;"
    if-eqz v9, :cond_10

    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v10

    if-eqz v10, :cond_11

    :cond_10
    :goto_10
    return-void

    :cond_11
    const/4 v1, 0x0

    .local v1, i:I
    :goto_12
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v10

    if-ge v1, v10, :cond_91

    invoke-interface {v9, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lmiui/app/resourcebrowser/resource/Resource;

    .local v8, updatableResource:Lmiui/app/resourcebrowser/resource/Resource;
    iget-object v10, p0, Lmiui/app/resourcebrowser/LocalResourceAdapter;->mFileHashMap:Ljava/util/Map;

    invoke-virtual {v8}, Lmiui/app/resourcebrowser/resource/Resource;->getFileHash()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v10, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lmiui/app/resourcebrowser/resource/Resource;

    .local v7, oldResource:Lmiui/app/resourcebrowser/resource/Resource;
    if-eqz v7, :cond_7e

    invoke-virtual {v8}, Lmiui/app/resourcebrowser/resource/Resource;->getId()Ljava/lang/String;

    move-result-object v2

    .local v2, newId:Ljava/lang/String;
    new-instance v5, Ljava/io/File;

    invoke-virtual {v7}, Lmiui/app/resourcebrowser/resource/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v5, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .local v5, oldResFile:Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    .local v6, oldResName:Ljava/lang/String;
    const/16 v10, 0x2e

    invoke-virtual {v6, v10}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v10

    invoke-virtual {v6, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .local v0, extention:Ljava/lang/String;
    const/4 v10, 0x0

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v11

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v12

    sub-int/2addr v11, v12

    invoke-virtual {v6, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .local v4, oldId:Ljava/lang/String;
    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_81

    new-instance v3, Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v3, v10, v11}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .local v3, newResourceFile:Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_81

    invoke-direct {p0, v7}, Lmiui/app/resourcebrowser/LocalResourceAdapter;->deleteUnusedResource(Lmiui/app/resourcebrowser/resource/Resource;)Z

    .end local v0           #extention:Ljava/lang/String;
    .end local v2           #newId:Ljava/lang/String;
    .end local v3           #newResourceFile:Ljava/io/File;
    .end local v4           #oldId:Ljava/lang/String;
    .end local v5           #oldResFile:Ljava/io/File;
    .end local v6           #oldResName:Ljava/lang/String;
    :cond_7e
    :goto_7e
    add-int/lit8 v1, v1, 0x1

    goto :goto_12

    .restart local v0       #extention:Ljava/lang/String;
    .restart local v2       #newId:Ljava/lang/String;
    .restart local v4       #oldId:Ljava/lang/String;
    .restart local v5       #oldResFile:Ljava/io/File;
    .restart local v6       #oldResName:Ljava/lang/String;
    :cond_81
    invoke-static {}, Lmiui/app/resourcebrowser/service/online/OnlineService;->getInstance()Lmiui/app/resourcebrowser/service/online/OnlineService;

    move-result-object v10

    invoke-virtual {v10, v2}, Lmiui/app/resourcebrowser/service/online/OnlineService;->getDownloadUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Lmiui/app/resourcebrowser/resource/Resource;->updateOnlinePath(Ljava/lang/String;)V

    const/4 v10, 0x1

    invoke-virtual {v7, v10}, Lmiui/app/resourcebrowser/resource/Resource;->updateStatus(I)V

    goto :goto_7e

    .end local v0           #extention:Ljava/lang/String;
    .end local v2           #newId:Ljava/lang/String;
    .end local v4           #oldId:Ljava/lang/String;
    .end local v5           #oldResFile:Ljava/io/File;
    .end local v6           #oldResName:Ljava/lang/String;
    .end local v7           #oldResource:Lmiui/app/resourcebrowser/resource/Resource;
    .end local v8           #updatableResource:Lmiui/app/resourcebrowser/resource/Resource;
    :cond_91
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/LocalResourceAdapter;->notifyDataSetChanged()V

    goto/16 :goto_10
.end method

.method private deleteUnusedResource(Lmiui/app/resourcebrowser/resource/Resource;)Z
    .registers 6
    .parameter "r"

    .prologue
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/LocalResourceAdapter;->getResourceSet()Lmiui/app/resourcebrowser/resource/ResourceSet;

    move-result-object v1

    .local v1, set:Lmiui/app/resourcebrowser/resource/ResourceSet;
    const/4 v0, 0x0

    .local v0, g:I
    :goto_5
    invoke-virtual {v1}, Lmiui/app/resourcebrowser/resource/ResourceSet;->size()I

    move-result v2

    if-ge v0, v2, :cond_28

    invoke-virtual {v1, v0}, Lmiui/app/resourcebrowser/resource/ResourceSet;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/widget/DataGroup;

    invoke-virtual {v2, p1}, Lmiui/widget/DataGroup;->remove(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_25

    new-instance v2, Ljava/io/File;

    invoke-virtual {p1}, Lmiui/app/resourcebrowser/resource/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    const/4 v2, 0x1

    :goto_24
    return v2

    :cond_25
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_28
    const/4 v2, 0x0

    goto :goto_24
.end method

.method private initParams()V
    .registers 3

    .prologue
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lmiui/app/resourcebrowser/ResourceConstants;->VERSION_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mResourceSetCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lmiui/os/ExtraFileUtils;->standardizeFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmiui/app/resourcebrowser/LocalResourceAdapter;->mVersionFolder:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected checkResourceModifyTime()Z
    .registers 2

    .prologue
    const/4 v0, 0x0

    return v0
.end method

.method protected generateVisitors([Ljava/lang/String;)Ljava/util/List;
    .registers 5
    .parameter "folders"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lmiui/widget/AsyncAdapter$AsyncLoadDataVisitor",
            "<",
            "Lmiui/app/resourcebrowser/resource/Resource;",
            ">;>;"
        }
    .end annotation

    .prologue
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .local v1, visitors:Ljava/util/List;,"Ljava/util/List<Lmiui/widget/AsyncAdapter$AsyncLoadDataVisitor<Lmiui/app/resourcebrowser/resource/Resource;>;>;"
    const/4 v0, 0x0

    .local v0, i:I
    :goto_6
    array-length v2, p1

    if-ge v0, v2, :cond_15

    aget-object v2, p1, v0

    invoke-virtual {p0, v2}, Lmiui/app/resourcebrowser/LocalResourceAdapter;->getVisitor(Ljava/lang/String;)Lmiui/widget/AsyncAdapter$AsyncLoadDataVisitor;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_15
    return-object v1
.end method

.method protected getDownloadableFlag(Lmiui/app/resourcebrowser/resource/Resource;)I
    .registers 4
    .parameter "resourceItem"

    .prologue
    invoke-super {p0, p1}, Lmiui/app/resourcebrowser/ResourceAdapter;->getDownloadableFlag(Lmiui/app/resourcebrowser/resource/Resource;)I

    move-result v0

    .local v0, flag:I
    const v1, 0x602003a

    if-ne v0, v1, :cond_a

    const/4 v0, 0x0

    :cond_a
    return v0
.end method

.method protected getLoadDataTask()Ljava/util/List;
    .registers 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lmiui/widget/AsyncAdapter",
            "<",
            "Lmiui/app/resourcebrowser/resource/Resource;",
            ">.Async",
            "LoadDataTask;",
            ">;"
        }
    .end annotation

    .prologue
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .local v2, tasks:Ljava/util/List;,"Ljava/util/List<Lmiui/widget/AsyncAdapter<Lmiui/app/resourcebrowser/resource/Resource;>.AsyncLoadDataTask;>;"
    new-instance v1, Lmiui/app/resourcebrowser/LocalResourceAdapter$AsyncLoadResourceTask;

    invoke-direct {v1, p0}, Lmiui/app/resourcebrowser/LocalResourceAdapter$AsyncLoadResourceTask;-><init>(Lmiui/app/resourcebrowser/LocalResourceAdapter;)V

    .local v1, task:Lmiui/app/resourcebrowser/LocalResourceAdapter$AsyncLoadResourceTask;
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/LocalResourceAdapter;->getRegisterAsyncTaskObserver()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lmiui/os/AsyncTaskObserver;

    invoke-virtual {v1, v4}, Lmiui/app/resourcebrowser/LocalResourceAdapter$AsyncLoadResourceTask;->addObserver(Lmiui/os/AsyncTaskObserver;)V

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/LocalResourceAdapter;->getVisitors()Ljava/util/List;

    move-result-object v3

    .local v3, visitors:Ljava/util/List;,"Ljava/util/List<Lmiui/widget/AsyncAdapter$AsyncLoadDataVisitor<Lmiui/app/resourcebrowser/resource/Resource;>;>;"
    const/4 v0, 0x0

    .local v0, i:I
    :goto_18
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_2a

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lmiui/widget/AsyncAdapter$AsyncLoadDataVisitor;

    invoke-virtual {v1, v4}, Lmiui/app/resourcebrowser/LocalResourceAdapter$AsyncLoadResourceTask;->addVisitor(Lmiui/widget/AsyncAdapter$AsyncLoadDataVisitor;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_18

    :cond_2a
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v2
.end method

.method protected getVisitor(Ljava/lang/String;)Lmiui/widget/AsyncAdapter$AsyncLoadDataVisitor;
    .registers 5
    .parameter "folder"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lmiui/widget/AsyncAdapter$AsyncLoadDataVisitor",
            "<",
            "Lmiui/app/resourcebrowser/resource/Resource;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .local v0, result:Lmiui/app/resourcebrowser/service/local/ResourceFolder;
    iget v1, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mResourceSetCategory:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_10

    new-instance v0, Lmiui/app/resourcebrowser/service/local/ImageResourceFolder;

    .end local v0           #result:Lmiui/app/resourcebrowser/service/local/ResourceFolder;
    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mContext:Landroid/app/Activity;

    iget-object v2, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mMetaData:Landroid/os/Bundle;

    invoke-direct {v0, v1, v2, p1}, Lmiui/app/resourcebrowser/service/local/ImageResourceFolder;-><init>(Landroid/content/Context;Landroid/os/Bundle;Ljava/lang/String;)V

    .restart local v0       #result:Lmiui/app/resourcebrowser/service/local/ResourceFolder;
    :goto_f
    return-object v0

    :cond_10
    iget v1, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mResourceSetCategory:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1f

    new-instance v0, Lmiui/app/resourcebrowser/service/local/AudioResourceFolder;

    .end local v0           #result:Lmiui/app/resourcebrowser/service/local/ResourceFolder;
    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mContext:Landroid/app/Activity;

    iget-object v2, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mMetaData:Landroid/os/Bundle;

    invoke-direct {v0, v1, v2, p1}, Lmiui/app/resourcebrowser/service/local/AudioResourceFolder;-><init>(Landroid/content/Context;Landroid/os/Bundle;Ljava/lang/String;)V

    .restart local v0       #result:Lmiui/app/resourcebrowser/service/local/ResourceFolder;
    goto :goto_f

    :cond_1f
    new-instance v0, Lmiui/app/resourcebrowser/service/local/ZipResourceFolder;

    .end local v0           #result:Lmiui/app/resourcebrowser/service/local/ResourceFolder;
    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mContext:Landroid/app/Activity;

    iget-object v2, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mMetaData:Landroid/os/Bundle;

    invoke-direct {v0, v1, v2, p1}, Lmiui/app/resourcebrowser/service/local/ZipResourceFolder;-><init>(Landroid/content/Context;Landroid/os/Bundle;Ljava/lang/String;)V

    .restart local v0       #result:Lmiui/app/resourcebrowser/service/local/ResourceFolder;
    goto :goto_f
.end method

.method protected getVisitors()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lmiui/widget/AsyncAdapter$AsyncLoadDataVisitor",
            "<",
            "Lmiui/app/resourcebrowser/resource/Resource;",
            ">;>;"
        }
    .end annotation

    .prologue
    iget-object v1, p0, Lmiui/app/resourcebrowser/LocalResourceAdapter;->mVisitors:Ljava/util/List;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lmiui/app/resourcebrowser/LocalResourceAdapter;->mVisitors:Ljava/util/List;

    :goto_6
    return-object v1

    :cond_7
    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mMetaData:Landroid/os/Bundle;

    const-string v2, "miui.app.resourcebrowser.SOURCE_FOLDERS"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .local v0, folders:[Ljava/lang/String;
    invoke-virtual {p0, v0}, Lmiui/app/resourcebrowser/LocalResourceAdapter;->generateVisitors([Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lmiui/app/resourcebrowser/LocalResourceAdapter;->mVisitors:Ljava/util/List;

    iget-object v1, p0, Lmiui/app/resourcebrowser/LocalResourceAdapter;->mVisitors:Ljava/util/List;

    goto :goto_6
.end method

.method protected postLoadData(Ljava/util/List;)V
    .registers 18
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lmiui/app/resourcebrowser/resource/Resource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, result:Ljava/util/List;,"Ljava/util/List<Lmiui/app/resourcebrowser/resource/Resource;>;"
    move-object/from16 v0, p0

    iget-object v13, v0, Lmiui/app/resourcebrowser/ResourceAdapter;->mMetaData:Landroid/os/Bundle;

    const-string v14, "miui.app.resourcebrowser.VERSION_SUPPORTED"

    invoke-virtual {v13, v14}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_99

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .local v9, repeatResource:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lmiui/app/resourcebrowser/resource/Resource;>;"
    new-instance v13, Ljava/util/HashMap;

    invoke-direct {v13}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, p0

    iput-object v13, v0, Lmiui/app/resourcebrowser/LocalResourceAdapter;->mFileHashMap:Ljava/util/Map;

    const/4 v5, 0x0

    .local v5, i:I
    :goto_1b
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v13

    if-ge v5, v13, :cond_8f

    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lmiui/app/resourcebrowser/resource/Resource;

    .local v10, resource:Lmiui/app/resourcebrowser/resource/Resource;
    invoke-virtual {v10}, Lmiui/app/resourcebrowser/resource/Resource;->getFileHash()Ljava/lang/String;

    move-result-object v4

    .local v4, hash:Ljava/lang/String;
    if-eqz v4, :cond_84

    move-object/from16 v0, p0

    iget-object v13, v0, Lmiui/app/resourcebrowser/LocalResourceAdapter;->mFileHashMap:Ljava/util/Map;

    invoke-interface {v13, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/app/resourcebrowser/resource/Resource;

    .local v2, existRes:Lmiui/app/resourcebrowser/resource/Resource;
    if-eqz v2, :cond_87

    const-string v13, "Theme"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "delete repeat resources: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v10}, Lmiui/app/resourcebrowser/resource/Resource;->getTitle()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " vs "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v2}, Lmiui/app/resourcebrowser/resource/Resource;->getTitle()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v10}, Lmiui/app/resourcebrowser/resource/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " vs "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v2}, Lmiui/app/resourcebrowser/resource/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .end local v2           #existRes:Lmiui/app/resourcebrowser/resource/Resource;
    :cond_84
    :goto_84
    add-int/lit8 v5, v5, 0x1

    goto :goto_1b

    .restart local v2       #existRes:Lmiui/app/resourcebrowser/resource/Resource;
    :cond_87
    move-object/from16 v0, p0

    iget-object v13, v0, Lmiui/app/resourcebrowser/LocalResourceAdapter;->mFileHashMap:Ljava/util/Map;

    invoke-interface {v13, v4, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_84

    .end local v2           #existRes:Lmiui/app/resourcebrowser/resource/Resource;
    .end local v4           #hash:Ljava/lang/String;
    .end local v10           #resource:Lmiui/app/resourcebrowser/resource/Resource;
    :cond_8f
    move-object/from16 v0, p0

    iget-object v13, v0, Lmiui/app/resourcebrowser/LocalResourceAdapter;->mFileHashMap:Ljava/util/Map;

    invoke-interface {v13}, Ljava/util/Map;->isEmpty()Z

    move-result v13

    if-eqz v13, :cond_9a

    .end local v5           #i:I
    .end local v9           #repeatResource:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lmiui/app/resourcebrowser/resource/Resource;>;"
    :cond_99
    :goto_99
    return-void

    .restart local v5       #i:I
    .restart local v9       #repeatResource:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lmiui/app/resourcebrowser/resource/Resource;>;"
    :cond_9a
    invoke-virtual {v9}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v13

    if-nez v13, :cond_b9

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, i$:Ljava/util/Iterator;
    :goto_a4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_b6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lmiui/app/resourcebrowser/resource/Resource;

    .local v8, r:Lmiui/app/resourcebrowser/resource/Resource;
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lmiui/app/resourcebrowser/LocalResourceAdapter;->deleteUnusedResource(Lmiui/app/resourcebrowser/resource/Resource;)Z

    goto :goto_a4

    .end local v8           #r:Lmiui/app/resourcebrowser/resource/Resource;
    :cond_b6
    invoke-virtual/range {p0 .. p0}, Lmiui/app/resourcebrowser/LocalResourceAdapter;->notifyDataSetChanged()V

    .end local v6           #i$:Ljava/util/Iterator;
    :cond_b9
    move-object/from16 v0, p0

    iget-object v14, v0, Lmiui/app/resourcebrowser/LocalResourceAdapter;->mService:Lmiui/app/resourcebrowser/service/online/OnlineService;

    move-object/from16 v0, p0

    iget-object v13, v0, Lmiui/app/resourcebrowser/LocalResourceAdapter;->mFileHashMap:Ljava/util/Map;

    invoke-interface {v13}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v13

    const/4 v15, 0x0

    new-array v15, v15, [Ljava/lang/String;

    invoke-interface {v13, v15}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v13

    check-cast v13, [Ljava/lang/String;

    invoke-virtual {v14, v13}, Lmiui/app/resourcebrowser/service/online/OnlineService;->getUpdateUrl([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .local v12, url:Ljava/lang/String;
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v14, v0, Lmiui/app/resourcebrowser/LocalResourceAdapter;->mVersionFolder:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "version"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .local v7, path:Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .local v3, file:Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v13

    if-eqz v13, :cond_ff

    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lmiui/app/resourcebrowser/LocalResourceAdapter;->checkVersionStatus(Ljava/lang/String;)V

    invoke-static {v3}, Lmiui/app/resourcebrowser/util/ResourceHelper;->isCacheValid(Ljava/io/File;)Z

    move-result v13

    if-nez v13, :cond_99

    :cond_ff
    sget-boolean v13, Lmiui/app/resourcebrowser/util/ResourceDebug;->DEBUG:Z

    if-eqz v13, :cond_11b

    const-string v13, "Theme"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Local list page check update url: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_11b
    new-instance v11, Lmiui/app/resourcebrowser/LocalResourceAdapter$DownloadVersionTask;

    move-object/from16 v0, p0

    invoke-direct {v11, v0}, Lmiui/app/resourcebrowser/LocalResourceAdapter$DownloadVersionTask;-><init>(Lmiui/app/resourcebrowser/LocalResourceAdapter;)V

    .local v11, task:Lmiui/app/resourcebrowser/LocalResourceAdapter$DownloadVersionTask;
    const-string v13, "version"

    invoke-virtual {v11, v13}, Lmiui/app/resourcebrowser/LocalResourceAdapter$DownloadVersionTask;->setId(Ljava/lang/String;)V

    new-instance v1, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;

    invoke-direct {v1}, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;-><init>()V

    .local v1, entry:Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;
    invoke-virtual {v1, v12}, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->setUrl(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;->setPath(Ljava/lang/String;)V

    const/4 v13, 0x1

    new-array v13, v13, [Lmiui/app/resourcebrowser/util/DownloadFileTask$DownloadFileEntry;

    const/4 v14, 0x0

    aput-object v1, v13, v14

    invoke-virtual {v11, v13}, Lmiui/app/resourcebrowser/LocalResourceAdapter$DownloadVersionTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_99
.end method

.method public setKeyword(Ljava/lang/String;)V
    .registers 2
    .parameter "keyword"

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/LocalResourceAdapter;->mKeyword:Ljava/lang/String;

    return-void
.end method
