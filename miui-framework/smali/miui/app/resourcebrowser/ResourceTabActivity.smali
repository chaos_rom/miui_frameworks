.class public Lmiui/app/resourcebrowser/ResourceTabActivity;
.super Lmiui/app/resourcebrowser/BaseTabActivity;
.source "ResourceTabActivity.java"

# interfaces
.implements Lmiui/app/resourcebrowser/IntentConstants;


# instance fields
.field protected mAction:Ljava/lang/String;

.field protected mMetaData:Landroid/os/Bundle;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    invoke-direct {p0}, Lmiui/app/resourcebrowser/BaseTabActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected buildDefaultMetaData(Landroid/os/Bundle;Ljava/lang/String;)Landroid/os/Bundle;
    .registers 4
    .parameter "metaData"
    .parameter "action"

    .prologue
    invoke-static {p1, p2, p0}, Lmiui/app/resourcebrowser/util/ResourceHelper;->buildDefaultMetaData(Landroid/os/Bundle;Ljava/lang/String;Landroid/content/Context;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method protected buildLocalResourceListMetaData(Landroid/os/Bundle;Ljava/lang/String;)Landroid/os/Bundle;
    .registers 4
    .parameter "metaData"
    .parameter "action"

    .prologue
    invoke-virtual {p1}, Landroid/os/Bundle;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    return-object v0
.end method

.method protected buildOnlineResourceListMetaData(Landroid/os/Bundle;Ljava/lang/String;)Landroid/os/Bundle;
    .registers 4
    .parameter "metaData"
    .parameter "action"

    .prologue
    invoke-virtual {p1}, Landroid/os/Bundle;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    return-object v0
.end method

.method protected getActionBarTabs()Ljava/util/ArrayList;
    .registers 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/ActionBar$Tab;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .local v2, ret:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/ActionBar$Tab;>;"
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceTabActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .local v0, bar:Landroid/app/ActionBar;
    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceTabActivity;->mMetaData:Landroid/os/Bundle;

    const-string v4, "miui.app.resourcebrowser.RESOURCE_SET_NAME"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .local v1, resourceSetName:Ljava/lang/String;
    invoke-virtual {v0}, Landroid/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v3

    const v4, 0x60c0014

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v1, v5, v6

    invoke-virtual {p0, v4, v5}, Lmiui/app/resourcebrowser/ResourceTabActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/ActionBar$Tab;->setText(Ljava/lang/CharSequence;)Landroid/app/ActionBar$Tab;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Landroid/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v3

    const v4, 0x60c0015

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v1, v5, v6

    invoke-virtual {p0, v4, v5}, Lmiui/app/resourcebrowser/ResourceTabActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/ActionBar$Tab;->setText(Ljava/lang/CharSequence;)Landroid/app/ActionBar$Tab;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v2
.end method

.method protected getLocalResourceListActivity()Landroid/util/Pair;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    new-instance v0, Landroid/util/Pair;

    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceTabActivity;->mMetaData:Landroid/os/Bundle;

    const-string v2, "miui.app.resourcebrowser.LOCAL_LIST_ACTIVITY_PACKAGE"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lmiui/app/resourcebrowser/ResourceTabActivity;->mMetaData:Landroid/os/Bundle;

    const-string v3, "miui.app.resourcebrowser.LOCAL_LIST_ACTIVITY_CLASS"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method protected getOnlineResourceListActivity()Landroid/util/Pair;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    new-instance v0, Landroid/util/Pair;

    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceTabActivity;->mMetaData:Landroid/os/Bundle;

    const-string v2, "miui.app.resourcebrowser.ONLINE_LIST_ACTIVITY_PACKAGE"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lmiui/app/resourcebrowser/ResourceTabActivity;->mMetaData:Landroid/os/Bundle;

    const-string v3, "miui.app.resourcebrowser.ONLINE_LIST_ACTIVITY_CLASS"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method protected initTabFragment(I)Lmiui/app/resourcebrowser/BaseFragment;
    .registers 3
    .parameter "tabPosition"

    .prologue
    if-nez p1, :cond_8

    new-instance v0, Lmiui/app/resourcebrowser/LocalResourceListFragment;

    invoke-direct {v0}, Lmiui/app/resourcebrowser/LocalResourceListFragment;-><init>()V

    :goto_7
    return-object v0

    :cond_8
    const/4 v0, 0x1

    if-ne p1, v0, :cond_11

    new-instance v0, Lmiui/app/resourcebrowser/OnlineResourceListFragment;

    invoke-direct {v0}, Lmiui/app/resourcebrowser/OnlineResourceListFragment;-><init>()V

    goto :goto_7

    :cond_11
    const/4 v0, 0x0

    goto :goto_7
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 9
    .parameter "savedInstanceState"

    .prologue
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceTabActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .local v0, intent:Landroid/content/Intent;
    const-string v3, "META_DATA"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    .local v2, metaData:Landroid/os/Bundle;
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .local v1, intentExtras:Landroid/os/Bundle;
    if-nez v2, :cond_4d

    if-eqz v1, :cond_4d

    move-object v2, v1

    :cond_13
    :goto_13
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lmiui/app/resourcebrowser/ResourceTabActivity;->mAction:Ljava/lang/String;

    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceTabActivity;->mAction:Ljava/lang/String;

    invoke-virtual {p0, v2, v3}, Lmiui/app/resourcebrowser/ResourceTabActivity;->buildDefaultMetaData(Landroid/os/Bundle;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    iput-object v3, p0, Lmiui/app/resourcebrowser/ResourceTabActivity;->mMetaData:Landroid/os/Bundle;

    const-string v3, "META_DATA_FOR_LOCAL"

    iget-object v4, p0, Lmiui/app/resourcebrowser/ResourceTabActivity;->mAction:Ljava/lang/String;

    invoke-virtual {p0, v2, v4}, Lmiui/app/resourcebrowser/ResourceTabActivity;->buildLocalResourceListMetaData(Landroid/os/Bundle;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    const-string v3, "META_DATA_FOR_ONLINE"

    iget-object v4, p0, Lmiui/app/resourcebrowser/ResourceTabActivity;->mAction:Ljava/lang/String;

    invoke-virtual {p0, v2, v4}, Lmiui/app/resourcebrowser/ResourceTabActivity;->buildOnlineResourceListMetaData(Landroid/os/Bundle;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    invoke-super {p0, p1}, Lmiui/app/resourcebrowser/BaseTabActivity;->onCreate(Landroid/os/Bundle;)V

    new-instance v3, Lmiui/app/resourcebrowser/ResourceTabActivity$1;

    invoke-direct {v3, p0}, Lmiui/app/resourcebrowser/ResourceTabActivity$1;-><init>(Lmiui/app/resourcebrowser/ResourceTabActivity;)V

    const/4 v4, 0x0

    const-wide/16 v5, 0x7d0

    invoke-virtual {v3, v4, v5, v6}, Lmiui/app/resourcebrowser/ResourceTabActivity$1;->sendEmptyMessageDelayed(IJ)Z

    invoke-static {}, Lmiui/app/resourcebrowser/util/CleanCacheTask;->getInstance()Lmiui/app/resourcebrowser/util/CleanCacheTask;

    move-result-object v3

    invoke-virtual {v3}, Lmiui/app/resourcebrowser/util/CleanCacheTask;->run()V

    return-void

    :cond_4d
    if-nez v2, :cond_55

    new-instance v2, Landroid/os/Bundle;

    .end local v2           #metaData:Landroid/os/Bundle;
    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .restart local v2       #metaData:Landroid/os/Bundle;
    goto :goto_13

    :cond_55
    if-eqz v1, :cond_13

    const-string v3, "META_DATA"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    goto :goto_13
.end method
