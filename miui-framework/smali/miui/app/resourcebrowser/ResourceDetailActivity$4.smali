.class Lmiui/app/resourcebrowser/ResourceDetailActivity$4;
.super Ljava/lang/Object;
.source "ResourceDetailActivity.java"

# interfaces
.implements Lmiui/app/resourcebrowser/util/ImageCacheDecoder$ImageDecodingListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiui/app/resourcebrowser/ResourceDetailActivity;->initImageDecoder()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;


# direct methods
.method constructor <init>(Lmiui/app/resourcebrowser/ResourceDetailActivity;)V
    .registers 2
    .parameter

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$4;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleDecodingResult(ZLjava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "result"
    .parameter "localPath"
    .parameter "onlinePath"

    .prologue
    if-eqz p1, :cond_17

    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$4;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    #calls: Lmiui/app/resourcebrowser/ResourceDetailActivity;->getScreenViewIndexForPreviewImage(Ljava/lang/String;)I
    invoke-static {v1, p2}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->access$200(Lmiui/app/resourcebrowser/ResourceDetailActivity;Ljava/lang/String;)I

    move-result v0

    .local v0, screenIndex:I
    if-ltz v0, :cond_17

    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$4;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    #calls: Lmiui/app/resourcebrowser/ResourceDetailActivity;->isVisiableScreen(I)Z
    invoke-static {v1, v0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->access$300(Lmiui/app/resourcebrowser/ResourceDetailActivity;I)Z

    move-result v1

    if-eqz v1, :cond_17

    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$4;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    #calls: Lmiui/app/resourcebrowser/ResourceDetailActivity;->decodeImageForScreenView(I)V
    invoke-static {v1, v0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->access$500(Lmiui/app/resourcebrowser/ResourceDetailActivity;I)V

    .end local v0           #screenIndex:I
    :cond_17
    return-void
.end method

.method public handleDownloadResult(ZLjava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter "result"
    .parameter "localPath"
    .parameter "onlinePath"

    .prologue
    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$4;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    #calls: Lmiui/app/resourcebrowser/ResourceDetailActivity;->getScreenViewIndexForPreviewImage(Ljava/lang/String;)I
    invoke-static {v1, p2}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->access$200(Lmiui/app/resourcebrowser/ResourceDetailActivity;Ljava/lang/String;)I

    move-result v0

    .local v0, screenIndex:I
    if-ltz v0, :cond_1b

    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$4;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    #calls: Lmiui/app/resourcebrowser/ResourceDetailActivity;->isVisiableScreen(I)Z
    invoke-static {v1, v0}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->access$300(Lmiui/app/resourcebrowser/ResourceDetailActivity;I)Z

    move-result v1

    if-eqz v1, :cond_1b

    if-eqz p1, :cond_1c

    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$4;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    #getter for: Lmiui/app/resourcebrowser/ResourceDetailActivity;->mImageCacheDecoder:Lmiui/app/resourcebrowser/util/ImageCacheDecoder;
    invoke-static {v1}, Lmiui/app/resourcebrowser/ResourceDetailActivity;->access$400(Lmiui/app/resourcebrowser/ResourceDetailActivity;)Lmiui/app/resourcebrowser/util/ImageCacheDecoder;

    move-result-object v1

    invoke-virtual {v1, p2, p3, v0}, Lmiui/app/resourcebrowser/util/ImageCacheDecoder;->decodeImageAsync(Ljava/lang/String;Ljava/lang/String;I)V

    :cond_1b
    :goto_1b
    return-void

    :cond_1c
    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceDetailActivity$4;->this$0:Lmiui/app/resourcebrowser/ResourceDetailActivity;

    const v2, 0x60c0024

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_1b
.end method
