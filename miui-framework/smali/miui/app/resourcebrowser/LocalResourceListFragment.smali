.class public Lmiui/app/resourcebrowser/LocalResourceListFragment;
.super Lmiui/app/resourcebrowser/ResourceListFragment;
.source "LocalResourceListFragment.java"

# interfaces
.implements Lmiui/os/AsyncTaskObserver;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmiui/app/resourcebrowser/ResourceListFragment;",
        "Lmiui/os/AsyncTaskObserver",
        "<",
        "Ljava/lang/Void;",
        "Lmiui/app/resourcebrowser/resource/Resource;",
        "Ljava/util/List",
        "<",
        "Lmiui/app/resourcebrowser/resource/Resource;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    invoke-direct {p0}, Lmiui/app/resourcebrowser/ResourceListFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected addMetaData(Landroid/os/Bundle;)V
    .registers 4
    .parameter "metaData"

    .prologue
    invoke-super {p0, p1}, Lmiui/app/resourcebrowser/ResourceListFragment;->addMetaData(Landroid/os/Bundle;)V

    const-string v0, "miui.app.resourcebrowser.RESOURCE_SET_SUBPACKAGE"

    const-string v1, ".local"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected getAdapter()Lmiui/app/resourcebrowser/ResourceAdapter;
    .registers 3

    .prologue
    new-instance v0, Lmiui/app/resourcebrowser/LocalResourceAdapter;

    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mMetaData:Landroid/os/Bundle;

    invoke-direct {v0, p0, v1}, Lmiui/app/resourcebrowser/LocalResourceAdapter;-><init>(Lmiui/app/resourcebrowser/BaseFragment;Landroid/os/Bundle;)V

    return-object v0
.end method

.method protected getContentView()I
    .registers 2

    .prologue
    const v0, 0x6030018

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .registers 8
    .parameter "savedInstanceState"

    .prologue
    const/4 v5, 0x1

    invoke-super {p0, p1}, Lmiui/app/resourcebrowser/ResourceListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    sget-object v0, Lmiui/app/resourcebrowser/ResourceConstants;->CACHE_PATH:Ljava/lang/String;

    invoke-static {v0}, Lmiui/os/ExtraFileUtils;->addNoMedia(Ljava/lang/String;)V

    sget-object v0, Lmiui/app/resourcebrowser/ResourceConstants;->MIUI_PATH:Ljava/lang/String;

    sget-object v1, Lmiui/app/resourcebrowser/ResourceConstants;->MIUI_EXTERNAL_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_20

    const/4 v0, 0x0

    const-string v1, "rm -r %s_data_sdcard_*"

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    sget-object v4, Lmiui/app/resourcebrowser/ResourceConstants;->PREVIEW_PATH:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v5, v0, v1, v2}, Lmiui/util/CommandLineUtils;->run(ZLjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)Z

    :cond_20
    return-void
.end method

.method public onResume()V
    .registers 2

    .prologue
    invoke-super {p0}, Lmiui/app/resourcebrowser/ResourceListFragment;->onResume()V

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceListFragment;->mAdapter:Lmiui/app/resourcebrowser/ResourceAdapter;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/ResourceAdapter;->loadData()V

    return-void
.end method
