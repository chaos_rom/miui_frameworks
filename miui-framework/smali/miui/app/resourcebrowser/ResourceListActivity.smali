.class public abstract Lmiui/app/resourcebrowser/ResourceListActivity;
.super Landroid/app/Activity;
.source "ResourceListActivity.java"

# interfaces
.implements Lmiui/os/AsyncTaskObserver;
.implements Lmiui/app/SDCardMonitor$SDCardStatusListener;
.implements Lmiui/app/resourcebrowser/IntentConstants;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/app/Activity;",
        "Lmiui/os/AsyncTaskObserver",
        "<",
        "Ljava/lang/Void;",
        "Lmiui/app/resourcebrowser/resource/Resource;",
        "Ljava/util/List",
        "<",
        "Lmiui/app/resourcebrowser/resource/Resource;",
        ">;>;",
        "Lmiui/app/SDCardMonitor$SDCardStatusListener;",
        "Lmiui/app/resourcebrowser/IntentConstants;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract isPicker()Z
.end method

.method public abstract startDetailActivityForResource(Landroid/util/Pair;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation
.end method
