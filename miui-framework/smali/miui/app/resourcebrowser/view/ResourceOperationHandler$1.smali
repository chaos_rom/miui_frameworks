.class Lmiui/app/resourcebrowser/view/ResourceOperationHandler$1;
.super Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;
.source "ResourceOperationHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->getResourceDownloadHandler(Landroid/content/Context;)Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/app/resourcebrowser/view/ResourceOperationHandler;


# direct methods
.method constructor <init>(Lmiui/app/resourcebrowser/view/ResourceOperationHandler;Landroid/content/Context;)V
    .registers 3
    .parameter
    .parameter "x0"

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler$1;->this$0:Lmiui/app/resourcebrowser/view/ResourceOperationHandler;

    invoke-direct {p0, p2}, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public handleDownloadFailed(Ljava/lang/String;)V
    .registers 3
    .parameter "downloadFilePath"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler$1;->this$0:Lmiui/app/resourcebrowser/view/ResourceOperationHandler;

    invoke-virtual {v0, p1}, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->handleResourceDownloadFailedEvent(Ljava/lang/String;)V

    return-void
.end method

.method public handleDownloadProgress(Ljava/lang/String;II)V
    .registers 5
    .parameter "downloadFilePath"
    .parameter "downloadBytes"
    .parameter "totalBytes"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler$1;->this$0:Lmiui/app/resourcebrowser/view/ResourceOperationHandler;

    invoke-virtual {v0, p1, p2, p3}, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->handleResourceDownloadProgressEvent(Ljava/lang/String;II)V

    return-void
.end method

.method public handleDownloadSuccessed(Ljava/lang/String;)V
    .registers 3
    .parameter "downloadFilePath"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler$1;->this$0:Lmiui/app/resourcebrowser/view/ResourceOperationHandler;

    invoke-virtual {v0, p1}, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->handleResourceDownloadSuccessedEvent(Ljava/lang/String;)V

    return-void
.end method
