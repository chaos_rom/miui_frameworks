.class public Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;
.super Lmiui/app/resourcebrowser/view/BatchResourceHandler;
.source "AudioBatchResourceHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;
    }
.end annotation


# instance fields
.field protected mMusicPlayer:Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;

.field protected mMusicUI:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;


# direct methods
.method public constructor <init>(Lmiui/app/resourcebrowser/ResourceListActivity;Lmiui/app/resourcebrowser/ResourceAdapter;)V
    .registers 3
    .parameter "activity"
    .parameter "adapter"

    .prologue
    invoke-direct {p0, p1, p2}, Lmiui/app/resourcebrowser/view/BatchResourceHandler;-><init>(Lmiui/app/resourcebrowser/ResourceListActivity;Lmiui/app/resourcebrowser/ResourceAdapter;)V

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;->init()V

    return-void
.end method

.method public constructor <init>(Lmiui/app/resourcebrowser/ResourceListFragment;Lmiui/app/resourcebrowser/ResourceAdapter;)V
    .registers 3
    .parameter "fragment"
    .parameter "adapter"

    .prologue
    invoke-direct {p0, p1, p2}, Lmiui/app/resourcebrowser/view/BatchResourceHandler;-><init>(Lmiui/app/resourcebrowser/ResourceListFragment;Lmiui/app/resourcebrowser/ResourceAdapter;)V

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;->init()V

    return-void
.end method

.method private clickMusicView(Landroid/view/View;)V
    .registers 6
    .parameter "view"

    .prologue
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    .local v1, position:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    invoke-virtual {p0, v1}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;->getResource(Landroid/util/Pair;)Lmiui/app/resourcebrowser/resource/Resource;

    move-result-object v2

    .local v2, r:Lmiui/app/resourcebrowser/resource/Resource;
    if-nez v2, :cond_d

    :goto_c
    return-void

    :cond_d
    iget-object v3, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;->mMusicPlayer:Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;

    invoke-virtual {v3, v2}, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->canPlay(Lmiui/app/resourcebrowser/resource/Resource;)Z

    move-result v0

    .local v0, canPlay:Z
    iget-object v3, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;->mMusicPlayer:Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;

    invoke-virtual {v3}, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->stopMusic()V

    if-eqz v0, :cond_1f

    iget-object v3, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;->mMusicPlayer:Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;

    invoke-virtual {v3, v2}, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->playMusic(Lmiui/app/resourcebrowser/resource/Resource;)V

    :cond_1f
    iget-object v3, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;->mMusicUI:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;

    invoke-virtual {v3, p1}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->clickMusicUI(Landroid/view/View;)V

    goto :goto_c
.end method


# virtual methods
.method protected enterEditMode(Landroid/view/View;Landroid/util/Pair;)V
    .registers 4
    .parameter "v"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, position:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    invoke-super {p0, p1, p2}, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->enterEditMode(Landroid/view/View;Landroid/util/Pair;)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;->mMusicPlayer:Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->stopMusic()V

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;->mMusicUI:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->reset()V

    return-void
.end method

.method protected getFormatPlayingRingtoneName(Ljava/lang/String;II)Ljava/lang/String;
    .registers 5
    .parameter "ringtonePath"
    .parameter "current"
    .parameter "total"

    .prologue
    invoke-static {p1, p2, p3}, Lmiui/app/resourcebrowser/util/ResourceHelper;->getDefaultFormatPlayingRingtoneName(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected handleApplyEvent(Lmiui/app/resourcebrowser/resource/Resource;)V
    .registers 2
    .parameter "r"

    .prologue
    return-void
.end method

.method protected handleCancelDownloadEvent(Lmiui/app/resourcebrowser/resource/Resource;)V
    .registers 2
    .parameter "r"

    .prologue
    invoke-virtual {p0, p1}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;->doCancelDownload(Lmiui/app/resourcebrowser/resource/Resource;)V

    return-void
.end method

.method protected handleDownloadEvent(Lmiui/app/resourcebrowser/resource/Resource;)V
    .registers 2
    .parameter "r"

    .prologue
    invoke-virtual {p0, p1}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;->doDownloadResource(Lmiui/app/resourcebrowser/resource/Resource;)V

    return-void
.end method

.method protected handlePickEvent(Lmiui/app/resourcebrowser/resource/Resource;)V
    .registers 5
    .parameter "r"

    .prologue
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .local v0, resultIntent:Landroid/content/Intent;
    const-string v1, "miui.app.resourcebrowser.PICKED_RESOURCE"

    invoke-virtual {p1}, Lmiui/app/resourcebrowser/resource/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.extra.ringtone.PICKED_URI"

    invoke-virtual {p1}, Lmiui/app/resourcebrowser/resource/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lmiui/app/resourcebrowser/util/ResourceHelper;->getUriByPath(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v1, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mAdapter:Lmiui/app/resourcebrowser/ResourceAdapter;

    iget-object v1, v1, Lmiui/app/resourcebrowser/ResourceAdapter;->mContext:Landroid/app/Activity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    iget-object v1, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mAdapter:Lmiui/app/resourcebrowser/ResourceAdapter;

    iget-object v1, v1, Lmiui/app/resourcebrowser/ResourceAdapter;->mContext:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method protected init()V
    .registers 4

    .prologue
    new-instance v0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;

    iget-object v1, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mActivity:Landroid/app/Activity;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;-><init>(Landroid/app/Activity;Z)V

    iput-object v0, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;->mMusicPlayer:Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;

    new-instance v0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;

    invoke-direct {v0, p0}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;-><init>(Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;)V

    iput-object v0, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;->mMusicUI:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;->mMusicPlayer:Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;

    iget-object v1, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;->mMusicUI:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;

    invoke-virtual {v1}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->getMusicPlayListener()Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$PlayProgressListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->regeistePlayProgressListener(Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$PlayProgressListener;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mListenDownloadProgress:Z

    return-void
.end method

.method public initViewState(Landroid/view/View;Landroid/util/Pair;)V
    .registers 4
    .parameter "v"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, position:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    invoke-super {p0, p1, p2}, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->initViewState(Landroid/view/View;Landroid/util/Pair;)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;->mMusicUI:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;

    invoke-virtual {v0, p1}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->initMusicUI(Landroid/view/View;)V

    return-void
.end method

.method protected onClick_Impl(Landroid/view/View;)V
    .registers 4
    .parameter "v"

    .prologue
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;->isEditMode()Z

    move-result v0

    if-nez v0, :cond_11

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mAdapter:Lmiui/app/resourcebrowser/ResourceAdapter;

    iget v0, v0, Lmiui/app/resourcebrowser/ResourceAdapter;->mDisplayType:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_11

    invoke-direct {p0, p1}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;->clickMusicView(Landroid/view/View;)V

    :goto_10
    return-void

    :cond_11
    invoke-super {p0, p1}, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->onClick_Impl(Landroid/view/View;)V

    goto :goto_10
.end method

.method public quitEditMode()V
    .registers 2

    .prologue
    invoke-super {p0}, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->quitEditMode()V

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;->mMusicPlayer:Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->stopMusic()V

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;->mMusicUI:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->reset()V

    return-void
.end method

.method public viewStateChanged(Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;)V
    .registers 3
    .parameter "state"

    .prologue
    invoke-super {p0, p1}, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->viewStateChanged(Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;->mMusicPlayer:Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->stopMusic()V

    sget-object v0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;->PAUSE:Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;

    if-eq p1, v0, :cond_11

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;->mMusicUI:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->reset()V

    :cond_11
    return-void
.end method
