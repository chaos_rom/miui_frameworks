.class public Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;
.super Ljava/lang/Object;
.source "AudioBatchResourceHandler.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ResourceMusicUI"
.end annotation


# static fields
.field private static final ANIMATION_LAST_TIME:I = 0xfa

.field private static final BTN_STATE_APPLY:I = 0x1

.field private static final BTN_STATE_CANCEL_DOWNLOAD:I = 0x4

.field private static final BTN_STATE_DOWNLOAD:I = 0x3

.field private static final BTN_STATE_NONE:I = 0x0

.field private static final BTN_STATE_PICK:I = 0x2

.field private static final PROGRESS_MAX:I = 0x64


# instance fields
.field private mGoneViewWhenClick:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field mLastPlayProgress:I

.field private mLastSelectPosition:Landroid/util/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectPosition:Landroid/util/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;


# direct methods
.method public constructor <init>(Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;)V
    .registers 3
    .parameter

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->mGoneViewWhenClick:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$000(Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;)Ljava/util/ArrayList;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->mGoneViewWhenClick:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$100(Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;Landroid/widget/ProgressBar;ZI)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    .prologue
    invoke-direct {p0, p1, p2, p3}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->setProgressBarState(Landroid/widget/ProgressBar;ZI)V

    return-void
.end method

.method static synthetic access$200(Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;)Landroid/util/Pair;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->mSelectPosition:Landroid/util/Pair;

    return-object v0
.end method

.method static synthetic access$300(Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;)Landroid/util/Pair;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->mLastSelectPosition:Landroid/util/Pair;

    return-object v0
.end method

.method static synthetic access$400(Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;Landroid/view/View;Z)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    invoke-direct {p0, p1, p2}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->startProgressBarAnimation(Landroid/view/View;Z)V

    return-void
.end method

.method private clearGoneViews(Landroid/view/View;)V
    .registers 6
    .parameter "remainState"

    .prologue
    iget-object v2, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->mGoneViewWhenClick:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :cond_6
    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_21

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .local v0, gv:Landroid/view/View;
    if-eq v0, p1, :cond_6

    iget-object v2, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;

    iget-object v3, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->mSelectPosition:Landroid/util/Pair;

    invoke-virtual {v2, v3}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;->getResource(Landroid/util/Pair;)Lmiui/app/resourcebrowser/resource/Resource;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {p0, v0, v2, v3}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->setOperatingViewsState(Landroid/view/View;Lmiui/app/resourcebrowser/resource/Resource;Z)V

    goto :goto_6

    .end local v0           #gv:Landroid/view/View;
    :cond_21
    iget-object v2, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->mGoneViewWhenClick:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method private isPicker()Z
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;

    iget-object v0, v0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mFragment:Lmiui/app/resourcebrowser/ResourceListFragment;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;

    iget-object v0, v0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mFragment:Lmiui/app/resourcebrowser/ResourceListFragment;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/ResourceListFragment;->isPicker()Z

    move-result v0

    :goto_e
    return v0

    :cond_f
    iget-object v0, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;

    iget-object v0, v0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mActivity:Landroid/app/Activity;

    check-cast v0, Lmiui/app/resourcebrowser/ResourceListActivity;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/ResourceListActivity;->isPicker()Z

    move-result v0

    goto :goto_e
.end method

.method private realSetOperateButtonState(Landroid/view/View;Ljava/lang/String;I)V
    .registers 8
    .parameter "v"
    .parameter "text"
    .parameter "btnState"

    .prologue
    const v2, 0x60b006b

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .local v0, opBtn:Landroid/widget/Button;
    if-nez p3, :cond_11

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    :goto_10
    return-void

    :cond_11
    invoke-virtual {v0, p2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;

    iget-object v2, v2, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;->mMusicUI:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    .local v1, position:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    new-instance v2, Landroid/util/Pair;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v2, v1, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    goto :goto_10
.end method

.method private refreshPlayProgressBarState(Landroid/view/View;Lmiui/app/resourcebrowser/resource/Resource;Z)V
    .registers 7
    .parameter "v"
    .parameter "r"
    .parameter "visiable"

    .prologue
    const v1, 0x60b005e

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    move-object v0, v1

    check-cast v0, Landroid/widget/ProgressBar;

    .local v0, p:Landroid/widget/ProgressBar;
    if-eqz p3, :cond_22

    iget-object v1, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;

    iget-object v1, v1, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;->mMusicPlayer:Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;

    invoke-virtual {v1}, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_22

    invoke-virtual {p2}, Lmiui/app/resourcebrowser/resource/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_28

    :cond_22
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :goto_27
    return-void

    :cond_28
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    const/4 v1, 0x1

    const/4 v2, -0x1

    invoke-direct {p0, v0, v1, v2}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->setProgressBarState(Landroid/widget/ProgressBar;ZI)V

    goto :goto_27
.end method

.method private setApplyOrPickState(Landroid/view/View;Z)V
    .registers 6
    .parameter "v"
    .parameter "pick"

    .prologue
    if-eqz p2, :cond_12

    const v1, 0x60c001b

    .local v1, textId:I
    const/4 v0, 0x2

    .local v0, btnState:I
    :goto_6
    iget-object v2, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;

    iget-object v2, v2, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, p1, v2, v0}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->realSetOperateButtonState(Landroid/view/View;Ljava/lang/String;I)V

    return-void

    .end local v0           #btnState:I
    .end local v1           #textId:I
    :cond_12
    const v1, 0x60c0019

    .restart local v1       #textId:I
    const/4 v0, 0x1

    .restart local v0       #btnState:I
    goto :goto_6
.end method

.method private setDownloadOrCancelState(Landroid/view/View;Lmiui/app/resourcebrowser/resource/Resource;Z)V
    .registers 10
    .parameter "v"
    .parameter "r"
    .parameter "downloading"

    .prologue
    const v5, 0x60c0017

    if-eqz p3, :cond_3b

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;

    iget-object v3, v3, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mActivity:Landroid/app/Activity;

    const/high16 v4, 0x104

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;

    iget-object v3, v3, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .local v1, text:Ljava/lang/String;
    iget-object v3, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;

    const v2, 0x60b004d

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    invoke-virtual {v3, v2, p2}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;->setDownloadProgress(Landroid/widget/ProgressBar;Lmiui/app/resourcebrowser/resource/Resource;)V

    const/4 v0, 0x4

    .local v0, btnState:I
    :goto_37
    invoke-direct {p0, p1, v1, v0}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->realSetOperateButtonState(Landroid/view/View;Ljava/lang/String;I)V

    return-void

    .end local v0           #btnState:I
    .end local v1           #text:Ljava/lang/String;
    :cond_3b
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;

    iget-object v3, v3, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lmiui/app/resourcebrowser/resource/Resource;->getFileSize()J

    move-result-wide v3

    invoke-static {v3, v4}, Lmiui/app/resourcebrowser/util/ResourceHelper;->getFormattedSize(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .restart local v1       #text:Ljava/lang/String;
    const/4 v0, 0x3

    .restart local v0       #btnState:I
    goto :goto_37
.end method

.method private setOperatingViewsState(Landroid/view/View;Lmiui/app/resourcebrowser/resource/Resource;Z)V
    .registers 8
    .parameter "v"
    .parameter "r"
    .parameter "visiable"

    .prologue
    const v3, 0x60b004c

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-nez p3, :cond_f

    iget-object v2, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;

    invoke-virtual {v2}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;->isEditMode()Z

    move-result v2

    if-eqz v2, :cond_30

    :cond_f
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    :goto_17
    invoke-direct {p0, p1, p2, p3}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->refreshPlayProgressBarState(Landroid/view/View;Lmiui/app/resourcebrowser/resource/Resource;Z)V

    if-nez p3, :cond_3d

    iget-object v2, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;

    iget-boolean v2, v2, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mLocalResourcePage:Z

    if-nez v2, :cond_38

    if-eqz p2, :cond_38

    iget-object v2, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;

    invoke-virtual {v2, p2}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;->isDownloading(Lmiui/app/resourcebrowser/resource/Resource;)Z

    move-result v2

    if-eqz v2, :cond_38

    invoke-direct {p0, p1, p2, v1}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->setDownloadOrCancelState(Landroid/view/View;Lmiui/app/resourcebrowser/resource/Resource;Z)V

    :goto_2f
    return-void

    :cond_30
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_17

    :cond_38
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1, v0}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->realSetOperateButtonState(Landroid/view/View;Ljava/lang/String;I)V

    goto :goto_2f

    :cond_3d
    iget-object v2, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;

    iget-boolean v2, v2, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mLocalResourcePage:Z

    if-nez v2, :cond_55

    invoke-virtual {p2}, Lmiui/app/resourcebrowser/resource/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lmiui/app/resourcebrowser/util/ResourceHelper;->isSystemResource(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_55

    iget-object v2, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;

    invoke-virtual {v2, p2}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;->canDeletableResource(Lmiui/app/resourcebrowser/resource/Resource;)Z

    move-result v2

    if-eqz v2, :cond_56

    :cond_55
    move v0, v1

    .local v0, isLocalResource:Z
    :cond_56
    if-eqz v0, :cond_60

    invoke-direct {p0}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->isPicker()Z

    move-result v1

    invoke-direct {p0, p1, v1}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->setApplyOrPickState(Landroid/view/View;Z)V

    goto :goto_2f

    :cond_60
    iget-object v1, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;

    invoke-virtual {v1, p2}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;->isDownloading(Lmiui/app/resourcebrowser/resource/Resource;)Z

    move-result v1

    invoke-direct {p0, p1, p2, v1}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->setDownloadOrCancelState(Landroid/view/View;Lmiui/app/resourcebrowser/resource/Resource;Z)V

    goto :goto_2f
.end method

.method private setProgressBarState(Landroid/widget/ProgressBar;ZI)V
    .registers 5
    .parameter "p"
    .parameter "indeterminate"
    .parameter "progress"

    .prologue
    invoke-virtual {p1}, Landroid/widget/ProgressBar;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_7

    .end local p3
    :goto_6
    return-void

    .restart local p3
    :cond_7
    invoke-virtual {p1, p2}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    const/16 v0, 0x64

    invoke-virtual {p1, v0}, Landroid/widget/ProgressBar;->setMax(I)V

    if-ltz p3, :cond_15

    .end local p3
    :goto_11
    invoke-virtual {p1, p3}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto :goto_6

    .restart local p3
    :cond_15
    iget p3, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->mLastPlayProgress:I

    goto :goto_11
.end method

.method private startProgressBarAnimation(Landroid/view/View;Z)V
    .registers 15
    .parameter "v"
    .parameter "progressBarQuit"

    .prologue
    const-wide/16 v10, 0xfa

    const/high16 v9, 0x3f80

    const/4 v8, 0x0

    const v5, 0x60b005e

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    .local v0, p:Landroid/widget/ProgressBar;
    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getVisibility()I

    move-result v5

    if-eqz v5, :cond_15

    :goto_14
    return-void

    :cond_15
    const v5, 0x60b005f

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .local v3, tv:Landroid/view/View;
    if-eqz p2, :cond_44

    const/16 v5, 0x8

    invoke-virtual {v0, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    new-instance v1, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v1, v9, v8}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .local v1, pAnim:Landroid/view/animation/Animation;
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v5

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getLeft()I

    move-result v6

    sub-int/2addr v5, v6

    int-to-float v5, v5

    invoke-direct {v2, v5, v8, v8, v8}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .local v2, tAnim:Landroid/view/animation/Animation;
    :goto_37
    invoke-virtual {v1, v10, v11}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    invoke-virtual {v2, v10, v11}, Landroid/view/animation/Animation;->setDuration(J)V

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->startAnimation(Landroid/view/animation/Animation;)V

    invoke-virtual {v3, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_14

    .end local v1           #pAnim:Landroid/view/animation/Animation;
    .end local v2           #tAnim:Landroid/view/animation/Animation;
    :cond_44
    const/4 v5, 0x1

    const v6, 0x424551ec

    iget-object v7, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;

    iget-object v7, v7, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mActivity:Landroid/app/Activity;

    invoke-virtual {v7}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v7

    invoke-static {v5, v6, v7}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v4

    .local v4, xmove:F
    new-instance v1, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v1, v8, v9}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .restart local v1       #pAnim:Landroid/view/animation/Animation;
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    neg-float v5, v4

    invoke-direct {v2, v5, v8, v8, v8}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .restart local v2       #tAnim:Landroid/view/animation/Animation;
    goto :goto_37
.end method


# virtual methods
.method public clickMusicUI(Landroid/view/View;)V
    .registers 5
    .parameter "v"

    .prologue
    invoke-direct {p0, p1}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->clearGoneViews(Landroid/view/View;)V

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .local v0, position:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    iget-object v1, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;

    invoke-virtual {v1, v0}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;->getResource(Landroid/util/Pair;)Lmiui/app/resourcebrowser/resource/Resource;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {p0, p1, v1, v2}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->setOperatingViewsState(Landroid/view/View;Lmiui/app/resourcebrowser/resource/Resource;Z)V

    iget-object v1, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->mSelectPosition:Landroid/util/Pair;

    iput-object v1, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->mLastSelectPosition:Landroid/util/Pair;

    iput-object v0, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->mSelectPosition:Landroid/util/Pair;

    iget-object v1, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->mGoneViewWhenClick:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->startProgressBarAnimation(Landroid/view/View;Z)V

    return-void
.end method

.method public getMusicPlayListener()Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$PlayProgressListener;
    .registers 2

    .prologue
    new-instance v0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI$1;

    invoke-direct {v0, p0}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI$1;-><init>(Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;)V

    return-object v0
.end method

.method public initMusicUI(Landroid/view/View;)V
    .registers 6
    .parameter "v"

    .prologue
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .local v0, position:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    iget-object v3, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;

    invoke-virtual {v3, v0}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;->getResource(Landroid/util/Pair;)Lmiui/app/resourcebrowser/resource/Resource;

    move-result-object v1

    .local v1, r:Lmiui/app/resourcebrowser/resource/Resource;
    iget-object v3, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->mSelectPosition:Landroid/util/Pair;

    invoke-virtual {v0, v3}, Landroid/util/Pair;->equals(Ljava/lang/Object;)Z

    move-result v2

    .local v2, selected:Z
    invoke-direct {p0, p1, v1, v2}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->setOperatingViewsState(Landroid/view/View;Lmiui/app/resourcebrowser/resource/Resource;Z)V

    if-eqz v2, :cond_1c

    iget-object v3, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->mGoneViewWhenClick:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1c
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 11
    .parameter "v"

    .prologue
    move-object v0, p1

    check-cast v0, Landroid/widget/Button;

    .local v0, btn:Landroid/widget/Button;
    invoke-virtual {v0}, Landroid/widget/Button;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/util/Pair;

    .local v5, tag:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;Ljava/lang/Integer;>;"
    iget-object v3, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Landroid/util/Pair;

    .local v3, position:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    iget-object v6, v5, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .local v1, btnState:I
    iget-object v6, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;

    invoke-virtual {v6, v3}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;->getResource(Landroid/util/Pair;)Lmiui/app/resourcebrowser/resource/Resource;

    move-result-object v4

    .local v4, r:Lmiui/app/resourcebrowser/resource/Resource;
    packed-switch v1, :pswitch_data_7e

    :goto_1e
    return-void

    :pswitch_1f
    iget-object v6, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;

    invoke-virtual {v6, v4}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;->handleApplyEvent(Lmiui/app/resourcebrowser/resource/Resource;)V

    goto :goto_1e

    :pswitch_25
    iget-object v6, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;

    invoke-virtual {v6, v4}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;->handlePickEvent(Lmiui/app/resourcebrowser/resource/Resource;)V

    goto :goto_1e

    :pswitch_2b
    iget-object v6, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;

    invoke-virtual {v6, v4}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;->handleDownloadEvent(Lmiui/app/resourcebrowser/resource/Resource;)V

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;

    iget-object v7, v7, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mActivity:Landroid/app/Activity;

    const/high16 v8, 0x104

    invoke-virtual {v7, v8}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;

    iget-object v7, v7, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mActivity:Landroid/app/Activity;

    const v8, 0x60c0017

    invoke-virtual {v7, v8}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .local v2, cancelTip:Ljava/lang/String;
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    new-instance v6, Landroid/util/Pair;

    const/4 v7, 0x4

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-direct {v6, v3, v7}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v0}, Landroid/widget/Button;->getParent()Landroid/view/ViewParent;

    move-result-object v6

    check-cast v6, Landroid/view/View;

    const v7, 0x60b004d

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1e

    .end local v2           #cancelTip:Ljava/lang/String;
    :pswitch_78
    iget-object v6, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->this$0:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;

    invoke-virtual {v6, v4}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler;->handleCancelDownloadEvent(Lmiui/app/resourcebrowser/resource/Resource;)V

    goto :goto_1e

    :pswitch_data_7e
    .packed-switch 0x1
        :pswitch_1f
        :pswitch_25
        :pswitch_2b
        :pswitch_78
    .end packed-switch
.end method

.method public reset()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->clearGoneViews(Landroid/view/View;)V

    iput-object v0, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->mSelectPosition:Landroid/util/Pair;

    return-void
.end method
