.class public Lmiui/app/resourcebrowser/view/ResourceOperationHandler;
.super Ljava/lang/Object;
.source "ResourceOperationHandler.java"


# static fields
#the value of this static final field might be set in the static constructor
.field protected static final DBG:Z = false

.field protected static final TAG:Ljava/lang/String; = "Theme"


# instance fields
.field protected mActivity:Landroid/app/Activity;

.field protected mDownloadHandler:Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;

.field protected mOperationView:Lmiui/app/resourcebrowser/view/ResourceOperationView;

.field protected mResource:Lmiui/app/resourcebrowser/view/ResourceState;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    sget-boolean v0, Lmiui/app/resourcebrowser/util/ResourceDebug;->DEBUG:Z

    sput-boolean v0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->DBG:Z

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lmiui/app/resourcebrowser/view/ResourceOperationView;Lmiui/app/resourcebrowser/view/ResourceState;)V
    .registers 6
    .parameter "activity"
    .parameter "view"
    .parameter "resource"

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    if-nez p2, :cond_d

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Operated view can not be null."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_d
    sget-boolean v0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->DBG:Z

    if-eqz v0, :cond_1a

    const-string v0, "Theme"

    invoke-virtual {p3}, Lmiui/app/resourcebrowser/view/ResourceState;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1a
    iput-object p3, p0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/app/resourcebrowser/view/ResourceState;

    iput-object p2, p0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->mOperationView:Lmiui/app/resourcebrowser/view/ResourceOperationView;

    iput-object p1, p0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->mActivity:Landroid/app/Activity;

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->mActivity:Landroid/app/Activity;

    invoke-direct {p0, v0}, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->getResourceDownloadHandler(Landroid/content/Context;)Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;

    move-result-object v0

    iput-object v0, p0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->mDownloadHandler:Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->mDownloadHandler:Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;->registerDownloadReceiver()V

    return-void
.end method

.method private doDownloadResource()V
    .registers 5

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->mDownloadHandler:Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;

    iget-object v1, p0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/app/resourcebrowser/view/ResourceState;

    iget-object v1, v1, Lmiui/app/resourcebrowser/view/ResourceState;->downloadUrl:Ljava/lang/String;

    iget-object v2, p0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/app/resourcebrowser/view/ResourceState;

    iget-object v2, v2, Lmiui/app/resourcebrowser/view/ResourceState;->downloadSavePath:Ljava/lang/String;

    iget-object v3, p0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/app/resourcebrowser/view/ResourceState;

    iget-object v3, v3, Lmiui/app/resourcebrowser/view/ResourceState;->title:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;->downloadResource(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    return-void
.end method

.method private getResourceDownloadHandler(Landroid/content/Context;)Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;
    .registers 3
    .parameter "context"

    .prologue
    new-instance v0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler$1;

    invoke-direct {v0, p0, p1}, Lmiui/app/resourcebrowser/view/ResourceOperationHandler$1;-><init>(Lmiui/app/resourcebrowser/view/ResourceOperationHandler;Landroid/content/Context;)V

    return-object v0
.end method

.method private isVirtualResource()Z
    .registers 4

    .prologue
    const/4 v0, 0x0

    .local v0, ret:Z
    iget-object v1, p0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/app/resourcebrowser/view/ResourceState;

    iget-object v1, v1, Lmiui/app/resourcebrowser/view/ResourceState;->localPath:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_14

    iget-object v1, p0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/app/resourcebrowser/view/ResourceState;

    iget-object v1, v1, Lmiui/app/resourcebrowser/view/ResourceState;->downloadUrl:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    :goto_13
    return v0

    :cond_14
    iget-object v1, p0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/app/resourcebrowser/view/ResourceState;

    iget-object v1, v1, Lmiui/app/resourcebrowser/view/ResourceState;->localPath:Ljava/lang/String;

    const-string v2, "content://"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    goto :goto_13
.end method


# virtual methods
.method protected clean()V
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->mDownloadHandler:Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;->unregisterDownloadReceiver()V

    return-void
.end method

.method protected finalize()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->clean()V

    return-void
.end method

.method public getResourceState()Lmiui/app/resourcebrowser/view/ResourceState;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/app/resourcebrowser/view/ResourceState;

    return-object v0
.end method

.method public handleApplyEvent()V
    .registers 4

    .prologue
    sget-boolean v0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->DBG:Z

    if-eqz v0, :cond_20

    const-string v0, "Theme"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "OperationHandler.handleApplyEvent(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/app/resourcebrowser/view/ResourceState;

    iget-object v2, v2, Lmiui/app/resourcebrowser/view/ResourceState;->localPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_20
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->refreshUI()V

    return-void
.end method

.method public handleDeleteEvent()V
    .registers 4

    .prologue
    sget-boolean v0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->DBG:Z

    if-eqz v0, :cond_20

    const-string v0, "Theme"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "OperationHandler.handleDeleteEvent(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/app/resourcebrowser/view/ResourceState;

    iget-object v2, v2, Lmiui/app/resourcebrowser/view/ResourceState;->localPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_20
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/app/resourcebrowser/view/ResourceState;

    iget-object v1, v1, Lmiui/app/resourcebrowser/view/ResourceState;->localPath:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->refreshUI()V

    return-void
.end method

.method public handleDownloadEvent()V
    .registers 4

    .prologue
    sget-boolean v0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->DBG:Z

    if-eqz v0, :cond_20

    const-string v0, "Theme"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "OperationHandler.handleDownloadEvent(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/app/resourcebrowser/view/ResourceState;

    iget-object v2, v2, Lmiui/app/resourcebrowser/view/ResourceState;->localPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_20
    invoke-direct {p0}, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->doDownloadResource()V

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->refreshUI()V

    return-void
.end method

.method public handleMagicEvent()V
    .registers 4

    .prologue
    sget-boolean v0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->DBG:Z

    if-eqz v0, :cond_20

    const-string v0, "Theme"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "OperationHandler.handleMagicEvent(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/app/resourcebrowser/view/ResourceState;

    iget-object v2, v2, Lmiui/app/resourcebrowser/view/ResourceState;->localPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_20
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->refreshUI()V

    return-void
.end method

.method public handlePickEvent()V
    .registers 4

    .prologue
    sget-boolean v0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->DBG:Z

    if-eqz v0, :cond_20

    const-string v0, "Theme"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "OperationHandler.handlePickEvent(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/app/resourcebrowser/view/ResourceState;

    iget-object v2, v2, Lmiui/app/resourcebrowser/view/ResourceState;->localPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_20
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->refreshUI()V

    return-void
.end method

.method public handleResourceDownloadFailedEvent(Ljava/lang/String;)V
    .registers 5
    .parameter "downloadFilePath"

    .prologue
    sget-boolean v0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->DBG:Z

    if-eqz v0, :cond_1c

    const-string v0, "Theme"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleDownloadFailedEvent(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1c
    invoke-virtual {p0, p1}, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->isCurrentResourceDownloadSavePath(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_33

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->mActivity:Landroid/app/Activity;

    const v1, 0x60c0023

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->refreshUI(Lmiui/app/resourcebrowser/view/ResourceState;)V

    :cond_33
    return-void
.end method

.method public handleResourceDownloadProgressEvent(Ljava/lang/String;II)V
    .registers 7
    .parameter "downloadFilePath"
    .parameter "downloadBytes"
    .parameter "totalBytes"

    .prologue
    sget-boolean v0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->DBG:Z

    if-eqz v0, :cond_1c

    const-string v0, "Theme"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "OperationHandler.handleResourceDownloadProgressEvent(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1c
    invoke-virtual {p0, p1}, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->isCurrentResourceDownloadSavePath(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_27

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->mOperationView:Lmiui/app/resourcebrowser/view/ResourceOperationView;

    invoke-virtual {v0, p2, p3}, Lmiui/app/resourcebrowser/view/ResourceOperationView;->updateDownloadProgressBar(II)V

    :cond_27
    return-void
.end method

.method public handleResourceDownloadSuccessedEvent(Ljava/lang/String;)V
    .registers 5
    .parameter "downloadFilePath"

    .prologue
    sget-boolean v0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->DBG:Z

    if-eqz v0, :cond_1c

    const-string v0, "Theme"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "OperationHandler.handleDownloadSuccessedEvent(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1c
    invoke-virtual {p0, p1}, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->isCurrentResourceDownloadSavePath(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_26

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->refreshUI(Lmiui/app/resourcebrowser/view/ResourceState;)V

    :cond_26
    return-void
.end method

.method public handleUpdateEvent()V
    .registers 4

    .prologue
    sget-boolean v0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->DBG:Z

    if-eqz v0, :cond_20

    const-string v0, "Theme"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "OperationHandler.handleUpdateEvent(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/app/resourcebrowser/view/ResourceState;

    iget-object v2, v2, Lmiui/app/resourcebrowser/view/ResourceState;->localPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_20
    invoke-direct {p0}, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->doDownloadResource()V

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->refreshUI()V

    return-void
.end method

.method public isCurrentResourceDownloadSavePath(Ljava/lang/String;)Z
    .registers 3
    .parameter "downloadSavePath"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/app/resourcebrowser/view/ResourceState;

    iget-object v0, v0, Lmiui/app/resourcebrowser/view/ResourceState;->downloadSavePath:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public isDeletable()Z
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/app/resourcebrowser/view/ResourceState;

    iget-boolean v0, v0, Lmiui/app/resourcebrowser/view/ResourceState;->allowDelete:Z

    if-eqz v0, :cond_14

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->isLocalResource()Z

    move-result v0

    if-eqz v0, :cond_14

    invoke-direct {p0}, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->isVirtualResource()Z

    move-result v0

    if-nez v0, :cond_14

    const/4 v0, 0x1

    :goto_13
    return v0

    :cond_14
    const/4 v0, 0x0

    goto :goto_13
.end method

.method public isDownloading()Z
    .registers 3

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->mDownloadHandler:Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;

    iget-object v1, p0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/app/resourcebrowser/view/ResourceState;

    iget-object v1, v1, Lmiui/app/resourcebrowser/view/ResourceState;->downloadSavePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;->isResourceDownloading(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isLocalResource()Z
    .registers 3

    .prologue
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/app/resourcebrowser/view/ResourceState;

    iget-object v1, v1, Lmiui/app/resourcebrowser/view/ResourceState;->localPath:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_15

    invoke-direct {p0}, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->isVirtualResource()Z

    move-result v0

    if-eqz v0, :cond_17

    :cond_15
    const/4 v0, 0x1

    :goto_16
    return v0

    :cond_17
    const/4 v0, 0x0

    goto :goto_16
.end method

.method public isOldVersionResource()Z
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/app/resourcebrowser/view/ResourceState;

    iget-boolean v0, v0, Lmiui/app/resourcebrowser/view/ResourceState;->hasUpdate:Z

    return v0
.end method

.method public isPicker()Z
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/app/resourcebrowser/view/ResourceState;

    iget-boolean v0, v0, Lmiui/app/resourcebrowser/view/ResourceState;->isPicker:Z

    return v0
.end method

.method public onActivitResultEvent(IILandroid/content/Intent;)V
    .registers 6
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    sget-boolean v0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->DBG:Z

    if-eqz v0, :cond_b

    const-string v0, "Theme"

    const-string v1, "OperationHandler.onActivitResultEvent()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_b
    return-void
.end method

.method public onActivityOnDestroyEvent()V
    .registers 3

    .prologue
    sget-boolean v0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->DBG:Z

    if-eqz v0, :cond_b

    const-string v0, "Theme"

    const-string v1, "OperationHandler.onActivityOnDestroyEvent()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_b
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->clean()V

    return-void
.end method

.method public onActivityOnPauseEvent()V
    .registers 1

    .prologue
    return-void
.end method

.method public onActivityOnResumeEvent()V
    .registers 1

    .prologue
    return-void
.end method

.method public refreshUI()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->refreshUI(Lmiui/app/resourcebrowser/view/ResourceState;)V

    return-void
.end method

.method public refreshUI(Lmiui/app/resourcebrowser/view/ResourceState;)V
    .registers 3
    .parameter "resource"

    .prologue
    if-eqz p1, :cond_4

    iput-object p1, p0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->mResource:Lmiui/app/resourcebrowser/view/ResourceState;

    :cond_4
    iget-object v0, p0, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->mOperationView:Lmiui/app/resourcebrowser/view/ResourceOperationView;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/view/ResourceOperationView;->setResourceStatus()V

    return-void
.end method
