.class Lmiui/app/resourcebrowser/view/BatchResourceHandler$1;
.super Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;
.source "BatchResourceHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiui/app/resourcebrowser/view/BatchResourceHandler;->getResourceDownloadHandler()Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mLastNotifyProgressTime:J

.field final synthetic this$0:Lmiui/app/resourcebrowser/view/BatchResourceHandler;


# direct methods
.method constructor <init>(Lmiui/app/resourcebrowser/view/BatchResourceHandler;Landroid/content/Context;)V
    .registers 5
    .parameter
    .parameter "x0"

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$1;->this$0:Lmiui/app/resourcebrowser/view/BatchResourceHandler;

    invoke-direct {p0, p2}, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;-><init>(Landroid/content/Context;)V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$1;->mLastNotifyProgressTime:J

    return-void
.end method


# virtual methods
.method public handleDownloadFailed(Ljava/lang/String;)V
    .registers 5
    .parameter "downloadFilePath"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$1;->this$0:Lmiui/app/resourcebrowser/view/BatchResourceHandler;

    iget-object v0, v0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mActivity:Landroid/app/Activity;

    const v1, 0x60c0023

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$1;->this$0:Lmiui/app/resourcebrowser/view/BatchResourceHandler;

    iget-object v0, v0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mDownloadBytes:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public handleDownloadProgress(Ljava/lang/String;II)V
    .registers 8
    .parameter "downloadFilePath"
    .parameter "downloadBytes"
    .parameter "totalBytes"

    .prologue
    if-eq p2, p3, :cond_27

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$1;->this$0:Lmiui/app/resourcebrowser/view/BatchResourceHandler;

    iget-object v0, v0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mDownloadBytes:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$1;->mLastNotifyProgressTime:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x258

    cmp-long v0, v0, v2

    if-lez v0, :cond_27

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$1;->mLastNotifyProgressTime:J

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$1;->this$0:Lmiui/app/resourcebrowser/view/BatchResourceHandler;

    iget-object v0, v0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mAdapter:Lmiui/app/resourcebrowser/ResourceAdapter;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/ResourceAdapter;->notifyDataSetChanged()V

    :cond_27
    return-void
.end method

.method public handleDownloadSuccessed(Ljava/lang/String;)V
    .registers 4
    .parameter "downloadFilePath"

    .prologue
    const/4 v1, 0x0

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$1;->this$0:Lmiui/app/resourcebrowser/view/BatchResourceHandler;

    iget-object v0, v0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mAdapter:Lmiui/app/resourcebrowser/ResourceAdapter;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/ResourceAdapter;->getResourceSet()Lmiui/app/resourcebrowser/resource/ResourceSet;

    move-result-object v0

    invoke-virtual {v0, v1}, Lmiui/app/resourcebrowser/resource/ResourceSet;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/widget/DataGroup;

    invoke-static {p1, v0, v1}, Lmiui/app/resourcebrowser/util/ResourceHelper;->setResourceStatus(Ljava/lang/String;Lmiui/widget/DataGroup;I)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$1;->this$0:Lmiui/app/resourcebrowser/view/BatchResourceHandler;

    iget-object v0, v0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mAdapter:Lmiui/app/resourcebrowser/ResourceAdapter;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/ResourceAdapter;->notifyDataSetChanged()V

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$1;->this$0:Lmiui/app/resourcebrowser/view/BatchResourceHandler;

    iget-object v0, v0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mAdapter:Lmiui/app/resourcebrowser/ResourceAdapter;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/ResourceAdapter;->loadData()V

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$1;->this$0:Lmiui/app/resourcebrowser/view/BatchResourceHandler;

    iget-object v0, v0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mDownloadBytes:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
