.class Lmiui/app/resourcebrowser/view/BatchResourceHandler$5;
.super Landroid/os/AsyncTask;
.source "BatchResourceHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiui/app/resourcebrowser/view/BatchResourceHandler;->deleteOrDownloadResources()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private mProgress:Landroid/app/ProgressDialog;

.field final synthetic this$0:Lmiui/app/resourcebrowser/view/BatchResourceHandler;


# direct methods
.method constructor <init>(Lmiui/app/resourcebrowser/view/BatchResourceHandler;)V
    .registers 2
    .parameter

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$5;->this$0:Lmiui/app/resourcebrowser/view/BatchResourceHandler;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    .prologue
    check-cast p1, [Ljava/lang/Void;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/app/resourcebrowser/view/BatchResourceHandler$5;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .registers 9
    .parameter "params"

    .prologue
    iget-object v5, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$5;->this$0:Lmiui/app/resourcebrowser/view/BatchResourceHandler;

    iget-boolean v5, v5, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mLocalResourcePage:Z

    if-eqz v5, :cond_75

    iget-object v5, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$5;->this$0:Lmiui/app/resourcebrowser/view/BatchResourceHandler;

    iget-object v5, v5, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mAdapter:Lmiui/app/resourcebrowser/ResourceAdapter;

    invoke-virtual {v5}, Lmiui/app/resourcebrowser/ResourceAdapter;->getResourceSet()Lmiui/app/resourcebrowser/resource/ResourceSet;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lmiui/app/resourcebrowser/resource/ResourceSet;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmiui/widget/DataGroup;

    .local v3, originDataSet:Lmiui/widget/DataGroup;,"Lmiui/widget/DataGroup<Lmiui/app/resourcebrowser/resource/Resource;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {v3}, Lmiui/widget/DataGroup;->size()I

    move-result v5

    iget-object v6, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$5;->this$0:Lmiui/app/resourcebrowser/view/BatchResourceHandler;

    #getter for: Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mCheckedResource:Ljava/util/HashSet;
    invoke-static {v6}, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->access$200(Lmiui/app/resourcebrowser/view/BatchResourceHandler;)Ljava/util/HashSet;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/HashSet;->size()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-direct {v2, v5}, Ljava/util/ArrayList;-><init>(I)V

    .local v2, newDataSets:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lmiui/app/resourcebrowser/resource/Resource;>;"
    const/4 v0, 0x0

    .local v0, dividerTitle:Ljava/lang/String;
    invoke-virtual {v3}, Lmiui/widget/DataGroup;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_2e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lmiui/app/resourcebrowser/resource/Resource;

    .local v4, r:Lmiui/app/resourcebrowser/resource/Resource;
    iget-object v5, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$5;->this$0:Lmiui/app/resourcebrowser/view/BatchResourceHandler;

    #getter for: Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mCheckedResource:Ljava/util/HashSet;
    invoke-static {v5}, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->access$200(Lmiui/app/resourcebrowser/view/BatchResourceHandler;)Ljava/util/HashSet;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5d

    invoke-virtual {v4}, Lmiui/app/resourcebrowser/resource/Resource;->getDividerTitle()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_50

    invoke-virtual {v4}, Lmiui/app/resourcebrowser/resource/Resource;->getDividerTitle()Ljava/lang/String;

    move-result-object v0

    :cond_50
    new-instance v5, Ljava/io/File;

    invoke-virtual {v4}, Lmiui/app/resourcebrowser/resource/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    goto :goto_2e

    :cond_5d
    invoke-virtual {v4}, Lmiui/app/resourcebrowser/resource/Resource;->getDividerTitle()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_68

    if-eqz v0, :cond_68

    invoke-virtual {v4, v0}, Lmiui/app/resourcebrowser/resource/Resource;->setDividerTitle(Ljava/lang/String;)V

    :cond_68
    const/4 v0, 0x0

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2e

    .end local v4           #r:Lmiui/app/resourcebrowser/resource/Resource;
    :cond_6d
    invoke-virtual {v3}, Lmiui/widget/DataGroup;->clear()V

    invoke-virtual {v3, v2}, Lmiui/widget/DataGroup;->addAll(Ljava/util/Collection;)Z

    .end local v0           #dividerTitle:Ljava/lang/String;
    .end local v2           #newDataSets:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lmiui/app/resourcebrowser/resource/Resource;>;"
    .end local v3           #originDataSet:Lmiui/widget/DataGroup;,"Lmiui/widget/DataGroup<Lmiui/app/resourcebrowser/resource/Resource;>;"
    :cond_73
    const/4 v5, 0x0

    return-object v5

    .end local v1           #i$:Ljava/util/Iterator;
    :cond_75
    iget-object v5, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$5;->this$0:Lmiui/app/resourcebrowser/view/BatchResourceHandler;

    #getter for: Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mCheckedResource:Ljava/util/HashSet;
    invoke-static {v5}, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->access$200(Lmiui/app/resourcebrowser/view/BatchResourceHandler;)Ljava/util/HashSet;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1       #i$:Ljava/util/Iterator;
    :goto_7f
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_73

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lmiui/app/resourcebrowser/resource/Resource;

    .restart local v4       #r:Lmiui/app/resourcebrowser/resource/Resource;
    iget-object v5, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$5;->this$0:Lmiui/app/resourcebrowser/view/BatchResourceHandler;

    invoke-virtual {v5, v4}, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->doDownloadResource(Lmiui/app/resourcebrowser/resource/Resource;)V

    goto :goto_7f
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 2
    .parameter "x0"

    .prologue
    check-cast p1, Ljava/lang/Void;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/app/resourcebrowser/view/BatchResourceHandler$5;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .registers 3
    .parameter "result"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$5;->this$0:Lmiui/app/resourcebrowser/view/BatchResourceHandler;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->quitEditMode()V

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$5;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    return-void
.end method

.method protected onPreExecute()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$5;->this$0:Lmiui/app/resourcebrowser/view/BatchResourceHandler;

    iget-object v1, v1, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mAdapter:Lmiui/app/resourcebrowser/ResourceAdapter;

    iget-object v1, v1, Lmiui/app/resourcebrowser/ResourceAdapter;->mContext:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$5;->mProgress:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$5;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$5;->mProgress:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$5;->this$0:Lmiui/app/resourcebrowser/view/BatchResourceHandler;

    iget-object v1, v1, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mAdapter:Lmiui/app/resourcebrowser/ResourceAdapter;

    iget-object v1, v1, Lmiui/app/resourcebrowser/ResourceAdapter;->mContext:Landroid/app/Activity;

    const v2, 0x60c01b8

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$5;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$5;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    return-void
.end method
