.class public final enum Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;
.super Ljava/lang/Enum;
.source "BatchResourceHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/app/resourcebrowser/view/BatchResourceHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ViewState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;

.field public static final enum DESTYOY:Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;

.field public static final enum INVISIABLE:Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;

.field public static final enum PAUSE:Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;

.field public static final enum UI_UPDATE:Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;

.field public static final enum VISIABLE:Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;

    const-string v1, "VISIABLE"

    invoke-direct {v0, v1, v2}, Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;->VISIABLE:Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;

    new-instance v0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;

    const-string v1, "INVISIABLE"

    invoke-direct {v0, v1, v3}, Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;->INVISIABLE:Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;

    new-instance v0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;

    const-string v1, "UI_UPDATE"

    invoke-direct {v0, v1, v4}, Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;->UI_UPDATE:Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;

    new-instance v0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;

    const-string v1, "PAUSE"

    invoke-direct {v0, v1, v5}, Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;->PAUSE:Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;

    new-instance v0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;

    const-string v1, "DESTYOY"

    invoke-direct {v0, v1, v6}, Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;->DESTYOY:Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;

    const/4 v0, 0x5

    new-array v0, v0, [Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;

    sget-object v1, Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;->VISIABLE:Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;

    aput-object v1, v0, v2

    sget-object v1, Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;->INVISIABLE:Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;

    aput-object v1, v0, v3

    sget-object v1, Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;->UI_UPDATE:Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;

    aput-object v1, v0, v4

    sget-object v1, Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;->PAUSE:Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;

    aput-object v1, v0, v5

    sget-object v1, Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;->DESTYOY:Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;

    aput-object v1, v0, v6

    sput-object v0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;->$VALUES:[Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;
    .registers 2
    .parameter "name"

    .prologue
    const-class v0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;

    return-object v0
.end method

.method public static values()[Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;
    .registers 1

    .prologue
    sget-object v0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;->$VALUES:[Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;

    invoke-virtual {v0}, [Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;

    return-object v0
.end method
