.class Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI$1;
.super Ljava/lang/Object;
.source "AudioBatchResourceHandler.java"

# interfaces
.implements Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$PlayProgressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->getMusicPlayListener()Lmiui/app/resourcebrowser/util/ResourceMusicPlayer$PlayProgressListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;


# direct methods
.method constructor <init>(Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;)V
    .registers 2
    .parameter

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI$1;->this$1:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressUpdate(II)V
    .registers 10
    .parameter "playDuration"
    .parameter "totalDuration"

    .prologue
    mul-int/lit8 v3, p1, 0x64

    int-to-double v3, v3

    const-wide/high16 v5, 0x3ff0

    mul-double/2addr v3, v5

    int-to-double v5, p2

    div-double/2addr v3, v5

    double-to-int v1, v3

    .local v1, progress:I
    iget-object v3, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI$1;->this$1:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;

    iput v1, v3, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->mLastPlayProgress:I

    iget-object v3, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI$1;->this$1:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;

    #getter for: Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->mGoneViewWhenClick:Ljava/util/ArrayList;
    invoke-static {v3}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->access$000(Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_17
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_37

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .local v2, v:Landroid/view/View;
    iget-object v4, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI$1;->this$1:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;

    const v3, 0x60b005e

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ProgressBar;

    const/4 v5, 0x0

    iget-object v6, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI$1;->this$1:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;

    iget v6, v6, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->mLastPlayProgress:I

    #calls: Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->setProgressBarState(Landroid/widget/ProgressBar;ZI)V
    invoke-static {v4, v3, v5, v6}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->access$100(Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;Landroid/widget/ProgressBar;ZI)V

    goto :goto_17

    .end local v2           #v:Landroid/view/View;
    :cond_37
    return-void
.end method

.method public onStartPlaying()V
    .registers 7

    .prologue
    const/4 v5, 0x0

    iget-object v2, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI$1;->this$1:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;

    iput v5, v2, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->mLastPlayProgress:I

    iget-object v2, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI$1;->this$1:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;

    #getter for: Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->mGoneViewWhenClick:Ljava/util/ArrayList;
    invoke-static {v2}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->access$000(Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_f
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2e

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .local v1, v:Landroid/view/View;
    iget-object v3, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI$1;->this$1:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;

    const v2, 0x60b005e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    iget-object v4, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI$1;->this$1:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;

    iget v4, v4, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->mLastPlayProgress:I

    #calls: Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->setProgressBarState(Landroid/widget/ProgressBar;ZI)V
    invoke-static {v3, v2, v5, v4}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->access$100(Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;Landroid/widget/ProgressBar;ZI)V

    goto :goto_f

    .end local v1           #v:Landroid/view/View;
    :cond_2e
    return-void
.end method

.method public onStopPlaying()V
    .registers 6

    .prologue
    iget-object v3, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI$1;->this$1:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;

    #getter for: Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->mGoneViewWhenClick:Ljava/util/ArrayList;
    invoke-static {v3}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->access$000(Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :cond_a
    :goto_a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3b

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .local v2, v:Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    .local v1, position:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    iget-object v3, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI$1;->this$1:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;

    #getter for: Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->mSelectPosition:Landroid/util/Pair;
    invoke-static {v3}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->access$200(Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;)Landroid/util/Pair;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/util/Pair;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_34

    iget-object v3, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI$1;->this$1:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;

    #getter for: Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->mLastSelectPosition:Landroid/util/Pair;
    invoke-static {v3}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->access$300(Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;)Landroid/util/Pair;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/util/Pair;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    :cond_34
    iget-object v3, p0, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI$1;->this$1:Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;

    const/4 v4, 0x1

    #calls: Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->startProgressBarAnimation(Landroid/view/View;Z)V
    invoke-static {v3, v2, v4}, Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;->access$400(Lmiui/app/resourcebrowser/view/AudioBatchResourceHandler$ResourceMusicUI;Landroid/view/View;Z)V

    goto :goto_a

    .end local v1           #position:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    .end local v2           #v:Landroid/view/View;
    :cond_3b
    return-void
.end method
