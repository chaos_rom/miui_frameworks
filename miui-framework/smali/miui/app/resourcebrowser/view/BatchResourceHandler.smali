.class public Lmiui/app/resourcebrowser/view/BatchResourceHandler;
.super Ljava/lang/Object;
.source "BatchResourceHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;
    }
.end annotation


# instance fields
.field private mActionMode:Landroid/view/ActionMode;

.field private mActionModeCb:Landroid/view/ActionMode$Callback;

.field protected mActivity:Landroid/app/Activity;

.field protected mAdapter:Lmiui/app/resourcebrowser/ResourceAdapter;

.field private mCheckedResource:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lmiui/app/resourcebrowser/resource/Resource;",
            ">;"
        }
    .end annotation
.end field

.field protected mDownloadBytes:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mDownloadHandler:Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;

.field private mEditableMode:Z

.field protected mFragment:Lmiui/app/resourcebrowser/ResourceListFragment;

.field private mItemClickListener:Landroid/view/View$OnClickListener;

.field private mItemLongClickListener:Landroid/view/View$OnLongClickListener;

.field protected mListenDownloadProgress:Z

.field protected mLocalResourcePage:Z

.field private mSelectTv:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lmiui/app/resourcebrowser/ResourceListActivity;Lmiui/app/resourcebrowser/ResourceAdapter;)V
    .registers 5
    .parameter "activity"
    .parameter "adapter"

    .prologue
    const/4 v1, 0x0

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v1, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mEditableMode:Z

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mCheckedResource:Ljava/util/HashSet;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mDownloadBytes:Ljava/util/HashMap;

    iput-boolean v1, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mListenDownloadProgress:Z

    new-instance v0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$2;

    invoke-direct {v0, p0}, Lmiui/app/resourcebrowser/view/BatchResourceHandler$2;-><init>(Lmiui/app/resourcebrowser/view/BatchResourceHandler;)V

    iput-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mItemClickListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$3;

    invoke-direct {v0, p0}, Lmiui/app/resourcebrowser/view/BatchResourceHandler$3;-><init>(Lmiui/app/resourcebrowser/view/BatchResourceHandler;)V

    iput-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mItemLongClickListener:Landroid/view/View$OnLongClickListener;

    new-instance v0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$4;

    invoke-direct {v0, p0}, Lmiui/app/resourcebrowser/view/BatchResourceHandler$4;-><init>(Lmiui/app/resourcebrowser/view/BatchResourceHandler;)V

    iput-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mActionModeCb:Landroid/view/ActionMode$Callback;

    if-eqz p1, :cond_2f

    if-nez p2, :cond_37

    :cond_2f
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "BatchResourceOperationHandler() parameters can not be null!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_37
    const/4 v0, 0x0

    iput-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mFragment:Lmiui/app/resourcebrowser/ResourceListFragment;

    iput-object p1, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mActivity:Landroid/app/Activity;

    iput-object p2, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mAdapter:Lmiui/app/resourcebrowser/ResourceAdapter;

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->getResourceDownloadHandler()Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;

    move-result-object v0

    iput-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mDownloadHandler:Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mDownloadHandler:Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;->registerDownloadReceiver(Z)V

    return-void
.end method

.method public constructor <init>(Lmiui/app/resourcebrowser/ResourceListFragment;Lmiui/app/resourcebrowser/ResourceAdapter;)V
    .registers 6
    .parameter "fragment"
    .parameter "adapter"

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v1, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mEditableMode:Z

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mCheckedResource:Ljava/util/HashSet;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mDownloadBytes:Ljava/util/HashMap;

    iput-boolean v1, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mListenDownloadProgress:Z

    new-instance v0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$2;

    invoke-direct {v0, p0}, Lmiui/app/resourcebrowser/view/BatchResourceHandler$2;-><init>(Lmiui/app/resourcebrowser/view/BatchResourceHandler;)V

    iput-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mItemClickListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$3;

    invoke-direct {v0, p0}, Lmiui/app/resourcebrowser/view/BatchResourceHandler$3;-><init>(Lmiui/app/resourcebrowser/view/BatchResourceHandler;)V

    iput-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mItemLongClickListener:Landroid/view/View$OnLongClickListener;

    new-instance v0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$4;

    invoke-direct {v0, p0}, Lmiui/app/resourcebrowser/view/BatchResourceHandler$4;-><init>(Lmiui/app/resourcebrowser/view/BatchResourceHandler;)V

    iput-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mActionModeCb:Landroid/view/ActionMode$Callback;

    if-eqz p1, :cond_30

    if-nez p2, :cond_38

    :cond_30
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "BatchResourceOperationHandler() parameters can not be null!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_38
    iput-object p1, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mFragment:Lmiui/app/resourcebrowser/ResourceListFragment;

    invoke-virtual {p1}, Lmiui/app/resourcebrowser/ResourceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mActivity:Landroid/app/Activity;

    iput-object p2, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mAdapter:Lmiui/app/resourcebrowser/ResourceAdapter;

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->getResourceDownloadHandler()Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;

    move-result-object v0

    iput-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mDownloadHandler:Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mDownloadHandler:Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;

    invoke-virtual {v0, v2}, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;->registerDownloadReceiver(Z)V

    instance-of v0, p1, Lmiui/app/resourcebrowser/OnlineResourceListFragment;

    if-eqz v0, :cond_54

    iput-boolean v1, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mLocalResourcePage:Z

    :goto_53
    return-void

    :cond_54
    iput-boolean v2, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mLocalResourcePage:Z

    goto :goto_53
.end method

.method static synthetic access$002(Lmiui/app/resourcebrowser/view/BatchResourceHandler;Landroid/widget/TextView;)Landroid/widget/TextView;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mSelectTv:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$100(Lmiui/app/resourcebrowser/view/BatchResourceHandler;)V
    .registers 1
    .parameter "x0"

    .prologue
    invoke-direct {p0}, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->deleteOrDownloadResources()V

    return-void
.end method

.method static synthetic access$200(Lmiui/app/resourcebrowser/view/BatchResourceHandler;)Ljava/util/HashSet;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mCheckedResource:Ljava/util/HashSet;

    return-object v0
.end method

.method private deleteOrDownloadResources()V
    .registers 3

    .prologue
    new-instance v0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$5;

    invoke-direct {v0, p0}, Lmiui/app/resourcebrowser/view/BatchResourceHandler$5;-><init>(Lmiui/app/resourcebrowser/view/BatchResourceHandler;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lmiui/app/resourcebrowser/view/BatchResourceHandler$5;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private setCheckBoxState(Landroid/widget/CheckBox;ZZ)V
    .registers 5
    .parameter "checkBox"
    .parameter "visiable"
    .parameter "checked"

    .prologue
    if-eqz p1, :cond_b

    if-eqz p2, :cond_c

    const/4 v0, 0x0

    :goto_5
    invoke-virtual {p1, v0}, Landroid/widget/CheckBox;->setVisibility(I)V

    invoke-virtual {p1, p3}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_b
    return-void

    :cond_c
    const/16 v0, 0x8

    goto :goto_5
.end method

.method private setViewBatchSelectedState(Landroid/view/View;)V
    .registers 9
    .parameter "v"

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    .local v1, position:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    if-nez v1, :cond_b

    :goto_a
    return-void

    :cond_b
    invoke-virtual {p0, v1}, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->getResource(Landroid/util/Pair;)Lmiui/app/resourcebrowser/resource/Resource;

    move-result-object v2

    .local v2, r:Lmiui/app/resourcebrowser/resource/Resource;
    iget-boolean v6, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mEditableMode:Z

    if-eqz v6, :cond_50

    iget-object v6, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mCheckedResource:Ljava/util/HashSet;

    invoke-virtual {v6, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_50

    move v3, v4

    .local v3, selected:Z
    :goto_1c
    iget-boolean v6, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mEditableMode:Z

    if-eqz v6, :cond_52

    invoke-virtual {p0, v2}, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->canSelected(Lmiui/app/resourcebrowser/resource/Resource;)Z

    move-result v6

    if-eqz v6, :cond_52

    move v0, v4

    .local v0, checkBoxVisiable:Z
    :goto_27
    invoke-virtual {p1, v3}, Landroid/view/View;->setSelected(Z)V

    const v4, 0x60b004c

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz v0, :cond_34

    const/4 v5, 0x4

    :cond_34
    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    const v4, 0x1020001

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    invoke-direct {p0, v4, v0, v3}, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->setCheckBoxState(Landroid/widget/CheckBox;ZZ)V

    const v4, 0x60b004d

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ProgressBar;

    invoke-virtual {p0, v4, v2}, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->setDownloadProgress(Landroid/widget/ProgressBar;Lmiui/app/resourcebrowser/resource/Resource;)V

    goto :goto_a

    .end local v0           #checkBoxVisiable:Z
    .end local v3           #selected:Z
    :cond_50
    move v3, v5

    goto :goto_1c

    .restart local v3       #selected:Z
    :cond_52
    move v0, v5

    goto :goto_27
.end method


# virtual methods
.method protected canDeletableResource(Lmiui/app/resourcebrowser/resource/Resource;)Z
    .registers 4
    .parameter "r"

    .prologue
    invoke-virtual {p1}, Lmiui/app/resourcebrowser/resource/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v0

    .local v0, localPath:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1d

    invoke-static {v0}, Lmiui/app/resourcebrowser/util/ResourceHelper;->isSystemResource(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1d

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1d

    const/4 v1, 0x1

    :goto_1c
    return v1

    :cond_1d
    const/4 v1, 0x0

    goto :goto_1c
.end method

.method protected canDownloadResource(Lmiui/app/resourcebrowser/resource/Resource;)Z
    .registers 4
    .parameter "r"

    .prologue
    invoke-virtual {p1}, Lmiui/app/resourcebrowser/resource/Resource;->getStatus()I

    move-result v0

    if-eqz v0, :cond_14

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mDownloadHandler:Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;

    invoke-virtual {p1}, Lmiui/app/resourcebrowser/resource/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;->isResourceDownloading(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_14

    const/4 v0, 0x1

    :goto_13
    return v0

    :cond_14
    const/4 v0, 0x0

    goto :goto_13
.end method

.method protected canSelected(Lmiui/app/resourcebrowser/resource/Resource;)Z
    .registers 3
    .parameter "r"

    .prologue
    iget-boolean v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mLocalResourcePage:Z

    if-eqz v0, :cond_a

    invoke-virtual {p0, p1}, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->canDeletableResource(Lmiui/app/resourcebrowser/resource/Resource;)Z

    move-result v0

    if-nez v0, :cond_14

    :cond_a
    iget-boolean v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mLocalResourcePage:Z

    if-nez v0, :cond_16

    invoke-virtual {p0, p1}, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->canDownloadResource(Lmiui/app/resourcebrowser/resource/Resource;)Z

    move-result v0

    if-eqz v0, :cond_16

    :cond_14
    const/4 v0, 0x1

    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method protected doCancelDownload(Lmiui/app/resourcebrowser/resource/Resource;)V
    .registers 4
    .parameter "r"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mDownloadHandler:Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;

    invoke-virtual {p1}, Lmiui/app/resourcebrowser/resource/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;->cancelDownload(Ljava/lang/String;)Z

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mDownloadBytes:Ljava/util/HashMap;

    invoke-virtual {p1}, Lmiui/app/resourcebrowser/resource/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mAdapter:Lmiui/app/resourcebrowser/ResourceAdapter;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/ResourceAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method protected doDownloadResource(Lmiui/app/resourcebrowser/resource/Resource;)V
    .registers 8
    .parameter "r"

    .prologue
    iget-object v1, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mDownloadHandler:Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;

    invoke-virtual {p1}, Lmiui/app/resourcebrowser/resource/Resource;->getOnlinePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lmiui/app/resourcebrowser/resource/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lmiui/app/resourcebrowser/resource/Resource;->getTitle()Ljava/lang/String;

    move-result-object v4

    iget-boolean v5, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mListenDownloadProgress:Z

    invoke-virtual {v1, v2, v3, v4, v5}, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;->downloadResource(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    .local v0, ret:Z
    if-eqz v0, :cond_24

    iget-object v1, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mDownloadBytes:Ljava/util/HashMap;

    invoke-virtual {p1}, Lmiui/app/resourcebrowser/resource/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_24
    return-void
.end method

.method protected enterEditMode(Landroid/view/View;Landroid/util/Pair;)V
    .registers 5
    .parameter "v"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, position:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mEditableMode:Z

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mCheckedResource:Ljava/util/HashSet;

    invoke-virtual {p0, p2}, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->getResource(Landroid/util/Pair;)Lmiui/app/resourcebrowser/resource/Resource;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-direct {p0, p1}, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->setViewBatchSelectedState(Landroid/view/View;)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mAdapter:Lmiui/app/resourcebrowser/ResourceAdapter;

    iget-object v0, v0, Lmiui/app/resourcebrowser/ResourceAdapter;->mContext:Landroid/app/Activity;

    iget-object v1, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mActionModeCb:Landroid/view/ActionMode$Callback;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v0

    iput-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mActionMode:Landroid/view/ActionMode;

    return-void
.end method

.method protected getDownloadBytes(Lmiui/app/resourcebrowser/resource/Resource;)I
    .registers 5
    .parameter "r"

    .prologue
    iget-object v1, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mDownloadBytes:Ljava/util/HashMap;

    invoke-virtual {p1}, Lmiui/app/resourcebrowser/resource/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .local v0, ret:Ljava/lang/Integer;
    if-eqz v0, :cond_13

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :goto_12
    return v1

    :cond_13
    const/4 v1, -0x1

    goto :goto_12
.end method

.method protected getResource(Landroid/util/Pair;)Lmiui/app/resourcebrowser/resource/Resource;
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)",
            "Lmiui/app/resourcebrowser/resource/Resource;"
        }
    .end annotation

    .prologue
    .local p1, position:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mAdapter:Lmiui/app/resourcebrowser/ResourceAdapter;

    invoke-virtual {v1}, Lmiui/app/resourcebrowser/ResourceAdapter;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_29

    iget-object v1, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mAdapter:Lmiui/app/resourcebrowser/ResourceAdapter;

    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v0, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v2, v0}, Lmiui/app/resourcebrowser/ResourceAdapter;->getDataItem(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/app/resourcebrowser/resource/Resource;

    :goto_28
    return-object v0

    :cond_29
    const/4 v0, 0x0

    goto :goto_28
.end method

.method public getResourceClickListener()Landroid/view/View$OnClickListener;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mItemClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method protected getResourceDownloadHandler()Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;
    .registers 3

    .prologue
    new-instance v0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$1;

    iget-object v1, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mActivity:Landroid/app/Activity;

    invoke-direct {v0, p0, v1}, Lmiui/app/resourcebrowser/view/BatchResourceHandler$1;-><init>(Lmiui/app/resourcebrowser/view/BatchResourceHandler;Landroid/content/Context;)V

    return-object v0
.end method

.method public getResourceLongClickListener()Landroid/view/View$OnLongClickListener;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mItemLongClickListener:Landroid/view/View$OnLongClickListener;

    return-object v0
.end method

.method public initViewState(Landroid/view/View;Landroid/util/Pair;)V
    .registers 4
    .parameter "v"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, position:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    invoke-virtual {p1, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mItemClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mItemLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    invoke-direct {p0, p1}, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->setViewBatchSelectedState(Landroid/view/View;)V

    return-void
.end method

.method protected isDownloading(Lmiui/app/resourcebrowser/resource/Resource;)Z
    .registers 4
    .parameter "r"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mDownloadBytes:Ljava/util/HashMap;

    invoke-virtual {p1}, Lmiui/app/resourcebrowser/resource/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isEditMode()Z
    .registers 2

    .prologue
    iget-boolean v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mEditableMode:Z

    return v0
.end method

.method protected onClick_Impl(Landroid/view/View;)V
    .registers 11
    .parameter "v"

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .local v0, position:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    if-nez v0, :cond_b

    :goto_a
    return-void

    :cond_b
    iget-boolean v5, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mEditableMode:Z

    if-eqz v5, :cond_78

    invoke-virtual {p0, v0}, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->getResource(Landroid/util/Pair;)Lmiui/app/resourcebrowser/resource/Resource;

    move-result-object v1

    .local v1, r:Lmiui/app/resourcebrowser/resource/Resource;
    invoke-virtual {p0, v1}, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->canSelected(Lmiui/app/resourcebrowser/resource/Resource;)Z

    move-result v5

    if-eqz v5, :cond_48

    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v5

    if-nez v5, :cond_39

    :goto_1f
    invoke-virtual {p1, v3}, Landroid/view/View;->setSelected(Z)V

    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_3b

    iget-object v3, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mCheckedResource:Ljava/util/HashSet;

    invoke-virtual {v3, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :goto_2d
    iget-object v3, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mCheckedResource:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_41

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->quitEditMode()V

    goto :goto_a

    :cond_39
    move v3, v4

    goto :goto_1f

    :cond_3b
    iget-object v3, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mCheckedResource:Ljava/util/HashSet;

    invoke-virtual {v3, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto :goto_2d

    :cond_41
    invoke-direct {p0, p1}, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->setViewBatchSelectedState(Landroid/view/View;)V

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->updateSelectedTitle()V

    goto :goto_a

    :cond_48
    iget-object v5, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mActivity:Landroid/app/Activity;

    const v6, 0x60c01b9

    new-array v7, v3, [Ljava/lang/Object;

    iget-boolean v3, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mLocalResourcePage:Z

    if-eqz v3, :cond_6e

    iget-object v3, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mActivity:Landroid/app/Activity;

    const v8, 0x60c01ba

    invoke-virtual {v3, v8}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_5c
    aput-object v3, v7, v4

    invoke-virtual {v5, v6, v7}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .local v2, tips:Ljava/lang/String;
    iget-object v3, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mAdapter:Lmiui/app/resourcebrowser/ResourceAdapter;

    iget-object v3, v3, Lmiui/app/resourcebrowser/ResourceAdapter;->mContext:Landroid/app/Activity;

    invoke-static {v3, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_a

    .end local v2           #tips:Ljava/lang/String;
    :cond_6e
    iget-object v3, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mActivity:Landroid/app/Activity;

    const v8, 0x60c01bb

    invoke-virtual {v3, v8}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_5c

    .end local v1           #r:Lmiui/app/resourcebrowser/resource/Resource;
    :cond_78
    iget-object v3, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mFragment:Lmiui/app/resourcebrowser/ResourceListFragment;

    if-eqz v3, :cond_82

    iget-object v3, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mFragment:Lmiui/app/resourcebrowser/ResourceListFragment;

    invoke-virtual {v3, v0}, Lmiui/app/resourcebrowser/ResourceListFragment;->startDetailActivityForResource(Landroid/util/Pair;)V

    goto :goto_a

    :cond_82
    iget-object v3, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mActivity:Landroid/app/Activity;

    check-cast v3, Lmiui/app/resourcebrowser/ResourceListActivity;

    invoke-virtual {v3, v0}, Lmiui/app/resourcebrowser/ResourceListActivity;->startDetailActivityForResource(Landroid/util/Pair;)V

    goto :goto_a
.end method

.method protected onLongClick_Impl(Landroid/view/View;)Z
    .registers 7
    .parameter "v"

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    .local v1, position:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    if-nez v1, :cond_b

    :cond_a
    :goto_a
    return v2

    :cond_b
    iget-boolean v4, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mLocalResourcePage:Z

    if-eqz v4, :cond_2f

    iget-object v4, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mAdapter:Lmiui/app/resourcebrowser/ResourceAdapter;

    invoke-virtual {v4}, Lmiui/app/resourcebrowser/ResourceAdapter;->loadingData()Z

    move-result v4

    if-nez v4, :cond_2d

    move v0, v3

    .local v0, allowLongClick:Z
    :goto_18
    if-eqz v0, :cond_a

    iget-boolean v4, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mEditableMode:Z

    if-nez v4, :cond_a

    invoke-virtual {p0, v1}, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->getResource(Landroid/util/Pair;)Lmiui/app/resourcebrowser/resource/Resource;

    move-result-object v4

    invoke-virtual {p0, v4}, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->canSelected(Lmiui/app/resourcebrowser/resource/Resource;)Z

    move-result v4

    if-eqz v4, :cond_a

    invoke-virtual {p0, p1, v1}, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->enterEditMode(Landroid/view/View;Landroid/util/Pair;)V

    move v2, v3

    goto :goto_a

    .end local v0           #allowLongClick:Z
    :cond_2d
    move v0, v2

    goto :goto_18

    :cond_2f
    move v0, v3

    goto :goto_18
.end method

.method public quitEditMode()V
    .registers 2

    .prologue
    iget-boolean v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mEditableMode:Z

    if-eqz v0, :cond_19

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mEditableMode:Z

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mActionMode:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mActionMode:Landroid/view/ActionMode;

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mCheckedResource:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mAdapter:Lmiui/app/resourcebrowser/ResourceAdapter;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/ResourceAdapter;->notifyDataSetChanged()V

    :cond_19
    return-void
.end method

.method protected setDownloadProgress(Landroid/widget/ProgressBar;Lmiui/app/resourcebrowser/resource/Resource;)V
    .registers 10
    .parameter "progress"
    .parameter "r"

    .prologue
    const/4 v2, 0x0

    if-eqz p1, :cond_35

    iget-object v3, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mDownloadBytes:Ljava/util/HashMap;

    invoke-virtual {p2}, Lmiui/app/resourcebrowser/resource/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .local v0, downloadBytes:Ljava/lang/Integer;
    if-eqz v0, :cond_36

    invoke-virtual {p1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    const/16 v1, 0x64

    .local v1, max:I
    const/16 v3, 0x64

    invoke-virtual {p1, v3}, Landroid/widget/ProgressBar;->setMax(I)V

    invoke-virtual {p2}, Lmiui/app/resourcebrowser/resource/Resource;->getFileSize()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-lez v3, :cond_32

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    mul-int/lit8 v3, v3, 0x64

    int-to-long v3, v3

    invoke-virtual {p2}, Lmiui/app/resourcebrowser/resource/Resource;->getFileSize()J

    move-result-wide v5

    div-long/2addr v3, v5

    long-to-int v2, v3

    .local v2, step:I
    :cond_32
    invoke-virtual {p1, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    .end local v0           #downloadBytes:Ljava/lang/Integer;
    .end local v1           #max:I
    .end local v2           #step:I
    :cond_35
    :goto_35
    return-void

    .restart local v0       #downloadBytes:Ljava/lang/Integer;
    :cond_36
    const/16 v3, 0x8

    invoke-virtual {p1, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_35
.end method

.method protected updateSelectedTitle()V
    .registers 7

    .prologue
    iget-object v1, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mActivity:Landroid/app/Activity;

    const v2, 0x60c01b6

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mCheckedResource:Ljava/util/HashSet;

    invoke-virtual {v5}, Ljava/util/HashSet;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .local v0, title:Ljava/lang/String;
    iget-object v1, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mSelectTv:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public viewStateChanged(Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;)V
    .registers 3
    .parameter "state"

    .prologue
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->quitEditMode()V

    sget-object v0, Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;->DESTYOY:Lmiui/app/resourcebrowser/view/BatchResourceHandler$ViewState;

    if-ne p1, v0, :cond_10

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mDownloadHandler:Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->mDownloadHandler:Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/util/ResourceDownloadHandler;->unregisterDownloadReceiver()V

    :cond_10
    return-void
.end method
