.class public Lmiui/app/resourcebrowser/view/ResourceOperationView;
.super Landroid/widget/LinearLayout;
.source "ResourceOperationView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/app/resourcebrowser/view/ResourceOperationView$UIParameter;
    }
.end annotation


# instance fields
.field protected mActivity:Landroid/app/Activity;

.field protected mApplyBtn:Landroid/widget/TextView;

.field protected mDeleteBtn:Landroid/widget/ImageView;

.field protected mDownloadBtn:Landroid/widget/TextView;

.field protected mDownloadProgress:Landroid/widget/ProgressBar;

.field protected mHandler:Landroid/os/Handler;

.field protected mMagicBtn:Landroid/widget/ImageView;

.field protected mOperationHandler:Lmiui/app/resourcebrowser/view/ResourceOperationHandler;

.field protected mUIparameter:Lmiui/app/resourcebrowser/view/ResourceOperationView$UIParameter;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lmiui/app/resourcebrowser/view/ResourceOperationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    .prologue
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lmiui/app/resourcebrowser/view/ResourceOperationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lmiui/app/resourcebrowser/view/ResourceOperationView;Landroid/view/View;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    invoke-direct {p0, p1}, Lmiui/app/resourcebrowser/view/ResourceOperationView;->disableAndDelayEnableView(Landroid/view/View;)V

    return-void
.end method

.method private disableAndDelayEnableView(Landroid/view/View;)V
    .registers 6
    .parameter "v"

    .prologue
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mHandler:Landroid/os/Handler;

    new-instance v1, Lmiui/app/resourcebrowser/view/ResourceOperationView$1;

    invoke-direct {v1, p0, p1}, Lmiui/app/resourcebrowser/view/ResourceOperationView$1;-><init>(Lmiui/app/resourcebrowser/view/ResourceOperationView;Landroid/view/View;)V

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private getDefaultUIParameter()Lmiui/app/resourcebrowser/view/ResourceOperationView$UIParameter;
    .registers 3

    .prologue
    new-instance v0, Lmiui/app/resourcebrowser/view/ResourceOperationView$UIParameter;

    invoke-direct {v0}, Lmiui/app/resourcebrowser/view/ResourceOperationView$UIParameter;-><init>()V

    .local v0, para:Lmiui/app/resourcebrowser/view/ResourceOperationView$UIParameter;
    const v1, 0x60201b7

    iput v1, v0, Lmiui/app/resourcebrowser/view/ResourceOperationView$UIParameter;->operationViewBgId:I

    const v1, 0x60201a9

    iput v1, v0, Lmiui/app/resourcebrowser/view/ResourceOperationView$UIParameter;->deleteBtnSrcId:I

    return-object v0
.end method


# virtual methods
.method public init(Lmiui/app/resourcebrowser/view/ResourceOperationHandler;Lmiui/app/resourcebrowser/view/ResourceOperationView$UIParameter;)V
    .registers 5
    .parameter "operationHandler"
    .parameter "uiParameter"

    .prologue
    if-nez p1, :cond_a

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Operated handler can not be null."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_a
    iput-object p2, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mUIparameter:Lmiui/app/resourcebrowser/view/ResourceOperationView$UIParameter;

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mUIparameter:Lmiui/app/resourcebrowser/view/ResourceOperationView$UIParameter;

    if-nez v0, :cond_16

    invoke-direct {p0}, Lmiui/app/resourcebrowser/view/ResourceOperationView;->getDefaultUIParameter()Lmiui/app/resourcebrowser/view/ResourceOperationView$UIParameter;

    move-result-object v0

    iput-object v0, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mUIparameter:Lmiui/app/resourcebrowser/view/ResourceOperationView$UIParameter;

    :cond_16
    iget-object v0, p1, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->mActivity:Landroid/app/Activity;

    iput-object v0, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mActivity:Landroid/app/Activity;

    iput-object p1, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mOperationHandler:Lmiui/app/resourcebrowser/view/ResourceOperationHandler;

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/view/ResourceOperationView;->setupUI()V

    return-void
.end method

.method protected setApplyOrPickStatus()V
    .registers 4

    .prologue
    const/4 v0, 0x0

    .local v0, title:Ljava/lang/String;
    iget-object v1, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mOperationHandler:Lmiui/app/resourcebrowser/view/ResourceOperationHandler;

    invoke-virtual {v1}, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->isLocalResource()Z

    move-result v1

    if-eqz v1, :cond_22

    iget-object v1, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mOperationHandler:Lmiui/app/resourcebrowser/view/ResourceOperationHandler;

    invoke-virtual {v1}, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->isDownloading()Z

    move-result v1

    if-nez v1, :cond_22

    iget-object v1, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mOperationHandler:Lmiui/app/resourcebrowser/view/ResourceOperationHandler;

    invoke-virtual {v1}, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->isPicker()Z

    move-result v1

    if-eqz v1, :cond_30

    iget-object v1, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mContext:Landroid/content/Context;

    const v2, 0x60c001b

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_22
    :goto_22
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3a

    iget-object v1, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mApplyBtn:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_2f
    return-void

    :cond_30
    iget-object v1, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mContext:Landroid/content/Context;

    const v2, 0x60c0019

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_22

    :cond_3a
    iget-object v1, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mApplyBtn:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mApplyBtn:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2f
.end method

.method protected setDeleteStatus()V
    .registers 3

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mDeleteBtn:Landroid/widget/ImageView;

    iget-object v1, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mUIparameter:Lmiui/app/resourcebrowser/view/ResourceOperationView$UIParameter;

    iget v1, v1, Lmiui/app/resourcebrowser/view/ResourceOperationView$UIParameter;->deleteBtnBgId:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mDeleteBtn:Landroid/widget/ImageView;

    iget-object v1, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mUIparameter:Lmiui/app/resourcebrowser/view/ResourceOperationView$UIParameter;

    iget v1, v1, Lmiui/app/resourcebrowser/view/ResourceOperationView$UIParameter;->deleteBtnSrcId:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v1, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mDeleteBtn:Landroid/widget/ImageView;

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mOperationHandler:Lmiui/app/resourcebrowser/view/ResourceOperationHandler;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->isDeletable()Z

    move-result v0

    if-eqz v0, :cond_29

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mOperationHandler:Lmiui/app/resourcebrowser/view/ResourceOperationHandler;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->isDownloading()Z

    move-result v0

    if-nez v0, :cond_29

    const/4 v0, 0x0

    :goto_25
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void

    :cond_29
    const/4 v0, 0x4

    goto :goto_25
.end method

.method protected setDownloadOrUpdateStatus()V
    .registers 5

    .prologue
    const/4 v0, 0x1

    .local v0, enabled:Z
    const/4 v1, 0x0

    .local v1, title:Ljava/lang/String;
    iget-object v2, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mOperationHandler:Lmiui/app/resourcebrowser/view/ResourceOperationHandler;

    invoke-virtual {v2}, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->isDownloading()Z

    move-result v2

    if-eqz v2, :cond_22

    iget-object v2, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mContext:Landroid/content/Context;

    const v3, 0x60c0018

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x0

    :cond_14
    :goto_14
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_46

    iget-object v2, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mDownloadBtn:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_21
    return-void

    :cond_22
    iget-object v2, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mOperationHandler:Lmiui/app/resourcebrowser/view/ResourceOperationHandler;

    invoke-virtual {v2}, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->isOldVersionResource()Z

    move-result v2

    if-eqz v2, :cond_34

    iget-object v2, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mContext:Landroid/content/Context;

    const v3, 0x60c001a

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_14

    :cond_34
    iget-object v2, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mOperationHandler:Lmiui/app/resourcebrowser/view/ResourceOperationHandler;

    invoke-virtual {v2}, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->isLocalResource()Z

    move-result v2

    if-nez v2, :cond_14

    iget-object v2, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mContext:Landroid/content/Context;

    const v3, 0x60c0017

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_14

    :cond_46
    iget-object v2, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mDownloadBtn:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v2, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mDownloadBtn:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v2, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mDownloadBtn:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_21
.end method

.method protected setDownloadProgressOrPreviewNavigationStatus()V
    .registers 3

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mOperationHandler:Lmiui/app/resourcebrowser/view/ResourceOperationHandler;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/view/ResourceOperationHandler;->isDownloading()Z

    move-result v0

    if-eqz v0, :cond_f

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mDownloadProgress:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :goto_e
    return-void

    :cond_f
    iget-object v0, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mDownloadProgress:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_e
.end method

.method protected setMagicStatus()V
    .registers 3

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mUIparameter:Lmiui/app/resourcebrowser/view/ResourceOperationView$UIParameter;

    iget v0, v0, Lmiui/app/resourcebrowser/view/ResourceOperationView$UIParameter;->magicBtnBgId:I

    if-nez v0, :cond_c

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mUIparameter:Lmiui/app/resourcebrowser/view/ResourceOperationView$UIParameter;

    iget v0, v0, Lmiui/app/resourcebrowser/view/ResourceOperationView$UIParameter;->magicBtnSrcId:I

    if-eqz v0, :cond_25

    :cond_c
    iget-object v0, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mMagicBtn:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mMagicBtn:Landroid/widget/ImageView;

    iget-object v1, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mUIparameter:Lmiui/app/resourcebrowser/view/ResourceOperationView$UIParameter;

    iget v1, v1, Lmiui/app/resourcebrowser/view/ResourceOperationView$UIParameter;->magicBtnBgId:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mMagicBtn:Landroid/widget/ImageView;

    iget-object v1, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mUIparameter:Lmiui/app/resourcebrowser/view/ResourceOperationView$UIParameter;

    iget v1, v1, Lmiui/app/resourcebrowser/view/ResourceOperationView$UIParameter;->magicBtnSrcId:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_24
    return-void

    :cond_25
    iget-object v0, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mMagicBtn:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_24
.end method

.method protected setResourceStatus()V
    .registers 1

    .prologue
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/view/ResourceOperationView;->setApplyOrPickStatus()V

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/view/ResourceOperationView;->setDownloadOrUpdateStatus()V

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/view/ResourceOperationView;->setDeleteStatus()V

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/view/ResourceOperationView;->setMagicStatus()V

    return-void
.end method

.method protected setupUI()V
    .registers 3

    .prologue
    const v0, 0x60b006c

    invoke-virtual {p0, v0}, Lmiui/app/resourcebrowser/view/ResourceOperationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mUIparameter:Lmiui/app/resourcebrowser/view/ResourceOperationView$UIParameter;

    iget v1, v1, Lmiui/app/resourcebrowser/view/ResourceOperationView$UIParameter;->operationViewBgId:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    const v0, 0x60b005a

    invoke-virtual {p0, v0}, Lmiui/app/resourcebrowser/view/ResourceOperationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mDownloadBtn:Landroid/widget/TextView;

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mDownloadBtn:Landroid/widget/TextView;

    new-instance v1, Lmiui/app/resourcebrowser/view/ResourceOperationView$2;

    invoke-direct {v1, p0}, Lmiui/app/resourcebrowser/view/ResourceOperationView$2;-><init>(Lmiui/app/resourcebrowser/view/ResourceOperationView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x60b005b

    invoke-virtual {p0, v0}, Lmiui/app/resourcebrowser/view/ResourceOperationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mApplyBtn:Landroid/widget/TextView;

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mApplyBtn:Landroid/widget/TextView;

    new-instance v1, Lmiui/app/resourcebrowser/view/ResourceOperationView$3;

    invoke-direct {v1, p0}, Lmiui/app/resourcebrowser/view/ResourceOperationView$3;-><init>(Lmiui/app/resourcebrowser/view/ResourceOperationView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x60b005c

    invoke-virtual {p0, v0}, Lmiui/app/resourcebrowser/view/ResourceOperationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mDeleteBtn:Landroid/widget/ImageView;

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mDeleteBtn:Landroid/widget/ImageView;

    new-instance v1, Lmiui/app/resourcebrowser/view/ResourceOperationView$4;

    invoke-direct {v1, p0}, Lmiui/app/resourcebrowser/view/ResourceOperationView$4;-><init>(Lmiui/app/resourcebrowser/view/ResourceOperationView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x60b005d

    invoke-virtual {p0, v0}, Lmiui/app/resourcebrowser/view/ResourceOperationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mMagicBtn:Landroid/widget/ImageView;

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mMagicBtn:Landroid/widget/ImageView;

    new-instance v1, Lmiui/app/resourcebrowser/view/ResourceOperationView$5;

    invoke-direct {v1, p0}, Lmiui/app/resourcebrowser/view/ResourceOperationView$5;-><init>(Lmiui/app/resourcebrowser/view/ResourceOperationView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x60b004d

    invoke-virtual {p0, v0}, Lmiui/app/resourcebrowser/view/ResourceOperationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mDownloadProgress:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mDownloadProgress:Landroid/widget/ProgressBar;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mDownloadProgress:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/view/ResourceOperationView;->setResourceStatus()V

    return-void
.end method

.method protected updateDownloadProgressBar(II)V
    .registers 8
    .parameter "downloadBytes"
    .parameter "totalBytes"

    .prologue
    int-to-double v1, p1

    const-wide/high16 v3, 0x4059

    mul-double/2addr v1, v3

    int-to-double v3, p2

    div-double/2addr v1, v3

    double-to-int v0, v1

    .local v0, progress:I
    iget-object v1, p0, Lmiui/app/resourcebrowser/view/ResourceOperationView;->mDownloadProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    return-void
.end method
