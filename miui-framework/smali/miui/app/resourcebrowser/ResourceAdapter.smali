.class public abstract Lmiui/app/resourcebrowser/ResourceAdapter;
.super Lmiui/widget/AsyncImageAdapter;
.source "ResourceAdapter.java"

# interfaces
.implements Lmiui/app/resourcebrowser/IntentConstants;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmiui/widget/AsyncImageAdapter",
        "<",
        "Lmiui/app/resourcebrowser/resource/Resource;",
        ">;",
        "Lmiui/app/resourcebrowser/IntentConstants;"
    }
.end annotation


# static fields
.field protected static final mRootDataParams:Landroid/widget/LinearLayout$LayoutParams;


# instance fields
.field private mBatchHandler:Lmiui/app/resourcebrowser/view/BatchResourceHandler;

.field public mContext:Landroid/app/Activity;

.field protected mCurrentUsingPath:Ljava/lang/String;

.field private mDecodeImageLowQuality:Z

.field public mDisplayType:I

.field public mFragment:Lmiui/app/resourcebrowser/BaseFragment;

.field protected mInflater:Landroid/view/LayoutInflater;

.field private mItemHorizontalSpacing:I

.field private mItemVerticalSpaceing:I

.field protected mMetaData:Landroid/os/Bundle;

.field private mMusicPlayer:Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;

.field protected mResourceSet:Lmiui/app/resourcebrowser/resource/ResourceSet;

.field protected mResourceSetCategory:I

.field protected mResourceSetCode:Ljava/lang/String;

.field protected mResourceSetPackage:Ljava/lang/String;

.field protected mResourceSetSubpackage:Ljava/lang/String;

.field private mThumbnailHeight:I

.field private mThumbnailWidth:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    sput-object v0, Lmiui/app/resourcebrowser/ResourceAdapter;->mRootDataParams:Landroid/widget/LinearLayout$LayoutParams;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Bundle;)V
    .registers 4
    .parameter "context"
    .parameter "metaData"

    .prologue
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2}, Lmiui/app/resourcebrowser/ResourceAdapter;-><init>(Lmiui/app/resourcebrowser/BaseFragment;Landroid/content/Context;Landroid/os/Bundle;)V

    return-void
.end method

.method private constructor <init>(Lmiui/app/resourcebrowser/BaseFragment;Landroid/content/Context;Landroid/os/Bundle;)V
    .registers 6
    .parameter "fragment"
    .parameter "context"
    .parameter "metaData"

    .prologue
    const/4 v0, -0x1

    invoke-direct {p0}, Lmiui/widget/AsyncImageAdapter;-><init>()V

    iput v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mItemHorizontalSpacing:I

    iput v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mItemVerticalSpaceing:I

    iput-object p1, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mFragment:Lmiui/app/resourcebrowser/BaseFragment;

    check-cast p2, Landroid/app/Activity;

    .end local p2
    iput-object p2, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mContext:Landroid/app/Activity;

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mFragment:Lmiui/app/resourcebrowser/BaseFragment;

    if-nez v0, :cond_1e

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mContext:Landroid/app/Activity;

    if-nez v0, :cond_1e

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "invalid parameters: fragment and activity can not both be null."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1e
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mContext:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x609000f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mDecodeImageLowQuality:Z

    iput-object p3, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mMetaData:Landroid/os/Bundle;

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mMetaData:Landroid/os/Bundle;

    const-string v1, "miui.app.resourcebrowser.RESOURCE_SET_CODE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mResourceSetCode:Ljava/lang/String;

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mMetaData:Landroid/os/Bundle;

    const-string v1, "miui.app.resourcebrowser.RESOURCE_SET_CATEGORY"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mResourceSetCategory:I

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceAdapter;->refreshDataSet()V

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mMetaData:Landroid/os/Bundle;

    const-string v1, "miui.app.resourcebrowser.DISPLAY_TYPE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mDisplayType:I

    iget v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mDisplayType:I

    invoke-static {v0}, Lmiui/app/resourcebrowser/util/ResourceHelper;->getDataPerLine(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lmiui/app/resourcebrowser/ResourceAdapter;->setDataPerLine(I)V

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lmiui/app/resourcebrowser/ResourceAdapter;->setAutoLoadMoreStyle(I)V

    const/16 v0, 0x1e

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceAdapter;->getDataPerLine()I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    div-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lmiui/app/resourcebrowser/ResourceAdapter;->setPreloadOffset(I)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mContext:Landroid/app/Activity;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mInflater:Landroid/view/LayoutInflater;

    invoke-direct {p0}, Lmiui/app/resourcebrowser/ResourceAdapter;->resolveThumbnailSize()V

    return-void
.end method

.method public constructor <init>(Lmiui/app/resourcebrowser/BaseFragment;Landroid/os/Bundle;)V
    .registers 4
    .parameter "fragment"
    .parameter "metaData"

    .prologue
    invoke-virtual {p1}, Lmiui/app/resourcebrowser/BaseFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lmiui/app/resourcebrowser/ResourceAdapter;-><init>(Lmiui/app/resourcebrowser/BaseFragment;Landroid/content/Context;Landroid/os/Bundle;)V

    return-void
.end method

.method private bindText(Landroid/view/View;ILmiui/app/resourcebrowser/resource/Resource;Ljava/lang/String;)V
    .registers 13
    .parameter "view"
    .parameter "id"
    .parameter "resourceItem"
    .parameter "key"

    .prologue
    const/4 v7, 0x0

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .local v2, textView:Landroid/widget/TextView;
    if-eqz v2, :cond_71

    invoke-virtual {p3}, Lmiui/app/resourcebrowser/resource/Resource;->getInformation()Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v4, p4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setVisibility(I)V

    sget-boolean v4, Lmiui/app/resourcebrowser/util/ResourceDebug;->DEBUG:Z

    if-eqz v4, :cond_71

    invoke-virtual {p3, v7}, Lmiui/app/resourcebrowser/resource/Resource;->getLocalThumbnail(I)Ljava/lang/String;

    move-result-object v3

    .local v3, thumbnailPath:Ljava/lang/String;
    if-eqz v3, :cond_71

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .local v0, f:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_71

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v5

    invoke-static {v5, v6}, Lmiui/app/resourcebrowser/util/ResourceHelper;->getFormattedSize(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .local v1, text:Ljava/lang/String;
    sget-object v4, Lmiui/app/resourcebrowser/util/ResourceDebug;->sMap:Ljava/util/HashMap;

    invoke-virtual {p3, v7}, Lmiui/app/resourcebrowser/resource/Resource;->getOnlineThumbnail(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_72

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lmiui/app/resourcebrowser/util/ResourceDebug;->sMap:Ljava/util/HashMap;

    invoke-virtual {p3, v7}, Lmiui/app/resourcebrowser/resource/Resource;->getOnlineThumbnail(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_6e
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .end local v0           #f:Ljava/io/File;
    .end local v1           #text:Ljava/lang/String;
    .end local v3           #thumbnailPath:Ljava/lang/String;
    :cond_71
    return-void

    .restart local v0       #f:Ljava/io/File;
    .restart local v1       #text:Ljava/lang/String;
    .restart local v3       #thumbnailPath:Ljava/lang/String;
    :cond_72
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_6e
.end method

.method private resolveThumbnailSize()V
    .registers 8

    .prologue
    iget-object v4, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mContext:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x602018e

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .local v0, bgItem:Landroid/graphics/drawable/Drawable;
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .local v2, rect:Landroid/graphics/Rect;
    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    iget v4, v2, Landroid/graphics/Rect;->left:I

    iget v5, v2, Landroid/graphics/Rect;->right:I

    add-int v1, v4, v5

    .local v1, horizontalPadding:I
    iget-object v4, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mContext:Landroid/app/Activity;

    iget v5, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mDisplayType:I

    invoke-static {v4, v5, v1}, Lmiui/app/resourcebrowser/util/ResourceHelper;->getThumbnailSize(Landroid/app/Activity;II)Landroid/util/Pair;

    move-result-object v3

    .local v3, size:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    iget-object v4, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iput v4, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mThumbnailWidth:I

    iget-object v4, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iput v4, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mThumbnailHeight:I

    iget-object v4, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mMetaData:Landroid/os/Bundle;

    const-string v5, "miui.app.resourcebrowser.THUMBNAIL_WIDTH"

    iget v6, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mThumbnailWidth:I

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v4, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mContext:Landroid/app/Activity;

    invoke-static {v4}, Lmiui/app/resourcebrowser/util/ResourceHelper;->getThumbnailGap(Landroid/content/Context;)I

    move-result v4

    iput v4, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mItemHorizontalSpacing:I

    iget v4, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mItemHorizontalSpacing:I

    iput v4, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mItemVerticalSpaceing:I

    return-void
.end method

.method private setListItemViewBackground(Landroid/view/View;II)V
    .registers 8
    .parameter "view"
    .parameter "groupPos"
    .parameter "group"

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .local v1, resId:I
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceAdapter;->getDataPerLine()I

    move-result v2

    if-ne v2, v3, :cond_3a

    invoke-virtual {p0, p3}, Lmiui/app/resourcebrowser/ResourceAdapter;->getCount(I)I

    move-result v0

    .local v0, dataCnt:I
    if-ne v0, v3, :cond_15

    const v1, 0x602018d

    .end local v0           #dataCnt:I
    :goto_11
    invoke-virtual {p1, v1}, Landroid/view/View;->setBackgroundResource(I)V

    return-void

    .restart local v0       #dataCnt:I
    :cond_15
    add-int/lit8 v2, v0, -0x1

    if-eq p2, v2, :cond_2c

    add-int/lit8 v2, p2, 0x1

    invoke-virtual {p0, v2, p3}, Lmiui/app/resourcebrowser/ResourceAdapter;->getItem(II)Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/app/resourcebrowser/resource/Resource;

    invoke-virtual {v2}, Lmiui/app/resourcebrowser/resource/Resource;->getDividerTitle()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_30

    :cond_2c
    const v1, 0x6020189

    goto :goto_11

    :cond_30
    if-nez p2, :cond_36

    const v1, 0x6020181

    goto :goto_11

    :cond_36
    const v1, 0x6020185

    goto :goto_11

    .end local v0           #dataCnt:I
    :cond_3a
    const v1, 0x602018e

    goto :goto_11
.end method

.method private setResourceFlag(Landroid/view/View;Lmiui/app/resourcebrowser/resource/Resource;)V
    .registers 5
    .parameter "view"
    .parameter "resourceItem"

    .prologue
    if-eqz p2, :cond_3d

    const v0, 0x60b004c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x60b004b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {p0, p2}, Lmiui/app/resourcebrowser/ResourceAdapter;->getTopFlagId(Lmiui/app/resourcebrowser/resource/Resource;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    const v0, 0x60b0063

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {p0, p2}, Lmiui/app/resourcebrowser/ResourceAdapter;->getCenterFlagId(Lmiui/app/resourcebrowser/resource/Resource;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    const v0, 0x60b0066

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {p0, p2}, Lmiui/app/resourcebrowser/ResourceAdapter;->getBottomFlagId(Lmiui/app/resourcebrowser/resource/Resource;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_3d
    return-void
.end method

.method private setThumbnail(Landroid/widget/ImageView;Lmiui/app/resourcebrowser/resource/Resource;ILjava/util/List;Z)V
    .registers 9
    .parameter "view"
    .parameter "data"
    .parameter "index"
    .parameter
    .parameter "showEmpty"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/ImageView;",
            "Lmiui/app/resourcebrowser/resource/Resource;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .local p4, partialData:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Object;>;"
    if-nez p1, :cond_3

    :goto_2
    return-void

    :cond_3
    iget v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mResourceSetCategory:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_20

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mBatchHandler:Lmiui/app/resourcebrowser/view/BatchResourceHandler;

    if-nez v0, :cond_20

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mMusicPlayer:Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;

    if-nez v0, :cond_1a

    new-instance v0, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;

    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mContext:Landroid/app/Activity;

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;-><init>(Landroid/app/Activity;Z)V

    iput-object v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mMusicPlayer:Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;

    :cond_1a
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mMusicPlayer:Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;

    invoke-virtual {v0, p1, p2}, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->initPlayButtonIfNeed(Landroid/widget/ImageView;Lmiui/app/resourcebrowser/resource/Resource;)V

    goto :goto_2

    :cond_20
    if-eqz p4, :cond_28

    invoke-interface {p4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2d

    :cond_28
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_2

    :cond_2d
    invoke-interface {p4, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_2
.end method


# virtual methods
.method protected bindContentView(Landroid/view/View;Ljava/util/List;III)Landroid/view/View;
    .registers 20
    .parameter "view"
    .parameter
    .parameter "position"
    .parameter "groupPos"
    .parameter "group"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/List",
            "<",
            "Lmiui/app/resourcebrowser/resource/Resource;",
            ">;III)",
            "Landroid/view/View;"
        }
    .end annotation

    .prologue
    .local p2, data:Ljava/util/List;,"Ljava/util/List<Lmiui/app/resourcebrowser/resource/Resource;>;"
    move-object v8, p1

    check-cast v8, Landroid/widget/LinearLayout;

    .local v8, root:Landroid/widget/LinearLayout;
    if-nez v8, :cond_6d

    new-instance v8, Landroid/widget/LinearLayout;

    .end local v8           #root:Landroid/widget/LinearLayout;
    iget-object v10, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mContext:Landroid/app/Activity;

    invoke-direct {v8, v10}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .restart local v8       #root:Landroid/widget/LinearLayout;
    const/4 v10, 0x1

    invoke-virtual {v8, v10}, Landroid/widget/LinearLayout;->setOrientation(I)V

    new-instance v9, Landroid/widget/TextView;

    iget-object v10, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mContext:Landroid/app/Activity;

    const/4 v11, 0x0

    const v12, 0x1010208

    invoke-direct {v9, v10, v11, v12}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .local v9, tv:Landroid/widget/TextView;
    const v10, 0x6020197

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setBackgroundResource(I)V

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setVisibility(I)V

    sget-object v10, Lmiui/app/resourcebrowser/ResourceAdapter;->mRootDataParams:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v8, v9, v10}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v2, Landroid/widget/LinearLayout;

    iget-object v10, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mContext:Landroid/app/Activity;

    invoke-direct {v2, v10}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .local v2, dataLayout:Landroid/widget/LinearLayout;
    const/4 v10, 0x0

    invoke-virtual {v2, v10}, Landroid/widget/LinearLayout;->setOrientation(I)V

    const/16 v10, 0x11

    invoke-virtual {v2, v10}, Landroid/widget/LinearLayout;->setGravity(I)V

    sget-object v10, Lmiui/app/resourcebrowser/ResourceAdapter;->mRootDataParams:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v8, v2, v10}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceAdapter;->getDataPerLine()I

    move-result v4

    .local v4, horizontalTotalItem:I
    const/4 v5, 0x0

    .local v5, i:I
    :goto_45
    if-ge v5, v4, :cond_5e

    iget-object v10, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mInflater:Landroid/view/LayoutInflater;

    iget v11, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mDisplayType:I

    invoke-static {v11}, Lmiui/app/resourcebrowser/util/ResourceHelper;->getThumbnailViewResource(I)I

    move-result v11

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .local v6, itemView:Landroid/view/View;
    invoke-virtual {p0, v6, v5, v4}, Lmiui/app/resourcebrowser/ResourceAdapter;->getItemViewLayoutParams(Landroid/view/View;II)Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v10

    invoke-virtual {v2, v6, v10}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_45

    .end local v6           #itemView:Landroid/view/View;
    :cond_5e
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceAdapter;->getDataPerLine()I

    move-result v10

    const/4 v11, 0x1

    if-gt v10, v11, :cond_ee

    const/4 v10, 0x1

    :goto_66
    invoke-virtual {v2, v10}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    const/4 v10, 0x1

    invoke-virtual {v8, v10}, Landroid/widget/LinearLayout;->setDrawingCacheEnabled(Z)V

    .end local v2           #dataLayout:Landroid/widget/LinearLayout;
    .end local v4           #horizontalTotalItem:I
    .end local v5           #i:I
    .end local v9           #tv:Landroid/widget/TextView;
    :cond_6d
    const/4 v10, 0x0

    invoke-virtual {v8, v10}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .local v3, dividerText:Landroid/widget/TextView;
    const/16 v10, 0x8

    invoke-virtual {v3, v10}, Landroid/widget/TextView;->setVisibility(I)V

    const/4 v10, 0x1

    invoke-virtual {v8, v10}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .restart local v2       #dataLayout:Landroid/widget/LinearLayout;
    move/from16 v0, p4

    move/from16 v1, p5

    invoke-direct {p0, v2, v0, v1}, Lmiui/app/resourcebrowser/ResourceAdapter;->setListItemViewBackground(Landroid/view/View;II)V

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceAdapter;->getDataPerLine()I

    move-result v10

    const/4 v11, 0x1

    if-ne v10, v11, :cond_b1

    new-instance v10, Landroid/util/Pair;

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceAdapter;->getDataPerLine()I

    move-result v11

    mul-int v11, v11, p4

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-direct {v10, v11, v12}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v2, v10}, Landroid/widget/LinearLayout;->setTag(Ljava/lang/Object;)V

    iget-object v10, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mBatchHandler:Lmiui/app/resourcebrowser/view/BatchResourceHandler;

    if-eqz v10, :cond_b1

    iget-object v10, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mBatchHandler:Lmiui/app/resourcebrowser/view/BatchResourceHandler;

    invoke-virtual {v10}, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->getResourceClickListener()Landroid/view/View$OnClickListener;

    move-result-object v10

    invoke-virtual {v2, v10}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_b1
    const/4 v5, 0x0

    .restart local v5       #i:I
    :goto_b2
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceAdapter;->getDataPerLine()I

    move-result v10

    if-ge v5, v10, :cond_f6

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .restart local v6       #itemView:Landroid/view/View;
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v10

    if-ge v5, v10, :cond_f1

    const/4 v10, 0x0

    invoke-virtual {v6, v10}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p2

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lmiui/app/resourcebrowser/resource/Resource;

    .local v7, res:Lmiui/app/resourcebrowser/resource/Resource;
    invoke-virtual {v7}, Lmiui/app/resourcebrowser/resource/Resource;->getDividerTitle()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_df

    const/4 v10, 0x0

    invoke-virtual {v3, v10}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v7}, Lmiui/app/resourcebrowser/resource/Resource;->getDividerTitle()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_df
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceAdapter;->getDataPerLine()I

    move-result v10

    mul-int v10, v10, p4

    add-int/2addr v10, v5

    move/from16 v0, p5

    invoke-virtual {p0, v6, v7, v10, v0}, Lmiui/app/resourcebrowser/ResourceAdapter;->bindView(Landroid/view/View;Lmiui/app/resourcebrowser/resource/Resource;II)V

    .end local v7           #res:Lmiui/app/resourcebrowser/resource/Resource;
    :goto_eb
    add-int/lit8 v5, v5, 0x1

    goto :goto_b2

    .end local v3           #dividerText:Landroid/widget/TextView;
    .end local v6           #itemView:Landroid/view/View;
    .restart local v4       #horizontalTotalItem:I
    .restart local v9       #tv:Landroid/widget/TextView;
    :cond_ee
    const/4 v10, 0x0

    goto/16 :goto_66

    .end local v4           #horizontalTotalItem:I
    .end local v9           #tv:Landroid/widget/TextView;
    .restart local v3       #dividerText:Landroid/widget/TextView;
    .restart local v6       #itemView:Landroid/view/View;
    :cond_f1
    const/4 v10, 0x4

    invoke-virtual {v6, v10}, Landroid/view/View;->setVisibility(I)V

    goto :goto_eb

    .end local v6           #itemView:Landroid/view/View;
    :cond_f6
    iget v10, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mDisplayType:I

    const/4 v11, 0x5

    if-ne v10, v11, :cond_113

    iget-object v10, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mBatchHandler:Lmiui/app/resourcebrowser/view/BatchResourceHandler;

    new-instance v11, Landroid/util/Pair;

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceAdapter;->getDataPerLine()I

    move-result v12

    mul-int v12, v12, p4

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-direct {v11, v12, v13}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v10, v2, v11}, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->initViewState(Landroid/view/View;Landroid/util/Pair;)V

    :cond_113
    return-object v8
.end method

.method protected bridge synthetic bindPartialContentView(Landroid/view/View;Ljava/lang/Object;ILjava/util/List;)V
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    .prologue
    check-cast p2, Lmiui/app/resourcebrowser/resource/Resource;

    .end local p2
    invoke-virtual {p0, p1, p2, p3, p4}, Lmiui/app/resourcebrowser/ResourceAdapter;->bindPartialContentView(Landroid/view/View;Lmiui/app/resourcebrowser/resource/Resource;ILjava/util/List;)V

    return-void
.end method

.method protected bindPartialContentView(Landroid/view/View;Lmiui/app/resourcebrowser/resource/Resource;ILjava/util/List;)V
    .registers 12
    .parameter "view"
    .parameter "data"
    .parameter "offset"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lmiui/app/resourcebrowser/resource/Resource;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p4, partialData:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Object;>;"
    iget v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mDisplayType:I

    invoke-static {v0}, Lmiui/app/resourcebrowser/util/ResourceHelper;->isMultipleView(I)Z

    move-result v0

    if-eqz v0, :cond_3c

    const v0, 0x60b0049

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    const/4 v3, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    move-object v2, p2

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lmiui/app/resourcebrowser/ResourceAdapter;->setThumbnail(Landroid/widget/ImageView;Lmiui/app/resourcebrowser/resource/Resource;ILjava/util/List;Z)V

    const v0, 0x60b0067

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    const/4 v3, 0x1

    const/4 v5, 0x0

    move-object v0, p0

    move-object v2, p2

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lmiui/app/resourcebrowser/ResourceAdapter;->setThumbnail(Landroid/widget/ImageView;Lmiui/app/resourcebrowser/resource/Resource;ILjava/util/List;Z)V

    const v0, 0x60b0068

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    const/4 v3, 0x2

    const/4 v5, 0x0

    move-object v0, p0

    move-object v2, p2

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lmiui/app/resourcebrowser/ResourceAdapter;->setThumbnail(Landroid/widget/ImageView;Lmiui/app/resourcebrowser/resource/Resource;ILjava/util/List;Z)V

    .end local p1
    :goto_3b
    return-void

    .restart local p1
    :cond_3c
    check-cast p1, Landroid/widget/LinearLayout;

    .end local p1
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .local v6, itemView:Landroid/view/View;
    check-cast v6, Landroid/widget/LinearLayout;

    .end local v6           #itemView:Landroid/view/View;
    invoke-virtual {v6, p3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .restart local v6       #itemView:Landroid/view/View;
    const v0, 0x60b0048

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    const/4 v3, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    move-object v2, p2

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lmiui/app/resourcebrowser/ResourceAdapter;->setThumbnail(Landroid/widget/ImageView;Lmiui/app/resourcebrowser/resource/Resource;ILjava/util/List;Z)V

    goto :goto_3b
.end method

.method protected bindView(Landroid/view/View;Lmiui/app/resourcebrowser/resource/Resource;II)V
    .registers 12
    .parameter "view"
    .parameter "resourceItem"
    .parameter "groupIndex"
    .parameter "group"

    .prologue
    const/4 v5, 0x4

    const v6, 0x60b0041

    iget v3, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mDisplayType:I

    const/4 v4, 0x5

    if-ne v3, v4, :cond_1e

    invoke-direct {p0, p1, p2}, Lmiui/app/resourcebrowser/ResourceAdapter;->setResourceFlag(Landroid/view/View;Lmiui/app/resourcebrowser/resource/Resource;)V

    const-string v3, "NAME"

    invoke-direct {p0, p1, v6, p2, v3}, Lmiui/app/resourcebrowser/ResourceAdapter;->bindText(Landroid/view/View;ILmiui/app/resourcebrowser/resource/Resource;Ljava/lang/String;)V

    const v3, 0x60b004a

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .local v2, tv:Landroid/widget/TextView;
    invoke-virtual {p0, v2, p2}, Lmiui/app/resourcebrowser/ResourceAdapter;->setSubTitle(Landroid/widget/TextView;Lmiui/app/resourcebrowser/resource/Resource;)V

    .end local v2           #tv:Landroid/widget/TextView;
    :cond_1d
    :goto_1d
    return-void

    :cond_1e
    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mBatchHandler:Lmiui/app/resourcebrowser/view/BatchResourceHandler;

    if-eqz v3, :cond_1d

    const v3, 0x60b0048

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .local v0, thumbnail:Landroid/widget/ImageView;
    if-eqz p2, :cond_53

    new-instance v3, Landroid/util/Pair;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    iget-object v3, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mBatchHandler:Lmiui/app/resourcebrowser/view/BatchResourceHandler;

    invoke-virtual {v3}, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->getResourceClickListener()Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-direct {p0, p1, p2}, Lmiui/app/resourcebrowser/ResourceAdapter;->setResourceFlag(Landroid/view/View;Lmiui/app/resourcebrowser/resource/Resource;)V

    const-string v3, "NAME"

    invoke-direct {p0, p1, v6, p2, v3}, Lmiui/app/resourcebrowser/ResourceAdapter;->bindText(Landroid/view/View;ILmiui/app/resourcebrowser/resource/Resource;Ljava/lang/String;)V

    goto :goto_1d

    :cond_53
    invoke-virtual {v0}, Landroid/widget/ImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .local v1, title:Landroid/view/View;
    if-eqz v1, :cond_1d

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1d
.end method

.method protected checkResourceModifyTime()Z
    .registers 2

    .prologue
    const/4 v0, 0x1

    return v0
.end method

.method public clearResourceSet()V
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mResourceSet:Lmiui/app/resourcebrowser/resource/ResourceSet;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mResourceSet:Lmiui/app/resourcebrowser/resource/ResourceSet;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/resource/ResourceSet;->clear()V

    :cond_9
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mBatchHandler:Lmiui/app/resourcebrowser/view/BatchResourceHandler;

    if-eqz v0, :cond_12

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mBatchHandler:Lmiui/app/resourcebrowser/view/BatchResourceHandler;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->quitEditMode()V

    :cond_12
    return-void
.end method

.method public clearResourceSet(I)V
    .registers 3
    .parameter "group"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mResourceSet:Lmiui/app/resourcebrowser/resource/ResourceSet;

    if-eqz v0, :cond_17

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mResourceSet:Lmiui/app/resourcebrowser/resource/ResourceSet;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/resource/ResourceSet;->size()I

    move-result v0

    if-le v0, p1, :cond_17

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mResourceSet:Lmiui/app/resourcebrowser/resource/ResourceSet;

    invoke-virtual {v0, p1}, Lmiui/app/resourcebrowser/resource/ResourceSet;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/widget/DataGroup;

    invoke-virtual {v0}, Lmiui/widget/DataGroup;->clear()V

    :cond_17
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mBatchHandler:Lmiui/app/resourcebrowser/view/BatchResourceHandler;

    if-eqz v0, :cond_20

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mBatchHandler:Lmiui/app/resourcebrowser/view/BatchResourceHandler;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/view/BatchResourceHandler;->quitEditMode()V

    :cond_20
    return-void
.end method

.method protected getBottomFlagId(Lmiui/app/resourcebrowser/resource/Resource;)I
    .registers 4
    .parameter "resourceItem"

    .prologue
    iget v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mResourceSetCategory:I

    if-eqz v0, :cond_9

    iget v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mResourceSetCategory:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_e

    :cond_9
    invoke-virtual {p0, p1}, Lmiui/app/resourcebrowser/ResourceAdapter;->getDownloadableFlag(Lmiui/app/resourcebrowser/resource/Resource;)I

    move-result v0

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method protected bridge synthetic getCacheKeys(Ljava/lang/Object;)Ljava/util/List;
    .registers 3
    .parameter "x0"

    .prologue
    check-cast p1, Lmiui/app/resourcebrowser/resource/Resource;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/app/resourcebrowser/ResourceAdapter;->getCacheKeys(Lmiui/app/resourcebrowser/resource/Resource;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected getCacheKeys(Lmiui/app/resourcebrowser/resource/Resource;)Ljava/util/List;
    .registers 7
    .parameter "data"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiui/app/resourcebrowser/resource/Resource;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .local v0, cacheKeys:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Object;>;"
    iget v4, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mDisplayType:I

    invoke-static {v4}, Lmiui/app/resourcebrowser/util/ResourceHelper;->isMultipleView(I)Z

    move-result v4

    if-eqz v4, :cond_21

    const/4 v3, 0x3

    .local v3, total:I
    :goto_e
    const/4 v1, 0x0

    .local v1, i:I
    :goto_f
    if-ge v1, v3, :cond_23

    invoke-virtual {p1, v1}, Lmiui/app/resourcebrowser/resource/Resource;->getLocalThumbnail(I)Ljava/lang/String;

    move-result-object v2

    .local v2, thumbnailPath:Ljava/lang/String;
    if-eqz v2, :cond_1e

    invoke-virtual {p1, v1}, Lmiui/app/resourcebrowser/resource/Resource;->getLocalThumbnail(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1e
    add-int/lit8 v1, v1, 0x1

    goto :goto_f

    .end local v1           #i:I
    .end local v2           #thumbnailPath:Ljava/lang/String;
    .end local v3           #total:I
    :cond_21
    const/4 v3, 0x1

    goto :goto_e

    .restart local v1       #i:I
    .restart local v3       #total:I
    :cond_23
    return-object v0
.end method

.method protected getCenterFlagId(Lmiui/app/resourcebrowser/resource/Resource;)I
    .registers 4
    .parameter "resourceItem"

    .prologue
    iget v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mResourceSetCategory:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_a

    invoke-virtual {p0, p1}, Lmiui/app/resourcebrowser/ResourceAdapter;->getDownloadableFlag(Lmiui/app/resourcebrowser/resource/Resource;)I

    move-result v0

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method protected getDownloadableFlag(Lmiui/app/resourcebrowser/resource/Resource;)I
    .registers 5
    .parameter "resourceItem"

    .prologue
    invoke-virtual {p1}, Lmiui/app/resourcebrowser/resource/Resource;->getStatus()I

    move-result v0

    .local v0, updateStatus:I
    if-nez v0, :cond_1a

    invoke-virtual {p1}, Lmiui/app/resourcebrowser/resource/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mCurrentUsingPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    const v1, 0x602003c

    :goto_15
    return v1

    :cond_16
    const v1, 0x602003a

    goto :goto_15

    :cond_1a
    const/4 v1, 0x0

    goto :goto_15
.end method

.method protected getFlagDownloaded()I
    .registers 2

    .prologue
    const v0, 0x602003a

    return v0
.end method

.method protected getFlagUsing()I
    .registers 2

    .prologue
    const v0, 0x602003c

    return v0
.end method

.method protected getItemViewLayoutParams(Landroid/view/View;II)Landroid/widget/LinearLayout$LayoutParams;
    .registers 8
    .parameter "itemView"
    .parameter "horizontalPos"
    .parameter "horizontalCount"

    .prologue
    const/4 v2, 0x0

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    iget v1, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mThumbnailWidth:I

    iget v3, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mThumbnailHeight:I

    invoke-direct {v0, v1, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .local v0, param:Landroid/widget/LinearLayout$LayoutParams;
    const/4 v1, 0x2

    if-lt p3, v1, :cond_1d

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    add-int/lit8 v1, p3, -0x1

    if-ge p2, v1, :cond_1e

    iget v1, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mItemHorizontalSpacing:I

    :goto_15
    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    iget v1, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mItemVerticalSpaceing:I

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    :cond_1d
    return-object v0

    :cond_1e
    move v1, v2

    goto :goto_15
.end method

.method protected getLoadPartialDataTask()Lmiui/widget/AsyncAdapter$AsyncLoadPartialDataTask;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lmiui/widget/AsyncAdapter",
            "<",
            "Lmiui/app/resourcebrowser/resource/Resource;",
            ">.Async",
            "LoadPartialDataTask;"
        }
    .end annotation

    .prologue
    new-instance v0, Lmiui/widget/AsyncImageAdapter$AsyncLoadImageTask;

    invoke-direct {v0, p0}, Lmiui/widget/AsyncImageAdapter$AsyncLoadImageTask;-><init>(Lmiui/widget/AsyncImageAdapter;)V

    .local v0, task:Lmiui/widget/AsyncImageAdapter$AsyncLoadImageTask;,"Lmiui/widget/AsyncImageAdapter<Lmiui/app/resourcebrowser/resource/Resource;>.AsyncLoadImageTask;"
    new-instance v1, Lmiui/os/DaemonAsyncTask$StackJobPool;

    invoke-direct {v1}, Lmiui/os/DaemonAsyncTask$StackJobPool;-><init>()V

    invoke-virtual {v0, v1}, Lmiui/widget/AsyncImageAdapter$AsyncLoadImageTask;->setJobPool(Lmiui/os/DaemonAsyncTask$JobPool;)V

    iget v1, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mThumbnailWidth:I

    iget v2, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mThumbnailHeight:I

    invoke-virtual {v0, v1, v2}, Lmiui/widget/AsyncImageAdapter$AsyncLoadImageTask;->setTargetSize(II)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmiui/widget/AsyncImageAdapter$AsyncLoadImageTask;->setScaled(Z)V

    return-object v0
.end method

.method protected final getRegisterAsyncTaskObserver()Ljava/lang/Object;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mFragment:Lmiui/app/resourcebrowser/BaseFragment;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mFragment:Lmiui/app/resourcebrowser/BaseFragment;

    :goto_6
    return-object v0

    :cond_7
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mContext:Landroid/app/Activity;

    goto :goto_6
.end method

.method public getResourceSet()Lmiui/app/resourcebrowser/resource/ResourceSet;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mResourceSet:Lmiui/app/resourcebrowser/resource/ResourceSet;

    return-object v0
.end method

.method protected getTopFlagId(Lmiui/app/resourcebrowser/resource/Resource;)I
    .registers 3
    .parameter "resourceItem"

    .prologue
    iget v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mResourceSetCategory:I

    if-nez v0, :cond_9

    invoke-virtual {p0, p1}, Lmiui/app/resourcebrowser/ResourceAdapter;->getUpdatableFlag(Lmiui/app/resourcebrowser/resource/Resource;)I

    move-result v0

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method protected getUpdatableFlag(Lmiui/app/resourcebrowser/resource/Resource;)I
    .registers 4
    .parameter "resourceItem"

    .prologue
    invoke-virtual {p1}, Lmiui/app/resourcebrowser/resource/Resource;->getStatus()I

    move-result v0

    .local v0, updateStatus:I
    const/4 v1, 0x1

    if-ne v0, v1, :cond_b

    const v1, 0x602003b

    :goto_a
    return v1

    :cond_b
    const/4 v1, 0x0

    goto :goto_a
.end method

.method public isEnabled(I)Z
    .registers 3
    .parameter "position"

    .prologue
    const/4 v0, 0x0

    return v0
.end method

.method protected bridge synthetic isValidKey(Ljava/lang/Object;Ljava/lang/Object;I)Z
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    check-cast p2, Lmiui/app/resourcebrowser/resource/Resource;

    .end local p2
    invoke-virtual {p0, p1, p2, p3}, Lmiui/app/resourcebrowser/ResourceAdapter;->isValidKey(Ljava/lang/Object;Lmiui/app/resourcebrowser/resource/Resource;I)Z

    move-result v0

    return v0
.end method

.method protected isValidKey(Ljava/lang/Object;Lmiui/app/resourcebrowser/resource/Resource;I)Z
    .registers 11
    .parameter "key"
    .parameter "data"
    .parameter "position"

    .prologue
    const/4 v2, 0x0

    move-object v1, p1

    check-cast v1, Ljava/lang/String;

    .local v1, localPath:Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .local v0, file:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_10

    :goto_f
    return v2

    :cond_10
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceAdapter;->checkResourceModifyTime()Z

    move-result v3

    if-eqz v3, :cond_26

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v3

    invoke-virtual {p2}, Lmiui/app/resourcebrowser/resource/Resource;->getFileModifiedTime()J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-gez v3, :cond_26

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_f

    :cond_26
    invoke-super {p0, p1, p2, p3}, Lmiui/widget/AsyncImageAdapter;->isValidKey(Ljava/lang/Object;Ljava/lang/Object;I)Z

    move-result v2

    goto :goto_f
.end method

.method public refreshDataSet()V
    .registers 3

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mMetaData:Landroid/os/Bundle;

    const-string v1, "miui.app.resourcebrowser.RESOURCE_SET_PACKAGE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mResourceSetPackage:Ljava/lang/String;

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mMetaData:Landroid/os/Bundle;

    const-string v1, "miui.app.resourcebrowser.RESOURCE_SET_SUBPACKAGE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mResourceSetSubpackage:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mResourceSetPackage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mResourceSetSubpackage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lmiui/app/resourcebrowser/resource/ResourceSet;->getInstance(Ljava/lang/String;)Lmiui/app/resourcebrowser/resource/ResourceSet;

    move-result-object v0

    iput-object v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mResourceSet:Lmiui/app/resourcebrowser/resource/ResourceSet;

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mResourceSet:Lmiui/app/resourcebrowser/resource/ResourceSet;

    invoke-virtual {p0, v0}, Lmiui/app/resourcebrowser/ResourceAdapter;->setDataSet(Ljava/util/List;)V

    return-void
.end method

.method public setMusicPlayer(Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;)V
    .registers 2
    .parameter "player"

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mMusicPlayer:Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;

    return-void
.end method

.method public setResourceBatchHandler(Lmiui/app/resourcebrowser/view/BatchResourceHandler;)V
    .registers 2
    .parameter "handler"

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mBatchHandler:Lmiui/app/resourcebrowser/view/BatchResourceHandler;

    return-void
.end method

.method protected setSubTitle(Landroid/widget/TextView;Lmiui/app/resourcebrowser/resource/Resource;)V
    .registers 7
    .parameter "tv"
    .parameter "resourceItem"

    .prologue
    const/4 v3, 0x0

    invoke-virtual {p2}, Lmiui/app/resourcebrowser/resource/Resource;->getInformation()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "audio_duration"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .local v0, duration:I
    if-nez v0, :cond_21

    invoke-virtual {p2}, Lmiui/app/resourcebrowser/resource/Resource;->getLocalPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lmiui/app/resourcebrowser/service/local/AudioResourceFolder;->getLocalRingtoneDuration(Ljava/lang/String;)J

    move-result-wide v1

    long-to-int v0, v1

    if-lez v0, :cond_21

    invoke-virtual {p2}, Lmiui/app/resourcebrowser/resource/Resource;->getInformation()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "audio_duration"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_21
    if-gez v0, :cond_29

    const/16 v1, 0x8

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_28
    return-void

    :cond_29
    if-lez v0, :cond_36

    invoke-virtual {p1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-static {v0}, Lmiui/app/resourcebrowser/service/local/AudioResourceFolder;->formatDuration(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_28

    :cond_36
    invoke-virtual {p1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    const v1, 0x60c000b

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_28
.end method

.method public stopMusic()V
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mMusicPlayer:Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mMusicPlayer:Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/util/ResourceMusicPlayer;->stopMusic()V

    :cond_9
    return-void
.end method

.method public updateCurrentUsingPath(Ljava/lang/String;)Z
    .registers 5
    .parameter "path"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mCurrentUsingPath:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_15

    iput-object p1, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mCurrentUsingPath:Ljava/lang/String;

    iget-object v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mMetaData:Landroid/os/Bundle;

    const-string v1, "miui.app.resourcebrowser.CURRENT_USING_PATH"

    iget-object v2, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mCurrentUsingPath:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    :goto_14
    return v0

    :cond_15
    const/4 v0, 0x0

    goto :goto_14
.end method

.method protected useLowQualityDecoding()Z
    .registers 2

    .prologue
    iget-boolean v0, p0, Lmiui/app/resourcebrowser/ResourceAdapter;->mDecodeImageLowQuality:Z

    return v0
.end method
