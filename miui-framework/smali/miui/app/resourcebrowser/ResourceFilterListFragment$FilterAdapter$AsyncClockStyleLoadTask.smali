.class public Lmiui/app/resourcebrowser/ResourceFilterListFragment$FilterAdapter$AsyncClockStyleLoadTask;
.super Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;
.source "ResourceFilterListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/app/resourcebrowser/ResourceFilterListFragment$FilterAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "AsyncClockStyleLoadTask"
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/app/resourcebrowser/ResourceFilterListFragment$FilterAdapter;


# direct methods
.method public constructor <init>(Lmiui/app/resourcebrowser/ResourceFilterListFragment$FilterAdapter;)V
    .registers 2
    .parameter

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/ResourceFilterListFragment$FilterAdapter$AsyncClockStyleLoadTask;->this$0:Lmiui/app/resourcebrowser/ResourceFilterListFragment$FilterAdapter;

    invoke-direct {p0, p1}, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;-><init>(Lmiui/widget/AsyncAdapter;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic loadData(I)[Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    .prologue
    invoke-virtual {p0, p1}, Lmiui/app/resourcebrowser/ResourceFilterListFragment$FilterAdapter$AsyncClockStyleLoadTask;->loadData(I)[Lmiui/app/resourcebrowser/resource/Resource;

    move-result-object v0

    return-object v0
.end method

.method protected loadData(I)[Lmiui/app/resourcebrowser/resource/Resource;
    .registers 3
    .parameter "index"

    .prologue
    const/4 v0, 0x0

    return-object v0
.end method

.method public bridge synthetic onLoadData([Ljava/lang/Object;)V
    .registers 2
    .parameter "x0"

    .prologue
    check-cast p1, [Lmiui/app/resourcebrowser/resource/Resource;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/app/resourcebrowser/ResourceFilterListFragment$FilterAdapter$AsyncClockStyleLoadTask;->onLoadData([Lmiui/app/resourcebrowser/resource/Resource;)V

    return-void
.end method

.method public varargs onLoadData([Lmiui/app/resourcebrowser/resource/Resource;)V
    .registers 13
    .parameter "partialDataSet"

    .prologue
    if-eqz p1, :cond_58

    array-length v9, p1

    new-array v2, v9, [Z

    .local v2, flag:[Z
    const/4 v0, 0x0

    .local v0, c:I
    const/4 v3, 0x0

    .local v3, i:I
    :goto_7
    array-length v9, p1

    if-ge v3, v9, :cond_42

    aget-object v6, p1, v3

    .local v6, r:Lmiui/app/resourcebrowser/resource/Resource;
    invoke-virtual {v6}, Lmiui/app/resourcebrowser/resource/Resource;->getInformation()Landroid/os/Bundle;

    move-result-object v9

    const-string v10, "NVP"

    invoke-virtual {v9, v10}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v7

    check-cast v7, Ljava/util/HashMap;

    .local v7, s:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    if-nez v7, :cond_1d

    :cond_1a
    :goto_1a
    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    :cond_1d
    const-string v9, "type"

    invoke-virtual {v7, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .local v1, filterStr:Ljava/lang/String;
    :try_start_25
    iget-object v9, p0, Lmiui/app/resourcebrowser/ResourceFilterListFragment$FilterAdapter$AsyncClockStyleLoadTask;->this$0:Lmiui/app/resourcebrowser/ResourceFilterListFragment$FilterAdapter;

    #getter for: Lmiui/app/resourcebrowser/ResourceFilterListFragment$FilterAdapter;->mFilterCode:I
    invoke-static {v9}, Lmiui/app/resourcebrowser/ResourceFilterListFragment$FilterAdapter;->access$000(Lmiui/app/resourcebrowser/ResourceFilterListFragment$FilterAdapter;)I

    move-result v9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    and-int/2addr v9, v10

    if-eqz v9, :cond_40

    const/4 v9, 0x1

    :goto_37
    aput-boolean v9, v2, v3

    aget-boolean v9, v2, v3
    :try_end_3b
    .catch Ljava/lang/NumberFormatException; {:try_start_25 .. :try_end_3b} :catch_5c

    if-eqz v9, :cond_1a

    add-int/lit8 v0, v0, 0x1

    goto :goto_1a

    :cond_40
    const/4 v9, 0x0

    goto :goto_37

    .end local v1           #filterStr:Ljava/lang/String;
    .end local v6           #r:Lmiui/app/resourcebrowser/resource/Resource;
    .end local v7           #s:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_42
    new-array v8, v0, [Lmiui/app/resourcebrowser/resource/Resource;

    .local v8, usable:[Lmiui/app/resourcebrowser/resource/Resource;
    const/4 v4, 0x0

    .local v4, k:I
    const/4 v3, 0x0

    :goto_46
    array-length v9, v8

    if-ge v3, v9, :cond_57

    aget-boolean v9, v2, v3

    if-eqz v9, :cond_54

    add-int/lit8 v5, v4, 0x1

    .end local v4           #k:I
    .local v5, k:I
    aget-object v9, p1, v3

    aput-object v9, v8, v4

    move v4, v5

    .end local v5           #k:I
    .restart local v4       #k:I
    :cond_54
    add-int/lit8 v3, v3, 0x1

    goto :goto_46

    :cond_57
    move-object p1, v8

    .end local v0           #c:I
    .end local v2           #flag:[Z
    .end local v3           #i:I
    .end local v4           #k:I
    .end local v8           #usable:[Lmiui/app/resourcebrowser/resource/Resource;
    :cond_58
    invoke-super {p0, p1}, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->onLoadData([Ljava/lang/Object;)V

    return-void

    .restart local v0       #c:I
    .restart local v1       #filterStr:Ljava/lang/String;
    .restart local v2       #flag:[Z
    .restart local v3       #i:I
    .restart local v6       #r:Lmiui/app/resourcebrowser/resource/Resource;
    .restart local v7       #s:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :catch_5c
    move-exception v9

    goto :goto_1a
.end method
