.class public Lmiui/app/resourcebrowser/ResourceFilterListFragment$FilterAdapter;
.super Lmiui/app/resourcebrowser/LocalResourceAdapter;
.source "ResourceFilterListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/app/resourcebrowser/ResourceFilterListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FilterAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/app/resourcebrowser/ResourceFilterListFragment$FilterAdapter$AsyncClockStyleLoadTask;
    }
.end annotation


# instance fields
.field private final mFilterCode:I


# direct methods
.method public constructor <init>(Lmiui/app/resourcebrowser/BaseFragment;Landroid/os/Bundle;I)V
    .registers 4
    .parameter "fragment"
    .parameter "metaData"
    .parameter "filterCode"

    .prologue
    invoke-direct {p0, p1, p2}, Lmiui/app/resourcebrowser/LocalResourceAdapter;-><init>(Lmiui/app/resourcebrowser/BaseFragment;Landroid/os/Bundle;)V

    iput p3, p0, Lmiui/app/resourcebrowser/ResourceFilterListFragment$FilterAdapter;->mFilterCode:I

    return-void
.end method

.method static synthetic access$000(Lmiui/app/resourcebrowser/ResourceFilterListFragment$FilterAdapter;)I
    .registers 2
    .parameter "x0"

    .prologue
    iget v0, p0, Lmiui/app/resourcebrowser/ResourceFilterListFragment$FilterAdapter;->mFilterCode:I

    return v0
.end method


# virtual methods
.method protected getLoadDataTask()Ljava/util/List;
    .registers 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lmiui/widget/AsyncAdapter",
            "<",
            "Lmiui/app/resourcebrowser/resource/Resource;",
            ">.Async",
            "LoadDataTask;",
            ">;"
        }
    .end annotation

    .prologue
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .local v2, tasks:Ljava/util/List;,"Ljava/util/List<Lmiui/widget/AsyncAdapter<Lmiui/app/resourcebrowser/resource/Resource;>.AsyncLoadDataTask;>;"
    new-instance v1, Lmiui/app/resourcebrowser/ResourceFilterListFragment$FilterAdapter$AsyncClockStyleLoadTask;

    invoke-direct {v1, p0}, Lmiui/app/resourcebrowser/ResourceFilterListFragment$FilterAdapter$AsyncClockStyleLoadTask;-><init>(Lmiui/app/resourcebrowser/ResourceFilterListFragment$FilterAdapter;)V

    .local v1, task:Lmiui/app/resourcebrowser/ResourceFilterListFragment$FilterAdapter$AsyncClockStyleLoadTask;
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceFilterListFragment$FilterAdapter;->getRegisterAsyncTaskObserver()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lmiui/os/AsyncTaskObserver;

    invoke-virtual {v1, v4}, Lmiui/app/resourcebrowser/ResourceFilterListFragment$FilterAdapter$AsyncClockStyleLoadTask;->addObserver(Lmiui/os/AsyncTaskObserver;)V

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/ResourceFilterListFragment$FilterAdapter;->getVisitors()Ljava/util/List;

    move-result-object v3

    .local v3, visitors:Ljava/util/List;,"Ljava/util/List<Lmiui/widget/AsyncAdapter$AsyncLoadDataVisitor<Lmiui/app/resourcebrowser/resource/Resource;>;>;"
    const/4 v0, 0x0

    .local v0, i:I
    :goto_18
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_2a

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lmiui/widget/AsyncAdapter$AsyncLoadDataVisitor;

    invoke-virtual {v1, v4}, Lmiui/app/resourcebrowser/ResourceFilterListFragment$FilterAdapter$AsyncClockStyleLoadTask;->addVisitor(Lmiui/widget/AsyncAdapter$AsyncLoadDataVisitor;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_18

    :cond_2a
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v2
.end method
