.class public interface abstract Lmiui/app/resourcebrowser/IntentConstants$ThemeWidget;
.super Ljava/lang/Object;
.source "IntentConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/app/resourcebrowser/IntentConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ThemeWidget"
.end annotation


# static fields
.field public static final ACTION_SELECT_THEME_RESOURCE:Ljava/lang/String; = "android.intent.action.PICK_RESOURCE"

.field public static final CLOCK_COMPONENT_NAME:Ljava/lang/String; = "clock"

.field public static final PHOTO_FRAME_COMPONENT_NAME:Ljava/lang/String; = "photo_frame"

.field public static final RETURN_SELECT_THEME_PATH:Ljava/lang/String; = "miui.app.resourcebrowser.PICKED_RESOURCE"

.field public static final SHOW_COMPONENT_NAME:Ljava/lang/String; = "android.intent.extra.SHOW_COMPONENT_NAME"

.field public static final SHOW_COMPONENT_SIZE:Ljava/lang/String; = "android.intent.extra.SHOW_COMPONENT_SIZE"

.field public static final WIDGET_SIZE_1x2:Ljava/lang/String; = "1x2"

.field public static final WIDGET_SIZE_2x2:Ljava/lang/String; = "2x2"

.field public static final WIDGET_SIZE_2x4:Ljava/lang/String; = "2x4"

.field public static final WIDGET_SIZE_4x4:Ljava/lang/String; = "4x4"

.field public static final sClockPath:Ljava/lang/String; = "clock_"

.field public static final sPhotoFramePath:Ljava/lang/String; = "photoframe_"
