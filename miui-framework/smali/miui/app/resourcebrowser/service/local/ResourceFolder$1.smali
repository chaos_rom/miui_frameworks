.class Lmiui/app/resourcebrowser/service/local/ResourceFolder$1;
.super Ljava/lang/Object;
.source "ResourceFolder.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiui/app/resourcebrowser/service/local/ResourceFolder;->sortResource(Ljava/util/List;Lmiui/cache/FolderCache$FolderInfo;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/app/resourcebrowser/service/local/ResourceFolder;

.field final synthetic val$folderInfo:Lmiui/cache/FolderCache$FolderInfo;


# direct methods
.method constructor <init>(Lmiui/app/resourcebrowser/service/local/ResourceFolder;Lmiui/cache/FolderCache$FolderInfo;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder$1;->this$0:Lmiui/app/resourcebrowser/service/local/ResourceFolder;

    iput-object p2, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder$1;->val$folderInfo:Lmiui/cache/FolderCache$FolderInfo;

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4
    .parameter "x0"
    .parameter "x1"

    .prologue
    check-cast p1, Ljava/lang/String;

    .end local p1
    check-cast p2, Ljava/lang/String;

    .end local p2
    invoke-virtual {p0, p1, p2}, Lmiui/app/resourcebrowser/service/local/ResourceFolder$1;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public compare(Ljava/lang/String;Ljava/lang/String;)I
    .registers 8
    .parameter "object1"
    .parameter "object2"

    .prologue
    :try_start_0
    iget-object v3, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder$1;->val$folderInfo:Lmiui/cache/FolderCache$FolderInfo;

    iget-object v3, v3, Lmiui/cache/FolderCache$FolderInfo;->files:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmiui/cache/FolderCache$FileInfo;

    iget-wide v3, v3, Lmiui/cache/FolderCache$FileInfo;->modifiedTime:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .local v1, time1:Ljava/lang/Long;
    iget-object v3, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder$1;->val$folderInfo:Lmiui/cache/FolderCache$FolderInfo;

    iget-object v3, v3, Lmiui/cache/FolderCache$FolderInfo;->files:Ljava/util/Map;

    invoke-interface {v3, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmiui/cache/FolderCache$FileInfo;

    iget-wide v3, v3, Lmiui/cache/FolderCache$FileInfo;->modifiedTime:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .local v2, time2:Ljava/lang/Long;
    invoke-virtual {v2, v1}, Ljava/lang/Long;->compareTo(Ljava/lang/Long;)I
    :try_end_23
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_23} :catch_25

    move-result v3

    .end local v1           #time1:Ljava/lang/Long;
    .end local v2           #time2:Ljava/lang/Long;
    :goto_24
    return v3

    :catch_25
    move-exception v0

    .local v0, e:Ljava/lang/Exception;
    const/4 v3, 0x0

    goto :goto_24
.end method
