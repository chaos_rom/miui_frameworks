.class public Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;
.super Ljava/lang/Object;
.source "ZipResourceInfo.java"

# interfaces
.implements Lmiui/app/resourcebrowser/IntentConstants;


# static fields
.field protected static final AUTHOR_TAG:Ljava/lang/String; = "author"

.field protected static final DESIGNER_TAG:Ljava/lang/String; = "designer"

.field protected static final MAXIMUM_PREVIEW_COUNT:I = 0xa

.field protected static final PLATFORM_VERSION_TAG:Ljava/lang/String; = "platformVersion"

.field protected static final TITLE_TAG:Ljava/lang/String; = "title"

.field protected static final UI_VERSION_TAG:Ljava/lang/String; = "uiVersion"

.field protected static final VERSION_TAG:Ljava/lang/String; = "version"


# instance fields
.field public mAuthor:Ljava/lang/String;

.field public mContext:Landroid/content/Context;

.field public mDesigner:Ljava/lang/String;

.field protected mEncodedPath:Ljava/lang/String;

.field public mLastModified:J

.field protected mNvp:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mPath:Ljava/lang/String;

.field public mPlatformVersion:I

.field protected mPrefix:[Ljava/lang/String;

.field public mPreviews:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mSize:J

.field public mThumbnails:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mTitle:Ljava/lang/String;

.field public mVersion:Ljava/lang/String;

.field public mZipResourceCache:Lmiui/app/resourcebrowser/service/local/ZipResourceCache;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;Lmiui/app/resourcebrowser/service/local/ZipResourceCache;)V
    .registers 10
    .parameter "context"
    .parameter "path"
    .parameter "zipResourceCache"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljavax/xml/parsers/ParserConfigurationException;,
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mThumbnails:Ljava/util/ArrayList;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mPreviews:Ljava/util/ArrayList;

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mNvp:Ljava/util/HashMap;

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->getCacheInstance()Lmiui/app/resourcebrowser/service/local/ZipResourceCache;

    move-result-object v2

    iput-object v2, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mZipResourceCache:Lmiui/app/resourcebrowser/service/local/ZipResourceCache;

    iput-object p1, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mPath:Ljava/lang/String;

    const/16 v2, 0x2f

    const/16 v3, 0x5f

    invoke-virtual {p2, v2, v3}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mEncodedPath:Ljava/lang/String;

    :try_start_2c
    iget-object v2, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "META_DATA_FOR_LOCAL"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .local v1, metaData:Landroid/os/Bundle;
    const-string v2, "miui.app.resourcebrowser.PREVIEW_PREFIX"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mPrefix:[Ljava/lang/String;
    :try_end_42
    .catch Ljava/lang/Exception; {:try_start_2c .. :try_end_42} :catch_c5

    .end local v1           #metaData:Landroid/os/Bundle;
    :goto_42
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .local v0, file:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    iput-wide v2, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mLastModified:J

    iget-wide v2, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mLastModified:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_64

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/io/File;->setLastModified(J)Z

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    iput-wide v2, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mLastModified:J

    :cond_64
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    iput-wide v2, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mSize:J

    if-eqz p3, :cond_8b

    invoke-virtual {p3}, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->valid()Z

    move-result v2

    if-eqz v2, :cond_8b

    iget-object v2, p3, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->title:Ljava/lang/String;

    iput-object v2, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mTitle:Ljava/lang/String;

    iget-object v2, p3, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->author:Ljava/lang/String;

    iput-object v2, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mAuthor:Ljava/lang/String;

    iget-object v2, p3, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->designer:Ljava/lang/String;

    iput-object v2, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mDesigner:Ljava/lang/String;

    iget-object v2, p3, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->version:Ljava/lang/String;

    iput-object v2, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mVersion:Ljava/lang/String;

    iget v2, p3, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->platformVersion:I

    iput v2, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mPlatformVersion:I

    iget-object v2, p3, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->nvp:Ljava/util/HashMap;

    iput-object v2, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mNvp:Ljava/util/HashMap;

    :goto_8a
    return-void

    :cond_8b
    invoke-virtual {p0, p2}, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->parseBasicInformation(Ljava/lang/String;)V

    iget-object v2, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mZipResourceCache:Lmiui/app/resourcebrowser/service/local/ZipResourceCache;

    iget-object v3, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mPath:Ljava/lang/String;

    iput-object v3, v2, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->filePath:Ljava/lang/String;

    iget-object v2, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mZipResourceCache:Lmiui/app/resourcebrowser/service/local/ZipResourceCache;

    iget-wide v3, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mLastModified:J

    iput-wide v3, v2, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->lastModifyTime:J

    iget-object v2, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mZipResourceCache:Lmiui/app/resourcebrowser/service/local/ZipResourceCache;

    iget-wide v3, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mSize:J

    iput-wide v3, v2, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->fileSize:J

    iget-object v2, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mZipResourceCache:Lmiui/app/resourcebrowser/service/local/ZipResourceCache;

    iget-object v3, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mTitle:Ljava/lang/String;

    iput-object v3, v2, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->title:Ljava/lang/String;

    iget-object v2, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mZipResourceCache:Lmiui/app/resourcebrowser/service/local/ZipResourceCache;

    iget-object v3, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mAuthor:Ljava/lang/String;

    iput-object v3, v2, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->author:Ljava/lang/String;

    iget-object v2, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mZipResourceCache:Lmiui/app/resourcebrowser/service/local/ZipResourceCache;

    iget-object v3, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mDesigner:Ljava/lang/String;

    iput-object v3, v2, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->designer:Ljava/lang/String;

    iget-object v2, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mZipResourceCache:Lmiui/app/resourcebrowser/service/local/ZipResourceCache;

    iget-object v3, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mVersion:Ljava/lang/String;

    iput-object v3, v2, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->version:Ljava/lang/String;

    iget-object v2, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mZipResourceCache:Lmiui/app/resourcebrowser/service/local/ZipResourceCache;

    iget v3, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mPlatformVersion:I

    iput v3, v2, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->platformVersion:I

    iget-object v2, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mZipResourceCache:Lmiui/app/resourcebrowser/service/local/ZipResourceCache;

    iget-object v3, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mNvp:Ljava/util/HashMap;

    iput-object v3, v2, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->nvp:Ljava/util/HashMap;

    goto :goto_8a

    .end local v0           #file:Ljava/io/File;
    :catch_c5
    move-exception v2

    goto/16 :goto_42
.end method

.method public static varargs createZipResourceInfo(Landroid/content/Context;Ljava/lang/String;Lmiui/app/resourcebrowser/service/local/ZipResourceCache;[Ljava/lang/Object;)Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;
    .registers 8
    .parameter "context"
    .parameter "path"
    .parameter "cache"
    .parameter "args"

    .prologue
    const/4 v1, 0x0

    .local v1, result:Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;
    :try_start_1
    new-instance v2, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;

    invoke-direct {v2, p0, p1, p2}, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;-><init>(Landroid/content/Context;Ljava/lang/String;Lmiui/app/resourcebrowser/service/local/ZipResourceCache;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_6} :catch_1c

    .end local v1           #result:Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;
    .local v2, result:Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;
    if-eqz p2, :cond_e

    :try_start_8
    invoke-virtual {p2}, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->valid()Z

    move-result v3

    if-nez v3, :cond_13

    :cond_e
    invoke-virtual {v2}, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->loadResourcePreviews()V

    iget-object p2, v2, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mZipResourceCache:Lmiui/app/resourcebrowser/service/local/ZipResourceCache;

    :cond_13
    invoke-virtual {v2, p2}, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->loadPreviewsFromCache(Lmiui/app/resourcebrowser/service/local/ZipResourceCache;)V

    if-eqz p2, :cond_1a

    iput-object p2, v2, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mZipResourceCache:Lmiui/app/resourcebrowser/service/local/ZipResourceCache;
    :try_end_1a
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_1a} :catch_21

    :cond_1a
    move-object v1, v2

    .end local v2           #result:Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;
    .restart local v1       #result:Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;
    :goto_1b
    return-object v1

    :catch_1c
    move-exception v0

    .local v0, e:Ljava/lang/Exception;
    :goto_1d
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1b

    .end local v0           #e:Ljava/lang/Exception;
    .end local v1           #result:Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;
    .restart local v2       #result:Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;
    :catch_21
    move-exception v0

    move-object v1, v2

    .end local v2           #result:Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;
    .restart local v1       #result:Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;
    goto :goto_1d
.end method


# virtual methods
.method public clearCache()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    iput-object v0, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mZipResourceCache:Lmiui/app/resourcebrowser/service/local/ZipResourceCache;

    return-void
.end method

.method protected extract(Ljava/util/zip/ZipFile;Ljava/util/zip/ZipEntry;Ljava/lang/String;)Ljava/lang/String;
    .registers 11
    .parameter "zipfile"
    .parameter "entry"
    .parameter "path"

    .prologue
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->getPreviewPathPrefix()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .local v2, localPath:Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .local v1, file:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_3f

    invoke-virtual {v1}, Ljava/io/File;->lastModified()J

    move-result-wide v3

    iget-wide v5, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mLastModified:J

    cmp-long v3, v3, v5

    if-ltz v3, :cond_3c

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v3

    invoke-virtual {p2}, Ljava/util/zip/ZipEntry;->getSize()J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-eqz v3, :cond_3f

    :cond_3c
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_3f
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_4c

    :try_start_45
    invoke-virtual {p1, p2}, Ljava/util/zip/ZipFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;

    move-result-object v3

    invoke-static {v3, v2}, Lmiui/app/resourcebrowser/util/ResourceHelper;->writeTo(Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_4c
    .catch Ljava/io/IOException; {:try_start_45 .. :try_end_4c} :catch_4d

    :cond_4c
    :goto_4c
    return-object v2

    :catch_4d
    move-exception v0

    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4c
.end method

.method public getCache()Lmiui/app/resourcebrowser/service/local/ZipResourceCache;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mZipResourceCache:Lmiui/app/resourcebrowser/service/local/ZipResourceCache;

    return-object v0
.end method

.method protected getCacheInstance()Lmiui/app/resourcebrowser/service/local/ZipResourceCache;
    .registers 2

    .prologue
    new-instance v0, Lmiui/app/resourcebrowser/service/local/ZipResourceCache$DefaultZipCacheImpl;

    invoke-direct {v0}, Lmiui/app/resourcebrowser/service/local/ZipResourceCache$DefaultZipCacheImpl;-><init>()V

    return-object v0
.end method

.method public getInformation()Landroid/os/Bundle;
    .registers 5

    .prologue
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .local v0, information:Landroid/os/Bundle;
    const-string v1, "SIZE"

    iget-wide v2, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mSize:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "NAME"

    iget-object v2, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mTitle:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "AUTHOR"

    iget-object v2, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mAuthor:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "DESIGNER"

    iget-object v2, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mDesigner:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "VERSION"

    iget-object v2, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mVersion:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "PLATFORM_VERSION"

    iget v2, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mPlatformVersion:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "MODIFIED_TIME"

    iget-wide v2, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mLastModified:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v1, "LOCAL_PATH"

    iget-object v2, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mPath:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "LOCAL_PREVIEW"

    iget-object v2, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mPreviews:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v1, "LOCAL_THUMBNAIL"

    iget-object v2, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mThumbnails:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v1, "NVP"

    iget-object v2, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mNvp:Ljava/util/HashMap;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    return-object v0
.end method

.method protected getPreviewPathPrefix()Ljava/lang/String;
    .registers 3

    .prologue
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lmiui/app/resourcebrowser/ResourceConstants;->PREVIEW_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mEncodedPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lmiui/os/ExtraFileUtils;->standardizeFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected loadPreview(Ljava/util/zip/ZipFile;Ljava/lang/String;Ljava/util/ArrayList;)Z
    .registers 6
    .parameter "zipfile"
    .parameter "name"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/zip/ZipFile;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p3, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p1, p2}, Ljava/util/zip/ZipFile;->getEntry(Ljava/lang/String;)Ljava/util/zip/ZipEntry;

    move-result-object v0

    .local v0, entry:Ljava/util/zip/ZipEntry;
    if-eqz v0, :cond_f

    invoke-virtual {p0, p1, v0, p2}, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->extract(Ljava/util/zip/ZipFile;Ljava/util/zip/ZipEntry;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x1

    :goto_e
    return v1

    :cond_f
    const/4 v1, 0x0

    goto :goto_e
.end method

.method protected loadPreviews(Ljava/util/zip/ZipFile;Ljava/lang/String;)Ljava/util/ArrayList;
    .registers 11
    .parameter "zipfile"
    .parameter "prefix"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/zip/ZipFile;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .local v1, result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .local v0, i:I
    :goto_9
    const/16 v2, 0xa

    if-ge v0, v2, :cond_39

    const-string v2, "%s%d.jpg"

    new-array v3, v7, [Ljava/lang/Object;

    aput-object p2, v3, v5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p1, v2, v1}, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->loadPreview(Ljava/util/zip/ZipFile;Ljava/lang/String;Ljava/util/ArrayList;)Z

    move-result v2

    if-nez v2, :cond_36

    const-string v2, "%s%d.png"

    new-array v3, v7, [Ljava/lang/Object;

    aput-object p2, v3, v5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p1, v2, v1}, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->loadPreview(Ljava/util/zip/ZipFile;Ljava/lang/String;Ljava/util/ArrayList;)Z

    :cond_36
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    :cond_39
    return-object v1
.end method

.method protected loadPreviewsFromCache(Lmiui/app/resourcebrowser/service/local/ZipResourceCache;)V
    .registers 3
    .parameter "zipResourceCache"

    .prologue
    if-nez p1, :cond_3

    :goto_2
    return-void

    :cond_3
    iget-object v0, p1, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->previews:Ljava/util/ArrayList;

    iput-object v0, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mPreviews:Ljava/util/ArrayList;

    iget-object v0, p1, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->thumbnails:Ljava/util/ArrayList;

    iput-object v0, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mThumbnails:Ljava/util/ArrayList;

    goto :goto_2
.end method

.method protected loadResourcePreviews()V
    .registers 11

    .prologue
    :try_start_0
    new-instance v7, Ljava/util/zip/ZipFile;

    iget-object v8, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mPath:Ljava/lang/String;

    invoke-direct {v7, v8}, Ljava/util/zip/ZipFile;-><init>(Ljava/lang/String;)V

    .local v7, zipfile:Ljava/util/zip/ZipFile;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .local v2, componentThumnail:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .local v1, componentPreview:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v4, 0x0

    .local v4, i:I
    :goto_12
    iget-object v8, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mPrefix:[Ljava/lang/String;

    array-length v8, v8

    if-ge v4, v8, :cond_83

    const-string v5, "preview/"

    .local v5, prefix:Ljava/lang/String;
    iget-object v8, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mPrefix:[Ljava/lang/String;

    aget-object v8, v8, v4

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_38

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mPrefix:[Ljava/lang/String;

    aget-object v9, v9, v4

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    :cond_38
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "big_"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v7, v8}, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->loadPreviews(Ljava/util/zip/ZipFile;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .local v0, biglist:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "small_"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v7, v8}, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->loadPreviews(Ljava/util/zip/ZipFile;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    .local v6, smalllist:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-nez v8, :cond_73

    invoke-virtual {p0, v7, v5}, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->loadPreviews(Ljava/util/zip/ZipFile;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_73
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-nez v8, :cond_7a

    move-object v6, v0

    :cond_7a
    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_12

    .end local v0           #biglist:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v5           #prefix:Ljava/lang/String;
    .end local v6           #smalllist:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_83
    iget-object v8, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mZipResourceCache:Lmiui/app/resourcebrowser/service/local/ZipResourceCache;

    iput-object v2, v8, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->thumbnails:Ljava/util/ArrayList;

    iget-object v8, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mZipResourceCache:Lmiui/app/resourcebrowser/service/local/ZipResourceCache;

    iput-object v1, v8, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->previews:Ljava/util/ArrayList;
    :try_end_8b
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_8b} :catch_8c

    .end local v1           #componentPreview:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v2           #componentThumnail:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v4           #i:I
    .end local v7           #zipfile:Ljava/util/zip/ZipFile;
    :goto_8b
    return-void

    :catch_8c
    move-exception v3

    .local v3, e:Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8b
.end method

.method protected parseBasicInformation(Ljava/lang/String;)V
    .registers 7
    .parameter "zipFilePath"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/zip/ZipException;,
            Ljava/io/IOException;,
            Ljavax/xml/parsers/ParserConfigurationException;,
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const-string v1, "description.xml"

    invoke-static {p1, v1}, Lmiui/app/resourcebrowser/util/ZipFileHelper;->getZipResourceDescribeInfo(Ljava/lang/String;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v1

    iput-object v1, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mNvp:Ljava/util/HashMap;

    iget-object v1, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mNvp:Ljava/util/HashMap;

    if-eqz v1, :cond_72

    iget-object v1, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mNvp:Ljava/util/HashMap;

    const-string v3, "title"

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mTitle:Ljava/lang/String;

    iget-object v1, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mTitle:Ljava/lang/String;

    if-nez v1, :cond_c2

    move-object v1, v2

    :goto_1e
    iput-object v1, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mTitle:Ljava/lang/String;

    iget-object v1, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mNvp:Ljava/util/HashMap;

    const-string v3, "author"

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mAuthor:Ljava/lang/String;

    iget-object v1, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mAuthor:Ljava/lang/String;

    if-nez v1, :cond_ca

    move-object v1, v2

    :goto_31
    iput-object v1, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mAuthor:Ljava/lang/String;

    iget-object v1, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mNvp:Ljava/util/HashMap;

    const-string v3, "designer"

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mDesigner:Ljava/lang/String;

    iget-object v1, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mDesigner:Ljava/lang/String;

    if-nez v1, :cond_d2

    move-object v1, v2

    :goto_44
    iput-object v1, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mDesigner:Ljava/lang/String;

    iget-object v1, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mNvp:Ljava/util/HashMap;

    const-string v3, "version"

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mVersion:Ljava/lang/String;

    iget-object v1, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mVersion:Ljava/lang/String;

    if-nez v1, :cond_da

    :goto_56
    iput-object v2, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mVersion:Ljava/lang/String;

    :try_start_58
    iget-object v1, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mNvp:Ljava/util/HashMap;

    const-string v2, "platformVersion"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e2

    iget-object v1, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mNvp:Ljava/util/HashMap;

    const-string v2, "platformVersion"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mPlatformVersion:I
    :try_end_72
    .catch Ljava/lang/NumberFormatException; {:try_start_58 .. :try_end_72} :catch_f3

    :cond_72
    :goto_72
    iget-object v1, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mTitle:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_94

    iget-object v1, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mPath:Ljava/lang/String;

    iget-object v2, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mPath:Ljava/lang/String;

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    iget-object v3, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mPath:Ljava/lang/String;

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mTitle:Ljava/lang/String;

    :cond_94
    iget-object v1, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mAuthor:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_ab

    iget-object v1, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x60c000b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mAuthor:Ljava/lang/String;

    :cond_ab
    iget-object v1, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mVersion:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_c1

    const-string v1, "yyyy.MM.d"

    iget-wide v2, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mLastModified:J

    invoke-static {v1, v2, v3}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mVersion:Ljava/lang/String;

    :cond_c1
    return-void

    :cond_c2
    iget-object v1, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mTitle:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1e

    :cond_ca
    iget-object v1, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mAuthor:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_31

    :cond_d2
    iget-object v1, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mDesigner:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_44

    :cond_da
    iget-object v1, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mVersion:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_56

    :cond_e2
    :try_start_e2
    iget-object v1, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mNvp:Ljava/util/HashMap;

    const-string v2, "uiVersion"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mPlatformVersion:I
    :try_end_f2
    .catch Ljava/lang/NumberFormatException; {:try_start_e2 .. :try_end_f2} :catch_f3

    goto :goto_72

    :catch_f3
    move-exception v0

    .local v0, e:Ljava/lang/NumberFormatException;
    const/4 v1, 0x0

    iput v1, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->mPlatformVersion:I

    goto/16 :goto_72
.end method
