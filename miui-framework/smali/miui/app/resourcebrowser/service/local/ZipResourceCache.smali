.class public abstract Lmiui/app/resourcebrowser/service/local/ZipResourceCache;
.super Ljava/lang/Object;
.source "ZipResourceCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/app/resourcebrowser/service/local/ZipResourceCache$DefaultZipCacheImpl;
    }
.end annotation


# instance fields
.field public author:Ljava/lang/String;

.field public designer:Ljava/lang/String;

.field public fileHash:Ljava/lang/String;

.field public filePath:Ljava/lang/String;

.field public fileSize:J

.field public lastModifyTime:J

.field public nvp:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public platformVersion:I

.field public previews:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public thumbnails:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public title:Ljava/lang/String;

.field public version:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->nvp:Ljava/util/HashMap;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->thumbnails:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->previews:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method protected imageCached()Z
    .registers 4

    .prologue
    const/4 v1, 0x0

    iget-object v0, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->thumbnails:Ljava/util/ArrayList;

    if-eqz v0, :cond_26

    iget-object v0, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->previews:Ljava/util/ArrayList;

    if-eqz v0, :cond_26

    iget-object v0, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->thumbnails:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_26

    new-instance v2, Ljava/io/File;

    iget-object v0, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->thumbnails:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_26

    const/4 v0, 0x1

    :goto_25
    return v0

    :cond_26
    move v0, v1

    goto :goto_25
.end method

.method protected readBaiscInformation(Ljava/io/ObjectInputStream;)V
    .registers 4
    .parameter "stream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->filePath:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->fileSize:J

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->lastModifyTime:J

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->title:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->designer:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->author:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->version:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v0

    iput v0, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->platformVersion:I

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    iput-object v0, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->nvp:Ljava/util/HashMap;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->fileHash:Ljava/lang/String;

    return-void
.end method

.method public final readCache(Ljava/io/ObjectInputStream;)V
    .registers 2
    .parameter "stream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    invoke-virtual {p0, p1}, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->readBaiscInformation(Ljava/io/ObjectInputStream;)V

    invoke-virtual {p0, p1}, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->readImageInformation(Ljava/io/ObjectInputStream;)V

    return-void
.end method

.method protected readImageInformation(Ljava/io/ObjectInputStream;)V
    .registers 7
    .parameter "stream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->thumbnails:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    move-object v0, v4

    check-cast v0, [Ljava/lang/String;

    .local v0, arr$:[Ljava/lang/String;
    array-length v2, v0

    .local v2, len$:I
    const/4 v1, 0x0

    .local v1, i$:I
    :goto_12
    if-ge v1, v2, :cond_1e

    aget-object v3, v0, v1

    .local v3, str:Ljava/lang/String;
    iget-object v4, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->thumbnails:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_12

    .end local v3           #str:Ljava/lang/String;
    :cond_1e
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->previews:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    move-object v0, v4

    check-cast v0, [Ljava/lang/String;

    array-length v2, v0

    const/4 v1, 0x0

    :goto_30
    if-ge v1, v2, :cond_3c

    aget-object v3, v0, v1

    .restart local v3       #str:Ljava/lang/String;
    iget-object v4, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->previews:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_30

    .end local v3           #str:Ljava/lang/String;
    :cond_3c
    return-void
.end method

.method public valid()Z
    .registers 7

    .prologue
    const/4 v1, 0x0

    .local v1, ret:Z
    iget-object v2, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->filePath:Ljava/lang/String;

    if-eqz v2, :cond_27

    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->filePath:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .local v0, file:Ljava/io/File;
    iget-wide v2, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->lastModifyTime:J

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-ltz v2, :cond_28

    iget-wide v2, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->fileSize:J

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_28

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->imageCached()Z

    move-result v2

    if-eqz v2, :cond_28

    const/4 v1, 0x1

    .end local v0           #file:Ljava/io/File;
    :cond_27
    :goto_27
    return v1

    .restart local v0       #file:Ljava/io/File;
    :cond_28
    const/4 v1, 0x0

    goto :goto_27
.end method

.method protected writeBaiscInformation(Ljava/io/ObjectOutputStream;)V
    .registers 4
    .parameter "stream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->filePath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-wide v0, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->fileSize:J

    invoke-virtual {p1, v0, v1}, Ljava/io/ObjectOutputStream;->writeLong(J)V

    iget-wide v0, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->lastModifyTime:J

    invoke-virtual {p1, v0, v1}, Ljava/io/ObjectOutputStream;->writeLong(J)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->title:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->designer:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->author:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->version:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget v0, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->platformVersion:I

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->nvp:Ljava/util/HashMap;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->fileHash:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    return-void
.end method

.method public final writeCache(Ljava/io/ObjectOutputStream;)V
    .registers 2
    .parameter "stream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    invoke-virtual {p0, p1}, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->writeBaiscInformation(Ljava/io/ObjectOutputStream;)V

    invoke-virtual {p0, p1}, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->writeImageInformation(Ljava/io/ObjectOutputStream;)V

    return-void
.end method

.method protected writeImageInformation(Ljava/io/ObjectOutputStream;)V
    .registers 4
    .parameter "stream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    iget-object v1, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->thumbnails:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v0, v1, [Ljava/lang/String;

    .local v0, tmp:[Ljava/lang/String;
    iget-object v1, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->thumbnails:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v1, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->previews:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v0, v1, [Ljava/lang/String;

    iget-object v1, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->previews:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    return-void
.end method
