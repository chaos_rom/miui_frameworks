.class public Lmiui/app/resourcebrowser/service/local/AudioResourceFolder;
.super Lmiui/app/resourcebrowser/service/local/ResourceFolder;
.source "AudioResourceFolder.java"


# static fields
.field private static final COMMON_RINGTONE_SUFFIX:[Ljava/lang/String; = null

.field public static final RESOURCE_AUDIO_DURATION:Ljava/lang/String; = "audio_duration"


# instance fields
.field private mDefaultOption:Lmiui/app/resourcebrowser/resource/Resource;

.field private mDefaultUri:Landroid/net/Uri;

.field private mMaxDurationLimit:I

.field private mMinDurationLimit:I

.field private mMuteOption:Lmiui/app/resourcebrowser/resource/Resource;

.field private mRingtoneType:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, ".mp3"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, ".ogg"

    aput-object v2, v0, v1

    sput-object v0, Lmiui/app/resourcebrowser/service/local/AudioResourceFolder;->COMMON_RINGTONE_SUFFIX:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Bundle;Ljava/lang/String;)V
    .registers 5
    .parameter "context"
    .parameter "metaData"
    .parameter "folderPath"

    .prologue
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lmiui/app/resourcebrowser/service/local/AudioResourceFolder;-><init>(Landroid/content/Context;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V
    .registers 14
    .parameter "context"
    .parameter "metaData"
    .parameter "folderPath"
    .parameter "keyword"

    .prologue
    const v4, 0x7fffffff

    const/16 v3, 0x1388

    const/4 v8, 0x2

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, p3, p4}, Lmiui/app/resourcebrowser/service/local/ResourceFolder;-><init>(Landroid/content/Context;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    iput v2, p0, Lmiui/app/resourcebrowser/service/local/AudioResourceFolder;->mMinDurationLimit:I

    iput v4, p0, Lmiui/app/resourcebrowser/service/local/AudioResourceFolder;->mMaxDurationLimit:I

    iget-object v5, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mMetaData:Landroid/os/Bundle;

    const-string v6, "android.intent.extra.ringtone.TYPE"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    iput v5, p0, Lmiui/app/resourcebrowser/service/local/AudioResourceFolder;->mRingtoneType:I

    iget-object v5, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mMetaData:Landroid/os/Bundle;

    const-string v6, "miui.app.resourcebrowser.RINGTONE_MIN_DURATION_LIMIT"

    iget v7, p0, Lmiui/app/resourcebrowser/service/local/AudioResourceFolder;->mRingtoneType:I

    if-ne v7, v8, :cond_34

    :goto_20
    invoke-virtual {v5, v6, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .local v1, minLimit:I
    iget-object v2, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mMetaData:Landroid/os/Bundle;

    const-string v5, "miui.app.resourcebrowser.RINGTONE_MAX_DURATION_LIMIT"

    iget v6, p0, Lmiui/app/resourcebrowser/service/local/AudioResourceFolder;->mRingtoneType:I

    if-ne v6, v8, :cond_36

    :goto_2c
    invoke-virtual {v2, v5, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .local v0, maxLimit:I
    invoke-virtual {p0, v1, v0}, Lmiui/app/resourcebrowser/service/local/AudioResourceFolder;->setDurationLimitation(II)V

    return-void

    .end local v0           #maxLimit:I
    .end local v1           #minLimit:I
    :cond_34
    move v2, v3

    goto :goto_20

    .restart local v1       #minLimit:I
    :cond_36
    move v3, v4

    goto :goto_2c
.end method

.method public static formatDuration(I)Ljava/lang/String;
    .registers 5
    .parameter "duration"

    .prologue
    div-int/lit16 p0, p0, 0x3e8

    const-string v0, "%02d:%02d"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    div-int/lit8 v3, p0, 0x3c

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    rem-int/lit8 v3, p0, 0x3c

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getLocalRingtoneDuration(Ljava/lang/String;)J
    .registers 6
    .parameter "filePath"

    .prologue
    const-wide/16 v0, 0x0

    .local v0, duration:J
    :try_start_2
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_10

    const-wide/16 v0, -0x1

    :goto_f
    return-wide v0

    :cond_10
    new-instance v3, Landroid/media/MediaPlayer;

    invoke-direct {v3}, Landroid/media/MediaPlayer;-><init>()V

    .local v3, mediaPlayer:Landroid/media/MediaPlayer;
    invoke-virtual {v3, p0}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->prepare()V

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v4

    int-to-long v0, v4

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->release()V
    :try_end_23
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_23} :catch_24

    goto :goto_f

    .end local v3           #mediaPlayer:Landroid/media/MediaPlayer;
    :catch_24
    move-exception v2

    .local v2, e:Ljava/lang/Exception;
    const-wide/16 v0, -0x1

    goto :goto_f
.end method

.method private getTitleForDefaultUri()Ljava/lang/String;
    .registers 8

    .prologue
    const/4 v6, 0x0

    const v0, 0x60c0007

    .local v0, resId:I
    iget v3, p0, Lmiui/app/resourcebrowser/service/local/AudioResourceFolder;->mRingtoneType:I

    packed-switch v3, :pswitch_data_4e

    :goto_9
    :pswitch_9
    iget-object v3, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lmiui/app/resourcebrowser/service/local/AudioResourceFolder;->mDefaultUri:Landroid/net/Uri;

    invoke-static {v3, v4, v6}, Landroid/media/ExtraRingtone;->getRingtoneTitle(Landroid/content/Context;Landroid/net/Uri;Z)Ljava/lang/String;

    move-result-object v2

    .local v2, songName:Ljava/lang/String;
    invoke-direct {p0, v2}, Lmiui/app/resourcebrowser/service/local/AudioResourceFolder;->removeFileNameSuffix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v1, ""

    .local v1, retTitle:Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_34

    iget-object v3, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_27
    return-object v1

    .end local v1           #retTitle:Ljava/lang/String;
    .end local v2           #songName:Ljava/lang/String;
    :pswitch_28
    const v0, 0x60c0008

    goto :goto_9

    :pswitch_2c
    const v0, 0x60c0009

    goto :goto_9

    :pswitch_30
    const v0, 0x60c000a

    goto :goto_9

    .restart local v1       #retTitle:Ljava/lang/String;
    .restart local v2       #songName:Ljava/lang/String;
    :cond_34
    const-string v3, "%s (%s)"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    const/4 v5, 0x1

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_27

    nop

    :pswitch_data_4e
    .packed-switch 0x1
        :pswitch_28
        :pswitch_2c
        :pswitch_9
        :pswitch_30
    .end packed-switch
.end method

.method private matchLimitation(J)Z
    .registers 5
    .parameter "duration"

    .prologue
    iget v0, p0, Lmiui/app/resourcebrowser/service/local/AudioResourceFolder;->mMinDurationLimit:I

    int-to-long v0, v0

    cmp-long v0, v0, p1

    if-gtz v0, :cond_10

    iget v0, p0, Lmiui/app/resourcebrowser/service/local/AudioResourceFolder;->mMaxDurationLimit:I

    int-to-long v0, v0

    cmp-long v0, p1, v0

    if-gtz v0, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method private removeFileNameSuffix(Ljava/lang/String;)Ljava/lang/String;
    .registers 9
    .parameter "fileName"

    .prologue
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_21

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    .local v2, ingoreCaseName:Ljava/lang/String;
    sget-object v0, Lmiui/app/resourcebrowser/service/local/AudioResourceFolder;->COMMON_RINGTONE_SUFFIX:[Ljava/lang/String;

    .local v0, arr$:[Ljava/lang/String;
    array-length v3, v0

    .local v3, len$:I
    const/4 v1, 0x0

    .local v1, i$:I
    :goto_e
    if-ge v1, v3, :cond_21

    aget-object v4, v0, v1

    .local v4, suffix:Ljava/lang/String;
    invoke-virtual {v2, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_22

    const/4 v5, 0x0

    invoke-virtual {v2, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {p1, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #i$:I
    .end local v2           #ingoreCaseName:Ljava/lang/String;
    .end local v3           #len$:I
    .end local v4           #suffix:Ljava/lang/String;
    .end local p1
    :cond_21
    return-object p1

    .restart local v0       #arr$:[Ljava/lang/String;
    .restart local v1       #i$:I
    .restart local v2       #ingoreCaseName:Ljava/lang/String;
    .restart local v3       #len$:I
    .restart local v4       #suffix:Ljava/lang/String;
    .restart local p1
    :cond_22
    add-int/lit8 v1, v1, 0x1

    goto :goto_e
.end method


# virtual methods
.method protected addItem(Ljava/lang/String;)V
    .registers 6
    .parameter "filePath"

    .prologue
    invoke-static {p1}, Lmiui/app/resourcebrowser/service/local/AudioResourceFolder;->getLocalRingtoneDuration(Ljava/lang/String;)J

    move-result-wide v0

    .local v0, duration:J
    iget-object v2, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mFileFlags:Ljava/util/Map;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, p1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method protected buildResource(Ljava/lang/String;)Lmiui/app/resourcebrowser/resource/Resource;
    .registers 9
    .parameter "filePath"

    .prologue
    const-wide/16 v5, 0x0

    iget-object v4, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mFileFlags:Ljava/util/Map;

    invoke-interface {v4, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .local v0, duration:J
    cmp-long v4, v0, v5

    if-nez v4, :cond_16

    invoke-static {p1}, Lmiui/app/resourcebrowser/service/local/AudioResourceFolder;->getLocalRingtoneDuration(Ljava/lang/String;)J

    move-result-wide v0

    :cond_16
    cmp-long v4, v0, v5

    if-ltz v4, :cond_20

    invoke-direct {p0, v0, v1}, Lmiui/app/resourcebrowser/service/local/AudioResourceFolder;->matchLimitation(J)Z

    move-result v4

    if-nez v4, :cond_22

    :cond_20
    const/4 v3, 0x0

    :goto_21
    return-object v3

    :cond_22
    invoke-super {p0, p1}, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->buildResource(Ljava/lang/String;)Lmiui/app/resourcebrowser/resource/Resource;

    move-result-object v3

    .local v3, resource:Lmiui/app/resourcebrowser/resource/Resource;
    invoke-virtual {v3}, Lmiui/app/resourcebrowser/resource/Resource;->getInformation()Landroid/os/Bundle;

    move-result-object v2

    .local v2, information:Landroid/os/Bundle;
    const-string v4, "NAME"

    new-instance v5, Ljava/io/File;

    invoke-direct {v5, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lmiui/app/resourcebrowser/service/local/AudioResourceFolder;->removeFileNameSuffix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "audio_duration"

    long-to-int v5, v0

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v3, v2}, Lmiui/app/resourcebrowser/resource/Resource;->setInformation(Landroid/os/Bundle;)V

    goto :goto_21
.end method

.method public enableDefaultOption(Z)V
    .registers 12
    .parameter "enable"

    .prologue
    const-wide/16 v6, 0x0

    const/4 v9, 0x0

    if-eqz p1, :cond_a3

    iget-object v5, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mMetaData:Landroid/os/Bundle;

    const-string v8, "android.intent.extra.ringtone.DEFAULT_URI"

    invoke-virtual {v5, v8}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Landroid/net/Uri;

    iput-object v5, p0, Lmiui/app/resourcebrowser/service/local/AudioResourceFolder;->mDefaultUri:Landroid/net/Uri;

    iget-object v5, p0, Lmiui/app/resourcebrowser/service/local/AudioResourceFolder;->mDefaultUri:Landroid/net/Uri;

    if-nez v5, :cond_1a

    iget v5, p0, Lmiui/app/resourcebrowser/service/local/AudioResourceFolder;->mRingtoneType:I

    packed-switch v5, :pswitch_data_a8

    :cond_1a
    :goto_1a
    :pswitch_1a
    iget-object v5, p0, Lmiui/app/resourcebrowser/service/local/AudioResourceFolder;->mDefaultUri:Landroid/net/Uri;

    if-nez v5, :cond_30

    iput-object v9, p0, Lmiui/app/resourcebrowser/service/local/AudioResourceFolder;->mDefaultOption:Lmiui/app/resourcebrowser/resource/Resource;

    :goto_20
    return-void

    :pswitch_21
    sget-object v5, Landroid/provider/Settings$System;->DEFAULT_RINGTONE_URI:Landroid/net/Uri;

    iput-object v5, p0, Lmiui/app/resourcebrowser/service/local/AudioResourceFolder;->mDefaultUri:Landroid/net/Uri;

    goto :goto_1a

    :pswitch_26
    sget-object v5, Landroid/provider/Settings$System;->DEFAULT_NOTIFICATION_URI:Landroid/net/Uri;

    iput-object v5, p0, Lmiui/app/resourcebrowser/service/local/AudioResourceFolder;->mDefaultUri:Landroid/net/Uri;

    goto :goto_1a

    :pswitch_2b
    sget-object v5, Landroid/provider/Settings$System;->DEFAULT_ALARM_ALERT_URI:Landroid/net/Uri;

    iput-object v5, p0, Lmiui/app/resourcebrowser/service/local/AudioResourceFolder;->mDefaultUri:Landroid/net/Uri;

    goto :goto_1a

    :cond_30
    iget-object v5, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lmiui/app/resourcebrowser/service/local/AudioResourceFolder;->mDefaultUri:Landroid/net/Uri;

    invoke-static {v5, v8}, Lmiui/app/resourcebrowser/util/ResourceHelper;->getPathByUri(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .local v1, filePath:Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_55

    new-instance v5, Ljava/io/File;

    iget-object v8, p0, Lmiui/app/resourcebrowser/service/local/AudioResourceFolder;->mDefaultUri:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_9e

    iget-object v5, p0, Lmiui/app/resourcebrowser/service/local/AudioResourceFolder;->mDefaultUri:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_55
    :goto_55
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .local v0, file:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_a1

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    .local v2, fileSize:J
    :goto_64
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .local v4, information:Landroid/os/Bundle;
    const-string v5, "NAME"

    invoke-direct {p0}, Lmiui/app/resourcebrowser/service/local/AudioResourceFolder;->getTitleForDefaultUri()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "SIZE"

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "MODIFIED_TIME"

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v5, "LOCAL_PATH"

    iget-object v6, p0, Lmiui/app/resourcebrowser/service/local/AudioResourceFolder;->mDefaultUri:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "audio_duration"

    const/4 v6, -0x1

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    new-instance v5, Lmiui/app/resourcebrowser/resource/Resource;

    invoke-direct {v5}, Lmiui/app/resourcebrowser/resource/Resource;-><init>()V

    iput-object v5, p0, Lmiui/app/resourcebrowser/service/local/AudioResourceFolder;->mDefaultOption:Lmiui/app/resourcebrowser/resource/Resource;

    iget-object v5, p0, Lmiui/app/resourcebrowser/service/local/AudioResourceFolder;->mDefaultOption:Lmiui/app/resourcebrowser/resource/Resource;

    invoke-virtual {v5, v4}, Lmiui/app/resourcebrowser/resource/Resource;->setInformation(Landroid/os/Bundle;)V

    goto :goto_20

    .end local v0           #file:Ljava/io/File;
    .end local v2           #fileSize:J
    .end local v4           #information:Landroid/os/Bundle;
    :cond_9e
    const-string v1, ""

    goto :goto_55

    .restart local v0       #file:Ljava/io/File;
    :cond_a1
    move-wide v2, v6

    goto :goto_64

    .end local v0           #file:Ljava/io/File;
    .end local v1           #filePath:Ljava/lang/String;
    :cond_a3
    iput-object v9, p0, Lmiui/app/resourcebrowser/service/local/AudioResourceFolder;->mDefaultOption:Lmiui/app/resourcebrowser/resource/Resource;

    goto/16 :goto_20

    nop

    :pswitch_data_a8
    .packed-switch 0x1
        :pswitch_21
        :pswitch_26
        :pswitch_1a
        :pswitch_2b
    .end packed-switch
.end method

.method public enableMuteOption(Z)V
    .registers 6
    .parameter "enable"

    .prologue
    if-eqz p1, :cond_3d

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .local v0, information:Landroid/os/Bundle;
    const-string v1, "NAME"

    iget-object v2, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mContext:Landroid/content/Context;

    const v3, 0x60c0006

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "SIZE"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "MODIFIED_TIME"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v1, "LOCAL_PATH"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "audio_duration"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    new-instance v1, Lmiui/app/resourcebrowser/resource/Resource;

    invoke-direct {v1}, Lmiui/app/resourcebrowser/resource/Resource;-><init>()V

    iput-object v1, p0, Lmiui/app/resourcebrowser/service/local/AudioResourceFolder;->mMuteOption:Lmiui/app/resourcebrowser/resource/Resource;

    iget-object v1, p0, Lmiui/app/resourcebrowser/service/local/AudioResourceFolder;->mMuteOption:Lmiui/app/resourcebrowser/resource/Resource;

    invoke-virtual {v1, v0}, Lmiui/app/resourcebrowser/resource/Resource;->setInformation(Landroid/os/Bundle;)V

    .end local v0           #information:Landroid/os/Bundle;
    :goto_3c
    return-void

    :cond_3d
    const/4 v1, 0x0

    iput-object v1, p0, Lmiui/app/resourcebrowser/service/local/AudioResourceFolder;->mMuteOption:Lmiui/app/resourcebrowser/resource/Resource;

    goto :goto_3c
.end method

.method public getHeadExtraResource()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lmiui/app/resourcebrowser/resource/Resource;",
            ">;"
        }
    .end annotation

    .prologue
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .local v0, ret:Ljava/util/List;,"Ljava/util/List<Lmiui/app/resourcebrowser/resource/Resource;>;"
    iget-object v1, p0, Lmiui/app/resourcebrowser/service/local/AudioResourceFolder;->mMuteOption:Lmiui/app/resourcebrowser/resource/Resource;

    if-eqz v1, :cond_e

    iget-object v1, p0, Lmiui/app/resourcebrowser/service/local/AudioResourceFolder;->mMuteOption:Lmiui/app/resourcebrowser/resource/Resource;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_e
    iget-object v1, p0, Lmiui/app/resourcebrowser/service/local/AudioResourceFolder;->mDefaultOption:Lmiui/app/resourcebrowser/resource/Resource;

    if-eqz v1, :cond_17

    iget-object v1, p0, Lmiui/app/resourcebrowser/service/local/AudioResourceFolder;->mDefaultOption:Lmiui/app/resourcebrowser/resource/Resource;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_17
    return-object v0
.end method

.method public setDurationLimitation(II)V
    .registers 3
    .parameter "minLimit"
    .parameter "maxLimit"

    .prologue
    iput p1, p0, Lmiui/app/resourcebrowser/service/local/AudioResourceFolder;->mMinDurationLimit:I

    iput p2, p0, Lmiui/app/resourcebrowser/service/local/AudioResourceFolder;->mMaxDurationLimit:I

    return-void
.end method
