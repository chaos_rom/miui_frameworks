.class public Lmiui/app/resourcebrowser/service/local/ImageResourceFolder;
.super Lmiui/app/resourcebrowser/service/local/ResourceFolder;
.source "ImageResourceFolder.java"


# instance fields
.field private mEmptyTranslatePaper:Lmiui/app/resourcebrowser/resource/Resource;

.field private mOptions:Landroid/graphics/BitmapFactory$Options;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Bundle;Ljava/lang/String;)V
    .registers 5
    .parameter "context"
    .parameter "metaData"
    .parameter "folderPath"

    .prologue
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lmiui/app/resourcebrowser/service/local/ImageResourceFolder;-><init>(Landroid/content/Context;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "context"
    .parameter "metaData"
    .parameter "folderPath"
    .parameter "keyword"

    .prologue
    invoke-direct {p0, p1, p2, p3, p4}, Lmiui/app/resourcebrowser/service/local/ResourceFolder;-><init>(Landroid/content/Context;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput-object v0, p0, Lmiui/app/resourcebrowser/service/local/ImageResourceFolder;->mOptions:Landroid/graphics/BitmapFactory$Options;

    iget-object v0, p0, Lmiui/app/resourcebrowser/service/local/ImageResourceFolder;->mOptions:Landroid/graphics/BitmapFactory$Options;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    return-void
.end method

.method private getResource(Ljava/lang/String;)Lmiui/app/resourcebrowser/resource/Resource;
    .registers 10
    .parameter "filePath"

    .prologue
    const/4 v6, 0x0

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .local v0, file:Ljava/io/File;
    new-instance v3, Lmiui/app/resourcebrowser/resource/Resource;

    invoke-direct {v3}, Lmiui/app/resourcebrowser/resource/Resource;-><init>()V

    .local v3, resource:Lmiui/app/resourcebrowser/resource/Resource;
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .local v1, information:Landroid/os/Bundle;
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    .local v4, title:Ljava/lang/String;
    const-string v5, ".jpg"

    invoke-virtual {v4, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_24

    const-string v5, ".png"

    invoke-virtual {v4, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2e

    :cond_24
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x4

    invoke-virtual {v4, v6, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    :cond_2e
    const-string v5, "__"

    invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    if-lez v5, :cond_40

    const-string v5, "__"

    invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v4, v6, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    :cond_40
    const-string v5, "NAME"

    invoke-virtual {v1, v5, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "SIZE"

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "MODIFIED_TIME"

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v6

    invoke-virtual {v1, v5, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v5, "LOCAL_PATH"

    invoke-virtual {v1, v5, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Ljava/util/ArrayList;

    const/4 v5, 0x1

    invoke-direct {v2, v5}, Ljava/util/ArrayList;-><init>(I)V

    .local v2, previews:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v5, "LOCAL_PREVIEW"

    invoke-virtual {v1, v5, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v5, "LOCAL_THUMBNAIL"

    invoke-virtual {v1, v5, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-virtual {v3, v1}, Lmiui/app/resourcebrowser/resource/Resource;->setInformation(Landroid/os/Bundle;)V

    return-object v3
.end method


# virtual methods
.method protected addItem(Ljava/lang/String;)V
    .registers 5
    .parameter "filePath"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/service/local/ImageResourceFolder;->mOptions:Landroid/graphics/BitmapFactory$Options;

    invoke-static {p1, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    iget-object v2, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mFileFlags:Ljava/util/Map;

    iget-object v0, p0, Lmiui/app/resourcebrowser/service/local/ImageResourceFolder;->mOptions:Landroid/graphics/BitmapFactory$Options;

    iget v0, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_18

    const-wide/16 v0, 0x0

    :goto_10
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    :cond_18
    const-wide/16 v0, 0x1

    goto :goto_10
.end method

.method protected buildResource(Ljava/lang/String;)Lmiui/app/resourcebrowser/resource/Resource;
    .registers 6
    .parameter "filePath"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mFileFlags:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_14

    const/4 v0, 0x0

    :goto_13
    return-object v0

    :cond_14
    invoke-direct {p0, p1}, Lmiui/app/resourcebrowser/service/local/ImageResourceFolder;->getResource(Ljava/lang/String;)Lmiui/app/resourcebrowser/resource/Resource;

    move-result-object v0

    goto :goto_13
.end method

.method public enableTransparentWallpaper(Z)V
    .registers 9
    .parameter "enable"

    .prologue
    const-string v2, "%s%s.png"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mFolderPath:Ljava/lang/String;

    invoke-static {v5}, Lmiui/os/ExtraFileUtils;->standardizeFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mContext:Landroid/content/Context;

    const v6, 0x60c0005

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .local v1, filePath:Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .local v0, file:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_38

    iget-object v2, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x605

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v2

    invoke-static {v2, v1}, Lmiui/app/resourcebrowser/util/ResourceHelper;->writeTo(Ljava/io/InputStream;Ljava/lang/String;)V

    :cond_38
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_47

    if-eqz p1, :cond_47

    invoke-direct {p0, v1}, Lmiui/app/resourcebrowser/service/local/ImageResourceFolder;->getResource(Ljava/lang/String;)Lmiui/app/resourcebrowser/resource/Resource;

    move-result-object v2

    iput-object v2, p0, Lmiui/app/resourcebrowser/service/local/ImageResourceFolder;->mEmptyTranslatePaper:Lmiui/app/resourcebrowser/resource/Resource;

    :goto_46
    return-void

    :cond_47
    const/4 v2, 0x0

    iput-object v2, p0, Lmiui/app/resourcebrowser/service/local/ImageResourceFolder;->mEmptyTranslatePaper:Lmiui/app/resourcebrowser/resource/Resource;

    goto :goto_46
.end method

.method public getHeadExtraResource()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lmiui/app/resourcebrowser/resource/Resource;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .local v0, ret:Ljava/util/List;,"Ljava/util/List<Lmiui/app/resourcebrowser/resource/Resource;>;"
    iget-object v1, p0, Lmiui/app/resourcebrowser/service/local/ImageResourceFolder;->mEmptyTranslatePaper:Lmiui/app/resourcebrowser/resource/Resource;

    if-eqz v1, :cond_f

    new-instance v0, Ljava/util/ArrayList;

    .end local v0           #ret:Ljava/util/List;,"Ljava/util/List<Lmiui/app/resourcebrowser/resource/Resource;>;"
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .restart local v0       #ret:Ljava/util/List;,"Ljava/util/List<Lmiui/app/resourcebrowser/resource/Resource;>;"
    iget-object v1, p0, Lmiui/app/resourcebrowser/service/local/ImageResourceFolder;->mEmptyTranslatePaper:Lmiui/app/resourcebrowser/resource/Resource;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_f
    return-object v0
.end method
