.class public abstract Lmiui/app/resourcebrowser/service/local/ResourceFolder;
.super Ljava/lang/Object;
.source "ResourceFolder.java"

# interfaces
.implements Lmiui/widget/AsyncAdapter$AsyncLoadDataVisitor;
.implements Lmiui/app/resourcebrowser/IntentConstants;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lmiui/widget/AsyncAdapter$AsyncLoadDataVisitor",
        "<",
        "Lmiui/app/resourcebrowser/resource/Resource;",
        ">;",
        "Lmiui/app/resourcebrowser/IntentConstants;"
    }
.end annotation


# instance fields
.field protected mCacheFileName:Ljava/lang/String;

.field protected mContext:Landroid/content/Context;

.field protected mDirtyResources:Z

.field protected mFileFlags:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mFirstLoadData:Z

.field protected mFolderDescription:Ljava/lang/String;

.field protected mFolderPath:Ljava/lang/String;

.field protected mKeyword:Ljava/lang/String;

.field protected mMetaData:Landroid/os/Bundle;

.field private mResourceSeq:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Bundle;Ljava/lang/String;)V
    .registers 5
    .parameter "context"
    .parameter "metaData"
    .parameter "folderPath"

    .prologue
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lmiui/app/resourcebrowser/service/local/ResourceFolder;-><init>(Landroid/content/Context;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V
    .registers 12
    .parameter "context"
    .parameter "metaData"
    .parameter "folderPath"
    .parameter "keyword"

    .prologue
    const/16 v6, 0x5f

    const/16 v5, 0x2f

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    const/4 v3, 0x1

    iput-boolean v3, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mFirstLoadData:Z

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mFileFlags:Ljava/util/Map;

    const/4 v3, 0x0

    iput v3, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mResourceSeq:I

    iput-object p1, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mMetaData:Landroid/os/Bundle;

    iput-object p3, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mFolderPath:Ljava/lang/String;

    iput-object p4, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mKeyword:Ljava/lang/String;

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6e

    iget-object v3, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mMetaData:Landroid/os/Bundle;

    const-string v4, "miui.app.resourcebrowser.CACHE_LIST_FOLDER"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lmiui/os/ExtraFileUtils;->standardizeFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .local v0, cacheFolder:Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p3, v5, v6}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .local v2, oldFileName:Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .local v1, file:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_51

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_51
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "cache"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p3, v5, v6}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mCacheFileName:Ljava/lang/String;

    .end local v0           #cacheFolder:Ljava/lang/String;
    .end local v1           #file:Ljava/io/File;
    .end local v2           #oldFileName:Ljava/lang/String;
    :cond_6e
    return-void
.end method

.method private addResourceIntoUI(Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;Lmiui/app/resourcebrowser/resource/Resource;)V
    .registers 5
    .parameter
    .parameter "r"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiui/widget/AsyncAdapter",
            "<",
            "Lmiui/app/resourcebrowser/resource/Resource;",
            ">.Async",
            "LoadDataTask;",
            "Lmiui/app/resourcebrowser/resource/Resource;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, task:Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;,"Lmiui/widget/AsyncAdapter<Lmiui/app/resourcebrowser/resource/Resource;>.AsyncLoadDataTask;"
    if-eqz p2, :cond_1a

    iget v0, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mResourceSeq:I

    if-nez v0, :cond_b

    iget-object v0, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mFolderDescription:Ljava/lang/String;

    invoke-virtual {p2, v0}, Lmiui/app/resourcebrowser/resource/Resource;->setDividerTitle(Ljava/lang/String;)V

    :cond_b
    const/4 v0, 0x1

    new-array v0, v0, [Lmiui/app/resourcebrowser/resource/Resource;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    invoke-virtual {p1, v0}, Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;->onLoadData([Ljava/lang/Object;)V

    iget v0, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mResourceSeq:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mResourceSeq:I

    :cond_1a
    return-void
.end method


# virtual methods
.method protected addItem(Ljava/lang/String;)V
    .registers 2
    .parameter "filePath"

    .prologue
    return-void
.end method

.method protected buildResource(Ljava/lang/String;)Lmiui/app/resourcebrowser/resource/Resource;
    .registers 8
    .parameter "filePath"

    .prologue
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .local v1, information:Landroid/os/Bundle;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .local v0, file:Ljava/io/File;
    const-string v3, "NAME"

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "SIZE"

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "MODIFIED_TIME"

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v4

    invoke-virtual {v1, v3, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v3, "LOCAL_PATH"

    invoke-virtual {v1, v3, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Lmiui/app/resourcebrowser/resource/Resource;

    invoke-direct {v2}, Lmiui/app/resourcebrowser/resource/Resource;-><init>()V

    .local v2, resource:Lmiui/app/resourcebrowser/resource/Resource;
    invoke-virtual {v2, v1}, Lmiui/app/resourcebrowser/resource/Resource;->setInformation(Landroid/os/Bundle;)V

    invoke-static {p1}, Lmiui/app/resourcebrowser/util/ResourceHelper;->getFileHash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lmiui/app/resourcebrowser/resource/Resource;->setFileHash(Ljava/lang/String;)V

    return-object v2
.end method

.method public dataChanged()Z
    .registers 6

    .prologue
    const/4 v3, 0x1

    iget-object v4, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mFolderPath:Ljava/lang/String;

    invoke-static {v4}, Lmiui/app/resourcebrowser/util/ResourceHelper;->getFolderInfoCache(Ljava/lang/String;)Lmiui/cache/FolderCache$FolderInfo;

    move-result-object v0

    .local v0, folderInfo:Lmiui/cache/FolderCache$FolderInfo;
    if-eqz v0, :cond_4d

    iget-object v4, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mFileFlags:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :cond_13
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_28

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .local v2, path:Ljava/lang/String;
    iget-object v4, v0, Lmiui/cache/FolderCache$FolderInfo;->files:Ljava/util/Map;

    invoke-interface {v4, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_13

    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #path:Ljava/lang/String;
    :goto_27
    return v3

    .restart local v1       #i$:Ljava/util/Iterator;
    :cond_28
    iget-object v4, v0, Lmiui/cache/FolderCache$FolderInfo;->files:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_32
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .restart local v2       #path:Ljava/lang/String;
    iget-object v4, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mFileFlags:Ljava/util/Map;

    invoke-interface {v4, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_32

    invoke-virtual {p0, v2}, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->isInterestedResource(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_32

    goto :goto_27

    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #path:Ljava/lang/String;
    :cond_4d
    const/4 v3, 0x0

    goto :goto_27
.end method

.method public getFolderPath()Ljava/lang/String;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mFolderPath:Ljava/lang/String;

    return-object v0
.end method

.method public getHeadExtraResource()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lmiui/app/resourcebrowser/resource/Resource;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    return-object v0
.end method

.method protected isInterestedResource(Ljava/lang/String;)Z
    .registers 3
    .parameter "path"

    .prologue
    const/4 v0, 0x1

    return v0
.end method

.method public final loadData(Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiui/widget/AsyncAdapter",
            "<",
            "Lmiui/app/resourcebrowser/resource/Resource;",
            ">.Async",
            "LoadDataTask;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, task:Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;,"Lmiui/widget/AsyncAdapter<Lmiui/app/resourcebrowser/resource/Resource;>.AsyncLoadDataTask;"
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->onPreLoadData()V

    invoke-virtual {p0, p1}, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->onLoadData(Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;)V

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->onPostLoadData()V

    return-void
.end method

.method protected final onLoadData(Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;)V
    .registers 15
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiui/widget/AsyncAdapter",
            "<",
            "Lmiui/app/resourcebrowser/resource/Resource;",
            ">.Async",
            "LoadDataTask;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, task:Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;,"Lmiui/widget/AsyncAdapter<Lmiui/app/resourcebrowser/resource/Resource;>.AsyncLoadDataTask;"
    const/4 v12, 0x1

    const/4 v11, 0x0

    iput v11, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mResourceSeq:I

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->getHeadExtraResource()Ljava/util/List;

    move-result-object v2

    .local v2, headExtraResource:Ljava/util/List;,"Ljava/util/List<Lmiui/app/resourcebrowser/resource/Resource;>;"
    if-eqz v2, :cond_1e

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, i$:Ljava/util/Iterator;
    :goto_e
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1e

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lmiui/app/resourcebrowser/resource/Resource;

    .local v7, r:Lmiui/app/resourcebrowser/resource/Resource;
    invoke-direct {p0, p1, v7}, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->addResourceIntoUI(Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;Lmiui/app/resourcebrowser/resource/Resource;)V

    goto :goto_e

    .end local v4           #i$:Ljava/util/Iterator;
    .end local v7           #r:Lmiui/app/resourcebrowser/resource/Resource;
    :cond_1e
    iget-object v10, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mFolderPath:Ljava/lang/String;

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_27

    :cond_26
    :goto_26
    return-void

    :cond_27
    iget-boolean v10, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mFirstLoadData:Z

    if-eqz v10, :cond_30

    :try_start_2b
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->readCache()V
    :try_end_2e
    .catch Ljava/io/FileNotFoundException; {:try_start_2b .. :try_end_2e} :catch_60
    .catch Ljava/lang/Exception; {:try_start_2b .. :try_end_2e} :catch_65

    :goto_2e
    iput-boolean v11, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mFirstLoadData:Z

    :cond_30
    iput-boolean v11, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mDirtyResources:Z

    iget-object v10, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mFolderPath:Ljava/lang/String;

    invoke-static {v10}, Lmiui/app/resourcebrowser/util/ResourceHelper;->getFolderInfoCache(Ljava/lang/String;)Lmiui/cache/FolderCache$FolderInfo;

    move-result-object v1

    .local v1, folderInfo:Lmiui/cache/FolderCache$FolderInfo;
    if-eqz v1, :cond_d8

    new-instance v5, Ljava/util/ArrayList;

    iget-object v10, v1, Lmiui/cache/FolderCache$FolderInfo;->files:Ljava/util/Map;

    invoke-interface {v10}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v10

    invoke-direct {v5, v10}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .local v5, list:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0, v5, v1}, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->sortResource(Ljava/util/List;Lmiui/cache/FolderCache$FolderInfo;)V

    const/4 v3, 0x0

    .local v3, i:I
    :goto_49
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v10

    if-ge v3, v10, :cond_b0

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .local v6, path:Ljava/lang/String;
    const-string v10, ".temp"

    invoke-virtual {v6, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_6d

    :cond_5d
    :goto_5d
    add-int/lit8 v3, v3, 0x1

    goto :goto_49

    .end local v1           #folderInfo:Lmiui/cache/FolderCache$FolderInfo;
    .end local v3           #i:I
    .end local v5           #list:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .end local v6           #path:Ljava/lang/String;
    :catch_60
    move-exception v0

    .local v0, e:Ljava/io/FileNotFoundException;
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->reset()V

    goto :goto_2e

    .end local v0           #e:Ljava/io/FileNotFoundException;
    :catch_65
    move-exception v0

    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    invoke-virtual {p0}, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->reset()V

    goto :goto_2e

    .end local v0           #e:Ljava/lang/Exception;
    .restart local v1       #folderInfo:Lmiui/cache/FolderCache$FolderInfo;
    .restart local v3       #i:I
    .restart local v5       #list:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .restart local v6       #path:Ljava/lang/String;
    :cond_6d
    iget-object v10, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mFileFlags:Ljava/util/Map;

    invoke-interface {v10, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_7a

    invoke-virtual {p0, v6}, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->addItem(Ljava/lang/String;)V

    iput-boolean v12, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mDirtyResources:Z

    :cond_7a
    invoke-virtual {p0, v6}, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->buildResource(Ljava/lang/String;)Lmiui/app/resourcebrowser/resource/Resource;

    move-result-object v8

    .local v8, resource:Lmiui/app/resourcebrowser/resource/Resource;
    if-eqz v8, :cond_5d

    invoke-virtual {v8}, Lmiui/app/resourcebrowser/resource/Resource;->getTitle()Ljava/lang/String;

    move-result-object v9

    .local v9, title:Ljava/lang/String;
    iget-object v10, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mKeyword:Ljava/lang/String;

    if-eqz v10, :cond_ac

    if-eqz v9, :cond_96

    invoke-virtual {v9}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v10

    iget-object v11, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mKeyword:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_ac

    :cond_96
    iget-object v10, v1, Lmiui/cache/FolderCache$FolderInfo;->files:Ljava/util/Map;

    invoke-interface {v10, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lmiui/cache/FolderCache$FileInfo;

    iget-object v10, v10, Lmiui/cache/FolderCache$FileInfo;->name:Ljava/lang/String;

    invoke-virtual {v10}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v10

    iget-object v11, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mKeyword:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_5d

    :cond_ac
    invoke-direct {p0, p1, v8}, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->addResourceIntoUI(Lmiui/widget/AsyncAdapter$AsyncLoadDataTask;Lmiui/app/resourcebrowser/resource/Resource;)V

    goto :goto_5d

    .end local v6           #path:Ljava/lang/String;
    .end local v8           #resource:Lmiui/app/resourcebrowser/resource/Resource;
    .end local v9           #title:Ljava/lang/String;
    :cond_b0
    new-instance v5, Ljava/util/ArrayList;

    .end local v5           #list:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    iget-object v10, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mFileFlags:Ljava/util/Map;

    invoke-interface {v10}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v10

    invoke-direct {v5, v10}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .restart local v5       #list:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const/4 v3, 0x0

    :goto_bc
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v10

    if-ge v3, v10, :cond_d8

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .restart local v6       #path:Ljava/lang/String;
    iget-object v10, v1, Lmiui/cache/FolderCache$FolderInfo;->files:Ljava/util/Map;

    invoke-interface {v10, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_d5

    invoke-virtual {p0, v6}, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->removeItem(Ljava/lang/String;)V

    iput-boolean v12, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mDirtyResources:Z

    :cond_d5
    add-int/lit8 v3, v3, 0x1

    goto :goto_bc

    .end local v3           #i:I
    .end local v5           #list:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .end local v6           #path:Ljava/lang/String;
    :cond_d8
    iget-boolean v10, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mDirtyResources:Z

    if-eqz v10, :cond_26

    :try_start_dc
    invoke-virtual {p0}, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->saveCache()V
    :try_end_df
    .catch Ljava/lang/Exception; {:try_start_dc .. :try_end_df} :catch_e1

    goto/16 :goto_26

    :catch_e1
    move-exception v0

    .restart local v0       #e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_26
.end method

.method protected onPostLoadData()V
    .registers 1

    .prologue
    return-void
.end method

.method protected onPreLoadData()V
    .registers 1

    .prologue
    return-void
.end method

.method protected readCache()V
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    new-instance v0, Ljava/io/File;

    iget-object v3, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mCacheFileName:Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .local v0, file:Ljava/io/File;
    const/4 v1, 0x0

    .local v1, objStream:Ljava/io/ObjectInputStream;
    :try_start_8
    new-instance v2, Ljava/io/ObjectInputStream;

    new-instance v3, Ljava/io/BufferedInputStream;

    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const/16 v5, 0x4000

    invoke-direct {v3, v4, v5}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    invoke-direct {v2, v3}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_19
    .catchall {:try_start_8 .. :try_end_19} :catchall_22

    .end local v1           #objStream:Ljava/io/ObjectInputStream;
    .local v2, objStream:Ljava/io/ObjectInputStream;
    :try_start_19
    invoke-virtual {p0, v2}, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->readDataFromStream(Ljava/io/ObjectInputStream;)V
    :try_end_1c
    .catchall {:try_start_19 .. :try_end_1c} :catchall_29

    if-eqz v2, :cond_21

    invoke-virtual {v2}, Ljava/io/ObjectInputStream;->close()V

    :cond_21
    return-void

    .end local v2           #objStream:Ljava/io/ObjectInputStream;
    .restart local v1       #objStream:Ljava/io/ObjectInputStream;
    :catchall_22
    move-exception v3

    :goto_23
    if-eqz v1, :cond_28

    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->close()V

    :cond_28
    throw v3

    .end local v1           #objStream:Ljava/io/ObjectInputStream;
    .restart local v2       #objStream:Ljava/io/ObjectInputStream;
    :catchall_29
    move-exception v3

    move-object v1, v2

    .end local v2           #objStream:Ljava/io/ObjectInputStream;
    .restart local v1       #objStream:Ljava/io/ObjectInputStream;
    goto :goto_23
.end method

.method protected readDataFromStream(Ljava/io/ObjectInputStream;)V
    .registers 3
    .parameter "objStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    iput-object v0, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mFileFlags:Ljava/util/Map;

    return-void
.end method

.method protected removeItem(Ljava/lang/String;)V
    .registers 3
    .parameter "filePath"

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mFileFlags:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method protected reset()V
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mFileFlags:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    return-void
.end method

.method protected saveCache()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    new-instance v0, Ljava/io/File;

    iget-object v3, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mCacheFileName:Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .local v0, file:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    const/4 v1, 0x0

    .local v1, objStream:Ljava/io/ObjectOutputStream;
    :try_start_b
    new-instance v2, Ljava/io/ObjectOutputStream;

    new-instance v3, Ljava/io/BufferedOutputStream;

    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v2, v3}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1a
    .catchall {:try_start_b .. :try_end_1a} :catchall_23

    .end local v1           #objStream:Ljava/io/ObjectOutputStream;
    .local v2, objStream:Ljava/io/ObjectOutputStream;
    :try_start_1a
    invoke-virtual {p0, v2}, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->writeDataToStream(Ljava/io/ObjectOutputStream;)V
    :try_end_1d
    .catchall {:try_start_1a .. :try_end_1d} :catchall_2a

    if-eqz v2, :cond_22

    invoke-virtual {v2}, Ljava/io/ObjectOutputStream;->close()V

    :cond_22
    return-void

    .end local v2           #objStream:Ljava/io/ObjectOutputStream;
    .restart local v1       #objStream:Ljava/io/ObjectOutputStream;
    :catchall_23
    move-exception v3

    :goto_24
    if-eqz v1, :cond_29

    invoke-virtual {v1}, Ljava/io/ObjectOutputStream;->close()V

    :cond_29
    throw v3

    .end local v1           #objStream:Ljava/io/ObjectOutputStream;
    .restart local v2       #objStream:Ljava/io/ObjectOutputStream;
    :catchall_2a
    move-exception v3

    move-object v1, v2

    .end local v2           #objStream:Ljava/io/ObjectOutputStream;
    .restart local v1       #objStream:Ljava/io/ObjectOutputStream;
    goto :goto_24
.end method

.method public setFolderDescription(Ljava/lang/String;)V
    .registers 2
    .parameter "folderTitle"

    .prologue
    iput-object p1, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mFolderDescription:Ljava/lang/String;

    return-void
.end method

.method public setKeyword(Ljava/lang/String;)V
    .registers 3
    .parameter "keyword"

    .prologue
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mKeyword:Ljava/lang/String;

    return-void
.end method

.method protected sortResource(Ljava/util/List;Lmiui/cache/FolderCache$FolderInfo;)V
    .registers 4
    .parameter
    .parameter "folderInfo"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lmiui/cache/FolderCache$FolderInfo;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, list:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mFolderPath:Ljava/lang/String;

    invoke-static {v0}, Lmiui/app/resourcebrowser/util/ResourceHelper;->isSystemResource(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_10

    iget-object v0, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mFolderPath:Ljava/lang/String;

    invoke-static {v0}, Lmiui/app/resourcebrowser/util/ResourceHelper;->isDataResource(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    :cond_10
    invoke-static {p1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    :goto_13
    return-void

    :cond_14
    new-instance v0, Lmiui/app/resourcebrowser/service/local/ResourceFolder$1;

    invoke-direct {v0, p0, p2}, Lmiui/app/resourcebrowser/service/local/ResourceFolder$1;-><init>(Lmiui/app/resourcebrowser/service/local/ResourceFolder;Lmiui/cache/FolderCache$FolderInfo;)V

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_13
.end method

.method protected writeDataToStream(Ljava/io/ObjectOutputStream;)V
    .registers 3
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    iget-object v0, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mFileFlags:Ljava/util/Map;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    return-void
.end method
