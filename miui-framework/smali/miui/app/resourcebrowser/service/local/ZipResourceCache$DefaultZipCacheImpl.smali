.class public final Lmiui/app/resourcebrowser/service/local/ZipResourceCache$DefaultZipCacheImpl;
.super Lmiui/app/resourcebrowser/service/local/ZipResourceCache;
.source "ZipResourceCache.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/app/resourcebrowser/service/local/ZipResourceCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DefaultZipCacheImpl"
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    invoke-direct {p0}, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;-><init>()V

    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .registers 2
    .parameter "stream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    invoke-virtual {p0, p1}, Lmiui/app/resourcebrowser/service/local/ZipResourceCache$DefaultZipCacheImpl;->readCache(Ljava/io/ObjectInputStream;)V

    return-void
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .registers 2
    .parameter "stream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    invoke-virtual {p0, p1}, Lmiui/app/resourcebrowser/service/local/ZipResourceCache$DefaultZipCacheImpl;->writeCache(Ljava/io/ObjectOutputStream;)V

    return-void
.end method
