.class public Lmiui/app/resourcebrowser/service/local/ZipResourceFolder;
.super Lmiui/app/resourcebrowser/service/local/ResourceFolder;
.source "ZipResourceFolder.java"


# instance fields
.field private mZipResourceInfoCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lmiui/app/resourcebrowser/service/local/ZipResourceCache;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Bundle;Ljava/lang/String;)V
    .registers 5
    .parameter "context"
    .parameter "metaData"
    .parameter "folderPath"

    .prologue
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lmiui/app/resourcebrowser/service/local/ZipResourceFolder;-><init>(Landroid/content/Context;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "context"
    .parameter "metaData"
    .parameter "folderPath"
    .parameter "keyword"

    .prologue
    invoke-direct {p0, p1, p2, p3, p4}, Lmiui/app/resourcebrowser/service/local/ResourceFolder;-><init>(Landroid/content/Context;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceFolder;->mZipResourceInfoCache:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method protected addItem(Ljava/lang/String;)V
    .registers 2
    .parameter "filePath"

    .prologue
    invoke-super {p0, p1}, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->addItem(Ljava/lang/String;)V

    return-void
.end method

.method protected buildResource(Ljava/lang/String;)Lmiui/app/resourcebrowser/resource/Resource;
    .registers 11
    .parameter "filePath"

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    iget-object v6, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceFolder;->mZipResourceInfoCache:Ljava/util/Map;

    invoke-interface {v6, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;

    .local v2, oldCache:Lmiui/app/resourcebrowser/service/local/ZipResourceCache;
    iget-object v6, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v6, p1, v2}, Lmiui/app/resourcebrowser/service/local/ZipResourceFolder;->createZipResource(Landroid/content/Context;Ljava/lang/String;Lmiui/app/resourcebrowser/service/local/ZipResourceCache;)Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;

    move-result-object v0

    .local v0, info:Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;
    if-nez v0, :cond_14

    move-object v3, v5

    :cond_13
    :goto_13
    return-object v3

    :cond_14
    invoke-virtual {v0}, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->getCache()Lmiui/app/resourcebrowser/service/local/ZipResourceCache;

    move-result-object v1

    .local v1, newCache:Lmiui/app/resourcebrowser/service/local/ZipResourceCache;
    iget-object v6, v1, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->fileHash:Ljava/lang/String;

    if-nez v6, :cond_4c

    invoke-static {p1}, Lmiui/app/resourcebrowser/util/ResourceHelper;->isSystemResource(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_4c

    sget-boolean v6, Lmiui/app/resourcebrowser/util/ResourceDebug;->DEBUG:Z

    if-eqz v6, :cond_44

    const-string v6, "Theme"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Recompute file hash for resource: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " because of invalid cache."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_44
    invoke-static {p1}, Lmiui/app/resourcebrowser/util/ResourceHelper;->getFileHash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v1, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->fileHash:Ljava/lang/String;

    iput-boolean v4, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mDirtyResources:Z

    :cond_4c
    iget-object v6, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceFolder;->mZipResourceInfoCache:Ljava/util/Map;

    invoke-interface {v6, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-boolean v6, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mDirtyResources:Z

    if-nez v6, :cond_57

    if-eq v1, v2, :cond_89

    :cond_57
    :goto_57
    iput-boolean v4, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mDirtyResources:Z

    invoke-virtual {v0}, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->clearCache()V

    new-instance v3, Lmiui/app/resourcebrowser/resource/Resource;

    invoke-direct {v3}, Lmiui/app/resourcebrowser/resource/Resource;-><init>()V

    .local v3, resource:Lmiui/app/resourcebrowser/resource/Resource;
    invoke-virtual {v0}, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->getInformation()Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v3, v4}, Lmiui/app/resourcebrowser/resource/Resource;->setInformation(Landroid/os/Bundle;)V

    iget-object v4, v1, Lmiui/app/resourcebrowser/service/local/ZipResourceCache;->fileHash:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lmiui/app/resourcebrowser/resource/Resource;->setFileHash(Ljava/lang/String;)V

    invoke-virtual {v3}, Lmiui/app/resourcebrowser/resource/Resource;->getPlatformVersion()I

    move-result v4

    iget-object v6, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mMetaData:Landroid/os/Bundle;

    const-string v7, "miui.app.resourcebrowser.PLATFORM_VERSION_START"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    iget-object v7, p0, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->mMetaData:Landroid/os/Bundle;

    const-string v8, "miui.app.resourcebrowser.PLATFORM_VERSION_END"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    invoke-static {v4, v6, v7}, Lmiui/app/resourcebrowser/util/ResourceHelper;->isCompatiblePlatformVersion(III)Z

    move-result v4

    if-nez v4, :cond_13

    move-object v3, v5

    goto :goto_13

    .end local v3           #resource:Lmiui/app/resourcebrowser/resource/Resource;
    :cond_89
    const/4 v4, 0x0

    goto :goto_57
.end method

.method protected createZipResource(Landroid/content/Context;Ljava/lang/String;Lmiui/app/resourcebrowser/service/local/ZipResourceCache;)Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;
    .registers 5
    .parameter "context"
    .parameter "filePath"
    .parameter "zipCache"

    .prologue
    invoke-static {p2}, Lmiui/app/resourcebrowser/util/ResourceHelper;->isZipResource(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    const/4 v0, 0x0

    :goto_7
    return-object v0

    :cond_8
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1, p2, p3, v0}, Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;->createZipResourceInfo(Landroid/content/Context;Ljava/lang/String;Lmiui/app/resourcebrowser/service/local/ZipResourceCache;[Ljava/lang/Object;)Lmiui/app/resourcebrowser/service/local/ZipResourceInfo;

    move-result-object v0

    goto :goto_7
.end method

.method protected isInterestedResource(Ljava/lang/String;)Z
    .registers 3
    .parameter "path"

    .prologue
    invoke-static {p1}, Lmiui/app/resourcebrowser/util/ResourceHelper;->isZipResource(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected readDataFromStream(Ljava/io/ObjectInputStream;)V
    .registers 3
    .parameter "objStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    invoke-super {p0, p1}, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->readDataFromStream(Ljava/io/ObjectInputStream;)V

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    iput-object v0, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceFolder;->mZipResourceInfoCache:Ljava/util/Map;

    return-void
.end method

.method protected removeItem(Ljava/lang/String;)V
    .registers 3
    .parameter "filePath"

    .prologue
    invoke-super {p0, p1}, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->removeItem(Ljava/lang/String;)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceFolder;->mZipResourceInfoCache:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method protected reset()V
    .registers 2

    .prologue
    invoke-super {p0}, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->reset()V

    iget-object v0, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceFolder;->mZipResourceInfoCache:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    return-void
.end method

.method protected writeDataToStream(Ljava/io/ObjectOutputStream;)V
    .registers 3
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    invoke-super {p0, p1}, Lmiui/app/resourcebrowser/service/local/ResourceFolder;->writeDataToStream(Ljava/io/ObjectOutputStream;)V

    iget-object v0, p0, Lmiui/app/resourcebrowser/service/local/ZipResourceFolder;->mZipResourceInfoCache:Ljava/util/Map;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    return-void
.end method
