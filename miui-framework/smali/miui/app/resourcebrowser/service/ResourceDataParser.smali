.class public abstract Lmiui/app/resourcebrowser/service/ResourceDataParser;
.super Ljava/lang/Object;
.source "ResourceDataParser.java"


# static fields
.field protected static final BUFFER_SIZE:I = 0x400


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract buildCategories(Ljava/io/File;Landroid/os/Bundle;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Landroid/os/Bundle;",
            ")",
            "Ljava/util/List",
            "<",
            "Lmiui/app/resourcebrowser/resource/ResourceCategory;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method public abstract buildListMetaData(Ljava/io/File;Landroid/os/Bundle;)Lmiui/app/resourcebrowser/resource/ListMetaData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method public abstract buildRecommendations(Ljava/io/File;Landroid/os/Bundle;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Landroid/os/Bundle;",
            ")",
            "Ljava/util/List",
            "<",
            "Lmiui/app/resourcebrowser/recommended/RecommendItemData;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method public abstract buildResource(Ljava/io/File;Landroid/os/Bundle;)Lmiui/app/resourcebrowser/resource/Resource;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method public abstract buildResources(Ljava/io/File;Landroid/os/Bundle;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Landroid/os/Bundle;",
            ")",
            "Ljava/util/List",
            "<",
            "Lmiui/app/resourcebrowser/resource/Resource;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method public abstract buildUpdatableResources(Ljava/io/File;Landroid/os/Bundle;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Landroid/os/Bundle;",
            ")",
            "Ljava/util/List",
            "<",
            "Lmiui/app/resourcebrowser/resource/Resource;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method public readCategories(Ljava/lang/String;Landroid/os/Bundle;)Ljava/util/List;
    .registers 6
    .parameter "filePath"
    .parameter "metaData"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/os/Bundle;",
            ")",
            "Ljava/util/List",
            "<",
            "Lmiui/app/resourcebrowser/resource/ResourceCategory;",
            ">;"
        }
    .end annotation

    .prologue
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .local v1, file:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_14

    :try_start_b
    invoke-virtual {p0, v1, p2}, Lmiui/app/resourcebrowser/service/ResourceDataParser;->buildCategories(Ljava/io/File;Landroid/os/Bundle;)Ljava/util/List;
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_e} :catch_10

    move-result-object v2

    :goto_f
    return-object v2

    :catch_10
    move-exception v0

    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .end local v0           #e:Ljava/lang/Exception;
    :cond_14
    const/4 v2, 0x0

    goto :goto_f
.end method

.method public readListMetaData(Ljava/lang/String;Landroid/os/Bundle;)Lmiui/app/resourcebrowser/resource/ListMetaData;
    .registers 6
    .parameter "filePath"
    .parameter "metaData"

    .prologue
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .local v1, file:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_14

    :try_start_b
    invoke-virtual {p0, v1, p2}, Lmiui/app/resourcebrowser/service/ResourceDataParser;->buildListMetaData(Ljava/io/File;Landroid/os/Bundle;)Lmiui/app/resourcebrowser/resource/ListMetaData;
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_e} :catch_10

    move-result-object v2

    :goto_f
    return-object v2

    :catch_10
    move-exception v0

    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .end local v0           #e:Ljava/lang/Exception;
    :cond_14
    const/4 v2, 0x0

    goto :goto_f
.end method

.method public readRecommendations(Ljava/lang/String;Landroid/os/Bundle;)Ljava/util/List;
    .registers 6
    .parameter "filePath"
    .parameter "metaData"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/os/Bundle;",
            ")",
            "Ljava/util/List",
            "<",
            "Lmiui/app/resourcebrowser/recommended/RecommendItemData;",
            ">;"
        }
    .end annotation

    .prologue
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .local v1, file:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_14

    :try_start_b
    invoke-virtual {p0, v1, p2}, Lmiui/app/resourcebrowser/service/ResourceDataParser;->buildRecommendations(Ljava/io/File;Landroid/os/Bundle;)Ljava/util/List;
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_e} :catch_10

    move-result-object v2

    :goto_f
    return-object v2

    :catch_10
    move-exception v0

    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .end local v0           #e:Ljava/lang/Exception;
    :cond_14
    const/4 v2, 0x0

    goto :goto_f
.end method

.method public readResource(Ljava/lang/String;Landroid/os/Bundle;)Lmiui/app/resourcebrowser/resource/Resource;
    .registers 6
    .parameter "filePath"
    .parameter "metaData"

    .prologue
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .local v1, file:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_14

    :try_start_b
    invoke-virtual {p0, v1, p2}, Lmiui/app/resourcebrowser/service/ResourceDataParser;->buildResource(Ljava/io/File;Landroid/os/Bundle;)Lmiui/app/resourcebrowser/resource/Resource;
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_e} :catch_10

    move-result-object v2

    :goto_f
    return-object v2

    :catch_10
    move-exception v0

    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .end local v0           #e:Ljava/lang/Exception;
    :cond_14
    const/4 v2, 0x0

    goto :goto_f
.end method

.method public readResources(Ljava/lang/String;Landroid/os/Bundle;)Ljava/util/List;
    .registers 6
    .parameter "filePath"
    .parameter "metaData"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/os/Bundle;",
            ")",
            "Ljava/util/List",
            "<",
            "Lmiui/app/resourcebrowser/resource/Resource;",
            ">;"
        }
    .end annotation

    .prologue
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .local v1, file:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_14

    :try_start_b
    invoke-virtual {p0, v1, p2}, Lmiui/app/resourcebrowser/service/ResourceDataParser;->buildResources(Ljava/io/File;Landroid/os/Bundle;)Ljava/util/List;
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_e} :catch_10

    move-result-object v2

    :goto_f
    return-object v2

    :catch_10
    move-exception v0

    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .end local v0           #e:Ljava/lang/Exception;
    :cond_14
    const/4 v2, 0x0

    goto :goto_f
.end method

.method public readUpdatableResources(Ljava/lang/String;Landroid/os/Bundle;)Ljava/util/List;
    .registers 6
    .parameter "filePath"
    .parameter "metaData"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/os/Bundle;",
            ")",
            "Ljava/util/List",
            "<",
            "Lmiui/app/resourcebrowser/resource/Resource;",
            ">;"
        }
    .end annotation

    .prologue
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .local v1, file:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_14

    :try_start_b
    invoke-virtual {p0, v1, p2}, Lmiui/app/resourcebrowser/service/ResourceDataParser;->buildUpdatableResources(Ljava/io/File;Landroid/os/Bundle;)Ljava/util/List;
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_e} :catch_10

    move-result-object v2

    :goto_f
    return-object v2

    :catch_10
    move-exception v0

    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .end local v0           #e:Ljava/lang/Exception;
    :cond_14
    const/4 v2, 0x0

    goto :goto_f
.end method
