.class public interface abstract Lmiui/app/resourcebrowser/service/online/OnlineProtocolConstants;
.super Ljava/lang/Object;
.source "OnlineProtocolConstants.java"


# static fields
.field public static final ATTR_AUTHOR:Ljava/lang/String; = "author"

.field public static final ATTR_CATEGORY_NAME:Ljava/lang/String; = "clazzNameMap"

.field public static final ATTR_CATEGORY_ORDER:Ljava/lang/String; = "sortList"

.field public static final ATTR_COMPONENT_FLAG:Ljava/lang/String; = "modulesFlag"

.field public static final ATTR_COMPONENT_ID:Ljava/lang/String; = "moduleId"

.field public static final ATTR_COMPONENT_TYPE:Ljava/lang/String; = "moduleType"

.field public static final ATTR_CREATED_TIME:Ljava/lang/String; = "createTime"

.field public static final ATTR_DESIGNER:Ljava/lang/String; = "designer"

.field public static final ATTR_DOWNLOAD_COUNT:Ljava/lang/String; = "downloads"

.field public static final ATTR_FILE_SIZE:Ljava/lang/String; = "fileSize"

.field public static final ATTR_LAYOUT_COLUMN:Ljava/lang/String; = "layoutCol"

.field public static final ATTR_LAYOUT_COLUMNS:Ljava/lang/String; = "layoutCols"

.field public static final ATTR_LAYOUT_LEFT:Ljava/lang/String; = "layoutLeft"

.field public static final ATTR_LAYOUT_ROW:Ljava/lang/String; = "layoutRow"

.field public static final ATTR_LAYOUT_TOP:Ljava/lang/String; = "layoutTop"

.field public static final ATTR_LONG_DESCRITION:Ljava/lang/String; = "description"

.field public static final ATTR_MODIFIED_TIME:Ljava/lang/String; = "modifyTime"

.field public static final ATTR_NAME:Ljava/lang/String; = "name"

.field public static final ATTR_PLATFORM_VERSION:Ljava/lang/String; = "uiVersion"

.field public static final ATTR_PREVIEWS:Ljava/lang/String; = "snapshotsUrl"

.field public static final ATTR_RECOMMENDATIONS:Ljava/lang/String; = "recommendations"

.field public static final ATTR_RECOMMENDATION_ID:Ljava/lang/String; = "relatedId"

.field public static final ATTR_RECOMMENDATION_SUBJECTS:Ljava/lang/String; = "subjects"

.field public static final ATTR_RECOMMENDATION_THUMBNAIL:Ljava/lang/String; = "picUrl"

.field public static final ATTR_RECOMMENDATION_TITLE:Ljava/lang/String; = "title"

.field public static final ATTR_RECOMMENDATION_TYPE:Ljava/lang/String; = "type"

.field public static final ATTR_RECOMMENDATION_URL_ROOT:Ljava/lang/String; = "picUrlRoot"

.field public static final ATTR_RESOURCE_ID:Ljava/lang/String; = "assemblyId"

.field public static final ATTR_SHORT_DESCRIPTION:Ljava/lang/String; = "brief"

.field public static final ATTR_STAR:Ljava/lang/String; = "star"

.field public static final ATTR_STAR_DETAIL:Ljava/lang/String; = "starCount"

.field public static final ATTR_SUB_RECOMMENDATION_ID:Ljava/lang/String; = "id"

.field public static final ATTR_SUB_RECOMMENDATION_THUMBNAIL:Ljava/lang/String; = "picture"

.field public static final ATTR_SUB_RECOMMENDATION_TITLE:Ljava/lang/String; = "name"

.field public static final ATTR_SUB_RECOMMENDATION_URL_ROOT:Ljava/lang/String; = "picUrlRoot"

.field public static final ATTR_TAG:Ljava/lang/String; = "tags"

.field public static final ATTR_THUMBNAILS:Ljava/lang/String; = "frontCover"

.field public static final ATTR_TYPE:Ljava/lang/String; = "category"

.field public static final ATTR_URL_ROOT:Ljava/lang/String; = "downloadUrlRoot"

.field public static final ATTR_VERSION:Ljava/lang/String; = "version"

.field public static final PARAM_CATEGORY_CODE:Ljava/lang/String; = "clazz"

.field public static final PARAM_COUNT:Ljava/lang/String; = "count"

.field public static final PARAM_DEVICE:Ljava/lang/String; = "device"

.field public static final PARAM_HASH:Ljava/lang/String; = "fileshash"

.field public static final PARAM_KEYWORD:Ljava/lang/String; = "keywords"

.field public static final PARAM_PLATFORM_VERSION:Ljava/lang/String; = "version"

.field public static final PARAM_ROM:Ljava/lang/String; = "system"

.field public static final PARAM_SORT_BY:Ljava/lang/String; = "sortby"

.field public static final PARAM_START:Ljava/lang/String; = "start"

.field public static final PARAM_TYPE:Ljava/lang/String; = "category"

.field public static final URL_CATEGORY:Ljava/lang/String; = "http://market.xiaomi.com/thm/config/clazz/%s/zh-cn?"

.field public static final URL_COMMON_LIST:Ljava/lang/String; = "http://market.xiaomi.com/thm/subject/index?category=%s"

.field public static final URL_DETAIL:Ljava/lang/String; = "http://market.xiaomi.com/thm/details/%s?"

.field public static final URL_DOWNLOAD:Ljava/lang/String; = "http://market.xiaomi.com/thm/download/%s?"

.field public static final URL_LIST_METADATA:Ljava/lang/String; = "http://market.xiaomi.com/thm/subject/metadata/%s?"

.field public static final URL_PART_CATEGORY:Ljava/lang/String; = "&clazz=%s"

.field public static final URL_PART_IMAGE:Ljava/lang/String; = "jpeg/w%s/"

.field public static final URL_PART_PAGINATION:Ljava/lang/String; = "&start=%s&count=%s"

.field public static final URL_PREFIX:Ljava/lang/String; = "http://market.xiaomi.com/thm/"

.field public static final URL_RECOMMENDATION:Ljava/lang/String; = "http://market.xiaomi.com/thm/recommendation/list/%s?"

.field public static final URL_RECOMMENDATION_LIST:Ljava/lang/String; = "http://market.xiaomi.com/thm/subject/%s?category=%s"

.field public static final URL_SEARCH_LIST:Ljava/lang/String; = "http://market.xiaomi.com/thm/search?apiversion=1&keywords=%s&category=%s"

.field public static final URL_SUFFIX_USER:Ljava/lang/String; = "&imei=%s&user=%s&token=%s"

.field public static final URL_SUFFIX_VERSION:Ljava/lang/String; = "&device=%s&system=%s&version=%s&imei=%s"

.field public static final URL_UPDATE:Ljava/lang/String; = "http://market.xiaomi.com/thm/checkupdate?fileshash=%s"
