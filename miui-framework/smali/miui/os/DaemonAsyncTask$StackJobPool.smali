.class public Lmiui/os/DaemonAsyncTask$StackJobPool;
.super Ljava/lang/Object;
.source "DaemonAsyncTask.java"

# interfaces
.implements Lmiui/os/DaemonAsyncTask$JobPool;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/os/DaemonAsyncTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "StackJobPool"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lmiui/os/DaemonAsyncTask$JobPool",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private mJobs:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .local p0, this:Lmiui/os/DaemonAsyncTask$StackJobPool;,"Lmiui/os/DaemonAsyncTask$StackJobPool<TT;>;"
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lmiui/os/DaemonAsyncTask$StackJobPool;->mJobs:Ljava/util/Stack;

    return-void
.end method


# virtual methods
.method public declared-synchronized addJob(Ljava/lang/Object;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .local p0, this:Lmiui/os/DaemonAsyncTask$StackJobPool;,"Lmiui/os/DaemonAsyncTask$StackJobPool<TT;>;"
    .local p1, job:Ljava/lang/Object;,"TT;"
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lmiui/os/DaemonAsyncTask$StackJobPool;->mJobs:Ljava/util/Stack;

    invoke-virtual {v0, p1}, Ljava/util/Stack;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e

    iget-object v0, p0, Lmiui/os/DaemonAsyncTask$StackJobPool;->mJobs:Ljava/util/Stack;

    invoke-virtual {v0, p1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_10

    :cond_e
    monitor-exit p0

    return-void

    :catchall_10
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized finishJob(Ljava/lang/Object;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .local p0, this:Lmiui/os/DaemonAsyncTask$StackJobPool;,"Lmiui/os/DaemonAsyncTask$StackJobPool<TT;>;"
    .local p1, job:Ljava/lang/Object;,"TT;"
    monitor-enter p0

    monitor-exit p0

    return-void
.end method

.method public declared-synchronized getNextJob()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .local p0, this:Lmiui/os/DaemonAsyncTask$StackJobPool;,"Lmiui/os/DaemonAsyncTask$StackJobPool<TT;>;"
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lmiui/os/DaemonAsyncTask$StackJobPool;->mJobs:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->empty()Z
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_13

    move-result v0

    if-eqz v0, :cond_c

    const/4 v0, 0x0

    :goto_a
    monitor-exit p0

    return-object v0

    :cond_c
    :try_start_c
    iget-object v0, p0, Lmiui/os/DaemonAsyncTask$StackJobPool;->mJobs:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;
    :try_end_11
    .catchall {:try_start_c .. :try_end_11} :catchall_13

    move-result-object v0

    goto :goto_a

    :catchall_13
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isEmpty()Z
    .registers 2

    .prologue
    .local p0, this:Lmiui/os/DaemonAsyncTask$StackJobPool;,"Lmiui/os/DaemonAsyncTask$StackJobPool<TT;>;"
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lmiui/os/DaemonAsyncTask$StackJobPool;->mJobs:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->empty()Z
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_9

    move-result v0

    monitor-exit p0

    return v0

    :catchall_9
    move-exception v0

    monitor-exit p0

    throw v0
.end method
