.class public abstract Lmiui/os/DaemonAsyncTask;
.super Lmiui/os/ObservableAsyncTask;
.source "DaemonAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/os/DaemonAsyncTask$StackJobPool;,
        Lmiui/os/DaemonAsyncTask$JobPool;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Job:",
        "Ljava/lang/Object;",
        "JobResult:",
        "Ljava/lang/Object;",
        ">",
        "Lmiui/os/ObservableAsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Landroid/util/Pair",
        "<TJob;TJobResult;>;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private mAutoStop:Z

.field private mAutoStopDelayedTime:J

.field private mJobPool:Lmiui/os/DaemonAsyncTask$JobPool;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lmiui/os/DaemonAsyncTask$JobPool",
            "<TJob;>;"
        }
    .end annotation
.end field

.field private mLocker:[B

.field private mNeedStop:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    invoke-static {v0}, Landroid/os/AsyncTask;->setDefaultExecutor(Ljava/util/concurrent/Executor;)V

    return-void
.end method

.method public constructor <init>()V
    .registers 3

    .prologue
    .local p0, this:Lmiui/os/DaemonAsyncTask;,"Lmiui/os/DaemonAsyncTask<TJob;TJobResult;>;"
    invoke-direct {p0}, Lmiui/os/ObservableAsyncTask;-><init>()V

    const-wide/16 v0, 0x7d0

    iput-wide v0, p0, Lmiui/os/DaemonAsyncTask;->mAutoStopDelayedTime:J

    const/4 v0, 0x0

    new-array v0, v0, [B

    iput-object v0, p0, Lmiui/os/DaemonAsyncTask;->mLocker:[B

    return-void
.end method


# virtual methods
.method public addJob(Ljava/lang/Object;)V
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TJob;)V"
        }
    .end annotation

    .prologue
    .local p0, this:Lmiui/os/DaemonAsyncTask;,"Lmiui/os/DaemonAsyncTask<TJob;TJobResult;>;"
    .local p1, job:Ljava/lang/Object;,"TJob;"
    iget-object v0, p0, Lmiui/os/DaemonAsyncTask;->mJobPool:Lmiui/os/DaemonAsyncTask$JobPool;

    invoke-interface {v0, p1}, Lmiui/os/DaemonAsyncTask$JobPool;->addJob(Ljava/lang/Object;)V

    iget-object v1, p0, Lmiui/os/DaemonAsyncTask;->mLocker:[B

    monitor-enter v1

    :try_start_8
    iget-object v0, p0, Lmiui/os/DaemonAsyncTask;->mLocker:[B

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v1

    return-void

    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_8 .. :try_end_11} :catchall_f

    throw v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    .prologue
    .local p0, this:Lmiui/os/DaemonAsyncTask;,"Lmiui/os/DaemonAsyncTask<TJob;TJobResult;>;"
    check-cast p1, [Ljava/lang/Void;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/os/DaemonAsyncTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .registers 8
    .parameter "params"

    .prologue
    .local p0, this:Lmiui/os/DaemonAsyncTask;,"Lmiui/os/DaemonAsyncTask<TJob;TJobResult;>;"
    const/4 v0, 0x0

    .local v0, delayed:Z
    :goto_1
    iget-boolean v4, p0, Lmiui/os/DaemonAsyncTask;->mNeedStop:Z

    if-nez v4, :cond_1c

    const/4 v3, 0x0

    .local v3, job:Ljava/lang/Object;,"TJob;"
    iget-object v4, p0, Lmiui/os/DaemonAsyncTask;->mJobPool:Lmiui/os/DaemonAsyncTask$JobPool;

    invoke-interface {v4}, Lmiui/os/DaemonAsyncTask$JobPool;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_14

    iget-object v4, p0, Lmiui/os/DaemonAsyncTask;->mJobPool:Lmiui/os/DaemonAsyncTask$JobPool;

    invoke-interface {v4}, Lmiui/os/DaemonAsyncTask$JobPool;->getNextJob()Ljava/lang/Object;

    move-result-object v3

    .end local v3           #job:Ljava/lang/Object;,"TJob;"
    :cond_14
    if-nez v3, :cond_40

    iget-boolean v4, p0, Lmiui/os/DaemonAsyncTask;->mAutoStop:Z

    if-eqz v4, :cond_1e

    if-eqz v0, :cond_1e

    :cond_1c
    const/4 v4, 0x0

    return-object v4

    :cond_1e
    iget-boolean v4, p0, Lmiui/os/DaemonAsyncTask;->mAutoStop:Z

    if-eqz v4, :cond_2e

    const/4 v0, 0x1

    :try_start_23
    iget-wide v4, p0, Lmiui/os/DaemonAsyncTask;->mAutoStopDelayedTime:J

    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_28
    .catch Ljava/lang/InterruptedException; {:try_start_23 .. :try_end_28} :catch_29

    goto :goto_1

    :catch_29
    move-exception v1

    .local v1, e:Ljava/lang/InterruptedException;
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    .end local v1           #e:Ljava/lang/InterruptedException;
    :cond_2e
    iget-object v5, p0, Lmiui/os/DaemonAsyncTask;->mLocker:[B

    monitor-enter v5

    :try_start_31
    iget-object v4, p0, Lmiui/os/DaemonAsyncTask;->mLocker:[B

    invoke-virtual {v4}, Ljava/lang/Object;->wait()V
    :try_end_36
    .catchall {:try_start_31 .. :try_end_36} :catchall_38
    .catch Ljava/lang/InterruptedException; {:try_start_31 .. :try_end_36} :catch_3b

    :goto_36
    :try_start_36
    monitor-exit v5

    goto :goto_1

    :catchall_38
    move-exception v4

    monitor-exit v5
    :try_end_3a
    .catchall {:try_start_36 .. :try_end_3a} :catchall_38

    throw v4

    :catch_3b
    move-exception v1

    .restart local v1       #e:Ljava/lang/InterruptedException;
    :try_start_3c
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_3f
    .catchall {:try_start_3c .. :try_end_3f} :catchall_38

    goto :goto_36

    .end local v1           #e:Ljava/lang/InterruptedException;
    :cond_40
    const/4 v0, 0x0

    invoke-virtual {p0, v3}, Lmiui/os/DaemonAsyncTask;->needsDoJob(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_59

    new-instance v2, Landroid/util/Pair;

    invoke-virtual {p0, v3}, Lmiui/os/DaemonAsyncTask;->doJob(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .local v2, entry:Landroid/util/Pair;,"Landroid/util/Pair<TJob;TJobResult;>;"
    const/4 v4, 0x1

    new-array v4, v4, [Landroid/util/Pair;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    invoke-virtual {p0, v4}, Lmiui/os/DaemonAsyncTask;->publishProgress([Ljava/lang/Object;)V

    .end local v2           #entry:Landroid/util/Pair;,"Landroid/util/Pair<TJob;TJobResult;>;"
    :cond_59
    iget-object v4, p0, Lmiui/os/DaemonAsyncTask;->mJobPool:Lmiui/os/DaemonAsyncTask$JobPool;

    invoke-interface {v4, v3}, Lmiui/os/DaemonAsyncTask$JobPool;->finishJob(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method protected abstract doJob(Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TJob;)TJobResult;"
        }
    .end annotation
.end method

.method protected needsDoJob(Ljava/lang/Object;)Z
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TJob;)Z"
        }
    .end annotation

    .prologue
    .local p0, this:Lmiui/os/DaemonAsyncTask;,"Lmiui/os/DaemonAsyncTask<TJob;TJobResult;>;"
    .local p1, job:Ljava/lang/Object;,"TJob;"
    const/4 v0, 0x1

    return v0
.end method

.method public setAutoStop(Z)V
    .registers 2
    .parameter "autoStop"

    .prologue
    .local p0, this:Lmiui/os/DaemonAsyncTask;,"Lmiui/os/DaemonAsyncTask<TJob;TJobResult;>;"
    iput-boolean p1, p0, Lmiui/os/DaemonAsyncTask;->mAutoStop:Z

    return-void
.end method

.method public setAutoStopDelayedTime(J)V
    .registers 3
    .parameter "autoStopDelayedTime"

    .prologue
    .local p0, this:Lmiui/os/DaemonAsyncTask;,"Lmiui/os/DaemonAsyncTask<TJob;TJobResult;>;"
    iput-wide p1, p0, Lmiui/os/DaemonAsyncTask;->mAutoStopDelayedTime:J

    return-void
.end method

.method public setJobPool(Lmiui/os/DaemonAsyncTask$JobPool;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiui/os/DaemonAsyncTask$JobPool",
            "<TJob;>;)V"
        }
    .end annotation

    .prologue
    .local p0, this:Lmiui/os/DaemonAsyncTask;,"Lmiui/os/DaemonAsyncTask<TJob;TJobResult;>;"
    .local p1, jobs:Lmiui/os/DaemonAsyncTask$JobPool;,"Lmiui/os/DaemonAsyncTask$JobPool<TJob;>;"
    iput-object p1, p0, Lmiui/os/DaemonAsyncTask;->mJobPool:Lmiui/os/DaemonAsyncTask$JobPool;

    return-void
.end method

.method public setLocker([B)V
    .registers 2
    .parameter "locker"

    .prologue
    .local p0, this:Lmiui/os/DaemonAsyncTask;,"Lmiui/os/DaemonAsyncTask<TJob;TJobResult;>;"
    iput-object p1, p0, Lmiui/os/DaemonAsyncTask;->mLocker:[B

    return-void
.end method

.method public stop()V
    .registers 3

    .prologue
    .local p0, this:Lmiui/os/DaemonAsyncTask;,"Lmiui/os/DaemonAsyncTask<TJob;TJobResult;>;"
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiui/os/DaemonAsyncTask;->mNeedStop:Z

    iget-object v1, p0, Lmiui/os/DaemonAsyncTask;->mLocker:[B

    monitor-enter v1

    :try_start_6
    iget-object v0, p0, Lmiui/os/DaemonAsyncTask;->mLocker:[B

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v1

    return-void

    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_6 .. :try_end_f} :catchall_d

    throw v0
.end method
