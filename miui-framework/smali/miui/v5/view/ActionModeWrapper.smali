.class public Lmiui/v5/view/ActionModeWrapper;
.super Landroid/widget/RelativeLayout;
.source "ActionModeWrapper.java"

# interfaces
.implements Lmiui/v5/view/MiuiActionMode$ActionModeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/v5/view/ActionModeWrapper$AnimatorModeCallback;,
        Lmiui/v5/view/ActionModeWrapper$ActionModeImpl;
    }
.end annotation


# instance fields
.field private mActionMode:Lmiui/v5/view/MiuiActionMode;

.field private mActionModeListener:Lmiui/v5/view/MiuiActionMode$ActionModeListener;

.field private mBottomContainer:Landroid/view/ViewGroup;

.field private mEditModeMenuBar:Lmiui/v5/widget/menubar/MenuBar;

.field private mMenuPresenter:Lmiui/v5/widget/menubar/MenuBarPresenter;

.field private mTopContainer:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lmiui/v5/view/ActionModeWrapper;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    .prologue
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lmiui/v5/view/ActionModeWrapper;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private createEditableActionMode(Landroid/view/View;Landroid/view/ActionMode$Callback;)Lmiui/v5/view/MiuiActionMode;
    .registers 8
    .parameter "originalView"
    .parameter "callback"

    .prologue
    if-eqz p1, :cond_52

    iget-object v1, p0, Lmiui/v5/view/ActionModeWrapper;->mEditModeMenuBar:Lmiui/v5/widget/menubar/MenuBar;

    if-nez v1, :cond_46

    invoke-direct {p0}, Lmiui/v5/view/ActionModeWrapper;->ensureViews()V

    new-instance v1, Lmiui/v5/widget/menubar/MenuBar;

    iget-object v2, p0, Lmiui/v5/view/ActionModeWrapper;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lmiui/v5/widget/menubar/MenuBar;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lmiui/v5/view/ActionModeWrapper;->mEditModeMenuBar:Lmiui/v5/widget/menubar/MenuBar;

    new-instance v0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;

    iget-object v1, p0, Lmiui/v5/view/ActionModeWrapper;->mContext:Landroid/content/Context;

    const v2, 0x603005b

    const v3, 0x603005c

    const v4, 0x603005d

    invoke-direct {v0, v1, v2, v3, v4}, Lmiui/v5/widget/menubar/IconMenuBarPresenter;-><init>(Landroid/content/Context;III)V

    .local v0, presenter:Lmiui/v5/widget/menubar/IconMenuBarPresenter;
    iget-object v1, p0, Lmiui/v5/view/ActionModeWrapper;->mContext:Landroid/content/Context;

    const v2, 0x6010061

    invoke-static {v1, v2}, Lmiui/util/UiUtils;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->setMoreIconDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v1, p0, Lmiui/v5/view/ActionModeWrapper;->mContext:Landroid/content/Context;

    const v2, 0x601006f

    invoke-static {v1, v2}, Lmiui/util/UiUtils;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->setPrimaryMaskDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v1, p0, Lmiui/v5/view/ActionModeWrapper;->mEditModeMenuBar:Lmiui/v5/widget/menubar/MenuBar;

    invoke-virtual {v1, v0}, Lmiui/v5/widget/menubar/MenuBar;->addMenuPresenter(Lmiui/v5/widget/menubar/MenuBarPresenter;)V

    iget-object v1, p0, Lmiui/v5/view/ActionModeWrapper;->mBottomContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->getMenuView(Landroid/view/ViewGroup;)Lmiui/v5/widget/menubar/MenuBarView;

    iput-object v0, p0, Lmiui/v5/view/ActionModeWrapper;->mMenuPresenter:Lmiui/v5/widget/menubar/MenuBarPresenter;

    .end local v0           #presenter:Lmiui/v5/widget/menubar/IconMenuBarPresenter;
    :cond_46
    new-instance v1, Lmiui/v5/view/EditableActionMode;

    iget-object v2, p0, Lmiui/v5/view/ActionModeWrapper;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lmiui/v5/view/ActionModeWrapper;->mTopContainer:Landroid/view/ViewGroup;

    iget-object v4, p0, Lmiui/v5/view/ActionModeWrapper;->mEditModeMenuBar:Lmiui/v5/widget/menubar/MenuBar;

    invoke-direct {v1, v2, v3, v4}, Lmiui/v5/view/EditableActionMode;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;Lmiui/v5/widget/menubar/MenuBar;)V

    :goto_51
    return-object v1

    :cond_52
    const/4 v1, 0x0

    goto :goto_51
.end method

.method private ensureViews()V
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/view/ActionModeWrapper;->mTopContainer:Landroid/view/ViewGroup;

    if-nez v0, :cond_1a

    const v0, 0x60b0096

    invoke-virtual {p0, v0}, Lmiui/v5/view/ActionModeWrapper;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lmiui/v5/view/ActionModeWrapper;->mTopContainer:Landroid/view/ViewGroup;

    const v0, 0x60b0095

    invoke-virtual {p0, v0}, Lmiui/v5/view/ActionModeWrapper;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lmiui/v5/view/ActionModeWrapper;->mBottomContainer:Landroid/view/ViewGroup;

    :cond_1a
    return-void
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .registers 5
    .parameter "event"

    .prologue
    const/4 v0, 0x1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_18

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_18

    iget-object v1, p0, Lmiui/v5/view/ActionModeWrapper;->mActionMode:Lmiui/v5/view/MiuiActionMode;

    if-eqz v1, :cond_18

    iget-object v1, p0, Lmiui/v5/view/ActionModeWrapper;->mActionMode:Lmiui/v5/view/MiuiActionMode;

    invoke-virtual {v1}, Lmiui/v5/view/MiuiActionMode;->tryToFinish()Z

    :goto_17
    return v0

    :cond_18
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_17
.end method

.method public finishActionMode()V
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/view/ActionModeWrapper;->mActionMode:Lmiui/v5/view/MiuiActionMode;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lmiui/v5/view/ActionModeWrapper;->mActionMode:Lmiui/v5/view/MiuiActionMode;

    invoke-virtual {v0}, Lmiui/v5/view/MiuiActionMode;->finish()V

    :cond_9
    return-void
.end method

.method public isActionModeActive()Z
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/view/ActionModeWrapper;->mActionMode:Lmiui/v5/view/MiuiActionMode;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public onActionModeFinished(Lmiui/v5/view/MiuiActionMode;)V
    .registers 3
    .parameter "mode"

    .prologue
    const/4 v0, 0x0

    iput-object v0, p0, Lmiui/v5/view/ActionModeWrapper;->mActionMode:Lmiui/v5/view/MiuiActionMode;

    iget-object v0, p0, Lmiui/v5/view/ActionModeWrapper;->mActionModeListener:Lmiui/v5/view/MiuiActionMode$ActionModeListener;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lmiui/v5/view/ActionModeWrapper;->mActionModeListener:Lmiui/v5/view/MiuiActionMode$ActionModeListener;

    invoke-interface {v0, p1}, Lmiui/v5/view/MiuiActionMode$ActionModeListener;->onActionModeFinished(Lmiui/v5/view/MiuiActionMode;)V

    :cond_c
    return-void
.end method

.method public onActionModeStarted(Lmiui/v5/view/MiuiActionMode;)V
    .registers 3
    .parameter "mode"

    .prologue
    iput-object p1, p0, Lmiui/v5/view/ActionModeWrapper;->mActionMode:Lmiui/v5/view/MiuiActionMode;

    iget-object v0, p0, Lmiui/v5/view/ActionModeWrapper;->mActionModeListener:Lmiui/v5/view/MiuiActionMode$ActionModeListener;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lmiui/v5/view/ActionModeWrapper;->mActionModeListener:Lmiui/v5/view/MiuiActionMode$ActionModeListener;

    invoke-interface {v0, p1}, Lmiui/v5/view/MiuiActionMode$ActionModeListener;->onActionModeStarted(Lmiui/v5/view/MiuiActionMode;)V

    :cond_b
    return-void
.end method

.method public setActionModeListener(Lmiui/v5/view/MiuiActionMode$ActionModeListener;)V
    .registers 2
    .parameter "l"

    .prologue
    iput-object p1, p0, Lmiui/v5/view/ActionModeWrapper;->mActionModeListener:Lmiui/v5/view/MiuiActionMode$ActionModeListener;

    return-void
.end method

.method public startActionModeForChild(Landroid/view/View;Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;
    .registers 5
    .parameter "originalView"
    .parameter "callback"

    .prologue
    iget-object v1, p0, Lmiui/v5/view/ActionModeWrapper;->mActionMode:Lmiui/v5/view/MiuiActionMode;

    if-eqz v1, :cond_6

    const/4 v0, 0x0

    :goto_5
    return-object v0

    :cond_6
    invoke-direct {p0, p1, p2}, Lmiui/v5/view/ActionModeWrapper;->createEditableActionMode(Landroid/view/View;Landroid/view/ActionMode$Callback;)Lmiui/v5/view/MiuiActionMode;

    move-result-object v0

    .local v0, mode:Lmiui/v5/view/MiuiActionMode;
    if-eqz v0, :cond_13

    invoke-virtual {v0, p0}, Lmiui/v5/view/MiuiActionMode;->setActionModeListener(Lmiui/v5/view/MiuiActionMode$ActionModeListener;)V

    invoke-virtual {v0, p2}, Lmiui/v5/view/MiuiActionMode;->start(Landroid/view/ActionMode$Callback;)V

    goto :goto_5

    :cond_13
    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->startActionModeForChild(Landroid/view/View;Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v0

    goto :goto_5
.end method

.method public startAnimatorActionMode(Landroid/animation/Animator;Landroid/animation/Animator;Lmiui/v5/view/MiuiActionMode$ActionModeListener;)Landroid/view/ActionMode;
    .registers 6
    .parameter "startAnimator"
    .parameter "finishAnimator"
    .parameter "l"

    .prologue
    iget-object v1, p0, Lmiui/v5/view/ActionModeWrapper;->mActionMode:Lmiui/v5/view/MiuiActionMode;

    if-eqz v1, :cond_6

    const/4 v0, 0x0

    :goto_5
    return-object v0

    :cond_6
    new-instance v0, Lmiui/v5/view/ActionModeWrapper$ActionModeImpl;

    invoke-direct {v0}, Lmiui/v5/view/ActionModeWrapper$ActionModeImpl;-><init>()V

    .local v0, mode:Lmiui/v5/view/MiuiActionMode;
    invoke-virtual {v0, p0}, Lmiui/v5/view/MiuiActionMode;->setActionModeListener(Lmiui/v5/view/MiuiActionMode$ActionModeListener;)V

    new-instance v1, Lmiui/v5/view/ActionModeWrapper$AnimatorModeCallback;

    invoke-direct {v1, p3, p1, p2}, Lmiui/v5/view/ActionModeWrapper$AnimatorModeCallback;-><init>(Lmiui/v5/view/MiuiActionMode$ActionModeListener;Landroid/animation/Animator;Landroid/animation/Animator;)V

    invoke-virtual {v0, v1}, Lmiui/v5/view/ActionModeWrapper$ActionModeImpl;->start(Landroid/view/ActionMode$Callback;)V

    iput-object v0, p0, Lmiui/v5/view/ActionModeWrapper;->mActionMode:Lmiui/v5/view/MiuiActionMode;

    goto :goto_5
.end method
