.class Lmiui/v5/view/ActionModeWrapper$ActionModeImpl;
.super Lmiui/v5/view/MiuiActionMode;
.source "ActionModeWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/v5/view/ActionModeWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ActionModeImpl"
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    invoke-direct {p0}, Lmiui/v5/view/MiuiActionMode;-><init>()V

    return-void
.end method


# virtual methods
.method public finish()V
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/view/MiuiActionMode;->mCallback:Landroid/view/ActionMode$Callback;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lmiui/v5/view/MiuiActionMode;->mCallback:Landroid/view/ActionMode$Callback;

    invoke-interface {v0, p0}, Landroid/view/ActionMode$Callback;->onDestroyActionMode(Landroid/view/ActionMode;)V

    :cond_9
    invoke-super {p0}, Lmiui/v5/view/MiuiActionMode;->finish()V

    return-void
.end method

.method public start(Landroid/view/ActionMode$Callback;)V
    .registers 3
    .parameter "callback"

    .prologue
    invoke-super {p0, p1}, Lmiui/v5/view/MiuiActionMode;->start(Landroid/view/ActionMode$Callback;)V

    if-eqz p1, :cond_9

    const/4 v0, 0x0

    invoke-interface {p1, p0, v0}, Landroid/view/ActionMode$Callback;->onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    :cond_9
    return-void
.end method
