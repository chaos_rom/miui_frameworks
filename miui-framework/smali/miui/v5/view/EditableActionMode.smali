.class public Lmiui/v5/view/EditableActionMode;
.super Lmiui/v5/view/MiuiActionMode;
.source "EditableActionMode.java"

# interfaces
.implements Lmiui/v5/widget/menubar/MenuBar$Callback;
.implements Lmiui/v5/view/EditModeTitleBar;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/v5/view/EditableActionMode$TitleBarImpl;
    }
.end annotation


# static fields
.field static final ACTION_NUM:I = 0x2


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mMenuBar:Lmiui/v5/widget/menubar/MenuBar;

.field private final mTitleBar:Lmiui/v5/view/EditableActionMode$TitleBarImpl;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;Lmiui/v5/widget/menubar/MenuBar;)V
    .registers 6
    .parameter "context"
    .parameter "topContainer"
    .parameter "menuBar"

    .prologue
    invoke-direct {p0}, Lmiui/v5/view/MiuiActionMode;-><init>()V

    iput-object p1, p0, Lmiui/v5/view/EditableActionMode;->mContext:Landroid/content/Context;

    iput-object p3, p0, Lmiui/v5/view/EditableActionMode;->mMenuBar:Lmiui/v5/widget/menubar/MenuBar;

    iget-object v0, p0, Lmiui/v5/view/EditableActionMode;->mMenuBar:Lmiui/v5/widget/menubar/MenuBar;

    invoke-virtual {v0, p0}, Lmiui/v5/widget/menubar/MenuBar;->setCallback(Lmiui/v5/widget/menubar/MenuBar$Callback;)V

    new-instance v0, Lmiui/v5/view/EditableActionMode$TitleBarImpl;

    iget-object v1, p0, Lmiui/v5/view/EditableActionMode;->mMenuBar:Lmiui/v5/widget/menubar/MenuBar;

    invoke-direct {v0, p2, v1}, Lmiui/v5/view/EditableActionMode$TitleBarImpl;-><init>(Landroid/view/ViewGroup;Lmiui/v5/widget/menubar/MenuBar;)V

    iput-object v0, p0, Lmiui/v5/view/EditableActionMode;->mTitleBar:Lmiui/v5/view/EditableActionMode$TitleBarImpl;

    return-void
.end method


# virtual methods
.method public finish()V
    .registers 3

    .prologue
    iget-object v0, p0, Lmiui/v5/view/EditableActionMode;->mMenuBar:Lmiui/v5/widget/menubar/MenuBar;

    invoke-virtual {v0}, Lmiui/v5/widget/menubar/MenuBar;->close()V

    iget-object v0, p0, Lmiui/v5/view/EditableActionMode;->mMenuBar:Lmiui/v5/widget/menubar/MenuBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmiui/v5/widget/menubar/MenuBar;->setCallback(Lmiui/v5/widget/menubar/MenuBar$Callback;)V

    iget-object v0, p0, Lmiui/v5/view/EditableActionMode;->mTitleBar:Lmiui/v5/view/EditableActionMode$TitleBarImpl;

    invoke-virtual {v0}, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->detach()V

    invoke-super {p0}, Lmiui/v5/view/MiuiActionMode;->finish()V

    return-void
.end method

.method public getButtonText(I)Ljava/lang/CharSequence;
    .registers 3
    .parameter "id"

    .prologue
    iget-object v0, p0, Lmiui/v5/view/EditableActionMode;->mTitleBar:Lmiui/v5/view/EditableActionMode$TitleBarImpl;

    invoke-virtual {v0, p1}, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->getButtonText(I)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getButtonVisiblity(I)I
    .registers 3
    .parameter "id"

    .prologue
    iget-object v0, p0, Lmiui/v5/view/EditableActionMode;->mTitleBar:Lmiui/v5/view/EditableActionMode$TitleBarImpl;

    invoke-virtual {v0, p1}, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->getButtonVisiblity(I)I

    move-result v0

    return v0
.end method

.method public getMenu()Landroid/view/Menu;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/view/EditableActionMode;->mMenuBar:Lmiui/v5/widget/menubar/MenuBar;

    return-object v0
.end method

.method public getMenuInflater()Landroid/view/MenuInflater;
    .registers 3

    .prologue
    new-instance v0, Landroid/view/MenuInflater;

    iget-object v1, p0, Lmiui/v5/view/EditableActionMode;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/view/MenuInflater;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/view/EditableActionMode;->mTitleBar:Lmiui/v5/view/EditableActionMode$TitleBarImpl;

    invoke-virtual {v0}, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public invalidate()V
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/view/MiuiActionMode;->mCallback:Landroid/view/ActionMode$Callback;

    if-nez v0, :cond_a

    iget-object v0, p0, Lmiui/v5/view/EditableActionMode;->mMenuBar:Lmiui/v5/widget/menubar/MenuBar;

    invoke-virtual {v0}, Lmiui/v5/widget/menubar/MenuBar;->clear()V

    :goto_9
    return-void

    :cond_a
    iget-object v0, p0, Lmiui/v5/view/EditableActionMode;->mMenuBar:Lmiui/v5/widget/menubar/MenuBar;

    invoke-virtual {v0}, Lmiui/v5/widget/menubar/MenuBar;->open()V

    goto :goto_9
.end method

.method public onCreateMenuBarPanel(Landroid/view/Menu;)Z
    .registers 3
    .parameter "menu"

    .prologue
    iget-object v0, p0, Lmiui/v5/view/MiuiActionMode;->mCallback:Landroid/view/ActionMode$Callback;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lmiui/v5/view/MiuiActionMode;->mCallback:Landroid/view/ActionMode$Callback;

    invoke-interface {v0, p0, p1}, Landroid/view/ActionMode$Callback;->onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    move-result v0

    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public onMenuBarPanelClose(Landroid/view/Menu;)V
    .registers 3
    .parameter "menu"

    .prologue
    iget-object v0, p0, Lmiui/v5/view/MiuiActionMode;->mCallback:Landroid/view/ActionMode$Callback;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lmiui/v5/view/MiuiActionMode;->mCallback:Landroid/view/ActionMode$Callback;

    invoke-interface {v0, p0}, Landroid/view/ActionMode$Callback;->onDestroyActionMode(Landroid/view/ActionMode;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lmiui/v5/view/MiuiActionMode;->mCallback:Landroid/view/ActionMode$Callback;

    :cond_c
    return-void
.end method

.method public onMenuBarPanelItemSelected(Landroid/view/Menu;Landroid/view/MenuItem;)Z
    .registers 4
    .parameter "menu"
    .parameter "item"

    .prologue
    iget-object v0, p0, Lmiui/v5/view/MiuiActionMode;->mCallback:Landroid/view/ActionMode$Callback;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lmiui/v5/view/MiuiActionMode;->mCallback:Landroid/view/ActionMode$Callback;

    invoke-interface {v0, p0, p2}, Landroid/view/ActionMode$Callback;->onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z

    move-result v0

    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public onMenuBarPanelModeChange(Landroid/view/Menu;I)V
    .registers 3
    .parameter "menu"
    .parameter "mode"

    .prologue
    return-void
.end method

.method public onMenuBarPanelOpen(Landroid/view/Menu;)V
    .registers 2
    .parameter "menu"

    .prologue
    return-void
.end method

.method public onPrepareMenuBarPanel(Landroid/view/Menu;)Z
    .registers 3
    .parameter "menu"

    .prologue
    iget-object v0, p0, Lmiui/v5/view/MiuiActionMode;->mCallback:Landroid/view/ActionMode$Callback;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lmiui/v5/view/MiuiActionMode;->mCallback:Landroid/view/ActionMode$Callback;

    invoke-interface {v0, p0, p1}, Landroid/view/ActionMode$Callback;->onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    move-result v0

    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public setBackground(I)V
    .registers 3
    .parameter "resId"

    .prologue
    iget-object v0, p0, Lmiui/v5/view/EditableActionMode;->mTitleBar:Lmiui/v5/view/EditableActionMode$TitleBarImpl;

    invoke-virtual {v0, p1}, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->setBackground(I)V

    return-void
.end method

.method public setBackground(Landroid/graphics/drawable/Drawable;)V
    .registers 3
    .parameter "d"

    .prologue
    iget-object v0, p0, Lmiui/v5/view/EditableActionMode;->mTitleBar:Lmiui/v5/view/EditableActionMode$TitleBarImpl;

    invoke-virtual {v0, p1}, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->setBackground(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public setButtonText(II)V
    .registers 4
    .parameter "id"
    .parameter "text"

    .prologue
    iget-object v0, p0, Lmiui/v5/view/EditableActionMode;->mTitleBar:Lmiui/v5/view/EditableActionMode$TitleBarImpl;

    invoke-virtual {v0, p1, p2}, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->setButtonText(II)V

    return-void
.end method

.method public setButtonText(ILjava/lang/CharSequence;)V
    .registers 4
    .parameter "id"
    .parameter "text"

    .prologue
    iget-object v0, p0, Lmiui/v5/view/EditableActionMode;->mTitleBar:Lmiui/v5/view/EditableActionMode$TitleBarImpl;

    invoke-virtual {v0, p1, p2}, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->setButtonText(ILjava/lang/CharSequence;)V

    return-void
.end method

.method public setButtonVisibility(II)V
    .registers 4
    .parameter "id"
    .parameter "visibility"

    .prologue
    iget-object v0, p0, Lmiui/v5/view/EditableActionMode;->mTitleBar:Lmiui/v5/view/EditableActionMode$TitleBarImpl;

    invoke-virtual {v0, p1, p2}, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->setButtonVisibility(II)V

    return-void
.end method

.method public setTitle(I)V
    .registers 3
    .parameter "resId"

    .prologue
    iget-object v0, p0, Lmiui/v5/view/EditableActionMode;->mTitleBar:Lmiui/v5/view/EditableActionMode$TitleBarImpl;

    invoke-virtual {v0, p1}, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->setTitle(I)V

    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "title"

    .prologue
    iget-object v0, p0, Lmiui/v5/view/EditableActionMode;->mTitleBar:Lmiui/v5/view/EditableActionMode$TitleBarImpl;

    invoke-virtual {v0, p1}, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public start(Landroid/view/ActionMode$Callback;)V
    .registers 3
    .parameter "callback"

    .prologue
    invoke-super {p0, p1}, Lmiui/v5/view/MiuiActionMode;->start(Landroid/view/ActionMode$Callback;)V

    iget-object v0, p0, Lmiui/v5/view/EditableActionMode;->mTitleBar:Lmiui/v5/view/EditableActionMode$TitleBarImpl;

    invoke-virtual {v0}, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->attach()V

    iget-object v0, p0, Lmiui/v5/view/EditableActionMode;->mMenuBar:Lmiui/v5/widget/menubar/MenuBar;

    invoke-virtual {v0}, Lmiui/v5/widget/menubar/MenuBar;->reopen()V

    return-void
.end method

.method public tryToFinish()Z
    .registers 3

    .prologue
    const/4 v0, 0x0

    iget-object v1, p0, Lmiui/v5/view/EditableActionMode;->mMenuBar:Lmiui/v5/widget/menubar/MenuBar;

    invoke-virtual {v1, v0}, Lmiui/v5/widget/menubar/MenuBar;->expand(Z)Z

    move-result v1

    if-eqz v1, :cond_a

    :goto_9
    return v0

    :cond_a
    invoke-super {p0}, Lmiui/v5/view/MiuiActionMode;->tryToFinish()Z

    move-result v0

    goto :goto_9
.end method
