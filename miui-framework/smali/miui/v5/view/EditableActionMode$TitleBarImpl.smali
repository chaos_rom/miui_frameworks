.class Lmiui/v5/view/EditableActionMode$TitleBarImpl;
.super Ljava/lang/Object;
.source "EditableActionMode.java"

# interfaces
.implements Lmiui/v5/view/EditModeTitleBar;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/v5/view/EditableActionMode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TitleBarImpl"
.end annotation


# instance fields
.field private final mActionItems:[Landroid/view/MenuItem;

.field private final mActionViews:[Landroid/widget/TextView;

.field final mContainer:Landroid/view/ViewGroup;

.field private final mContext:Landroid/content/Context;

.field private final mDetachListener:Landroid/view/animation/Animation$AnimationListener;

.field private final mMenuBar:Lmiui/v5/widget/menubar/MenuBar;

.field final mParent:Landroid/view/ViewGroup;

.field private final mTitleView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;Lmiui/v5/widget/menubar/MenuBar;)V
    .registers 11
    .parameter "parent"
    .parameter "bar"

    .prologue
    const/4 v2, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    new-array v1, v2, [Landroid/widget/TextView;

    iput-object v1, p0, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->mActionViews:[Landroid/widget/TextView;

    new-array v1, v2, [Landroid/view/MenuItem;

    iput-object v1, p0, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->mActionItems:[Landroid/view/MenuItem;

    new-instance v1, Lmiui/v5/view/EditableActionMode$TitleBarImpl$1;

    invoke-direct {v1, p0}, Lmiui/v5/view/EditableActionMode$TitleBarImpl$1;-><init>(Lmiui/v5/view/EditableActionMode$TitleBarImpl;)V

    iput-object v1, p0, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->mDetachListener:Landroid/view/animation/Animation$AnimationListener;

    iput-object p1, p0, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->mParent:Landroid/view/ViewGroup;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->mMenuBar:Lmiui/v5/widget/menubar/MenuBar;

    iget-object v1, p0, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->mContext:Landroid/content/Context;

    const v2, 0x6030050

    invoke-static {v1, v2, p1, v6}, Lmiui/v5/widget/Views;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .local v0, container:Landroid/view/ViewGroup;
    iput-object v0, p0, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->mContainer:Landroid/view/ViewGroup;

    const v1, 0x1020016

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->mTitleView:Landroid/widget/TextView;

    iget-object v2, p0, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->mActionViews:[Landroid/widget/TextView;

    const v1, 0x1020019

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    aput-object v1, v2, v6

    iget-object v1, p0, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->mActionViews:[Landroid/widget/TextView;

    aget-object v1, v1, v6

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->mActionViews:[Landroid/widget/TextView;

    const v1, 0x102001a

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    aput-object v1, v2, v7

    iget-object v1, p0, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->mActionViews:[Landroid/widget/TextView;

    aget-object v1, v1, v7

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->mActionItems:[Landroid/view/MenuItem;

    new-instance v2, Lmiui/v5/widget/menubar/MenuBarItem;

    iget-object v3, p0, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->mMenuBar:Lmiui/v5/widget/menubar/MenuBar;

    iget-object v4, p0, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->mActionViews:[Landroid/widget/TextView;

    aget-object v4, v4, v6

    invoke-virtual {v4}, Landroid/widget/TextView;->getId()I

    move-result v4

    const-string v5, ""

    invoke-direct {v2, v3, v4, v6, v5}, Lmiui/v5/widget/menubar/MenuBarItem;-><init>(Lmiui/v5/widget/menubar/MenuBar;IILjava/lang/CharSequence;)V

    aput-object v2, v1, v6

    iget-object v1, p0, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->mActionItems:[Landroid/view/MenuItem;

    new-instance v2, Lmiui/v5/widget/menubar/MenuBarItem;

    iget-object v3, p0, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->mMenuBar:Lmiui/v5/widget/menubar/MenuBar;

    iget-object v4, p0, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->mActionViews:[Landroid/widget/TextView;

    aget-object v4, v4, v7

    invoke-virtual {v4}, Landroid/widget/TextView;->getId()I

    move-result v4

    const-string v5, ""

    invoke-direct {v2, v3, v4, v6, v5}, Lmiui/v5/widget/menubar/MenuBarItem;-><init>(Lmiui/v5/widget/menubar/MenuBar;IILjava/lang/CharSequence;)V

    aput-object v2, v1, v7

    return-void
.end method

.method private getActionView(I)Landroid/widget/TextView;
    .registers 5
    .parameter "id"

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->mActionViews:[Landroid/widget/TextView;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Landroid/widget/TextView;->getId()I

    move-result v0

    if-ne p1, v0, :cond_11

    iget-object v0, p0, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->mActionViews:[Landroid/widget/TextView;

    aget-object v0, v0, v1

    :goto_10
    return-object v0

    :cond_11
    iget-object v0, p0, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->mActionViews:[Landroid/widget/TextView;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Landroid/widget/TextView;->getId()I

    move-result v0

    if-ne p1, v0, :cond_20

    iget-object v0, p0, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->mActionViews:[Landroid/widget/TextView;

    aget-object v0, v0, v2

    goto :goto_10

    :cond_20
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No ActionView for id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public attach()V
    .registers 4

    .prologue
    iget-object v0, p0, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->mContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_1d

    iget-object v0, p0, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->mParent:Landroid/view/ViewGroup;

    iget-object v1, p0, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->mContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->mContainer:Landroid/view/ViewGroup;

    iget-object v1, p0, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->mContext:Landroid/content/Context;

    const v2, 0x604001c

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_1d
    return-void
.end method

.method public detach()V
    .registers 4

    .prologue
    iget-object v1, p0, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->mContext:Landroid/content/Context;

    const v2, 0x604001d

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .local v0, anim:Landroid/view/animation/Animation;
    iget-object v1, p0, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->mDetachListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v1, p0, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->mContainer:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method public getButtonText(I)Ljava/lang/CharSequence;
    .registers 3
    .parameter "id"

    .prologue
    invoke-direct {p0, p1}, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->getActionView(I)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getButtonVisiblity(I)I
    .registers 3
    .parameter "id"

    .prologue
    invoke-direct {p0, p1}, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->getActionView(I)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    return v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .registers 6
    .parameter "v"

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->mActionViews:[Landroid/widget/TextView;

    aget-object v0, v0, v2

    if-ne p1, v0, :cond_12

    iget-object v0, p0, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->mMenuBar:Lmiui/v5/widget/menubar/MenuBar;

    iget-object v1, p0, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->mActionItems:[Landroid/view/MenuItem;

    aget-object v1, v1, v2

    invoke-virtual {v0, v1, v2}, Lmiui/v5/widget/menubar/MenuBar;->performItemAction(Landroid/view/MenuItem;I)Z

    :cond_11
    :goto_11
    return-void

    :cond_12
    iget-object v0, p0, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->mActionViews:[Landroid/widget/TextView;

    aget-object v0, v0, v3

    if-ne p1, v0, :cond_11

    iget-object v0, p0, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->mMenuBar:Lmiui/v5/widget/menubar/MenuBar;

    iget-object v1, p0, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->mActionItems:[Landroid/view/MenuItem;

    aget-object v1, v1, v3

    invoke-virtual {v0, v1, v2}, Lmiui/v5/widget/menubar/MenuBar;->performItemAction(Landroid/view/MenuItem;I)Z

    goto :goto_11
.end method

.method public setBackground(I)V
    .registers 3
    .parameter "resId"

    .prologue
    iget-object v0, p0, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->mContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    return-void
.end method

.method public setBackground(Landroid/graphics/drawable/Drawable;)V
    .registers 3
    .parameter "d"

    .prologue
    iget-object v0, p0, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->mContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->setBackground(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public setButtonText(II)V
    .registers 4
    .parameter "id"
    .parameter "text"

    .prologue
    invoke-direct {p0, p1}, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->getActionView(I)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method public setButtonText(ILjava/lang/CharSequence;)V
    .registers 4
    .parameter "id"
    .parameter "text"

    .prologue
    invoke-direct {p0, p1}, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->getActionView(I)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setButtonVisibility(II)V
    .registers 4
    .parameter "id"
    .parameter "visibility"

    .prologue
    invoke-direct {p0, p1}, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->getActionView(I)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method public setTitle(I)V
    .registers 3
    .parameter "text"

    .prologue
    iget-object v0, p0, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "text"

    .prologue
    iget-object v0, p0, Lmiui/v5/view/EditableActionMode$TitleBarImpl;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
