.class public Lmiui/v5/widget/menubar/MenuBar;
.super Ljava/lang/Object;
.source "MenuBar.java"

# interfaces
.implements Landroid/view/Menu;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/v5/widget/menubar/MenuBar$ItemInvoker;,
        Lmiui/v5/widget/menubar/MenuBar$MenuBarScrollHandler;,
        Lmiui/v5/widget/menubar/MenuBar$Callback;
    }
.end annotation


# static fields
.field public static final MENU_BAR_MODE_COLLAPSE:I = 0x1

.field public static final MENU_BAR_MODE_EXPAND:I = 0x0

.field private static final MENU_BAR_STATE_CLOSED:I = 0x0

.field private static final MENU_BAR_STATE_CLOSING:I = 0x3

.field private static final MENU_BAR_STATE_OPENED:I = 0x2

.field private static final MENU_BAR_STATE_OPENING:I = 0x1


# instance fields
.field private mCallback:Lmiui/v5/widget/menubar/MenuBar$Callback;

.field private mCloseMenuBarViewCount:I

.field protected mContext:Landroid/content/Context;

.field private mIsCreated:Z

.field private mIsPrepared:Z

.field protected mItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lmiui/v5/widget/menubar/MenuBarItem;",
            ">;"
        }
    .end annotation
.end field

.field private mItemsChangedWhileDispatchPrevented:Z

.field mMenuBarScrollHandler:Lmiui/v5/widget/menubar/MenuBar$MenuBarScrollHandler;

.field private mMenuBarState:I

.field mMenuPresenterCallback:Lmiui/v5/widget/menubar/MenuBarPresenter$Callback;

.field private mOpenMenuBarViewCount:I

.field private mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lmiui/v5/widget/menubar/MenuBarPresenter;",
            ">;>;"
        }
    .end annotation
.end field

.field private mPreventDispatchingItemsChanged:Z

.field protected mVisibleItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lmiui/v5/widget/menubar/MenuBarItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/v5/widget/menubar/MenuBar;->mItems:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/v5/widget/menubar/MenuBar;->mVisibleItems:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lmiui/v5/widget/menubar/MenuBar;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    const/4 v0, 0x0

    iput v0, p0, Lmiui/v5/widget/menubar/MenuBar;->mMenuBarState:I

    new-instance v0, Lmiui/v5/widget/menubar/MenuBar$1;

    invoke-direct {v0, p0}, Lmiui/v5/widget/menubar/MenuBar$1;-><init>(Lmiui/v5/widget/menubar/MenuBar;)V

    iput-object v0, p0, Lmiui/v5/widget/menubar/MenuBar;->mMenuBarScrollHandler:Lmiui/v5/widget/menubar/MenuBar$MenuBarScrollHandler;

    new-instance v0, Lmiui/v5/widget/menubar/MenuBar$2;

    invoke-direct {v0, p0}, Lmiui/v5/widget/menubar/MenuBar$2;-><init>(Lmiui/v5/widget/menubar/MenuBar;)V

    iput-object v0, p0, Lmiui/v5/widget/menubar/MenuBar;->mMenuPresenterCallback:Lmiui/v5/widget/menubar/MenuBarPresenter$Callback;

    iput-object p1, p0, Lmiui/v5/widget/menubar/MenuBar;->mContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$000(Lmiui/v5/widget/menubar/MenuBar;F)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    invoke-direct {p0, p1}, Lmiui/v5/widget/menubar/MenuBar;->dispatchMenuBarScroll(F)V

    return-void
.end method

.method static synthetic access$100(Lmiui/v5/widget/menubar/MenuBar;)I
    .registers 2
    .parameter "x0"

    .prologue
    iget v0, p0, Lmiui/v5/widget/menubar/MenuBar;->mOpenMenuBarViewCount:I

    return v0
.end method

.method static synthetic access$110(Lmiui/v5/widget/menubar/MenuBar;)I
    .registers 3
    .parameter "x0"

    .prologue
    iget v0, p0, Lmiui/v5/widget/menubar/MenuBar;->mOpenMenuBarViewCount:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lmiui/v5/widget/menubar/MenuBar;->mOpenMenuBarViewCount:I

    return v0
.end method

.method static synthetic access$202(Lmiui/v5/widget/menubar/MenuBar;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    iput p1, p0, Lmiui/v5/widget/menubar/MenuBar;->mMenuBarState:I

    return p1
.end method

.method static synthetic access$300(Lmiui/v5/widget/menubar/MenuBar;)I
    .registers 2
    .parameter "x0"

    .prologue
    iget v0, p0, Lmiui/v5/widget/menubar/MenuBar;->mCloseMenuBarViewCount:I

    return v0
.end method

.method static synthetic access$310(Lmiui/v5/widget/menubar/MenuBar;)I
    .registers 3
    .parameter "x0"

    .prologue
    iget v0, p0, Lmiui/v5/widget/menubar/MenuBar;->mCloseMenuBarViewCount:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lmiui/v5/widget/menubar/MenuBar;->mCloseMenuBarViewCount:I

    return v0
.end method

.method private dispatchMenuBarScroll(F)V
    .registers 6
    .parameter "percent"

    .prologue
    iget-object v3, p0, Lmiui/v5/widget/menubar/MenuBar;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_9

    :goto_8
    return-void

    :cond_9
    invoke-virtual {p0}, Lmiui/v5/widget/menubar/MenuBar;->stopDispatchingItemsChanged()V

    iget-object v3, p0, Lmiui/v5/widget/menubar/MenuBar;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_12
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_30

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/WeakReference;

    .local v2, ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Lmiui/v5/widget/menubar/MenuBarPresenter;>;"
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/v5/widget/menubar/MenuBarPresenter;

    .local v1, presenter:Lmiui/v5/widget/menubar/MenuBarPresenter;
    if-nez v1, :cond_2c

    iget-object v3, p0, Lmiui/v5/widget/menubar/MenuBar;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3, v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_12

    :cond_2c
    invoke-interface {v1, p1}, Lmiui/v5/widget/menubar/MenuBarPresenter;->scroll(F)V

    goto :goto_12

    .end local v1           #presenter:Lmiui/v5/widget/menubar/MenuBarPresenter;
    .end local v2           #ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Lmiui/v5/widget/menubar/MenuBarPresenter;>;"
    :cond_30
    invoke-virtual {p0}, Lmiui/v5/widget/menubar/MenuBar;->startDispatchingItemsChanged()V

    goto :goto_8
.end method

.method private dispatchPresenterUpdate(Z)V
    .registers 6
    .parameter "cleared"

    .prologue
    iget-object v3, p0, Lmiui/v5/widget/menubar/MenuBar;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_9

    :goto_8
    return-void

    :cond_9
    invoke-virtual {p0}, Lmiui/v5/widget/menubar/MenuBar;->stopDispatchingItemsChanged()V

    iget-object v3, p0, Lmiui/v5/widget/menubar/MenuBar;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_12
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_30

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/WeakReference;

    .local v2, ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Lmiui/v5/widget/menubar/MenuBarPresenter;>;"
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/v5/widget/menubar/MenuBarPresenter;

    .local v1, presenter:Lmiui/v5/widget/menubar/MenuBarPresenter;
    if-nez v1, :cond_2c

    iget-object v3, p0, Lmiui/v5/widget/menubar/MenuBar;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3, v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_12

    :cond_2c
    invoke-interface {v1, p1}, Lmiui/v5/widget/menubar/MenuBarPresenter;->updateMenuView(Z)V

    goto :goto_12

    .end local v1           #presenter:Lmiui/v5/widget/menubar/MenuBarPresenter;
    .end local v2           #ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Lmiui/v5/widget/menubar/MenuBarPresenter;>;"
    :cond_30
    invoke-virtual {p0}, Lmiui/v5/widget/menubar/MenuBar;->startDispatchingItemsChanged()V

    goto :goto_8
.end method

.method private static findInsertIndex(Ljava/util/ArrayList;I)I
    .registers 5
    .parameter
    .parameter "ordering"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lmiui/v5/widget/menubar/MenuBarItem;",
            ">;I)I"
        }
    .end annotation

    .prologue
    .local p0, items:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lmiui/v5/widget/menubar/MenuBarItem;>;"
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .local v0, i:I
    :goto_6
    if-ltz v0, :cond_1a

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/v5/widget/menubar/MenuBarItem;

    .local v1, item:Lmiui/v5/widget/menubar/MenuBarItem;
    invoke-virtual {v1}, Lmiui/v5/widget/menubar/MenuBarItem;->getOrder()I

    move-result v2

    if-gt v2, p1, :cond_17

    add-int/lit8 v2, v0, 0x1

    .end local v1           #item:Lmiui/v5/widget/menubar/MenuBarItem;
    :goto_16
    return v2

    .restart local v1       #item:Lmiui/v5/widget/menubar/MenuBarItem;
    :cond_17
    add-int/lit8 v0, v0, -0x1

    goto :goto_6

    .end local v1           #item:Lmiui/v5/widget/menubar/MenuBarItem;
    :cond_1a
    const/4 v2, 0x0

    goto :goto_16
.end method

.method private findItemIndex(I)I
    .registers 5
    .parameter "id"

    .prologue
    iget-object v2, p0, Lmiui/v5/widget/menubar/MenuBar;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    .local v1, size:I
    move v0, v1

    .local v0, i:I
    :goto_7
    if-ltz v0, :cond_1b

    iget-object v2, p0, Lmiui/v5/widget/menubar/MenuBar;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/v5/widget/menubar/MenuBarItem;

    invoke-virtual {v2}, Lmiui/v5/widget/menubar/MenuBarItem;->getItemId()I

    move-result v2

    if-ne v2, p1, :cond_18

    .end local v0           #i:I
    :goto_17
    return v0

    .restart local v0       #i:I
    :cond_18
    add-int/lit8 v0, v0, -0x1

    goto :goto_7

    :cond_1b
    const/4 v0, -0x1

    goto :goto_17
.end method

.method private removeItemAtInt(IZ)V
    .registers 4
    .parameter "index"
    .parameter "updateChildrenOnMenuViews"

    .prologue
    if-ltz p1, :cond_a

    iget-object v0, p0, Lmiui/v5/widget/menubar/MenuBar;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_b

    :cond_a
    :goto_a
    return-void

    :cond_b
    if-eqz p2, :cond_a

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lmiui/v5/widget/menubar/MenuBar;->onItemsChanged(Z)V

    goto :goto_a
.end method


# virtual methods
.method public add(I)Landroid/view/MenuItem;
    .registers 4
    .parameter "titleRes"

    .prologue
    const/4 v1, 0x0

    iget-object v0, p0, Lmiui/v5/widget/menubar/MenuBar;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v1, v0}, Lmiui/v5/widget/menubar/MenuBar;->addInternal(IILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public add(IIII)Landroid/view/MenuItem;
    .registers 6
    .parameter "groupId"
    .parameter "itemId"
    .parameter "order"
    .parameter "titleRes"

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/menubar/MenuBar;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p2, p3, v0}, Lmiui/v5/widget/menubar/MenuBar;->addInternal(IILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
    .registers 6
    .parameter "groupId"
    .parameter "itemId"
    .parameter "order"
    .parameter "title"

    .prologue
    invoke-virtual {p0, p2, p3, p4}, Lmiui/v5/widget/menubar/MenuBar;->addInternal(IILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .registers 3
    .parameter "title"

    .prologue
    const/4 v0, 0x0

    invoke-virtual {p0, v0, v0, p1}, Lmiui/v5/widget/menubar/MenuBar;->addInternal(IILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public addIntentOptions(IIILandroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I[Landroid/view/MenuItem;)I
    .registers 10
    .parameter "groupId"
    .parameter "itemId"
    .parameter "order"
    .parameter "caller"
    .parameter "specifics"
    .parameter "intent"
    .parameter "flags"
    .parameter "outSpecificItems"

    .prologue
    const/4 v0, 0x0

    return v0
.end method

.method addInternal(IILjava/lang/CharSequence;)Landroid/view/MenuItem;
    .registers 7
    .parameter "itemId"
    .parameter "order"
    .parameter "title"

    .prologue
    new-instance v0, Lmiui/v5/widget/menubar/MenuBarItem;

    invoke-direct {v0, p0, p1, p2, p3}, Lmiui/v5/widget/menubar/MenuBarItem;-><init>(Lmiui/v5/widget/menubar/MenuBar;IILjava/lang/CharSequence;)V

    .local v0, item:Lmiui/v5/widget/menubar/MenuBarItem;
    iget-object v1, p0, Lmiui/v5/widget/menubar/MenuBar;->mItems:Ljava/util/ArrayList;

    iget-object v2, p0, Lmiui/v5/widget/menubar/MenuBar;->mItems:Ljava/util/ArrayList;

    invoke-static {v2, p2}, Lmiui/v5/widget/menubar/MenuBar;->findInsertIndex(Ljava/util/ArrayList;I)I

    move-result v2

    invoke-virtual {v1, v2, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lmiui/v5/widget/menubar/MenuBar;->onItemsChanged(Z)V

    return-object v0
.end method

.method public addMenuPresenter(Lmiui/v5/widget/menubar/MenuBarPresenter;)V
    .registers 4
    .parameter "presenter"

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/menubar/MenuBar;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmiui/v5/widget/menubar/MenuBar;->mContext:Landroid/content/Context;

    invoke-interface {p1, v0, p0}, Lmiui/v5/widget/menubar/MenuBarPresenter;->initForMenu(Landroid/content/Context;Lmiui/v5/widget/menubar/MenuBar;)V

    iget-object v0, p0, Lmiui/v5/widget/menubar/MenuBar;->mMenuPresenterCallback:Lmiui/v5/widget/menubar/MenuBarPresenter$Callback;

    invoke-interface {p1, v0}, Lmiui/v5/widget/menubar/MenuBarPresenter;->setCallback(Lmiui/v5/widget/menubar/MenuBarPresenter$Callback;)V

    return-void
.end method

.method public addSubMenu(I)Landroid/view/SubMenu;
    .registers 3
    .parameter "titleRes"

    .prologue
    const/4 v0, 0x0

    return-object v0
.end method

.method public addSubMenu(IIII)Landroid/view/SubMenu;
    .registers 6
    .parameter "groupId"
    .parameter "itemId"
    .parameter "order"
    .parameter "titleRes"

    .prologue
    const/4 v0, 0x0

    return-object v0
.end method

.method public addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;
    .registers 6
    .parameter "groupId"
    .parameter "itemId"
    .parameter "order"
    .parameter "title"

    .prologue
    const/4 v0, 0x0

    return-object v0
.end method

.method public addSubMenu(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
    .registers 3
    .parameter "title"

    .prologue
    const/4 v0, 0x0

    return-object v0
.end method

.method public clear()V
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/menubar/MenuBar;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lmiui/v5/widget/menubar/MenuBar;->onItemsChanged(Z)V

    return-void
.end method

.method public close()V
    .registers 2

    .prologue
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lmiui/v5/widget/menubar/MenuBar;->close(Z)V

    return-void
.end method

.method final close(Z)V
    .registers 9
    .parameter "animate"

    .prologue
    const/4 v6, 0x0

    iget v4, p0, Lmiui/v5/widget/menubar/MenuBar;->mMenuBarState:I

    const/4 v5, 0x3

    if-eq v4, v5, :cond_f

    iget v4, p0, Lmiui/v5/widget/menubar/MenuBar;->mMenuBarState:I

    const/4 v5, 0x1

    if-eq v4, v5, :cond_f

    iget v4, p0, Lmiui/v5/widget/menubar/MenuBar;->mMenuBarState:I

    if-nez v4, :cond_10

    :cond_f
    :goto_f
    return-void

    :cond_10
    const/4 v0, 0x0

    .local v0, handled:Z
    iput v6, p0, Lmiui/v5/widget/menubar/MenuBar;->mCloseMenuBarViewCount:I

    iget-object v4, p0, Lmiui/v5/widget/menubar/MenuBar;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v4}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_19
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/ref/WeakReference;

    .local v3, ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Lmiui/v5/widget/menubar/MenuBarPresenter;>;"
    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/v5/widget/menubar/MenuBarPresenter;

    .local v2, presenter:Lmiui/v5/widget/menubar/MenuBarPresenter;
    if-nez v2, :cond_33

    iget-object v4, p0, Lmiui/v5/widget/menubar/MenuBar;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v4, v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_19

    :cond_33
    const/4 v0, 0x1

    iget v4, p0, Lmiui/v5/widget/menubar/MenuBar;->mCloseMenuBarViewCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lmiui/v5/widget/menubar/MenuBar;->mCloseMenuBarViewCount:I

    invoke-interface {v2, p0, p1}, Lmiui/v5/widget/menubar/MenuBarPresenter;->onCloseMenu(Lmiui/v5/widget/menubar/MenuBar;Z)V

    goto :goto_19

    .end local v2           #presenter:Lmiui/v5/widget/menubar/MenuBarPresenter;
    .end local v3           #ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Lmiui/v5/widget/menubar/MenuBarPresenter;>;"
    :cond_3e
    if-nez v0, :cond_f

    iput v6, p0, Lmiui/v5/widget/menubar/MenuBar;->mMenuBarState:I

    goto :goto_f
.end method

.method dispatchMenuClose()V
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/menubar/MenuBar;->mCallback:Lmiui/v5/widget/menubar/MenuBar$Callback;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lmiui/v5/widget/menubar/MenuBar;->mCallback:Lmiui/v5/widget/menubar/MenuBar$Callback;

    invoke-interface {v0, p0}, Lmiui/v5/widget/menubar/MenuBar$Callback;->onMenuBarPanelClose(Landroid/view/Menu;)V

    :cond_9
    return-void
.end method

.method dispatchMenuItemSelected(Lmiui/v5/widget/menubar/MenuBar;Landroid/view/MenuItem;)Z
    .registers 4
    .parameter "menu"
    .parameter "item"

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/menubar/MenuBar;->mCallback:Lmiui/v5/widget/menubar/MenuBar$Callback;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lmiui/v5/widget/menubar/MenuBar;->mCallback:Lmiui/v5/widget/menubar/MenuBar$Callback;

    invoke-interface {v0, p1, p2}, Lmiui/v5/widget/menubar/MenuBar$Callback;->onMenuBarPanelItemSelected(Landroid/view/Menu;Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method dispatchMenuModeChange(I)V
    .registers 3
    .parameter "mode"

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/menubar/MenuBar;->mCallback:Lmiui/v5/widget/menubar/MenuBar$Callback;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lmiui/v5/widget/menubar/MenuBar;->mCallback:Lmiui/v5/widget/menubar/MenuBar$Callback;

    invoke-interface {v0, p0, p1}, Lmiui/v5/widget/menubar/MenuBar$Callback;->onMenuBarPanelModeChange(Landroid/view/Menu;I)V

    :cond_9
    return-void
.end method

.method dispatchMenuOpen()V
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/menubar/MenuBar;->mCallback:Lmiui/v5/widget/menubar/MenuBar$Callback;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lmiui/v5/widget/menubar/MenuBar;->mCallback:Lmiui/v5/widget/menubar/MenuBar$Callback;

    invoke-interface {v0, p0}, Lmiui/v5/widget/menubar/MenuBar$Callback;->onMenuBarPanelOpen(Landroid/view/Menu;)V

    :cond_9
    return-void
.end method

.method public expand(Z)Z
    .registers 8
    .parameter "expand"

    .prologue
    iget v4, p0, Lmiui/v5/widget/menubar/MenuBar;->mMenuBarState:I

    const/4 v5, 0x2

    if-eq v4, v5, :cond_7

    const/4 v0, 0x0

    :cond_6
    return v0

    :cond_7
    const/4 v0, 0x0

    .local v0, handled:Z
    iget-object v4, p0, Lmiui/v5/widget/menubar/MenuBar;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v4}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/ref/WeakReference;

    .local v3, ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Lmiui/v5/widget/menubar/MenuBarPresenter;>;"
    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/v5/widget/menubar/MenuBarPresenter;

    .local v2, presenter:Lmiui/v5/widget/menubar/MenuBarPresenter;
    if-nez v2, :cond_28

    iget-object v4, p0, Lmiui/v5/widget/menubar/MenuBar;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v4, v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_e

    :cond_28
    invoke-interface {v2, p0, p1}, Lmiui/v5/widget/menubar/MenuBarPresenter;->onExpandMenu(Lmiui/v5/widget/menubar/MenuBar;Z)Z

    move-result v4

    or-int/2addr v0, v4

    goto :goto_e
.end method

.method public findItem(I)Landroid/view/MenuItem;
    .registers 6
    .parameter "id"

    .prologue
    invoke-virtual {p0}, Lmiui/v5/widget/menubar/MenuBar;->size()I

    move-result v2

    .local v2, size:I
    const/4 v0, 0x0

    .local v0, i:I
    :goto_5
    if-ge v0, v2, :cond_19

    iget-object v3, p0, Lmiui/v5/widget/menubar/MenuBar;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/v5/widget/menubar/MenuBarItem;

    .local v1, item:Lmiui/v5/widget/menubar/MenuBarItem;
    invoke-virtual {v1}, Lmiui/v5/widget/menubar/MenuBarItem;->getItemId()I

    move-result v3

    if-ne v3, p1, :cond_16

    .end local v1           #item:Lmiui/v5/widget/menubar/MenuBarItem;
    :goto_15
    return-object v1

    .restart local v1       #item:Lmiui/v5/widget/menubar/MenuBarItem;
    :cond_16
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .end local v1           #item:Lmiui/v5/widget/menubar/MenuBarItem;
    :cond_19
    const/4 v1, 0x0

    goto :goto_15
.end method

.method public getContext()Landroid/content/Context;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/menubar/MenuBar;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getItem(I)Landroid/view/MenuItem;
    .registers 3
    .parameter "index"

    .prologue
    if-ltz p1, :cond_a

    iget-object v0, p0, Lmiui/v5/widget/menubar/MenuBar;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_c

    :cond_a
    const/4 v0, 0x0

    :goto_b
    return-object v0

    :cond_c
    iget-object v0, p0, Lmiui/v5/widget/menubar/MenuBar;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MenuItem;

    goto :goto_b
.end method

.method public getMenuBarSrollHandler()Lmiui/v5/widget/menubar/MenuBar$MenuBarScrollHandler;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/menubar/MenuBar;->mMenuBarScrollHandler:Lmiui/v5/widget/menubar/MenuBar$MenuBarScrollHandler;

    return-object v0
.end method

.method getVisibleItems()Ljava/util/ArrayList;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lmiui/v5/widget/menubar/MenuBarItem;",
            ">;"
        }
    .end annotation

    .prologue
    iget-object v3, p0, Lmiui/v5/widget/menubar/MenuBar;->mVisibleItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    iget-object v3, p0, Lmiui/v5/widget/menubar/MenuBar;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    .local v2, itemsSize:I
    const/4 v0, 0x0

    .local v0, i:I
    :goto_c
    if-ge v0, v2, :cond_29

    iget-object v3, p0, Lmiui/v5/widget/menubar/MenuBar;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/v5/widget/menubar/MenuBarItem;

    .local v1, item:Lmiui/v5/widget/menubar/MenuBarItem;
    invoke-virtual {v1}, Lmiui/v5/widget/menubar/MenuBarItem;->isVisible()Z

    move-result v3

    if-eqz v3, :cond_24

    iget-object v3, p0, Lmiui/v5/widget/menubar/MenuBar;->mVisibleItems:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_21
    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    :cond_24
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lmiui/v5/widget/menubar/MenuBarItem;->setTag(Ljava/lang/Object;)Landroid/view/MenuItem;

    goto :goto_21

    .end local v1           #item:Lmiui/v5/widget/menubar/MenuBarItem;
    :cond_29
    iget-object v3, p0, Lmiui/v5/widget/menubar/MenuBar;->mVisibleItems:Ljava/util/ArrayList;

    return-object v3
.end method

.method public hasVisibleItems()Z
    .registers 5

    .prologue
    invoke-virtual {p0}, Lmiui/v5/widget/menubar/MenuBar;->size()I

    move-result v2

    .local v2, size:I
    const/4 v0, 0x0

    .local v0, i:I
    :goto_5
    if-ge v0, v2, :cond_1a

    iget-object v3, p0, Lmiui/v5/widget/menubar/MenuBar;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/v5/widget/menubar/MenuBarItem;

    .local v1, item:Lmiui/v5/widget/menubar/MenuBarItem;
    invoke-virtual {v1}, Lmiui/v5/widget/menubar/MenuBarItem;->isVisible()Z

    move-result v3

    if-eqz v3, :cond_17

    const/4 v3, 0x1

    .end local v1           #item:Lmiui/v5/widget/menubar/MenuBarItem;
    :goto_16
    return v3

    .restart local v1       #item:Lmiui/v5/widget/menubar/MenuBarItem;
    :cond_17
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .end local v1           #item:Lmiui/v5/widget/menubar/MenuBarItem;
    :cond_1a
    const/4 v3, 0x0

    goto :goto_16
.end method

.method public invalidate()V
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/menubar/MenuBar;->mCallback:Lmiui/v5/widget/menubar/MenuBar$Callback;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lmiui/v5/widget/menubar/MenuBar;->mCallback:Lmiui/v5/widget/menubar/MenuBar$Callback;

    invoke-interface {v0, p0}, Lmiui/v5/widget/menubar/MenuBar$Callback;->onPrepareMenuBarPanel(Landroid/view/Menu;)Z

    :cond_9
    return-void
.end method

.method public isOpen()Z
    .registers 3

    .prologue
    iget v0, p0, Lmiui/v5/widget/menubar/MenuBar;->mMenuBarState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public isShortcutKey(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter "keyCode"
    .parameter "event"

    .prologue
    const/4 v0, 0x0

    return v0
.end method

.method onItemVisibleChanged(Lmiui/v5/widget/menubar/MenuBarItem;)V
    .registers 3
    .parameter "item"

    .prologue
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lmiui/v5/widget/menubar/MenuBar;->onItemsChanged(Z)V

    return-void
.end method

.method onItemsChanged(Z)V
    .registers 3
    .parameter "structureChanged"

    .prologue
    iget-boolean v0, p0, Lmiui/v5/widget/menubar/MenuBar;->mPreventDispatchingItemsChanged:Z

    if-nez v0, :cond_8

    invoke-direct {p0, p1}, Lmiui/v5/widget/menubar/MenuBar;->dispatchPresenterUpdate(Z)V

    :goto_7
    return-void

    :cond_8
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiui/v5/widget/menubar/MenuBar;->mItemsChangedWhileDispatchPrevented:Z

    goto :goto_7
.end method

.method public open()V
    .registers 2

    .prologue
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lmiui/v5/widget/menubar/MenuBar;->open(Z)V

    return-void
.end method

.method public open(Z)V
    .registers 10
    .parameter "animate"

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    iget v4, p0, Lmiui/v5/widget/menubar/MenuBar;->mMenuBarState:I

    const/4 v5, 0x3

    if-eq v4, v5, :cond_b

    iget v4, p0, Lmiui/v5/widget/menubar/MenuBar;->mMenuBarState:I

    if-ne v4, v7, :cond_c

    :cond_b
    :goto_b
    return-void

    :cond_c
    iput-boolean v7, p0, Lmiui/v5/widget/menubar/MenuBar;->mPreventDispatchingItemsChanged:Z

    iget-object v4, p0, Lmiui/v5/widget/menubar/MenuBar;->mCallback:Lmiui/v5/widget/menubar/MenuBar$Callback;

    if-eqz v4, :cond_6f

    iget-boolean v4, p0, Lmiui/v5/widget/menubar/MenuBar;->mIsCreated:Z

    if-eqz v4, :cond_57

    iget-object v4, p0, Lmiui/v5/widget/menubar/MenuBar;->mCallback:Lmiui/v5/widget/menubar/MenuBar$Callback;

    invoke-interface {v4, p0}, Lmiui/v5/widget/menubar/MenuBar$Callback;->onPrepareMenuBarPanel(Landroid/view/Menu;)Z

    move-result v4

    iput-boolean v4, p0, Lmiui/v5/widget/menubar/MenuBar;->mIsPrepared:Z

    :goto_1e
    iput-boolean v6, p0, Lmiui/v5/widget/menubar/MenuBar;->mPreventDispatchingItemsChanged:Z

    iput-boolean v6, p0, Lmiui/v5/widget/menubar/MenuBar;->mItemsChangedWhileDispatchPrevented:Z

    const/4 v0, 0x0

    .local v0, handled:Z
    iget-boolean v4, p0, Lmiui/v5/widget/menubar/MenuBar;->mIsCreated:Z

    if-eqz v4, :cond_7f

    iget-boolean v4, p0, Lmiui/v5/widget/menubar/MenuBar;->mIsPrepared:Z

    if-eqz v4, :cond_7f

    invoke-virtual {p0}, Lmiui/v5/widget/menubar/MenuBar;->getVisibleItems()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_7f

    iput v6, p0, Lmiui/v5/widget/menubar/MenuBar;->mOpenMenuBarViewCount:I

    iget-object v4, p0, Lmiui/v5/widget/menubar/MenuBar;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v4}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_3d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/ref/WeakReference;

    .local v3, ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Lmiui/v5/widget/menubar/MenuBarPresenter;>;"
    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/v5/widget/menubar/MenuBarPresenter;

    .local v2, presenter:Lmiui/v5/widget/menubar/MenuBarPresenter;
    if-nez v2, :cond_74

    iget-object v4, p0, Lmiui/v5/widget/menubar/MenuBar;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v4, v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_3d

    .end local v0           #handled:Z
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #presenter:Lmiui/v5/widget/menubar/MenuBarPresenter;
    .end local v3           #ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Lmiui/v5/widget/menubar/MenuBarPresenter;>;"
    :cond_57
    iget-object v4, p0, Lmiui/v5/widget/menubar/MenuBar;->mCallback:Lmiui/v5/widget/menubar/MenuBar$Callback;

    invoke-interface {v4, p0}, Lmiui/v5/widget/menubar/MenuBar$Callback;->onCreateMenuBarPanel(Landroid/view/Menu;)Z

    move-result v4

    iput-boolean v4, p0, Lmiui/v5/widget/menubar/MenuBar;->mIsCreated:Z

    iget-boolean v4, p0, Lmiui/v5/widget/menubar/MenuBar;->mIsCreated:Z

    if-eqz v4, :cond_6c

    iget-object v4, p0, Lmiui/v5/widget/menubar/MenuBar;->mCallback:Lmiui/v5/widget/menubar/MenuBar$Callback;

    invoke-interface {v4, p0}, Lmiui/v5/widget/menubar/MenuBar$Callback;->onPrepareMenuBarPanel(Landroid/view/Menu;)Z

    move-result v4

    iput-boolean v4, p0, Lmiui/v5/widget/menubar/MenuBar;->mIsPrepared:Z

    goto :goto_1e

    :cond_6c
    iput-boolean v6, p0, Lmiui/v5/widget/menubar/MenuBar;->mIsPrepared:Z

    goto :goto_1e

    :cond_6f
    iput-boolean v6, p0, Lmiui/v5/widget/menubar/MenuBar;->mIsCreated:Z

    iput-boolean v6, p0, Lmiui/v5/widget/menubar/MenuBar;->mIsPrepared:Z

    goto :goto_1e

    .restart local v0       #handled:Z
    .restart local v1       #i$:Ljava/util/Iterator;
    .restart local v2       #presenter:Lmiui/v5/widget/menubar/MenuBarPresenter;
    .restart local v3       #ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Lmiui/v5/widget/menubar/MenuBarPresenter;>;"
    :cond_74
    const/4 v0, 0x1

    iget v4, p0, Lmiui/v5/widget/menubar/MenuBar;->mOpenMenuBarViewCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lmiui/v5/widget/menubar/MenuBar;->mOpenMenuBarViewCount:I

    invoke-interface {v2, p0, p1}, Lmiui/v5/widget/menubar/MenuBarPresenter;->onOpenMenu(Lmiui/v5/widget/menubar/MenuBar;Z)V

    goto :goto_3d

    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #presenter:Lmiui/v5/widget/menubar/MenuBarPresenter;
    .end local v3           #ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Lmiui/v5/widget/menubar/MenuBarPresenter;>;"
    :cond_7f
    if-nez v0, :cond_b

    iget v4, p0, Lmiui/v5/widget/menubar/MenuBar;->mMenuBarState:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_b

    invoke-virtual {p0, v7}, Lmiui/v5/widget/menubar/MenuBar;->close(Z)V

    goto :goto_b
.end method

.method public performIdentifierAction(II)Z
    .registers 4
    .parameter "id"
    .parameter "flags"

    .prologue
    const/4 v0, 0x0

    return v0
.end method

.method public performItemAction(Landroid/view/MenuItem;I)Z
    .registers 6
    .parameter "item"
    .parameter "flags"

    .prologue
    move-object v1, p1

    check-cast v1, Lmiui/v5/widget/menubar/MenuBarItem;

    .local v1, itemImpl:Lmiui/v5/widget/menubar/MenuBarItem;
    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lmiui/v5/widget/menubar/MenuBarItem;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_d

    :cond_b
    const/4 v0, 0x0

    :goto_c
    return v0

    :cond_d
    invoke-virtual {v1}, Lmiui/v5/widget/menubar/MenuBarItem;->invoke()Z

    move-result v0

    .local v0, invoked:Z
    goto :goto_c
.end method

.method public performShortcut(ILandroid/view/KeyEvent;I)Z
    .registers 5
    .parameter "keyCode"
    .parameter "event"
    .parameter "flags"

    .prologue
    const/4 v0, 0x0

    return v0
.end method

.method public removeGroup(I)V
    .registers 2
    .parameter "groupId"

    .prologue
    return-void
.end method

.method public removeItem(I)V
    .registers 4
    .parameter "id"

    .prologue
    invoke-direct {p0, p1}, Lmiui/v5/widget/menubar/MenuBar;->findItemIndex(I)I

    move-result v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lmiui/v5/widget/menubar/MenuBar;->removeItemAtInt(IZ)V

    return-void
.end method

.method public reopen()V
    .registers 2

    .prologue
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lmiui/v5/widget/menubar/MenuBar;->reopen(Z)V

    return-void
.end method

.method public reopen(Z)V
    .registers 3
    .parameter "animate"

    .prologue
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiui/v5/widget/menubar/MenuBar;->mIsCreated:Z

    iget-object v0, p0, Lmiui/v5/widget/menubar/MenuBar;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lmiui/v5/widget/menubar/MenuBar;->mVisibleItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    invoke-virtual {p0, p1}, Lmiui/v5/widget/menubar/MenuBar;->open(Z)V

    return-void
.end method

.method public setCallback(Lmiui/v5/widget/menubar/MenuBar$Callback;)V
    .registers 3
    .parameter "callback"

    .prologue
    const/4 v0, 0x0

    iput-object v0, p0, Lmiui/v5/widget/menubar/MenuBar;->mCallback:Lmiui/v5/widget/menubar/MenuBar$Callback;

    iput-object p1, p0, Lmiui/v5/widget/menubar/MenuBar;->mCallback:Lmiui/v5/widget/menubar/MenuBar$Callback;

    return-void
.end method

.method public setGroupCheckable(IZZ)V
    .registers 4
    .parameter "group"
    .parameter "checkable"
    .parameter "exclusive"

    .prologue
    return-void
.end method

.method public setGroupEnabled(IZ)V
    .registers 3
    .parameter "group"
    .parameter "enabled"

    .prologue
    return-void
.end method

.method public setGroupVisible(IZ)V
    .registers 3
    .parameter "group"
    .parameter "visible"

    .prologue
    return-void
.end method

.method public setQwertyMode(Z)V
    .registers 2
    .parameter "isQwerty"

    .prologue
    return-void
.end method

.method public size()I
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/menubar/MenuBar;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public startDispatchingItemsChanged()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    iput-boolean v1, p0, Lmiui/v5/widget/menubar/MenuBar;->mPreventDispatchingItemsChanged:Z

    iget-boolean v0, p0, Lmiui/v5/widget/menubar/MenuBar;->mItemsChangedWhileDispatchPrevented:Z

    if-eqz v0, :cond_d

    iput-boolean v1, p0, Lmiui/v5/widget/menubar/MenuBar;->mItemsChangedWhileDispatchPrevented:Z

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lmiui/v5/widget/menubar/MenuBar;->onItemsChanged(Z)V

    :cond_d
    return-void
.end method

.method public stopDispatchingItemsChanged()V
    .registers 2

    .prologue
    iget-boolean v0, p0, Lmiui/v5/widget/menubar/MenuBar;->mPreventDispatchingItemsChanged:Z

    if-nez v0, :cond_a

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiui/v5/widget/menubar/MenuBar;->mPreventDispatchingItemsChanged:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiui/v5/widget/menubar/MenuBar;->mItemsChangedWhileDispatchPrevented:Z

    :cond_a
    return-void
.end method
