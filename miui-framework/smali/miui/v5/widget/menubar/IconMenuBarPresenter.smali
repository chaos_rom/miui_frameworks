.class public Lmiui/v5/widget/menubar/IconMenuBarPresenter;
.super Ljava/lang/Object;
.source "IconMenuBarPresenter.java"

# interfaces
.implements Lmiui/v5/widget/menubar/MenuBarPresenter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/v5/widget/menubar/IconMenuBarPresenter$MenuBarAnimatorListener;
    }
.end annotation


# static fields
.field private static final DEFAULT_CLOSE_ANIMATOR_DURATION:I = 0x12c

.field private static final DEFAULT_OPEN_ANIMATOR_DURATION:I = 0x12c

.field private static final MAX_ITEMS:I = 0x4


# instance fields
.field mCallback:Lmiui/v5/widget/menubar/MenuBarPresenter$Callback;

.field private mCloseMenuBarAnimator:Landroid/animation/Animator;

.field private mContext:Landroid/content/Context;

.field private mIconMenuBarLayoutResId:I

.field private mIconMenuBarPrimaryItemResId:I

.field private mIconMenuBarSecondaryItemResId:I

.field private mMaxItems:I

.field private mMenu:Lmiui/v5/widget/menubar/MenuBar;

.field private mMenuBarAnimatorListener:Lmiui/v5/widget/menubar/IconMenuBarPresenter$MenuBarAnimatorListener;

.field private mMenuView:Lmiui/v5/widget/menubar/IconMenuBarView;

.field private mMoreIconDrawable:Landroid/graphics/drawable/Drawable;

.field private mMoreView:Lmiui/v5/widget/menubar/IconMenuBarPrimaryItemView;

.field private mOpenMenuBarAnimator:Landroid/animation/Animator;

.field private mPrimaryMaskDrawable:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;III)V
    .registers 6
    .parameter "context"
    .parameter "iconMenuBarLayoutResId"
    .parameter "iconMenuBarPrimaryItemResid"
    .parameter "iconMenuBarSecondaryItemResId"

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x4

    iput v0, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMaxItems:I

    iput-object p1, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mContext:Landroid/content/Context;

    iput p2, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mIconMenuBarLayoutResId:I

    iput p3, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mIconMenuBarPrimaryItemResId:I

    iput p4, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mIconMenuBarSecondaryItemResId:I

    new-instance v0, Lmiui/v5/widget/menubar/IconMenuBarPresenter$MenuBarAnimatorListener;

    invoke-direct {v0, p0}, Lmiui/v5/widget/menubar/IconMenuBarPresenter$MenuBarAnimatorListener;-><init>(Lmiui/v5/widget/menubar/IconMenuBarPresenter;)V

    iput-object v0, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenuBarAnimatorListener:Lmiui/v5/widget/menubar/IconMenuBarPresenter$MenuBarAnimatorListener;

    return-void
.end method

.method static synthetic access$000(Lmiui/v5/widget/menubar/IconMenuBarPresenter;)Landroid/animation/Animator;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mOpenMenuBarAnimator:Landroid/animation/Animator;

    return-object v0
.end method

.method static synthetic access$002(Lmiui/v5/widget/menubar/IconMenuBarPresenter;Landroid/animation/Animator;)Landroid/animation/Animator;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    iput-object p1, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mOpenMenuBarAnimator:Landroid/animation/Animator;

    return-object p1
.end method

.method static synthetic access$100(Lmiui/v5/widget/menubar/IconMenuBarPresenter;)Lmiui/v5/widget/menubar/IconMenuBarView;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenuView:Lmiui/v5/widget/menubar/IconMenuBarView;

    return-object v0
.end method

.method static synthetic access$200(Lmiui/v5/widget/menubar/IconMenuBarPresenter;)Lmiui/v5/widget/menubar/MenuBar;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenu:Lmiui/v5/widget/menubar/MenuBar;

    return-object v0
.end method

.method static synthetic access$302(Lmiui/v5/widget/menubar/IconMenuBarPresenter;Landroid/animation/Animator;)Landroid/animation/Animator;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    iput-object p1, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mCloseMenuBarAnimator:Landroid/animation/Animator;

    return-object p1
.end method


# virtual methods
.method protected addItemView(Landroid/view/ViewGroup;Landroid/view/View;I)V
    .registers 5
    .parameter "parent"
    .parameter "itemView"
    .parameter "childIndex"

    .prologue
    invoke-virtual {p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .local v0, currentParent:Landroid/view/ViewGroup;
    if-eqz v0, :cond_b

    invoke-virtual {v0, p2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_b
    invoke-virtual {p1, p2, p3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    return-void
.end method

.method public collapseItemActionView(Lmiui/v5/widget/menubar/MenuBar;Lmiui/v5/widget/menubar/MenuBarItem;)Z
    .registers 4
    .parameter "menu"
    .parameter "item"

    .prologue
    const/4 v0, 0x0

    return v0
.end method

.method protected createPrimaryItemView()Lmiui/v5/widget/menubar/MenuBarView$ItemView;
    .registers 4

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mContext:Landroid/content/Context;

    iget v1, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mIconMenuBarPrimaryItemResId:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiui/v5/widget/menubar/MenuBarView$ItemView;

    return-object v0
.end method

.method protected createSecondaryItemView()Lmiui/v5/widget/menubar/MenuBarView$ItemView;
    .registers 4

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mContext:Landroid/content/Context;

    iget v1, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mIconMenuBarSecondaryItemResId:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiui/v5/widget/menubar/MenuBarView$ItemView;

    return-object v0
.end method

.method public expandItemActionView(Lmiui/v5/widget/menubar/MenuBar;Lmiui/v5/widget/menubar/MenuBarItem;)Z
    .registers 4
    .parameter "menu"
    .parameter "item"

    .prologue
    const/4 v0, 0x0

    return v0
.end method

.method protected filterLeftoverView(Landroid/view/ViewGroup;I)Z
    .registers 5
    .parameter "parent"
    .parameter "childIndex"

    .prologue
    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMoreView:Lmiui/v5/widget/menubar/IconMenuBarPrimaryItemView;

    if-eq v0, v1, :cond_d

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->removeViewAt(I)V

    const/4 v0, 0x1

    :goto_c
    return v0

    :cond_d
    const/4 v0, 0x0

    goto :goto_c
.end method

.method public flagActionItems()Z
    .registers 2

    .prologue
    const/4 v0, 0x0

    return v0
.end method

.method public getId()I
    .registers 2

    .prologue
    const/4 v0, 0x0

    return v0
.end method

.method public getMaxItems()I
    .registers 2

    .prologue
    iget v0, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMaxItems:I

    return v0
.end method

.method public getMenuView(Landroid/view/ViewGroup;)Lmiui/v5/widget/menubar/MenuBarView;
    .registers 5
    .parameter "root"

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenuView:Lmiui/v5/widget/menubar/IconMenuBarView;

    if-nez v0, :cond_39

    iget-object v0, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mContext:Landroid/content/Context;

    iget v1, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mIconMenuBarLayoutResId:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiui/v5/widget/menubar/IconMenuBarView;

    iput-object v0, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenuView:Lmiui/v5/widget/menubar/IconMenuBarView;

    iget-object v0, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenuView:Lmiui/v5/widget/menubar/IconMenuBarView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lmiui/v5/widget/menubar/IconMenuBarView;->setVisibility(I)V

    iget-object v0, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenuView:Lmiui/v5/widget/menubar/IconMenuBarView;

    iget-object v1, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenu:Lmiui/v5/widget/menubar/MenuBar;

    invoke-virtual {v0, v1}, Lmiui/v5/widget/menubar/IconMenuBarView;->initialize(Lmiui/v5/widget/menubar/MenuBar;)V

    iget-object v0, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenuView:Lmiui/v5/widget/menubar/IconMenuBarView;

    iget-object v1, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mPrimaryMaskDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Lmiui/v5/widget/menubar/IconMenuBarView;->setPrimaryMaskDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenuView:Lmiui/v5/widget/menubar/IconMenuBarView;

    iget-object v1, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mContext:Landroid/content/Context;

    const v2, 0x6040016

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadLayoutAnimation(Landroid/content/Context;I)Landroid/view/animation/LayoutAnimationController;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/v5/widget/menubar/IconMenuBarView;->setPrimaryContainerLayoutAnimation(Landroid/view/animation/LayoutAnimationController;)V

    iget-object v0, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenuView:Lmiui/v5/widget/menubar/IconMenuBarView;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_39
    iget-object v0, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenuView:Lmiui/v5/widget/menubar/IconMenuBarView;

    return-object v0
.end method

.method protected getPrimaryItemView(Lmiui/v5/widget/menubar/MenuBarItem;Landroid/view/View;)Landroid/view/View;
    .registers 5
    .parameter "item"
    .parameter "convertView"

    .prologue
    instance-of v1, p2, Lmiui/v5/widget/menubar/MenuBarView$ItemView;

    if-eqz v1, :cond_11

    move-object v0, p2

    check-cast v0, Lmiui/v5/widget/menubar/MenuBarView$ItemView;

    .local v0, itemView:Lmiui/v5/widget/menubar/MenuBarView$ItemView;
    :goto_7
    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lmiui/v5/widget/menubar/MenuBarView$ItemView;->initialize(Lmiui/v5/widget/menubar/MenuBarItem;I)V

    invoke-virtual {p1, v0}, Lmiui/v5/widget/menubar/MenuBarItem;->setTag(Ljava/lang/Object;)Landroid/view/MenuItem;

    check-cast v0, Landroid/view/View;

    .end local v0           #itemView:Lmiui/v5/widget/menubar/MenuBarView$ItemView;
    return-object v0

    :cond_11
    invoke-virtual {p0}, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->createPrimaryItemView()Lmiui/v5/widget/menubar/MenuBarView$ItemView;

    move-result-object v0

    .restart local v0       #itemView:Lmiui/v5/widget/menubar/MenuBarView$ItemView;
    goto :goto_7
.end method

.method public getSecondaryItemView(Lmiui/v5/widget/menubar/MenuBarItem;Landroid/view/View;)Landroid/view/View;
    .registers 5
    .parameter "item"
    .parameter "convertView"

    .prologue
    instance-of v1, p2, Lmiui/v5/widget/menubar/MenuBarView$ItemView;

    if-eqz v1, :cond_11

    move-object v0, p2

    check-cast v0, Lmiui/v5/widget/menubar/MenuBarView$ItemView;

    .local v0, itemView:Lmiui/v5/widget/menubar/MenuBarView$ItemView;
    :goto_7
    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lmiui/v5/widget/menubar/MenuBarView$ItemView;->initialize(Lmiui/v5/widget/menubar/MenuBarItem;I)V

    invoke-virtual {p1, v0}, Lmiui/v5/widget/menubar/MenuBarItem;->setTag(Ljava/lang/Object;)Landroid/view/MenuItem;

    check-cast v0, Landroid/view/View;

    .end local v0           #itemView:Lmiui/v5/widget/menubar/MenuBarView$ItemView;
    return-object v0

    :cond_11
    invoke-virtual {p0}, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->createSecondaryItemView()Lmiui/v5/widget/menubar/MenuBarView$ItemView;

    move-result-object v0

    .restart local v0       #itemView:Lmiui/v5/widget/menubar/MenuBarView$ItemView;
    goto :goto_7
.end method

.method public initForMenu(Landroid/content/Context;Lmiui/v5/widget/menubar/MenuBar;)V
    .registers 3
    .parameter "context"
    .parameter "menu"

    .prologue
    iput-object p1, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenu:Lmiui/v5/widget/menubar/MenuBar;

    return-void
.end method

.method public onCloseMenu(Lmiui/v5/widget/menubar/MenuBar;Z)V
    .registers 7
    .parameter "menu"
    .parameter "animate"

    .prologue
    iget-object v1, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenuView:Lmiui/v5/widget/menubar/IconMenuBarView;

    if-nez v1, :cond_5

    :cond_4
    :goto_4
    return-void

    :cond_5
    iget-object v1, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenuView:Lmiui/v5/widget/menubar/IconMenuBarView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lmiui/v5/widget/menubar/IconMenuBarView;->requestExpand(Z)Z

    if-eqz p2, :cond_19

    iget-object v1, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenuView:Lmiui/v5/widget/menubar/IconMenuBarView;

    invoke-virtual {v1}, Lmiui/v5/widget/menubar/IconMenuBarView;->getVisibility()I

    move-result v0

    .local v0, oldMenuViewVisibility:I
    if-nez v0, :cond_4

    invoke-virtual {p0}, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->playCloseMenuBarAnimator()V

    goto :goto_4

    .end local v0           #oldMenuViewVisibility:I
    :cond_19
    iget-object v1, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenuView:Lmiui/v5/widget/menubar/IconMenuBarView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lmiui/v5/widget/menubar/IconMenuBarView;->setVisibility(I)V

    iget-object v1, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mCallback:Lmiui/v5/widget/menubar/MenuBarPresenter$Callback;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mCallback:Lmiui/v5/widget/menubar/MenuBarPresenter$Callback;

    iget-object v2, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenu:Lmiui/v5/widget/menubar/MenuBar;

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Lmiui/v5/widget/menubar/MenuBarPresenter$Callback;->onCloseMenu(Lmiui/v5/widget/menubar/MenuBar;Z)V

    goto :goto_4
.end method

.method public onExpandMenu(Lmiui/v5/widget/menubar/MenuBar;Z)Z
    .registers 4
    .parameter "menu"
    .parameter "expand"

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenuView:Lmiui/v5/widget/menubar/IconMenuBarView;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenuView:Lmiui/v5/widget/menubar/IconMenuBarView;

    invoke-virtual {v0, p2}, Lmiui/v5/widget/menubar/IconMenuBarView;->requestExpand(Z)Z

    move-result v0

    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public onOpenMenu(Lmiui/v5/widget/menubar/MenuBar;Z)V
    .registers 7
    .parameter "menu"
    .parameter "animate"

    .prologue
    const/4 v3, 0x1

    iget-object v1, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenuView:Lmiui/v5/widget/menubar/IconMenuBarView;

    if-nez v1, :cond_6

    :cond_5
    :goto_5
    return-void

    :cond_6
    invoke-virtual {p0, v3}, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->updateMenuView(Z)V

    iget-object v1, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenuView:Lmiui/v5/widget/menubar/IconMenuBarView;

    invoke-virtual {v1}, Lmiui/v5/widget/menubar/IconMenuBarView;->getVisibility()I

    move-result v0

    .local v0, oldMenuViewVisibility:I
    iget-object v1, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenuView:Lmiui/v5/widget/menubar/IconMenuBarView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lmiui/v5/widget/menubar/IconMenuBarView;->setVisibility(I)V

    if-eqz p2, :cond_27

    if-eqz v0, :cond_1d

    invoke-virtual {p0}, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->playOpenMenuBarAnimator()V

    goto :goto_5

    :cond_1d
    iget-object v1, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenuView:Lmiui/v5/widget/menubar/IconMenuBarView;

    invoke-virtual {v1}, Lmiui/v5/widget/menubar/IconMenuBarView;->getPrimaryContainer()Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->startLayoutAnimation()V

    goto :goto_5

    :cond_27
    iget-object v1, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mCallback:Lmiui/v5/widget/menubar/MenuBarPresenter$Callback;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mCallback:Lmiui/v5/widget/menubar/MenuBarPresenter$Callback;

    iget-object v2, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenu:Lmiui/v5/widget/menubar/MenuBar;

    invoke-interface {v1, v2, v3}, Lmiui/v5/widget/menubar/MenuBarPresenter$Callback;->onOpenMenu(Lmiui/v5/widget/menubar/MenuBar;Z)V

    goto :goto_5
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .registers 2
    .parameter "state"

    .prologue
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .registers 2

    .prologue
    const/4 v0, 0x0

    return-object v0
.end method

.method protected playCloseMenuBarAnimator()V
    .registers 14

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    const-wide/16 v9, 0x12c

    const/4 v8, 0x2

    iget-object v4, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenuView:Lmiui/v5/widget/menubar/IconMenuBarView;

    invoke-virtual {v4}, Lmiui/v5/widget/menubar/IconMenuBarView;->getPrimaryContainer()Landroid/widget/LinearLayout;

    move-result-object v1

    .local v1, primaryContainer:Landroid/view/View;
    iget-object v4, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenuView:Lmiui/v5/widget/menubar/IconMenuBarView;

    const-string v5, "TranslationY"

    new-array v6, v8, [F

    const/4 v7, 0x0

    aput v7, v6, v11

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v7

    int-to-float v7, v7

    aput v7, v6, v12

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .local v3, transite:Landroid/animation/Animator;
    invoke-virtual {v3, v9, v10}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    iget-object v4, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenuView:Lmiui/v5/widget/menubar/IconMenuBarView;

    const-string v5, "Alpha"

    new-array v6, v8, [F

    fill-array-data v6, :array_4e

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .local v0, alpha:Landroid/animation/Animator;
    invoke-virtual {v0, v9, v10}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    .local v2, set:Landroid/animation/AnimatorSet;
    invoke-virtual {v2, v9, v10}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    new-array v4, v8, [Landroid/animation/Animator;

    aput-object v3, v4, v11

    aput-object v0, v4, v12

    invoke-virtual {v2, v4}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    iget-object v4, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenuBarAnimatorListener:Lmiui/v5/widget/menubar/IconMenuBarPresenter$MenuBarAnimatorListener;

    invoke-virtual {v2, v4}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->start()V

    iput-object v2, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mCloseMenuBarAnimator:Landroid/animation/Animator;

    return-void

    :array_4e
    .array-data 0x4
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method protected playOpenMenuBarAnimator()V
    .registers 14

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    const-wide/16 v9, 0x12c

    const/4 v8, 0x2

    iget-object v4, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenuView:Lmiui/v5/widget/menubar/IconMenuBarView;

    invoke-virtual {v4}, Lmiui/v5/widget/menubar/IconMenuBarView;->getPrimaryContainer()Landroid/widget/LinearLayout;

    move-result-object v1

    .local v1, primaryContainer:Landroid/view/ViewGroup;
    const/16 v4, 0x8

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v4, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenuView:Lmiui/v5/widget/menubar/IconMenuBarView;

    const-string v5, "TranslationY"

    new-array v6, v8, [F

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v7

    int-to-float v7, v7

    aput v7, v6, v11

    const/4 v7, 0x0

    aput v7, v6, v12

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .local v3, transite:Landroid/animation/Animator;
    invoke-virtual {v3, v9, v10}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    iget-object v4, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenuView:Lmiui/v5/widget/menubar/IconMenuBarView;

    const-string v5, "Alpha"

    new-array v6, v8, [F

    fill-array-data v6, :array_54

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .local v0, alpha:Landroid/animation/Animator;
    invoke-virtual {v0, v9, v10}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    .local v2, set:Landroid/animation/AnimatorSet;
    invoke-virtual {v2, v9, v10}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    new-array v4, v8, [Landroid/animation/Animator;

    aput-object v3, v4, v11

    aput-object v0, v4, v12

    invoke-virtual {v2, v4}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    iget-object v4, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenuBarAnimatorListener:Lmiui/v5/widget/menubar/IconMenuBarPresenter$MenuBarAnimatorListener;

    invoke-virtual {v2, v4}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->start()V

    iput-object v2, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mOpenMenuBarAnimator:Landroid/animation/Animator;

    return-void

    nop

    :array_54
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
    .end array-data
.end method

.method public scroll(F)V
    .registers 3
    .parameter "percent"

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenuView:Lmiui/v5/widget/menubar/IconMenuBarView;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenuView:Lmiui/v5/widget/menubar/IconMenuBarView;

    invoke-virtual {v0, p1}, Lmiui/v5/widget/menubar/IconMenuBarView;->scroll(F)V

    :cond_9
    return-void
.end method

.method public scrollStateChanged(I)V
    .registers 3
    .parameter "state"

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenuView:Lmiui/v5/widget/menubar/IconMenuBarView;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenuView:Lmiui/v5/widget/menubar/IconMenuBarView;

    invoke-virtual {v0, p1}, Lmiui/v5/widget/menubar/IconMenuBarView;->scrollStateChanged(I)V

    :cond_9
    return-void
.end method

.method public setCallback(Lmiui/v5/widget/menubar/MenuBarPresenter$Callback;)V
    .registers 2
    .parameter "cb"

    .prologue
    iput-object p1, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mCallback:Lmiui/v5/widget/menubar/MenuBarPresenter$Callback;

    return-void
.end method

.method public setMaxItems(I)V
    .registers 3
    .parameter "maxItems"

    .prologue
    const/4 v0, 0x4

    if-ge p1, v0, :cond_4

    :goto_3
    return-void

    :cond_4
    iput p1, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMaxItems:I

    goto :goto_3
.end method

.method public setMoreIconDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 2
    .parameter "drawable"

    .prologue
    iput-object p1, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMoreIconDrawable:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method public setPrimaryMaskDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 2
    .parameter "drawable"

    .prologue
    iput-object p1, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mPrimaryMaskDrawable:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method public updateMenuView(Z)V
    .registers 26
    .parameter "cleared"

    .prologue
    move-object/from16 v0, p0

    iget-object v12, v0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenuView:Lmiui/v5/widget/menubar/IconMenuBarView;

    .local v12, menuView:Lmiui/v5/widget/menubar/IconMenuBarView;
    move-object/from16 v0, p0

    iget v0, v0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMaxItems:I

    move/from16 v20, v0

    if-gez v20, :cond_16

    invoke-virtual {v12}, Lmiui/v5/widget/menubar/IconMenuBarView;->getMaxItems()I

    move-result v20

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMaxItems:I

    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenu:Lmiui/v5/widget/menubar/MenuBar;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lmiui/v5/widget/menubar/MenuBar;->getVisibleItems()Ljava/util/ArrayList;

    move-result-object v19

    .local v19, visibleItems:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lmiui/v5/widget/menubar/MenuBarItem;>;"
    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v10

    .local v10, itemCount:I
    move-object/from16 v0, p0

    iget v0, v0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMaxItems:I

    move/from16 v20, v0

    move/from16 v0, v20

    if-le v10, v0, :cond_38

    const/4 v15, 0x1

    .local v15, needsMore:Z
    :goto_2f
    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenuView:Lmiui/v5/widget/menubar/IconMenuBarView;

    move-object/from16 v17, v0

    .local v17, parent:Landroid/view/ViewGroup;
    if-nez v17, :cond_3a

    :cond_37
    return-void

    .end local v15           #needsMore:Z
    .end local v17           #parent:Landroid/view/ViewGroup;
    :cond_38
    const/4 v15, 0x0

    goto :goto_2f

    .restart local v15       #needsMore:Z
    .restart local v17       #parent:Landroid/view/ViewGroup;
    :cond_3a
    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenu:Lmiui/v5/widget/menubar/MenuBar;

    move-object/from16 v20, v0

    if-eqz v20, :cond_37

    if-eqz v15, :cond_b4

    move-object/from16 v0, p0

    iget v0, v0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMaxItems:I

    move/from16 v20, v0

    add-int/lit8 v18, v20, -0x1

    .local v18, primaryCount:I
    :goto_4c
    const/4 v8, 0x0

    .local v8, i:I
    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenuView:Lmiui/v5/widget/menubar/IconMenuBarView;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lmiui/v5/widget/menubar/IconMenuBarView;->getPrimaryContainer()Landroid/widget/LinearLayout;

    move-result-object v6

    .local v6, container:Landroid/view/ViewGroup;
    const/4 v8, 0x0

    :goto_58
    move/from16 v0, v18

    if-ge v8, v0, :cond_bc

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lmiui/v5/widget/menubar/MenuBarItem;

    .local v9, item:Lmiui/v5/widget/menubar/MenuBarItem;
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v9, v0}, Lmiui/v5/widget/menubar/MenuBarItem;->setIsSecondary(Z)V

    invoke-virtual {v6, v8}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .local v7, convertView:Landroid/view/View;
    instance-of v0, v7, Lmiui/v5/widget/menubar/MenuBarView$ItemView;

    move/from16 v20, v0

    if-eqz v20, :cond_b9

    move-object/from16 v20, v7

    check-cast v20, Lmiui/v5/widget/menubar/MenuBarView$ItemView;

    invoke-interface/range {v20 .. v20}, Lmiui/v5/widget/menubar/MenuBarView$ItemView;->getItemData()Lmiui/v5/widget/menubar/MenuBarItem;

    move-result-object v16

    .local v16, oldItem:Lmiui/v5/widget/menubar/MenuBarItem;
    :goto_7d
    move-object/from16 v0, p0

    invoke-virtual {v0, v9, v7}, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->getPrimaryItemView(Lmiui/v5/widget/menubar/MenuBarItem;Landroid/view/View;)Landroid/view/View;

    move-result-object v11

    .local v11, itemView:Landroid/view/View;
    const v20, 0x7fffffff

    const/16 v21, 0x0

    move/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v11, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    move-object/from16 v0, v16

    if-eq v9, v0, :cond_9d

    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v11, v0}, Landroid/view/View;->setPressed(Z)V

    invoke-virtual {v11}, Landroid/view/View;->jumpDrawablesToCurrentState()V

    :cond_9d
    if-eq v11, v7, :cond_b1

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v11, v8}, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->addItemView(Landroid/view/ViewGroup;Landroid/view/View;I)V

    check-cast v11, Lmiui/v5/widget/menubar/IconMenuBarPrimaryItemView;

    .end local v11           #itemView:Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenuView:Lmiui/v5/widget/menubar/IconMenuBarView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v11, v0}, Lmiui/v5/widget/menubar/IconMenuBarPrimaryItemView;->setItemInvoker(Lmiui/v5/widget/menubar/MenuBar$ItemInvoker;)V

    :cond_b1
    add-int/lit8 v8, v8, 0x1

    goto :goto_58

    .end local v6           #container:Landroid/view/ViewGroup;
    .end local v7           #convertView:Landroid/view/View;
    .end local v8           #i:I
    .end local v9           #item:Lmiui/v5/widget/menubar/MenuBarItem;
    .end local v16           #oldItem:Lmiui/v5/widget/menubar/MenuBarItem;
    .end local v18           #primaryCount:I
    :cond_b4
    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v18

    goto :goto_4c

    .restart local v6       #container:Landroid/view/ViewGroup;
    .restart local v7       #convertView:Landroid/view/View;
    .restart local v8       #i:I
    .restart local v9       #item:Lmiui/v5/widget/menubar/MenuBarItem;
    .restart local v18       #primaryCount:I
    :cond_b9
    const/16 v16, 0x0

    goto :goto_7d

    .end local v7           #convertView:Landroid/view/View;
    .end local v9           #item:Lmiui/v5/widget/menubar/MenuBarItem;
    :cond_bc
    if-eqz v15, :cond_18a

    invoke-virtual {v6, v8}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v13

    .local v13, moreView:Landroid/view/View;
    if-eqz v13, :cond_171

    const v20, 0x7fffffff

    move/from16 v0, v20

    invoke-virtual {v13, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v14

    .local v14, moreViewTag:Ljava/lang/Object;
    :goto_cd
    if-eqz v13, :cond_d1

    if-nez v14, :cond_110

    :cond_d1
    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenuView:Lmiui/v5/widget/menubar/IconMenuBarView;

    move-object/from16 v21, v0

    move-object/from16 v20, v13

    check-cast v20, Lmiui/v5/widget/menubar/IconMenuBarPrimaryItemView;

    move-object/from16 v0, p0

    iget v0, v0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mIconMenuBarPrimaryItemResId:I

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMoreIconDrawable:Landroid/graphics/drawable/Drawable;

    move-object/from16 v23, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    move/from16 v2, v22

    move-object/from16 v3, v23

    invoke-virtual {v0, v1, v2, v3}, Lmiui/v5/widget/menubar/IconMenuBarView;->createMoreItemView(Lmiui/v5/widget/menubar/IconMenuBarPrimaryItemView;ILandroid/graphics/drawable/Drawable;)Lmiui/v5/widget/menubar/IconMenuBarPrimaryItemView;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMoreView:Lmiui/v5/widget/menubar/IconMenuBarPrimaryItemView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMoreView:Lmiui/v5/widget/menubar/IconMenuBarPrimaryItemView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    if-eq v0, v13, :cond_110

    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMoreView:Lmiui/v5/widget/menubar/IconMenuBarPrimaryItemView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v6, v1, v8}, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->addItemView(Landroid/view/ViewGroup;Landroid/view/View;I)V

    :cond_110
    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenuView:Lmiui/v5/widget/menubar/IconMenuBarView;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lmiui/v5/widget/menubar/IconMenuBarView;->getSecondaryContainer()Landroid/widget/LinearLayout;

    move-result-object v6

    const/4 v4, 0x0

    .local v4, childIndex:I
    move v5, v4

    .end local v4           #childIndex:I
    .local v5, childIndex:I
    :goto_11c
    if-ge v8, v10, :cond_1bb

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lmiui/v5/widget/menubar/MenuBarItem;

    .restart local v9       #item:Lmiui/v5/widget/menubar/MenuBarItem;
    const/16 v20, 0x1

    move/from16 v0, v20

    invoke-virtual {v9, v0}, Lmiui/v5/widget/menubar/MenuBarItem;->setIsSecondary(Z)V

    add-int/lit8 v4, v5, 0x1

    .end local v5           #childIndex:I
    .restart local v4       #childIndex:I
    invoke-virtual {v6, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .restart local v7       #convertView:Landroid/view/View;
    instance-of v0, v7, Lmiui/v5/widget/menubar/MenuBarView$ItemView;

    move/from16 v20, v0

    if-eqz v20, :cond_174

    move-object/from16 v20, v7

    check-cast v20, Lmiui/v5/widget/menubar/MenuBarView$ItemView;

    invoke-interface/range {v20 .. v20}, Lmiui/v5/widget/menubar/MenuBarView$ItemView;->getItemData()Lmiui/v5/widget/menubar/MenuBarItem;

    move-result-object v16

    .restart local v16       #oldItem:Lmiui/v5/widget/menubar/MenuBarItem;
    :goto_141
    move-object/from16 v0, p0

    invoke-virtual {v0, v9, v7}, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->getSecondaryItemView(Lmiui/v5/widget/menubar/MenuBarItem;Landroid/view/View;)Landroid/view/View;

    move-result-object v11

    .restart local v11       #itemView:Landroid/view/View;
    move-object/from16 v0, v16

    if-eq v9, v0, :cond_155

    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v11, v0}, Landroid/view/View;->setPressed(Z)V

    invoke-virtual {v11}, Landroid/view/View;->jumpDrawablesToCurrentState()V

    :cond_155
    if-eq v11, v7, :cond_16d

    sub-int v20, v8, v18

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v6, v11, v1}, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->addItemView(Landroid/view/ViewGroup;Landroid/view/View;I)V

    check-cast v11, Lmiui/v5/widget/menubar/IconMenuBarSecondaryItemView;

    .end local v11           #itemView:Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenuView:Lmiui/v5/widget/menubar/IconMenuBarView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v11, v0}, Lmiui/v5/widget/menubar/IconMenuBarSecondaryItemView;->setItemInvoker(Lmiui/v5/widget/menubar/MenuBar$ItemInvoker;)V

    :cond_16d
    add-int/lit8 v8, v8, 0x1

    move v5, v4

    .end local v4           #childIndex:I
    .restart local v5       #childIndex:I
    goto :goto_11c

    .end local v5           #childIndex:I
    .end local v7           #convertView:Landroid/view/View;
    .end local v9           #item:Lmiui/v5/widget/menubar/MenuBarItem;
    .end local v14           #moreViewTag:Ljava/lang/Object;
    .end local v16           #oldItem:Lmiui/v5/widget/menubar/MenuBarItem;
    :cond_171
    const/4 v14, 0x0

    goto/16 :goto_cd

    .restart local v4       #childIndex:I
    .restart local v7       #convertView:Landroid/view/View;
    .restart local v9       #item:Lmiui/v5/widget/menubar/MenuBarItem;
    .restart local v14       #moreViewTag:Ljava/lang/Object;
    :cond_174
    const/16 v16, 0x0

    goto :goto_141

    .end local v7           #convertView:Landroid/view/View;
    .end local v9           #item:Lmiui/v5/widget/menubar/MenuBarItem;
    :cond_177
    :goto_177
    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v20

    move/from16 v0, v20

    if-ge v4, v0, :cond_37

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v4}, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->filterLeftoverView(Landroid/view/ViewGroup;I)Z

    move-result v20

    if-nez v20, :cond_177

    add-int/lit8 v4, v4, 0x1

    goto :goto_177

    .end local v4           #childIndex:I
    .end local v13           #moreView:Landroid/view/View;
    .end local v14           #moreViewTag:Ljava/lang/Object;
    :cond_18a
    :goto_18a
    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v20

    move/from16 v0, v20

    if-ge v8, v0, :cond_19d

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v8}, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->filterLeftoverView(Landroid/view/ViewGroup;I)Z

    move-result v20

    if-nez v20, :cond_18a

    add-int/lit8 v8, v8, 0x1

    goto :goto_18a

    :cond_19d
    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenuView:Lmiui/v5/widget/menubar/IconMenuBarView;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lmiui/v5/widget/menubar/IconMenuBarView;->getSecondaryContainer()Landroid/widget/LinearLayout;

    move-result-object v6

    :cond_1a8
    :goto_1a8
    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v20

    move/from16 v0, v20

    if-ge v8, v0, :cond_37

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v8}, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->filterLeftoverView(Landroid/view/ViewGroup;I)Z

    move-result v20

    if-nez v20, :cond_1a8

    add-int/lit8 v8, v8, 0x1

    goto :goto_1a8

    .restart local v5       #childIndex:I
    .restart local v13       #moreView:Landroid/view/View;
    .restart local v14       #moreViewTag:Ljava/lang/Object;
    :cond_1bb
    move v4, v5

    .end local v5           #childIndex:I
    .restart local v4       #childIndex:I
    goto :goto_177
.end method
