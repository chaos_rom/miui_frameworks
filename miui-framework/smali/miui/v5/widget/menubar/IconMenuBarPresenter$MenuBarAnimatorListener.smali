.class Lmiui/v5/widget/menubar/IconMenuBarPresenter$MenuBarAnimatorListener;
.super Ljava/lang/Object;
.source "IconMenuBarPresenter.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/v5/widget/menubar/IconMenuBarPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MenuBarAnimatorListener"
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/v5/widget/menubar/IconMenuBarPresenter;


# direct methods
.method public constructor <init>(Lmiui/v5/widget/menubar/IconMenuBarPresenter;)V
    .registers 2
    .parameter

    .prologue
    iput-object p1, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter$MenuBarAnimatorListener;->this$0:Lmiui/v5/widget/menubar/IconMenuBarPresenter;

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .registers 2
    .parameter "animation"

    .prologue
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .registers 7
    .parameter "animator"

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    iget-object v1, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter$MenuBarAnimatorListener;->this$0:Lmiui/v5/widget/menubar/IconMenuBarPresenter;

    iget-object v1, v1, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mCallback:Lmiui/v5/widget/menubar/MenuBarPresenter$Callback;

    if-eqz v1, :cond_33

    iget-object v1, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter$MenuBarAnimatorListener;->this$0:Lmiui/v5/widget/menubar/IconMenuBarPresenter;

    #getter for: Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mOpenMenuBarAnimator:Landroid/animation/Animator;
    invoke-static {v1}, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->access$000(Lmiui/v5/widget/menubar/IconMenuBarPresenter;)Landroid/animation/Animator;

    move-result-object v1

    if-ne p1, v1, :cond_34

    iget-object v1, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter$MenuBarAnimatorListener;->this$0:Lmiui/v5/widget/menubar/IconMenuBarPresenter;

    #getter for: Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenuView:Lmiui/v5/widget/menubar/IconMenuBarView;
    invoke-static {v1}, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->access$100(Lmiui/v5/widget/menubar/IconMenuBarPresenter;)Lmiui/v5/widget/menubar/IconMenuBarView;

    move-result-object v1

    invoke-virtual {v1}, Lmiui/v5/widget/menubar/IconMenuBarView;->getPrimaryContainer()Landroid/widget/LinearLayout;

    move-result-object v0

    .local v0, primaryContainer:Landroid/widget/LinearLayout;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->startLayoutAnimation()V

    iget-object v1, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter$MenuBarAnimatorListener;->this$0:Lmiui/v5/widget/menubar/IconMenuBarPresenter;

    iget-object v1, v1, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mCallback:Lmiui/v5/widget/menubar/MenuBarPresenter$Callback;

    iget-object v2, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter$MenuBarAnimatorListener;->this$0:Lmiui/v5/widget/menubar/IconMenuBarPresenter;

    #getter for: Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenu:Lmiui/v5/widget/menubar/MenuBar;
    invoke-static {v2}, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->access$200(Lmiui/v5/widget/menubar/IconMenuBarPresenter;)Lmiui/v5/widget/menubar/MenuBar;

    move-result-object v2

    invoke-interface {v1, v2, v3}, Lmiui/v5/widget/menubar/MenuBarPresenter$Callback;->onOpenMenu(Lmiui/v5/widget/menubar/MenuBar;Z)V

    iget-object v1, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter$MenuBarAnimatorListener;->this$0:Lmiui/v5/widget/menubar/IconMenuBarPresenter;

    #setter for: Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mOpenMenuBarAnimator:Landroid/animation/Animator;
    invoke-static {v1, v4}, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->access$002(Lmiui/v5/widget/menubar/IconMenuBarPresenter;Landroid/animation/Animator;)Landroid/animation/Animator;

    .end local v0           #primaryContainer:Landroid/widget/LinearLayout;
    :cond_33
    :goto_33
    return-void

    :cond_34
    iget-object v1, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter$MenuBarAnimatorListener;->this$0:Lmiui/v5/widget/menubar/IconMenuBarPresenter;

    iget-object v1, v1, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mCallback:Lmiui/v5/widget/menubar/MenuBarPresenter$Callback;

    iget-object v2, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter$MenuBarAnimatorListener;->this$0:Lmiui/v5/widget/menubar/IconMenuBarPresenter;

    #getter for: Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenu:Lmiui/v5/widget/menubar/MenuBar;
    invoke-static {v2}, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->access$200(Lmiui/v5/widget/menubar/IconMenuBarPresenter;)Lmiui/v5/widget/menubar/MenuBar;

    move-result-object v2

    invoke-interface {v1, v2, v3}, Lmiui/v5/widget/menubar/MenuBarPresenter$Callback;->onCloseMenu(Lmiui/v5/widget/menubar/MenuBar;Z)V

    iget-object v1, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter$MenuBarAnimatorListener;->this$0:Lmiui/v5/widget/menubar/IconMenuBarPresenter;

    #getter for: Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenuView:Lmiui/v5/widget/menubar/IconMenuBarView;
    invoke-static {v1}, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->access$100(Lmiui/v5/widget/menubar/IconMenuBarPresenter;)Lmiui/v5/widget/menubar/IconMenuBarView;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lmiui/v5/widget/menubar/IconMenuBarView;->setVisibility(I)V

    iget-object v1, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter$MenuBarAnimatorListener;->this$0:Lmiui/v5/widget/menubar/IconMenuBarPresenter;

    #setter for: Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mCloseMenuBarAnimator:Landroid/animation/Animator;
    invoke-static {v1, v4}, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->access$302(Lmiui/v5/widget/menubar/IconMenuBarPresenter;Landroid/animation/Animator;)Landroid/animation/Animator;

    goto :goto_33
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .registers 2
    .parameter "animator"

    .prologue
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .registers 4
    .parameter "animator"

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter$MenuBarAnimatorListener;->this$0:Lmiui/v5/widget/menubar/IconMenuBarPresenter;

    #getter for: Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mOpenMenuBarAnimator:Landroid/animation/Animator;
    invoke-static {v0}, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->access$000(Lmiui/v5/widget/menubar/IconMenuBarPresenter;)Landroid/animation/Animator;

    move-result-object v0

    if-ne p1, v0, :cond_12

    iget-object v0, p0, Lmiui/v5/widget/menubar/IconMenuBarPresenter$MenuBarAnimatorListener;->this$0:Lmiui/v5/widget/menubar/IconMenuBarPresenter;

    #getter for: Lmiui/v5/widget/menubar/IconMenuBarPresenter;->mMenuView:Lmiui/v5/widget/menubar/IconMenuBarView;
    invoke-static {v0}, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->access$100(Lmiui/v5/widget/menubar/IconMenuBarPresenter;)Lmiui/v5/widget/menubar/IconMenuBarView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmiui/v5/widget/menubar/IconMenuBarView;->setVisibility(I)V

    :cond_12
    return-void
.end method
