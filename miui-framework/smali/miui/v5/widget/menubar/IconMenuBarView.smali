.class public Lmiui/v5/widget/menubar/IconMenuBarView;
.super Landroid/view/ViewGroup;
.source "IconMenuBarView.java"

# interfaces
.implements Lmiui/v5/widget/menubar/MenuBar$ItemInvoker;
.implements Lmiui/v5/widget/menubar/MenuBarView;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/v5/widget/menubar/IconMenuBarView$ExpandAnimation;,
        Lmiui/v5/widget/menubar/IconMenuBarView$LayoutParams;
    }
.end annotation


# static fields
.field static final IS_MORE:I = 0x7fffffff

.field private static final VIEW_STATE_COLLAPSED:I = 0x0

.field private static final VIEW_STATE_COLLAPSING:I = 0x1

.field private static final VIEW_STATE_EXPANDED:I = 0x3

.field private static final VIEW_STATE_EXPANDING:I = 0x2


# instance fields
.field private mDimContainer:Landroid/view/View;

.field mExpandAnimation:Lmiui/v5/widget/menubar/IconMenuBarView$ExpandAnimation;

.field private mExpandDuration:I

.field private mMaxItems:I

.field private mMenu:Lmiui/v5/widget/menubar/MenuBar;

.field private mMoreIconView:Landroid/view/View;

.field private mPrimaryContainer:Landroid/widget/LinearLayout;

.field private mPrimaryContainerAndMask:Landroid/view/View;

.field private mPrimaryMask:Landroid/view/View;

.field private mSecondaryContainer:Landroid/widget/LinearLayout;

.field private mViewState:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    .prologue
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput v0, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mViewState:I

    new-instance v0, Lmiui/v5/widget/menubar/IconMenuBarView$ExpandAnimation;

    invoke-direct {v0, p0}, Lmiui/v5/widget/menubar/IconMenuBarView$ExpandAnimation;-><init>(Lmiui/v5/widget/menubar/IconMenuBarView;)V

    iput-object v0, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mExpandAnimation:Lmiui/v5/widget/menubar/IconMenuBarView$ExpandAnimation;

    const/16 v0, 0x12c

    iput v0, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mExpandDuration:I

    return-void
.end method

.method static synthetic access$000(Lmiui/v5/widget/menubar/IconMenuBarView;)Landroid/view/View;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mPrimaryContainerAndMask:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$100(Lmiui/v5/widget/menubar/IconMenuBarView;)Landroid/widget/LinearLayout;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mSecondaryContainer:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$200(Lmiui/v5/widget/menubar/IconMenuBarView;)Landroid/view/View;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mDimContainer:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$300(Lmiui/v5/widget/menubar/IconMenuBarView;)Landroid/view/View;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mPrimaryMask:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$402(Lmiui/v5/widget/menubar/IconMenuBarView;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    iput p1, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mViewState:I

    return p1
.end method

.method static synthetic access$500(Lmiui/v5/widget/menubar/IconMenuBarView;)Landroid/view/View;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mMoreIconView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$600(Lmiui/v5/widget/menubar/IconMenuBarView;)Lmiui/v5/widget/menubar/MenuBar;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mMenu:Lmiui/v5/widget/menubar/MenuBar;

    return-object v0
.end method


# virtual methods
.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .registers 3
    .parameter "p"

    .prologue
    instance-of v0, p1, Lmiui/v5/widget/menubar/IconMenuBarView$LayoutParams;

    return v0
.end method

.method protected createMoreItemView(Lmiui/v5/widget/menubar/IconMenuBarPrimaryItemView;ILandroid/graphics/drawable/Drawable;)Lmiui/v5/widget/menubar/IconMenuBarPrimaryItemView;
    .registers 9
    .parameter "convertView"
    .parameter "iconMenuBarPrimaryItemResId"
    .parameter "moreIconDrawable"

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x0

    .local v0, itemView:Lmiui/v5/widget/menubar/IconMenuBarPrimaryItemView;
    if-eqz p1, :cond_2c

    move-object v0, p1

    :goto_6
    iget-object v2, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .local v1, r:Landroid/content/res/Resources;
    const v2, 0x60c0252

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Lmiui/v5/widget/menubar/IconMenuBarPrimaryItemView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v3, p3, v3, v3}, Lmiui/v5/widget/menubar/IconMenuBarPrimaryItemView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v0, v4}, Lmiui/v5/widget/menubar/IconMenuBarPrimaryItemView;->setCheckable(Z)V

    invoke-virtual {v0, p0}, Lmiui/v5/widget/menubar/IconMenuBarPrimaryItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7fffffff

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lmiui/v5/widget/menubar/IconMenuBarPrimaryItemView;->setTag(ILjava/lang/Object;)V

    iput-object v0, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mMoreIconView:Landroid/view/View;

    return-object v0

    .end local v1           #r:Landroid/content/res/Resources;
    :cond_2c
    iget-object v2, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mContext:Landroid/content/Context;

    invoke-static {v2, p2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .end local v0           #itemView:Lmiui/v5/widget/menubar/IconMenuBarPrimaryItemView;
    check-cast v0, Lmiui/v5/widget/menubar/IconMenuBarPrimaryItemView;

    .restart local v0       #itemView:Lmiui/v5/widget/menubar/IconMenuBarPrimaryItemView;
    goto :goto_6
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .registers 3
    .parameter "x0"

    .prologue
    invoke-virtual {p0, p1}, Lmiui/v5/widget/menubar/IconMenuBarView;->generateLayoutParams(Landroid/util/AttributeSet;)Lmiui/v5/widget/menubar/IconMenuBarView$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Lmiui/v5/widget/menubar/IconMenuBarView$LayoutParams;
    .registers 4
    .parameter "attrs"

    .prologue
    new-instance v0, Lmiui/v5/widget/menubar/IconMenuBarView$LayoutParams;

    invoke-virtual {p0}, Lmiui/v5/widget/menubar/IconMenuBarView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lmiui/v5/widget/menubar/IconMenuBarView$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public getMaxItems()I
    .registers 2

    .prologue
    iget v0, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mMaxItems:I

    return v0
.end method

.method getPrimaryContainer()Landroid/widget/LinearLayout;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mPrimaryContainer:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public getPrimaryContainerLayoutAnimation()Landroid/view/animation/LayoutAnimationController;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mPrimaryContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutAnimation()Landroid/view/animation/LayoutAnimationController;

    move-result-object v0

    return-object v0
.end method

.method getSecondaryContainer()Landroid/widget/LinearLayout;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mSecondaryContainer:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public getSecondaryContainerLayoutAnimation()Landroid/view/animation/LayoutAnimationController;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mSecondaryContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutAnimation()Landroid/view/animation/LayoutAnimationController;

    move-result-object v0

    return-object v0
.end method

.method public getWindowAnimations()I
    .registers 2

    .prologue
    const/4 v0, 0x0

    return v0
.end method

.method public initialize(Lmiui/v5/widget/menubar/MenuBar;)V
    .registers 2
    .parameter "menu"

    .prologue
    iput-object p1, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mMenu:Lmiui/v5/widget/menubar/MenuBar;

    return-void
.end method

.method public invokeItem(Lmiui/v5/widget/menubar/MenuBarItem;)Z
    .registers 5
    .parameter "item"

    .prologue
    const/4 v2, 0x0

    iget-object v1, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mMenu:Lmiui/v5/widget/menubar/MenuBar;

    invoke-virtual {v1, p1, v2}, Lmiui/v5/widget/menubar/MenuBar;->performItemAction(Landroid/view/MenuItem;I)Z

    move-result v0

    .local v0, invoked:Z
    if-eqz v0, :cond_c

    invoke-virtual {p0, v2}, Lmiui/v5/widget/menubar/IconMenuBarView;->requestExpand(Z)Z

    :cond_c
    return v0
.end method

.method public isExpanded()Z
    .registers 3

    .prologue
    iget v0, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mViewState:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public onClick(Landroid/view/View;)V
    .registers 3
    .parameter "v"

    .prologue
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_c

    :goto_7
    return-void

    :pswitch_8
    invoke-virtual {p0}, Lmiui/v5/widget/menubar/IconMenuBarView;->onMoreItemClick()V

    goto :goto_7

    :pswitch_data_c
    .packed-switch 0x60b00a4
        :pswitch_8
        :pswitch_8
    .end packed-switch
.end method

.method protected onFinishInflate()V
    .registers 3

    .prologue
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    const v0, 0x60b00a5

    invoke-virtual {p0, v0}, Lmiui/v5/widget/menubar/IconMenuBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mDimContainer:Landroid/view/View;

    iget-object v0, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mDimContainer:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x60b00a7

    invoke-virtual {p0, v0}, Lmiui/v5/widget/menubar/IconMenuBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mPrimaryContainerAndMask:Landroid/view/View;

    iget-object v0, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mPrimaryContainerAndMask:Landroid/view/View;

    const v1, 0x60b00a8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mPrimaryContainer:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mPrimaryContainerAndMask:Landroid/view/View;

    const v1, 0x60b00a6

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mPrimaryMask:Landroid/view/View;

    const v0, 0x60b00a9

    invoke-virtual {p0, v0}, Lmiui/v5/widget/menubar/IconMenuBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mSecondaryContainer:Landroid/widget/LinearLayout;

    return-void
.end method

.method protected onLayout(ZIIII)V
    .registers 9
    .parameter "changed"
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"

    .prologue
    sub-int v0, p5, p3

    .local v0, height:I
    iget-object v1, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mDimContainer:Landroid/view/View;

    invoke-virtual {v1, p2, p3, p4, p5}, Landroid/view/View;->layout(IIII)V

    iget-object v1, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mPrimaryContainerAndMask:Landroid/view/View;

    iget-object v2, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mPrimaryContainerAndMask:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    sub-int v2, v0, v2

    invoke-virtual {v1, p2, v2, p4, p5}, Landroid/view/View;->layout(IIII)V

    iget-object v1, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mSecondaryContainer:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mSecondaryContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v2, p5

    invoke-virtual {v1, p2, p5, p4, v2}, Landroid/widget/LinearLayout;->layout(IIII)V

    return-void
.end method

.method protected onMeasure(II)V
    .registers 12
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    .prologue
    const/4 v7, 0x0

    const/high16 v6, 0x4000

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v5

    .local v5, widthMode:I
    if-eq v5, v6, :cond_30

    new-instance v6, Ljava/lang/IllegalStateException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " can only be used "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "with android:layout_width=\"match_parent\" (or fill_parent)"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_30
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .local v3, heightMode:I
    if-eq v3, v6, :cond_5d

    new-instance v6, Ljava/lang/IllegalStateException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " can only be used "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "with android:layout_width=\"match_parent\" (or fill_parent)"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_5d
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .local v4, width:I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .local v2, height:I
    invoke-static {v4, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .local v1, childWidthMeasureSpec:I
    invoke-static {v7, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .local v0, childHeightMeasureSpec:I
    iget-object v6, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mDimContainer:Landroid/view/View;

    invoke-virtual {v6, v1, v0}, Landroid/view/View;->measure(II)V

    iget-object v6, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mPrimaryContainerAndMask:Landroid/view/View;

    invoke-virtual {v6, v1, v0}, Landroid/view/View;->measure(II)V

    iget-object v6, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mSecondaryContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v1, v0}, Landroid/widget/LinearLayout;->measure(II)V

    invoke-virtual {p0, v4, v2}, Lmiui/v5/widget/menubar/IconMenuBarView;->setMeasuredDimension(II)V

    return-void
.end method

.method onMoreItemClick()V
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mMoreIconView:Landroid/view/View;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mMoreIconView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isSelected()Z

    move-result v0

    if-nez v0, :cond_11

    const/4 v0, 0x1

    :goto_d
    invoke-virtual {p0, v0}, Lmiui/v5/widget/menubar/IconMenuBarView;->requestExpand(Z)Z

    :cond_10
    return-void

    :cond_11
    const/4 v0, 0x0

    goto :goto_d
.end method

.method requestExpand(Z)Z
    .registers 6
    .parameter "expand"

    .prologue
    const/4 v0, 0x0

    .local v0, handled:Z
    iget v1, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mViewState:I

    packed-switch v1, :pswitch_data_40

    :cond_6
    :goto_6
    :pswitch_6
    if-eqz v0, :cond_31

    if-eqz p1, :cond_f

    iget-object v1, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mSecondaryContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->startLayoutAnimation()V

    :cond_f
    iget-object v1, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mExpandAnimation:Lmiui/v5/widget/menubar/IconMenuBarView$ExpandAnimation;

    invoke-virtual {v1}, Lmiui/v5/widget/menubar/IconMenuBarView$ExpandAnimation;->reset()V

    iget-object v1, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mExpandAnimation:Lmiui/v5/widget/menubar/IconMenuBarView$ExpandAnimation;

    iget v2, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mExpandDuration:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Lmiui/v5/widget/menubar/IconMenuBarView$ExpandAnimation;->setDuration(J)V

    iget-object v1, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mExpandAnimation:Lmiui/v5/widget/menubar/IconMenuBarView$ExpandAnimation;

    invoke-virtual {v1, p1}, Lmiui/v5/widget/menubar/IconMenuBarView$ExpandAnimation;->expand(Z)Lmiui/v5/widget/menubar/IconMenuBarView$ExpandAnimation;

    move-result-object v1

    iget-object v2, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mSecondaryContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Lmiui/v5/widget/menubar/IconMenuBarView$ExpandAnimation;->transition(F)Lmiui/v5/widget/menubar/IconMenuBarView$ExpandAnimation;

    iget-object v1, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mExpandAnimation:Lmiui/v5/widget/menubar/IconMenuBarView$ExpandAnimation;

    invoke-virtual {p0, v1}, Lmiui/v5/widget/menubar/IconMenuBarView;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_31
    return v0

    :pswitch_32
    if-eqz p1, :cond_6

    const/4 v1, 0x2

    iput v1, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mViewState:I

    const/4 v0, 0x1

    goto :goto_6

    :pswitch_39
    if-nez p1, :cond_6

    const/4 v1, 0x1

    iput v1, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mViewState:I

    const/4 v0, 0x1

    goto :goto_6

    :pswitch_data_40
    .packed-switch 0x0
        :pswitch_32
        :pswitch_6
        :pswitch_6
        :pswitch_39
    .end packed-switch
.end method

.method public scroll(F)V
    .registers 5
    .parameter "percent"

    .prologue
    iget-object v1, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mPrimaryContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v1

    int-to-float v0, v1

    .local v0, height:F
    iget-object v1, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mPrimaryContainer:Landroid/widget/LinearLayout;

    mul-float v2, p1, v0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setTranslationY(F)V

    return-void
.end method

.method public scrollStateChanged(I)V
    .registers 2
    .parameter "state"

    .prologue
    return-void
.end method

.method public setPrimaryContainerLayoutAnimation(Landroid/view/animation/LayoutAnimationController;)V
    .registers 3
    .parameter "animation"

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mPrimaryContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setLayoutAnimation(Landroid/view/animation/LayoutAnimationController;)V

    return-void
.end method

.method public setPrimaryMaskDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 3
    .parameter "drawable"

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mPrimaryMask:Landroid/view/View;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mPrimaryMask:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :cond_9
    return-void
.end method

.method public setSecondaryContainerLayoutAnimation(Landroid/view/animation/LayoutAnimationController;)V
    .registers 3
    .parameter "animation"

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/menubar/IconMenuBarView;->mSecondaryContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setLayoutAnimation(Landroid/view/animation/LayoutAnimationController;)V

    return-void
.end method
