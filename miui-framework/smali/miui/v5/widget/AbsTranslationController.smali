.class public abstract Lmiui/v5/widget/AbsTranslationController;
.super Ljava/lang/Object;
.source "AbsTranslationController.java"

# interfaces
.implements Lmiui/v5/widget/MotionDetectListener;
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/v5/widget/AbsTranslationController$OnTranslateListener;,
        Lmiui/v5/widget/AbsTranslationController$TranslateAnimationListener;
    }
.end annotation


# static fields
.field static final TAG:Ljava/lang/String; = null

.field public static final TRANSLATE_STATE_FLING:I = 0x2

.field public static final TRANSLATE_STATE_IDLE:I = 0x0

.field public static final TRANSLATE_STATE_TOUCH:I = 0x1


# instance fields
.field private mAnimator:Landroid/animation/ValueAnimator;

.field private final mListener:Lmiui/v5/widget/MotionDetectStrategy;

.field private final mMaxAnchorDuration:I

.field protected final mMaximumVelocity:I

.field protected final mMinimumVelocity:I

.field private mTranslateListener:Lmiui/v5/widget/AbsTranslationController$OnTranslateListener;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    const-class v0, Lmiui/v5/widget/AbsTranslationController;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lmiui/v5/widget/AbsTranslationController;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lmiui/v5/widget/MotionDetectStrategy;)V
    .registers 6
    .parameter "context"
    .parameter "mml"

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lmiui/v5/widget/AbsTranslationController;->mListener:Lmiui/v5/widget/MotionDetectStrategy;

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .local v0, configuration:Landroid/view/ViewConfiguration;
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v2

    iput v2, p0, Lmiui/v5/widget/AbsTranslationController;->mMinimumVelocity:I

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v2

    iput v2, p0, Lmiui/v5/widget/AbsTranslationController;->mMaximumVelocity:I

    invoke-static {p1}, Lmiui/v5/widget/MiuiViewConfiguration;->get(Landroid/content/Context;)Lmiui/v5/widget/MiuiViewConfiguration;

    move-result-object v1

    .local v1, miuiConfig:Lmiui/v5/widget/MiuiViewConfiguration;
    invoke-virtual {v1}, Lmiui/v5/widget/MiuiViewConfiguration;->getMaxAnchorDuration()I

    move-result v2

    iput v2, p0, Lmiui/v5/widget/AbsTranslationController;->mMaxAnchorDuration:I

    return-void
.end method

.method private fling(Landroid/view/View;IIIZ)V
    .registers 10
    .parameter "view"
    .parameter "from"
    .parameter "delta"
    .parameter "velocity"
    .parameter "anim"

    .prologue
    const/4 v3, 0x2

    iget-object v0, p0, Lmiui/v5/widget/AbsTranslationController;->mAnimator:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lmiui/v5/widget/AbsTranslationController;->mAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmiui/v5/widget/AbsTranslationController;->mAnimator:Landroid/animation/ValueAnimator;

    :cond_d
    if-eqz p3, :cond_44

    if-eqz p5, :cond_4c

    new-array v0, v3, [F

    fill-array-data v0, :array_58

    invoke-static {v0}, Landroid/animation/ObjectAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lmiui/v5/widget/AbsTranslationController;->mAnimator:Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lmiui/v5/widget/AbsTranslationController;->mAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v0, p0, Lmiui/v5/widget/AbsTranslationController;->mAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {p0, p3, p4}, Lmiui/v5/widget/AbsTranslationController;->getDuration(II)I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lmiui/v5/widget/AbsTranslationController;->mAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, p0}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v0, p0, Lmiui/v5/widget/AbsTranslationController;->mAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lmiui/v5/widget/AbsTranslationController$TranslateAnimationListener;

    invoke-direct {v1, p0, p1, p2, p3}, Lmiui/v5/widget/AbsTranslationController$TranslateAnimationListener;-><init>(Lmiui/v5/widget/AbsTranslationController;Landroid/view/View;II)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    iget-object v0, p0, Lmiui/v5/widget/AbsTranslationController;->mAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    :cond_44
    :goto_44
    iget-object v0, p0, Lmiui/v5/widget/AbsTranslationController;->mAnimator:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_53

    invoke-virtual {p0, v3}, Lmiui/v5/widget/AbsTranslationController;->onTranslateStateChanged(I)V

    :goto_4b
    return-void

    :cond_4c
    add-int v0, p2, p3

    int-to-float v0, v0

    invoke-virtual {p0, p1, v0}, Lmiui/v5/widget/AbsTranslationController;->onTranslate(Landroid/view/View;F)V

    goto :goto_44

    :cond_53
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lmiui/v5/widget/AbsTranslationController;->onTranslateStateChanged(I)V

    goto :goto_4b

    :array_58
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
    .end array-data
.end method


# virtual methods
.method protected abstract computVelocity(Landroid/view/VelocityTracker;)I
.end method

.method protected fling(Landroid/view/View;IIIII)V
    .registers 13
    .parameter "view"
    .parameter "x"
    .parameter "y"
    .parameter "startX"
    .parameter "startY"
    .parameter "velocity"

    .prologue
    invoke-virtual {p1}, Landroid/view/View;->getY()F

    move-result v0

    float-to-int v2, v0

    .local v2, from:I
    invoke-virtual/range {p0 .. p6}, Lmiui/v5/widget/AbsTranslationController;->getAnchorPostion(Landroid/view/View;IIIII)I

    move-result v0

    sub-int v3, v0, v2

    .local v3, delta:I
    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move v4, p6

    invoke-direct/range {v0 .. v5}, Lmiui/v5/widget/AbsTranslationController;->fling(Landroid/view/View;IIIZ)V

    return-void
.end method

.method protected abstract getAnchorPostion(Landroid/view/View;IIIII)I
.end method

.method protected getDuration(II)I
    .registers 7
    .parameter "delta"
    .parameter "velocity"

    .prologue
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .local v0, s:I
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    add-int/lit16 v2, v2, 0x3e8

    add-int/lit8 v2, v2, -0x1

    div-int/lit16 v1, v2, 0x3e8

    .local v1, v:I
    if-lez v1, :cond_1a

    iget v2, p0, Lmiui/v5/widget/AbsTranslationController;->mMaxAnchorDuration:I

    mul-int/lit8 v3, v0, 0x2

    div-int/2addr v3, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    :goto_19
    return v2

    :cond_1a
    iget v2, p0, Lmiui/v5/widget/AbsTranslationController;->mMaxAnchorDuration:I

    goto :goto_19
.end method

.method protected abstract getValidMovePosition(Landroid/view/View;IIII)I
.end method

.method public isAnimationPlaying()Z
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/AbsTranslationController;->mAnimator:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public isMovable(Landroid/view/View;IIII)Z
    .registers 12
    .parameter "view"
    .parameter "x"
    .parameter "y"
    .parameter "startX"
    .parameter "startY"

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/AbsTranslationController;->mListener:Lmiui/v5/widget/MotionDetectStrategy;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lmiui/v5/widget/AbsTranslationController;->mListener:Lmiui/v5/widget/MotionDetectStrategy;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lmiui/v5/widget/MotionDetectStrategy;->isMovable(Landroid/view/View;IIII)Z

    move-result v0

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x1

    goto :goto_f
.end method

.method public moveImmediately(Landroid/view/View;II)Z
    .registers 5
    .parameter "view"
    .parameter "x"
    .parameter "y"

    .prologue
    const/4 v0, 0x0

    return v0
.end method

.method public onAnimationCancel(Landroid/animation/Animator;)V
    .registers 3
    .parameter "animation"

    .prologue
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lmiui/v5/widget/AbsTranslationController;->onTranslateStateChanged(I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lmiui/v5/widget/AbsTranslationController;->mAnimator:Landroid/animation/ValueAnimator;

    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .registers 3
    .parameter "animation"

    .prologue
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lmiui/v5/widget/AbsTranslationController;->onTranslateStateChanged(I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lmiui/v5/widget/AbsTranslationController;->mAnimator:Landroid/animation/ValueAnimator;

    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .registers 2
    .parameter "animation"

    .prologue
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .registers 2
    .parameter "animation"

    .prologue
    return-void
.end method

.method public onMove(Landroid/view/View;IIII)Z
    .registers 9
    .parameter "view"
    .parameter "x"
    .parameter "y"
    .parameter "startX"
    .parameter "startY"

    .prologue
    invoke-virtual/range {p0 .. p5}, Lmiui/v5/widget/AbsTranslationController;->isMovable(Landroid/view/View;IIII)Z

    move-result v0

    .local v0, movale:Z
    if-eqz v0, :cond_e

    invoke-virtual/range {p0 .. p5}, Lmiui/v5/widget/AbsTranslationController;->getValidMovePosition(Landroid/view/View;IIII)I

    move-result v1

    .local v1, p:I
    int-to-float v2, v1

    invoke-virtual {p0, p1, v2}, Lmiui/v5/widget/AbsTranslationController;->onTranslate(Landroid/view/View;F)V

    .end local v1           #p:I
    :cond_e
    return v0
.end method

.method public onMoveCancel(Landroid/view/View;II)V
    .registers 11
    .parameter "view"
    .parameter "x"
    .parameter "y"

    .prologue
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p2

    move v5, p3

    invoke-virtual/range {v0 .. v6}, Lmiui/v5/widget/AbsTranslationController;->fling(Landroid/view/View;IIIII)V

    return-void
.end method

.method public onMoveFinish(Landroid/view/View;IIIILandroid/view/VelocityTracker;)V
    .registers 22
    .parameter "view"
    .parameter "x"
    .parameter "y"
    .parameter "startX"
    .parameter "startY"
    .parameter "tracker"

    .prologue
    move-object/from16 v0, p6

    invoke-virtual {p0, v0}, Lmiui/v5/widget/AbsTranslationController;->computVelocity(Landroid/view/VelocityTracker;)I

    move-result v7

    .local v7, velocity:I
    invoke-static {v7}, Ljava/lang/Math;->abs(I)I

    move-result v1

    iget v2, p0, Lmiui/v5/widget/AbsTranslationController;->mMinimumVelocity:I

    if-le v1, v2, :cond_1d

    move-object v1, p0

    move-object/from16 v2, p1

    move/from16 v3, p2

    move/from16 v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    invoke-virtual/range {v1 .. v7}, Lmiui/v5/widget/AbsTranslationController;->fling(Landroid/view/View;IIIII)V

    :goto_1c
    return-void

    :cond_1d
    const/4 v14, 0x0

    move-object v8, p0

    move-object/from16 v9, p1

    move/from16 v10, p2

    move/from16 v11, p3

    move/from16 v12, p4

    move/from16 v13, p5

    invoke-virtual/range {v8 .. v14}, Lmiui/v5/widget/AbsTranslationController;->fling(Landroid/view/View;IIIII)V

    goto :goto_1c
.end method

.method public onMoveStart(Landroid/view/View;II)V
    .registers 5
    .parameter "view"
    .parameter "x"
    .parameter "y"

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/AbsTranslationController;->mAnimator:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lmiui/v5/widget/AbsTranslationController;->mAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmiui/v5/widget/AbsTranslationController;->mAnimator:Landroid/animation/ValueAnimator;

    :cond_c
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lmiui/v5/widget/AbsTranslationController;->onTranslateStateChanged(I)V

    return-void
.end method

.method protected onTranslate(Landroid/view/View;F)V
    .registers 4
    .parameter "v"
    .parameter "t"

    .prologue
    invoke-virtual {p0, p1, p2}, Lmiui/v5/widget/AbsTranslationController;->translate(Landroid/view/View;F)V

    iget-object v0, p0, Lmiui/v5/widget/AbsTranslationController;->mTranslateListener:Lmiui/v5/widget/AbsTranslationController$OnTranslateListener;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lmiui/v5/widget/AbsTranslationController;->mTranslateListener:Lmiui/v5/widget/AbsTranslationController$OnTranslateListener;

    invoke-interface {v0, p1, p2}, Lmiui/v5/widget/AbsTranslationController$OnTranslateListener;->onTranslate(Landroid/view/View;F)V

    :cond_c
    return-void
.end method

.method protected onTranslateStateChanged(I)V
    .registers 3
    .parameter "state"

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/AbsTranslationController;->mTranslateListener:Lmiui/v5/widget/AbsTranslationController$OnTranslateListener;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lmiui/v5/widget/AbsTranslationController;->mTranslateListener:Lmiui/v5/widget/AbsTranslationController$OnTranslateListener;

    invoke-interface {v0, p1}, Lmiui/v5/widget/AbsTranslationController$OnTranslateListener;->onTranslateStateChanged(I)V

    :cond_9
    return-void
.end method

.method public setTranslateListener(Lmiui/v5/widget/AbsTranslationController$OnTranslateListener;)V
    .registers 2
    .parameter "l"

    .prologue
    iput-object p1, p0, Lmiui/v5/widget/AbsTranslationController;->mTranslateListener:Lmiui/v5/widget/AbsTranslationController$OnTranslateListener;

    return-void
.end method

.method protected abstract translate(Landroid/view/View;F)V
.end method
