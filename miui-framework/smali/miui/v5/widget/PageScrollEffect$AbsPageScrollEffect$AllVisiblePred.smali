.class public Lmiui/v5/widget/PageScrollEffect$AbsPageScrollEffect$AllVisiblePred;
.super Ljava/lang/Object;
.source "PageScrollEffect.java"

# interfaces
.implements Lcom/android/internal/util/Predicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/v5/widget/PageScrollEffect$AbsPageScrollEffect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "AllVisiblePred"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/internal/util/Predicate",
        "<",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# instance fields
.field private final mParentVisibleRect:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/graphics/Rect;)V
    .registers 2
    .parameter "rect"

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmiui/v5/widget/PageScrollEffect$AbsPageScrollEffect$AllVisiblePred;->mParentVisibleRect:Landroid/graphics/Rect;

    return-void
.end method


# virtual methods
.method public apply(Landroid/view/View;)Z
    .registers 3
    .parameter "t"

    .prologue
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_10

    iget-object v0, p0, Lmiui/v5/widget/PageScrollEffect$AbsPageScrollEffect$AllVisiblePred;->mParentVisibleRect:Landroid/graphics/Rect;

    invoke-static {p1, v0}, Lmiui/v5/widget/Views;->isIntersectWithRect(Landroid/view/View;Landroid/graphics/Rect;)Z

    move-result v0

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Z
    .registers 3
    .parameter "x0"

    .prologue
    check-cast p1, Landroid/view/View;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/v5/widget/PageScrollEffect$AbsPageScrollEffect$AllVisiblePred;->apply(Landroid/view/View;)Z

    move-result v0

    return v0
.end method
