.class Lmiui/v5/widget/TabController$TabImpl;
.super Landroid/app/ActionBar$Tab;
.source "TabController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/v5/widget/TabController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TabImpl"
.end annotation


# instance fields
.field private mCallback:Landroid/app/ActionBar$TabListener;

.field private mContentDesc:Ljava/lang/CharSequence;

.field private mCustomView:Landroid/view/View;

.field private mIcon:Landroid/graphics/drawable/Drawable;

.field private mTag:Ljava/lang/Object;

.field private mText:Ljava/lang/CharSequence;

.field final synthetic this$0:Lmiui/v5/widget/TabController;


# direct methods
.method public constructor <init>(Lmiui/v5/widget/TabController;Landroid/app/ActionBar$TabListener;)V
    .registers 3
    .parameter
    .parameter "callback"

    .prologue
    iput-object p1, p0, Lmiui/v5/widget/TabController$TabImpl;->this$0:Lmiui/v5/widget/TabController;

    invoke-direct {p0}, Landroid/app/ActionBar$Tab;-><init>()V

    iput-object p2, p0, Lmiui/v5/widget/TabController$TabImpl;->mCallback:Landroid/app/ActionBar$TabListener;

    return-void
.end method


# virtual methods
.method public getCallback()Landroid/app/ActionBar$TabListener;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/TabController$TabImpl;->mCallback:Landroid/app/ActionBar$TabListener;

    return-object v0
.end method

.method public getContentDescription()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/TabController$TabImpl;->mContentDesc:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getCustomView()Landroid/view/View;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/TabController$TabImpl;->mCustomView:Landroid/view/View;

    return-object v0
.end method

.method public getIcon()Landroid/graphics/drawable/Drawable;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/TabController$TabImpl;->mIcon:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getPosition()I
    .registers 2

    .prologue
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getTag()Ljava/lang/Object;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/TabController$TabImpl;->mTag:Ljava/lang/Object;

    return-object v0
.end method

.method public getText()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/TabController$TabImpl;->mText:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public select()V
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/TabController$TabImpl;->this$0:Lmiui/v5/widget/TabController;

    invoke-virtual {v0, p0}, Lmiui/v5/widget/TabController;->selectTab(Landroid/app/ActionBar$Tab;)Z

    return-void
.end method

.method public setContentDescription(I)Landroid/app/ActionBar$Tab;
    .registers 3
    .parameter "resId"

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/TabController$TabImpl;->this$0:Lmiui/v5/widget/TabController;

    iget-object v0, v0, Lmiui/v5/widget/TabController;->mTabContainer:Lmiui/v5/widget/TabContainerLayout;

    invoke-virtual {v0}, Lmiui/v5/widget/TabContainerLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmiui/v5/widget/TabController$TabImpl;->setContentDescription(Ljava/lang/CharSequence;)Landroid/app/ActionBar$Tab;

    move-result-object v0

    return-object v0
.end method

.method public setContentDescription(Ljava/lang/CharSequence;)Landroid/app/ActionBar$Tab;
    .registers 2
    .parameter "contentDesc"

    .prologue
    iput-object p1, p0, Lmiui/v5/widget/TabController$TabImpl;->mContentDesc:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public setCustomView(I)Landroid/app/ActionBar$Tab;
    .registers 4
    .parameter "layoutResId"

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/TabController$TabImpl;->this$0:Lmiui/v5/widget/TabController;

    iget-object v0, v0, Lmiui/v5/widget/TabController;->mTabContainer:Lmiui/v5/widget/TabContainerLayout;

    invoke-virtual {v0}, Lmiui/v5/widget/TabContainerLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmiui/v5/widget/TabController$TabImpl;->setCustomView(Landroid/view/View;)Landroid/app/ActionBar$Tab;

    move-result-object v0

    return-object v0
.end method

.method public setCustomView(Landroid/view/View;)Landroid/app/ActionBar$Tab;
    .registers 2
    .parameter "view"

    .prologue
    iput-object p1, p0, Lmiui/v5/widget/TabController$TabImpl;->mCustomView:Landroid/view/View;

    return-object p0
.end method

.method public setIcon(I)Landroid/app/ActionBar$Tab;
    .registers 3
    .parameter "resId"

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/TabController$TabImpl;->this$0:Lmiui/v5/widget/TabController;

    iget-object v0, v0, Lmiui/v5/widget/TabController;->mTabContainer:Lmiui/v5/widget/TabContainerLayout;

    invoke-virtual {v0}, Lmiui/v5/widget/TabContainerLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmiui/v5/widget/TabController$TabImpl;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/app/ActionBar$Tab;

    move-result-object v0

    return-object v0
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)Landroid/app/ActionBar$Tab;
    .registers 2
    .parameter "icon"

    .prologue
    iput-object p1, p0, Lmiui/v5/widget/TabController$TabImpl;->mIcon:Landroid/graphics/drawable/Drawable;

    return-object p0
.end method

.method public setTabListener(Landroid/app/ActionBar$TabListener;)Landroid/app/ActionBar$Tab;
    .registers 2
    .parameter "listener"

    .prologue
    iput-object p1, p0, Lmiui/v5/widget/TabController$TabImpl;->mCallback:Landroid/app/ActionBar$TabListener;

    return-object p0
.end method

.method public setTag(Ljava/lang/Object;)Landroid/app/ActionBar$Tab;
    .registers 2
    .parameter "tag"

    .prologue
    iput-object p1, p0, Lmiui/v5/widget/TabController$TabImpl;->mTag:Ljava/lang/Object;

    return-object p0
.end method

.method public setText(I)Landroid/app/ActionBar$Tab;
    .registers 3
    .parameter "resId"

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/TabController$TabImpl;->this$0:Lmiui/v5/widget/TabController;

    iget-object v0, v0, Lmiui/v5/widget/TabController;->mTabContainer:Lmiui/v5/widget/TabContainerLayout;

    invoke-virtual {v0}, Lmiui/v5/widget/TabContainerLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmiui/v5/widget/TabController$TabImpl;->setText(Ljava/lang/CharSequence;)Landroid/app/ActionBar$Tab;

    move-result-object v0

    return-object v0
.end method

.method public setText(Ljava/lang/CharSequence;)Landroid/app/ActionBar$Tab;
    .registers 2
    .parameter "text"

    .prologue
    iput-object p1, p0, Lmiui/v5/widget/TabController$TabImpl;->mText:Ljava/lang/CharSequence;

    return-object p0
.end method
