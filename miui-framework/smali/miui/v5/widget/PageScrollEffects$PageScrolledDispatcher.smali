.class Lmiui/v5/widget/PageScrollEffects$PageScrolledDispatcher;
.super Ljava/lang/Object;
.source "PageScrollEffects.java"

# interfaces
.implements Lmiui/v5/widget/PageChangeAdapter$OnPageScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/v5/widget/PageScrollEffects;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PageScrolledDispatcher"
.end annotation


# instance fields
.field private final mFactory:Lmiui/v5/util/Factory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lmiui/v5/util/Factory",
            "<",
            "Lmiui/v5/widget/PageScrollEffect;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lmiui/v5/util/Factory;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiui/v5/util/Factory",
            "<",
            "Lmiui/v5/widget/PageScrollEffect;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, factory:Lmiui/v5/util/Factory;,"Lmiui/v5/util/Factory<Lmiui/v5/widget/PageScrollEffect;Ljava/lang/Integer;>;"
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmiui/v5/widget/PageScrollEffects$PageScrolledDispatcher;->mFactory:Lmiui/v5/util/Factory;

    return-void
.end method

.method private onScrolled(Landroid/support/v4/view/ViewPager;IFZ)V
    .registers 8
    .parameter "pager"
    .parameter "newPagePosition"
    .parameter "percent"
    .parameter "isRight"

    .prologue
    iget-object v1, p0, Lmiui/v5/widget/PageScrollEffects$PageScrolledDispatcher;->mFactory:Lmiui/v5/util/Factory;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Lmiui/v5/util/Factory;->create(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/v5/widget/PageScrollEffect;

    .local v0, listener:Lmiui/v5/widget/PageScrollEffect;
    if-eqz v0, :cond_11

    invoke-interface {v0, p3, p4}, Lmiui/v5/widget/PageScrollEffect;->scroll(FZ)V

    :cond_11
    return-void
.end method


# virtual methods
.method public onReset(Landroid/support/v4/view/ViewPager;II)V
    .registers 6
    .parameter "pager"
    .parameter "from"
    .parameter "to"

    .prologue
    const/high16 v0, 0x3f80

    const/4 v1, 0x1

    invoke-direct {p0, p1, p3, v0, v1}, Lmiui/v5/widget/PageScrollEffects$PageScrolledDispatcher;->onScrolled(Landroid/support/v4/view/ViewPager;IFZ)V

    return-void
.end method

.method public onScroll(Landroid/support/v4/view/ViewPager;IIF)V
    .registers 6
    .parameter "pager"
    .parameter "from"
    .parameter "to"
    .parameter "percent"

    .prologue
    if-ge p2, p3, :cond_7

    const/4 v0, 0x1

    :goto_3
    invoke-direct {p0, p1, p3, p4, v0}, Lmiui/v5/widget/PageScrollEffects$PageScrolledDispatcher;->onScrolled(Landroid/support/v4/view/ViewPager;IFZ)V

    return-void

    :cond_7
    const/4 v0, 0x0

    goto :goto_3
.end method
