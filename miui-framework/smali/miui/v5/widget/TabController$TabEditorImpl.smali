.class Lmiui/v5/widget/TabController$TabEditorImpl;
.super Ljava/lang/Object;
.source "TabController.java"

# interfaces
.implements Lmiui/v5/widget/TabController$TabEditor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/v5/widget/TabController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TabEditorImpl"
.end annotation


# instance fields
.field private final mCommands:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lmiui/v5/widget/TabController$TabEditCommand;",
            ">;"
        }
    .end annotation
.end field

.field private final mTabControllerRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lmiui/v5/widget/TabController;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lmiui/v5/widget/TabController;)V
    .registers 3
    .parameter "controller"

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lmiui/v5/widget/TabController$TabEditorImpl;->mCommands:Ljava/util/List;

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lmiui/v5/widget/TabController$TabEditorImpl;->mTabControllerRef:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public add(Lmiui/v5/widget/TabController$TabEditCommand;)Lmiui/v5/widget/TabController$TabEditor;
    .registers 3
    .parameter "command"

    .prologue
    if-eqz p1, :cond_7

    iget-object v0, p0, Lmiui/v5/widget/TabController$TabEditorImpl;->mCommands:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_7
    const/4 v0, 0x0

    return-object v0
.end method

.method public addTab(Landroid/app/ActionBar$Tab;Landroid/app/Fragment;)Lmiui/v5/widget/TabController$TabEditor;
    .registers 4
    .parameter "tab"
    .parameter "fragment"

    .prologue
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lmiui/v5/widget/TabController$TabEditorImpl;->addTab(Landroid/app/ActionBar$Tab;Landroid/app/Fragment;Ljava/lang/String;)Lmiui/v5/widget/TabController$TabEditor;

    move-result-object v0

    return-object v0
.end method

.method public addTab(Landroid/app/ActionBar$Tab;Landroid/app/Fragment;Ljava/lang/String;)Lmiui/v5/widget/TabController$TabEditor;
    .registers 7
    .parameter "tab"
    .parameter "fragment"
    .parameter "fragmentTag"

    .prologue
    if-nez p1, :cond_25

    new-instance v0, Ljava/lang/NullPointerException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "tab and fragment cannot be null! tab="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " fragment="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_25
    new-instance v0, Lmiui/v5/widget/TabController$TabEditAdd;

    invoke-direct {v0, p1, p2, p3}, Lmiui/v5/widget/TabController$TabEditAdd;-><init>(Landroid/app/ActionBar$Tab;Landroid/app/Fragment;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lmiui/v5/widget/TabController$TabEditorImpl;->add(Lmiui/v5/widget/TabController$TabEditCommand;)Lmiui/v5/widget/TabController$TabEditor;

    return-object p0
.end method

.method public commit()Z
    .registers 15

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    iget-object v13, p0, Lmiui/v5/widget/TabController$TabEditorImpl;->mTabControllerRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v13}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmiui/v5/widget/TabController;

    .local v3, controller:Lmiui/v5/widget/TabController;
    if-nez v3, :cond_d

    :goto_c
    return v11

    :cond_d
    iget-object v13, p0, Lmiui/v5/widget/TabController$TabEditorImpl;->mCommands:Ljava/util/List;

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v4

    .local v4, count:I
    if-gtz v4, :cond_17

    move v11, v12

    goto :goto_c

    :cond_17
    iget-object v8, v3, Lmiui/v5/widget/TabController;->mViewPager:Lmiui/v5/widget/CooperativeViewPager;

    .local v8, pager:Landroid/support/v4/view/ViewPager;
    iget-object v0, v3, Lmiui/v5/widget/TabController;->mAdapter:Lmiui/v5/widget/TabController$DynamicPagerAdapter;

    .local v0, adapter:Lmiui/v5/widget/TabController$DynamicPagerAdapter;
    invoke-virtual {v0}, Lmiui/v5/widget/TabController$DynamicPagerAdapter;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v7

    .local v7, manager:Landroid/app/FragmentManager;
    iget-object v2, v3, Lmiui/v5/widget/TabController;->mTabContainer:Lmiui/v5/widget/TabContainerLayout;

    .local v2, containter:Lmiui/v5/widget/TabContainerLayout;
    invoke-virtual {v7}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v10

    .local v10, transaction:Landroid/app/FragmentTransaction;
    iget-object v13, p0, Lmiui/v5/widget/TabController$TabEditorImpl;->mCommands:Ljava/util/List;

    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, i$:Ljava/util/Iterator;
    :goto_2b
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_3b

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/v5/widget/TabController$TabEditCommand;

    .local v1, command:Lmiui/v5/widget/TabController$TabEditCommand;
    invoke-interface {v1, v2, v8, v0, v10}, Lmiui/v5/widget/TabController$TabEditCommand;->execute(Lmiui/v5/widget/TabContainerLayout;Landroid/support/v4/view/ViewPager;Lmiui/v5/widget/TabController$DynamicPagerAdapter;Landroid/app/FragmentTransaction;)Z

    goto :goto_2b

    .end local v1           #command:Lmiui/v5/widget/TabController$TabEditCommand;
    :cond_3b
    invoke-virtual {v10}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    invoke-virtual {v7}, Landroid/app/FragmentManager;->executePendingTransactions()Z

    invoke-virtual {v0}, Lmiui/v5/widget/TabController$DynamicPagerAdapter;->notifyDataSetChanged()V

    const/4 v9, 0x0

    .local v9, selectedChanged:Z
    invoke-virtual {v2}, Lmiui/v5/widget/TabContainerLayout;->getSelectedTab()Landroid/app/ActionBar$Tab;

    move-result-object v13

    if-nez v13, :cond_4f

    invoke-virtual {v2, v11}, Lmiui/v5/widget/TabContainerLayout;->selectTabAt(I)V

    const/4 v9, 0x1

    :cond_4f
    iget-object v6, v3, Lmiui/v5/widget/TabController;->mAdapterChangedListener:Lmiui/v5/widget/TabController$PagerAdapterChangedListener;

    .local v6, l:Lmiui/v5/widget/TabController$PagerAdapterChangedListener;
    if-eqz v6, :cond_56

    invoke-interface {v6, v8, v9}, Lmiui/v5/widget/TabController$PagerAdapterChangedListener;->onAdapterChanged(Landroid/support/v4/view/ViewPager;Z)V

    :cond_56
    iget-object v11, p0, Lmiui/v5/widget/TabController$TabEditorImpl;->mCommands:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->clear()V

    move v11, v12

    goto :goto_c
.end method

.method public removeTab(Landroid/app/ActionBar$Tab;)Lmiui/v5/widget/TabController$TabEditor;
    .registers 5
    .parameter "tab"

    .prologue
    if-nez p1, :cond_1b

    new-instance v0, Ljava/lang/NullPointerException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "tab and fragment cannot be null! tab="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1b
    new-instance v0, Lmiui/v5/widget/TabController$TabEditRemove;

    invoke-direct {v0, p1}, Lmiui/v5/widget/TabController$TabEditRemove;-><init>(Landroid/app/ActionBar$Tab;)V

    invoke-virtual {p0, v0}, Lmiui/v5/widget/TabController$TabEditorImpl;->add(Lmiui/v5/widget/TabController$TabEditCommand;)Lmiui/v5/widget/TabController$TabEditor;

    return-object p0
.end method
