.class public Lmiui/v5/widget/PageChangeAdapter;
.super Ljava/lang/Object;
.source "PageChangeAdapter.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/v5/widget/PageChangeAdapter$OnPageScrollListener;
    }
.end annotation


# instance fields
.field private mAppearingPosition:I

.field private mChangeStartPosition:I

.field private mScrolledListener:Lmiui/v5/widget/PageChangeAdapter$OnPageScrollListener;

.field private final mViewPager:Landroid/support/v4/view/ViewPager;


# direct methods
.method public constructor <init>(Landroid/support/v4/view/ViewPager;)V
    .registers 3
    .parameter "pager"

    .prologue
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lmiui/v5/widget/PageChangeAdapter;-><init>(Landroid/support/v4/view/ViewPager;Lmiui/v5/widget/PageChangeAdapter$OnPageScrollListener;)V

    return-void
.end method

.method public constructor <init>(Landroid/support/v4/view/ViewPager;Lmiui/v5/widget/PageChangeAdapter$OnPageScrollListener;)V
    .registers 4
    .parameter "pager"
    .parameter "scrolledListener"

    .prologue
    const/4 v0, -0x1

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lmiui/v5/widget/PageChangeAdapter;->mChangeStartPosition:I

    iput v0, p0, Lmiui/v5/widget/PageChangeAdapter;->mAppearingPosition:I

    iput-object p1, p0, Lmiui/v5/widget/PageChangeAdapter;->mViewPager:Landroid/support/v4/view/ViewPager;

    iput-object p2, p0, Lmiui/v5/widget/PageChangeAdapter;->mScrolledListener:Lmiui/v5/widget/PageChangeAdapter$OnPageScrollListener;

    return-void
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .registers 8
    .parameter "state"

    .prologue
    const/4 v5, -0x1

    const/4 v1, 0x1

    if-ne p1, v1, :cond_26

    iget-object v1, p0, Lmiui/v5/widget/PageChangeAdapter;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    .local v0, position:I
    iget v1, p0, Lmiui/v5/widget/PageChangeAdapter;->mChangeStartPosition:I

    if-eq v1, v0, :cond_25

    iget v1, p0, Lmiui/v5/widget/PageChangeAdapter;->mAppearingPosition:I

    if-eq v1, v5, :cond_23

    iget-object v1, p0, Lmiui/v5/widget/PageChangeAdapter;->mScrolledListener:Lmiui/v5/widget/PageChangeAdapter$OnPageScrollListener;

    if-eqz v1, :cond_21

    iget-object v1, p0, Lmiui/v5/widget/PageChangeAdapter;->mScrolledListener:Lmiui/v5/widget/PageChangeAdapter$OnPageScrollListener;

    iget-object v2, p0, Lmiui/v5/widget/PageChangeAdapter;->mViewPager:Landroid/support/v4/view/ViewPager;

    iget v3, p0, Lmiui/v5/widget/PageChangeAdapter;->mChangeStartPosition:I

    iget v4, p0, Lmiui/v5/widget/PageChangeAdapter;->mAppearingPosition:I

    invoke-interface {v1, v2, v3, v4}, Lmiui/v5/widget/PageChangeAdapter$OnPageScrollListener;->onReset(Landroid/support/v4/view/ViewPager;II)V

    :cond_21
    iput v5, p0, Lmiui/v5/widget/PageChangeAdapter;->mAppearingPosition:I

    :cond_23
    iput v0, p0, Lmiui/v5/widget/PageChangeAdapter;->mChangeStartPosition:I

    .end local v0           #position:I
    :cond_25
    :goto_25
    return-void

    :cond_26
    if-nez p1, :cond_25

    iget-object v1, p0, Lmiui/v5/widget/PageChangeAdapter;->mScrolledListener:Lmiui/v5/widget/PageChangeAdapter$OnPageScrollListener;

    if-eqz v1, :cond_3b

    iget-object v1, p0, Lmiui/v5/widget/PageChangeAdapter;->mScrolledListener:Lmiui/v5/widget/PageChangeAdapter$OnPageScrollListener;

    iget-object v2, p0, Lmiui/v5/widget/PageChangeAdapter;->mViewPager:Landroid/support/v4/view/ViewPager;

    iget v3, p0, Lmiui/v5/widget/PageChangeAdapter;->mChangeStartPosition:I

    iget-object v4, p0, Lmiui/v5/widget/PageChangeAdapter;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v4}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v4

    invoke-interface {v1, v2, v3, v4}, Lmiui/v5/widget/PageChangeAdapter$OnPageScrollListener;->onReset(Landroid/support/v4/view/ViewPager;II)V

    :cond_3b
    iput v5, p0, Lmiui/v5/widget/PageChangeAdapter;->mChangeStartPosition:I

    iput v5, p0, Lmiui/v5/widget/PageChangeAdapter;->mAppearingPosition:I

    goto :goto_25
.end method

.method public onPageScrolled(IFI)V
    .registers 9
    .parameter "position"
    .parameter "positionOffset"
    .parameter "positionOffsetPixels"

    .prologue
    iget-object v3, p0, Lmiui/v5/widget/PageChangeAdapter;->mScrolledListener:Lmiui/v5/widget/PageChangeAdapter$OnPageScrollListener;

    if-eqz v3, :cond_27

    iget v0, p0, Lmiui/v5/widget/PageChangeAdapter;->mChangeStartPosition:I

    .local v0, from:I
    move v2, v0

    .local v2, to:I
    const/high16 v1, 0x3f80

    .local v1, percent:F
    if-ne v0, p1, :cond_28

    iget-object v3, p0, Lmiui/v5/widget/PageChangeAdapter;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_28

    add-int/lit8 v2, v0, 0x1

    move v1, p2

    :cond_1c
    :goto_1c
    if-eq v0, v2, :cond_27

    iget-object v3, p0, Lmiui/v5/widget/PageChangeAdapter;->mScrolledListener:Lmiui/v5/widget/PageChangeAdapter$OnPageScrollListener;

    iget-object v4, p0, Lmiui/v5/widget/PageChangeAdapter;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-interface {v3, v4, v0, v2, v1}, Lmiui/v5/widget/PageChangeAdapter$OnPageScrollListener;->onScroll(Landroid/support/v4/view/ViewPager;IIF)V

    iput v2, p0, Lmiui/v5/widget/PageChangeAdapter;->mAppearingPosition:I

    .end local v0           #from:I
    .end local v1           #percent:F
    .end local v2           #to:I
    :cond_27
    return-void

    .restart local v0       #from:I
    .restart local v1       #percent:F
    .restart local v2       #to:I
    :cond_28
    add-int/lit8 v3, p1, 0x1

    if-ne v0, v3, :cond_1c

    if-lez v0, :cond_1c

    add-int/lit8 v2, v0, -0x1

    const/high16 v3, 0x3f80

    sub-float v1, v3, p2

    goto :goto_1c
.end method

.method public onPageSelected(I)V
    .registers 2
    .parameter "position"

    .prologue
    return-void
.end method

.method public setScrolledListener(Lmiui/v5/widget/PageChangeAdapter$OnPageScrollListener;)V
    .registers 2
    .parameter "scrolledListener"

    .prologue
    iput-object p1, p0, Lmiui/v5/widget/PageChangeAdapter;->mScrolledListener:Lmiui/v5/widget/PageChangeAdapter$OnPageScrollListener;

    return-void
.end method
