.class public Lmiui/v5/widget/CooperativeViewPager;
.super Landroid/support/v4/view/ViewPager;
.source "CooperativeViewPager.java"


# instance fields
.field private mDragEnabled:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    invoke-direct {p0, p1}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiui/v5/widget/CooperativeViewPager;->mDragEnabled:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    .prologue
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiui/v5/widget/CooperativeViewPager;->mDragEnabled:Z

    return-void
.end method


# virtual methods
.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "ev"

    .prologue
    iget-boolean v0, p0, Lmiui/v5/widget/CooperativeViewPager;->mDragEnabled:Z

    if-nez v0, :cond_6

    const/4 v0, 0x0

    :goto_5
    return v0

    :cond_6
    invoke-super {p0, p1}, Landroid/support/v4/view/ViewPager;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_5
.end method

.method public setDraggable(Z)V
    .registers 2
    .parameter "enabled"

    .prologue
    iput-boolean p1, p0, Lmiui/v5/widget/CooperativeViewPager;->mDragEnabled:Z

    return-void
.end method
