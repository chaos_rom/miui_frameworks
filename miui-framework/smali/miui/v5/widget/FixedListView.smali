.class public Lmiui/v5/widget/FixedListView;
.super Landroid/widget/ListView;
.source "FixedListView.java"


# instance fields
.field private mExtraHeight:I

.field private mMaxItemCount:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lmiui/v5/widget/FixedListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    .prologue
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lmiui/v5/widget/FixedListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 7
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v1, -0x1

    iput v1, p0, Lmiui/v5/widget/FixedListView;->mMaxItemCount:I

    iput v2, p0, Lmiui/v5/widget/FixedListView;->mExtraHeight:I

    sget-object v1, Lmiui/R$styleable;->FixedListView:[I

    invoke-virtual {p1, p2, v1, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .local v0, a:Landroid/content/res/TypedArray;
    iget v1, p0, Lmiui/v5/widget/FixedListView;->mMaxItemCount:I

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lmiui/v5/widget/FixedListView;->mMaxItemCount:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .registers 13
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    .prologue
    const/high16 v9, -0x8000

    const/4 v8, 0x0

    iget v7, p0, Lmiui/v5/widget/FixedListView;->mMaxItemCount:I

    if-gez v7, :cond_12

    const v7, 0x1fffffff

    invoke-static {v7, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    invoke-super {p0, p1, p2}, Landroid/widget/ListView;->onMeasure(II)V

    :goto_11
    return-void

    :cond_12
    invoke-virtual {p0}, Lmiui/v5/widget/FixedListView;->getCount()I

    move-result v7

    if-lez v7, :cond_60

    invoke-virtual {p0}, Lmiui/v5/widget/FixedListView;->getListPaddingTop()I

    move-result v6

    .local v6, paddingTop:I
    invoke-virtual {p0}, Lmiui/v5/widget/FixedListView;->getListPaddingBottom()I

    move-result v5

    .local v5, paddingBottom:I
    invoke-virtual {p0}, Lmiui/v5/widget/FixedListView;->getVerticalFadingEdgeLength()I

    move-result v7

    mul-int/lit8 v3, v7, 0x2

    .local v3, fadingLength:I
    add-int v7, v6, v5

    add-int v2, v7, v3

    .local v2, extraHeight:I
    invoke-virtual {p0}, Lmiui/v5/widget/FixedListView;->getDividerHeight()I

    move-result v1

    .local v1, dividerHeight:I
    invoke-virtual {p0}, Lmiui/v5/widget/FixedListView;->getChildCount()I

    move-result v7

    if-lez v7, :cond_52

    invoke-virtual {p0, v8}, Lmiui/v5/widget/FixedListView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    .local v0, childHeight:I
    :goto_3c
    iget v7, p0, Lmiui/v5/widget/FixedListView;->mMaxItemCount:I

    mul-int/2addr v7, v0

    add-int/2addr v7, v2

    iget v8, p0, Lmiui/v5/widget/FixedListView;->mMaxItemCount:I

    add-int/lit8 v8, v8, -0x1

    mul-int/2addr v8, v1

    add-int v4, v7, v8

    .local v4, h:I
    iget v7, p0, Lmiui/v5/widget/FixedListView;->mExtraHeight:I

    add-int/2addr v7, v4

    invoke-static {v7, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    invoke-super {p0, p1, p2}, Landroid/widget/ListView;->onMeasure(II)V

    goto :goto_11

    .end local v0           #childHeight:I
    .end local v4           #h:I
    :cond_52
    invoke-static {v8, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-super {p0, p1, v7}, Landroid/widget/ListView;->onMeasure(II)V

    invoke-virtual {p0}, Lmiui/v5/widget/FixedListView;->getMeasuredHeight()I

    move-result v7

    sub-int v0, v7, v2

    .restart local v0       #childHeight:I
    goto :goto_3c

    .end local v0           #childHeight:I
    .end local v1           #dividerHeight:I
    .end local v2           #extraHeight:I
    .end local v3           #fadingLength:I
    .end local v5           #paddingBottom:I
    .end local v6           #paddingTop:I
    :cond_60
    invoke-super {p0, p1, p2}, Landroid/widget/ListView;->onMeasure(II)V

    goto :goto_11
.end method

.method public setExtraHeight(I)V
    .registers 2
    .parameter "height"

    .prologue
    iput p1, p0, Lmiui/v5/widget/FixedListView;->mExtraHeight:I

    return-void
.end method

.method public setMaxItemCount(I)V
    .registers 3
    .parameter "maxItemCount"

    .prologue
    iget v0, p0, Lmiui/v5/widget/FixedListView;->mMaxItemCount:I

    if-eq p1, v0, :cond_9

    iput p1, p0, Lmiui/v5/widget/FixedListView;->mMaxItemCount:I

    invoke-virtual {p0}, Lmiui/v5/widget/FixedListView;->requestLayout()V

    :cond_9
    return-void
.end method
