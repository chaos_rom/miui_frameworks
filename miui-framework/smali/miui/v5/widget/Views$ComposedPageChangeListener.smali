.class public Lmiui/v5/widget/Views$ComposedPageChangeListener;
.super Ljava/lang/Object;
.source "Views.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/v5/widget/Views;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ComposedPageChangeListener"
.end annotation


# instance fields
.field final mListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/support/v4/view/ViewPager$OnPageChangeListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lmiui/v5/widget/Views$ComposedPageChangeListener;->mListeners:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public add(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V
    .registers 3
    .parameter "l"

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/Views$ComposedPageChangeListener;->mListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public onPageScrollStateChanged(I)V
    .registers 5
    .parameter "state"

    .prologue
    iget-object v2, p0, Lmiui/v5/widget/Views$ComposedPageChangeListener;->mListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_16

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    .local v1, l:Landroid/support/v4/view/ViewPager$OnPageChangeListener;
    invoke-interface {v1, p1}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageScrollStateChanged(I)V

    goto :goto_6

    .end local v1           #l:Landroid/support/v4/view/ViewPager$OnPageChangeListener;
    :cond_16
    return-void
.end method

.method public onPageScrolled(IFI)V
    .registers 7
    .parameter "position"
    .parameter "positionOffset"
    .parameter "positionOffsetPixels"

    .prologue
    iget-object v2, p0, Lmiui/v5/widget/Views$ComposedPageChangeListener;->mListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_16

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    .local v1, l:Landroid/support/v4/view/ViewPager$OnPageChangeListener;
    invoke-interface {v1, p1, p2, p3}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageScrolled(IFI)V

    goto :goto_6

    .end local v1           #l:Landroid/support/v4/view/ViewPager$OnPageChangeListener;
    :cond_16
    return-void
.end method

.method public onPageSelected(I)V
    .registers 5
    .parameter "position"

    .prologue
    iget-object v2, p0, Lmiui/v5/widget/Views$ComposedPageChangeListener;->mListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_16

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    .local v1, l:Landroid/support/v4/view/ViewPager$OnPageChangeListener;
    invoke-interface {v1, p1}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageSelected(I)V

    goto :goto_6

    .end local v1           #l:Landroid/support/v4/view/ViewPager$OnPageChangeListener;
    :cond_16
    return-void
.end method

.method public remove(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V
    .registers 3
    .parameter "l"

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/Views$ComposedPageChangeListener;->mListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public reset()V
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/Views$ComposedPageChangeListener;->mListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method
