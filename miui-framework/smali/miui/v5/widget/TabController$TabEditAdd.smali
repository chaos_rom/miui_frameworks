.class Lmiui/v5/widget/TabController$TabEditAdd;
.super Ljava/lang/Object;
.source "TabController.java"

# interfaces
.implements Lmiui/v5/widget/TabController$TabEditCommand;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/v5/widget/TabController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TabEditAdd"
.end annotation


# instance fields
.field private final mFragment:Landroid/app/Fragment;

.field private final mTab:Landroid/app/ActionBar$Tab;

.field private final mTag:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/ActionBar$Tab;Landroid/app/Fragment;Ljava/lang/String;)V
    .registers 4
    .parameter "tab"
    .parameter "fragment"
    .parameter "tag"

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmiui/v5/widget/TabController$TabEditAdd;->mTab:Landroid/app/ActionBar$Tab;

    iput-object p2, p0, Lmiui/v5/widget/TabController$TabEditAdd;->mFragment:Landroid/app/Fragment;

    iput-object p3, p0, Lmiui/v5/widget/TabController$TabEditAdd;->mTag:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public execute(Lmiui/v5/widget/TabContainerLayout;Landroid/support/v4/view/ViewPager;Lmiui/v5/widget/TabController$DynamicPagerAdapter;Landroid/app/FragmentTransaction;)Z
    .registers 8
    .parameter "container"
    .parameter "pager"
    .parameter "adapter"
    .parameter "transaction"

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/TabController$TabEditAdd;->mTab:Landroid/app/ActionBar$Tab;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lmiui/v5/widget/TabContainerLayout;->addTab(Landroid/app/ActionBar$Tab;Z)V

    iget-object v0, p0, Lmiui/v5/widget/TabController$TabEditAdd;->mFragment:Landroid/app/Fragment;

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lmiui/v5/widget/TabController$TabEditAdd;->mFragment:Landroid/app/Fragment;

    invoke-virtual {p3, v0}, Lmiui/v5/widget/TabController$DynamicPagerAdapter;->addFragment(Landroid/app/Fragment;)V

    invoke-virtual {p2}, Landroid/support/v4/view/ViewPager;->getId()I

    move-result v0

    iget-object v1, p0, Lmiui/v5/widget/TabController$TabEditAdd;->mFragment:Landroid/app/Fragment;

    iget-object v2, p0, Lmiui/v5/widget/TabController$TabEditAdd;->mTag:Ljava/lang/String;

    invoke-virtual {p4, v0, v1, v2}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    :cond_1a
    const/4 v0, 0x1

    return v0
.end method
