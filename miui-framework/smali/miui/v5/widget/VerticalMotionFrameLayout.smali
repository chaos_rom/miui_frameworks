.class public Lmiui/v5/widget/VerticalMotionFrameLayout;
.super Landroid/widget/FrameLayout;
.source "VerticalMotionFrameLayout.java"


# instance fields
.field private final mMotionDetector:Lmiui/v5/widget/VerticalMotionDetector;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lmiui/v5/widget/VerticalMotionFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    .prologue
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lmiui/v5/widget/VerticalMotionFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Lmiui/v5/widget/VerticalMotionDetector;

    invoke-direct {v0, p0}, Lmiui/v5/widget/VerticalMotionDetector;-><init>(Landroid/view/ViewGroup;)V

    iput-object v0, p0, Lmiui/v5/widget/VerticalMotionFrameLayout;->mMotionDetector:Lmiui/v5/widget/VerticalMotionDetector;

    return-void
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 5
    .parameter "ev"

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/VerticalMotionFrameLayout;->mMotionDetector:Lmiui/v5/widget/VerticalMotionDetector;

    invoke-virtual {v0}, Lmiui/v5/widget/VerticalMotionDetector;->isBeingDragged()Z

    move-result v0

    if-eqz v0, :cond_2e

    iget-object v0, p0, Lmiui/v5/widget/VerticalMotionFrameLayout;->mMotionDetector:Lmiui/v5/widget/VerticalMotionDetector;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Lmiui/v5/widget/VerticalMotionDetector;->isMovable(II)Z

    move-result v0

    if-nez v0, :cond_2e

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2e

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->setAction(I)V

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->setAction(I)V

    :cond_2e
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "ev"

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/VerticalMotionFrameLayout;->mMotionDetector:Lmiui/v5/widget/VerticalMotionDetector;

    invoke-virtual {v0, p1}, Lmiui/v5/widget/VerticalMotionDetector;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "ev"

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/VerticalMotionFrameLayout;->mMotionDetector:Lmiui/v5/widget/VerticalMotionDetector;

    invoke-virtual {v0, p1}, Lmiui/v5/widget/VerticalMotionDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public setMotionStrategy(Lmiui/v5/widget/MotionDetectListener;)V
    .registers 3
    .parameter "strategy"

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/VerticalMotionFrameLayout;->mMotionDetector:Lmiui/v5/widget/VerticalMotionDetector;

    invoke-virtual {v0, p1}, Lmiui/v5/widget/VerticalMotionDetector;->setMotionStrategy(Lmiui/v5/widget/MotionDetectListener;)V

    return-void
.end method
