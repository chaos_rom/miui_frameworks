.class public Lmiui/v5/widget/VerticalTranslationController;
.super Lmiui/v5/widget/AbsTranslationController;
.source "VerticalTranslationController.java"


# instance fields
.field private final mMaxY:I

.field private final mMaxYBounce:I

.field private final mMinAnchorVelocity:I

.field private final mMinY:I

.field private final mMinYBounce:I

.field private mStartY:I

.field private final mTranslateSlop:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lmiui/v5/widget/MotionDetectStrategy;IIII)V
    .registers 14
    .parameter "context"
    .parameter "mml"
    .parameter "minY"
    .parameter "maxY"
    .parameter "minYBounce"
    .parameter "maxYBounce"

    .prologue
    const/4 v6, 0x0

    invoke-direct {p0, p1, p2}, Lmiui/v5/widget/AbsTranslationController;-><init>(Landroid/content/Context;Lmiui/v5/widget/MotionDetectStrategy;)V

    iput v6, p0, Lmiui/v5/widget/VerticalTranslationController;->mStartY:I

    if-gt p5, p3, :cond_25

    if-ge p3, p4, :cond_25

    if-gt p4, p6, :cond_25

    iput p3, p0, Lmiui/v5/widget/VerticalTranslationController;->mMinY:I

    iput p5, p0, Lmiui/v5/widget/VerticalTranslationController;->mMinYBounce:I

    iput p4, p0, Lmiui/v5/widget/VerticalTranslationController;->mMaxY:I

    iput p6, p0, Lmiui/v5/widget/VerticalTranslationController;->mMaxYBounce:I

    invoke-static {p1}, Lmiui/v5/widget/MiuiViewConfiguration;->get(Landroid/content/Context;)Lmiui/v5/widget/MiuiViewConfiguration;

    move-result-object v0

    .local v0, config:Lmiui/v5/widget/MiuiViewConfiguration;
    invoke-virtual {v0}, Lmiui/v5/widget/MiuiViewConfiguration;->getScaledTranslateSlop()I

    move-result v1

    iput v1, p0, Lmiui/v5/widget/VerticalTranslationController;->mTranslateSlop:I

    invoke-virtual {v0}, Lmiui/v5/widget/MiuiViewConfiguration;->getScaledMinAnchorVelocity()I

    move-result v1

    iput v1, p0, Lmiui/v5/widget/VerticalTranslationController;->mMinAnchorVelocity:I

    return-void

    .end local v0           #config:Lmiui/v5/widget/MiuiViewConfiguration;
    :cond_25
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "minYBounce <= minY < maxY <= maxYBounce is necessary!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%d %d %d %d"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    const/4 v5, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method protected computVelocity(Landroid/view/VelocityTracker;)I
    .registers 5
    .parameter "tracker"

    .prologue
    if-eqz p1, :cond_10

    const/16 v1, 0x3e8

    iget v2, p0, Lmiui/v5/widget/AbsTranslationController;->mMaximumVelocity:I

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    invoke-virtual {p1}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v1

    float-to-int v0, v1

    .local v0, velocity:I
    :goto_f
    return v0

    .end local v0           #velocity:I
    :cond_10
    const/4 v0, 0x0

    .restart local v0       #velocity:I
    goto :goto_f
.end method

.method protected getAnchorPostion(Landroid/view/View;IIIII)I
    .registers 13
    .parameter "v"
    .parameter "x"
    .parameter "y"
    .parameter "startX"
    .parameter "startY"
    .parameter "velocity"

    .prologue
    const v0, 0x7fffffff

    .local v0, anchor:I
    invoke-virtual {p1}, Landroid/view/View;->getY()F

    move-result v4

    float-to-int v1, v4

    .local v1, currentY:I
    invoke-static {p6}, Ljava/lang/Math;->abs(I)I

    move-result v4

    iget v5, p0, Lmiui/v5/widget/VerticalTranslationController;->mMinAnchorVelocity:I

    if-ge v4, v5, :cond_1d

    iget v4, p0, Lmiui/v5/widget/VerticalTranslationController;->mStartY:I

    if-ge v1, v4, :cond_3a

    iget v4, p0, Lmiui/v5/widget/VerticalTranslationController;->mMaxY:I

    iget v5, p0, Lmiui/v5/widget/VerticalTranslationController;->mTranslateSlop:I

    sub-int/2addr v4, v5

    if-ge v1, v4, :cond_3a

    iget v0, p0, Lmiui/v5/widget/VerticalTranslationController;->mMinY:I

    :cond_1d
    :goto_1d
    const v4, 0x7fffffff

    if-ne v0, v4, :cond_39

    div-int/lit8 v4, p6, 0x2

    add-int/2addr v1, v4

    iget v4, p0, Lmiui/v5/widget/VerticalTranslationController;->mMinY:I

    sub-int v3, v4, v1

    .local v3, deltaMinY:I
    iget v4, p0, Lmiui/v5/widget/VerticalTranslationController;->mMaxY:I

    sub-int v2, v4, v1

    .local v2, deltaMaxY:I
    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v4

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v5

    if-ge v4, v5, :cond_48

    iget v0, p0, Lmiui/v5/widget/VerticalTranslationController;->mMinY:I

    .end local v2           #deltaMaxY:I
    .end local v3           #deltaMinY:I
    :cond_39
    :goto_39
    return v0

    :cond_3a
    iget v4, p0, Lmiui/v5/widget/VerticalTranslationController;->mStartY:I

    if-le v1, v4, :cond_1d

    iget v4, p0, Lmiui/v5/widget/VerticalTranslationController;->mMinY:I

    iget v5, p0, Lmiui/v5/widget/VerticalTranslationController;->mTranslateSlop:I

    add-int/2addr v4, v5

    if-le v1, v4, :cond_1d

    iget v0, p0, Lmiui/v5/widget/VerticalTranslationController;->mMaxY:I

    goto :goto_1d

    .restart local v2       #deltaMaxY:I
    .restart local v3       #deltaMinY:I
    :cond_48
    iget v0, p0, Lmiui/v5/widget/VerticalTranslationController;->mMaxY:I

    goto :goto_39
.end method

.method protected getValidMovePosition(Landroid/view/View;IIII)I
    .registers 10
    .parameter "v"
    .parameter "x"
    .parameter "y"
    .parameter "startX"
    .parameter "startY"

    .prologue
    invoke-virtual {p1}, Landroid/view/View;->getTranslationY()F

    move-result v2

    int-to-float v3, p3

    add-float/2addr v2, v3

    int-to-float v3, p5

    sub-float/2addr v2, v3

    float-to-int v1, v2

    .local v1, posY:I
    iget v2, p0, Lmiui/v5/widget/VerticalTranslationController;->mMinYBounce:I

    if-ge v1, v2, :cond_10

    iget v0, p0, Lmiui/v5/widget/VerticalTranslationController;->mMinYBounce:I

    .local v0, dest:I
    :goto_f
    return v0

    .end local v0           #dest:I
    :cond_10
    iget v2, p0, Lmiui/v5/widget/VerticalTranslationController;->mMaxYBounce:I

    if-le v1, v2, :cond_17

    iget v0, p0, Lmiui/v5/widget/VerticalTranslationController;->mMaxYBounce:I

    .restart local v0       #dest:I
    goto :goto_f

    .end local v0           #dest:I
    :cond_17
    move v0, v1

    .restart local v0       #dest:I
    goto :goto_f
.end method

.method public onMoveStart(Landroid/view/View;II)V
    .registers 5
    .parameter "view"
    .parameter "x"
    .parameter "y"

    .prologue
    invoke-super {p0, p1, p2, p3}, Lmiui/v5/widget/AbsTranslationController;->onMoveStart(Landroid/view/View;II)V

    invoke-virtual {p1}, Landroid/view/View;->getY()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lmiui/v5/widget/VerticalTranslationController;->mStartY:I

    return-void
.end method

.method protected translate(Landroid/view/View;F)V
    .registers 3
    .parameter "v"
    .parameter "t"

    .prologue
    invoke-virtual {p1, p2}, Landroid/view/View;->setTranslationY(F)V

    return-void
.end method
