.class Lmiui/v5/widget/TabController$MyDynamicPagerAdapter;
.super Lmiui/v5/widget/TabController$DynamicPagerAdapter;
.source "TabController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/v5/widget/TabController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MyDynamicPagerAdapter"
.end annotation


# instance fields
.field private final mFragmentManager:Landroid/app/FragmentManager;

.field private final mFragments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/app/Fragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/FragmentManager;)V
    .registers 3
    .parameter "fm"

    .prologue
    invoke-direct {p0, p1}, Lmiui/v5/widget/TabController$DynamicPagerAdapter;-><init>(Landroid/app/FragmentManager;)V

    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lmiui/v5/widget/TabController$MyDynamicPagerAdapter;->mFragments:Ljava/util/List;

    iput-object p1, p0, Lmiui/v5/widget/TabController$MyDynamicPagerAdapter;->mFragmentManager:Landroid/app/FragmentManager;

    return-void
.end method


# virtual methods
.method addFragment(Landroid/app/Fragment;)V
    .registers 3
    .parameter "fragment"

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/TabController$MyDynamicPagerAdapter;->mFragments:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;
    .registers 5
    .parameter "tag"

    .prologue
    if-eqz p1, :cond_1f

    iget-object v2, p0, Lmiui/v5/widget/TabController$MyDynamicPagerAdapter;->mFragments:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :cond_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Fragment;

    .local v0, f:Landroid/app/Fragment;
    invoke-virtual {v0}, Landroid/app/Fragment;->getTag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .end local v0           #f:Landroid/app/Fragment;
    .end local v1           #i$:Ljava/util/Iterator;
    :goto_1e
    return-object v0

    :cond_1f
    const/4 v0, 0x0

    goto :goto_1e
.end method

.method public getCount()I
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/TabController$MyDynamicPagerAdapter;->mFragments:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method getFragmentManager()Landroid/app/FragmentManager;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/TabController$MyDynamicPagerAdapter;->mFragmentManager:Landroid/app/FragmentManager;

    return-object v0
.end method

.method public getItem(I)Landroid/app/Fragment;
    .registers 3
    .parameter "position"

    .prologue
    if-ltz p1, :cond_13

    iget-object v0, p0, Lmiui/v5/widget/TabController$MyDynamicPagerAdapter;->mFragments:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_13

    iget-object v0, p0, Lmiui/v5/widget/TabController$MyDynamicPagerAdapter;->mFragments:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Fragment;

    :goto_12
    return-object v0

    :cond_13
    const/4 v0, 0x0

    goto :goto_12
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .registers 3
    .parameter "object"

    .prologue
    const/4 v0, -0x2

    return v0
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .registers 4
    .parameter "view"
    .parameter "object"

    .prologue
    check-cast p2, Landroid/app/Fragment;

    .end local p2
    invoke-virtual {p2}, Landroid/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    if-ne v0, p1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method removeFragment(Landroid/app/Fragment;)Z
    .registers 3
    .parameter "fragment"

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/TabController$MyDynamicPagerAdapter;->mFragments:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
