.class public Lmiui/v5/widget/PageScrollEffects;
.super Ljava/lang/Object;
.source "PageScrollEffects.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/v5/widget/PageScrollEffects$DefaultPageScrollEffect;,
        Lmiui/v5/widget/PageScrollEffects$PageScrolledDispatcher;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static makePageChangeAdapter(Landroid/support/v4/view/ViewPager;Lmiui/v5/util/Factory;)Landroid/support/v4/view/ViewPager$OnPageChangeListener;
    .registers 4
    .parameter "pager"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/view/ViewPager;",
            "Lmiui/v5/util/Factory",
            "<",
            "Lmiui/v5/widget/PageScrollEffect;",
            "Ljava/lang/Integer;",
            ">;)",
            "Landroid/support/v4/view/ViewPager$OnPageChangeListener;"
        }
    .end annotation

    .prologue
    .local p1, factory:Lmiui/v5/util/Factory;,"Lmiui/v5/util/Factory<Lmiui/v5/widget/PageScrollEffect;Ljava/lang/Integer;>;"
    new-instance v0, Lmiui/v5/widget/PageChangeAdapter;

    invoke-direct {v0, p0}, Lmiui/v5/widget/PageChangeAdapter;-><init>(Landroid/support/v4/view/ViewPager;)V

    .local v0, adapter:Lmiui/v5/widget/PageChangeAdapter;
    new-instance v1, Lmiui/v5/widget/PageScrollEffects$PageScrolledDispatcher;

    invoke-direct {v1, p1}, Lmiui/v5/widget/PageScrollEffects$PageScrolledDispatcher;-><init>(Lmiui/v5/util/Factory;)V

    invoke-virtual {v0, v1}, Lmiui/v5/widget/PageChangeAdapter;->setScrolledListener(Lmiui/v5/widget/PageChangeAdapter$OnPageScrollListener;)V

    return-object v0
.end method

.method public static makePageScrollEffect(Landroid/view/ViewGroup;)Lmiui/v5/widget/PageScrollEffect;
    .registers 2
    .parameter "group"

    .prologue
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lmiui/v5/widget/PageScrollEffects;->makePageScrollEffect(Landroid/view/ViewGroup;[I)Lmiui/v5/widget/PageScrollEffect;

    move-result-object v0

    return-object v0
.end method

.method public static makePageScrollEffect(Landroid/view/ViewGroup;[I)Lmiui/v5/widget/PageScrollEffect;
    .registers 3
    .parameter "group"
    .parameter "resIds"

    .prologue
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lmiui/v5/widget/PageScrollEffects;->makePageScrollEffect(Landroid/view/ViewGroup;[IZ)Lmiui/v5/widget/PageScrollEffect;

    move-result-object v0

    return-object v0
.end method

.method public static makePageScrollEffect(Landroid/view/ViewGroup;[IZ)Lmiui/v5/widget/PageScrollEffect;
    .registers 4
    .parameter "group"
    .parameter "resIds"
    .parameter "attach"

    .prologue
    const/4 v0, 0x0

    .local v0, effect:Lmiui/v5/widget/PageScrollEffect;
    if-eqz p0, :cond_d

    new-instance v0, Lmiui/v5/widget/PageScrollEffects$DefaultPageScrollEffect;

    .end local v0           #effect:Lmiui/v5/widget/PageScrollEffect;
    invoke-direct {v0, p0, p1}, Lmiui/v5/widget/PageScrollEffects$DefaultPageScrollEffect;-><init>(Landroid/view/ViewGroup;[I)V

    .restart local v0       #effect:Lmiui/v5/widget/PageScrollEffect;
    if-eqz p2, :cond_d

    invoke-interface {v0}, Lmiui/v5/widget/PageScrollEffect;->attach()Z

    :cond_d
    return-object v0
.end method
