.class public Lmiui/v5/widget/TabController;
.super Ljava/lang/Object;
.source "TabController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/v5/widget/TabController$PagerAdapterChangedListener;,
        Lmiui/v5/widget/TabController$MyDynamicPagerAdapter;,
        Lmiui/v5/widget/TabController$DynamicPagerAdapter;,
        Lmiui/v5/widget/TabController$TabEditorImpl;,
        Lmiui/v5/widget/TabController$TabEditRemove;,
        Lmiui/v5/widget/TabController$TabEditAdd;,
        Lmiui/v5/widget/TabController$TabEditCommand;,
        Lmiui/v5/widget/TabController$TabEditor;,
        Lmiui/v5/widget/TabController$TabImpl;
    }
.end annotation


# static fields
.field static final TAG:Ljava/lang/String;


# instance fields
.field final mAdapter:Lmiui/v5/widget/TabController$DynamicPagerAdapter;

.field mAdapterChangedListener:Lmiui/v5/widget/TabController$PagerAdapterChangedListener;

.field final mTabContainer:Lmiui/v5/widget/TabContainerLayout;

.field mTabIndicator:Lmiui/v5/widget/TabIndicator;

.field final mViewPager:Lmiui/v5/widget/CooperativeViewPager;

.field mViewPagerListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

.field private final mViewPagerListenerDecorator:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

.field mViewPagerState:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    const-class v0, Lmiui/v5/widget/TabController;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lmiui/v5/widget/TabController;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lmiui/v5/widget/TabContainerLayout;Lmiui/v5/widget/CooperativeViewPager;Lmiui/v5/widget/TabIndicator;)V
    .registers 7
    .parameter "a"
    .parameter "container"
    .parameter "pager"
    .parameter "indicator"

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lmiui/v5/widget/TabController;->mViewPagerState:I

    new-instance v0, Lmiui/v5/widget/TabController$2;

    invoke-direct {v0, p0}, Lmiui/v5/widget/TabController$2;-><init>(Lmiui/v5/widget/TabController;)V

    iput-object v0, p0, Lmiui/v5/widget/TabController;->mViewPagerListenerDecorator:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    iput-object p2, p0, Lmiui/v5/widget/TabController;->mTabContainer:Lmiui/v5/widget/TabContainerLayout;

    new-instance v0, Lmiui/v5/widget/TabController$1;

    invoke-direct {v0, p0}, Lmiui/v5/widget/TabController$1;-><init>(Lmiui/v5/widget/TabController;)V

    invoke-virtual {p2, v0}, Lmiui/v5/widget/TabContainerLayout;->setTabWidthChangedListener(Lmiui/v5/widget/TabContainerLayout$TabWidthChangedListener;)V

    invoke-virtual {p0, p4}, Lmiui/v5/widget/TabController;->setTabIndicator(Lmiui/v5/widget/TabIndicator;)V

    new-instance v0, Lmiui/v5/widget/TabController$MyDynamicPagerAdapter;

    invoke-virtual {p1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-direct {v0, v1}, Lmiui/v5/widget/TabController$MyDynamicPagerAdapter;-><init>(Landroid/app/FragmentManager;)V

    iput-object v0, p0, Lmiui/v5/widget/TabController;->mAdapter:Lmiui/v5/widget/TabController$DynamicPagerAdapter;

    iput-object p3, p0, Lmiui/v5/widget/TabController;->mViewPager:Lmiui/v5/widget/CooperativeViewPager;

    iget-object v0, p0, Lmiui/v5/widget/TabController;->mViewPager:Lmiui/v5/widget/CooperativeViewPager;

    iget-object v1, p0, Lmiui/v5/widget/TabController;->mAdapter:Lmiui/v5/widget/TabController$DynamicPagerAdapter;

    invoke-virtual {v0, v1}, Lmiui/v5/widget/CooperativeViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    iget-object v0, p0, Lmiui/v5/widget/TabController;->mViewPager:Lmiui/v5/widget/CooperativeViewPager;

    iget-object v1, p0, Lmiui/v5/widget/TabController;->mViewPagerListenerDecorator:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    invoke-virtual {v0, v1}, Lmiui/v5/widget/CooperativeViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    return-void
.end method


# virtual methods
.method public addTab(Landroid/app/ActionBar$Tab;Landroid/app/Fragment;)V
    .registers 4
    .parameter "tab"
    .parameter "fragment"

    .prologue
    invoke-virtual {p0}, Lmiui/v5/widget/TabController;->newTabEditor()Lmiui/v5/widget/TabController$TabEditor;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lmiui/v5/widget/TabController$TabEditor;->addTab(Landroid/app/ActionBar$Tab;Landroid/app/Fragment;)Lmiui/v5/widget/TabController$TabEditor;

    move-result-object v0

    invoke-interface {v0}, Lmiui/v5/widget/TabController$TabEditor;->commit()Z

    return-void
.end method

.method public findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;
    .registers 3
    .parameter "tag"

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/TabController;->mAdapter:Lmiui/v5/widget/TabController$DynamicPagerAdapter;

    invoke-virtual {v0, p1}, Lmiui/v5/widget/TabController$DynamicPagerAdapter;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    return-object v0
.end method

.method public getFragment(I)Landroid/app/Fragment;
    .registers 3
    .parameter "position"

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/TabController;->mAdapter:Lmiui/v5/widget/TabController$DynamicPagerAdapter;

    invoke-virtual {v0, p1}, Lmiui/v5/widget/TabController$DynamicPagerAdapter;->getItem(I)Landroid/app/Fragment;

    move-result-object v0

    return-object v0
.end method

.method public getFragmentCount()I
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/TabController;->mAdapter:Lmiui/v5/widget/TabController$DynamicPagerAdapter;

    invoke-virtual {v0}, Lmiui/v5/widget/TabController$DynamicPagerAdapter;->getCount()I

    move-result v0

    return v0
.end method

.method public getSelectedFragment()Landroid/app/Fragment;
    .registers 3

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/TabController;->mAdapter:Lmiui/v5/widget/TabController$DynamicPagerAdapter;

    invoke-virtual {p0}, Lmiui/v5/widget/TabController;->getSelectedPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lmiui/v5/widget/TabController$DynamicPagerAdapter;->getItem(I)Landroid/app/Fragment;

    move-result-object v0

    return-object v0
.end method

.method public getSelectedPosition()I
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/TabController;->mViewPager:Lmiui/v5/widget/CooperativeViewPager;

    invoke-virtual {v0}, Lmiui/v5/widget/CooperativeViewPager;->getCurrentItem()I

    move-result v0

    return v0
.end method

.method public getSelectedTab()Landroid/app/ActionBar$Tab;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/TabController;->mTabContainer:Lmiui/v5/widget/TabContainerLayout;

    invoke-virtual {v0}, Lmiui/v5/widget/TabContainerLayout;->getSelectedTab()Landroid/app/ActionBar$Tab;

    move-result-object v0

    return-object v0
.end method

.method public getTabContainer()Lmiui/v5/widget/TabContainerLayout;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/TabController;->mTabContainer:Lmiui/v5/widget/TabContainerLayout;

    return-object v0
.end method

.method public getViewPager()Landroid/support/v4/view/ViewPager;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/TabController;->mViewPager:Lmiui/v5/widget/CooperativeViewPager;

    return-object v0
.end method

.method public getViewPagerState()I
    .registers 2

    .prologue
    iget v0, p0, Lmiui/v5/widget/TabController;->mViewPagerState:I

    return v0
.end method

.method public newTab()Landroid/app/ActionBar$Tab;
    .registers 3

    .prologue
    new-instance v0, Lmiui/v5/widget/TabController$TabImpl;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lmiui/v5/widget/TabController$TabImpl;-><init>(Lmiui/v5/widget/TabController;Landroid/app/ActionBar$TabListener;)V

    return-object v0
.end method

.method public newTab(Landroid/app/ActionBar$TabListener;)Landroid/app/ActionBar$Tab;
    .registers 3
    .parameter "l"

    .prologue
    new-instance v0, Lmiui/v5/widget/TabController$TabImpl;

    invoke-direct {v0, p0, p1}, Lmiui/v5/widget/TabController$TabImpl;-><init>(Lmiui/v5/widget/TabController;Landroid/app/ActionBar$TabListener;)V

    return-object v0
.end method

.method public newTabEditor()Lmiui/v5/widget/TabController$TabEditor;
    .registers 2

    .prologue
    new-instance v0, Lmiui/v5/widget/TabController$TabEditorImpl;

    invoke-direct {v0, p0}, Lmiui/v5/widget/TabController$TabEditorImpl;-><init>(Lmiui/v5/widget/TabController;)V

    return-object v0
.end method

.method protected onTabReselected(Landroid/app/ActionBar$Tab;)V
    .registers 5
    .parameter "tab"

    .prologue
    if-nez p1, :cond_3

    :cond_2
    :goto_2
    return-void

    :cond_3
    move-object v2, p1

    check-cast v2, Lmiui/v5/widget/TabController$TabImpl;

    invoke-virtual {v2}, Lmiui/v5/widget/TabController$TabImpl;->getCallback()Landroid/app/ActionBar$TabListener;

    move-result-object v1

    .local v1, l:Landroid/app/ActionBar$TabListener;
    if-eqz v1, :cond_2

    iget-object v2, p0, Lmiui/v5/widget/TabController;->mAdapter:Lmiui/v5/widget/TabController$DynamicPagerAdapter;

    invoke-virtual {v2}, Lmiui/v5/widget/TabController$DynamicPagerAdapter;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->disallowAddToBackStack()Landroid/app/FragmentTransaction;

    move-result-object v0

    .local v0, ft:Landroid/app/FragmentTransaction;
    invoke-interface {v1, p1, v0}, Landroid/app/ActionBar$TabListener;->onTabReselected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    goto :goto_2
.end method

.method protected onTabSelected(Landroid/app/ActionBar$Tab;)V
    .registers 7
    .parameter "tab"

    .prologue
    if-nez p1, :cond_3

    :cond_2
    :goto_2
    return-void

    :cond_3
    iget-object v3, p0, Lmiui/v5/widget/TabController;->mTabContainer:Lmiui/v5/widget/TabContainerLayout;

    invoke-virtual {v3, p1}, Lmiui/v5/widget/TabContainerLayout;->findTabPosition(Landroid/app/ActionBar$Tab;)I

    move-result v2

    .local v2, pos:I
    if-ltz v2, :cond_21

    iget-object v3, p0, Lmiui/v5/widget/TabController;->mAdapter:Lmiui/v5/widget/TabController$DynamicPagerAdapter;

    invoke-virtual {v3}, Lmiui/v5/widget/TabController$DynamicPagerAdapter;->getCount()I

    move-result v3

    if-ge v2, v3, :cond_21

    iget-object v3, p0, Lmiui/v5/widget/TabController;->mViewPager:Lmiui/v5/widget/CooperativeViewPager;

    invoke-virtual {v3}, Lmiui/v5/widget/CooperativeViewPager;->getCurrentItem()I

    move-result v3

    if-eq v2, v3, :cond_21

    iget-object v3, p0, Lmiui/v5/widget/TabController;->mViewPager:Lmiui/v5/widget/CooperativeViewPager;

    const/4 v4, 0x1

    invoke-virtual {v3, v2, v4}, Lmiui/v5/widget/CooperativeViewPager;->setCurrentItem(IZ)V

    :cond_21
    move-object v3, p1

    check-cast v3, Lmiui/v5/widget/TabController$TabImpl;

    invoke-virtual {v3}, Lmiui/v5/widget/TabController$TabImpl;->getCallback()Landroid/app/ActionBar$TabListener;

    move-result-object v1

    .local v1, l:Landroid/app/ActionBar$TabListener;
    if-eqz v1, :cond_2

    iget-object v3, p0, Lmiui/v5/widget/TabController;->mAdapter:Lmiui/v5/widget/TabController$DynamicPagerAdapter;

    invoke-virtual {v3}, Lmiui/v5/widget/TabController$DynamicPagerAdapter;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentTransaction;->disallowAddToBackStack()Landroid/app/FragmentTransaction;

    move-result-object v0

    .local v0, ft:Landroid/app/FragmentTransaction;
    invoke-interface {v1, p1, v0}, Landroid/app/ActionBar$TabListener;->onTabSelected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    goto :goto_2
.end method

.method protected onTabUnselected(Landroid/app/ActionBar$Tab;)V
    .registers 5
    .parameter "tab"

    .prologue
    if-nez p1, :cond_3

    :cond_2
    :goto_2
    return-void

    :cond_3
    move-object v2, p1

    check-cast v2, Lmiui/v5/widget/TabController$TabImpl;

    invoke-virtual {v2}, Lmiui/v5/widget/TabController$TabImpl;->getCallback()Landroid/app/ActionBar$TabListener;

    move-result-object v1

    .local v1, l:Landroid/app/ActionBar$TabListener;
    if-eqz v1, :cond_2

    iget-object v2, p0, Lmiui/v5/widget/TabController;->mAdapter:Lmiui/v5/widget/TabController$DynamicPagerAdapter;

    invoke-virtual {v2}, Lmiui/v5/widget/TabController$DynamicPagerAdapter;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->disallowAddToBackStack()Landroid/app/FragmentTransaction;

    move-result-object v0

    .local v0, ft:Landroid/app/FragmentTransaction;
    invoke-interface {v1, p1, v0}, Landroid/app/ActionBar$TabListener;->onTabUnselected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    goto :goto_2
.end method

.method public release()V
    .registers 3

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/TabController;->mViewPager:Lmiui/v5/widget/CooperativeViewPager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmiui/v5/widget/CooperativeViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    iget-object v0, p0, Lmiui/v5/widget/TabController;->mTabContainer:Lmiui/v5/widget/TabContainerLayout;

    iget-object v1, p0, Lmiui/v5/widget/TabController;->mTabIndicator:Lmiui/v5/widget/TabIndicator;

    invoke-virtual {v0, v1}, Lmiui/v5/widget/TabContainerLayout;->detachIndicator(Lmiui/v5/widget/TabIndicator;)V

    return-void
.end method

.method public removeAll()V
    .registers 5

    .prologue
    invoke-virtual {p0}, Lmiui/v5/widget/TabController;->newTabEditor()Lmiui/v5/widget/TabController$TabEditor;

    move-result-object v1

    .local v1, editor:Lmiui/v5/widget/TabController$TabEditor;
    iget-object v3, p0, Lmiui/v5/widget/TabController;->mTabContainer:Lmiui/v5/widget/TabContainerLayout;

    invoke-virtual {v3}, Lmiui/v5/widget/TabContainerLayout;->getTabCount()I

    move-result v0

    .local v0, count:I
    const/4 v2, 0x0

    .local v2, i:I
    :goto_b
    if-ge v2, v0, :cond_19

    iget-object v3, p0, Lmiui/v5/widget/TabController;->mTabContainer:Lmiui/v5/widget/TabContainerLayout;

    invoke-virtual {v3, v2}, Lmiui/v5/widget/TabContainerLayout;->getTabAt(I)Landroid/app/ActionBar$Tab;

    move-result-object v3

    invoke-interface {v1, v3}, Lmiui/v5/widget/TabController$TabEditor;->removeTab(Landroid/app/ActionBar$Tab;)Lmiui/v5/widget/TabController$TabEditor;

    add-int/lit8 v2, v2, 0x1

    goto :goto_b

    :cond_19
    invoke-interface {v1}, Lmiui/v5/widget/TabController$TabEditor;->commit()Z

    return-void
.end method

.method public removeTab(Landroid/app/ActionBar$Tab;)V
    .registers 3
    .parameter "tab"

    .prologue
    if-eqz p1, :cond_d

    invoke-virtual {p0}, Lmiui/v5/widget/TabController;->newTabEditor()Lmiui/v5/widget/TabController$TabEditor;

    move-result-object v0

    invoke-interface {v0, p1}, Lmiui/v5/widget/TabController$TabEditor;->removeTab(Landroid/app/ActionBar$Tab;)Lmiui/v5/widget/TabController$TabEditor;

    move-result-object v0

    invoke-interface {v0}, Lmiui/v5/widget/TabController$TabEditor;->commit()Z

    :cond_d
    return-void
.end method

.method public removeTabAt(I)V
    .registers 3
    .parameter "position"

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/TabController;->mTabContainer:Lmiui/v5/widget/TabContainerLayout;

    invoke-virtual {v0, p1}, Lmiui/v5/widget/TabContainerLayout;->getTabAt(I)Landroid/app/ActionBar$Tab;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmiui/v5/widget/TabController;->removeTab(Landroid/app/ActionBar$Tab;)V

    return-void
.end method

.method public selectTab(I)Z
    .registers 3
    .parameter "position"

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/TabController;->mTabContainer:Lmiui/v5/widget/TabContainerLayout;

    invoke-virtual {v0, p1}, Lmiui/v5/widget/TabContainerLayout;->getTabAt(I)Landroid/app/ActionBar$Tab;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmiui/v5/widget/TabController;->selectTab(Landroid/app/ActionBar$Tab;)Z

    move-result v0

    return v0
.end method

.method public selectTab(Landroid/app/ActionBar$Tab;)Z
    .registers 5
    .parameter "tab"

    .prologue
    const/4 v0, 0x0

    .local v0, changed:Z
    iget-object v2, p0, Lmiui/v5/widget/TabController;->mTabContainer:Lmiui/v5/widget/TabContainerLayout;

    invoke-virtual {v2, p1}, Lmiui/v5/widget/TabContainerLayout;->selectTab(Landroid/app/ActionBar$Tab;)Landroid/app/ActionBar$Tab;

    move-result-object v1

    .local v1, oldSelected:Landroid/app/ActionBar$Tab;
    if-ne p1, v1, :cond_d

    invoke-virtual {p0, p1}, Lmiui/v5/widget/TabController;->onTabReselected(Landroid/app/ActionBar$Tab;)V

    :goto_c
    return v0

    :cond_d
    const/4 v0, 0x1

    invoke-virtual {p0, v1}, Lmiui/v5/widget/TabController;->onTabUnselected(Landroid/app/ActionBar$Tab;)V

    invoke-virtual {p0, p1}, Lmiui/v5/widget/TabController;->onTabSelected(Landroid/app/ActionBar$Tab;)V

    goto :goto_c
.end method

.method public setCooperative(Z)V
    .registers 6
    .parameter "enabled"

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lmiui/v5/widget/TabController;->mTabContainer:Lmiui/v5/widget/TabContainerLayout;

    if-nez p1, :cond_12

    move v0, v1

    :goto_7
    invoke-virtual {v3, v0}, Lmiui/v5/widget/TabContainerLayout;->setInteractive(Z)V

    iget-object v0, p0, Lmiui/v5/widget/TabController;->mViewPager:Lmiui/v5/widget/CooperativeViewPager;

    if-nez p1, :cond_14

    :goto_e
    invoke-virtual {v0, v1}, Lmiui/v5/widget/CooperativeViewPager;->setDraggable(Z)V

    return-void

    :cond_12
    move v0, v2

    goto :goto_7

    :cond_14
    move v1, v2

    goto :goto_e
.end method

.method public setPagerAdapterChangedListener(Lmiui/v5/widget/TabController$PagerAdapterChangedListener;)V
    .registers 2
    .parameter "l"

    .prologue
    iput-object p1, p0, Lmiui/v5/widget/TabController;->mAdapterChangedListener:Lmiui/v5/widget/TabController$PagerAdapterChangedListener;

    return-void
.end method

.method public setTabIndicator(Lmiui/v5/widget/TabIndicator;)V
    .registers 4
    .parameter "indicator"

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/TabController;->mTabIndicator:Lmiui/v5/widget/TabIndicator;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lmiui/v5/widget/TabController;->mTabContainer:Lmiui/v5/widget/TabContainerLayout;

    iget-object v1, p0, Lmiui/v5/widget/TabController;->mTabIndicator:Lmiui/v5/widget/TabIndicator;

    invoke-virtual {v0, v1}, Lmiui/v5/widget/TabContainerLayout;->detachIndicator(Lmiui/v5/widget/TabIndicator;)V

    :cond_b
    iput-object p1, p0, Lmiui/v5/widget/TabController;->mTabIndicator:Lmiui/v5/widget/TabIndicator;

    if-eqz p1, :cond_14

    iget-object v0, p0, Lmiui/v5/widget/TabController;->mTabContainer:Lmiui/v5/widget/TabContainerLayout;

    invoke-virtual {v0, p1}, Lmiui/v5/widget/TabContainerLayout;->attachIndicator(Lmiui/v5/widget/TabIndicator;)V

    :cond_14
    return-void
.end method

.method public setTabIndicatorImage(Landroid/graphics/drawable/Drawable;)V
    .registers 3
    .parameter "d"

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/TabController;->mTabIndicator:Lmiui/v5/widget/TabIndicator;

    invoke-interface {v0, p1}, Lmiui/v5/widget/TabIndicator;->setIndicator(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public setViewPagerBackground(Landroid/graphics/drawable/Drawable;)V
    .registers 3
    .parameter "d"

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/TabController;->mViewPager:Lmiui/v5/widget/CooperativeViewPager;

    invoke-virtual {v0, p1}, Lmiui/v5/widget/CooperativeViewPager;->setBackground(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public setViewPagerListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V
    .registers 2
    .parameter "l"

    .prologue
    iput-object p1, p0, Lmiui/v5/widget/TabController;->mViewPagerListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    return-void
.end method
