.class public abstract Lmiui/v5/widget/TabController$DynamicPagerAdapter;
.super Lmiui/v5/widget/FragmentPagerAdapter;
.source "TabController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/v5/widget/TabController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "DynamicPagerAdapter"
.end annotation


# direct methods
.method public constructor <init>(Landroid/app/FragmentManager;)V
    .registers 2
    .parameter "fm"

    .prologue
    invoke-direct {p0, p1}, Lmiui/v5/widget/FragmentPagerAdapter;-><init>(Landroid/app/FragmentManager;)V

    return-void
.end method


# virtual methods
.method abstract addFragment(Landroid/app/Fragment;)V
.end method

.method abstract findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;
.end method

.method abstract getFragmentManager()Landroid/app/FragmentManager;
.end method

.method abstract removeFragment(Landroid/app/Fragment;)Z
.end method
