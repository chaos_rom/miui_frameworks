.class Lmiui/v5/widget/PageScrollEffects$DefaultPageScrollEffect;
.super Lmiui/v5/widget/PageScrollEffect$AbsPageScrollEffect;
.source "PageScrollEffects.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/v5/widget/PageScrollEffects;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DefaultPageScrollEffect"
.end annotation


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;[I)V
    .registers 3
    .parameter "v"
    .parameter "resIds"

    .prologue
    invoke-direct {p0, p1, p2}, Lmiui/v5/widget/PageScrollEffect$AbsPageScrollEffect;-><init>(Landroid/view/ViewGroup;[I)V

    return-void
.end method

.method private static computOffset(IIIF)I
    .registers 11
    .parameter "top"
    .parameter "width"
    .parameter "height"
    .parameter "percent"

    .prologue
    const v0, 0x3dcccccd

    .local v0, FINE_PARAM:F
    mul-int v4, p0, p1

    div-int v2, v4, p2

    .local v2, indent:I
    mul-float v1, p3, p3

    .local v1, coeff:F
    int-to-float v4, v2

    const v5, 0x3dcccccd

    const v6, 0x3f666666

    div-float v6, v1, v6

    sub-float/2addr v5, v6

    int-to-float v6, p1

    mul-float/2addr v5, v6

    add-float v3, v4, v5

    .local v3, offset:F
    const/4 v4, 0x0

    cmpl-float v4, v3, v4

    if-lez v4, :cond_1e

    float-to-int v4, v3

    :goto_1d
    return v4

    :cond_1e
    const/4 v4, 0x0

    goto :goto_1d
.end method

.method static translateView(Landroid/view/ViewGroup;IIFZ)V
    .registers 15
    .parameter "view"
    .parameter "width"
    .parameter "height"
    .parameter "percent"
    .parameter "isRight"

    .prologue
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .local v0, contentRect:Landroid/graphics/Rect;
    invoke-static {p0, v0}, Lmiui/v5/widget/Views;->getContentRect(Landroid/view/ViewGroup;Landroid/graphics/Rect;)V

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_f

    :cond_e
    return-void

    :cond_f
    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v6

    .local v6, list:Ljava/util/List;,"Ljava/util/List<Landroid/view/View;>;"
    invoke-static {p0}, Lmiui/v5/widget/PageScrollEffects$DefaultPageScrollEffect;->getEffectType(Landroid/view/View;)I

    move-result v2

    .local v2, effectType:I
    const/4 v9, 0x3

    if-ne v2, v9, :cond_6e

    new-instance v9, Lmiui/v5/widget/PageScrollEffect$AbsPageScrollEffect$AllVisiblePred;

    invoke-direct {v9, v0}, Lmiui/v5/widget/PageScrollEffect$AbsPageScrollEffect$AllVisiblePred;-><init>(Landroid/graphics/Rect;)V

    invoke-static {p0, v9, v6}, Lmiui/v5/widget/Views;->collectChildren(Landroid/view/ViewGroup;Lcom/android/internal/util/Predicate;Ljava/util/List;)V

    :cond_22
    :goto_22
    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_e

    sget-object v9, Lmiui/v5/widget/Views;->TOP_COMPARATOR:Ljava/util/Comparator;

    invoke-static {v6, v9}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    const/4 v9, 0x0

    invoke-interface {v6, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getTop()I

    move-result v9

    neg-int v7, v9

    .local v7, offset:I
    const v5, 0x7fffffff

    .local v5, lastTop:I
    const/4 v4, 0x0

    .local v4, lastDelta:I
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, i$:Ljava/util/Iterator;
    :cond_41
    :goto_41
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_e

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/View;

    .local v8, v:Landroid/view/View;
    invoke-virtual {v8}, Landroid/view/View;->getTop()I

    move-result v9

    if-eq v5, v9, :cond_60

    invoke-virtual {v8}, Landroid/view/View;->getTop()I

    move-result v5

    add-int v9, v5, v7

    invoke-static {v9, p1, p2, p3}, Lmiui/v5/widget/PageScrollEffects$DefaultPageScrollEffect;->computOffset(IIIF)I

    move-result v1

    .local v1, distance:I
    if-eqz p4, :cond_7a

    move v4, v1

    .end local v1           #distance:I
    :cond_60
    :goto_60
    int-to-float v9, v4

    invoke-virtual {v8, v9}, Landroid/view/View;->setTranslationX(F)V

    instance-of v9, v8, Landroid/view/ViewGroup;

    if-eqz v9, :cond_41

    check-cast v8, Landroid/view/ViewGroup;

    .end local v8           #v:Landroid/view/View;
    invoke-static {v8, p1, p2, p3, p4}, Lmiui/v5/widget/PageScrollEffects$DefaultPageScrollEffect;->translateView(Landroid/view/ViewGroup;IIFZ)V

    goto :goto_41

    .end local v3           #i$:Ljava/util/Iterator;
    .end local v4           #lastDelta:I
    .end local v5           #lastTop:I
    .end local v7           #offset:I
    :cond_6e
    const/4 v9, 0x2

    if-ne v2, v9, :cond_22

    new-instance v9, Lmiui/v5/widget/PageScrollEffect$AbsPageScrollEffect$AnyVisiblePred;

    invoke-direct {v9, v0}, Lmiui/v5/widget/PageScrollEffect$AbsPageScrollEffect$AnyVisiblePred;-><init>(Landroid/graphics/Rect;)V

    invoke-static {p0, v9, v6}, Lmiui/v5/widget/Views;->collectChildren(Landroid/view/ViewGroup;Lcom/android/internal/util/Predicate;Ljava/util/List;)V

    goto :goto_22

    .restart local v1       #distance:I
    .restart local v3       #i$:Ljava/util/Iterator;
    .restart local v4       #lastDelta:I
    .restart local v5       #lastTop:I
    .restart local v7       #offset:I
    .restart local v8       #v:Landroid/view/View;
    :cond_7a
    neg-int v4, v1

    goto :goto_60
.end method


# virtual methods
.method public scroll(FZ)V
    .registers 6
    .parameter "percent"
    .parameter "isLeft"

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/PageScrollEffect$AbsPageScrollEffect;->mView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_9

    :goto_8
    return-void

    :cond_9
    iget-object v0, p0, Lmiui/v5/widget/PageScrollEffect$AbsPageScrollEffect;->mView:Landroid/view/ViewGroup;

    iget-object v1, p0, Lmiui/v5/widget/PageScrollEffect$AbsPageScrollEffect;->mView:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getWidth()I

    move-result v1

    iget-object v2, p0, Lmiui/v5/widget/PageScrollEffect$AbsPageScrollEffect;->mView:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getHeight()I

    move-result v2

    invoke-static {v0, v1, v2, p1, p2}, Lmiui/v5/widget/PageScrollEffects$DefaultPageScrollEffect;->translateView(Landroid/view/ViewGroup;IIFZ)V

    goto :goto_8
.end method
