.class Lmiui/v5/widget/TabContainerLayout$TabViewImpl;
.super Landroid/widget/LinearLayout;
.source "TabContainerLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/v5/widget/TabContainerLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TabViewImpl"
.end annotation


# instance fields
.field private mCustomView:Landroid/view/View;

.field private mIconView:Landroid/widget/ImageView;

.field private mTab:Landroid/app/ActionBar$Tab;

.field private mTextView:Landroid/widget/TextView;

.field final synthetic this$0:Lmiui/v5/widget/TabContainerLayout;


# direct methods
.method public constructor <init>(Lmiui/v5/widget/TabContainerLayout;Landroid/content/Context;Landroid/app/ActionBar$Tab;)V
    .registers 6
    .parameter
    .parameter "context"
    .parameter "tab"

    .prologue
    iput-object p1, p0, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->this$0:Lmiui/v5/widget/TabContainerLayout;

    const/4 v0, 0x0

    const v1, 0x601005c

    invoke-direct {p0, p2, v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->setGravity(I)V

    iput-object p3, p0, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->mTab:Landroid/app/ActionBar$Tab;

    invoke-virtual {p0}, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->update()V

    return-void
.end method


# virtual methods
.method public getTab()Landroid/app/ActionBar$Tab;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->mTab:Landroid/app/ActionBar$Tab;

    return-object v0
.end method

.method public onMeasure(II)V
    .registers 5
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->this$0:Lmiui/v5/widget/TabContainerLayout;

    iget v0, v0, Lmiui/v5/widget/TabContainerLayout;->mTabWidth:I

    if-lez v0, :cond_10

    iget-object v0, p0, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->this$0:Lmiui/v5/widget/TabContainerLayout;

    iget v0, v0, Lmiui/v5/widget/TabContainerLayout;->mTabWidth:I

    const/high16 v1, 0x4000

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    :cond_10
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    return-void
.end method

.method public update()V
    .registers 13

    .prologue
    const/4 v11, 0x0

    const/16 v10, 0x8

    const/4 v9, 0x0

    iget-object v6, p0, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->this$0:Lmiui/v5/widget/TabContainerLayout;

    iget v6, v6, Lmiui/v5/widget/TabContainerLayout;->mTabBackgroundResId:I

    if-eqz v6, :cond_11

    iget-object v6, p0, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->this$0:Lmiui/v5/widget/TabContainerLayout;

    iget v6, v6, Lmiui/v5/widget/TabContainerLayout;->mTabBackgroundResId:I

    invoke-virtual {p0, v6}, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->setBackgroundResource(I)V

    :cond_11
    iget-object v4, p0, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->mTab:Landroid/app/ActionBar$Tab;

    .local v4, tab:Landroid/app/ActionBar$Tab;
    invoke-virtual {v4}, Landroid/app/ActionBar$Tab;->getCustomView()Landroid/view/View;

    move-result-object v0

    .local v0, custom:Landroid/view/View;
    if-eqz v0, :cond_43

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .local v1, customParent:Landroid/view/ViewParent;
    if-eq v1, p0, :cond_29

    if-eqz v1, :cond_26

    check-cast v1, Landroid/view/ViewGroup;

    .end local v1           #customParent:Landroid/view/ViewParent;
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_26
    invoke-virtual {p0, v0}, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->addView(Landroid/view/View;)V

    :cond_29
    iput-object v0, p0, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->mCustomView:Landroid/view/View;

    iget-object v6, p0, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->mTextView:Landroid/widget/TextView;

    if-eqz v6, :cond_34

    iget-object v6, p0, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v6, v10}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_34
    iget-object v6, p0, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->mIconView:Landroid/widget/ImageView;

    if-eqz v6, :cond_42

    iget-object v6, p0, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->mIconView:Landroid/widget/ImageView;

    invoke-virtual {v6, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v6, p0, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->mIconView:Landroid/widget/ImageView;

    invoke-virtual {v6, v11}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_42
    :goto_42
    return-void

    :cond_43
    iget-object v6, p0, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->mCustomView:Landroid/view/View;

    if-eqz v6, :cond_4e

    iget-object v6, p0, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->mCustomView:Landroid/view/View;

    invoke-virtual {p0, v6}, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->removeView(Landroid/view/View;)V

    iput-object v11, p0, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->mCustomView:Landroid/view/View;

    :cond_4e
    invoke-virtual {v4}, Landroid/app/ActionBar$Tab;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .local v2, icon:Landroid/graphics/drawable/Drawable;
    invoke-virtual {v4}, Landroid/app/ActionBar$Tab;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    .local v5, text:Ljava/lang/CharSequence;
    iget-object v6, p0, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->mContext:Landroid/content/Context;

    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .local v3, inflater:Landroid/view/LayoutInflater;
    if-eqz v2, :cond_cd

    iget-object v6, p0, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->mIconView:Landroid/widget/ImageView;

    if-nez v6, :cond_72

    const v6, 0x6030043

    invoke-virtual {v3, v6, p0, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    iput-object v6, p0, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->mIconView:Landroid/widget/ImageView;

    iget-object v6, p0, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->mIconView:Landroid/widget/ImageView;

    invoke-virtual {p0, v6, v9}, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->addView(Landroid/view/View;I)V

    :cond_72
    iget-object v6, p0, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->mIconView:Landroid/widget/ImageView;

    invoke-virtual {v6, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v6, p0, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->mIconView:Landroid/widget/ImageView;

    invoke-virtual {v6, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v6, p0, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->this$0:Lmiui/v5/widget/TabContainerLayout;

    iget-object v6, v6, Lmiui/v5/widget/TabContainerLayout;->mTabViewStyle:Lmiui/v5/widget/TabContainerLayout$TabViewStyle;

    if-eqz v6, :cond_8d

    iget-object v6, p0, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->this$0:Lmiui/v5/widget/TabContainerLayout;

    iget-object v6, v6, Lmiui/v5/widget/TabContainerLayout;->mTabViewStyle:Lmiui/v5/widget/TabContainerLayout$TabViewStyle;

    iget-object v7, p0, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->mTab:Landroid/app/ActionBar$Tab;

    iget-object v8, p0, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->mIconView:Landroid/widget/ImageView;

    invoke-interface {v6, v7, v8}, Lmiui/v5/widget/TabContainerLayout$TabViewStyle;->bindIconView(Landroid/app/ActionBar$Tab;Landroid/widget/ImageView;)V

    :cond_8d
    :goto_8d
    if-eqz v5, :cond_dc

    iget-object v6, p0, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->mTextView:Landroid/widget/TextView;

    if-nez v6, :cond_a3

    const v6, 0x6030042

    invoke-virtual {v3, v6, p0, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->mTextView:Landroid/widget/TextView;

    iget-object v6, p0, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->mTextView:Landroid/widget/TextView;

    invoke-virtual {p0, v6}, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->addView(Landroid/view/View;)V

    :cond_a3
    iget-object v6, p0, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v6, p0, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v6, v9}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v6, p0, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->this$0:Lmiui/v5/widget/TabContainerLayout;

    iget-object v6, v6, Lmiui/v5/widget/TabContainerLayout;->mTabViewStyle:Lmiui/v5/widget/TabContainerLayout$TabViewStyle;

    if-eqz v6, :cond_be

    iget-object v6, p0, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->this$0:Lmiui/v5/widget/TabContainerLayout;

    iget-object v6, v6, Lmiui/v5/widget/TabContainerLayout;->mTabViewStyle:Lmiui/v5/widget/TabContainerLayout$TabViewStyle;

    iget-object v7, p0, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->mTab:Landroid/app/ActionBar$Tab;

    iget-object v8, p0, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->mTextView:Landroid/widget/TextView;

    invoke-interface {v6, v7, v8}, Lmiui/v5/widget/TabContainerLayout$TabViewStyle;->bindTextView(Landroid/app/ActionBar$Tab;Landroid/widget/TextView;)V

    :cond_be
    :goto_be
    iget-object v6, p0, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->mIconView:Landroid/widget/ImageView;

    if-eqz v6, :cond_42

    iget-object v6, p0, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->mIconView:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/app/ActionBar$Tab;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_42

    :cond_cd
    iget-object v6, p0, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->mIconView:Landroid/widget/ImageView;

    if-eqz v6, :cond_8d

    iget-object v6, p0, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->mIconView:Landroid/widget/ImageView;

    invoke-virtual {v6, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v6, p0, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->mIconView:Landroid/widget/ImageView;

    invoke-virtual {v6, v11}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_8d

    :cond_dc
    iget-object v6, p0, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->mTextView:Landroid/widget/TextView;

    if-eqz v6, :cond_be

    iget-object v6, p0, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v6, v10}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v6, p0, Lmiui/v5/widget/TabContainerLayout$TabViewImpl;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v6, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_be
.end method
