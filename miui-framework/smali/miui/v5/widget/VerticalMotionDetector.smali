.class public Lmiui/v5/widget/VerticalMotionDetector;
.super Ljava/lang/Object;
.source "VerticalMotionDetector.java"


# static fields
.field static final INVALID_POINTER:I = -0x1

.field static final TAG:Ljava/lang/String;


# instance fields
.field private mActivePointerId:I

.field private mIsBeingDragged:Z

.field private mLastMotionX:I

.field private mLastMotionY:I

.field private mStartMotionX:I

.field private mStartMotionY:I

.field private mStrategy:Lmiui/v5/widget/MotionDetectListener;

.field private final mTouchSlop:I

.field private mVelocityTracker:Landroid/view/VelocityTracker;

.field private final mView:Landroid/view/ViewGroup;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    const-class v0, Lmiui/v5/widget/VerticalMotionDetector;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lmiui/v5/widget/VerticalMotionDetector;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup;)V
    .registers 6
    .parameter "view"

    .prologue
    const/4 v3, 0x0

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    const/4 v1, -0x1

    iput v1, p0, Lmiui/v5/widget/VerticalMotionDetector;->mActivePointerId:I

    iput v3, p0, Lmiui/v5/widget/VerticalMotionDetector;->mLastMotionY:I

    iput v3, p0, Lmiui/v5/widget/VerticalMotionDetector;->mLastMotionX:I

    iput v3, p0, Lmiui/v5/widget/VerticalMotionDetector;->mStartMotionY:I

    iput v3, p0, Lmiui/v5/widget/VerticalMotionDetector;->mStartMotionX:I

    iput-boolean v3, p0, Lmiui/v5/widget/VerticalMotionDetector;->mIsBeingDragged:Z

    const/4 v1, 0x0

    iput-object v1, p0, Lmiui/v5/widget/VerticalMotionDetector;->mStrategy:Lmiui/v5/widget/MotionDetectListener;

    iput-object p1, p0, Lmiui/v5/widget/VerticalMotionDetector;->mView:Landroid/view/ViewGroup;

    iget-object v1, p0, Lmiui/v5/widget/VerticalMotionDetector;->mView:Landroid/view/ViewGroup;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setFocusable(Z)V

    iget-object v1, p0, Lmiui/v5/widget/VerticalMotionDetector;->mView:Landroid/view/ViewGroup;

    const/high16 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setDescendantFocusability(I)V

    iget-object v1, p0, Lmiui/v5/widget/VerticalMotionDetector;->mView:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->setWillNotDraw(Z)V

    iget-object v1, p0, Lmiui/v5/widget/VerticalMotionDetector;->mView:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .local v0, configuration:Landroid/view/ViewConfiguration;
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Lmiui/v5/widget/VerticalMotionDetector;->mTouchSlop:I

    return-void
.end method

.method private cancelDragging(II)V
    .registers 5
    .parameter "x"
    .parameter "y"

    .prologue
    iget-boolean v0, p0, Lmiui/v5/widget/VerticalMotionDetector;->mIsBeingDragged:Z

    if-nez v0, :cond_5

    :cond_4
    :goto_4
    return-void

    :cond_5
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiui/v5/widget/VerticalMotionDetector;->mIsBeingDragged:Z

    iget-object v0, p0, Lmiui/v5/widget/VerticalMotionDetector;->mStrategy:Lmiui/v5/widget/MotionDetectListener;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lmiui/v5/widget/VerticalMotionDetector;->mStrategy:Lmiui/v5/widget/MotionDetectListener;

    iget-object v1, p0, Lmiui/v5/widget/VerticalMotionDetector;->mView:Landroid/view/ViewGroup;

    invoke-interface {v0, v1, p1, p2}, Lmiui/v5/widget/MotionDetectListener;->onMoveCancel(Landroid/view/View;II)V

    goto :goto_4
.end method

.method private clearVelocityTracker()V
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/VerticalMotionDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lmiui/v5/widget/VerticalMotionDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    :cond_9
    return-void
.end method

.method private finishDragging(IIZ)V
    .registers 11
    .parameter "x"
    .parameter "y"
    .parameter "hasTracker"

    .prologue
    iget-boolean v0, p0, Lmiui/v5/widget/VerticalMotionDetector;->mIsBeingDragged:Z

    if-nez v0, :cond_5

    :cond_4
    :goto_4
    return-void

    :cond_5
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiui/v5/widget/VerticalMotionDetector;->mIsBeingDragged:Z

    iget-object v0, p0, Lmiui/v5/widget/VerticalMotionDetector;->mStrategy:Lmiui/v5/widget/MotionDetectListener;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lmiui/v5/widget/VerticalMotionDetector;->mStrategy:Lmiui/v5/widget/MotionDetectListener;

    iget-object v1, p0, Lmiui/v5/widget/VerticalMotionDetector;->mView:Landroid/view/ViewGroup;

    iget v4, p0, Lmiui/v5/widget/VerticalMotionDetector;->mStartMotionX:I

    iget v5, p0, Lmiui/v5/widget/VerticalMotionDetector;->mStartMotionY:I

    if-eqz p3, :cond_1e

    iget-object v6, p0, Lmiui/v5/widget/VerticalMotionDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    :goto_18
    move v2, p1

    move v3, p2

    invoke-interface/range {v0 .. v6}, Lmiui/v5/widget/MotionDetectListener;->onMoveFinish(Landroid/view/View;IIIILandroid/view/VelocityTracker;)V

    goto :goto_4

    :cond_1e
    const/4 v6, 0x0

    goto :goto_18
.end method

.method private initOrResetVelocityTracker()V
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/VerticalMotionDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-nez v0, :cond_b

    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lmiui/v5/widget/VerticalMotionDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    :goto_a
    return-void

    :cond_b
    iget-object v0, p0, Lmiui/v5/widget/VerticalMotionDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    goto :goto_a
.end method

.method private initVelocityTrackerIfNotExists()V
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/VerticalMotionDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-nez v0, :cond_a

    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lmiui/v5/widget/VerticalMotionDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    :cond_a
    return-void
.end method

.method private onSecondaryPointerDown(Landroid/view/MotionEvent;)V
    .registers 7
    .parameter "ev"

    .prologue
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v1

    .local v1, index:I
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    .local v0, id:I
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v4

    float-to-int v3, v4

    .local v3, y:I
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v4

    float-to-int v2, v4

    .local v2, x:I
    iput v0, p0, Lmiui/v5/widget/VerticalMotionDetector;->mActivePointerId:I

    iput v3, p0, Lmiui/v5/widget/VerticalMotionDetector;->mStartMotionY:I

    iput v2, p0, Lmiui/v5/widget/VerticalMotionDetector;->mStartMotionX:I

    iput v3, p0, Lmiui/v5/widget/VerticalMotionDetector;->mLastMotionY:I

    iput v2, p0, Lmiui/v5/widget/VerticalMotionDetector;->mLastMotionX:I

    invoke-direct {p0}, Lmiui/v5/widget/VerticalMotionDetector;->clearVelocityTracker()V

    return-void
.end method

.method private onSecondaryPointerUp(Landroid/view/MotionEvent;)V
    .registers 9
    .parameter "ev"

    .prologue
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    const v6, 0xff00

    and-int/2addr v5, v6

    shr-int/lit8 v2, v5, 0x8

    .local v2, pointerIndex:I
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    .local v1, pointerId:I
    iget v5, p0, Lmiui/v5/widget/VerticalMotionDetector;->mActivePointerId:I

    if-ne v1, v5, :cond_2d

    if-nez v2, :cond_2e

    const/4 v0, 0x1

    .local v0, newPointerIndex:I
    :goto_15
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v5

    float-to-int v4, v5

    .local v4, y:I
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v5

    float-to-int v3, v5

    .local v3, x:I
    iput v4, p0, Lmiui/v5/widget/VerticalMotionDetector;->mStartMotionY:I

    iput v3, p0, Lmiui/v5/widget/VerticalMotionDetector;->mStartMotionX:I

    iput v4, p0, Lmiui/v5/widget/VerticalMotionDetector;->mLastMotionY:I

    iput v3, p0, Lmiui/v5/widget/VerticalMotionDetector;->mLastMotionX:I

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v5

    iput v5, p0, Lmiui/v5/widget/VerticalMotionDetector;->mActivePointerId:I

    .end local v0           #newPointerIndex:I
    .end local v3           #x:I
    .end local v4           #y:I
    :cond_2d
    return-void

    :cond_2e
    const/4 v0, 0x0

    goto :goto_15
.end method

.method private recycleVelocityTracker()V
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/VerticalMotionDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lmiui/v5/widget/VerticalMotionDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmiui/v5/widget/VerticalMotionDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    :cond_c
    return-void
.end method

.method private startDragging(II)V
    .registers 5
    .parameter "x"
    .parameter "y"

    .prologue
    iput p2, p0, Lmiui/v5/widget/VerticalMotionDetector;->mStartMotionY:I

    iput p1, p0, Lmiui/v5/widget/VerticalMotionDetector;->mStartMotionX:I

    iget-boolean v0, p0, Lmiui/v5/widget/VerticalMotionDetector;->mIsBeingDragged:Z

    if-eqz v0, :cond_9

    :cond_8
    :goto_8
    return-void

    :cond_9
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiui/v5/widget/VerticalMotionDetector;->mIsBeingDragged:Z

    iget-object v0, p0, Lmiui/v5/widget/VerticalMotionDetector;->mStrategy:Lmiui/v5/widget/MotionDetectListener;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lmiui/v5/widget/VerticalMotionDetector;->mStrategy:Lmiui/v5/widget/MotionDetectListener;

    iget-object v1, p0, Lmiui/v5/widget/VerticalMotionDetector;->mView:Landroid/view/ViewGroup;

    invoke-interface {v0, v1, p1, p2}, Lmiui/v5/widget/MotionDetectListener;->onMoveStart(Landroid/view/View;II)V

    goto :goto_8
.end method

.method private trackMovement(Landroid/view/MotionEvent;)V
    .registers 6
    .parameter "event"

    .prologue
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    sub-float v0, v2, v3

    .local v0, deltaX:F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    sub-float v1, v2, v3

    .local v1, deltaY:F
    invoke-virtual {p1, v0, v1}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    iget-object v2, p0, Lmiui/v5/widget/VerticalMotionDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v2, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    neg-float v2, v0

    neg-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    return-void
.end method


# virtual methods
.method public getMotionStrategy()Lmiui/v5/widget/MotionDetectListener;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/VerticalMotionDetector;->mStrategy:Lmiui/v5/widget/MotionDetectListener;

    return-object v0
.end method

.method public isBeingDragged()Z
    .registers 2

    .prologue
    iget-boolean v0, p0, Lmiui/v5/widget/VerticalMotionDetector;->mIsBeingDragged:Z

    return v0
.end method

.method public isMovable(II)Z
    .registers 9
    .parameter "x"
    .parameter "y"

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/VerticalMotionDetector;->mStrategy:Lmiui/v5/widget/MotionDetectListener;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lmiui/v5/widget/VerticalMotionDetector;->mStrategy:Lmiui/v5/widget/MotionDetectListener;

    iget-object v1, p0, Lmiui/v5/widget/VerticalMotionDetector;->mView:Landroid/view/ViewGroup;

    iget v4, p0, Lmiui/v5/widget/VerticalMotionDetector;->mStartMotionX:I

    iget v5, p0, Lmiui/v5/widget/VerticalMotionDetector;->mStartMotionY:I

    move v2, p1

    move v3, p2

    invoke-interface/range {v0 .. v5}, Lmiui/v5/widget/MotionDetectListener;->isMovable(Landroid/view/View;IIII)Z

    move-result v0

    :goto_12
    return v0

    :cond_13
    const/4 v0, 0x1

    goto :goto_12
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 15
    .parameter "ev"

    .prologue
    const/4 v5, -0x1

    const/4 v12, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lmiui/v5/widget/VerticalMotionDetector;->mStrategy:Lmiui/v5/widget/MotionDetectListener;

    .local v0, strategy:Lmiui/v5/widget/MotionDetectListener;
    if-nez v0, :cond_8

    :goto_7
    return v1

    :cond_8
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    .local v6, action:I
    const/4 v4, 0x2

    if-ne v6, v4, :cond_15

    iget-boolean v4, p0, Lmiui/v5/widget/VerticalMotionDetector;->mIsBeingDragged:Z

    if-eqz v4, :cond_15

    move v1, v12

    goto :goto_7

    :cond_15
    and-int/lit16 v4, v6, 0xff

    packed-switch v4, :pswitch_data_d6

    :cond_1a
    :goto_1a
    :pswitch_1a
    iget-boolean v1, p0, Lmiui/v5/widget/VerticalMotionDetector;->mIsBeingDragged:Z

    goto :goto_7

    :pswitch_1d
    iget v7, p0, Lmiui/v5/widget/VerticalMotionDetector;->mActivePointerId:I

    .local v7, activePointerId:I
    if-eq v7, v5, :cond_1a

    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v9

    .local v9, pointerIndex:I
    invoke-virtual {p1, v9}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    float-to-int v3, v1

    .local v3, y:I
    invoke-virtual {p1, v9}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    float-to-int v2, v1

    .local v2, x:I
    iget-object v1, p0, Lmiui/v5/widget/VerticalMotionDetector;->mView:Landroid/view/ViewGroup;

    iget v4, p0, Lmiui/v5/widget/VerticalMotionDetector;->mLastMotionX:I

    iget v5, p0, Lmiui/v5/widget/VerticalMotionDetector;->mLastMotionY:I

    invoke-interface/range {v0 .. v5}, Lmiui/v5/widget/MotionDetectListener;->isMovable(Landroid/view/View;IIII)Z

    move-result v1

    if-nez v1, :cond_3f

    invoke-direct {p0}, Lmiui/v5/widget/VerticalMotionDetector;->clearVelocityTracker()V

    goto :goto_1a

    :cond_3f
    iget v1, p0, Lmiui/v5/widget/VerticalMotionDetector;->mLastMotionY:I

    sub-int v1, v3, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v11

    .local v11, yDiff:I
    iget v1, p0, Lmiui/v5/widget/VerticalMotionDetector;->mLastMotionX:I

    sub-int v1, v2, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v10

    .local v10, xDiff:I
    iget v1, p0, Lmiui/v5/widget/VerticalMotionDetector;->mTouchSlop:I

    if-le v11, v1, :cond_1a

    if-ge v10, v11, :cond_1a

    invoke-direct {p0}, Lmiui/v5/widget/VerticalMotionDetector;->initVelocityTrackerIfNotExists()V

    invoke-direct {p0, p1}, Lmiui/v5/widget/VerticalMotionDetector;->trackMovement(Landroid/view/MotionEvent;)V

    iget v1, p0, Lmiui/v5/widget/VerticalMotionDetector;->mLastMotionX:I

    iget v4, p0, Lmiui/v5/widget/VerticalMotionDetector;->mLastMotionY:I

    invoke-direct {p0, v1, v4}, Lmiui/v5/widget/VerticalMotionDetector;->startDragging(II)V

    iput v3, p0, Lmiui/v5/widget/VerticalMotionDetector;->mLastMotionY:I

    iput v2, p0, Lmiui/v5/widget/VerticalMotionDetector;->mLastMotionX:I

    iget-object v1, p0, Lmiui/v5/widget/VerticalMotionDetector;->mView:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v8

    .local v8, parent:Landroid/view/ViewParent;
    if-eqz v8, :cond_1a

    invoke-interface {v8, v12}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_1a

    .end local v2           #x:I
    .end local v3           #y:I
    .end local v7           #activePointerId:I
    .end local v8           #parent:Landroid/view/ViewParent;
    .end local v9           #pointerIndex:I
    .end local v10           #xDiff:I
    .end local v11           #yDiff:I
    :pswitch_72
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v3, v4

    .restart local v3       #y:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v2, v4

    .restart local v2       #x:I
    iput v3, p0, Lmiui/v5/widget/VerticalMotionDetector;->mLastMotionY:I

    iput v2, p0, Lmiui/v5/widget/VerticalMotionDetector;->mLastMotionX:I

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    iput v1, p0, Lmiui/v5/widget/VerticalMotionDetector;->mActivePointerId:I

    invoke-direct {p0}, Lmiui/v5/widget/VerticalMotionDetector;->initOrResetVelocityTracker()V

    invoke-direct {p0, p1}, Lmiui/v5/widget/VerticalMotionDetector;->trackMovement(Landroid/view/MotionEvent;)V

    iget-object v1, p0, Lmiui/v5/widget/VerticalMotionDetector;->mView:Landroid/view/ViewGroup;

    invoke-interface {v0, v1, v2, v3}, Lmiui/v5/widget/MotionDetectListener;->moveImmediately(Landroid/view/View;II)Z

    move-result v1

    if-eqz v1, :cond_98

    invoke-direct {p0, v2, v3}, Lmiui/v5/widget/VerticalMotionDetector;->startDragging(II)V

    goto :goto_1a

    :cond_98
    invoke-direct {p0, v2, v3}, Lmiui/v5/widget/VerticalMotionDetector;->cancelDragging(II)V

    goto/16 :goto_1a

    .end local v2           #x:I
    .end local v3           #y:I
    :pswitch_9d
    iget v4, p0, Lmiui/v5/widget/VerticalMotionDetector;->mActivePointerId:I

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v9

    .restart local v9       #pointerIndex:I
    invoke-virtual {p1, v9}, Landroid/view/MotionEvent;->getY(I)F

    move-result v4

    float-to-int v3, v4

    .restart local v3       #y:I
    invoke-virtual {p1, v9}, Landroid/view/MotionEvent;->getX(I)F

    move-result v4

    float-to-int v2, v4

    .restart local v2       #x:I
    if-ne v6, v12, :cond_b9

    invoke-direct {p0, v2, v3, v1}, Lmiui/v5/widget/VerticalMotionDetector;->finishDragging(IIZ)V

    :goto_b2
    iput v5, p0, Lmiui/v5/widget/VerticalMotionDetector;->mActivePointerId:I

    invoke-direct {p0}, Lmiui/v5/widget/VerticalMotionDetector;->recycleVelocityTracker()V

    goto/16 :goto_1a

    :cond_b9
    invoke-direct {p0, v2, v3}, Lmiui/v5/widget/VerticalMotionDetector;->cancelDragging(II)V

    goto :goto_b2

    .end local v2           #x:I
    .end local v3           #y:I
    .end local v9           #pointerIndex:I
    :pswitch_bd
    invoke-direct {p0, p1}, Lmiui/v5/widget/VerticalMotionDetector;->onSecondaryPointerUp(Landroid/view/MotionEvent;)V

    iget v1, p0, Lmiui/v5/widget/VerticalMotionDetector;->mActivePointerId:I

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v9

    .restart local v9       #pointerIndex:I
    invoke-virtual {p1, v9}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lmiui/v5/widget/VerticalMotionDetector;->mLastMotionY:I

    invoke-virtual {p1, v9}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lmiui/v5/widget/VerticalMotionDetector;->mLastMotionX:I

    goto/16 :goto_1a

    :pswitch_data_d6
    .packed-switch 0x0
        :pswitch_72
        :pswitch_9d
        :pswitch_1d
        :pswitch_9d
        :pswitch_1a
        :pswitch_1a
        :pswitch_bd
    .end packed-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 16
    .parameter "ev"

    .prologue
    const/4 v1, 0x0

    const/4 v4, -0x1

    const/4 v13, 0x1

    iget-object v0, p0, Lmiui/v5/widget/VerticalMotionDetector;->mStrategy:Lmiui/v5/widget/MotionDetectListener;

    .local v0, strategy:Lmiui/v5/widget/MotionDetectListener;
    if-nez v0, :cond_8

    :goto_7
    return v1

    :cond_8
    invoke-direct {p0}, Lmiui/v5/widget/VerticalMotionDetector;->initVelocityTrackerIfNotExists()V

    invoke-direct {p0, p1}, Lmiui/v5/widget/VerticalMotionDetector;->trackMovement(Landroid/view/MotionEvent;)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    .local v6, action:I
    and-int/lit16 v3, v6, 0xff

    packed-switch v3, :pswitch_data_e0

    :cond_17
    :goto_17
    :pswitch_17
    move v1, v13

    goto :goto_7

    :pswitch_19
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v12, v3

    .local v12, y:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v2, v3

    .local v2, x:I
    iput v12, p0, Lmiui/v5/widget/VerticalMotionDetector;->mLastMotionY:I

    iput v2, p0, Lmiui/v5/widget/VerticalMotionDetector;->mLastMotionX:I

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    iput v1, p0, Lmiui/v5/widget/VerticalMotionDetector;->mActivePointerId:I

    iget-object v1, p0, Lmiui/v5/widget/VerticalMotionDetector;->mView:Landroid/view/ViewGroup;

    invoke-interface {v0, v1, v2, v12}, Lmiui/v5/widget/MotionDetectListener;->moveImmediately(Landroid/view/View;II)Z

    move-result v1

    if-eqz v1, :cond_44

    invoke-direct {p0, v2, v12}, Lmiui/v5/widget/VerticalMotionDetector;->startDragging(II)V

    iget-object v1, p0, Lmiui/v5/widget/VerticalMotionDetector;->mView:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v10

    .local v10, parent:Landroid/view/ViewParent;
    if-eqz v10, :cond_17

    invoke-interface {v10, v13}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_17

    .end local v10           #parent:Landroid/view/ViewParent;
    :cond_44
    invoke-direct {p0, v2, v12}, Lmiui/v5/widget/VerticalMotionDetector;->cancelDragging(II)V

    goto :goto_17

    .end local v2           #x:I
    .end local v12           #y:I
    :pswitch_48
    iget v1, p0, Lmiui/v5/widget/VerticalMotionDetector;->mActivePointerId:I

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v7

    .local v7, activePointerIndex:I
    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    float-to-int v12, v1

    .restart local v12       #y:I
    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    float-to-int v2, v1

    .restart local v2       #x:I
    iget v1, p0, Lmiui/v5/widget/VerticalMotionDetector;->mLastMotionY:I

    sub-int v9, v1, v12

    .local v9, deltaY:I
    const/4 v8, 0x0

    .local v8, adjust:I
    iget-boolean v1, p0, Lmiui/v5/widget/VerticalMotionDetector;->mIsBeingDragged:Z

    if-nez v1, :cond_7c

    invoke-static {v9}, Ljava/lang/Math;->abs(I)I

    move-result v1

    iget v3, p0, Lmiui/v5/widget/VerticalMotionDetector;->mTouchSlop:I

    if-le v1, v3, :cond_7c

    invoke-direct {p0, v2, v12}, Lmiui/v5/widget/VerticalMotionDetector;->startDragging(II)V

    iget-object v1, p0, Lmiui/v5/widget/VerticalMotionDetector;->mView:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v10

    .restart local v10       #parent:Landroid/view/ViewParent;
    if-eqz v10, :cond_77

    invoke-interface {v10, v13}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    :cond_77
    if-lez v9, :cond_96

    iget v1, p0, Lmiui/v5/widget/VerticalMotionDetector;->mTouchSlop:I

    neg-int v8, v1

    .end local v10           #parent:Landroid/view/ViewParent;
    :cond_7c
    :goto_7c
    iget-boolean v1, p0, Lmiui/v5/widget/VerticalMotionDetector;->mIsBeingDragged:Z

    if-eqz v1, :cond_17

    iget-object v1, p0, Lmiui/v5/widget/VerticalMotionDetector;->mView:Landroid/view/ViewGroup;

    add-int v3, v12, v8

    iget v4, p0, Lmiui/v5/widget/VerticalMotionDetector;->mStartMotionX:I

    iget v5, p0, Lmiui/v5/widget/VerticalMotionDetector;->mStartMotionY:I

    invoke-interface/range {v0 .. v5}, Lmiui/v5/widget/MotionDetectListener;->onMove(Landroid/view/View;IIII)Z

    move-result v1

    if-nez v1, :cond_91

    invoke-direct {p0}, Lmiui/v5/widget/VerticalMotionDetector;->clearVelocityTracker()V

    :cond_91
    iput v12, p0, Lmiui/v5/widget/VerticalMotionDetector;->mLastMotionY:I

    iput v2, p0, Lmiui/v5/widget/VerticalMotionDetector;->mLastMotionX:I

    goto :goto_17

    .restart local v10       #parent:Landroid/view/ViewParent;
    :cond_96
    iget v8, p0, Lmiui/v5/widget/VerticalMotionDetector;->mTouchSlop:I

    goto :goto_7c

    .end local v2           #x:I
    .end local v7           #activePointerIndex:I
    .end local v8           #adjust:I
    .end local v9           #deltaY:I
    .end local v10           #parent:Landroid/view/ViewParent;
    .end local v12           #y:I
    :pswitch_99
    iget-boolean v1, p0, Lmiui/v5/widget/VerticalMotionDetector;->mIsBeingDragged:Z

    if-eqz v1, :cond_17

    iget v1, p0, Lmiui/v5/widget/VerticalMotionDetector;->mActivePointerId:I

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v11

    .local v11, pointerIndex:I
    invoke-virtual {p1, v11}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    float-to-int v12, v1

    .restart local v12       #y:I
    invoke-virtual {p1, v11}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    float-to-int v2, v1

    .restart local v2       #x:I
    invoke-direct {p0, v2, v12, v13}, Lmiui/v5/widget/VerticalMotionDetector;->finishDragging(IIZ)V

    iput v4, p0, Lmiui/v5/widget/VerticalMotionDetector;->mActivePointerId:I

    invoke-direct {p0}, Lmiui/v5/widget/VerticalMotionDetector;->recycleVelocityTracker()V

    goto/16 :goto_17

    .end local v2           #x:I
    .end local v11           #pointerIndex:I
    .end local v12           #y:I
    :pswitch_b7
    iget-boolean v1, p0, Lmiui/v5/widget/VerticalMotionDetector;->mIsBeingDragged:Z

    if-eqz v1, :cond_17

    iget v1, p0, Lmiui/v5/widget/VerticalMotionDetector;->mActivePointerId:I

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v11

    .restart local v11       #pointerIndex:I
    invoke-virtual {p1, v11}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    float-to-int v12, v1

    .restart local v12       #y:I
    invoke-virtual {p1, v11}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    float-to-int v2, v1

    .restart local v2       #x:I
    invoke-direct {p0, v2, v12}, Lmiui/v5/widget/VerticalMotionDetector;->cancelDragging(II)V

    iput v4, p0, Lmiui/v5/widget/VerticalMotionDetector;->mActivePointerId:I

    invoke-direct {p0}, Lmiui/v5/widget/VerticalMotionDetector;->recycleVelocityTracker()V

    goto/16 :goto_17

    .end local v2           #x:I
    .end local v11           #pointerIndex:I
    .end local v12           #y:I
    :pswitch_d5
    invoke-direct {p0, p1}, Lmiui/v5/widget/VerticalMotionDetector;->onSecondaryPointerUp(Landroid/view/MotionEvent;)V

    goto/16 :goto_17

    :pswitch_da
    invoke-direct {p0, p1}, Lmiui/v5/widget/VerticalMotionDetector;->onSecondaryPointerDown(Landroid/view/MotionEvent;)V

    goto/16 :goto_17

    nop

    :pswitch_data_e0
    .packed-switch 0x0
        :pswitch_19
        :pswitch_99
        :pswitch_48
        :pswitch_b7
        :pswitch_17
        :pswitch_da
        :pswitch_d5
    .end packed-switch
.end method

.method public setMotionStrategy(Lmiui/v5/widget/MotionDetectListener;)V
    .registers 2
    .parameter "strategy"

    .prologue
    iput-object p1, p0, Lmiui/v5/widget/VerticalMotionDetector;->mStrategy:Lmiui/v5/widget/MotionDetectListener;

    return-void
.end method
