.class public abstract Lmiui/v5/widget/PageScrollEffect$AbsPageScrollEffect;
.super Ljava/lang/Object;
.source "PageScrollEffect.java"

# interfaces
.implements Lmiui/v5/widget/PageScrollEffect;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/v5/widget/PageScrollEffect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "AbsPageScrollEffect"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/v5/widget/PageScrollEffect$AbsPageScrollEffect$AllVisiblePred;,
        Lmiui/v5/widget/PageScrollEffect$AbsPageScrollEffect$AnyVisiblePred;
    }
.end annotation


# static fields
.field public static final EFFECT_ALL_CHILDREN:I = 0x3

.field public static final EFFECT_ANY_CHILDREN:I = 0x2

.field public static final EFFECT_NONE:I = 0x0

.field public static final EFFECT_ONLY_SELF:I = 0x1

.field protected static final KEY_EFFECT_TYPE:Ljava/lang/Integer;


# instance fields
.field private mAttached:Z

.field private final mResIds:[I

.field protected final mView:Landroid/view/ViewGroup;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    const v0, 0x60b008b

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lmiui/v5/widget/PageScrollEffect$AbsPageScrollEffect;->KEY_EFFECT_TYPE:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup;[I)V
    .registers 4
    .parameter "v"
    .parameter "resIds"

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiui/v5/widget/PageScrollEffect$AbsPageScrollEffect;->mAttached:Z

    iput-object p1, p0, Lmiui/v5/widget/PageScrollEffect$AbsPageScrollEffect;->mView:Landroid/view/ViewGroup;

    iput-object p2, p0, Lmiui/v5/widget/PageScrollEffect$AbsPageScrollEffect;->mResIds:[I

    return-void
.end method

.method protected static chainEffectTypes(Landroid/view/ViewGroup;[I)Z
    .registers 13
    .parameter "group"
    .parameter "resIds"

    .prologue
    const/4 v10, 0x1

    if-eqz p0, :cond_5

    if-nez p1, :cond_7

    :cond_5
    const/4 v2, 0x0

    :cond_6
    :goto_6
    return v2

    :cond_7
    const/4 v2, 0x1

    .local v2, chainAll:Z
    move-object v0, p1

    .local v0, arr$:[I
    array-length v5, v0

    .local v5, len$:I
    const/4 v3, 0x0

    .local v3, i$:I
    :goto_b
    if-ge v3, v5, :cond_6

    aget v4, v0, v3

    .local v4, id:I
    invoke-virtual {p0, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .local v1, c:Landroid/view/View;
    if-nez v1, :cond_17

    const/4 v2, 0x0

    goto :goto_6

    :cond_17
    if-ne v1, p0, :cond_1c

    :cond_19
    add-int/lit8 v3, v3, 0x1

    goto :goto_b

    :cond_1c
    instance-of v8, v1, Landroid/widget/AbsListView;

    if-eqz v8, :cond_36

    const/4 v8, 0x3

    invoke-static {v1, v8}, Lmiui/v5/widget/PageScrollEffect$AbsPageScrollEffect;->setViewEffectType(Landroid/view/View;I)V

    :cond_24
    :goto_24
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v6

    .local v6, p:Landroid/view/ViewParent;
    :goto_28
    if-eq v6, p0, :cond_19

    move-object v8, v6

    check-cast v8, Landroid/view/View;

    const/4 v9, 0x2

    invoke-static {v8, v9}, Lmiui/v5/widget/PageScrollEffect$AbsPageScrollEffect;->setViewEffectType(Landroid/view/View;I)V

    invoke-interface {v6}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v6

    goto :goto_28

    .end local v6           #p:Landroid/view/ViewParent;
    :cond_36
    invoke-static {v1}, Lmiui/v5/widget/PageScrollEffect$AbsPageScrollEffect;->getEffectType(Landroid/view/View;)I

    move-result v7

    .local v7, type:I
    if-ge v7, v10, :cond_24

    invoke-static {v1, v10}, Lmiui/v5/widget/PageScrollEffect$AbsPageScrollEffect;->setViewEffectType(Landroid/view/View;I)V

    goto :goto_24
.end method

.method protected static clearChain(Landroid/view/ViewGroup;)V
    .registers 6
    .parameter "group"

    .prologue
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .local v0, count:I
    const/4 v1, 0x0

    .local v1, i:I
    :goto_5
    if-ge v1, v0, :cond_21

    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .local v2, v:Landroid/view/View;
    sget-object v3, Lmiui/v5/widget/PageScrollEffect$AbsPageScrollEffect;->KEY_EFFECT_TYPE:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    instance-of v3, v2, Landroid/view/ViewGroup;

    if-eqz v3, :cond_1e

    check-cast v2, Landroid/view/ViewGroup;

    .end local v2           #v:Landroid/view/View;
    invoke-static {v2}, Lmiui/v5/widget/PageScrollEffect$AbsPageScrollEffect;->clearChain(Landroid/view/ViewGroup;)V

    :cond_1e
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_21
    return-void
.end method

.method protected static existsEffect(Landroid/view/View;)Z
    .registers 4
    .parameter "view"

    .prologue
    const/4 v1, 0x0

    sget-object v2, Lmiui/v5/widget/PageScrollEffect$AbsPageScrollEffect;->KEY_EFFECT_TYPE:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    .local v0, tag:Ljava/lang/Object;
    if-eqz v0, :cond_16

    check-cast v0, Ljava/lang/Integer;

    .end local v0           #tag:Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-lez v2, :cond_16

    const/4 v1, 0x1

    :cond_16
    return v1
.end method

.method protected static getEffectType(Landroid/view/View;)I
    .registers 3
    .parameter "view"

    .prologue
    sget-object v1, Lmiui/v5/widget/PageScrollEffect$AbsPageScrollEffect;->KEY_EFFECT_TYPE:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    .local v0, tag:Ljava/lang/Object;
    if-eqz v0, :cond_13

    check-cast v0, Ljava/lang/Integer;

    .end local v0           #tag:Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :goto_12
    return v1

    .restart local v0       #tag:Ljava/lang/Object;
    :cond_13
    const/4 v1, 0x0

    goto :goto_12
.end method

.method protected static setViewEffectType(Landroid/view/View;I)V
    .registers 4
    .parameter "view"
    .parameter "type"

    .prologue
    sget-object v0, Lmiui/v5/widget/PageScrollEffect$AbsPageScrollEffect;->KEY_EFFECT_TYPE:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public attach()Z
    .registers 5

    .prologue
    const/4 v1, 0x1

    iget-boolean v2, p0, Lmiui/v5/widget/PageScrollEffect$AbsPageScrollEffect;->mAttached:Z

    if-eqz v2, :cond_7

    move v0, v1

    :cond_6
    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    .local v0, success:Z
    iget-object v2, p0, Lmiui/v5/widget/PageScrollEffect$AbsPageScrollEffect;->mResIds:[I

    if-eqz v2, :cond_22

    iget-object v2, p0, Lmiui/v5/widget/PageScrollEffect$AbsPageScrollEffect;->mView:Landroid/view/ViewGroup;

    iget-object v3, p0, Lmiui/v5/widget/PageScrollEffect$AbsPageScrollEffect;->mResIds:[I

    invoke-static {v2, v3}, Lmiui/v5/widget/PageScrollEffect$AbsPageScrollEffect;->chainEffectTypes(Landroid/view/ViewGroup;[I)Z

    move-result v2

    if-eqz v2, :cond_1d

    iget-object v2, p0, Lmiui/v5/widget/PageScrollEffect$AbsPageScrollEffect;->mView:Landroid/view/ViewGroup;

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lmiui/v5/widget/PageScrollEffect$AbsPageScrollEffect;->setViewEffectType(Landroid/view/View;I)V

    const/4 v0, 0x1

    :cond_1d
    :goto_1d
    if-eqz v0, :cond_6

    iput-boolean v1, p0, Lmiui/v5/widget/PageScrollEffect$AbsPageScrollEffect;->mAttached:Z

    goto :goto_6

    :cond_22
    iget-object v2, p0, Lmiui/v5/widget/PageScrollEffect$AbsPageScrollEffect;->mView:Landroid/view/ViewGroup;

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lmiui/v5/widget/PageScrollEffect$AbsPageScrollEffect;->setViewEffectType(Landroid/view/View;I)V

    const/4 v0, 0x1

    goto :goto_1d
.end method

.method public detach()Z
    .registers 4

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/PageScrollEffect$AbsPageScrollEffect;->mView:Landroid/view/ViewGroup;

    invoke-static {v0}, Lmiui/v5/widget/PageScrollEffect$AbsPageScrollEffect;->clearChain(Landroid/view/ViewGroup;)V

    iget-object v0, p0, Lmiui/v5/widget/PageScrollEffect$AbsPageScrollEffect;->mView:Landroid/view/ViewGroup;

    sget-object v1, Lmiui/v5/widget/PageScrollEffect$AbsPageScrollEffect;->KEY_EFFECT_TYPE:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->setTag(ILjava/lang/Object;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiui/v5/widget/PageScrollEffect$AbsPageScrollEffect;->mAttached:Z

    const/4 v0, 0x1

    return v0
.end method

.method public isAttached()Z
    .registers 2

    .prologue
    iget-boolean v0, p0, Lmiui/v5/widget/PageScrollEffect$AbsPageScrollEffect;->mAttached:Z

    return v0
.end method
