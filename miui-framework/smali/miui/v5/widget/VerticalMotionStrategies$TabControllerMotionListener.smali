.class Lmiui/v5/widget/VerticalMotionStrategies$TabControllerMotionListener;
.super Ljava/lang/Object;
.source "VerticalMotionStrategies.java"

# interfaces
.implements Lmiui/v5/widget/MotionDetectStrategy;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/v5/widget/VerticalMotionStrategies;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TabControllerMotionListener"
.end annotation


# instance fields
.field private final mMinY:I

.field private final mPagerStrategyFactory:Lmiui/v5/util/Factory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lmiui/v5/util/Factory",
            "<",
            "Lmiui/v5/widget/MotionDetectStrategy;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mTabController:Lmiui/v5/widget/TabController;


# direct methods
.method public constructor <init>(Lmiui/v5/widget/TabController;ILmiui/v5/util/Factory;)V
    .registers 4
    .parameter "controller"
    .parameter "minY"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiui/v5/widget/TabController;",
            "I",
            "Lmiui/v5/util/Factory",
            "<",
            "Lmiui/v5/widget/MotionDetectStrategy;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p3, pagerStrategyFactory:Lmiui/v5/util/Factory;,"Lmiui/v5/util/Factory<Lmiui/v5/widget/MotionDetectStrategy;Ljava/lang/Integer;>;"
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmiui/v5/widget/VerticalMotionStrategies$TabControllerMotionListener;->mTabController:Lmiui/v5/widget/TabController;

    iput p2, p0, Lmiui/v5/widget/VerticalMotionStrategies$TabControllerMotionListener;->mMinY:I

    iput-object p3, p0, Lmiui/v5/widget/VerticalMotionStrategies$TabControllerMotionListener;->mPagerStrategyFactory:Lmiui/v5/util/Factory;

    return-void
.end method


# virtual methods
.method public isMovable(Landroid/view/View;IIII)Z
    .registers 14
    .parameter "view"
    .parameter "x"
    .parameter "y"
    .parameter "startX"
    .parameter "startY"

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v3, p0, Lmiui/v5/widget/VerticalMotionStrategies$TabControllerMotionListener;->mTabController:Lmiui/v5/widget/TabController;

    invoke-virtual {v3}, Lmiui/v5/widget/TabController;->getViewPagerState()I

    move-result v3

    if-eqz v3, :cond_c

    move v1, v2

    :cond_b
    :goto_b
    return v1

    :cond_c
    iget-object v3, p0, Lmiui/v5/widget/VerticalMotionStrategies$TabControllerMotionListener;->mTabController:Lmiui/v5/widget/TabController;

    invoke-virtual {v3}, Lmiui/v5/widget/TabController;->getTabContainer()Lmiui/v5/widget/TabContainerLayout;

    move-result-object v7

    .local v7, tabs:Landroid/view/View;
    invoke-static {v7, p4, p5}, Lmiui/v5/widget/Views;->isContainPoint(Landroid/view/View;II)Z

    move-result v3

    if-nez v3, :cond_b

    if-ge p3, p5, :cond_27

    invoke-virtual {p1}, Landroid/view/View;->getY()F

    move-result v3

    iget v4, p0, Lmiui/v5/widget/VerticalMotionStrategies$TabControllerMotionListener;->mMinY:I

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-gtz v3, :cond_b

    move v1, v2

    goto :goto_b

    :cond_27
    iget-object v2, p0, Lmiui/v5/widget/VerticalMotionStrategies$TabControllerMotionListener;->mPagerStrategyFactory:Lmiui/v5/util/Factory;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lmiui/v5/widget/VerticalMotionStrategies$TabControllerMotionListener;->mTabController:Lmiui/v5/widget/TabController;

    invoke-virtual {v2}, Lmiui/v5/widget/TabController;->getViewPager()Landroid/support/v4/view/ViewPager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v6

    .local v6, index:I
    iget-object v2, p0, Lmiui/v5/widget/VerticalMotionStrategies$TabControllerMotionListener;->mPagerStrategyFactory:Lmiui/v5/util/Factory;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Lmiui/v5/util/Factory;->create(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/v5/widget/MotionDetectStrategy;

    .local v0, strategy:Lmiui/v5/widget/MotionDetectStrategy;
    if-eqz v0, :cond_b

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lmiui/v5/widget/MotionDetectStrategy;->isMovable(Landroid/view/View;IIII)Z

    move-result v1

    goto :goto_b
.end method
