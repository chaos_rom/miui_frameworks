.class public interface abstract Lmiui/v5/widget/PageScrollEffect;
.super Ljava/lang/Object;
.source "PageScrollEffect.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/v5/widget/PageScrollEffect$AbsPageScrollEffect;,
        Lmiui/v5/widget/PageScrollEffect$Creator;
    }
.end annotation


# virtual methods
.method public abstract attach()Z
.end method

.method public abstract detach()Z
.end method

.method public abstract isAttached()Z
.end method

.method public abstract scroll(FZ)V
.end method
