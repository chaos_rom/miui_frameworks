.class Lmiui/v5/widget/TabController$2;
.super Ljava/lang/Object;
.source "TabController.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/v5/widget/TabController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/v5/widget/TabController;


# direct methods
.method constructor <init>(Lmiui/v5/widget/TabController;)V
    .registers 2
    .parameter

    .prologue
    iput-object p1, p0, Lmiui/v5/widget/TabController$2;->this$0:Lmiui/v5/widget/TabController;

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .registers 3
    .parameter "state"

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/TabController$2;->this$0:Lmiui/v5/widget/TabController;

    iput p1, v0, Lmiui/v5/widget/TabController;->mViewPagerState:I

    iget-object v0, p0, Lmiui/v5/widget/TabController$2;->this$0:Lmiui/v5/widget/TabController;

    iget-object v0, v0, Lmiui/v5/widget/TabController;->mViewPagerListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lmiui/v5/widget/TabController$2;->this$0:Lmiui/v5/widget/TabController;

    iget-object v0, v0, Lmiui/v5/widget/TabController;->mViewPagerListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    invoke-interface {v0, p1}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageScrollStateChanged(I)V

    :cond_11
    return-void
.end method

.method public onPageScrolled(IFI)V
    .registers 5
    .parameter "position"
    .parameter "positionOffset"
    .parameter "positionOffsetPixels"

    .prologue
    iget-object v0, p0, Lmiui/v5/widget/TabController$2;->this$0:Lmiui/v5/widget/TabController;

    iget-object v0, v0, Lmiui/v5/widget/TabController;->mTabIndicator:Lmiui/v5/widget/TabIndicator;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lmiui/v5/widget/TabController$2;->this$0:Lmiui/v5/widget/TabController;

    iget-object v0, v0, Lmiui/v5/widget/TabController;->mTabIndicator:Lmiui/v5/widget/TabIndicator;

    invoke-interface {v0, p1, p2}, Lmiui/v5/widget/TabIndicator;->apply(IF)F

    :cond_d
    iget-object v0, p0, Lmiui/v5/widget/TabController$2;->this$0:Lmiui/v5/widget/TabController;

    iget-object v0, v0, Lmiui/v5/widget/TabController;->mViewPagerListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lmiui/v5/widget/TabController$2;->this$0:Lmiui/v5/widget/TabController;

    iget-object v0, v0, Lmiui/v5/widget/TabController;->mViewPagerListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    invoke-interface {v0, p1, p2, p3}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageScrolled(IFI)V

    :cond_1a
    return-void
.end method

.method public onPageSelected(I)V
    .registers 3
    .parameter "position"

    .prologue
    if-ltz p1, :cond_1b

    iget-object v0, p0, Lmiui/v5/widget/TabController$2;->this$0:Lmiui/v5/widget/TabController;

    iget-object v0, v0, Lmiui/v5/widget/TabController;->mTabContainer:Lmiui/v5/widget/TabContainerLayout;

    invoke-virtual {v0}, Lmiui/v5/widget/TabContainerLayout;->getTabCount()I

    move-result v0

    if-ge p1, v0, :cond_1b

    iget-object v0, p0, Lmiui/v5/widget/TabController$2;->this$0:Lmiui/v5/widget/TabController;

    iget-object v0, v0, Lmiui/v5/widget/TabController;->mTabContainer:Lmiui/v5/widget/TabContainerLayout;

    invoke-virtual {v0}, Lmiui/v5/widget/TabContainerLayout;->findCurrentTabPos()I

    move-result v0

    if-eq p1, v0, :cond_1b

    iget-object v0, p0, Lmiui/v5/widget/TabController$2;->this$0:Lmiui/v5/widget/TabController;

    invoke-virtual {v0, p1}, Lmiui/v5/widget/TabController;->selectTab(I)Z

    :cond_1b
    iget-object v0, p0, Lmiui/v5/widget/TabController$2;->this$0:Lmiui/v5/widget/TabController;

    iget-object v0, v0, Lmiui/v5/widget/TabController;->mViewPagerListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    if-eqz v0, :cond_28

    iget-object v0, p0, Lmiui/v5/widget/TabController$2;->this$0:Lmiui/v5/widget/TabController;

    iget-object v0, v0, Lmiui/v5/widget/TabController;->mViewPagerListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    invoke-interface {v0, p1}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageSelected(I)V

    :cond_28
    return-void
.end method
