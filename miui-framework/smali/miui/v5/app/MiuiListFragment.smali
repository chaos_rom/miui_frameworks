.class public Lmiui/v5/app/MiuiListFragment;
.super Lmiui/v5/app/MiuiFragment;
.source "MiuiListFragment.java"


# instance fields
.field protected mList:Landroid/widget/AbsListView;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    invoke-direct {p0}, Lmiui/v5/app/MiuiFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public createLayoutObserver()Lmiui/v5/app/LayoutObserver;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/app/MiuiListFragment;->mList:Landroid/widget/AbsListView;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lmiui/v5/app/MiuiListFragment;->mList:Landroid/widget/AbsListView;

    invoke-static {v0}, Lmiui/v5/app/LayoutObservers;->makeLayoutObserverForListView(Landroid/widget/AdapterView;)Lmiui/v5/app/LayoutObserver;

    move-result-object v0

    :goto_a
    return-object v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public createMotionDetectStrategy()Lmiui/v5/widget/MotionDetectStrategy;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/app/MiuiListFragment;->mList:Landroid/widget/AbsListView;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lmiui/v5/app/MiuiListFragment;->mList:Landroid/widget/AbsListView;

    invoke-static {v0}, Lmiui/v5/widget/VerticalMotionStrategies;->makeMotionStrategyForList(Landroid/widget/AdapterView;)Lmiui/v5/widget/MotionDetectStrategy;

    move-result-object v0

    :goto_a
    return-object v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public createPageScrollEffect()Lmiui/v5/widget/PageScrollEffect;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/app/MiuiListFragment;->mList:Landroid/widget/AbsListView;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lmiui/v5/app/MiuiListFragment;->mList:Landroid/widget/AbsListView;

    invoke-static {v0}, Lmiui/v5/widget/PageScrollEffects;->makePageScrollEffect(Landroid/view/ViewGroup;)Lmiui/v5/widget/PageScrollEffect;

    move-result-object v0

    :goto_a
    return-object v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .registers 5
    .parameter "view"
    .parameter "icicle"

    .prologue
    invoke-super {p0, p1, p2}, Lmiui/v5/app/MiuiFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    const v0, 0x102000a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AbsListView;

    iput-object v0, p0, Lmiui/v5/app/MiuiListFragment;->mList:Landroid/widget/AbsListView;

    iget-object v0, p0, Lmiui/v5/app/MiuiListFragment;->mList:Landroid/widget/AbsListView;

    if-eqz v0, :cond_1d

    invoke-virtual {p0}, Lmiui/v5/app/MiuiListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lmiui/v5/app/MiuiActivity;

    iget-object v1, p0, Lmiui/v5/app/MiuiListFragment;->mList:Landroid/widget/AbsListView;

    invoke-virtual {v0, v1}, Lmiui/v5/app/MiuiActivity;->addLayoutObserver(Landroid/view/View;)V

    :cond_1d
    return-void
.end method
