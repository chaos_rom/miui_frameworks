.class Lmiui/v5/app/LayoutObservers$ListViewLayoutObserver;
.super Ljava/lang/Object;
.source "LayoutObservers.java"

# interfaces
.implements Lmiui/v5/app/LayoutObserver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/v5/app/LayoutObservers;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ListViewLayoutObserver"
.end annotation


# instance fields
.field private final mListView:Landroid/widget/AdapterView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/AdapterView",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/widget/AdapterView;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .local p1, lv:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmiui/v5/app/LayoutObservers$ListViewLayoutObserver;->mListView:Landroid/widget/AdapterView;

    return-void
.end method


# virtual methods
.method public isContentFilled()Z
    .registers 3

    .prologue
    iget-object v0, p0, Lmiui/v5/app/LayoutObservers$ListViewLayoutObserver;->mListView:Landroid/widget/AdapterView;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getCount()I

    move-result v0

    iget-object v1, p0, Lmiui/v5/app/LayoutObservers$ListViewLayoutObserver;->mListView:Landroid/widget/AdapterView;

    invoke-virtual {v1}, Landroid/widget/AdapterView;->getChildCount()I

    move-result v1

    if-le v0, v1, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method
