.class public Lmiui/v5/app/MiuiFragmentForList;
.super Landroid/app/ListFragment;
.source "MiuiFragmentForList.java"

# interfaces
.implements Lmiui/v5/app/MiuiViewPagerItem;


# instance fields
.field private mList:Landroid/widget/ListView;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    invoke-direct {p0}, Landroid/app/ListFragment;-><init>()V

    return-void
.end method

.method private peekListView()Landroid/widget/ListView;
    .registers 3

    .prologue
    iget-object v1, p0, Lmiui/v5/app/MiuiFragmentForList;->mList:Landroid/widget/ListView;

    if-nez v1, :cond_15

    invoke-virtual {p0}, Lmiui/v5/app/MiuiFragmentForList;->getView()Landroid/view/View;

    move-result-object v0

    .local v0, v:Landroid/view/View;
    if-eqz v0, :cond_15

    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lmiui/v5/app/MiuiFragmentForList;->mList:Landroid/widget/ListView;

    .end local v0           #v:Landroid/view/View;
    :cond_15
    iget-object v1, p0, Lmiui/v5/app/MiuiFragmentForList;->mList:Landroid/widget/ListView;

    return-object v1
.end method


# virtual methods
.method public createLayoutObserver()Lmiui/v5/app/LayoutObserver;
    .registers 3

    .prologue
    invoke-direct {p0}, Lmiui/v5/app/MiuiFragmentForList;->peekListView()Landroid/widget/ListView;

    move-result-object v0

    .local v0, lv:Landroid/widget/ListView;
    if-eqz v0, :cond_b

    invoke-static {v0}, Lmiui/v5/app/LayoutObservers;->makeLayoutObserverForListView(Landroid/widget/AdapterView;)Lmiui/v5/app/LayoutObserver;

    move-result-object v1

    :goto_a
    return-object v1

    :cond_b
    const/4 v1, 0x0

    goto :goto_a
.end method

.method public createMotionDetectStrategy()Lmiui/v5/widget/MotionDetectStrategy;
    .registers 3

    .prologue
    invoke-direct {p0}, Lmiui/v5/app/MiuiFragmentForList;->peekListView()Landroid/widget/ListView;

    move-result-object v0

    .local v0, lv:Landroid/widget/ListView;
    if-eqz v0, :cond_b

    invoke-static {v0}, Lmiui/v5/widget/VerticalMotionStrategies;->makeMotionStrategyForList(Landroid/widget/AdapterView;)Lmiui/v5/widget/MotionDetectStrategy;

    move-result-object v1

    :goto_a
    return-object v1

    :cond_b
    const/4 v1, 0x0

    goto :goto_a
.end method

.method public createPageScrollEffect()Lmiui/v5/widget/PageScrollEffect;
    .registers 3

    .prologue
    invoke-direct {p0}, Lmiui/v5/app/MiuiFragmentForList;->peekListView()Landroid/widget/ListView;

    move-result-object v0

    .local v0, lv:Landroid/widget/ListView;
    if-eqz v0, :cond_b

    invoke-static {v0}, Lmiui/v5/widget/PageScrollEffects;->makePageScrollEffect(Landroid/view/ViewGroup;)Lmiui/v5/widget/PageScrollEffect;

    move-result-object v1

    :goto_a
    return-object v1

    :cond_b
    const/4 v1, 0x0

    goto :goto_a
.end method

.method public onCreateMenuBar(Landroid/view/Menu;Landroid/view/MenuInflater;)Z
    .registers 4
    .parameter "menu"
    .parameter "inflater"

    .prologue
    const/4 v0, 0x0

    return v0
.end method

.method public onMenuBarClose(Landroid/view/Menu;)V
    .registers 2
    .parameter "menu"

    .prologue
    return-void
.end method

.method public onMenuBarItemSelected(Landroid/view/MenuItem;)Z
    .registers 3
    .parameter "item"

    .prologue
    const/4 v0, 0x0

    return v0
.end method

.method public onMenuBarModeChange(Landroid/view/Menu;I)V
    .registers 3
    .parameter "menu"
    .parameter "mode"

    .prologue
    return-void
.end method

.method public onMenuBarOpen(Landroid/view/Menu;)V
    .registers 2
    .parameter "menu"

    .prologue
    return-void
.end method

.method public onPrepareMenuBar(Landroid/view/Menu;)Z
    .registers 3
    .parameter "menu"

    .prologue
    const/4 v0, 0x1

    return v0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .registers 5
    .parameter "view"
    .parameter "icicle"

    .prologue
    invoke-super {p0, p1, p2}, Landroid/app/ListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-direct {p0}, Lmiui/v5/app/MiuiFragmentForList;->peekListView()Landroid/widget/ListView;

    move-result-object v0

    .local v0, lv:Landroid/widget/ListView;
    if-eqz v0, :cond_12

    invoke-virtual {p0}, Lmiui/v5/app/MiuiFragmentForList;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lmiui/v5/app/MiuiActivity;

    invoke-virtual {v1, v0}, Lmiui/v5/app/MiuiActivity;->addLayoutObserver(Landroid/view/View;)V

    :cond_12
    return-void
.end method
