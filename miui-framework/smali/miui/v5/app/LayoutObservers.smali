.class public Lmiui/v5/app/LayoutObservers;
.super Ljava/lang/Object;
.source "LayoutObservers.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/v5/app/LayoutObservers$ListViewLayoutObserver;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static makeLayoutObserverForListView(Landroid/widget/AdapterView;)Lmiui/v5/app/LayoutObserver;
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)",
            "Lmiui/v5/app/LayoutObserver;"
        }
    .end annotation

    .prologue
    .local p0, lv:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    new-instance v0, Lmiui/v5/app/LayoutObservers$ListViewLayoutObserver;

    invoke-direct {v0, p0}, Lmiui/v5/app/LayoutObservers$ListViewLayoutObserver;-><init>(Landroid/widget/AdapterView;)V

    return-object v0
.end method
