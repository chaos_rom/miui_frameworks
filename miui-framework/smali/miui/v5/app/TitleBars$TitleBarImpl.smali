.class Lmiui/v5/app/TitleBars$TitleBarImpl;
.super Ljava/lang/Object;
.source "TitleBars.java"

# interfaces
.implements Lmiui/v5/app/TitleBar;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/v5/app/TitleBars;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "TitleBarImpl"
.end annotation


# instance fields
.field private final mActivity:Landroid/app/Activity;

.field protected final mContainer:Landroid/view/View;

.field protected final mCustomViewContainer:Landroid/view/ViewGroup;

.field protected final mHomeIcon:Landroid/widget/ImageView;

.field protected final mLogoIcon:Landroid/widget/ImageView;

.field private final mMenuBuilder:Landroid/view/Menu;

.field protected final mPrimaryText:Landroid/widget/TextView;

.field protected final mSecondaryText:Landroid/widget/TextView;

.field protected final mShortcutIcon:Landroid/widget/ImageView;

.field private final mType:I


# direct methods
.method constructor <init>(Landroid/app/Activity;Landroid/view/View;I)V
    .registers 5
    .parameter "a"
    .parameter "container"
    .parameter "type"

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mActivity:Landroid/app/Activity;

    iput-object p2, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mContainer:Landroid/view/View;

    new-instance v0, Lcom/android/internal/view/menu/MenuBuilder;

    invoke-direct {v0, p1}, Lcom/android/internal/view/menu/MenuBuilder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mMenuBuilder:Landroid/view/Menu;

    const v0, 0x102000c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mPrimaryText:Landroid/widget/TextView;

    const v0, 0x102000b

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mSecondaryText:Landroid/widget/TextView;

    const v0, 0x102002c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mHomeIcon:Landroid/widget/ImageView;

    const v0, 0x1020006

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mLogoIcon:Landroid/widget/ImageView;

    const v0, 0x1020017

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mShortcutIcon:Landroid/widget/ImageView;

    const v0, 0x102002b

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mCustomViewContainer:Landroid/view/ViewGroup;

    iput p3, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mType:I

    iget-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mHomeIcon:Landroid/widget/ImageView;

    if-eqz v0, :cond_5b

    iget-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mHomeIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_5b
    iget-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mShortcutIcon:Landroid/widget/ImageView;

    if-eqz v0, :cond_64

    iget-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mShortcutIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_64
    iget-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mCustomViewContainer:Landroid/view/ViewGroup;

    if-eqz v0, :cond_6d

    iget-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mCustomViewContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_6d
    return-void
.end method


# virtual methods
.method public destroy()V
    .registers 3

    .prologue
    iget-object v1, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mContainer:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .local v0, parent:Landroid/view/ViewGroup;
    iget-object v1, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mContainer:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    return-void
.end method

.method public getPrimaryText()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mPrimaryText:Landroid/widget/TextView;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mPrimaryText:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    :goto_a
    return-object v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public getSecondaryText()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mSecondaryText:Landroid/widget/TextView;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mSecondaryText:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    :goto_a
    return-object v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public getType()I
    .registers 2

    .prologue
    iget v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mType:I

    return v0
.end method

.method public getView()Landroid/view/View;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mContainer:Landroid/view/View;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .registers 7
    .parameter "v"

    .prologue
    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .local v0, id:I
    iget-object v2, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mMenuBuilder:Landroid/view/Menu;

    invoke-interface {v2, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .local v1, item:Landroid/view/MenuItem;
    if-nez v1, :cond_15

    iget-object v2, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mMenuBuilder:Landroid/view/Menu;

    const-string v3, ""

    invoke-interface {v2, v4, v0, v4, v3}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v1

    :cond_15
    iget-object v2, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2, v4, v1}, Landroid/app/Activity;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    return-void
.end method

.method public setCustomView(I)Landroid/view/View;
    .registers 7
    .parameter "layoutId"

    .prologue
    const/4 v2, 0x0

    iget-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mCustomViewContainer:Landroid/view/ViewGroup;

    .local v0, parent:Landroid/view/ViewGroup;
    if-eqz v0, :cond_12

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v3, p1, v0, v4}, Lmiui/v5/widget/Views;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .local v1, view:Landroid/view/View;
    invoke-virtual {p0, v1, v2}, Lmiui/v5/app/TitleBars$TitleBarImpl;->setCustomView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)Landroid/view/View;

    move-result-object v2

    .end local v1           #view:Landroid/view/View;
    :cond_12
    return-object v2
.end method

.method public setCustomView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)Landroid/view/View;
    .registers 4
    .parameter "view"
    .parameter "params"

    .prologue
    iget-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mCustomViewContainer:Landroid/view/ViewGroup;

    if-eqz v0, :cond_17

    iget-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mCustomViewContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    if-eqz p2, :cond_11

    iget-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mCustomViewContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .end local p1
    :goto_10
    return-object p1

    .restart local p1
    :cond_11
    iget-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mCustomViewContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_10

    :cond_17
    const/4 p1, 0x0

    goto :goto_10
.end method

.method public setCustomViewVisibility(I)V
    .registers 3
    .parameter "visibility"

    .prologue
    iget-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mCustomViewContainer:Landroid/view/ViewGroup;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mCustomViewContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_9
    return-void
.end method

.method public setHomeIcon(I)V
    .registers 3
    .parameter "iconId"

    .prologue
    iget-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mHomeIcon:Landroid/widget/ImageView;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mHomeIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :cond_9
    return-void
.end method

.method public setHomeIcon(Landroid/graphics/drawable/Drawable;)V
    .registers 3
    .parameter "d"

    .prologue
    iget-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mHomeIcon:Landroid/widget/ImageView;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mHomeIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :cond_9
    return-void
.end method

.method public setHomeIconVisibility(I)V
    .registers 3
    .parameter "visibility"

    .prologue
    iget-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mHomeIcon:Landroid/widget/ImageView;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mHomeIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_9
    return-void
.end method

.method public setLogoIcon(I)V
    .registers 3
    .parameter "iconId"

    .prologue
    iget-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mLogoIcon:Landroid/widget/ImageView;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mLogoIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_9
    return-void
.end method

.method public setLogoIcon(Landroid/graphics/drawable/Drawable;)V
    .registers 3
    .parameter "d"

    .prologue
    iget-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mLogoIcon:Landroid/widget/ImageView;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mLogoIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_9
    return-void
.end method

.method public setLogoIconVisibility(I)V
    .registers 3
    .parameter "visibility"

    .prologue
    iget-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mLogoIcon:Landroid/widget/ImageView;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mLogoIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_9
    return-void
.end method

.method public setPrimaryText(I)V
    .registers 3
    .parameter "textId"

    .prologue
    iget-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mPrimaryText:Landroid/widget/TextView;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mPrimaryText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    :cond_9
    return-void
.end method

.method public setPrimaryText(Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "text"

    .prologue
    iget-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mPrimaryText:Landroid/widget/TextView;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mPrimaryText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_9
    return-void
.end method

.method public setPrimaryTextVisibility(I)V
    .registers 3
    .parameter "visibility"

    .prologue
    iget-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mPrimaryText:Landroid/widget/TextView;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mPrimaryText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_9
    return-void
.end method

.method public setSecondaryText(I)V
    .registers 3
    .parameter "text"

    .prologue
    iget-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mSecondaryText:Landroid/widget/TextView;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mSecondaryText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    :cond_9
    return-void
.end method

.method public setSecondaryText(Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "text"

    .prologue
    iget-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mSecondaryText:Landroid/widget/TextView;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mSecondaryText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_9
    return-void
.end method

.method public setSecondaryTextVisibility(I)V
    .registers 3
    .parameter "visibility"

    .prologue
    iget-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mSecondaryText:Landroid/widget/TextView;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mSecondaryText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_9
    return-void
.end method

.method public setShortcutIcon(I)V
    .registers 3
    .parameter "iconId"

    .prologue
    iget-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mShortcutIcon:Landroid/widget/ImageView;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mShortcutIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_9
    return-void
.end method

.method public setShortcutIcon(Landroid/graphics/drawable/Drawable;)V
    .registers 3
    .parameter "d"

    .prologue
    iget-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mShortcutIcon:Landroid/widget/ImageView;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mShortcutIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_9
    return-void
.end method

.method public setShortcutIconVisibility(I)V
    .registers 3
    .parameter "visibility"

    .prologue
    iget-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mShortcutIcon:Landroid/widget/ImageView;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lmiui/v5/app/TitleBars$TitleBarImpl;->mShortcutIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_9
    return-void
.end method
