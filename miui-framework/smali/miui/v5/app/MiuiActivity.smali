.class public Lmiui/v5/app/MiuiActivity;
.super Landroid/app/Activity;
.source "MiuiActivity.java"

# interfaces
.implements Lmiui/v5/view/MiuiActionMode$ActionModeListener;
.implements Lmiui/v5/widget/menubar/MenuBar$Callback;


# instance fields
.field private mBottomBarContainer:Landroid/widget/FrameLayout;

.field private mBottomBarTopLine:Landroid/view/View;

.field private final mBottomHierarchyChangeListener:Landroid/view/ViewGroup$OnHierarchyChangeListener;

.field private mContentContainer:Landroid/widget/FrameLayout;

.field private mDecoratedView:Landroid/view/View;

.field mIconMenuBarPresenter:Lmiui/v5/widget/menubar/IconMenuBarPresenter;

.field private final mLayoutListener:Landroid/view/View$OnLayoutChangeListener;

.field mMenuBar:Lmiui/v5/widget/menubar/MenuBar;

.field mTitleBar:Lmiui/v5/app/TitleBar;

.field private mTitleBarContainer:Landroid/view/ViewGroup;

.field private mWrapper:Lmiui/v5/view/ActionModeWrapper;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Lmiui/v5/app/MiuiActivity$1;

    invoke-direct {v0, p0}, Lmiui/v5/app/MiuiActivity$1;-><init>(Lmiui/v5/app/MiuiActivity;)V

    iput-object v0, p0, Lmiui/v5/app/MiuiActivity;->mBottomHierarchyChangeListener:Landroid/view/ViewGroup$OnHierarchyChangeListener;

    new-instance v0, Lmiui/v5/app/MiuiActivity$2;

    invoke-direct {v0, p0}, Lmiui/v5/app/MiuiActivity$2;-><init>(Lmiui/v5/app/MiuiActivity;)V

    iput-object v0, p0, Lmiui/v5/app/MiuiActivity;->mLayoutListener:Landroid/view/View$OnLayoutChangeListener;

    return-void
.end method

.method private initBottomBarTopLine()V
    .registers 3

    .prologue
    iget-object v1, p0, Lmiui/v5/app/MiuiActivity;->mBottomBarTopLine:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .local v0, params:Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-virtual {p0}, Lmiui/v5/app/MiuiActivity;->getBottomPlaceholderHeight()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    iget-object v1, p0, Lmiui/v5/app/MiuiActivity;->mBottomBarTopLine:Landroid/view/View;

    invoke-static {v1}, Lmiui/v5/widget/Views;->getBackgroundHeight(Landroid/view/View;)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v1, p0, Lmiui/v5/app/MiuiActivity;->mBottomBarTopLine:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->requestLayout()V

    return-void
.end method

.method private setBottomBarTopLineVisible(Z)V
    .registers 4
    .parameter "visible"

    .prologue
    iget-object v1, p0, Lmiui/v5/app/MiuiActivity;->mBottomBarTopLine:Landroid/view/View;

    if-eqz p1, :cond_9

    const/4 v0, 0x0

    :goto_5
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_9
    const/16 v0, 0x8

    goto :goto_5
.end method

.method private updateContentBorder(Z)V
    .registers 5
    .parameter "bottomBarEnabled"

    .prologue
    const/4 v1, 0x0

    .local v1, visible:Z
    if-eqz p1, :cond_10

    invoke-virtual {p0}, Lmiui/v5/app/MiuiActivity;->getLayoutObserver()Lmiui/v5/app/LayoutObserver;

    move-result-object v0

    .local v0, observer:Lmiui/v5/app/LayoutObserver;
    if-eqz v0, :cond_14

    invoke-interface {v0}, Lmiui/v5/app/LayoutObserver;->isContentFilled()Z

    move-result v2

    if-eqz v2, :cond_14

    const/4 v1, 0x1

    .end local v0           #observer:Lmiui/v5/app/LayoutObserver;
    :cond_10
    :goto_10
    invoke-direct {p0, v1}, Lmiui/v5/app/MiuiActivity;->setBottomBarTopLineVisible(Z)V

    return-void

    .restart local v0       #observer:Lmiui/v5/app/LayoutObserver;
    :cond_14
    const/4 v1, 0x0

    goto :goto_10
.end method


# virtual methods
.method public addLayoutObserver(Landroid/view/View;)V
    .registers 3
    .parameter "view"

    .prologue
    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mLayoutListener:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mLayoutListener:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    invoke-virtual {p0}, Lmiui/v5/app/MiuiActivity;->updateContentBorder()V

    return-void
.end method

.method protected getBottomPlaceholderHeight()I
    .registers 3

    .prologue
    const v1, 0x6010033

    invoke-static {p0, v1}, Lmiui/util/UiUtils;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .local v0, d:Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_e

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    :goto_d
    return v1

    :cond_e
    const/4 v1, 0x0

    goto :goto_d
.end method

.method protected getCurrentPagerItem()Lmiui/v5/app/MiuiViewPagerItem;
    .registers 2

    .prologue
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getLayoutObserver()Lmiui/v5/app/LayoutObserver;
    .registers 2

    .prologue
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMenuBar()Lmiui/v5/widget/menubar/MenuBar;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mMenuBar:Lmiui/v5/widget/menubar/MenuBar;

    return-object v0
.end method

.method public getTitleBar()Lmiui/v5/app/TitleBar;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mTitleBar:Lmiui/v5/app/TitleBar;

    return-object v0
.end method

.method protected getTopPlaceholderHeight()I
    .registers 3

    .prologue
    const v1, 0x6010032

    invoke-static {p0, v1}, Lmiui/util/UiUtils;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .local v0, d:Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_e

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    :goto_d
    return v1

    :cond_e
    const/4 v1, 0x0

    goto :goto_d
.end method

.method public getWrapperView()Landroid/view/ViewGroup;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mWrapper:Lmiui/v5/view/ActionModeWrapper;

    return-object v0
.end method

.method protected initMenuBar(Landroid/view/ViewGroup;)V
    .registers 6
    .parameter "container"

    .prologue
    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mMenuBar:Lmiui/v5/widget/menubar/MenuBar;

    if-nez v0, :cond_46

    new-instance v0, Lmiui/v5/widget/menubar/MenuBar;

    invoke-direct {v0, p0}, Lmiui/v5/widget/menubar/MenuBar;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lmiui/v5/app/MiuiActivity;->mMenuBar:Lmiui/v5/widget/menubar/MenuBar;

    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mMenuBar:Lmiui/v5/widget/menubar/MenuBar;

    invoke-virtual {v0, p0}, Lmiui/v5/widget/menubar/MenuBar;->setCallback(Lmiui/v5/widget/menubar/MenuBar$Callback;)V

    new-instance v0, Lmiui/v5/widget/menubar/IconMenuBarPresenter;

    const v1, 0x6030057

    const v2, 0x6030058

    const v3, 0x6030059

    invoke-direct {v0, p0, v1, v2, v3}, Lmiui/v5/widget/menubar/IconMenuBarPresenter;-><init>(Landroid/content/Context;III)V

    iput-object v0, p0, Lmiui/v5/app/MiuiActivity;->mIconMenuBarPresenter:Lmiui/v5/widget/menubar/IconMenuBarPresenter;

    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mIconMenuBarPresenter:Lmiui/v5/widget/menubar/IconMenuBarPresenter;

    const v1, 0x6010061

    invoke-static {p0, v1}, Lmiui/util/UiUtils;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->setMoreIconDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mIconMenuBarPresenter:Lmiui/v5/widget/menubar/IconMenuBarPresenter;

    const v1, 0x601006f

    invoke-static {p0, v1}, Lmiui/util/UiUtils;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->setPrimaryMaskDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mMenuBar:Lmiui/v5/widget/menubar/MenuBar;

    iget-object v1, p0, Lmiui/v5/app/MiuiActivity;->mIconMenuBarPresenter:Lmiui/v5/widget/menubar/IconMenuBarPresenter;

    invoke-virtual {v0, v1}, Lmiui/v5/widget/menubar/MenuBar;->addMenuPresenter(Lmiui/v5/widget/menubar/MenuBarPresenter;)V

    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mIconMenuBarPresenter:Lmiui/v5/widget/menubar/IconMenuBarPresenter;

    iget-object v1, p0, Lmiui/v5/app/MiuiActivity;->mBottomBarContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Lmiui/v5/widget/menubar/IconMenuBarPresenter;->getMenuView(Landroid/view/ViewGroup;)Lmiui/v5/widget/menubar/MenuBarView;

    :cond_46
    return-void
.end method

.method protected isBottomPlaceholderEnabled()Z
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mContentContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    if-lez v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method protected isTopPlaceholderEnabled()Z
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mContentContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    if-lez v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public onActionModeFinished(Lmiui/v5/view/MiuiActionMode;)V
    .registers 2
    .parameter "mode"

    .prologue
    return-void
.end method

.method public onActionModeStarted(Lmiui/v5/view/MiuiActionMode;)V
    .registers 2
    .parameter "mode"

    .prologue
    return-void
.end method

.method public onBackPressed()V
    .registers 3

    .prologue
    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mMenuBar:Lmiui/v5/widget/menubar/MenuBar;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mMenuBar:Lmiui/v5/widget/menubar/MenuBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmiui/v5/widget/menubar/MenuBar;->expand(Z)Z

    move-result v0

    if-eqz v0, :cond_e

    :goto_d
    return-void

    :cond_e
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_d
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 4
    .parameter "savedInstanceState"

    .prologue
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x603004a

    invoke-virtual {p0, v0}, Lmiui/v5/app/MiuiActivity;->setContentView(I)V

    const v0, 0x60b0093

    invoke-virtual {p0, v0}, Lmiui/v5/app/MiuiActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiui/v5/view/ActionModeWrapper;

    iput-object v0, p0, Lmiui/v5/app/MiuiActivity;->mWrapper:Lmiui/v5/view/ActionModeWrapper;

    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mWrapper:Lmiui/v5/view/ActionModeWrapper;

    invoke-virtual {v0, p0}, Lmiui/v5/view/ActionModeWrapper;->setActionModeListener(Lmiui/v5/view/MiuiActionMode$ActionModeListener;)V

    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mWrapper:Lmiui/v5/view/ActionModeWrapper;

    const v1, 0x60b0094

    invoke-virtual {v0, v1}, Lmiui/v5/view/ActionModeWrapper;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lmiui/v5/app/MiuiActivity;->mContentContainer:Landroid/widget/FrameLayout;

    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mWrapper:Lmiui/v5/view/ActionModeWrapper;

    const v1, 0x60b0095

    invoke-virtual {v0, v1}, Lmiui/v5/view/ActionModeWrapper;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lmiui/v5/app/MiuiActivity;->mBottomBarContainer:Landroid/widget/FrameLayout;

    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mBottomBarContainer:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lmiui/v5/app/MiuiActivity;->mBottomHierarchyChangeListener:Landroid/view/ViewGroup$OnHierarchyChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V

    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mWrapper:Lmiui/v5/view/ActionModeWrapper;

    const v1, 0x60b009b

    invoke-virtual {v0, v1}, Lmiui/v5/view/ActionModeWrapper;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lmiui/v5/app/MiuiActivity;->mTitleBarContainer:Landroid/view/ViewGroup;

    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mWrapper:Lmiui/v5/view/ActionModeWrapper;

    const v1, 0x60b00a3

    invoke-virtual {v0, v1}, Lmiui/v5/view/ActionModeWrapper;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lmiui/v5/app/MiuiActivity;->mBottomBarTopLine:Landroid/view/View;

    invoke-direct {p0}, Lmiui/v5/app/MiuiActivity;->initBottomBarTopLine()V

    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mBottomBarContainer:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v0}, Lmiui/v5/app/MiuiActivity;->initMenuBar(Landroid/view/ViewGroup;)V

    const v0, 0x6010034

    invoke-static {p0, v0}, Lmiui/util/UiUtils;->getBoolean(Landroid/content/Context;I)Z

    move-result v0

    invoke-virtual {p0, v0}, Lmiui/v5/app/MiuiActivity;->setTopPlaceholderEnabled(Z)V

    return-void
.end method

.method protected onCreateMenuBar(Landroid/view/Menu;)Z
    .registers 3
    .parameter "menu"

    .prologue
    const/4 v0, 0x0

    return v0
.end method

.method public onCreateMenuBarPanel(Landroid/view/Menu;)Z
    .registers 5
    .parameter "menu"

    .prologue
    const/4 v1, 0x0

    .local v1, show:Z
    invoke-virtual {p0, p1}, Lmiui/v5/app/MiuiActivity;->onCreateMenuBar(Landroid/view/Menu;)Z

    move-result v1

    invoke-virtual {p0}, Lmiui/v5/app/MiuiActivity;->getCurrentPagerItem()Lmiui/v5/app/MiuiViewPagerItem;

    move-result-object v0

    .local v0, current:Lmiui/v5/app/MiuiViewPagerItem;
    if-eqz v0, :cond_14

    invoke-virtual {p0}, Lmiui/v5/app/MiuiActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v2

    invoke-interface {v0, p1, v2}, Lmiui/v5/app/MiuiViewPagerItem;->onCreateMenuBar(Landroid/view/Menu;Landroid/view/MenuInflater;)Z

    move-result v2

    or-int/2addr v1, v2

    :cond_14
    return v1
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter "keyCode"
    .parameter "event"

    .prologue
    const/16 v0, 0x52

    if-ne p1, v0, :cond_e

    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mWrapper:Lmiui/v5/view/ActionModeWrapper;

    invoke-virtual {v0}, Lmiui/v5/view/ActionModeWrapper;->isActionModeActive()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_d
.end method

.method protected onMenuBarClose(Landroid/view/Menu;)V
    .registers 2
    .parameter "menu"

    .prologue
    invoke-virtual {p0}, Lmiui/v5/app/MiuiActivity;->updateBottomPlaceholder()V

    return-void
.end method

.method protected onMenuBarItemSelected(Landroid/view/MenuItem;)Z
    .registers 3
    .parameter "item"

    .prologue
    const/4 v0, 0x0

    return v0
.end method

.method protected onMenuBarModeChange(Landroid/view/Menu;I)V
    .registers 3
    .parameter "menu"
    .parameter "mode"

    .prologue
    return-void
.end method

.method protected onMenuBarOpen(Landroid/view/Menu;)V
    .registers 2
    .parameter "menu"

    .prologue
    invoke-virtual {p0}, Lmiui/v5/app/MiuiActivity;->updateBottomPlaceholder()V

    return-void
.end method

.method public onMenuBarPanelClose(Landroid/view/Menu;)V
    .registers 3
    .parameter "menu"

    .prologue
    invoke-virtual {p0}, Lmiui/v5/app/MiuiActivity;->getCurrentPagerItem()Lmiui/v5/app/MiuiViewPagerItem;

    move-result-object v0

    .local v0, current:Lmiui/v5/app/MiuiViewPagerItem;
    if-eqz v0, :cond_9

    invoke-interface {v0, p1}, Lmiui/v5/app/MiuiViewPagerItem;->onMenuBarClose(Landroid/view/Menu;)V

    :cond_9
    invoke-virtual {p0, p1}, Lmiui/v5/app/MiuiActivity;->onMenuBarClose(Landroid/view/Menu;)V

    return-void
.end method

.method public onMenuBarPanelItemSelected(Landroid/view/Menu;Landroid/view/MenuItem;)Z
    .registers 6
    .parameter "menu"
    .parameter "item"

    .prologue
    const/4 v1, 0x0

    .local v1, handled:Z
    invoke-virtual {p0}, Lmiui/v5/app/MiuiActivity;->getCurrentPagerItem()Lmiui/v5/app/MiuiViewPagerItem;

    move-result-object v0

    .local v0, current:Lmiui/v5/app/MiuiViewPagerItem;
    if-eqz v0, :cond_b

    invoke-interface {v0, p2}, Lmiui/v5/app/MiuiViewPagerItem;->onMenuBarItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    :cond_b
    invoke-virtual {p0, p2}, Lmiui/v5/app/MiuiActivity;->onMenuBarItemSelected(Landroid/view/MenuItem;)Z

    move-result v2

    or-int/2addr v1, v2

    return v1
.end method

.method public onMenuBarPanelModeChange(Landroid/view/Menu;I)V
    .registers 4
    .parameter "menu"
    .parameter "mode"

    .prologue
    invoke-virtual {p0}, Lmiui/v5/app/MiuiActivity;->getCurrentPagerItem()Lmiui/v5/app/MiuiViewPagerItem;

    move-result-object v0

    .local v0, current:Lmiui/v5/app/MiuiViewPagerItem;
    if-eqz v0, :cond_9

    invoke-interface {v0, p1, p2}, Lmiui/v5/app/MiuiViewPagerItem;->onMenuBarModeChange(Landroid/view/Menu;I)V

    :cond_9
    invoke-virtual {p0, p1, p2}, Lmiui/v5/app/MiuiActivity;->onMenuBarModeChange(Landroid/view/Menu;I)V

    return-void
.end method

.method public onMenuBarPanelOpen(Landroid/view/Menu;)V
    .registers 3
    .parameter "menu"

    .prologue
    invoke-virtual {p0}, Lmiui/v5/app/MiuiActivity;->getCurrentPagerItem()Lmiui/v5/app/MiuiViewPagerItem;

    move-result-object v0

    .local v0, current:Lmiui/v5/app/MiuiViewPagerItem;
    if-eqz v0, :cond_9

    invoke-interface {v0, p1}, Lmiui/v5/app/MiuiViewPagerItem;->onMenuBarOpen(Landroid/view/Menu;)V

    :cond_9
    invoke-virtual {p0, p1}, Lmiui/v5/app/MiuiActivity;->onMenuBarOpen(Landroid/view/Menu;)V

    return-void
.end method

.method protected onPrepareMenuBar(Landroid/view/Menu;)Z
    .registers 3
    .parameter "menu"

    .prologue
    const/4 v0, 0x1

    return v0
.end method

.method public onPrepareMenuBarPanel(Landroid/view/Menu;)Z
    .registers 5
    .parameter "menu"

    .prologue
    const/4 v1, 0x0

    .local v1, show:Z
    invoke-virtual {p0, p1}, Lmiui/v5/app/MiuiActivity;->onPrepareMenuBar(Landroid/view/Menu;)Z

    move-result v1

    invoke-virtual {p0}, Lmiui/v5/app/MiuiActivity;->getCurrentPagerItem()Lmiui/v5/app/MiuiViewPagerItem;

    move-result-object v0

    .local v0, current:Lmiui/v5/app/MiuiViewPagerItem;
    if-eqz v0, :cond_10

    invoke-interface {v0, p1}, Lmiui/v5/app/MiuiViewPagerItem;->onPrepareMenuBar(Landroid/view/Menu;)Z

    move-result v2

    or-int/2addr v1, v2

    :cond_10
    return v1
.end method

.method protected onStart()V
    .registers 2

    .prologue
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mMenuBar:Lmiui/v5/widget/menubar/MenuBar;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mMenuBar:Lmiui/v5/widget/menubar/MenuBar;

    invoke-virtual {v0}, Lmiui/v5/widget/menubar/MenuBar;->open()V

    :cond_c
    return-void
.end method

.method protected onStop()V
    .registers 2

    .prologue
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mMenuBar:Lmiui/v5/widget/menubar/MenuBar;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mMenuBar:Lmiui/v5/widget/menubar/MenuBar;

    invoke-virtual {v0}, Lmiui/v5/widget/menubar/MenuBar;->close()V

    :cond_c
    return-void
.end method

.method protected removeAllTitleViews()V
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mTitleBarContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    return-void
.end method

.method public removeLayoutObserver(Landroid/view/View;)V
    .registers 3
    .parameter "view"

    .prologue
    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mLayoutListener:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    return-void
.end method

.method protected setBackground(I)V
    .registers 3
    .parameter "resId"

    .prologue
    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mWrapper:Lmiui/v5/view/ActionModeWrapper;

    invoke-virtual {v0, p1}, Lmiui/v5/view/ActionModeWrapper;->setBackgroundResource(I)V

    return-void
.end method

.method protected setBackground(Landroid/graphics/drawable/Drawable;)V
    .registers 3
    .parameter "d"

    .prologue
    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mWrapper:Lmiui/v5/view/ActionModeWrapper;

    invoke-virtual {v0, p1}, Lmiui/v5/view/ActionModeWrapper;->setBackground(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method setBottomPlaceholderEnabled(Z)V
    .registers 7
    .parameter "enabled"

    .prologue
    const/4 v3, 0x0

    invoke-virtual {p0}, Lmiui/v5/app/MiuiActivity;->getBottomPlaceholderHeight()I

    move-result v0

    .local v0, bottomMargin:I
    iget-object v2, p0, Lmiui/v5/app/MiuiActivity;->mContentContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .local v1, params:Landroid/view/ViewGroup$MarginLayoutParams;
    iget v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    if-eqz v2, :cond_30

    iget v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    if-eq v2, v0, :cond_30

    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lmiui/v5/app/MiuiActivity;->mContentContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Cannot be set margin external!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_30
    if-eqz p1, :cond_42

    iget v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    if-nez v2, :cond_42

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    iget-object v2, p0, Lmiui/v5/app/MiuiActivity;->mContentContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->requestLayout()V

    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lmiui/v5/app/MiuiActivity;->updateContentBorder(Z)V

    :cond_41
    :goto_41
    return-void

    :cond_42
    if-nez p1, :cond_41

    iget v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    if-eqz v2, :cond_41

    iput v3, v1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    iget-object v2, p0, Lmiui/v5/app/MiuiActivity;->mContentContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->requestLayout()V

    invoke-direct {p0, v3}, Lmiui/v5/app/MiuiActivity;->updateContentBorder(Z)V

    goto :goto_41
.end method

.method protected setMiuiContentView(I)Landroid/view/View;
    .registers 4
    .parameter "layoutResID"

    .prologue
    const/4 v1, 0x0

    invoke-static {p0, p1, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Lmiui/v5/app/MiuiActivity;->setMiuiContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected setMiuiContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)Landroid/view/View;
    .registers 5
    .parameter "decorateView"
    .parameter "lp"

    .prologue
    if-eqz p1, :cond_1f

    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mDecoratedView:Landroid/view/View;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mContentContainer:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lmiui/v5/app/MiuiActivity;->mDecoratedView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    :cond_d
    if-eqz p2, :cond_19

    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mContentContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1, p2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :goto_14
    iput-object p1, p0, Lmiui/v5/app/MiuiActivity;->mDecoratedView:Landroid/view/View;

    :cond_16
    :goto_16
    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mDecoratedView:Landroid/view/View;

    return-object v0

    :cond_19
    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mContentContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    goto :goto_14

    :cond_1f
    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mDecoratedView:Landroid/view/View;

    if-eqz v0, :cond_16

    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mContentContainer:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lmiui/v5/app/MiuiActivity;->mDecoratedView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lmiui/v5/app/MiuiActivity;->mDecoratedView:Landroid/view/View;

    goto :goto_16
.end method

.method protected setTitleBarType(I)Lmiui/v5/app/TitleBar;
    .registers 4
    .parameter "type"

    .prologue
    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mTitleBar:Lmiui/v5/app/TitleBar;

    if-eqz v0, :cond_1d

    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mTitleBar:Lmiui/v5/app/TitleBar;

    invoke-interface {v0}, Lmiui/v5/app/TitleBar;->getType()I

    move-result v0

    if-ne v0, p1, :cond_f

    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mTitleBar:Lmiui/v5/app/TitleBar;

    :goto_e
    return-object v0

    :cond_f
    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mTitleBarContainer:Landroid/view/ViewGroup;

    iget-object v1, p0, Lmiui/v5/app/MiuiActivity;->mTitleBar:Lmiui/v5/app/TitleBar;

    invoke-interface {v1}, Lmiui/v5/app/TitleBar;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lmiui/v5/app/MiuiActivity;->mTitleBar:Lmiui/v5/app/TitleBar;

    :cond_1d
    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mTitleBarContainer:Landroid/view/ViewGroup;

    invoke-static {p0, p1, v0}, Lmiui/v5/app/TitleBars;->inflate(Landroid/app/Activity;ILandroid/view/ViewGroup;)Lmiui/v5/app/TitleBar;

    move-result-object v0

    iput-object v0, p0, Lmiui/v5/app/MiuiActivity;->mTitleBar:Lmiui/v5/app/TitleBar;

    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mTitleBar:Lmiui/v5/app/TitleBar;

    goto :goto_e
.end method

.method protected setTitleView(I)Landroid/view/View;
    .registers 5
    .parameter "layoutId"

    .prologue
    iget-object v1, p0, Lmiui/v5/app/MiuiActivity;->mTitleBarContainer:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    invoke-static {p0, p1, v1, v2}, Lmiui/v5/widget/Views;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .local v0, v:Landroid/view/View;
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lmiui/v5/app/MiuiActivity;->setTitleView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method

.method protected setTitleView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)Landroid/view/View;
    .registers 4
    .parameter "v"
    .parameter "params"

    .prologue
    if-eqz p2, :cond_8

    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mTitleBarContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :goto_7
    return-object p1

    :cond_8
    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mTitleBarContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_7
.end method

.method setTopPlaceholderEnabled(Z)V
    .registers 7
    .parameter "enabled"

    .prologue
    invoke-virtual {p0}, Lmiui/v5/app/MiuiActivity;->getTopPlaceholderHeight()I

    move-result v1

    .local v1, topMargin:I
    iget-object v2, p0, Lmiui/v5/app/MiuiActivity;->mContentContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .local v0, params:Landroid/view/ViewGroup$MarginLayoutParams;
    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    if-eqz v2, :cond_2f

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    if-eq v2, v1, :cond_2f

    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lmiui/v5/app/MiuiActivity;->mContentContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Cannot be set margin external!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_2f
    if-eqz p1, :cond_3d

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    if-nez v2, :cond_3d

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget-object v2, p0, Lmiui/v5/app/MiuiActivity;->mContentContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->requestLayout()V

    :cond_3c
    :goto_3c
    return-void

    :cond_3d
    if-nez p1, :cond_3c

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    if-eqz v2, :cond_3c

    const/4 v2, 0x0

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget-object v2, p0, Lmiui/v5/app/MiuiActivity;->mContentContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->requestLayout()V

    goto :goto_3c
.end method

.method public startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;
    .registers 3
    .parameter "callback"

    .prologue
    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mWrapper:Lmiui/v5/view/ActionModeWrapper;

    invoke-virtual {v0, p1}, Lmiui/v5/view/ActionModeWrapper;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v0

    return-object v0
.end method

.method public startSearchMode(Landroid/view/View;Landroid/view/View;Ljava/lang/CharSequence;Lmiui/v5/view/MiuiActionMode$ActionModeListener;)Lmiui/v5/app/SearchMode$Token;
    .registers 6
    .parameter "anchor"
    .parameter "animView"
    .parameter "hint"
    .parameter "l"

    .prologue
    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mWrapper:Lmiui/v5/view/ActionModeWrapper;

    invoke-static {v0, p1, p2, p3, p4}, Lmiui/v5/app/SearchMode;->startSearchMode(Lmiui/v5/view/ActionModeWrapper;Landroid/view/View;Landroid/view/View;Ljava/lang/CharSequence;Lmiui/v5/view/MiuiActionMode$ActionModeListener;)Lmiui/v5/app/SearchMode$Token;

    move-result-object v0

    return-object v0
.end method

.method public startSearchMode(Landroid/view/View;Landroid/view/View;Lmiui/v5/view/MiuiActionMode$ActionModeListener;)Lmiui/v5/app/SearchMode$Token;
    .registers 7
    .parameter "anchor"
    .parameter "animView"
    .parameter "l"

    .prologue
    if-eqz p1, :cond_1a

    const v2, 0x1020009

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .local v1, v:Landroid/view/View;
    instance-of v2, v1, Landroid/widget/TextView;

    if-eqz v2, :cond_18

    check-cast v1, Landroid/widget/TextView;

    .end local v1           #v:Landroid/view/View;
    invoke-virtual {v1}, Landroid/widget/TextView;->getHint()Ljava/lang/CharSequence;

    move-result-object v0

    .local v0, hint:Ljava/lang/CharSequence;
    :goto_13
    invoke-virtual {p0, p1, p2, v0, p3}, Lmiui/v5/app/MiuiActivity;->startSearchMode(Landroid/view/View;Landroid/view/View;Ljava/lang/CharSequence;Lmiui/v5/view/MiuiActionMode$ActionModeListener;)Lmiui/v5/app/SearchMode$Token;

    move-result-object v2

    return-object v2

    .end local v0           #hint:Ljava/lang/CharSequence;
    .restart local v1       #v:Landroid/view/View;
    :cond_18
    const/4 v0, 0x0

    goto :goto_13

    .end local v1           #v:Landroid/view/View;
    :cond_1a
    const/4 v0, 0x0

    .restart local v0       #hint:Ljava/lang/CharSequence;
    goto :goto_13
.end method

.method updateBottomPlaceholder()V
    .registers 7

    .prologue
    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mBottomBarContainer:Landroid/widget/FrameLayout;

    .local v0, container:Landroid/view/ViewGroup;
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    .local v1, count:I
    const/4 v2, 0x0

    .local v2, hasVisibleChild:Z
    const/4 v3, 0x0

    .local v3, i:I
    :goto_8
    if-ge v3, v1, :cond_17

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v4

    const/16 v5, 0x8

    if-eq v4, v5, :cond_1b

    const/4 v2, 0x1

    :cond_17
    invoke-virtual {p0, v2}, Lmiui/v5/app/MiuiActivity;->setBottomPlaceholderEnabled(Z)V

    return-void

    :cond_1b
    add-int/lit8 v3, v3, 0x1

    goto :goto_8
.end method

.method public updateContentBorder()V
    .registers 2

    .prologue
    invoke-virtual {p0}, Lmiui/v5/app/MiuiActivity;->isBottomPlaceholderEnabled()Z

    move-result v0

    invoke-direct {p0, v0}, Lmiui/v5/app/MiuiActivity;->updateContentBorder(Z)V

    return-void
.end method
