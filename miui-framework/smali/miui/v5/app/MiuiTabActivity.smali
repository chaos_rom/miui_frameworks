.class public Lmiui/v5/app/MiuiTabActivity;
.super Lmiui/v5/app/MiuiActivity;
.source "MiuiTabActivity.java"


# instance fields
.field private final mAdapterChangedListener:Lmiui/v5/widget/TabController$PagerAdapterChangedListener;

.field protected mContainer:Landroid/view/ViewGroup;

.field private final mLayoutObserverFactory:Lmiui/v5/util/Factory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lmiui/v5/util/Factory",
            "<",
            "Lmiui/v5/app/LayoutObserver;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mPageEffectFactory:Lmiui/v5/util/Factory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lmiui/v5/util/Factory",
            "<",
            "Lmiui/v5/widget/PageScrollEffect;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mPageScrollListener:Lmiui/v5/widget/PageChangeAdapter$OnPageScrollListener;

.field private mTabContainer:Lmiui/v5/widget/TabContainerLayout;

.field protected mTabController:Lmiui/v5/widget/TabController;

.field private mViewPager:Lmiui/v5/widget/CooperativeViewPager;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    invoke-direct {p0}, Lmiui/v5/app/MiuiActivity;-><init>()V

    new-instance v0, Lmiui/v5/app/MiuiTabActivity$1;

    invoke-direct {v0, p0}, Lmiui/v5/app/MiuiTabActivity$1;-><init>(Lmiui/v5/app/MiuiTabActivity;)V

    invoke-static {v0}, Lmiui/v5/util/Factory$CachedFactory;->newFactory(Lmiui/v5/util/Factory;)Lmiui/v5/util/Factory$CachedFactory;

    move-result-object v0

    iput-object v0, p0, Lmiui/v5/app/MiuiTabActivity;->mPageEffectFactory:Lmiui/v5/util/Factory;

    new-instance v0, Lmiui/v5/app/MiuiTabActivity$2;

    invoke-direct {v0, p0}, Lmiui/v5/app/MiuiTabActivity$2;-><init>(Lmiui/v5/app/MiuiTabActivity;)V

    iput-object v0, p0, Lmiui/v5/app/MiuiTabActivity;->mPageScrollListener:Lmiui/v5/widget/PageChangeAdapter$OnPageScrollListener;

    new-instance v0, Lmiui/v5/app/MiuiTabActivity$3;

    invoke-direct {v0, p0}, Lmiui/v5/app/MiuiTabActivity$3;-><init>(Lmiui/v5/app/MiuiTabActivity;)V

    iput-object v0, p0, Lmiui/v5/app/MiuiTabActivity;->mAdapterChangedListener:Lmiui/v5/widget/TabController$PagerAdapterChangedListener;

    new-instance v0, Lmiui/v5/app/MiuiTabActivity$4;

    invoke-direct {v0, p0}, Lmiui/v5/app/MiuiTabActivity$4;-><init>(Lmiui/v5/app/MiuiTabActivity;)V

    invoke-static {v0}, Lmiui/v5/util/Factory$CachedFactory;->newFactory(Lmiui/v5/util/Factory;)Lmiui/v5/util/Factory$CachedFactory;

    move-result-object v0

    iput-object v0, p0, Lmiui/v5/app/MiuiTabActivity;->mLayoutObserverFactory:Lmiui/v5/util/Factory;

    return-void
.end method

.method private initTabPlaceholder()V
    .registers 4

    .prologue
    const v2, 0x6010035

    invoke-static {p0, v2}, Lmiui/util/UiUtils;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .local v0, d:Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_1c

    iget-object v2, p0, Lmiui/v5/app/MiuiTabActivity;->mViewPager:Lmiui/v5/widget/CooperativeViewPager;

    invoke-virtual {v2}, Lmiui/v5/widget/CooperativeViewPager;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .local v1, params:Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget-object v2, p0, Lmiui/v5/app/MiuiTabActivity;->mViewPager:Lmiui/v5/widget/CooperativeViewPager;

    invoke-virtual {v2}, Lmiui/v5/widget/CooperativeViewPager;->requestLayout()V

    .end local v1           #params:Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_1c
    return-void
.end method


# virtual methods
.method protected addViewPagerListeners(Lmiui/v5/widget/Views$ComposedPageChangeListener;)V
    .registers 2
    .parameter "listeners"

    .prologue
    return-void
.end method

.method protected createPageScrollEffect(I)Lmiui/v5/widget/PageScrollEffect;
    .registers 4
    .parameter "position"

    .prologue
    iget-object v1, p0, Lmiui/v5/app/MiuiTabActivity;->mTabController:Lmiui/v5/widget/TabController;

    invoke-virtual {v1, p1}, Lmiui/v5/widget/TabController;->getFragment(I)Landroid/app/Fragment;

    move-result-object v0

    .local v0, fragment:Landroid/app/Fragment;
    instance-of v1, v0, Lmiui/v5/widget/PageScrollEffect$Creator;

    if-eqz v1, :cond_11

    check-cast v0, Lmiui/v5/widget/PageScrollEffect$Creator;

    .end local v0           #fragment:Landroid/app/Fragment;
    invoke-interface {v0}, Lmiui/v5/widget/PageScrollEffect$Creator;->createPageScrollEffect()Lmiui/v5/widget/PageScrollEffect;

    move-result-object v1

    :goto_10
    return-object v1

    .restart local v0       #fragment:Landroid/app/Fragment;
    :cond_11
    const/4 v1, 0x0

    goto :goto_10
.end method

.method protected getCurrentPagerItem()Lmiui/v5/app/MiuiViewPagerItem;
    .registers 5

    .prologue
    const/4 v1, 0x0

    iget-object v2, p0, Lmiui/v5/app/MiuiTabActivity;->mTabController:Lmiui/v5/widget/TabController;

    if-nez v2, :cond_6

    :goto_5
    return-object v1

    :cond_6
    iget-object v2, p0, Lmiui/v5/app/MiuiTabActivity;->mTabController:Lmiui/v5/widget/TabController;

    iget-object v3, p0, Lmiui/v5/app/MiuiTabActivity;->mViewPager:Lmiui/v5/widget/CooperativeViewPager;

    invoke-virtual {v3}, Lmiui/v5/widget/CooperativeViewPager;->getCurrentItem()I

    move-result v3

    invoke-virtual {v2, v3}, Lmiui/v5/widget/TabController;->getFragment(I)Landroid/app/Fragment;

    move-result-object v0

    .local v0, current:Landroid/app/Fragment;
    instance-of v2, v0, Lmiui/v5/app/MiuiViewPagerItem;

    if-eqz v2, :cond_1a

    check-cast v0, Lmiui/v5/app/MiuiViewPagerItem;

    .end local v0           #current:Landroid/app/Fragment;
    :goto_18
    move-object v1, v0

    goto :goto_5

    .restart local v0       #current:Landroid/app/Fragment;
    :cond_1a
    move-object v0, v1

    goto :goto_18
.end method

.method protected getLayoutId()I
    .registers 2

    .prologue
    const v0, 0x603004b

    return v0
.end method

.method protected getLayoutObserver()Lmiui/v5/app/LayoutObserver;
    .registers 3

    .prologue
    iget-object v0, p0, Lmiui/v5/app/MiuiTabActivity;->mTabController:Lmiui/v5/widget/TabController;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    :goto_5
    return-object v0

    :cond_6
    iget-object v0, p0, Lmiui/v5/app/MiuiTabActivity;->mLayoutObserverFactory:Lmiui/v5/util/Factory;

    iget-object v1, p0, Lmiui/v5/app/MiuiTabActivity;->mTabController:Lmiui/v5/widget/TabController;

    invoke-virtual {v1}, Lmiui/v5/widget/TabController;->getSelectedPosition()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Lmiui/v5/util/Factory;->create(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/v5/app/LayoutObserver;

    goto :goto_5
.end method

.method public getTabController()Lmiui/v5/widget/TabController;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/app/MiuiTabActivity;->mTabController:Lmiui/v5/widget/TabController;

    return-object v0
.end method

.method public onActionModeFinished(Lmiui/v5/view/MiuiActionMode;)V
    .registers 4
    .parameter "mode"

    .prologue
    iget-object v0, p0, Lmiui/v5/app/MiuiTabActivity;->mTabController:Lmiui/v5/widget/TabController;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmiui/v5/widget/TabController;->setCooperative(Z)V

    invoke-super {p0, p1}, Lmiui/v5/app/MiuiActivity;->onActionModeFinished(Lmiui/v5/view/MiuiActionMode;)V

    return-void
.end method

.method public onActionModeStarted(Lmiui/v5/view/MiuiActionMode;)V
    .registers 4
    .parameter "mode"

    .prologue
    iget-object v0, p0, Lmiui/v5/app/MiuiTabActivity;->mTabController:Lmiui/v5/widget/TabController;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmiui/v5/widget/TabController;->setCooperative(Z)V

    invoke-super {p0, p1}, Lmiui/v5/app/MiuiActivity;->onActionModeStarted(Lmiui/v5/view/MiuiActionMode;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 7
    .parameter "icicle"

    .prologue
    invoke-super {p0, p1}, Lmiui/v5/app/MiuiActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lmiui/v5/app/MiuiTabActivity;->getLayoutId()I

    move-result v2

    invoke-virtual {p0, v2}, Lmiui/v5/app/MiuiTabActivity;->setMiuiContentView(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, p0, Lmiui/v5/app/MiuiTabActivity;->mContainer:Landroid/view/ViewGroup;

    iget-object v2, p0, Lmiui/v5/app/MiuiTabActivity;->mContainer:Landroid/view/ViewGroup;

    const v3, 0x60b0098

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lmiui/v5/widget/TabContainerLayout;

    iput-object v2, p0, Lmiui/v5/app/MiuiTabActivity;->mTabContainer:Lmiui/v5/widget/TabContainerLayout;

    iget-object v2, p0, Lmiui/v5/app/MiuiTabActivity;->mContainer:Landroid/view/ViewGroup;

    const v3, 0x60b0099

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lmiui/v5/widget/CooperativeViewPager;

    iput-object v2, p0, Lmiui/v5/app/MiuiTabActivity;->mViewPager:Lmiui/v5/widget/CooperativeViewPager;

    invoke-direct {p0}, Lmiui/v5/app/MiuiTabActivity;->initTabPlaceholder()V

    new-instance v1, Lmiui/v5/widget/TranslateIndicator;

    const v2, 0x601005e

    invoke-direct {v1, p0, v2}, Lmiui/v5/widget/TranslateIndicator;-><init>(Landroid/content/Context;I)V

    .local v1, ti:Lmiui/v5/widget/TabIndicator;
    new-instance v2, Lmiui/v5/widget/TabController;

    iget-object v3, p0, Lmiui/v5/app/MiuiTabActivity;->mTabContainer:Lmiui/v5/widget/TabContainerLayout;

    iget-object v4, p0, Lmiui/v5/app/MiuiTabActivity;->mViewPager:Lmiui/v5/widget/CooperativeViewPager;

    invoke-direct {v2, p0, v3, v4, v1}, Lmiui/v5/widget/TabController;-><init>(Landroid/app/Activity;Lmiui/v5/widget/TabContainerLayout;Lmiui/v5/widget/CooperativeViewPager;Lmiui/v5/widget/TabIndicator;)V

    iput-object v2, p0, Lmiui/v5/app/MiuiTabActivity;->mTabController:Lmiui/v5/widget/TabController;

    iget-object v2, p0, Lmiui/v5/app/MiuiTabActivity;->mTabController:Lmiui/v5/widget/TabController;

    iget-object v3, p0, Lmiui/v5/app/MiuiTabActivity;->mAdapterChangedListener:Lmiui/v5/widget/TabController$PagerAdapterChangedListener;

    invoke-virtual {v2, v3}, Lmiui/v5/widget/TabController;->setPagerAdapterChangedListener(Lmiui/v5/widget/TabController$PagerAdapterChangedListener;)V

    new-instance v0, Lmiui/v5/widget/Views$ComposedPageChangeListener;

    invoke-direct {v0}, Lmiui/v5/widget/Views$ComposedPageChangeListener;-><init>()V

    .local v0, listeners:Lmiui/v5/widget/Views$ComposedPageChangeListener;
    invoke-virtual {p0, v0}, Lmiui/v5/app/MiuiTabActivity;->addViewPagerListeners(Lmiui/v5/widget/Views$ComposedPageChangeListener;)V

    iget-object v2, p0, Lmiui/v5/app/MiuiTabActivity;->mViewPager:Lmiui/v5/widget/CooperativeViewPager;

    iget-object v3, p0, Lmiui/v5/app/MiuiTabActivity;->mPageEffectFactory:Lmiui/v5/util/Factory;

    invoke-static {v2, v3}, Lmiui/v5/widget/PageScrollEffects;->makePageChangeAdapter(Landroid/support/v4/view/ViewPager;Lmiui/v5/util/Factory;)Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    move-result-object v2

    invoke-virtual {v0, v2}, Lmiui/v5/widget/Views$ComposedPageChangeListener;->add(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    new-instance v2, Lmiui/v5/widget/PageChangeAdapter;

    iget-object v3, p0, Lmiui/v5/app/MiuiTabActivity;->mViewPager:Lmiui/v5/widget/CooperativeViewPager;

    iget-object v4, p0, Lmiui/v5/app/MiuiTabActivity;->mPageScrollListener:Lmiui/v5/widget/PageChangeAdapter$OnPageScrollListener;

    invoke-direct {v2, v3, v4}, Lmiui/v5/widget/PageChangeAdapter;-><init>(Landroid/support/v4/view/ViewPager;Lmiui/v5/widget/PageChangeAdapter$OnPageScrollListener;)V

    invoke-virtual {v0, v2}, Lmiui/v5/widget/Views$ComposedPageChangeListener;->add(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    iget-object v2, p0, Lmiui/v5/app/MiuiTabActivity;->mTabController:Lmiui/v5/widget/TabController;

    invoke-virtual {v2, v0}, Lmiui/v5/widget/TabController;->setViewPagerListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    return-void
.end method

.method protected onViewPagerAdapterChanged(Z)V
    .registers 3
    .parameter "selectedChanged"

    .prologue
    if-eqz p1, :cond_b

    iget-object v0, p0, Lmiui/v5/app/MiuiTabActivity;->mViewPager:Lmiui/v5/widget/CooperativeViewPager;

    invoke-virtual {v0}, Lmiui/v5/widget/CooperativeViewPager;->getCurrentItem()I

    move-result v0

    invoke-virtual {p0, v0}, Lmiui/v5/app/MiuiTabActivity;->updateMenuBar(I)V

    :cond_b
    return-void
.end method

.method protected onViewPagerReset(Landroid/support/v4/view/ViewPager;II)V
    .registers 4
    .parameter "pager"
    .parameter "from"
    .parameter "to"

    .prologue
    if-eq p2, p3, :cond_5

    invoke-virtual {p0, p3}, Lmiui/v5/app/MiuiTabActivity;->updateMenuBar(I)V

    :cond_5
    return-void
.end method

.method protected onViewPagerScroll(Landroid/support/v4/view/ViewPager;IIF)V
    .registers 5
    .parameter "pager"
    .parameter "from"
    .parameter "to"
    .parameter "percent"

    .prologue
    return-void
.end method

.method public updateMenuBar()V
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/app/MiuiTabActivity;->mViewPager:Lmiui/v5/widget/CooperativeViewPager;

    invoke-virtual {v0}, Lmiui/v5/widget/CooperativeViewPager;->getCurrentItem()I

    move-result v0

    invoke-virtual {p0, v0}, Lmiui/v5/app/MiuiTabActivity;->updateMenuBar(I)V

    return-void
.end method

.method protected updateMenuBar(I)V
    .registers 3
    .parameter "position"

    .prologue
    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mMenuBar:Lmiui/v5/widget/menubar/MenuBar;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lmiui/v5/app/MiuiActivity;->mMenuBar:Lmiui/v5/widget/menubar/MenuBar;

    invoke-virtual {v0}, Lmiui/v5/widget/menubar/MenuBar;->reopen()V

    :cond_9
    return-void
.end method
