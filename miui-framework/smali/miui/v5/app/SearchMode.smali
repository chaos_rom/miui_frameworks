.class public Lmiui/v5/app/SearchMode;
.super Ljava/lang/Object;
.source "SearchMode.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/v5/app/SearchMode$SearchViewLayoutListener;,
        Lmiui/v5/app/SearchMode$InputDownAnim;,
        Lmiui/v5/app/SearchMode$InputUpAnim;,
        Lmiui/v5/app/SearchMode$FinishActionModeClick;,
        Lmiui/v5/app/SearchMode$Token;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getAnchor(Landroid/view/ViewGroup;Landroid/view/View;)I
    .registers 6
    .parameter "root"
    .parameter "v"

    .prologue
    const/4 v3, 0x1

    if-nez p1, :cond_5

    const/4 v2, 0x0

    :goto_4
    return v2

    :cond_5
    const/4 v2, 0x2

    new-array v0, v2, [I

    .local v0, location:[I
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->getLocationInWindow([I)V

    aget v1, v0, v3

    .local v1, rootTop:I
    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationInWindow([I)V

    aget v2, v0, v3

    sub-int/2addr v2, v1

    goto :goto_4
.end method

.method public static inflateSearchView(Landroid/content/Context;Landroid/view/ViewGroup;Z)Landroid/view/View;
    .registers 4
    .parameter "context"
    .parameter "parent"
    .parameter "attachToRoot"

    .prologue
    const v0, 0x6030066

    invoke-static {p0, v0, p1, p2}, Lmiui/v5/widget/Views;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static setHeight(Landroid/view/View;I)V
    .registers 3
    .parameter "v"
    .parameter "height"

    .prologue
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .local v0, params:Landroid/view/ViewGroup$LayoutParams;
    if-eqz v0, :cond_b

    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    :cond_b
    return-void
.end method

.method static setLeft(Landroid/view/View;I)V
    .registers 3
    .parameter "v"
    .parameter "left"

    .prologue
    invoke-virtual {p0, p1}, Landroid/view/View;->setLeft(I)V

    invoke-virtual {p0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .local v0, tag:Ljava/lang/Object;
    if-eqz v0, :cond_d

    check-cast v0, Landroid/graphics/Rect;

    .end local v0           #tag:Ljava/lang/Object;
    iput p1, v0, Landroid/graphics/Rect;->left:I

    :cond_d
    return-void
.end method

.method public static startSearchMode(Lmiui/v5/view/ActionModeWrapper;Landroid/view/View;Landroid/view/View;Ljava/lang/CharSequence;Lmiui/v5/view/MiuiActionMode$ActionModeListener;)Lmiui/v5/app/SearchMode$Token;
    .registers 36
    .parameter "wrapper"
    .parameter "anchorView"
    .parameter "animView"
    .parameter "hint"
    .parameter "l"

    .prologue
    const/16 v26, 0x0

    .local v26, firstInit:Z
    const v10, 0x60b00bb

    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lmiui/v5/view/ActionModeWrapper;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .local v5, searchView:Landroid/view/View;
    if-nez v5, :cond_27

    const/16 v26, 0x1

    invoke-virtual/range {p0 .. p0}, Lmiui/v5/view/ActionModeWrapper;->getContext()Landroid/content/Context;

    move-result-object v10

    const v16, 0x6030065

    const/16 v17, 0x0

    move/from16 v0, v16

    move-object/from16 v1, p0

    move/from16 v2, v17

    invoke-static {v10, v0, v1, v2}, Lmiui/v5/widget/Views;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lmiui/v5/view/ActionModeWrapper;->addView(Landroid/view/View;)V

    :cond_27
    const v10, 0x1020004

    invoke-virtual {v5, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .local v8, alphaView:Landroid/view/View;
    const v10, 0x1020009

    invoke-virtual {v5, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/EditText;

    .local v6, editView:Landroid/widget/EditText;
    const v10, 0x102002c

    invoke-virtual {v5, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .local v7, backView:Landroid/view/View;
    if-eqz v26, :cond_48

    new-instance v10, Lmiui/v5/app/SearchMode$SearchViewLayoutListener;

    invoke-direct {v10}, Lmiui/v5/app/SearchMode$SearchViewLayoutListener;-><init>()V

    invoke-virtual {v6, v10}, Landroid/widget/EditText;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    :cond_48
    new-instance v4, Lmiui/v5/app/SearchMode$Token;

    move-object/from16 v9, p2

    invoke-direct/range {v4 .. v9}, Lmiui/v5/app/SearchMode$Token;-><init>(Landroid/view/View;Landroid/widget/EditText;Landroid/view/View;Landroid/view/View;Landroid/view/View;)V

    .local v4, token:Lmiui/v5/app/SearchMode$Token;
    const-wide/16 v24, 0x12c

    .local v24, dur:J
    const-wide/16 v21, 0x32

    .local v21, appearDelay:J
    invoke-static/range {p0 .. p1}, Lmiui/v5/app/SearchMode;->getAnchor(Landroid/view/ViewGroup;Landroid/view/View;)I

    move-result v11

    .local v11, anchorDist:I
    invoke-static {v7}, Lmiui/v5/widget/Views;->getBackgroundWidth(Landroid/view/View;)I

    move-result v12

    .local v12, backViewWidth:I
    invoke-virtual {v6}, Landroid/widget/EditText;->getParent()Landroid/view/ViewParent;

    move-result-object v10

    check-cast v10, Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->getPaddingLeft()I

    move-result v13

    .local v13, editAnimStart:I
    sub-int v14, v12, v13

    .local v14, editAnimWidth:I
    const/4 v10, 0x2

    new-array v10, v10, [F

    fill-array-data v10, :array_11c

    invoke-static {v10}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v29

    .local v29, inputUp:Landroid/animation/ValueAnimator;
    const-wide/16 v16, 0x12c

    move-object/from16 v0, v29

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    new-instance v10, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v10}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    move-object/from16 v0, v29

    invoke-virtual {v0, v10}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    const-wide/16 v16, 0x32

    move-object/from16 v0, v29

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    new-instance v9, Lmiui/v5/app/SearchMode$InputUpAnim;

    move-object v10, v4

    invoke-direct/range {v9 .. v14}, Lmiui/v5/app/SearchMode$InputUpAnim;-><init>(Lmiui/v5/app/SearchMode$Token;IIII)V

    .local v9, startListener:Lmiui/v5/app/SearchMode$InputUpAnim;
    move-object/from16 v0, v29

    invoke-virtual {v0, v9}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    move-object/from16 v0, v29

    invoke-virtual {v0, v9}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    const/4 v10, 0x2

    new-array v10, v10, [F

    fill-array-data v10, :array_124

    invoke-static {v10}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v28

    .local v28, inputDown:Landroid/animation/ValueAnimator;
    const-wide/16 v16, 0x12c

    move-object/from16 v0, v28

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    new-instance v10, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v10}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    move-object/from16 v0, v28

    invoke-virtual {v0, v10}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    new-instance v15, Lmiui/v5/app/SearchMode$InputDownAnim;

    move-object/from16 v16, v4

    move/from16 v17, v11

    move/from16 v18, v12

    move/from16 v19, v13

    move/from16 v20, v14

    invoke-direct/range {v15 .. v20}, Lmiui/v5/app/SearchMode$InputDownAnim;-><init>(Lmiui/v5/app/SearchMode$Token;IIII)V

    .local v15, finishListener:Lmiui/v5/app/SearchMode$InputDownAnim;
    move-object/from16 v0, v28

    invoke-virtual {v0, v15}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    move-object/from16 v0, v28

    invoke-virtual {v0, v15}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    move-object/from16 v2, v28

    move-object/from16 v3, p4

    invoke-virtual {v0, v1, v2, v3}, Lmiui/v5/view/ActionModeWrapper;->startAnimatorActionMode(Landroid/animation/Animator;Landroid/animation/Animator;Lmiui/v5/view/MiuiActionMode$ActionModeListener;)Landroid/view/ActionMode;

    move-result-object v30

    .local v30, mode:Landroid/view/ActionMode;
    if-eqz v30, :cond_119

    new-instance v23, Lmiui/v5/app/SearchMode$FinishActionModeClick;

    move-object/from16 v0, v23

    invoke-direct {v0, v4}, Lmiui/v5/app/SearchMode$FinishActionModeClick;-><init>(Lmiui/v5/app/SearchMode$Token;)V

    .local v23, clickListener:Landroid/view/View$OnClickListener;
    move-object/from16 v0, v23

    invoke-virtual {v8, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, v23

    invoke-virtual {v7, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/16 v10, 0x8

    invoke-virtual {v5, v10}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p3

    invoke-virtual {v6, v0}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    invoke-virtual {v6}, Landroid/widget/EditText;->requestFocus()Z

    invoke-virtual {v6}, Landroid/widget/EditText;->getContext()Landroid/content/Context;

    move-result-object v10

    const-string v16, "input_method"

    move-object/from16 v0, v16

    invoke-virtual {v10, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Landroid/view/inputmethod/InputMethodManager;

    .local v27, imm:Landroid/view/inputmethod/InputMethodManager;
    const/4 v10, 0x2

    move-object/from16 v0, v27

    invoke-virtual {v0, v6, v10}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    move-object/from16 v0, v30

    invoke-virtual {v4, v0}, Lmiui/v5/app/SearchMode$Token;->setActionMode(Landroid/view/ActionMode;)V

    .end local v4           #token:Lmiui/v5/app/SearchMode$Token;
    .end local v23           #clickListener:Landroid/view/View$OnClickListener;
    .end local v27           #imm:Landroid/view/inputmethod/InputMethodManager;
    :goto_118
    return-object v4

    .restart local v4       #token:Lmiui/v5/app/SearchMode$Token;
    :cond_119
    const/4 v4, 0x0

    goto :goto_118

    nop

    :array_11c
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
    .end array-data

    :array_124
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
    .end array-data
.end method
