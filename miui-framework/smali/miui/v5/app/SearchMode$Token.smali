.class public Lmiui/v5/app/SearchMode$Token;
.super Ljava/lang/Object;
.source "SearchMode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/v5/app/SearchMode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Token"
.end annotation


# instance fields
.field mActionMode:Landroid/view/ActionMode;

.field public final mAlphaView:Landroid/view/View;

.field final mAnimView:Landroid/view/View;

.field mAnimationCount:I

.field public final mBackView:Landroid/view/View;

.field public final mEditText:Landroid/widget/EditText;

.field public final mSearchView:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/view/View;Landroid/widget/EditText;Landroid/view/View;Landroid/view/View;Landroid/view/View;)V
    .registers 6
    .parameter "searchView"
    .parameter "editView"
    .parameter "backView"
    .parameter "alphaView"
    .parameter "animView"

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmiui/v5/app/SearchMode$Token;->mSearchView:Landroid/view/View;

    iput-object p2, p0, Lmiui/v5/app/SearchMode$Token;->mEditText:Landroid/widget/EditText;

    iput-object p3, p0, Lmiui/v5/app/SearchMode$Token;->mBackView:Landroid/view/View;

    iput-object p4, p0, Lmiui/v5/app/SearchMode$Token;->mAlphaView:Landroid/view/View;

    iput-object p5, p0, Lmiui/v5/app/SearchMode$Token;->mAnimView:Landroid/view/View;

    return-void
.end method


# virtual methods
.method public finish()V
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/v5/app/SearchMode$Token;->mActionMode:Landroid/view/ActionMode;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lmiui/v5/app/SearchMode$Token;->mActionMode:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    :cond_9
    return-void
.end method

.method public isAnimationPlaying()Z
    .registers 2

    .prologue
    iget v0, p0, Lmiui/v5/app/SearchMode$Token;->mAnimationCount:I

    if-lez v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method setActionMode(Landroid/view/ActionMode;)V
    .registers 2
    .parameter "mode"

    .prologue
    iput-object p1, p0, Lmiui/v5/app/SearchMode$Token;->mActionMode:Landroid/view/ActionMode;

    return-void
.end method
