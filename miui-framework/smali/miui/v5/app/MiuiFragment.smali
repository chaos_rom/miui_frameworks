.class public Lmiui/v5/app/MiuiFragment;
.super Landroid/app/Fragment;
.source "MiuiFragment.java"

# interfaces
.implements Lmiui/v5/app/MiuiViewPagerItem;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    return-void
.end method


# virtual methods
.method public createLayoutObserver()Lmiui/v5/app/LayoutObserver;
    .registers 2

    .prologue
    const/4 v0, 0x0

    return-object v0
.end method

.method public createMotionDetectStrategy()Lmiui/v5/widget/MotionDetectStrategy;
    .registers 2

    .prologue
    const/4 v0, 0x0

    return-object v0
.end method

.method public createPageScrollEffect()Lmiui/v5/widget/PageScrollEffect;
    .registers 2

    .prologue
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreateMenuBar(Landroid/view/Menu;Landroid/view/MenuInflater;)Z
    .registers 4
    .parameter "menu"
    .parameter "inflater"

    .prologue
    const/4 v0, 0x0

    return v0
.end method

.method public onMenuBarClose(Landroid/view/Menu;)V
    .registers 2
    .parameter "menu"

    .prologue
    return-void
.end method

.method public onMenuBarItemSelected(Landroid/view/MenuItem;)Z
    .registers 3
    .parameter "item"

    .prologue
    const/4 v0, 0x0

    return v0
.end method

.method public onMenuBarModeChange(Landroid/view/Menu;I)V
    .registers 3
    .parameter "menu"
    .parameter "mode"

    .prologue
    return-void
.end method

.method public onMenuBarOpen(Landroid/view/Menu;)V
    .registers 2
    .parameter "menu"

    .prologue
    return-void
.end method

.method public onPrepareMenuBar(Landroid/view/Menu;)Z
    .registers 3
    .parameter "menu"

    .prologue
    const/4 v0, 0x1

    return v0
.end method
