.class public Lmiui/net/CloudManager;
.super Ljava/lang/Object;
.source "CloudManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/net/CloudManager$PhoneInfo;,
        Lmiui/net/CloudManager$DevSettingName;,
        Lmiui/net/CloudManager$CloudManagerFuture;,
        Lmiui/net/CloudManager$CmTask;
    }
.end annotation


# static fields
.field public static final ACTIVATE_STATUS_ACTIVATED:I = 0x2

.field public static final ACTIVATE_STATUS_ACTIVATING:I = 0x1

.field public static final ACTIVATE_STATUS_UNACTIVATED:I = 0x3

.field private static final CARRIER_SELECTOR:Lmiui/telephony/CarrierSelector; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lmiui/telephony/CarrierSelector",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final DEBUG:Z = true

.field public static final DEVICE_INFO_TOKEN:Ljava/lang/String; = "deviceinfo"

.field public static final ERROR_CODE_ACTIVATE_TIMEOUT:I = 0x2

.field public static final ERROR_CODE_NO_ACCOUNT:I = 0x3

.field public static final ERROR_CODE_SEND_SMS_FAILURE:I = 0x1

.field public static final ERROR_SIM_NOT_ACTIVATED:I = 0x1

.field private static final INT_0:Ljava/lang/Integer; = null

.field public static final KEY_ACTIVATE_PHONE:Ljava/lang/String; = "activate_phone"

.field public static final KEY_ACTIVATE_STATUS:Ljava/lang/String; = "activate_status"

.field public static final KEY_FIND_DEVICE_TOKEN:Ljava/lang/String; = "find_device_token"

.field public static final KEY_PASSTOKEN:Ljava/lang/String; = "pass_token"

.field public static final KEY_PHONE_TICKET:Ljava/lang/String; = "phone_ticket"

.field public static final KEY_RESULT:Ljava/lang/String; = "result"

.field public static final KEY_SECONDARY_SYNC_ON:Ljava/lang/String; = "secondary_sync_on"

.field public static final KEY_SMS_GATEWAY_CHINA_MOBILE:Ljava/lang/String; = "sms_gw_china_mobile"

.field public static final KEY_SMS_GATEWAY_CHINA_TELECOM:Ljava/lang/String; = "sms_gw_china_telecom"

.field public static final KEY_SMS_GATEWAY_CHINA_UNICOM:Ljava/lang/String; = "sms_gw_china_unicom"

.field public static final KEY_SMS_GATEWAY_GLOBAL:Ljava/lang/String; = "sms_gw_global"

.field public static final KEY_USER_SECURITY:Ljava/lang/String; = "user_security"

.field public static final KEY_USER_TOKEN:Ljava/lang/String; = "user_token"

.field public static final NOTIFICATION_ACTIVATE_ERROR:I = 0x10000001

.field public static final SMS_GW_CM:Ljava/lang/String; = "106571203855788"

.field public static final SMS_GW_CT:Ljava/lang/String; = "10659057100335678"

.field public static final SMS_GW_CU:Ljava/lang/String; = "1065507729555678"

.field public static final SMS_GW_GLOBAL:Ljava/lang/String; = "+447786209730"

.field private static final TAG:Ljava/lang/String; = "CloudManager"

.field public static final URL_ACCOUNT_BASE:Ljava/lang/String; = "https://account.xiaomi.com/pass"

.field public static final URL_ACCOUNT_SAFE_API_BASE:Ljava/lang/String; = "http://api.account.xiaomi.com/pass/v2/safe"

.field public static final URL_ACOUNT_API_BASE:Ljava/lang/String; = "http://api.account.xiaomi.com/pass"

.field public static final URL_CONTACT_BASE:Ljava/lang/String; = "http://api.micloud.xiaomi.net"

.field public static final URL_DEV_BASE:Ljava/lang/String; = "http://api.device.xiaomi.net"

.field public static final URL_DEV_SETTING:Ljava/lang/String; = "/udi/v1/user/%s/device/%s/setting"

.field public static final URL_FIND_DEVICE_BASE:Ljava/lang/String; = "http://api.micloud.xiaomi.net"

.field public static final URL_GALLERY_BASE:Ljava/lang/String; = "http://galleryapi.micloud.xiaomi.net"

.field public static final URL_MMS_BASE:Ljava/lang/String; = "http://api.micloud.xiaomi.net"

.field public static final URL_RICH_MEDIA_BASE:Ljava/lang/String; = "http://fileapi.micloud.xiaomi.net"

.field public static final URL_WIFI_BASE:Ljava/lang/String; = "http://api.micloud.xiaomi.net"

.field private static mUserAgent:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    new-instance v0, Lmiui/telephony/CarrierSelector;

    invoke-direct {v0}, Lmiui/telephony/CarrierSelector;-><init>()V

    sput-object v0, Lmiui/net/CloudManager;->CARRIER_SELECTOR:Lmiui/telephony/CarrierSelector;

    sget-object v0, Lmiui/net/CloudManager;->CARRIER_SELECTOR:Lmiui/telephony/CarrierSelector;

    sget-object v1, Lmiui/telephony/CarrierSelector$CARRIER;->CHINA_MOBILE:Lmiui/telephony/CarrierSelector$CARRIER;

    const-string v2, "106571203855788"

    invoke-virtual {v0, v1, v2}, Lmiui/telephony/CarrierSelector;->register(Lmiui/telephony/CarrierSelector$CARRIER;Ljava/lang/Object;)V

    sget-object v0, Lmiui/net/CloudManager;->CARRIER_SELECTOR:Lmiui/telephony/CarrierSelector;

    sget-object v1, Lmiui/telephony/CarrierSelector$CARRIER;->CHINA_UNICOM:Lmiui/telephony/CarrierSelector$CARRIER;

    const-string v2, "1065507729555678"

    invoke-virtual {v0, v1, v2}, Lmiui/telephony/CarrierSelector;->register(Lmiui/telephony/CarrierSelector$CARRIER;Ljava/lang/Object;)V

    sget-object v0, Lmiui/net/CloudManager;->CARRIER_SELECTOR:Lmiui/telephony/CarrierSelector;

    sget-object v1, Lmiui/telephony/CarrierSelector$CARRIER;->CHINA_TELECOM:Lmiui/telephony/CarrierSelector$CARRIER;

    const-string v2, "10659057100335678"

    invoke-virtual {v0, v1, v2}, Lmiui/telephony/CarrierSelector;->register(Lmiui/telephony/CarrierSelector$CARRIER;Ljava/lang/Object;)V

    sget-object v0, Lmiui/net/CloudManager;->CARRIER_SELECTOR:Lmiui/telephony/CarrierSelector;

    sget-object v1, Lmiui/telephony/CarrierSelector$CARRIER;->DEFAULT:Lmiui/telephony/CarrierSelector$CARRIER;

    const-string v2, "+447786209730"

    invoke-virtual {v0, v1, v2}, Lmiui/telephony/CarrierSelector;->register(Lmiui/telephony/CarrierSelector$CARRIER;Ljava/lang/Object;)V

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lmiui/net/CloudManager;->INT_0:Ljava/lang/Integer;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmiui/net/CloudManager;->mContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$000(Lmiui/net/CloudManager;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/net/CloudManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public static get(Landroid/content/Context;)Lmiui/net/CloudManager;
    .registers 2
    .parameter "context"

    .prologue
    new-instance v0, Lmiui/net/CloudManager;

    invoke-direct {v0, p0}, Lmiui/net/CloudManager;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public static getDeviceId(Landroid/content/Context;)Ljava/lang/String;
    .registers 3
    .parameter "context"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lmiui/telephony/exception/IllegalDeviceException;
        }
    .end annotation

    .prologue
    invoke-static {p0}, Lmiui/telephony/ExtraTelephonyManager;->blockingGetDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .local v0, deviceId:Ljava/lang/String;
    invoke-static {v0}, Lmiui/net/CloudCoder;->hashDeviceInfo(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getResourceId(Landroid/content/Context;)Ljava/lang/String;
    .registers 2
    .parameter "context"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lmiui/telephony/exception/IllegalDeviceException;
        }
    .end annotation

    .prologue
    invoke-static {p0}, Lmiui/net/CloudManager;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getUserAgent()Ljava/lang/String;
    .registers 2

    .prologue
    sget-object v1, Lmiui/net/CloudManager;->mUserAgent:Ljava/lang/String;

    if-nez v1, :cond_1e

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .local v0, sb:Ljava/lang/StringBuilder;
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "; MIUI/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lmiui/net/CloudManager;->mUserAgent:Ljava/lang/String;

    :cond_1e
    sget-object v1, Lmiui/net/CloudManager;->mUserAgent:Ljava/lang/String;

    return-object v1
.end method

.method public static isSimSupported(Landroid/content/Context;)Z
    .registers 10
    .parameter "context"

    .prologue
    const/4 v8, 0x3

    const/4 v4, 0x1

    const/4 v5, 0x0

    const-string v6, "phone"

    invoke-virtual {p0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/telephony/TelephonyManager;

    .local v3, tm:Landroid/telephony/TelephonyManager;
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v1

    .local v1, mccmnc:Ljava/lang/String;
    if-nez v1, :cond_12

    :cond_11
    :goto_11
    return v4

    :cond_12
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v6

    const/4 v7, 0x5

    if-eq v6, v7, :cond_3a

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v6

    const/4 v7, 0x6

    if-eq v6, v7, :cond_3a

    const-string v4, "CloudManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "unsupported mcc mnc: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v5

    goto :goto_11

    :cond_3a
    invoke-virtual {v1, v5, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .local v0, mcc:Ljava/lang/String;
    invoke-virtual {v1, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .local v2, mnc:Ljava/lang/String;
    sget-object v6, Lmiui/net/CloudManager;->CARRIER_SELECTOR:Lmiui/telephony/CarrierSelector;

    invoke-virtual {v6, v0, v2}, Lmiui/telephony/CarrierSelector;->selectValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    if-nez v6, :cond_11

    move v4, v5

    goto :goto_11
.end method

.method public static selectSmsGateway(Landroid/content/Context;)Ljava/lang/String;
    .registers 16
    .parameter "context"

    .prologue
    const/4 v14, 0x3

    const-string v11, "phone"

    invoke-virtual {p0, v11}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/telephony/TelephonyManager;

    .local v10, tm:Landroid/telephony/TelephonyManager;
    invoke-static {p0}, Lmiui/net/CloudManager;->get(Landroid/content/Context;)Lmiui/net/CloudManager;

    move-result-object v0

    .local v0, cloudMgr:Lmiui/net/CloudManager;
    :try_start_d
    invoke-virtual {v0}, Lmiui/net/CloudManager;->getSmsGateway()Lmiui/net/CloudManager$CloudManagerFuture;

    move-result-object v11

    invoke-interface {v11}, Lmiui/net/CloudManager$CloudManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/os/Bundle;

    .local v9, result:Landroid/os/Bundle;
    const-string v11, "sms_gw_china_mobile"

    invoke-virtual {v9, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .local v1, cm:Ljava/lang/String;
    const-string v11, "sms_gw_china_unicom"

    invoke-virtual {v9, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .local v3, cu:Ljava/lang/String;
    const-string v11, "sms_gw_china_telecom"

    invoke-virtual {v9, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .local v2, ct:Ljava/lang/String;
    const-string v11, "sms_gw_global"

    invoke-virtual {v9, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .local v5, global:Ljava/lang/String;
    sget-object v11, Lmiui/net/CloudManager;->CARRIER_SELECTOR:Lmiui/telephony/CarrierSelector;

    sget-object v12, Lmiui/telephony/CarrierSelector$CARRIER;->CHINA_MOBILE:Lmiui/telephony/CarrierSelector$CARRIER;

    invoke-virtual {v11, v12, v1}, Lmiui/telephony/CarrierSelector;->register(Lmiui/telephony/CarrierSelector$CARRIER;Ljava/lang/Object;)V

    sget-object v11, Lmiui/net/CloudManager;->CARRIER_SELECTOR:Lmiui/telephony/CarrierSelector;

    sget-object v12, Lmiui/telephony/CarrierSelector$CARRIER;->CHINA_UNICOM:Lmiui/telephony/CarrierSelector$CARRIER;

    invoke-virtual {v11, v12, v3}, Lmiui/telephony/CarrierSelector;->register(Lmiui/telephony/CarrierSelector$CARRIER;Ljava/lang/Object;)V

    sget-object v11, Lmiui/net/CloudManager;->CARRIER_SELECTOR:Lmiui/telephony/CarrierSelector;

    sget-object v12, Lmiui/telephony/CarrierSelector$CARRIER;->CHINA_TELECOM:Lmiui/telephony/CarrierSelector$CARRIER;

    invoke-virtual {v11, v12, v2}, Lmiui/telephony/CarrierSelector;->register(Lmiui/telephony/CarrierSelector$CARRIER;Ljava/lang/Object;)V

    sget-object v11, Lmiui/net/CloudManager;->CARRIER_SELECTOR:Lmiui/telephony/CarrierSelector;

    sget-object v12, Lmiui/telephony/CarrierSelector$CARRIER;->DEFAULT:Lmiui/telephony/CarrierSelector$CARRIER;

    invoke-virtual {v11, v12, v5}, Lmiui/telephony/CarrierSelector;->register(Lmiui/telephony/CarrierSelector$CARRIER;Ljava/lang/Object;)V
    :try_end_4b
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_4b} :catch_8a

    .end local v1           #cm:Ljava/lang/String;
    .end local v2           #ct:Ljava/lang/String;
    .end local v3           #cu:Ljava/lang/String;
    .end local v5           #global:Ljava/lang/String;
    .end local v9           #result:Landroid/os/Bundle;
    :goto_4b
    invoke-virtual {v10}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v7

    .local v7, mccmnc:Ljava/lang/String;
    const-string v11, "CloudManager"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "device mccmnc:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v7, :cond_70

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v11

    const/4 v12, 0x5

    if-ge v11, v12, :cond_93

    :cond_70
    const-string v11, "CloudManager"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "illegal mcc mnc: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v11, 0x0

    :goto_89
    return-object v11

    .end local v7           #mccmnc:Ljava/lang/String;
    :catch_8a
    move-exception v4

    .local v4, e:Ljava/lang/Exception;
    const-string v11, "CloudManager"

    const-string v12, "error when get sms gateway"

    invoke-static {v11, v12, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4b

    .end local v4           #e:Ljava/lang/Exception;
    .restart local v7       #mccmnc:Ljava/lang/String;
    :cond_93
    const/4 v11, 0x0

    invoke-virtual {v7, v11, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .local v6, mcc:Ljava/lang/String;
    invoke-virtual {v7, v14}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    .local v8, mnc:Ljava/lang/String;
    sget-object v11, Lmiui/net/CloudManager;->CARRIER_SELECTOR:Lmiui/telephony/CarrierSelector;

    const/4 v12, 0x1

    invoke-virtual {v11, v6, v8, v12}, Lmiui/telephony/CarrierSelector;->selectValue(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    goto :goto_89
.end method

.method public static uploadDeviceInfo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Z
    .registers 22
    .parameter "context"
    .parameter "userId"
    .parameter "token"
    .parameter "security"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lmiui/net/exception/AuthenticationFailureException;
        }
    .end annotation

    .prologue
    .local p4, devSettings:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    :try_start_0
    invoke-static/range {p0 .. p0}, Lmiui/net/CloudManager;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .local v5, devId:Ljava/lang/String;
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "http://api.device.xiaomi.net"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/udi/v1/user/%s/device/%s/setting"

    const/4 v15, 0x2

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object p1, v15, v16

    const/16 v16, 0x1

    aput-object v5, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .local v12, url:Ljava/lang/String;
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .local v3, cookies:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v13, "serviceToken"

    move-object/from16 v0, p2

    invoke-interface {v3, v13, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v13, "userId"

    move-object/from16 v0, p1

    invoke-interface {v3, v13, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    .local v10, params:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .local v2, content:Lorg/json/JSONArray;
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, i$:Ljava/util/Iterator;
    :goto_49
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_76

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/http/NameValuePair;

    .local v6, devSetting:Lorg/apache/http/NameValuePair;
    new-instance v9, Lorg/json/JSONObject;

    invoke-direct {v9}, Lorg/json/JSONObject;-><init>()V

    .local v9, o:Lorg/json/JSONObject;
    const-string v13, "name"

    invoke-interface {v6}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v9, v13, v14}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v13, "value"

    invoke-interface {v6}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v9, v13, v14}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    invoke-virtual {v2, v9}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_6f
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_6f} :catch_70
    .catch Lmiui/net/exception/AccessDeniedException; {:try_start_0 .. :try_end_6f} :catch_bf
    .catch Lmiui/net/exception/InvalidResponseException; {:try_start_0 .. :try_end_6f} :catch_c4
    .catch Lmiui/net/exception/CipherException; {:try_start_0 .. :try_end_6f} :catch_c9
    .catch Lmiui/telephony/exception/IllegalDeviceException; {:try_start_0 .. :try_end_6f} :catch_ce
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_6f} :catch_d3

    goto :goto_49

    .end local v2           #content:Lorg/json/JSONArray;
    .end local v3           #cookies:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v5           #devId:Ljava/lang/String;
    .end local v6           #devSetting:Lorg/apache/http/NameValuePair;
    .end local v8           #i$:Ljava/util/Iterator;
    .end local v9           #o:Lorg/json/JSONObject;
    .end local v10           #params:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v12           #url:Ljava/lang/String;
    :catch_70
    move-exception v7

    .local v7, e:Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    .end local v7           #e:Ljava/io/IOException;
    :goto_74
    const/4 v13, 0x0

    :goto_75
    return v13

    .restart local v2       #content:Lorg/json/JSONArray;
    .restart local v3       #cookies:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v5       #devId:Ljava/lang/String;
    .restart local v8       #i$:Ljava/util/Iterator;
    .restart local v10       #params:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v12       #url:Ljava/lang/String;
    :cond_76
    :try_start_76
    const-string v13, "content"

    invoke-virtual {v2}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-interface {v10, v13, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v13, 0x1

    move-object/from16 v0, p3

    invoke-static {v12, v10, v3, v13, v0}, Lmiui/net/SecureRequest;->postAsMap(Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;ZLjava/lang/String;)Lmiui/net/SimpleRequest$MapContent;

    move-result-object v11

    .local v11, resp:Lmiui/net/SimpleRequest$MapContent;
    const-string v13, "code"

    invoke-virtual {v11, v13}, Lmiui/net/SimpleRequest$MapContent;->getFromBody(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .local v1, code:Ljava/lang/Object;
    const-string v13, "description"

    invoke-virtual {v11, v13}, Lmiui/net/SimpleRequest$MapContent;->getFromBody(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    .local v4, description:Ljava/lang/Object;
    sget-object v13, Lmiui/net/CloudManager;->INT_0:Ljava/lang/Integer;

    invoke-virtual {v13, v1}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_9c

    const/4 v13, 0x1

    goto :goto_75

    :cond_9c
    const-string v13, "CloudManager"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "failed upload dev settings, code: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "; description: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_be
    .catch Ljava/io/IOException; {:try_start_76 .. :try_end_be} :catch_70
    .catch Lmiui/net/exception/AccessDeniedException; {:try_start_76 .. :try_end_be} :catch_bf
    .catch Lmiui/net/exception/InvalidResponseException; {:try_start_76 .. :try_end_be} :catch_c4
    .catch Lmiui/net/exception/CipherException; {:try_start_76 .. :try_end_be} :catch_c9
    .catch Lmiui/telephony/exception/IllegalDeviceException; {:try_start_76 .. :try_end_be} :catch_ce
    .catch Lorg/json/JSONException; {:try_start_76 .. :try_end_be} :catch_d3

    goto :goto_74

    .end local v1           #code:Ljava/lang/Object;
    .end local v2           #content:Lorg/json/JSONArray;
    .end local v3           #cookies:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v4           #description:Ljava/lang/Object;
    .end local v5           #devId:Ljava/lang/String;
    .end local v8           #i$:Ljava/util/Iterator;
    .end local v10           #params:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v11           #resp:Lmiui/net/SimpleRequest$MapContent;
    .end local v12           #url:Ljava/lang/String;
    :catch_bf
    move-exception v7

    .local v7, e:Lmiui/net/exception/AccessDeniedException;
    invoke-virtual {v7}, Lmiui/net/exception/AccessDeniedException;->printStackTrace()V

    goto :goto_74

    .end local v7           #e:Lmiui/net/exception/AccessDeniedException;
    :catch_c4
    move-exception v7

    .local v7, e:Lmiui/net/exception/InvalidResponseException;
    invoke-virtual {v7}, Lmiui/net/exception/InvalidResponseException;->printStackTrace()V

    goto :goto_74

    .end local v7           #e:Lmiui/net/exception/InvalidResponseException;
    :catch_c9
    move-exception v7

    .local v7, e:Lmiui/net/exception/CipherException;
    invoke-virtual {v7}, Lmiui/net/exception/CipherException;->printStackTrace()V

    goto :goto_74

    .end local v7           #e:Lmiui/net/exception/CipherException;
    :catch_ce
    move-exception v7

    .local v7, e:Lmiui/telephony/exception/IllegalDeviceException;
    invoke-virtual {v7}, Lmiui/telephony/exception/IllegalDeviceException;->printStackTrace()V

    goto :goto_74

    .end local v7           #e:Lmiui/telephony/exception/IllegalDeviceException;
    :catch_d3
    move-exception v7

    .local v7, e:Lorg/json/JSONException;
    invoke-virtual {v7}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_74
.end method


# virtual methods
.method public cancelNotification(I)Lmiui/net/CloudManager$CloudManagerFuture;
    .registers 3
    .parameter "notificationId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lmiui/net/CloudManager$CloudManagerFuture",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .prologue
    new-instance v0, Lmiui/net/CloudManager$8;

    invoke-direct {v0, p0, p1}, Lmiui/net/CloudManager$8;-><init>(Lmiui/net/CloudManager;I)V

    invoke-virtual {v0}, Lmiui/net/CloudManager$8;->start()Lmiui/net/CloudManager$CloudManagerFuture;

    move-result-object v0

    return-object v0
.end method

.method public getActivateStatus()Lmiui/net/CloudManager$CloudManagerFuture;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lmiui/net/CloudManager$CloudManagerFuture",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .prologue
    new-instance v0, Lmiui/net/CloudManager$7;

    invoke-direct {v0, p0}, Lmiui/net/CloudManager$7;-><init>(Lmiui/net/CloudManager;)V

    invoke-virtual {v0}, Lmiui/net/CloudManager$7;->start()Lmiui/net/CloudManager$CloudManagerFuture;

    move-result-object v0

    return-object v0
.end method

.method public getActivatedPhoneNumber()Lmiui/net/CloudManager$CloudManagerFuture;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lmiui/net/CloudManager$CloudManagerFuture",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .prologue
    new-instance v0, Lmiui/net/CloudManager$1;

    invoke-direct {v0, p0}, Lmiui/net/CloudManager$1;-><init>(Lmiui/net/CloudManager;)V

    invoke-virtual {v0}, Lmiui/net/CloudManager$1;->start()Lmiui/net/CloudManager$CloudManagerFuture;

    move-result-object v0

    return-object v0
.end method

.method public getFindDeviceToken()Lmiui/net/CloudManager$CloudManagerFuture;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lmiui/net/CloudManager$CloudManagerFuture",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .prologue
    new-instance v0, Lmiui/net/CloudManager$6;

    invoke-direct {v0, p0}, Lmiui/net/CloudManager$6;-><init>(Lmiui/net/CloudManager;)V

    invoke-virtual {v0}, Lmiui/net/CloudManager$6;->start()Lmiui/net/CloudManager$CloudManagerFuture;

    move-result-object v0

    return-object v0
.end method

.method public getSmsGateway()Lmiui/net/CloudManager$CloudManagerFuture;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lmiui/net/CloudManager$CloudManagerFuture",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .prologue
    new-instance v0, Lmiui/net/CloudManager$3;

    invoke-direct {v0, p0}, Lmiui/net/CloudManager$3;-><init>(Lmiui/net/CloudManager;)V

    invoke-virtual {v0}, Lmiui/net/CloudManager$3;->start()Lmiui/net/CloudManager$CloudManagerFuture;

    move-result-object v0

    return-object v0
.end method

.method public getSubSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Lmiui/net/CloudManager$CloudManagerFuture;
    .registers 4
    .parameter "account"
    .parameter "authority"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Ljava/lang/String;",
            ")",
            "Lmiui/net/CloudManager$CloudManagerFuture",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .prologue
    new-instance v0, Lmiui/net/CloudManager$2;

    invoke-direct {v0, p0, p1, p2}, Lmiui/net/CloudManager$2;-><init>(Lmiui/net/CloudManager;Landroid/accounts/Account;Ljava/lang/String;)V

    invoke-virtual {v0}, Lmiui/net/CloudManager$2;->start()Lmiui/net/CloudManager$CloudManagerFuture;

    move-result-object v0

    return-object v0
.end method

.method public getUserSecurity()Lmiui/net/CloudManager$CloudManagerFuture;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lmiui/net/CloudManager$CloudManagerFuture",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .prologue
    new-instance v0, Lmiui/net/CloudManager$4;

    invoke-direct {v0, p0}, Lmiui/net/CloudManager$4;-><init>(Lmiui/net/CloudManager;)V

    invoke-virtual {v0}, Lmiui/net/CloudManager$4;->start()Lmiui/net/CloudManager$CloudManagerFuture;

    move-result-object v0

    return-object v0
.end method

.method public invalidateUserSecurity(Ljava/lang/String;Ljava/lang/String;)Lmiui/net/CloudManager$CloudManagerFuture;
    .registers 4
    .parameter "token"
    .parameter "security"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lmiui/net/CloudManager$CloudManagerFuture",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .prologue
    new-instance v0, Lmiui/net/CloudManager$5;

    invoke-direct {v0, p0, p1, p2}, Lmiui/net/CloudManager$5;-><init>(Lmiui/net/CloudManager;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lmiui/net/CloudManager$5;->start()Lmiui/net/CloudManager$CloudManagerFuture;

    move-result-object v0

    return-object v0
.end method
