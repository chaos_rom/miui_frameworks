.class Lmiui/net/PaymentManager$3;
.super Lmiui/net/PaymentManager$AddAccountCallback;
.source "PaymentManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiui/net/PaymentManager;->gotoMiliCenter(Landroid/app/Activity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/net/PaymentManager;

.field final synthetic val$activity:Landroid/app/Activity;


# direct methods
.method constructor <init>(Lmiui/net/PaymentManager;Landroid/app/Activity;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    iput-object p1, p0, Lmiui/net/PaymentManager$3;->this$0:Lmiui/net/PaymentManager;

    iput-object p2, p0, Lmiui/net/PaymentManager$3;->val$activity:Landroid/app/Activity;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lmiui/net/PaymentManager$AddAccountCallback;-><init>(Lmiui/net/PaymentManager;Lmiui/net/PaymentManager$1;)V

    return-void
.end method


# virtual methods
.method protected onFailed(ILjava/lang/String;)V
    .registers 3
    .parameter "error"
    .parameter "message"

    .prologue
    return-void
.end method

.method protected onSuccess(Landroid/accounts/Account;)V
    .registers 4
    .parameter "account"

    .prologue
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.xiaomi.xmsf.action.MILICENTER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .local v0, intent:Landroid/content/Intent;
    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/high16 v1, 0x1000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget-object v1, p0, Lmiui/net/PaymentManager$3;->val$activity:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method
