.class public abstract Lmiui/net/MiCloudSyncAdapterBase;
.super Landroid/content/AbstractThreadedSyncAdapter;
.source "MiCloudSyncAdapterBase.java"


# static fields
.field public static final ACTION_RESUME_SYNC:Ljava/lang/String; = "com.miui.net.RESUME_SYNC"

.field private static final BAD_REQUEST_LIMIT_PER_DAY:I = 0x64

.field public static final PREF_RESUME_SYNC_TIME:Ljava/lang/String; = "ResumeSyncTime_%s"

.field private static final PREF_TOKEN_EXPIRED_COUNT:Ljava/lang/String; = "TokenExpiredCount_%s"

.field private static final PREF_TOKEN_EXPIRED_DAY:Ljava/lang/String; = "TokenExpiredDay_%s"

.field private static final RESUME_SYNC_INTERVAL:I = 0x493e0

.field private static final TAG:Ljava/lang/String; = "MiCloudSyncAdapterBase"


# instance fields
.field protected mAccount:Landroid/accounts/Account;

.field protected final mAuthType:Ljava/lang/String;

.field protected mAuthority:Ljava/lang/String;

.field protected mContext:Landroid/content/Context;

.field protected mExtToken:Lmiui/net/ExtendedAuthToken;

.field protected mExtTokenStr:Ljava/lang/String;

.field protected mResolver:Landroid/content/ContentResolver;

.field protected mSyncResult:Landroid/content/SyncResult;


# direct methods
.method public constructor <init>(Landroid/content/Context;ZLjava/lang/String;)V
    .registers 5
    .parameter "context"
    .parameter "autoInitialize"
    .parameter "authType"

    .prologue
    invoke-direct {p0, p1, p2}, Landroid/content/AbstractThreadedSyncAdapter;-><init>(Landroid/content/Context;Z)V

    iput-object p1, p0, Lmiui/net/MiCloudSyncAdapterBase;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lmiui/net/MiCloudSyncAdapterBase;->mResolver:Landroid/content/ContentResolver;

    iput-object p3, p0, Lmiui/net/MiCloudSyncAdapterBase;->mAuthType:Ljava/lang/String;

    return-void
.end method

.method private handle5xx()V
    .registers 3

    .prologue
    const-string v0, "MiCloudSyncAdapterBase"

    const-string v1, "Http 5xx error. Suspending sync."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lmiui/net/MiCloudSyncAdapterBase;->suspendSync()V

    return-void
.end method

.method private handleBadRequest()V
    .registers 3

    .prologue
    const-string v0, "MiCloudSyncAdapterBase"

    const-string v1, "Http bad request error. Suspending sync."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lmiui/net/MiCloudSyncAdapterBase;->suspendSync()V

    return-void
.end method

.method private handleForbidden()V
    .registers 3

    .prologue
    const-string v0, "MiCloudSyncAdapterBase"

    const-string v1, "Http forbidden error. Turning off sync."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const v0, 0x60c01f0

    invoke-direct {p0, v0}, Lmiui/net/MiCloudSyncAdapterBase;->turnOffSyncAndNotify(I)V

    return-void
.end method

.method private handleNotAcceptable()V
    .registers 3

    .prologue
    const-string v0, "MiCloudSyncAdapterBase"

    const-string v1, "Http not-acceptable error. Turniong off sync."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const v0, 0x60c01f1

    invoke-direct {p0, v0}, Lmiui/net/MiCloudSyncAdapterBase;->turnOffSyncAndNotify(I)V

    return-void
.end method

.method private handleUnauthorized()V
    .registers 15

    .prologue
    const/4 v12, 0x1

    const/4 v13, 0x0

    const-string v9, "TokenExpiredCount_%s"

    new-array v10, v12, [Ljava/lang/Object;

    iget-object v11, p0, Lmiui/net/MiCloudSyncAdapterBase;->mAuthority:Ljava/lang/String;

    aput-object v11, v10, v13

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .local v2, prefTokenExpiredCount:Ljava/lang/String;
    const-string v9, "TokenExpiredDay_%s"

    new-array v10, v12, [Ljava/lang/Object;

    iget-object v11, p0, Lmiui/net/MiCloudSyncAdapterBase;->mAuthority:Ljava/lang/String;

    aput-object v11, v10, v13

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .local v3, prefTokenxpiredDay:Ljava/lang/String;
    invoke-virtual {p0}, Lmiui/net/MiCloudSyncAdapterBase;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v4

    .local v4, sp:Landroid/content/SharedPreferences;
    const-wide/16 v9, 0x0

    invoke-interface {v4, v3, v9, v10}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v7

    .local v7, tokenExpiredDay:J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    const-wide/32 v11, 0x5265c00

    div-long v5, v9, v11

    .local v5, today:J
    const/4 v0, 0x0

    .local v0, count:I
    cmp-long v9, v5, v7

    if-nez v9, :cond_3a

    invoke-interface {v4, v2, v13}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    :cond_3a
    add-int/lit8 v0, v0, 0x1

    const-string v9, "MiCloudSyncAdapterBase"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Http unauthorized error: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " times today."

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v9, 0x64

    if-lt v0, v9, :cond_6c

    const-string v9, "MiCloudSyncAdapterBase"

    const-string v10, "Http unauthorized error reached limit. Turning off sync."

    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const v9, 0x60c01ef

    invoke-direct {p0, v9}, Lmiui/net/MiCloudSyncAdapterBase;->turnOffSyncAndNotify(I)V

    const/4 v0, 0x0

    :cond_6c
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .local v1, editor:Landroid/content/SharedPreferences$Editor;
    invoke-interface {v1, v3, v5, v6}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method private suspendSync()V
    .registers 15

    .prologue
    const/4 v13, 0x1

    const/4 v12, 0x0

    iget-object v8, p0, Lmiui/net/MiCloudSyncAdapterBase;->mAccount:Landroid/accounts/Account;

    iget-object v9, p0, Lmiui/net/MiCloudSyncAdapterBase;->mAuthority:Ljava/lang/String;

    invoke-static {v8, v9}, Landroid/content/ContentResolver;->cancelSync(Landroid/accounts/Account;Ljava/lang/String;)V

    iget-object v8, p0, Lmiui/net/MiCloudSyncAdapterBase;->mContext:Landroid/content/Context;

    const-string v9, "alarm"

    invoke-virtual {v8, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .local v0, am:Landroid/app/AlarmManager;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    const-wide/32 v10, 0x493e0

    add-long v5, v8, v10

    .local v5, resumeTime:J
    new-instance v2, Landroid/content/Intent;

    const-string v8, "com.miui.net.RESUME_SYNC"

    invoke-direct {v2, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .local v2, intent:Landroid/content/Intent;
    const-string v8, "authority"

    iget-object v9, p0, Lmiui/net/MiCloudSyncAdapterBase;->mAuthority:Ljava/lang/String;

    invoke-virtual {v2, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v8, "account"

    iget-object v9, p0, Lmiui/net/MiCloudSyncAdapterBase;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v2, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v8, p0, Lmiui/net/MiCloudSyncAdapterBase;->mContext:Landroid/content/Context;

    invoke-static {v8, v12, v2, v12}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .local v3, pi:Landroid/app/PendingIntent;
    invoke-virtual {v0, v13, v5, v6, v3}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    invoke-virtual {p0}, Lmiui/net/MiCloudSyncAdapterBase;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v7

    .local v7, sp:Landroid/content/SharedPreferences;
    const-string v8, "ResumeSyncTime_%s"

    new-array v9, v13, [Ljava/lang/Object;

    iget-object v10, p0, Lmiui/net/MiCloudSyncAdapterBase;->mAuthority:Ljava/lang/String;

    aput-object v10, v9, v12

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .local v4, prefResumeSyncTime:Ljava/lang/String;
    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .local v1, editor:Landroid/content/SharedPreferences$Editor;
    invoke-interface {v1, v4, v5, v6}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method private turnOffSyncAndNotify(I)V
    .registers 15
    .parameter "messageId"

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    iget-object v7, p0, Lmiui/net/MiCloudSyncAdapterBase;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    .local v6, pm:Landroid/content/pm/PackageManager;
    iget-object v7, p0, Lmiui/net/MiCloudSyncAdapterBase;->mAuthority:Ljava/lang/String;

    invoke-virtual {v6, v7, v11}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v5

    .local v5, pi:Landroid/content/pm/ProviderInfo;
    invoke-virtual {v5, v6}, Landroid/content/pm/ProviderInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v1

    .local v1, label:Ljava/lang/CharSequence;
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .local v0, intent:Landroid/content/Intent;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lmiui/net/MiCloudSyncAdapterBase;->mAuthority:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".SYNC_SETTINGS"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v7, "authority"

    iget-object v8, p0, Lmiui/net/MiCloudSyncAdapterBase;->mAuthority:Ljava/lang/String;

    invoke-virtual {v0, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v7, "account"

    iget-object v8, p0, Lmiui/net/MiCloudSyncAdapterBase;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v0, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v7, p0, Lmiui/net/MiCloudSyncAdapterBase;->mContext:Landroid/content/Context;

    invoke-static {v7, v11, v0, v11}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    .local v4, pendingIntent:Landroid/app/PendingIntent;
    new-instance v7, Landroid/app/Notification$Builder;

    iget-object v8, p0, Lmiui/net/MiCloudSyncAdapterBase;->mContext:Landroid/content/Context;

    invoke-direct {v7, v8}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    const v8, 0x108007c

    invoke-virtual {v7, v8}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v7

    iget-object v8, p0, Lmiui/net/MiCloudSyncAdapterBase;->mContext:Landroid/content/Context;

    const v9, 0x60c01ee

    new-array v10, v12, [Ljava/lang/Object;

    aput-object v1, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v7

    iget-object v8, p0, Lmiui/net/MiCloudSyncAdapterBase;->mContext:Landroid/content/Context;

    invoke-virtual {v8, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v7

    invoke-virtual {v7, v4}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v7

    invoke-virtual {v7, v12}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v2

    .local v2, n:Landroid/app/Notification;
    iget-object v7, p0, Lmiui/net/MiCloudSyncAdapterBase;->mContext:Landroid/content/Context;

    const-string v8, "notification"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/NotificationManager;

    .local v3, nm:Landroid/app/NotificationManager;
    invoke-virtual {v3, v11, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    iget-object v7, p0, Lmiui/net/MiCloudSyncAdapterBase;->mContext:Landroid/content/Context;

    invoke-static {v7}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v7

    iget-object v8, p0, Lmiui/net/MiCloudSyncAdapterBase;->mAccount:Landroid/accounts/Account;

    iget-object v8, v8, Landroid/accounts/Account;->type:Ljava/lang/String;

    iget-object v9, p0, Lmiui/net/MiCloudSyncAdapterBase;->mExtTokenStr:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v7, p0, Lmiui/net/MiCloudSyncAdapterBase;->mAccount:Landroid/accounts/Account;

    iget-object v8, p0, Lmiui/net/MiCloudSyncAdapterBase;->mAuthority:Ljava/lang/String;

    invoke-static {v7, v8, v11}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    iget-object v7, p0, Lmiui/net/MiCloudSyncAdapterBase;->mAccount:Landroid/accounts/Account;

    iget-object v8, p0, Lmiui/net/MiCloudSyncAdapterBase;->mAuthority:Ljava/lang/String;

    invoke-static {v7, v8}, Landroid/content/ContentResolver;->cancelSync(Landroid/accounts/Account;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected abstract onPerformMiCloudSync()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lmiui/net/exception/MiCloudServerException;
        }
    .end annotation
.end method

.method public onPerformSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .registers 24
    .parameter "account"
    .parameter "extras"
    .parameter "authority"
    .parameter "provider"
    .parameter "syncResult"

    .prologue
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, Lmiui/net/MiCloudSyncAdapterBase;->mAuthority:Ljava/lang/String;

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lmiui/net/MiCloudSyncAdapterBase;->mAccount:Landroid/accounts/Account;

    move-object/from16 v0, p5

    move-object/from16 v1, p0

    iput-object v0, v1, Lmiui/net/MiCloudSyncAdapterBase;->mSyncResult:Landroid/content/SyncResult;

    invoke-virtual/range {p0 .. p0}, Lmiui/net/MiCloudSyncAdapterBase;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v17

    .local v17, sp:Landroid/content/SharedPreferences;
    const-string v3, "ResumeSyncTime_%s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lmiui/net/MiCloudSyncAdapterBase;->mAuthority:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .local v12, prefResumeSyncTime:Ljava/lang/String;
    const-wide/16 v3, 0x0

    move-object/from16 v0, v17

    invoke-interface {v0, v12, v3, v4}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v15

    .local v15, resumeTime:J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    sub-long v13, v15, v3

    .local v13, remaining:J
    const-wide/16 v3, 0x0

    cmp-long v3, v13, v3

    if-lez v3, :cond_6f

    const-string v3, "MiCloudSyncAdapterBase"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onPerformSync: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lmiui/net/MiCloudSyncAdapterBase;->mAuthority:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " sync suspended. "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-wide/16 v5, 0x3e8

    div-long v5, v13, v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " seconds to resume."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_6e
    return-void

    :cond_6f
    const-wide/16 v3, 0x0

    cmp-long v3, v15, v3

    if-eqz v3, :cond_a3

    const-string v3, "MiCloudSyncAdapterBase"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onPerformSync: The suspension of "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lmiui/net/MiCloudSyncAdapterBase;->mAuthority:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " sync is expired now."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface/range {v17 .. v17}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v10

    .local v10, editor:Landroid/content/SharedPreferences$Editor;
    const-wide/16 v3, 0x0

    invoke-interface {v10, v12, v3, v4}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v10}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .end local v10           #editor:Landroid/content/SharedPreferences$Editor;
    :cond_a3
    invoke-virtual/range {p0 .. p0}, Lmiui/net/MiCloudSyncAdapterBase;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    .local v2, am:Landroid/accounts/AccountManager;
    :try_start_ab
    const-string v3, "MiCloudSyncAdapterBase"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onPerformSync: getting auth token. authority: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lmiui/net/MiCloudSyncAdapterBase;->mAuthority:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v4, v0, Lmiui/net/MiCloudSyncAdapterBase;->mAuthType:Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v3, p1

    invoke-virtual/range {v2 .. v8}, Landroid/accounts/AccountManager;->getAuthToken(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;ZLandroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    move-result-object v11

    .local v11, future:Landroid/accounts/AccountManagerFuture;,"Landroid/accounts/AccountManagerFuture<Landroid/os/Bundle;>;"
    if-nez v11, :cond_e8

    const-string v3, "MiCloudSyncAdapterBase"

    const-string v4, "onPerformSync: Null future."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_de
    .catch Landroid/accounts/OperationCanceledException; {:try_start_ab .. :try_end_de} :catch_df
    .catch Landroid/accounts/AuthenticatorException; {:try_start_ab .. :try_end_de} :catch_f7
    .catch Ljava/io/IOException; {:try_start_ab .. :try_end_de} :catch_12d

    goto :goto_6e

    .end local v11           #future:Landroid/accounts/AccountManagerFuture;,"Landroid/accounts/AccountManagerFuture<Landroid/os/Bundle;>;"
    :catch_df
    move-exception v9

    .local v9, e:Landroid/accounts/OperationCanceledException;
    const-string v3, "MiCloudSyncAdapterBase"

    const-string v4, "onPerformSync"

    invoke-static {v3, v4, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_6e

    .end local v9           #e:Landroid/accounts/OperationCanceledException;
    .restart local v11       #future:Landroid/accounts/AccountManagerFuture;,"Landroid/accounts/AccountManagerFuture<Landroid/os/Bundle;>;"
    :cond_e8
    :try_start_e8
    invoke-interface {v11}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_101

    const-string v3, "MiCloudSyncAdapterBase"

    const-string v4, "onPerformSync: Null future result."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_f5
    .catch Landroid/accounts/OperationCanceledException; {:try_start_e8 .. :try_end_f5} :catch_df
    .catch Landroid/accounts/AuthenticatorException; {:try_start_e8 .. :try_end_f5} :catch_f7
    .catch Ljava/io/IOException; {:try_start_e8 .. :try_end_f5} :catch_12d

    goto/16 :goto_6e

    .end local v11           #future:Landroid/accounts/AccountManagerFuture;,"Landroid/accounts/AccountManagerFuture<Landroid/os/Bundle;>;"
    :catch_f7
    move-exception v9

    .local v9, e:Landroid/accounts/AuthenticatorException;
    const-string v3, "MiCloudSyncAdapterBase"

    const-string v4, "onPerformSync"

    invoke-static {v3, v4, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_6e

    .end local v9           #e:Landroid/accounts/AuthenticatorException;
    .restart local v11       #future:Landroid/accounts/AccountManagerFuture;,"Landroid/accounts/AccountManagerFuture<Landroid/os/Bundle;>;"
    :cond_101
    :try_start_101
    invoke-interface {v11}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/Bundle;

    const-string v4, "authtoken"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lmiui/net/MiCloudSyncAdapterBase;->mExtTokenStr:Ljava/lang/String;
    :try_end_111
    .catch Landroid/accounts/OperationCanceledException; {:try_start_101 .. :try_end_111} :catch_df
    .catch Landroid/accounts/AuthenticatorException; {:try_start_101 .. :try_end_111} :catch_f7
    .catch Ljava/io/IOException; {:try_start_101 .. :try_end_111} :catch_12d

    move-object/from16 v0, p0

    iget-object v3, v0, Lmiui/net/MiCloudSyncAdapterBase;->mExtTokenStr:Ljava/lang/String;

    if-nez v3, :cond_144

    move-object/from16 v0, p0

    iget-object v3, v0, Lmiui/net/MiCloudSyncAdapterBase;->mSyncResult:Landroid/content/SyncResult;

    iget-object v3, v3, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v3, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v3, Landroid/content/SyncStats;->numIoExceptions:J

    const-string v3, "MiCloudSyncAdapterBase"

    const-string v4, "onPerformSync: No ext token string."

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_6e

    .end local v11           #future:Landroid/accounts/AccountManagerFuture;,"Landroid/accounts/AccountManagerFuture<Landroid/os/Bundle;>;"
    :catch_12d
    move-exception v9

    .local v9, e:Ljava/io/IOException;
    move-object/from16 v0, p0

    iget-object v3, v0, Lmiui/net/MiCloudSyncAdapterBase;->mSyncResult:Landroid/content/SyncResult;

    iget-object v3, v3, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v3, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v3, Landroid/content/SyncStats;->numIoExceptions:J

    const-string v3, "MiCloudSyncAdapterBase"

    const-string v4, "onPerformSync"

    invoke-static {v3, v4, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_6e

    .end local v9           #e:Ljava/io/IOException;
    .restart local v11       #future:Landroid/accounts/AccountManagerFuture;,"Landroid/accounts/AccountManagerFuture<Landroid/os/Bundle;>;"
    :cond_144
    const-string v3, "MiCloudSyncAdapterBase"

    const-string v4, "onPerformSync: Got extTokenStr."

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v3, v0, Lmiui/net/MiCloudSyncAdapterBase;->mExtTokenStr:Ljava/lang/String;

    invoke-static {v3}, Lmiui/net/ExtendedAuthToken;->parse(Ljava/lang/String;)Lmiui/net/ExtendedAuthToken;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lmiui/net/MiCloudSyncAdapterBase;->mExtToken:Lmiui/net/ExtendedAuthToken;

    :try_start_157
    invoke-virtual/range {p0 .. p0}, Lmiui/net/MiCloudSyncAdapterBase;->onPerformMiCloudSync()V
    :try_end_15a
    .catch Lmiui/net/exception/MiCloudServerException; {:try_start_157 .. :try_end_15a} :catch_15c

    goto/16 :goto_6e

    :catch_15c
    move-exception v9

    .local v9, e:Lmiui/net/exception/MiCloudServerException;
    const-string v3, "MiCloudSyncAdapterBase"

    const-string v4, "onPerformSync"

    invoke-static {v3, v4, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget v3, v9, Lmiui/net/exception/MiCloudServerException;->statusCode:I

    packed-switch v3, :pswitch_data_1a6

    :pswitch_169
    iget v3, v9, Lmiui/net/exception/MiCloudServerException;->statusCode:I

    div-int/lit8 v3, v3, 0x64

    const/4 v4, 0x5

    if-ne v3, v4, :cond_189

    invoke-direct/range {p0 .. p0}, Lmiui/net/MiCloudSyncAdapterBase;->handle5xx()V

    goto/16 :goto_6e

    :pswitch_175
    invoke-direct/range {p0 .. p0}, Lmiui/net/MiCloudSyncAdapterBase;->handleBadRequest()V

    goto/16 :goto_6e

    :pswitch_17a
    invoke-direct/range {p0 .. p0}, Lmiui/net/MiCloudSyncAdapterBase;->handleUnauthorized()V

    goto/16 :goto_6e

    :pswitch_17f
    invoke-direct/range {p0 .. p0}, Lmiui/net/MiCloudSyncAdapterBase;->handleForbidden()V

    goto/16 :goto_6e

    :pswitch_184
    invoke-direct/range {p0 .. p0}, Lmiui/net/MiCloudSyncAdapterBase;->handleNotAcceptable()V

    goto/16 :goto_6e

    :cond_189
    const-string v3, "MiCloudSyncAdapterBase"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unrecognized server error "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v9, Lmiui/net/exception/MiCloudServerException;->statusCode:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_6e

    nop

    :pswitch_data_1a6
    .packed-switch 0x190
        :pswitch_175
        :pswitch_17a
        :pswitch_169
        :pswitch_17f
        :pswitch_169
        :pswitch_169
        :pswitch_184
    .end packed-switch
.end method
