.class Lmiui/net/PaymentManager$1;
.super Lmiui/net/PaymentManager$AddAccountCallback;
.source "PaymentManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiui/net/PaymentManager;->pay(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lmiui/net/PaymentManager$PaymentListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/net/PaymentManager;

.field final synthetic val$extra:Landroid/os/Bundle;

.field final synthetic val$paymentListener:Lmiui/net/PaymentManager$PaymentListener;

.field final synthetic val$productId:Ljava/lang/String;

.field final synthetic val$serviceId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lmiui/net/PaymentManager;Ljava/lang/String;Ljava/lang/String;Lmiui/net/PaymentManager$PaymentListener;Landroid/os/Bundle;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    iput-object p1, p0, Lmiui/net/PaymentManager$1;->this$0:Lmiui/net/PaymentManager;

    iput-object p2, p0, Lmiui/net/PaymentManager$1;->val$serviceId:Ljava/lang/String;

    iput-object p3, p0, Lmiui/net/PaymentManager$1;->val$productId:Ljava/lang/String;

    iput-object p4, p0, Lmiui/net/PaymentManager$1;->val$paymentListener:Lmiui/net/PaymentManager$PaymentListener;

    iput-object p5, p0, Lmiui/net/PaymentManager$1;->val$extra:Landroid/os/Bundle;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lmiui/net/PaymentManager$AddAccountCallback;-><init>(Lmiui/net/PaymentManager;Lmiui/net/PaymentManager$1;)V

    return-void
.end method


# virtual methods
.method protected onFailed(ILjava/lang/String;)V
    .registers 5
    .parameter "error"
    .parameter "message"

    .prologue
    iget-object v0, p0, Lmiui/net/PaymentManager$1;->val$paymentListener:Lmiui/net/PaymentManager$PaymentListener;

    iget-object v1, p0, Lmiui/net/PaymentManager$1;->val$productId:Ljava/lang/String;

    invoke-interface {v0, v1, p1, p2}, Lmiui/net/PaymentManager$PaymentListener;->onFailed(Ljava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method protected onSuccess(Landroid/accounts/Account;)V
    .registers 9
    .parameter "account"

    .prologue
    new-instance v5, Lmiui/net/PaymentManager$PaymentCallback;

    iget-object v0, p0, Lmiui/net/PaymentManager$1;->this$0:Lmiui/net/PaymentManager;

    iget-object v1, p0, Lmiui/net/PaymentManager$1;->val$serviceId:Ljava/lang/String;

    iget-object v2, p0, Lmiui/net/PaymentManager$1;->val$productId:Ljava/lang/String;

    iget-object v3, p0, Lmiui/net/PaymentManager$1;->val$paymentListener:Lmiui/net/PaymentManager$PaymentListener;

    invoke-direct {v5, v0, v1, v2, v3}, Lmiui/net/PaymentManager$PaymentCallback;-><init>(Lmiui/net/PaymentManager;Ljava/lang/String;Ljava/lang/String;Lmiui/net/PaymentManager$PaymentListener;)V

    .local v5, paymentCallback:Lmiui/net/PaymentManager$PaymentCallback;
    iget-object v0, p0, Lmiui/net/PaymentManager$1;->this$0:Lmiui/net/PaymentManager;

    iget-object v2, p0, Lmiui/net/PaymentManager$1;->val$serviceId:Ljava/lang/String;

    iget-object v3, p0, Lmiui/net/PaymentManager$1;->val$productId:Ljava/lang/String;

    iget-object v4, p0, Lmiui/net/PaymentManager$1;->val$extra:Landroid/os/Bundle;

    const/4 v6, 0x0

    move-object v1, p1

    #calls: Lmiui/net/PaymentManager;->internalPay(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lmiui/net/PaymentManager$PaymentManagerCallback;Landroid/os/Handler;)Lmiui/net/PaymentManager$PaymentManagerFuture;
    invoke-static/range {v0 .. v6}, Lmiui/net/PaymentManager;->access$100(Lmiui/net/PaymentManager;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lmiui/net/PaymentManager$PaymentManagerCallback;Landroid/os/Handler;)Lmiui/net/PaymentManager$PaymentManagerFuture;

    return-void
.end method
