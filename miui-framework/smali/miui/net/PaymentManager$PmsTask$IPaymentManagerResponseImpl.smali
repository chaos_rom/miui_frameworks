.class Lmiui/net/PaymentManager$PmsTask$IPaymentManagerResponseImpl;
.super Lmiui/net/IPaymentManagerResponse$Stub;
.source "PaymentManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/net/PaymentManager$PmsTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "IPaymentManagerResponseImpl"
.end annotation


# instance fields
.field final synthetic this$1:Lmiui/net/PaymentManager$PmsTask;


# direct methods
.method constructor <init>(Lmiui/net/PaymentManager$PmsTask;)V
    .registers 2
    .parameter

    .prologue
    iput-object p1, p0, Lmiui/net/PaymentManager$PmsTask$IPaymentManagerResponseImpl;->this$1:Lmiui/net/PaymentManager$PmsTask;

    invoke-direct {p0}, Lmiui/net/IPaymentManagerResponse$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(ILjava/lang/String;)V
    .registers 5
    .parameter "code"
    .parameter "message"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x4

    if-ne p1, v0, :cond_f

    iget-object v0, p0, Lmiui/net/PaymentManager$PmsTask$IPaymentManagerResponseImpl;->this$1:Lmiui/net/PaymentManager$PmsTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmiui/net/PaymentManager$PmsTask;->cancel(Z)Z

    iget-object v0, p0, Lmiui/net/PaymentManager$PmsTask$IPaymentManagerResponseImpl;->this$1:Lmiui/net/PaymentManager$PmsTask;

    invoke-virtual {v0}, Lmiui/net/PaymentManager$PmsTask;->unBind()V

    :goto_e
    return-void

    :cond_f
    iget-object v0, p0, Lmiui/net/PaymentManager$PmsTask$IPaymentManagerResponseImpl;->this$1:Lmiui/net/PaymentManager$PmsTask;

    iget-object v1, p0, Lmiui/net/PaymentManager$PmsTask$IPaymentManagerResponseImpl;->this$1:Lmiui/net/PaymentManager$PmsTask;

    #calls: Lmiui/net/PaymentManager$PmsTask;->convertErrorCodeToException(ILjava/lang/String;)Ljava/lang/Exception;
    invoke-static {v1, p1, p2}, Lmiui/net/PaymentManager$PmsTask;->access$700(Lmiui/net/PaymentManager$PmsTask;ILjava/lang/String;)Ljava/lang/Exception;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/net/PaymentManager$PmsTask;->setException(Ljava/lang/Throwable;)V

    goto :goto_e
.end method

.method public onResult(Landroid/os/Bundle;)V
    .registers 3
    .parameter "bundle"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    iget-object v0, p0, Lmiui/net/PaymentManager$PmsTask$IPaymentManagerResponseImpl;->this$1:Lmiui/net/PaymentManager$PmsTask;

    invoke-virtual {v0, p1}, Lmiui/net/PaymentManager$PmsTask;->set(Landroid/os/Bundle;)V

    return-void
.end method
