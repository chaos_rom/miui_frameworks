.class public Lmiui/net/exception/PaymentServiceFailureException;
.super Ljava/lang/Exception;
.source "PaymentServiceFailureException.java"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private mErrorCode:I


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .registers 3
    .parameter "error"
    .parameter "message"

    .prologue
    invoke-direct {p0, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    iput p1, p0, Lmiui/net/exception/PaymentServiceFailureException;->mErrorCode:I

    return-void
.end method


# virtual methods
.method public getError()I
    .registers 2

    .prologue
    iget v0, p0, Lmiui/net/exception/PaymentServiceFailureException;->mErrorCode:I

    return v0
.end method
