.class abstract Lmiui/net/PaymentManager$PmsTask;
.super Ljava/util/concurrent/FutureTask;
.source "PaymentManager.java"

# interfaces
.implements Lmiui/net/PaymentManager$PaymentManagerFuture;
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/net/PaymentManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "PmsTask"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/net/PaymentManager$PmsTask$IPaymentManagerResponseImpl;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/concurrent/FutureTask",
        "<",
        "Landroid/os/Bundle;",
        ">;",
        "Lmiui/net/PaymentManager$PaymentManagerFuture",
        "<",
        "Landroid/os/Bundle;",
        ">;",
        "Landroid/content/ServiceConnection;"
    }
.end annotation


# instance fields
.field private final mCallback:Lmiui/net/PaymentManager$PaymentManagerCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lmiui/net/PaymentManager$PaymentManagerCallback",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field

.field private final mHandler:Landroid/os/Handler;

.field private mResponse:Lmiui/net/IPaymentManagerResponse;

.field private mService:Lmiui/net/IPaymentManagerService;

.field final synthetic this$0:Lmiui/net/PaymentManager;


# direct methods
.method protected constructor <init>(Lmiui/net/PaymentManager;Landroid/os/Handler;Lmiui/net/PaymentManager$PaymentManagerCallback;)V
    .registers 5
    .parameter
    .parameter "handler"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Handler;",
            "Lmiui/net/PaymentManager$PaymentManagerCallback",
            "<",
            "Landroid/os/Bundle;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p3, callback:Lmiui/net/PaymentManager$PaymentManagerCallback;,"Lmiui/net/PaymentManager$PaymentManagerCallback<Landroid/os/Bundle;>;"
    iput-object p1, p0, Lmiui/net/PaymentManager$PmsTask;->this$0:Lmiui/net/PaymentManager;

    new-instance v0, Lmiui/net/PaymentManager$PmsTask$1;

    invoke-direct {v0, p1}, Lmiui/net/PaymentManager$PmsTask$1;-><init>(Lmiui/net/PaymentManager;)V

    invoke-direct {p0, v0}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    iput-object p2, p0, Lmiui/net/PaymentManager$PmsTask;->mHandler:Landroid/os/Handler;

    iput-object p3, p0, Lmiui/net/PaymentManager$PmsTask;->mCallback:Lmiui/net/PaymentManager$PaymentManagerCallback;

    new-instance v0, Lmiui/net/PaymentManager$PmsTask$IPaymentManagerResponseImpl;

    invoke-direct {v0, p0}, Lmiui/net/PaymentManager$PmsTask$IPaymentManagerResponseImpl;-><init>(Lmiui/net/PaymentManager$PmsTask;)V

    iput-object v0, p0, Lmiui/net/PaymentManager$PmsTask;->mResponse:Lmiui/net/IPaymentManagerResponse;

    return-void
.end method

.method static synthetic access$600(Lmiui/net/PaymentManager$PmsTask;)Lmiui/net/PaymentManager$PaymentManagerCallback;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/net/PaymentManager$PmsTask;->mCallback:Lmiui/net/PaymentManager$PaymentManagerCallback;

    return-object v0
.end method

.method static synthetic access$700(Lmiui/net/PaymentManager$PmsTask;ILjava/lang/String;)Ljava/lang/Exception;
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    invoke-direct {p0, p1, p2}, Lmiui/net/PaymentManager$PmsTask;->convertErrorCodeToException(ILjava/lang/String;)Ljava/lang/Exception;

    move-result-object v0

    return-object v0
.end method

.method private convertErrorCodeToException(ILjava/lang/String;)Ljava/lang/Exception;
    .registers 4
    .parameter "code"
    .parameter "message"

    .prologue
    const/4 v0, 0x3

    if-ne p1, v0, :cond_9

    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    :goto_8
    return-object v0

    :cond_9
    const/4 v0, 0x5

    if-ne p1, v0, :cond_12

    new-instance v0, Lmiui/net/exception/AuthenticationFailureException;

    invoke-direct {v0, p2}, Lmiui/net/exception/AuthenticationFailureException;-><init>(Ljava/lang/String;)V

    goto :goto_8

    :cond_12
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1a

    const-string p2, "Unknown payment failure"

    :cond_1a
    new-instance v0, Lmiui/net/exception/PaymentServiceFailureException;

    invoke-direct {v0, p1, p2}, Lmiui/net/exception/PaymentServiceFailureException;-><init>(ILjava/lang/String;)V

    goto :goto_8
.end method

.method private ensureNotOnMainThread()V
    .registers 5

    .prologue
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    .local v1, looper:Landroid/os/Looper;
    if-eqz v1, :cond_21

    iget-object v2, p0, Lmiui/net/PaymentManager$PmsTask;->this$0:Lmiui/net/PaymentManager;

    #getter for: Lmiui/net/PaymentManager;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lmiui/net/PaymentManager;->access$400(Lmiui/net/PaymentManager;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    if-ne v1, v2, :cond_21

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "calling this from your main thread can lead to deadlock"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .local v0, exception:Ljava/lang/IllegalStateException;
    const-string v2, "PaymentManager"

    const-string v3, "calling this from your main thread can lead to deadlock and/or ANRs"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    throw v0

    .end local v0           #exception:Ljava/lang/IllegalStateException;
    :cond_21
    return-void
.end method

.method private internalGetResult(Ljava/lang/Long;Ljava/util/concurrent/TimeUnit;)Landroid/os/Bundle;
    .registers 8
    .parameter "timeout"
    .parameter "unit"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lmiui/net/exception/OperationCancelledException;,
            Lmiui/net/exception/AuthenticationFailureException;,
            Lmiui/net/exception/PaymentServiceFailureException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    invoke-virtual {p0}, Lmiui/net/PaymentManager$PmsTask;->isDone()Z

    move-result v2

    if-nez v2, :cond_a

    invoke-direct {p0}, Lmiui/net/PaymentManager$PmsTask;->ensureNotOnMainThread()V

    :cond_a
    if-nez p1, :cond_16

    :try_start_c
    invoke-virtual {p0}, Lmiui/net/PaymentManager$PmsTask;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;
    :try_end_12
    .catchall {:try_start_c .. :try_end_12} :catchall_2d
    .catch Ljava/util/concurrent/CancellationException; {:try_start_c .. :try_end_12} :catch_24
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_c .. :try_end_12} :catch_32
    .catch Ljava/lang/InterruptedException; {:try_start_c .. :try_end_12} :catch_3e
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_c .. :try_end_12} :catch_43

    invoke-virtual {p0, v4}, Lmiui/net/PaymentManager$PmsTask;->cancel(Z)Z

    :goto_15
    return-object v2

    :cond_16
    :try_start_16
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3, p2}, Lmiui/net/PaymentManager$PmsTask;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;
    :try_end_20
    .catchall {:try_start_16 .. :try_end_20} :catchall_2d
    .catch Ljava/util/concurrent/CancellationException; {:try_start_16 .. :try_end_20} :catch_24
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_16 .. :try_end_20} :catch_32
    .catch Ljava/lang/InterruptedException; {:try_start_16 .. :try_end_20} :catch_3e
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_16 .. :try_end_20} :catch_43

    invoke-virtual {p0, v4}, Lmiui/net/PaymentManager$PmsTask;->cancel(Z)Z

    goto :goto_15

    :catch_24
    move-exception v1

    .local v1, e:Ljava/util/concurrent/CancellationException;
    :try_start_25
    new-instance v2, Lmiui/net/exception/OperationCancelledException;

    const-string v3, "cancelled by user"

    invoke-direct {v2, v3}, Lmiui/net/exception/OperationCancelledException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_2d
    .catchall {:try_start_25 .. :try_end_2d} :catchall_2d

    .end local v1           #e:Ljava/util/concurrent/CancellationException;
    :catchall_2d
    move-exception v2

    invoke-virtual {p0, v4}, Lmiui/net/PaymentManager$PmsTask;->cancel(Z)Z

    throw v2

    :catch_32
    move-exception v2

    invoke-virtual {p0, v4}, Lmiui/net/PaymentManager$PmsTask;->cancel(Z)Z

    :goto_36
    new-instance v2, Lmiui/net/exception/OperationCancelledException;

    const-string v3, "cancelled by exception"

    invoke-direct {v2, v3}, Lmiui/net/exception/OperationCancelledException;-><init>(Ljava/lang/String;)V

    throw v2

    :catch_3e
    move-exception v2

    invoke-virtual {p0, v4}, Lmiui/net/PaymentManager$PmsTask;->cancel(Z)Z

    goto :goto_36

    :catch_43
    move-exception v1

    .local v1, e:Ljava/util/concurrent/ExecutionException;
    :try_start_44
    invoke-virtual {v1}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    .local v0, cause:Ljava/lang/Throwable;
    instance-of v2, v0, Ljava/io/IOException;

    if-eqz v2, :cond_4f

    check-cast v0, Ljava/io/IOException;

    .end local v0           #cause:Ljava/lang/Throwable;
    throw v0

    .restart local v0       #cause:Ljava/lang/Throwable;
    :cond_4f
    instance-of v2, v0, Lmiui/net/exception/PaymentServiceFailureException;

    if-eqz v2, :cond_56

    check-cast v0, Lmiui/net/exception/PaymentServiceFailureException;

    .end local v0           #cause:Ljava/lang/Throwable;
    throw v0

    .restart local v0       #cause:Ljava/lang/Throwable;
    :cond_56
    instance-of v2, v0, Lmiui/net/exception/AuthenticationFailureException;

    if-eqz v2, :cond_5d

    check-cast v0, Lmiui/net/exception/AuthenticationFailureException;

    .end local v0           #cause:Ljava/lang/Throwable;
    throw v0

    .restart local v0       #cause:Ljava/lang/Throwable;
    :cond_5d
    instance-of v2, v0, Ljava/lang/RuntimeException;

    if-eqz v2, :cond_64

    check-cast v0, Ljava/lang/RuntimeException;

    .end local v0           #cause:Ljava/lang/Throwable;
    throw v0

    .restart local v0       #cause:Ljava/lang/Throwable;
    :cond_64
    instance-of v2, v0, Ljava/lang/Error;

    if-eqz v2, :cond_6b

    check-cast v0, Ljava/lang/Error;

    .end local v0           #cause:Ljava/lang/Throwable;
    throw v0

    .restart local v0       #cause:Ljava/lang/Throwable;
    :cond_6b
    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_71
    .catchall {:try_start_44 .. :try_end_71} :catchall_2d
.end method


# virtual methods
.method protected bind()V
    .registers 4

    .prologue
    invoke-virtual {p0}, Lmiui/net/PaymentManager$PmsTask;->bindToPaymentService()Z

    move-result v0

    if-nez v0, :cond_11

    new-instance v0, Lmiui/net/exception/PaymentServiceFailureException;

    const/4 v1, 0x1

    const-string v2, "bind to service failed"

    invoke-direct {v0, v1, v2}, Lmiui/net/exception/PaymentServiceFailureException;-><init>(ILjava/lang/String;)V

    invoke-virtual {p0, v0}, Lmiui/net/PaymentManager$PmsTask;->setException(Ljava/lang/Throwable;)V

    :cond_11
    return-void
.end method

.method protected bindToPaymentService()Z
    .registers 4

    .prologue
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.xiaomi.xmsf.action.PAYMENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .local v0, intent:Landroid/content/Intent;
    iget-object v1, p0, Lmiui/net/PaymentManager$PmsTask;->this$0:Lmiui/net/PaymentManager;

    #getter for: Lmiui/net/PaymentManager;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lmiui/net/PaymentManager;->access$400(Lmiui/net/PaymentManager;)Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v0, p0, v2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    return v1
.end method

.method protected abstract doWork()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method protected done()V
    .registers 3

    .prologue
    iget-object v1, p0, Lmiui/net/PaymentManager$PmsTask;->mCallback:Lmiui/net/PaymentManager$PaymentManagerCallback;

    if-eqz v1, :cond_16

    iget-object v1, p0, Lmiui/net/PaymentManager$PmsTask;->mHandler:Landroid/os/Handler;

    if-nez v1, :cond_17

    iget-object v1, p0, Lmiui/net/PaymentManager$PmsTask;->this$0:Lmiui/net/PaymentManager;

    #getter for: Lmiui/net/PaymentManager;->mMainHandler:Landroid/os/Handler;
    invoke-static {v1}, Lmiui/net/PaymentManager;->access$500(Lmiui/net/PaymentManager;)Landroid/os/Handler;

    move-result-object v0

    .local v0, handler:Landroid/os/Handler;
    :goto_e
    new-instance v1, Lmiui/net/PaymentManager$PmsTask$2;

    invoke-direct {v1, p0}, Lmiui/net/PaymentManager$PmsTask$2;-><init>(Lmiui/net/PaymentManager$PmsTask;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .end local v0           #handler:Landroid/os/Handler;
    :cond_16
    return-void

    :cond_17
    iget-object v0, p0, Lmiui/net/PaymentManager$PmsTask;->mHandler:Landroid/os/Handler;

    goto :goto_e
.end method

.method protected getResponse()Lmiui/net/IPaymentManagerResponse;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/net/PaymentManager$PmsTask;->mResponse:Lmiui/net/IPaymentManagerResponse;

    return-object v0
.end method

.method public getResult()Landroid/os/Bundle;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lmiui/net/exception/OperationCancelledException;,
            Lmiui/net/exception/AuthenticationFailureException;,
            Lmiui/net/exception/PaymentServiceFailureException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    invoke-direct {p0, v0, v0}, Lmiui/net/PaymentManager$PmsTask;->internalGetResult(Ljava/lang/Long;Ljava/util/concurrent/TimeUnit;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public getResult(JLjava/util/concurrent/TimeUnit;)Landroid/os/Bundle;
    .registers 5
    .parameter "timeout"
    .parameter "unit"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lmiui/net/exception/OperationCancelledException;,
            Lmiui/net/exception/AuthenticationFailureException;,
            Lmiui/net/exception/PaymentServiceFailureException;
        }
    .end annotation

    .prologue
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-direct {p0, v0, p3}, Lmiui/net/PaymentManager$PmsTask;->internalGetResult(Ljava/lang/Long;Ljava/util/concurrent/TimeUnit;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getResult()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lmiui/net/exception/OperationCancelledException;,
            Lmiui/net/exception/AuthenticationFailureException;,
            Lmiui/net/exception/PaymentServiceFailureException;
        }
    .end annotation

    .prologue
    invoke-virtual {p0}, Lmiui/net/PaymentManager$PmsTask;->getResult()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getResult(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lmiui/net/exception/OperationCancelledException;,
            Lmiui/net/exception/AuthenticationFailureException;,
            Lmiui/net/exception/PaymentServiceFailureException;
        }
    .end annotation

    .prologue
    invoke-virtual {p0, p1, p2, p3}, Lmiui/net/PaymentManager$PmsTask;->getResult(JLjava/util/concurrent/TimeUnit;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method protected getService()Lmiui/net/IPaymentManagerService;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/net/PaymentManager$PmsTask;->mService:Lmiui/net/IPaymentManagerService;

    return-object v0
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .registers 7
    .parameter "name"
    .parameter "service"

    .prologue
    const-string v1, "PaymentManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onServiceConnected, component:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p2}, Lmiui/net/IPaymentManagerService$Stub;->asInterface(Landroid/os/IBinder;)Lmiui/net/IPaymentManagerService;

    move-result-object v1

    iput-object v1, p0, Lmiui/net/PaymentManager$PmsTask;->mService:Lmiui/net/IPaymentManagerService;

    :try_start_1e
    invoke-virtual {p0}, Lmiui/net/PaymentManager$PmsTask;->doWork()V
    :try_end_21
    .catch Landroid/os/RemoteException; {:try_start_1e .. :try_end_21} :catch_22

    :goto_21
    return-void

    :catch_22
    move-exception v0

    .local v0, e:Landroid/os/RemoteException;
    invoke-virtual {p0, v0}, Lmiui/net/PaymentManager$PmsTask;->setException(Ljava/lang/Throwable;)V

    goto :goto_21
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .registers 5
    .parameter "name"

    .prologue
    invoke-virtual {p0}, Lmiui/net/PaymentManager$PmsTask;->isDone()Z

    move-result v0

    if-nez v0, :cond_18

    const-string v0, "PaymentManager"

    const-string v1, "payment service disconnected, but task is not completed"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lmiui/net/exception/PaymentServiceFailureException;

    const/4 v1, 0x1

    const-string v2, "active service exits unexpectedly"

    invoke-direct {v0, v1, v2}, Lmiui/net/exception/PaymentServiceFailureException;-><init>(ILjava/lang/String;)V

    invoke-virtual {p0, v0}, Lmiui/net/PaymentManager$PmsTask;->setException(Ljava/lang/Throwable;)V

    :cond_18
    const/4 v0, 0x0

    iput-object v0, p0, Lmiui/net/PaymentManager$PmsTask;->mService:Lmiui/net/IPaymentManagerService;

    return-void
.end method

.method protected set(Landroid/os/Bundle;)V
    .registers 2
    .parameter "bundle"

    .prologue
    invoke-super {p0, p1}, Ljava/util/concurrent/FutureTask;->set(Ljava/lang/Object;)V

    invoke-virtual {p0}, Lmiui/net/PaymentManager$PmsTask;->unBind()V

    return-void
.end method

.method protected bridge synthetic set(Ljava/lang/Object;)V
    .registers 2
    .parameter "x0"

    .prologue
    check-cast p1, Landroid/os/Bundle;

    .end local p1
    invoke-virtual {p0, p1}, Lmiui/net/PaymentManager$PmsTask;->set(Landroid/os/Bundle;)V

    return-void
.end method

.method protected setException(Ljava/lang/Throwable;)V
    .registers 2
    .parameter "t"

    .prologue
    invoke-super {p0, p1}, Ljava/util/concurrent/FutureTask;->setException(Ljava/lang/Throwable;)V

    invoke-virtual {p0}, Lmiui/net/PaymentManager$PmsTask;->unBind()V

    return-void
.end method

.method public final start()Lmiui/net/PaymentManager$PaymentManagerFuture;
    .registers 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lmiui/net/PaymentManager$PaymentManagerFuture",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .prologue
    invoke-virtual {p0}, Lmiui/net/PaymentManager$PmsTask;->bind()V

    return-object p0
.end method

.method protected unBind()V
    .registers 3

    .prologue
    iget-object v0, p0, Lmiui/net/PaymentManager$PmsTask;->this$0:Lmiui/net/PaymentManager;

    #getter for: Lmiui/net/PaymentManager;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lmiui/net/PaymentManager;->access$400(Lmiui/net/PaymentManager;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    const-string v0, "PaymentManager"

    const-string v1, "service unbinded"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
