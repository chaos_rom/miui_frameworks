.class public Lmiui/net/PaymentManager;
.super Ljava/lang/Object;
.source "PaymentManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/net/PaymentManager$PaymentManagerFuture;,
        Lmiui/net/PaymentManager$PmsTask;,
        Lmiui/net/PaymentManager$PaymentCallback;,
        Lmiui/net/PaymentManager$PaymentManagerCallback;,
        Lmiui/net/PaymentManager$PaymentListener;,
        Lmiui/net/PaymentManager$AddAccountCallback;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = true

.field public static final ERROR_CODE_AUTHENTICATION_ERROR:I = 0x5

.field public static final ERROR_CODE_CANCELED:I = 0x4

.field public static final ERROR_CODE_DUPLICATE_PURCHASE:I = 0x7

.field public static final ERROR_CODE_EXCEPTION:I = 0x1

.field public static final ERROR_CODE_INVALID_PARAMS:I = 0x2

.field public static final ERROR_CODE_NETWORK_ERROR:I = 0x3

.field public static final ERROR_CODE_SERVER_ERROR:I = 0x6

.field public static final ERROR_CODE_USER_ID_MISMATCH:I = 0x8

.field public static final KEY_ACCOUNT:Ljava/lang/String; = "account"

.field public static final KEY_INTENT:Ljava/lang/String; = "intent"

.field public static final PAYMENT_KEY_ORDER_ID:Ljava/lang/String; = "payment_order_id"

.field public static final PAYMENT_KEY_ORDER_PRICE:Ljava/lang/String; = "payment_order_price"

.field public static final PAYMENT_KEY_ORDER_TITLE:Ljava/lang/String; = "payment_order_title"

.field public static final PAYMENT_KEY_PAYMENT_RESULT:Ljava/lang/String; = "payment_payment_result"

.field public static final PAYMENT_KEY_PRODUCT_ID:Ljava/lang/String; = "payment_product_id"

.field public static final PAYMENT_KEY_QUICK_PAYMENT:Ljava/lang/String; = "payment_quick_payment"

.field public static final PAYMENT_KEY_SERVICE_ID:Ljava/lang/String; = "payment_service_id"

.field public static final PAYMENT_KEY_TRADE_BALANCE:Ljava/lang/String; = "payment_trade_balance"

.field public static final PAYMENT_KEY_TRADE_ID:Ljava/lang/String; = "payment_trade_id"

.field public static final PAYMENT_KEY_TRADE_PRICE:Ljava/lang/String; = "payment_trade_price"

.field private static final TAG:Ljava/lang/String; = "PaymentManager"

.field public static final URL_PAYMENT_BASE:Ljava/lang/String; = "https://billapi.xiaomi.com/"

.field public static final URL_STAGING_PAYMENT_BASE:Ljava/lang/String; = "http://staging.billapi.n.xiaomi.com/"

.field public static final XIAOMI_ACCOUNT_TYPE:Ljava/lang/String; = "com.xiaomi"

.field public static final XIAOMI_PAYMENT_AUTH_TOKEN_TYPE:Ljava/lang/String; = "billcenter"

.field public static final XIAOMI_STAGING_PAYMENT_AUTH_TOKEN_TYPE:Ljava/lang/String; = "sbillcenter"


# instance fields
.field private final mAccountManager:Landroid/accounts/AccountManager;

.field private final mContext:Landroid/content/Context;

.field private final mMainHandler:Landroid/os/Handler;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lmiui/net/PaymentManager;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lmiui/net/PaymentManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    iput-object v0, p0, Lmiui/net/PaymentManager;->mAccountManager:Landroid/accounts/AccountManager;

    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lmiui/net/PaymentManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lmiui/net/PaymentManager;->mMainHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$100(Lmiui/net/PaymentManager;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lmiui/net/PaymentManager$PaymentManagerCallback;Landroid/os/Handler;)Lmiui/net/PaymentManager$PaymentManagerFuture;
    .registers 8
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"
    .parameter "x5"
    .parameter "x6"

    .prologue
    invoke-direct/range {p0 .. p6}, Lmiui/net/PaymentManager;->internalPay(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lmiui/net/PaymentManager$PaymentManagerCallback;Landroid/os/Handler;)Lmiui/net/PaymentManager$PaymentManagerFuture;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lmiui/net/PaymentManager;Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;Lmiui/net/PaymentManager$PaymentManagerCallback;Landroid/os/Handler;)Lmiui/net/PaymentManager$PaymentManagerFuture;
    .registers 7
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"
    .parameter "x5"

    .prologue
    invoke-direct/range {p0 .. p5}, Lmiui/net/PaymentManager;->internalPayForOrder(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;Lmiui/net/PaymentManager$PaymentManagerCallback;Landroid/os/Handler;)Lmiui/net/PaymentManager$PaymentManagerFuture;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lmiui/net/PaymentManager;)Landroid/accounts/AccountManager;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/net/PaymentManager;->mAccountManager:Landroid/accounts/AccountManager;

    return-object v0
.end method

.method static synthetic access$400(Lmiui/net/PaymentManager;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/net/PaymentManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$500(Lmiui/net/PaymentManager;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    .prologue
    iget-object v0, p0, Lmiui/net/PaymentManager;->mMainHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public static get(Landroid/content/Context;)Lmiui/net/PaymentManager;
    .registers 2
    .parameter "context"

    .prologue
    new-instance v0, Lmiui/net/PaymentManager;

    invoke-direct {v0, p0}, Lmiui/net/PaymentManager;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private internalPay(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lmiui/net/PaymentManager$PaymentManagerCallback;Landroid/os/Handler;)Lmiui/net/PaymentManager$PaymentManagerFuture;
    .registers 15
    .parameter "account"
    .parameter "serviceId"
    .parameter "productId"
    .parameter "extra"
    .parameter
    .parameter "handler"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/os/Bundle;",
            "Lmiui/net/PaymentManager$PaymentManagerCallback",
            "<",
            "Landroid/os/Bundle;",
            ">;",
            "Landroid/os/Handler;",
            ")",
            "Lmiui/net/PaymentManager$PaymentManagerFuture",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .prologue
    .local p5, callback:Lmiui/net/PaymentManager$PaymentManagerCallback;,"Lmiui/net/PaymentManager$PaymentManagerCallback<Landroid/os/Bundle;>;"
    new-instance v0, Lmiui/net/PaymentManager$4;

    move-object v1, p0

    move-object v2, p6

    move-object v3, p5

    move-object v4, p4

    move-object v5, p1

    move-object v6, p2

    move-object v7, p3

    invoke-direct/range {v0 .. v7}, Lmiui/net/PaymentManager$4;-><init>(Lmiui/net/PaymentManager;Landroid/os/Handler;Lmiui/net/PaymentManager$PaymentManagerCallback;Landroid/os/Bundle;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lmiui/net/PaymentManager$4;->start()Lmiui/net/PaymentManager$PaymentManagerFuture;

    move-result-object v0

    return-object v0
.end method

.method private internalPayForOrder(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;Lmiui/net/PaymentManager$PaymentManagerCallback;Landroid/os/Handler;)Lmiui/net/PaymentManager$PaymentManagerFuture;
    .registers 13
    .parameter "account"
    .parameter "order"
    .parameter "extra"
    .parameter
    .parameter "handler"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Ljava/lang/String;",
            "Landroid/os/Bundle;",
            "Lmiui/net/PaymentManager$PaymentManagerCallback",
            "<",
            "Landroid/os/Bundle;",
            ">;",
            "Landroid/os/Handler;",
            ")",
            "Lmiui/net/PaymentManager$PaymentManagerFuture",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .prologue
    .local p4, callback:Lmiui/net/PaymentManager$PaymentManagerCallback;,"Lmiui/net/PaymentManager$PaymentManagerCallback<Landroid/os/Bundle;>;"
    new-instance v0, Lmiui/net/PaymentManager$5;

    move-object v1, p0

    move-object v2, p5

    move-object v3, p4

    move-object v4, p3

    move-object v5, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lmiui/net/PaymentManager$5;-><init>(Lmiui/net/PaymentManager;Landroid/os/Handler;Lmiui/net/PaymentManager$PaymentManagerCallback;Landroid/os/Bundle;Landroid/accounts/Account;Ljava/lang/String;)V

    invoke-virtual {v0}, Lmiui/net/PaymentManager$5;->start()Lmiui/net/PaymentManager$PaymentManagerFuture;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public gotoMiliCenter(Landroid/app/Activity;)V
    .registers 12
    .parameter "activity"

    .prologue
    const/4 v3, 0x0

    iget-object v0, p0, Lmiui/net/PaymentManager;->mAccountManager:Landroid/accounts/AccountManager;

    const-string v1, "com.xiaomi"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v8

    .local v8, account:[Landroid/accounts/Account;
    array-length v0, v8

    if-nez v0, :cond_1e

    new-instance v6, Lmiui/net/PaymentManager$3;

    invoke-direct {v6, p0, p1}, Lmiui/net/PaymentManager$3;-><init>(Lmiui/net/PaymentManager;Landroid/app/Activity;)V

    .local v6, accountCallback:Lmiui/net/PaymentManager$AddAccountCallback;
    iget-object v0, p0, Lmiui/net/PaymentManager;->mAccountManager:Landroid/accounts/AccountManager;

    const-string v1, "com.xiaomi"

    const-string v2, "billcenter"

    move-object v4, v3

    move-object v5, p1

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/accounts/AccountManager;->addAccount(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .end local v6           #accountCallback:Lmiui/net/PaymentManager$AddAccountCallback;
    :goto_1d
    return-void

    :cond_1e
    new-instance v9, Landroid/content/Intent;

    const-string v0, "com.xiaomi.xmsf.action.MILICENTER"

    invoke-direct {v9, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .local v9, intent:Landroid/content/Intent;
    const-string v0, "account"

    const/4 v1, 0x0

    aget-object v1, v8, v1

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/high16 v0, 0x1000

    invoke-virtual {v9, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p1, v9}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1d
.end method

.method public pay(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lmiui/net/PaymentManager$PaymentListener;)V
    .registers 16
    .parameter "activity"
    .parameter "serviceId"
    .parameter "productId"
    .parameter "extra"
    .parameter "paymentListener"

    .prologue
    if-nez p1, :cond_a

    new-instance v1, Ljava/security/InvalidParameterException;

    const-string v2, "activity cannot be null"

    invoke-direct {v1, v2}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_a
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_18

    new-instance v1, Ljava/security/InvalidParameterException;

    const-string v2, "service id cannot be empty"

    invoke-direct {v1, v2}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_18
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_26

    new-instance v1, Ljava/security/InvalidParameterException;

    const-string v2, "product id cannot be empty"

    invoke-direct {v1, v2}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_26
    iget-object v1, p0, Lmiui/net/PaymentManager;->mAccountManager:Landroid/accounts/AccountManager;

    const-string v2, "com.xiaomi"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v9

    .local v9, account:[Landroid/accounts/Account;
    array-length v1, v9

    if-nez v1, :cond_4a

    new-instance v0, Lmiui/net/PaymentManager$1;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p5

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lmiui/net/PaymentManager$1;-><init>(Lmiui/net/PaymentManager;Ljava/lang/String;Ljava/lang/String;Lmiui/net/PaymentManager$PaymentListener;Landroid/os/Bundle;)V

    .local v0, accountCallback:Lmiui/net/PaymentManager$AddAccountCallback;
    iget-object v1, p0, Lmiui/net/PaymentManager;->mAccountManager:Landroid/accounts/AccountManager;

    const-string v2, "com.xiaomi"

    const-string v3, "billcenter"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v8, 0x0

    move-object v6, p1

    move-object v7, v0

    invoke-virtual/range {v1 .. v8}, Landroid/accounts/AccountManager;->addAccount(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .end local v0           #accountCallback:Lmiui/net/PaymentManager$AddAccountCallback;
    :goto_49
    return-void

    :cond_4a
    new-instance v6, Lmiui/net/PaymentManager$PaymentCallback;

    invoke-direct {v6, p0, p2, p3, p5}, Lmiui/net/PaymentManager$PaymentCallback;-><init>(Lmiui/net/PaymentManager;Ljava/lang/String;Ljava/lang/String;Lmiui/net/PaymentManager$PaymentListener;)V

    .local v6, paymentCallback:Lmiui/net/PaymentManager$PaymentCallback;
    const/4 v1, 0x0

    aget-object v2, v9, v1

    const/4 v7, 0x0

    move-object v1, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v1 .. v7}, Lmiui/net/PaymentManager;->internalPay(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lmiui/net/PaymentManager$PaymentManagerCallback;Landroid/os/Handler;)Lmiui/net/PaymentManager$PaymentManagerFuture;

    goto :goto_49
.end method

.method public payForOrder(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lmiui/net/PaymentManager$PaymentListener;)V
    .registers 16
    .parameter "activity"
    .parameter "paymentId"
    .parameter "order"
    .parameter "extra"
    .parameter "paymentListener"

    .prologue
    if-nez p1, :cond_a

    new-instance v1, Ljava/security/InvalidParameterException;

    const-string v2, "activity cannot be null"

    invoke-direct {v1, v2}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_a
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_18

    new-instance v1, Ljava/security/InvalidParameterException;

    const-string v2, "paymentId cannot be empty"

    invoke-direct {v1, v2}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_18
    iget-object v1, p0, Lmiui/net/PaymentManager;->mAccountManager:Landroid/accounts/AccountManager;

    const-string v2, "com.xiaomi"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v9

    .local v9, account:[Landroid/accounts/Account;
    array-length v1, v9

    if-nez v1, :cond_3c

    new-instance v0, Lmiui/net/PaymentManager$2;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p5

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lmiui/net/PaymentManager$2;-><init>(Lmiui/net/PaymentManager;Ljava/lang/String;Lmiui/net/PaymentManager$PaymentListener;Ljava/lang/String;Landroid/os/Bundle;)V

    .local v0, accountCallback:Lmiui/net/PaymentManager$AddAccountCallback;
    iget-object v1, p0, Lmiui/net/PaymentManager;->mAccountManager:Landroid/accounts/AccountManager;

    const-string v2, "com.xiaomi"

    const-string v3, "billcenter"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v8, 0x0

    move-object v6, p1

    move-object v7, v0

    invoke-virtual/range {v1 .. v8}, Landroid/accounts/AccountManager;->addAccount(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .end local v0           #accountCallback:Lmiui/net/PaymentManager$AddAccountCallback;
    :goto_3b
    return-void

    :cond_3c
    new-instance v5, Lmiui/net/PaymentManager$PaymentCallback;

    const-string v1, "thd"

    invoke-direct {v5, p0, v1, p2, p5}, Lmiui/net/PaymentManager$PaymentCallback;-><init>(Lmiui/net/PaymentManager;Ljava/lang/String;Ljava/lang/String;Lmiui/net/PaymentManager$PaymentListener;)V

    .local v5, paymentCallback:Lmiui/net/PaymentManager$PaymentCallback;
    const/4 v1, 0x0

    aget-object v2, v9, v1

    const/4 v6, 0x0

    move-object v1, p0

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v1 .. v6}, Lmiui/net/PaymentManager;->internalPayForOrder(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;Lmiui/net/PaymentManager$PaymentManagerCallback;Landroid/os/Handler;)Lmiui/net/PaymentManager$PaymentManagerFuture;

    goto :goto_3b
.end method
