.class Lmiui/net/micloudrichmedia/SimpleUploadChunkRequest;
.super Lmiui/net/micloudrichmedia/FirstUploadChunkRequest;
.source "SimpleUploadChunkRequest.java"


# static fields
.field private static final BASE_URL:Ljava/lang/String; = "http://fileapi.micloud.xiaomi.net/mic/v1/file/user/%s/full/more"


# instance fields
.field private mOffset:I


# direct methods
.method public constructor <init>(Lmiui/net/micloudrichmedia/UploadEntity;ZII)V
    .registers 5
    .parameter "file"
    .parameter "isLastChunk"
    .parameter "length"
    .parameter "offset"

    .prologue
    invoke-direct {p0, p1, p2, p3}, Lmiui/net/micloudrichmedia/FirstUploadChunkRequest;-><init>(Lmiui/net/micloudrichmedia/UploadEntity;ZI)V

    iput p4, p0, Lmiui/net/micloudrichmedia/SimpleUploadChunkRequest;->mOffset:I

    return-void
.end method


# virtual methods
.method protected getBaseUrl()Ljava/lang/String;
    .registers 2

    .prologue
    const-string v0, "http://fileapi.micloud.xiaomi.net/mic/v1/file/user/%s/full/more"

    return-object v0
.end method

.method protected getParams(Ljava/lang/String;)Ljava/util/List;
    .registers 7
    .parameter "security"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;,
            Ljavax/crypto/IllegalBlockSizeException;,
            Ljavax/crypto/BadPaddingException;
        }
    .end annotation

    .prologue
    iget-object v1, p0, Lmiui/net/micloudrichmedia/UploadRequest;->mFile:Lmiui/net/micloudrichmedia/UploadEntity;

    invoke-virtual {v1}, Lmiui/net/micloudrichmedia/UploadEntity;->getTempId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_18

    iget-object v1, p0, Lmiui/net/micloudrichmedia/UploadRequest;->mFile:Lmiui/net/micloudrichmedia/UploadEntity;

    invoke-virtual {v1}, Lmiui/net/micloudrichmedia/UploadEntity;->getHostingServer()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_20

    :cond_18
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "The tempid or hosting server should not be null for the non first chunk"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_20
    invoke-super {p0, p1}, Lmiui/net/micloudrichmedia/FirstUploadChunkRequest;->getParams(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .local v0, list:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "tmpid"

    iget-object v3, p0, Lmiui/net/micloudrichmedia/UploadRequest;->mFile:Lmiui/net/micloudrichmedia/UploadEntity;

    invoke-virtual {v3}, Lmiui/net/micloudrichmedia/UploadEntity;->getTempId()Ljava/lang/String;

    move-result-object v3

    const-string v4, "UTF-8"

    invoke-static {p1, v3, v4}, Lmiui/net/CloudCoder;->encodeString(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "_hostingserver"

    iget-object v3, p0, Lmiui/net/micloudrichmedia/UploadRequest;->mFile:Lmiui/net/micloudrichmedia/UploadEntity;

    invoke-virtual {v3}, Lmiui/net/micloudrichmedia/UploadEntity;->getHostingServer()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "offset"

    iget v3, p0, Lmiui/net/micloudrichmedia/SimpleUploadChunkRequest;->mOffset:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "UTF-8"

    invoke-static {p1, v3, v4}, Lmiui/net/CloudCoder;->encodeString(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method
