.class abstract Lmiui/net/micloudrichmedia/UploadRequest;
.super Lmiui/net/micloudrichmedia/Request;
.source "UploadRequest.java"


# static fields
.field protected static final BOUNDARY:Ljava/lang/String; = "*****"

.field protected static final LINE_END:Ljava/lang/String; = "\r\n"

.field protected static final TWO_HYPHENS:Ljava/lang/String; = "--"


# instance fields
.field protected mFile:Lmiui/net/micloudrichmedia/UploadEntity;


# direct methods
.method constructor <init>(Lmiui/net/micloudrichmedia/UploadEntity;)V
    .registers 4
    .parameter "file"

    .prologue
    invoke-direct {p0}, Lmiui/net/micloudrichmedia/Request;-><init>()V

    if-nez p1, :cond_d

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The file should not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_d
    iput-object p1, p0, Lmiui/net/micloudrichmedia/UploadRequest;->mFile:Lmiui/net/micloudrichmedia/UploadEntity;

    return-void
.end method


# virtual methods
.method protected getConn(Ljava/lang/String;Ljava/lang/String;)Ljava/net/HttpURLConnection;
    .registers 6
    .parameter "url"
    .parameter "cookie"

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-super {p0, p1, p2}, Lmiui/net/micloudrichmedia/Request;->getConn(Ljava/lang/String;Ljava/lang/String;)Ljava/net/HttpURLConnection;

    move-result-object v0

    .local v0, conn:Ljava/net/HttpURLConnection;
    if-eqz v0, :cond_14

    invoke-virtual {v0, v2}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    invoke-virtual {v0, v2}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setUseCaches(Z)V

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    :cond_14
    return-object v0
.end method

.method protected getHttpMethod()Ljava/lang/String;
    .registers 2

    .prologue
    const-string v0, "POST"

    return-object v0
.end method
