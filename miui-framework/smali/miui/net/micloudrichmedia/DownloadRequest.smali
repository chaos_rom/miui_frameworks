.class Lmiui/net/micloudrichmedia/DownloadRequest;
.super Lmiui/net/micloudrichmedia/Request;
.source "DownloadRequest.java"


# static fields
.field private static final CONN_RANGE_PROPERTY:Ljava/lang/String; = "RANGE"

.field private static final CONN_RANGE_VALUE_FORMAT:Ljava/lang/String; = "bytes=%d-"


# instance fields
.field private mBaseUrl:Ljava/lang/String;

.field private mCkey:Ljava/lang/String;

.field private mFi:Ljava/lang/String;

.field private mFileSha1:Ljava/lang/String;

.field private mRetryTimes:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "baseUrl"
    .parameter "ckey"
    .parameter "fi"
    .parameter "fileSha1"

    .prologue
    invoke-direct {p0}, Lmiui/net/micloudrichmedia/Request;-><init>()V

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1b

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1b

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1b

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_23

    :cond_1b
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The download requset parameters should not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_23
    iput-object p1, p0, Lmiui/net/micloudrichmedia/DownloadRequest;->mBaseUrl:Ljava/lang/String;

    iput-object p2, p0, Lmiui/net/micloudrichmedia/DownloadRequest;->mCkey:Ljava/lang/String;

    iput-object p3, p0, Lmiui/net/micloudrichmedia/DownloadRequest;->mFi:Ljava/lang/String;

    iput-object p4, p0, Lmiui/net/micloudrichmedia/DownloadRequest;->mFileSha1:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lmiui/net/micloudrichmedia/DownloadRequest;->mRetryTimes:I

    return-void
.end method

.method private getTemporaryDownloadData(Landroid/content/Context;)Ljava/io/ByteArrayOutputStream;
    .registers 14
    .parameter "context"

    .prologue
    const/4 v11, 0x0

    new-instance v2, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lmiui/net/micloudrichmedia/DownloadRequest;->mFileSha1:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v2, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .local v2, file:Ljava/io/File;
    const/4 v6, 0x0

    .local v6, outputStream:Ljava/io/ByteArrayOutputStream;
    const/4 v3, 0x0

    .local v3, inputStream:Ljava/io/InputStream;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_66

    const-string v8, "getTemporaryDownloadData:The temporary downloaded file %s exist"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v11

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->log(Ljava/lang/String;)V

    :try_start_3d
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_42
    .catchall {:try_start_3d .. :try_end_42} :catchall_91
    .catch Ljava/io/FileNotFoundException; {:try_start_3d .. :try_end_42} :catch_ae
    .catch Ljava/io/IOException; {:try_start_3d .. :try_end_42} :catch_7f

    .end local v3           #inputStream:Ljava/io/InputStream;
    .local v4, inputStream:Ljava/io/InputStream;
    :try_start_42
    new-instance v7, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v7}, Ljava/io/ByteArrayOutputStream;-><init>()V
    :try_end_47
    .catchall {:try_start_42 .. :try_end_47} :catchall_a0
    .catch Ljava/io/FileNotFoundException; {:try_start_42 .. :try_end_47} :catch_b0
    .catch Ljava/io/IOException; {:try_start_42 .. :try_end_47} :catch_a7

    .end local v6           #outputStream:Ljava/io/ByteArrayOutputStream;
    .local v7, outputStream:Ljava/io/ByteArrayOutputStream;
    const/16 v8, 0x400

    :try_start_49
    new-array v0, v8, [B

    .local v0, buff:[B
    const/4 v5, 0x0

    .local v5, len:I
    :goto_4c
    invoke-virtual {v4, v0}, Ljava/io/FileInputStream;->read([B)I

    move-result v5

    const/4 v8, -0x1

    if-eq v5, v8, :cond_67

    const/4 v8, 0x0

    invoke-virtual {v7, v0, v8, v5}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_57
    .catchall {:try_start_49 .. :try_end_57} :catchall_a3
    .catch Ljava/io/FileNotFoundException; {:try_start_49 .. :try_end_57} :catch_58
    .catch Ljava/io/IOException; {:try_start_49 .. :try_end_57} :catch_aa

    goto :goto_4c

    .end local v0           #buff:[B
    .end local v5           #len:I
    :catch_58
    move-exception v1

    move-object v3, v4

    .end local v4           #inputStream:Ljava/io/InputStream;
    .restart local v3       #inputStream:Ljava/io/InputStream;
    move-object v6, v7

    .end local v7           #outputStream:Ljava/io/ByteArrayOutputStream;
    .local v1, e:Ljava/io/FileNotFoundException;
    .restart local v6       #outputStream:Ljava/io/ByteArrayOutputStream;
    :goto_5b
    :try_start_5b
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_5e
    .catchall {:try_start_5b .. :try_end_5e} :catchall_91

    if-eqz v3, :cond_63

    :try_start_60
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_63
    .catch Ljava/io/IOException; {:try_start_60 .. :try_end_63} :catch_7a

    .end local v1           #e:Ljava/io/FileNotFoundException;
    :cond_63
    :goto_63
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    :cond_66
    :goto_66
    return-object v6

    .end local v3           #inputStream:Ljava/io/InputStream;
    .end local v6           #outputStream:Ljava/io/ByteArrayOutputStream;
    .restart local v0       #buff:[B
    .restart local v4       #inputStream:Ljava/io/InputStream;
    .restart local v5       #len:I
    .restart local v7       #outputStream:Ljava/io/ByteArrayOutputStream;
    :cond_67
    :try_start_67
    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->flush()V
    :try_end_6a
    .catchall {:try_start_67 .. :try_end_6a} :catchall_a3
    .catch Ljava/io/FileNotFoundException; {:try_start_67 .. :try_end_6a} :catch_58
    .catch Ljava/io/IOException; {:try_start_67 .. :try_end_6a} :catch_aa

    if-eqz v4, :cond_6f

    :try_start_6c
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_6f
    .catch Ljava/io/IOException; {:try_start_6c .. :try_end_6f} :catch_75

    :cond_6f
    :goto_6f
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    move-object v3, v4

    .end local v4           #inputStream:Ljava/io/InputStream;
    .restart local v3       #inputStream:Ljava/io/InputStream;
    move-object v6, v7

    .end local v7           #outputStream:Ljava/io/ByteArrayOutputStream;
    .restart local v6       #outputStream:Ljava/io/ByteArrayOutputStream;
    goto :goto_66

    .end local v3           #inputStream:Ljava/io/InputStream;
    .end local v6           #outputStream:Ljava/io/ByteArrayOutputStream;
    .restart local v4       #inputStream:Ljava/io/InputStream;
    .restart local v7       #outputStream:Ljava/io/ByteArrayOutputStream;
    :catch_75
    move-exception v1

    .local v1, e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6f

    .end local v0           #buff:[B
    .end local v4           #inputStream:Ljava/io/InputStream;
    .end local v5           #len:I
    .end local v7           #outputStream:Ljava/io/ByteArrayOutputStream;
    .local v1, e:Ljava/io/FileNotFoundException;
    .restart local v3       #inputStream:Ljava/io/InputStream;
    .restart local v6       #outputStream:Ljava/io/ByteArrayOutputStream;
    :catch_7a
    move-exception v1

    .local v1, e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_63

    .end local v1           #e:Ljava/io/IOException;
    :catch_7f
    move-exception v1

    .restart local v1       #e:Ljava/io/IOException;
    :goto_80
    :try_start_80
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_83
    .catchall {:try_start_80 .. :try_end_83} :catchall_91

    if-eqz v3, :cond_88

    :try_start_85
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_88
    .catch Ljava/io/IOException; {:try_start_85 .. :try_end_88} :catch_8c

    :cond_88
    :goto_88
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    goto :goto_66

    :catch_8c
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_88

    .end local v1           #e:Ljava/io/IOException;
    :catchall_91
    move-exception v8

    :goto_92
    if-eqz v3, :cond_97

    :try_start_94
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_97
    .catch Ljava/io/IOException; {:try_start_94 .. :try_end_97} :catch_9b

    :cond_97
    :goto_97
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    throw v8

    :catch_9b
    move-exception v1

    .restart local v1       #e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_97

    .end local v1           #e:Ljava/io/IOException;
    .end local v3           #inputStream:Ljava/io/InputStream;
    .restart local v4       #inputStream:Ljava/io/InputStream;
    :catchall_a0
    move-exception v8

    move-object v3, v4

    .end local v4           #inputStream:Ljava/io/InputStream;
    .restart local v3       #inputStream:Ljava/io/InputStream;
    goto :goto_92

    .end local v3           #inputStream:Ljava/io/InputStream;
    .end local v6           #outputStream:Ljava/io/ByteArrayOutputStream;
    .restart local v4       #inputStream:Ljava/io/InputStream;
    .restart local v7       #outputStream:Ljava/io/ByteArrayOutputStream;
    :catchall_a3
    move-exception v8

    move-object v3, v4

    .end local v4           #inputStream:Ljava/io/InputStream;
    .restart local v3       #inputStream:Ljava/io/InputStream;
    move-object v6, v7

    .end local v7           #outputStream:Ljava/io/ByteArrayOutputStream;
    .restart local v6       #outputStream:Ljava/io/ByteArrayOutputStream;
    goto :goto_92

    .end local v3           #inputStream:Ljava/io/InputStream;
    .restart local v4       #inputStream:Ljava/io/InputStream;
    :catch_a7
    move-exception v1

    move-object v3, v4

    .end local v4           #inputStream:Ljava/io/InputStream;
    .restart local v3       #inputStream:Ljava/io/InputStream;
    goto :goto_80

    .end local v3           #inputStream:Ljava/io/InputStream;
    .end local v6           #outputStream:Ljava/io/ByteArrayOutputStream;
    .restart local v4       #inputStream:Ljava/io/InputStream;
    .restart local v7       #outputStream:Ljava/io/ByteArrayOutputStream;
    :catch_aa
    move-exception v1

    move-object v3, v4

    .end local v4           #inputStream:Ljava/io/InputStream;
    .restart local v3       #inputStream:Ljava/io/InputStream;
    move-object v6, v7

    .end local v7           #outputStream:Ljava/io/ByteArrayOutputStream;
    .restart local v6       #outputStream:Ljava/io/ByteArrayOutputStream;
    goto :goto_80

    :catch_ae
    move-exception v1

    goto :goto_5b

    .end local v3           #inputStream:Ljava/io/InputStream;
    .restart local v4       #inputStream:Ljava/io/InputStream;
    :catch_b0
    move-exception v1

    move-object v3, v4

    .end local v4           #inputStream:Ljava/io/InputStream;
    .restart local v3       #inputStream:Ljava/io/InputStream;
    goto :goto_5b
.end method

.method private saveTemporaryDownloadedData(Landroid/content/Context;Ljava/io/ByteArrayOutputStream;)V
    .registers 10
    .parameter "context"
    .parameter "out"

    .prologue
    const/4 v1, 0x0

    .local v1, outputStream:Ljava/io/FileOutputStream;
    :try_start_1
    const-string v3, "Save temporary downloaded data to file %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lmiui/net/micloudrichmedia/DownloadRequest;->mFileSha1:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->log(Ljava/lang/String;)V

    new-instance v2, Ljava/io/FileOutputStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lmiui/net/micloudrichmedia/DownloadRequest;->mFileSha1:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_34
    .catchall {:try_start_1 .. :try_end_34} :catchall_69
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_34} :catch_4b
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_34} :catch_5a

    .end local v1           #outputStream:Ljava/io/FileOutputStream;
    .local v2, outputStream:Ljava/io/FileOutputStream;
    :try_start_34
    invoke-virtual {p2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/FileOutputStream;->write([B)V

    invoke-virtual {v2}, Ljava/io/FileOutputStream;->flush()V
    :try_end_3e
    .catchall {:try_start_34 .. :try_end_3e} :catchall_75
    .catch Ljava/io/FileNotFoundException; {:try_start_34 .. :try_end_3e} :catch_7b
    .catch Ljava/io/IOException; {:try_start_34 .. :try_end_3e} :catch_78

    if-eqz v2, :cond_7e

    :try_start_40
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_43
    .catch Ljava/io/IOException; {:try_start_40 .. :try_end_43} :catch_45

    move-object v1, v2

    .end local v2           #outputStream:Ljava/io/FileOutputStream;
    .restart local v1       #outputStream:Ljava/io/FileOutputStream;
    :cond_44
    :goto_44
    return-void

    .end local v1           #outputStream:Ljava/io/FileOutputStream;
    .restart local v2       #outputStream:Ljava/io/FileOutputStream;
    :catch_45
    move-exception v0

    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move-object v1, v2

    .end local v2           #outputStream:Ljava/io/FileOutputStream;
    .restart local v1       #outputStream:Ljava/io/FileOutputStream;
    goto :goto_44

    .end local v0           #e:Ljava/io/IOException;
    :catch_4b
    move-exception v0

    .local v0, e:Ljava/io/FileNotFoundException;
    :goto_4c
    :try_start_4c
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_4f
    .catchall {:try_start_4c .. :try_end_4f} :catchall_69

    if-eqz v1, :cond_44

    :try_start_51
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_54
    .catch Ljava/io/IOException; {:try_start_51 .. :try_end_54} :catch_55

    goto :goto_44

    :catch_55
    move-exception v0

    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_44

    .end local v0           #e:Ljava/io/IOException;
    :catch_5a
    move-exception v0

    .restart local v0       #e:Ljava/io/IOException;
    :goto_5b
    :try_start_5b
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5e
    .catchall {:try_start_5b .. :try_end_5e} :catchall_69

    if-eqz v1, :cond_44

    :try_start_60
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_63
    .catch Ljava/io/IOException; {:try_start_60 .. :try_end_63} :catch_64

    goto :goto_44

    :catch_64
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_44

    .end local v0           #e:Ljava/io/IOException;
    :catchall_69
    move-exception v3

    :goto_6a
    if-eqz v1, :cond_6f

    :try_start_6c
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_6f
    .catch Ljava/io/IOException; {:try_start_6c .. :try_end_6f} :catch_70

    :cond_6f
    :goto_6f
    throw v3

    :catch_70
    move-exception v0

    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6f

    .end local v0           #e:Ljava/io/IOException;
    .end local v1           #outputStream:Ljava/io/FileOutputStream;
    .restart local v2       #outputStream:Ljava/io/FileOutputStream;
    :catchall_75
    move-exception v3

    move-object v1, v2

    .end local v2           #outputStream:Ljava/io/FileOutputStream;
    .restart local v1       #outputStream:Ljava/io/FileOutputStream;
    goto :goto_6a

    .end local v1           #outputStream:Ljava/io/FileOutputStream;
    .restart local v2       #outputStream:Ljava/io/FileOutputStream;
    :catch_78
    move-exception v0

    move-object v1, v2

    .end local v2           #outputStream:Ljava/io/FileOutputStream;
    .restart local v1       #outputStream:Ljava/io/FileOutputStream;
    goto :goto_5b

    .end local v1           #outputStream:Ljava/io/FileOutputStream;
    .restart local v2       #outputStream:Ljava/io/FileOutputStream;
    :catch_7b
    move-exception v0

    move-object v1, v2

    .end local v2           #outputStream:Ljava/io/FileOutputStream;
    .restart local v1       #outputStream:Ljava/io/FileOutputStream;
    goto :goto_4c

    .end local v1           #outputStream:Ljava/io/FileOutputStream;
    .restart local v2       #outputStream:Ljava/io/FileOutputStream;
    :cond_7e
    move-object v1, v2

    .end local v2           #outputStream:Ljava/io/FileOutputStream;
    .restart local v1       #outputStream:Ljava/io/FileOutputStream;
    goto :goto_44
.end method


# virtual methods
.method public download(Landroid/content/Context;Ljava/lang/String;Lmiui/net/ExtendedAuthToken;Ljava/lang/String;)[B
    .registers 23
    .parameter "context"
    .parameter "userId"
    .parameter "authToken"
    .parameter "cookie"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/crypto/IllegalBlockSizeException;,
            Ljavax/crypto/BadPaddingException;,
            Ljava/io/IOException;,
            Lmiui/net/exception/CloudServiceFailureException;,
            Lorg/apache/commons/codec/DecoderException;,
            Lmiui/net/exception/MiCloudServerException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .local v5, conn:Ljava/net/HttpURLConnection;
    const/4 v7, 0x0

    .local v7, dis:Ljavax/crypto/CipherInputStream;
    const/4 v3, 0x0

    .local v3, byteStream:Ljava/io/ByteArrayOutputStream;
    :try_start_3
    move-object/from16 v0, p3

    iget-object v13, v0, Lmiui/net/ExtendedAuthToken;->security:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v13, v1}, Lmiui/net/micloudrichmedia/DownloadRequest;->getParamsWithSignature(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v11

    .local v11, nameValuePairs:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    invoke-virtual/range {p0 .. p0}, Lmiui/net/micloudrichmedia/DownloadRequest;->getBaseUrl()Ljava/lang/String;

    move-result-object v13

    const-string v14, "UTF-8"

    invoke-static {v11, v14}, Lorg/apache/http/client/utils/URLEncodedUtils;->format(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v14}, Lmiui/net/micloudrichmedia/DownloadRequest;->getTargetUrl(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-virtual {v0, v13, v1}, Lmiui/net/micloudrichmedia/DownloadRequest;->getConn(Ljava/lang/String;Ljava/lang/String;)Ljava/net/HttpURLConnection;

    move-result-object v5

    invoke-direct/range {p0 .. p1}, Lmiui/net/micloudrichmedia/DownloadRequest;->getTemporaryDownloadData(Landroid/content/Context;)Ljava/io/ByteArrayOutputStream;

    move-result-object v3

    if-eqz v3, :cond_a6

    const-string v13, "RANGE"

    const-string v14, "bytes=%d-"

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v5, v13, v14}, Ljava/net/HttpURLConnection;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    :goto_47
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->connect()V

    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v12

    .local v12, responseCode:I
    const/16 v13, 0xc8

    if-eq v12, v13, :cond_5c

    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v13

    if-lez v13, :cond_146

    const/16 v13, 0xce

    if-ne v12, v13, :cond_146

    :cond_5c
    new-instance v8, Ljavax/crypto/CipherInputStream;

    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lmiui/net/micloudrichmedia/DownloadRequest;->mCkey:Ljava/lang/String;

    invoke-virtual {v14}, Ljava/lang/String;->toCharArray()[C

    move-result-object v14

    invoke-static {v14}, Lorg/apache/commons/codec/binary/Hex;->decodeHex([C)[B

    move-result-object v14

    const/4 v15, 0x2

    invoke-static {v14, v15}, Lmiui/net/CloudCoder;->newRC4Cipher([BI)Ljavax/crypto/Cipher;

    move-result-object v14

    invoke-direct {v8, v13, v14}, Ljavax/crypto/CipherInputStream;-><init>(Ljava/io/InputStream;Ljavax/crypto/Cipher;)V
    :try_end_76
    .catchall {:try_start_3 .. :try_end_76} :catchall_152

    .end local v7           #dis:Ljavax/crypto/CipherInputStream;
    .local v8, dis:Ljavax/crypto/CipherInputStream;
    const/16 v13, 0x400

    :try_start_78
    new-array v2, v13, [B

    .local v2, buff:[B
    const/4 v10, 0x0

    .local v10, len:I
    :goto_7b
    invoke-virtual {v8, v2}, Ljavax/crypto/CipherInputStream;->read([B)I

    move-result v10

    const/4 v13, -0x1

    if-eq v10, v13, :cond_ad

    const/4 v13, 0x0

    invoke-virtual {v3, v2, v13, v10}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_86
    .catchall {:try_start_78 .. :try_end_86} :catchall_87

    goto :goto_7b

    .end local v2           #buff:[B
    .end local v10           #len:I
    :catchall_87
    move-exception v13

    move-object v7, v8

    .end local v8           #dis:Ljavax/crypto/CipherInputStream;
    .end local v11           #nameValuePairs:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    .end local v12           #responseCode:I
    .restart local v7       #dis:Ljavax/crypto/CipherInputStream;
    :goto_89
    if-eqz v7, :cond_8e

    :try_start_8b
    invoke-virtual {v7}, Ljavax/crypto/CipherInputStream;->close()V
    :try_end_8e
    .catch Ljava/io/IOException; {:try_start_8b .. :try_end_8e} :catch_179

    :cond_8e
    :goto_8e
    if-eqz v3, :cond_a0

    :try_start_90
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v14

    if-lez v14, :cond_9d

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3}, Lmiui/net/micloudrichmedia/DownloadRequest;->saveTemporaryDownloadedData(Landroid/content/Context;Ljava/io/ByteArrayOutputStream;)V

    :cond_9d
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_a0
    .catch Ljava/io/IOException; {:try_start_90 .. :try_end_a0} :catch_17f

    :cond_a0
    :goto_a0
    if-eqz v5, :cond_a5

    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_a5
    throw v13

    .restart local v11       #nameValuePairs:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    :cond_a6
    :try_start_a6
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V
    :try_end_ab
    .catchall {:try_start_a6 .. :try_end_ab} :catchall_152

    .end local v3           #byteStream:Ljava/io/ByteArrayOutputStream;
    .local v4, byteStream:Ljava/io/ByteArrayOutputStream;
    move-object v3, v4

    .end local v4           #byteStream:Ljava/io/ByteArrayOutputStream;
    .restart local v3       #byteStream:Ljava/io/ByteArrayOutputStream;
    goto :goto_47

    .end local v7           #dis:Ljavax/crypto/CipherInputStream;
    .restart local v2       #buff:[B
    .restart local v8       #dis:Ljavax/crypto/CipherInputStream;
    .restart local v10       #len:I
    .restart local v12       #responseCode:I
    :cond_ad
    :try_start_ad
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    .local v6, data:[B
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->reset()V

    invoke-static {v6}, Lmiui/net/CloudCoder;->getDataSha1Digest([B)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lmiui/net/micloudrichmedia/DownloadRequest;->mFileSha1:Ljava/lang/String;

    invoke-static {v13, v14}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    :try_end_bf
    .catchall {:try_start_ad .. :try_end_bf} :catchall_87

    move-result v13

    if-eqz v13, :cond_e9

    if-eqz v8, :cond_c7

    :try_start_c4
    invoke-virtual {v8}, Ljavax/crypto/CipherInputStream;->close()V
    :try_end_c7
    .catch Ljava/io/IOException; {:try_start_c4 .. :try_end_c7} :catch_df

    :cond_c7
    :goto_c7
    if-eqz v3, :cond_d9

    :try_start_c9
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v13

    if-lez v13, :cond_d6

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3}, Lmiui/net/micloudrichmedia/DownloadRequest;->saveTemporaryDownloadedData(Landroid/content/Context;Ljava/io/ByteArrayOutputStream;)V

    :cond_d6
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_d9
    .catch Ljava/io/IOException; {:try_start_c9 .. :try_end_d9} :catch_e4

    :cond_d9
    :goto_d9
    if-eqz v5, :cond_de

    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    .end local v6           #data:[B
    :cond_de
    :goto_de
    return-object v6

    .restart local v6       #data:[B
    :catch_df
    move-exception v9

    .local v9, e:Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_c7

    .end local v9           #e:Ljava/io/IOException;
    :catch_e4
    move-exception v9

    .restart local v9       #e:Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_d9

    .end local v9           #e:Ljava/io/IOException;
    :cond_e9
    :try_start_e9
    move-object/from16 v0, p0

    iget v13, v0, Lmiui/net/micloudrichmedia/DownloadRequest;->mRetryTimes:I

    const/4 v14, 0x3

    if-ge v13, v14, :cond_13e

    move-object/from16 v0, p0

    iget v13, v0, Lmiui/net/micloudrichmedia/DownloadRequest;->mRetryTimes:I

    add-int/lit8 v13, v13, 0x1

    move-object/from16 v0, p0

    iput v13, v0, Lmiui/net/micloudrichmedia/DownloadRequest;->mRetryTimes:I

    const-string v13, "download:Retry %s time to download file because of mismatch sha1"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lmiui/net/micloudrichmedia/DownloadRequest;->mRetryTimes:I

    move/from16 v16, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v13, v14}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->log(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p4}, Lmiui/net/micloudrichmedia/DownloadRequest;->download(Landroid/content/Context;Ljava/lang/String;Lmiui/net/ExtendedAuthToken;Ljava/lang/String;)[B
    :try_end_116
    .catchall {:try_start_e9 .. :try_end_116} :catchall_87

    move-result-object v6

    .end local v6           #data:[B
    if-eqz v8, :cond_11c

    :try_start_119
    invoke-virtual {v8}, Ljavax/crypto/CipherInputStream;->close()V
    :try_end_11c
    .catch Ljava/io/IOException; {:try_start_119 .. :try_end_11c} :catch_134

    :cond_11c
    :goto_11c
    if-eqz v3, :cond_12e

    :try_start_11e
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v13

    if-lez v13, :cond_12b

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3}, Lmiui/net/micloudrichmedia/DownloadRequest;->saveTemporaryDownloadedData(Landroid/content/Context;Ljava/io/ByteArrayOutputStream;)V

    :cond_12b
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_12e
    .catch Ljava/io/IOException; {:try_start_11e .. :try_end_12e} :catch_139

    :cond_12e
    :goto_12e
    if-eqz v5, :cond_de

    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_de

    :catch_134
    move-exception v9

    .restart local v9       #e:Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_11c

    .end local v9           #e:Ljava/io/IOException;
    :catch_139
    move-exception v9

    .restart local v9       #e:Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_12e

    .end local v9           #e:Ljava/io/IOException;
    .restart local v6       #data:[B
    :cond_13e
    :try_start_13e
    new-instance v13, Lmiui/net/exception/CloudServiceFailureException;

    const-string v14, "The download data sha1 is not consistant with server sha1"

    invoke-direct {v13, v14}, Lmiui/net/exception/CloudServiceFailureException;-><init>(Ljava/lang/String;)V

    throw v13
    :try_end_146
    .catchall {:try_start_13e .. :try_end_146} :catchall_87

    .end local v2           #buff:[B
    .end local v6           #data:[B
    .end local v8           #dis:Ljavax/crypto/CipherInputStream;
    .end local v10           #len:I
    .restart local v7       #dis:Ljavax/crypto/CipherInputStream;
    :cond_146
    :try_start_146
    invoke-static {v12}, Lmiui/net/exception/MiCloudServerException;->isMiCloudServerException(I)Z

    move-result v13

    if-eqz v13, :cond_155

    new-instance v13, Lmiui/net/exception/MiCloudServerException;

    invoke-direct {v13, v12}, Lmiui/net/exception/MiCloudServerException;-><init>(I)V

    throw v13

    .end local v11           #nameValuePairs:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    .end local v12           #responseCode:I
    :catchall_152
    move-exception v13

    goto/16 :goto_89

    .restart local v11       #nameValuePairs:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    .restart local v12       #responseCode:I
    :cond_155
    const-string v13, "download:The responsed message is %s, code is %d"

    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getResponseMessage()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x1

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v13, v14}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->log(Ljava/lang/String;)V

    new-instance v13, Lmiui/net/exception/CloudServiceFailureException;

    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getResponseMessage()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Lmiui/net/exception/CloudServiceFailureException;-><init>(Ljava/lang/String;)V

    throw v13
    :try_end_179
    .catchall {:try_start_146 .. :try_end_179} :catchall_152

    .end local v11           #nameValuePairs:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    .end local v12           #responseCode:I
    :catch_179
    move-exception v9

    .restart local v9       #e:Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_8e

    .end local v9           #e:Ljava/io/IOException;
    :catch_17f
    move-exception v9

    .restart local v9       #e:Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_a0
.end method

.method protected getBaseUrl()Ljava/lang/String;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/net/micloudrichmedia/DownloadRequest;->mBaseUrl:Ljava/lang/String;

    return-object v0
.end method

.method protected getConn(Ljava/lang/String;Ljava/lang/String;)Ljava/net/HttpURLConnection;
    .registers 6
    .parameter "url"
    .parameter "cookie"

    .prologue
    invoke-super {p0, p1, p2}, Lmiui/net/micloudrichmedia/Request;->getConn(Ljava/lang/String;Ljava/lang/String;)Ljava/net/HttpURLConnection;

    move-result-object v0

    .local v0, conn:Ljava/net/HttpURLConnection;
    if-eqz v0, :cond_d

    const-string v1, "Content-Type"

    const-string v2, "application/x-www-form-urlencoded"

    invoke-virtual {v0, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    :cond_d
    return-object v0
.end method

.method protected getHttpMethod()Ljava/lang/String;
    .registers 2

    .prologue
    const-string v0, "GET"

    return-object v0
.end method

.method protected getParams(Ljava/lang/String;)Ljava/util/List;
    .registers 7
    .parameter "security"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;,
            Ljavax/crypto/IllegalBlockSizeException;,
            Ljavax/crypto/BadPaddingException;
        }
    .end annotation

    .prologue
    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .local v0, list:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "fi"

    iget-object v3, p0, Lmiui/net/micloudrichmedia/DownloadRequest;->mFi:Ljava/lang/String;

    const-string v4, "UTF-8"

    invoke-static {p1, v3, v4}, Lmiui/net/CloudCoder;->encodeString(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method public getParamsWithSignature(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .registers 11
    .parameter "security"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;,
            Ljavax/crypto/IllegalBlockSizeException;,
            Ljavax/crypto/BadPaddingException;
        }
    .end annotation

    .prologue
    invoke-virtual {p0, p1}, Lmiui/net/micloudrichmedia/DownloadRequest;->getParams(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .local v1, list:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    new-instance v3, Ljava/util/TreeMap;

    invoke-direct {v3}, Ljava/util/TreeMap;-><init>()V

    .local v3, params:Ljava/util/TreeMap;,"Ljava/util/TreeMap<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v1, :cond_27

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_f
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_27

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/http/NameValuePair;

    .local v2, pair:Lorg/apache/http/NameValuePair;
    invoke-interface {v2}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_f

    .end local v0           #i$:Ljava/util/Iterator;
    .end local v2           #pair:Lorg/apache/http/NameValuePair;
    :cond_27
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "signature"

    invoke-virtual {p0}, Lmiui/net/micloudrichmedia/DownloadRequest;->getHttpMethod()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Lmiui/net/micloudrichmedia/DownloadRequest;->getBaseUrl()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v3, p1}, Lmiui/net/CloudCoder;->generateSignature(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v1
.end method

.method public request(Ljava/lang/String;Lmiui/net/ExtendedAuthToken;Ljava/lang/String;)Lorg/json/JSONObject;
    .registers 6
    .parameter "userId"
    .parameter "authToken"
    .parameter "cookie"

    .prologue
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "The method was not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
