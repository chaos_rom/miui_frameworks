.class Lmiui/net/micloudrichmedia/FirstUploadChunkRequest;
.super Lmiui/net/micloudrichmedia/UploadRequest;
.source "FirstUploadChunkRequest.java"


# static fields
.field private static final BASE_URL:Ljava/lang/String; = "http://fileapi.micloud.xiaomi.net/mic/v1/file/user/%s/full"


# instance fields
.field private mChunkLength:I

.field private mCkeyHint:Ljava/lang/String;

.field private mIsLastChunk:Z


# direct methods
.method public constructor <init>(Lmiui/net/micloudrichmedia/UploadEntity;ZI)V
    .registers 5
    .parameter "file"
    .parameter "isLastChunk"
    .parameter "length"

    .prologue
    invoke-direct {p0, p1}, Lmiui/net/micloudrichmedia/UploadRequest;-><init>(Lmiui/net/micloudrichmedia/UploadEntity;)V

    iput-boolean p2, p0, Lmiui/net/micloudrichmedia/FirstUploadChunkRequest;->mIsLastChunk:Z

    iput p3, p0, Lmiui/net/micloudrichmedia/FirstUploadChunkRequest;->mChunkLength:I

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmiui/net/micloudrichmedia/FirstUploadChunkRequest;->mCkeyHint:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected getBaseUrl()Ljava/lang/String;
    .registers 2

    .prologue
    const-string v0, "http://fileapi.micloud.xiaomi.net/mic/v1/file/user/%s/full"

    return-object v0
.end method

.method protected getConn(Ljava/lang/String;Ljava/lang/String;)Ljava/net/HttpURLConnection;
    .registers 6
    .parameter "url"
    .parameter "cookie"

    .prologue
    invoke-super {p0, p1, p2}, Lmiui/net/micloudrichmedia/UploadRequest;->getConn(Ljava/lang/String;Ljava/lang/String;)Ljava/net/HttpURLConnection;

    move-result-object v0

    .local v0, conn:Ljava/net/HttpURLConnection;
    if-eqz v0, :cond_14

    const-string v1, "Connection"

    const-string v2, "Keep-Alive"

    invoke-virtual {v0, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "Content-Type"

    const-string v2, "multipart/form-data;boundary=*****"

    invoke-virtual {v0, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    :cond_14
    return-object v0
.end method

.method protected getParams(Ljava/lang/String;)Ljava/util/List;
    .registers 7
    .parameter "security"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;,
            Ljavax/crypto/IllegalBlockSizeException;,
            Ljavax/crypto/BadPaddingException;
        }
    .end annotation

    .prologue
    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .local v0, list:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "type"

    iget-object v3, p0, Lmiui/net/micloudrichmedia/UploadRequest;->mFile:Lmiui/net/micloudrichmedia/UploadEntity;

    invoke-virtual {v3}, Lmiui/net/micloudrichmedia/UploadEntity;->getType()Ljava/lang/String;

    move-result-object v3

    const-string v4, "UTF-8"

    invoke-static {p1, v3, v4}, Lmiui/net/CloudCoder;->encodeString(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "ckeyhint"

    iget-object v3, p0, Lmiui/net/micloudrichmedia/FirstUploadChunkRequest;->mCkeyHint:Ljava/lang/String;

    const-string v4, "ASCII"

    invoke-static {p1, v3, v4}, Lmiui/net/CloudCoder;->encodeString(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-boolean v1, p0, Lmiui/net/micloudrichmedia/FirstUploadChunkRequest;->mIsLastChunk:Z

    if-eqz v1, :cond_6f

    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "st"

    const-string v3, "2"

    const-string v4, "UTF-8"

    invoke-static {p1, v3, v4}, Lmiui/net/CloudCoder;->encodeString(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "ext"

    iget-object v3, p0, Lmiui/net/micloudrichmedia/UploadRequest;->mFile:Lmiui/net/micloudrichmedia/UploadEntity;

    invoke-virtual {v3}, Lmiui/net/micloudrichmedia/UploadEntity;->getExt()Ljava/lang/String;

    move-result-object v3

    const-string v4, "UTF-8"

    invoke-static {p1, v3, v4}, Lmiui/net/CloudCoder;->encodeString(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "digest"

    iget-object v3, p0, Lmiui/net/micloudrichmedia/UploadRequest;->mFile:Lmiui/net/micloudrichmedia/UploadEntity;

    invoke-virtual {v3}, Lmiui/net/micloudrichmedia/UploadEntity;->getHexDigest()Ljava/lang/String;

    move-result-object v3

    const-string v4, "UTF-8"

    invoke-static {p1, v3, v4}, Lmiui/net/CloudCoder;->encodeString(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_6e
    return-object v0

    :cond_6f
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "st"

    const-string v3, "1"

    const-string v4, "UTF-8"

    invoke-static {p1, v3, v4}, Lmiui/net/CloudCoder;->encodeString(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6e
.end method

.method public request(Ljava/lang/String;Lmiui/net/ExtendedAuthToken;Ljava/lang/String;)Lorg/json/JSONObject;
    .registers 22
    .parameter "userId"
    .parameter "authToken"
    .parameter "cookie"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lmiui/net/exception/MiCloudServerException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .local v4, conn:Ljava/net/HttpURLConnection;
    const/4 v5, 0x0

    .local v5, dos:Ljava/io/DataOutputStream;
    const/4 v2, 0x0

    .local v2, br:Ljava/io/BufferedReader;
    const/4 v8, 0x0

    .local v8, json:Lorg/json/JSONObject;
    :try_start_4
    move-object/from16 v0, p2

    iget-object v14, v0, Lmiui/net/ExtendedAuthToken;->security:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v14, v1}, Lmiui/net/micloudrichmedia/FirstUploadChunkRequest;->getParamsWithSignature(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v11

    .local v11, nameValuePairs:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    invoke-virtual/range {p0 .. p0}, Lmiui/net/micloudrichmedia/FirstUploadChunkRequest;->getBaseUrl()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object p1, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    const-string v15, "UTF-8"

    invoke-static {v11, v15}, Lorg/apache/http/client/utils/URLEncodedUtils;->format(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15}, Lmiui/net/micloudrichmedia/FirstUploadChunkRequest;->getTargetUrl(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v14, v1}, Lmiui/net/micloudrichmedia/FirstUploadChunkRequest;->getConn(Ljava/lang/String;Ljava/lang/String;)Ljava/net/HttpURLConnection;
    :try_end_32
    .catchall {:try_start_4 .. :try_end_32} :catchall_18b
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_32} :catch_1c3
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_32} :catch_1bb
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_4 .. :try_end_32} :catch_14d
    .catch Ljavax/crypto/BadPaddingException; {:try_start_4 .. :try_end_32} :catch_16c

    move-result-object v4

    if-nez v4, :cond_4c

    const/4 v14, 0x0

    if-eqz v5, :cond_39

    :try_start_38
    throw v5
    :try_end_39
    .catch Ljava/io/IOException; {:try_start_38 .. :try_end_39} :catch_42

    :cond_39
    :goto_39
    if-eqz v2, :cond_3c

    :try_start_3b
    throw v2
    :try_end_3c
    .catch Ljava/io/IOException; {:try_start_3b .. :try_end_3c} :catch_47

    :cond_3c
    :goto_3c
    if-eqz v4, :cond_41

    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V

    .end local v11           #nameValuePairs:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    :cond_41
    :goto_41
    return-object v14

    .restart local v11       #nameValuePairs:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    :catch_42
    move-exception v7

    .local v7, e:Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_39

    .end local v7           #e:Ljava/io/IOException;
    :catch_47
    move-exception v7

    .restart local v7       #e:Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3c

    .end local v7           #e:Ljava/io/IOException;
    :cond_4c
    :try_start_4c
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->connect()V

    new-instance v6, Ljava/io/DataOutputStream;

    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v14

    invoke-direct {v6, v14}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_58
    .catchall {:try_start_4c .. :try_end_58} :catchall_18b
    .catch Ljava/io/IOException; {:try_start_4c .. :try_end_58} :catch_1c3
    .catch Lorg/json/JSONException; {:try_start_4c .. :try_end_58} :catch_1bb
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_4c .. :try_end_58} :catch_14d
    .catch Ljavax/crypto/BadPaddingException; {:try_start_4c .. :try_end_58} :catch_16c

    .end local v5           #dos:Ljava/io/DataOutputStream;
    .local v6, dos:Ljava/io/DataOutputStream;
    :try_start_58
    const-string v14, "--*****\r\n"

    invoke-virtual {v6, v14}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    const-string v14, "Content-Disposition:form-data;name=\"uploadfile0\";filename=\"uploadfile0\"\r\n"

    invoke-virtual {v6, v14}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    const-string v14, "\r\n"

    invoke-virtual {v6, v14}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lmiui/net/micloudrichmedia/FirstUploadChunkRequest;->mCkeyHint:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, Lmiui/net/micloudrichmedia/UploadRequest;->mFile:Lmiui/net/micloudrichmedia/UploadEntity;

    move-object/from16 v0, p0

    iget v0, v0, Lmiui/net/micloudrichmedia/FirstUploadChunkRequest;->mChunkLength:I

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Lmiui/net/micloudrichmedia/UploadEntity;->getData(I)[B

    move-result-object v15

    invoke-static {v14, v15}, Lmiui/net/CloudCoder;->encodeStream(Ljava/lang/String;[B)[B

    move-result-object v14

    invoke-virtual {v6, v14}, Ljava/io/DataOutputStream;->write([B)V

    const-string v14, "\r\n--*****--\r\n"

    invoke-virtual {v6, v14}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/DataOutputStream;->flush()V

    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v12

    .local v12, responseCode:I
    const/16 v14, 0xc8

    if-ne v12, v14, :cond_ec

    new-instance v3, Ljava/io/BufferedReader;

    new-instance v14, Ljava/io/InputStreamReader;

    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v15

    invoke-direct {v14, v15}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v3, v14}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_9e
    .catchall {:try_start_58 .. :try_end_9e} :catchall_1a6
    .catch Ljava/io/IOException; {:try_start_58 .. :try_end_9e} :catch_f8
    .catch Lorg/json/JSONException; {:try_start_58 .. :try_end_9e} :catch_118
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_58 .. :try_end_9e} :catch_1b4
    .catch Ljavax/crypto/BadPaddingException; {:try_start_58 .. :try_end_9e} :catch_1ad

    .end local v2           #br:Ljava/io/BufferedReader;
    .local v3, br:Ljava/io/BufferedReader;
    :try_start_9e
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    .local v13, sb:Ljava/lang/StringBuilder;
    const/4 v10, 0x0

    .local v10, line:Ljava/lang/String;
    :goto_a4
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_c6

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_ad
    .catchall {:try_start_9e .. :try_end_ad} :catchall_1a9
    .catch Ljava/io/IOException; {:try_start_9e .. :try_end_ad} :catch_ae
    .catch Lorg/json/JSONException; {:try_start_9e .. :try_end_ad} :catch_1be
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_9e .. :try_end_ad} :catch_1b7
    .catch Ljavax/crypto/BadPaddingException; {:try_start_9e .. :try_end_ad} :catch_1b0

    goto :goto_a4

    .end local v10           #line:Ljava/lang/String;
    .end local v13           #sb:Ljava/lang/StringBuilder;
    :catch_ae
    move-exception v7

    move-object v2, v3

    .end local v3           #br:Ljava/io/BufferedReader;
    .restart local v2       #br:Ljava/io/BufferedReader;
    move-object v5, v6

    .end local v6           #dos:Ljava/io/DataOutputStream;
    .end local v11           #nameValuePairs:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    .end local v12           #responseCode:I
    .restart local v5       #dos:Ljava/io/DataOutputStream;
    .restart local v7       #e:Ljava/io/IOException;
    :goto_b1
    :try_start_b1
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V
    :try_end_b4
    .catchall {:try_start_b1 .. :try_end_b4} :catchall_18b

    if-eqz v5, :cond_b9

    :try_start_b6
    invoke-virtual {v5}, Ljava/io/DataOutputStream;->close()V
    :try_end_b9
    .catch Ljava/io/IOException; {:try_start_b6 .. :try_end_b9} :catch_137

    :cond_b9
    :goto_b9
    if-eqz v2, :cond_be

    :try_start_bb
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_be
    .catch Ljava/io/IOException; {:try_start_bb .. :try_end_be} :catch_13d

    :cond_be
    :goto_be
    if-eqz v4, :cond_c3

    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V

    .end local v7           #e:Ljava/io/IOException;
    :cond_c3
    :goto_c3
    move-object v14, v8

    goto/16 :goto_41

    .end local v2           #br:Ljava/io/BufferedReader;
    .end local v5           #dos:Ljava/io/DataOutputStream;
    .restart local v3       #br:Ljava/io/BufferedReader;
    .restart local v6       #dos:Ljava/io/DataOutputStream;
    .restart local v10       #line:Ljava/lang/String;
    .restart local v11       #nameValuePairs:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    .restart local v12       #responseCode:I
    .restart local v13       #sb:Ljava/lang/StringBuilder;
    :cond_c6
    :try_start_c6
    new-instance v9, Lorg/json/JSONObject;

    move-object/from16 v0, p2

    iget-object v14, v0, Lmiui/net/ExtendedAuthToken;->security:Ljava/lang/String;

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    const-string v16, "UTF-8"

    invoke-static/range {v14 .. v16}, Lmiui/net/CloudCoder;->decodeString(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v9, v14}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_d9
    .catchall {:try_start_c6 .. :try_end_d9} :catchall_1a9
    .catch Ljava/io/IOException; {:try_start_c6 .. :try_end_d9} :catch_ae
    .catch Lorg/json/JSONException; {:try_start_c6 .. :try_end_d9} :catch_1be
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_c6 .. :try_end_d9} :catch_1b7
    .catch Ljavax/crypto/BadPaddingException; {:try_start_c6 .. :try_end_d9} :catch_1b0

    .end local v8           #json:Lorg/json/JSONObject;
    .local v9, json:Lorg/json/JSONObject;
    move-object v8, v9

    .end local v9           #json:Lorg/json/JSONObject;
    .restart local v8       #json:Lorg/json/JSONObject;
    move-object v2, v3

    .end local v3           #br:Ljava/io/BufferedReader;
    .end local v10           #line:Ljava/lang/String;
    .end local v13           #sb:Ljava/lang/StringBuilder;
    .restart local v2       #br:Ljava/io/BufferedReader;
    :goto_db
    if-eqz v6, :cond_e0

    :try_start_dd
    invoke-virtual {v6}, Ljava/io/DataOutputStream;->close()V
    :try_end_e0
    .catch Ljava/io/IOException; {:try_start_dd .. :try_end_e0} :catch_12d

    :cond_e0
    :goto_e0
    if-eqz v2, :cond_e5

    :try_start_e2
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_e5
    .catch Ljava/io/IOException; {:try_start_e2 .. :try_end_e5} :catch_132

    :cond_e5
    :goto_e5
    if-eqz v4, :cond_1c6

    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V

    move-object v5, v6

    .end local v6           #dos:Ljava/io/DataOutputStream;
    .restart local v5       #dos:Ljava/io/DataOutputStream;
    goto :goto_c3

    .end local v5           #dos:Ljava/io/DataOutputStream;
    .restart local v6       #dos:Ljava/io/DataOutputStream;
    :cond_ec
    :try_start_ec
    invoke-static {v12}, Lmiui/net/exception/MiCloudServerException;->isMiCloudServerException(I)Z

    move-result v14

    if-eqz v14, :cond_fb

    new-instance v14, Lmiui/net/exception/MiCloudServerException;

    invoke-direct {v14, v12}, Lmiui/net/exception/MiCloudServerException;-><init>(I)V

    throw v14

    .end local v12           #responseCode:I
    :catch_f8
    move-exception v7

    move-object v5, v6

    .end local v6           #dos:Ljava/io/DataOutputStream;
    .restart local v5       #dos:Ljava/io/DataOutputStream;
    goto :goto_b1

    .end local v5           #dos:Ljava/io/DataOutputStream;
    .restart local v6       #dos:Ljava/io/DataOutputStream;
    .restart local v12       #responseCode:I
    :cond_fb
    const-string v14, "The responsed message is %s, code is %d"

    const/4 v15, 0x2

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getResponseMessage()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x1

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->log(Ljava/lang/String;)V
    :try_end_117
    .catchall {:try_start_ec .. :try_end_117} :catchall_1a6
    .catch Ljava/io/IOException; {:try_start_ec .. :try_end_117} :catch_f8
    .catch Lorg/json/JSONException; {:try_start_ec .. :try_end_117} :catch_118
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_ec .. :try_end_117} :catch_1b4
    .catch Ljavax/crypto/BadPaddingException; {:try_start_ec .. :try_end_117} :catch_1ad

    goto :goto_db

    .end local v12           #responseCode:I
    :catch_118
    move-exception v7

    move-object v5, v6

    .end local v6           #dos:Ljava/io/DataOutputStream;
    .end local v11           #nameValuePairs:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    .restart local v5       #dos:Ljava/io/DataOutputStream;
    .local v7, e:Lorg/json/JSONException;
    :goto_11a
    :try_start_11a
    invoke-virtual {v7}, Lorg/json/JSONException;->printStackTrace()V
    :try_end_11d
    .catchall {:try_start_11a .. :try_end_11d} :catchall_18b

    if-eqz v5, :cond_122

    :try_start_11f
    invoke-virtual {v5}, Ljava/io/DataOutputStream;->close()V
    :try_end_122
    .catch Ljava/io/IOException; {:try_start_11f .. :try_end_122} :catch_143

    .end local v7           #e:Lorg/json/JSONException;
    :cond_122
    :goto_122
    if-eqz v2, :cond_127

    :try_start_124
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_127
    .catch Ljava/io/IOException; {:try_start_124 .. :try_end_127} :catch_148

    :cond_127
    :goto_127
    if-eqz v4, :cond_c3

    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_c3

    .end local v5           #dos:Ljava/io/DataOutputStream;
    .restart local v6       #dos:Ljava/io/DataOutputStream;
    .restart local v11       #nameValuePairs:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    .restart local v12       #responseCode:I
    :catch_12d
    move-exception v7

    .local v7, e:Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_e0

    .end local v7           #e:Ljava/io/IOException;
    :catch_132
    move-exception v7

    .restart local v7       #e:Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_e5

    .end local v6           #dos:Ljava/io/DataOutputStream;
    .end local v11           #nameValuePairs:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    .end local v12           #responseCode:I
    .restart local v5       #dos:Ljava/io/DataOutputStream;
    :catch_137
    move-exception v7

    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_b9

    :catch_13d
    move-exception v7

    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_be

    .local v7, e:Lorg/json/JSONException;
    :catch_143
    move-exception v7

    .local v7, e:Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_122

    .end local v7           #e:Ljava/io/IOException;
    :catch_148
    move-exception v7

    .restart local v7       #e:Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_127

    .end local v7           #e:Ljava/io/IOException;
    :catch_14d
    move-exception v7

    .local v7, e:Ljavax/crypto/IllegalBlockSizeException;
    :goto_14e
    :try_start_14e
    invoke-virtual {v7}, Ljavax/crypto/IllegalBlockSizeException;->printStackTrace()V
    :try_end_151
    .catchall {:try_start_14e .. :try_end_151} :catchall_18b

    if-eqz v5, :cond_156

    :try_start_153
    invoke-virtual {v5}, Ljava/io/DataOutputStream;->close()V
    :try_end_156
    .catch Ljava/io/IOException; {:try_start_153 .. :try_end_156} :catch_162

    .end local v7           #e:Ljavax/crypto/IllegalBlockSizeException;
    :cond_156
    :goto_156
    if-eqz v2, :cond_15b

    :try_start_158
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_15b
    .catch Ljava/io/IOException; {:try_start_158 .. :try_end_15b} :catch_167

    :cond_15b
    :goto_15b
    if-eqz v4, :cond_c3

    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V

    goto/16 :goto_c3

    .restart local v7       #e:Ljavax/crypto/IllegalBlockSizeException;
    :catch_162
    move-exception v7

    .local v7, e:Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_156

    .end local v7           #e:Ljava/io/IOException;
    :catch_167
    move-exception v7

    .restart local v7       #e:Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_15b

    .end local v7           #e:Ljava/io/IOException;
    :catch_16c
    move-exception v7

    .local v7, e:Ljavax/crypto/BadPaddingException;
    :goto_16d
    :try_start_16d
    invoke-virtual {v7}, Ljavax/crypto/BadPaddingException;->printStackTrace()V
    :try_end_170
    .catchall {:try_start_16d .. :try_end_170} :catchall_18b

    if-eqz v5, :cond_175

    :try_start_172
    invoke-virtual {v5}, Ljava/io/DataOutputStream;->close()V
    :try_end_175
    .catch Ljava/io/IOException; {:try_start_172 .. :try_end_175} :catch_181

    .end local v7           #e:Ljavax/crypto/BadPaddingException;
    :cond_175
    :goto_175
    if-eqz v2, :cond_17a

    :try_start_177
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_17a
    .catch Ljava/io/IOException; {:try_start_177 .. :try_end_17a} :catch_186

    :cond_17a
    :goto_17a
    if-eqz v4, :cond_c3

    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V

    goto/16 :goto_c3

    .restart local v7       #e:Ljavax/crypto/BadPaddingException;
    :catch_181
    move-exception v7

    .local v7, e:Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_175

    .end local v7           #e:Ljava/io/IOException;
    :catch_186
    move-exception v7

    .restart local v7       #e:Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_17a

    .end local v7           #e:Ljava/io/IOException;
    :catchall_18b
    move-exception v14

    :goto_18c
    if-eqz v5, :cond_191

    :try_start_18e
    invoke-virtual {v5}, Ljava/io/DataOutputStream;->close()V
    :try_end_191
    .catch Ljava/io/IOException; {:try_start_18e .. :try_end_191} :catch_19c

    :cond_191
    :goto_191
    if-eqz v2, :cond_196

    :try_start_193
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_196
    .catch Ljava/io/IOException; {:try_start_193 .. :try_end_196} :catch_1a1

    :cond_196
    :goto_196
    if-eqz v4, :cond_19b

    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_19b
    throw v14

    :catch_19c
    move-exception v7

    .restart local v7       #e:Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_191

    .end local v7           #e:Ljava/io/IOException;
    :catch_1a1
    move-exception v7

    .restart local v7       #e:Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_196

    .end local v5           #dos:Ljava/io/DataOutputStream;
    .end local v7           #e:Ljava/io/IOException;
    .restart local v6       #dos:Ljava/io/DataOutputStream;
    .restart local v11       #nameValuePairs:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    :catchall_1a6
    move-exception v14

    move-object v5, v6

    .end local v6           #dos:Ljava/io/DataOutputStream;
    .restart local v5       #dos:Ljava/io/DataOutputStream;
    goto :goto_18c

    .end local v2           #br:Ljava/io/BufferedReader;
    .end local v5           #dos:Ljava/io/DataOutputStream;
    .restart local v3       #br:Ljava/io/BufferedReader;
    .restart local v6       #dos:Ljava/io/DataOutputStream;
    .restart local v12       #responseCode:I
    :catchall_1a9
    move-exception v14

    move-object v2, v3

    .end local v3           #br:Ljava/io/BufferedReader;
    .restart local v2       #br:Ljava/io/BufferedReader;
    move-object v5, v6

    .end local v6           #dos:Ljava/io/DataOutputStream;
    .restart local v5       #dos:Ljava/io/DataOutputStream;
    goto :goto_18c

    .end local v5           #dos:Ljava/io/DataOutputStream;
    .end local v12           #responseCode:I
    .restart local v6       #dos:Ljava/io/DataOutputStream;
    :catch_1ad
    move-exception v7

    move-object v5, v6

    .end local v6           #dos:Ljava/io/DataOutputStream;
    .restart local v5       #dos:Ljava/io/DataOutputStream;
    goto :goto_16d

    .end local v2           #br:Ljava/io/BufferedReader;
    .end local v5           #dos:Ljava/io/DataOutputStream;
    .restart local v3       #br:Ljava/io/BufferedReader;
    .restart local v6       #dos:Ljava/io/DataOutputStream;
    .restart local v12       #responseCode:I
    :catch_1b0
    move-exception v7

    move-object v2, v3

    .end local v3           #br:Ljava/io/BufferedReader;
    .restart local v2       #br:Ljava/io/BufferedReader;
    move-object v5, v6

    .end local v6           #dos:Ljava/io/DataOutputStream;
    .restart local v5       #dos:Ljava/io/DataOutputStream;
    goto :goto_16d

    .end local v5           #dos:Ljava/io/DataOutputStream;
    .end local v12           #responseCode:I
    .restart local v6       #dos:Ljava/io/DataOutputStream;
    :catch_1b4
    move-exception v7

    move-object v5, v6

    .end local v6           #dos:Ljava/io/DataOutputStream;
    .restart local v5       #dos:Ljava/io/DataOutputStream;
    goto :goto_14e

    .end local v2           #br:Ljava/io/BufferedReader;
    .end local v5           #dos:Ljava/io/DataOutputStream;
    .restart local v3       #br:Ljava/io/BufferedReader;
    .restart local v6       #dos:Ljava/io/DataOutputStream;
    .restart local v12       #responseCode:I
    :catch_1b7
    move-exception v7

    move-object v2, v3

    .end local v3           #br:Ljava/io/BufferedReader;
    .restart local v2       #br:Ljava/io/BufferedReader;
    move-object v5, v6

    .end local v6           #dos:Ljava/io/DataOutputStream;
    .restart local v5       #dos:Ljava/io/DataOutputStream;
    goto :goto_14e

    .end local v11           #nameValuePairs:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    .end local v12           #responseCode:I
    :catch_1bb
    move-exception v7

    goto/16 :goto_11a

    .end local v2           #br:Ljava/io/BufferedReader;
    .end local v5           #dos:Ljava/io/DataOutputStream;
    .restart local v3       #br:Ljava/io/BufferedReader;
    .restart local v6       #dos:Ljava/io/DataOutputStream;
    .restart local v11       #nameValuePairs:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    .restart local v12       #responseCode:I
    :catch_1be
    move-exception v7

    move-object v2, v3

    .end local v3           #br:Ljava/io/BufferedReader;
    .restart local v2       #br:Ljava/io/BufferedReader;
    move-object v5, v6

    .end local v6           #dos:Ljava/io/DataOutputStream;
    .restart local v5       #dos:Ljava/io/DataOutputStream;
    goto/16 :goto_11a

    .end local v11           #nameValuePairs:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    .end local v12           #responseCode:I
    :catch_1c3
    move-exception v7

    goto/16 :goto_b1

    .end local v5           #dos:Ljava/io/DataOutputStream;
    .restart local v6       #dos:Ljava/io/DataOutputStream;
    .restart local v11       #nameValuePairs:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    .restart local v12       #responseCode:I
    :cond_1c6
    move-object v5, v6

    .end local v6           #dos:Ljava/io/DataOutputStream;
    .restart local v5       #dos:Ljava/io/DataOutputStream;
    goto/16 :goto_c3
.end method
