.class public Lmiui/net/micloudrichmedia/UploadResult;
.super Ljava/lang/Object;
.source "UploadResult.java"


# instance fields
.field public final expireAt:J

.field public final fileId:Ljava/lang/String;

.field public final shareId:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;J)V
    .registers 5
    .parameter "fileId"
    .parameter "shareId"
    .parameter "expireAt"

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmiui/net/micloudrichmedia/UploadResult;->fileId:Ljava/lang/String;

    iput-object p2, p0, Lmiui/net/micloudrichmedia/UploadResult;->shareId:Ljava/lang/String;

    iput-wide p3, p0, Lmiui/net/micloudrichmedia/UploadResult;->expireAt:J

    return-void
.end method

.method public static fromJson(Lorg/json/JSONObject;)Lmiui/net/micloudrichmedia/UploadResult;
    .registers 9
    .parameter "jsonObj"

    .prologue
    const/4 v4, 0x0

    const-string v5, "fileId"

    invoke-virtual {p0, v5, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .local v2, fileId:Ljava/lang/String;
    const-string v5, "shareId"

    invoke-virtual {p0, v5, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .local v3, shareId:Ljava/lang/String;
    const-string v5, "expireAt"

    const-wide/16 v6, 0x0

    invoke-virtual {p0, v5, v6, v7}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v0

    .local v0, expireAt:J
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_21

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_26

    :cond_21
    new-instance v4, Lmiui/net/micloudrichmedia/UploadResult;

    invoke-direct {v4, v2, v3, v0, v1}, Lmiui/net/micloudrichmedia/UploadResult;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    :cond_26
    return-object v4
.end method
