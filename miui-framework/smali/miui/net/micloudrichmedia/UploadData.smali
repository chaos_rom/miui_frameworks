.class public Lmiui/net/micloudrichmedia/UploadData;
.super Lmiui/net/micloudrichmedia/UploadEntity;
.source "UploadData.java"


# instance fields
.field private mData:[B


# direct methods
.method public constructor <init>([BLjava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "data"
    .parameter "type"
    .parameter "ext"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    invoke-direct {p0, p2, p3}, Lmiui/net/micloudrichmedia/UploadEntity;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p1, :cond_8

    array-length v0, p1

    if-nez v0, :cond_10

    :cond_8
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The data to be uploaded should not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_10
    iput-object p1, p0, Lmiui/net/micloudrichmedia/UploadData;->mData:[B

    iget-object v0, p0, Lmiui/net/micloudrichmedia/UploadData;->mData:[B

    invoke-static {v0}, Lmiui/net/CloudCoder;->getDataSha1Digest([B)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmiui/net/micloudrichmedia/UploadEntity;->mHexDigest:Ljava/lang/String;

    iget-object v0, p0, Lmiui/net/micloudrichmedia/UploadEntity;->mHexDigest:Ljava/lang/String;

    if-nez v0, :cond_26

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Calculate file sha-1 digest error"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_26
    return-void
.end method


# virtual methods
.method getData(I)[B
    .registers 7
    .parameter "length"

    .prologue
    iget v1, p0, Lmiui/net/micloudrichmedia/UploadEntity;->mOffset:I

    invoke-virtual {p0}, Lmiui/net/micloudrichmedia/UploadData;->getLength()I

    move-result v2

    if-ge v1, v2, :cond_2e

    const/4 v0, 0x0

    .local v0, data:[B
    invoke-virtual {p0}, Lmiui/net/micloudrichmedia/UploadData;->getLength()I

    move-result v1

    iget v2, p0, Lmiui/net/micloudrichmedia/UploadEntity;->mOffset:I

    sub-int/2addr v1, v2

    if-ge p1, v1, :cond_24

    new-array v0, p1, [B

    :goto_14
    iget-object v1, p0, Lmiui/net/micloudrichmedia/UploadData;->mData:[B

    iget v2, p0, Lmiui/net/micloudrichmedia/UploadEntity;->mOffset:I

    const/4 v3, 0x0

    array-length v4, v0

    invoke-static {v1, v2, v0, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v1, p0, Lmiui/net/micloudrichmedia/UploadEntity;->mOffset:I

    array-length v2, v0

    add-int/2addr v1, v2

    iput v1, p0, Lmiui/net/micloudrichmedia/UploadEntity;->mOffset:I

    .end local v0           #data:[B
    :goto_23
    return-object v0

    .restart local v0       #data:[B
    :cond_24
    invoke-virtual {p0}, Lmiui/net/micloudrichmedia/UploadData;->getLength()I

    move-result v1

    iget v2, p0, Lmiui/net/micloudrichmedia/UploadEntity;->mOffset:I

    sub-int/2addr v1, v2

    new-array v0, v1, [B

    goto :goto_14

    .end local v0           #data:[B
    :cond_2e
    const/4 v0, 0x0

    goto :goto_23
.end method

.method getLength()I
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/net/micloudrichmedia/UploadData;->mData:[B

    array-length v0, v0

    return v0
.end method
