.class Lmiui/net/micloudrichmedia/DownloadUrlRequest;
.super Lmiui/net/micloudrichmedia/Request;
.source "DownloadUrlRequest.java"


# static fields
.field private static final BASE_URL:Ljava/lang/String; = "http://fileapi.micloud.xiaomi.net/mic/v1/file/user/%s/full"


# instance fields
.field private mFileId:Ljava/lang/String;

.field private mType:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter "fileId"
    .parameter "type"

    .prologue
    invoke-direct {p0}, Lmiui/net/micloudrichmedia/Request;-><init>()V

    iput-object p1, p0, Lmiui/net/micloudrichmedia/DownloadUrlRequest;->mFileId:Ljava/lang/String;

    iput-object p2, p0, Lmiui/net/micloudrichmedia/DownloadUrlRequest;->mType:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected getBaseUrl()Ljava/lang/String;
    .registers 2

    .prologue
    const-string v0, "http://fileapi.micloud.xiaomi.net/mic/v1/file/user/%s/full"

    return-object v0
.end method

.method protected getConn(Ljava/lang/String;Ljava/lang/String;)Ljava/net/HttpURLConnection;
    .registers 6
    .parameter "url"
    .parameter "cookie"

    .prologue
    invoke-super {p0, p1, p2}, Lmiui/net/micloudrichmedia/Request;->getConn(Ljava/lang/String;Ljava/lang/String;)Ljava/net/HttpURLConnection;

    move-result-object v0

    .local v0, conn:Ljava/net/HttpURLConnection;
    if-eqz v0, :cond_d

    const-string v1, "Content-Type"

    const-string v2, "application/x-www-form-urlencoded"

    invoke-virtual {v0, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    :cond_d
    return-object v0
.end method

.method protected getHttpMethod()Ljava/lang/String;
    .registers 2

    .prologue
    const-string v0, "GET"

    return-object v0
.end method

.method protected getParams(Ljava/lang/String;)Ljava/util/List;
    .registers 7
    .parameter "security"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;,
            Ljavax/crypto/IllegalBlockSizeException;,
            Ljavax/crypto/BadPaddingException;
        }
    .end annotation

    .prologue
    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .local v0, list:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "type"

    iget-object v3, p0, Lmiui/net/micloudrichmedia/DownloadUrlRequest;->mType:Ljava/lang/String;

    const-string v4, "UTF-8"

    invoke-static {p1, v3, v4}, Lmiui/net/CloudCoder;->encodeString(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "files"

    iget-object v3, p0, Lmiui/net/micloudrichmedia/DownloadUrlRequest;->mFileId:Ljava/lang/String;

    const-string v4, "UTF-8"

    invoke-static {p1, v3, v4}, Lmiui/net/CloudCoder;->encodeString(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method
