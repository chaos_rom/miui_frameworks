.class public Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;
.super Ljava/lang/Object;
.source "MiCloudRichMediaManager.java"


# static fields
.field private static final AUTH_TOKEN_TYPE:Ljava/lang/String; = "micfile"

.field private static final DBG:Z = true

.field private static final TAG:Ljava/lang/String; = "MiCloudRichMediaManager"


# instance fields
.field private mAccount:Landroid/accounts/Account;

.field private mAuthToken:Lmiui/net/ExtendedAuthToken;

.field private mContext:Landroid/content/Context;

.field private mMiCloudRichUtils:Lmiui/net/micloudrichmedia/Utils;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/accounts/Account;)V
    .registers 6
    .parameter "context"
    .parameter "account"

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    if-eqz p2, :cond_15

    iget-object v0, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_15

    iget-object v0, p2, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2e

    :cond_15
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The account should not be null"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2e
    iput-object p2, p0, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->mAccount:Landroid/accounts/Account;

    iput-object p1, p0, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->mContext:Landroid/content/Context;

    new-instance v0, Lmiui/net/micloudrichmedia/Utils;

    iget-object v1, p0, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->mContext:Landroid/content/Context;

    iget-object v2, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lmiui/net/micloudrichmedia/Utils;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->mMiCloudRichUtils:Lmiui/net/micloudrichmedia/Utils;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lmiui/net/ExtendedAuthToken;)V
    .registers 6
    .parameter "context"
    .parameter "userId"
    .parameter "authToken"

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->mContext:Landroid/content/Context;

    iput-object p3, p0, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->mAuthToken:Lmiui/net/ExtendedAuthToken;

    new-instance v0, Lmiui/net/micloudrichmedia/Utils;

    iget-object v1, p0, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p2}, Lmiui/net/micloudrichmedia/Utils;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->mMiCloudRichUtils:Lmiui/net/micloudrichmedia/Utils;

    iget-object v0, p0, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->mMiCloudRichUtils:Lmiui/net/micloudrichmedia/Utils;

    invoke-virtual {v0, p3}, Lmiui/net/micloudrichmedia/Utils;->updateToken(Lmiui/net/ExtendedAuthToken;)V

    return-void
.end method

.method private initAuthToken()Lmiui/net/ExtendedAuthToken;
    .registers 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    iget-object v0, p0, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->mAuthToken:Lmiui/net/ExtendedAuthToken;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->mAuthToken:Lmiui/net/ExtendedAuthToken;

    :goto_7
    return-object v0

    :cond_8
    iget-object v0, p0, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    iget-object v1, p0, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->mAccount:Landroid/accounts/Account;

    const-string v2, "micfile"

    const/4 v4, 0x1

    move-object v5, v3

    move-object v6, v3

    invoke-virtual/range {v0 .. v6}, Landroid/accounts/AccountManager;->getAuthToken(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;ZLandroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    move-result-object v9

    .local v9, future:Landroid/accounts/AccountManagerFuture;,"Landroid/accounts/AccountManagerFuture<Landroid/os/Bundle;>;"
    if-eqz v9, :cond_50

    :try_start_1b
    invoke-interface {v9}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/os/Bundle;

    .local v10, result:Landroid/os/Bundle;
    if-eqz v10, :cond_43

    invoke-interface {v9}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    const-string v1, "authtoken"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .local v8, extTokenStr:Ljava/lang/String;
    invoke-static {v8}, Lmiui/net/ExtendedAuthToken;->parse(Ljava/lang/String;)Lmiui/net/ExtendedAuthToken;

    move-result-object v0

    iput-object v0, p0, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->mAuthToken:Lmiui/net/ExtendedAuthToken;

    iget-object v0, p0, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->mAuthToken:Lmiui/net/ExtendedAuthToken;

    if-eqz v0, :cond_48

    iget-object v0, p0, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->mMiCloudRichUtils:Lmiui/net/micloudrichmedia/Utils;

    iget-object v1, p0, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->mAuthToken:Lmiui/net/ExtendedAuthToken;

    invoke-virtual {v0, v1}, Lmiui/net/micloudrichmedia/Utils;->updateToken(Lmiui/net/ExtendedAuthToken;)V

    iget-object v0, p0, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->mAuthToken:Lmiui/net/ExtendedAuthToken;

    goto :goto_7

    .end local v8           #extTokenStr:Ljava/lang/String;
    :cond_43
    const-string v0, "getAuthToken: future getResult is null"

    invoke-static {v0}, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->log(Ljava/lang/String;)V
    :try_end_48
    .catch Landroid/accounts/OperationCanceledException; {:try_start_1b .. :try_end_48} :catch_56
    .catch Landroid/accounts/AuthenticatorException; {:try_start_1b .. :try_end_48} :catch_5b
    .catch Ljava/io/IOException; {:try_start_1b .. :try_end_48} :catch_60

    .end local v10           #result:Landroid/os/Bundle;
    :cond_48
    :goto_48
    new-instance v0, Ljava/io/IOException;

    const-string v1, "failed to get auth token"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_50
    :try_start_50
    const-string v0, "getAuthToken: future is null"

    invoke-static {v0}, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->log(Ljava/lang/String;)V
    :try_end_55
    .catch Landroid/accounts/OperationCanceledException; {:try_start_50 .. :try_end_55} :catch_56
    .catch Landroid/accounts/AuthenticatorException; {:try_start_50 .. :try_end_55} :catch_5b
    .catch Ljava/io/IOException; {:try_start_50 .. :try_end_55} :catch_60

    goto :goto_48

    :catch_56
    move-exception v7

    .local v7, e:Landroid/accounts/OperationCanceledException;
    invoke-virtual {v7}, Landroid/accounts/OperationCanceledException;->printStackTrace()V

    goto :goto_48

    .end local v7           #e:Landroid/accounts/OperationCanceledException;
    :catch_5b
    move-exception v7

    .local v7, e:Landroid/accounts/AuthenticatorException;
    invoke-virtual {v7}, Landroid/accounts/AuthenticatorException;->printStackTrace()V

    goto :goto_48

    .end local v7           #e:Landroid/accounts/AuthenticatorException;
    :catch_60
    move-exception v7

    .local v7, e:Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_48
.end method

.method private invalidateAuthToken()V
    .registers 4

    .prologue
    const-string v0, "invalidateAuthToken:MiCloud rich media token expired."

    invoke-static {v0}, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->mAuthToken:Lmiui/net/ExtendedAuthToken;

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    iget-object v1, p0, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->mAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->type:Ljava/lang/String;

    iget-object v2, p0, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->mAuthToken:Lmiui/net/ExtendedAuthToken;

    invoke-virtual {v2}, Lmiui/net/ExtendedAuthToken;->toPlain()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1c
    const/4 v0, 0x0

    iput-object v0, p0, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->mAuthToken:Lmiui/net/ExtendedAuthToken;

    return-void
.end method

.method static final log(Ljava/lang/String;)V
    .registers 2
    .parameter "message"

    .prologue
    const-string v0, "MiCloudRichMediaManager"

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method public download(Ljava/lang/String;Ljava/lang/String;)[B
    .registers 7
    .parameter "fileId"
    .parameter "type"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/NetworkErrorException;,
            Lmiui/net/exception/MiCloudParameterError;,
            Ljava/io/IOException;,
            Lmiui/net/exception/MiCloudRichMediaServerException;
        }
    .end annotation

    .prologue
    invoke-direct {p0}, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->initAuthToken()Lmiui/net/ExtendedAuthToken;

    :try_start_3
    iget-object v2, p0, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->mMiCloudRichUtils:Lmiui/net/micloudrichmedia/Utils;

    invoke-virtual {v2, p1, p2}, Lmiui/net/micloudrichmedia/Utils;->download(Ljava/lang/String;Ljava/lang/String;)[B
    :try_end_8
    .catch Lmiui/net/exception/CloudServiceFailureException; {:try_start_3 .. :try_end_8} :catch_a
    .catch Lmiui/net/exception/MiCloudServerException; {:try_start_3 .. :try_end_8} :catch_23
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_8} :catch_79

    move-result-object v2

    :goto_9
    return-object v2

    :catch_a
    move-exception v0

    .local v0, e:Lmiui/net/exception/CloudServiceFailureException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "download:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->log(Ljava/lang/String;)V

    .end local v0           #e:Lmiui/net/exception/CloudServiceFailureException;
    :goto_21
    const/4 v2, 0x0

    goto :goto_9

    :catch_23
    move-exception v0

    .local v0, e:Lmiui/net/exception/MiCloudServerException;
    iget v2, v0, Lmiui/net/exception/MiCloudServerException;->statusCode:I

    const/16 v3, 0x191

    if-ne v2, v3, :cond_71

    iget-object v2, p0, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->mAccount:Landroid/accounts/Account;

    if-eqz v2, :cond_71

    invoke-direct {p0}, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->invalidateAuthToken()V

    :try_start_31
    iget-object v2, p0, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->mMiCloudRichUtils:Lmiui/net/micloudrichmedia/Utils;

    invoke-virtual {v2, p1, p2}, Lmiui/net/micloudrichmedia/Utils;->download(Ljava/lang/String;Ljava/lang/String;)[B
    :try_end_36
    .catch Lmiui/net/exception/CloudServiceFailureException; {:try_start_31 .. :try_end_36} :catch_38
    .catch Lorg/json/JSONException; {:try_start_31 .. :try_end_36} :catch_50
    .catch Lmiui/net/exception/MiCloudServerException; {:try_start_31 .. :try_end_36} :catch_68

    move-result-object v2

    goto :goto_9

    :catch_38
    move-exception v1

    .local v1, e1:Lmiui/net/exception/CloudServiceFailureException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "download:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->log(Ljava/lang/String;)V

    goto :goto_21

    .end local v1           #e1:Lmiui/net/exception/CloudServiceFailureException;
    :catch_50
    move-exception v1

    .local v1, e1:Lorg/json/JSONException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "download:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->log(Ljava/lang/String;)V

    goto :goto_21

    .end local v1           #e1:Lorg/json/JSONException;
    :catch_68
    move-exception v1

    .local v1, e1:Lmiui/net/exception/MiCloudServerException;
    new-instance v2, Lmiui/net/exception/MiCloudRichMediaServerException;

    iget v3, v1, Lmiui/net/exception/MiCloudServerException;->statusCode:I

    invoke-direct {v2, v3}, Lmiui/net/exception/MiCloudRichMediaServerException;-><init>(I)V

    throw v2

    .end local v1           #e1:Lmiui/net/exception/MiCloudServerException;
    :cond_71
    new-instance v2, Lmiui/net/exception/MiCloudRichMediaServerException;

    iget v3, v0, Lmiui/net/exception/MiCloudServerException;->statusCode:I

    invoke-direct {v2, v3}, Lmiui/net/exception/MiCloudRichMediaServerException;-><init>(I)V

    throw v2

    .end local v0           #e:Lmiui/net/exception/MiCloudServerException;
    :catch_79
    move-exception v0

    .local v0, e:Lorg/json/JSONException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "download:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->log(Ljava/lang/String;)V

    goto :goto_21
.end method

.method public getFileId(Lmiui/net/micloudrichmedia/UploadEntity;)Lmiui/net/micloudrichmedia/UploadResult;
    .registers 3
    .parameter "file"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/client/ClientProtocolException;,
            Ljava/io/IOException;,
            Lmiui/net/exception/MiCloudParameterError;,
            Lmiui/net/exception/MiCloudRichMediaServerException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->getFileId(Lmiui/net/micloudrichmedia/UploadEntity;Ljava/util/Collection;)Lmiui/net/micloudrichmedia/UploadResult;

    move-result-object v0

    return-object v0
.end method

.method public getFileId(Lmiui/net/micloudrichmedia/UploadEntity;Ljava/util/Collection;)Lmiui/net/micloudrichmedia/UploadResult;
    .registers 8
    .parameter "file"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiui/net/micloudrichmedia/UploadEntity;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lmiui/net/micloudrichmedia/UploadResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/client/ClientProtocolException;,
            Ljava/io/IOException;,
            Lmiui/net/exception/MiCloudParameterError;,
            Lmiui/net/exception/MiCloudRichMediaServerException;
        }
    .end annotation

    .prologue
    .local p2, recipients:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/lang/String;>;"
    const/4 v2, 0x0

    invoke-direct {p0}, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->initAuthToken()Lmiui/net/ExtendedAuthToken;

    :try_start_4
    iget-object v3, p0, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->mMiCloudRichUtils:Lmiui/net/micloudrichmedia/Utils;

    invoke-virtual {v3, p1, p2}, Lmiui/net/micloudrichmedia/Utils;->getFileId(Lmiui/net/micloudrichmedia/UploadEntity;Ljava/util/Collection;)Lmiui/net/micloudrichmedia/UploadResult;
    :try_end_9
    .catch Lmiui/net/exception/CloudServiceFailureException; {:try_start_4 .. :try_end_9} :catch_b
    .catch Lmiui/net/exception/MiCloudServerException; {:try_start_4 .. :try_end_9} :catch_23
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_9} :catch_79

    move-result-object v2

    :goto_a
    return-object v2

    :catch_b
    move-exception v0

    .local v0, e:Lmiui/net/exception/CloudServiceFailureException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getFileId:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->log(Ljava/lang/String;)V

    goto :goto_a

    .end local v0           #e:Lmiui/net/exception/CloudServiceFailureException;
    :catch_23
    move-exception v0

    .local v0, e:Lmiui/net/exception/MiCloudServerException;
    iget v3, v0, Lmiui/net/exception/MiCloudServerException;->statusCode:I

    const/16 v4, 0x191

    if-ne v3, v4, :cond_71

    iget-object v3, p0, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->mAccount:Landroid/accounts/Account;

    if-eqz v3, :cond_71

    invoke-direct {p0}, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->invalidateAuthToken()V

    :try_start_31
    iget-object v3, p0, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->mMiCloudRichUtils:Lmiui/net/micloudrichmedia/Utils;

    invoke-virtual {v3, p1, p2}, Lmiui/net/micloudrichmedia/Utils;->getFileId(Lmiui/net/micloudrichmedia/UploadEntity;Ljava/util/Collection;)Lmiui/net/micloudrichmedia/UploadResult;
    :try_end_36
    .catch Lmiui/net/exception/CloudServiceFailureException; {:try_start_31 .. :try_end_36} :catch_38
    .catch Lorg/json/JSONException; {:try_start_31 .. :try_end_36} :catch_50
    .catch Lmiui/net/exception/MiCloudServerException; {:try_start_31 .. :try_end_36} :catch_68

    move-result-object v2

    goto :goto_a

    :catch_38
    move-exception v1

    .local v1, e1:Lmiui/net/exception/CloudServiceFailureException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getFileId:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->log(Ljava/lang/String;)V

    goto :goto_a

    .end local v1           #e1:Lmiui/net/exception/CloudServiceFailureException;
    :catch_50
    move-exception v1

    .local v1, e1:Lorg/json/JSONException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getFileId:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->log(Ljava/lang/String;)V

    goto :goto_a

    .end local v1           #e1:Lorg/json/JSONException;
    :catch_68
    move-exception v1

    .local v1, e1:Lmiui/net/exception/MiCloudServerException;
    new-instance v2, Lmiui/net/exception/MiCloudRichMediaServerException;

    iget v3, v1, Lmiui/net/exception/MiCloudServerException;->statusCode:I

    invoke-direct {v2, v3}, Lmiui/net/exception/MiCloudRichMediaServerException;-><init>(I)V

    throw v2

    .end local v1           #e1:Lmiui/net/exception/MiCloudServerException;
    :cond_71
    new-instance v2, Lmiui/net/exception/MiCloudRichMediaServerException;

    iget v3, v0, Lmiui/net/exception/MiCloudServerException;->statusCode:I

    invoke-direct {v2, v3}, Lmiui/net/exception/MiCloudRichMediaServerException;-><init>(I)V

    throw v2

    .end local v0           #e:Lmiui/net/exception/MiCloudServerException;
    :catch_79
    move-exception v0

    .local v0, e:Lorg/json/JSONException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getFileId:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->log(Ljava/lang/String;)V

    goto/16 :goto_a
.end method

.method public updateAuthToken(Lmiui/net/ExtendedAuthToken;)V
    .registers 3
    .parameter "token"

    .prologue
    iget-object v0, p0, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->mMiCloudRichUtils:Lmiui/net/micloudrichmedia/Utils;

    invoke-virtual {v0, p1}, Lmiui/net/micloudrichmedia/Utils;->updateToken(Lmiui/net/ExtendedAuthToken;)V

    return-void
.end method

.method public upload(Lmiui/net/micloudrichmedia/UploadEntity;)Lmiui/net/micloudrichmedia/UploadResult;
    .registers 3
    .parameter "file"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lmiui/net/exception/FileTooLargeException;,
            Lmiui/net/exception/MiCloudParameterError;,
            Landroid/accounts/NetworkErrorException;,
            Ljava/io/IOException;,
            Lmiui/net/exception/MiCloudRichMediaServerException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->upload(Lmiui/net/micloudrichmedia/UploadEntity;Ljava/util/Collection;)Lmiui/net/micloudrichmedia/UploadResult;

    move-result-object v0

    return-object v0
.end method

.method public upload(Lmiui/net/micloudrichmedia/UploadEntity;Ljava/util/Collection;)Lmiui/net/micloudrichmedia/UploadResult;
    .registers 7
    .parameter "file"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiui/net/micloudrichmedia/UploadEntity;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lmiui/net/micloudrichmedia/UploadResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lmiui/net/exception/FileTooLargeException;,
            Lmiui/net/exception/MiCloudParameterError;,
            Landroid/accounts/NetworkErrorException;,
            Ljava/io/IOException;,
            Lmiui/net/exception/MiCloudRichMediaServerException;
        }
    .end annotation

    .prologue
    .local p2, recipients:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-direct {p0}, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->initAuthToken()Lmiui/net/ExtendedAuthToken;

    :try_start_3
    iget-object v2, p0, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->mMiCloudRichUtils:Lmiui/net/micloudrichmedia/Utils;

    invoke-virtual {v2, p1, p2}, Lmiui/net/micloudrichmedia/Utils;->upload(Lmiui/net/micloudrichmedia/UploadEntity;Ljava/util/Collection;)Lmiui/net/micloudrichmedia/UploadResult;
    :try_end_8
    .catch Lmiui/net/exception/CloudServiceFailureException; {:try_start_3 .. :try_end_8} :catch_a
    .catch Lmiui/net/exception/MiCloudServerException; {:try_start_3 .. :try_end_8} :catch_23

    move-result-object v2

    :goto_9
    return-object v2

    :catch_a
    move-exception v0

    .local v0, e:Lmiui/net/exception/CloudServiceFailureException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "upload:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->log(Ljava/lang/String;)V

    .end local v0           #e:Lmiui/net/exception/CloudServiceFailureException;
    :goto_21
    const/4 v2, 0x0

    goto :goto_9

    :catch_23
    move-exception v0

    .local v0, e:Lmiui/net/exception/MiCloudServerException;
    iget v2, v0, Lmiui/net/exception/MiCloudServerException;->statusCode:I

    const/16 v3, 0x191

    if-ne v2, v3, :cond_59

    iget-object v2, p0, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->mAccount:Landroid/accounts/Account;

    if-eqz v2, :cond_59

    invoke-direct {p0}, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->invalidateAuthToken()V

    :try_start_31
    iget-object v2, p0, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->mMiCloudRichUtils:Lmiui/net/micloudrichmedia/Utils;

    invoke-virtual {v2, p1, p2}, Lmiui/net/micloudrichmedia/Utils;->upload(Lmiui/net/micloudrichmedia/UploadEntity;Ljava/util/Collection;)Lmiui/net/micloudrichmedia/UploadResult;
    :try_end_36
    .catch Lmiui/net/exception/CloudServiceFailureException; {:try_start_31 .. :try_end_36} :catch_38
    .catch Lmiui/net/exception/MiCloudServerException; {:try_start_31 .. :try_end_36} :catch_50

    move-result-object v2

    goto :goto_9

    :catch_38
    move-exception v1

    .local v1, e1:Lmiui/net/exception/CloudServiceFailureException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "upload:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->log(Ljava/lang/String;)V

    goto :goto_21

    .end local v1           #e1:Lmiui/net/exception/CloudServiceFailureException;
    :catch_50
    move-exception v1

    .local v1, e1:Lmiui/net/exception/MiCloudServerException;
    new-instance v2, Lmiui/net/exception/MiCloudRichMediaServerException;

    iget v3, v1, Lmiui/net/exception/MiCloudServerException;->statusCode:I

    invoke-direct {v2, v3}, Lmiui/net/exception/MiCloudRichMediaServerException;-><init>(I)V

    throw v2

    .end local v1           #e1:Lmiui/net/exception/MiCloudServerException;
    :cond_59
    new-instance v2, Lmiui/net/exception/MiCloudRichMediaServerException;

    invoke-virtual {v0}, Lmiui/net/exception/MiCloudServerException;->getStatusCode()I

    move-result v3

    invoke-direct {v2, v3}, Lmiui/net/exception/MiCloudRichMediaServerException;-><init>(I)V

    throw v2
.end method
