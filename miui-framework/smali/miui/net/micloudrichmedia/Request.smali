.class abstract Lmiui/net/micloudrichmedia/Request;
.super Ljava/lang/Object;
.source "Request.java"


# static fields
.field public static final CHUNK_SIZE_2G:I = 0x5000

.field public static final CHUNK_SIZE_3G:I = 0xc800

.field public static final CHUNK_SIZE_WIFI:I = 0x19000

.field public static final HTTP_REQUEST_DELAY_MS:I = 0x1388

.field public static final HTTP_REQUEST_TIMEOUT_MS:I = 0x7530

.field protected static final URL:Ljava/lang/String; = "http://fileapi.micloud.xiaomi.net"


# instance fields
.field private final mExtParams:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .registers 2

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/android/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lmiui/net/micloudrichmedia/Request;->mExtParams:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public addParam(Ljava/lang/String;Ljava/lang/String;)Lmiui/net/micloudrichmedia/Request;
    .registers 4
    .parameter "key"
    .parameter "value"

    .prologue
    iget-object v0, p0, Lmiui/net/micloudrichmedia/Request;->mExtParams:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method protected abstract getBaseUrl()Ljava/lang/String;
.end method

.method protected getConn(Ljava/lang/String;Ljava/lang/String;)Ljava/net/HttpURLConnection;
    .registers 9
    .parameter "url"
    .parameter "cookie"

    .prologue
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_8

    const/4 v1, 0x0

    :goto_7
    return-object v1

    :cond_8
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "The connection url is:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->log(Ljava/lang/String;)V

    const/4 v1, 0x0

    .local v1, conn:Ljava/net/HttpURLConnection;
    :try_start_1f
    new-instance v3, Ljava/net/URL;

    invoke-direct {v3, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .local v3, req:Ljava/net/URL;
    invoke-virtual {v3}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Ljava/net/HttpURLConnection;

    move-object v1, v0

    const/16 v4, 0x7530

    invoke-virtual {v1, v4}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    const/16 v4, 0x7530

    invoke-virtual {v1, v4}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    invoke-virtual {p0}, Lmiui/net/micloudrichmedia/Request;->getHttpMethod()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    const-string v4, "Cookie"

    invoke-virtual {v1, v4, p2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "User-Agent"

    invoke-static {}, Lmiui/net/CloudManager;->getUserAgent()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4b
    .catch Ljava/net/MalformedURLException; {:try_start_1f .. :try_end_4b} :catch_4c
    .catch Ljava/io/IOException; {:try_start_1f .. :try_end_4b} :catch_51

    goto :goto_7

    .end local v3           #req:Ljava/net/URL;
    :catch_4c
    move-exception v2

    .local v2, e:Ljava/net/MalformedURLException;
    invoke-virtual {v2}, Ljava/net/MalformedURLException;->printStackTrace()V

    goto :goto_7

    .end local v2           #e:Ljava/net/MalformedURLException;
    :catch_51
    move-exception v2

    .local v2, e:Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7
.end method

.method protected abstract getHttpMethod()Ljava/lang/String;
.end method

.method protected abstract getParams(Ljava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;,
            Ljavax/crypto/IllegalBlockSizeException;,
            Ljavax/crypto/BadPaddingException;
        }
    .end annotation
.end method

.method public getParamsWithSignature(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .registers 15
    .parameter "security"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;,
            Ljavax/crypto/IllegalBlockSizeException;,
            Ljavax/crypto/BadPaddingException;
        }
    .end annotation

    .prologue
    invoke-virtual {p0, p1}, Lmiui/net/micloudrichmedia/Request;->getParams(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    .local v3, list:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    if-nez v3, :cond_b

    new-instance v3, Ljava/util/ArrayList;

    .end local v3           #list:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .restart local v3       #list:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    :cond_b
    iget-object v6, p0, Lmiui/net/micloudrichmedia/Request;->mExtParams:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_44

    iget-object v6, p0, Lmiui/net/micloudrichmedia/Request;->mExtParams:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    .local v1, extParams:Ljava/util/Set;,"Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, i$:Ljava/util/Iterator;
    :goto_1d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_44

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .local v0, extParam:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v8, Lorg/apache/http/message/BasicNameValuePair;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    const-string v9, "UTF-8"

    invoke-static {p1, v7, v9}, Lmiui/net/CloudCoder;->encodeString(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v8, v6, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1d

    .end local v0           #extParam:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v1           #extParams:Ljava/util/Set;,"Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    .end local v2           #i$:Ljava/util/Iterator;
    :cond_44
    new-instance v5, Ljava/util/TreeMap;

    invoke-direct {v5}, Ljava/util/TreeMap;-><init>()V

    .local v5, params:Ljava/util/TreeMap;,"Ljava/util/TreeMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .restart local v2       #i$:Ljava/util/Iterator;
    :goto_4d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_65

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/http/NameValuePair;

    .local v4, pair:Lorg/apache/http/NameValuePair;
    invoke-interface {v4}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4d

    .end local v4           #pair:Lorg/apache/http/NameValuePair;
    :cond_65
    new-instance v6, Lorg/apache/http/message/BasicNameValuePair;

    const-string v7, "signature"

    invoke-virtual {p0}, Lmiui/net/micloudrichmedia/Request;->getHttpMethod()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0}, Lmiui/net/micloudrichmedia/Request;->getBaseUrl()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object p2, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9, v5, p1}, Lmiui/net/CloudCoder;->generateSignature(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v3
.end method

.method protected getTargetUrl(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .parameter "url"
    .parameter "params"

    .prologue
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .end local p1
    :goto_6
    return-object p1

    .restart local p1
    :cond_7
    const-string v0, "%s?%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_6
.end method

.method public request(Ljava/lang/String;Lmiui/net/ExtendedAuthToken;Ljava/lang/String;)Lorg/json/JSONObject;
    .registers 19
    .parameter "userId"
    .parameter "authToken"
    .parameter "cookie"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lmiui/net/exception/MiCloudServerException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .local v3, conn:Ljava/net/HttpURLConnection;
    const/4 v1, 0x0

    .local v1, br:Ljava/io/BufferedReader;
    const/4 v5, 0x0

    .local v5, json:Lorg/json/JSONObject;
    :try_start_3
    move-object/from16 v0, p2

    iget-object v11, v0, Lmiui/net/ExtendedAuthToken;->security:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {p0, v11, v0}, Lmiui/net/micloudrichmedia/Request;->getParamsWithSignature(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v8

    .local v8, nameValuePairs:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    invoke-virtual {p0}, Lmiui/net/micloudrichmedia/Request;->getBaseUrl()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object p1, v12, v13

    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    const-string v12, "UTF-8"

    invoke-static {v8, v12}, Lorg/apache/http/client/utils/URLEncodedUtils;->format(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {p0, v11, v12}, Lmiui/net/micloudrichmedia/Request;->getTargetUrl(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p3

    invoke-virtual {p0, v11, v0}, Lmiui/net/micloudrichmedia/Request;->getConn(Ljava/lang/String;Ljava/lang/String;)Ljava/net/HttpURLConnection;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->connect()V

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v9

    .local v9, responseCode:I
    const/16 v11, 0xc8

    if-ne v9, v11, :cond_84

    new-instance v2, Ljava/io/BufferedReader;

    new-instance v11, Ljava/io/InputStreamReader;

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v11}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_44
    .catchall {:try_start_3 .. :try_end_44} :catchall_f4
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_44} :catch_90
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_44} :catch_ad
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_3 .. :try_end_44} :catch_cb
    .catch Ljavax/crypto/BadPaddingException; {:try_start_3 .. :try_end_44} :catch_df

    .end local v1           #br:Ljava/io/BufferedReader;
    .local v2, br:Ljava/io/BufferedReader;
    :try_start_44
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .local v10, sb:Ljava/lang/StringBuilder;
    const/4 v7, 0x0

    .local v7, line:Ljava/lang/String;
    :goto_4a
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_64

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_53
    .catchall {:try_start_44 .. :try_end_53} :catchall_105
    .catch Ljava/io/IOException; {:try_start_44 .. :try_end_53} :catch_54
    .catch Lorg/json/JSONException; {:try_start_44 .. :try_end_53} :catch_10e
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_44 .. :try_end_53} :catch_10b
    .catch Ljavax/crypto/BadPaddingException; {:try_start_44 .. :try_end_53} :catch_108

    goto :goto_4a

    .end local v7           #line:Ljava/lang/String;
    .end local v10           #sb:Ljava/lang/StringBuilder;
    :catch_54
    move-exception v4

    move-object v1, v2

    .end local v2           #br:Ljava/io/BufferedReader;
    .end local v8           #nameValuePairs:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    .end local v9           #responseCode:I
    .restart local v1       #br:Ljava/io/BufferedReader;
    .local v4, e:Ljava/io/IOException;
    :goto_56
    :try_start_56
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V
    :try_end_59
    .catchall {:try_start_56 .. :try_end_59} :catchall_f4

    if-eqz v1, :cond_5e

    :try_start_5b
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_5e
    .catch Ljava/io/IOException; {:try_start_5b .. :try_end_5e} :catch_c1

    :cond_5e
    :goto_5e
    if-eqz v3, :cond_63

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V

    .end local v4           #e:Ljava/io/IOException;
    :cond_63
    :goto_63
    return-object v5

    .end local v1           #br:Ljava/io/BufferedReader;
    .restart local v2       #br:Ljava/io/BufferedReader;
    .restart local v7       #line:Ljava/lang/String;
    .restart local v8       #nameValuePairs:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    .restart local v9       #responseCode:I
    .restart local v10       #sb:Ljava/lang/StringBuilder;
    :cond_64
    :try_start_64
    new-instance v6, Lorg/json/JSONObject;

    move-object/from16 v0, p2

    iget-object v11, v0, Lmiui/net/ExtendedAuthToken;->security:Ljava/lang/String;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    const-string v13, "UTF-8"

    invoke-static {v11, v12, v13}, Lmiui/net/CloudCoder;->decodeString(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v6, v11}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_77
    .catchall {:try_start_64 .. :try_end_77} :catchall_105
    .catch Ljava/io/IOException; {:try_start_64 .. :try_end_77} :catch_54
    .catch Lorg/json/JSONException; {:try_start_64 .. :try_end_77} :catch_10e
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_64 .. :try_end_77} :catch_10b
    .catch Ljavax/crypto/BadPaddingException; {:try_start_64 .. :try_end_77} :catch_108

    .end local v5           #json:Lorg/json/JSONObject;
    .local v6, json:Lorg/json/JSONObject;
    move-object v5, v6

    .end local v6           #json:Lorg/json/JSONObject;
    .restart local v5       #json:Lorg/json/JSONObject;
    move-object v1, v2

    .end local v2           #br:Ljava/io/BufferedReader;
    .end local v7           #line:Ljava/lang/String;
    .end local v10           #sb:Ljava/lang/StringBuilder;
    .restart local v1       #br:Ljava/io/BufferedReader;
    :goto_79
    if-eqz v1, :cond_7e

    :try_start_7b
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_7e
    .catch Ljava/io/IOException; {:try_start_7b .. :try_end_7e} :catch_bc

    :cond_7e
    :goto_7e
    if-eqz v3, :cond_63

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_63

    :cond_84
    :try_start_84
    invoke-static {v9}, Lmiui/net/exception/MiCloudServerException;->isMiCloudServerException(I)Z

    move-result v11

    if-eqz v11, :cond_92

    new-instance v11, Lmiui/net/exception/MiCloudServerException;

    invoke-direct {v11, v9}, Lmiui/net/exception/MiCloudServerException;-><init>(I)V

    throw v11

    .end local v8           #nameValuePairs:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    .end local v9           #responseCode:I
    :catch_90
    move-exception v4

    goto :goto_56

    .restart local v8       #nameValuePairs:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    .restart local v9       #responseCode:I
    :cond_92
    const-string v11, "The responsed message is %s, code is %d"

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getResponseMessage()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->log(Ljava/lang/String;)V
    :try_end_ac
    .catchall {:try_start_84 .. :try_end_ac} :catchall_f4
    .catch Ljava/io/IOException; {:try_start_84 .. :try_end_ac} :catch_90
    .catch Lorg/json/JSONException; {:try_start_84 .. :try_end_ac} :catch_ad
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_84 .. :try_end_ac} :catch_cb
    .catch Ljavax/crypto/BadPaddingException; {:try_start_84 .. :try_end_ac} :catch_df

    goto :goto_79

    .end local v8           #nameValuePairs:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    .end local v9           #responseCode:I
    :catch_ad
    move-exception v4

    .local v4, e:Lorg/json/JSONException;
    :goto_ae
    :try_start_ae
    invoke-virtual {v4}, Lorg/json/JSONException;->printStackTrace()V
    :try_end_b1
    .catchall {:try_start_ae .. :try_end_b1} :catchall_f4

    if-eqz v1, :cond_b6

    :try_start_b3
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_b6
    .catch Ljava/io/IOException; {:try_start_b3 .. :try_end_b6} :catch_c6

    .end local v4           #e:Lorg/json/JSONException;
    :cond_b6
    :goto_b6
    if-eqz v3, :cond_63

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_63

    .restart local v8       #nameValuePairs:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    .restart local v9       #responseCode:I
    :catch_bc
    move-exception v4

    .local v4, e:Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7e

    .end local v8           #nameValuePairs:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    .end local v9           #responseCode:I
    :catch_c1
    move-exception v4

    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5e

    .local v4, e:Lorg/json/JSONException;
    :catch_c6
    move-exception v4

    .local v4, e:Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_b6

    .end local v4           #e:Ljava/io/IOException;
    :catch_cb
    move-exception v4

    .local v4, e:Ljavax/crypto/IllegalBlockSizeException;
    :goto_cc
    :try_start_cc
    invoke-virtual {v4}, Ljavax/crypto/IllegalBlockSizeException;->printStackTrace()V
    :try_end_cf
    .catchall {:try_start_cc .. :try_end_cf} :catchall_f4

    if-eqz v1, :cond_d4

    :try_start_d1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_d4
    .catch Ljava/io/IOException; {:try_start_d1 .. :try_end_d4} :catch_da

    .end local v4           #e:Ljavax/crypto/IllegalBlockSizeException;
    :cond_d4
    :goto_d4
    if-eqz v3, :cond_63

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_63

    .restart local v4       #e:Ljavax/crypto/IllegalBlockSizeException;
    :catch_da
    move-exception v4

    .local v4, e:Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_d4

    .end local v4           #e:Ljava/io/IOException;
    :catch_df
    move-exception v4

    .local v4, e:Ljavax/crypto/BadPaddingException;
    :goto_e0
    :try_start_e0
    invoke-virtual {v4}, Ljavax/crypto/BadPaddingException;->printStackTrace()V
    :try_end_e3
    .catchall {:try_start_e0 .. :try_end_e3} :catchall_f4

    if-eqz v1, :cond_e8

    :try_start_e5
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_e8
    .catch Ljava/io/IOException; {:try_start_e5 .. :try_end_e8} :catch_ef

    .end local v4           #e:Ljavax/crypto/BadPaddingException;
    :cond_e8
    :goto_e8
    if-eqz v3, :cond_63

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V

    goto/16 :goto_63

    .restart local v4       #e:Ljavax/crypto/BadPaddingException;
    :catch_ef
    move-exception v4

    .local v4, e:Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_e8

    .end local v4           #e:Ljava/io/IOException;
    :catchall_f4
    move-exception v11

    :goto_f5
    if-eqz v1, :cond_fa

    :try_start_f7
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_fa
    .catch Ljava/io/IOException; {:try_start_f7 .. :try_end_fa} :catch_100

    :cond_fa
    :goto_fa
    if-eqz v3, :cond_ff

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_ff
    throw v11

    :catch_100
    move-exception v4

    .restart local v4       #e:Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_fa

    .end local v1           #br:Ljava/io/BufferedReader;
    .end local v4           #e:Ljava/io/IOException;
    .restart local v2       #br:Ljava/io/BufferedReader;
    .restart local v8       #nameValuePairs:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    .restart local v9       #responseCode:I
    :catchall_105
    move-exception v11

    move-object v1, v2

    .end local v2           #br:Ljava/io/BufferedReader;
    .restart local v1       #br:Ljava/io/BufferedReader;
    goto :goto_f5

    .end local v1           #br:Ljava/io/BufferedReader;
    .restart local v2       #br:Ljava/io/BufferedReader;
    :catch_108
    move-exception v4

    move-object v1, v2

    .end local v2           #br:Ljava/io/BufferedReader;
    .restart local v1       #br:Ljava/io/BufferedReader;
    goto :goto_e0

    .end local v1           #br:Ljava/io/BufferedReader;
    .restart local v2       #br:Ljava/io/BufferedReader;
    :catch_10b
    move-exception v4

    move-object v1, v2

    .end local v2           #br:Ljava/io/BufferedReader;
    .restart local v1       #br:Ljava/io/BufferedReader;
    goto :goto_cc

    .end local v1           #br:Ljava/io/BufferedReader;
    .restart local v2       #br:Ljava/io/BufferedReader;
    :catch_10e
    move-exception v4

    move-object v1, v2

    .end local v2           #br:Ljava/io/BufferedReader;
    .restart local v1       #br:Ljava/io/BufferedReader;
    goto :goto_ae
.end method
