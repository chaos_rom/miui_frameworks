.class Lmiui/net/micloudrichmedia/ResponseParameters;
.super Ljava/lang/Object;
.source "ResponseParameters.java"


# static fields
.field public static final CODE_FILE_TOO_LARGE:I = 0x13884

.field public static final CODE_INVALID_POSITION:I = 0x13883

.field public static final CODE_MISMATCH_DIGEST:I = 0x13881

.field public static final CODE_MISSING_TMPID:I = 0x13885

.field public static final CODE_PARAMETER_ERROR:I = 0x2718

.field public static final CODE_SUCCESS:I = 0x0

.field public static final CODE_WRITE_CONFLICT:I = 0x13882

.field public static final TAG_CODE:Ljava/lang/String; = "code"

.field public static final TAG_DATA:Ljava/lang/String; = "data"

.field public static final TAG_DATA_CKEY:Ljava/lang/String; = "ckey"

.field public static final TAG_DATA_EXPIRE_AT:Ljava/lang/String; = "expireAt"

.field public static final TAG_DATA_FILEID:Ljava/lang/String; = "fileId"

.field public static final TAG_DATA_HOSTING_SERVER:Ljava/lang/String; = "_hostingserver"

.field public static final TAG_DATA_SHA1:Ljava/lang/String; = "fileSha1"

.field public static final TAG_DATA_SHAREID:Ljava/lang/String; = "shareId"

.field public static final TAG_DATA_TEMPURL:Ljava/lang/String; = "tmpUrl"

.field public static final TAG_DATA_TMPID:Ljava/lang/String; = "tmpid"

.field public static final TAG_DES:Ljava/lang/String; = "description"

.field public static final TAG_REASON:Ljava/lang/String; = "reason"

.field public static final TAG_RESULT:Ljava/lang/String; = "result"


# instance fields
.field public mCode:I

.field public mData:Lorg/json/JSONObject;

.field public mDescription:Ljava/lang/String;

.field public mReason:Ljava/lang/String;

.field public mResult:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static parseResponse(Lorg/json/JSONObject;)Lmiui/net/micloudrichmedia/ResponseParameters;
    .registers 4
    .parameter "json"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The response json string is:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->log(Ljava/lang/String;)V

    new-instance v0, Lmiui/net/micloudrichmedia/ResponseParameters;

    invoke-direct {v0}, Lmiui/net/micloudrichmedia/ResponseParameters;-><init>()V

    .local v0, response:Lmiui/net/micloudrichmedia/ResponseParameters;
    const-string v1, "code"

    const/4 v2, -0x1

    invoke-virtual {p0, v1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, v0, Lmiui/net/micloudrichmedia/ResponseParameters;->mCode:I

    const-string v1, "result"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lmiui/net/micloudrichmedia/ResponseParameters;->mResult:Ljava/lang/String;

    const-string v1, "description"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lmiui/net/micloudrichmedia/ResponseParameters;->mDescription:Ljava/lang/String;

    const-string v1, "reason"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lmiui/net/micloudrichmedia/ResponseParameters;->mReason:Ljava/lang/String;

    iget v1, v0, Lmiui/net/micloudrichmedia/ResponseParameters;->mCode:I

    if-nez v1, :cond_4e

    const-string v1, "data"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    iput-object v1, v0, Lmiui/net/micloudrichmedia/ResponseParameters;->mData:Lorg/json/JSONObject;

    :cond_4e
    return-object v0
.end method
