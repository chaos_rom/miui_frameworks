.class public abstract Lmiui/net/micloudrichmedia/UploadEntity;
.super Ljava/lang/Object;
.source "UploadEntity.java"


# static fields
.field private static final EXT_MAX_LEN:I = 0x5


# instance fields
.field protected mExt:Ljava/lang/String;

.field protected mHexDigest:Ljava/lang/String;

.field protected mHostingServer:Ljava/lang/String;

.field protected mOffset:I

.field protected mTempId:Ljava/lang/String;

.field protected mType:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter "type"
    .parameter "ext"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x5

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_13

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1b

    :cond_13
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The upload file parameters should not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1b
    invoke-static {p1}, Lmiui/net/micloudrichmedia/MiCloudRichMediaSupportedFileType;->isSupported(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_31

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The type %s is not supported"

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_31
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v5, :cond_4e

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The ext\'s %s length should not exceeds %d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p2, v2, v3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4e
    iput-object p1, p0, Lmiui/net/micloudrichmedia/UploadEntity;->mType:Ljava/lang/String;

    iput-object p2, p0, Lmiui/net/micloudrichmedia/UploadEntity;->mExt:Ljava/lang/String;

    iput-object v1, p0, Lmiui/net/micloudrichmedia/UploadEntity;->mTempId:Ljava/lang/String;

    iput-object v1, p0, Lmiui/net/micloudrichmedia/UploadEntity;->mHostingServer:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method close()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    return-void
.end method

.method abstract getData(I)[B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public getExt()Ljava/lang/String;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/net/micloudrichmedia/UploadEntity;->mExt:Ljava/lang/String;

    return-object v0
.end method

.method public getHexDigest()Ljava/lang/String;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/net/micloudrichmedia/UploadEntity;->mHexDigest:Ljava/lang/String;

    return-object v0
.end method

.method getHostingServer()Ljava/lang/String;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/net/micloudrichmedia/UploadEntity;->mHostingServer:Ljava/lang/String;

    return-object v0
.end method

.method abstract getLength()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method getOffset()I
    .registers 2

    .prologue
    iget v0, p0, Lmiui/net/micloudrichmedia/UploadEntity;->mOffset:I

    return v0
.end method

.method getTempId()Ljava/lang/String;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/net/micloudrichmedia/UploadEntity;->mTempId:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .registers 2

    .prologue
    iget-object v0, p0, Lmiui/net/micloudrichmedia/UploadEntity;->mType:Ljava/lang/String;

    return-object v0
.end method

.method isFirstChunk()Z
    .registers 2

    .prologue
    iget v0, p0, Lmiui/net/micloudrichmedia/UploadEntity;->mOffset:I

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method isLastChunk(I)Z
    .registers 4
    .parameter "length"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    iget v0, p0, Lmiui/net/micloudrichmedia/UploadEntity;->mOffset:I

    invoke-virtual {p0}, Lmiui/net/micloudrichmedia/UploadEntity;->getLength()I

    move-result v1

    if-ge v0, v1, :cond_13

    iget v0, p0, Lmiui/net/micloudrichmedia/UploadEntity;->mOffset:I

    add-int/2addr v0, p1

    invoke-virtual {p0}, Lmiui/net/micloudrichmedia/UploadEntity;->getLength()I

    move-result v1

    if-lt v0, v1, :cond_13

    const/4 v0, 0x1

    :goto_12
    return v0

    :cond_13
    const/4 v0, 0x0

    goto :goto_12
.end method

.method open()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    return-void
.end method

.method resetOffset()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    iput v0, p0, Lmiui/net/micloudrichmedia/UploadEntity;->mOffset:I

    return-void
.end method

.method setHostingServer(Ljava/lang/String;)V
    .registers 2
    .parameter "server"

    .prologue
    iput-object p1, p0, Lmiui/net/micloudrichmedia/UploadEntity;->mHostingServer:Ljava/lang/String;

    return-void
.end method

.method setTempId(Ljava/lang/String;)V
    .registers 2
    .parameter "tempId"

    .prologue
    iput-object p1, p0, Lmiui/net/micloudrichmedia/UploadEntity;->mTempId:Ljava/lang/String;

    return-void
.end method
