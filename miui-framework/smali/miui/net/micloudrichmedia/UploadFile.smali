.class public Lmiui/net/micloudrichmedia/UploadFile;
.super Lmiui/net/micloudrichmedia/UploadEntity;
.source "UploadFile.java"


# instance fields
.field private mFile:Ljava/io/RandomAccessFile;

.field private mFilePath:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "filePath"
    .parameter "type"
    .parameter "ext"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    invoke-direct {p0, p2, p3}, Lmiui/net/micloudrichmedia/UploadEntity;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_11

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The upload file parameters should not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_11
    iput-object p1, p0, Lmiui/net/micloudrichmedia/UploadFile;->mFilePath:Ljava/lang/String;

    iget-object v0, p0, Lmiui/net/micloudrichmedia/UploadFile;->mFilePath:Ljava/lang/String;

    invoke-static {v0}, Lmiui/net/CloudCoder;->getFileSha1Digest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmiui/net/micloudrichmedia/UploadEntity;->mHexDigest:Ljava/lang/String;

    iget-object v0, p0, Lmiui/net/micloudrichmedia/UploadEntity;->mHexDigest:Ljava/lang/String;

    if-nez v0, :cond_27

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Calculate file sha-1 digest error"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_27
    return-void
.end method


# virtual methods
.method close()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    iget-object v0, p0, Lmiui/net/micloudrichmedia/UploadFile;->mFile:Ljava/io/RandomAccessFile;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lmiui/net/micloudrichmedia/UploadFile;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V

    :cond_9
    const/4 v0, 0x0

    iput-object v0, p0, Lmiui/net/micloudrichmedia/UploadFile;->mFile:Ljava/io/RandomAccessFile;

    return-void
.end method

.method getData(I)[B
    .registers 7
    .parameter "length"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    iget-object v1, p0, Lmiui/net/micloudrichmedia/UploadFile;->mFile:Ljava/io/RandomAccessFile;

    if-eqz v1, :cond_3b

    iget v1, p0, Lmiui/net/micloudrichmedia/UploadEntity;->mOffset:I

    int-to-long v1, v1

    iget-object v3, p0, Lmiui/net/micloudrichmedia/UploadFile;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-gez v1, :cond_3b

    const/4 v0, 0x0

    .local v0, data:[B
    invoke-virtual {p0}, Lmiui/net/micloudrichmedia/UploadFile;->getLength()I

    move-result v1

    iget v2, p0, Lmiui/net/micloudrichmedia/UploadEntity;->mOffset:I

    sub-int/2addr v1, v2

    if-ge p1, v1, :cond_31

    new-array v0, p1, [B

    :goto_1d
    iget-object v1, p0, Lmiui/net/micloudrichmedia/UploadFile;->mFile:Ljava/io/RandomAccessFile;

    iget v2, p0, Lmiui/net/micloudrichmedia/UploadEntity;->mOffset:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/io/RandomAccessFile;->seek(J)V

    iget-object v1, p0, Lmiui/net/micloudrichmedia/UploadFile;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v1, v0}, Ljava/io/RandomAccessFile;->read([B)I

    iget v1, p0, Lmiui/net/micloudrichmedia/UploadEntity;->mOffset:I

    array-length v2, v0

    add-int/2addr v1, v2

    iput v1, p0, Lmiui/net/micloudrichmedia/UploadEntity;->mOffset:I

    .end local v0           #data:[B
    :goto_30
    return-object v0

    .restart local v0       #data:[B
    :cond_31
    invoke-virtual {p0}, Lmiui/net/micloudrichmedia/UploadFile;->getLength()I

    move-result v1

    iget v2, p0, Lmiui/net/micloudrichmedia/UploadEntity;->mOffset:I

    sub-int/2addr v1, v2

    new-array v0, v1, [B

    goto :goto_1d

    .end local v0           #data:[B
    :cond_3b
    const/4 v0, 0x0

    goto :goto_30
.end method

.method getLength()I
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    iget-object v0, p0, Lmiui/net/micloudrichmedia/UploadFile;->mFile:Ljava/io/RandomAccessFile;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    :goto_5
    return v0

    :cond_6
    iget-object v0, p0, Lmiui/net/micloudrichmedia/UploadFile;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v0

    long-to-int v0, v0

    goto :goto_5
.end method

.method open()V
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    new-instance v0, Ljava/io/RandomAccessFile;

    iget-object v1, p0, Lmiui/net/micloudrichmedia/UploadFile;->mFilePath:Ljava/lang/String;

    const-string v2, "r"

    invoke-direct {v0, v1, v2}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lmiui/net/micloudrichmedia/UploadFile;->mFile:Ljava/io/RandomAccessFile;

    return-void
.end method
