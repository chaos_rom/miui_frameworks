.class Lmiui/net/micloudrichmedia/Utils;
.super Ljava/lang/Object;
.source "Utils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/net/micloudrichmedia/Utils$1;,
        Lmiui/net/micloudrichmedia/Utils$NetworkConnectivityChangedReceiver;,
        Lmiui/net/micloudrichmedia/Utils$DownloadFileInfo;
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCookie:Ljava/lang/String;

.field private mCurrentUploadChunk:I

.field private mExtAuthToken:Lmiui/net/ExtendedAuthToken;

.field private mNetworkConnectivityReceiver:Lmiui/net/micloudrichmedia/Utils$NetworkConnectivityChangedReceiver;

.field private mUserId:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .registers 5
    .parameter "context"
    .parameter "userId"

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_11

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The userId and authtoken should not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_11
    iput-object p2, p0, Lmiui/net/micloudrichmedia/Utils;->mUserId:Ljava/lang/String;

    iput-object p1, p0, Lmiui/net/micloudrichmedia/Utils;->mContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$202(Lmiui/net/micloudrichmedia/Utils;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    iput p1, p0, Lmiui/net/micloudrichmedia/Utils;->mCurrentUploadChunk:I

    return p1
.end method

.method static synthetic access$300(Lmiui/net/micloudrichmedia/Utils;Landroid/content/Context;)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    invoke-direct {p0, p1}, Lmiui/net/micloudrichmedia/Utils;->getChunkSize(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method private getChunkSize(Landroid/content/Context;)I
    .registers 9
    .parameter "context"

    .prologue
    const/16 v4, 0x5000

    const/4 v5, 0x0

    const-string v6, "connectivity"

    invoke-virtual {p1, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .local v0, connManager:Landroid/net/ConnectivityManager;
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v2

    .local v2, state:Landroid/net/NetworkInfo$State;
    sget-object v6, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    if-ne v6, v2, :cond_1c

    const v4, 0x19000

    :goto_1b
    :pswitch_1b
    return v4

    :cond_1c
    invoke-virtual {v0, v5}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v2

    sget-object v6, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    if-ne v6, v2, :cond_3c

    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v3

    .local v3, telephony:Landroid/telephony/TelephonyManager;
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v5

    invoke-static {v5}, Landroid/telephony/TelephonyManager;->getNetworkClass(I)I

    move-result v1

    .local v1, netWorkClass:I
    packed-switch v1, :pswitch_data_3e

    goto :goto_1b

    :pswitch_38
    const v4, 0xc800

    goto :goto_1b

    .end local v1           #netWorkClass:I
    .end local v3           #telephony:Landroid/telephony/TelephonyManager;
    :cond_3c
    move v4, v5

    goto :goto_1b

    :pswitch_data_3e
    .packed-switch 0x1
        :pswitch_1b
        :pswitch_38
        :pswitch_38
    .end packed-switch
.end method

.method static getCookies(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "userId"
    .parameter "authToken"

    .prologue
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .local v0, builder:Ljava/lang/StringBuilder;
    const-string v1, "serviceToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "; userId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private getDownloadUrl(Ljava/lang/String;Ljava/lang/String;)Lmiui/net/micloudrichmedia/Utils$DownloadFileInfo;
    .registers 16
    .parameter "fileId"
    .parameter "type"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/NetworkErrorException;,
            Lorg/json/JSONException;,
            Lmiui/net/exception/CloudServiceFailureException;,
            Lmiui/net/exception/MiCloudServerException;,
            Lmiui/net/exception/MiCloudParameterError;
        }
    .end annotation

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    const/4 v7, 0x0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "The download file id is:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->log(Ljava/lang/String;)V

    new-instance v5, Lmiui/net/micloudrichmedia/DownloadUrlRequest;

    invoke-direct {v5, p1, p2}, Lmiui/net/micloudrichmedia/DownloadUrlRequest;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .local v5, request:Lmiui/net/micloudrichmedia/DownloadUrlRequest;
    iget-object v8, p0, Lmiui/net/micloudrichmedia/Utils;->mUserId:Ljava/lang/String;

    iget-object v9, p0, Lmiui/net/micloudrichmedia/Utils;->mExtAuthToken:Lmiui/net/ExtendedAuthToken;

    iget-object v10, p0, Lmiui/net/micloudrichmedia/Utils;->mCookie:Ljava/lang/String;

    invoke-virtual {v5, v8, v9, v10}, Lmiui/net/micloudrichmedia/DownloadUrlRequest;->request(Ljava/lang/String;Lmiui/net/ExtendedAuthToken;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .local v3, json:Lorg/json/JSONObject;
    if-eqz v3, :cond_b6

    invoke-static {v3}, Lmiui/net/micloudrichmedia/ResponseParameters;->parseResponse(Lorg/json/JSONObject;)Lmiui/net/micloudrichmedia/ResponseParameters;

    move-result-object v6

    .local v6, response:Lmiui/net/micloudrichmedia/ResponseParameters;
    iget v8, v6, Lmiui/net/micloudrichmedia/ResponseParameters;->mCode:I

    if-nez v8, :cond_a8

    iget-object v8, v6, Lmiui/net/micloudrichmedia/ResponseParameters;->mData:Lorg/json/JSONObject;

    if-eqz v8, :cond_a8

    iget-object v8, v6, Lmiui/net/micloudrichmedia/ResponseParameters;->mData:Lorg/json/JSONObject;

    const-string v9, "data"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .local v1, downloadData:Lorg/json/JSONArray;
    if-eqz v1, :cond_99

    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ne v8, v12, :cond_99

    invoke-virtual {v1, v11}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    .local v0, data:Lorg/json/JSONObject;
    const-string v8, "fileId"

    invoke-virtual {v0, v8, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .local v4, rFileId:Ljava/lang/String;
    invoke-static {p1, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_86

    new-instance v2, Lmiui/net/micloudrichmedia/Utils$DownloadFileInfo;

    invoke-direct {v2, p0, v7}, Lmiui/net/micloudrichmedia/Utils$DownloadFileInfo;-><init>(Lmiui/net/micloudrichmedia/Utils;Lmiui/net/micloudrichmedia/Utils$1;)V

    .local v2, info:Lmiui/net/micloudrichmedia/Utils$DownloadFileInfo;
    const-string v8, "tmpUrl"

    invoke-virtual {v0, v8, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v2, Lmiui/net/micloudrichmedia/Utils$DownloadFileInfo;->tmpUrl:Ljava/lang/String;

    const-string v8, "ckey"

    invoke-virtual {v0, v8, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v2, Lmiui/net/micloudrichmedia/Utils$DownloadFileInfo;->ckey:Ljava/lang/String;

    iput-object v4, v2, Lmiui/net/micloudrichmedia/Utils$DownloadFileInfo;->fileId:Ljava/lang/String;

    const-string v8, "fileSha1"

    invoke-virtual {v0, v8, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v2, Lmiui/net/micloudrichmedia/Utils$DownloadFileInfo;->fileSha1:Ljava/lang/String;

    iget-object v7, v2, Lmiui/net/micloudrichmedia/Utils$DownloadFileInfo;->tmpUrl:Ljava/lang/String;

    if-eqz v7, :cond_b6

    iget-object v7, v2, Lmiui/net/micloudrichmedia/Utils$DownloadFileInfo;->ckey:Ljava/lang/String;

    if-eqz v7, :cond_b6

    iget-object v7, v2, Lmiui/net/micloudrichmedia/Utils$DownloadFileInfo;->fileId:Ljava/lang/String;

    if-eqz v7, :cond_b6

    iget-object v7, v2, Lmiui/net/micloudrichmedia/Utils$DownloadFileInfo;->fileSha1:Ljava/lang/String;

    if-eqz v7, :cond_b6

    .end local v0           #data:Lorg/json/JSONObject;
    .end local v2           #info:Lmiui/net/micloudrichmedia/Utils$DownloadFileInfo;
    .end local v4           #rFileId:Ljava/lang/String;
    :goto_85
    return-object v2

    .restart local v0       #data:Lorg/json/JSONObject;
    .restart local v4       #rFileId:Ljava/lang/String;
    :cond_86
    new-instance v7, Lmiui/net/exception/CloudServiceFailureException;

    const-string v8, "The local fileId %s is not accord with server fileId %s"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    aput-object p1, v9, v11

    aput-object v4, v9, v12

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lmiui/net/exception/CloudServiceFailureException;-><init>(Ljava/lang/String;)V

    throw v7

    .end local v0           #data:Lorg/json/JSONObject;
    .end local v4           #rFileId:Ljava/lang/String;
    :cond_99
    const-string v8, "The local fileId %s unable to obtain the download"

    new-array v9, v12, [Ljava/lang/Object;

    aput-object p1, v9, v11

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->log(Ljava/lang/String;)V

    move-object v2, v7

    goto :goto_85

    .end local v1           #downloadData:Lorg/json/JSONArray;
    :cond_a8
    iget v7, v6, Lmiui/net/micloudrichmedia/ResponseParameters;->mCode:I

    const/16 v8, 0x2718

    if-ne v7, v8, :cond_b6

    new-instance v7, Lmiui/net/exception/MiCloudParameterError;

    iget-object v8, v6, Lmiui/net/micloudrichmedia/ResponseParameters;->mDescription:Ljava/lang/String;

    invoke-direct {v7, v8}, Lmiui/net/exception/MiCloudParameterError;-><init>(Ljava/lang/String;)V

    throw v7

    .end local v6           #response:Lmiui/net/micloudrichmedia/ResponseParameters;
    :cond_b6
    new-instance v7, Lmiui/net/exception/CloudServiceFailureException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Cloud service error on getDownloadUrl for "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lmiui/net/exception/CloudServiceFailureException;-><init>(Ljava/lang/String;)V

    throw v7
.end method

.method private registerConnectivityReceiver()V
    .registers 4

    .prologue
    const-string v1, "Register network connectivity changed receiver"

    invoke-static {v1}, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->log(Ljava/lang/String;)V

    iget-object v1, p0, Lmiui/net/micloudrichmedia/Utils;->mNetworkConnectivityReceiver:Lmiui/net/micloudrichmedia/Utils$NetworkConnectivityChangedReceiver;

    if-nez v1, :cond_11

    new-instance v1, Lmiui/net/micloudrichmedia/Utils$NetworkConnectivityChangedReceiver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lmiui/net/micloudrichmedia/Utils$NetworkConnectivityChangedReceiver;-><init>(Lmiui/net/micloudrichmedia/Utils;Lmiui/net/micloudrichmedia/Utils$1;)V

    iput-object v1, p0, Lmiui/net/micloudrichmedia/Utils;->mNetworkConnectivityReceiver:Lmiui/net/micloudrichmedia/Utils$NetworkConnectivityChangedReceiver;

    :cond_11
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .local v0, filter:Landroid/content/IntentFilter;
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lmiui/net/micloudrichmedia/Utils;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lmiui/net/micloudrichmedia/Utils;->mNetworkConnectivityReceiver:Lmiui/net/micloudrichmedia/Utils$NetworkConnectivityChangedReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method private unregisterConnectivityReceiver()V
    .registers 3

    .prologue
    const-string v0, "Unregister network connectivity changed receiver"

    invoke-static {v0}, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lmiui/net/micloudrichmedia/Utils;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lmiui/net/micloudrichmedia/Utils;->mNetworkConnectivityReceiver:Lmiui/net/micloudrichmedia/Utils$NetworkConnectivityChangedReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method private uploadFile(Lmiui/net/micloudrichmedia/UploadEntity;Ljava/util/Collection;)Lmiui/net/micloudrichmedia/UploadResult;
    .registers 15
    .parameter "file"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiui/net/micloudrichmedia/UploadEntity;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lmiui/net/micloudrichmedia/UploadResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/client/ClientProtocolException;,
            Ljava/io/IOException;,
            Lorg/json/JSONException;,
            Lmiui/net/exception/InvalidWritePositionException;,
            Lmiui/net/exception/FileTooLargeException;,
            Lmiui/net/exception/CloudServiceFailureException;,
            Lmiui/net/exception/MiCloudServerException;,
            Lmiui/net/exception/MiCloudParameterError;
        }
    .end annotation

    .prologue
    .local p2, recipients:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/lang/String;>;"
    const/4 v5, 0x0

    .local v5, request:Lmiui/net/micloudrichmedia/UploadRequest;
    const/4 v7, 0x0

    .local v7, retryTimes:I
    :cond_2
    :goto_2
    invoke-virtual {p1}, Lmiui/net/micloudrichmedia/UploadEntity;->getOffset()I

    move-result v9

    invoke-virtual {p1}, Lmiui/net/micloudrichmedia/UploadEntity;->getLength()I

    move-result v10

    if-ge v9, v10, :cond_d2

    iget v0, p0, Lmiui/net/micloudrichmedia/Utils;->mCurrentUploadChunk:I

    .local v0, currentChunkSize:I
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Current chunk size is:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " the "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p1}, Lmiui/net/micloudrichmedia/UploadEntity;->getOffset()I

    move-result v10

    div-int/2addr v10, v0

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " chunk"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->log(Ljava/lang/String;)V

    invoke-virtual {p1}, Lmiui/net/micloudrichmedia/UploadEntity;->isFirstChunk()Z

    move-result v2

    .local v2, isFirstChunk:Z
    invoke-virtual {p1, v0}, Lmiui/net/micloudrichmedia/UploadEntity;->isLastChunk(I)Z

    move-result v3

    .local v3, isLastChunk:Z
    if-eqz v2, :cond_a9

    new-instance v5, Lmiui/net/micloudrichmedia/FirstUploadChunkRequest;

    .end local v5           #request:Lmiui/net/micloudrichmedia/UploadRequest;
    invoke-direct {v5, p1, v3, v0}, Lmiui/net/micloudrichmedia/FirstUploadChunkRequest;-><init>(Lmiui/net/micloudrichmedia/UploadEntity;ZI)V

    .restart local v5       #request:Lmiui/net/micloudrichmedia/UploadRequest;
    :goto_48
    if-eqz p2, :cond_5b

    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_5b

    const-string v9, ","

    invoke-static {v9, p2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v8

    .local v8, shareTo:Ljava/lang/String;
    const-string v9, "shareTo"

    invoke-virtual {v5, v9, v8}, Lmiui/net/micloudrichmedia/UploadRequest;->addParam(Ljava/lang/String;Ljava/lang/String;)Lmiui/net/micloudrichmedia/Request;

    .end local v8           #shareTo:Ljava/lang/String;
    :cond_5b
    iget-object v9, p0, Lmiui/net/micloudrichmedia/Utils;->mUserId:Ljava/lang/String;

    iget-object v10, p0, Lmiui/net/micloudrichmedia/Utils;->mExtAuthToken:Lmiui/net/ExtendedAuthToken;

    iget-object v11, p0, Lmiui/net/micloudrichmedia/Utils;->mCookie:Ljava/lang/String;

    invoke-virtual {v5, v9, v10, v11}, Lmiui/net/micloudrichmedia/FirstUploadChunkRequest;->request(Ljava/lang/String;Lmiui/net/ExtendedAuthToken;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    .local v4, jsonObj:Lorg/json/JSONObject;
    if-eqz v4, :cond_154

    invoke-static {v4}, Lmiui/net/micloudrichmedia/ResponseParameters;->parseResponse(Lorg/json/JSONObject;)Lmiui/net/micloudrichmedia/ResponseParameters;

    move-result-object v6

    .local v6, response:Lmiui/net/micloudrichmedia/ResponseParameters;
    iget v9, v6, Lmiui/net/micloudrichmedia/ResponseParameters;->mCode:I

    const v10, 0x13882

    if-eq v9, v10, :cond_73

    const/4 v7, 0x0

    :cond_73
    iget v9, v6, Lmiui/net/micloudrichmedia/ResponseParameters;->mCode:I

    if-nez v9, :cond_bc

    if-eqz v2, :cond_b3

    if-nez v3, :cond_b3

    iget-object v9, v6, Lmiui/net/micloudrichmedia/ResponseParameters;->mData:Lorg/json/JSONObject;

    const-string v10, "tmpid"

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p1, Lmiui/net/micloudrichmedia/UploadEntity;->mTempId:Ljava/lang/String;

    iget-object v9, v6, Lmiui/net/micloudrichmedia/ResponseParameters;->mData:Lorg/json/JSONObject;

    const-string v10, "_hostingserver"

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p1, Lmiui/net/micloudrichmedia/UploadEntity;->mHostingServer:Ljava/lang/String;

    iget-object v9, p1, Lmiui/net/micloudrichmedia/UploadEntity;->mTempId:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_a1

    iget-object v9, p1, Lmiui/net/micloudrichmedia/UploadEntity;->mHostingServer:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2

    :cond_a1
    new-instance v9, Ljava/lang/IllegalStateException;

    const-string v10, "Server error: The first chunk\'s response does not contain the tempid or hosting server"

    invoke-direct {v9, v10}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v9

    .end local v4           #jsonObj:Lorg/json/JSONObject;
    .end local v6           #response:Lmiui/net/micloudrichmedia/ResponseParameters;
    :cond_a9
    new-instance v5, Lmiui/net/micloudrichmedia/SimpleUploadChunkRequest;

    .end local v5           #request:Lmiui/net/micloudrichmedia/UploadRequest;
    invoke-virtual {p1}, Lmiui/net/micloudrichmedia/UploadEntity;->getOffset()I

    move-result v9

    invoke-direct {v5, p1, v3, v0, v9}, Lmiui/net/micloudrichmedia/SimpleUploadChunkRequest;-><init>(Lmiui/net/micloudrichmedia/UploadEntity;ZII)V

    .restart local v5       #request:Lmiui/net/micloudrichmedia/UploadRequest;
    goto :goto_48

    .restart local v4       #jsonObj:Lorg/json/JSONObject;
    .restart local v6       #response:Lmiui/net/micloudrichmedia/ResponseParameters;
    :cond_b3
    if-eqz v3, :cond_2

    iget-object v9, v6, Lmiui/net/micloudrichmedia/ResponseParameters;->mData:Lorg/json/JSONObject;

    invoke-static {v9}, Lmiui/net/micloudrichmedia/UploadResult;->fromJson(Lorg/json/JSONObject;)Lmiui/net/micloudrichmedia/UploadResult;

    move-result-object v9

    return-object v9

    :cond_bc
    iget v9, v6, Lmiui/net/micloudrichmedia/ResponseParameters;->mCode:I

    const v10, 0x13885

    if-ne v9, v10, :cond_c8

    invoke-virtual {p1}, Lmiui/net/micloudrichmedia/UploadEntity;->resetOffset()V

    goto/16 :goto_2

    :cond_c8
    iget v9, v6, Lmiui/net/micloudrichmedia/ResponseParameters;->mCode:I

    const v10, 0x13882

    if-ne v9, v10, :cond_e9

    const/4 v9, 0x3

    if-ne v7, v9, :cond_da

    .end local v0           #currentChunkSize:I
    .end local v2           #isFirstChunk:Z
    .end local v3           #isLastChunk:Z
    .end local v4           #jsonObj:Lorg/json/JSONObject;
    .end local v6           #response:Lmiui/net/micloudrichmedia/ResponseParameters;
    :cond_d2
    new-instance v9, Ljava/io/IOException;

    const-string v10, "Read file error"

    invoke-direct {v9, v10}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v9

    .restart local v0       #currentChunkSize:I
    .restart local v2       #isFirstChunk:Z
    .restart local v3       #isLastChunk:Z
    .restart local v4       #jsonObj:Lorg/json/JSONObject;
    .restart local v6       #response:Lmiui/net/micloudrichmedia/ResponseParameters;
    :cond_da
    const-wide/16 v9, 0x1388

    :try_start_dc
    invoke-static {v9, v10}, Ljava/lang/Thread;->sleep(J)V
    :try_end_df
    .catch Ljava/lang/InterruptedException; {:try_start_dc .. :try_end_df} :catch_e3

    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_2

    :catch_e3
    move-exception v1

    .local v1, e:Ljava/lang/InterruptedException;
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto/16 :goto_2

    .end local v1           #e:Ljava/lang/InterruptedException;
    :cond_e9
    iget v9, v6, Lmiui/net/micloudrichmedia/ResponseParameters;->mCode:I

    const v10, 0x13881

    if-ne v9, v10, :cond_f8

    new-instance v9, Ljava/io/IOException;

    const-string v10, "The file digest error"

    invoke-direct {v9, v10}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v9

    :cond_f8
    iget v9, v6, Lmiui/net/micloudrichmedia/ResponseParameters;->mCode:I

    const v10, 0x13883

    if-ne v9, v10, :cond_11c

    new-instance v9, Lmiui/net/exception/InvalidWritePositionException;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Can\'t write file at offset:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p1}, Lmiui/net/micloudrichmedia/UploadEntity;->getOffset()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Lmiui/net/exception/InvalidWritePositionException;-><init>(Ljava/lang/String;)V

    throw v9

    :cond_11c
    iget v9, v6, Lmiui/net/micloudrichmedia/ResponseParameters;->mCode:I

    const v10, 0x13884

    if-ne v9, v10, :cond_146

    new-instance v9, Lmiui/net/exception/FileTooLargeException;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "The file size:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p1}, Lmiui/net/micloudrichmedia/UploadEntity;->getLength()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " exceeds the limit"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Lmiui/net/exception/FileTooLargeException;-><init>(Ljava/lang/String;)V

    throw v9

    :cond_146
    iget v9, v6, Lmiui/net/micloudrichmedia/ResponseParameters;->mCode:I

    const/16 v10, 0x2718

    if-ne v9, v10, :cond_2

    new-instance v9, Lmiui/net/exception/MiCloudParameterError;

    iget-object v10, v6, Lmiui/net/micloudrichmedia/ResponseParameters;->mDescription:Ljava/lang/String;

    invoke-direct {v9, v10}, Lmiui/net/exception/MiCloudParameterError;-><init>(Ljava/lang/String;)V

    throw v9

    .end local v6           #response:Lmiui/net/micloudrichmedia/ResponseParameters;
    :cond_154
    new-instance v9, Lmiui/net/exception/CloudServiceFailureException;

    const-string v10, "Cloud service fails when upload file"

    invoke-direct {v9, v10}, Lmiui/net/exception/CloudServiceFailureException;-><init>(Ljava/lang/String;)V

    throw v9
.end method


# virtual methods
.method download(Ljava/lang/String;Ljava/lang/String;)[B
    .registers 12
    .parameter "fileId"
    .parameter "type"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/NetworkErrorException;,
            Ljava/io/IOException;,
            Lmiui/net/exception/CloudServiceFailureException;,
            Lmiui/net/exception/MiCloudServerException;,
            Lmiui/net/exception/MiCloudParameterError;,
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_c

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_14

    :cond_c
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "The download parameters should not be null"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_14
    invoke-static {p2}, Lmiui/net/micloudrichmedia/MiCloudRichMediaSupportedFileType;->isSupported(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2c

    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "The type %s is not supported"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object p2, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_2c
    iget-object v5, p0, Lmiui/net/micloudrichmedia/Utils;->mContext:Landroid/content/Context;

    invoke-direct {p0, v5}, Lmiui/net/micloudrichmedia/Utils;->getChunkSize(Landroid/content/Context;)I

    move-result v5

    iput v5, p0, Lmiui/net/micloudrichmedia/Utils;->mCurrentUploadChunk:I

    iget v5, p0, Lmiui/net/micloudrichmedia/Utils;->mCurrentUploadChunk:I

    if-nez v5, :cond_40

    new-instance v5, Landroid/accounts/NetworkErrorException;

    const-string v6, "Network is not connected"

    invoke-direct {v5, v6}, Landroid/accounts/NetworkErrorException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_40
    :try_start_40
    invoke-direct {p0, p1, p2}, Lmiui/net/micloudrichmedia/Utils;->getDownloadUrl(Ljava/lang/String;Ljava/lang/String;)Lmiui/net/micloudrichmedia/Utils$DownloadFileInfo;

    move-result-object v2

    .local v2, info:Lmiui/net/micloudrichmedia/Utils$DownloadFileInfo;
    if-nez v2, :cond_48

    const/4 v5, 0x0

    :goto_47
    return-object v5

    :cond_48
    iget-object v5, v2, Lmiui/net/micloudrichmedia/Utils$DownloadFileInfo;->tmpUrl:Ljava/lang/String;

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .local v4, uri:Landroid/net/Uri;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .local v0, baseUrl:Ljava/lang/StringBuilder;
    invoke-virtual {v4}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "://"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v3, Lmiui/net/micloudrichmedia/DownloadRequest;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, v2, Lmiui/net/micloudrichmedia/Utils$DownloadFileInfo;->ckey:Ljava/lang/String;

    const-string v7, "fi"

    invoke-virtual {v4, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iget-object v8, v2, Lmiui/net/micloudrichmedia/Utils$DownloadFileInfo;->fileSha1:Ljava/lang/String;

    invoke-direct {v3, v5, v6, v7, v8}, Lmiui/net/micloudrichmedia/DownloadRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .local v3, request:Lmiui/net/micloudrichmedia/DownloadRequest;
    iget-object v5, p0, Lmiui/net/micloudrichmedia/Utils;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lmiui/net/micloudrichmedia/Utils;->mUserId:Ljava/lang/String;

    iget-object v7, p0, Lmiui/net/micloudrichmedia/Utils;->mExtAuthToken:Lmiui/net/ExtendedAuthToken;

    iget-object v8, p0, Lmiui/net/micloudrichmedia/Utils;->mCookie:Ljava/lang/String;

    invoke-virtual {v3, v5, v6, v7, v8}, Lmiui/net/micloudrichmedia/DownloadRequest;->download(Landroid/content/Context;Ljava/lang/String;Lmiui/net/ExtendedAuthToken;Ljava/lang/String;)[B
    :try_end_8c
    .catch Lorg/apache/commons/codec/DecoderException; {:try_start_40 .. :try_end_8c} :catch_8e
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_40 .. :try_end_8c} :catch_95
    .catch Ljavax/crypto/BadPaddingException; {:try_start_40 .. :try_end_8c} :catch_9c

    move-result-object v5

    goto :goto_47

    .end local v0           #baseUrl:Ljava/lang/StringBuilder;
    .end local v2           #info:Lmiui/net/micloudrichmedia/Utils$DownloadFileInfo;
    .end local v3           #request:Lmiui/net/micloudrichmedia/DownloadRequest;
    .end local v4           #uri:Landroid/net/Uri;
    :catch_8e
    move-exception v1

    .local v1, e:Lorg/apache/commons/codec/DecoderException;
    new-instance v5, Ljava/io/IOException;

    invoke-direct {v5, v1}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v5

    .end local v1           #e:Lorg/apache/commons/codec/DecoderException;
    :catch_95
    move-exception v1

    .local v1, e:Ljavax/crypto/IllegalBlockSizeException;
    new-instance v5, Ljava/io/IOException;

    invoke-direct {v5, v1}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v5

    .end local v1           #e:Ljavax/crypto/IllegalBlockSizeException;
    :catch_9c
    move-exception v1

    .local v1, e:Ljavax/crypto/BadPaddingException;
    new-instance v5, Ljava/io/IOException;

    invoke-direct {v5, v1}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v5
.end method

.method getFileId(Lmiui/net/micloudrichmedia/UploadEntity;Ljava/util/Collection;)Lmiui/net/micloudrichmedia/UploadResult;
    .registers 10
    .parameter "file"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiui/net/micloudrichmedia/UploadEntity;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lmiui/net/micloudrichmedia/UploadResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/client/ClientProtocolException;,
            Ljava/io/IOException;,
            Lorg/json/JSONException;,
            Lmiui/net/exception/CloudServiceFailureException;,
            Lmiui/net/exception/MiCloudServerException;,
            Lmiui/net/exception/MiCloudParameterError;
        }
    .end annotation

    .prologue
    .local p2, recipients:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/lang/String;>;"
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Current chunk size is:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lmiui/net/micloudrichmedia/Utils;->mCurrentUploadChunk:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->log(Ljava/lang/String;)V

    new-instance v1, Lmiui/net/micloudrichmedia/CheckRequest;

    invoke-direct {v1, p1}, Lmiui/net/micloudrichmedia/CheckRequest;-><init>(Lmiui/net/micloudrichmedia/UploadEntity;)V

    .local v1, request:Lmiui/net/micloudrichmedia/CheckRequest;
    if-eqz p2, :cond_30

    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_30

    const-string v4, ","

    invoke-static {v4, p2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v3

    .local v3, shareTo:Ljava/lang/String;
    const-string v4, "shareTo"

    invoke-virtual {v1, v4, v3}, Lmiui/net/micloudrichmedia/CheckRequest;->addParam(Ljava/lang/String;Ljava/lang/String;)Lmiui/net/micloudrichmedia/Request;

    .end local v3           #shareTo:Ljava/lang/String;
    :cond_30
    iget-object v4, p0, Lmiui/net/micloudrichmedia/Utils;->mUserId:Ljava/lang/String;

    iget-object v5, p0, Lmiui/net/micloudrichmedia/Utils;->mExtAuthToken:Lmiui/net/ExtendedAuthToken;

    iget-object v6, p0, Lmiui/net/micloudrichmedia/Utils;->mCookie:Ljava/lang/String;

    invoke-virtual {v1, v4, v5, v6}, Lmiui/net/micloudrichmedia/CheckRequest;->request(Ljava/lang/String;Lmiui/net/ExtendedAuthToken;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .local v0, json:Lorg/json/JSONObject;
    if-eqz v0, :cond_59

    invoke-static {v0}, Lmiui/net/micloudrichmedia/ResponseParameters;->parseResponse(Lorg/json/JSONObject;)Lmiui/net/micloudrichmedia/ResponseParameters;

    move-result-object v2

    .local v2, response:Lmiui/net/micloudrichmedia/ResponseParameters;
    iget v4, v2, Lmiui/net/micloudrichmedia/ResponseParameters;->mCode:I

    if-nez v4, :cond_4b

    iget-object v4, v2, Lmiui/net/micloudrichmedia/ResponseParameters;->mData:Lorg/json/JSONObject;

    invoke-static {v4}, Lmiui/net/micloudrichmedia/UploadResult;->fromJson(Lorg/json/JSONObject;)Lmiui/net/micloudrichmedia/UploadResult;

    move-result-object v4

    return-object v4

    :cond_4b
    iget v4, v2, Lmiui/net/micloudrichmedia/ResponseParameters;->mCode:I

    const/16 v5, 0x2718

    if-ne v4, v5, :cond_59

    new-instance v4, Lmiui/net/exception/MiCloudParameterError;

    iget-object v5, v2, Lmiui/net/micloudrichmedia/ResponseParameters;->mDescription:Ljava/lang/String;

    invoke-direct {v4, v5}, Lmiui/net/exception/MiCloudParameterError;-><init>(Ljava/lang/String;)V

    throw v4

    .end local v2           #response:Lmiui/net/micloudrichmedia/ResponseParameters;
    :cond_59
    new-instance v4, Lmiui/net/exception/CloudServiceFailureException;

    const-string v5, "Cloud service error in check file exits"

    invoke-direct {v4, v5}, Lmiui/net/exception/CloudServiceFailureException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method updateToken(Lmiui/net/ExtendedAuthToken;)V
    .registers 4
    .parameter "token"

    .prologue
    if-nez p1, :cond_a

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The authtoken should not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_a
    iget-object v0, p0, Lmiui/net/micloudrichmedia/Utils;->mUserId:Ljava/lang/String;

    iget-object v1, p1, Lmiui/net/ExtendedAuthToken;->authToken:Ljava/lang/String;

    invoke-static {v0, v1}, Lmiui/net/micloudrichmedia/Utils;->getCookies(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmiui/net/micloudrichmedia/Utils;->mCookie:Ljava/lang/String;

    iput-object p1, p0, Lmiui/net/micloudrichmedia/Utils;->mExtAuthToken:Lmiui/net/ExtendedAuthToken;

    return-void
.end method

.method upload(Lmiui/net/micloudrichmedia/UploadEntity;Ljava/util/Collection;)Lmiui/net/micloudrichmedia/UploadResult;
    .registers 7
    .parameter "file"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiui/net/micloudrichmedia/UploadEntity;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lmiui/net/micloudrichmedia/UploadResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lmiui/net/exception/FileTooLargeException;,
            Landroid/accounts/NetworkErrorException;,
            Lmiui/net/exception/CloudServiceFailureException;,
            Lmiui/net/exception/MiCloudServerException;,
            Lmiui/net/exception/MiCloudParameterError;
        }
    .end annotation

    .prologue
    .local p2, recipients:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/lang/String;>;"
    if-nez p1, :cond_9

    const-string v2, "The file should not be null"

    invoke-static {v2}, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->log(Ljava/lang/String;)V

    const/4 v1, 0x0

    :goto_8
    return-object v1

    :cond_9
    iget-object v2, p0, Lmiui/net/micloudrichmedia/Utils;->mContext:Landroid/content/Context;

    invoke-direct {p0, v2}, Lmiui/net/micloudrichmedia/Utils;->getChunkSize(Landroid/content/Context;)I

    move-result v2

    iput v2, p0, Lmiui/net/micloudrichmedia/Utils;->mCurrentUploadChunk:I

    iget v2, p0, Lmiui/net/micloudrichmedia/Utils;->mCurrentUploadChunk:I

    if-nez v2, :cond_1d

    new-instance v2, Landroid/accounts/NetworkErrorException;

    const-string v3, "Network is not connected"

    invoke-direct {v2, v3}, Landroid/accounts/NetworkErrorException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1d
    invoke-direct {p0}, Lmiui/net/micloudrichmedia/Utils;->registerConnectivityReceiver()V

    :try_start_20
    invoke-virtual {p1}, Lmiui/net/micloudrichmedia/UploadEntity;->open()V

    invoke-virtual {p0, p1, p2}, Lmiui/net/micloudrichmedia/Utils;->getFileId(Lmiui/net/micloudrichmedia/UploadEntity;Ljava/util/Collection;)Lmiui/net/micloudrichmedia/UploadResult;

    move-result-object v1

    .local v1, result:Lmiui/net/micloudrichmedia/UploadResult;
    if-eqz v1, :cond_4e

    iget-object v2, v1, Lmiui/net/micloudrichmedia/UploadResult;->fileId:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4e

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "The file already exist:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->log(Ljava/lang/String;)V
    :try_end_47
    .catchall {:try_start_20 .. :try_end_47} :catchall_95
    .catch Lorg/json/JSONException; {:try_start_20 .. :try_end_47} :catch_7f
    .catch Lmiui/net/exception/InvalidWritePositionException; {:try_start_20 .. :try_end_47} :catch_8a

    invoke-virtual {p1}, Lmiui/net/micloudrichmedia/UploadEntity;->close()V

    invoke-direct {p0}, Lmiui/net/micloudrichmedia/Utils;->unregisterConnectivityReceiver()V

    goto :goto_8

    :cond_4e
    :try_start_4e
    invoke-direct {p0, p1, p2}, Lmiui/net/micloudrichmedia/Utils;->uploadFile(Lmiui/net/micloudrichmedia/UploadEntity;Ljava/util/Collection;)Lmiui/net/micloudrichmedia/UploadResult;

    move-result-object v1

    if-eqz v1, :cond_71

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "The file upload success:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lmiui/net/micloudrichmedia/MiCloudRichMediaManager;->log(Ljava/lang/String;)V
    :try_end_6a
    .catchall {:try_start_4e .. :try_end_6a} :catchall_95
    .catch Lorg/json/JSONException; {:try_start_4e .. :try_end_6a} :catch_7f
    .catch Lmiui/net/exception/InvalidWritePositionException; {:try_start_4e .. :try_end_6a} :catch_8a

    invoke-virtual {p1}, Lmiui/net/micloudrichmedia/UploadEntity;->close()V

    invoke-direct {p0}, Lmiui/net/micloudrichmedia/Utils;->unregisterConnectivityReceiver()V

    goto :goto_8

    :cond_71
    invoke-virtual {p1}, Lmiui/net/micloudrichmedia/UploadEntity;->close()V

    invoke-direct {p0}, Lmiui/net/micloudrichmedia/Utils;->unregisterConnectivityReceiver()V

    .end local v1           #result:Lmiui/net/micloudrichmedia/UploadResult;
    :goto_77
    new-instance v2, Lmiui/net/exception/CloudServiceFailureException;

    const-string v3, "Cloud server fails when upload files"

    invoke-direct {v2, v3}, Lmiui/net/exception/CloudServiceFailureException;-><init>(Ljava/lang/String;)V

    throw v2

    :catch_7f
    move-exception v0

    .local v0, e:Lorg/json/JSONException;
    :try_start_80
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V
    :try_end_83
    .catchall {:try_start_80 .. :try_end_83} :catchall_95

    invoke-virtual {p1}, Lmiui/net/micloudrichmedia/UploadEntity;->close()V

    invoke-direct {p0}, Lmiui/net/micloudrichmedia/Utils;->unregisterConnectivityReceiver()V

    goto :goto_77

    .end local v0           #e:Lorg/json/JSONException;
    :catch_8a
    move-exception v0

    .local v0, e:Lmiui/net/exception/InvalidWritePositionException;
    :try_start_8b
    invoke-virtual {v0}, Lmiui/net/exception/InvalidWritePositionException;->printStackTrace()V
    :try_end_8e
    .catchall {:try_start_8b .. :try_end_8e} :catchall_95

    invoke-virtual {p1}, Lmiui/net/micloudrichmedia/UploadEntity;->close()V

    invoke-direct {p0}, Lmiui/net/micloudrichmedia/Utils;->unregisterConnectivityReceiver()V

    goto :goto_77

    .end local v0           #e:Lmiui/net/exception/InvalidWritePositionException;
    :catchall_95
    move-exception v2

    invoke-virtual {p1}, Lmiui/net/micloudrichmedia/UploadEntity;->close()V

    invoke-direct {p0}, Lmiui/net/micloudrichmedia/Utils;->unregisterConnectivityReceiver()V

    throw v2
.end method
