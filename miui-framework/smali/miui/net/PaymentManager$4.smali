.class Lmiui/net/PaymentManager$4;
.super Lmiui/net/PaymentManager$PmsTask;
.source "PaymentManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiui/net/PaymentManager;->internalPay(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lmiui/net/PaymentManager$PaymentManagerCallback;Landroid/os/Handler;)Lmiui/net/PaymentManager$PaymentManagerFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/net/PaymentManager;

.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$extra:Landroid/os/Bundle;

.field final synthetic val$productId:Ljava/lang/String;

.field final synthetic val$serviceId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lmiui/net/PaymentManager;Landroid/os/Handler;Lmiui/net/PaymentManager$PaymentManagerCallback;Landroid/os/Bundle;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter
    .parameter "x0"
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .local p3, x1:Lmiui/net/PaymentManager$PaymentManagerCallback;,"Lmiui/net/PaymentManager$PaymentManagerCallback<Landroid/os/Bundle;>;"
    iput-object p1, p0, Lmiui/net/PaymentManager$4;->this$0:Lmiui/net/PaymentManager;

    iput-object p4, p0, Lmiui/net/PaymentManager$4;->val$extra:Landroid/os/Bundle;

    iput-object p5, p0, Lmiui/net/PaymentManager$4;->val$account:Landroid/accounts/Account;

    iput-object p6, p0, Lmiui/net/PaymentManager$4;->val$serviceId:Ljava/lang/String;

    iput-object p7, p0, Lmiui/net/PaymentManager$4;->val$productId:Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3}, Lmiui/net/PaymentManager$PmsTask;-><init>(Lmiui/net/PaymentManager;Landroid/os/Handler;Lmiui/net/PaymentManager$PaymentManagerCallback;)V

    return-void
.end method


# virtual methods
.method protected doWork()V
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    invoke-virtual {p0}, Lmiui/net/PaymentManager$4;->getService()Lmiui/net/IPaymentManagerService;

    move-result-object v0

    .local v0, service:Lmiui/net/IPaymentManagerService;
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .local v5, finalExtra:Landroid/os/Bundle;
    iget-object v1, p0, Lmiui/net/PaymentManager$4;->val$extra:Landroid/os/Bundle;

    if-eqz v1, :cond_12

    iget-object v1, p0, Lmiui/net/PaymentManager$4;->val$extra:Landroid/os/Bundle;

    invoke-virtual {v5, v1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    :cond_12
    invoke-virtual {p0}, Lmiui/net/PaymentManager$4;->getResponse()Lmiui/net/IPaymentManagerResponse;

    move-result-object v1

    iget-object v2, p0, Lmiui/net/PaymentManager$4;->val$account:Landroid/accounts/Account;

    iget-object v3, p0, Lmiui/net/PaymentManager$4;->val$serviceId:Ljava/lang/String;

    iget-object v4, p0, Lmiui/net/PaymentManager$4;->val$productId:Ljava/lang/String;

    invoke-interface/range {v0 .. v5}, Lmiui/net/IPaymentManagerService;->pay(Lmiui/net/IPaymentManagerResponse;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method
