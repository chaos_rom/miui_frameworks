.class Lmiui/net/PaymentManager$PaymentCallback;
.super Ljava/lang/Object;
.source "PaymentManager.java"

# interfaces
.implements Lmiui/net/PaymentManager$PaymentManagerCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/net/PaymentManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PaymentCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lmiui/net/PaymentManager$PaymentManagerCallback",
        "<",
        "Landroid/os/Bundle;",
        ">;"
    }
.end annotation


# instance fields
.field private mPaymentId:Ljava/lang/String;

.field private mPaymentListener:Lmiui/net/PaymentManager$PaymentListener;

.field private mServiceId:Ljava/lang/String;

.field final synthetic this$0:Lmiui/net/PaymentManager;


# direct methods
.method public constructor <init>(Lmiui/net/PaymentManager;Ljava/lang/String;Ljava/lang/String;Lmiui/net/PaymentManager$PaymentListener;)V
    .registers 5
    .parameter
    .parameter "serviceId"
    .parameter "paymentId"
    .parameter "paymentListener"

    .prologue
    iput-object p1, p0, Lmiui/net/PaymentManager$PaymentCallback;->this$0:Lmiui/net/PaymentManager;

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lmiui/net/PaymentManager$PaymentCallback;->mServiceId:Ljava/lang/String;

    iput-object p3, p0, Lmiui/net/PaymentManager$PaymentCallback;->mPaymentId:Ljava/lang/String;

    iput-object p4, p0, Lmiui/net/PaymentManager$PaymentCallback;->mPaymentListener:Lmiui/net/PaymentManager$PaymentListener;

    return-void
.end method


# virtual methods
.method public run(Lmiui/net/PaymentManager$PaymentManagerFuture;)V
    .registers 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiui/net/PaymentManager$PaymentManagerFuture",
            "<",
            "Landroid/os/Bundle;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, future:Lmiui/net/PaymentManager$PaymentManagerFuture;,"Lmiui/net/PaymentManager$PaymentManagerFuture<Landroid/os/Bundle;>;"
    iget-object v2, p0, Lmiui/net/PaymentManager$PaymentCallback;->mPaymentListener:Lmiui/net/PaymentManager$PaymentListener;

    if-nez v2, :cond_5

    :goto_4
    return-void

    :cond_5
    :try_start_5
    invoke-interface {p1}, Lmiui/net/PaymentManager$PaymentManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Bundle;

    .local v1, result:Landroid/os/Bundle;
    if-eqz v1, :cond_23

    iget-object v2, p0, Lmiui/net/PaymentManager$PaymentCallback;->mPaymentListener:Lmiui/net/PaymentManager$PaymentListener;

    iget-object v3, p0, Lmiui/net/PaymentManager$PaymentCallback;->mPaymentId:Ljava/lang/String;

    invoke-interface {v2, v3, v1}, Lmiui/net/PaymentManager$PaymentListener;->onSuccess(Ljava/lang/String;Landroid/os/Bundle;)V
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_14} :catch_15
    .catch Lmiui/net/exception/OperationCancelledException; {:try_start_5 .. :try_end_14} :catch_2e
    .catch Lmiui/net/exception/AuthenticationFailureException; {:try_start_5 .. :try_end_14} :catch_3c
    .catch Lmiui/net/exception/PaymentServiceFailureException; {:try_start_5 .. :try_end_14} :catch_4a

    goto :goto_4

    .end local v1           #result:Landroid/os/Bundle;
    :catch_15
    move-exception v0

    .local v0, e:Ljava/io/IOException;
    iget-object v2, p0, Lmiui/net/PaymentManager$PaymentCallback;->mPaymentListener:Lmiui/net/PaymentManager$PaymentListener;

    iget-object v3, p0, Lmiui/net/PaymentManager$PaymentCallback;->mPaymentId:Ljava/lang/String;

    const/4 v4, 0x3

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v3, v4, v5}, Lmiui/net/PaymentManager$PaymentListener;->onFailed(Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_4

    .end local v0           #e:Ljava/io/IOException;
    .restart local v1       #result:Landroid/os/Bundle;
    :cond_23
    :try_start_23
    iget-object v2, p0, Lmiui/net/PaymentManager$PaymentCallback;->mPaymentListener:Lmiui/net/PaymentManager$PaymentListener;

    iget-object v3, p0, Lmiui/net/PaymentManager$PaymentCallback;->mPaymentId:Ljava/lang/String;

    const/4 v4, 0x1

    const-string v5, "pay failed"

    invoke-interface {v2, v3, v4, v5}, Lmiui/net/PaymentManager$PaymentListener;->onFailed(Ljava/lang/String;ILjava/lang/String;)V
    :try_end_2d
    .catch Ljava/io/IOException; {:try_start_23 .. :try_end_2d} :catch_15
    .catch Lmiui/net/exception/OperationCancelledException; {:try_start_23 .. :try_end_2d} :catch_2e
    .catch Lmiui/net/exception/AuthenticationFailureException; {:try_start_23 .. :try_end_2d} :catch_3c
    .catch Lmiui/net/exception/PaymentServiceFailureException; {:try_start_23 .. :try_end_2d} :catch_4a

    goto :goto_4

    .end local v1           #result:Landroid/os/Bundle;
    :catch_2e
    move-exception v0

    .local v0, e:Lmiui/net/exception/OperationCancelledException;
    iget-object v2, p0, Lmiui/net/PaymentManager$PaymentCallback;->mPaymentListener:Lmiui/net/PaymentManager$PaymentListener;

    iget-object v3, p0, Lmiui/net/PaymentManager$PaymentCallback;->mPaymentId:Ljava/lang/String;

    const/4 v4, 0x4

    invoke-virtual {v0}, Lmiui/net/exception/OperationCancelledException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v3, v4, v5}, Lmiui/net/PaymentManager$PaymentListener;->onFailed(Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_4

    .end local v0           #e:Lmiui/net/exception/OperationCancelledException;
    :catch_3c
    move-exception v0

    .local v0, e:Lmiui/net/exception/AuthenticationFailureException;
    iget-object v2, p0, Lmiui/net/PaymentManager$PaymentCallback;->mPaymentListener:Lmiui/net/PaymentManager$PaymentListener;

    iget-object v3, p0, Lmiui/net/PaymentManager$PaymentCallback;->mPaymentId:Ljava/lang/String;

    const/4 v4, 0x5

    invoke-virtual {v0}, Lmiui/net/exception/AuthenticationFailureException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v3, v4, v5}, Lmiui/net/PaymentManager$PaymentListener;->onFailed(Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_4

    .end local v0           #e:Lmiui/net/exception/AuthenticationFailureException;
    :catch_4a
    move-exception v0

    .local v0, e:Lmiui/net/exception/PaymentServiceFailureException;
    iget-object v2, p0, Lmiui/net/PaymentManager$PaymentCallback;->mPaymentListener:Lmiui/net/PaymentManager$PaymentListener;

    iget-object v3, p0, Lmiui/net/PaymentManager$PaymentCallback;->mPaymentId:Ljava/lang/String;

    invoke-virtual {v0}, Lmiui/net/exception/PaymentServiceFailureException;->getError()I

    move-result v4

    invoke-virtual {v0}, Lmiui/net/exception/PaymentServiceFailureException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v3, v4, v5}, Lmiui/net/PaymentManager$PaymentListener;->onFailed(Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_4
.end method
