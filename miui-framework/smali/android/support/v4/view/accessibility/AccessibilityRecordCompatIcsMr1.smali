.class Landroid/support/v4/view/accessibility/AccessibilityRecordCompatIcsMr1;
.super Ljava/lang/Object;
.source "AccessibilityRecordCompatIcsMr1.java"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getMaxScrollX(Ljava/lang/Object;)I
    .registers 2
    .parameter "record"

    .prologue
    check-cast p0, Landroid/view/accessibility/AccessibilityRecord;

    .end local p0
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityRecord;->getMaxScrollX()I

    move-result v0

    return v0
.end method

.method public static getMaxScrollY(Ljava/lang/Object;)I
    .registers 2
    .parameter "record"

    .prologue
    check-cast p0, Landroid/view/accessibility/AccessibilityRecord;

    .end local p0
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityRecord;->getMaxScrollY()I

    move-result v0

    return v0
.end method

.method public static setMaxScrollX(Ljava/lang/Object;I)V
    .registers 2
    .parameter "record"
    .parameter "maxScrollX"

    .prologue
    check-cast p0, Landroid/view/accessibility/AccessibilityRecord;

    .end local p0
    invoke-virtual {p0, p1}, Landroid/view/accessibility/AccessibilityRecord;->setMaxScrollX(I)V

    return-void
.end method

.method public static setMaxScrollY(Ljava/lang/Object;I)V
    .registers 2
    .parameter "record"
    .parameter "maxScrollY"

    .prologue
    check-cast p0, Landroid/view/accessibility/AccessibilityRecord;

    .end local p0
    invoke-virtual {p0, p1}, Landroid/view/accessibility/AccessibilityRecord;->setMaxScrollY(I)V

    return-void
.end method
