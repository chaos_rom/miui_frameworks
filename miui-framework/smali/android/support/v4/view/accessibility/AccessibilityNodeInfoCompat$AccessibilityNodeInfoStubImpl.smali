.class Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat$AccessibilityNodeInfoStubImpl;
.super Ljava/lang/Object;
.source "AccessibilityNodeInfoCompat.java"

# interfaces
.implements Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat$AccessibilityNodeInfoImpl;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "AccessibilityNodeInfoStubImpl"
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public addAction(Ljava/lang/Object;I)V
    .registers 3
    .parameter "info"
    .parameter "action"

    .prologue
    return-void
.end method

.method public addChild(Ljava/lang/Object;Landroid/view/View;)V
    .registers 3
    .parameter "info"
    .parameter "child"

    .prologue
    return-void
.end method

.method public addChild(Ljava/lang/Object;Landroid/view/View;I)V
    .registers 4
    .parameter "info"
    .parameter "child"
    .parameter "virtualDescendantId"

    .prologue
    return-void
.end method

.method public findAccessibilityNodeInfosByText(Ljava/lang/Object;Ljava/lang/String;)Ljava/util/List;
    .registers 4
    .parameter "info"
    .parameter "text"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public findFocus(Ljava/lang/Object;I)Ljava/lang/Object;
    .registers 4
    .parameter "info"
    .parameter "focus"

    .prologue
    const/4 v0, 0x0

    return-object v0
.end method

.method public focusSearch(Ljava/lang/Object;I)Ljava/lang/Object;
    .registers 4
    .parameter "info"
    .parameter "direction"

    .prologue
    const/4 v0, 0x0

    return-object v0
.end method

.method public getActions(Ljava/lang/Object;)I
    .registers 3
    .parameter "info"

    .prologue
    const/4 v0, 0x0

    return v0
.end method

.method public getBoundsInParent(Ljava/lang/Object;Landroid/graphics/Rect;)V
    .registers 3
    .parameter "info"
    .parameter "outBounds"

    .prologue
    return-void
.end method

.method public getBoundsInScreen(Ljava/lang/Object;Landroid/graphics/Rect;)V
    .registers 3
    .parameter "info"
    .parameter "outBounds"

    .prologue
    return-void
.end method

.method public getChild(Ljava/lang/Object;I)Ljava/lang/Object;
    .registers 4
    .parameter "info"
    .parameter "index"

    .prologue
    const/4 v0, 0x0

    return-object v0
.end method

.method public getChildCount(Ljava/lang/Object;)I
    .registers 3
    .parameter "info"

    .prologue
    const/4 v0, 0x0

    return v0
.end method

.method public getClassName(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .registers 3
    .parameter "info"

    .prologue
    const/4 v0, 0x0

    return-object v0
.end method

.method public getContentDescription(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .registers 3
    .parameter "info"

    .prologue
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMovementGranularities(Ljava/lang/Object;)I
    .registers 3
    .parameter "info"

    .prologue
    const/4 v0, 0x0

    return v0
.end method

.method public getPackageName(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .registers 3
    .parameter "info"

    .prologue
    const/4 v0, 0x0

    return-object v0
.end method

.method public getParent(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter "info"

    .prologue
    const/4 v0, 0x0

    return-object v0
.end method

.method public getText(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .registers 3
    .parameter "info"

    .prologue
    const/4 v0, 0x0

    return-object v0
.end method

.method public getWindowId(Ljava/lang/Object;)I
    .registers 3
    .parameter "info"

    .prologue
    const/4 v0, 0x0

    return v0
.end method

.method public isAccessibilityFocused(Ljava/lang/Object;)Z
    .registers 3
    .parameter "info"

    .prologue
    const/4 v0, 0x0

    return v0
.end method

.method public isCheckable(Ljava/lang/Object;)Z
    .registers 3
    .parameter "info"

    .prologue
    const/4 v0, 0x0

    return v0
.end method

.method public isChecked(Ljava/lang/Object;)Z
    .registers 3
    .parameter "info"

    .prologue
    const/4 v0, 0x0

    return v0
.end method

.method public isClickable(Ljava/lang/Object;)Z
    .registers 3
    .parameter "info"

    .prologue
    const/4 v0, 0x0

    return v0
.end method

.method public isEnabled(Ljava/lang/Object;)Z
    .registers 3
    .parameter "info"

    .prologue
    const/4 v0, 0x0

    return v0
.end method

.method public isFocusable(Ljava/lang/Object;)Z
    .registers 3
    .parameter "info"

    .prologue
    const/4 v0, 0x0

    return v0
.end method

.method public isFocused(Ljava/lang/Object;)Z
    .registers 3
    .parameter "info"

    .prologue
    const/4 v0, 0x0

    return v0
.end method

.method public isLongClickable(Ljava/lang/Object;)Z
    .registers 3
    .parameter "info"

    .prologue
    const/4 v0, 0x0

    return v0
.end method

.method public isPassword(Ljava/lang/Object;)Z
    .registers 3
    .parameter "info"

    .prologue
    const/4 v0, 0x0

    return v0
.end method

.method public isScrollable(Ljava/lang/Object;)Z
    .registers 3
    .parameter "info"

    .prologue
    const/4 v0, 0x0

    return v0
.end method

.method public isSelected(Ljava/lang/Object;)Z
    .registers 3
    .parameter "info"

    .prologue
    const/4 v0, 0x0

    return v0
.end method

.method public isVisibleToUser(Ljava/lang/Object;)Z
    .registers 3
    .parameter "info"

    .prologue
    const/4 v0, 0x0

    return v0
.end method

.method public obtain()Ljava/lang/Object;
    .registers 2

    .prologue
    const/4 v0, 0x0

    return-object v0
.end method

.method public obtain(Landroid/view/View;)Ljava/lang/Object;
    .registers 3
    .parameter "source"

    .prologue
    const/4 v0, 0x0

    return-object v0
.end method

.method public obtain(Landroid/view/View;I)Ljava/lang/Object;
    .registers 4
    .parameter "root"
    .parameter "virtualDescendantId"

    .prologue
    const/4 v0, 0x0

    return-object v0
.end method

.method public obtain(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter "info"

    .prologue
    const/4 v0, 0x0

    return-object v0
.end method

.method public performAction(Ljava/lang/Object;I)Z
    .registers 4
    .parameter "info"
    .parameter "action"

    .prologue
    const/4 v0, 0x0

    return v0
.end method

.method public performAction(Ljava/lang/Object;ILandroid/os/Bundle;)Z
    .registers 5
    .parameter "info"
    .parameter "action"
    .parameter "arguments"

    .prologue
    const/4 v0, 0x0

    return v0
.end method

.method public recycle(Ljava/lang/Object;)V
    .registers 2
    .parameter "info"

    .prologue
    return-void
.end method

.method public setAccessibilityFocused(Ljava/lang/Object;Z)V
    .registers 3
    .parameter "info"
    .parameter "focused"

    .prologue
    return-void
.end method

.method public setBoundsInParent(Ljava/lang/Object;Landroid/graphics/Rect;)V
    .registers 3
    .parameter "info"
    .parameter "bounds"

    .prologue
    return-void
.end method

.method public setBoundsInScreen(Ljava/lang/Object;Landroid/graphics/Rect;)V
    .registers 3
    .parameter "info"
    .parameter "bounds"

    .prologue
    return-void
.end method

.method public setCheckable(Ljava/lang/Object;Z)V
    .registers 3
    .parameter "info"
    .parameter "checkable"

    .prologue
    return-void
.end method

.method public setChecked(Ljava/lang/Object;Z)V
    .registers 3
    .parameter "info"
    .parameter "checked"

    .prologue
    return-void
.end method

.method public setClassName(Ljava/lang/Object;Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "info"
    .parameter "className"

    .prologue
    return-void
.end method

.method public setClickable(Ljava/lang/Object;Z)V
    .registers 3
    .parameter "info"
    .parameter "clickable"

    .prologue
    return-void
.end method

.method public setContentDescription(Ljava/lang/Object;Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "info"
    .parameter "contentDescription"

    .prologue
    return-void
.end method

.method public setEnabled(Ljava/lang/Object;Z)V
    .registers 3
    .parameter "info"
    .parameter "enabled"

    .prologue
    return-void
.end method

.method public setFocusable(Ljava/lang/Object;Z)V
    .registers 3
    .parameter "info"
    .parameter "focusable"

    .prologue
    return-void
.end method

.method public setFocused(Ljava/lang/Object;Z)V
    .registers 3
    .parameter "info"
    .parameter "focused"

    .prologue
    return-void
.end method

.method public setLongClickable(Ljava/lang/Object;Z)V
    .registers 3
    .parameter "info"
    .parameter "longClickable"

    .prologue
    return-void
.end method

.method public setMovementGranularities(Ljava/lang/Object;I)V
    .registers 3
    .parameter "info"
    .parameter "granularities"

    .prologue
    return-void
.end method

.method public setPackageName(Ljava/lang/Object;Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "info"
    .parameter "packageName"

    .prologue
    return-void
.end method

.method public setParent(Ljava/lang/Object;Landroid/view/View;)V
    .registers 3
    .parameter "info"
    .parameter "parent"

    .prologue
    return-void
.end method

.method public setParent(Ljava/lang/Object;Landroid/view/View;I)V
    .registers 4
    .parameter "info"
    .parameter "root"
    .parameter "virtualDescendantId"

    .prologue
    return-void
.end method

.method public setPassword(Ljava/lang/Object;Z)V
    .registers 3
    .parameter "info"
    .parameter "password"

    .prologue
    return-void
.end method

.method public setScrollable(Ljava/lang/Object;Z)V
    .registers 3
    .parameter "info"
    .parameter "scrollable"

    .prologue
    return-void
.end method

.method public setSelected(Ljava/lang/Object;Z)V
    .registers 3
    .parameter "info"
    .parameter "selected"

    .prologue
    return-void
.end method

.method public setSource(Ljava/lang/Object;Landroid/view/View;)V
    .registers 3
    .parameter "info"
    .parameter "source"

    .prologue
    return-void
.end method

.method public setSource(Ljava/lang/Object;Landroid/view/View;I)V
    .registers 4
    .parameter "info"
    .parameter "root"
    .parameter "virtualDescendantId"

    .prologue
    return-void
.end method

.method public setText(Ljava/lang/Object;Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "info"
    .parameter "text"

    .prologue
    return-void
.end method

.method public setVisibleToUser(Ljava/lang/Object;Z)V
    .registers 3
    .parameter "info"
    .parameter "visibleToUser"

    .prologue
    return-void
.end method
