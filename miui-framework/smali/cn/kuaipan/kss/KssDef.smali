.class public Lcn/kuaipan/kss/KssDef;
.super Ljava/lang/Object;
.source "KssDef.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcn/kuaipan/kss/KssDef$OnUpDownload;,
        Lcn/kuaipan/kss/KssDef$NetState;,
        Lcn/kuaipan/kss/KssDef$KssAPIResult;
    }
.end annotation


# static fields
.field public static BLOCKSIZE:I = 0x0

.field public static CHUNKSIZE_MIN:I = 0x0

.field public static DOWNLOAD_BUFFER:I = 0x0

.field public static final FUNC_UPLOADBLOCKCHUNK:Ljava/lang/String; = "upload_block_chunk"

.field public static final KEY_BLOCKINFO:Ljava/lang/String; = "block_infos"

.field public static final KEY_BLOCKMETA:Ljava/lang/String; = "block_meta"

.field public static final KEY_BLOCKMETAS:Ljava/lang/String; = "block_metas"

.field public static final KEY_BLOCKS:Ljava/lang/String; = "blocks"

.field public static final KEY_BODYSUM:Ljava/lang/String; = "body_sum"

.field public static final KEY_CHUNKPOS:Ljava/lang/String; = "chunk_pos"

.field public static final KEY_COMMITMETA:Ljava/lang/String; = "commit_meta"

.field public static final KEY_COMMITMETAS:Ljava/lang/String; = "commit_metas"

.field public static final KEY_DESTURL:Ljava/lang/String; = "Dest-Url"

.field public static final KEY_FILEMETA:Ljava/lang/String; = "file_meta"

.field public static final KEY_ISEXISTED:Ljava/lang/String; = "is_existed"

.field public static final KEY_MD5:Ljava/lang/String; = "md5"

.field public static final KEY_NODEURLS:Ljava/lang/String; = "node_urls"

.field public static final KEY_PROXIES:Ljava/lang/String; = "proxies"

.field public static final KEY_SECUREKEY:Ljava/lang/String; = "secure_key"

.field public static final KEY_SHA1:Ljava/lang/String; = "sha1"

.field public static final KEY_SIZE:Ljava/lang/String; = "size"

.field public static final KEY_STAT:Ljava/lang/String; = "stat"

.field public static final KEY_STOID:Ljava/lang/String; = "stoid"

.field public static final KEY_UPLOADID:Ljava/lang/String; = "upload_id"

.field public static final KEY_URL:Ljava/lang/String; = "url"

.field public static final KEY_URLS:Ljava/lang/String; = "urls"

.field public static NET_RETRY_TIMES:I = 0x0

.field public static final VALUE_BLOCKCOMPLETED:Ljava/lang/String; = "BLOCK_COMPLETED"

.field public static final VALUE_CONTINUEUPLOAD:Ljava/lang/String; = "CONTINUE_UPLOAD"

.field public static final VALUE_FILEEXISTED:Ljava/lang/String; = "FILE_EXISTED"

.field public static final VALUE_OK:Ljava/lang/String; = "OK"


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    const/high16 v0, 0x40

    sput v0, Lcn/kuaipan/kss/KssDef;->BLOCKSIZE:I

    const/high16 v0, 0x1

    sput v0, Lcn/kuaipan/kss/KssDef;->CHUNKSIZE_MIN:I

    const/16 v0, 0x2000

    sput v0, Lcn/kuaipan/kss/KssDef;->DOWNLOAD_BUFFER:I

    const/4 v0, 0x3

    sput v0, Lcn/kuaipan/kss/KssDef;->NET_RETRY_TIMES:I

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
