.class Lcn/kuaipan/kss/implement/_DownloadProgress;
.super Ljava/lang/Object;
.source "HttpClientDownload.java"


# instance fields
.field private m_isCanceled:Z

.field private m_lastSize:I

.field private m_notifySize:I

.field private m_onDownload:Lcn/kuaipan/kss/KssDef$OnUpDownload;

.field private m_totalSize:I

.field private m_transSize:I


# direct methods
.method constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcn/kuaipan/kss/implement/_DownloadProgress;->m_onDownload:Lcn/kuaipan/kss/KssDef$OnUpDownload;

    iput-boolean v1, p0, Lcn/kuaipan/kss/implement/_DownloadProgress;->m_isCanceled:Z

    iput v1, p0, Lcn/kuaipan/kss/implement/_DownloadProgress;->m_transSize:I

    iput v1, p0, Lcn/kuaipan/kss/implement/_DownloadProgress;->m_totalSize:I

    iput v1, p0, Lcn/kuaipan/kss/implement/_DownloadProgress;->m_lastSize:I

    iput v1, p0, Lcn/kuaipan/kss/implement/_DownloadProgress;->m_notifySize:I

    return-void
.end method


# virtual methods
.method public TransBytes(I)V
    .registers 5
    .parameter "transBytes"

    .prologue
    const/4 v0, 0x1

    iget-object v1, p0, Lcn/kuaipan/kss/implement/_DownloadProgress;->m_onDownload:Lcn/kuaipan/kss/KssDef$OnUpDownload;

    if-eqz v1, :cond_7

    if-ge p1, v0, :cond_8

    :cond_7
    :goto_7
    return-void

    :cond_8
    iget v1, p0, Lcn/kuaipan/kss/implement/_DownloadProgress;->m_transSize:I

    add-int/2addr v1, p1

    iput v1, p0, Lcn/kuaipan/kss/implement/_DownloadProgress;->m_transSize:I

    iget v1, p0, Lcn/kuaipan/kss/implement/_DownloadProgress;->m_transSize:I

    iget v2, p0, Lcn/kuaipan/kss/implement/_DownloadProgress;->m_totalSize:I

    if-ge v1, v2, :cond_1c

    iget v1, p0, Lcn/kuaipan/kss/implement/_DownloadProgress;->m_transSize:I

    iget v2, p0, Lcn/kuaipan/kss/implement/_DownloadProgress;->m_lastSize:I

    sub-int/2addr v1, v2

    iget v2, p0, Lcn/kuaipan/kss/implement/_DownloadProgress;->m_notifySize:I

    if-lt v1, v2, :cond_7

    :cond_1c
    iget-object v1, p0, Lcn/kuaipan/kss/implement/_DownloadProgress;->m_onDownload:Lcn/kuaipan/kss/KssDef$OnUpDownload;

    iget v2, p0, Lcn/kuaipan/kss/implement/_DownloadProgress;->m_transSize:I

    invoke-interface {v1, v2}, Lcn/kuaipan/kss/KssDef$OnUpDownload;->OnTransData(I)Z

    move-result v1

    if-nez v1, :cond_2d

    :goto_26
    iput-boolean v0, p0, Lcn/kuaipan/kss/implement/_DownloadProgress;->m_isCanceled:Z

    iget v0, p0, Lcn/kuaipan/kss/implement/_DownloadProgress;->m_transSize:I

    iput v0, p0, Lcn/kuaipan/kss/implement/_DownloadProgress;->m_lastSize:I

    goto :goto_7

    :cond_2d
    const/4 v0, 0x0

    goto :goto_26
.end method

.method public getIsCanceled()Z
    .registers 2

    .prologue
    iget-boolean v0, p0, Lcn/kuaipan/kss/implement/_DownloadProgress;->m_isCanceled:Z

    return v0
.end method

.method public init(Lcn/kuaipan/kss/KssDownload$RequestDownloadInfo;Lcn/kuaipan/kss/KssDef$OnUpDownload;)V
    .registers 5
    .parameter "requestInfo"
    .parameter "onUpload"

    .prologue
    const/4 v1, 0x0

    if-nez p1, :cond_4

    :goto_3
    return-void

    :cond_4
    iput-object p2, p0, Lcn/kuaipan/kss/implement/_DownloadProgress;->m_onDownload:Lcn/kuaipan/kss/KssDef$OnUpDownload;

    iput-boolean v1, p0, Lcn/kuaipan/kss/implement/_DownloadProgress;->m_isCanceled:Z

    iput v1, p0, Lcn/kuaipan/kss/implement/_DownloadProgress;->m_transSize:I

    invoke-interface {p1}, Lcn/kuaipan/kss/KssDownload$RequestDownloadInfo;->getFileSize()I

    move-result v0

    iput v0, p0, Lcn/kuaipan/kss/implement/_DownloadProgress;->m_totalSize:I

    iput v1, p0, Lcn/kuaipan/kss/implement/_DownloadProgress;->m_lastSize:I

    iget v0, p0, Lcn/kuaipan/kss/implement/_DownloadProgress;->m_totalSize:I

    div-int/lit8 v0, v0, 0x32

    iput v0, p0, Lcn/kuaipan/kss/implement/_DownloadProgress;->m_notifySize:I

    goto :goto_3
.end method

.method public setIsCanceled(Z)V
    .registers 2
    .parameter "isCancel"

    .prologue
    iput-boolean p1, p0, Lcn/kuaipan/kss/implement/_DownloadProgress;->m_isCanceled:Z

    return-void
.end method

.method public startTrans(Lcn/kuaipan/kss/KssDownload$DownloadTransControl;)V
    .registers 3
    .parameter "downloadCtr"

    .prologue
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcn/kuaipan/kss/implement/_DownloadProgress;->m_isCanceled:Z

    invoke-interface {p1}, Lcn/kuaipan/kss/KssDownload$DownloadTransControl;->getStartPos()I

    move-result v0

    iput v0, p0, Lcn/kuaipan/kss/implement/_DownloadProgress;->m_transSize:I

    iget v0, p0, Lcn/kuaipan/kss/implement/_DownloadProgress;->m_transSize:I

    iput v0, p0, Lcn/kuaipan/kss/implement/_DownloadProgress;->m_lastSize:I

    return-void
.end method
