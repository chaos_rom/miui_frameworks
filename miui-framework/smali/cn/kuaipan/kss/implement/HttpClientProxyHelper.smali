.class public Lcn/kuaipan/kss/implement/HttpClientProxyHelper;
.super Ljava/lang/Object;
.source "HttpClientProxyHelper.java"


# instance fields
.field private m_ProxyURLs:[Ljava/lang/String;

.field private m_proxyIndex:I


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcn/kuaipan/kss/implement/HttpClientProxyHelper;->m_proxyIndex:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcn/kuaipan/kss/implement/HttpClientProxyHelper;->m_ProxyURLs:[Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public createGet(Ljava/lang/String;)Lorg/apache/http/client/methods/HttpGet;
    .registers 4
    .parameter "uriString"

    .prologue
    invoke-virtual {p0}, Lcn/kuaipan/kss/implement/HttpClientProxyHelper;->isUsingProxy()Z

    move-result v1

    if-nez v1, :cond_c

    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v0, p1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    :goto_b
    return-object v0

    :cond_c
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {p0}, Lcn/kuaipan/kss/implement/HttpClientProxyHelper;->getCurrentProxy()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .local v0, get:Lorg/apache/http/client/methods/HttpGet;
    const-string v1, "Dest-Url"

    invoke-virtual {v0, v1, p1}, Lorg/apache/http/client/methods/HttpGet;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_b
.end method

.method public createGet(Ljava/net/URI;)Lorg/apache/http/client/methods/HttpGet;
    .registers 5
    .parameter "uri"

    .prologue
    invoke-virtual {p0}, Lcn/kuaipan/kss/implement/HttpClientProxyHelper;->isUsingProxy()Z

    move-result v1

    if-nez v1, :cond_c

    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v0, p1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/net/URI;)V

    :goto_b
    return-object v0

    :cond_c
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {p0}, Lcn/kuaipan/kss/implement/HttpClientProxyHelper;->getCurrentProxy()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .local v0, get:Lorg/apache/http/client/methods/HttpGet;
    const-string v1, "Dest-Url"

    invoke-virtual {p1}, Ljava/net/URI;->toASCIIString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpGet;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_b
.end method

.method public createPost(Ljava/net/URI;)Lorg/apache/http/client/methods/HttpPost;
    .registers 5
    .parameter "uri"

    .prologue
    invoke-virtual {p0}, Lcn/kuaipan/kss/implement/HttpClientProxyHelper;->isUsingProxy()Z

    move-result v1

    if-nez v1, :cond_c

    new-instance v0, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v0, p1}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/net/URI;)V

    :goto_b
    return-object v0

    :cond_c
    new-instance v0, Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {p0}, Lcn/kuaipan/kss/implement/HttpClientProxyHelper;->getCurrentProxy()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .local v0, post:Lorg/apache/http/client/methods/HttpPost;
    const-string v1, "Dest-Url"

    invoke-virtual {p1}, Ljava/net/URI;->toASCIIString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_b
.end method

.method public getCurrentProxy()Ljava/lang/String;
    .registers 4

    .prologue
    const/4 v0, 0x0

    iget-object v1, p0, Lcn/kuaipan/kss/implement/HttpClientProxyHelper;->m_ProxyURLs:[Ljava/lang/String;

    if-nez v1, :cond_6

    :cond_5
    :goto_5
    return-object v0

    :cond_6
    iget v1, p0, Lcn/kuaipan/kss/implement/HttpClientProxyHelper;->m_proxyIndex:I

    if-ltz v1, :cond_5

    iget v1, p0, Lcn/kuaipan/kss/implement/HttpClientProxyHelper;->m_proxyIndex:I

    iget-object v2, p0, Lcn/kuaipan/kss/implement/HttpClientProxyHelper;->m_ProxyURLs:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_5

    iget-object v0, p0, Lcn/kuaipan/kss/implement/HttpClientProxyHelper;->m_ProxyURLs:[Ljava/lang/String;

    iget v1, p0, Lcn/kuaipan/kss/implement/HttpClientProxyHelper;->m_proxyIndex:I

    aget-object v0, v0, v1

    goto :goto_5
.end method

.method public init([Ljava/lang/String;)V
    .registers 3
    .parameter "proxies"

    .prologue
    iput-object p1, p0, Lcn/kuaipan/kss/implement/HttpClientProxyHelper;->m_ProxyURLs:[Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcn/kuaipan/kss/implement/HttpClientProxyHelper;->m_proxyIndex:I

    return-void
.end method

.method public isUsingProxy()Z
    .registers 3

    .prologue
    iget v0, p0, Lcn/kuaipan/kss/implement/HttpClientProxyHelper;->m_proxyIndex:I

    const/4 v1, -0x1

    if-le v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public nextProxy()Z
    .registers 4

    .prologue
    const/4 v0, 0x0

    iget-object v1, p0, Lcn/kuaipan/kss/implement/HttpClientProxyHelper;->m_ProxyURLs:[Ljava/lang/String;

    if-nez v1, :cond_6

    :goto_5
    return v0

    :cond_6
    iget v1, p0, Lcn/kuaipan/kss/implement/HttpClientProxyHelper;->m_proxyIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcn/kuaipan/kss/implement/HttpClientProxyHelper;->m_proxyIndex:I

    iget v1, p0, Lcn/kuaipan/kss/implement/HttpClientProxyHelper;->m_proxyIndex:I

    iget-object v2, p0, Lcn/kuaipan/kss/implement/HttpClientProxyHelper;->m_ProxyURLs:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_15

    const/4 v0, 0x1

    goto :goto_5

    :cond_15
    iget v1, p0, Lcn/kuaipan/kss/implement/HttpClientProxyHelper;->m_proxyIndex:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcn/kuaipan/kss/implement/HttpClientProxyHelper;->m_proxyIndex:I

    goto :goto_5
.end method

.method public reset()Z
    .registers 2

    .prologue
    const/4 v0, -0x1

    iput v0, p0, Lcn/kuaipan/kss/implement/HttpClientProxyHelper;->m_proxyIndex:I

    const/4 v0, 0x1

    return v0
.end method
