.class Lcn/kuaipan/kss/implement/_UploadProgress;
.super Ljava/lang/Object;
.source "HttpClientUpload.java"


# instance fields
.field private m_isCanceled:Z

.field private m_onUpload:Lcn/kuaipan/kss/KssDef$OnUpDownload;

.field private m_transSize:I


# direct methods
.method constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcn/kuaipan/kss/implement/_UploadProgress;->m_onUpload:Lcn/kuaipan/kss/KssDef$OnUpDownload;

    iput-boolean v1, p0, Lcn/kuaipan/kss/implement/_UploadProgress;->m_isCanceled:Z

    iput v1, p0, Lcn/kuaipan/kss/implement/_UploadProgress;->m_transSize:I

    return-void
.end method


# virtual methods
.method public TransBytes(I)Z
    .registers 4
    .parameter "transBytes"

    .prologue
    if-lez p1, :cond_15

    iget v0, p0, Lcn/kuaipan/kss/implement/_UploadProgress;->m_transSize:I

    add-int/2addr v0, p1

    iput v0, p0, Lcn/kuaipan/kss/implement/_UploadProgress;->m_transSize:I

    iget-object v0, p0, Lcn/kuaipan/kss/implement/_UploadProgress;->m_onUpload:Lcn/kuaipan/kss/KssDef$OnUpDownload;

    if-eqz v0, :cond_15

    iget-object v0, p0, Lcn/kuaipan/kss/implement/_UploadProgress;->m_onUpload:Lcn/kuaipan/kss/KssDef$OnUpDownload;

    iget v1, p0, Lcn/kuaipan/kss/implement/_UploadProgress;->m_transSize:I

    invoke-interface {v0, v1}, Lcn/kuaipan/kss/KssDef$OnUpDownload;->OnTransData(I)Z

    move-result v0

    iput-boolean v0, p0, Lcn/kuaipan/kss/implement/_UploadProgress;->m_isCanceled:Z

    :cond_15
    iget-boolean v0, p0, Lcn/kuaipan/kss/implement/_UploadProgress;->m_isCanceled:Z

    return v0
.end method

.method public getIsCanceled()Z
    .registers 2

    .prologue
    iget-boolean v0, p0, Lcn/kuaipan/kss/implement/_UploadProgress;->m_isCanceled:Z

    return v0
.end method

.method public init(Lcn/kuaipan/kss/KssUpload$RequestUploadInfo;Lcn/kuaipan/kss/KssDef$OnUpDownload;)V
    .registers 3
    .parameter "requestInfo"
    .parameter "onUpload"

    .prologue
    if-nez p1, :cond_3

    :goto_2
    return-void

    :cond_3
    iput-object p2, p0, Lcn/kuaipan/kss/implement/_UploadProgress;->m_onUpload:Lcn/kuaipan/kss/KssDef$OnUpDownload;

    goto :goto_2
.end method

.method public setIsCanceled(Z)V
    .registers 2
    .parameter "isCancel"

    .prologue
    iput-boolean p1, p0, Lcn/kuaipan/kss/implement/_UploadProgress;->m_isCanceled:Z

    return-void
.end method
