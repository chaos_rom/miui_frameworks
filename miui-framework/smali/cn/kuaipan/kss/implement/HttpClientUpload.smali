.class public Lcn/kuaipan/kss/implement/HttpClientUpload;
.super Ljava/lang/Object;
.source "HttpClientUpload.java"

# interfaces
.implements Lcn/kuaipan/kss/KssUpload;


# instance fields
.field private CHUNKSIZE:I

.field private m_chunkDataBuffer:[B

.field private m_client:Lorg/apache/http/client/HttpClient;

.field private m_crc32:Ljava/util/zip/CRC32;

.field private m_progress:Lcn/kuaipan/kss/implement/_UploadProgress;

.field private m_proxyHelper:Lcn/kuaipan/kss/implement/HttpClientProxyHelper;

.field private m_rc4:Lcn/kuaipan/kss/utils/RC4;

.field private m_requestInfo:Lcn/kuaipan/kss/KssUpload$RequestUploadInfo;

.field private m_uploadResult:Lcn/kuaipan/kss/implement/_UploadResult;


# direct methods
.method public constructor <init>(Lorg/apache/http/client/HttpClient;Lcn/kuaipan/kss/implement/HttpClientProxyHelper;)V
    .registers 5
    .parameter "client"
    .parameter "proxyHelper"

    .prologue
    const/4 v1, 0x0

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    sget v0, Lcn/kuaipan/kss/KssDef;->CHUNKSIZE_MIN:I

    iput v0, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->CHUNKSIZE:I

    iput-object v1, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_client:Lorg/apache/http/client/HttpClient;

    iput-object v1, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_proxyHelper:Lcn/kuaipan/kss/implement/HttpClientProxyHelper;

    iput-object v1, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_requestInfo:Lcn/kuaipan/kss/KssUpload$RequestUploadInfo;

    iput-object v1, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_progress:Lcn/kuaipan/kss/implement/_UploadProgress;

    iput-object v1, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_uploadResult:Lcn/kuaipan/kss/implement/_UploadResult;

    iget v0, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->CHUNKSIZE:I

    new-array v0, v0, [B

    iput-object v0, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_chunkDataBuffer:[B

    new-instance v0, Lcn/kuaipan/kss/utils/RC4;

    invoke-direct {v0}, Lcn/kuaipan/kss/utils/RC4;-><init>()V

    iput-object v0, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_rc4:Lcn/kuaipan/kss/utils/RC4;

    new-instance v0, Ljava/util/zip/CRC32;

    invoke-direct {v0}, Ljava/util/zip/CRC32;-><init>()V

    iput-object v0, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_crc32:Ljava/util/zip/CRC32;

    iput-object p1, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_client:Lorg/apache/http/client/HttpClient;

    iput-object p2, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_proxyHelper:Lcn/kuaipan/kss/implement/HttpClientProxyHelper;

    new-instance v0, Lcn/kuaipan/kss/implement/_UploadProgress;

    invoke-direct {v0}, Lcn/kuaipan/kss/implement/_UploadProgress;-><init>()V

    iput-object v0, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_progress:Lcn/kuaipan/kss/implement/_UploadProgress;

    new-instance v0, Lcn/kuaipan/kss/implement/_UploadResult;

    invoke-direct {v0}, Lcn/kuaipan/kss/implement/_UploadResult;-><init>()V

    iput-object v0, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_uploadResult:Lcn/kuaipan/kss/implement/_UploadResult;

    return-void
.end method

.method private _genUploadChunkPost(Ljava/net/URI;[BI)Lorg/apache/http/client/methods/HttpPost;
    .registers 9
    .parameter "uri"
    .parameter "transData"
    .parameter "transLength"

    .prologue
    const/4 v2, 0x0

    .local v2, post:Lorg/apache/http/client/methods/HttpPost;
    iget-object v3, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_proxyHelper:Lcn/kuaipan/kss/implement/HttpClientProxyHelper;

    if-nez v3, :cond_17

    new-instance v2, Lorg/apache/http/client/methods/HttpPost;

    .end local v2           #post:Lorg/apache/http/client/methods/HttpPost;
    invoke-direct {v2, p1}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/net/URI;)V

    .restart local v2       #post:Lorg/apache/http/client/methods/HttpPost;
    :goto_a
    const/4 v0, 0x0

    .local v0, entity:Lorg/apache/http/HttpEntity;
    array-length v3, p2

    if-ne p3, v3, :cond_1e

    new-instance v0, Lorg/apache/http/entity/ByteArrayEntity;

    .end local v0           #entity:Lorg/apache/http/HttpEntity;
    invoke-direct {v0, p2}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    .restart local v0       #entity:Lorg/apache/http/HttpEntity;
    :goto_13
    invoke-virtual {v2, v0}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    return-object v2

    .end local v0           #entity:Lorg/apache/http/HttpEntity;
    :cond_17
    iget-object v3, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_proxyHelper:Lcn/kuaipan/kss/implement/HttpClientProxyHelper;

    invoke-virtual {v3, p1}, Lcn/kuaipan/kss/implement/HttpClientProxyHelper;->createPost(Ljava/net/URI;)Lorg/apache/http/client/methods/HttpPost;

    move-result-object v2

    goto :goto_a

    .restart local v0       #entity:Lorg/apache/http/HttpEntity;
    :cond_1e
    new-instance v1, Ljava/io/ByteArrayInputStream;

    const/4 v3, 0x0

    invoke-direct {v1, p2, v3, p3}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    .local v1, in:Ljava/io/ByteArrayInputStream;
    new-instance v0, Lorg/apache/http/entity/InputStreamEntity;

    .end local v0           #entity:Lorg/apache/http/HttpEntity;
    int-to-long v3, p3

    invoke-direct {v0, v1, v3, v4}, Lorg/apache/http/entity/InputStreamEntity;-><init>(Ljava/io/InputStream;J)V

    .restart local v0       #entity:Lorg/apache/http/HttpEntity;
    goto :goto_13
.end method

.method private _genUploadChunkURI(Ljava/lang/String;IILcn/kuaipan/kss/implement/_UploadChunkInfo;)Ljava/net/URI;
    .registers 13
    .parameter "dataCRC32"
    .parameter "chunkPos"
    .parameter "blockIdx"
    .parameter "chunkInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .local v7, uriParam:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "chunk_pos"

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "body_sum"

    invoke-direct {v0, v1, p1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-nez p2, :cond_5b

    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "file_meta"

    iget-object v2, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_requestInfo:Lcn/kuaipan/kss/KssUpload$RequestUploadInfo;

    invoke-interface {v2}, Lcn/kuaipan/kss/KssUpload$RequestUploadInfo;->getFileMeta()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "block_meta"

    iget-object v2, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_requestInfo:Lcn/kuaipan/kss/KssUpload$RequestUploadInfo;

    invoke-interface {v2, p3}, Lcn/kuaipan/kss/KssUpload$RequestUploadInfo;->getBlockMeta(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_3f
    iget-object v0, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_requestInfo:Lcn/kuaipan/kss/KssUpload$RequestUploadInfo;

    invoke-interface {v0}, Lcn/kuaipan/kss/KssUpload$RequestUploadInfo;->getProtocol()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_requestInfo:Lcn/kuaipan/kss/KssUpload$RequestUploadInfo;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcn/kuaipan/kss/KssUpload$RequestUploadInfo;->getNodeIP(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, -0x1

    const-string v3, "upload_block_chunk"

    const-string v4, "UTF-8"

    invoke-static {v7, v4}, Lorg/apache/http/client/utils/URLEncodedUtils;->format(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lorg/apache/http/client/utils/URIUtils;->createURI(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/net/URI;

    move-result-object v6

    .local v6, uri:Ljava/net/URI;
    return-object v6

    .end local v6           #uri:Ljava/net/URI;
    :cond_5b
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "upload_id"

    invoke-virtual {p4}, Lcn/kuaipan/kss/implement/_UploadChunkInfo;->getUploadID()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3f
.end method

.method private _parseResponse(Lorg/apache/http/HttpResponse;Lcn/kuaipan/kss/implement/_UploadChunkInfo;)Lcn/kuaipan/kss/KssDef$KssAPIResult;
    .registers 11
    .parameter "response"
    .parameter "chunkInfo"

    .prologue
    sget-object v4, Lcn/kuaipan/kss/KssDef$KssAPIResult;->OK:Lcn/kuaipan/kss/KssDef$KssAPIResult;

    .local v4, rt:Lcn/kuaipan/kss/KssDef$KssAPIResult;
    const/4 v1, 0x0

    .local v1, in:Ljava/io/BufferedReader;
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    .local v5, sb:Ljava/lang/StringBuffer;
    :try_start_8
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v6, Ljava/io/InputStreamReader;

    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v7

    invoke-interface {v7}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1a
    .catchall {:try_start_8 .. :try_end_1a} :catchall_5f
    .catch Ljava/lang/IllegalStateException; {:try_start_8 .. :try_end_1a} :catch_71
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_1a} :catch_51

    .end local v1           #in:Ljava/io/BufferedReader;
    .local v2, in:Ljava/io/BufferedReader;
    :try_start_1a
    const-string v3, ""

    .local v3, line:Ljava/lang/String;
    :goto_1c
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3f

    invoke-virtual {v5, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_25
    .catchall {:try_start_1a .. :try_end_25} :catchall_6b
    .catch Ljava/lang/IllegalStateException; {:try_start_1a .. :try_end_25} :catch_26
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_25} :catch_6e

    goto :goto_1c

    .end local v3           #line:Ljava/lang/String;
    :catch_26
    move-exception v0

    move-object v1, v2

    .end local v2           #in:Ljava/io/BufferedReader;
    .local v0, e:Ljava/lang/IllegalStateException;
    .restart local v1       #in:Ljava/io/BufferedReader;
    :goto_28
    :try_start_28
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    sget-object v4, Lcn/kuaipan/kss/KssDef$KssAPIResult;->Error:Lcn/kuaipan/kss/KssDef$KssAPIResult;
    :try_end_2d
    .catchall {:try_start_28 .. :try_end_2d} :catchall_5f

    if-eqz v1, :cond_32

    :try_start_2f
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_32
    .catch Ljava/lang/Exception; {:try_start_2f .. :try_end_32} :catch_4c

    .end local v0           #e:Ljava/lang/IllegalStateException;
    :cond_32
    :goto_32
    sget-object v6, Lcn/kuaipan/kss/KssDef$KssAPIResult;->OK:Lcn/kuaipan/kss/KssDef$KssAPIResult;

    if-ne v4, v6, :cond_3e

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Lcn/kuaipan/kss/implement/_UploadChunkInfo;->parseInfo(Ljava/lang/String;)Lcn/kuaipan/kss/KssDef$KssAPIResult;

    move-result-object v4

    :cond_3e
    return-object v4

    .end local v1           #in:Ljava/io/BufferedReader;
    .restart local v2       #in:Ljava/io/BufferedReader;
    .restart local v3       #line:Ljava/lang/String;
    :cond_3f
    if-eqz v2, :cond_73

    :try_start_41
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_44
    .catch Ljava/lang/Exception; {:try_start_41 .. :try_end_44} :catch_46

    move-object v1, v2

    .end local v2           #in:Ljava/io/BufferedReader;
    .restart local v1       #in:Ljava/io/BufferedReader;
    goto :goto_32

    .end local v1           #in:Ljava/io/BufferedReader;
    .restart local v2       #in:Ljava/io/BufferedReader;
    :catch_46
    move-exception v0

    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v1, v2

    .end local v2           #in:Ljava/io/BufferedReader;
    .restart local v1       #in:Ljava/io/BufferedReader;
    goto :goto_32

    .end local v3           #line:Ljava/lang/String;
    .local v0, e:Ljava/lang/IllegalStateException;
    :catch_4c
    move-exception v0

    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_32

    .end local v0           #e:Ljava/lang/Exception;
    :catch_51
    move-exception v0

    .local v0, e:Ljava/io/IOException;
    :goto_52
    :try_start_52
    sget-object v4, Lcn/kuaipan/kss/KssDef$KssAPIResult;->NetTimeout:Lcn/kuaipan/kss/KssDef$KssAPIResult;
    :try_end_54
    .catchall {:try_start_52 .. :try_end_54} :catchall_5f

    if-eqz v1, :cond_32

    :try_start_56
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_59
    .catch Ljava/lang/Exception; {:try_start_56 .. :try_end_59} :catch_5a

    goto :goto_32

    :catch_5a
    move-exception v0

    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_32

    .end local v0           #e:Ljava/lang/Exception;
    :catchall_5f
    move-exception v6

    :goto_60
    if-eqz v1, :cond_65

    :try_start_62
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_65
    .catch Ljava/lang/Exception; {:try_start_62 .. :try_end_65} :catch_66

    :cond_65
    :goto_65
    throw v6

    :catch_66
    move-exception v0

    .restart local v0       #e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_65

    .end local v0           #e:Ljava/lang/Exception;
    .end local v1           #in:Ljava/io/BufferedReader;
    .restart local v2       #in:Ljava/io/BufferedReader;
    :catchall_6b
    move-exception v6

    move-object v1, v2

    .end local v2           #in:Ljava/io/BufferedReader;
    .restart local v1       #in:Ljava/io/BufferedReader;
    goto :goto_60

    .end local v1           #in:Ljava/io/BufferedReader;
    .restart local v2       #in:Ljava/io/BufferedReader;
    :catch_6e
    move-exception v0

    move-object v1, v2

    .end local v2           #in:Ljava/io/BufferedReader;
    .restart local v1       #in:Ljava/io/BufferedReader;
    goto :goto_52

    :catch_71
    move-exception v0

    goto :goto_28

    .end local v1           #in:Ljava/io/BufferedReader;
    .restart local v2       #in:Ljava/io/BufferedReader;
    .restart local v3       #line:Ljava/lang/String;
    :cond_73
    move-object v1, v2

    .end local v2           #in:Ljava/io/BufferedReader;
    .restart local v1       #in:Ljava/io/BufferedReader;
    goto :goto_32
.end method

.method private _readChunkData(Ljava/io/InputStream;[B)I
    .registers 6
    .parameter "in"
    .parameter "chunkData"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .local v0, offset:I
    const/4 v1, 0x0

    .local v1, readsize:I
    :goto_2
    iget v2, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->CHUNKSIZE:I

    if-ge v0, v2, :cond_11

    iget v2, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->CHUNKSIZE:I

    sub-int/2addr v2, v0

    invoke-virtual {p1, p2, v0, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    if-ltz v1, :cond_11

    add-int/2addr v0, v1

    goto :goto_2

    :cond_11
    return v0
.end method

.method private _upload(Ljava/io/InputStream;)Lcn/kuaipan/kss/KssDef$KssAPIResult;
    .registers 8
    .parameter "in"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    iget-object v4, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_requestInfo:Lcn/kuaipan/kss/KssUpload$RequestUploadInfo;

    invoke-interface {v4}, Lcn/kuaipan/kss/KssUpload$RequestUploadInfo;->getBlockCount()I

    move-result v0

    .local v0, blockCount:I
    const/4 v1, 0x0

    .local v1, blockIdx:I
    :goto_7
    if-ge v1, v0, :cond_48

    iget-object v4, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_requestInfo:Lcn/kuaipan/kss/KssUpload$RequestUploadInfo;

    invoke-interface {v4, v1}, Lcn/kuaipan/kss/KssUpload$RequestUploadInfo;->getBlockIsExist(I)Z

    move-result v4

    if-eqz v4, :cond_2f

    sget v4, Lcn/kuaipan/kss/KssDef;->BLOCKSIZE:I

    int-to-long v4, v4

    invoke-virtual {p1, v4, v5}, Ljava/io/InputStream;->skip(J)J

    iget-object v4, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_uploadResult:Lcn/kuaipan/kss/implement/_UploadResult;

    iget-object v5, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_requestInfo:Lcn/kuaipan/kss/KssUpload$RequestUploadInfo;

    invoke-interface {v5, v1}, Lcn/kuaipan/kss/KssUpload$RequestUploadInfo;->getBlockMeta(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcn/kuaipan/kss/implement/_UploadResult;->addCommitMeta(Ljava/lang/String;)V

    iget-object v4, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_progress:Lcn/kuaipan/kss/implement/_UploadProgress;

    sget v5, Lcn/kuaipan/kss/KssDef;->BLOCKSIZE:I

    invoke-virtual {v4, v5}, Lcn/kuaipan/kss/implement/_UploadProgress;->TransBytes(I)Z

    move-result v4

    if-nez v4, :cond_45

    sget-object v3, Lcn/kuaipan/kss/KssDef$KssAPIResult;->Cancel:Lcn/kuaipan/kss/KssDef$KssAPIResult;

    :cond_2e
    :goto_2e
    return-object v3

    :cond_2f
    new-instance v2, Lcn/kuaipan/kss/implement/_UploadChunkInfo;

    invoke-direct {v2}, Lcn/kuaipan/kss/implement/_UploadChunkInfo;-><init>()V

    .local v2, chunkInfo:Lcn/kuaipan/kss/implement/_UploadChunkInfo;
    invoke-direct {p0, p1, v1, v2}, Lcn/kuaipan/kss/implement/HttpClientUpload;->_uploadBlock(Ljava/io/InputStream;ILcn/kuaipan/kss/implement/_UploadChunkInfo;)Lcn/kuaipan/kss/KssDef$KssAPIResult;

    move-result-object v3

    .local v3, rt:Lcn/kuaipan/kss/KssDef$KssAPIResult;
    sget-object v4, Lcn/kuaipan/kss/KssDef$KssAPIResult;->OK:Lcn/kuaipan/kss/KssDef$KssAPIResult;

    if-ne v3, v4, :cond_2e

    iget-object v4, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_uploadResult:Lcn/kuaipan/kss/implement/_UploadResult;

    invoke-virtual {v2}, Lcn/kuaipan/kss/implement/_UploadChunkInfo;->getCommitMeta()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcn/kuaipan/kss/implement/_UploadResult;->addCommitMeta(Ljava/lang/String;)V

    .end local v2           #chunkInfo:Lcn/kuaipan/kss/implement/_UploadChunkInfo;
    .end local v3           #rt:Lcn/kuaipan/kss/KssDef$KssAPIResult;
    :cond_45
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    :cond_48
    sget-object v3, Lcn/kuaipan/kss/KssDef$KssAPIResult;->OK:Lcn/kuaipan/kss/KssDef$KssAPIResult;

    goto :goto_2e
.end method

.method private _uploadBlock(Ljava/io/InputStream;ILcn/kuaipan/kss/implement/_UploadChunkInfo;)Lcn/kuaipan/kss/KssDef$KssAPIResult;
    .registers 14
    .parameter "in"
    .parameter "blockIndex"
    .parameter "chunkInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    sget-object v9, Lcn/kuaipan/kss/KssDef$KssAPIResult;->OK:Lcn/kuaipan/kss/KssDef$KssAPIResult;

    .local v9, rt:Lcn/kuaipan/kss/KssDef$KssAPIResult;
    const/4 v6, 0x0

    .local v6, dataPos:I
    :cond_4
    const/4 v3, 0x0

    .local v3, curDataLen:I
    :try_start_5
    iget-object v0, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_chunkDataBuffer:[B

    invoke-direct {p0, p1, v0}, Lcn/kuaipan/kss/implement/HttpClientUpload;->_readChunkData(Ljava/io/InputStream;[B)I
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_a} :catch_e

    move-result v3

    :goto_b
    if-nez v3, :cond_13

    :cond_d
    :goto_d
    return-object v9

    :catch_e
    move-exception v7

    .local v7, e:Ljava/io/IOException;
    sget-object v9, Lcn/kuaipan/kss/KssDef$KssAPIResult;->Error:Lcn/kuaipan/kss/KssDef$KssAPIResult;

    const/4 v3, 0x0

    goto :goto_b

    .end local v7           #e:Ljava/io/IOException;
    :cond_13
    iget-object v0, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_rc4:Lcn/kuaipan/kss/utils/RC4;

    iget-object v1, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_requestInfo:Lcn/kuaipan/kss/KssUpload$RequestUploadInfo;

    invoke-interface {v1}, Lcn/kuaipan/kss/KssUpload$RequestUploadInfo;->getSeureKey()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcn/kuaipan/kss/utils/RC4;->makeKey([B)V

    iget-object v0, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_rc4:Lcn/kuaipan/kss/utils/RC4;

    iget-object v1, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_chunkDataBuffer:[B

    iget-object v4, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_chunkDataBuffer:[B

    move v5, v2

    invoke-virtual/range {v0 .. v5}, Lcn/kuaipan/kss/utils/RC4;->genRC4([BII[BI)V

    const/4 v8, 0x0

    .local v8, proxiesRetry:Z
    :cond_29
    invoke-direct {p0, p2, v6, v3, p3}, Lcn/kuaipan/kss/implement/HttpClientUpload;->_uploadChunk(IIILcn/kuaipan/kss/implement/_UploadChunkInfo;)Lcn/kuaipan/kss/KssDef$KssAPIResult;

    move-result-object v9

    sget-object v0, Lcn/kuaipan/kss/KssDef$KssAPIResult;->NetTimeout:Lcn/kuaipan/kss/KssDef$KssAPIResult;

    if-ne v0, v9, :cond_3e

    iget-object v0, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_proxyHelper:Lcn/kuaipan/kss/implement/HttpClientProxyHelper;

    if-eqz v0, :cond_4a

    iget-object v0, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_proxyHelper:Lcn/kuaipan/kss/implement/HttpClientProxyHelper;

    invoke-virtual {v0}, Lcn/kuaipan/kss/implement/HttpClientProxyHelper;->nextProxy()Z

    move-result v0

    if-eqz v0, :cond_4a

    const/4 v8, 0x1

    :cond_3e
    :goto_3e
    if-nez v8, :cond_29

    sget-object v0, Lcn/kuaipan/kss/KssDef$KssAPIResult;->OK:Lcn/kuaipan/kss/KssDef$KssAPIResult;

    if-ne v0, v9, :cond_d

    add-int/2addr v6, v3

    sget v0, Lcn/kuaipan/kss/KssDef;->BLOCKSIZE:I

    if-lt v6, v0, :cond_4

    goto :goto_d

    :cond_4a
    const/4 v8, 0x0

    goto :goto_3e
.end method

.method private _uploadChunk(IIILcn/kuaipan/kss/implement/_UploadChunkInfo;)Lcn/kuaipan/kss/KssDef$KssAPIResult;
    .registers 15
    .parameter "blockIndex"
    .parameter "dataPos"
    .parameter "curDataLen"
    .parameter "chunkInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    sget-object v4, Lcn/kuaipan/kss/KssDef$KssAPIResult;->OK:Lcn/kuaipan/kss/KssDef$KssAPIResult;

    .local v4, rt:Lcn/kuaipan/kss/KssDef$KssAPIResult;
    const/4 v1, 0x0

    .local v1, post:Lorg/apache/http/client/methods/HttpPost;
    iget-object v7, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_crc32:Ljava/util/zip/CRC32;

    invoke-virtual {v7}, Ljava/util/zip/CRC32;->reset()V

    iget-object v7, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_crc32:Ljava/util/zip/CRC32;

    iget-object v8, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_chunkDataBuffer:[B

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9, p3}, Ljava/util/zip/CRC32;->update([BII)V

    iget-object v7, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_crc32:Ljava/util/zip/CRC32;

    invoke-virtual {v7}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v7

    long-to-int v6, v7

    .local v6, v:I
    :try_start_17
    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7, p2, p1, p4}, Lcn/kuaipan/kss/implement/HttpClientUpload;->_genUploadChunkURI(Ljava/lang/String;IILcn/kuaipan/kss/implement/_UploadChunkInfo;)Ljava/net/URI;

    move-result-object v2

    .local v2, postURI:Ljava/net/URI;
    const/4 v3, 0x0

    .local v3, response:Lorg/apache/http/HttpResponse;
    const/4 v5, 0x0

    .local v5, tryCount:I
    :cond_21
    sget v7, Lcn/kuaipan/kss/KssDef;->NET_RETRY_TIMES:I

    if-ge v5, v7, :cond_35

    iget-object v7, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_chunkDataBuffer:[B

    invoke-direct {p0, v2, v7, p3}, Lcn/kuaipan/kss/implement/HttpClientUpload;->_genUploadChunkPost(Ljava/net/URI;[BI)Lorg/apache/http/client/methods/HttpPost;
    :try_end_2a
    .catchall {:try_start_17 .. :try_end_2a} :catchall_41
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_2a} :catch_3f

    move-result-object v1

    :try_start_2b
    iget-object v7, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_client:Lorg/apache/http/client/HttpClient;

    invoke-interface {v7, v1}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    :try_end_30
    .catchall {:try_start_2b .. :try_end_30} :catchall_41
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_2b .. :try_end_30} :catch_3d
    .catch Ljava/io/IOException; {:try_start_2b .. :try_end_30} :catch_46
    .catch Ljava/lang/Exception; {:try_start_2b .. :try_end_30} :catch_4c

    move-result-object v3

    :goto_31
    add-int/lit8 v5, v5, 0x1

    if-eqz v3, :cond_21

    :cond_35
    if-nez v3, :cond_4e

    :try_start_37
    sget-object v4, Lcn/kuaipan/kss/KssDef$KssAPIResult;->NetTimeout:Lcn/kuaipan/kss/KssDef$KssAPIResult;
    :try_end_39
    .catchall {:try_start_37 .. :try_end_39} :catchall_41
    .catch Ljava/lang/Exception; {:try_start_37 .. :try_end_39} :catch_3f

    :cond_39
    :goto_39
    invoke-virtual {v1}, Lorg/apache/http/client/methods/HttpPost;->abort()V

    return-object v4

    :catch_3d
    move-exception v0

    .local v0, e:Lorg/apache/http/client/ClientProtocolException;
    :try_start_3e
    throw v0
    :try_end_3f
    .catchall {:try_start_3e .. :try_end_3f} :catchall_41
    .catch Ljava/lang/Exception; {:try_start_3e .. :try_end_3f} :catch_3f

    .end local v0           #e:Lorg/apache/http/client/ClientProtocolException;
    .end local v2           #postURI:Ljava/net/URI;
    .end local v3           #response:Lorg/apache/http/HttpResponse;
    .end local v5           #tryCount:I
    :catch_3f
    move-exception v0

    .local v0, e:Ljava/lang/Exception;
    :try_start_40
    throw v0
    :try_end_41
    .catchall {:try_start_40 .. :try_end_41} :catchall_41

    .end local v0           #e:Ljava/lang/Exception;
    :catchall_41
    move-exception v7

    invoke-virtual {v1}, Lorg/apache/http/client/methods/HttpPost;->abort()V

    throw v7

    .restart local v2       #postURI:Ljava/net/URI;
    .restart local v3       #response:Lorg/apache/http/HttpResponse;
    .restart local v5       #tryCount:I
    :catch_46
    move-exception v0

    .local v0, e:Ljava/io/IOException;
    :try_start_47
    invoke-virtual {v1}, Lorg/apache/http/client/methods/HttpPost;->abort()V

    const/4 v3, 0x0

    goto :goto_31

    .end local v0           #e:Ljava/io/IOException;
    :catch_4c
    move-exception v0

    .local v0, e:Ljava/lang/Exception;
    throw v0

    .end local v0           #e:Ljava/lang/Exception;
    :cond_4e
    invoke-direct {p0, v3, p4}, Lcn/kuaipan/kss/implement/HttpClientUpload;->_parseResponse(Lorg/apache/http/HttpResponse;Lcn/kuaipan/kss/implement/_UploadChunkInfo;)Lcn/kuaipan/kss/KssDef$KssAPIResult;

    move-result-object v4

    sget-object v7, Lcn/kuaipan/kss/KssDef$KssAPIResult;->OK:Lcn/kuaipan/kss/KssDef$KssAPIResult;

    if-ne v4, v7, :cond_39

    add-int/2addr p2, p3

    iget-object v7, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_progress:Lcn/kuaipan/kss/implement/_UploadProgress;

    invoke-virtual {v7, p3}, Lcn/kuaipan/kss/implement/_UploadProgress;->TransBytes(I)Z

    move-result v7

    if-nez v7, :cond_39

    sget-object v4, Lcn/kuaipan/kss/KssDef$KssAPIResult;->Cancel:Lcn/kuaipan/kss/KssDef$KssAPIResult;
    :try_end_61
    .catchall {:try_start_47 .. :try_end_61} :catchall_41
    .catch Ljava/lang/Exception; {:try_start_47 .. :try_end_61} :catch_3f

    goto :goto_39
.end method


# virtual methods
.method public getCommitInfo()Lcn/kuaipan/kss/KssUpload$UploadResult;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    iget-object v0, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_uploadResult:Lcn/kuaipan/kss/implement/_UploadResult;

    return-object v0
.end method

.method public init(Lcn/kuaipan/kss/KssUpload$RequestUploadInfo;Lcn/kuaipan/kss/KssDef$OnUpDownload;Lcn/kuaipan/kss/KssDef$NetState;)Z
    .registers 6
    .parameter "requestInfo"
    .parameter "onUpload"
    .parameter "ns"

    .prologue
    if-nez p1, :cond_4

    const/4 v0, 0x0

    :goto_3
    return v0

    :cond_4
    iput-object p1, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_requestInfo:Lcn/kuaipan/kss/KssUpload$RequestUploadInfo;

    iget-object v0, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_progress:Lcn/kuaipan/kss/implement/_UploadProgress;

    invoke-virtual {v0, p1, p2}, Lcn/kuaipan/kss/implement/_UploadProgress;->init(Lcn/kuaipan/kss/KssUpload$RequestUploadInfo;Lcn/kuaipan/kss/KssDef$OnUpDownload;)V

    iget-object v0, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_uploadResult:Lcn/kuaipan/kss/implement/_UploadResult;

    iget-object v1, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_requestInfo:Lcn/kuaipan/kss/KssUpload$RequestUploadInfo;

    invoke-interface {v1}, Lcn/kuaipan/kss/KssUpload$RequestUploadInfo;->getFileMeta()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcn/kuaipan/kss/implement/_UploadResult;->setFileMeta(Ljava/lang/String;)V

    const/4 v0, 0x1

    goto :goto_3
.end method

.method public terminal()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    iput-object v0, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_client:Lorg/apache/http/client/HttpClient;

    iput-object v0, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_requestInfo:Lcn/kuaipan/kss/KssUpload$RequestUploadInfo;

    iput-object v0, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_uploadResult:Lcn/kuaipan/kss/implement/_UploadResult;

    iput-object v0, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_progress:Lcn/kuaipan/kss/implement/_UploadProgress;

    iput-object v0, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_proxyHelper:Lcn/kuaipan/kss/implement/HttpClientProxyHelper;

    iput-object v0, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_crc32:Ljava/util/zip/CRC32;

    iput-object v0, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_rc4:Lcn/kuaipan/kss/utils/RC4;

    iput-object v0, p0, Lcn/kuaipan/kss/implement/HttpClientUpload;->m_chunkDataBuffer:[B

    return-void
.end method

.method public upload(Ljava/io/InputStream;)Lcn/kuaipan/kss/KssDef$KssAPIResult;
    .registers 3
    .parameter "in"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    invoke-direct {p0, p1}, Lcn/kuaipan/kss/implement/HttpClientUpload;->_upload(Ljava/io/InputStream;)Lcn/kuaipan/kss/KssDef$KssAPIResult;

    move-result-object v0

    return-object v0
.end method

.method public upload([B)Lcn/kuaipan/kss/KssDef$KssAPIResult;
    .registers 4
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .local v0, dataIn:Ljava/io/ByteArrayInputStream;
    invoke-direct {p0, v0}, Lcn/kuaipan/kss/implement/HttpClientUpload;->_upload(Ljava/io/InputStream;)Lcn/kuaipan/kss/KssDef$KssAPIResult;

    move-result-object v1

    return-object v1
.end method
