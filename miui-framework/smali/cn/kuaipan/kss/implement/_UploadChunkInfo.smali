.class Lcn/kuaipan/kss/implement/_UploadChunkInfo;
.super Ljava/lang/Object;
.source "HttpClientUpload.java"


# instance fields
.field private m_commitMeta:Ljava/lang/String;

.field private m_stateStr:Ljava/lang/String;

.field private m_uploadID:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcn/kuaipan/kss/implement/_UploadChunkInfo;->m_stateStr:Ljava/lang/String;

    iput-object v0, p0, Lcn/kuaipan/kss/implement/_UploadChunkInfo;->m_uploadID:Ljava/lang/String;

    iput-object v0, p0, Lcn/kuaipan/kss/implement/_UploadChunkInfo;->m_commitMeta:Ljava/lang/String;

    return-void
.end method

.method private needReRequestMetadata(Ljava/lang/String;)Z
    .registers 11
    .parameter "stateStr"

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x4

    new-array v3, v7, [Ljava/lang/String;

    const-string v7, "ERR_INVALID_FILE_META"

    aput-object v7, v3, v6

    const-string v7, "ERR_INVALID_BLOCK_META"

    aput-object v7, v3, v5

    const/4 v7, 0x2

    const-string v8, "ERR_INVALID_UPLOAD_ID"

    aput-object v8, v3, v7

    const/4 v7, 0x3

    const-string v8, "ERR_BLOCK_CORRUPTED"

    aput-object v8, v3, v7

    .local v3, reRequstState:[Ljava/lang/String;
    move-object v0, v3

    .local v0, arr$:[Ljava/lang/String;
    array-length v2, v0

    .local v2, len$:I
    const/4 v1, 0x0

    .local v1, i$:I
    :goto_1a
    if-ge v1, v2, :cond_28

    aget-object v4, v0, v1

    .local v4, str:Ljava/lang/String;
    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_25

    .end local v4           #str:Ljava/lang/String;
    :goto_24
    return v5

    .restart local v4       #str:Ljava/lang/String;
    :cond_25
    add-int/lit8 v1, v1, 0x1

    goto :goto_1a

    .end local v4           #str:Ljava/lang/String;
    :cond_28
    move v5, v6

    goto :goto_24
.end method


# virtual methods
.method public getCommitMeta()Ljava/lang/String;
    .registers 2

    .prologue
    iget-object v0, p0, Lcn/kuaipan/kss/implement/_UploadChunkInfo;->m_commitMeta:Ljava/lang/String;

    return-object v0
.end method

.method public getUploadID()Ljava/lang/String;
    .registers 2

    .prologue
    iget-object v0, p0, Lcn/kuaipan/kss/implement/_UploadChunkInfo;->m_uploadID:Ljava/lang/String;

    return-object v0
.end method

.method public parseInfo(Ljava/lang/String;)Lcn/kuaipan/kss/KssDef$KssAPIResult;
    .registers 7
    .parameter "jsonStr"

    .prologue
    sget-object v2, Lcn/kuaipan/kss/KssDef$KssAPIResult;->OK:Lcn/kuaipan/kss/KssDef$KssAPIResult;

    .local v2, rt:Lcn/kuaipan/kss/KssDef$KssAPIResult;
    :try_start_2
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .local v1, jsonInfo:Lorg/json/JSONObject;
    const-string v3, "stat"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcn/kuaipan/kss/implement/_UploadChunkInfo;->m_stateStr:Ljava/lang/String;

    iget-object v3, p0, Lcn/kuaipan/kss/implement/_UploadChunkInfo;->m_stateStr:Ljava/lang/String;

    const-string v4, "CONTINUE_UPLOAD"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_34

    iget-object v3, p0, Lcn/kuaipan/kss/implement/_UploadChunkInfo;->m_uploadID:Ljava/lang/String;

    if-nez v3, :cond_2d

    const-string v3, "upload_id"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2d

    const-string v3, "upload_id"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcn/kuaipan/kss/implement/_UploadChunkInfo;->m_uploadID:Ljava/lang/String;

    :cond_2d
    iget-object v3, p0, Lcn/kuaipan/kss/implement/_UploadChunkInfo;->m_uploadID:Ljava/lang/String;

    if-nez v3, :cond_33

    sget-object v2, Lcn/kuaipan/kss/KssDef$KssAPIResult;->Error:Lcn/kuaipan/kss/KssDef$KssAPIResult;

    .end local v1           #jsonInfo:Lorg/json/JSONObject;
    :cond_33
    :goto_33
    return-object v2

    .restart local v1       #jsonInfo:Lorg/json/JSONObject;
    :cond_34
    iget-object v3, p0, Lcn/kuaipan/kss/implement/_UploadChunkInfo;->m_stateStr:Ljava/lang/String;

    const-string v4, "BLOCK_COMPLETED"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_56

    const-string v3, "commit_meta"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_33

    const-string v3, "commit_meta"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcn/kuaipan/kss/implement/_UploadChunkInfo;->m_commitMeta:Ljava/lang/String;
    :try_end_4e
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_4e} :catch_4f

    goto :goto_33

    .end local v1           #jsonInfo:Lorg/json/JSONObject;
    :catch_4f
    move-exception v0

    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    sget-object v2, Lcn/kuaipan/kss/KssDef$KssAPIResult;->Error:Lcn/kuaipan/kss/KssDef$KssAPIResult;

    goto :goto_33

    .end local v0           #e:Ljava/lang/Exception;
    .restart local v1       #jsonInfo:Lorg/json/JSONObject;
    :cond_56
    :try_start_56
    iget-object v3, p0, Lcn/kuaipan/kss/implement/_UploadChunkInfo;->m_stateStr:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcn/kuaipan/kss/implement/_UploadChunkInfo;->needReRequestMetadata(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_33

    sget-object v2, Lcn/kuaipan/kss/KssDef$KssAPIResult;->NeedRequest:Lcn/kuaipan/kss/KssDef$KssAPIResult;
    :try_end_60
    .catch Ljava/lang/Exception; {:try_start_56 .. :try_end_60} :catch_4f

    goto :goto_33
.end method

.method public reset()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    iput-object v0, p0, Lcn/kuaipan/kss/implement/_UploadChunkInfo;->m_stateStr:Ljava/lang/String;

    iput-object v0, p0, Lcn/kuaipan/kss/implement/_UploadChunkInfo;->m_uploadID:Ljava/lang/String;

    iput-object v0, p0, Lcn/kuaipan/kss/implement/_UploadChunkInfo;->m_commitMeta:Ljava/lang/String;

    return-void
.end method
