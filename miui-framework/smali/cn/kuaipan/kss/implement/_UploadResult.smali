.class Lcn/kuaipan/kss/implement/_UploadResult;
.super Ljava/lang/Object;
.source "HttpClientUpload.java"

# interfaces
.implements Lcn/kuaipan/kss/KssUpload$UploadResult;


# instance fields
.field private m_Result:Lcn/kuaipan/kss/KssDef$KssAPIResult;

.field private m_commitMetas:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private m_fileMeta:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcn/kuaipan/kss/KssDef$KssAPIResult;->OK:Lcn/kuaipan/kss/KssDef$KssAPIResult;

    iput-object v0, p0, Lcn/kuaipan/kss/implement/_UploadResult;->m_Result:Lcn/kuaipan/kss/KssDef$KssAPIResult;

    const/4 v0, 0x0

    iput-object v0, p0, Lcn/kuaipan/kss/implement/_UploadResult;->m_fileMeta:Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcn/kuaipan/kss/implement/_UploadResult;->m_commitMetas:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public addCommitMeta(Ljava/lang/String;)V
    .registers 3
    .parameter "metaStr"

    .prologue
    iget-object v0, p0, Lcn/kuaipan/kss/implement/_UploadResult;->m_commitMetas:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public getBlockCommitMeta(I)Ljava/lang/String;
    .registers 3
    .parameter "idx"

    .prologue
    if-ltz p1, :cond_a

    iget-object v0, p0, Lcn/kuaipan/kss/implement/_UploadResult;->m_commitMetas:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_c

    :cond_a
    const/4 v0, 0x0

    :goto_b
    return-object v0

    :cond_c
    iget-object v0, p0, Lcn/kuaipan/kss/implement/_UploadResult;->m_commitMetas:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_b
.end method

.method public getBlockCount()I
    .registers 2

    .prologue
    iget-object v0, p0, Lcn/kuaipan/kss/implement/_UploadResult;->m_commitMetas:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getFileMeta()Ljava/lang/String;
    .registers 2

    .prologue
    iget-object v0, p0, Lcn/kuaipan/kss/implement/_UploadResult;->m_fileMeta:Ljava/lang/String;

    return-object v0
.end method

.method public getResult()Lcn/kuaipan/kss/KssDef$KssAPIResult;
    .registers 2

    .prologue
    iget-object v0, p0, Lcn/kuaipan/kss/implement/_UploadResult;->m_Result:Lcn/kuaipan/kss/KssDef$KssAPIResult;

    return-object v0
.end method

.method public setFileMeta(Ljava/lang/String;)V
    .registers 2
    .parameter "fileMeta"

    .prologue
    iput-object p1, p0, Lcn/kuaipan/kss/implement/_UploadResult;->m_fileMeta:Ljava/lang/String;

    return-void
.end method
