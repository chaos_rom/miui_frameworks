.class public Lcn/kuaipan/kss/implement/HttpClientDownload;
.super Ljava/lang/Object;
.source "HttpClientDownload.java"

# interfaces
.implements Lcn/kuaipan/kss/KssDownload;


# instance fields
.field private m_client:Lorg/apache/http/client/HttpClient;

.field private m_currentBlockPos:I

.field private m_dataBuffer:[B

.field private m_downProgress:Lcn/kuaipan/kss/implement/_DownloadProgress;

.field private m_downloadInfo:Lcn/kuaipan/kss/KssDownload$RequestDownloadInfo;

.field private m_maxRetryCount:I

.field private m_netState:Lcn/kuaipan/kss/KssDef$NetState;

.field private m_proxyHelper:Lcn/kuaipan/kss/implement/HttpClientProxyHelper;


# direct methods
.method public constructor <init>(Lorg/apache/http/client/HttpClient;Lcn/kuaipan/kss/implement/HttpClientProxyHelper;)V
    .registers 5
    .parameter "client"
    .parameter "proxyHelper"

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcn/kuaipan/kss/implement/HttpClientDownload;->m_client:Lorg/apache/http/client/HttpClient;

    iput-object v0, p0, Lcn/kuaipan/kss/implement/HttpClientDownload;->m_dataBuffer:[B

    iput-object v0, p0, Lcn/kuaipan/kss/implement/HttpClientDownload;->m_proxyHelper:Lcn/kuaipan/kss/implement/HttpClientProxyHelper;

    iput-object v0, p0, Lcn/kuaipan/kss/implement/HttpClientDownload;->m_downloadInfo:Lcn/kuaipan/kss/KssDownload$RequestDownloadInfo;

    iput-object v0, p0, Lcn/kuaipan/kss/implement/HttpClientDownload;->m_downProgress:Lcn/kuaipan/kss/implement/_DownloadProgress;

    iput v1, p0, Lcn/kuaipan/kss/implement/HttpClientDownload;->m_currentBlockPos:I

    sget-object v0, Lcn/kuaipan/kss/KssDef$NetState;->Wifi:Lcn/kuaipan/kss/KssDef$NetState;

    iput-object v0, p0, Lcn/kuaipan/kss/implement/HttpClientDownload;->m_netState:Lcn/kuaipan/kss/KssDef$NetState;

    iput v1, p0, Lcn/kuaipan/kss/implement/HttpClientDownload;->m_maxRetryCount:I

    iput-object p1, p0, Lcn/kuaipan/kss/implement/HttpClientDownload;->m_client:Lorg/apache/http/client/HttpClient;

    iput-object p2, p0, Lcn/kuaipan/kss/implement/HttpClientDownload;->m_proxyHelper:Lcn/kuaipan/kss/implement/HttpClientProxyHelper;

    new-instance v0, Lcn/kuaipan/kss/implement/_DownloadProgress;

    invoke-direct {v0}, Lcn/kuaipan/kss/implement/_DownloadProgress;-><init>()V

    iput-object v0, p0, Lcn/kuaipan/kss/implement/HttpClientDownload;->m_downProgress:Lcn/kuaipan/kss/implement/_DownloadProgress;

    sget v0, Lcn/kuaipan/kss/KssDef;->DOWNLOAD_BUFFER:I

    new-array v0, v0, [B

    iput-object v0, p0, Lcn/kuaipan/kss/implement/HttpClientDownload;->m_dataBuffer:[B

    return-void
.end method

.method private _downloadBlock(Lcn/kuaipan/kss/KssDownload$DownloadTransControl;Lcn/kuaipan/kss/KssDownload$BlockDownloadInfo;)Lcn/kuaipan/kss/KssDef$KssAPIResult;
    .registers 18
    .parameter "downloadCtr"
    .parameter "blockInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    new-instance v1, Lcn/kuaipan/kss/utils/RC4;

    invoke-direct {v1}, Lcn/kuaipan/kss/utils/RC4;-><init>()V

    .local v1, rc4:Lcn/kuaipan/kss/utils/RC4;
    iget-object v2, p0, Lcn/kuaipan/kss/implement/HttpClientDownload;->m_downloadInfo:Lcn/kuaipan/kss/KssDownload$RequestDownloadInfo;

    invoke-interface {v2}, Lcn/kuaipan/kss/KssDownload$RequestDownloadInfo;->getSecureKey()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcn/kuaipan/kss/utils/Encode;->hexStringToByteArray(Ljava/lang/String;)[B

    move-result-object v11

    .local v11, rc4Key:[B
    sget-object v14, Lcn/kuaipan/kss/KssDef$KssAPIResult;->Error:Lcn/kuaipan/kss/KssDef$KssAPIResult;

    .local v14, rt:Lcn/kuaipan/kss/KssDef$KssAPIResult;
    const/4 v13, 0x0

    .local v13, retryCount:I
    :goto_12
    iget v2, p0, Lcn/kuaipan/kss/implement/HttpClientDownload;->m_maxRetryCount:I

    if-ge v13, v2, :cond_97

    const/4 v8, 0x0

    .local v8, get:Lorg/apache/http/client/methods/HttpGet;
    :try_start_17
    iget-object v2, p0, Lcn/kuaipan/kss/implement/HttpClientDownload;->m_proxyHelper:Lcn/kuaipan/kss/implement/HttpClientProxyHelper;

    if-nez v2, :cond_98

    new-instance v9, Lorg/apache/http/client/methods/HttpGet;

    const/4 v2, 0x0

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Lcn/kuaipan/kss/KssDownload$BlockDownloadInfo;->getBlockDownload_FullURL(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v9, v2}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .end local v8           #get:Lorg/apache/http/client/methods/HttpGet;
    .local v9, get:Lorg/apache/http/client/methods/HttpGet;
    move-object v8, v9

    .end local v9           #get:Lorg/apache/http/client/methods/HttpGet;
    .restart local v8       #get:Lorg/apache/http/client/methods/HttpGet;
    :goto_28
    const-string v2, "Range"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "bytes="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v5, p0, Lcn/kuaipan/kss/implement/HttpClientDownload;->m_currentBlockPos:I

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "-"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8, v2, v3}, Lorg/apache/http/client/methods/HttpGet;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v11}, Lcn/kuaipan/kss/utils/RC4;->makeKey([B)V

    iget-object v2, p0, Lcn/kuaipan/kss/implement/HttpClientDownload;->m_client:Lorg/apache/http/client/HttpClient;

    invoke-interface {v2, v8}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v12

    .local v12, response:Lorg/apache/http/HttpResponse;
    invoke-interface {v12}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v10

    .local v10, in:Ljava/io/InputStream;
    sget-object v14, Lcn/kuaipan/kss/KssDef$KssAPIResult;->OK:Lcn/kuaipan/kss/KssDef$KssAPIResult;

    const/4 v4, 0x0

    .local v4, len:I
    :cond_60
    iget-object v2, p0, Lcn/kuaipan/kss/implement/HttpClientDownload;->m_dataBuffer:[B

    const/4 v3, 0x0

    sget v5, Lcn/kuaipan/kss/KssDef;->DOWNLOAD_BUFFER:I

    invoke-virtual {v10, v2, v3, v5}, Ljava/io/InputStream;->read([BII)I

    move-result v4

    if-ltz v4, :cond_92

    iget-object v2, p0, Lcn/kuaipan/kss/implement/HttpClientDownload;->m_dataBuffer:[B

    const/4 v3, 0x0

    iget-object v5, p0, Lcn/kuaipan/kss/implement/HttpClientDownload;->m_dataBuffer:[B

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Lcn/kuaipan/kss/utils/RC4;->genRC4([BII[BI)V
    :try_end_74
    .catchall {:try_start_17 .. :try_end_74} :catchall_c2
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_74} :catch_ad
    .catch Ljava/lang/RuntimeException; {:try_start_17 .. :try_end_74} :catch_b5
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_74} :catch_bd

    :try_start_74
    iget-object v2, p0, Lcn/kuaipan/kss/implement/HttpClientDownload;->m_dataBuffer:[B

    move-object/from16 v0, p1

    invoke-interface {v0, v2, v4}, Lcn/kuaipan/kss/KssDownload$DownloadTransControl;->addData([BI)I
    :try_end_7b
    .catchall {:try_start_74 .. :try_end_7b} :catchall_c2
    .catch Ljava/io/IOException; {:try_start_74 .. :try_end_7b} :catch_a6
    .catch Ljava/lang/RuntimeException; {:try_start_74 .. :try_end_7b} :catch_b5
    .catch Ljava/lang/Exception; {:try_start_74 .. :try_end_7b} :catch_bd

    :try_start_7b
    iget v2, p0, Lcn/kuaipan/kss/implement/HttpClientDownload;->m_currentBlockPos:I

    add-int/2addr v2, v4

    iput v2, p0, Lcn/kuaipan/kss/implement/HttpClientDownload;->m_currentBlockPos:I

    iget-object v2, p0, Lcn/kuaipan/kss/implement/HttpClientDownload;->m_downProgress:Lcn/kuaipan/kss/implement/_DownloadProgress;

    invoke-virtual {v2, v4}, Lcn/kuaipan/kss/implement/_DownloadProgress;->TransBytes(I)V

    iget-object v2, p0, Lcn/kuaipan/kss/implement/HttpClientDownload;->m_downProgress:Lcn/kuaipan/kss/implement/_DownloadProgress;

    invoke-virtual {v2}, Lcn/kuaipan/kss/implement/_DownloadProgress;->getIsCanceled()Z

    move-result v2

    if-eqz v2, :cond_60

    invoke-virtual {v8}, Lorg/apache/http/client/methods/HttpGet;->abort()V

    sget-object v14, Lcn/kuaipan/kss/KssDef$KssAPIResult;->Cancel:Lcn/kuaipan/kss/KssDef$KssAPIResult;
    :try_end_92
    .catchall {:try_start_7b .. :try_end_92} :catchall_c2
    .catch Ljava/io/IOException; {:try_start_7b .. :try_end_92} :catch_ad
    .catch Ljava/lang/RuntimeException; {:try_start_7b .. :try_end_92} :catch_b5
    .catch Ljava/lang/Exception; {:try_start_7b .. :try_end_92} :catch_bd

    :cond_92
    :goto_92
    const/4 v8, 0x0

    .end local v4           #len:I
    .end local v10           #in:Ljava/io/InputStream;
    .end local v12           #response:Lorg/apache/http/HttpResponse;
    :goto_93
    sget-object v2, Lcn/kuaipan/kss/KssDef$KssAPIResult;->NetTimeout:Lcn/kuaipan/kss/KssDef$KssAPIResult;

    if-eq v14, v2, :cond_c5

    .end local v8           #get:Lorg/apache/http/client/methods/HttpGet;
    :cond_97
    return-object v14

    .restart local v8       #get:Lorg/apache/http/client/methods/HttpGet;
    :cond_98
    :try_start_98
    iget-object v2, p0, Lcn/kuaipan/kss/implement/HttpClientDownload;->m_proxyHelper:Lcn/kuaipan/kss/implement/HttpClientProxyHelper;

    const/4 v3, 0x0

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Lcn/kuaipan/kss/KssDownload$BlockDownloadInfo;->getBlockDownload_FullURL(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcn/kuaipan/kss/implement/HttpClientProxyHelper;->createGet(Ljava/lang/String;)Lorg/apache/http/client/methods/HttpGet;

    move-result-object v8

    goto :goto_28

    .restart local v4       #len:I
    .restart local v10       #in:Ljava/io/InputStream;
    .restart local v12       #response:Lorg/apache/http/HttpResponse;
    :catch_a6
    move-exception v7

    .local v7, e:Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    sget-object v14, Lcn/kuaipan/kss/KssDef$KssAPIResult;->Error:Lcn/kuaipan/kss/KssDef$KssAPIResult;
    :try_end_ac
    .catchall {:try_start_98 .. :try_end_ac} :catchall_c2
    .catch Ljava/io/IOException; {:try_start_98 .. :try_end_ac} :catch_ad
    .catch Ljava/lang/RuntimeException; {:try_start_98 .. :try_end_ac} :catch_b5
    .catch Ljava/lang/Exception; {:try_start_98 .. :try_end_ac} :catch_bd

    goto :goto_92

    .end local v4           #len:I
    .end local v7           #e:Ljava/io/IOException;
    .end local v10           #in:Ljava/io/InputStream;
    .end local v12           #response:Lorg/apache/http/HttpResponse;
    :catch_ad
    move-exception v7

    .restart local v7       #e:Ljava/io/IOException;
    :try_start_ae
    invoke-virtual {v8}, Lorg/apache/http/client/methods/HttpGet;->abort()V

    sget-object v14, Lcn/kuaipan/kss/KssDef$KssAPIResult;->NetTimeout:Lcn/kuaipan/kss/KssDef$KssAPIResult;

    const/4 v8, 0x0

    goto :goto_93

    .end local v7           #e:Ljava/io/IOException;
    :catch_b5
    move-exception v7

    .local v7, e:Ljava/lang/RuntimeException;
    invoke-virtual {v8}, Lorg/apache/http/client/methods/HttpGet;->abort()V

    sget-object v14, Lcn/kuaipan/kss/KssDef$KssAPIResult;->Error:Lcn/kuaipan/kss/KssDef$KssAPIResult;

    const/4 v8, 0x0

    goto :goto_93

    .end local v7           #e:Ljava/lang/RuntimeException;
    :catch_bd
    move-exception v7

    .local v7, e:Ljava/lang/Exception;
    sget-object v14, Lcn/kuaipan/kss/KssDef$KssAPIResult;->Error:Lcn/kuaipan/kss/KssDef$KssAPIResult;
    :try_end_c0
    .catchall {:try_start_ae .. :try_end_c0} :catchall_c2

    const/4 v8, 0x0

    goto :goto_93

    .end local v7           #e:Ljava/lang/Exception;
    :catchall_c2
    move-exception v2

    const/4 v8, 0x0

    throw v2

    :cond_c5
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_12
.end method


# virtual methods
.method public download(Lcn/kuaipan/kss/KssDownload$DownloadTransControl;)Lcn/kuaipan/kss/KssDef$KssAPIResult;
    .registers 9
    .parameter "downloadCtr"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    iget-object v5, p0, Lcn/kuaipan/kss/implement/HttpClientDownload;->m_downProgress:Lcn/kuaipan/kss/implement/_DownloadProgress;

    invoke-virtual {v5, p1}, Lcn/kuaipan/kss/implement/_DownloadProgress;->startTrans(Lcn/kuaipan/kss/KssDownload$DownloadTransControl;)V

    invoke-interface {p1}, Lcn/kuaipan/kss/KssDownload$DownloadTransControl;->getStartPos()I

    move-result v5

    sget v6, Lcn/kuaipan/kss/KssDef;->BLOCKSIZE:I

    div-int v0, v5, v6

    .local v0, blockIdx:I
    invoke-interface {p1}, Lcn/kuaipan/kss/KssDownload$DownloadTransControl;->getStartPos()I

    move-result v5

    sget v6, Lcn/kuaipan/kss/KssDef;->BLOCKSIZE:I

    rem-int/2addr v5, v6

    iput v5, p0, Lcn/kuaipan/kss/implement/HttpClientDownload;->m_currentBlockPos:I

    sget-object v3, Lcn/kuaipan/kss/KssDef$KssAPIResult;->Error:Lcn/kuaipan/kss/KssDef$KssAPIResult;

    .local v3, rt:Lcn/kuaipan/kss/KssDef$KssAPIResult;
    :goto_19
    iget-object v5, p0, Lcn/kuaipan/kss/implement/HttpClientDownload;->m_downloadInfo:Lcn/kuaipan/kss/KssDownload$RequestDownloadInfo;

    invoke-interface {v5}, Lcn/kuaipan/kss/KssDownload$RequestDownloadInfo;->getBlockCount()I

    move-result v5

    if-ge v0, v5, :cond_4d

    iget-object v5, p0, Lcn/kuaipan/kss/implement/HttpClientDownload;->m_downloadInfo:Lcn/kuaipan/kss/KssDownload$RequestDownloadInfo;

    invoke-interface {v5, v0}, Lcn/kuaipan/kss/KssDownload$RequestDownloadInfo;->getBlockInfos(I)Lcn/kuaipan/kss/KssDownload$BlockDownloadInfo;

    move-result-object v1

    .local v1, blockInfo:Lcn/kuaipan/kss/KssDownload$BlockDownloadInfo;
    const/4 v2, 0x1

    .local v2, proxyDo:Z
    :goto_28
    if-eqz v2, :cond_44

    invoke-direct {p0, p1, v1}, Lcn/kuaipan/kss/implement/HttpClientDownload;->_downloadBlock(Lcn/kuaipan/kss/KssDownload$DownloadTransControl;Lcn/kuaipan/kss/KssDownload$BlockDownloadInfo;)Lcn/kuaipan/kss/KssDef$KssAPIResult;

    move-result-object v3

    sget-object v5, Lcn/kuaipan/kss/KssDef$KssAPIResult;->NetTimeout:Lcn/kuaipan/kss/KssDef$KssAPIResult;

    if-ne v3, v5, :cond_42

    iget-object v5, p0, Lcn/kuaipan/kss/implement/HttpClientDownload;->m_proxyHelper:Lcn/kuaipan/kss/implement/HttpClientProxyHelper;

    if-eqz v5, :cond_40

    iget-object v5, p0, Lcn/kuaipan/kss/implement/HttpClientDownload;->m_proxyHelper:Lcn/kuaipan/kss/implement/HttpClientProxyHelper;

    invoke-virtual {v5}, Lcn/kuaipan/kss/implement/HttpClientProxyHelper;->nextProxy()Z

    move-result v5

    if-eqz v5, :cond_40

    const/4 v2, 0x1

    :goto_3f
    goto :goto_28

    :cond_40
    move v2, v4

    goto :goto_3f

    :cond_42
    const/4 v2, 0x0

    goto :goto_28

    :cond_44
    sget-object v5, Lcn/kuaipan/kss/KssDef$KssAPIResult;->OK:Lcn/kuaipan/kss/KssDef$KssAPIResult;

    if-ne v3, v5, :cond_4d

    iput v4, p0, Lcn/kuaipan/kss/implement/HttpClientDownload;->m_currentBlockPos:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_19

    .end local v1           #blockInfo:Lcn/kuaipan/kss/KssDownload$BlockDownloadInfo;
    .end local v2           #proxyDo:Z
    :cond_4d
    sget-object v4, Lcn/kuaipan/kss/KssDef$KssAPIResult;->OK:Lcn/kuaipan/kss/KssDef$KssAPIResult;

    if-ne v3, v4, :cond_57

    sget-object v4, Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;->Completed:Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;

    invoke-interface {p1, v4}, Lcn/kuaipan/kss/KssDownload$DownloadTransControl;->setEndState(Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;)V

    :goto_56
    return-object v3

    :cond_57
    sget-object v4, Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;->Interrupt:Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;

    invoke-interface {p1, v4}, Lcn/kuaipan/kss/KssDownload$DownloadTransControl;->setEndState(Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;)V

    goto :goto_56
.end method

.method public download(Ljava/io/OutputStream;I)Lcn/kuaipan/kss/KssDef$KssAPIResult;
    .registers 5
    .parameter "outStream"
    .parameter "startPos"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    if-gez p2, :cond_3

    const/4 p2, 0x0

    :cond_3
    new-instance v0, Lcn/kuaipan/kss/implement/KPDownloadTransControl;

    invoke-direct {v0}, Lcn/kuaipan/kss/implement/KPDownloadTransControl;-><init>()V

    .local v0, transFile:Lcn/kuaipan/kss/implement/KPDownloadTransControl;
    invoke-virtual {v0, p1}, Lcn/kuaipan/kss/implement/KPDownloadTransControl;->init(Ljava/io/OutputStream;)V

    invoke-virtual {v0, p2}, Lcn/kuaipan/kss/implement/KPDownloadTransControl;->setStartPos(I)I

    invoke-virtual {p0, v0}, Lcn/kuaipan/kss/implement/HttpClientDownload;->download(Lcn/kuaipan/kss/KssDownload$DownloadTransControl;)Lcn/kuaipan/kss/KssDef$KssAPIResult;

    move-result-object v1

    return-object v1
.end method

.method public init(Lcn/kuaipan/kss/KssDownload$RequestDownloadInfo;Lcn/kuaipan/kss/KssDef$OnUpDownload;Lcn/kuaipan/kss/KssDef$NetState;)Z
    .registers 6
    .parameter "reqDownInfo"
    .parameter "onDownload"
    .parameter "ns"

    .prologue
    iput-object p1, p0, Lcn/kuaipan/kss/implement/HttpClientDownload;->m_downloadInfo:Lcn/kuaipan/kss/KssDownload$RequestDownloadInfo;

    iget-object v0, p0, Lcn/kuaipan/kss/implement/HttpClientDownload;->m_downProgress:Lcn/kuaipan/kss/implement/_DownloadProgress;

    invoke-virtual {v0, p1, p2}, Lcn/kuaipan/kss/implement/_DownloadProgress;->init(Lcn/kuaipan/kss/KssDownload$RequestDownloadInfo;Lcn/kuaipan/kss/KssDef$OnUpDownload;)V

    iput-object p3, p0, Lcn/kuaipan/kss/implement/HttpClientDownload;->m_netState:Lcn/kuaipan/kss/KssDef$NetState;

    sget v0, Lcn/kuaipan/kss/KssDef;->NET_RETRY_TIMES:I

    iput v0, p0, Lcn/kuaipan/kss/implement/HttpClientDownload;->m_maxRetryCount:I

    iget-object v0, p0, Lcn/kuaipan/kss/implement/HttpClientDownload;->m_netState:Lcn/kuaipan/kss/KssDef$NetState;

    sget-object v1, Lcn/kuaipan/kss/KssDef$NetState;->Wifi:Lcn/kuaipan/kss/KssDef$NetState;

    if-eq v0, v1, :cond_19

    sget v0, Lcn/kuaipan/kss/KssDef;->NET_RETRY_TIMES:I

    mul-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcn/kuaipan/kss/implement/HttpClientDownload;->m_maxRetryCount:I

    :cond_19
    const/4 v0, 0x1

    return v0
.end method

.method public terminal()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    iput-object v0, p0, Lcn/kuaipan/kss/implement/HttpClientDownload;->m_client:Lorg/apache/http/client/HttpClient;

    iput-object v0, p0, Lcn/kuaipan/kss/implement/HttpClientDownload;->m_dataBuffer:[B

    iput-object v0, p0, Lcn/kuaipan/kss/implement/HttpClientDownload;->m_proxyHelper:Lcn/kuaipan/kss/implement/HttpClientProxyHelper;

    iput-object v0, p0, Lcn/kuaipan/kss/implement/HttpClientDownload;->m_downloadInfo:Lcn/kuaipan/kss/KssDownload$RequestDownloadInfo;

    iput-object v0, p0, Lcn/kuaipan/kss/implement/HttpClientDownload;->m_downProgress:Lcn/kuaipan/kss/implement/_DownloadProgress;

    return-void
.end method
