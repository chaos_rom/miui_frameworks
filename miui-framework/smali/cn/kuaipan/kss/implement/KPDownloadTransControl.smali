.class public Lcn/kuaipan/kss/implement/KPDownloadTransControl;
.super Ljava/lang/Object;
.source "KPDownloadTransControl.java"

# interfaces
.implements Lcn/kuaipan/kss/KssDownload$DownloadTransControl;


# instance fields
.field private m_curPos:I

.field private m_endState:Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;

.field private m_outputStream:Ljava/io/OutputStream;

.field private m_startPos:I


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcn/kuaipan/kss/implement/KPDownloadTransControl;->m_curPos:I

    iput v0, p0, Lcn/kuaipan/kss/implement/KPDownloadTransControl;->m_startPos:I

    sget-object v0, Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;->Transing:Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;

    iput-object v0, p0, Lcn/kuaipan/kss/implement/KPDownloadTransControl;->m_endState:Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;

    const/4 v0, 0x0

    iput-object v0, p0, Lcn/kuaipan/kss/implement/KPDownloadTransControl;->m_outputStream:Ljava/io/OutputStream;

    return-void
.end method

.method private _init()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    iput v0, p0, Lcn/kuaipan/kss/implement/KPDownloadTransControl;->m_curPos:I

    iput v0, p0, Lcn/kuaipan/kss/implement/KPDownloadTransControl;->m_startPos:I

    sget-object v0, Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;->Transing:Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;

    iput-object v0, p0, Lcn/kuaipan/kss/implement/KPDownloadTransControl;->m_endState:Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;

    const/4 v0, 0x0

    iput-object v0, p0, Lcn/kuaipan/kss/implement/KPDownloadTransControl;->m_outputStream:Ljava/io/OutputStream;

    return-void
.end method


# virtual methods
.method public addData([BI)I
    .registers 5
    .parameter "data"
    .parameter "len"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    iget-object v0, p0, Lcn/kuaipan/kss/implement/KPDownloadTransControl;->m_outputStream:Ljava/io/OutputStream;

    if-nez v0, :cond_a

    new-instance v0, Ljava/io/FileNotFoundException;

    invoke-direct {v0}, Ljava/io/FileNotFoundException;-><init>()V

    throw v0

    :cond_a
    iget-object v0, p0, Lcn/kuaipan/kss/implement/KPDownloadTransControl;->m_outputStream:Ljava/io/OutputStream;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, p2}, Ljava/io/OutputStream;->write([BII)V

    iget v0, p0, Lcn/kuaipan/kss/implement/KPDownloadTransControl;->m_curPos:I

    add-int/2addr v0, p2

    iput v0, p0, Lcn/kuaipan/kss/implement/KPDownloadTransControl;->m_curPos:I

    return p2
.end method

.method public close()V
    .registers 2

    .prologue
    iget-object v0, p0, Lcn/kuaipan/kss/implement/KPDownloadTransControl;->m_outputStream:Ljava/io/OutputStream;

    if-eqz v0, :cond_e

    :try_start_4
    iget-object v0, p0, Lcn/kuaipan/kss/implement/KPDownloadTransControl;->m_outputStream:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    iget-object v0, p0, Lcn/kuaipan/kss/implement/KPDownloadTransControl;->m_outputStream:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_e} :catch_f

    :cond_e
    :goto_e
    return-void

    :catch_f
    move-exception v0

    goto :goto_e
.end method

.method public getCurrentPos()I
    .registers 2

    .prologue
    iget v0, p0, Lcn/kuaipan/kss/implement/KPDownloadTransControl;->m_curPos:I

    return v0
.end method

.method public getEndState()Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;
    .registers 2

    .prologue
    iget-object v0, p0, Lcn/kuaipan/kss/implement/KPDownloadTransControl;->m_endState:Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;

    return-object v0
.end method

.method public getStartPos()I
    .registers 2

    .prologue
    iget v0, p0, Lcn/kuaipan/kss/implement/KPDownloadTransControl;->m_startPos:I

    return v0
.end method

.method public init(Ljava/io/File;Z)V
    .registers 5
    .parameter "fileName"
    .parameter "append"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    invoke-direct {p0}, Lcn/kuaipan/kss/implement/KPDownloadTransControl;->_init()V

    if-nez p2, :cond_e

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    :cond_e
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, p1, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    iput-object v0, p0, Lcn/kuaipan/kss/implement/KPDownloadTransControl;->m_outputStream:Ljava/io/OutputStream;

    if-eqz p2, :cond_1f

    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v0

    long-to-int v0, v0

    invoke-virtual {p0, v0}, Lcn/kuaipan/kss/implement/KPDownloadTransControl;->setStartPos(I)I

    :cond_1f
    return-void
.end method

.method public init(Ljava/io/OutputStream;)V
    .registers 2
    .parameter "outputStream"

    .prologue
    invoke-direct {p0}, Lcn/kuaipan/kss/implement/KPDownloadTransControl;->_init()V

    iput-object p1, p0, Lcn/kuaipan/kss/implement/KPDownloadTransControl;->m_outputStream:Ljava/io/OutputStream;

    return-void
.end method

.method public setEndState(Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;)V
    .registers 2
    .parameter "es"

    .prologue
    iput-object p1, p0, Lcn/kuaipan/kss/implement/KPDownloadTransControl;->m_endState:Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;

    return-void
.end method

.method public setStartPos(I)I
    .registers 3
    .parameter "startPos"

    .prologue
    if-gez p1, :cond_3

    .end local p1
    :goto_2
    return p1

    .restart local p1
    :cond_3
    iget v0, p0, Lcn/kuaipan/kss/implement/KPDownloadTransControl;->m_curPos:I

    .local v0, temp:I
    iput p1, p0, Lcn/kuaipan/kss/implement/KPDownloadTransControl;->m_startPos:I

    iput p1, p0, Lcn/kuaipan/kss/implement/KPDownloadTransControl;->m_curPos:I

    move p1, v0

    goto :goto_2
.end method

.method public terminel()V
    .registers 2

    .prologue
    invoke-virtual {p0}, Lcn/kuaipan/kss/implement/KPDownloadTransControl;->close()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcn/kuaipan/kss/implement/KPDownloadTransControl;->m_outputStream:Ljava/io/OutputStream;

    return-void
.end method
