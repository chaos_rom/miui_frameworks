.class public Lcn/kuaipan/kss/utils/KssUtility;
.super Ljava/lang/Object;
.source "KssUtility.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static closeBufferedReader(Ljava/io/BufferedReader;)V
    .registers 2
    .parameter "reader"

    .prologue
    if-eqz p0, :cond_5

    :try_start_2
    invoke-virtual {p0}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_5} :catch_6

    :cond_5
    :goto_5
    return-void

    :catch_6
    move-exception v0

    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_5
.end method

.method public static closeInputStream(Ljava/io/InputStream;)V
    .registers 2
    .parameter "in"

    .prologue
    if-eqz p0, :cond_5

    :try_start_2
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_5} :catch_6

    :cond_5
    :goto_5
    return-void

    :catch_6
    move-exception v0

    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_5
.end method

.method public static getResponseStringBody(Lorg/apache/http/HttpResponse;)Ljava/lang/String;
    .registers 8
    .parameter "response"

    .prologue
    const/4 v1, 0x0

    .local v1, in:Ljava/io/BufferedReader;
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .local v4, sb:Ljava/lang/StringBuffer;
    :try_start_6
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v5, Ljava/io/InputStreamReader;

    invoke-interface {p0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v6

    invoke-interface {v6}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_18
    .catchall {:try_start_6 .. :try_end_18} :catchall_38
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_18} :catch_41

    .end local v1           #in:Ljava/io/BufferedReader;
    .local v2, in:Ljava/io/BufferedReader;
    :try_start_18
    const-string v3, ""

    .local v3, line:Ljava/lang/String;
    :goto_1a
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2f

    invoke-virtual {v4, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_23
    .catchall {:try_start_18 .. :try_end_23} :catchall_3e
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_23} :catch_24

    goto :goto_1a

    .end local v3           #line:Ljava/lang/String;
    :catch_24
    move-exception v0

    move-object v1, v2

    .end local v2           #in:Ljava/io/BufferedReader;
    .local v0, e:Ljava/lang/Exception;
    .restart local v1       #in:Ljava/io/BufferedReader;
    :goto_26
    :try_start_26
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_29
    .catchall {:try_start_26 .. :try_end_29} :catchall_38

    const/4 v5, 0x0

    invoke-static {v1}, Lcn/kuaipan/kss/utils/KssUtility;->closeBufferedReader(Ljava/io/BufferedReader;)V

    const/4 v1, 0x0

    .end local v0           #e:Ljava/lang/Exception;
    :goto_2e
    return-object v5

    .end local v1           #in:Ljava/io/BufferedReader;
    .restart local v2       #in:Ljava/io/BufferedReader;
    .restart local v3       #line:Ljava/lang/String;
    :cond_2f
    invoke-static {v2}, Lcn/kuaipan/kss/utils/KssUtility;->closeBufferedReader(Ljava/io/BufferedReader;)V

    const/4 v1, 0x0

    .end local v2           #in:Ljava/io/BufferedReader;
    .restart local v1       #in:Ljava/io/BufferedReader;
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_2e

    .end local v3           #line:Ljava/lang/String;
    :catchall_38
    move-exception v5

    :goto_39
    invoke-static {v1}, Lcn/kuaipan/kss/utils/KssUtility;->closeBufferedReader(Ljava/io/BufferedReader;)V

    const/4 v1, 0x0

    throw v5

    .end local v1           #in:Ljava/io/BufferedReader;
    .restart local v2       #in:Ljava/io/BufferedReader;
    :catchall_3e
    move-exception v5

    move-object v1, v2

    .end local v2           #in:Ljava/io/BufferedReader;
    .restart local v1       #in:Ljava/io/BufferedReader;
    goto :goto_39

    :catch_41
    move-exception v0

    goto :goto_26
.end method
