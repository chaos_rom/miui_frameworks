.class public Lcn/kuaipan/kss/utils/Encode;
.super Ljava/lang/Object;
.source "Encode.java"


# static fields
.field static final HEXDIGITS:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    const/16 v0, 0x10

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "0"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "1"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "2"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "3"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "4"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "5"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "6"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "7"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "8"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "9"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "a"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "b"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "c"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "d"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "e"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "f"

    aput-object v2, v0, v1

    sput-object v0, Lcn/kuaipan/kss/utils/Encode;->HEXDIGITS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static MD5Encode([B)Ljava/lang/String;
    .registers 5
    .parameter "oriData"

    .prologue
    const/4 v2, 0x0

    .local v2, md5:Ljava/lang/String;
    :try_start_1
    const-string v3, "MD5"

    invoke-static {v3}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    .local v1, md:Ljava/security/MessageDigest;
    invoke-virtual {v1, p0}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v3

    invoke-static {v3}, Lcn/kuaipan/kss/utils/Encode;->byteArrayToHexString([B)Ljava/lang/String;
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_e} :catch_11

    move-result-object v2

    move-object v3, v2

    .end local v1           #md:Ljava/security/MessageDigest;
    :goto_10
    return-object v3

    :catch_11
    move-exception v0

    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v3, 0x0

    goto :goto_10
.end method

.method public static SHA1Encode([B)Ljava/lang/String;
    .registers 5
    .parameter "oriData"

    .prologue
    const/4 v2, 0x0

    .local v2, sha1:Ljava/lang/String;
    :try_start_1
    const-string v3, "sha1"

    invoke-static {v3}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    .local v1, md:Ljava/security/MessageDigest;
    invoke-virtual {v1, p0}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v3

    invoke-static {v3}, Lcn/kuaipan/kss/utils/Encode;->byteArrayToHexString([B)Ljava/lang/String;
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_e} :catch_11

    move-result-object v2

    move-object v3, v2

    .end local v1           #md:Ljava/security/MessageDigest;
    :goto_10
    return-object v3

    :catch_11
    move-exception v0

    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v3, 0x0

    goto :goto_10
.end method

.method public static byteArrayToHexString([B)Ljava/lang/String;
    .registers 5
    .parameter "b"

    .prologue
    if-nez p0, :cond_4

    const/4 v2, 0x0

    :goto_3
    return-object v2

    :cond_4
    new-instance v1, Ljava/lang/StringBuffer;

    array-length v2, p0

    mul-int/lit8 v2, v2, 0x2

    invoke-direct {v1, v2}, Ljava/lang/StringBuffer;-><init>(I)V

    .local v1, resultSb:Ljava/lang/StringBuffer;
    const/4 v0, 0x0

    .local v0, i:I
    :goto_d
    array-length v2, p0

    if-ge v0, v2, :cond_2b

    sget-object v2, Lcn/kuaipan/kss/utils/Encode;->HEXDIGITS:[Ljava/lang/String;

    aget-byte v3, p0, v0

    ushr-int/lit8 v3, v3, 0x4

    and-int/lit8 v3, v3, 0xf

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    sget-object v2, Lcn/kuaipan/kss/utils/Encode;->HEXDIGITS:[Ljava/lang/String;

    aget-byte v3, p0, v0

    and-int/lit8 v3, v3, 0xf

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    :cond_2b
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_3
.end method

.method public static byteArrayToInt([BI)I
    .registers 5
    .parameter "arr"
    .parameter "startIdx"

    .prologue
    aget-byte v1, p0, p1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x18

    add-int/lit8 v2, p1, 0x1

    aget-byte v2, p0, v2

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x10

    or-int/2addr v1, v2

    add-int/lit8 v2, p1, 0x2

    aget-byte v2, p0, v2

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x8

    or-int/2addr v1, v2

    add-int/lit8 v2, p1, 0x3

    aget-byte v2, p0, v2

    and-int/lit16 v2, v2, 0xff

    or-int v0, v1, v2

    .local v0, r:I
    return v0
.end method

.method public static byteArrayToLong([BI)J
    .registers 8
    .parameter "arr"
    .parameter "startIdx"

    .prologue
    add-int/lit8 v0, p1, 0x8

    .local v0, endIdx:I
    aget-byte v4, p0, p1

    int-to-long v2, v4

    .local v2, r:J
    add-int/lit8 v1, p1, 0x1

    .local v1, i:I
    :goto_7
    if-ge v1, v0, :cond_15

    const/16 v4, 0x8

    shl-long/2addr v2, v4

    aget-byte v4, p0, v1

    and-int/lit16 v4, v4, 0xff

    int-to-long v4, v4

    or-long/2addr v2, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    :cond_15
    return-wide v2
.end method

.method public static byteArrayToShort([BI)S
    .registers 5
    .parameter "arr"
    .parameter "startIdx"

    .prologue
    aget-byte v1, p0, p1

    shl-int/lit8 v1, v1, 0x8

    add-int/lit8 v2, p1, 0x1

    aget-byte v2, p0, v2

    and-int/lit16 v2, v2, 0xff

    or-int/2addr v1, v2

    int-to-short v0, v1

    .local v0, r:S
    return v0
.end method

.method public static byteToHexString(B)Ljava/lang/String;
    .registers 4
    .parameter "b"

    .prologue
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcn/kuaipan/kss/utils/Encode;->HEXDIGITS:[Ljava/lang/String;

    ushr-int/lit8 v2, p0, 0x4

    and-int/lit8 v2, v2, 0xf

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcn/kuaipan/kss/utils/Encode;->HEXDIGITS:[Ljava/lang/String;

    and-int/lit8 v2, p0, 0xf

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static hexStringToByteArray(Ljava/lang/String;)[B
    .registers 6
    .parameter "string"

    .prologue
    const/16 v4, 0x10

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    new-array v0, v2, [B

    .local v0, bytes:[B
    const/4 v1, 0x0

    .local v1, i:I
    :goto_b
    array-length v2, v0

    if-ge v1, v2, :cond_2d

    mul-int/lit8 v2, v1, 0x2

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2, v4}, Ljava/lang/Character;->digit(CI)I

    move-result v2

    mul-int/lit8 v2, v2, 0x10

    mul-int/lit8 v3, v1, 0x2

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3, v4}, Ljava/lang/Character;->digit(CI)I

    move-result v3

    add-int/2addr v2, v3

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_b

    :cond_2d
    return-object v0
.end method

.method public static intToHexString(I)Ljava/lang/String;
    .registers 4
    .parameter "num"

    .prologue
    const/4 v1, 0x4

    new-array v0, v1, [B

    .local v0, arr:[B
    const/4 v1, 0x0

    shr-int/lit8 v2, p0, 0x18

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/4 v1, 0x1

    shr-int/lit8 v2, p0, 0x10

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/4 v1, 0x2

    shr-int/lit8 v2, p0, 0x8

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/4 v1, 0x3

    and-int/lit16 v2, p0, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    invoke-static {v0}, Lcn/kuaipan/kss/utils/Encode;->byteArrayToHexString([B)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static longToHexString(J)Ljava/lang/String;
    .registers 9
    .parameter "num"

    .prologue
    const/16 v6, 0x8

    new-array v0, v6, [B

    .local v0, arr:[B
    const/4 v1, 0x0

    .local v1, i:I
    :goto_5
    if-ge v1, v6, :cond_17

    rsub-int/lit8 v2, v1, 0x7

    mul-int/lit8 v2, v2, 0x8

    shr-long v2, p0, v2

    const-wide/16 v4, 0xff

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_17
    invoke-static {v0}, Lcn/kuaipan/kss/utils/Encode;->byteArrayToHexString([B)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
