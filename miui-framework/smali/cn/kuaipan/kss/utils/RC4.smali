.class public Lcn/kuaipan/kss/utils/RC4;
.super Ljava/lang/Object;
.source "RC4.java"


# instance fields
.field private final sBox:[I

.field private x:I

.field private y:I


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x100

    new-array v0, v0, [I

    iput-object v0, p0, Lcn/kuaipan/kss/utils/RC4;->sBox:[I

    return-void
.end method


# virtual methods
.method public genRC4([BII[BI)V
    .registers 15
    .parameter "in"
    .parameter "inOffset"
    .parameter "inLen"
    .parameter "out"
    .parameter "outOffset"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .local v4, xorIndex:I
    const/4 v3, 0x0

    .local v3, t:I
    const/4 v0, 0x0

    .local v0, i:I
    move v2, p5

    .end local p5
    .local v2, outOffset:I
    move v1, p2

    .end local p2
    .local v1, inOffset:I
    :goto_5
    if-ge v0, p3, :cond_56

    iget v5, p0, Lcn/kuaipan/kss/utils/RC4;->x:I

    add-int/lit8 v5, v5, 0x1

    and-int/lit16 v5, v5, 0xff

    iput v5, p0, Lcn/kuaipan/kss/utils/RC4;->x:I

    iget-object v5, p0, Lcn/kuaipan/kss/utils/RC4;->sBox:[I

    iget v6, p0, Lcn/kuaipan/kss/utils/RC4;->x:I

    aget v5, v5, v6

    iget v6, p0, Lcn/kuaipan/kss/utils/RC4;->y:I

    add-int/2addr v5, v6

    and-int/lit16 v5, v5, 0xff

    iput v5, p0, Lcn/kuaipan/kss/utils/RC4;->y:I

    iget-object v5, p0, Lcn/kuaipan/kss/utils/RC4;->sBox:[I

    iget v6, p0, Lcn/kuaipan/kss/utils/RC4;->x:I

    aget v3, v5, v6

    iget-object v5, p0, Lcn/kuaipan/kss/utils/RC4;->sBox:[I

    iget v6, p0, Lcn/kuaipan/kss/utils/RC4;->x:I

    iget-object v7, p0, Lcn/kuaipan/kss/utils/RC4;->sBox:[I

    iget v8, p0, Lcn/kuaipan/kss/utils/RC4;->y:I

    aget v7, v7, v8

    aput v7, v5, v6

    iget-object v5, p0, Lcn/kuaipan/kss/utils/RC4;->sBox:[I

    iget v6, p0, Lcn/kuaipan/kss/utils/RC4;->y:I

    aput v3, v5, v6

    iget-object v5, p0, Lcn/kuaipan/kss/utils/RC4;->sBox:[I

    iget v6, p0, Lcn/kuaipan/kss/utils/RC4;->x:I

    aget v5, v5, v6

    iget-object v6, p0, Lcn/kuaipan/kss/utils/RC4;->sBox:[I

    iget v7, p0, Lcn/kuaipan/kss/utils/RC4;->y:I

    aget v6, v6, v7

    add-int/2addr v5, v6

    and-int/lit16 v4, v5, 0xff

    add-int/lit8 p5, v2, 0x1

    .end local v2           #outOffset:I
    .restart local p5
    add-int/lit8 p2, v1, 0x1

    .end local v1           #inOffset:I
    .restart local p2
    aget-byte v5, p1, v1

    iget-object v6, p0, Lcn/kuaipan/kss/utils/RC4;->sBox:[I

    aget v6, v6, v4

    xor-int/2addr v5, v6

    int-to-byte v5, v5

    aput-byte v5, p4, v2

    add-int/lit8 v0, v0, 0x1

    move v2, p5

    .end local p5
    .restart local v2       #outOffset:I
    move v1, p2

    .end local p2
    .restart local v1       #inOffset:I
    goto :goto_5

    :cond_56
    return-void
.end method

.method public makeKey([B)V
    .registers 11
    .parameter "key"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;
        }
    .end annotation

    .prologue
    const/16 v8, 0x100

    const/4 v6, 0x0

    move-object v5, p1

    .local v5, userkey:[B
    if-nez v5, :cond_e

    new-instance v6, Ljava/security/InvalidKeyException;

    const-string v7, "Null user key"

    invoke-direct {v6, v7}, Ljava/security/InvalidKeyException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_e
    array-length v3, v5

    .local v3, len:I
    if-nez v3, :cond_19

    new-instance v6, Ljava/security/InvalidKeyException;

    const-string v7, "Invalid user key length"

    invoke-direct {v6, v7}, Ljava/security/InvalidKeyException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_19
    iput v6, p0, Lcn/kuaipan/kss/utils/RC4;->x:I

    iput v6, p0, Lcn/kuaipan/kss/utils/RC4;->y:I

    const/4 v0, 0x0

    .local v0, i:I
    :goto_1e
    if-ge v0, v8, :cond_27

    iget-object v6, p0, Lcn/kuaipan/kss/utils/RC4;->sBox:[I

    aput v0, v6, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1e

    :cond_27
    const/4 v1, 0x0

    .local v1, i1:I
    const/4 v2, 0x0

    .local v2, i2:I
    const/4 v4, 0x0

    .local v4, t:I
    const/4 v0, 0x0

    :goto_2b
    if-ge v0, v8, :cond_50

    aget-byte v6, v5, v1

    and-int/lit16 v6, v6, 0xff

    iget-object v7, p0, Lcn/kuaipan/kss/utils/RC4;->sBox:[I

    aget v7, v7, v0

    add-int/2addr v6, v7

    add-int/2addr v6, v2

    and-int/lit16 v2, v6, 0xff

    iget-object v6, p0, Lcn/kuaipan/kss/utils/RC4;->sBox:[I

    aget v4, v6, v0

    iget-object v6, p0, Lcn/kuaipan/kss/utils/RC4;->sBox:[I

    iget-object v7, p0, Lcn/kuaipan/kss/utils/RC4;->sBox:[I

    aget v7, v7, v2

    aput v7, v6, v0

    iget-object v6, p0, Lcn/kuaipan/kss/utils/RC4;->sBox:[I

    aput v4, v6, v2

    add-int/lit8 v6, v1, 0x1

    rem-int v1, v6, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_2b

    :cond_50
    return-void
.end method
