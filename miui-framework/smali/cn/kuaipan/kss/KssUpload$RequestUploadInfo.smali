.class public interface abstract Lcn/kuaipan/kss/KssUpload$RequestUploadInfo;
.super Ljava/lang/Object;
.source "KssUpload.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcn/kuaipan/kss/KssUpload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "RequestUploadInfo"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcn/kuaipan/kss/KssUpload$RequestUploadInfo$RequestUploadState;
    }
.end annotation


# virtual methods
.method public abstract getBlockCount()I
.end method

.method public abstract getBlockIsExist(I)Z
.end method

.method public abstract getBlockMeta(I)Ljava/lang/String;
.end method

.method public abstract getFileMeta()Ljava/lang/String;
.end method

.method public abstract getFileSize()I
.end method

.method public abstract getFileStoreID()Ljava/lang/String;
.end method

.method public abstract getNodeIP(I)Ljava/lang/String;
.end method

.method public abstract getNodeIPCount()I
.end method

.method public abstract getProtocol()Ljava/lang/String;
.end method

.method public abstract getRequestUploadState()Lcn/kuaipan/kss/KssUpload$RequestUploadInfo$RequestUploadState;
.end method

.method public abstract getResult()Lcn/kuaipan/kss/KssDef$KssAPIResult;
.end method

.method public abstract getSeureKey()[B
.end method
