.class public final enum Lcn/kuaipan/kss/KssDef$KssAPIResult;
.super Ljava/lang/Enum;
.source "KssDef.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcn/kuaipan/kss/KssDef;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "KssAPIResult"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcn/kuaipan/kss/KssDef$KssAPIResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcn/kuaipan/kss/KssDef$KssAPIResult;

.field public static final enum Cancel:Lcn/kuaipan/kss/KssDef$KssAPIResult;

.field public static final enum DataCorrupted:Lcn/kuaipan/kss/KssDef$KssAPIResult;

.field public static final enum Error:Lcn/kuaipan/kss/KssDef$KssAPIResult;

.field public static final enum NeedRequest:Lcn/kuaipan/kss/KssDef$KssAPIResult;

.field public static final enum NetTimeout:Lcn/kuaipan/kss/KssDef$KssAPIResult;

.field public static final enum OK:Lcn/kuaipan/kss/KssDef$KssAPIResult;

.field public static final enum ServerDenyReadOnly:Lcn/kuaipan/kss/KssDef$KssAPIResult;

.field public static final enum SpaceOver:Lcn/kuaipan/kss/KssDef$KssAPIResult;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcn/kuaipan/kss/KssDef$KssAPIResult;

    const-string v1, "OK"

    invoke-direct {v0, v1, v3}, Lcn/kuaipan/kss/KssDef$KssAPIResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcn/kuaipan/kss/KssDef$KssAPIResult;->OK:Lcn/kuaipan/kss/KssDef$KssAPIResult;

    new-instance v0, Lcn/kuaipan/kss/KssDef$KssAPIResult;

    const-string v1, "Error"

    invoke-direct {v0, v1, v4}, Lcn/kuaipan/kss/KssDef$KssAPIResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcn/kuaipan/kss/KssDef$KssAPIResult;->Error:Lcn/kuaipan/kss/KssDef$KssAPIResult;

    new-instance v0, Lcn/kuaipan/kss/KssDef$KssAPIResult;

    const-string v1, "Cancel"

    invoke-direct {v0, v1, v5}, Lcn/kuaipan/kss/KssDef$KssAPIResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcn/kuaipan/kss/KssDef$KssAPIResult;->Cancel:Lcn/kuaipan/kss/KssDef$KssAPIResult;

    new-instance v0, Lcn/kuaipan/kss/KssDef$KssAPIResult;

    const-string v1, "NetTimeout"

    invoke-direct {v0, v1, v6}, Lcn/kuaipan/kss/KssDef$KssAPIResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcn/kuaipan/kss/KssDef$KssAPIResult;->NetTimeout:Lcn/kuaipan/kss/KssDef$KssAPIResult;

    new-instance v0, Lcn/kuaipan/kss/KssDef$KssAPIResult;

    const-string v1, "NeedRequest"

    invoke-direct {v0, v1, v7}, Lcn/kuaipan/kss/KssDef$KssAPIResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcn/kuaipan/kss/KssDef$KssAPIResult;->NeedRequest:Lcn/kuaipan/kss/KssDef$KssAPIResult;

    new-instance v0, Lcn/kuaipan/kss/KssDef$KssAPIResult;

    const-string v1, "DataCorrupted"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcn/kuaipan/kss/KssDef$KssAPIResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcn/kuaipan/kss/KssDef$KssAPIResult;->DataCorrupted:Lcn/kuaipan/kss/KssDef$KssAPIResult;

    new-instance v0, Lcn/kuaipan/kss/KssDef$KssAPIResult;

    const-string v1, "SpaceOver"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcn/kuaipan/kss/KssDef$KssAPIResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcn/kuaipan/kss/KssDef$KssAPIResult;->SpaceOver:Lcn/kuaipan/kss/KssDef$KssAPIResult;

    new-instance v0, Lcn/kuaipan/kss/KssDef$KssAPIResult;

    const-string v1, "ServerDenyReadOnly"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcn/kuaipan/kss/KssDef$KssAPIResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcn/kuaipan/kss/KssDef$KssAPIResult;->ServerDenyReadOnly:Lcn/kuaipan/kss/KssDef$KssAPIResult;

    const/16 v0, 0x8

    new-array v0, v0, [Lcn/kuaipan/kss/KssDef$KssAPIResult;

    sget-object v1, Lcn/kuaipan/kss/KssDef$KssAPIResult;->OK:Lcn/kuaipan/kss/KssDef$KssAPIResult;

    aput-object v1, v0, v3

    sget-object v1, Lcn/kuaipan/kss/KssDef$KssAPIResult;->Error:Lcn/kuaipan/kss/KssDef$KssAPIResult;

    aput-object v1, v0, v4

    sget-object v1, Lcn/kuaipan/kss/KssDef$KssAPIResult;->Cancel:Lcn/kuaipan/kss/KssDef$KssAPIResult;

    aput-object v1, v0, v5

    sget-object v1, Lcn/kuaipan/kss/KssDef$KssAPIResult;->NetTimeout:Lcn/kuaipan/kss/KssDef$KssAPIResult;

    aput-object v1, v0, v6

    sget-object v1, Lcn/kuaipan/kss/KssDef$KssAPIResult;->NeedRequest:Lcn/kuaipan/kss/KssDef$KssAPIResult;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcn/kuaipan/kss/KssDef$KssAPIResult;->DataCorrupted:Lcn/kuaipan/kss/KssDef$KssAPIResult;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcn/kuaipan/kss/KssDef$KssAPIResult;->SpaceOver:Lcn/kuaipan/kss/KssDef$KssAPIResult;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcn/kuaipan/kss/KssDef$KssAPIResult;->ServerDenyReadOnly:Lcn/kuaipan/kss/KssDef$KssAPIResult;

    aput-object v2, v0, v1

    sput-object v0, Lcn/kuaipan/kss/KssDef$KssAPIResult;->$VALUES:[Lcn/kuaipan/kss/KssDef$KssAPIResult;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcn/kuaipan/kss/KssDef$KssAPIResult;
    .registers 2
    .parameter "name"

    .prologue
    const-class v0, Lcn/kuaipan/kss/KssDef$KssAPIResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcn/kuaipan/kss/KssDef$KssAPIResult;

    return-object v0
.end method

.method public static values()[Lcn/kuaipan/kss/KssDef$KssAPIResult;
    .registers 1

    .prologue
    sget-object v0, Lcn/kuaipan/kss/KssDef$KssAPIResult;->$VALUES:[Lcn/kuaipan/kss/KssDef$KssAPIResult;

    invoke-virtual {v0}, [Lcn/kuaipan/kss/KssDef$KssAPIResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcn/kuaipan/kss/KssDef$KssAPIResult;

    return-object v0
.end method
