.class public interface abstract Lcn/kuaipan/kss/KssDownload$RequestDownloadInfo;
.super Ljava/lang/Object;
.source "KssDownload.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcn/kuaipan/kss/KssDownload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "RequestDownloadInfo"
.end annotation


# virtual methods
.method public abstract getBlockCount()I
.end method

.method public abstract getBlockInfos(I)Lcn/kuaipan/kss/KssDownload$BlockDownloadInfo;
.end method

.method public abstract getFileSize()I
.end method

.method public abstract getResult()Lcn/kuaipan/kss/KssDef$KssAPIResult;
.end method

.method public abstract getSecureKey()Ljava/lang/String;
.end method
