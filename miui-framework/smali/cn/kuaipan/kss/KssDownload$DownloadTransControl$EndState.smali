.class public final enum Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;
.super Ljava/lang/Enum;
.source "KssDownload.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcn/kuaipan/kss/KssDownload$DownloadTransControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EndState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;

.field public static final enum Completed:Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;

.field public static final enum DataCorrupted:Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;

.field public static final enum Interrupt:Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;

.field public static final enum Transing:Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;


# direct methods
.method static constructor <clinit>()V
    .registers 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;

    const-string v1, "Completed"

    invoke-direct {v0, v1, v2}, Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;->Completed:Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;

    new-instance v0, Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;

    const-string v1, "Interrupt"

    invoke-direct {v0, v1, v3}, Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;->Interrupt:Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;

    new-instance v0, Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;

    const-string v1, "Transing"

    invoke-direct {v0, v1, v4}, Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;->Transing:Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;

    new-instance v0, Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;

    const-string v1, "DataCorrupted"

    invoke-direct {v0, v1, v5}, Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;->DataCorrupted:Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;

    const/4 v0, 0x4

    new-array v0, v0, [Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;

    sget-object v1, Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;->Completed:Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;

    aput-object v1, v0, v2

    sget-object v1, Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;->Interrupt:Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;

    aput-object v1, v0, v3

    sget-object v1, Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;->Transing:Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;

    aput-object v1, v0, v4

    sget-object v1, Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;->DataCorrupted:Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;

    aput-object v1, v0, v5

    sput-object v0, Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;->$VALUES:[Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;
    .registers 2
    .parameter "name"

    .prologue
    const-class v0, Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;

    return-object v0
.end method

.method public static values()[Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;
    .registers 1

    .prologue
    sget-object v0, Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;->$VALUES:[Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;

    invoke-virtual {v0}, [Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcn/kuaipan/kss/KssDownload$DownloadTransControl$EndState;

    return-object v0
.end method
