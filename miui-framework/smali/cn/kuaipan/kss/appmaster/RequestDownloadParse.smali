.class public Lcn/kuaipan/kss/appmaster/RequestDownloadParse;
.super Ljava/lang/Object;
.source "RequestDownloadParse.java"

# interfaces
.implements Lcn/kuaipan/kss/KssDownload$RequestDownloadInfo;


# instance fields
.field private m_Result:Lcn/kuaipan/kss/KssDef$KssAPIResult;

.field private m_blockInfos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcn/kuaipan/kss/appmaster/_BlockInfo;",
            ">;"
        }
    .end annotation
.end field

.field private m_fileSize:I

.field private m_secureKey:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcn/kuaipan/kss/KssDef$KssAPIResult;->Error:Lcn/kuaipan/kss/KssDef$KssAPIResult;

    iput-object v0, p0, Lcn/kuaipan/kss/appmaster/RequestDownloadParse;->m_Result:Lcn/kuaipan/kss/KssDef$KssAPIResult;

    const/4 v0, 0x0

    iput-object v0, p0, Lcn/kuaipan/kss/appmaster/RequestDownloadParse;->m_secureKey:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcn/kuaipan/kss/appmaster/RequestDownloadParse;->m_fileSize:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcn/kuaipan/kss/appmaster/RequestDownloadParse;->m_blockInfos:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public getBlockCount()I
    .registers 2

    .prologue
    iget-object v0, p0, Lcn/kuaipan/kss/appmaster/RequestDownloadParse;->m_blockInfos:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getBlockInfos(I)Lcn/kuaipan/kss/KssDownload$BlockDownloadInfo;
    .registers 3
    .parameter "blockIndex"

    .prologue
    if-ltz p1, :cond_a

    iget-object v0, p0, Lcn/kuaipan/kss/appmaster/RequestDownloadParse;->m_blockInfos:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_c

    :cond_a
    const/4 v0, 0x0

    :goto_b
    return-object v0

    :cond_c
    iget-object v0, p0, Lcn/kuaipan/kss/appmaster/RequestDownloadParse;->m_blockInfos:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcn/kuaipan/kss/KssDownload$BlockDownloadInfo;

    goto :goto_b
.end method

.method public getFileSize()I
    .registers 2

    .prologue
    iget v0, p0, Lcn/kuaipan/kss/appmaster/RequestDownloadParse;->m_fileSize:I

    return v0
.end method

.method public getResult()Lcn/kuaipan/kss/KssDef$KssAPIResult;
    .registers 2

    .prologue
    iget-object v0, p0, Lcn/kuaipan/kss/appmaster/RequestDownloadParse;->m_Result:Lcn/kuaipan/kss/KssDef$KssAPIResult;

    return-object v0
.end method

.method public getSecureKey()Ljava/lang/String;
    .registers 2

    .prologue
    iget-object v0, p0, Lcn/kuaipan/kss/appmaster/RequestDownloadParse;->m_secureKey:Ljava/lang/String;

    return-object v0
.end method

.method public parseRequestDownloadInfo(Ljava/lang/String;)Z
    .registers 5
    .parameter "jsonStr"

    .prologue
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .local v1, rootObj:Lorg/json/JSONObject;
    invoke-virtual {p0, v1}, Lcn/kuaipan/kss/appmaster/RequestDownloadParse;->parseRequestDownloadInfo(Lorg/json/JSONObject;)Z
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_8} :catch_a

    move-result v2

    .end local v1           #rootObj:Lorg/json/JSONObject;
    :goto_9
    return v2

    :catch_a
    move-exception v0

    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_9
.end method

.method public parseRequestDownloadInfo(Lorg/json/JSONObject;)Z
    .registers 10
    .parameter "jsonInfo"

    .prologue
    const/4 v5, 0x0

    iput v5, p0, Lcn/kuaipan/kss/appmaster/RequestDownloadParse;->m_fileSize:I

    :try_start_3
    const-string v6, "stat"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    const-string v7, "OK"

    invoke-virtual {v6, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_12

    :goto_11
    return v5

    :cond_12
    sget-object v6, Lcn/kuaipan/kss/KssDef$KssAPIResult;->OK:Lcn/kuaipan/kss/KssDef$KssAPIResult;

    iput-object v6, p0, Lcn/kuaipan/kss/appmaster/RequestDownloadParse;->m_Result:Lcn/kuaipan/kss/KssDef$KssAPIResult;

    const-string v6, "secure_key"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcn/kuaipan/kss/appmaster/RequestDownloadParse;->m_secureKey:Ljava/lang/String;

    const-string v6, "blocks"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .local v1, blockObjs:Lorg/json/JSONArray;
    const/4 v3, 0x0

    .local v3, i:I
    :goto_25
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v3, v6, :cond_4c

    invoke-virtual {v1, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .local v4, oneJsonBlock:Lorg/json/JSONObject;
    new-instance v0, Lcn/kuaipan/kss/appmaster/_BlockInfo;

    invoke-direct {v0, v3}, Lcn/kuaipan/kss/appmaster/_BlockInfo;-><init>(I)V

    .local v0, block:Lcn/kuaipan/kss/appmaster/_BlockInfo;
    invoke-virtual {v0, v4}, Lcn/kuaipan/kss/appmaster/_BlockInfo;->parseBlockInfo(Lorg/json/JSONObject;)Z

    iget-object v6, p0, Lcn/kuaipan/kss/appmaster/RequestDownloadParse;->m_blockInfos:Ljava/util/List;

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget v6, p0, Lcn/kuaipan/kss/appmaster/RequestDownloadParse;->m_fileSize:I

    invoke-virtual {v0}, Lcn/kuaipan/kss/appmaster/_BlockInfo;->getBlockSize()I

    move-result v7

    add-int/2addr v6, v7

    iput v6, p0, Lcn/kuaipan/kss/appmaster/RequestDownloadParse;->m_fileSize:I
    :try_end_45
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_45} :catch_48

    add-int/lit8 v3, v3, 0x1

    goto :goto_25

    .end local v0           #block:Lcn/kuaipan/kss/appmaster/_BlockInfo;
    .end local v1           #blockObjs:Lorg/json/JSONArray;
    .end local v3           #i:I
    .end local v4           #oneJsonBlock:Lorg/json/JSONObject;
    :catch_48
    move-exception v2

    .local v2, e:Ljava/lang/Exception;
    iput v5, p0, Lcn/kuaipan/kss/appmaster/RequestDownloadParse;->m_fileSize:I

    goto :goto_11

    .end local v2           #e:Ljava/lang/Exception;
    .restart local v1       #blockObjs:Lorg/json/JSONArray;
    .restart local v3       #i:I
    :cond_4c
    const/4 v5, 0x1

    goto :goto_11
.end method
