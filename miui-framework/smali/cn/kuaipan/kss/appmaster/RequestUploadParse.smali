.class public Lcn/kuaipan/kss/appmaster/RequestUploadParse;
.super Ljava/lang/Object;
.source "RequestUploadParse.java"

# interfaces
.implements Lcn/kuaipan/kss/KssUpload$RequestUploadInfo;


# instance fields
.field private m_blockIsExist:[Z

.field private m_blockMeta:[Ljava/lang/String;

.field private m_dataSize:I

.field private m_fileMeta:Ljava/lang/String;

.field private m_fileStoreID:Ljava/lang/String;

.field private m_nodeIPs:[Ljava/lang/String;

.field private m_protocolsStr:Ljava/lang/String;

.field private m_secureKey:[B

.field private m_uploadState:Lcn/kuaipan/kss/KssUpload$RequestUploadInfo$RequestUploadState;


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcn/kuaipan/kss/KssUpload$RequestUploadInfo$RequestUploadState;->NeedUpload:Lcn/kuaipan/kss/KssUpload$RequestUploadInfo$RequestUploadState;

    iput-object v0, p0, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->m_uploadState:Lcn/kuaipan/kss/KssUpload$RequestUploadInfo$RequestUploadState;

    iput-object v1, p0, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->m_fileStoreID:Ljava/lang/String;

    iput-object v1, p0, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->m_nodeIPs:[Ljava/lang/String;

    iput-object v1, p0, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->m_protocolsStr:Ljava/lang/String;

    iput-object v1, p0, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->m_secureKey:[B

    iput-object v1, p0, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->m_fileMeta:Ljava/lang/String;

    iput-object v1, p0, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->m_blockMeta:[Ljava/lang/String;

    iput-object v1, p0, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->m_blockIsExist:[Z

    const/4 v0, 0x0

    iput v0, p0, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->m_dataSize:I

    return-void
.end method

.method private _clear()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    iput-object v0, p0, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->m_nodeIPs:[Ljava/lang/String;

    iput-object v0, p0, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->m_protocolsStr:Ljava/lang/String;

    iput-object v0, p0, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->m_secureKey:[B

    iput-object v0, p0, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->m_fileMeta:Ljava/lang/String;

    iput-object v0, p0, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->m_blockMeta:[Ljava/lang/String;

    iput-object v0, p0, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->m_blockIsExist:[Z

    const/4 v0, 0x0

    iput v0, p0, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->m_dataSize:I

    return-void
.end method

.method public static getFileExitRequestInfo()Lcn/kuaipan/kss/KssUpload$RequestUploadInfo;
    .registers 1

    .prologue
    invoke-static {}, Lcn/kuaipan/kss/appmaster/_RequestInfo_FileExisted;->getInstance()Lcn/kuaipan/kss/KssUpload$RequestUploadInfo;

    move-result-object v0

    return-object v0
.end method

.method private parseInfo(Lorg/json/JSONObject;)Z
    .registers 15
    .parameter "jsonInfo"

    .prologue
    :try_start_0
    const-string v10, "stat"

    invoke-virtual {p1, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .local v8, statStr:Ljava/lang/String;
    const-string v10, "FILE_EXISTED"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1c

    sget-object v10, Lcn/kuaipan/kss/KssUpload$RequestUploadInfo$RequestUploadState;->FileExisted:Lcn/kuaipan/kss/KssUpload$RequestUploadInfo$RequestUploadState;

    iput-object v10, p0, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->m_uploadState:Lcn/kuaipan/kss/KssUpload$RequestUploadInfo$RequestUploadState;

    const-string v10, "stoid"

    invoke-virtual {p1, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->m_fileStoreID:Ljava/lang/String;

    const/4 v10, 0x1

    .end local v8           #statStr:Ljava/lang/String;
    :goto_1b
    return v10

    .restart local v8       #statStr:Ljava/lang/String;
    :cond_1c
    sget-object v10, Lcn/kuaipan/kss/KssUpload$RequestUploadInfo$RequestUploadState;->NeedUpload:Lcn/kuaipan/kss/KssUpload$RequestUploadInfo$RequestUploadState;

    iput-object v10, p0, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->m_uploadState:Lcn/kuaipan/kss/KssUpload$RequestUploadInfo$RequestUploadState;

    const-string v10, ""

    iput-object v10, p0, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->m_fileStoreID:Ljava/lang/String;

    const-string v10, "node_urls"

    invoke-virtual {p1, v10}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .local v5, jsonURLS:Lorg/json/JSONArray;
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v10

    const/4 v11, 0x1

    if-ge v10, v11, :cond_33

    const/4 v10, 0x0

    goto :goto_1b

    :cond_33
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v10

    new-array v10, v10, [Ljava/lang/String;

    iput-object v10, p0, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->m_nodeIPs:[Ljava/lang/String;

    const/4 v3, 0x0

    .local v3, i:I
    :goto_3c
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v10

    if-ge v3, v10, :cond_6a

    invoke-virtual {v5, v3}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    .local v1, curIP:Ljava/lang/String;
    const-string v10, "://"

    invoke-virtual {v1, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    .local v7, proPos:I
    iget-object v10, p0, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->m_protocolsStr:Ljava/lang/String;

    if-nez v10, :cond_59

    if-lez v7, :cond_59

    const/4 v10, 0x0

    invoke-virtual {v1, v10, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->m_protocolsStr:Ljava/lang/String;

    :cond_59
    iget-object v10, p0, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->m_nodeIPs:[Ljava/lang/String;

    add-int/lit8 v11, v7, 0x3

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v12

    invoke-virtual {v1, v11, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3c

    .end local v1           #curIP:Ljava/lang/String;
    .end local v7           #proPos:I
    :cond_6a
    const-string v10, "secure_key"

    invoke-virtual {p1, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .local v9, strKey:Ljava/lang/String;
    invoke-static {v9}, Lcn/kuaipan/kss/utils/Encode;->hexStringToByteArray(Ljava/lang/String;)[B

    move-result-object v10

    iput-object v10, p0, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->m_secureKey:[B

    const-string v10, "file_meta"

    invoke-virtual {p1, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->m_fileMeta:Ljava/lang/String;

    const-string v10, "block_metas"

    invoke-virtual {p1, v10}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .local v0, blockInfos:Lorg/json/JSONArray;
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v10

    new-array v10, v10, [Ljava/lang/String;

    iput-object v10, p0, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->m_blockMeta:[Ljava/lang/String;

    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v10

    new-array v10, v10, [Z

    iput-object v10, p0, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->m_blockIsExist:[Z

    const/4 v4, 0x0

    .local v4, isExist:I
    const/4 v3, 0x0

    :goto_96
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v10

    if-ge v3, v10, :cond_d0

    invoke-virtual {v0, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    .local v6, oneInfo:Lorg/json/JSONObject;
    const-string v10, "is_existed"

    invoke-virtual {v6, v10}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v4

    iget-object v11, p0, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->m_blockIsExist:[Z

    if-nez v4, :cond_bc

    const/4 v10, 0x0

    :goto_ab
    aput-boolean v10, v11, v3

    if-eqz v10, :cond_be

    iget-object v10, p0, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->m_blockMeta:[Ljava/lang/String;

    const-string v11, "commit_meta"

    invoke-virtual {v6, v11}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v3

    :goto_b9
    add-int/lit8 v3, v3, 0x1

    goto :goto_96

    :cond_bc
    const/4 v10, 0x1

    goto :goto_ab

    :cond_be
    iget-object v10, p0, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->m_blockMeta:[Ljava/lang/String;

    const-string v11, "block_meta"

    invoke-virtual {v6, v11}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v3
    :try_end_c8
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_c8} :catch_c9

    goto :goto_b9

    .end local v0           #blockInfos:Lorg/json/JSONArray;
    .end local v3           #i:I
    .end local v4           #isExist:I
    .end local v5           #jsonURLS:Lorg/json/JSONArray;
    .end local v6           #oneInfo:Lorg/json/JSONObject;
    .end local v8           #statStr:Ljava/lang/String;
    .end local v9           #strKey:Ljava/lang/String;
    :catch_c9
    move-exception v2

    .local v2, e:Lorg/json/JSONException;
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    const/4 v10, 0x0

    goto/16 :goto_1b

    .end local v2           #e:Lorg/json/JSONException;
    .restart local v0       #blockInfos:Lorg/json/JSONArray;
    .restart local v3       #i:I
    .restart local v4       #isExist:I
    .restart local v5       #jsonURLS:Lorg/json/JSONArray;
    .restart local v8       #statStr:Ljava/lang/String;
    .restart local v9       #strKey:Ljava/lang/String;
    :cond_d0
    const/4 v10, 0x1

    goto/16 :goto_1b
.end method


# virtual methods
.method public getBlockCount()I
    .registers 2

    .prologue
    iget-object v0, p0, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->m_blockMeta:[Ljava/lang/String;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    :goto_5
    return v0

    :cond_6
    iget-object v0, p0, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->m_blockMeta:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_5
.end method

.method public getBlockIsExist(I)Z
    .registers 3
    .parameter "idx"

    .prologue
    iget-object v0, p0, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->m_blockMeta:[Ljava/lang/String;

    if-eqz v0, :cond_10

    if-ltz p1, :cond_10

    iget-object v0, p0, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->m_blockMeta:[Ljava/lang/String;

    array-length v0, v0

    if-ge p1, v0, :cond_10

    iget-object v0, p0, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->m_blockIsExist:[Z

    aget-boolean v0, v0, p1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public getBlockMeta(I)Ljava/lang/String;
    .registers 3
    .parameter "idx"

    .prologue
    iget-object v0, p0, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->m_blockMeta:[Ljava/lang/String;

    if-eqz v0, :cond_10

    if-ltz p1, :cond_10

    iget-object v0, p0, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->m_blockMeta:[Ljava/lang/String;

    array-length v0, v0

    if-ge p1, v0, :cond_10

    iget-object v0, p0, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->m_blockMeta:[Ljava/lang/String;

    aget-object v0, v0, p1

    :goto_f
    return-object v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public getFileMeta()Ljava/lang/String;
    .registers 2

    .prologue
    iget-object v0, p0, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->m_fileMeta:Ljava/lang/String;

    return-object v0
.end method

.method public getFileSize()I
    .registers 2

    .prologue
    iget v0, p0, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->m_dataSize:I

    return v0
.end method

.method public getFileStoreID()Ljava/lang/String;
    .registers 2

    .prologue
    iget-object v0, p0, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->m_fileStoreID:Ljava/lang/String;

    return-object v0
.end method

.method public getNodeIP(I)Ljava/lang/String;
    .registers 4
    .parameter "idx"

    .prologue
    iget-object v0, p0, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->m_nodeIPs:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getNodeIPCount()I
    .registers 2

    .prologue
    iget-object v0, p0, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->m_nodeIPs:[Ljava/lang/String;

    array-length v0, v0

    return v0
.end method

.method public getProtocol()Ljava/lang/String;
    .registers 2

    .prologue
    iget-object v0, p0, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->m_protocolsStr:Ljava/lang/String;

    return-object v0
.end method

.method public getRequestUploadState()Lcn/kuaipan/kss/KssUpload$RequestUploadInfo$RequestUploadState;
    .registers 2

    .prologue
    iget-object v0, p0, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->m_uploadState:Lcn/kuaipan/kss/KssUpload$RequestUploadInfo$RequestUploadState;

    return-object v0
.end method

.method public getResult()Lcn/kuaipan/kss/KssDef$KssAPIResult;
    .registers 2

    .prologue
    sget-object v0, Lcn/kuaipan/kss/KssDef$KssAPIResult;->OK:Lcn/kuaipan/kss/KssDef$KssAPIResult;

    return-object v0
.end method

.method public getSeureKey()[B
    .registers 2

    .prologue
    iget-object v0, p0, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->m_secureKey:[B

    return-object v0
.end method

.method public parseRequestUploadInfo(Ljava/lang/String;)Z
    .registers 7
    .parameter "jsonStr"

    .prologue
    invoke-direct {p0}, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->_clear()V

    const/4 v1, 0x0

    .local v1, jsonInfo:Lorg/json/JSONObject;
    :try_start_4
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_9} :catch_f

    .end local v1           #jsonInfo:Lorg/json/JSONObject;
    .local v2, jsonInfo:Lorg/json/JSONObject;
    invoke-direct {p0, v2}, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->parseInfo(Lorg/json/JSONObject;)Z

    move-result v3

    move-object v1, v2

    .end local v2           #jsonInfo:Lorg/json/JSONObject;
    .restart local v1       #jsonInfo:Lorg/json/JSONObject;
    :goto_e
    return v3

    :catch_f
    move-exception v0

    .local v0, e:Ljava/lang/Exception;
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/4 v3, 0x0

    goto :goto_e
.end method

.method public parseRequestUploadInfo(Lorg/json/JSONObject;)Z
    .registers 3
    .parameter "jsonObj"

    .prologue
    if-nez p1, :cond_4

    const/4 v0, 0x0

    :goto_3
    return v0

    :cond_4
    invoke-direct {p0, p1}, Lcn/kuaipan/kss/appmaster/RequestUploadParse;->parseInfo(Lorg/json/JSONObject;)Z

    move-result v0

    goto :goto_3
.end method
