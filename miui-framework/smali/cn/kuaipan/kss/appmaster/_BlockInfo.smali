.class Lcn/kuaipan/kss/appmaster/_BlockInfo;
.super Ljava/lang/Object;
.source "RequestDownloadParse.java"

# interfaces
.implements Lcn/kuaipan/kss/KssDownload$BlockDownloadInfo;


# instance fields
.field private m_blockIndex:I

.field private m_blockSHA1:[B

.field private m_blockSize:I

.field private m_fullURL:Ljava/lang/String;


# direct methods
.method public constructor <init>(I)V
    .registers 4
    .parameter "index"

    .prologue
    const/4 v1, 0x0

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcn/kuaipan/kss/appmaster/_BlockInfo;->m_blockIndex:I

    const/4 v0, 0x0

    iput v0, p0, Lcn/kuaipan/kss/appmaster/_BlockInfo;->m_blockSize:I

    iput-object v1, p0, Lcn/kuaipan/kss/appmaster/_BlockInfo;->m_fullURL:Ljava/lang/String;

    iput-object v1, p0, Lcn/kuaipan/kss/appmaster/_BlockInfo;->m_blockSHA1:[B

    iput p1, p0, Lcn/kuaipan/kss/appmaster/_BlockInfo;->m_blockIndex:I

    return-void
.end method


# virtual methods
.method public getBlockDownload_FullURL(I)Ljava/lang/String;
    .registers 3
    .parameter "idx"

    .prologue
    iget-object v0, p0, Lcn/kuaipan/kss/appmaster/_BlockInfo;->m_fullURL:Ljava/lang/String;

    return-object v0
.end method

.method public getBlockIndex()I
    .registers 2

    .prologue
    iget v0, p0, Lcn/kuaipan/kss/appmaster/_BlockInfo;->m_blockIndex:I

    return v0
.end method

.method public getBlockSHA1()[B
    .registers 2

    .prologue
    iget-object v0, p0, Lcn/kuaipan/kss/appmaster/_BlockInfo;->m_blockSHA1:[B

    return-object v0
.end method

.method public getBlockSize()I
    .registers 2

    .prologue
    iget v0, p0, Lcn/kuaipan/kss/appmaster/_BlockInfo;->m_blockSize:I

    return v0
.end method

.method public getDownloadURLCount()I
    .registers 3

    .prologue
    iget-object v0, p0, Lcn/kuaipan/kss/appmaster/_BlockInfo;->m_fullURL:Ljava/lang/String;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcn/kuaipan/kss/appmaster/_BlockInfo;->m_fullURL:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    :cond_e
    const/4 v0, 0x0

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x1

    goto :goto_f
.end method

.method public parseBlockInfo(Lorg/json/JSONObject;)Z
    .registers 7
    .parameter "jsonInfo"

    .prologue
    const/4 v3, 0x0

    if-nez p1, :cond_4

    :goto_3
    return v3

    :cond_4
    :try_start_4
    const-string v4, "sha1"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .local v1, sha1:Ljava/lang/String;
    invoke-static {v1}, Lcn/kuaipan/kss/utils/Encode;->hexStringToByteArray(Ljava/lang/String;)[B

    move-result-object v4

    iput-object v4, p0, Lcn/kuaipan/kss/appmaster/_BlockInfo;->m_blockSHA1:[B

    const-string v4, "urls"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .local v2, urls:Lorg/json/JSONArray;
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcn/kuaipan/kss/appmaster/_BlockInfo;->m_fullURL:Ljava/lang/String;

    const-string v4, "size"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcn/kuaipan/kss/appmaster/_BlockInfo;->m_blockSize:I
    :try_end_25
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_25} :catch_27

    const/4 v3, 0x1

    goto :goto_3

    .end local v1           #sha1:Ljava/lang/String;
    .end local v2           #urls:Lorg/json/JSONArray;
    :catch_27
    move-exception v0

    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const-string v4, ""

    iput-object v4, p0, Lcn/kuaipan/kss/appmaster/_BlockInfo;->m_fullURL:Ljava/lang/String;

    iput v3, p0, Lcn/kuaipan/kss/appmaster/_BlockInfo;->m_blockSize:I

    goto :goto_3
.end method
