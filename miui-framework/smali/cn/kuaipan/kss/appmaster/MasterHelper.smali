.class public Lcn/kuaipan/kss/appmaster/MasterHelper;
.super Ljava/lang/Object;
.source "MasterHelper.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static genCommiteUploadJson(Lcn/kuaipan/kss/KssUpload$UploadResult;)Lorg/json/JSONObject;
    .registers 7
    .parameter "uploadResult"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .local v1, commitObj:Lorg/json/JSONObject;
    const-string v3, "file_meta"

    invoke-interface {p0}, Lcn/kuaipan/kss/KssUpload$UploadResult;->getFileMeta()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    .local v0, cmtMetas:Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, i:I
    :goto_14
    invoke-interface {p0}, Lcn/kuaipan/kss/KssUpload$UploadResult;->getBlockCount()I

    move-result v3

    if-ge v2, v3, :cond_2f

    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    const-string v4, "commit_meta"

    invoke-interface {p0, v2}, Lcn/kuaipan/kss/KssUpload$UploadResult;->getBlockCommitMeta(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v0, v3}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    add-int/lit8 v2, v2, 0x1

    goto :goto_14

    :cond_2f
    const-string v3, "commit_metas"

    invoke-virtual {v1, v3, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    return-object v1
.end method

.method public static genRequestUploadJson(Ljava/io/InputStream;)Lorg/json/JSONObject;
    .registers 13
    .parameter "in"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v11, 0x0

    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3}, Lorg/json/JSONArray;-><init>()V

    .local v3, jsonBlockArr:Lorg/json/JSONArray;
    const-string v9, "SHA1"

    invoke-static {v9}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v8

    .local v8, sha1:Ljava/security/MessageDigest;
    const-string v9, "MD5"

    invoke-static {v9}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v5

    .local v5, md5:Ljava/security/MessageDigest;
    sget v9, Lcn/kuaipan/kss/KssDef;->CHUNKSIZE_MIN:I

    new-array v0, v9, [B

    .local v0, chunkData:[B
    const/4 v6, 0x1

    .local v6, notEofData:Z
    :goto_17
    if-eqz v6, :cond_38

    const/4 v1, 0x0

    .local v1, dataLen:I
    const/4 v7, 0x0

    .local v7, readLen:I
    :goto_1b
    sget v9, Lcn/kuaipan/kss/KssDef;->BLOCKSIZE:I

    if-ge v1, v9, :cond_36

    array-length v9, v0

    sget v10, Lcn/kuaipan/kss/KssDef;->BLOCKSIZE:I

    sub-int/2addr v10, v1

    invoke-static {v9, v10}, Ljava/lang/Math;->min(II)I

    move-result v9

    invoke-virtual {p0, v0, v11, v9}, Ljava/io/InputStream;->read([BII)I

    move-result v7

    if-ltz v7, :cond_35

    invoke-virtual {v8, v0, v11, v7}, Ljava/security/MessageDigest;->update([BII)V

    invoke-virtual {v5, v0, v11, v7}, Ljava/security/MessageDigest;->update([BII)V

    add-int/2addr v1, v7

    goto :goto_1b

    :cond_35
    const/4 v6, 0x0

    :cond_36
    if-nez v1, :cond_43

    .end local v1           #dataLen:I
    .end local v7           #readLen:I
    :cond_38
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    .local v4, jsonBody:Lorg/json/JSONObject;
    const-string v9, "block_infos"

    invoke-virtual {v4, v9, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    return-object v4

    .end local v4           #jsonBody:Lorg/json/JSONObject;
    .restart local v1       #dataLen:I
    .restart local v7       #readLen:I
    :cond_43
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .local v2, jsonBlock:Lorg/json/JSONObject;
    const-string v9, "sha1"

    invoke-virtual {v8}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v10

    invoke-static {v10}, Lcn/kuaipan/kss/utils/Encode;->byteArrayToHexString([B)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v9, "md5"

    invoke-virtual {v5}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v10

    invoke-static {v10}, Lcn/kuaipan/kss/utils/Encode;->byteArrayToHexString([B)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v9, "size"

    invoke-virtual {v2, v9, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_17
.end method

.method public static parseKssProxiesInfo(Ljava/lang/String;)[Ljava/lang/String;
    .registers 9
    .parameter "jsonString"

    .prologue
    const/4 v5, 0x0

    :try_start_1
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .local v2, requesInfo:Lorg/json/JSONObject;
    const-string v6, "stat"

    invoke-virtual {v2, v6}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    const-string v7, "OK"

    invoke-virtual {v6, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_16

    move-object v4, v5

    .end local v2           #requesInfo:Lorg/json/JSONObject;
    :cond_15
    :goto_15
    return-object v4

    .restart local v2       #requesInfo:Lorg/json/JSONObject;
    :cond_16
    const-string v6, "proxies"

    invoke-virtual {v2, v6}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .local v3, uriJsonArray:Lorg/json/JSONArray;
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v6

    new-array v4, v6, [Ljava/lang/String;

    .local v4, uriStrings:[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, i:I
    :goto_23
    array-length v6, v4

    if-ge v1, v6, :cond_15

    invoke-virtual {v3, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    const-string v7, "url"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v1
    :try_end_32
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_32} :catch_35

    add-int/lit8 v1, v1, 0x1

    goto :goto_23

    .end local v1           #i:I
    .end local v2           #requesInfo:Lorg/json/JSONObject;
    .end local v3           #uriJsonArray:Lorg/json/JSONArray;
    .end local v4           #uriStrings:[Ljava/lang/String;
    :catch_35
    move-exception v0

    .local v0, e:Ljava/lang/Exception;
    move-object v4, v5

    goto :goto_15
.end method
