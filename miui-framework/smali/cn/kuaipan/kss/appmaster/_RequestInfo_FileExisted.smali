.class Lcn/kuaipan/kss/appmaster/_RequestInfo_FileExisted;
.super Ljava/lang/Object;
.source "RequestUploadParse.java"

# interfaces
.implements Lcn/kuaipan/kss/KssUpload$RequestUploadInfo;


# static fields
.field static requestInfo:Lcn/kuaipan/kss/KssUpload$RequestUploadInfo;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    const/4 v0, 0x0

    sput-object v0, Lcn/kuaipan/kss/appmaster/_RequestInfo_FileExisted;->requestInfo:Lcn/kuaipan/kss/KssUpload$RequestUploadInfo;

    return-void
.end method

.method constructor <init>()V
    .registers 1

    .prologue
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcn/kuaipan/kss/KssUpload$RequestUploadInfo;
    .registers 1

    .prologue
    sget-object v0, Lcn/kuaipan/kss/appmaster/_RequestInfo_FileExisted;->requestInfo:Lcn/kuaipan/kss/KssUpload$RequestUploadInfo;

    if-nez v0, :cond_b

    new-instance v0, Lcn/kuaipan/kss/appmaster/_RequestInfo_FileExisted;

    invoke-direct {v0}, Lcn/kuaipan/kss/appmaster/_RequestInfo_FileExisted;-><init>()V

    sput-object v0, Lcn/kuaipan/kss/appmaster/_RequestInfo_FileExisted;->requestInfo:Lcn/kuaipan/kss/KssUpload$RequestUploadInfo;

    :cond_b
    sget-object v0, Lcn/kuaipan/kss/appmaster/_RequestInfo_FileExisted;->requestInfo:Lcn/kuaipan/kss/KssUpload$RequestUploadInfo;

    return-object v0
.end method


# virtual methods
.method public getBlockCount()I
    .registers 2

    .prologue
    const/4 v0, 0x0

    return v0
.end method

.method public getBlockIsExist(I)Z
    .registers 3
    .parameter "idx"

    .prologue
    const/4 v0, 0x0

    return v0
.end method

.method public getBlockMeta(I)Ljava/lang/String;
    .registers 3
    .parameter "idx"

    .prologue
    const/4 v0, 0x0

    return-object v0
.end method

.method public getFileMeta()Ljava/lang/String;
    .registers 2

    .prologue
    const/4 v0, 0x0

    return-object v0
.end method

.method public getFileSize()I
    .registers 2

    .prologue
    const/4 v0, 0x0

    return v0
.end method

.method public getFileStoreID()Ljava/lang/String;
    .registers 2

    .prologue
    const/4 v0, 0x0

    return-object v0
.end method

.method public getNodeIP(I)Ljava/lang/String;
    .registers 3
    .parameter "idx"

    .prologue
    const/4 v0, 0x0

    return-object v0
.end method

.method public getNodeIPCount()I
    .registers 2

    .prologue
    const/4 v0, 0x0

    return v0
.end method

.method public getProtocol()Ljava/lang/String;
    .registers 2

    .prologue
    const/4 v0, 0x0

    return-object v0
.end method

.method public getRequestUploadState()Lcn/kuaipan/kss/KssUpload$RequestUploadInfo$RequestUploadState;
    .registers 2

    .prologue
    sget-object v0, Lcn/kuaipan/kss/KssUpload$RequestUploadInfo$RequestUploadState;->FileExisted:Lcn/kuaipan/kss/KssUpload$RequestUploadInfo$RequestUploadState;

    return-object v0
.end method

.method public getResult()Lcn/kuaipan/kss/KssDef$KssAPIResult;
    .registers 2

    .prologue
    sget-object v0, Lcn/kuaipan/kss/KssDef$KssAPIResult;->OK:Lcn/kuaipan/kss/KssDef$KssAPIResult;

    return-object v0
.end method

.method public getSeureKey()[B
    .registers 2

    .prologue
    const/4 v0, 0x0

    return-object v0
.end method
