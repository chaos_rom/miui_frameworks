// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package android.support.v13.dreams;

import android.app.Activity;
import android.content.*;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.Window;

public class BasicDream extends Activity {
    class BasicDreamView extends View {

        public void onAttachedToWindow() {
            setSystemUiVisibility(1);
        }

        public void onDraw(Canvas canvas) {
            BasicDream.this.onDraw(canvas);
        }

        final BasicDream this$0;

        public BasicDreamView(Context context) {
            this$0 = BasicDream.this;
            super(context);
        }

        public BasicDreamView(Context context, AttributeSet attributeset) {
            this$0 = BasicDream.this;
            super(context, attributeset);
        }
    }


    public BasicDream() {
        mPlugged = false;
    }

    protected View getContentView() {
        return mView;
    }

    protected void invalidate() {
        getContentView().invalidate();
    }

    public void onDraw(Canvas canvas) {
    }

    public void onPause() {
        super.onPause();
        Log.d("BasicDream", "exiting onPause");
        finish();
    }

    public void onStart() {
        super.onStart();
        setContentView(new BasicDreamView(this));
        getWindow().addFlags(0x80001);
        IntentFilter intentfilter = new IntentFilter();
        intentfilter.addAction("android.intent.action.BATTERY_CHANGED");
        registerReceiver(mPowerIntentReceiver, intentfilter);
    }

    public void onStop() {
        super.onStop();
        unregisterReceiver(mPowerIntentReceiver);
    }

    public void onUserInteraction() {
        Log.d("BasicDream", "exiting onUserInteraction");
        finish();
    }

    public void setContentView(View view) {
        super.setContentView(view);
        mView = view;
    }

    private static final boolean DEBUG = true;
    private static final String TAG = "BasicDream";
    private boolean mPlugged;
    private final BroadcastReceiver mPowerIntentReceiver = new BroadcastReceiver() {

        public void onReceive(Context context, Intent intent) {
            boolean flag = true;
            if("android.intent.action.BATTERY_CHANGED".equals(intent.getAction())) {
                if(flag != intent.getIntExtra("plugged", 0))
                    flag = false;
                if(flag != mPlugged) {
                    StringBuilder stringbuilder = (new StringBuilder()).append("now ");
                    String s;
                    if(flag)
                        s = "plugged in";
                    else
                        s = "unplugged";
                    Log.d("BasicDream", stringbuilder.append(s).toString());
                    mPlugged = flag;
                    if(mPlugged)
                        getWindow().addFlags(128);
                    else
                        getWindow().clearFlags(128);
                }
            }
        }

        final BasicDream this$0;

             {
                this$0 = BasicDream.this;
                super();
            }
    };
    private View mView;



/*
    static boolean access$002(BasicDream basicdream, boolean flag) {
        basicdream.mPlugged = flag;
        return flag;
    }

*/
}
