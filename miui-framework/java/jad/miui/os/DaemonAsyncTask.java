// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.os;

import android.os.AsyncTask;
import android.util.Pair;
import java.util.Stack;

// Referenced classes of package miui.os:
//            ObservableAsyncTask

public abstract class DaemonAsyncTask extends ObservableAsyncTask {
    public static class StackJobPool
        implements JobPool {

        /**
         * @deprecated Method addJob is deprecated
         */

        public void addJob(Object obj) {
            this;
            JVM INSTR monitorenter ;
            if(!mJobs.contains(obj))
                mJobs.push(obj);
            this;
            JVM INSTR monitorexit ;
            return;
            Exception exception;
            exception;
            throw exception;
        }

        /**
         * @deprecated Method finishJob is deprecated
         */

        public void finishJob(Object obj) {
            this;
            JVM INSTR monitorenter ;
        }

        /**
         * @deprecated Method getNextJob is deprecated
         */

        public Object getNextJob() {
            this;
            JVM INSTR monitorenter ;
            boolean flag = mJobs.empty();
            if(!flag) goto _L2; else goto _L1
_L1:
            Object obj1 = null;
_L4:
            this;
            JVM INSTR monitorexit ;
            return obj1;
_L2:
            Object obj = mJobs.pop();
            obj1 = obj;
            if(true) goto _L4; else goto _L3
_L3:
            Exception exception;
            exception;
            throw exception;
        }

        /**
         * @deprecated Method isEmpty is deprecated
         */

        public boolean isEmpty() {
            this;
            JVM INSTR monitorenter ;
            boolean flag = mJobs.empty();
            this;
            JVM INSTR monitorexit ;
            return flag;
            Exception exception;
            exception;
            throw exception;
        }

        private Stack mJobs;

        public StackJobPool() {
            mJobs = new Stack();
        }
    }

    public static interface JobPool {

        public abstract void addJob(Object obj);

        public abstract void finishJob(Object obj);

        public abstract Object getNextJob();

        public abstract boolean isEmpty();
    }


    public DaemonAsyncTask() {
        mAutoStopDelayedTime = 2000L;
        mLocker = new byte[0];
    }

    public void addJob(Object obj) {
        mJobPool.addJob(obj);
        byte abyte0[] = mLocker;
        abyte0;
        JVM INSTR monitorenter ;
        mLocker.notifyAll();
        return;
    }

    protected volatile Object doInBackground(Object aobj[]) {
        return doInBackground((Void[])aobj);
    }

    protected transient Void doInBackground(Void avoid[]) {
        boolean flag = false;
_L8:
        if(mNeedStop) goto _L2; else goto _L1
_L1:
        Object obj;
        obj = null;
        if(!mJobPool.isEmpty())
            obj = mJobPool.getNextJob();
        if(obj != null) goto _L4; else goto _L3
_L3:
        if(!mAutoStop || !flag) goto _L5; else goto _L2
_L2:
        return null;
_L5:
        if(mAutoStop) {
            flag = true;
            try {
                Thread.sleep(mAutoStopDelayedTime);
            }
            catch(InterruptedException interruptedexception1) {
                interruptedexception1.printStackTrace();
            }
            continue; /* Loop/switch isn't completed */
        }
        byte abyte0[] = mLocker;
        abyte0;
        JVM INSTR monitorenter ;
        mLocker.wait();
_L6:
        abyte0;
        JVM INSTR monitorexit ;
        continue; /* Loop/switch isn't completed */
        Exception exception;
        exception;
        throw exception;
        InterruptedException interruptedexception;
        interruptedexception;
        interruptedexception.printStackTrace();
        if(true) goto _L6; else goto _L4
_L4:
        flag = false;
        if(needsDoJob(obj)) {
            Pair pair = new Pair(obj, doJob(obj));
            Pair apair[] = new Pair[1];
            apair[0] = pair;
            publishProgress(apair);
        }
        mJobPool.finishJob(obj);
        if(true) goto _L8; else goto _L7
_L7:
    }

    protected abstract Object doJob(Object obj);

    protected boolean needsDoJob(Object obj) {
        return true;
    }

    public void setAutoStop(boolean flag) {
        mAutoStop = flag;
    }

    public void setAutoStopDelayedTime(long l) {
        mAutoStopDelayedTime = l;
    }

    public void setJobPool(JobPool jobpool) {
        mJobPool = jobpool;
    }

    public void setLocker(byte abyte0[]) {
        mLocker = abyte0;
    }

    public void stop() {
        mNeedStop = true;
        byte abyte0[] = mLocker;
        abyte0;
        JVM INSTR monitorenter ;
        mLocker.notifyAll();
        return;
    }

    private boolean mAutoStop;
    private long mAutoStopDelayedTime;
    private JobPool mJobPool;
    private byte mLocker[];
    private boolean mNeedStop;

    static  {
        AsyncTask.setDefaultExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
}
