// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.os;


// Referenced classes of package miui.os:
//            ObservableAsyncTask

public abstract class UniqueAsyncTask extends ObservableAsyncTask {

    public UniqueAsyncTask() {
        mId = String.valueOf(super.hashCode());
    }

    public UniqueAsyncTask(String s) {
        mId = s;
    }

    public boolean equals(Object obj) {
        boolean flag;
        if(obj instanceof UniqueAsyncTask)
            flag = mId.equals(((UniqueAsyncTask)obj).mId);
        else
            flag = false;
        return flag;
    }

    public String getId() {
        return mId;
    }

    protected abstract boolean hasEquivalentRunningTasks();

    public int hashCode() {
        return mId.hashCode();
    }

    protected void onPreExecute() {
        super.onPreExecute();
        if(hasEquivalentRunningTasks())
            cancel(false);
    }

    public void setId(String s) {
        mId = s;
    }

    private String mId;
}
