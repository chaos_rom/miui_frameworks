// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.os;


public interface AsyncTaskObserver {

    public abstract void onCancelled();

    public abstract void onPostExecute(Object obj);

    public abstract void onPreExecute();

    public transient abstract void onProgressUpdate(Object aobj[]);
}
