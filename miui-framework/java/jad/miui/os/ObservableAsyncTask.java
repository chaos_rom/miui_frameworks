// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.os;

import android.os.AsyncTask;
import java.util.*;

// Referenced classes of package miui.os:
//            AsyncTaskObserver

public abstract class ObservableAsyncTask extends AsyncTask {

    public ObservableAsyncTask() {
        mObservers = new ArrayList();
    }

    public void addObserver(AsyncTaskObserver asynctaskobserver) {
        if(asynctaskobserver != null)
            mObservers.add(asynctaskobserver);
    }

    protected void onCancelled() {
        super.onCancelled();
        List list = mObservers;
        list;
        JVM INSTR monitorenter ;
        for(Iterator iterator = mObservers.iterator(); iterator.hasNext(); ((AsyncTaskObserver)iterator.next()).onCancelled());
        break MISSING_BLOCK_LABEL_52;
        Exception exception;
        exception;
        throw exception;
        list;
        JVM INSTR monitorexit ;
    }

    protected void onPostExecute(Object obj) {
        super.onPostExecute(obj);
        List list = mObservers;
        list;
        JVM INSTR monitorenter ;
        for(Iterator iterator = mObservers.iterator(); iterator.hasNext(); ((AsyncTaskObserver)iterator.next()).onPostExecute(obj));
        break MISSING_BLOCK_LABEL_57;
        Exception exception;
        exception;
        throw exception;
        list;
        JVM INSTR monitorexit ;
    }

    protected void onPreExecute() {
        super.onPreExecute();
        List list = mObservers;
        list;
        JVM INSTR monitorenter ;
        for(Iterator iterator = mObservers.iterator(); iterator.hasNext(); ((AsyncTaskObserver)iterator.next()).onPreExecute());
        break MISSING_BLOCK_LABEL_52;
        Exception exception;
        exception;
        throw exception;
        list;
        JVM INSTR monitorexit ;
    }

    protected transient void onProgressUpdate(Object aobj[]) {
        super.onProgressUpdate(aobj);
        List list = mObservers;
        list;
        JVM INSTR monitorenter ;
        for(Iterator iterator = mObservers.iterator(); iterator.hasNext(); ((AsyncTaskObserver)iterator.next()).onProgressUpdate(aobj));
        break MISSING_BLOCK_LABEL_57;
        Exception exception;
        exception;
        throw exception;
        list;
        JVM INSTR monitorexit ;
    }

    public void removeObserver(AsyncTaskObserver asynctaskobserver) {
        if(asynctaskobserver != null)
            mObservers.remove(asynctaskobserver);
    }

    private List mObservers;
}
