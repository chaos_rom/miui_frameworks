// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.v5.view;

import android.graphics.drawable.Drawable;

public interface EditModeTitleBar {

    public abstract CharSequence getButtonText(int i);

    public abstract int getButtonVisiblity(int i);

    public abstract CharSequence getTitle();

    public abstract void setBackground(int i);

    public abstract void setBackground(Drawable drawable);

    public abstract void setButtonText(int i, int j);

    public abstract void setButtonText(int i, CharSequence charsequence);

    public abstract void setButtonVisibility(int i, int j);

    public abstract void setTitle(int i);

    public abstract void setTitle(CharSequence charsequence);
}
