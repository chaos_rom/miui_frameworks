// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.v5.view;

import android.view.*;

public class MiuiActionMode extends ActionMode {
    public static interface ActionModeListener {

        public abstract void onActionModeFinished(MiuiActionMode miuiactionmode);

        public abstract void onActionModeStarted(MiuiActionMode miuiactionmode);
    }


    public MiuiActionMode() {
        mActive = false;
    }

    public void finish() {
        mActive = false;
        mCallback = null;
        if(mActionModeListener != null)
            mActionModeListener.onActionModeFinished(this);
    }

    public View getCustomView() {
        return null;
    }

    public Menu getMenu() {
        return null;
    }

    public MenuInflater getMenuInflater() {
        return null;
    }

    public CharSequence getSubtitle() {
        return null;
    }

    public CharSequence getTitle() {
        return null;
    }

    public void invalidate() {
    }

    public boolean isActive() {
        return mActive;
    }

    public void setActionModeListener(ActionModeListener actionmodelistener) {
        mActionModeListener = actionmodelistener;
    }

    public void setCustomView(View view) {
    }

    public void setSubtitle(int i) {
    }

    public void setSubtitle(CharSequence charsequence) {
    }

    public void setTitle(int i) {
    }

    public void setTitle(CharSequence charsequence) {
    }

    public void start(android.view.ActionMode.Callback callback) {
        if(mCallback != null)
            finish();
        mActive = true;
        mCallback = callback;
        if(mActionModeListener != null)
            mActionModeListener.onActionModeStarted(this);
    }

    public boolean tryToFinish() {
        finish();
        return true;
    }

    private ActionModeListener mActionModeListener;
    private boolean mActive;
    protected android.view.ActionMode.Callback mCallback;
}
