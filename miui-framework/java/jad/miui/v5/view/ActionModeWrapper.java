// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.v5.view;

import android.animation.Animator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.*;
import android.widget.RelativeLayout;
import miui.util.UiUtils;
import miui.v5.widget.menubar.*;

// Referenced classes of package miui.v5.view:
//            EditableActionMode, MiuiActionMode

public class ActionModeWrapper extends RelativeLayout
    implements MiuiActionMode.ActionModeListener {
    private static class AnimatorModeCallback
        implements android.view.ActionMode.Callback {

        public boolean onActionItemClicked(ActionMode actionmode, MenuItem menuitem) {
            return true;
        }

        public boolean onCreateActionMode(ActionMode actionmode, Menu menu) {
            if(mStartAnimator != null)
                mStartAnimator.start();
            if(mListener != null)
                mListener.onActionModeStarted((MiuiActionMode)actionmode);
            return true;
        }

        public void onDestroyActionMode(ActionMode actionmode) {
            if(mFinishAnimator != null)
                mFinishAnimator.start();
            if(mListener != null)
                mListener.onActionModeFinished((MiuiActionMode)actionmode);
        }

        public boolean onPrepareActionMode(ActionMode actionmode, Menu menu) {
            return false;
        }

        private final Animator mFinishAnimator;
        private final MiuiActionMode.ActionModeListener mListener;
        private final Animator mStartAnimator;

        public AnimatorModeCallback(MiuiActionMode.ActionModeListener actionmodelistener, Animator animator, Animator animator1) {
            mListener = actionmodelistener;
            mStartAnimator = animator;
            mFinishAnimator = animator1;
        }
    }

    static class ActionModeImpl extends MiuiActionMode {

        public void finish() {
            if(super.mCallback != null)
                super.mCallback.onDestroyActionMode(this);
            super.finish();
        }

        public void start(android.view.ActionMode.Callback callback) {
            super.start(callback);
            if(callback != null)
                callback.onCreateActionMode(this, null);
        }

        ActionModeImpl() {
        }
    }


    public ActionModeWrapper(Context context) {
        this(context, null);
    }

    public ActionModeWrapper(Context context, AttributeSet attributeset) {
        this(context, attributeset, 0);
    }

    public ActionModeWrapper(Context context, AttributeSet attributeset, int i) {
        super(context, attributeset, i);
    }

    private MiuiActionMode createEditableActionMode(View view, android.view.ActionMode.Callback callback) {
        EditableActionMode editableactionmode;
        if(view != null) {
            if(mEditModeMenuBar == null) {
                ensureViews();
                mEditModeMenuBar = new MenuBar(mContext);
                IconMenuBarPresenter iconmenubarpresenter = new IconMenuBarPresenter(mContext, 0x603005b, 0x603005c, 0x603005d);
                iconmenubarpresenter.setMoreIconDrawable(UiUtils.getDrawable(mContext, 0x6010061));
                iconmenubarpresenter.setPrimaryMaskDrawable(UiUtils.getDrawable(mContext, 0x601006f));
                mEditModeMenuBar.addMenuPresenter(iconmenubarpresenter);
                iconmenubarpresenter.getMenuView(mBottomContainer);
                mMenuPresenter = iconmenubarpresenter;
            }
            editableactionmode = new EditableActionMode(mContext, mTopContainer, mEditModeMenuBar);
        } else {
            editableactionmode = null;
        }
        return editableactionmode;
    }

    private void ensureViews() {
        if(mTopContainer == null) {
            mTopContainer = (ViewGroup)findViewById(0x60b0096);
            mBottomContainer = (ViewGroup)findViewById(0x60b0095);
        }
    }

    public boolean dispatchKeyEvent(KeyEvent keyevent) {
        boolean flag = true;
        if(keyevent.getKeyCode() == 4 && keyevent.getAction() == flag && mActionMode != null)
            mActionMode.tryToFinish();
        else
            flag = super.dispatchKeyEvent(keyevent);
        return flag;
    }

    public void finishActionMode() {
        if(mActionMode != null)
            mActionMode.finish();
    }

    public boolean isActionModeActive() {
        boolean flag;
        if(mActionMode != null)
            flag = true;
        else
            flag = false;
        return flag;
    }

    public void onActionModeFinished(MiuiActionMode miuiactionmode) {
        mActionMode = null;
        if(mActionModeListener != null)
            mActionModeListener.onActionModeFinished(miuiactionmode);
    }

    public void onActionModeStarted(MiuiActionMode miuiactionmode) {
        mActionMode = miuiactionmode;
        if(mActionModeListener != null)
            mActionModeListener.onActionModeStarted(miuiactionmode);
    }

    public void setActionModeListener(MiuiActionMode.ActionModeListener actionmodelistener) {
        mActionModeListener = actionmodelistener;
    }

    public ActionMode startActionModeForChild(View view, android.view.ActionMode.Callback callback) {
        Object obj;
        if(mActionMode != null) {
            obj = null;
        } else {
            obj = createEditableActionMode(view, callback);
            if(obj != null) {
                ((MiuiActionMode) (obj)).setActionModeListener(this);
                ((MiuiActionMode) (obj)).start(callback);
            } else {
                obj = super.startActionModeForChild(view, callback);
            }
        }
        return ((ActionMode) (obj));
    }

    public ActionMode startAnimatorActionMode(Animator animator, Animator animator1, MiuiActionMode.ActionModeListener actionmodelistener) {
        Object obj;
        if(mActionMode != null) {
            obj = null;
        } else {
            obj = new ActionModeImpl();
            ((MiuiActionMode) (obj)).setActionModeListener(this);
            ((ActionModeImpl) (obj)).start(new AnimatorModeCallback(actionmodelistener, animator, animator1));
            mActionMode = ((MiuiActionMode) (obj));
        }
        return ((ActionMode) (obj));
    }

    private MiuiActionMode mActionMode;
    private MiuiActionMode.ActionModeListener mActionModeListener;
    private ViewGroup mBottomContainer;
    private MenuBar mEditModeMenuBar;
    private MenuBarPresenter mMenuPresenter;
    private ViewGroup mTopContainer;
}
