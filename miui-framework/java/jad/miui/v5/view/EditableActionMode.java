// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.v5.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.*;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import miui.v5.widget.Views;
import miui.v5.widget.menubar.MenuBar;
import miui.v5.widget.menubar.MenuBarItem;

// Referenced classes of package miui.v5.view:
//            MiuiActionMode, EditModeTitleBar

public class EditableActionMode extends MiuiActionMode
    implements miui.v5.widget.menubar.MenuBar.Callback, EditModeTitleBar {
    private static class TitleBarImpl
        implements EditModeTitleBar, android.view.View.OnClickListener {

        private TextView getActionView(int i) {
            TextView textview;
            if(i == mActionViews[0].getId())
                textview = mActionViews[0];
            else
            if(i == mActionViews[1].getId())
                textview = mActionViews[1];
            else
                throw new IllegalArgumentException((new StringBuilder()).append("No ActionView for id=").append(i).toString());
            return textview;
        }

        public void attach() {
            if(mContainer.getParent() == null) {
                mParent.addView(mContainer);
                mContainer.startAnimation(AnimationUtils.loadAnimation(mContext, 0x604001c));
            }
        }

        public void detach() {
            Animation animation = AnimationUtils.loadAnimation(mContext, 0x604001d);
            animation.setAnimationListener(mDetachListener);
            mContainer.startAnimation(animation);
        }

        public CharSequence getButtonText(int i) {
            return getActionView(i).getText();
        }

        public int getButtonVisiblity(int i) {
            return getActionView(i).getVisibility();
        }

        public CharSequence getTitle() {
            return mTitleView.getText();
        }

        public void onClick(View view) {
            if(view != mActionViews[0]) goto _L2; else goto _L1
_L1:
            mMenuBar.performItemAction(mActionItems[0], 0);
_L4:
            return;
_L2:
            if(view == mActionViews[1])
                mMenuBar.performItemAction(mActionItems[1], 0);
            if(true) goto _L4; else goto _L3
_L3:
        }

        public void setBackground(int i) {
            mContainer.setBackgroundResource(i);
        }

        public void setBackground(Drawable drawable) {
            mContainer.setBackground(drawable);
        }

        public void setButtonText(int i, int j) {
            getActionView(i).setText(j);
        }

        public void setButtonText(int i, CharSequence charsequence) {
            getActionView(i).setText(charsequence);
        }

        public void setButtonVisibility(int i, int j) {
            getActionView(i).setVisibility(j);
        }

        public void setTitle(int i) {
            mTitleView.setText(i);
        }

        public void setTitle(CharSequence charsequence) {
            mTitleView.setText(charsequence);
        }

        private final MenuItem mActionItems[] = new MenuItem[2];
        private final TextView mActionViews[] = new TextView[2];
        final ViewGroup mContainer;
        private final Context mContext;
        private final android.view.animation.Animation.AnimationListener mDetachListener = new android.view.animation.Animation.AnimationListener() {

            public void onAnimationEnd(Animation animation) {
                Views.detach(mContainer);
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }

            final TitleBarImpl this$0;

                 {
                    this$0 = TitleBarImpl.this;
                    super();
                }
        };
        private final MenuBar mMenuBar;
        final ViewGroup mParent;
        private final TextView mTitleView;

        public TitleBarImpl(ViewGroup viewgroup, MenuBar menubar) {
            mParent = viewgroup;
            mContext = viewgroup.getContext();
            mMenuBar = menubar;
            ViewGroup viewgroup1 = (ViewGroup)Views.inflate(mContext, 0x6030050, viewgroup, false);
            mContainer = viewgroup1;
            mTitleView = (TextView)viewgroup1.findViewById(0x1020016);
            mActionViews[0] = (TextView)viewgroup1.findViewById(0x1020019);
            mActionViews[0].setOnClickListener(this);
            mActionViews[1] = (TextView)viewgroup1.findViewById(0x102001a);
            mActionViews[1].setOnClickListener(this);
            mActionItems[0] = new MenuBarItem(mMenuBar, mActionViews[0].getId(), 0, "");
            mActionItems[1] = new MenuBarItem(mMenuBar, mActionViews[1].getId(), 0, "");
        }
    }


    EditableActionMode(Context context, ViewGroup viewgroup, MenuBar menubar) {
        mContext = context;
        mMenuBar = menubar;
        mMenuBar.setCallback(this);
        mTitleBar = new TitleBarImpl(viewgroup, mMenuBar);
    }

    public void finish() {
        mMenuBar.close();
        mMenuBar.setCallback(null);
        mTitleBar.detach();
        super.finish();
    }

    public CharSequence getButtonText(int i) {
        return mTitleBar.getButtonText(i);
    }

    public int getButtonVisiblity(int i) {
        return mTitleBar.getButtonVisiblity(i);
    }

    public Menu getMenu() {
        return mMenuBar;
    }

    public MenuInflater getMenuInflater() {
        return new MenuInflater(mContext);
    }

    public CharSequence getTitle() {
        return mTitleBar.getTitle();
    }

    public void invalidate() {
        if(super.mCallback == null)
            mMenuBar.clear();
        else
            mMenuBar.open();
    }

    public boolean onCreateMenuBarPanel(Menu menu) {
        boolean flag;
        if(super.mCallback != null)
            flag = super.mCallback.onCreateActionMode(this, menu);
        else
            flag = false;
        return flag;
    }

    public void onMenuBarPanelClose(Menu menu) {
        if(super.mCallback != null) {
            super.mCallback.onDestroyActionMode(this);
            super.mCallback = null;
        }
    }

    public boolean onMenuBarPanelItemSelected(Menu menu, MenuItem menuitem) {
        boolean flag;
        if(super.mCallback != null)
            flag = super.mCallback.onActionItemClicked(this, menuitem);
        else
            flag = false;
        return flag;
    }

    public void onMenuBarPanelModeChange(Menu menu, int i) {
    }

    public void onMenuBarPanelOpen(Menu menu) {
    }

    public boolean onPrepareMenuBarPanel(Menu menu) {
        boolean flag;
        if(super.mCallback != null)
            flag = super.mCallback.onPrepareActionMode(this, menu);
        else
            flag = false;
        return flag;
    }

    public void setBackground(int i) {
        mTitleBar.setBackground(i);
    }

    public void setBackground(Drawable drawable) {
        mTitleBar.setBackground(drawable);
    }

    public void setButtonText(int i, int j) {
        mTitleBar.setButtonText(i, j);
    }

    public void setButtonText(int i, CharSequence charsequence) {
        mTitleBar.setButtonText(i, charsequence);
    }

    public void setButtonVisibility(int i, int j) {
        mTitleBar.setButtonVisibility(i, j);
    }

    public void setTitle(int i) {
        mTitleBar.setTitle(i);
    }

    public void setTitle(CharSequence charsequence) {
        mTitleBar.setTitle(charsequence);
    }

    public void start(android.view.ActionMode.Callback callback) {
        super.start(callback);
        mTitleBar.attach();
        mMenuBar.reopen();
    }

    public boolean tryToFinish() {
        boolean flag = false;
        if(!mMenuBar.expand(false))
            flag = super.tryToFinish();
        return flag;
    }

    static final int ACTION_NUM = 2;
    private final Context mContext;
    private final MenuBar mMenuBar;
    private final TitleBarImpl mTitleBar;
}
