// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.v5.app;

import android.widget.AdapterView;

// Referenced classes of package miui.v5.app:
//            LayoutObserver

public class LayoutObservers {
    private static class ListViewLayoutObserver
        implements LayoutObserver {

        public boolean isContentFilled() {
            boolean flag;
            if(mListView.getCount() > mListView.getChildCount())
                flag = true;
            else
                flag = false;
            return flag;
        }

        private final AdapterView mListView;

        ListViewLayoutObserver(AdapterView adapterview) {
            mListView = adapterview;
        }
    }


    public LayoutObservers() {
    }

    public static LayoutObserver makeLayoutObserverForListView(AdapterView adapterview) {
        return new ListViewLayoutObserver(adapterview);
    }
}
