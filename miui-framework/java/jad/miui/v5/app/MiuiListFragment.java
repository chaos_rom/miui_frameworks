// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.v5.app;

import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import miui.v5.widget.*;

// Referenced classes of package miui.v5.app:
//            MiuiFragment, LayoutObservers, MiuiActivity, LayoutObserver

public class MiuiListFragment extends MiuiFragment {

    public MiuiListFragment() {
    }

    public LayoutObserver createLayoutObserver() {
        LayoutObserver layoutobserver;
        if(mList != null)
            layoutobserver = LayoutObservers.makeLayoutObserverForListView(mList);
        else
            layoutobserver = null;
        return layoutobserver;
    }

    public MotionDetectStrategy createMotionDetectStrategy() {
        MotionDetectStrategy motiondetectstrategy;
        if(mList != null)
            motiondetectstrategy = VerticalMotionStrategies.makeMotionStrategyForList(mList);
        else
            motiondetectstrategy = null;
        return motiondetectstrategy;
    }

    public PageScrollEffect createPageScrollEffect() {
        PageScrollEffect pagescrolleffect;
        if(mList != null)
            pagescrolleffect = PageScrollEffects.makePageScrollEffect(mList);
        else
            pagescrolleffect = null;
        return pagescrolleffect;
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        mList = (AbsListView)view.findViewById(0x102000a);
        if(mList != null)
            ((MiuiActivity)getActivity()).addLayoutObserver(mList);
    }

    protected AbsListView mList;
}
