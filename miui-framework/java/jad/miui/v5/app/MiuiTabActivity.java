// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.v5.app;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.ViewGroup;
import miui.util.UiUtils;
import miui.v5.util.Factory;
import miui.v5.view.MiuiActionMode;
import miui.v5.widget.*;
import miui.v5.widget.menubar.MenuBar;

// Referenced classes of package miui.v5.app:
//            MiuiActivity, MiuiViewPagerItem, LayoutObserver

public class MiuiTabActivity extends MiuiActivity {

    public MiuiTabActivity() {
    }

    private void initTabPlaceholder() {
        Drawable drawable = UiUtils.getDrawable(this, 0x6010035);
        if(drawable != null) {
            ((android.view.ViewGroup.MarginLayoutParams)mViewPager.getLayoutParams()).topMargin = drawable.getIntrinsicHeight();
            mViewPager.requestLayout();
        }
    }

    protected void addViewPagerListeners(miui.v5.widget.Views.ComposedPageChangeListener composedpagechangelistener) {
    }

    protected PageScrollEffect createPageScrollEffect(int i) {
        android.app.Fragment fragment = mTabController.getFragment(i);
        PageScrollEffect pagescrolleffect;
        if(fragment instanceof miui.v5.widget.PageScrollEffect.Creator)
            pagescrolleffect = ((miui.v5.widget.PageScrollEffect.Creator)fragment).createPageScrollEffect();
        else
            pagescrolleffect = null;
        return pagescrolleffect;
    }

    protected MiuiViewPagerItem getCurrentPagerItem() {
        MiuiViewPagerItem miuiviewpageritem = null;
        if(mTabController != null) {
            android.app.Fragment fragment = mTabController.getFragment(mViewPager.getCurrentItem());
            MiuiViewPagerItem miuiviewpageritem1;
            if(fragment instanceof MiuiViewPagerItem)
                miuiviewpageritem1 = (MiuiViewPagerItem)fragment;
            else
                miuiviewpageritem1 = null;
            miuiviewpageritem = miuiviewpageritem1;
        }
        return miuiviewpageritem;
    }

    protected int getLayoutId() {
        return 0x603004b;
    }

    protected LayoutObserver getLayoutObserver() {
        LayoutObserver layoutobserver;
        if(mTabController == null)
            layoutobserver = null;
        else
            layoutobserver = (LayoutObserver)mLayoutObserverFactory.create(Integer.valueOf(mTabController.getSelectedPosition()));
        return layoutobserver;
    }

    public TabController getTabController() {
        return mTabController;
    }

    public void onActionModeFinished(MiuiActionMode miuiactionmode) {
        mTabController.setCooperative(false);
        super.onActionModeFinished(miuiactionmode);
    }

    public void onActionModeStarted(MiuiActionMode miuiactionmode) {
        mTabController.setCooperative(true);
        super.onActionModeStarted(miuiactionmode);
    }

    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        mContainer = (ViewGroup)setMiuiContentView(getLayoutId());
        mTabContainer = (TabContainerLayout)mContainer.findViewById(0x60b0098);
        mViewPager = (CooperativeViewPager)mContainer.findViewById(0x60b0099);
        initTabPlaceholder();
        TranslateIndicator translateindicator = new TranslateIndicator(this, 0x601005e);
        mTabController = new TabController(this, mTabContainer, mViewPager, translateindicator);
        mTabController.setPagerAdapterChangedListener(mAdapterChangedListener);
        miui.v5.widget.Views.ComposedPageChangeListener composedpagechangelistener = new miui.v5.widget.Views.ComposedPageChangeListener();
        addViewPagerListeners(composedpagechangelistener);
        composedpagechangelistener.add(PageScrollEffects.makePageChangeAdapter(mViewPager, mPageEffectFactory));
        composedpagechangelistener.add(new PageChangeAdapter(mViewPager, mPageScrollListener));
        mTabController.setViewPagerListener(composedpagechangelistener);
    }

    protected void onViewPagerAdapterChanged(boolean flag) {
        if(flag)
            updateMenuBar(mViewPager.getCurrentItem());
    }

    protected void onViewPagerReset(ViewPager viewpager, int i, int j) {
        if(i != j)
            updateMenuBar(j);
    }

    protected void onViewPagerScroll(ViewPager viewpager, int i, int j, float f) {
    }

    public void updateMenuBar() {
        updateMenuBar(mViewPager.getCurrentItem());
    }

    protected void updateMenuBar(int i) {
        if(super.mMenuBar != null)
            super.mMenuBar.reopen();
    }

    private final miui.v5.widget.TabController.PagerAdapterChangedListener mAdapterChangedListener = new miui.v5.widget.TabController.PagerAdapterChangedListener() {

        public void onAdapterChanged(ViewPager viewpager, boolean flag) {
            onViewPagerAdapterChanged(flag);
        }

        final MiuiTabActivity this$0;

             {
                this$0 = MiuiTabActivity.this;
                super();
            }
    };
    protected ViewGroup mContainer;
    private final Factory mLayoutObserverFactory = miui.v5.util.Factory.CachedFactory.newFactory(new Factory() {

        public volatile Object create(Object obj) {
            return create((Integer)obj);
        }

        public LayoutObserver create(Integer integer) {
            android.app.Fragment fragment = mTabController.getFragment(integer.intValue());
            LayoutObserver layoutobserver;
            if(fragment instanceof MiuiViewPagerItem)
                layoutobserver = ((MiuiViewPagerItem)fragment).createLayoutObserver();
            else
                layoutobserver = null;
            return layoutobserver;
        }

        final MiuiTabActivity this$0;

             {
                this$0 = MiuiTabActivity.this;
                super();
            }
    });
    private final Factory mPageEffectFactory = miui.v5.util.Factory.CachedFactory.newFactory(new Factory() {

        public volatile Object create(Object obj) {
            return create((Integer)obj);
        }

        public PageScrollEffect create(Integer integer) {
            PageScrollEffect pagescrolleffect;
            if(integer == null)
                pagescrolleffect = null;
            else
                pagescrolleffect = createPageScrollEffect(integer.intValue());
            return pagescrolleffect;
        }

        final MiuiTabActivity this$0;

             {
                this$0 = MiuiTabActivity.this;
                super();
            }
    });
    private final miui.v5.widget.PageChangeAdapter.OnPageScrollListener mPageScrollListener = new miui.v5.widget.PageChangeAdapter.OnPageScrollListener() {

        public void onReset(ViewPager viewpager, int i, int j) {
            onViewPagerReset(viewpager, i, j);
        }

        public void onScroll(ViewPager viewpager, int i, int j, float f) {
            onViewPagerScroll(viewpager, i, j, f);
        }

        final MiuiTabActivity this$0;

             {
                this$0 = MiuiTabActivity.this;
                super();
            }
    };
    private TabContainerLayout mTabContainer;
    protected TabController mTabController;
    private CooperativeViewPager mViewPager;
}
