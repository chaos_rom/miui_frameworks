// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.v5.app;

import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.*;
import miui.v5.util.Factory;
import miui.v5.widget.*;

// Referenced classes of package miui.v5.app:
//            MiuiTabActivity

public class MiuiMovableTabActivity extends MiuiTabActivity {

    public MiuiMovableTabActivity() {
    }

    protected int computTabMaxY(int i) {
        return i - super.mTabController.getTabContainer().getBackgroundHeight() - mTabOverTranslation;
    }

    protected MotionDetectStrategy createMotionDetectStrategy(int i) {
        android.app.Fragment fragment = super.mTabController.getFragment(i);
        MotionDetectStrategy motiondetectstrategy;
        if(fragment instanceof miui.v5.widget.MotionDetectStrategy.Creator)
            motiondetectstrategy = ((miui.v5.widget.MotionDetectStrategy.Creator)fragment).createMotionDetectStrategy();
        else
            motiondetectstrategy = null;
        return motiondetectstrategy;
    }

    protected int getLayoutId() {
        return 0x603004c;
    }

    protected int getTabMaxY() {
        return getWindow().getWindowManager().getDefaultDisplay().getRawHeight() / 3;
    }

    protected int getTabMinY() {
        int i;
        if(isTopPlaceholderEnabled())
            i = 0;
        else
            i = getTopPlaceholderHeight();
        return i;
    }

    public boolean isTabAtTop() {
        boolean flag;
        if(Math.abs(mMotionContainer.getTranslationY() - (float)mTabMinY) < (float)mTabOverTranslation)
            flag = true;
        else
            flag = false;
        return flag;
    }

    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        mMotionContainer = (VerticalMotionFrameLayout)super.mContainer.findViewById(0x60b0097);
        mTabOverTranslation = MiuiViewConfiguration.get(this).getScaledOverDistance();
        mTabMinY = getTabMinY();
        mTabMaxY = getTabMaxY();
        VerticalTranslationController verticaltranslationcontroller = new VerticalTranslationController(this, VerticalMotionStrategies.makeMotionStrategyTabController(super.mTabController, mTabMinY, mMotionStrategyFactory), mTabMinY, mTabMaxY, mTabMinY, mTabMaxY + mTabOverTranslation);
        verticaltranslationcontroller.setTranslateListener(mTranslateListener);
        mMotionContainer.setMotionStrategy(verticaltranslationcontroller);
        Views.setPadding(mMotionContainer, -1, -1, -1, mTabMinY);
        setMotionContainerPosition(false);
    }

    protected void onTabTranslate(float f) {
        if(mFloatingDisplayView != null) {
            int i = 0;
            android.view.ViewGroup.MarginLayoutParams marginlayoutparams = (android.view.ViewGroup.MarginLayoutParams)mFloatingDisplayView.getLayoutParams();
            if(marginlayoutparams != null && marginlayoutparams.topMargin < 0)
                i = marginlayoutparams.topMargin;
            if(i >= 0) {
                Log.w(TAG, (new StringBuilder()).append("FloatingView must be topMargin < 0, topMargin=").append(i).toString());
            } else {
                float f1 = ((f - (float)mTabMaxY) * (float)(-i)) / (float)mTabOverTranslation;
                mFloatingDisplayView.setTranslationY(f1);
            }
        }
    }

    protected void onTabTranslateStateChanged(int i) {
    }

    protected final View replaceView(ViewGroup viewgroup, View view, View view1, android.view.ViewGroup.LayoutParams layoutparams) {
        if(view != null)
            viewgroup.removeView(view);
        if(view1 != null) {
            if(layoutparams != null)
                viewgroup.addView(view1, layoutparams);
            else
                viewgroup.addView(view1);
            mMotionContainer.bringToFront();
            if(mFixedDisplayView != null)
                mFixedDisplayView.bringToFront();
        }
        return view1;
    }

    protected View setFixedDisplayView(int i) {
        return setFixedDisplayView(Views.inflate(this, i, super.mContainer, false), null);
    }

    protected View setFixedDisplayView(View view, android.view.ViewGroup.MarginLayoutParams marginlayoutparams) {
        mFixedDisplayView = replaceView(super.mContainer, mFixedDisplayView, view, marginlayoutparams);
        return mFixedDisplayView;
    }

    protected View setFloatingDisplayView(int i) {
        return setFloatingDisplayView(Views.inflate(this, i, super.mContainer, false), null);
    }

    protected View setFloatingDisplayView(View view, android.view.ViewGroup.MarginLayoutParams marginlayoutparams) {
        int i = getResources().getDimensionPixelSize(0x60a002f);
        if(marginlayoutparams == null) {
            android.view.ViewGroup.LayoutParams layoutparams = view.getLayoutParams();
            if(layoutparams instanceof android.view.ViewGroup.MarginLayoutParams)
                marginlayoutparams = (android.view.ViewGroup.MarginLayoutParams)layoutparams;
        }
        if(marginlayoutparams == null) {
            marginlayoutparams = new android.view.ViewGroup.MarginLayoutParams(-1, -2);
            marginlayoutparams.topMargin = -i;
        } else {
            marginlayoutparams.topMargin = -i;
            Log.i(TAG, "Adjust topMargin of floating view");
        }
        mFloatingDisplayView = replaceView(super.mContainer, mFloatingDisplayView, view, marginlayoutparams);
        return mFloatingDisplayView;
    }

    protected void setMotionContainerPosition(boolean flag) {
        VerticalMotionFrameLayout verticalmotionframelayout = mMotionContainer;
        float f;
        if(flag)
            f = mTabMinY;
        else
            f = mTabMaxY;
        verticalmotionframelayout.setTranslationY(f);
    }

    private static final String TAG = miui/v5/app/MiuiMovableTabActivity.getName();
    protected View mFixedDisplayView;
    protected View mFloatingDisplayView;
    private VerticalMotionFrameLayout mMotionContainer;
    private final Factory mMotionStrategyFactory = miui.v5.util.Factory.CachedFactory.newFactory(new Factory() {

        public volatile Object create(Object obj) {
            return create((Integer)obj);
        }

        public MotionDetectStrategy create(Integer integer) {
            MotionDetectStrategy motiondetectstrategy;
            if(integer == null)
                motiondetectstrategy = null;
            else
                motiondetectstrategy = createMotionDetectStrategy(integer.intValue());
            return motiondetectstrategy;
        }

        final MiuiMovableTabActivity this$0;

             {
                this$0 = MiuiMovableTabActivity.this;
                super();
            }
    });
    private int mTabMaxY;
    private int mTabMinY;
    protected int mTabOverTranslation;
    private final miui.v5.widget.AbsTranslationController.OnTranslateListener mTranslateListener = new miui.v5.widget.AbsTranslationController.OnTranslateListener() {

        public void onTranslate(View view, float f) {
            onTabTranslate(f);
        }

        public void onTranslateStateChanged(int i) {
            onTabTranslateStateChanged(i);
        }

        final MiuiMovableTabActivity this$0;

             {
                this$0 = MiuiMovableTabActivity.this;
                super();
            }
    };

}
