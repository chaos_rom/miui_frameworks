// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.v5.app;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Rect;
import android.view.*;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import miui.v5.view.ActionModeWrapper;
import miui.v5.widget.Views;

public class SearchMode {
    static class SearchViewLayoutListener
        implements android.view.View.OnLayoutChangeListener {

        public void onLayoutChange(View view, int i, int j, int k, int l, int i1, int j1, 
                int k1, int l1) {
            Object obj = view.getTag();
            if(obj != null) {
                Rect rect = (Rect)obj;
                view.setLeft(rect.left);
                view.setTop(rect.top);
                view.setRight(rect.right);
                view.setBottom(rect.bottom);
            } else {
                view.setTag(new Rect(i, j, k, l));
            }
        }

        SearchViewLayoutListener() {
        }
    }

    private static class InputDownAnim
        implements android.animation.ValueAnimator.AnimatorUpdateListener, android.animation.Animator.AnimatorListener {

        private void reset() {
            mBackView.setVisibility(8);
            mBackView.setAlpha(1.0F);
            mSearchView.setVisibility(8);
            SearchMode.setLeft(mEditView, mEditAnimStart);
            if(mAnimView != null) {
                mAnimView.setTranslationY(0.0F);
                if(mAnchorDist != 0 && mAnimView != null)
                    SearchMode.setHeight(mAnimView, mAnimViewHeight);
            }
        }

        public void onAnimationCancel(Animator animator) {
            reset();
            Token token = mToken;
            token.mAnimationCount = -1 + token.mAnimationCount;
        }

        public void onAnimationEnd(Animator animator) {
            reset();
            Token token = mToken;
            token.mAnimationCount = -1 + token.mAnimationCount;
        }

        public void onAnimationRepeat(Animator animator) {
        }

        public void onAnimationStart(Animator animator) {
            boolean flag;
            Token token;
            if(mAlphaView.getVisibility() == 0)
                flag = true;
            else
                flag = false;
            mAlphaViewVisible = flag;
            ((InputMethodManager)mEditView.getContext().getSystemService("input_method")).hideSoftInputFromWindow(mEditView.getWindowToken(), 0);
            token = mToken;
            token.mAnimationCount = 1 + token.mAnimationCount;
        }

        public void onAnimationUpdate(ValueAnimator valueanimator) {
            float f = valueanimator.getAnimatedFraction();
            float f1 = f * (float)mAnchorDist;
            if(mAnimView != null) {
                mAnimView.setTranslationY(f1 + (float)(-mAnchorDist));
                mAnimView.setBottom((int)((float)(mOrignalBottom + mAnchorDist) - f1));
            }
            mSearchView.setTranslationY(f1);
            if(mAlphaViewVisible)
                mAlphaView.setAlpha(1.0F - f);
            SearchMode.setLeft(mEditView, (int)((1.0F - f) * (float)mEditAnimWidth + (float)mEditAnimStart));
            mBackView.setTranslationX(f * (float)(-mBackViewWidth));
            mBackView.setAlpha(1.0F - f);
        }

        private final View mAlphaView;
        private boolean mAlphaViewVisible;
        private final int mAnchorDist;
        private final View mAnimView;
        private final int mAnimViewHeight;
        private final View mBackView;
        private final int mBackViewWidth;
        private final int mEditAnimStart;
        private final int mEditAnimWidth;
        private final View mEditView;
        private final int mOrignalBottom;
        private final View mSearchView;
        private final Token mToken;

        public InputDownAnim(Token token, int i, int j, int k, int l) {
            mToken = token;
            mAnchorDist = i;
            mAnimView = token.mAnimView;
            mSearchView = token.mSearchView;
            mAlphaView = token.mAlphaView;
            mEditView = token.mEditText;
            mBackView = token.mBackView;
            mBackViewWidth = j;
            mEditAnimStart = k;
            mEditAnimWidth = l;
            if(mAnimView != null) {
                mAnimViewHeight = mAnimView.getLayoutParams().height;
                mOrignalBottom = mAnimView.getBottom();
            } else {
                mAnimViewHeight = 0;
                mOrignalBottom = 0;
            }
        }
    }

    private static class InputUpAnim
        implements android.animation.ValueAnimator.AnimatorUpdateListener, android.animation.Animator.AnimatorListener {

        private void reset() {
            mSearchView.setTranslationY(0.0F);
            mAlphaView.setAlpha(1.0F);
            mBackView.setTranslationX(0.0F);
            SearchMode.setLeft(mEditView, mEditAnimWidth + mEditAnimStart);
            if(mAnimView != null) {
                mAnimView.setTranslationY(-mAnchorDist);
                if(mAnchorDist != 0 && mAnimView != null)
                    SearchMode.setHeight(mAnimView, mAnimView.getHeight());
            }
        }

        public void onAnimationCancel(Animator animator) {
            reset();
            Token token = mToken;
            token.mAnimationCount = -1 + token.mAnimationCount;
        }

        public void onAnimationEnd(Animator animator) {
            reset();
            Token token = mToken;
            token.mAnimationCount = -1 + token.mAnimationCount;
        }

        public void onAnimationRepeat(Animator animator) {
        }

        public void onAnimationStart(Animator animator) {
            mSearchView.setVisibility(0);
            mSearchView.setTranslationY(mAnchorDist);
            mBackView.setVisibility(0);
            Token token = mToken;
            token.mAnimationCount = -1 + token.mAnimationCount;
        }

        public void onAnimationUpdate(ValueAnimator valueanimator) {
            float f = valueanimator.getAnimatedFraction();
            float f1 = f * (float)mAnchorDist;
            if(mAnimView != null) {
                mAnimView.setTranslationY(-f1);
                mAnimView.setBottom((int)(f1 + (float)mOrignalBottom));
            }
            SearchMode.setLeft(mEditView, (int)((float)mEditAnimStart + f * (float)mEditAnimWidth));
            mSearchView.setTranslationY((float)mAnchorDist - f1);
            mAlphaView.setAlpha(f);
            mBackView.setAlpha(f);
            mBackView.setTranslationX((float)(-mBackViewWidth) + f * (float)mBackViewWidth);
        }

        private final View mAlphaView;
        private final int mAnchorDist;
        private final View mAnimView;
        private final View mBackView;
        private final int mBackViewWidth;
        private final int mEditAnimStart;
        private final int mEditAnimWidth;
        private final View mEditView;
        private final int mOrignalBottom;
        private final View mSearchView;
        private final Token mToken;

        public InputUpAnim(Token token, int i, int j, int k, int l) {
            mToken = token;
            mAnchorDist = i;
            mAnimView = token.mAnimView;
            mSearchView = token.mSearchView;
            mAlphaView = token.mAlphaView;
            mEditView = token.mEditText;
            mBackView = token.mBackView;
            mBackViewWidth = j;
            mEditAnimStart = k;
            mEditAnimWidth = l;
            if(mAnimView != null)
                mOrignalBottom = mAnimView.getBottom();
            else
                mOrignalBottom = 0;
        }
    }

    private static class FinishActionModeClick
        implements android.view.View.OnClickListener {

        public void onClick(View view) {
            if(!mToken.isAnimationPlaying())
                mToken.finish();
        }

        private final Token mToken;

        public FinishActionModeClick(Token token) {
            mToken = token;
        }
    }

    public static class Token {

        public void finish() {
            if(mActionMode != null)
                mActionMode.finish();
        }

        public boolean isAnimationPlaying() {
            boolean flag;
            if(mAnimationCount > 0)
                flag = true;
            else
                flag = false;
            return flag;
        }

        void setActionMode(ActionMode actionmode) {
            mActionMode = actionmode;
        }

        ActionMode mActionMode;
        public final View mAlphaView;
        final View mAnimView;
        int mAnimationCount;
        public final View mBackView;
        public final EditText mEditText;
        public final View mSearchView;

        Token(View view, EditText edittext, View view1, View view2, View view3) {
            mSearchView = view;
            mEditText = edittext;
            mBackView = view1;
            mAlphaView = view2;
            mAnimView = view3;
        }
    }


    public SearchMode() {
    }

    private static int getAnchor(ViewGroup viewgroup, View view) {
        int j;
        if(view == null) {
            j = 0;
        } else {
            int ai[] = new int[2];
            viewgroup.getLocationInWindow(ai);
            int i = ai[1];
            view.getLocationInWindow(ai);
            j = ai[1] - i;
        }
        return j;
    }

    public static View inflateSearchView(Context context, ViewGroup viewgroup, boolean flag) {
        return Views.inflate(context, 0x6030066, viewgroup, flag);
    }

    static void setHeight(View view, int i) {
        android.view.ViewGroup.LayoutParams layoutparams = view.getLayoutParams();
        if(layoutparams != null) {
            layoutparams.height = i;
            view.requestLayout();
        }
    }

    static void setLeft(View view, int i) {
        view.setLeft(i);
        Object obj = view.getTag();
        if(obj != null)
            ((Rect)obj).left = i;
    }

    public static Token startSearchMode(ActionModeWrapper actionmodewrapper, View view, View view1, CharSequence charsequence, miui.v5.view.MiuiActionMode.ActionModeListener actionmodelistener) {
        boolean flag = false;
        View view2 = actionmodewrapper.findViewById(0x60b00bb);
        if(view2 == null) {
            flag = true;
            view2 = Views.inflate(actionmodewrapper.getContext(), 0x6030065, actionmodewrapper, false);
            actionmodewrapper.addView(view2);
        }
        View view3 = view2.findViewById(0x1020004);
        EditText edittext = (EditText)view2.findViewById(0x1020009);
        View view4 = view2.findViewById(0x102002c);
        if(flag)
            edittext.addOnLayoutChangeListener(new SearchViewLayoutListener());
        Token token = new Token(view2, edittext, view4, view3, view1);
        int i = getAnchor(actionmodewrapper, view);
        int j = Views.getBackgroundWidth(view4);
        int k = ((View)edittext.getParent()).getPaddingLeft();
        int l = j - k;
        float af[] = new float[2];
        af[0] = 0.0F;
        af[1] = 1.0F;
        ValueAnimator valueanimator = ValueAnimator.ofFloat(af);
        valueanimator.setDuration(300L);
        valueanimator.setInterpolator(new DecelerateInterpolator());
        valueanimator.setStartDelay(50L);
        InputUpAnim inputupanim = new InputUpAnim(token, i, j, k, l);
        valueanimator.addListener(inputupanim);
        valueanimator.addUpdateListener(inputupanim);
        float af1[] = new float[2];
        af1[0] = 0.0F;
        af1[1] = 1.0F;
        ValueAnimator valueanimator1 = ValueAnimator.ofFloat(af1);
        valueanimator1.setDuration(300L);
        valueanimator1.setInterpolator(new DecelerateInterpolator());
        InputDownAnim inputdownanim = new InputDownAnim(token, i, j, k, l);
        valueanimator1.addListener(inputdownanim);
        valueanimator1.addUpdateListener(inputdownanim);
        ActionMode actionmode = actionmodewrapper.startAnimatorActionMode(valueanimator, valueanimator1, actionmodelistener);
        if(actionmode != null) {
            FinishActionModeClick finishactionmodeclick = new FinishActionModeClick(token);
            view3.setOnClickListener(finishactionmodeclick);
            view4.setOnClickListener(finishactionmodeclick);
            view2.setVisibility(8);
            edittext.setHint(charsequence);
            edittext.requestFocus();
            ((InputMethodManager)edittext.getContext().getSystemService("input_method")).showSoftInput(edittext, 2);
            token.setActionMode(actionmode);
        } else {
            token = null;
        }
        return token;
    }
}
