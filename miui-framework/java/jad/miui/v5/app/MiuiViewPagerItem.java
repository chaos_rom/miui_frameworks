// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.v5.app;

import android.view.*;

public interface MiuiViewPagerItem
    extends miui.v5.widget.PageScrollEffect.Creator, miui.v5.widget.MotionDetectStrategy.Creator, LayoutObserver.Creator {

    public abstract boolean onCreateMenuBar(Menu menu, MenuInflater menuinflater);

    public abstract void onMenuBarClose(Menu menu);

    public abstract boolean onMenuBarItemSelected(MenuItem menuitem);

    public abstract void onMenuBarModeChange(Menu menu, int i);

    public abstract void onMenuBarOpen(Menu menu);

    public abstract boolean onPrepareMenuBar(Menu menu);
}
