// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.v5.app;

import android.app.ListFragment;
import android.os.Bundle;
import android.view.*;
import android.widget.ListView;
import miui.v5.widget.*;

// Referenced classes of package miui.v5.app:
//            MiuiViewPagerItem, LayoutObservers, MiuiActivity, LayoutObserver

public class MiuiFragmentForList extends ListFragment
    implements MiuiViewPagerItem {

    public MiuiFragmentForList() {
    }

    private ListView peekListView() {
        if(mList == null) {
            View view = getView();
            if(view != null)
                mList = (ListView)view.findViewById(0x102000a);
        }
        return mList;
    }

    public LayoutObserver createLayoutObserver() {
        ListView listview = peekListView();
        LayoutObserver layoutobserver;
        if(listview != null)
            layoutobserver = LayoutObservers.makeLayoutObserverForListView(listview);
        else
            layoutobserver = null;
        return layoutobserver;
    }

    public MotionDetectStrategy createMotionDetectStrategy() {
        ListView listview = peekListView();
        MotionDetectStrategy motiondetectstrategy;
        if(listview != null)
            motiondetectstrategy = VerticalMotionStrategies.makeMotionStrategyForList(listview);
        else
            motiondetectstrategy = null;
        return motiondetectstrategy;
    }

    public PageScrollEffect createPageScrollEffect() {
        ListView listview = peekListView();
        PageScrollEffect pagescrolleffect;
        if(listview != null)
            pagescrolleffect = PageScrollEffects.makePageScrollEffect(listview);
        else
            pagescrolleffect = null;
        return pagescrolleffect;
    }

    public boolean onCreateMenuBar(Menu menu, MenuInflater menuinflater) {
        return false;
    }

    public void onMenuBarClose(Menu menu) {
    }

    public boolean onMenuBarItemSelected(MenuItem menuitem) {
        return false;
    }

    public void onMenuBarModeChange(Menu menu, int i) {
    }

    public void onMenuBarOpen(Menu menu) {
    }

    public boolean onPrepareMenuBar(Menu menu) {
        return true;
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        ListView listview = peekListView();
        if(listview != null)
            ((MiuiActivity)getActivity()).addLayoutObserver(listview);
    }

    private ListView mList;
}
