// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.v5.app;

import android.app.Fragment;
import android.view.*;
import miui.v5.widget.MotionDetectStrategy;
import miui.v5.widget.PageScrollEffect;

// Referenced classes of package miui.v5.app:
//            MiuiViewPagerItem, LayoutObserver

public class MiuiFragment extends Fragment
    implements MiuiViewPagerItem {

    public MiuiFragment() {
    }

    public LayoutObserver createLayoutObserver() {
        return null;
    }

    public MotionDetectStrategy createMotionDetectStrategy() {
        return null;
    }

    public PageScrollEffect createPageScrollEffect() {
        return null;
    }

    public boolean onCreateMenuBar(Menu menu, MenuInflater menuinflater) {
        return false;
    }

    public void onMenuBarClose(Menu menu) {
    }

    public boolean onMenuBarItemSelected(MenuItem menuitem) {
        return false;
    }

    public void onMenuBarModeChange(Menu menu, int i) {
    }

    public void onMenuBarOpen(Menu menu) {
    }

    public boolean onPrepareMenuBar(Menu menu) {
        return true;
    }
}
