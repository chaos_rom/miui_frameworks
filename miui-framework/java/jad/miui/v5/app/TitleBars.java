// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.v5.app;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.view.*;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.internal.view.menu.MenuBuilder;
import miui.v5.widget.Views;

// Referenced classes of package miui.v5.app:
//            TitleBar

public class TitleBars {
    static class TitleBarImpl
        implements TitleBar, android.view.View.OnClickListener {

        public void destroy() {
            ((ViewGroup)mContainer.getParent()).removeView(mContainer);
        }

        public CharSequence getPrimaryText() {
            CharSequence charsequence;
            if(mPrimaryText != null)
                charsequence = mPrimaryText.getText();
            else
                charsequence = null;
            return charsequence;
        }

        public CharSequence getSecondaryText() {
            CharSequence charsequence;
            if(mSecondaryText != null)
                charsequence = mSecondaryText.getText();
            else
                charsequence = null;
            return charsequence;
        }

        public int getType() {
            return mType;
        }

        public View getView() {
            return mContainer;
        }

        public void onClick(View view) {
            int i = view.getId();
            android.view.MenuItem menuitem = mMenuBuilder.findItem(i);
            if(menuitem == null)
                menuitem = mMenuBuilder.add(0, i, 0, "");
            mActivity.onMenuItemSelected(0, menuitem);
        }

        public View setCustomView(int i) {
            View view = null;
            ViewGroup viewgroup = mCustomViewContainer;
            if(viewgroup != null)
                view = setCustomView(Views.inflate(viewgroup.getContext(), i, viewgroup, false), null);
            return view;
        }

        public View setCustomView(View view, android.view.ViewGroup.LayoutParams layoutparams) {
            if(mCustomViewContainer != null) {
                mCustomViewContainer.removeAllViews();
                if(layoutparams != null)
                    mCustomViewContainer.addView(view, layoutparams);
                else
                    mCustomViewContainer.addView(view);
            } else {
                view = null;
            }
            return view;
        }

        public void setCustomViewVisibility(int i) {
            if(mCustomViewContainer != null)
                mCustomViewContainer.setVisibility(i);
        }

        public void setHomeIcon(int i) {
            if(mHomeIcon != null)
                mHomeIcon.setBackgroundResource(i);
        }

        public void setHomeIcon(Drawable drawable) {
            if(mHomeIcon != null)
                mHomeIcon.setBackground(drawable);
        }

        public void setHomeIconVisibility(int i) {
            if(mHomeIcon != null)
                mHomeIcon.setVisibility(i);
        }

        public void setLogoIcon(int i) {
            if(mLogoIcon != null)
                mLogoIcon.setImageResource(i);
        }

        public void setLogoIcon(Drawable drawable) {
            if(mLogoIcon != null)
                mLogoIcon.setImageDrawable(drawable);
        }

        public void setLogoIconVisibility(int i) {
            if(mLogoIcon != null)
                mLogoIcon.setVisibility(i);
        }

        public void setPrimaryText(int i) {
            if(mPrimaryText != null)
                mPrimaryText.setText(i);
        }

        public void setPrimaryText(CharSequence charsequence) {
            if(mPrimaryText != null)
                mPrimaryText.setText(charsequence);
        }

        public void setPrimaryTextVisibility(int i) {
            if(mPrimaryText != null)
                mPrimaryText.setVisibility(i);
        }

        public void setSecondaryText(int i) {
            if(mSecondaryText != null)
                mSecondaryText.setText(i);
        }

        public void setSecondaryText(CharSequence charsequence) {
            if(mSecondaryText != null)
                mSecondaryText.setText(charsequence);
        }

        public void setSecondaryTextVisibility(int i) {
            if(mSecondaryText != null)
                mSecondaryText.setVisibility(i);
        }

        public void setShortcutIcon(int i) {
            if(mShortcutIcon != null)
                mShortcutIcon.setImageResource(i);
        }

        public void setShortcutIcon(Drawable drawable) {
            if(mShortcutIcon != null)
                mShortcutIcon.setImageDrawable(drawable);
        }

        public void setShortcutIconVisibility(int i) {
            if(mShortcutIcon != null)
                mShortcutIcon.setVisibility(i);
        }

        private final Activity mActivity;
        protected final View mContainer;
        protected final ViewGroup mCustomViewContainer;
        protected final ImageView mHomeIcon;
        protected final ImageView mLogoIcon;
        private final Menu mMenuBuilder;
        protected final TextView mPrimaryText;
        protected final TextView mSecondaryText;
        protected final ImageView mShortcutIcon;
        private final int mType;

        TitleBarImpl(Activity activity, View view, int i) {
            mActivity = activity;
            mContainer = view;
            mMenuBuilder = new MenuBuilder(activity);
            mPrimaryText = (TextView)view.findViewById(0x102000c);
            mSecondaryText = (TextView)view.findViewById(0x102000b);
            mHomeIcon = (ImageView)view.findViewById(0x102002c);
            mLogoIcon = (ImageView)view.findViewById(0x1020006);
            mShortcutIcon = (ImageView)view.findViewById(0x1020017);
            mCustomViewContainer = (ViewGroup)view.findViewById(0x102002b);
            mType = i;
            if(mHomeIcon != null)
                mHomeIcon.setOnClickListener(this);
            if(mShortcutIcon != null)
                mShortcutIcon.setOnClickListener(this);
            if(mCustomViewContainer != null)
                mCustomViewContainer.setOnClickListener(this);
        }
    }


    public TitleBars() {
    }

    public static TitleBar create(Activity activity, int i, int j) {
        return new TitleBarImpl(activity, activity.findViewById(i), j);
    }

    public static TitleBar create(Activity activity, View view, int i) {
        return new TitleBarImpl(activity, view, i);
    }

    public static TitleBar inflate(Activity activity, int i, ViewGroup viewgroup) {
        i;
        JVM INSTR tableswitch 0 2: default 28
    //                   0 55
    //                   1 84
    //                   2 90;
           goto _L1 _L2 _L3 _L4
_L1:
        throw new IllegalArgumentException((new StringBuilder()).append("Bad TitleBar type. type=").append(i).toString());
_L2:
        int j = 0x6030056;
_L6:
        View view = Views.inflate(viewgroup.getContext(), j, viewgroup, false);
        viewgroup.addView(view);
        return create(activity, view, i);
_L3:
        j = 0x6030055;
        continue; /* Loop/switch isn't completed */
_L4:
        j = 0x603006d;
        if(true) goto _L6; else goto _L5
_L5:
    }
}
