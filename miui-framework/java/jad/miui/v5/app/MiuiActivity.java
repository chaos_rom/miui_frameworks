// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.v5.app;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.*;
import android.widget.FrameLayout;
import android.widget.TextView;
import miui.util.UiUtils;
import miui.v5.view.ActionModeWrapper;
import miui.v5.view.MiuiActionMode;
import miui.v5.widget.Views;
import miui.v5.widget.menubar.IconMenuBarPresenter;
import miui.v5.widget.menubar.MenuBar;

// Referenced classes of package miui.v5.app:
//            LayoutObserver, MiuiViewPagerItem, TitleBar, TitleBars, 
//            SearchMode

public class MiuiActivity extends Activity
    implements miui.v5.view.MiuiActionMode.ActionModeListener, miui.v5.widget.menubar.MenuBar.Callback {

    public MiuiActivity() {
    }

    private void initBottomBarTopLine() {
        android.view.ViewGroup.MarginLayoutParams marginlayoutparams = (android.view.ViewGroup.MarginLayoutParams)mBottomBarTopLine.getLayoutParams();
        marginlayoutparams.bottomMargin = getBottomPlaceholderHeight();
        marginlayoutparams.height = Views.getBackgroundHeight(mBottomBarTopLine);
        mBottomBarTopLine.requestLayout();
    }

    private void setBottomBarTopLineVisible(boolean flag) {
        View view = mBottomBarTopLine;
        int i;
        if(flag)
            i = 0;
        else
            i = 8;
        view.setVisibility(i);
    }

    private void updateContentBorder(boolean flag) {
        boolean flag1 = false;
        if(flag) {
            LayoutObserver layoutobserver = getLayoutObserver();
            if(layoutobserver != null && layoutobserver.isContentFilled())
                flag1 = true;
            else
                flag1 = false;
        }
        setBottomBarTopLineVisible(flag1);
    }

    public void addLayoutObserver(View view) {
        view.removeOnLayoutChangeListener(mLayoutListener);
        view.addOnLayoutChangeListener(mLayoutListener);
        updateContentBorder();
    }

    protected int getBottomPlaceholderHeight() {
        Drawable drawable = UiUtils.getDrawable(this, 0x6010033);
        int i;
        if(drawable != null)
            i = drawable.getIntrinsicHeight();
        else
            i = 0;
        return i;
    }

    protected MiuiViewPagerItem getCurrentPagerItem() {
        return null;
    }

    protected LayoutObserver getLayoutObserver() {
        return null;
    }

    public MenuBar getMenuBar() {
        return mMenuBar;
    }

    public TitleBar getTitleBar() {
        return mTitleBar;
    }

    protected int getTopPlaceholderHeight() {
        Drawable drawable = UiUtils.getDrawable(this, 0x6010032);
        int i;
        if(drawable != null)
            i = drawable.getIntrinsicHeight();
        else
            i = 0;
        return i;
    }

    public ViewGroup getWrapperView() {
        return mWrapper;
    }

    protected void initMenuBar(ViewGroup viewgroup) {
        if(mMenuBar == null) {
            mMenuBar = new MenuBar(this);
            mMenuBar.setCallback(this);
            mIconMenuBarPresenter = new IconMenuBarPresenter(this, 0x6030057, 0x6030058, 0x6030059);
            mIconMenuBarPresenter.setMoreIconDrawable(UiUtils.getDrawable(this, 0x6010061));
            mIconMenuBarPresenter.setPrimaryMaskDrawable(UiUtils.getDrawable(this, 0x601006f));
            mMenuBar.addMenuPresenter(mIconMenuBarPresenter);
            mIconMenuBarPresenter.getMenuView(mBottomBarContainer);
        }
    }

    protected boolean isBottomPlaceholderEnabled() {
        boolean flag;
        if(((android.view.ViewGroup.MarginLayoutParams)mContentContainer.getLayoutParams()).bottomMargin > 0)
            flag = true;
        else
            flag = false;
        return flag;
    }

    protected boolean isTopPlaceholderEnabled() {
        boolean flag;
        if(((android.view.ViewGroup.MarginLayoutParams)mContentContainer.getLayoutParams()).topMargin > 0)
            flag = true;
        else
            flag = false;
        return flag;
    }

    public void onActionModeFinished(MiuiActionMode miuiactionmode) {
    }

    public void onActionModeStarted(MiuiActionMode miuiactionmode) {
    }

    public void onBackPressed() {
        if(mMenuBar == null || !mMenuBar.expand(false))
            super.onBackPressed();
    }

    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(0x603004a);
        mWrapper = (ActionModeWrapper)findViewById(0x60b0093);
        mWrapper.setActionModeListener(this);
        mContentContainer = (FrameLayout)mWrapper.findViewById(0x60b0094);
        mBottomBarContainer = (FrameLayout)mWrapper.findViewById(0x60b0095);
        mBottomBarContainer.setOnHierarchyChangeListener(mBottomHierarchyChangeListener);
        mTitleBarContainer = (ViewGroup)mWrapper.findViewById(0x60b009b);
        mBottomBarTopLine = mWrapper.findViewById(0x60b00a3);
        initBottomBarTopLine();
        initMenuBar(mBottomBarContainer);
        setTopPlaceholderEnabled(UiUtils.getBoolean(this, 0x6010034));
    }

    protected boolean onCreateMenuBar(Menu menu) {
        return false;
    }

    public boolean onCreateMenuBarPanel(Menu menu) {
        boolean flag = onCreateMenuBar(menu);
        MiuiViewPagerItem miuiviewpageritem = getCurrentPagerItem();
        if(miuiviewpageritem != null)
            flag |= miuiviewpageritem.onCreateMenuBar(menu, getMenuInflater());
        return flag;
    }

    public boolean onKeyUp(int i, KeyEvent keyevent) {
        boolean flag;
        if(i == 82 && mWrapper.isActionModeActive())
            flag = true;
        else
            flag = super.onKeyUp(i, keyevent);
        return flag;
    }

    protected void onMenuBarClose(Menu menu) {
        updateBottomPlaceholder();
    }

    protected boolean onMenuBarItemSelected(MenuItem menuitem) {
        return false;
    }

    protected void onMenuBarModeChange(Menu menu, int i) {
    }

    protected void onMenuBarOpen(Menu menu) {
        updateBottomPlaceholder();
    }

    public void onMenuBarPanelClose(Menu menu) {
        MiuiViewPagerItem miuiviewpageritem = getCurrentPagerItem();
        if(miuiviewpageritem != null)
            miuiviewpageritem.onMenuBarClose(menu);
        onMenuBarClose(menu);
    }

    public boolean onMenuBarPanelItemSelected(Menu menu, MenuItem menuitem) {
        boolean flag = false;
        MiuiViewPagerItem miuiviewpageritem = getCurrentPagerItem();
        if(miuiviewpageritem != null)
            flag = miuiviewpageritem.onMenuBarItemSelected(menuitem);
        return flag | onMenuBarItemSelected(menuitem);
    }

    public void onMenuBarPanelModeChange(Menu menu, int i) {
        MiuiViewPagerItem miuiviewpageritem = getCurrentPagerItem();
        if(miuiviewpageritem != null)
            miuiviewpageritem.onMenuBarModeChange(menu, i);
        onMenuBarModeChange(menu, i);
    }

    public void onMenuBarPanelOpen(Menu menu) {
        MiuiViewPagerItem miuiviewpageritem = getCurrentPagerItem();
        if(miuiviewpageritem != null)
            miuiviewpageritem.onMenuBarOpen(menu);
        onMenuBarOpen(menu);
    }

    protected boolean onPrepareMenuBar(Menu menu) {
        return true;
    }

    public boolean onPrepareMenuBarPanel(Menu menu) {
        boolean flag = onPrepareMenuBar(menu);
        MiuiViewPagerItem miuiviewpageritem = getCurrentPagerItem();
        if(miuiviewpageritem != null)
            flag |= miuiviewpageritem.onPrepareMenuBar(menu);
        return flag;
    }

    protected void onStart() {
        super.onStart();
        if(mMenuBar != null)
            mMenuBar.open();
    }

    protected void onStop() {
        super.onStop();
        if(mMenuBar != null)
            mMenuBar.close();
    }

    protected void removeAllTitleViews() {
        mTitleBarContainer.removeAllViews();
    }

    public void removeLayoutObserver(View view) {
        view.removeOnLayoutChangeListener(mLayoutListener);
    }

    protected void setBackground(int i) {
        mWrapper.setBackgroundResource(i);
    }

    protected void setBackground(Drawable drawable) {
        mWrapper.setBackground(drawable);
    }

    void setBottomPlaceholderEnabled(boolean flag) {
        int i;
        android.view.ViewGroup.MarginLayoutParams marginlayoutparams;
        i = getBottomPlaceholderHeight();
        marginlayoutparams = (android.view.ViewGroup.MarginLayoutParams)mContentContainer.getLayoutParams();
        if(marginlayoutparams.bottomMargin != 0 && marginlayoutparams.bottomMargin != i)
            throw new IllegalStateException((new StringBuilder()).append(mContentContainer).append(" Cannot be set margin external!").toString());
        if(!flag || marginlayoutparams.bottomMargin != 0) goto _L2; else goto _L1
_L1:
        marginlayoutparams.bottomMargin = i;
        mContentContainer.requestLayout();
        updateContentBorder(true);
_L4:
        return;
_L2:
        if(!flag && marginlayoutparams.bottomMargin != 0) {
            marginlayoutparams.bottomMargin = 0;
            mContentContainer.requestLayout();
            updateContentBorder(false);
        }
        if(true) goto _L4; else goto _L3
_L3:
    }

    protected View setMiuiContentView(int i) {
        return setMiuiContentView(View.inflate(this, i, null), null);
    }

    protected View setMiuiContentView(View view, android.view.ViewGroup.LayoutParams layoutparams) {
        if(view == null) goto _L2; else goto _L1
_L1:
        if(mDecoratedView != null)
            mContentContainer.removeView(mDecoratedView);
        if(layoutparams != null)
            mContentContainer.addView(view, layoutparams);
        else
            mContentContainer.addView(view);
        mDecoratedView = view;
_L4:
        return mDecoratedView;
_L2:
        if(mDecoratedView != null) {
            mContentContainer.removeView(mDecoratedView);
            mDecoratedView = null;
        }
        if(true) goto _L4; else goto _L3
_L3:
    }

    protected TitleBar setTitleBarType(int i) {
        if(mTitleBar == null) goto _L2; else goto _L1
_L1:
        if(mTitleBar.getType() != i) goto _L4; else goto _L3
_L3:
        TitleBar titlebar = mTitleBar;
_L6:
        return titlebar;
_L4:
        mTitleBarContainer.removeView(mTitleBar.getView());
        mTitleBar = null;
_L2:
        mTitleBar = TitleBars.inflate(this, i, mTitleBarContainer);
        titlebar = mTitleBar;
        if(true) goto _L6; else goto _L5
_L5:
    }

    protected View setTitleView(int i) {
        return setTitleView(Views.inflate(this, i, mTitleBarContainer, false), null);
    }

    protected View setTitleView(View view, android.view.ViewGroup.LayoutParams layoutparams) {
        if(layoutparams != null)
            mTitleBarContainer.addView(view, layoutparams);
        else
            mTitleBarContainer.addView(view);
        return view;
    }

    void setTopPlaceholderEnabled(boolean flag) {
        int i;
        android.view.ViewGroup.MarginLayoutParams marginlayoutparams;
        i = getTopPlaceholderHeight();
        marginlayoutparams = (android.view.ViewGroup.MarginLayoutParams)mContentContainer.getLayoutParams();
        if(marginlayoutparams.topMargin != 0 && marginlayoutparams.topMargin != i)
            throw new IllegalStateException((new StringBuilder()).append(mContentContainer).append(" Cannot be set margin external!").toString());
        if(!flag || marginlayoutparams.topMargin != 0) goto _L2; else goto _L1
_L1:
        marginlayoutparams.topMargin = i;
        mContentContainer.requestLayout();
_L4:
        return;
_L2:
        if(!flag && marginlayoutparams.topMargin != 0) {
            marginlayoutparams.topMargin = 0;
            mContentContainer.requestLayout();
        }
        if(true) goto _L4; else goto _L3
_L3:
    }

    public ActionMode startActionMode(android.view.ActionMode.Callback callback) {
        return mWrapper.startActionMode(callback);
    }

    public SearchMode.Token startSearchMode(View view, View view1, CharSequence charsequence, miui.v5.view.MiuiActionMode.ActionModeListener actionmodelistener) {
        return SearchMode.startSearchMode(mWrapper, view, view1, charsequence, actionmodelistener);
    }

    public SearchMode.Token startSearchMode(View view, View view1, miui.v5.view.MiuiActionMode.ActionModeListener actionmodelistener) {
        CharSequence charsequence;
        if(view != null) {
            View view2 = view.findViewById(0x1020009);
            if(view2 instanceof TextView)
                charsequence = ((TextView)view2).getHint();
            else
                charsequence = null;
        } else {
            charsequence = null;
        }
        return startSearchMode(view, view1, charsequence, actionmodelistener);
    }

    void updateBottomPlaceholder() {
        FrameLayout framelayout = mBottomBarContainer;
        int i = framelayout.getChildCount();
        boolean flag = false;
        int j = 0;
        do {
label0:
            {
                if(j < i) {
                    if(framelayout.getChildAt(j).getVisibility() == 8)
                        break label0;
                    flag = true;
                }
                setBottomPlaceholderEnabled(flag);
                return;
            }
            j++;
        } while(true);
    }

    public void updateContentBorder() {
        updateContentBorder(isBottomPlaceholderEnabled());
    }

    private FrameLayout mBottomBarContainer;
    private View mBottomBarTopLine;
    private final android.view.ViewGroup.OnHierarchyChangeListener mBottomHierarchyChangeListener = new android.view.ViewGroup.OnHierarchyChangeListener() {

        public void onChildViewAdded(View view, View view1) {
            updateBottomPlaceholder();
        }

        public void onChildViewRemoved(View view, View view1) {
            updateBottomPlaceholder();
        }

        final MiuiActivity this$0;

             {
                this$0 = MiuiActivity.this;
                super();
            }
    };
    private FrameLayout mContentContainer;
    private View mDecoratedView;
    IconMenuBarPresenter mIconMenuBarPresenter;
    private final android.view.View.OnLayoutChangeListener mLayoutListener = new android.view.View.OnLayoutChangeListener() {

        public void onLayoutChange(View view, int i, int j, int k, int l, int i1, int j1, 
                int k1, int l1) {
            updateContentBorder();
        }

        final MiuiActivity this$0;

             {
                this$0 = MiuiActivity.this;
                super();
            }
    };
    MenuBar mMenuBar;
    TitleBar mTitleBar;
    private ViewGroup mTitleBarContainer;
    private ActionModeWrapper mWrapper;
}
