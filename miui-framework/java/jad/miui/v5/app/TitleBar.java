// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.v5.app;

import android.graphics.drawable.Drawable;
import android.view.View;

public interface TitleBar {

    public abstract void destroy();

    public abstract CharSequence getPrimaryText();

    public abstract CharSequence getSecondaryText();

    public abstract int getType();

    public abstract View getView();

    public abstract View setCustomView(int i);

    public abstract View setCustomView(View view, android.view.ViewGroup.LayoutParams layoutparams);

    public abstract void setCustomViewVisibility(int i);

    public abstract void setHomeIcon(int i);

    public abstract void setHomeIcon(Drawable drawable);

    public abstract void setHomeIconVisibility(int i);

    public abstract void setLogoIcon(int i);

    public abstract void setLogoIcon(Drawable drawable);

    public abstract void setLogoIconVisibility(int i);

    public abstract void setPrimaryText(int i);

    public abstract void setPrimaryText(CharSequence charsequence);

    public abstract void setPrimaryTextVisibility(int i);

    public abstract void setSecondaryText(int i);

    public abstract void setSecondaryText(CharSequence charsequence);

    public abstract void setSecondaryTextVisibility(int i);

    public abstract void setShortcutIcon(int i);

    public abstract void setShortcutIcon(Drawable drawable);

    public abstract void setShortcutIconVisibility(int i);

    public static final int ACTION_TYPE_CUSTOM = 2;
    public static final int ACTION_TYPE_HOME = 0;
    public static final int ACTION_TYPE_SHORTCUT = 1;
    public static final int TYPE_INVALID = -1;
    public static final int TYPE_LARGE = 1;
    public static final int TYPE_MEDIUM = 2;
    public static final int TYPE_NORMAL;
}
