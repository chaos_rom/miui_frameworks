// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.v5.widget;

import android.view.VelocityTracker;
import android.view.View;

// Referenced classes of package miui.v5.widget:
//            MotionDetectStrategy

public interface MotionDetectListener
    extends MotionDetectStrategy {

    public abstract boolean moveImmediately(View view, int i, int j);

    public abstract boolean onMove(View view, int i, int j, int k, int l);

    public abstract void onMoveCancel(View view, int i, int j);

    public abstract void onMoveFinish(View view, int i, int j, int k, int l, VelocityTracker velocitytracker);

    public abstract void onMoveStart(View view, int i, int j);
}
