// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.v5.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import com.android.internal.view.menu.MenuBuilder;
import com.android.internal.view.menu.MenuView;

public class MenuPanel extends LinearLayout
    implements MenuView {

    public MenuPanel(Context context) {
        super(context);
    }

    public MenuPanel(Context context, AttributeSet attributeset) {
        this(context, attributeset, 0);
    }

    public MenuPanel(Context context, AttributeSet attributeset, int i) {
        super(context, attributeset, i);
    }

    public int getWindowAnimations() {
        return 0;
    }

    public void initialize(MenuBuilder menubuilder) {
    }
}
