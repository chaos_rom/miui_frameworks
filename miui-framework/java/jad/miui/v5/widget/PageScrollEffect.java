// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.v5.widget;

import android.graphics.Rect;
import android.view.*;
import android.widget.AbsListView;
import com.android.internal.util.Predicate;

// Referenced classes of package miui.v5.widget:
//            Views

public interface PageScrollEffect {
    public static abstract class AbsPageScrollEffect
        implements PageScrollEffect {
        protected static class AllVisiblePred
            implements Predicate {

            public boolean apply(View view) {
                boolean flag;
                if(view.getVisibility() == 0 && Views.isIntersectWithRect(view, mParentVisibleRect))
                    flag = true;
                else
                    flag = false;
                return flag;
            }

            public volatile boolean apply(Object obj) {
                return apply((View)obj);
            }

            private final Rect mParentVisibleRect;

            public AllVisiblePred(Rect rect) {
                mParentVisibleRect = rect;
            }
        }

        protected static class AnyVisiblePred
            implements Predicate {

            public boolean apply(View view) {
                boolean flag;
                if(AbsPageScrollEffect.existsEffect(view) && view.getVisibility() == 0 && Views.isIntersectWithRect(view, mParentVisibleRect))
                    flag = true;
                else
                    flag = false;
                return flag;
            }

            public volatile boolean apply(Object obj) {
                return apply((View)obj);
            }

            private final Rect mParentVisibleRect;

            public AnyVisiblePred(Rect rect) {
                mParentVisibleRect = rect;
            }
        }


        protected static boolean chainEffectTypes(ViewGroup viewgroup, int ai[]) {
            if(viewgroup != null && ai != null) goto _L2; else goto _L1
_L1:
            boolean flag = false;
_L4:
            return flag;
_L2:
            flag = true;
            int i = ai.length;
            int j = 0;
            do {
                View view;
                if(j < i) {
label0:
                    {
                        view = viewgroup.findViewById(ai[j]);
                        if(view != null)
                            break label0;
                        flag = false;
                    }
                }
                if(true)
                    continue;
                if(view != viewgroup) {
                    ViewParent viewparent;
                    if(view instanceof AbsListView)
                        setViewEffectType(view, 3);
                    else
                    if(getEffectType(view) < 1)
                        setViewEffectType(view, 1);
                    viewparent = view.getParent();
                    while(viewparent != viewgroup)  {
                        setViewEffectType((View)viewparent, 2);
                        viewparent = viewparent.getParent();
                    }
                }
                j++;
            } while(true);
            if(true) goto _L4; else goto _L3
_L3:
        }

        protected static void clearChain(ViewGroup viewgroup) {
            int i = viewgroup.getChildCount();
            for(int j = 0; j < i; j++) {
                View view = viewgroup.getChildAt(j);
                view.setTag(KEY_EFFECT_TYPE.intValue(), null);
                if(view instanceof ViewGroup)
                    clearChain((ViewGroup)view);
            }

        }

        protected static boolean existsEffect(View view) {
            boolean flag = false;
            Object obj = view.getTag(KEY_EFFECT_TYPE.intValue());
            if(obj != null && ((Integer)obj).intValue() > 0)
                flag = true;
            return flag;
        }

        protected static int getEffectType(View view) {
            Object obj = view.getTag(KEY_EFFECT_TYPE.intValue());
            int i;
            if(obj != null)
                i = ((Integer)obj).intValue();
            else
                i = 0;
            return i;
        }

        protected static void setViewEffectType(View view, int i) {
            view.setTag(KEY_EFFECT_TYPE.intValue(), Integer.valueOf(i));
        }

        public boolean attach() {
            if(!mAttached) goto _L2; else goto _L1
_L1:
            boolean flag = true;
_L4:
            return flag;
_L2:
            flag = false;
            if(mResIds != null) {
                if(chainEffectTypes(mView, mResIds)) {
                    setViewEffectType(mView, 2);
                    flag = true;
                }
            } else {
                setViewEffectType(mView, 3);
                flag = true;
            }
            if(flag)
                mAttached = true;
            if(true) goto _L4; else goto _L3
_L3:
        }

        public boolean detach() {
            clearChain(mView);
            mView.setTag(KEY_EFFECT_TYPE.intValue(), null);
            mAttached = false;
            return true;
        }

        public boolean isAttached() {
            return mAttached;
        }

        public static final int EFFECT_ALL_CHILDREN = 3;
        public static final int EFFECT_ANY_CHILDREN = 2;
        public static final int EFFECT_NONE = 0;
        public static final int EFFECT_ONLY_SELF = 1;
        protected static final Integer KEY_EFFECT_TYPE = Integer.valueOf(0x60b008b);
        private boolean mAttached;
        private final int mResIds[];
        protected final ViewGroup mView;


        public AbsPageScrollEffect(ViewGroup viewgroup, int ai[]) {
            mAttached = false;
            mView = viewgroup;
            mResIds = ai;
        }
    }

    public static interface Creator {

        public abstract PageScrollEffect createPageScrollEffect();
    }


    public abstract boolean attach();

    public abstract boolean detach();

    public abstract boolean isAttached();

    public abstract void scroll(float f, boolean flag);
}
