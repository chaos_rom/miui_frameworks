// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.v5.widget;

import android.animation.*;
import android.content.Context;
import android.view.*;
import android.view.animation.DecelerateInterpolator;
import java.lang.ref.WeakReference;

// Referenced classes of package miui.v5.widget:
//            MotionDetectListener, MiuiViewConfiguration, MotionDetectStrategy

public abstract class AbsTranslationController
    implements MotionDetectListener, android.animation.Animator.AnimatorListener {
    public static interface OnTranslateListener {

        public abstract void onTranslate(View view, float f);

        public abstract void onTranslateStateChanged(int i);
    }

    protected class TranslateAnimationListener
        implements android.animation.ValueAnimator.AnimatorUpdateListener {

        public void onAnimationUpdate(ValueAnimator valueanimator) {
            View view = (View)mViewRef.get();
            if(view != null) {
                float f = ((Float)valueanimator.getAnimatedValue()).floatValue();
                onTranslate(view, (float)mFrom + f * (float)mDelta);
            }
        }

        private final int mDelta;
        private final int mFrom;
        private final WeakReference mViewRef;
        final AbsTranslationController this$0;

        public TranslateAnimationListener(View view, int i, int j) {
            this$0 = AbsTranslationController.this;
            super();
            mViewRef = new WeakReference(view);
            mFrom = i;
            mDelta = j;
        }
    }


    public AbsTranslationController(Context context, MotionDetectStrategy motiondetectstrategy) {
        mListener = motiondetectstrategy;
        ViewConfiguration viewconfiguration = ViewConfiguration.get(context);
        mMinimumVelocity = viewconfiguration.getScaledMinimumFlingVelocity();
        mMaximumVelocity = viewconfiguration.getScaledMaximumFlingVelocity();
        mMaxAnchorDuration = MiuiViewConfiguration.get(context).getMaxAnchorDuration();
    }

    private void fling(View view, int i, int j, int k, boolean flag) {
        if(mAnimator != null) {
            mAnimator.cancel();
            mAnimator = null;
        }
        if(j != 0)
            if(flag) {
                float af[] = new float[2];
                af[0] = 0.0F;
                af[1] = 1.0F;
                mAnimator = ObjectAnimator.ofFloat(af);
                mAnimator.setInterpolator(new DecelerateInterpolator());
                mAnimator.setDuration(getDuration(j, k));
                mAnimator.addListener(this);
                mAnimator.addUpdateListener(new TranslateAnimationListener(view, i, j));
                mAnimator.start();
            } else {
                onTranslate(view, i + j);
            }
        if(mAnimator != null)
            onTranslateStateChanged(2);
        else
            onTranslateStateChanged(0);
    }

    protected abstract int computVelocity(VelocityTracker velocitytracker);

    protected void fling(View view, int i, int j, int k, int l, int i1) {
        int j1 = (int)view.getY();
        fling(view, j1, getAnchorPostion(view, i, j, k, l, i1) - j1, i1, true);
    }

    protected abstract int getAnchorPostion(View view, int i, int j, int k, int l, int i1);

    protected int getDuration(int i, int j) {
        int k = Math.abs(i);
        int l = (-1 + (1000 + Math.abs(j))) / 1000;
        int i1;
        if(l > 0)
            i1 = Math.min(mMaxAnchorDuration, (k * 2) / l);
        else
            i1 = mMaxAnchorDuration;
        return i1;
    }

    protected abstract int getValidMovePosition(View view, int i, int j, int k, int l);

    public boolean isAnimationPlaying() {
        boolean flag;
        if(mAnimator != null)
            flag = true;
        else
            flag = false;
        return flag;
    }

    public boolean isMovable(View view, int i, int j, int k, int l) {
        boolean flag;
        if(mListener != null)
            flag = mListener.isMovable(view, i, j, k, l);
        else
            flag = true;
        return flag;
    }

    public boolean moveImmediately(View view, int i, int j) {
        return false;
    }

    public void onAnimationCancel(Animator animator) {
        onTranslateStateChanged(0);
        mAnimator = null;
    }

    public void onAnimationEnd(Animator animator) {
        onTranslateStateChanged(0);
        mAnimator = null;
    }

    public void onAnimationRepeat(Animator animator) {
    }

    public void onAnimationStart(Animator animator) {
    }

    public boolean onMove(View view, int i, int j, int k, int l) {
        boolean flag = isMovable(view, i, j, k, l);
        if(flag)
            onTranslate(view, getValidMovePosition(view, i, j, k, l));
        return flag;
    }

    public void onMoveCancel(View view, int i, int j) {
        fling(view, i, j, i, j, 0);
    }

    public void onMoveFinish(View view, int i, int j, int k, int l, VelocityTracker velocitytracker) {
        int i1 = computVelocity(velocitytracker);
        if(Math.abs(i1) > mMinimumVelocity)
            fling(view, i, j, k, l, i1);
        else
            fling(view, i, j, k, l, 0);
    }

    public void onMoveStart(View view, int i, int j) {
        if(mAnimator != null) {
            mAnimator.cancel();
            mAnimator = null;
        }
        onTranslateStateChanged(1);
    }

    protected void onTranslate(View view, float f) {
        translate(view, f);
        if(mTranslateListener != null)
            mTranslateListener.onTranslate(view, f);
    }

    protected void onTranslateStateChanged(int i) {
        if(mTranslateListener != null)
            mTranslateListener.onTranslateStateChanged(i);
    }

    public void setTranslateListener(OnTranslateListener ontranslatelistener) {
        mTranslateListener = ontranslatelistener;
    }

    protected abstract void translate(View view, float f);

    static final String TAG = miui/v5/widget/AbsTranslationController.getName();
    public static final int TRANSLATE_STATE_FLING = 2;
    public static final int TRANSLATE_STATE_IDLE = 0;
    public static final int TRANSLATE_STATE_TOUCH = 1;
    private ValueAnimator mAnimator;
    private final MotionDetectStrategy mListener;
    private final int mMaxAnchorDuration;
    protected final int mMaximumVelocity;
    protected final int mMinimumVelocity;
    private OnTranslateListener mTranslateListener;

}
