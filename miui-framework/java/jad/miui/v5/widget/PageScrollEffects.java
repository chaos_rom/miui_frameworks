// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.v5.widget;

import android.graphics.Rect;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.collect.Lists;
import java.util.*;
import miui.v5.util.Factory;

// Referenced classes of package miui.v5.widget:
//            PageChangeAdapter, PageScrollEffect, Views

public class PageScrollEffects {
    private static class DefaultPageScrollEffect extends PageScrollEffect.AbsPageScrollEffect {

        private static int computOffset(int i, int j, int k, float f) {
            int l = (i * j) / k;
            float f1 = f * f;
            float f2 = (float)l + (0.1F - f1 / 0.9F) * (float)j;
            int i1;
            if(f2 > 0.0F)
                i1 = (int)f2;
            else
                i1 = 0;
            return i1;
        }

        static void translateView(ViewGroup viewgroup, int i, int j, float f, boolean flag) {
            Rect rect;
            rect = new Rect();
            Views.getContentRect(viewgroup, rect);
            if(!rect.isEmpty()) goto _L2; else goto _L1
_L1:
            return;
_L2:
            java.util.ArrayList arraylist;
            int k;
            arraylist = Lists.newArrayList();
            k = getEffectType(viewgroup);
            if(k != 3) goto _L4; else goto _L3
_L3:
            Views.collectChildren(viewgroup, new PageScrollEffect.AbsPageScrollEffect.AllVisiblePred(rect), arraylist);
_L5:
            if(!arraylist.isEmpty()) {
                Collections.sort(arraylist, Views.TOP_COMPARATOR);
                int l = -((View)arraylist.get(0)).getTop();
                int i1 = 0x7fffffff;
                int j1 = 0;
                Iterator iterator = arraylist.iterator();
                while(iterator.hasNext())  {
                    View view = (View)iterator.next();
                    if(i1 != view.getTop()) {
                        i1 = view.getTop();
                        int k1 = computOffset(i1 + l, i, j, f);
                        if(flag)
                            j1 = k1;
                        else
                            j1 = -k1;
                    }
                    view.setTranslationX(j1);
                    if(view instanceof ViewGroup)
                        translateView((ViewGroup)view, i, j, f, flag);
                }
            }
            if(true) goto _L1; else goto _L4
_L4:
            if(k == 2)
                Views.collectChildren(viewgroup, new PageScrollEffect.AbsPageScrollEffect.AnyVisiblePred(rect), arraylist);
              goto _L5
        }

        public void scroll(float f, boolean flag) {
            if(super.mView.getVisibility() == 0)
                translateView(super.mView, super.mView.getWidth(), super.mView.getHeight(), f, flag);
        }

        public DefaultPageScrollEffect(ViewGroup viewgroup, int ai[]) {
            super(viewgroup, ai);
        }
    }

    private static class PageScrolledDispatcher
        implements PageChangeAdapter.OnPageScrollListener {

        private void onScrolled(ViewPager viewpager, int i, float f, boolean flag) {
            PageScrollEffect pagescrolleffect = (PageScrollEffect)mFactory.create(Integer.valueOf(i));
            if(pagescrolleffect != null)
                pagescrolleffect.scroll(f, flag);
        }

        public void onReset(ViewPager viewpager, int i, int j) {
            onScrolled(viewpager, j, 1.0F, true);
        }

        public void onScroll(ViewPager viewpager, int i, int j, float f) {
            boolean flag;
            if(i < j)
                flag = true;
            else
                flag = false;
            onScrolled(viewpager, j, f, flag);
        }

        private final Factory mFactory;

        public PageScrolledDispatcher(Factory factory) {
            mFactory = factory;
        }
    }


    public PageScrollEffects() {
    }

    public static android.support.v4.view.ViewPager.OnPageChangeListener makePageChangeAdapter(ViewPager viewpager, Factory factory) {
        PageChangeAdapter pagechangeadapter = new PageChangeAdapter(viewpager);
        pagechangeadapter.setScrolledListener(new PageScrolledDispatcher(factory));
        return pagechangeadapter;
    }

    public static PageScrollEffect makePageScrollEffect(ViewGroup viewgroup) {
        return makePageScrollEffect(viewgroup, null);
    }

    public static PageScrollEffect makePageScrollEffect(ViewGroup viewgroup, int ai[]) {
        return makePageScrollEffect(viewgroup, ai, true);
    }

    public static PageScrollEffect makePageScrollEffect(ViewGroup viewgroup, int ai[], boolean flag) {
        DefaultPageScrollEffect defaultpagescrolleffect = null;
        if(viewgroup != null) {
            defaultpagescrolleffect = new DefaultPageScrollEffect(viewgroup, ai);
            if(flag)
                defaultpagescrolleffect.attach();
        }
        return defaultpagescrolleffect;
    }
}
