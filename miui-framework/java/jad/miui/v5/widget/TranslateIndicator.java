// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.v5.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.ViewGroup;
import android.widget.ImageView;

// Referenced classes of package miui.v5.widget:
//            TabIndicator, TabContainerLayout

public class TranslateIndicator extends ImageView
    implements TabIndicator {

    public TranslateIndicator(Context context, int i) {
        super(context, null, i);
        setScaleType(android.widget.ImageView.ScaleType.CENTER);
    }

    public float apply(int i, float f) {
        if((android.view.ViewGroup.MarginLayoutParams)getLayoutParams() != null)
            setTranslationX(Math.round((f + (float)i) * (float)mTabWidth));
        return 0.0F;
    }

    public void attach(TabContainerLayout tabcontainerlayout) {
        mTabContainer = tabcontainerlayout;
        tabcontainerlayout.getIndicatorContainer().addView(this);
        setTabWidth(tabcontainerlayout.getWidth());
    }

    public void detach() {
        android.view.ViewParent viewparent = getParent();
        if(viewparent instanceof ViewGroup)
            ((ViewGroup)viewparent).removeView(this);
    }

    protected void onMeasure(int i, int j) {
        super.onMeasure(android.view.View.MeasureSpec.makeMeasureSpec(mTabContainer.getTabWidth(), 0x40000000), j);
    }

    public void setIndicator(Drawable drawable) {
        setImageDrawable(drawable);
    }

    public void setTabWidth(int i) {
        mTabWidth = i;
        android.widget.FrameLayout.LayoutParams layoutparams = (android.widget.FrameLayout.LayoutParams)getLayoutParams();
        layoutparams.height = -2;
        layoutparams.width = i;
        layoutparams.leftMargin = mTabContainer.getOffsetX();
        layoutparams.gravity = 83;
        setLayoutParams(layoutparams);
    }

    private TabContainerLayout mTabContainer;
    private int mTabWidth;
}
