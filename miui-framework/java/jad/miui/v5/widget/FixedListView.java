// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.v5.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ListView;

public class FixedListView extends ListView {

    public FixedListView(Context context) {
        this(context, null);
    }

    public FixedListView(Context context, AttributeSet attributeset) {
        this(context, attributeset, 0);
    }

    public FixedListView(Context context, AttributeSet attributeset, int i) {
        super(context, attributeset, i);
        mMaxItemCount = -1;
        mExtraHeight = 0;
        TypedArray typedarray = context.obtainStyledAttributes(attributeset, miui.R.styleable.FixedListView, i, 0);
        mMaxItemCount = typedarray.getInt(0, mMaxItemCount);
        typedarray.recycle();
    }

    protected void onMeasure(int i, int j) {
        if(mMaxItemCount < 0)
            super.onMeasure(i, android.view.View.MeasureSpec.makeMeasureSpec(0x1fffffff, 0x80000000));
        else
        if(getCount() > 0) {
            int k = getListPaddingTop();
            int l = getListPaddingBottom();
            int i1 = 2 * getVerticalFadingEdgeLength() + (k + l);
            int j1 = getDividerHeight();
            int k1;
            if(getChildCount() > 0) {
                k1 = getChildAt(0).getMeasuredHeight();
            } else {
                super.onMeasure(i, android.view.View.MeasureSpec.makeMeasureSpec(0, 0));
                k1 = getMeasuredHeight() - i1;
            }
            super.onMeasure(i, android.view.View.MeasureSpec.makeMeasureSpec(i1 + k1 * mMaxItemCount + j1 * (-1 + mMaxItemCount) + mExtraHeight, 0x80000000));
        } else {
            super.onMeasure(i, j);
        }
    }

    public void setExtraHeight(int i) {
        mExtraHeight = i;
    }

    public void setMaxItemCount(int i) {
        if(i != mMaxItemCount) {
            mMaxItemCount = i;
            requestLayout();
        }
    }

    private int mExtraHeight;
    private int mMaxItemCount;
}
