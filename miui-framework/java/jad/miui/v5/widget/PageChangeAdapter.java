// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.v5.widget;

import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;

public class PageChangeAdapter
    implements android.support.v4.view.ViewPager.OnPageChangeListener {
    public static interface OnPageScrollListener {

        public abstract void onReset(ViewPager viewpager, int i, int j);

        public abstract void onScroll(ViewPager viewpager, int i, int j, float f);
    }


    public PageChangeAdapter(ViewPager viewpager) {
        this(viewpager, null);
    }

    public PageChangeAdapter(ViewPager viewpager, OnPageScrollListener onpagescrolllistener) {
        mChangeStartPosition = -1;
        mAppearingPosition = -1;
        mViewPager = viewpager;
        mScrolledListener = onpagescrolllistener;
    }

    public void onPageScrollStateChanged(int i) {
        if(i != 1) goto _L2; else goto _L1
_L1:
        int j = mViewPager.getCurrentItem();
        if(mChangeStartPosition != j) {
            if(mAppearingPosition != -1) {
                if(mScrolledListener != null)
                    mScrolledListener.onReset(mViewPager, mChangeStartPosition, mAppearingPosition);
                mAppearingPosition = -1;
            }
            mChangeStartPosition = j;
        }
_L4:
        return;
_L2:
        if(i == 0) {
            if(mScrolledListener != null)
                mScrolledListener.onReset(mViewPager, mChangeStartPosition, mViewPager.getCurrentItem());
            mChangeStartPosition = -1;
            mAppearingPosition = -1;
        }
        if(true) goto _L4; else goto _L3
_L3:
    }

    public void onPageScrolled(int i, float f, int j) {
        if(mScrolledListener == null) goto _L2; else goto _L1
_L1:
        int k;
        int l;
        float f1;
        k = mChangeStartPosition;
        l = k;
        f1 = 1.0F;
        if(k != i || k >= -1 + mViewPager.getAdapter().getCount()) goto _L4; else goto _L3
_L3:
        l = k + 1;
        f1 = f;
_L6:
        if(k != l) {
            mScrolledListener.onScroll(mViewPager, k, l, f1);
            mAppearingPosition = l;
        }
_L2:
        return;
_L4:
        if(k == i + 1 && k > 0) {
            l = k - 1;
            f1 = 1.0F - f;
        }
        if(true) goto _L6; else goto _L5
_L5:
    }

    public void onPageSelected(int i) {
    }

    public void setScrolledListener(OnPageScrollListener onpagescrolllistener) {
        mScrolledListener = onpagescrolllistener;
    }

    private int mAppearingPosition;
    private int mChangeStartPosition;
    private OnPageScrollListener mScrolledListener;
    private final ViewPager mViewPager;
}
