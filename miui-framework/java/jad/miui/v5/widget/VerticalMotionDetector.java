// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.v5.widget;

import android.view.*;

// Referenced classes of package miui.v5.widget:
//            MotionDetectListener

public class VerticalMotionDetector {

    public VerticalMotionDetector(ViewGroup viewgroup) {
        mActivePointerId = -1;
        mLastMotionY = 0;
        mLastMotionX = 0;
        mStartMotionY = 0;
        mStartMotionX = 0;
        mIsBeingDragged = false;
        mStrategy = null;
        mView = viewgroup;
        mView.setFocusable(true);
        mView.setDescendantFocusability(0x40000);
        mView.setWillNotDraw(false);
        mTouchSlop = ViewConfiguration.get(mView.getContext()).getScaledTouchSlop();
    }

    private void cancelDragging(int i, int j) {
        if(mIsBeingDragged) goto _L2; else goto _L1
_L1:
        return;
_L2:
        mIsBeingDragged = false;
        if(mStrategy != null)
            mStrategy.onMoveCancel(mView, i, j);
        if(true) goto _L1; else goto _L3
_L3:
    }

    private void clearVelocityTracker() {
        if(mVelocityTracker != null)
            mVelocityTracker.clear();
    }

    private void finishDragging(int i, int j, boolean flag) {
        if(mIsBeingDragged) goto _L2; else goto _L1
_L1:
        return;
_L2:
        mIsBeingDragged = false;
        if(mStrategy != null) {
            MotionDetectListener motiondetectlistener = mStrategy;
            ViewGroup viewgroup = mView;
            int k = mStartMotionX;
            int l = mStartMotionY;
            VelocityTracker velocitytracker;
            if(flag)
                velocitytracker = mVelocityTracker;
            else
                velocitytracker = null;
            motiondetectlistener.onMoveFinish(viewgroup, i, j, k, l, velocitytracker);
        }
        if(true) goto _L1; else goto _L3
_L3:
    }

    private void initOrResetVelocityTracker() {
        if(mVelocityTracker == null)
            mVelocityTracker = VelocityTracker.obtain();
        else
            mVelocityTracker.clear();
    }

    private void initVelocityTrackerIfNotExists() {
        if(mVelocityTracker == null)
            mVelocityTracker = VelocityTracker.obtain();
    }

    private void onSecondaryPointerDown(MotionEvent motionevent) {
        int i = motionevent.getActionIndex();
        int j = motionevent.getPointerId(i);
        int k = (int)motionevent.getY(i);
        int l = (int)motionevent.getX(i);
        mActivePointerId = j;
        mStartMotionY = k;
        mStartMotionX = l;
        mLastMotionY = k;
        mLastMotionX = l;
        clearVelocityTracker();
    }

    private void onSecondaryPointerUp(MotionEvent motionevent) {
        int i = (0xff00 & motionevent.getAction()) >> 8;
        if(motionevent.getPointerId(i) == mActivePointerId) {
            int j;
            int k;
            int l;
            if(i == 0)
                j = 1;
            else
                j = 0;
            k = (int)motionevent.getY(j);
            l = (int)motionevent.getX(j);
            mStartMotionY = k;
            mStartMotionX = l;
            mLastMotionY = k;
            mLastMotionX = l;
            mActivePointerId = motionevent.getPointerId(j);
        }
    }

    private void recycleVelocityTracker() {
        if(mVelocityTracker != null) {
            mVelocityTracker.recycle();
            mVelocityTracker = null;
        }
    }

    private void startDragging(int i, int j) {
        mStartMotionY = j;
        mStartMotionX = i;
        if(!mIsBeingDragged) goto _L2; else goto _L1
_L1:
        return;
_L2:
        mIsBeingDragged = true;
        if(mStrategy != null)
            mStrategy.onMoveStart(mView, i, j);
        if(true) goto _L1; else goto _L3
_L3:
    }

    private void trackMovement(MotionEvent motionevent) {
        float f = motionevent.getRawX() - motionevent.getX();
        float f1 = motionevent.getRawY() - motionevent.getY();
        motionevent.offsetLocation(f, f1);
        mVelocityTracker.addMovement(motionevent);
        motionevent.offsetLocation(-f, -f1);
    }

    public MotionDetectListener getMotionStrategy() {
        return mStrategy;
    }

    public boolean isBeingDragged() {
        return mIsBeingDragged;
    }

    public boolean isMovable(int i, int j) {
        boolean flag;
        if(mStrategy != null)
            flag = mStrategy.isMovable(mView, i, j, mStartMotionX, mStartMotionY);
        else
            flag = true;
        return flag;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionevent) {
        boolean flag;
        MotionDetectListener motiondetectlistener;
        flag = false;
        motiondetectlistener = mStrategy;
        if(motiondetectlistener != null) goto _L2; else goto _L1
_L1:
        return flag;
_L2:
        int i;
        i = motionevent.getAction();
        if(i == 2 && mIsBeingDragged) {
            flag = true;
            continue; /* Loop/switch isn't completed */
        }
        i & 0xff;
        JVM INSTR tableswitch 0 6: default 84
    //                   0 261
    //                   1 344
    //                   2 92
    //                   3 344
    //                   4 84
    //                   5 84
    //                   6 411;
           goto _L3 _L4 _L5 _L6 _L5 _L3 _L3 _L7
_L7:
        break MISSING_BLOCK_LABEL_411;
_L3:
        break; /* Loop/switch isn't completed */
_L6:
        break; /* Loop/switch isn't completed */
_L9:
        flag = mIsBeingDragged;
        if(true) goto _L1; else goto _L8
_L8:
        int l1 = mActivePointerId;
        if(l1 != -1) {
            int i2 = motionevent.findPointerIndex(l1);
            int j2 = (int)motionevent.getY(i2);
            int k2 = (int)motionevent.getX(i2);
            if(!motiondetectlistener.isMovable(mView, k2, j2, mLastMotionX, mLastMotionY)) {
                clearVelocityTracker();
            } else {
                int l2 = Math.abs(j2 - mLastMotionY);
                int i3 = Math.abs(k2 - mLastMotionX);
                if(l2 > mTouchSlop && i3 < l2) {
                    initVelocityTrackerIfNotExists();
                    trackMovement(motionevent);
                    startDragging(mLastMotionX, mLastMotionY);
                    mLastMotionY = j2;
                    mLastMotionX = k2;
                    ViewParent viewparent = mView.getParent();
                    if(viewparent != null)
                        viewparent.requestDisallowInterceptTouchEvent(true);
                }
            }
        }
          goto _L9
_L4:
        int j1 = (int)motionevent.getY();
        int k1 = (int)motionevent.getX();
        mLastMotionY = j1;
        mLastMotionX = k1;
        mActivePointerId = motionevent.getPointerId(0);
        initOrResetVelocityTracker();
        trackMovement(motionevent);
        if(motiondetectlistener.moveImmediately(mView, k1, j1))
            startDragging(k1, j1);
        else
            cancelDragging(k1, j1);
          goto _L9
_L5:
        int k = motionevent.findPointerIndex(mActivePointerId);
        int l = (int)motionevent.getY(k);
        int i1 = (int)motionevent.getX(k);
        if(i == 1)
            finishDragging(i1, l, false);
        else
            cancelDragging(i1, l);
        mActivePointerId = -1;
        recycleVelocityTracker();
          goto _L9
        onSecondaryPointerUp(motionevent);
        int j = motionevent.findPointerIndex(mActivePointerId);
        mLastMotionY = (int)motionevent.getY(j);
        mLastMotionX = (int)motionevent.getX(j);
          goto _L9
    }

    public boolean onTouchEvent(MotionEvent motionevent) {
        boolean flag;
        MotionDetectListener motiondetectlistener;
        flag = false;
        motiondetectlistener = mStrategy;
        if(motiondetectlistener != null) goto _L2; else goto _L1
_L1:
        return flag;
_L2:
        initVelocityTrackerIfNotExists();
        trackMovement(motionevent);
        0xff & motionevent.getAction();
        JVM INSTR tableswitch 0 6: default 72
    //                   0 77
    //                   1 337
    //                   2 173
    //                   3 390
    //                   4 72
    //                   5 450
    //                   6 442;
           goto _L3 _L4 _L5 _L6 _L7 _L3 _L8 _L9
_L8:
        break MISSING_BLOCK_LABEL_450;
_L3:
        break; /* Loop/switch isn't completed */
_L4:
        break; /* Loop/switch isn't completed */
_L11:
        flag = true;
        if(true) goto _L1; else goto _L10
_L10:
        int j2 = (int)motionevent.getY();
        int k2 = (int)motionevent.getX();
        mLastMotionY = j2;
        mLastMotionX = k2;
        mActivePointerId = motionevent.getPointerId(0);
        if(motiondetectlistener.moveImmediately(mView, k2, j2)) {
            startDragging(k2, j2);
            ViewParent viewparent1 = mView.getParent();
            if(viewparent1 != null)
                viewparent1.requestDisallowInterceptTouchEvent(true);
        } else {
            cancelDragging(k2, j2);
        }
          goto _L11
_L6:
        int i1 = motionevent.findPointerIndex(mActivePointerId);
        int j1 = (int)motionevent.getY(i1);
        int k1 = (int)motionevent.getX(i1);
        int l1 = mLastMotionY - j1;
        int i2 = 0;
        if(!mIsBeingDragged && Math.abs(l1) > mTouchSlop) {
            startDragging(k1, j1);
            ViewParent viewparent = mView.getParent();
            if(viewparent != null)
                viewparent.requestDisallowInterceptTouchEvent(true);
            if(l1 > 0)
                i2 = -mTouchSlop;
            else
                i2 = mTouchSlop;
        }
        if(mIsBeingDragged) {
            if(!motiondetectlistener.onMove(mView, k1, j1 + i2, mStartMotionX, mStartMotionY))
                clearVelocityTracker();
            mLastMotionY = j1;
            mLastMotionX = k1;
        }
          goto _L11
_L5:
        if(mIsBeingDragged) {
            int k = motionevent.findPointerIndex(mActivePointerId);
            int l = (int)motionevent.getY(k);
            finishDragging((int)motionevent.getX(k), l, true);
            mActivePointerId = -1;
            recycleVelocityTracker();
        }
          goto _L11
_L7:
        if(mIsBeingDragged) {
            int i = motionevent.findPointerIndex(mActivePointerId);
            int j = (int)motionevent.getY(i);
            cancelDragging((int)motionevent.getX(i), j);
            mActivePointerId = -1;
            recycleVelocityTracker();
        }
          goto _L11
_L9:
        onSecondaryPointerUp(motionevent);
          goto _L11
        onSecondaryPointerDown(motionevent);
          goto _L11
    }

    public void setMotionStrategy(MotionDetectListener motiondetectlistener) {
        mStrategy = motiondetectlistener;
    }

    static final int INVALID_POINTER = -1;
    static final String TAG = miui/v5/widget/VerticalMotionDetector.getName();
    private int mActivePointerId;
    private boolean mIsBeingDragged;
    private int mLastMotionX;
    private int mLastMotionY;
    private int mStartMotionX;
    private int mStartMotionY;
    private MotionDetectListener mStrategy;
    private final int mTouchSlop;
    private VelocityTracker mVelocityTracker;
    private final ViewGroup mView;

}
