// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.v5.widget;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.*;
import android.widget.*;

// Referenced classes of package miui.v5.widget:
//            MiuiViewConfiguration, TabIndicator, Views

public class TabContainerLayout extends HorizontalScrollView
    implements android.view.View.OnClickListener {
    static interface TabWidthChangedListener {

        public abstract void onTabWidthChanged(TabContainerLayout tabcontainerlayout);
    }

    private static class DefaultScrollStrategy
        implements ScrollStrategy {

        public int getScrollX(View view, ViewGroup viewgroup) {
            int i = view.getLeft();
            int j = view.getRight();
            Rect rect = mBounds;
            viewgroup.getLocalVisibleRect(rect);
            int k;
            if(i < rect.left)
                k = i;
            else
            if(j > rect.right)
                k = (j + rect.left) - rect.right;
            else
                k = rect.left;
            return k;
        }

        private final Rect mBounds = new Rect();

        public DefaultScrollStrategy() {
        }
    }

    public static interface ScrollStrategy {

        public abstract int getScrollX(View view, ViewGroup viewgroup);
    }

    private class TabViewImpl extends LinearLayout {

        public android.app.ActionBar.Tab getTab() {
            return mTab;
        }

        public void onMeasure(int i, int j) {
            if(mTabWidth > 0)
                i = android.view.View.MeasureSpec.makeMeasureSpec(mTabWidth, 0x40000000);
            super.onMeasure(i, j);
        }

        public void update() {
            android.app.ActionBar.Tab tab;
            View view;
            if(mTabBackgroundResId != 0)
                setBackgroundResource(mTabBackgroundResId);
            tab = mTab;
            view = tab.getCustomView();
            if(view == null) goto _L2; else goto _L1
_L1:
            android.view.ViewParent viewparent = view.getParent();
            if(viewparent != this) {
                if(viewparent != null)
                    ((ViewGroup)viewparent).removeView(view);
                addView(view);
            }
            mCustomView = view;
            if(mTextView != null)
                mTextView.setVisibility(8);
            if(mIconView != null) {
                mIconView.setVisibility(8);
                mIconView.setImageDrawable(null);
            }
_L4:
            return;
_L2:
            if(mCustomView != null) {
                removeView(mCustomView);
                mCustomView = null;
            }
            android.graphics.drawable.Drawable drawable = tab.getIcon();
            CharSequence charsequence = tab.getText();
            LayoutInflater layoutinflater = LayoutInflater.from(mContext);
            if(drawable != null) {
                if(mIconView == null) {
                    mIconView = (ImageView)layoutinflater.inflate(0x6030043, this, false);
                    addView(mIconView, 0);
                }
                mIconView.setImageDrawable(drawable);
                mIconView.setVisibility(0);
                if(mTabViewStyle != null)
                    mTabViewStyle.bindIconView(mTab, mIconView);
            } else
            if(mIconView != null) {
                mIconView.setVisibility(8);
                mIconView.setImageDrawable(null);
            }
            if(charsequence == null)
                break; /* Loop/switch isn't completed */
            if(mTextView == null) {
                mTextView = (TextView)layoutinflater.inflate(0x6030042, this, false);
                addView(mTextView);
            }
            mTextView.setText(charsequence);
            mTextView.setVisibility(0);
            if(mTabViewStyle != null)
                mTabViewStyle.bindTextView(mTab, mTextView);
_L6:
            if(mIconView != null)
                mIconView.setContentDescription(tab.getContentDescription());
            if(true) goto _L4; else goto _L3
_L3:
            if(mTextView == null) goto _L6; else goto _L5
_L5:
            mTextView.setVisibility(8);
            mTextView.setText(null);
              goto _L6
        }

        private View mCustomView;
        private ImageView mIconView;
        private android.app.ActionBar.Tab mTab;
        private TextView mTextView;
        final TabContainerLayout this$0;

        public TabViewImpl(Context context, android.app.ActionBar.Tab tab) {
            this$0 = TabContainerLayout.this;
            super(context, null, 0x601005c);
            setGravity(17);
            mTab = tab;
            update();
        }
    }

    public static interface TabViewStyle {

        public abstract void bindIconView(android.app.ActionBar.Tab tab, ImageView imageview);

        public abstract void bindTextView(android.app.ActionBar.Tab tab, TextView textview);
    }


    public TabContainerLayout(Context context) {
        this(context, null);
    }

    public TabContainerLayout(Context context, AttributeSet attributeset) {
        this(context, attributeset, 0);
    }

    public TabContainerLayout(Context context, AttributeSet attributeset, int i) {
        super(context, attributeset, i);
        mInteractive = true;
        mTabViewStyle = null;
        setHorizontalScrollBarEnabled(false);
        setScrollStrategy(null);
        setMaxVisibleTabCount(MiuiViewConfiguration.get(context).getMaxVisibleTabCount());
        FrameLayout framelayout = new FrameLayout(context);
        LinearLayout linearlayout = new LinearLayout(context, null, 0x601005a);
        linearlayout.setMeasureWithLargestChildEnabled(true);
        linearlayout.setGravity(17);
        framelayout.addView(linearlayout, new android.widget.FrameLayout.LayoutParams(-1, -1));
        mTabLayout = linearlayout;
        addView(framelayout, new android.widget.FrameLayout.LayoutParams(-1, -1));
        mWrapLayout = framelayout;
    }

    private void animateToTab(final View tabView) {
        if(mTabSelector != null)
            removeCallbacks(mTabSelector);
        mTabSelector = new Runnable() {

            public void run() {
                if(tabView != null) {
                    int i = mScrollStrategy.getScrollX(tabView, TabContainerLayout.this);
                    if(getScrollX() != i)
                        smoothScrollTo(i, 0);
                }
                mTabSelector = null;
            }

            final TabContainerLayout this$0;
            final View val$tabView;

             {
                this$0 = TabContainerLayout.this;
                tabView = view;
                super();
            }
        };
        post(mTabSelector);
    }

    private TabViewImpl createTabView(android.app.ActionBar.Tab tab) {
        TabViewImpl tabviewimpl = new TabViewImpl(getContext(), tab);
        tabviewimpl.setFocusable(true);
        tabviewimpl.setOnClickListener(this);
        return tabviewimpl;
    }

    private TabViewImpl selectedTabView(android.app.ActionBar.Tab tab) {
        TabViewImpl tabviewimpl = null;
        int i = mTabLayout.getChildCount();
        int j = 0;
        while(j < i)  {
            TabViewImpl tabviewimpl1 = (TabViewImpl)mTabLayout.getChildAt(j);
            if(tab == tabviewimpl1.getTab()) {
                tabviewimpl = tabviewimpl1;
                tabviewimpl1.setSelected(true);
            } else {
                tabviewimpl1.setSelected(false);
            }
            j++;
        }
        return tabviewimpl;
    }

    public void addTab(android.app.ActionBar.Tab tab, int i, boolean flag) {
        TabViewImpl tabviewimpl = createTabView(tab);
        mTabLayout.addView(tabviewimpl, i, new android.widget.LinearLayout.LayoutParams(0, -1, 1.0F));
        if(flag)
            selectTab(tab);
    }

    public void addTab(android.app.ActionBar.Tab tab, boolean flag) {
        addTab(tab, -1, flag);
    }

    public void attachIndicator(TabIndicator tabindicator) {
        if(tabindicator != null)
            tabindicator.attach(this);
    }

    public void detachIndicator(TabIndicator tabindicator) {
        if(tabindicator != null)
            tabindicator.detach();
    }

    public boolean dispatchTouchEvent(MotionEvent motionevent) {
        boolean flag;
        if(!mInteractive)
            flag = false;
        else
            flag = super.dispatchTouchEvent(motionevent);
        return flag;
    }

    public int findCurrentTabPos() {
        return findTabPosition(mSelectedTab);
    }

    public int findTabPosition(android.app.ActionBar.Tab tab) {
        int i = -1;
        int j = mTabLayout.getChildCount();
        int k = 0;
        do {
label0:
            {
                if(k < j) {
                    if(tab != ((TabViewImpl)mTabLayout.getChildAt(k)).getTab())
                        break label0;
                    i = k;
                }
                return i;
            }
            k++;
        } while(true);
    }

    public int getBackgroundHeight() {
        int i = Views.getBackgroundHeight(this);
        if(i <= 0)
            i = Views.getBackgroundHeight(mTabLayout);
        return Math.max(0, i);
    }

    public ViewGroup getIndicatorContainer() {
        return mWrapLayout;
    }

    int getOffsetX() {
        return mTabLayout.getPaddingLeft();
    }

    public android.app.ActionBar.Tab getSelectedTab() {
        return mSelectedTab;
    }

    public android.app.ActionBar.Tab getTabAt(int i) {
        TabViewImpl tabviewimpl = (TabViewImpl)mTabLayout.getChildAt(i);
        android.app.ActionBar.Tab tab;
        if(tabviewimpl != null)
            tab = tabviewimpl.getTab();
        else
            tab = null;
        return tab;
    }

    public int getTabCount() {
        return mTabLayout.getChildCount();
    }

    public int getTabWidth() {
        return mTabWidth;
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if(mTabSelector != null)
            post(mTabSelector);
    }

    public void onClick(View view) {
        android.app.ActionBar.Tab tab = ((TabViewImpl)view).getTab();
        tab.select();
        selectedTabView(tab);
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if(mTabSelector != null)
            removeCallbacks(mTabSelector);
    }

    public void onMeasure(int i, int j) {
        int k = android.view.View.MeasureSpec.makeMeasureSpec(getBackgroundHeight(), 0x40000000);
        int l = android.view.View.MeasureSpec.getMode(i);
        boolean flag;
        int i1;
        int j1;
        int k1;
        if(l == 0x40000000)
            flag = true;
        else
            flag = false;
        setFillViewport(flag);
        int _tmp = mTabWidth;
        i1 = mTabLayout.getChildCount();
        if(i1 > 1 && (l == 0x40000000 || l == 0x80000000)) {
            int l1 = Math.min(i1, mMaxVisibleTabCount);
            int i2 = mTabLayout.getPaddingLeft() + mTabLayout.getPaddingRight();
            j1 = (android.view.View.MeasureSpec.getSize(i) - i2) / l1;
        } else {
            j1 = -1;
        }
        if(mTabWidth != j1) {
            mTabWidth = j1;
            onTabWidthChanged();
        }
        k1 = getMeasuredWidth();
        super.onMeasure(i, k);
        if(k1 != getMeasuredWidth())
            selectTab(mSelectedTab);
    }

    protected void onTabWidthChanged() {
        if(mTabWidthChangedListener != null)
            mTabWidthChangedListener.onTabWidthChanged(this);
    }

    public boolean removeAllTabs() {
        mSelectedTab = null;
        mTabLayout.removeAllViews();
        return true;
    }

    public boolean removeTab(android.app.ActionBar.Tab tab) {
        boolean flag = false;
        if(mSelectedTab == tab && tab != null) {
            mSelectedTab = null;
            flag = true;
        }
        mTabLayout.removeViewAt(findTabPosition(tab));
        return flag;
    }

    public boolean removeTabAt(int i) {
        return removeTab(getTabAt(i));
    }

    public android.app.ActionBar.Tab selectTab(android.app.ActionBar.Tab tab) {
        android.app.ActionBar.Tab tab1 = mSelectedTab;
        Object obj;
        if(tab == tab1) {
            obj = mTabLayout.getChildAt(findTabPosition(tab));
        } else {
            mSelectedTab = tab;
            obj = selectedTabView(tab);
        }
        animateToTab(((View) (obj)));
        return tab1;
    }

    public void selectTabAt(int i) {
        selectTab(getTabAt(i));
    }

    public void setInteractive(boolean flag) {
        mInteractive = flag;
    }

    public void setMaxVisibleTabCount(int i) {
        if(i <= 0)
            throw new IllegalArgumentException((new StringBuilder()).append("count > 0 is need! count=").append(i).toString());
        if(mMaxVisibleTabCount != i) {
            mMaxVisibleTabCount = i;
            requestLayout();
        }
    }

    public void setScrollStrategy(ScrollStrategy scrollstrategy) {
        if(scrollstrategy != null)
            mScrollStrategy = scrollstrategy;
        else
            mScrollStrategy = new DefaultScrollStrategy();
    }

    public void setTabBackground(int i) {
        mTabBackgroundResId = i;
        for(int j = getTabCount(); j >= 0; j--)
            updateTabAt(j);

    }

    public void setTabViewStyle(TabViewStyle tabviewstyle) {
        mTabViewStyle = tabviewstyle;
    }

    void setTabWidthChangedListener(TabWidthChangedListener tabwidthchangedlistener) {
        mTabWidthChangedListener = tabwidthchangedlistener;
    }

    public void updateTabAt(int i) {
        TabViewImpl tabviewimpl = (TabViewImpl)mTabLayout.getChildAt(i);
        if(tabviewimpl != null)
            tabviewimpl.update();
    }

    static final String TAG = miui/v5/widget/TabContainerLayout.getName();
    private boolean mInteractive;
    int mMaxVisibleTabCount;
    ScrollStrategy mScrollStrategy;
    private android.app.ActionBar.Tab mSelectedTab;
    int mTabBackgroundResId;
    final LinearLayout mTabLayout;
    Runnable mTabSelector;
    TabViewStyle mTabViewStyle;
    int mTabWidth;
    private TabWidthChangedListener mTabWidthChangedListener;
    final FrameLayout mWrapLayout;

}
