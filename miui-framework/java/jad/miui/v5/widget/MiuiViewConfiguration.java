// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.v5.widget;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.SparseArray;

public class MiuiViewConfiguration {

    private MiuiViewConfiguration(Context context) {
        Resources resources = context.getResources();
        mMinAnchorVelocity = resources.getDimensionPixelSize(0x60a002b);
        mTranslateSlop = resources.getDimensionPixelSize(0x60a002a);
        mOverDistance = resources.getDimensionPixelSize(0x60a002c);
        mMaxAnchorDuration = resources.getInteger(0x608000d);
        mMaxVisibleTabCount = resources.getInteger(0x608000e);
    }

    public static MiuiViewConfiguration get(Context context) {
        int i = (int)(100F * context.getResources().getDisplayMetrics().density);
        MiuiViewConfiguration miuiviewconfiguration = (MiuiViewConfiguration)sConfigurations.get(i);
        if(miuiviewconfiguration == null) {
            miuiviewconfiguration = new MiuiViewConfiguration(context);
            sConfigurations.put(i, miuiviewconfiguration);
        }
        return miuiviewconfiguration;
    }

    public int getMaxAnchorDuration() {
        return mMaxAnchorDuration;
    }

    public int getMaxVisibleTabCount() {
        return mMaxVisibleTabCount;
    }

    public int getScaledMinAnchorVelocity() {
        return mMinAnchorVelocity;
    }

    public int getScaledOverDistance() {
        return mOverDistance;
    }

    public int getScaledTranslateSlop() {
        return mTranslateSlop;
    }

    static final SparseArray sConfigurations = new SparseArray(2);
    private final int mMaxAnchorDuration;
    private final int mMaxVisibleTabCount;
    private final int mMinAnchorVelocity;
    private final int mOverDistance;
    private final int mTranslateSlop;

}
