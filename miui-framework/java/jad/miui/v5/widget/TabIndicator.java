// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.v5.widget;

import android.graphics.drawable.Drawable;

// Referenced classes of package miui.v5.widget:
//            TabContainerLayout

public interface TabIndicator {

    public abstract float apply(int i, float f);

    public abstract void attach(TabContainerLayout tabcontainerlayout);

    public abstract void detach();

    public abstract void setIndicator(Drawable drawable);

    public abstract void setTabWidth(int i);
}
