// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.v5.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class NumberProgressView extends LinearLayout {

    public NumberProgressView(Context context) {
        super(context);
        mCurProgress = -1;
        mResNumber = new Drawable[10];
        mResPercent = null;
        init(context, null);
    }

    public NumberProgressView(Context context, AttributeSet attributeset) {
        super(context, attributeset);
        mCurProgress = -1;
        mResNumber = new Drawable[10];
        mResPercent = null;
        init(context, attributeset);
    }

    public NumberProgressView(Context context, AttributeSet attributeset, int i) {
        super(context, attributeset, i);
        mCurProgress = -1;
        mResNumber = new Drawable[10];
        mResPercent = null;
        init(context, attributeset);
    }

    private void init(Context context, AttributeSet attributeset) {
        for(int i = 0; i < mResNumber.length; i++)
            mResNumber[i] = null;

        if(attributeset != null) {
            TypedArray typedarray = context.obtainStyledAttributes(attributeset, miui.R.styleable.NumberProgressView);
            int k = typedarray.getResourceId(0, -1);
            if(-1 != k) {
                TypedArray typedarray1 = typedarray.getResources().obtainTypedArray(k);
                if(typedarray1.length() == mResNumber.length) {
                    for(int l = 0; l < mResNumber.length; l++)
                        mResNumber[l] = typedarray1.getDrawable(l);

                }
            }
            mResPercent = typedarray.getDrawable(1);
            typedarray.recycle();
        }
        if(mResPercent == null)
            mResPercent = context.getResources().getDrawable(0x60202a4);
        if(mResNumber[0] == null) {
            Resources resources = context.getResources();
            for(int j = 0; j < mResNumber.length; j++)
                mResNumber[j] = resources.getDrawable(0x602029a + j);

        }
        ((LayoutInflater)context.getSystemService("layout_inflater")).inflate(0x603005e, this);
        mNumberProgress1 = (ImageView)findViewById(0x60b00b7);
        mNumberProgress2 = (ImageView)findViewById(0x60b00b8);
        mNumberProgress3 = (ImageView)findViewById(0x60b00b9);
        mPercent = (ImageView)findViewById(0x60b00ba);
        if(mResPercent != null)
            mPercent.setBackground(mResPercent);
    }

    private void setNumber(int i, ImageView imageview) {
        if(mResNumber[i] != null)
            imageview.setImageDrawable(mResNumber[i]);
    }

    public boolean setProgress(int i) {
        boolean flag = false;
        if(i >= 0 && i <= 100 && i != mCurProgress) {
            mCurProgress = i;
            if(i > 99) {
                mNumberProgress3.setVisibility(0);
                setNumber(i / 100, mNumberProgress3);
                i %= 100;
            } else {
                mNumberProgress3.setVisibility(8);
            }
            if(i > 9 || mCurProgress > 99) {
                mNumberProgress2.setVisibility(0);
                setNumber(i / 10, mNumberProgress2);
                i %= 10;
            } else {
                mNumberProgress2.setVisibility(8);
            }
            setNumber(i, mNumberProgress1);
            flag = true;
        }
        return flag;
    }

    private static final int MAX_PROGRESS = 100;
    private int mCurProgress;
    private ImageView mNumberProgress1;
    private ImageView mNumberProgress2;
    private ImageView mNumberProgress3;
    private ImageView mPercent;
    private Drawable mResNumber[];
    private Drawable mResPercent;
}
