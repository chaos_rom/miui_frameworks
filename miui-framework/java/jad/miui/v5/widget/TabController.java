// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.v5.widget;

import android.app.*;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import com.google.android.collect.Lists;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package miui.v5.widget:
//            TabContainerLayout, CooperativeViewPager, TabIndicator, FragmentPagerAdapter

public class TabController {
    public static interface PagerAdapterChangedListener {

        public abstract void onAdapterChanged(ViewPager viewpager, boolean flag);
    }

    private static class MyDynamicPagerAdapter extends DynamicPagerAdapter {

        void addFragment(Fragment fragment) {
            mFragments.add(fragment);
        }

        Fragment findFragmentByTag(String s) {
            if(s == null) goto _L2; else goto _L1
_L1:
            Iterator iterator = mFragments.iterator();
_L5:
            if(!iterator.hasNext()) goto _L2; else goto _L3
_L3:
            Fragment fragment = (Fragment)iterator.next();
            if(!s.equals(fragment.getTag())) goto _L5; else goto _L4
_L4:
            return fragment;
_L2:
            fragment = null;
            if(true) goto _L4; else goto _L6
_L6:
        }

        public int getCount() {
            return mFragments.size();
        }

        FragmentManager getFragmentManager() {
            return mFragmentManager;
        }

        public Fragment getItem(int i) {
            Fragment fragment;
            if(i >= 0 && i < mFragments.size())
                fragment = (Fragment)mFragments.get(i);
            else
                fragment = null;
            return fragment;
        }

        public int getItemPosition(Object obj) {
            return -2;
        }

        public boolean isViewFromObject(View view, Object obj) {
            boolean flag;
            if(((Fragment)obj).getView() == view)
                flag = true;
            else
                flag = false;
            return flag;
        }

        boolean removeFragment(Fragment fragment) {
            return mFragments.remove(fragment);
        }

        private final FragmentManager mFragmentManager;
        private final List mFragments = Lists.newArrayList();

        public MyDynamicPagerAdapter(FragmentManager fragmentmanager) {
            super(fragmentmanager);
            mFragmentManager = fragmentmanager;
        }
    }

    public static abstract class DynamicPagerAdapter extends FragmentPagerAdapter {

        abstract void addFragment(Fragment fragment);

        abstract Fragment findFragmentByTag(String s);

        abstract FragmentManager getFragmentManager();

        abstract boolean removeFragment(Fragment fragment);

        public DynamicPagerAdapter(FragmentManager fragmentmanager) {
            super(fragmentmanager);
        }
    }

    private static class TabEditorImpl
        implements TabEditor {

        public TabEditor add(TabEditCommand tabeditcommand) {
            if(tabeditcommand != null)
                mCommands.add(tabeditcommand);
            return null;
        }

        public TabEditor addTab(android.app.ActionBar.Tab tab, Fragment fragment) {
            return addTab(tab, fragment, null);
        }

        public TabEditor addTab(android.app.ActionBar.Tab tab, Fragment fragment, String s) {
            if(tab == null) {
                throw new NullPointerException((new StringBuilder()).append("tab and fragment cannot be null! tab=").append(tab).append(" fragment=").append(fragment).toString());
            } else {
                add(new TabEditAdd(tab, fragment, s));
                return this;
            }
        }

        public boolean commit() {
            boolean flag = false;
            TabController tabcontroller = (TabController)mTabControllerRef.get();
            if(tabcontroller != null)
                if(mCommands.size() <= 0) {
                    flag = true;
                } else {
                    CooperativeViewPager cooperativeviewpager = tabcontroller.mViewPager;
                    DynamicPagerAdapter dynamicpageradapter = tabcontroller.mAdapter;
                    FragmentManager fragmentmanager = dynamicpageradapter.getFragmentManager();
                    TabContainerLayout tabcontainerlayout = tabcontroller.mTabContainer;
                    FragmentTransaction fragmenttransaction = fragmentmanager.beginTransaction();
                    for(Iterator iterator = mCommands.iterator(); iterator.hasNext(); ((TabEditCommand)iterator.next()).execute(tabcontainerlayout, cooperativeviewpager, dynamicpageradapter, fragmenttransaction));
                    fragmenttransaction.commitAllowingStateLoss();
                    fragmentmanager.executePendingTransactions();
                    dynamicpageradapter.notifyDataSetChanged();
                    boolean flag1 = false;
                    if(tabcontainerlayout.getSelectedTab() == null) {
                        tabcontainerlayout.selectTabAt(0);
                        flag1 = true;
                    }
                    PagerAdapterChangedListener pageradapterchangedlistener = tabcontroller.mAdapterChangedListener;
                    if(pageradapterchangedlistener != null)
                        pageradapterchangedlistener.onAdapterChanged(cooperativeviewpager, flag1);
                    mCommands.clear();
                    flag = true;
                }
            return flag;
        }

        public TabEditor removeTab(android.app.ActionBar.Tab tab) {
            if(tab == null) {
                throw new NullPointerException((new StringBuilder()).append("tab and fragment cannot be null! tab=").append(tab).toString());
            } else {
                add(new TabEditRemove(tab));
                return this;
            }
        }

        private final List mCommands = Lists.newArrayList();
        private final WeakReference mTabControllerRef;

        public TabEditorImpl(TabController tabcontroller) {
            mTabControllerRef = new WeakReference(tabcontroller);
        }
    }

    private static class TabEditRemove
        implements TabEditCommand {

        public boolean execute(TabContainerLayout tabcontainerlayout, ViewPager viewpager, DynamicPagerAdapter dynamicpageradapter, FragmentTransaction fragmenttransaction) {
            int i = tabcontainerlayout.findTabPosition(mTab);
            tabcontainerlayout.removeTab(mTab);
            Fragment fragment = dynamicpageradapter.getItem(i);
            if(fragment != null) {
                dynamicpageradapter.removeFragment(fragment);
                fragmenttransaction.remove(fragment);
            }
            return true;
        }

        private final android.app.ActionBar.Tab mTab;

        public TabEditRemove(android.app.ActionBar.Tab tab) {
            mTab = tab;
        }
    }

    private static class TabEditAdd
        implements TabEditCommand {

        public boolean execute(TabContainerLayout tabcontainerlayout, ViewPager viewpager, DynamicPagerAdapter dynamicpageradapter, FragmentTransaction fragmenttransaction) {
            tabcontainerlayout.addTab(mTab, false);
            if(mFragment != null) {
                dynamicpageradapter.addFragment(mFragment);
                fragmenttransaction.add(viewpager.getId(), mFragment, mTag);
            }
            return true;
        }

        private final Fragment mFragment;
        private final android.app.ActionBar.Tab mTab;
        private final String mTag;

        public TabEditAdd(android.app.ActionBar.Tab tab, Fragment fragment, String s) {
            mTab = tab;
            mFragment = fragment;
            mTag = s;
        }
    }

    public static interface TabEditCommand {

        public abstract boolean execute(TabContainerLayout tabcontainerlayout, ViewPager viewpager, DynamicPagerAdapter dynamicpageradapter, FragmentTransaction fragmenttransaction);
    }

    public static interface TabEditor {

        public abstract TabEditor add(TabEditCommand tabeditcommand);

        public abstract TabEditor addTab(android.app.ActionBar.Tab tab, Fragment fragment);

        public abstract TabEditor addTab(android.app.ActionBar.Tab tab, Fragment fragment, String s);

        public abstract boolean commit();

        public abstract TabEditor removeTab(android.app.ActionBar.Tab tab);
    }

    private class TabImpl extends android.app.ActionBar.Tab {

        public android.app.ActionBar.TabListener getCallback() {
            return mCallback;
        }

        public CharSequence getContentDescription() {
            return mContentDesc;
        }

        public View getCustomView() {
            return mCustomView;
        }

        public Drawable getIcon() {
            return mIcon;
        }

        public int getPosition() {
            throw new UnsupportedOperationException();
        }

        public Object getTag() {
            return mTag;
        }

        public CharSequence getText() {
            return mText;
        }

        public void select() {
            selectTab(this);
        }

        public android.app.ActionBar.Tab setContentDescription(int i) {
            return setContentDescription(mTabContainer.getResources().getText(i));
        }

        public android.app.ActionBar.Tab setContentDescription(CharSequence charsequence) {
            mContentDesc = charsequence;
            return this;
        }

        public android.app.ActionBar.Tab setCustomView(int i) {
            return setCustomView(LayoutInflater.from(mTabContainer.getContext()).inflate(i, null));
        }

        public android.app.ActionBar.Tab setCustomView(View view) {
            mCustomView = view;
            return this;
        }

        public android.app.ActionBar.Tab setIcon(int i) {
            return setIcon(mTabContainer.getResources().getDrawable(i));
        }

        public android.app.ActionBar.Tab setIcon(Drawable drawable) {
            mIcon = drawable;
            return this;
        }

        public android.app.ActionBar.Tab setTabListener(android.app.ActionBar.TabListener tablistener) {
            mCallback = tablistener;
            return this;
        }

        public android.app.ActionBar.Tab setTag(Object obj) {
            mTag = obj;
            return this;
        }

        public android.app.ActionBar.Tab setText(int i) {
            return setText(mTabContainer.getResources().getText(i));
        }

        public android.app.ActionBar.Tab setText(CharSequence charsequence) {
            mText = charsequence;
            return this;
        }

        private android.app.ActionBar.TabListener mCallback;
        private CharSequence mContentDesc;
        private View mCustomView;
        private Drawable mIcon;
        private Object mTag;
        private CharSequence mText;
        final TabController this$0;

        public TabImpl(android.app.ActionBar.TabListener tablistener) {
            this$0 = TabController.this;
            super();
            mCallback = tablistener;
        }
    }


    public TabController(Activity activity, TabContainerLayout tabcontainerlayout, CooperativeViewPager cooperativeviewpager, TabIndicator tabindicator) {
        mViewPagerState = 0;
        mTabContainer = tabcontainerlayout;
        tabcontainerlayout.setTabWidthChangedListener(new TabContainerLayout.TabWidthChangedListener() {

            public void onTabWidthChanged(TabContainerLayout tabcontainerlayout1) {
                mTabIndicator.setTabWidth(tabcontainerlayout1.getTabWidth());
                if(mViewPagerState == 0)
                    mTabIndicator.apply(getSelectedPosition(), 0.0F);
            }

            final TabController this$0;

             {
                this$0 = TabController.this;
                super();
            }
        });
        setTabIndicator(tabindicator);
        mAdapter = new MyDynamicPagerAdapter(activity.getFragmentManager());
        mViewPager = cooperativeviewpager;
        mViewPager.setAdapter(mAdapter);
        mViewPager.setOnPageChangeListener(mViewPagerListenerDecorator);
    }

    public void addTab(android.app.ActionBar.Tab tab, Fragment fragment) {
        newTabEditor().addTab(tab, fragment).commit();
    }

    public Fragment findFragmentByTag(String s) {
        return mAdapter.findFragmentByTag(s);
    }

    public Fragment getFragment(int i) {
        return mAdapter.getItem(i);
    }

    public int getFragmentCount() {
        return mAdapter.getCount();
    }

    public Fragment getSelectedFragment() {
        return mAdapter.getItem(getSelectedPosition());
    }

    public int getSelectedPosition() {
        return mViewPager.getCurrentItem();
    }

    public android.app.ActionBar.Tab getSelectedTab() {
        return mTabContainer.getSelectedTab();
    }

    public TabContainerLayout getTabContainer() {
        return mTabContainer;
    }

    public ViewPager getViewPager() {
        return mViewPager;
    }

    public int getViewPagerState() {
        return mViewPagerState;
    }

    public android.app.ActionBar.Tab newTab() {
        return new TabImpl(null);
    }

    public android.app.ActionBar.Tab newTab(android.app.ActionBar.TabListener tablistener) {
        return new TabImpl(tablistener);
    }

    public TabEditor newTabEditor() {
        return new TabEditorImpl(this);
    }

    protected void onTabReselected(android.app.ActionBar.Tab tab) {
        if(tab != null) goto _L2; else goto _L1
_L1:
        return;
_L2:
        android.app.ActionBar.TabListener tablistener = ((TabImpl)tab).getCallback();
        if(tablistener != null) {
            FragmentTransaction fragmenttransaction = mAdapter.getFragmentManager().beginTransaction().disallowAddToBackStack();
            tablistener.onTabReselected(tab, fragmenttransaction);
            if(!fragmenttransaction.isEmpty())
                fragmenttransaction.commit();
        }
        if(true) goto _L1; else goto _L3
_L3:
    }

    protected void onTabSelected(android.app.ActionBar.Tab tab) {
        if(tab != null) goto _L2; else goto _L1
_L1:
        return;
_L2:
        int i = mTabContainer.findTabPosition(tab);
        if(i >= 0 && i < mAdapter.getCount() && i != mViewPager.getCurrentItem())
            mViewPager.setCurrentItem(i, true);
        android.app.ActionBar.TabListener tablistener = ((TabImpl)tab).getCallback();
        if(tablistener != null) {
            FragmentTransaction fragmenttransaction = mAdapter.getFragmentManager().beginTransaction().disallowAddToBackStack();
            tablistener.onTabSelected(tab, fragmenttransaction);
            if(!fragmenttransaction.isEmpty())
                fragmenttransaction.commit();
        }
        if(true) goto _L1; else goto _L3
_L3:
    }

    protected void onTabUnselected(android.app.ActionBar.Tab tab) {
        if(tab != null) goto _L2; else goto _L1
_L1:
        return;
_L2:
        android.app.ActionBar.TabListener tablistener = ((TabImpl)tab).getCallback();
        if(tablistener != null) {
            FragmentTransaction fragmenttransaction = mAdapter.getFragmentManager().beginTransaction().disallowAddToBackStack();
            tablistener.onTabUnselected(tab, fragmenttransaction);
            if(!fragmenttransaction.isEmpty())
                fragmenttransaction.commit();
        }
        if(true) goto _L1; else goto _L3
_L3:
    }

    public void release() {
        mViewPager.setAdapter(null);
        mTabContainer.detachIndicator(mTabIndicator);
    }

    public void removeAll() {
        TabEditor tabeditor = newTabEditor();
        int i = mTabContainer.getTabCount();
        for(int j = 0; j < i; j++)
            tabeditor.removeTab(mTabContainer.getTabAt(j));

        tabeditor.commit();
    }

    public void removeTab(android.app.ActionBar.Tab tab) {
        if(tab != null)
            newTabEditor().removeTab(tab).commit();
    }

    public void removeTabAt(int i) {
        removeTab(mTabContainer.getTabAt(i));
    }

    public boolean selectTab(int i) {
        return selectTab(mTabContainer.getTabAt(i));
    }

    public boolean selectTab(android.app.ActionBar.Tab tab) {
        boolean flag = false;
        android.app.ActionBar.Tab tab1 = mTabContainer.selectTab(tab);
        if(tab == tab1) {
            onTabReselected(tab);
        } else {
            flag = true;
            onTabUnselected(tab1);
            onTabSelected(tab);
        }
        return flag;
    }

    public void setCooperative(boolean flag) {
        boolean flag1 = true;
        TabContainerLayout tabcontainerlayout = mTabContainer;
        boolean flag2;
        CooperativeViewPager cooperativeviewpager;
        if(!flag)
            flag2 = flag1;
        else
            flag2 = false;
        tabcontainerlayout.setInteractive(flag2);
        cooperativeviewpager = mViewPager;
        if(flag)
            flag1 = false;
        cooperativeviewpager.setDraggable(flag1);
    }

    public void setPagerAdapterChangedListener(PagerAdapterChangedListener pageradapterchangedlistener) {
        mAdapterChangedListener = pageradapterchangedlistener;
    }

    public void setTabIndicator(TabIndicator tabindicator) {
        if(mTabIndicator != null)
            mTabContainer.detachIndicator(mTabIndicator);
        mTabIndicator = tabindicator;
        if(tabindicator != null)
            mTabContainer.attachIndicator(tabindicator);
    }

    public void setTabIndicatorImage(Drawable drawable) {
        mTabIndicator.setIndicator(drawable);
    }

    public void setViewPagerBackground(Drawable drawable) {
        mViewPager.setBackground(drawable);
    }

    public void setViewPagerListener(android.support.v4.view.ViewPager.OnPageChangeListener onpagechangelistener) {
        mViewPagerListener = onpagechangelistener;
    }

    static final String TAG = miui/v5/widget/TabController.getName();
    final DynamicPagerAdapter mAdapter;
    PagerAdapterChangedListener mAdapterChangedListener;
    final TabContainerLayout mTabContainer;
    TabIndicator mTabIndicator;
    final CooperativeViewPager mViewPager;
    android.support.v4.view.ViewPager.OnPageChangeListener mViewPagerListener;
    private final android.support.v4.view.ViewPager.OnPageChangeListener mViewPagerListenerDecorator = new android.support.v4.view.ViewPager.OnPageChangeListener() {

        public void onPageScrollStateChanged(int i) {
            mViewPagerState = i;
            if(mViewPagerListener != null)
                mViewPagerListener.onPageScrollStateChanged(i);
        }

        public void onPageScrolled(int i, float f, int j) {
            if(mTabIndicator != null)
                mTabIndicator.apply(i, f);
            if(mViewPagerListener != null)
                mViewPagerListener.onPageScrolled(i, f, j);
        }

        public void onPageSelected(int i) {
            if(i >= 0 && i < mTabContainer.getTabCount() && i != mTabContainer.findCurrentTabPos())
                selectTab(i);
            if(mViewPagerListener != null)
                mViewPagerListener.onPageSelected(i);
        }

        final TabController this$0;

             {
                this$0 = TabController.this;
                super();
            }
    };
    int mViewPagerState;

}
