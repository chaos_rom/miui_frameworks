// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.v5.widget;

import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ScrollView;
import miui.v5.util.Factory;

// Referenced classes of package miui.v5.widget:
//            MotionDetectStrategy, TabController, Views

public class VerticalMotionStrategies {
    private static class ScrollViewMotionListener
        implements MotionDetectStrategy {

        public boolean isMovable(View view, int i, int j, int k, int l) {
            boolean flag;
            if(!VerticalMotionStrategies.canScrollViewScroll(mScroll, i, j, k, l))
                flag = true;
            else
                flag = false;
            return flag;
        }

        private final ScrollView mScroll;

        public ScrollViewMotionListener(ScrollView scrollview) {
            mScroll = scrollview;
        }
    }

    private static class ListMotionListener
        implements MotionDetectStrategy {

        public boolean isMovable(View view, int i, int j, int k, int l) {
            boolean flag;
            if(!VerticalMotionStrategies.canListScroll(mList, i, j, k, l))
                flag = true;
            else
                flag = false;
            return flag;
        }

        private final AdapterView mList;

        public ListMotionListener(AdapterView adapterview) {
            mList = adapterview;
        }
    }

    private static class TabControllerMotionListener
        implements MotionDetectStrategy {

        public boolean isMovable(View view, int i, int j, int k, int l) {
            boolean flag = true;
            if(mTabController.getViewPagerState() == 0) goto _L2; else goto _L1
_L1:
            flag = false;
_L4:
            return flag;
_L2:
            if(!Views.isContainPoint(mTabController.getTabContainer(), k, l))
                if(j < l) {
                    if(view.getY() <= (float)mMinY)
                        flag = false;
                } else
                if(mPagerStrategyFactory != null) {
                    int i1 = mTabController.getViewPager().getCurrentItem();
                    MotionDetectStrategy motiondetectstrategy = (MotionDetectStrategy)mPagerStrategyFactory.create(Integer.valueOf(i1));
                    if(motiondetectstrategy != null)
                        flag = motiondetectstrategy.isMovable(view, i, j, k, l);
                }
            if(true) goto _L4; else goto _L3
_L3:
        }

        private final int mMinY;
        private final Factory mPagerStrategyFactory;
        private final TabController mTabController;

        public TabControllerMotionListener(TabController tabcontroller, int i, Factory factory) {
            mTabController = tabcontroller;
            mMinY = i;
            mPagerStrategyFactory = factory;
        }
    }


    public VerticalMotionStrategies() {
    }

    public static boolean canListScroll(AdapterView adapterview, int i, int j, int k, int l) {
        boolean flag;
        if(j == l)
            flag = false;
        else
        if(j > l)
            flag = canListScrollDown(adapterview);
        else
            flag = canListScrollUp(adapterview);
        return flag;
    }

    private static boolean canListScrollDown(AdapterView adapterview) {
        boolean flag = true;
        if(adapterview.getFirstVisiblePosition() <= 0) goto _L2; else goto _L1
_L1:
        return flag;
_L2:
        int i = adapterview.getPaddingTop();
        int j = adapterview.getChildCount();
        for(int k = 0; k < j; k++)
            if(adapterview.getChildAt(k).getTop() < i)
                continue; /* Loop/switch isn't completed */

        flag = false;
        if(true) goto _L1; else goto _L3
_L3:
    }

    private static boolean canListScrollUp(AdapterView adapterview) {
        boolean flag = true;
        if(adapterview.getLastVisiblePosition() >= -1 + adapterview.getCount()) goto _L2; else goto _L1
_L1:
        return flag;
_L2:
        int i = adapterview.getHeight() - adapterview.getPaddingBottom() - adapterview.getPaddingTop();
        int j = adapterview.getChildCount();
        for(int k = 0; k < j; k++)
            if(adapterview.getChildAt(k).getBottom() >= i)
                continue; /* Loop/switch isn't completed */

        flag = false;
        if(true) goto _L1; else goto _L3
_L3:
    }

    public static boolean canScrollViewScroll(ScrollView scrollview, int i, int j, int k, int l) {
        boolean flag;
        if(j > l)
            flag = canScrollViewScrollDown(scrollview);
        else
            flag = canScrollViewScrollUp(scrollview);
        return flag;
    }

    private static boolean canScrollViewScrollDown(ScrollView scrollview) {
        boolean flag;
        flag = false;
        break MISSING_BLOCK_LABEL_2;
        if(scrollview.getChildAt(0) != null && scrollview.getScrollY() > 0)
            flag = true;
        return flag;
    }

    private static boolean canScrollViewScrollUp(ScrollView scrollview) {
        boolean flag;
        View view;
        flag = false;
        view = scrollview.getChildAt(0);
        if(view != null) goto _L2; else goto _L1
_L1:
        return flag;
_L2:
        int i = scrollview.getHeight() - scrollview.getPaddingBottom() - scrollview.getPaddingTop();
        if(view.getBottom() > i)
            flag = true;
        if(true) goto _L1; else goto _L3
_L3:
    }

    public static MotionDetectStrategy makeMotionStrategyForList(AdapterView adapterview) {
        ListMotionListener listmotionlistener;
        if(adapterview != null)
            listmotionlistener = new ListMotionListener(adapterview);
        else
            listmotionlistener = null;
        return listmotionlistener;
    }

    public static MotionDetectStrategy makeMotionStrategyForScrollView(ScrollView scrollview) {
        ScrollViewMotionListener scrollviewmotionlistener;
        if(scrollview != null)
            scrollviewmotionlistener = new ScrollViewMotionListener(scrollview);
        else
            scrollviewmotionlistener = null;
        return scrollviewmotionlistener;
    }

    public static MotionDetectStrategy makeMotionStrategyTabController(TabController tabcontroller, int i, Factory factory) {
        TabControllerMotionListener tabcontrollermotionlistener;
        if(tabcontroller != null)
            tabcontrollermotionlistener = new TabControllerMotionListener(tabcontroller, i, factory);
        else
            tabcontrollermotionlistener = null;
        return tabcontrollermotionlistener;
    }
}
