// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.v5.widget.menubar;

import android.animation.*;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Parcelable;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import java.util.ArrayList;

// Referenced classes of package miui.v5.widget.menubar:
//            MenuBarPresenter, IconMenuBarView, MenuBarItem, MenuBar, 
//            IconMenuBarPrimaryItemView, IconMenuBarSecondaryItemView, MenuBarView

public class IconMenuBarPresenter
    implements MenuBarPresenter {
    private class MenuBarAnimatorListener
        implements android.animation.Animator.AnimatorListener {

        public void onAnimationCancel(Animator animator) {
        }

        public void onAnimationEnd(Animator animator) {
            if(mCallback != null)
                if(animator == mOpenMenuBarAnimator) {
                    LinearLayout linearlayout = mMenuView.getPrimaryContainer();
                    linearlayout.setVisibility(0);
                    linearlayout.startLayoutAnimation();
                    mCallback.onOpenMenu(mMenu, true);
                    mOpenMenuBarAnimator = null;
                } else {
                    mCallback.onCloseMenu(mMenu, true);
                    mMenuView.setVisibility(8);
                    mCloseMenuBarAnimator = null;
                }
        }

        public void onAnimationRepeat(Animator animator) {
        }

        public void onAnimationStart(Animator animator) {
            if(animator == mOpenMenuBarAnimator)
                mMenuView.setVisibility(0);
        }

        final IconMenuBarPresenter this$0;

        public MenuBarAnimatorListener() {
            this$0 = IconMenuBarPresenter.this;
            super();
        }
    }


    public IconMenuBarPresenter(Context context, int i, int j, int k) {
        mMaxItems = 4;
        mContext = context;
        mIconMenuBarLayoutResId = i;
        mIconMenuBarPrimaryItemResId = j;
        mIconMenuBarSecondaryItemResId = k;
        mMenuBarAnimatorListener = new MenuBarAnimatorListener();
    }

    protected void addItemView(ViewGroup viewgroup, View view, int i) {
        ViewGroup viewgroup1 = (ViewGroup)view.getParent();
        if(viewgroup1 != null)
            viewgroup1.removeView(view);
        viewgroup.addView(view, i);
    }

    public boolean collapseItemActionView(MenuBar menubar, MenuBarItem menubaritem) {
        return false;
    }

    protected MenuBarView.ItemView createPrimaryItemView() {
        return (MenuBarView.ItemView)View.inflate(mContext, mIconMenuBarPrimaryItemResId, null);
    }

    protected MenuBarView.ItemView createSecondaryItemView() {
        return (MenuBarView.ItemView)View.inflate(mContext, mIconMenuBarSecondaryItemResId, null);
    }

    public boolean expandItemActionView(MenuBar menubar, MenuBarItem menubaritem) {
        return false;
    }

    protected boolean filterLeftoverView(ViewGroup viewgroup, int i) {
        boolean flag;
        if(viewgroup.getChildAt(i) != mMoreView) {
            viewgroup.removeViewAt(i);
            flag = true;
        } else {
            flag = false;
        }
        return flag;
    }

    public boolean flagActionItems() {
        return false;
    }

    public int getId() {
        return 0;
    }

    public int getMaxItems() {
        return mMaxItems;
    }

    public MenuBarView getMenuView(ViewGroup viewgroup) {
        if(mMenuView == null) {
            mMenuView = (IconMenuBarView)View.inflate(mContext, mIconMenuBarLayoutResId, null);
            mMenuView.setVisibility(8);
            mMenuView.initialize(mMenu);
            mMenuView.setPrimaryMaskDrawable(mPrimaryMaskDrawable);
            mMenuView.setPrimaryContainerLayoutAnimation(AnimationUtils.loadLayoutAnimation(mContext, 0x6040016));
            viewgroup.addView(mMenuView);
        }
        return mMenuView;
    }

    protected View getPrimaryItemView(MenuBarItem menubaritem, View view) {
        MenuBarView.ItemView itemview;
        if(view instanceof MenuBarView.ItemView)
            itemview = (MenuBarView.ItemView)view;
        else
            itemview = createPrimaryItemView();
        itemview.initialize(menubaritem, 0);
        menubaritem.setTag(itemview);
        return (View)itemview;
    }

    public View getSecondaryItemView(MenuBarItem menubaritem, View view) {
        MenuBarView.ItemView itemview;
        if(view instanceof MenuBarView.ItemView)
            itemview = (MenuBarView.ItemView)view;
        else
            itemview = createSecondaryItemView();
        itemview.initialize(menubaritem, 0);
        menubaritem.setTag(itemview);
        return (View)itemview;
    }

    public void initForMenu(Context context, MenuBar menubar) {
        mContext = context;
        mMenu = menubar;
    }

    public void onCloseMenu(MenuBar menubar, boolean flag) {
        if(mMenuView != null) goto _L2; else goto _L1
_L1:
        return;
_L2:
        mMenuView.requestExpand(false);
        if(flag) {
            if(mMenuView.getVisibility() == 0)
                playCloseMenuBarAnimator();
        } else {
            mMenuView.setVisibility(8);
            if(mCallback != null)
                mCallback.onCloseMenu(mMenu, true);
        }
        if(true) goto _L1; else goto _L3
_L3:
    }

    public boolean onExpandMenu(MenuBar menubar, boolean flag) {
        boolean flag1;
        if(mMenuView != null)
            flag1 = mMenuView.requestExpand(flag);
        else
            flag1 = false;
        return flag1;
    }

    public void onOpenMenu(MenuBar menubar, boolean flag) {
        if(mMenuView != null) goto _L2; else goto _L1
_L1:
        return;
_L2:
        updateMenuView(true);
        int i = mMenuView.getVisibility();
        mMenuView.setVisibility(0);
        if(flag) {
            if(i != 0)
                playOpenMenuBarAnimator();
            else
                mMenuView.getPrimaryContainer().startLayoutAnimation();
        } else
        if(mCallback != null)
            mCallback.onOpenMenu(mMenu, true);
        if(true) goto _L1; else goto _L3
_L3:
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
    }

    public Parcelable onSaveInstanceState() {
        return null;
    }

    protected void playCloseMenuBarAnimator() {
        LinearLayout linearlayout = mMenuView.getPrimaryContainer();
        IconMenuBarView iconmenubarview = mMenuView;
        float af[] = new float[2];
        af[0] = 0.0F;
        af[1] = linearlayout.getHeight();
        ObjectAnimator objectanimator = ObjectAnimator.ofFloat(iconmenubarview, "TranslationY", af);
        objectanimator.setDuration(300L);
        IconMenuBarView iconmenubarview1 = mMenuView;
        float af1[] = new float[2];
        af1[0] = 1.0F;
        af1[1] = 0.0F;
        ObjectAnimator objectanimator1 = ObjectAnimator.ofFloat(iconmenubarview1, "Alpha", af1);
        objectanimator1.setDuration(300L);
        AnimatorSet animatorset = new AnimatorSet();
        animatorset.setDuration(300L);
        Animator aanimator[] = new Animator[2];
        aanimator[0] = objectanimator;
        aanimator[1] = objectanimator1;
        animatorset.playTogether(aanimator);
        animatorset.addListener(mMenuBarAnimatorListener);
        animatorset.start();
        mCloseMenuBarAnimator = animatorset;
    }

    protected void playOpenMenuBarAnimator() {
        LinearLayout linearlayout = mMenuView.getPrimaryContainer();
        linearlayout.setVisibility(8);
        IconMenuBarView iconmenubarview = mMenuView;
        float af[] = new float[2];
        af[0] = linearlayout.getHeight();
        af[1] = 0.0F;
        ObjectAnimator objectanimator = ObjectAnimator.ofFloat(iconmenubarview, "TranslationY", af);
        objectanimator.setDuration(300L);
        IconMenuBarView iconmenubarview1 = mMenuView;
        float af1[] = new float[2];
        af1[0] = 0.0F;
        af1[1] = 1.0F;
        ObjectAnimator objectanimator1 = ObjectAnimator.ofFloat(iconmenubarview1, "Alpha", af1);
        objectanimator1.setDuration(300L);
        AnimatorSet animatorset = new AnimatorSet();
        animatorset.setDuration(300L);
        Animator aanimator[] = new Animator[2];
        aanimator[0] = objectanimator;
        aanimator[1] = objectanimator1;
        animatorset.playTogether(aanimator);
        animatorset.addListener(mMenuBarAnimatorListener);
        animatorset.start();
        mOpenMenuBarAnimator = animatorset;
    }

    public void scroll(float f) {
        if(mMenuView != null)
            mMenuView.scroll(f);
    }

    public void scrollStateChanged(int i) {
        if(mMenuView != null)
            mMenuView.scrollStateChanged(i);
    }

    public void setCallback(MenuBarPresenter.Callback callback) {
        mCallback = callback;
    }

    public void setMaxItems(int i) {
        if(i >= 4)
            mMaxItems = i;
    }

    public void setMoreIconDrawable(Drawable drawable) {
        mMoreIconDrawable = drawable;
    }

    public void setPrimaryMaskDrawable(Drawable drawable) {
        mPrimaryMaskDrawable = drawable;
    }

    public void updateMenuView(boolean flag) {
        ArrayList arraylist;
        int i;
        boolean flag1;
        IconMenuBarView iconmenubarview = mMenuView;
        if(mMaxItems < 0)
            mMaxItems = iconmenubarview.getMaxItems();
        arraylist = mMenu.getVisibleItems();
        i = arraylist.size();
        if(i > mMaxItems)
            flag1 = true;
        else
            flag1 = false;
        break MISSING_BLOCK_LABEL_46;
        int i1;
        int j1;
        while(true)  {
label0:
            {
                LinearLayout linearlayout;
                int k;
                LinearLayout linearlayout2;
                do {
                    do
                        return;
                    while(mMenuView == null || mMenu == null);
                    int j;
                    if(flag1)
                        j = -1 + mMaxItems;
                    else
                        j = arraylist.size();
                    linearlayout = mMenuView.getPrimaryContainer();
                    k = 0;
                    while(k < j)  {
                        MenuBarItem menubaritem2 = (MenuBarItem)arraylist.get(k);
                        menubaritem2.setIsSecondary(false);
                        View view3 = linearlayout.getChildAt(k);
                        MenuBarItem menubaritem3;
                        View view4;
                        if(view3 instanceof MenuBarView.ItemView)
                            menubaritem3 = ((MenuBarView.ItemView)view3).getItemData();
                        else
                            menubaritem3 = null;
                        view4 = getPrimaryItemView(menubaritem2, view3);
                        view4.setTag(0x7fffffff, null);
                        if(menubaritem2 != menubaritem3) {
                            view4.setPressed(false);
                            view4.jumpDrawablesToCurrentState();
                        }
                        if(view4 != view3) {
                            addItemView(linearlayout, view4, k);
                            ((IconMenuBarPrimaryItemView)view4).setItemInvoker(mMenuView);
                        }
                        k++;
                    }
                    if(!flag1)
                        break;
                    View view = linearlayout.getChildAt(k);
                    Object obj;
                    if(view != null)
                        obj = view.getTag(0x7fffffff);
                    else
                        obj = null;
                    if(view == null || obj == null) {
                        mMoreView = mMenuView.createMoreItemView((IconMenuBarPrimaryItemView)view, mIconMenuBarPrimaryItemResId, mMoreIconDrawable);
                        if(mMoreView != view)
                            addItemView(linearlayout, mMoreView, k);
                    }
                    linearlayout2 = mMenuView.getSecondaryContainer();
                    i1 = 0;
                    while(k < i)  {
                        MenuBarItem menubaritem = (MenuBarItem)arraylist.get(k);
                        menubaritem.setIsSecondary(true);
                        int k1 = i1 + 1;
                        View view1 = linearlayout2.getChildAt(i1);
                        MenuBarItem menubaritem1;
                        View view2;
                        if(view1 instanceof MenuBarView.ItemView)
                            menubaritem1 = ((MenuBarView.ItemView)view1).getItemData();
                        else
                            menubaritem1 = null;
                        view2 = getSecondaryItemView(menubaritem, view1);
                        if(menubaritem != menubaritem1) {
                            view2.setPressed(false);
                            view2.jumpDrawablesToCurrentState();
                        }
                        if(view2 != view1) {
                            addItemView(linearlayout2, view2, k - j);
                            ((IconMenuBarSecondaryItemView)view2).setItemInvoker(mMenuView);
                        }
                        k++;
                        i1 = k1;
                    }
                    break label0;
                } while(true);
                do {
                    if(k >= linearlayout.getChildCount())
                        break;
                    if(!filterLeftoverView(linearlayout, k))
                        k++;
                } while(true);
                int l = 0;
                LinearLayout linearlayout1 = mMenuView.getSecondaryContainer();
                while(l < linearlayout1.getChildCount()) 
                    if(!filterLeftoverView(linearlayout1, l))
                        l++;
            }
        }
        j1 = i1;
        while(j1 < linearlayout2.getChildCount()) 
            if(!filterLeftoverView(linearlayout2, j1))
                j1++;
        continue;
    }

    private static final int DEFAULT_CLOSE_ANIMATOR_DURATION = 300;
    private static final int DEFAULT_OPEN_ANIMATOR_DURATION = 300;
    private static final int MAX_ITEMS = 4;
    MenuBarPresenter.Callback mCallback;
    private Animator mCloseMenuBarAnimator;
    private Context mContext;
    private int mIconMenuBarLayoutResId;
    private int mIconMenuBarPrimaryItemResId;
    private int mIconMenuBarSecondaryItemResId;
    private int mMaxItems;
    private MenuBar mMenu;
    private MenuBarAnimatorListener mMenuBarAnimatorListener;
    private IconMenuBarView mMenuView;
    private Drawable mMoreIconDrawable;
    private IconMenuBarPrimaryItemView mMoreView;
    private Animator mOpenMenuBarAnimator;
    private Drawable mPrimaryMaskDrawable;



/*
    static Animator access$002(IconMenuBarPresenter iconmenubarpresenter, Animator animator) {
        iconmenubarpresenter.mOpenMenuBarAnimator = animator;
        return animator;
    }

*/




/*
    static Animator access$302(IconMenuBarPresenter iconmenubarpresenter, Animator animator) {
        iconmenubarpresenter.mCloseMenuBarAnimator = animator;
        return animator;
    }

*/
}
