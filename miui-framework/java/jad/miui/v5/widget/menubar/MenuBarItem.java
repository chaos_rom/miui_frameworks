// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.v5.widget.menubar;

import android.content.*;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.*;

// Referenced classes of package miui.v5.widget.menubar:
//            MenuBar

public class MenuBarItem
    implements MenuItem {

    public MenuBarItem(MenuBar menubar, int i, int j, CharSequence charsequence) {
        mMenu = menubar;
        mId = i;
        mOrder = j;
        mTitle = charsequence;
        mFlags = 0x10 | mFlags;
    }

    public boolean collapseActionView() {
        return false;
    }

    public boolean expandActionView() {
        return false;
    }

    public ActionProvider getActionProvider() {
        return null;
    }

    public View getActionView() {
        return null;
    }

    public char getAlphabeticShortcut() {
        return '\0';
    }

    public Drawable getBackground() {
        Drawable drawable;
        if(mBackgroundDrawable != null)
            drawable = mBackgroundDrawable;
        else
        if(mBackgroundResId != 0)
            drawable = mMenu.getContext().getResources().getDrawable(mBackgroundResId);
        else
            drawable = null;
        return drawable;
    }

    public int getGroupId() {
        return 0;
    }

    public Drawable getIcon() {
        Drawable drawable;
        if(mIconDrawable != null)
            drawable = mIconDrawable;
        else
        if(mIconResId != 0)
            drawable = mMenu.getContext().getResources().getDrawable(mIconResId);
        else
            drawable = null;
        return drawable;
    }

    public Intent getIntent() {
        return mIntent;
    }

    public int getItemId() {
        return mId;
    }

    public android.view.ContextMenu.ContextMenuInfo getMenuInfo() {
        return null;
    }

    public char getNumericShortcut() {
        return '\0';
    }

    public int getOrder() {
        return mOrder;
    }

    public SubMenu getSubMenu() {
        return null;
    }

    Object getTag() {
        return mTag;
    }

    public CharSequence getTitle() {
        return mTitle;
    }

    public CharSequence getTitleCondensed() {
        return null;
    }

    public boolean hasSubMenu() {
        return false;
    }

    public boolean invoke() {
        boolean flag;
        flag = true;
        break MISSING_BLOCK_LABEL_2;
        while(true)  {
            do
                return flag;
            while(mClickListener != null && mClickListener.onMenuItemClick(this) || mMenu.dispatchMenuItemSelected(mMenu, this));
            if(mItemCallback != null) {
                mItemCallback.run();
                continue;
            }
            if(mIntent != null)
                try {
                    mMenu.getContext().startActivity(mIntent);
                    continue;
                }
                catch(ActivityNotFoundException activitynotfoundexception) { }
            flag = false;
        }
    }

    public boolean isActionViewExpanded() {
        return false;
    }

    public boolean isCheckable() {
        boolean flag;
        if((1 & mFlags) != 0)
            flag = true;
        else
            flag = false;
        return flag;
    }

    public boolean isChecked() {
        boolean flag;
        if((2 & mFlags) != 0)
            flag = true;
        else
            flag = false;
        return flag;
    }

    public boolean isEnabled() {
        boolean flag;
        if((0x10 & mFlags) != 0)
            flag = true;
        else
            flag = false;
        return flag;
    }

    boolean isSecondary() {
        boolean flag;
        if((0x80000000 & mFlags) != 0)
            flag = true;
        else
            flag = false;
        return flag;
    }

    public boolean isVisible() {
        boolean flag;
        if((8 & mFlags) == 0)
            flag = true;
        else
            flag = false;
        return flag;
    }

    public MenuItem setActionProvider(ActionProvider actionprovider) {
        return null;
    }

    public MenuItem setActionView(int i) {
        return this;
    }

    public MenuItem setActionView(View view) {
        return null;
    }

    public MenuItem setAlphabeticShortcut(char c) {
        return this;
    }

    public MenuItem setCheckable(boolean flag) {
        Object obj;
        if(flag)
            mFlags = 1 | mFlags;
        else
            mFlags = -2 & mFlags;
        obj = getTag();
        if(obj instanceof MenuBarView.ItemView)
            ((MenuBarView.ItemView)obj).setCheckable(flag);
        else
            mMenu.onItemsChanged(false);
        return this;
    }

    public MenuItem setChecked(boolean flag) {
        Object obj;
        if(flag)
            mFlags = 2 | mFlags;
        else
            mFlags = -3 & mFlags;
        obj = getTag();
        if(obj instanceof MenuBarView.ItemView)
            ((MenuBarView.ItemView)obj).setChecked(flag);
        else
            mMenu.onItemsChanged(false);
        return this;
    }

    public MenuItem setEnabled(boolean flag) {
        Object obj;
        if(flag)
            mFlags = 0x10 | mFlags;
        else
            mFlags = 0xffffffef & mFlags;
        obj = getTag();
        if(obj instanceof MenuBarView.ItemView)
            ((MenuBarView.ItemView)obj).setEnabled(flag);
        else
            mMenu.onItemsChanged(false);
        return this;
    }

    public MenuItem setIcon(int i) {
        mIconDrawable = null;
        mIconResId = i;
        Object obj = getTag();
        if(obj instanceof MenuBarView.ItemView)
            ((MenuBarView.ItemView)obj).setIcon(mMenu.getContext().getResources().getDrawable(i));
        else
            mMenu.onItemsChanged(false);
        return this;
    }

    public MenuItem setIcon(Drawable drawable) {
        mIconResId = 0;
        mIconDrawable = drawable;
        Object obj = getTag();
        if(obj instanceof MenuBarView.ItemView)
            ((MenuBarView.ItemView)obj).setIcon(drawable);
        else
            mMenu.onItemsChanged(false);
        return this;
    }

    public MenuItem setIntent(Intent intent) {
        mIntent = null;
        mIntent = intent;
        return this;
    }

    void setIsSecondary(boolean flag) {
        if(flag)
            mFlags = 0x80000000 | mFlags;
        else
            mFlags = 0x7fffffff & mFlags;
    }

    public MenuItem setNumericShortcut(char c) {
        return this;
    }

    public MenuItem setOnActionExpandListener(android.view.MenuItem.OnActionExpandListener onactionexpandlistener) {
        return this;
    }

    public MenuItem setOnMenuItemClickListener(android.view.MenuItem.OnMenuItemClickListener onmenuitemclicklistener) {
        mClickListener = onmenuitemclicklistener;
        return this;
    }

    public MenuItem setShortcut(char c, char c1) {
        return this;
    }

    public void setShowAsAction(int i) {
    }

    public MenuItem setShowAsActionFlags(int i) {
        return this;
    }

    MenuItem setTag(Object obj) {
        mTag = obj;
        return this;
    }

    public MenuItem setTitle(int i) {
        return setTitle(((CharSequence) (mMenu.getContext().getString(i))));
    }

    public MenuItem setTitle(CharSequence charsequence) {
        mTitle = charsequence;
        Object obj = getTag();
        if(obj instanceof MenuBarView.ItemView)
            ((MenuBarView.ItemView)obj).setTitle(charsequence);
        else
            mMenu.onItemsChanged(false);
        return this;
    }

    public MenuItem setTitleCondensed(CharSequence charsequence) {
        return this;
    }

    public MenuItem setVisible(boolean flag) {
        if(setVisibleInt(flag))
            mMenu.onItemVisibleChanged(this);
        return this;
    }

    boolean setVisibleInt(boolean flag) {
        boolean flag1 = false;
        int i = mFlags;
        int j = -9 & mFlags;
        byte byte0;
        if(flag)
            byte0 = 0;
        else
            byte0 = 8;
        mFlags = byte0 | j;
        if(i != mFlags)
            flag1 = true;
        return flag1;
    }

    private static final int CHECKABLE = 1;
    private static final int CHECKED = 2;
    private static final int ENABLED = 16;
    private static final int EXCLUSIVE = 4;
    private static final int HIDDEN = 8;
    private static final int IS_ACTION = 32;
    private static final int IS_SECONDARY = 0x80000000;
    private static final int NO_ICON;
    private Drawable mBackgroundDrawable;
    private int mBackgroundResId;
    private android.view.MenuItem.OnMenuItemClickListener mClickListener;
    private int mFlags;
    private Drawable mIconDrawable;
    private int mIconResId;
    protected final int mId;
    private Intent mIntent;
    private Runnable mItemCallback;
    protected MenuBar mMenu;
    protected final int mOrder;
    private Object mTag;
    protected CharSequence mTitle;
}
