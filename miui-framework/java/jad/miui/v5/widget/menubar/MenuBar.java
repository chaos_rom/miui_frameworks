// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.v5.widget.menubar;

import android.content.*;
import android.content.res.Resources;
import android.view.*;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

// Referenced classes of package miui.v5.widget.menubar:
//            MenuBarPresenter, MenuBarItem

public class MenuBar
    implements Menu {
    public static interface ItemInvoker {

        public abstract boolean invokeItem(MenuBarItem menubaritem);
    }

    public static interface MenuBarScrollHandler {

        public abstract void onScrollStateChanged(int i);

        public abstract void onScrolling(float f);
    }

    public static interface Callback {

        public abstract boolean onCreateMenuBarPanel(Menu menu);

        public abstract void onMenuBarPanelClose(Menu menu);

        public abstract boolean onMenuBarPanelItemSelected(Menu menu, MenuItem menuitem);

        public abstract void onMenuBarPanelModeChange(Menu menu, int i);

        public abstract void onMenuBarPanelOpen(Menu menu);

        public abstract boolean onPrepareMenuBarPanel(Menu menu);
    }


    public MenuBar(Context context) {
        mItems = new ArrayList();
        mVisibleItems = new ArrayList();
        mPresenters = new CopyOnWriteArrayList();
        mMenuBarState = 0;
        mMenuBarScrollHandler = new MenuBarScrollHandler() {

            public void onScrollStateChanged(int i) {
            }

            public void onScrolling(float f) {
                dispatchMenuBarScroll(f);
            }

            final MenuBar this$0;

             {
                this$0 = MenuBar.this;
                super();
            }
        };
        mMenuPresenterCallback = new MenuBarPresenter.Callback() ;
        mContext = context;
    }

    private void dispatchMenuBarScroll(float f) {
        if(!mPresenters.isEmpty()) {
            stopDispatchingItemsChanged();
            for(Iterator iterator = mPresenters.iterator(); iterator.hasNext();) {
                WeakReference weakreference = (WeakReference)iterator.next();
                MenuBarPresenter menubarpresenter = (MenuBarPresenter)weakreference.get();
                if(menubarpresenter == null)
                    mPresenters.remove(weakreference);
                else
                    menubarpresenter.scroll(f);
            }

            startDispatchingItemsChanged();
        }
    }

    private void dispatchPresenterUpdate(boolean flag) {
        if(!mPresenters.isEmpty()) {
            stopDispatchingItemsChanged();
            for(Iterator iterator = mPresenters.iterator(); iterator.hasNext();) {
                WeakReference weakreference = (WeakReference)iterator.next();
                MenuBarPresenter menubarpresenter = (MenuBarPresenter)weakreference.get();
                if(menubarpresenter == null)
                    mPresenters.remove(weakreference);
                else
                    menubarpresenter.updateMenuView(flag);
            }

            startDispatchingItemsChanged();
        }
    }

    private static int findInsertIndex(ArrayList arraylist, int i) {
        int j = -1 + arraylist.size();
_L3:
        if(j < 0)
            break MISSING_BLOCK_LABEL_39;
        if(((MenuBarItem)arraylist.get(j)).getOrder() > i) goto _L2; else goto _L1
_L1:
        int k = j + 1;
_L4:
        return k;
_L2:
        j--;
          goto _L3
        k = 0;
          goto _L4
    }

    private int findItemIndex(int i) {
        int j = mItems.size();
_L3:
        if(j < 0)
            break MISSING_BLOCK_LABEL_38;
        if(((MenuBarItem)mItems.get(j)).getItemId() != i) goto _L2; else goto _L1
_L1:
        return j;
_L2:
        j--;
          goto _L3
        j = -1;
          goto _L1
    }

    private void removeItemAtInt(int i, boolean flag) {
        if(i >= 0 && i < mItems.size() && flag)
            onItemsChanged(true);
    }

    public MenuItem add(int i) {
        return addInternal(0, 0, mContext.getResources().getString(i));
    }

    public MenuItem add(int i, int j, int k, int l) {
        return addInternal(j, k, mContext.getResources().getString(l));
    }

    public MenuItem add(int i, int j, int k, CharSequence charsequence) {
        return addInternal(j, k, charsequence);
    }

    public MenuItem add(CharSequence charsequence) {
        return addInternal(0, 0, charsequence);
    }

    public int addIntentOptions(int i, int j, int k, ComponentName componentname, Intent aintent[], Intent intent, int l, 
            MenuItem amenuitem[]) {
        return 0;
    }

    MenuItem addInternal(int i, int j, CharSequence charsequence) {
        MenuBarItem menubaritem = new MenuBarItem(this, i, j, charsequence);
        mItems.add(findInsertIndex(mItems, j), menubaritem);
        onItemsChanged(true);
        return menubaritem;
    }

    public void addMenuPresenter(MenuBarPresenter menubarpresenter) {
        mPresenters.add(new WeakReference(menubarpresenter));
        menubarpresenter.initForMenu(mContext, this);
        menubarpresenter.setCallback(mMenuPresenterCallback);
    }

    public SubMenu addSubMenu(int i) {
        return null;
    }

    public SubMenu addSubMenu(int i, int j, int k, int l) {
        return null;
    }

    public SubMenu addSubMenu(int i, int j, int k, CharSequence charsequence) {
        return null;
    }

    public SubMenu addSubMenu(CharSequence charsequence) {
        return null;
    }

    public void clear() {
        mItems.clear();
        onItemsChanged(true);
    }

    public void close() {
        close(true);
    }

    final void close(boolean flag) {
        if(mMenuBarState != 3 && mMenuBarState != 1 && mMenuBarState != 0) goto _L2; else goto _L1
_L1:
        return;
_L2:
        boolean flag1 = false;
        mCloseMenuBarViewCount = 0;
        for(Iterator iterator = mPresenters.iterator(); iterator.hasNext();) {
            WeakReference weakreference = (WeakReference)iterator.next();
            MenuBarPresenter menubarpresenter = (MenuBarPresenter)weakreference.get();
            if(menubarpresenter == null) {
                mPresenters.remove(weakreference);
            } else {
                flag1 = true;
                mCloseMenuBarViewCount = 1 + mCloseMenuBarViewCount;
                menubarpresenter.onCloseMenu(this, flag);
            }
        }

        if(!flag1)
            mMenuBarState = 0;
        if(true) goto _L1; else goto _L3
_L3:
    }

    void dispatchMenuClose() {
        if(mCallback != null)
            mCallback.onMenuBarPanelClose(this);
    }

    boolean dispatchMenuItemSelected(MenuBar menubar, MenuItem menuitem) {
        boolean flag;
        if(mCallback != null && mCallback.onMenuBarPanelItemSelected(menubar, menuitem))
            flag = true;
        else
            flag = false;
        return flag;
    }

    void dispatchMenuModeChange(int i) {
        if(mCallback != null)
            mCallback.onMenuBarPanelModeChange(this, i);
    }

    void dispatchMenuOpen() {
        if(mCallback != null)
            mCallback.onMenuBarPanelOpen(this);
    }

    public boolean expand(boolean flag) {
        boolean flag1;
        if(mMenuBarState != 2) {
            flag1 = false;
        } else {
            flag1 = false;
            Iterator iterator = mPresenters.iterator();
            while(iterator.hasNext())  {
                WeakReference weakreference = (WeakReference)iterator.next();
                MenuBarPresenter menubarpresenter = (MenuBarPresenter)weakreference.get();
                if(menubarpresenter == null)
                    mPresenters.remove(weakreference);
                else
                    flag1 |= menubarpresenter.onExpandMenu(this, flag);
            }
        }
        return flag1;
    }

    public MenuItem findItem(int i) {
        int j;
        int k;
        j = size();
        k = 0;
_L3:
        MenuBarItem menubaritem;
        if(k >= j)
            break MISSING_BLOCK_LABEL_43;
        menubaritem = (MenuBarItem)mItems.get(k);
        if(menubaritem.getItemId() != i) goto _L2; else goto _L1
_L1:
        return menubaritem;
_L2:
        k++;
          goto _L3
        menubaritem = null;
          goto _L1
    }

    public Context getContext() {
        return mContext;
    }

    public MenuItem getItem(int i) {
        MenuItem menuitem;
        if(i < 0 || i >= mItems.size())
            menuitem = null;
        else
            menuitem = (MenuItem)mItems.get(i);
        return menuitem;
    }

    public MenuBarScrollHandler getMenuBarSrollHandler() {
        return mMenuBarScrollHandler;
    }

    ArrayList getVisibleItems() {
        mVisibleItems.clear();
        int i = mItems.size();
        int j = 0;
        while(j < i)  {
            MenuBarItem menubaritem = (MenuBarItem)mItems.get(j);
            if(menubaritem.isVisible())
                mVisibleItems.add(menubaritem);
            else
                menubaritem.setTag(null);
            j++;
        }
        return mVisibleItems;
    }

    public boolean hasVisibleItems() {
        int i;
        int j;
        i = size();
        j = 0;
_L3:
        if(j >= i)
            break MISSING_BLOCK_LABEL_39;
        if(!((MenuBarItem)mItems.get(j)).isVisible()) goto _L2; else goto _L1
_L1:
        boolean flag = true;
_L4:
        return flag;
_L2:
        j++;
          goto _L3
        flag = false;
          goto _L4
    }

    public void invalidate() {
        if(mCallback != null)
            mCallback.onPrepareMenuBarPanel(this);
    }

    public boolean isOpen() {
        boolean flag;
        if(mMenuBarState == 2)
            flag = true;
        else
            flag = false;
        return flag;
    }

    public boolean isShortcutKey(int i, KeyEvent keyevent) {
        return false;
    }

    void onItemVisibleChanged(MenuBarItem menubaritem) {
        onItemsChanged(true);
    }

    void onItemsChanged(boolean flag) {
        if(!mPreventDispatchingItemsChanged)
            dispatchPresenterUpdate(flag);
        else
            mItemsChangedWhileDispatchPrevented = true;
    }

    public void open() {
        open(true);
    }

    public void open(boolean flag) {
        if(mMenuBarState != 3 && mMenuBarState != 1) goto _L2; else goto _L1
_L1:
        return;
_L2:
        mPreventDispatchingItemsChanged = true;
        boolean flag1;
        if(mCallback != null) {
            if(mIsCreated) {
                mIsPrepared = mCallback.onPrepareMenuBarPanel(this);
            } else {
                mIsCreated = mCallback.onCreateMenuBarPanel(this);
                if(mIsCreated)
                    mIsPrepared = mCallback.onPrepareMenuBarPanel(this);
                else
                    mIsPrepared = false;
            }
        } else {
            mIsCreated = false;
            mIsPrepared = false;
        }
        mPreventDispatchingItemsChanged = false;
        mItemsChangedWhileDispatchPrevented = false;
        flag1 = false;
        if(mIsCreated && mIsPrepared && getVisibleItems().size() > 0) {
            mOpenMenuBarViewCount = 0;
            for(Iterator iterator = mPresenters.iterator(); iterator.hasNext();) {
                WeakReference weakreference = (WeakReference)iterator.next();
                MenuBarPresenter menubarpresenter = (MenuBarPresenter)weakreference.get();
                if(menubarpresenter == null) {
                    mPresenters.remove(weakreference);
                } else {
                    flag1 = true;
                    mOpenMenuBarViewCount = 1 + mOpenMenuBarViewCount;
                    menubarpresenter.onOpenMenu(this, flag);
                }
            }

        }
        if(!flag1 && mMenuBarState == 2)
            close(true);
        if(true) goto _L1; else goto _L3
_L3:
    }

    public boolean performIdentifierAction(int i, int j) {
        return false;
    }

    public boolean performItemAction(MenuItem menuitem, int i) {
        MenuBarItem menubaritem = (MenuBarItem)menuitem;
        boolean flag;
        if(menubaritem == null || !menubaritem.isEnabled())
            flag = false;
        else
            flag = menubaritem.invoke();
        return flag;
    }

    public boolean performShortcut(int i, KeyEvent keyevent, int j) {
        return false;
    }

    public void removeGroup(int i) {
    }

    public void removeItem(int i) {
        removeItemAtInt(findItemIndex(i), true);
    }

    public void reopen() {
        reopen(true);
    }

    public void reopen(boolean flag) {
        mIsCreated = false;
        mItems.clear();
        mVisibleItems.clear();
        open(flag);
    }

    public void setCallback(Callback callback) {
        mCallback = null;
        mCallback = callback;
    }

    public void setGroupCheckable(int i, boolean flag, boolean flag1) {
    }

    public void setGroupEnabled(int i, boolean flag) {
    }

    public void setGroupVisible(int i, boolean flag) {
    }

    public void setQwertyMode(boolean flag) {
    }

    public int size() {
        return mItems.size();
    }

    public void startDispatchingItemsChanged() {
        mPreventDispatchingItemsChanged = false;
        if(mItemsChangedWhileDispatchPrevented) {
            mItemsChangedWhileDispatchPrevented = false;
            onItemsChanged(true);
        }
    }

    public void stopDispatchingItemsChanged() {
        if(!mPreventDispatchingItemsChanged) {
            mPreventDispatchingItemsChanged = true;
            mItemsChangedWhileDispatchPrevented = false;
        }
    }

    public static final int MENU_BAR_MODE_COLLAPSE = 1;
    public static final int MENU_BAR_MODE_EXPAND = 0;
    private static final int MENU_BAR_STATE_CLOSED = 0;
    private static final int MENU_BAR_STATE_CLOSING = 3;
    private static final int MENU_BAR_STATE_OPENED = 2;
    private static final int MENU_BAR_STATE_OPENING = 1;
    private Callback mCallback;
    private int mCloseMenuBarViewCount;
    protected Context mContext;
    private boolean mIsCreated;
    private boolean mIsPrepared;
    protected ArrayList mItems;
    private boolean mItemsChangedWhileDispatchPrevented;
    MenuBarScrollHandler mMenuBarScrollHandler;
    private int mMenuBarState;
    MenuBarPresenter.Callback mMenuPresenterCallback;
    private int mOpenMenuBarViewCount;
    private CopyOnWriteArrayList mPresenters;
    private boolean mPreventDispatchingItemsChanged;
    protected ArrayList mVisibleItems;




/*
    static int access$110(MenuBar menubar) {
        int i = menubar.mOpenMenuBarViewCount;
        menubar.mOpenMenuBarViewCount = i - 1;
        return i;
    }

*/


/*
    static int access$202(MenuBar menubar, int i) {
        menubar.mMenuBarState = i;
        return i;
    }

*/



/*
    static int access$310(MenuBar menubar) {
        int i = menubar.mCloseMenuBarViewCount;
        menubar.mCloseMenuBarViewCount = i - 1;
        return i;
    }

*/
}
