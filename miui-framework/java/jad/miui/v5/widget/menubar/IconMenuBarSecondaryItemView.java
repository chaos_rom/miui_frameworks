// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.v5.widget.menubar;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.TextView;

// Referenced classes of package miui.v5.widget.menubar:
//            MenuBarItem

public class IconMenuBarSecondaryItemView extends TextView
    implements MenuBarView.ItemView {

    public IconMenuBarSecondaryItemView(Context context, AttributeSet attributeset) {
        super(context, attributeset);
    }

    public MenuBarItem getItemData() {
        return mItemData;
    }

    public void initialize(MenuBarItem menubaritem, int i) {
        mItemData = menubaritem;
        setTitle(menubaritem.getTitle());
        setIcon(menubaritem.getIcon());
    }

    public boolean performClick() {
        boolean flag = true;
        if(!super.performClick())
            if(mItemInvoker != null && mItemInvoker.invokeItem(mItemData))
                playSoundEffect(0);
            else
                flag = false;
        return flag;
    }

    public boolean prefersCondensedTitle() {
        return false;
    }

    public void setCheckable(boolean flag) {
    }

    public void setChecked(boolean flag) {
    }

    public void setEnabled(boolean flag) {
        setEnabled(flag);
    }

    public void setIcon(Drawable drawable) {
    }

    public void setItemInvoker(MenuBar.ItemInvoker iteminvoker) {
        mItemInvoker = iteminvoker;
    }

    public void setShortcut(boolean flag, char c) {
    }

    public void setTitle(CharSequence charsequence) {
        CharSequence charsequence1 = getText();
        if(charsequence1 == null || !charsequence1.equals(charsequence))
            setText(charsequence);
    }

    public boolean showsIcon() {
        return false;
    }

    private MenuBarItem mItemData;
    private MenuBar.ItemInvoker mItemInvoker;
}
