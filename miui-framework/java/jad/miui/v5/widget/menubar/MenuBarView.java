// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.v5.widget.menubar;

import android.graphics.drawable.Drawable;

// Referenced classes of package miui.v5.widget.menubar:
//            MenuBar, MenuBarItem

public interface MenuBarView {
    public static interface ItemView {

        public abstract MenuBarItem getItemData();

        public abstract void initialize(MenuBarItem menubaritem, int i);

        public abstract boolean prefersCondensedTitle();

        public abstract void setCheckable(boolean flag);

        public abstract void setChecked(boolean flag);

        public abstract void setEnabled(boolean flag);

        public abstract void setIcon(Drawable drawable);

        public abstract void setShortcut(boolean flag, char c);

        public abstract void setTitle(CharSequence charsequence);

        public abstract boolean showsIcon();
    }


    public abstract void initialize(MenuBar menubar);

    public abstract void scroll(float f);

    public abstract void scrollStateChanged(int i);
}
