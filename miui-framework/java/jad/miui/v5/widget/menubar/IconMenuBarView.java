// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.v5.widget.menubar;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.*;
import android.widget.LinearLayout;

// Referenced classes of package miui.v5.widget.menubar:
//            MenuBarView, IconMenuBarPrimaryItemView, MenuBar, MenuBarItem

public class IconMenuBarView extends ViewGroup
    implements MenuBar.ItemInvoker, MenuBarView, android.view.View.OnClickListener {
    class ExpandAnimation extends Animation
        implements android.view.animation.Animation.AnimationListener {

        protected void applyTransformation(float f, Transformation transformation) {
            float f1;
            float f2;
            if(mExpand) {
                f1 = f * -mTransition;
                f2 = f;
            } else {
                f1 = -mTransition * (1.0F - f);
                f2 = 1.0F - f;
            }
            mPrimaryContainerAndMask.setTranslationY(f1);
            mSecondaryContainer.setTranslationY(f1);
            mDimContainer.setAlpha(f2);
            mPrimaryMask.setAlpha(f2);
        }

        public ExpandAnimation expand(boolean flag) {
            mExpand = flag;
            return this;
        }

        public void onAnimationEnd(Animation animation) {
            int i = 0;
            MenuBar menubar;
            if(!mExpand) {
                mDimContainer.setVisibility(8);
                mPrimaryMask.setVisibility(8);
                mViewState = 0;
            } else {
                mViewState = 3;
            }
            mMoreIconView.setSelected(mExpand);
            menubar = mMenu;
            if(!mExpand)
                i = 1;
            menubar.dispatchMenuModeChange(i);
        }

        public void onAnimationRepeat(Animation animation) {
        }

        public void onAnimationStart(Animation animation) {
            if(mExpand) {
                mDimContainer.setVisibility(0);
                mDimContainer.setAlpha(0.0F);
                mPrimaryMask.setVisibility(0);
                mPrimaryMask.setAlpha(0.0F);
            }
        }

        public ExpandAnimation transition(float f) {
            mTransition = f;
            return this;
        }

        private boolean mExpand;
        private float mTransition;
        final IconMenuBarView this$0;

        public ExpandAnimation() {
            this$0 = IconMenuBarView.this;
            super();
            setAnimationListener(this);
        }
    }

    public static class LayoutParams extends android.view.ViewGroup.MarginLayoutParams {

        public LayoutParams(int i, int j) {
            super(i, j);
        }

        public LayoutParams(Context context, AttributeSet attributeset) {
            super(context, attributeset);
        }
    }


    public IconMenuBarView(Context context, AttributeSet attributeset) {
        super(context, attributeset);
        mViewState = 0;
        mExpandAnimation = new ExpandAnimation();
        mExpandDuration = 300;
    }

    protected boolean checkLayoutParams(android.view.ViewGroup.LayoutParams layoutparams) {
        return layoutparams instanceof LayoutParams;
    }

    protected IconMenuBarPrimaryItemView createMoreItemView(IconMenuBarPrimaryItemView iconmenubarprimaryitemview, int i, Drawable drawable) {
        IconMenuBarPrimaryItemView iconmenubarprimaryitemview1;
        if(iconmenubarprimaryitemview != null)
            iconmenubarprimaryitemview1 = iconmenubarprimaryitemview;
        else
            iconmenubarprimaryitemview1 = (IconMenuBarPrimaryItemView)View.inflate(mContext, i, null);
        iconmenubarprimaryitemview1.setText(mContext.getResources().getText(0x60c0252));
        iconmenubarprimaryitemview1.setCompoundDrawablesWithIntrinsicBounds(null, drawable, null, null);
        iconmenubarprimaryitemview1.setCheckable(true);
        iconmenubarprimaryitemview1.setOnClickListener(this);
        iconmenubarprimaryitemview1.setTag(0x7fffffff, Boolean.valueOf(true));
        mMoreIconView = iconmenubarprimaryitemview1;
        return iconmenubarprimaryitemview1;
    }

    public volatile android.view.ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeset) {
        return generateLayoutParams(attributeset);
    }

    public LayoutParams generateLayoutParams(AttributeSet attributeset) {
        return new LayoutParams(getContext(), attributeset);
    }

    public int getMaxItems() {
        return mMaxItems;
    }

    LinearLayout getPrimaryContainer() {
        return mPrimaryContainer;
    }

    public LayoutAnimationController getPrimaryContainerLayoutAnimation() {
        return mPrimaryContainer.getLayoutAnimation();
    }

    LinearLayout getSecondaryContainer() {
        return mSecondaryContainer;
    }

    public LayoutAnimationController getSecondaryContainerLayoutAnimation() {
        return mSecondaryContainer.getLayoutAnimation();
    }

    public int getWindowAnimations() {
        return 0;
    }

    public void initialize(MenuBar menubar) {
        mMenu = menubar;
    }

    public boolean invokeItem(MenuBarItem menubaritem) {
        boolean flag = mMenu.performItemAction(menubaritem, 0);
        if(flag)
            requestExpand(false);
        return flag;
    }

    public boolean isExpanded() {
        boolean flag;
        if(mViewState == 3)
            flag = true;
        else
            flag = false;
        return flag;
    }

    public void onClick(View view) {
        view.getId();
        JVM INSTR tableswitch 101384356 101384357: default 28
    //                   101384356 29
    //                   101384357 29;
           goto _L1 _L2 _L2
_L1:
        return;
_L2:
        onMoreItemClick();
        if(true) goto _L1; else goto _L3
_L3:
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        mDimContainer = findViewById(0x60b00a5);
        mDimContainer.setOnClickListener(this);
        mPrimaryContainerAndMask = findViewById(0x60b00a7);
        mPrimaryContainer = (LinearLayout)mPrimaryContainerAndMask.findViewById(0x60b00a8);
        mPrimaryMask = mPrimaryContainerAndMask.findViewById(0x60b00a6);
        mSecondaryContainer = (LinearLayout)findViewById(0x60b00a9);
    }

    protected void onLayout(boolean flag, int i, int j, int k, int l) {
        int i1 = l - j;
        mDimContainer.layout(i, j, k, l);
        mPrimaryContainerAndMask.layout(i, i1 - mPrimaryContainerAndMask.getMeasuredHeight(), k, l);
        mSecondaryContainer.layout(i, l, k, l + mSecondaryContainer.getMeasuredHeight());
    }

    protected void onMeasure(int i, int j) {
        if(android.view.View.MeasureSpec.getMode(i) != 0x40000000)
            throw new IllegalStateException((new StringBuilder()).append(getClass().getSimpleName()).append(" can only be used ").append("with android:layout_width=\"match_parent\" (or fill_parent)").toString());
        if(android.view.View.MeasureSpec.getMode(j) != 0x40000000) {
            throw new IllegalStateException((new StringBuilder()).append(getClass().getSimpleName()).append(" can only be used ").append("with android:layout_width=\"match_parent\" (or fill_parent)").toString());
        } else {
            int k = android.view.View.MeasureSpec.getSize(i);
            int l = android.view.View.MeasureSpec.getSize(j);
            int i1 = android.view.View.MeasureSpec.makeMeasureSpec(k, 0x40000000);
            int j1 = android.view.View.MeasureSpec.makeMeasureSpec(0, 0);
            mDimContainer.measure(i1, j1);
            mPrimaryContainerAndMask.measure(i1, j1);
            mSecondaryContainer.measure(i1, j1);
            setMeasuredDimension(k, l);
            return;
        }
    }

    void onMoreItemClick() {
        if(mMoreIconView != null) {
            boolean flag;
            if(!mMoreIconView.isSelected())
                flag = true;
            else
                flag = false;
            requestExpand(flag);
        }
    }

    boolean requestExpand(boolean flag) {
        boolean flag1 = false;
        mViewState;
        JVM INSTR tableswitch 0 3: default 36
    //                   0 100
    //                   1 36
    //                   2 36
    //                   3 114;
           goto _L1 _L2 _L1 _L1 _L3
_L1:
        if(flag1) {
            if(flag)
                mSecondaryContainer.startLayoutAnimation();
            mExpandAnimation.reset();
            mExpandAnimation.setDuration(mExpandDuration);
            mExpandAnimation.expand(flag).transition(mSecondaryContainer.getHeight());
            startAnimation(mExpandAnimation);
        }
        return flag1;
_L2:
        if(flag) {
            mViewState = 2;
            flag1 = true;
        }
        continue; /* Loop/switch isn't completed */
_L3:
        if(!flag) {
            mViewState = 1;
            flag1 = true;
        }
        if(true) goto _L1; else goto _L4
_L4:
    }

    public void scroll(float f) {
        float f1 = mPrimaryContainer.getHeight();
        mPrimaryContainer.setTranslationY(f * f1);
    }

    public void scrollStateChanged(int i) {
    }

    public void setPrimaryContainerLayoutAnimation(LayoutAnimationController layoutanimationcontroller) {
        mPrimaryContainer.setLayoutAnimation(layoutanimationcontroller);
    }

    public void setPrimaryMaskDrawable(Drawable drawable) {
        if(mPrimaryMask != null)
            mPrimaryMask.setBackground(drawable);
    }

    public void setSecondaryContainerLayoutAnimation(LayoutAnimationController layoutanimationcontroller) {
        mSecondaryContainer.setLayoutAnimation(layoutanimationcontroller);
    }

    static final int IS_MORE = 0x7fffffff;
    private static final int VIEW_STATE_COLLAPSED = 0;
    private static final int VIEW_STATE_COLLAPSING = 1;
    private static final int VIEW_STATE_EXPANDED = 3;
    private static final int VIEW_STATE_EXPANDING = 2;
    private View mDimContainer;
    ExpandAnimation mExpandAnimation;
    private int mExpandDuration;
    private int mMaxItems;
    private MenuBar mMenu;
    private View mMoreIconView;
    private LinearLayout mPrimaryContainer;
    private View mPrimaryContainerAndMask;
    private View mPrimaryMask;
    private LinearLayout mSecondaryContainer;
    private int mViewState;






/*
    static int access$402(IconMenuBarView iconmenubarview, int i) {
        iconmenubarview.mViewState = i;
        return i;
    }

*/


}
