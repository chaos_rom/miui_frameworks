// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.v5.widget.menubar;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.Button;

// Referenced classes of package miui.v5.widget.menubar:
//            MenuBarItem

public class IconMenuBarPrimaryItemView extends Button
    implements MenuBarView.ItemView {

    public IconMenuBarPrimaryItemView(Context context, AttributeSet attributeset) {
        super(context, attributeset);
    }

    public MenuBarItem getItemData() {
        return mItemData;
    }

    public void initialize(MenuBarItem menubaritem, int i) {
        mItemData = menubaritem;
        setTitle(menubaritem.getTitle());
        setIcon(menubaritem.getIcon());
    }

    boolean isMoreView() {
        return mIsMoreView;
    }

    public boolean performClick() {
        boolean flag = true;
        if(!super.performClick())
            if(mItemInvoker != null && mItemInvoker.invokeItem(mItemData))
                playSoundEffect(0);
            else
                flag = false;
        return flag;
    }

    public boolean prefersCondensedTitle() {
        return false;
    }

    public void setCheckable(boolean flag) {
        mIsCheckable = flag;
    }

    public void setChecked(boolean flag) {
        if(mIsCheckable)
            setSelected(flag);
    }

    public void setIcon(Drawable drawable) {
        Drawable drawable1 = getCompoundDrawables()[1];
        if(drawable != null && drawable1 != drawable)
            setCompoundDrawablesWithIntrinsicBounds(null, drawable, null, null);
    }

    void setIsMoreView(boolean flag) {
        mIsMoreView = flag;
    }

    public void setItemInvoker(MenuBar.ItemInvoker iteminvoker) {
        mItemInvoker = iteminvoker;
    }

    public void setShortcut(boolean flag, char c) {
    }

    public void setTitle(CharSequence charsequence) {
        CharSequence charsequence1 = getText();
        if(charsequence1 == null || !charsequence1.equals(charsequence))
            setText(charsequence);
    }

    public boolean showsIcon() {
        return true;
    }

    private boolean mIsCheckable;
    private boolean mIsMoreView;
    private MenuBarItem mItemData;
    private MenuBar.ItemInvoker mItemInvoker;
}
