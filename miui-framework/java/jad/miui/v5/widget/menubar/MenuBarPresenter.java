// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.v5.widget.menubar;

import android.content.Context;
import android.os.Parcelable;
import android.view.ViewGroup;

// Referenced classes of package miui.v5.widget.menubar:
//            MenuBar, MenuBarItem, MenuBarView

public interface MenuBarPresenter {
    public static interface Callback {

        public abstract void onCloseMenu(MenuBar menubar, boolean flag);

        public abstract void onOpenMenu(MenuBar menubar, boolean flag);

        public abstract boolean onOpenSubMenu(MenuBar menubar);
    }


    public abstract boolean collapseItemActionView(MenuBar menubar, MenuBarItem menubaritem);

    public abstract boolean expandItemActionView(MenuBar menubar, MenuBarItem menubaritem);

    public abstract boolean flagActionItems();

    public abstract int getId();

    public abstract MenuBarView getMenuView(ViewGroup viewgroup);

    public abstract void initForMenu(Context context, MenuBar menubar);

    public abstract void onCloseMenu(MenuBar menubar, boolean flag);

    public abstract boolean onExpandMenu(MenuBar menubar, boolean flag);

    public abstract void onOpenMenu(MenuBar menubar, boolean flag);

    public abstract void onRestoreInstanceState(Parcelable parcelable);

    public abstract Parcelable onSaveInstanceState();

    public abstract void scroll(float f);

    public abstract void scrollStateChanged(int i);

    public abstract void setCallback(Callback callback);

    public abstract void updateMenuView(boolean flag);
}
