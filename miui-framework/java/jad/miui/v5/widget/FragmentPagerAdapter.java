// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.v5.widget;

import android.app.*;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

public abstract class FragmentPagerAdapter extends PagerAdapter {

    public FragmentPagerAdapter(FragmentManager fragmentmanager) {
        mTransactionTmp = null;
        mFragmentManager = fragmentmanager;
    }

    public void destroyItem(ViewGroup viewgroup, int i, Object obj) {
        if(mTransactionTmp == null)
            mTransactionTmp = mFragmentManager.beginTransaction();
        mTransactionTmp.hide((Fragment)obj);
    }

    public void finishUpdate(ViewGroup viewgroup) {
        if(mTransactionTmp != null) {
            mTransactionTmp.commitAllowingStateLoss();
            mTransactionTmp = null;
            mFragmentManager.executePendingTransactions();
        }
    }

    public abstract Fragment getItem(int i);

    public Object instantiateItem(ViewGroup viewgroup, int i) {
        if(mTransactionTmp == null)
            mTransactionTmp = mFragmentManager.beginTransaction();
        Fragment fragment = getItem(i);
        mTransactionTmp.show(fragment);
        return fragment;
    }

    public boolean isViewFromObject(View view, Object obj) {
        boolean flag;
        if(((Fragment)obj).getView() == view)
            flag = true;
        else
            flag = false;
        return flag;
    }

    private final FragmentManager mFragmentManager;
    private FragmentTransaction mTransactionTmp;
}
