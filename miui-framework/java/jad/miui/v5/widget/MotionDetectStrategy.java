// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.v5.widget;

import android.view.View;

public interface MotionDetectStrategy {
    public static interface Creator {

        public abstract MotionDetectStrategy createMotionDetectStrategy();
    }


    public abstract boolean isMovable(View view, int i, int j, int k, int l);
}
