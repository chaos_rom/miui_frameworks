// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.v5.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.FrameLayout;

// Referenced classes of package miui.v5.widget:
//            VerticalMotionDetector, MotionDetectListener

public class VerticalMotionFrameLayout extends FrameLayout {

    public VerticalMotionFrameLayout(Context context) {
        this(context, null);
    }

    public VerticalMotionFrameLayout(Context context, AttributeSet attributeset) {
        this(context, attributeset, 0);
    }

    public VerticalMotionFrameLayout(Context context, AttributeSet attributeset, int i) {
        super(context, attributeset, i);
        mMotionDetector = new VerticalMotionDetector(this);
    }

    public boolean dispatchTouchEvent(MotionEvent motionevent) {
        if(mMotionDetector.isBeingDragged() && !mMotionDetector.isMovable((int)motionevent.getX(), (int)motionevent.getY()) && (0xff & motionevent.getAction()) == 2) {
            motionevent.setAction(3);
            super.dispatchTouchEvent(motionevent);
            motionevent.setAction(0);
        }
        return super.dispatchTouchEvent(motionevent);
    }

    public boolean onInterceptTouchEvent(MotionEvent motionevent) {
        return mMotionDetector.onInterceptTouchEvent(motionevent);
    }

    public boolean onTouchEvent(MotionEvent motionevent) {
        return mMotionDetector.onTouchEvent(motionevent);
    }

    public void setMotionStrategy(MotionDetectListener motiondetectlistener) {
        mMotionDetector.setMotionStrategy(motiondetectlistener);
    }

    private final VerticalMotionDetector mMotionDetector;
}
