// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.v5.widget;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class CooperativeViewPager extends ViewPager {

    public CooperativeViewPager(Context context) {
        super(context);
        mDragEnabled = true;
    }

    public CooperativeViewPager(Context context, AttributeSet attributeset) {
        super(context, attributeset);
        mDragEnabled = true;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionevent) {
        boolean flag;
        if(!mDragEnabled)
            flag = false;
        else
            flag = super.onInterceptTouchEvent(motionevent);
        return flag;
    }

    public void setDraggable(boolean flag) {
        mDragEnabled = flag;
    }

    private boolean mDragEnabled;
}
