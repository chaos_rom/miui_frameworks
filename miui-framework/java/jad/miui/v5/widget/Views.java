// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.v5.widget;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.*;
import android.widget.HeaderViewListAdapter;
import android.widget.ListView;
import com.android.internal.util.Predicate;
import com.google.android.collect.Lists;
import java.util.*;

public class Views {
    private static class Stub extends View {

        protected void onMeasure(int i, int j) {
            super.onMeasure(android.view.View.MeasureSpec.makeMeasureSpec(1, 0x40000000), android.view.View.MeasureSpec.makeMeasureSpec(mHeight, 0x40000000));
        }

        public void setHeight(int i) {
            mHeight = Math.max(0, i);
        }

        private int mHeight;

        public Stub(Context context) {
            super(context);
            mHeight = 0;
            setVisibility(4);
        }
    }

    public static class ComposedPageChangeListener
        implements android.support.v4.view.ViewPager.OnPageChangeListener {

        public void add(android.support.v4.view.ViewPager.OnPageChangeListener onpagechangelistener) {
            mListeners.add(onpagechangelistener);
        }

        public void onPageScrollStateChanged(int i) {
            for(Iterator iterator = mListeners.iterator(); iterator.hasNext(); ((android.support.v4.view.ViewPager.OnPageChangeListener)iterator.next()).onPageScrollStateChanged(i));
        }

        public void onPageScrolled(int i, float f, int j) {
            for(Iterator iterator = mListeners.iterator(); iterator.hasNext(); ((android.support.v4.view.ViewPager.OnPageChangeListener)iterator.next()).onPageScrolled(i, f, j));
        }

        public void onPageSelected(int i) {
            for(Iterator iterator = mListeners.iterator(); iterator.hasNext(); ((android.support.v4.view.ViewPager.OnPageChangeListener)iterator.next()).onPageSelected(i));
        }

        public void remove(android.support.v4.view.ViewPager.OnPageChangeListener onpagechangelistener) {
            mListeners.remove(onpagechangelistener);
        }

        public void reset() {
            mListeners.clear();
        }

        final List mListeners = Lists.newArrayList();

        public ComposedPageChangeListener() {
        }
    }


    public Views() {
    }

    public static void collectChildren(ViewGroup viewgroup, Predicate predicate, List list) {
        int i = viewgroup.getChildCount();
        for(int j = 0; j < i; j++) {
            View view = viewgroup.getChildAt(j);
            if(predicate.apply(view))
                list.add(view);
        }

    }

    public static void detach(View view) {
        android.view.ViewParent viewparent = view.getParent();
        if(viewparent instanceof ViewGroup)
            ((ViewGroup)viewparent).removeView(view);
    }

    public static void ensureBottomHeight(ListView listview, int i) {
        Stub stub = (Stub)listview.findViewWithTag(miui/v5/widget/Views$Stub);
        if(stub != null)
            listview.removeFooterView(stub);
        if(i > 0) {
            if(stub == null) {
                stub = new Stub(listview.getContext());
                stub.setTag(miui/v5/widget/Views$Stub);
            }
            stub.setHeight(i);
            android.widget.ListAdapter listadapter = listview.getAdapter();
            boolean flag;
            if(!(listadapter instanceof HeaderViewListAdapter))
                flag = true;
            else
                flag = false;
            if(flag)
                listview.setAdapter(null);
            listview.addFooterView(stub, null, false);
            if(flag)
                listview.setAdapter(listadapter);
        }
    }

    public static int getBackgroundHeight(View view) {
        Drawable drawable = view.getBackground();
        int i;
        if(drawable != null)
            i = drawable.getIntrinsicHeight();
        else
            i = -1;
        return i;
    }

    public static int getBackgroundWidth(View view) {
        Drawable drawable = view.getBackground();
        int i;
        if(drawable != null)
            i = drawable.getIntrinsicWidth();
        else
            i = -1;
        return i;
    }

    public static void getContentRect(ViewGroup viewgroup, Rect rect) {
        rect.left = viewgroup.getScrollX() + viewgroup.getPaddingLeft();
        rect.top = viewgroup.getScrollY() + viewgroup.getPaddingTop();
        rect.right = viewgroup.getWidth() - viewgroup.getPaddingRight() - rect.left;
        rect.bottom = viewgroup.getHeight() - viewgroup.getPaddingBottom() - rect.top;
    }

    public static View inflate(Context context, int i, ViewGroup viewgroup, boolean flag) {
        return LayoutInflater.from(context).inflate(i, viewgroup, flag);
    }

    public static boolean isBottomFullVisible(ListView listview) {
        View view = listview.findViewWithTag(miui/v5/widget/Views$Stub);
        if(view == null)
            throw new IllegalArgumentException("Can not find bottom view");
        boolean flag;
        if(view.getParent() != null && view.getBottom() < listview.getHeight())
            flag = true;
        else
            flag = false;
        return flag;
    }

    public static boolean isContainPoint(View view, int i, int j) {
        boolean flag;
        if(i > view.getLeft() && i < view.getRight() && j > view.getTop() && j < view.getBottom())
            flag = true;
        else
            flag = false;
        return flag;
    }

    public static boolean isIntersectWithRect(View view, Rect rect) {
        boolean flag;
        flag = false;
        break MISSING_BLOCK_LABEL_2;
        if(rect != null && view.getLeft() < rect.right && view.getTop() < rect.bottom && view.getRight() > rect.left && view.getBottom() > rect.top)
            flag = true;
        return flag;
    }

    public static int measureHeightWidhBackground(View view) {
        int i = Math.max(getBackgroundHeight(view), 0);
        setHeight(view, i);
        return i;
    }

    public static void setHeight(View view, int i) {
        android.view.ViewGroup.LayoutParams layoutparams;
        layoutparams = view.getLayoutParams();
        break MISSING_BLOCK_LABEL_5;
        if(layoutparams != null && layoutparams.height != i) {
            layoutparams.height = i;
            view.setLayoutParams(layoutparams);
        }
        return;
    }

    public static void setPadding(View view, int i, int j, int k, int l) {
        if(i < 0)
            i = view.getPaddingLeft();
        if(j < 0)
            j = view.getPaddingTop();
        if(k < 0)
            k = view.getPaddingRight();
        if(l < 0)
            l = view.getPaddingBottom();
        view.setPadding(i, j, k, l);
    }

    static final String TAG = miui/v5/widget/Views.getName();
    public static final Comparator TOP_COMPARATOR = new Comparator() {

        public int compare(View view, View view1) {
            return view.getTop() - view1.getTop();
        }

        public volatile int compare(Object obj, Object obj1) {
            return compare((View)obj, (View)obj1);
        }

    };

}
