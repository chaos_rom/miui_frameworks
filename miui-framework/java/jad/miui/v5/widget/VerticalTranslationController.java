// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.v5.widget;

import android.content.Context;
import android.view.VelocityTracker;
import android.view.View;

// Referenced classes of package miui.v5.widget:
//            AbsTranslationController, MiuiViewConfiguration, MotionDetectStrategy

public class VerticalTranslationController extends AbsTranslationController {

    public VerticalTranslationController(Context context, MotionDetectStrategy motiondetectstrategy, int i, int j, int k, int l) {
        super(context, motiondetectstrategy);
        mStartY = 0;
        if(k <= i && i < j && j <= l) {
            mMinY = i;
            mMinYBounce = k;
            mMaxY = j;
            mMaxYBounce = l;
            MiuiViewConfiguration miuiviewconfiguration = MiuiViewConfiguration.get(context);
            mTranslateSlop = miuiviewconfiguration.getScaledTranslateSlop();
            mMinAnchorVelocity = miuiviewconfiguration.getScaledMinAnchorVelocity();
            return;
        } else {
            StringBuilder stringbuilder = (new StringBuilder()).append("minYBounce <= minY < maxY <= maxYBounce is necessary!");
            Object aobj[] = new Object[4];
            aobj[0] = Integer.valueOf(k);
            aobj[1] = Integer.valueOf(i);
            aobj[2] = Integer.valueOf(j);
            aobj[3] = Integer.valueOf(l);
            throw new IllegalArgumentException(stringbuilder.append(String.format("%d %d %d %d", aobj)).toString());
        }
    }

    protected int computVelocity(VelocityTracker velocitytracker) {
        int i;
        if(velocitytracker != null) {
            velocitytracker.computeCurrentVelocity(1000, super.mMaximumVelocity);
            i = (int)velocitytracker.getYVelocity();
        } else {
            i = 0;
        }
        return i;
    }

    protected int getAnchorPostion(View view, int i, int j, int k, int l, int i1) {
        int j1 = 0x7fffffff;
        int k1 = (int)view.getY();
        if(Math.abs(i1) < mMinAnchorVelocity)
            if(k1 < mStartY && k1 < mMaxY - mTranslateSlop)
                j1 = mMinY;
            else
            if(k1 > mStartY && k1 > mMinY + mTranslateSlop)
                j1 = mMaxY;
        if(j1 == 0x7fffffff) {
            int l1 = k1 + i1 / 2;
            int i2 = mMinY - l1;
            int j2 = mMaxY - l1;
            if(Math.abs(i2) < Math.abs(j2))
                j1 = mMinY;
            else
                j1 = mMaxY;
        }
        return j1;
    }

    protected int getValidMovePosition(View view, int i, int j, int k, int l) {
        int i1 = (int)((view.getTranslationY() + (float)j) - (float)l);
        int j1;
        if(i1 < mMinYBounce)
            j1 = mMinYBounce;
        else
        if(i1 > mMaxYBounce)
            j1 = mMaxYBounce;
        else
            j1 = i1;
        return j1;
    }

    public void onMoveStart(View view, int i, int j) {
        super.onMoveStart(view, i, j);
        mStartY = (int)view.getY();
    }

    protected void translate(View view, float f) {
        view.setTranslationY(f);
    }

    private final int mMaxY;
    private final int mMaxYBounce;
    private final int mMinAnchorVelocity;
    private final int mMinY;
    private final int mMinYBounce;
    private int mStartY;
    private final int mTranslateSlop;
}
