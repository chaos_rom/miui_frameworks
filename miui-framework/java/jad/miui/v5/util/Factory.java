// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.v5.util;

import com.google.android.collect.Maps;
import java.util.Map;

public interface Factory {
    public static class CachedFactory
        implements Factory {

        public static CachedFactory newFactory(Factory factory) {
            CachedFactory cachedfactory;
            if(factory == null)
                cachedfactory = null;
            else
                cachedfactory = new CachedFactory(factory);
            return cachedfactory;
        }

        public void clear() {
            mCached.clear();
        }

        public Object create(Object obj) {
            Object obj1 = mCached.get(obj);
            if(obj1 == null) {
                obj1 = mFactory.create(obj);
                if(obj1 != null)
                    mCached.put(obj, obj1);
            }
            return obj1;
        }

        private final Map mCached = Maps.newHashMap();
        private final Factory mFactory;

        public CachedFactory(Factory factory) {
            mFactory = factory;
        }
    }


    public abstract Object create(Object obj);
}
