// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public interface FragmentLifecycleObserver {

    public abstract void onActivityCreated(Bundle bundle);

    public abstract void onActivityResult(int i, int j, Intent intent);

    public abstract void onAttach(Activity activity);

    public abstract void onCreate(Bundle bundle);

    public abstract void onDestroy();

    public abstract void onPause();

    public abstract void onResume();

    public abstract void onStart();

    public abstract void onStop();
}
