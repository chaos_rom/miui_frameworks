// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import java.util.*;

// Referenced classes of package miui.app:
//            ActivityLifecycleObserver

public class ObservableActivity extends Activity {

    public ObservableActivity() {
        mObservers = new ArrayList();
    }

    public void addObserver(ActivityLifecycleObserver activitylifecycleobserver) {
        if(activitylifecycleobserver != null)
            mObservers.add(activitylifecycleobserver);
    }

    protected void onActivityResult(int i, int j, Intent intent) {
        super.onActivityResult(i, j, intent);
        List list = mObservers;
        list;
        JVM INSTR monitorenter ;
        for(Iterator iterator = mObservers.iterator(); iterator.hasNext(); ((ActivityLifecycleObserver)iterator.next()).onActivityResult(i, j, intent));
        break MISSING_BLOCK_LABEL_66;
        Exception exception;
        exception;
        throw exception;
        list;
        JVM INSTR monitorexit ;
    }

    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        List list = mObservers;
        list;
        JVM INSTR monitorenter ;
        for(Iterator iterator = mObservers.iterator(); iterator.hasNext(); ((ActivityLifecycleObserver)iterator.next()).onCreate(bundle));
        break MISSING_BLOCK_LABEL_57;
        Exception exception;
        exception;
        throw exception;
        list;
        JVM INSTR monitorexit ;
    }

    protected void onDestroy() {
        super.onDestroy();
        List list = mObservers;
        list;
        JVM INSTR monitorenter ;
        for(Iterator iterator = mObservers.iterator(); iterator.hasNext(); ((ActivityLifecycleObserver)iterator.next()).onDestroy());
        break MISSING_BLOCK_LABEL_52;
        Exception exception;
        exception;
        throw exception;
        list;
        JVM INSTR monitorexit ;
    }

    protected void onPause() {
        super.onPause();
        List list = mObservers;
        list;
        JVM INSTR monitorenter ;
        for(Iterator iterator = mObservers.iterator(); iterator.hasNext(); ((ActivityLifecycleObserver)iterator.next()).onPause());
        break MISSING_BLOCK_LABEL_52;
        Exception exception;
        exception;
        throw exception;
        list;
        JVM INSTR monitorexit ;
    }

    protected void onResume() {
        super.onResume();
        List list = mObservers;
        list;
        JVM INSTR monitorenter ;
        for(Iterator iterator = mObservers.iterator(); iterator.hasNext(); ((ActivityLifecycleObserver)iterator.next()).onResume());
        break MISSING_BLOCK_LABEL_52;
        Exception exception;
        exception;
        throw exception;
        list;
        JVM INSTR monitorexit ;
    }

    protected void onStart() {
        super.onStart();
        List list = mObservers;
        list;
        JVM INSTR monitorenter ;
        for(Iterator iterator = mObservers.iterator(); iterator.hasNext(); ((ActivityLifecycleObserver)iterator.next()).onStart());
        break MISSING_BLOCK_LABEL_52;
        Exception exception;
        exception;
        throw exception;
        list;
        JVM INSTR monitorexit ;
    }

    protected void onStop() {
        super.onStop();
        List list = mObservers;
        list;
        JVM INSTR monitorenter ;
        for(Iterator iterator = mObservers.iterator(); iterator.hasNext(); ((ActivityLifecycleObserver)iterator.next()).onStop());
        break MISSING_BLOCK_LABEL_52;
        Exception exception;
        exception;
        throw exception;
        list;
        JVM INSTR monitorexit ;
    }

    public void removeObserver(ActivityLifecycleObserver activitylifecycleobserver) {
        if(activitylifecycleobserver != null)
            mObservers.remove(activitylifecycleobserver);
    }

    private List mObservers;
}
