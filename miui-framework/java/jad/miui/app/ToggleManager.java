// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app;

import android.app.AlertDialog;
import android.app.StatusBarManager;
import android.bluetooth.BluetoothAdapter;
import android.content.*;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.net.*;
import android.net.wifi.WifiManager;
import android.os.*;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.IWindowManager;
import android.view.Window;
import android.widget.*;
import java.io.*;
import java.lang.ref.WeakReference;
import java.util.*;
import miui.net.FirewallManager;
import miui.os.Build;
import miui.os.Environment;
import miui.security.ChooseLockSettingsHelper;
import miui.util.AudioManagerHelper;
import miui.util.CommandLineUtils;

public class ToggleManager {
    private static final class WifiStateTracker extends StateTracker {

        private static int wifiStateToFiveState(int i) {
            i;
            JVM INSTR tableswitch 0 3: default 32
        //                       0 46
        //                       1 36
        //                       2 51
        //                       3 41;
               goto _L1 _L2 _L3 _L4 _L5
_L1:
            byte byte0 = 4;
_L7:
            return byte0;
_L3:
            byte0 = 0;
            continue; /* Loop/switch isn't completed */
_L5:
            byte0 = 1;
            continue; /* Loop/switch isn't completed */
_L2:
            byte0 = 3;
            continue; /* Loop/switch isn't completed */
_L4:
            byte0 = 2;
            if(true) goto _L7; else goto _L6
_L6:
        }

        public int getActualState(Context context) {
            WifiManager wifimanager = (WifiManager)context.getSystemService("wifi");
            int i;
            if(wifimanager != null)
                i = wifiStateToFiveState(wifimanager.getWifiState());
            else
                i = 4;
            return i;
        }

        public void onActualStateChange(Context context, Intent intent) {
            boolean flag = false;
            if(!"android.net.wifi.WIFI_STATE_CHANGED".equals(intent.getAction())) goto _L2; else goto _L1
_L1:
            int j = intent.getIntExtra("wifi_state", -1);
            setCurrentState(context, wifiStateToFiveState(j));
            if(3 == j) {
                zConnected = true;
                zScanAttempt = 0;
            }
_L4:
            return;
_L2:
            if("android.net.wifi.SCAN_RESULTS".equals(intent.getAction())) {
                if(zScanAttempt < 3) {
                    int i = 1 + zScanAttempt;
                    zScanAttempt = i;
                    if(i == 3)
                        zConnected = false;
                }
            } else
            if("android.net.wifi.STATE_CHANGE".equals(intent.getAction())) {
                zScanAttempt = 3;
                android.net.NetworkInfo.DetailedState detailedstate = ((NetworkInfo)intent.getParcelableExtra("networkInfo")).getDetailedState();
                if(android.net.NetworkInfo.DetailedState.SCANNING == detailedstate || android.net.NetworkInfo.DetailedState.CONNECTING == detailedstate || android.net.NetworkInfo.DetailedState.AUTHENTICATING == detailedstate || android.net.NetworkInfo.DetailedState.OBTAINING_IPADDR == detailedstate || android.net.NetworkInfo.DetailedState.CONNECTED == detailedstate)
                    flag = true;
                zConnected = flag;
            }
            if(true) goto _L4; else goto _L3
_L3:
        }

        protected void requestStateChange(Context context, final boolean desiredState) {
            final WifiManager wifiManager = (WifiManager)context.getSystemService("wifi");
            if(wifiManager == null)
                Log.d("ToggleManager", "No wifiManager.");
            else
                (new AsyncTask() {

                    protected volatile Object doInBackground(Object aobj[]) {
                        return doInBackground((Void[])aobj);
                    }

                    protected transient Void doInBackground(Void avoid[]) {
                        int i = wifiManager.getWifiApState();
                        if(desiredState && (i == 12 || i == 13))
                            wifiManager.setWifiApEnabled(null, false);
                        wifiManager.setWifiEnabled(desiredState);
                        return null;
                    }

                    final WifiStateTracker this$0;
                    final boolean val$desiredState;
                    final WifiManager val$wifiManager;

                 {
                    this$0 = WifiStateTracker.this;
                    wifiManager = wifimanager;
                    desiredState = flag;
                    super();
                }
                }).execute(new Void[0]);
        }

        private static final int MAX_SCAN_ATTEMPT = 3;
        public boolean zConnected;
        private int zScanAttempt;

        private WifiStateTracker() {
            zConnected = false;
            zScanAttempt = 0;
        }

    }

    public static abstract class StateTracker {

        public abstract int getActualState(Context context);

        public final int getTriState(Context context) {
            byte byte0 = 5;
            if(!mInTransition) goto _L2; else goto _L1
_L1:
            return byte0;
_L2:
            switch(getActualState(context)) {
            case 0: // '\0'
                byte0 = 0;
                break;

            case 1: // '\001'
                byte0 = 1;
                break;
            }
            if(true) goto _L1; else goto _L3
_L3:
        }

        public final boolean isTurningOn() {
            boolean flag;
            if(mIntendedState != null && mIntendedState.booleanValue())
                flag = true;
            else
                flag = false;
            return flag;
        }

        public abstract void onActualStateChange(Context context, Intent intent);

        protected abstract void requestStateChange(Context context, boolean flag);

        protected final void setCurrentState(Context context, int i) {
            boolean flag = mInTransition;
            i;
            JVM INSTR tableswitch 0 3: default 36
        //                       0 104
        //                       1 120
        //                       2 136
        //                       3 152;
               goto _L1 _L2 _L3 _L4 _L5
_L1:
            break; /* Loop/switch isn't completed */
_L5:
            break MISSING_BLOCK_LABEL_152;
_L6:
            if(flag && !mInTransition && mDeferredStateChangeRequestNeeded) {
                Log.v("ToggleManager", "processing deferred state change");
                if(mActualState != null && mIntendedState != null && mIntendedState.equals(mActualState))
                    Log.v("ToggleManager", "... but intended state matches, so no changes.");
                else
                if(mIntendedState != null) {
                    mInTransition = true;
                    requestStateChange(context, mIntendedState.booleanValue());
                }
                mDeferredStateChangeRequestNeeded = false;
            }
            return;
_L2:
            mInTransition = false;
            mActualState = Boolean.valueOf(false);
              goto _L6
_L3:
            mInTransition = false;
            mActualState = Boolean.valueOf(true);
              goto _L6
_L4:
            mInTransition = true;
            mActualState = Boolean.valueOf(false);
              goto _L6
            mInTransition = true;
            mActualState = Boolean.valueOf(true);
              goto _L6
        }

        public final void toggleState(Context context) {
            int i;
            boolean flag;
            i = getTriState(context);
            flag = false;
            i;
            JVM INSTR tableswitch 0 5: default 48
        //                       0 74
        //                       1 69
        //                       2 48
        //                       3 48
        //                       4 48
        //                       5 79;
               goto _L1 _L2 _L3 _L1 _L1 _L1 _L4
_L1:
            break; /* Loop/switch isn't completed */
_L4:
            break MISSING_BLOCK_LABEL_79;
_L5:
            mIntendedState = Boolean.valueOf(flag);
            if(mInTransition) {
                mDeferredStateChangeRequestNeeded = true;
            } else {
                mInTransition = true;
                requestStateChange(context, flag);
            }
            return;
_L3:
            flag = false;
              goto _L5
_L2:
            flag = true;
              goto _L5
            if(mIntendedState != null)
                if(!mIntendedState.booleanValue())
                    flag = true;
                else
                    flag = false;
              goto _L5
        }

        private Boolean mActualState;
        private boolean mDeferredStateChangeRequestNeeded;
        private boolean mInTransition;
        private Boolean mIntendedState;

        public StateTracker() {
            mInTransition = false;
            mActualState = null;
            mIntendedState = null;
            mDeferredStateChangeRequestNeeded = false;
        }
    }

    public static interface OnToggleOrderChangedListener {

        public abstract void OnToggleOrderChanged();
    }

    public static interface OnToggleChangedListener {

        public abstract void OnToggleChanged(int i);
    }


    private ToggleManager(Context context) {
        mEnableNetType = true;
        mBluetoothAdapter = null;
        mBroadcastReceiver = new BroadcastReceiver() {

            public void onReceive(Context context1, Intent intent) {
                String s = intent.getAction();
                if(!"android.net.wifi.WIFI_STATE_CHANGED".equals(s) && !"android.net.wifi.SCAN_RESULTS".equals(s) && !"android.net.wifi.STATE_CHANGE".equals(s)) goto _L2; else goto _L1
_L1:
                mWifiState.onActualStateChange(context1, intent);
                updateWifiToggle();
_L4:
                return;
_L2:
                if("android.bluetooth.adapter.action.STATE_CHANGED".equals(s))
                    updateBluetoothToggle();
                else
                if("android.media.RINGER_MODE_CHANGED".equals(s)) {
                    AudioManagerHelper.validateVibrateSettings(context1);
                    updateRingerToggle();
                    updateVibrateToggle();
                } else
                if("android.intent.action.AIRPLANE_MODE".equals(s))
                    updateFlightModeToggle();
                if(true) goto _L4; else goto _L3
_L3:
            }

            final ToggleManager this$0;

             {
                this$0 = ToggleManager.this;
                super();
            }
        };
        mTogglOrderObserver = new ContentObserver(mHandler) {

            public void onChange(boolean flag) {
                if(mToggleOrderChangedListener.size() > 0) {
                    for(Iterator iterator = mToggleOrderChangedListener.iterator(); iterator.hasNext(); ((OnToggleOrderChangedListener)iterator.next()).OnToggleOrderChanged());
                }
            }

            final ToggleManager this$0;

             {
                this$0 = ToggleManager.this;
                super(handler);
            }
        };
        mFlightModeObserver = new ContentObserver(mHandler) {

            public void onChange(boolean flag) {
                updateFlightModeToggle();
            }

            final ToggleManager this$0;

             {
                this$0 = ToggleManager.this;
                super(handler);
            }
        };
        mDriveModeObserver = new ContentObserver(mHandler) {

            public void onChange(boolean flag) {
                updateDriveModeToggle();
            }

            final ToggleManager this$0;

             {
                this$0 = ToggleManager.this;
                super(handler);
            }
        };
        mMobileDataEnableObserver = new ContentObserver(mHandler) {

            public void onChange(boolean flag) {
                updateDataToggle();
            }

            final ToggleManager this$0;

             {
                this$0 = ToggleManager.this;
                super(handler);
            }
        };
        mMobilePolicyEnableObserver = new ContentObserver(mHandler) {

            public void onChange(boolean flag) {
                boolean flag1 = true;
                ToggleManager togglemanager = ToggleManager.this;
                if(android.provider.Settings.Secure.getInt(mResolver, "mobile_policy", 0) != flag1)
                    flag1 = false;
                togglemanager.mMobilePolicyEnable = flag1;
                updateDataToggle();
            }

            final ToggleManager this$0;

             {
                this$0 = ToggleManager.this;
                super(handler);
            }
        };
        mTorchEnableObserver = new ContentObserver(mHandler) {

            public void onChange(boolean flag) {
                updateTorchToggle();
            }

            final ToggleManager this$0;

             {
                this$0 = ToggleManager.this;
                super(handler);
            }
        };
        mPrivacyModeObserver = new ContentObserver(mHandler) {

            public void onChange(boolean flag) {
                updatePrivacyModeToggle();
            }

            final ToggleManager this$0;

             {
                this$0 = ToggleManager.this;
                super(handler);
            }
        };
        mScreenButtonStateObserver = new ContentObserver(mHandler) {

            public void onChange(boolean flag) {
                updateScreenButtonState();
            }

            final ToggleManager this$0;

             {
                this$0 = ToggleManager.this;
                super(handler);
            }
        };
        mLocationAllowedObserver = new ContentObserver(mHandler) {

            public void onChange(boolean flag) {
                updateGpsToggle();
            }

            final ToggleManager this$0;

             {
                this$0 = ToggleManager.this;
                super(handler);
            }
        };
        mAccelerometerRotationObserver = new ContentObserver(mHandler) {

            public void onChange(boolean flag) {
                updateAccelerometerToggle();
            }

            final ToggleManager this$0;

             {
                this$0 = ToggleManager.this;
                super(handler);
            }
        };
        mBrightnessObserver = new ContentObserver(mHandler) {

            public void onChange(boolean flag) {
                queryBrightnessStatus();
                updateBrightnessToggle();
            }

            final ToggleManager this$0;

             {
                this$0 = ToggleManager.this;
                super(handler);
            }
        };
        mPowerModeObserver = new ContentObserver(mHandler) {

            public void onChange(boolean flag) {
                updatePowerModeToggle();
            }

            final ToggleManager this$0;

             {
                this$0 = ToggleManager.this;
                super(handler);
            }
        };
        mVibrateEnableObserver = new ContentObserver(mHandler) {

            public void onChange(boolean flag) {
                updateVibrateToggle();
            }

            final ToggleManager this$0;

             {
                this$0 = ToggleManager.this;
                super(handler);
            }
        };
        sContext = context;
        mResolver = sContext.getContentResolver();
        mResource = sContext.getResources();
        mToggleChangedListener = new ArrayList();
        mToggleOrderChangedListener = new ArrayList();
        mBrightnessAutoAvailable = mResource.getBoolean(0x6090007);
        queryBrightnessStatus();
        IntentFilter intentfilter;
        try {
            Resources resources = context.getPackageManager().getResourcesForApplication("com.android.phone");
            mEnableNetType = resources.getBoolean(resources.getIdentifier("config_preferred_network_type_when_data_enabled", "bool", "com.android.phone"));
        }
        catch(Exception exception) { }
        intentfilter = new IntentFilter();
        intentfilter.addAction("android.media.RINGER_MODE_CHANGED");
        intentfilter.addAction("android.bluetooth.adapter.action.STATE_CHANGED");
        intentfilter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
        intentfilter.addAction("android.net.wifi.SCAN_RESULTS");
        intentfilter.addAction("android.net.wifi.STATE_CHANGE");
        intentfilter.addAction("android.intent.action.AIRPLANE_MODE");
        sContext.registerReceiver(mBroadcastReceiver, intentfilter);
        updateBluetoothToggle();
        updateRingerToggle();
        updateWifiToggle();
        mResolver.registerContentObserver(android.provider.Settings.System.getUriFor("status_bar_toggle_list"), false, mTogglOrderObserver);
        mResolver.registerContentObserver(android.provider.Settings.System.getUriFor("status_bar_toggle_page"), false, mTogglOrderObserver);
        mStatusChangeListenerHandle = ContentResolver.addStatusChangeListener(0x7fffffff, mSyncStatusObserver);
        updateAdvancedSyncToggle();
        updateSyncToggle();
        mResolver.registerContentObserver(android.provider.Settings.System.getUriFor("accelerometer_rotation"), false, mAccelerometerRotationObserver);
        updateAccelerometerToggle();
        mResolver.registerContentObserver(android.provider.Settings.System.getUriFor("airplane_mode_on"), false, mFlightModeObserver);
        updateFlightModeToggle();
        mResolver.registerContentObserver(android.provider.Settings.System.getUriFor("drive_mode_drive_mode"), false, mDriveModeObserver);
        updateDriveModeToggle();
        mResolver.registerContentObserver(android.provider.Settings.Secure.getUriFor("location_providers_allowed"), false, mLocationAllowedObserver);
        updateGpsToggle();
        mResolver.registerContentObserver(android.provider.Settings.Secure.getUriFor("mobile_data"), false, mMobileDataEnableObserver);
        mResolver.registerContentObserver(android.provider.Settings.Secure.getUriFor("mobile_policy"), false, mMobilePolicyEnableObserver);
        mMobilePolicyEnableObserver.onChange(true);
        mResolver.registerContentObserver(android.provider.Settings.Secure.getUriFor("privacy_mode_enabled"), false, mPrivacyModeObserver);
        mSecurityHelper = new ChooseLockSettingsHelper(sContext);
        updatePrivacyModeToggle();
        mResolver.registerContentObserver(android.provider.Settings.System.getUriFor("screen_brightness"), false, mBrightnessObserver);
        mResolver.registerContentObserver(android.provider.Settings.System.getUriFor("screen_auto_brightness_adj"), false, mBrightnessObserver);
        mResolver.registerContentObserver(android.provider.Settings.System.getUriFor("screen_brightness_mode"), false, mBrightnessObserver);
        updateBrightnessToggle();
        mResolver.registerContentObserver(android.provider.Settings.Secure.getUriFor("screen_buttons_state"), false, mScreenButtonStateObserver);
        updateScreenButtonState();
        mResolver.registerContentObserver(android.provider.Settings.System.getUriFor("torch_state"), false, mTorchEnableObserver);
        updateTorchToggle();
        mResolver.registerContentObserver(android.provider.Settings.System.getUriFor("vibrate_in_silent"), false, mVibrateEnableObserver);
        mResolver.registerContentObserver(android.provider.Settings.System.getUriFor("vibrate_in_normal"), false, mVibrateEnableObserver);
        updateVibrateToggle();
        mResolver.registerContentObserver(android.provider.Settings.System.getUriFor("power_mode"), false, mPowerModeObserver);
        updatePowerModeToggle();
    }

    private static void addIfUnselected(ArrayList arraylist, int i) {
        if(!arraylist.contains(Integer.valueOf(i)))
            arraylist.add(Integer.valueOf(i));
    }

    public static ToggleManager createInstance(Context context) {
        if(sToggleManager == null)
            sToggleManager = new ToggleManager(context);
        return sToggleManager;
    }

    private boolean ensureBluetoothAdapter() {
        if(mBluetoothAdapter == null)
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        boolean flag;
        if(mBluetoothAdapter != null)
            flag = true;
        else
            flag = false;
        return flag;
    }

    public static ArrayList getAllToggles() {
        ArrayList arraylist = new ArrayList(23);
        arraylist.add(Integer.valueOf(9));
        arraylist.add(Integer.valueOf(2));
        arraylist.add(Integer.valueOf(19));
        arraylist.add(Integer.valueOf(18));
        arraylist.add(Integer.valueOf(3));
        arraylist.add(Integer.valueOf(6));
        arraylist.add(Integer.valueOf(5));
        arraylist.add(Integer.valueOf(7));
        arraylist.add(Integer.valueOf(15));
        arraylist.add(Integer.valueOf(1));
        arraylist.add(Integer.valueOf(22));
        arraylist.add(Integer.valueOf(4));
        arraylist.add(Integer.valueOf(16));
        arraylist.add(Integer.valueOf(17));
        arraylist.add(Integer.valueOf(14));
        arraylist.add(Integer.valueOf(11));
        arraylist.add(Integer.valueOf(20));
        arraylist.add(Integer.valueOf(10));
        arraylist.add(Integer.valueOf(21));
        arraylist.add(Integer.valueOf(12));
        arraylist.add(Integer.valueOf(13));
        return arraylist;
    }

    public static int getDividerFixedPosition(Context context) {
        int i;
        if(isListStyle(context))
            i = 0;
        else
            i = 11;
        return i;
    }

    public static int getGeneralImage(int i) {
        return sToggleGeneralImages[i];
    }

    private static int getImage(int i) {
        return sToggleImages[i];
    }

    public static Drawable getImageDrawable(int i) {
        Drawable drawable = sContext.getResources().getDrawable(getImage(i));
        drawable.setAlpha(sToggleAlpha[i]);
        return drawable;
    }

    public static int getName(int i) {
        return sToggleNames[i];
    }

    public static boolean getStatus(int i) {
        return sToggleStatus[i];
    }

    private static int getTextColor(int i) {
        int j;
        if(getStatus(i))
            j = sTextColorEnable;
        else
            j = sTextColorDisable;
        return j;
    }

    private static String getToggleOrderSettingID(Context context) {
        String s;
        if(isListStyle(context))
            s = "status_bar_toggle_list";
        else
            s = "status_bar_toggle_page";
        return s;
    }

    public static ArrayList getUserSelectedToggleOrder(Context context) {
        ArrayList arraylist;
        String s;
        arraylist = new ArrayList();
        s = android.provider.Settings.System.getString(context.getContentResolver(), getToggleOrderSettingID(context));
        if(TextUtils.isEmpty(s)) goto _L2; else goto _L1
_L1:
        String as[];
        int i;
        as = s.split(" ");
        i = 0;
_L3:
        if(i >= as.length)
            break; /* Loop/switch isn't completed */
        int j = Integer.valueOf(as[i]).intValue();
        if(getName(j) != 0)
            arraylist.add(Integer.valueOf(j));
        i++;
        if(true) goto _L3; else goto _L2
        Exception exception;
        exception;
        arraylist.clear();
_L2:
        validateToggleOrder(context, arraylist);
        return arraylist;
    }

    public static void initDrawable(int i, Drawable drawable) {
        if(19 == i && (drawable instanceof AnimationDrawable))
            ((AnimationDrawable)drawable).start();
    }

    private boolean isAutoSync() {
        return ContentResolver.getMasterSyncAutomatically();
    }

    public static boolean isListStyle(Context context) {
        boolean flag = true;
        if(android.provider.Settings.System.getInt(context.getContentResolver(), "status_bar_style", flag) != 0)
            flag = false;
        return flag;
    }

    private boolean longClickScreenshot() {
        boolean flag = false;
        String s = null;
        try {
            Object aobj[] = new Object[1];
            aobj[0] = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath();
            BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(CommandLineUtils.runAndOutput("root", "busybox ls -t %s/Screenshots/*.png", aobj)));
            s = bufferedreader.readLine();
            bufferedreader.close();
        }
        catch(IOException ioexception) { }
        if(!TextUtils.isEmpty(s)) {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setDataAndType(Uri.fromFile(new File(s)), "image/*");
            intent.setFlags(0x10000000);
            sContext.startActivity(intent);
            flag = true;
        }
        return flag;
    }

    private void queryBrightnessStatus() {
        boolean flag = true;
        if(mBrightnessAutoAvailable) {
            if(flag != android.provider.Settings.System.getInt(mResolver, "screen_brightness_mode", 0))
                flag = false;
        } else {
            flag = false;
        }
        mBrightnessAutoMode = flag;
        mBrightnessManualLevel = android.provider.Settings.System.getInt(mResolver, "screen_brightness", 102);
        mBrightnessAutoLevel = ((1.0F + android.provider.Settings.System.getFloat(mResolver, "screen_auto_brightness_adj", 0.0F)) * (float)RANGE) / 2.0F;
    }

    public static void setTextColor(int i, int j) {
        sTextColorEnable = i;
        sTextColorDisable = j;
    }

    private void showToast(int i, int j) {
        showToast(((CharSequence) (sContext.getString(i))), j);
    }

    private void showToast(CharSequence charsequence, int i) {
        Toast toast = Toast.makeText(sContext, charsequence, i);
        toast.setType(2006);
        toast.show();
    }

    private void shutdown(final boolean isReboot) {
        int i;
        int j;
        AlertDialog alertdialog;
        if(isReboot)
            i = 0x60c018c;
        else
            i = 0x60c0191;
        if(isReboot)
            j = 0x60c0000;
        else
            j = 0x60c0192;
        alertdialog = (new android.app.AlertDialog.Builder(sContext, 3)).setIconAttribute(0x1010355).setTitle(i).setMessage(j).setPositiveButton(0x1040013, new android.content.DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialoginterface, int k) {
                ((StatusBarManager)ToggleManager.sContext.getSystemService("statusbar")).collapse();
                mHandler.postDelayed(new Runnable() {

                    public void run() {
                        if(isReboot) {
                            ((PowerManager)ToggleManager.sContext.getSystemService("power")).reboot(null);
                        } else {
                            Intent intent = new Intent("android.intent.action.ACTION_REQUEST_SHUTDOWN");
                            intent.setFlags(0x10000000);
                            ToggleManager.sContext.startActivity(intent);
                        }
                    }

                    final _cls19 this$1;

                     {
                        this$1 = _cls19.this;
                        super();
                    }
                }, 1000L);
            }

            final ToggleManager this$0;
            final boolean val$isReboot;

             {
                this$0 = ToggleManager.this;
                isReboot = flag;
                super();
            }
        }).setNegativeButton(0x1040009, null).create();
        alertdialog.getWindow().setType(2010);
        alertdialog.show();
    }

    private void toggleAccelerometer() {
        try {
            IWindowManager iwindowmanager = android.view.IWindowManager.Stub.asInterface(ServiceManager.getService("window"));
            if(!mAccelerometer) {
                iwindowmanager.thawRotation();
            } else {
                if(iwindowmanager.getRotation() != 0)
                    showToast(0x60c0215, 1);
                iwindowmanager.freezeRotation(-1);
            }
        }
        catch(RemoteException remoteexception) { }
    }

    private void toggleAdvancedSync() {
        List list = ContentResolver.getCurrentSyncs();
        boolean flag;
        if(list != null && !list.isEmpty())
            flag = true;
        else
            flag = false;
        if(!flag) {
            Bundle bundle = new Bundle();
            if(!isAutoSync())
                bundle.putBoolean("force", true);
            ContentResolver.requestSync(null, null, bundle);
        } else {
            ContentResolver.cancelSync(null, null);
        }
        updateAdvancedSyncToggle();
    }

    private void toggleAutoBrightness() {
        boolean flag = false;
        if(mBrightnessAutoMode) {
            mBrightnessAutoMode = false;
        } else {
            if(mBrightnessAutoAvailable)
                flag = true;
            mBrightnessAutoMode = flag;
        }
        applyBrightness(getCurBrightness(), true);
    }

    private void toggleBluetooth() {
        if(ensureBluetoothAdapter())
            if(mBluetoothEnable) {
                mBluetoothAdapter.disable();
                mBluetoothEnabling = false;
            } else {
                mBluetoothAdapter.enable();
                mBluetoothEnabling = true;
            }
    }

    private void toggleBrightness() {
        if(!mBrightnessAutoMode) goto _L2; else goto _L1
_L1:
        mBrightnessManualLevel = MINIMUM_BACKLIGHT;
        mBrightnessAutoMode = false;
_L4:
        applyBrightness(getCurBrightness(), true);
        return;
_L2:
        if(mBrightnessManualLevel <= MINIMUM_BACKLIGHT)
            mBrightnessManualLevel = 102;
        else
        if(mBrightnessManualLevel < 255)
            mBrightnessManualLevel = 255;
        else
        if(mBrightnessAutoAvailable) {
            mBrightnessAutoMode = true;
            mBrightnessManualLevel = MINIMUM_BACKLIGHT;
        }
        if(true) goto _L4; else goto _L3
_L3:
    }

    private boolean toggleData() {
        boolean flag;
        boolean flag1;
        ConnectivityManager connectivitymanager;
        flag = true;
        flag1 = false;
        connectivitymanager = (ConnectivityManager)sContext.getSystemService("connectivity");
        if(!mMobilePolicyEnable) goto _L2; else goto _L1
_L1:
        if(mMobileDataEnable)
            flag = false;
        connectivitymanager.setMobileDataEnabled(flag);
_L4:
        return flag1;
_L2:
        connectivitymanager.setMobileDataEnabled(flag);
        String s = ((TelephonyManager)sContext.getSystemService("phone")).getSubscriberId();
        if(!TextUtils.isEmpty(s)) {
            NetworkTemplate networktemplate = NetworkTemplate.buildTemplateMobileAll(s);
            Intent intent = new Intent();
            intent.setComponent(new ComponentName("com.android.systemui", "com.android.systemui.net.NetworkOverLimitActivity"));
            intent.addFlags(0x10000000);
            intent.putExtra("android.net.NETWORK_TEMPLATE", networktemplate);
            sContext.startActivity(intent);
            flag1 = true;
        }
        if(true) goto _L4; else goto _L3
_L3:
    }

    private void toggleDriveMode() {
        int i = 1;
        boolean flag;
        ContentResolver contentresolver;
        if(!mDriveMode)
            flag = i;
        else
            flag = false;
        mDriveMode = flag;
        if(mDriveMode)
            FirewallManager.getInstance().addOneShotFlag(4);
        else
            FirewallManager.getInstance().removeOneShotFlag(4);
        contentresolver = mResolver;
        if(!mDriveMode)
            i = 0;
        android.provider.Settings.System.putInt(contentresolver, "drive_mode_drive_mode", i);
        if(mDriveMode) {
            Intent intent = new Intent("com.miui.app.ExtraStatusBarManager.action_enter_drive_mode");
            sContext.sendBroadcast(intent);
        } else {
            Intent intent1 = new Intent("com.miui.app.ExtraStatusBarManager.action_leave_drive_mode");
            sContext.sendBroadcast(intent1);
        }
    }

    private void toggleFlightMode() {
        int i = 1;
        boolean flag;
        ContentResolver contentresolver;
        Intent intent;
        if(!mFlightMode)
            flag = i;
        else
            flag = false;
        mFlightMode = flag;
        contentresolver = mResolver;
        if(!mFlightMode)
            i = 0;
        android.provider.Settings.System.putInt(contentresolver, "airplane_mode_on", i);
        intent = new Intent("android.intent.action.AIRPLANE_MODE");
        intent.addFlags(0x20000000);
        intent.putExtra("state", mFlightMode);
        sContext.sendBroadcast(intent);
    }

    private void toggleGps() {
        ContentResolver contentresolver = mResolver;
        boolean flag;
        if(!mGpsEnable)
            flag = true;
        else
            flag = false;
        android.provider.Settings.Secure.setLocationProviderEnabled(contentresolver, "gps", flag);
    }

    private void togglePowerMode() {
        if("low".equals(mPowerMode))
            mPowerMode = "middle";
        else
        if("middle".equals(mPowerMode))
            mPowerMode = "high";
        else
            mPowerMode = "low";
        SystemProperties.set("persist.sys.aries.power_profile", mPowerMode);
        android.provider.Settings.System.putString(mResolver, "power_mode", mPowerMode);
    }

    private boolean togglePrivacyMode() {
        boolean flag = false;
        boolean flag1;
        if(!mPrivacyMode)
            flag1 = true;
        else
            flag1 = false;
        mPrivacyMode = flag1;
        if(mPrivacyMode) {
            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(sContext, 3);
            builder.setTitle(0x60c0171);
            builder.setMessage(0x60c0174);
            builder.setPositiveButton(0x104000a, new android.content.DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialoginterface, int i) {
                    mSecurityHelper.setPrivacyModeEnabled(true);
                }

                final ToggleManager this$0;

             {
                this$0 = ToggleManager.this;
                super();
            }
            });
            builder.setNegativeButton(0x1040000, new android.content.DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialoginterface, int i) {
                    ToggleManager togglemanager = ToggleManager.this;
                    boolean flag2;
                    if(!mPrivacyMode)
                        flag2 = true;
                    else
                        flag2 = false;
                    togglemanager.mPrivacyMode = flag2;
                    dialoginterface.cancel();
                }

                final ToggleManager this$0;

             {
                this$0 = ToggleManager.this;
                super();
            }
            });
            AlertDialog alertdialog = builder.create();
            alertdialog.getWindow().setType(2010);
            alertdialog.show();
        } else
        if(mSecurityHelper.isACLockEnabled() && mSecurityHelper.isPasswordForPrivacyModeEnabled()) {
            Intent intent = new Intent("android.app.action.CONFIRM_ACCESS_CONTROL");
            intent.putExtra("confirm_purpose", 4);
            intent.addFlags(0x10000000);
            intent.addFlags(0x4000000);
            intent.addFlags(0x20000000);
            sContext.startActivity(intent);
            flag = true;
        } else {
            mSecurityHelper.setPrivacyModeEnabled(false);
        }
        return flag;
    }

    private void toggleRinger() {
        AudioManagerHelper.toggleSilent(sContext, 5);
    }

    private void toggleScreenButtonState() {
        int i = 1;
        boolean flag;
        ContentResolver contentresolver;
        if(!mScreenButtonDisabled)
            flag = i;
        else
            flag = false;
        mScreenButtonDisabled = flag;
        if(android.provider.Settings.Secure.getInt(mResolver, "screen_buttons_has_been_disabled", 0) == 0) {
            android.provider.Settings.Secure.putInt(mResolver, "screen_buttons_has_been_disabled", i);
            AlertDialog alertdialog = (new android.app.AlertDialog.Builder(sContext, 3)).setMessage(0x60c0216).setPositiveButton(0x104000a, null).create();
            alertdialog.getWindow().setType(2010);
            alertdialog.show();
        } else {
            int j;
            if(mScreenButtonDisabled)
                j = 0x60c0216;
            else
                j = 0x60c0217;
            showToast(j, 0);
        }
        contentresolver = mResolver;
        if(!mScreenButtonDisabled)
            i = 0;
        android.provider.Settings.Secure.putInt(contentresolver, "screen_buttons_state", i);
    }

    private void toggleScreenshot() {
        ((StatusBarManager)sContext.getSystemService("statusbar")).collapse();
        sContext.sendBroadcast(new Intent("android.intent.action.CAPTURE_SCREENSHOT"));
    }

    private void toggleSync() {
        boolean flag;
        if(!ContentResolver.getMasterSyncAutomatically())
            flag = true;
        else
            flag = false;
        ContentResolver.setMasterSyncAutomatically(flag);
    }

    private void toggleTorch() {
        Intent intent = new Intent("miui.intent.action.TOGGLE_TORCH");
        intent.putExtra("miui.intent.extra.IS_TOGGLE", true);
        sContext.sendBroadcast(intent);
    }

    private void toggleVibrate() {
        AudioManagerHelper.toggleVibrateSetting(sContext);
    }

    private void updateAccelerometerToggle() {
        boolean flag;
        if(android.provider.Settings.System.getInt(mResolver, "accelerometer_rotation", 0) != 0)
            flag = true;
        else
            flag = false;
        mAccelerometer = flag;
        updateToggleStatus(3, mAccelerometer);
        if(mAccelerometer) {
            updateToggleImage(3, 0x60201e5);
        } else {
            int i = android.provider.Settings.System.getInt(mResolver, "user_rotation", 0);
            int j;
            if(i == 0 || 2 == i)
                j = 0x60201e3;
            else
                j = 0x60201e4;
            updateToggleImage(3, j);
        }
    }

    private void updateAdvancedSyncToggle() {
        List list = ContentResolver.getCurrentSyncs();
        boolean flag;
        if(list != null && !list.isEmpty())
            flag = true;
        else
            flag = false;
        updateAdvancedSyncToggle(isAutoSync(), flag);
    }

    private void updateAdvancedSyncToggle(boolean flag, boolean flag1) {
        int i;
        if(flag) {
            if(flag1)
                i = 0x60201ee;
            else
                i = 0x60201f1;
        } else
        if(flag1)
            i = 0x60201ef;
        else
            i = 0x60201f9;
        updateToggleStatus(19, flag1);
        updateToggleImage(19, i);
    }

    private void updateBrightnessToggle() {
        int i;
        int j;
        if(mBrightnessAutoMode) {
            j = 0x60201ce;
            i = j;
        } else {
            i = 0x60201d0;
            if(mBrightnessManualLevel <= MINIMUM_BACKLIGHT)
                j = 0x60201d1;
            else
            if(mBrightnessManualLevel < 255)
                j = 0x60201cf;
            else
                j = 0x60201d2;
        }
        updateToggleStatus(4, mBrightnessAutoMode);
        updateToggleStatus(22, mBrightnessAutoMode);
        updateToggleImage(4, j);
        updateToggleImage(22, i);
    }

    private void updateDataToggle() {
        mMobileDataEnable = ((ConnectivityManager)sContext.getSystemService("connectivity")).getMobileDataEnabled();
        boolean flag;
        int i;
        char c;
        if(mMobileDataEnable && mMobilePolicyEnable)
            flag = true;
        else
            flag = false;
        updateToggleStatus(1, flag);
        if(flag)
            i = 0x60201d4;
        else
            i = 0x60201d3;
        if(mFlightMode)
            c = '}';
        else
            c = '\377';
        updateToggleImage(1, i, c);
        if(!mEnableNetType && flag)
            updateToggleImage(17, 0x60201df, 125);
        else
            updateToggleImage(17, 0x60201df);
    }

    private void updateDriveModeToggle() {
        mDriveMode = FirewallManager.getInstance().getOneShotFlag(4);
        ContentResolver contentresolver = mResolver;
        int i;
        int j;
        if(mDriveMode)
            i = 1;
        else
            i = 0;
        android.provider.Settings.System.putInt(contentresolver, "drive_mode_drive_mode", i);
        if(mDriveMode) {
            Intent intent = new Intent("com.miui.app.ExtraStatusBarManager.action_enter_drive_mode");
            sContext.sendBroadcast(intent);
        } else {
            Intent intent1 = new Intent("com.miui.app.ExtraStatusBarManager.action_leave_drive_mode");
            sContext.sendBroadcast(intent1);
        }
        updateToggleStatus(21, mDriveMode);
        if(mDriveMode)
            j = 0x60202d4;
        else
            j = 0x60202d5;
        updateToggleImage(21, j);
        updateDataToggle();
    }

    private void updateFlightModeToggle() {
        boolean flag = false;
        if(android.provider.Settings.System.getInt(mResolver, "airplane_mode_on", 0) != 0)
            flag = true;
        mFlightMode = flag;
        updateToggleStatus(9, mFlightMode);
        int i;
        if(mFlightMode)
            i = 0x60201d9;
        else
            i = 0x60201d8;
        updateToggleImage(9, i);
        updateDataToggle();
    }

    private void updateGpsToggle() {
        mGpsEnable = android.provider.Settings.Secure.isLocationProviderEnabled(mResolver, "gps");
        updateToggleStatus(7, mGpsEnable);
        int i;
        if(mGpsEnable)
            i = 0x60201db;
        else
            i = 0x60201da;
        updateToggleImage(7, i);
    }

    public static void updateImageView(int i, ImageView imageview) {
        Drawable drawable = getImageDrawable(i);
        imageview.setImageDrawable(drawable);
        initDrawable(i, drawable);
    }

    private void updatePowerModeToggle() {
        mPowerMode = android.provider.Settings.System.getString(mResolver, "power_mode");
        if(TextUtils.isEmpty(mPowerMode))
            mPowerMode = "middle";
        int i;
        if("low".equals(mPowerMode))
            i = 0x602022b;
        else
        if("middle".equals(mPowerMode))
            i = 0x602022c;
        else
            i = 0x602022a;
        updateToggleImage(23, i);
    }

    private void updatePrivacyModeToggle() {
        mPrivacyMode = mSecurityHelper.isPrivacyModeEnabled();
        updateToggleStatus(14, mPrivacyMode);
        int i;
        if(mPrivacyMode)
            i = 0x60201e1;
        else
            i = 0x60201e0;
        updateToggleImage(14, i);
    }

    private void updateScreenButtonState() {
        boolean flag = false;
        if(android.provider.Settings.Secure.getInt(mResolver, "screen_buttons_state", 0) != 0)
            flag = true;
        mScreenButtonDisabled = flag;
        updateToggleStatus(20, mScreenButtonDisabled);
        int i;
        if(mScreenButtonDisabled)
            i = 0x60201e6;
        else
            i = 0x60201e7;
        updateToggleImage(20, i);
    }

    private void updateSyncToggle() {
        int i;
        if(ContentResolver.getMasterSyncAutomatically())
            i = 0x60201fa;
        else
            i = 0x60201f9;
        updateToggleImage(8, i);
    }

    public static void updateTextView(int i, TextView textview) {
        textview.setText(getName(i));
        textview.setTextColor(getTextColor(i));
    }

    private void updateToggleImage(int i, int j) {
        updateToggleImage(i, j, 255);
    }

    private void updateToggleImage(int i, int j, int k) {
        sToggleImages[i] = j;
        sToggleAlpha[i] = k;
        if(mToggleChangedListener.size() > 0) {
            int l = -1 + mToggleChangedListener.size();
            while(l >= 0)  {
                OnToggleChangedListener ontogglechangedlistener = (OnToggleChangedListener)((WeakReference)mToggleChangedListener.get(l)).get();
                if(ontogglechangedlistener == null)
                    mToggleChangedListener.remove(l);
                else
                    ontogglechangedlistener.OnToggleChanged(i);
                l--;
            }
        }
    }

    private static void updateToggleStatus(int i, boolean flag) {
        sToggleStatus[i] = flag;
    }

    private void updateTorchToggle() {
        boolean flag = true;
        int i;
        if(android.provider.Settings.System.getInt(sContext.getContentResolver(), "torch_state", 0) != flag)
            flag = false;
        mTorchEnable = flag;
        updateToggleStatus(11, mTorchEnable);
        if(mTorchEnable)
            i = 0x60201d6;
        else
            i = 0x60201c8;
        updateToggleImage(11, i);
    }

    private static void validateToggleList(ArrayList arraylist) {
        addIfUnselected(arraylist, 15);
        addIfUnselected(arraylist, 1);
        addIfUnselected(arraylist, 7);
        addIfUnselected(arraylist, 4);
        addIfUnselected(arraylist, 5);
        addIfUnselected(arraylist, 6);
        addIfUnselected(arraylist, 3);
        addIfUnselected(arraylist, 18);
        addIfUnselected(arraylist, 9);
        addIfUnselected(arraylist, 0);
        addIfUnselected(arraylist, 2);
        addIfUnselected(arraylist, 19);
        addIfUnselected(arraylist, 22);
        addIfUnselected(arraylist, 16);
        addIfUnselected(arraylist, 17);
        addIfUnselected(arraylist, 14);
        addIfUnselected(arraylist, 11);
        addIfUnselected(arraylist, 20);
        addIfUnselected(arraylist, 10);
        addIfUnselected(arraylist, 21);
        addIfUnselected(arraylist, 12);
        addIfUnselected(arraylist, 13);
        int i;
        if(Build.IS_MITWO)
            addIfUnselected(arraylist, 23);
        else
            arraylist.remove(Integer.valueOf(23));
        arraylist.remove(Integer.valueOf(8));
        i = arraylist.indexOf(Integer.valueOf(0));
        if(i < 4) {
            arraylist.add(5, Integer.valueOf(0));
            arraylist.remove(i);
        }
        sContext.getPackageManager().getPackageInfo("com.miui.voiceassist", 0);
_L1:
        return;
        android.content.pm.PackageManager.NameNotFoundException namenotfoundexception;
        namenotfoundexception;
        arraylist.remove(Integer.valueOf(21));
          goto _L1
    }

    private static void validateToggleOrder(Context context, ArrayList arraylist) {
        if(isListStyle(context))
            validateToggleList(arraylist);
        else
            validateTogglePage(arraylist);
    }

    private static void validateTogglePage(ArrayList arraylist) {
        if(arraylist.size() >= 10 && !arraylist.contains(Integer.valueOf(22)))
            arraylist.add(10, Integer.valueOf(22));
        addIfUnselected(arraylist, 9);
        addIfUnselected(arraylist, 2);
        addIfUnselected(arraylist, 19);
        addIfUnselected(arraylist, 18);
        addIfUnselected(arraylist, 3);
        addIfUnselected(arraylist, 6);
        addIfUnselected(arraylist, 5);
        addIfUnselected(arraylist, 7);
        addIfUnselected(arraylist, 15);
        addIfUnselected(arraylist, 1);
        addIfUnselected(arraylist, 22);
        addIfUnselected(arraylist, 4);
        addIfUnselected(arraylist, 16);
        addIfUnselected(arraylist, 17);
        addIfUnselected(arraylist, 14);
        addIfUnselected(arraylist, 11);
        addIfUnselected(arraylist, 20);
        addIfUnselected(arraylist, 10);
        addIfUnselected(arraylist, 21);
        addIfUnselected(arraylist, 12);
        addIfUnselected(arraylist, 13);
        if(Build.IS_MITWO)
            addIfUnselected(arraylist, 23);
        else
            arraylist.remove(Integer.valueOf(23));
        arraylist.remove(Integer.valueOf(0));
        arraylist.remove(Integer.valueOf(8));
        try {
            sContext.getPackageManager().getPackageInfo("com.miui.voiceassist", 0);
        }
        catch(android.content.pm.PackageManager.NameNotFoundException namenotfoundexception) {
            arraylist.remove(Integer.valueOf(21));
        }
        arraylist.add(11, Integer.valueOf(0));
    }

    public void applyBrightness(int i, boolean flag) {
        try {
            IPowerManager ipowermanager = android.os.IPowerManager.Stub.asInterface(ServiceManager.getService("power"));
            if(ipowermanager != null)
                if(mBrightnessAutoMode) {
                    float f = (2.0F * (float)i) / (float)RANGE - 1.0F;
                    ipowermanager.setAutoBrightnessAdjustment(f);
                    if(flag) {
                        android.provider.Settings.System.putInt(mResolver, "screen_brightness_mode", 1);
                        android.provider.Settings.System.putFloat(mResolver, "screen_auto_brightness_adj", f);
                    }
                } else {
                    int j = i + MINIMUM_BACKLIGHT;
                    ipowermanager.setBacklightBrightness(i);
                    if(flag) {
                        android.provider.Settings.System.putInt(mResolver, "screen_brightness_mode", 0);
                        android.provider.Settings.System.putInt(mResolver, "screen_brightness", j);
                    }
                }
        }
        catch(RemoteException remoteexception) {
            Log.d("ToggleManager", (new StringBuilder()).append("setBrightness: ").append(remoteexception).toString());
        }
    }

    public int getCurBrightness() {
        float f;
        if(mBrightnessAutoMode)
            f = mBrightnessAutoLevel;
        else
            f = mBrightnessManualLevel - MINIMUM_BACKLIGHT;
        return (int)f;
    }

    public void onDestroy() {
        sContext.unregisterReceiver(mBroadcastReceiver);
        mResolver.unregisterContentObserver(mTogglOrderObserver);
        mResolver.unregisterContentObserver(mMobileDataEnableObserver);
        mResolver.unregisterContentObserver(mMobilePolicyEnableObserver);
        mResolver.unregisterContentObserver(mTorchEnableObserver);
        mResolver.unregisterContentObserver(mPrivacyModeObserver);
        mResolver.unregisterContentObserver(mScreenButtonStateObserver);
        mResolver.unregisterContentObserver(mLocationAllowedObserver);
        mResolver.unregisterContentObserver(mAccelerometerRotationObserver);
        mResolver.unregisterContentObserver(mBrightnessObserver);
        mResolver.unregisterContentObserver(mVibrateEnableObserver);
        mResolver.unregisterContentObserver(mPowerModeObserver);
        mResolver.unregisterContentObserver(mDriveModeObserver);
        ContentResolver.removeStatusChangeListener(mStatusChangeListenerHandle);
        mToggleChangedListener.clear();
        mToggleOrderChangedListener.clear();
    }

    public boolean performToggle(int i) {
        boolean flag = PreferenceManager.getDefaultSharedPreferences(sContext).getBoolean("toggle_collapse_after_clicked", false);
        i;
        JVM INSTR tableswitch 0 23: default 128
    //                   0 225
    //                   1 208
    //                   2 192
    //                   3 373
    //                   4 199
    //                   5 317
    //                   6 418
    //                   7 287
    //                   8 404
    //                   9 280
    //                   10 294
    //                   11 411
    //                   12 365
    //                   13 396
    //                   14 357
    //                   15 425
    //                   16 130
    //                   17 324
    //                   18 387
    //                   19 176
    //                   20 380
    //                   21 445
    //                   22 183
    //                   23 438;
           goto _L1 _L2 _L3 _L4 _L5 _L6 _L7 _L8 _L9 _L10 _L11 _L12 _L13 _L14 _L15 _L16 _L17 _L18 _L19 _L20 _L21 _L22 _L23 _L24 _L25
_L1:
        return flag;
_L18:
        Intent intent1 = new Intent("android.intent.action.MAIN");
        intent1.setClassName("com.android.settings", "com.android.settings.AccessControlSetApp");
        intent1.setFlags(0x10000000);
        sContext.startActivity(intent1);
        flag = true;
        continue; /* Loop/switch isn't completed */
_L21:
        toggleAdvancedSync();
        continue; /* Loop/switch isn't completed */
_L24:
        toggleAutoBrightness();
        flag = false;
        continue; /* Loop/switch isn't completed */
_L4:
        toggleBluetooth();
        continue; /* Loop/switch isn't completed */
_L6:
        toggleBrightness();
        flag = false;
        continue; /* Loop/switch isn't completed */
_L3:
        if(!mFlightMode)
            flag |= toggleData();
        continue; /* Loop/switch isn't completed */
_L2:
        Intent intent = new Intent("android.intent.action.MAIN");
        intent.setClassName("com.android.systemui", "com.android.systemui.settings.ToggleArrangement");
        intent.setFlags(0x14000000);
        intent.putExtra("com.android.systemui.settings.EXTRA_LOCATE_DIVIDER", true);
        sContext.startActivity(intent);
        flag = true;
        continue; /* Loop/switch isn't completed */
_L11:
        toggleFlightMode();
        continue; /* Loop/switch isn't completed */
_L9:
        toggleGps();
        continue; /* Loop/switch isn't completed */
_L12:
        ((PowerManager)sContext.getSystemService("power")).goToSleep(1L + SystemClock.uptimeMillis());
        continue; /* Loop/switch isn't completed */
_L7:
        toggleRinger();
        continue; /* Loop/switch isn't completed */
_L19:
        if(!mMobileDataEnable || !mMobilePolicyEnable || mEnableNetType) {
            startLongClickAction(17);
            flag = true;
        }
        continue; /* Loop/switch isn't completed */
_L16:
        flag = togglePrivacyMode();
        continue; /* Loop/switch isn't completed */
_L14:
        shutdown(true);
        continue; /* Loop/switch isn't completed */
_L5:
        toggleAccelerometer();
        continue; /* Loop/switch isn't completed */
_L22:
        toggleScreenButtonState();
        continue; /* Loop/switch isn't completed */
_L20:
        toggleScreenshot();
        flag = true;
        continue; /* Loop/switch isn't completed */
_L15:
        shutdown(false);
        continue; /* Loop/switch isn't completed */
_L10:
        toggleSync();
        continue; /* Loop/switch isn't completed */
_L13:
        toggleTorch();
        continue; /* Loop/switch isn't completed */
_L8:
        toggleVibrate();
        continue; /* Loop/switch isn't completed */
_L17:
        mWifiState.toggleState(sContext);
        continue; /* Loop/switch isn't completed */
_L25:
        togglePowerMode();
        continue; /* Loop/switch isn't completed */
_L23:
        toggleDriveMode();
        if(true) goto _L1; else goto _L26
_L26:
    }

    public void removeToggleChangedListener(OnToggleChangedListener ontogglechangedlistener) {
        for(int i = -1 + mToggleChangedListener.size(); i >= 0; i--) {
            WeakReference weakreference = (WeakReference)mToggleChangedListener.get(i);
            if(weakreference.get() == null || ontogglechangedlistener.equals(weakreference.get()))
                mToggleChangedListener.remove(i);
        }

    }

    public void removeToggleOrderChangeListener(OnToggleOrderChangedListener ontoggleorderchangedlistener) {
        mToggleOrderChangedListener.remove(ontoggleorderchangedlistener);
    }

    public void setOnToggleChangedListener(OnToggleChangedListener ontogglechangedlistener) {
        mToggleChangedListener.add(new WeakReference(ontogglechangedlistener));
    }

    public void setOnToggleOrderChangeListener(OnToggleOrderChangedListener ontoggleorderchangedlistener) {
        mToggleOrderChangedListener.add(ontoggleorderchangedlistener);
    }

    public void setUserSelectedToggleOrder(ArrayList arraylist) {
        validateToggleOrder(sContext, arraylist);
        StringBuilder stringbuilder = new StringBuilder(72);
        for(int i = 0; i < arraylist.size(); i++) {
            stringbuilder.append(((Integer)arraylist.get(i)).toString());
            stringbuilder.append(" ");
        }

        android.provider.Settings.System.putString(sContext.getContentResolver(), getToggleOrderSettingID(sContext), stringbuilder.toString());
    }

    public boolean startLongClickAction(int i) {
        boolean flag = false;
        if(18 != i) goto _L2; else goto _L1
_L1:
        flag = longClickScreenshot();
_L4:
        return flag;
_L2:
        if(1 != i || !mFlightMode) {
            int j = sLongClickActions[i];
            if(j != 0) {
                String s = sContext.getResources().getString(j);
                if(s != null) {
                    ComponentName componentname = ComponentName.unflattenFromString(s);
                    if(componentname != null) {
                        Intent intent = new Intent("android.intent.action.MAIN");
                        intent.setComponent(componentname);
                        if(i == 0)
                            intent.setFlags(0x14000000);
                        else
                            intent.setFlags(0x10000000);
                        if(21 == i)
                            intent.putExtra("com.android.settings.FRAGMENT_CLASS", "com.android.settings.MiuiDriveModeSettings");
                        sContext.startActivity(intent);
                        flag = true;
                    }
                }
            }
        }
        if(true) goto _L4; else goto _L3
_L3:
    }

    void updateBluetoothToggle() {
        if(ensureBluetoothAdapter())
            mBluetoothEnable = mBluetoothAdapter.isEnabled();
        boolean flag;
        if(mBluetoothEnable || mBluetoothEnabling)
            flag = true;
        else
            flag = false;
        updateToggleStatus(2, flag);
        if(mBluetoothEnable) {
            mBluetoothEnabling = false;
            updateToggleImage(2, 0x60201cd);
        } else
        if(mBluetoothEnabling)
            updateToggleImage(2, 0x60201cd, 125);
        else
            updateToggleImage(2, 0x60201cc);
    }

    public void updateRingerToggle() {
        updateToggleStatus(5, AudioManagerHelper.isSilentEnabled(sContext));
        int i;
        if(AudioManagerHelper.isSilentEnabled(sContext))
            i = 0x60201de;
        else
            i = 0x60201dd;
        updateToggleImage(5, i);
    }

    public void updateVibrateToggle() {
        boolean flag = AudioManagerHelper.isVibrateEnabled(sContext);
        updateToggleStatus(6, flag);
        int i;
        if(flag)
            i = 0x60201e9;
        else
            i = 0x60201d7;
        updateToggleImage(6, i);
    }

    void updateWifiToggle() {
        mWifiState.getTriState(sContext);
        JVM INSTR tableswitch 0 5: default 48
    //                   0 49
    //                   1 67
    //                   2 48
    //                   3 48
    //                   4 48
    //                   5 116;
           goto _L1 _L2 _L3 _L1 _L1 _L1 _L4
_L1:
        return;
_L2:
        updateToggleStatus(15, false);
        updateToggleImage(15, 0x60201eb);
        continue; /* Loop/switch isn't completed */
_L3:
        updateToggleStatus(15, ((WifiStateTracker)mWifiState).zConnected);
        int i;
        if(((WifiStateTracker)mWifiState).zConnected)
            i = 0x60201ec;
        else
            i = 0x60201ed;
        updateToggleImage(15, i);
        continue; /* Loop/switch isn't completed */
_L4:
        updateToggleStatus(15, mWifiState.isTurningOn());
        if(mWifiState.isTurningOn())
            updateToggleImage(15, 0x60201ec, 125);
        else
            updateToggleImage(15, 0x60201eb);
        if(true) goto _L1; else goto _L5
_L5:
    }

    public static final int ALPHA_DEFAULT = 255;
    public static final int ALPHA_HALF = 125;
    public static final int DEFAULT_BACKLIGHT = 102;
    private static final int DIVIDER_FIXED_POSITION_IN_PAGE_STYLE = 11;
    public static final String EXTRA_LOCATE_DIVIDER = "com.android.systemui.settings.EXTRA_LOCATE_DIVIDER";
    public static final String KEY_TOGGLE_COLLAPSE_AFTER_CLICKED = "toggle_collapse_after_clicked";
    public static final int MAXIMUM_BACKLIGHT = 255;
    public static final int MINIMUM_BACKLIGHT = 0;
    private static final int MINIMUM_COUNT_OF_TOGGLES_IN_STATUS_BAR = 4;
    public static final int RANGE = 0;
    private static final int STATE_DISABLED = 0;
    private static final int STATE_ENABLED = 1;
    private static final int STATE_INTERMEDIATE = 5;
    private static final int STATE_TURNING_OFF = 3;
    private static final int STATE_TURNING_ON = 2;
    private static final int STATE_UNKNOWN = 4;
    static final String TAG = "ToggleManager";
    public static final int TOGGLE_ACCESS_CONTROL = 16;
    public static final int TOGGLE_ADVANCED_SYNC = 19;
    public static final int TOGGLE_AUTO_BRIGHTNESS = 22;
    public static final int TOGGLE_BLUETOOTH = 2;
    public static final int TOGGLE_BRIGHTNESS = 4;
    public static final int TOGGLE_COUNT = 24;
    public static final int TOGGLE_DATA = 1;
    public static final int TOGGLE_DIVIDER = 0;
    public static final int TOGGLE_DRIVE_MODE = 21;
    public static final int TOGGLE_FLIGHT_MODE = 9;
    public static final int TOGGLE_GPS = 7;
    public static final int TOGGLE_LOCK = 10;
    public static final int TOGGLE_NETWORK_TYPE = 17;
    public static final int TOGGLE_POWER_MODE = 23;
    public static final int TOGGLE_PRIVACY_MODE = 14;
    public static final int TOGGLE_REBOOT = 12;
    public static final int TOGGLE_RINGER = 5;
    public static final int TOGGLE_ROTATE = 3;
    public static final int TOGGLE_SCREENSHOT = 18;
    public static final int TOGGLE_SCREEN_BUTTON = 20;
    public static final int TOGGLE_SHUTDOWN = 13;
    public static final int TOGGLE_SYNC = 8;
    public static final int TOGGLE_TORCH = 11;
    public static final int TOGGLE_VIBRATE = 6;
    public static final int TOGGLE_WIFI = 15;
    private static Context sContext;
    private static int sLongClickActions[];
    private static int sTextColorDisable;
    private static int sTextColorEnable;
    private static int sToggleAlpha[];
    private static int sToggleGeneralImages[];
    private static int sToggleImages[];
    private static ToggleManager sToggleManager = null;
    private static int sToggleNames[];
    private static boolean sToggleStatus[] = new boolean[24];
    private boolean mAccelerometer;
    private final ContentObserver mAccelerometerRotationObserver;
    private BluetoothAdapter mBluetoothAdapter;
    private boolean mBluetoothEnable;
    private boolean mBluetoothEnabling;
    private boolean mBrightnessAutoAvailable;
    private float mBrightnessAutoLevel;
    private boolean mBrightnessAutoMode;
    private int mBrightnessManualLevel;
    private final ContentObserver mBrightnessObserver;
    private BroadcastReceiver mBroadcastReceiver;
    private boolean mDriveMode;
    private final ContentObserver mDriveModeObserver;
    private boolean mEnableNetType;
    private boolean mFlightMode;
    private final ContentObserver mFlightModeObserver;
    private boolean mGpsEnable;
    private final Handler mHandler = new Handler();
    private final ContentObserver mLocationAllowedObserver;
    private boolean mMobileDataEnable;
    private final ContentObserver mMobileDataEnableObserver;
    private boolean mMobilePolicyEnable;
    private final ContentObserver mMobilePolicyEnableObserver;
    private String mPowerMode;
    private final ContentObserver mPowerModeObserver;
    private boolean mPrivacyMode;
    private final ContentObserver mPrivacyModeObserver;
    private final ContentResolver mResolver;
    private final Resources mResource;
    private boolean mScreenButtonDisabled;
    private final ContentObserver mScreenButtonStateObserver;
    private final ChooseLockSettingsHelper mSecurityHelper;
    private Object mStatusChangeListenerHandle;
    private final SyncStatusObserver mSyncStatusObserver = new SyncStatusObserver() {

        public void onStatusChanged(int i) {
            mHandler.removeCallbacks(mUpdateSyncStateRunnable);
            mHandler.postDelayed(mUpdateSyncStateRunnable, 300L);
        }

        final ToggleManager this$0;

             {
                this$0 = ToggleManager.this;
                super();
            }
    };
    private final ContentObserver mTogglOrderObserver;
    private List mToggleChangedListener;
    private List mToggleOrderChangedListener;
    private boolean mTorchEnable;
    private final ContentObserver mTorchEnableObserver;
    private final Runnable mUpdateSyncStateRunnable = new Runnable() {

        public void run() {
            updateAdvancedSyncToggle();
            updateSyncToggle();
        }

        final ToggleManager this$0;

             {
                this$0 = ToggleManager.this;
                super();
            }
    };
    private final ContentObserver mVibrateEnableObserver;
    private final StateTracker mWifiState = new WifiStateTracker();

    static  {
        sToggleNames = new int[24];
        sToggleNames[16] = 0x60c0201;
        sToggleNames[19] = 0x60c0211;
        sToggleNames[22] = 0x60c0251;
        sToggleNames[2] = 0x60c0202;
        sToggleNames[4] = 0x60c0203;
        sToggleNames[1] = 0x60c0204;
        sToggleNames[0] = 0x60c0205;
        sToggleNames[21] = 0x60c024f;
        sToggleNames[9] = 0x60c0206;
        sToggleNames[7] = 0x60c0207;
        sToggleNames[10] = 0x60c0208;
        sToggleNames[5] = 0x60c0209;
        sToggleNames[17] = 0x60c020a;
        sToggleNames[23] = 0x60c0248;
        sToggleNames[14] = 0x60c020b;
        sToggleNames[12] = 0x60c020c;
        sToggleNames[3] = 0x60c020d;
        sToggleNames[20] = 0x60c020e;
        sToggleNames[18] = 0x60c020f;
        sToggleNames[13] = 0x60c0210;
        sToggleNames[8] = 0x60c0211;
        sToggleNames[11] = 0x60c0212;
        sToggleNames[6] = 0x60c0213;
        sToggleNames[15] = 0x60c0214;
        sLongClickActions = new int[24];
        sLongClickActions[19] = 0x60c021a;
        sLongClickActions[22] = 0x60c021e;
        sLongClickActions[1] = 0x60c021b;
        sLongClickActions[0] = 0x60c021c;
        sLongClickActions[21] = 0x60c0250;
        sLongClickActions[2] = 0x60c021d;
        sLongClickActions[4] = 0x60c021e;
        sLongClickActions[9] = 0x60c021f;
        sLongClickActions[7] = 0x60c0220;
        sLongClickActions[5] = 0x60c0221;
        sLongClickActions[17] = 0x60c0222;
        sLongClickActions[23] = 0x60c0247;
        sLongClickActions[3] = 0x60c0223;
        sLongClickActions[8] = 0x60c0224;
        sLongClickActions[6] = 0x60c0225;
        sLongClickActions[15] = 0x60c0226;
        sToggleImages = new int[24];
        sToggleImages[16] = 0x60201c9;
        sToggleImages[19] = 0x60201fa;
        sToggleImages[22] = 0x60201ce;
        sToggleImages[2] = 0x60201cd;
        sToggleImages[4] = 0x60201d2;
        sToggleImages[1] = 0x60201d4;
        sToggleImages[0] = 0x60201d5;
        sToggleImages[21] = 0x60202d4;
        sToggleImages[9] = 0x60201d9;
        sToggleImages[7] = 0x60201db;
        sToggleImages[10] = 0x60201dc;
        sToggleImages[5] = 0x60201de;
        sToggleImages[17] = 0x60201df;
        sToggleImages[23] = 0x60201ec;
        sToggleImages[14] = 0x60201e1;
        sToggleImages[12] = 0x60201e2;
        sToggleImages[3] = 0x60201e5;
        sToggleImages[20] = 0x60201e6;
        sToggleImages[18] = 0x60201e8;
        sToggleImages[13] = 0x60201ea;
        sToggleImages[8] = 0x60201fa;
        sToggleImages[11] = 0x60201d6;
        sToggleImages[6] = 0x60201e9;
        sToggleImages[15] = 0x60201ec;
        sToggleGeneralImages = new int[24];
        for(int i = 0; i < 24; i++)
            sToggleGeneralImages[i] = sToggleImages[i];

        sToggleAlpha = new int[24];
        int ai[] = sToggleAlpha;
        int ai1[] = sToggleAlpha;
        int ai2[] = sToggleAlpha;
        int ai3[] = sToggleAlpha;
        int ai4[] = sToggleAlpha;
        int ai5[] = sToggleAlpha;
        int ai6[] = sToggleAlpha;
        int ai7[] = sToggleAlpha;
        int ai8[] = sToggleAlpha;
        int ai9[] = sToggleAlpha;
        int ai10[] = sToggleAlpha;
        int ai11[] = sToggleAlpha;
        int ai12[] = sToggleAlpha;
        int ai13[] = sToggleAlpha;
        int ai14[] = sToggleAlpha;
        int ai15[] = sToggleAlpha;
        int ai16[] = sToggleAlpha;
        int ai17[] = sToggleAlpha;
        int ai18[] = sToggleAlpha;
        int ai19[] = sToggleAlpha;
        int ai20[] = sToggleAlpha;
        int ai21[] = sToggleAlpha;
        int ai22[] = sToggleAlpha;
        sToggleAlpha[15] = 255;
        ai22[6] = 255;
        ai21[11] = 255;
        ai20[8] = 255;
        ai19[13] = 255;
        ai18[18] = 255;
        ai17[20] = 255;
        ai16[3] = 255;
        ai15[12] = 255;
        ai14[14] = 255;
        ai13[23] = 255;
        ai12[17] = 255;
        ai11[5] = 255;
        ai10[10] = 255;
        ai9[7] = 255;
        ai8[9] = 255;
        ai7[21] = 255;
        ai6[0] = 255;
        ai5[1] = 255;
        ai4[4] = 255;
        ai3[2] = 255;
        ai2[22] = 255;
        ai1[19] = 255;
        ai[16] = 255;
        MINIMUM_BACKLIGHT = Resources.getSystem().getInteger(0x6080005);
        RANGE = 255 - MINIMUM_BACKLIGHT;
    }
















/*
    static boolean access$2102(ToggleManager togglemanager, boolean flag) {
        togglemanager.mPrivacyMode = flag;
        return flag;
    }

*/






/*
    static boolean access$602(ToggleManager togglemanager, boolean flag) {
        togglemanager.mMobilePolicyEnable = flag;
        return flag;
    }

*/



}
