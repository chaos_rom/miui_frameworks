// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.format.DateUtils;
import java.util.Calendar;
import miui.widget.DateTimePicker;

public class DateTimePickerDialog extends AlertDialog
    implements android.content.DialogInterface.OnClickListener {
    public static interface OnDateTimeSetListener {

        public abstract void OnDateTimeSet(AlertDialog alertdialog, long l);
    }


    public DateTimePickerDialog(Context context, long l) {
        super(context);
        mDateTimePicker = new DateTimePicker(context);
        setView(mDateTimePicker);
        mDate = Calendar.getInstance();
        mDateTimePicker.setOnDateTimeChangedListener(new miui.widget.DateTimePicker.OnDateTimeChangedListener() {

            public void onDateTimeChanged(DateTimePicker datetimepicker, int i, int j, int k, int i1, int j1) {
                mDate.set(1, i);
                mDate.set(2, j);
                mDate.set(5, k);
                mDate.set(11, i1);
                mDate.set(12, j1);
                updateTitle(mDate.getTimeInMillis());
            }

            final DateTimePickerDialog this$0;

             {
                this$0 = DateTimePickerDialog.this;
                super();
            }
        });
        mDate.setTimeInMillis(l);
        mDateTimePicker.setCurrentDate(l);
        setButton(context.getString(0x104000a), this);
        setButton2(context.getString(0x1040000), (android.content.DialogInterface.OnClickListener)null);
        updateTitle(l);
    }

    private void updateTitle(long l) {
        char c;
        int i;
        if(mDateTimePicker.is24HourView())
            c = '\200';
        else
            c = '@';
        i = 0x15 | c;
        setTitle(DateUtils.formatDateTime(getContext(), l, i));
    }

    public void onClick(DialogInterface dialoginterface, int i) {
        if(mOnDateTimeSetListener != null)
            mOnDateTimeSetListener.OnDateTimeSet(this, mDate.getTimeInMillis());
    }

    public void setOnDateTimeSetListener(OnDateTimeSetListener ondatetimesetlistener) {
        mOnDateTimeSetListener = ondatetimesetlistener;
    }

    private Calendar mDate;
    private DateTimePicker mDateTimePicker;
    private OnDateTimeSetListener mOnDateTimeSetListener;


}
