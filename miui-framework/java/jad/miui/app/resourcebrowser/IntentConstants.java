// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser;


public interface IntentConstants {
    public static interface ThemeWidget {

        public static final String ACTION_SELECT_THEME_RESOURCE = "android.intent.action.PICK_RESOURCE";
        public static final String CLOCK_COMPONENT_NAME = "clock";
        public static final String PHOTO_FRAME_COMPONENT_NAME = "photo_frame";
        public static final String RETURN_SELECT_THEME_PATH = "miui.app.resourcebrowser.PICKED_RESOURCE";
        public static final String SHOW_COMPONENT_NAME = "android.intent.extra.SHOW_COMPONENT_NAME";
        public static final String SHOW_COMPONENT_SIZE = "android.intent.extra.SHOW_COMPONENT_SIZE";
        public static final String WIDGET_SIZE_1x2 = "1x2";
        public static final String WIDGET_SIZE_2x2 = "2x2";
        public static final String WIDGET_SIZE_2x4 = "2x4";
        public static final String WIDGET_SIZE_4x4 = "4x4";
        public static final String sClockPath = "clock_";
        public static final String sPhotoFramePath = "photoframe_";
    }


    public static final String ACTION_PICK_RESOURCE = "android.intent.action.PICK_RESOURCE";
    public static final int AUDIO_RESOURCE = 2;
    public static final String CACHE_LIST_FOLDER = "miui.app.resourcebrowser.CACHE_LIST_FOLDER";
    public static final String CATEGORY_SUPPORTED = "miui.app.resourcebrowser.CATEGORY_SUPPORTED";
    public static final String CURRENT_USING_PATH = "miui.app.resourcebrowser.CURRENT_USING_PATH";
    public static final String CUSTOM_FLAG = "miui.app.resourcebrowser.CUSTOM_FLAG";
    public static final String DETAIL_ACTIVITY_CLASS = "miui.app.resourcebrowser.DETAIL_ACTIVITY_CLASS";
    public static final String DETAIL_ACTIVITY_PACKAGE = "miui.app.resourcebrowser.DETAIL_ACTIVITY_PACKAGE";
    public static final int DISPLAY_DOUBLE_FLAT = 6;
    public static final int DISPLAY_DOUBLE_FLAT_FONT = 11;
    public static final int DISPLAY_DOUBLE_FLAT_ICON = 10;
    public static final int DISPLAY_SINGLE = 1;
    public static final int DISPLAY_SINGLE_DETAIL = 3;
    public static final int DISPLAY_SINGLE_GALLERY = 4;
    public static final int DISPLAY_SINGLE_MUSIC = 5;
    public static final int DISPLAY_SINGLE_SMALL = 2;
    public static final int DISPLAY_TRIPLE = 7;
    public static final int DISPLAY_TRIPLE_FLAT = 8;
    public static final int DISPLAY_TRIPLE_TEXT = 9;
    public static final String DISPLAY_TYPE = "miui.app.resourcebrowser.DISPLAY_TYPE";
    public static final String DOWNLOAD_FOLDER = "miui.app.resourcebrowser.DOWNLOAD_FOLDER";
    public static final String DOWNLOAD_FOLDER_EXTRA = "miui.app.resourcebrowser.DOWNLOAD_FOLDER_EXTRA";
    public static final int IMAGE_RESOURCE = 1;
    public static final String IS_RECOMMENDATION_LIST = "miui.app.resourcebrowser.IS_RECOMMENDATION_LIST";
    public static final String LIST_URL = "miui.app.resourcebrowser.LIST_URL";
    public static final String LOCAL_LIST_ACTIVITY_CLASS = "miui.app.resourcebrowser.LOCAL_LIST_ACTIVITY_CLASS";
    public static final String LOCAL_LIST_ACTIVITY_PACKAGE = "miui.app.resourcebrowser.LOCAL_LIST_ACTIVITY_PACKAGE";
    public static final String META_DATA = "META_DATA";
    public static final String META_DATA_FOR_LOCAL = "META_DATA_FOR_LOCAL";
    public static final String META_DATA_FOR_ONLINE = "META_DATA_FOR_ONLINE";
    public static final String ONLINE_LIST_ACTIVITY_CLASS = "miui.app.resourcebrowser.ONLINE_LIST_ACTIVITY_CLASS";
    public static final String ONLINE_LIST_ACTIVITY_PACKAGE = "miui.app.resourcebrowser.ONLINE_LIST_ACTIVITY_PACKAGE";
    public static final String PICKED_RESOURCE = "miui.app.resourcebrowser.PICKED_RESOURCE";
    public static final String PLATFORM_VERSION_END = "miui.app.resourcebrowser.PLATFORM_VERSION_END";
    public static final String PLATFORM_VERSION_START = "miui.app.resourcebrowser.PLATFORM_VERSION_START";
    public static final String PLATFORM_VERSION_SUPPORTED = "miui.app.resourcebrowser.PLATFORM_VERSION_SUPPORTED";
    public static final String PREVIEW_PREFIX = "miui.app.resourcebrowser.PREVIEW_PREFIX";
    public static final String PREVIEW_PREFIX_INDICATOR = "miui.app.resourcebrowser.PREVIEW_PREFIX_INDICATOR";
    public static final String PREVIEW_WIDTH = "miui.app.resourcebrowser.PREVIEW_WIDTH";
    public static final String RECOMMENDATION_ID = "miui.app.resourcebrowser.RECOMMENDATION_ID";
    public static final String RECOMMENDATION_LIST_ACTIVITY_CLASS = "miui.app.resourcebrowser.RECOMMENDATION_LIST_ACTIVITY_CLASS";
    public static final String RECOMMENDATION_LIST_ACTIVITY_PACKAGE = "miui.app.resourcebrowser.RECOMMENDATION_LIST_ACTIVITY_PACKAGE";
    public static final String RECOMMENDATION_WIDTH = "miui.app.resourcebrowser.RECOMMENDATION_WIDTH";
    public static final int REQUEST_CODE = 1;
    public static final String RESOURCE_FILE_EXTENSION = "miui.app.resourcebrowser.RESOURCE_FILE_EXTENSION";
    public static final String RESOURCE_GROUP = "miui.app.resourcebrowser.RESOURCE_GROUP";
    public static final String RESOURCE_INDEX = "miui.app.resourcebrowser.RESOURCE_INDEX";
    public static final String RESOURCE_SET_CATEGORY = "miui.app.resourcebrowser.RESOURCE_SET_CATEGORY";
    public static final String RESOURCE_SET_CODE = "miui.app.resourcebrowser.RESOURCE_SET_CODE";
    public static final String RESOURCE_SET_NAME = "miui.app.resourcebrowser.RESOURCE_SET_NAME";
    public static final String RESOURCE_SET_PACKAGE = "miui.app.resourcebrowser.RESOURCE_SET_PACKAGE";
    public static final String RESOURCE_SET_SUBPACKAGE = "miui.app.resourcebrowser.RESOURCE_SET_SUBPACKAGE";
    public static final String RESOURCE_SET_SUBPACKAGE_LOCAL = ".local";
    public static final String RESOURCE_SET_SUBPACKAGE_ONLINE = ".online";
    public static final String RESOURCE_SET_SUBPACKAGE_SEARCH = ".search";
    public static final String RESOURCE_SET_SUBPACKAGE_SINGLE = ".single";
    public static final String RESOURCE_TYPE_PARAMETER = "miui.app.resourcebrowser.RESOURCE_TYPE_PARAMETER";
    public static final String RINGTONE_MAX_DURATION_LIMIT = "miui.app.resourcebrowser.RINGTONE_MAX_DURATION_LIMIT";
    public static final String RINGTONE_MIN_DURATION_LIMIT = "miui.app.resourcebrowser.RINGTONE_MIN_DURATION_LIMIT";
    public static final String SHOW_RINGTONE_NAME = "miui.app.resourcebrowser.SHOW_RINGTONE_NAME";
    public static final String SOURCE_FOLDERS = "miui.app.resourcebrowser.SOURCE_FOLDERS";
    public static final String SUB_RECOMMENDS = "miui.app.resourcebrowser.SUB_RECOMMENDATIONS";
    public static final String THUMBNAIL_PREFIX = "miui.app.resourcebrowser.THUMBNAIL_PREFIX";
    public static final String THUMBNAIL_PREFIX_INDICATOR = "miui.app.resourcebrowser.THUMBNAIL_PREFIX_INDICATOR";
    public static final String THUMBNAIL_WIDTH = "miui.app.resourcebrowser.THUMBNAIL_WIDTH";
    public static final String TRACK_ID = "miui.app.resourcebrowser.TRACK_ID";
    public static final String USING_PICKER = "miui.app.resourcebrowser.USING_PICKER";
    public static final String VERSION_SUPPORTED = "miui.app.resourcebrowser.VERSION_SUPPORTED";
    public static final int ZIP_RESOURCE;
}
