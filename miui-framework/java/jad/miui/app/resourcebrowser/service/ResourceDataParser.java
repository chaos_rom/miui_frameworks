// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser.service;

import android.os.Bundle;
import java.io.File;
import java.util.List;
import miui.app.resourcebrowser.resource.ListMetaData;
import miui.app.resourcebrowser.resource.Resource;

public abstract class ResourceDataParser {

    public ResourceDataParser() {
    }

    public abstract List buildCategories(File file, Bundle bundle) throws Exception;

    public abstract ListMetaData buildListMetaData(File file, Bundle bundle) throws Exception;

    public abstract List buildRecommendations(File file, Bundle bundle) throws Exception;

    public abstract Resource buildResource(File file, Bundle bundle) throws Exception;

    public abstract List buildResources(File file, Bundle bundle) throws Exception;

    public abstract List buildUpdatableResources(File file, Bundle bundle) throws Exception;

    public List readCategories(String s, Bundle bundle) {
        File file = new File(s);
        if(!file.exists()) goto _L2; else goto _L1
_L1:
        List list1 = buildCategories(file, bundle);
        List list = list1;
_L4:
        return list;
        Exception exception;
        exception;
        file.delete();
_L2:
        list = null;
        if(true) goto _L4; else goto _L3
_L3:
    }

    public ListMetaData readListMetaData(String s, Bundle bundle) {
        File file = new File(s);
        if(!file.exists()) goto _L2; else goto _L1
_L1:
        ListMetaData listmetadata1 = buildListMetaData(file, bundle);
        ListMetaData listmetadata = listmetadata1;
_L4:
        return listmetadata;
        Exception exception;
        exception;
        file.delete();
_L2:
        listmetadata = null;
        if(true) goto _L4; else goto _L3
_L3:
    }

    public List readRecommendations(String s, Bundle bundle) {
        File file = new File(s);
        if(!file.exists()) goto _L2; else goto _L1
_L1:
        List list1 = buildRecommendations(file, bundle);
        List list = list1;
_L4:
        return list;
        Exception exception;
        exception;
        file.delete();
_L2:
        list = null;
        if(true) goto _L4; else goto _L3
_L3:
    }

    public Resource readResource(String s, Bundle bundle) {
        File file = new File(s);
        if(!file.exists()) goto _L2; else goto _L1
_L1:
        Resource resource1 = buildResource(file, bundle);
        Resource resource = resource1;
_L4:
        return resource;
        Exception exception;
        exception;
        file.delete();
_L2:
        resource = null;
        if(true) goto _L4; else goto _L3
_L3:
    }

    public List readResources(String s, Bundle bundle) {
        File file = new File(s);
        if(!file.exists()) goto _L2; else goto _L1
_L1:
        List list1 = buildResources(file, bundle);
        List list = list1;
_L4:
        return list;
        Exception exception;
        exception;
        file.delete();
_L2:
        list = null;
        if(true) goto _L4; else goto _L3
_L3:
    }

    public List readUpdatableResources(String s, Bundle bundle) {
        File file = new File(s);
        if(!file.exists()) goto _L2; else goto _L1
_L1:
        List list1 = buildUpdatableResources(file, bundle);
        List list = list1;
_L4:
        return list;
        Exception exception;
        exception;
        file.delete();
_L2:
        list = null;
        if(true) goto _L4; else goto _L3
_L3:
    }

    protected static final int BUFFER_SIZE = 1024;
}
