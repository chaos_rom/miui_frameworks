// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser.service;

import android.os.Bundle;
import android.text.TextUtils;
import java.io.*;
import java.util.*;
import miui.app.resourcebrowser.IntentConstants;
import miui.app.resourcebrowser.ResourceConstants;
import miui.app.resourcebrowser.recommended.RecommendItemData;
import miui.app.resourcebrowser.resource.*;
import miui.app.resourcebrowser.service.online.OnlineProtocolConstants;
import miui.app.resourcebrowser.service.online.OnlineService;
import miui.app.resourcebrowser.util.ResourceHelper;
import miui.os.ExtraFileUtils;
import org.json.*;

// Referenced classes of package miui.app.resourcebrowser.service:
//            ResourceDataParser

public class ResourceJSONDataParser extends ResourceDataParser
    implements IntentConstants, OnlineProtocolConstants {

    protected ResourceJSONDataParser() {
    }

    public static ResourceJSONDataParser getInstance() {
        if(mInstance != null) goto _L2; else goto _L1
_L1:
        miui/app/resourcebrowser/service/ResourceJSONDataParser;
        JVM INSTR monitorenter ;
        if(mInstance == null)
            mInstance = new ResourceJSONDataParser();
        miui/app/resourcebrowser/service/ResourceJSONDataParser;
        JVM INSTR monitorexit ;
_L2:
        return mInstance;
        Exception exception;
        exception;
        miui/app/resourcebrowser/service/ResourceJSONDataParser;
        JVM INSTR monitorexit ;
        throw exception;
    }

    private String getJSONInformation(File file) throws IOException, JSONException {
        BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(new FileInputStream(file)), 1024);
        StringBuffer stringbuffer = new StringBuffer();
        for(String s = bufferedreader.readLine(); s != null; s = bufferedreader.readLine())
            stringbuffer.append(s);

        return stringbuffer.toString();
    }

    public List buildCategories(File file, Bundle bundle) throws IOException, JSONException {
        ArrayList arraylist = new ArrayList();
        JSONObject jsonobject = new JSONObject(getJSONInformation(file));
        JSONObject jsonobject1 = jsonobject.getJSONObject("clazzNameMap");
        JSONArray jsonarray = jsonobject.getJSONArray("sortList");
        for(int i = 0; i < jsonarray.length(); i++) {
            ResourceCategory resourcecategory = new ResourceCategory();
            String s = jsonarray.getString(i);
            resourcecategory.setCode(s);
            resourcecategory.setName(jsonobject1.getString(s));
            arraylist.add(resourcecategory);
        }

        return arraylist;
    }

    public ListMetaData buildListMetaData(File file, Bundle bundle) throws IOException, JSONException {
        ListMetaData listmetadata = new ListMetaData();
        JSONObject jsonobject = new JSONObject(getJSONInformation(file));
        listmetadata.setName(jsonobject.optString("name"));
        listmetadata.setDescription(jsonobject.optString("description"));
        listmetadata.setCategory(jsonobject.optString("category"));
        return listmetadata;
    }

    public List buildRecommendations(File file, Bundle bundle) throws IOException, JSONException {
        ArrayList arraylist = new ArrayList();
        JSONObject jsonobject = new JSONObject(getJSONInformation(file));
        String s = bundle.getString("miui.app.resourcebrowser.RESOURCE_SET_CODE");
        String s1 = ExtraFileUtils.standardizeFolderPath((new StringBuilder()).append(ResourceConstants.RECOMMENDATION_PATH).append(s).toString());
        Object aobj[] = new Object[1];
        aobj[0] = String.valueOf(bundle.getInt("miui.app.resourcebrowser.RECOMMENDATION_WIDTH"));
        String s2 = String.format("jpeg/w%s/", aobj);
        JSONArray jsonarray = jsonobject.getJSONArray("recommendations");
        for(int i = 0; i < jsonarray.length(); i++) {
            RecommendItemData recommenditemdata = new RecommendItemData();
            JSONObject jsonobject1 = jsonarray.getJSONObject(i);
            String s3 = jsonobject1.getString("picUrlRoot");
            recommenditemdata.onlineThumbnail = (new StringBuilder()).append(s3).append(s2).append(jsonobject1.getString("picUrl")).toString();
            recommenditemdata.localThumbnail = ResourceHelper.getFilePathByURL(s1, recommenditemdata.onlineThumbnail);
            recommenditemdata.itemType = jsonobject1.getInt("type");
            recommenditemdata.itemId = jsonobject1.getString("relatedId");
            recommenditemdata.title = jsonobject1.getString("title");
            recommenditemdata.widthCount = jsonobject1.getInt("layoutCol");
            recommenditemdata.heightCount = jsonobject1.getInt("layoutRow");
            recommenditemdata.type = jsonobject.getString("category");
            JSONArray jsonarray1 = jsonobject1.getJSONArray("subjects");
            for(int j = 0; j < jsonarray1.length(); j++) {
                RecommendItemData recommenditemdata1 = new RecommendItemData();
                JSONObject jsonobject2 = jsonarray1.getJSONObject(j);
                recommenditemdata1.itemId = jsonobject2.getString("id");
                recommenditemdata1.title = jsonobject2.getString("name");
                recommenditemdata.subItems.add(recommenditemdata1);
            }

            arraylist.add(recommenditemdata);
        }

        return arraylist;
    }

    public Resource buildResource(File file, Bundle bundle) throws IOException, JSONException {
        Resource resource = new Resource();
        resource.setInformation(buildResourceInformation(new JSONObject(getJSONInformation(file)), bundle));
        return resource;
    }

    protected Bundle buildResourceInformation(JSONObject jsonobject, Bundle bundle) throws JSONException {
        Bundle bundle1;
        String s1;
        String s2;
        String s3;
        String s4;
        String s5;
        String s6;
        String s7;
        String s8;
        String s9;
        String s10;
        bundle1 = new Bundle();
        for(Iterator iterator = jsonobject.keys(); iterator.hasNext();) {
            String s13 = (String)iterator.next();
            try {
                bundle1.putSerializable(s13, (Serializable)jsonobject.get(s13));
            }
            catch(ClassCastException classcastexception) { }
        }

        String s = bundle.getString("miui.app.resourcebrowser.RESOURCE_SET_CODE");
        s1 = bundle.getString("miui.app.resourcebrowser.RESOURCE_FILE_EXTENSION");
        s2 = jsonobject.getString("downloadUrlRoot");
        s3 = ExtraFileUtils.standardizeFolderPath(bundle.getString("miui.app.resourcebrowser.DOWNLOAD_FOLDER"));
        s4 = ExtraFileUtils.standardizeFolderPath(bundle.getString("miui.app.resourcebrowser.DOWNLOAD_FOLDER_EXTRA"));
        s5 = ExtraFileUtils.standardizeFolderPath((new StringBuilder()).append(ResourceConstants.THUMBNAIL_PATH).append(s).toString());
        s6 = ExtraFileUtils.standardizeFolderPath((new StringBuilder()).append(ResourceConstants.PREVIEW_PATH).append(s).toString());
        Object aobj[] = new Object[1];
        aobj[0] = String.valueOf(bundle.getInt("miui.app.resourcebrowser.THUMBNAIL_WIDTH"));
        s7 = String.format("jpeg/w%s/", aobj);
        Object aobj1[] = new Object[1];
        aobj1[0] = String.valueOf(bundle.getInt("miui.app.resourcebrowser.PREVIEW_WIDTH"));
        s8 = String.format("jpeg/w%s/", aobj1);
        if(bundle.getInt("miui.app.resourcebrowser.RESOURCE_SET_CATEGORY") == 2)
            s7 = "";
        s9 = jsonobject.getString("moduleId");
        bundle1.putString("ID", s9);
        bundle1.putString("ONLINE_PATH", OnlineService.getInstance().getDownloadUrl(s9));
        s10 = (new StringBuilder()).append(s3).append(s9).append(s1).toString();
        if(!s1.equalsIgnoreCase(".mp3") && !s1.equalsIgnoreCase(".jpg") && !s1.equalsIgnoreCase(".ogg")) goto _L2; else goto _L1
_L1:
        String s11 = jsonobject.optString("name");
        if(!TextUtils.isEmpty(s11)) {
            if(s1.equalsIgnoreCase(".jpg"))
                s11 = (new StringBuilder()).append(s11).append("__").append(s9).toString();
            s10 = (new StringBuilder()).append(s3).append(s11).append(s1).toString();
        }
_L4:
        ArrayList arraylist1;
        bundle1.putString("LOCAL_PATH", s10);
        ArrayList arraylist = new ArrayList();
        String s12 = jsonobject.optString("frontCover");
        arraylist.add((new StringBuilder()).append(s2).append(s7).append(s12).toString());
        bundle1.putStringArrayList("ONLINE_THUMBNAIL", arraylist);
        int i = arraylist.size();
        arraylist1 = new ArrayList(i);
        for(int j = 0; j < i; j++)
            arraylist1.add((new StringBuilder()).append(s5).append(ResourceHelper.getFileName((String)arraylist.get(j))).toString());

        break; /* Loop/switch isn't completed */
_L2:
        if(!TextUtils.isEmpty(s4) && (new File((new StringBuilder()).append(s4).append(s9).append(s1).toString())).exists())
            s10 = (new StringBuilder()).append(s4).append(s9).append(s1).toString();
        if(true) goto _L4; else goto _L3
_L3:
        bundle1.putStringArrayList("LOCAL_THUMBNAIL", arraylist1);
        ArrayList arraylist2 = new ArrayList();
        JSONArray jsonarray = jsonobject.optJSONArray("snapshotsUrl");
        if(jsonarray != null) {
            for(int i1 = 0; i1 < jsonarray.length(); i1++)
                arraylist2.add((new StringBuilder()).append(s2).append(s8).append(jsonarray.getString(i1)).toString());

            bundle1.putStringArrayList("ONLINE_PREVIEW", arraylist2);
        }
        int k = arraylist2.size();
        ArrayList arraylist3 = new ArrayList(k);
        for(int l = 0; l < k; l++)
            arraylist3.add((new StringBuilder()).append(s6).append(ResourceHelper.getFileName((String)arraylist2.get(l))).toString());

        bundle1.putStringArrayList("LOCAL_PREVIEW", arraylist3);
        bundle1.putInt("STATUS", ResourceHelper.resolveResourceStatus(s10));
        bundle1.putInt("PLATFORM_VERSION", bundle.getInt("miui.app.resourcebrowser.PLATFORM_VERSION_END"));
        bundle1.putLong("CREATED_TIME", jsonobject.optLong("createTime"));
        bundle1.putLong("MODIFIED_TIME", jsonobject.optLong("modifyTime"));
        bundle1.putString("SIZE", String.valueOf(jsonobject.optLong("fileSize")));
        bundle1.putString("NAME", jsonobject.optString("name"));
        bundle1.putString("DESCRIPTION", jsonobject.optString("brief"));
        bundle1.putString("AUTHOR", jsonobject.optString("author"));
        bundle1.putString("DESIGNER", jsonobject.optString("designer"));
        bundle1.putString("VERSION", jsonobject.optString("version"));
        bundle1.putString("DOWNLOAD_COUNT", String.valueOf(jsonobject.optInt("downloads")));
        return bundle1;
    }

    public List buildResources(File file, Bundle bundle) throws IOException, JSONException {
        ArrayList arraylist = new ArrayList();
        JSONObject jsonobject = new JSONObject(getJSONInformation(file));
        JSONArray jsonarray = jsonobject.getJSONArray((String)jsonobject.keys().next());
        for(int i = 0; i < jsonarray.length(); i++) {
            Resource resource = new Resource();
            resource.setInformation(buildResourceInformation(jsonarray.getJSONObject(i), bundle));
            arraylist.add(resource);
        }

        return arraylist;
    }

    public List buildUpdatableResources(File file, Bundle bundle) throws IOException, JSONException {
        ArrayList arraylist = new ArrayList();
        JSONObject jsonobject = new JSONObject(getJSONInformation(file));
        Resource resource;
        for(Iterator iterator = jsonobject.keys(); iterator.hasNext(); arraylist.add(resource)) {
            String s = (String)iterator.next();
            String s1 = jsonobject.getString(s);
            resource = new Resource();
            resource.setFileHash(s);
            Bundle bundle1 = new Bundle();
            bundle1.putString("ID", s1);
            resource.setInformation(bundle1);
        }

        return arraylist;
    }

    private static ResourceJSONDataParser mInstance;
}
