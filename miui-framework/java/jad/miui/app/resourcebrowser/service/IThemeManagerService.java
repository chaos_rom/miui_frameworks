// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser.service;

import android.os.*;

public interface IThemeManagerService
    extends IInterface {
    public static abstract class Stub extends Binder
        implements IThemeManagerService {
        private static class Proxy
            implements IThemeManagerService {

            public IBinder asBinder() {
                return mRemote;
            }

            public String getInterfaceDescriptor() {
                return "miui.app.resourcebrowser.service.IThemeManagerService";
            }

            public boolean saveLockWallpaper(String s) throws RemoteException {
                boolean flag;
                Parcel parcel;
                Parcel parcel1;
                flag = true;
                parcel = Parcel.obtain();
                parcel1 = Parcel.obtain();
                int i;
                parcel.writeInterfaceToken("miui.app.resourcebrowser.service.IThemeManagerService");
                parcel.writeString(s);
                mRemote.transact(1, parcel, parcel1, 0);
                parcel1.readException();
                i = parcel1.readInt();
                if(i == 0)
                    flag = false;
                parcel1.recycle();
                parcel.recycle();
                return flag;
                Exception exception;
                exception;
                parcel1.recycle();
                parcel.recycle();
                throw exception;
            }

            private IBinder mRemote;

            Proxy(IBinder ibinder) {
                mRemote = ibinder;
            }
        }


        public static IThemeManagerService asInterface(IBinder ibinder) {
            Object obj;
            if(ibinder == null) {
                obj = null;
            } else {
                IInterface iinterface = ibinder.queryLocalInterface("miui.app.resourcebrowser.service.IThemeManagerService");
                if(iinterface != null && (iinterface instanceof IThemeManagerService))
                    obj = (IThemeManagerService)iinterface;
                else
                    obj = new Proxy(ibinder);
            }
            return ((IThemeManagerService) (obj));
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel1, int j) throws RemoteException {
            boolean flag = true;
            i;
            JVM INSTR lookupswitch 2: default 32
        //                       1: 55
        //                       1598968902: 46;
               goto _L1 _L2 _L3
_L1:
            flag = super.onTransact(i, parcel, parcel1, j);
_L5:
            return flag;
_L3:
            parcel1.writeString("miui.app.resourcebrowser.service.IThemeManagerService");
            continue; /* Loop/switch isn't completed */
_L2:
            parcel.enforceInterface("miui.app.resourcebrowser.service.IThemeManagerService");
            boolean flag1 = saveLockWallpaper(parcel.readString());
            parcel1.writeNoException();
            int k;
            if(flag1)
                k = ((flag) ? 1 : 0);
            else
                k = 0;
            parcel1.writeInt(k);
            if(true) goto _L5; else goto _L4
_L4:
        }

        private static final String DESCRIPTOR = "miui.app.resourcebrowser.service.IThemeManagerService";
        static final int TRANSACTION_saveLockWallpaper = 1;

        public Stub() {
            attachInterface(this, "miui.app.resourcebrowser.service.IThemeManagerService");
        }
    }


    public abstract boolean saveLockWallpaper(String s) throws RemoteException;
}
