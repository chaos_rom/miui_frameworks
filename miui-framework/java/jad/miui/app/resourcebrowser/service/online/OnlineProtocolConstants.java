// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser.service.online;


public interface OnlineProtocolConstants {

    public static final String ATTR_AUTHOR = "author";
    public static final String ATTR_CATEGORY_NAME = "clazzNameMap";
    public static final String ATTR_CATEGORY_ORDER = "sortList";
    public static final String ATTR_COMPONENT_FLAG = "modulesFlag";
    public static final String ATTR_COMPONENT_ID = "moduleId";
    public static final String ATTR_COMPONENT_TYPE = "moduleType";
    public static final String ATTR_CREATED_TIME = "createTime";
    public static final String ATTR_DESIGNER = "designer";
    public static final String ATTR_DOWNLOAD_COUNT = "downloads";
    public static final String ATTR_FILE_SIZE = "fileSize";
    public static final String ATTR_LAYOUT_COLUMN = "layoutCol";
    public static final String ATTR_LAYOUT_COLUMNS = "layoutCols";
    public static final String ATTR_LAYOUT_LEFT = "layoutLeft";
    public static final String ATTR_LAYOUT_ROW = "layoutRow";
    public static final String ATTR_LAYOUT_TOP = "layoutTop";
    public static final String ATTR_LONG_DESCRITION = "description";
    public static final String ATTR_MODIFIED_TIME = "modifyTime";
    public static final String ATTR_NAME = "name";
    public static final String ATTR_PLATFORM_VERSION = "uiVersion";
    public static final String ATTR_PREVIEWS = "snapshotsUrl";
    public static final String ATTR_RECOMMENDATIONS = "recommendations";
    public static final String ATTR_RECOMMENDATION_ID = "relatedId";
    public static final String ATTR_RECOMMENDATION_SUBJECTS = "subjects";
    public static final String ATTR_RECOMMENDATION_THUMBNAIL = "picUrl";
    public static final String ATTR_RECOMMENDATION_TITLE = "title";
    public static final String ATTR_RECOMMENDATION_TYPE = "type";
    public static final String ATTR_RECOMMENDATION_URL_ROOT = "picUrlRoot";
    public static final String ATTR_RESOURCE_ID = "assemblyId";
    public static final String ATTR_SHORT_DESCRIPTION = "brief";
    public static final String ATTR_STAR = "star";
    public static final String ATTR_STAR_DETAIL = "starCount";
    public static final String ATTR_SUB_RECOMMENDATION_ID = "id";
    public static final String ATTR_SUB_RECOMMENDATION_THUMBNAIL = "picture";
    public static final String ATTR_SUB_RECOMMENDATION_TITLE = "name";
    public static final String ATTR_SUB_RECOMMENDATION_URL_ROOT = "picUrlRoot";
    public static final String ATTR_TAG = "tags";
    public static final String ATTR_THUMBNAILS = "frontCover";
    public static final String ATTR_TYPE = "category";
    public static final String ATTR_URL_ROOT = "downloadUrlRoot";
    public static final String ATTR_VERSION = "version";
    public static final String PARAM_CATEGORY_CODE = "clazz";
    public static final String PARAM_COUNT = "count";
    public static final String PARAM_DEVICE = "device";
    public static final String PARAM_HASH = "fileshash";
    public static final String PARAM_KEYWORD = "keywords";
    public static final String PARAM_PLATFORM_VERSION = "version";
    public static final String PARAM_ROM = "system";
    public static final String PARAM_SORT_BY = "sortby";
    public static final String PARAM_START = "start";
    public static final String PARAM_TYPE = "category";
    public static final String URL_CATEGORY = "http://market.xiaomi.com/thm/config/clazz/%s/zh-cn?";
    public static final String URL_COMMON_LIST = "http://market.xiaomi.com/thm/subject/index?category=%s";
    public static final String URL_DETAIL = "http://market.xiaomi.com/thm/details/%s?";
    public static final String URL_DOWNLOAD = "http://market.xiaomi.com/thm/download/%s?";
    public static final String URL_LIST_METADATA = "http://market.xiaomi.com/thm/subject/metadata/%s?";
    public static final String URL_PART_CATEGORY = "&clazz=%s";
    public static final String URL_PART_IMAGE = "jpeg/w%s/";
    public static final String URL_PART_PAGINATION = "&start=%s&count=%s";
    public static final String URL_PREFIX = "http://market.xiaomi.com/thm/";
    public static final String URL_RECOMMENDATION = "http://market.xiaomi.com/thm/recommendation/list/%s?";
    public static final String URL_RECOMMENDATION_LIST = "http://market.xiaomi.com/thm/subject/%s?category=%s";
    public static final String URL_SEARCH_LIST = "http://market.xiaomi.com/thm/search?apiversion=1&keywords=%s&category=%s";
    public static final String URL_SUFFIX_USER = "&imei=%s&user=%s&token=%s";
    public static final String URL_SUFFIX_VERSION = "&device=%s&system=%s&version=%s&imei=%s";
    public static final String URL_UPDATE = "http://market.xiaomi.com/thm/checkupdate?fileshash=%s";
}
