// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser.service.online;

import android.accounts.AccountManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import java.io.*;
import java.net.*;
import miui.app.resourcebrowser.IntentConstants;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;

// Referenced classes of package miui.app.resourcebrowser.service.online:
//            OnlineProtocolConstants

public class OnlineService
    implements IntentConstants, OnlineProtocolConstants {

    private OnlineService() {
    }

    private String getDevice() {
        return Build.DEVICE;
    }

    private String getImei() {
        String s = getLongImei();
        if(!TextUtils.isEmpty(s)) {
            int i = s.length();
            if(i >= 9)
                s = s.substring(i - 9, i);
        }
        if(TextUtils.isEmpty(s))
            s = "";
        return s;
    }

    public static OnlineService getInstance() {
        if(mInstance != null) goto _L2; else goto _L1
_L1:
        miui/app/resourcebrowser/service/online/OnlineService;
        JVM INSTR monitorenter ;
        if(mInstance == null)
            mInstance = new OnlineService();
        miui/app/resourcebrowser/service/online/OnlineService;
        JVM INSTR monitorexit ;
_L2:
        return mInstance;
        Exception exception;
        exception;
        miui/app/resourcebrowser/service/online/OnlineService;
        JVM INSTR monitorexit ;
        throw exception;
    }

    private String getLongImei() {
        String s = TelephonyManager.getDefault().getDeviceId();
        if(TextUtils.isEmpty(s))
            s = "";
        return s;
    }

    private String getType() {
        return "miui";
    }

    private String getUserId(Context context) {
        return android.provider.Settings.Secure.getString(context.getContentResolver(), "android_id");
    }

    private String getUserSuffix(Context context) {
        if(USER_SUFFIX == null)
            try {
                Object aobj[] = new Object[3];
                aobj[0] = getImei();
                aobj[1] = getUserId(context);
                aobj[2] = getUserToken(context);
                USER_SUFFIX = String.format("&imei=%s&user=%s&token=%s", aobj);
            }
            catch(Exception exception) {
                exception.printStackTrace();
            }
        return USER_SUFFIX;
    }

    private String getUserToken(Context context) {
        AccountManager accountmanager;
        android.accounts.Account aaccount[];
        String s;
        accountmanager = AccountManager.get(context);
        aaccount = accountmanager.getAccountsByType("com.miui.auth");
        s = null;
        if(aaccount.length <= 0)
            break MISSING_BLOCK_LABEL_49;
        String s2;
        String s1 = accountmanager.getUserData(aaccount[0], "token");
        if(s1 == null)
            break MISSING_BLOCK_LABEL_49;
        s2 = URLEncoder.encode(s1, "UTF-8");
        s = s2;
_L2:
        return s;
        UnsupportedEncodingException unsupportedencodingexception;
        unsupportedencodingexception;
        unsupportedencodingexception.printStackTrace();
        if(true) goto _L2; else goto _L1
_L1:
    }

    private String getVersion() {
        return (new StringBuilder()).append(android.os.Build.VERSION.RELEASE).append("_").append(android.os.Build.VERSION.INCREMENTAL).toString();
    }

    private String getVersionSuffix() {
        if(VERSION_SUFFIX == null)
            try {
                Object aobj[] = new Object[4];
                aobj[0] = getDevice();
                aobj[1] = getType();
                aobj[2] = getVersion();
                aobj[3] = getLongImei();
                VERSION_SUFFIX = String.format("&device=%s&system=%s&version=%s&imei=%s", aobj);
            }
            catch(Exception exception) {
                exception.printStackTrace();
            }
        return VERSION_SUFFIX;
    }

    public String getCategoryUrl(Bundle bundle) {
        Object obj = null;
        bundle.getInt("miui.app.resourcebrowser.RESOURCE_SET_CATEGORY");
        JVM INSTR tableswitch 0 2: default 36
    //                   0 76
    //                   1 82
    //                   2 88;
           goto _L1 _L2 _L3 _L4
_L1:
        Object aobj[] = new Object[1];
        aobj[0] = obj;
        String s = String.format("http://market.xiaomi.com/thm/config/clazz/%s/zh-cn?", aobj);
        return (new StringBuilder()).append(s).append(getVersionSuffix()).toString();
_L2:
        obj = "compound";
        continue; /* Loop/switch isn't completed */
_L3:
        obj = "wallpaper";
        continue; /* Loop/switch isn't completed */
_L4:
        obj = "ringtone";
        if(true) goto _L1; else goto _L5
_L5:
    }

    public String getCommonListUrl(Bundle bundle, String s) {
        String s1 = bundle.getString("miui.app.resourcebrowser.RESOURCE_TYPE_PARAMETER");
        Object aobj[] = new Object[1];
        aobj[0] = s1;
        String s2 = String.format("http://market.xiaomi.com/thm/subject/index?category=%s", aobj);
        if(!TextUtils.isEmpty(s)) {
            StringBuilder stringbuilder = (new StringBuilder()).append(s2);
            Object aobj1[] = new Object[1];
            aobj1[0] = s;
            s2 = stringbuilder.append(String.format("&clazz=%s", aobj1)).toString();
        }
        return (new StringBuilder()).append(s2).append(getVersionSuffix()).toString();
    }

    public String getDetailUrl(String s) {
        Object aobj[] = new Object[1];
        aobj[0] = s;
        String s1 = String.format("http://market.xiaomi.com/thm/details/%s?", aobj);
        return (new StringBuilder()).append(s1).append(getVersionSuffix()).toString();
    }

    public String getDownloadUrl(String s) {
        Object aobj[] = new Object[1];
        aobj[0] = s;
        String s1 = String.format("http://market.xiaomi.com/thm/download/%s?", aobj);
        return (new StringBuilder()).append(s1).append(getVersionSuffix()).toString();
    }

    public String getListMetaDataUrl(Bundle bundle, String s) {
        Object aobj[] = new Object[1];
        aobj[0] = s;
        String s1 = String.format("http://market.xiaomi.com/thm/subject/metadata/%s?", aobj);
        return (new StringBuilder()).append(s1).append(getVersionSuffix()).toString();
    }

    public String getRecommendationListUrl(Bundle bundle, String s, String s1) {
        String s2 = bundle.getString("miui.app.resourcebrowser.RESOURCE_TYPE_PARAMETER");
        Object aobj[] = new Object[2];
        aobj[0] = s;
        aobj[1] = s2;
        String s3 = String.format("http://market.xiaomi.com/thm/subject/%s?category=%s", aobj);
        if(!TextUtils.isEmpty(s1)) {
            StringBuilder stringbuilder = (new StringBuilder()).append(s3);
            Object aobj1[] = new Object[1];
            aobj1[0] = s1;
            s3 = stringbuilder.append(String.format("&clazz=%s", aobj1)).toString();
        }
        return (new StringBuilder()).append(s3).append(getVersionSuffix()).toString();
    }

    public String getRecommendationUrl(Bundle bundle) {
        String s = bundle.getString("miui.app.resourcebrowser.RESOURCE_TYPE_PARAMETER");
        Object aobj[] = new Object[1];
        aobj[0] = s;
        String s1 = String.format("http://market.xiaomi.com/thm/recommendation/list/%s?", aobj);
        return (new StringBuilder()).append(s1).append(getVersionSuffix()).toString();
    }

    public String getSearchListUrl(Bundle bundle, String s) {
        String s1;
        String s2;
        s1 = bundle.getString("miui.app.resourcebrowser.RESOURCE_TYPE_PARAMETER");
        s2 = null;
        String s3;
        Object aobj[] = new Object[2];
        aobj[0] = URLEncoder.encode(s, "UTF-8");
        aobj[1] = s1;
        s3 = String.format("http://market.xiaomi.com/thm/search?apiversion=1&keywords=%s&category=%s", aobj);
        s2 = s3;
_L2:
        return (new StringBuilder()).append(s2).append(getVersionSuffix()).toString();
        UnsupportedEncodingException unsupportedencodingexception;
        unsupportedencodingexception;
        unsupportedencodingexception.printStackTrace();
        if(true) goto _L2; else goto _L1
_L1:
    }

    public transient String getUpdateUrl(String as[]) {
        StringBuilder stringbuilder = new StringBuilder();
        for(int i = 0; i < as.length; i++) {
            stringbuilder.append(as[i]);
            stringbuilder.append(",");
        }

        Object aobj[] = new Object[1];
        aobj[0] = stringbuilder.substring(0, -1 + stringbuilder.length());
        String s = String.format("http://market.xiaomi.com/thm/checkupdate?fileshash=%s", aobj);
        return (new StringBuilder()).append(s).append(getVersionSuffix()).toString();
    }

    public InputStream getUrlInputStream(String s) throws IOException {
        HttpGet httpget = new HttpGet(URI.create(s));
        BasicHttpParams basichttpparams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(basichttpparams, 20000);
        HttpConnectionParams.setSoTimeout(basichttpparams, 20000);
        return (new BufferedHttpEntity((new DefaultHttpClient(basichttpparams)).execute(httpget).getEntity())).getContent();
    }

    public InputStream getUrlInputStream2(String s) throws IOException {
        HttpURLConnection httpurlconnection = (HttpURLConnection)(new URL(s)).openConnection();
        httpurlconnection.setConnectTimeout(20000);
        httpurlconnection.setReadTimeout(20000);
        httpurlconnection.connect();
        return httpurlconnection.getInputStream();
    }

    private static final int CONNECTION_TIMEOUT = 20000;
    private static final int READ_TIMEOUT = 20000;
    private static String USER_SUFFIX;
    private static String VERSION_SUFFIX;
    private static OnlineService mInstance;
}
