// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser.service.local;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;

public abstract class ZipResourceCache {
    public static final class DefaultZipCacheImpl extends ZipResourceCache
        implements Serializable {

        private void readObject(ObjectInputStream objectinputstream) throws IOException, ClassNotFoundException {
            readCache(objectinputstream);
        }

        private void writeObject(ObjectOutputStream objectoutputstream) throws IOException {
            writeCache(objectoutputstream);
        }

        private static final long serialVersionUID = 1L;

        public DefaultZipCacheImpl() {
        }
    }


    public ZipResourceCache() {
        nvp = new HashMap();
        thumbnails = new ArrayList();
        previews = new ArrayList();
    }

    protected boolean imageCached() {
        boolean flag;
        if(thumbnails != null && previews != null && !thumbnails.isEmpty() && (new File((String)thumbnails.get(0))).exists())
            flag = true;
        else
            flag = false;
        return flag;
    }

    protected void readBaiscInformation(ObjectInputStream objectinputstream) throws IOException, ClassNotFoundException {
        filePath = (String)objectinputstream.readObject();
        fileSize = objectinputstream.readLong();
        lastModifyTime = objectinputstream.readLong();
        title = (String)objectinputstream.readObject();
        designer = (String)objectinputstream.readObject();
        author = (String)objectinputstream.readObject();
        version = (String)objectinputstream.readObject();
        platformVersion = objectinputstream.readInt();
        nvp = (HashMap)objectinputstream.readObject();
        fileHash = (String)objectinputstream.readObject();
    }

    public final void readCache(ObjectInputStream objectinputstream) throws IOException, ClassNotFoundException {
        readBaiscInformation(objectinputstream);
        readImageInformation(objectinputstream);
    }

    protected void readImageInformation(ObjectInputStream objectinputstream) throws IOException, ClassNotFoundException {
        thumbnails = new ArrayList();
        String as[] = (String[])(String[])objectinputstream.readObject();
        int i = as.length;
        for(int j = 0; j < i; j++) {
            String s1 = as[j];
            thumbnails.add(s1);
        }

        previews = new ArrayList();
        String as1[] = (String[])(String[])objectinputstream.readObject();
        int k = as1.length;
        for(int l = 0; l < k; l++) {
            String s = as1[l];
            previews.add(s);
        }

    }

    public boolean valid() {
        boolean flag = false;
        if(filePath != null) {
            File file = new File(filePath);
            if(lastModifyTime >= file.lastModified() && fileSize == file.length() && imageCached())
                flag = true;
            else
                flag = false;
        }
        return flag;
    }

    protected void writeBaiscInformation(ObjectOutputStream objectoutputstream) throws IOException {
        objectoutputstream.writeObject(filePath);
        objectoutputstream.writeLong(fileSize);
        objectoutputstream.writeLong(lastModifyTime);
        objectoutputstream.writeObject(title);
        objectoutputstream.writeObject(designer);
        objectoutputstream.writeObject(author);
        objectoutputstream.writeObject(version);
        objectoutputstream.writeInt(platformVersion);
        objectoutputstream.writeObject(nvp);
        objectoutputstream.writeObject(fileHash);
    }

    public final void writeCache(ObjectOutputStream objectoutputstream) throws IOException {
        writeBaiscInformation(objectoutputstream);
        writeImageInformation(objectoutputstream);
    }

    protected void writeImageInformation(ObjectOutputStream objectoutputstream) throws IOException {
        String as[] = new String[thumbnails.size()];
        thumbnails.toArray(as);
        objectoutputstream.writeObject(as);
        String as1[] = new String[previews.size()];
        previews.toArray(as1);
        objectoutputstream.writeObject(as1);
    }

    public String author;
    public String designer;
    public String fileHash;
    public String filePath;
    public long fileSize;
    public long lastModifyTime;
    public HashMap nvp;
    public int platformVersion;
    public ArrayList previews;
    public ArrayList thumbnails;
    public String title;
    public String version;
}
