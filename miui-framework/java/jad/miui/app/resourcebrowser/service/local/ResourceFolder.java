// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser.service.local;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import java.io.*;
import java.util.*;
import miui.app.resourcebrowser.IntentConstants;
import miui.app.resourcebrowser.resource.Resource;
import miui.app.resourcebrowser.util.ResourceHelper;
import miui.os.ExtraFileUtils;

public abstract class ResourceFolder
    implements miui.widget.AsyncAdapter.AsyncLoadDataVisitor, IntentConstants {

    public ResourceFolder(Context context, Bundle bundle, String s) {
        this(context, bundle, s, null);
    }

    public ResourceFolder(Context context, Bundle bundle, String s, String s1) {
        mFirstLoadData = true;
        mFileFlags = new HashMap();
        mResourceSeq = 0;
        mContext = context;
        mMetaData = bundle;
        mFolderPath = s;
        mKeyword = s1;
        if(!TextUtils.isEmpty(s)) {
            String s2 = ExtraFileUtils.standardizeFolderPath(mMetaData.getString("miui.app.resourcebrowser.CACHE_LIST_FOLDER"));
            File file = new File((new StringBuilder()).append(s2).append(s.replace('/', '_')).toString());
            if(file.exists())
                file.delete();
            mCacheFileName = (new StringBuilder()).append(s2).append("cache").append(s.replace('/', '_')).toString();
        }
    }

    private void addResourceIntoUI(miui.widget.AsyncAdapter.AsyncLoadDataTask asyncloaddatatask, Resource resource) {
        if(resource != null) {
            if(mResourceSeq == 0)
                resource.setDividerTitle(mFolderDescription);
            Resource aresource[] = new Resource[1];
            aresource[0] = resource;
            asyncloaddatatask.onLoadData(aresource);
            mResourceSeq = 1 + mResourceSeq;
        }
    }

    protected void addItem(String s) {
    }

    protected Resource buildResource(String s) {
        Bundle bundle = new Bundle();
        File file = new File(s);
        bundle.putString("NAME", file.getName());
        bundle.putString("SIZE", String.valueOf(file.length()));
        bundle.putLong("MODIFIED_TIME", file.lastModified());
        bundle.putString("LOCAL_PATH", s);
        Resource resource = new Resource();
        resource.setInformation(bundle);
        resource.setFileHash(ResourceHelper.getFileHash(s));
        return resource;
    }

    public boolean dataChanged() {
        boolean flag;
        miui.cache.FolderCache.FolderInfo folderinfo;
        flag = true;
        folderinfo = ResourceHelper.getFolderInfoCache(mFolderPath);
        if(folderinfo == null) goto _L2; else goto _L1
_L1:
        Iterator iterator = mFileFlags.keySet().iterator();
_L6:
        if(!iterator.hasNext()) goto _L4; else goto _L3
_L3:
        String s1 = (String)iterator.next();
        if(folderinfo.files.containsKey(s1)) goto _L6; else goto _L5
_L5:
        return flag;
_L4:
        Iterator iterator1 = folderinfo.files.keySet().iterator();
_L9:
        if(!iterator1.hasNext()) goto _L2; else goto _L7
_L7:
        String s = (String)iterator1.next();
        if(mFileFlags.containsKey(s) || !isInterestedResource(s)) goto _L9; else goto _L8
_L8:
        continue; /* Loop/switch isn't completed */
_L2:
        flag = false;
        if(true) goto _L5; else goto _L10
_L10:
    }

    public String getFolderPath() {
        return mFolderPath;
    }

    public List getHeadExtraResource() {
        return null;
    }

    protected boolean isInterestedResource(String s) {
        return true;
    }

    public final void loadData(miui.widget.AsyncAdapter.AsyncLoadDataTask asyncloaddatatask) {
        onPreLoadData();
        onLoadData(asyncloaddatatask);
        onPostLoadData();
    }

    protected final void onLoadData(miui.widget.AsyncAdapter.AsyncLoadDataTask asyncloaddatatask) {
        mResourceSeq = 0;
        List list = getHeadExtraResource();
        if(list != null) {
            for(Iterator iterator = list.iterator(); iterator.hasNext(); addResourceIntoUI(asyncloaddatatask, (Resource)iterator.next()));
        }
        if(!TextUtils.isEmpty(mFolderPath)) goto _L2; else goto _L1
_L1:
        return;
_L2:
        miui.cache.FolderCache.FolderInfo folderinfo;
        if(mFirstLoadData) {
            ArrayList arraylist;
            int i;
            try {
                readCache();
            }
            catch(FileNotFoundException filenotfoundexception) {
                reset();
            }
            catch(Exception exception1) {
                exception1.printStackTrace();
                reset();
            }
            mFirstLoadData = false;
        }
        mDirtyResources = false;
        folderinfo = ResourceHelper.getFolderInfoCache(mFolderPath);
        if(folderinfo != null) {
            arraylist = new ArrayList(folderinfo.files.keySet());
            sortResource(arraylist, folderinfo);
            i = 0;
            while(i < arraylist.size())  {
                String s1 = (String)arraylist.get(i);
                if(!s1.endsWith(".temp")) {
                    if(!mFileFlags.containsKey(s1)) {
                        addItem(s1);
                        mDirtyResources = true;
                    }
                    Resource resource = buildResource(s1);
                    if(resource != null) {
                        String s2 = resource.getTitle();
                        if(mKeyword == null || s2 != null && s2.toLowerCase().contains(mKeyword) || ((miui.cache.FolderCache.FileInfo)folderinfo.files.get(s1)).name.toLowerCase().contains(mKeyword))
                            addResourceIntoUI(asyncloaddatatask, resource);
                    }
                }
                i++;
            }
            ArrayList arraylist1 = new ArrayList(mFileFlags.keySet());
            for(int j = 0; j < arraylist1.size(); j++) {
                String s = (String)arraylist1.get(j);
                if(!folderinfo.files.containsKey(s)) {
                    removeItem(s);
                    mDirtyResources = true;
                }
            }

        }
        if(mDirtyResources)
            try {
                saveCache();
            }
            catch(Exception exception) {
                exception.printStackTrace();
            }
        if(true) goto _L1; else goto _L3
_L3:
    }

    protected void onPostLoadData() {
    }

    protected void onPreLoadData() {
    }

    protected void readCache() throws Exception {
        File file;
        ObjectInputStream objectinputstream;
        file = new File(mCacheFileName);
        objectinputstream = null;
        ObjectInputStream objectinputstream1 = new ObjectInputStream(new BufferedInputStream(new FileInputStream(file), 16384));
        readDataFromStream(objectinputstream1);
        if(objectinputstream1 != null)
            objectinputstream1.close();
        return;
        Exception exception;
        exception;
_L2:
        if(objectinputstream != null)
            objectinputstream.close();
        throw exception;
        exception;
        objectinputstream = objectinputstream1;
        if(true) goto _L2; else goto _L1
_L1:
    }

    protected void readDataFromStream(ObjectInputStream objectinputstream) throws Exception {
        mFileFlags = (HashMap)objectinputstream.readObject();
    }

    protected void removeItem(String s) {
        mFileFlags.remove(s);
    }

    protected void reset() {
        mFileFlags.clear();
    }

    protected void saveCache() throws Exception {
        File file;
        ObjectOutputStream objectoutputstream;
        file = new File(mCacheFileName);
        file.delete();
        objectoutputstream = null;
        ObjectOutputStream objectoutputstream1 = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(file)));
        writeDataToStream(objectoutputstream1);
        if(objectoutputstream1 != null)
            objectoutputstream1.close();
        return;
        Exception exception;
        exception;
_L2:
        if(objectoutputstream != null)
            objectoutputstream.close();
        throw exception;
        exception;
        objectoutputstream = objectoutputstream1;
        if(true) goto _L2; else goto _L1
_L1:
    }

    public void setFolderDescription(String s) {
        mFolderDescription = s;
    }

    public void setKeyword(String s) {
        mKeyword = s.toLowerCase();
    }

    protected void sortResource(List list, final miui.cache.FolderCache.FolderInfo folderInfo) {
        if(ResourceHelper.isSystemResource(mFolderPath) || ResourceHelper.isDataResource(mFolderPath))
            Collections.sort(list);
        else
            Collections.sort(list, new Comparator() {

                public volatile int compare(Object obj, Object obj1) {
                    return compare((String)obj, (String)obj1);
                }

                public int compare(String s, String s1) {
                    int j;
                    Long long1 = Long.valueOf(((miui.cache.FolderCache.FileInfo)folderInfo.files.get(s)).modifiedTime);
                    j = Long.valueOf(((miui.cache.FolderCache.FileInfo)folderInfo.files.get(s1)).modifiedTime).compareTo(long1);
                    int i = j;
_L2:
                    return i;
                    Exception exception;
                    exception;
                    i = 0;
                    if(true) goto _L2; else goto _L1
_L1:
                }

                final ResourceFolder this$0;
                final miui.cache.FolderCache.FolderInfo val$folderInfo;

             {
                this$0 = ResourceFolder.this;
                folderInfo = folderinfo;
                super();
            }
            });
    }

    protected void writeDataToStream(ObjectOutputStream objectoutputstream) throws IOException {
        objectoutputstream.writeObject(mFileFlags);
    }

    protected String mCacheFileName;
    protected Context mContext;
    protected boolean mDirtyResources;
    protected Map mFileFlags;
    private boolean mFirstLoadData;
    protected String mFolderDescription;
    protected String mFolderPath;
    protected String mKeyword;
    protected Bundle mMetaData;
    private int mResourceSeq;
}
