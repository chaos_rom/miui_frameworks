// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser.service.local;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import java.io.*;
import java.util.HashMap;
import java.util.Map;
import miui.app.resourcebrowser.resource.Resource;
import miui.app.resourcebrowser.util.ResourceDebug;
import miui.app.resourcebrowser.util.ResourceHelper;

// Referenced classes of package miui.app.resourcebrowser.service.local:
//            ResourceFolder, ZipResourceCache, ZipResourceInfo

public class ZipResourceFolder extends ResourceFolder {

    public ZipResourceFolder(Context context, Bundle bundle, String s) {
        this(context, bundle, s, null);
    }

    public ZipResourceFolder(Context context, Bundle bundle, String s, String s1) {
        super(context, bundle, s, s1);
        mZipResourceInfoCache = new HashMap();
    }

    protected void addItem(String s) {
        super.addItem(s);
    }

    protected Resource buildResource(String s) {
        boolean flag;
        ZipResourceCache zipresourcecache;
        ZipResourceInfo zipresourceinfo;
        flag = true;
        zipresourcecache = (ZipResourceCache)mZipResourceInfoCache.get(s);
        zipresourceinfo = createZipResource(super.mContext, s, zipresourcecache);
        if(zipresourceinfo != null) goto _L2; else goto _L1
_L1:
        Resource resource = null;
_L4:
        return resource;
_L2:
        ZipResourceCache zipresourcecache1 = zipresourceinfo.getCache();
        if(zipresourcecache1.fileHash == null && !ResourceHelper.isSystemResource(s)) {
            if(ResourceDebug.DEBUG)
                Log.d("Theme", (new StringBuilder()).append("Recompute file hash for resource: ").append(s).append(" because of invalid cache.").toString());
            zipresourcecache1.fileHash = ResourceHelper.getFileHash(s);
            super.mDirtyResources = flag;
        }
        mZipResourceInfoCache.put(s, zipresourcecache1);
        if(!super.mDirtyResources && zipresourcecache1 == zipresourcecache)
            flag = false;
        super.mDirtyResources = flag;
        zipresourceinfo.clearCache();
        resource = new Resource();
        resource.setInformation(zipresourceinfo.getInformation());
        resource.setFileHash(zipresourcecache1.fileHash);
        if(!ResourceHelper.isCompatiblePlatformVersion(resource.getPlatformVersion(), super.mMetaData.getInt("miui.app.resourcebrowser.PLATFORM_VERSION_START"), super.mMetaData.getInt("miui.app.resourcebrowser.PLATFORM_VERSION_END")))
            resource = null;
        if(true) goto _L4; else goto _L3
_L3:
    }

    protected ZipResourceInfo createZipResource(Context context, String s, ZipResourceCache zipresourcecache) {
        ZipResourceInfo zipresourceinfo;
        if(!ResourceHelper.isZipResource(s))
            zipresourceinfo = null;
        else
            zipresourceinfo = ZipResourceInfo.createZipResourceInfo(context, s, zipresourcecache, new Object[0]);
        return zipresourceinfo;
    }

    protected boolean isInterestedResource(String s) {
        return ResourceHelper.isZipResource(s);
    }

    protected void readDataFromStream(ObjectInputStream objectinputstream) throws Exception {
        super.readDataFromStream(objectinputstream);
        mZipResourceInfoCache = (HashMap)objectinputstream.readObject();
    }

    protected void removeItem(String s) {
        super.removeItem(s);
        mZipResourceInfoCache.remove(s);
    }

    protected void reset() {
        super.reset();
        mZipResourceInfoCache.clear();
    }

    protected void writeDataToStream(ObjectOutputStream objectoutputstream) throws IOException {
        super.writeDataToStream(objectoutputstream);
        objectoutputstream.writeObject(mZipResourceInfoCache);
    }

    private Map mZipResourceInfoCache;
}
