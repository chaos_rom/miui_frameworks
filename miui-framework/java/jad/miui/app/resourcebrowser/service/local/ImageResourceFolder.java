// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser.service.local;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import java.io.File;
import java.util.*;
import miui.app.resourcebrowser.resource.Resource;
import miui.app.resourcebrowser.util.ResourceHelper;
import miui.os.ExtraFileUtils;

// Referenced classes of package miui.app.resourcebrowser.service.local:
//            ResourceFolder

public class ImageResourceFolder extends ResourceFolder {

    public ImageResourceFolder(Context context, Bundle bundle, String s) {
        this(context, bundle, s, null);
    }

    public ImageResourceFolder(Context context, Bundle bundle, String s, String s1) {
        super(context, bundle, s, s1);
        mOptions = new android.graphics.BitmapFactory.Options();
        mOptions.inJustDecodeBounds = true;
    }

    private Resource getResource(String s) {
        File file = new File(s);
        Resource resource = new Resource();
        Bundle bundle = new Bundle();
        String s1 = file.getName();
        if(s1.endsWith(".jpg") || s1.endsWith(".png"))
            s1 = s1.substring(0, -4 + s1.length());
        if(s1.indexOf("__") > 0)
            s1 = s1.substring(0, s1.indexOf("__"));
        bundle.putString("NAME", s1);
        bundle.putString("SIZE", String.valueOf(file.length()));
        bundle.putLong("MODIFIED_TIME", file.lastModified());
        bundle.putString("LOCAL_PATH", s);
        ArrayList arraylist = new ArrayList(1);
        arraylist.add(s);
        bundle.putStringArrayList("LOCAL_PREVIEW", arraylist);
        bundle.putStringArrayList("LOCAL_THUMBNAIL", arraylist);
        resource.setInformation(bundle);
        return resource;
    }

    protected void addItem(String s) {
        BitmapFactory.decodeFile(s, mOptions);
        Map map = super.mFileFlags;
        long l;
        if(mOptions.outHeight == -1)
            l = 0L;
        else
            l = 1L;
        map.put(s, Long.valueOf(l));
    }

    protected Resource buildResource(String s) {
        Resource resource;
        if(((Long)super.mFileFlags.get(s)).longValue() == 0L)
            resource = null;
        else
            resource = getResource(s);
        return resource;
    }

    public void enableTransparentWallpaper(boolean flag) {
        Object aobj[] = new Object[2];
        aobj[0] = ExtraFileUtils.standardizeFolderPath(super.mFolderPath);
        aobj[1] = super.mContext.getString(0x60c0005);
        String s = String.format("%s%s.png", aobj);
        File file = new File(s);
        if(!file.exists())
            ResourceHelper.writeTo(super.mContext.getResources().openRawResource(0x6050000), s);
        if(file.exists() && flag)
            mEmptyTranslatePaper = getResource(s);
        else
            mEmptyTranslatePaper = null;
    }

    public List getHeadExtraResource() {
        ArrayList arraylist = null;
        if(mEmptyTranslatePaper != null) {
            arraylist = new ArrayList();
            arraylist.add(mEmptyTranslatePaper);
        }
        return arraylist;
    }

    private Resource mEmptyTranslatePaper;
    private android.graphics.BitmapFactory.Options mOptions;
}
