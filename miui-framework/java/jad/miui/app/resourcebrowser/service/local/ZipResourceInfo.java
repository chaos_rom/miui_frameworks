// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser.service.local;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.format.DateFormat;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.zip.*;
import javax.xml.parsers.ParserConfigurationException;
import miui.app.resourcebrowser.IntentConstants;
import miui.app.resourcebrowser.ResourceConstants;
import miui.app.resourcebrowser.util.ResourceHelper;
import miui.app.resourcebrowser.util.ZipFileHelper;
import miui.os.ExtraFileUtils;
import org.xml.sax.SAXException;

// Referenced classes of package miui.app.resourcebrowser.service.local:
//            ZipResourceCache

public class ZipResourceInfo
    implements IntentConstants {

    protected ZipResourceInfo(Context context, String s, ZipResourceCache zipresourcecache) throws IOException, ParserConfigurationException, SAXException {
        mThumbnails = new ArrayList();
        mPreviews = new ArrayList();
        mNvp = new HashMap();
        mZipResourceCache = getCacheInstance();
        mContext = context;
        mPath = s;
        mEncodedPath = s.replace('/', '_');
        File file;
        try {
            mPrefix = ((Activity)mContext).getIntent().getBundleExtra("META_DATA_FOR_LOCAL").getStringArray("miui.app.resourcebrowser.PREVIEW_PREFIX");
        }
        catch(Exception exception) { }
        file = new File(s);
        mLastModified = file.lastModified();
        if(mLastModified > System.currentTimeMillis()) {
            file.setLastModified(System.currentTimeMillis());
            mLastModified = file.lastModified();
        }
        mSize = file.length();
        if(zipresourcecache != null && zipresourcecache.valid()) {
            mTitle = zipresourcecache.title;
            mAuthor = zipresourcecache.author;
            mDesigner = zipresourcecache.designer;
            mVersion = zipresourcecache.version;
            mPlatformVersion = zipresourcecache.platformVersion;
            mNvp = zipresourcecache.nvp;
        } else {
            parseBasicInformation(s);
            mZipResourceCache.filePath = mPath;
            mZipResourceCache.lastModifyTime = mLastModified;
            mZipResourceCache.fileSize = mSize;
            mZipResourceCache.title = mTitle;
            mZipResourceCache.author = mAuthor;
            mZipResourceCache.designer = mDesigner;
            mZipResourceCache.version = mVersion;
            mZipResourceCache.platformVersion = mPlatformVersion;
            mZipResourceCache.nvp = mNvp;
        }
    }

    public static transient ZipResourceInfo createZipResourceInfo(Context context, String s, ZipResourceCache zipresourcecache, Object aobj[]) {
        ZipResourceInfo zipresourceinfo = null;
        ZipResourceInfo zipresourceinfo1 = new ZipResourceInfo(context, s, zipresourcecache);
        if(zipresourcecache == null)
            break MISSING_BLOCK_LABEL_26;
        if(zipresourcecache.valid())
            break MISSING_BLOCK_LABEL_37;
        zipresourceinfo1.loadResourcePreviews();
        zipresourcecache = zipresourceinfo1.mZipResourceCache;
        zipresourceinfo1.loadPreviewsFromCache(zipresourcecache);
        if(zipresourcecache != null)
            zipresourceinfo1.mZipResourceCache = zipresourcecache;
        zipresourceinfo = zipresourceinfo1;
_L2:
        return zipresourceinfo;
        Exception exception;
        exception;
_L3:
        exception.printStackTrace();
        if(true) goto _L2; else goto _L1
_L1:
        exception;
        zipresourceinfo = zipresourceinfo1;
          goto _L3
    }

    public void clearCache() {
        mZipResourceCache = null;
    }

    protected String extract(ZipFile zipfile, ZipEntry zipentry, String s) {
        String s1 = (new StringBuilder()).append(getPreviewPathPrefix()).append(File.separator).append(s).toString();
        File file = new File(s1);
        if(file.exists() && (file.lastModified() < mLastModified || file.length() != zipentry.getSize()))
            file.delete();
        if(!file.exists())
            try {
                ResourceHelper.writeTo(zipfile.getInputStream(zipentry), s1);
            }
            catch(IOException ioexception) {
                ioexception.printStackTrace();
            }
        return s1;
    }

    public ZipResourceCache getCache() {
        return mZipResourceCache;
    }

    protected ZipResourceCache getCacheInstance() {
        return new ZipResourceCache.DefaultZipCacheImpl();
    }

    public Bundle getInformation() {
        Bundle bundle = new Bundle();
        bundle.putString("SIZE", String.valueOf(mSize));
        bundle.putString("NAME", mTitle);
        bundle.putString("AUTHOR", mAuthor);
        bundle.putString("DESIGNER", mDesigner);
        bundle.putString("VERSION", mVersion);
        bundle.putInt("PLATFORM_VERSION", mPlatformVersion);
        bundle.putLong("MODIFIED_TIME", mLastModified);
        bundle.putString("LOCAL_PATH", mPath);
        bundle.putStringArrayList("LOCAL_PREVIEW", mPreviews);
        bundle.putStringArrayList("LOCAL_THUMBNAIL", mThumbnails);
        bundle.putSerializable("NVP", mNvp);
        return bundle;
    }

    protected String getPreviewPathPrefix() {
        return ExtraFileUtils.standardizeFolderPath((new StringBuilder()).append(ResourceConstants.PREVIEW_PATH).append(mEncodedPath).toString());
    }

    protected boolean loadPreview(ZipFile zipfile, String s, ArrayList arraylist) {
        ZipEntry zipentry = zipfile.getEntry(s);
        boolean flag;
        if(zipentry != null) {
            arraylist.add(extract(zipfile, zipentry, s));
            flag = true;
        } else {
            flag = false;
        }
        return flag;
    }

    protected ArrayList loadPreviews(ZipFile zipfile, String s) {
        ArrayList arraylist = new ArrayList();
        for(int i = 0; i < 10; i++) {
            Object aobj[] = new Object[2];
            aobj[0] = s;
            aobj[1] = Integer.valueOf(i);
            if(!loadPreview(zipfile, String.format("%s%d.jpg", aobj), arraylist)) {
                Object aobj1[] = new Object[2];
                aobj1[0] = s;
                aobj1[1] = Integer.valueOf(i);
                loadPreview(zipfile, String.format("%s%d.png", aobj1), arraylist);
            }
        }

        return arraylist;
    }

    protected void loadPreviewsFromCache(ZipResourceCache zipresourcecache) {
        if(zipresourcecache != null) {
            mPreviews = zipresourcecache.previews;
            mThumbnails = zipresourcecache.thumbnails;
        }
    }

    protected void loadResourcePreviews() {
        ZipFile zipfile = new ZipFile(mPath);
        ArrayList arraylist = new ArrayList();
        ArrayList arraylist1 = new ArrayList();
        for(int i = 0; i < mPrefix.length; i++) {
            String s = "preview/";
            if(!TextUtils.isEmpty(mPrefix[i]))
                s = (new StringBuilder()).append(s).append(mPrefix[i]).toString();
            ArrayList arraylist2 = loadPreviews(zipfile, (new StringBuilder()).append(s).append("big_").toString());
            ArrayList arraylist3 = loadPreviews(zipfile, (new StringBuilder()).append(s).append("small_").toString());
            if(arraylist2.size() == 0)
                arraylist2.addAll(loadPreviews(zipfile, s));
            if(arraylist3.size() == 0)
                arraylist3 = arraylist2;
            arraylist.addAll(arraylist3);
            arraylist1.addAll(arraylist2);
        }

        mZipResourceCache.thumbnails = arraylist;
        mZipResourceCache.previews = arraylist1;
_L1:
        return;
        IOException ioexception;
        ioexception;
        ioexception.printStackTrace();
          goto _L1
    }

    protected void parseBasicInformation(String s) throws ZipException, IOException, ParserConfigurationException, SAXException {
        String s1;
        s1 = null;
        mNvp = ZipFileHelper.getZipResourceDescribeInfo(s, "description.xml");
        if(mNvp == null) goto _L2; else goto _L1
_L1:
        mTitle = (String)mNvp.get("title");
        String s2;
        String s3;
        String s4;
        if(mTitle == null)
            s2 = null;
        else
            s2 = mTitle.trim();
        mTitle = s2;
        mAuthor = (String)mNvp.get("author");
        if(mAuthor == null)
            s3 = null;
        else
            s3 = mAuthor.trim();
        mAuthor = s3;
        mDesigner = (String)mNvp.get("designer");
        if(mDesigner == null)
            s4 = null;
        else
            s4 = mDesigner.trim();
        mDesigner = s4;
        mVersion = (String)mNvp.get("version");
        if(mVersion != null)
            s1 = mVersion.trim();
        mVersion = s1;
        if(!mNvp.containsKey("platformVersion")) goto _L4; else goto _L3
_L3:
        mPlatformVersion = Integer.parseInt((String)mNvp.get("platformVersion"));
_L2:
        if(TextUtils.isEmpty(mTitle))
            mTitle = mPath.substring(1 + mPath.lastIndexOf("/"), mPath.lastIndexOf("."));
        if(TextUtils.isEmpty(mAuthor))
            mAuthor = mContext.getResources().getString(0x60c000b);
        if(TextUtils.isEmpty(mVersion))
            mVersion = DateFormat.format("yyyy.MM.d", mLastModified).toString();
        return;
_L4:
        try {
            mPlatformVersion = Integer.parseInt((String)mNvp.get("uiVersion"));
        }
        catch(NumberFormatException numberformatexception) {
            mPlatformVersion = 0;
        }
        if(true) goto _L2; else goto _L5
_L5:
    }

    protected static final String AUTHOR_TAG = "author";
    protected static final String DESIGNER_TAG = "designer";
    protected static final int MAXIMUM_PREVIEW_COUNT = 10;
    protected static final String PLATFORM_VERSION_TAG = "platformVersion";
    protected static final String TITLE_TAG = "title";
    protected static final String UI_VERSION_TAG = "uiVersion";
    protected static final String VERSION_TAG = "version";
    public String mAuthor;
    public Context mContext;
    public String mDesigner;
    protected String mEncodedPath;
    public long mLastModified;
    protected HashMap mNvp;
    public String mPath;
    public int mPlatformVersion;
    protected String mPrefix[];
    public ArrayList mPreviews;
    public long mSize;
    public ArrayList mThumbnails;
    public String mTitle;
    public String mVersion;
    public ZipResourceCache mZipResourceCache;
}
