// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser.service.local;

import android.content.Context;
import android.content.res.Resources;
import android.media.ExtraRingtone;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import java.io.File;
import java.util.*;
import miui.app.resourcebrowser.resource.Resource;
import miui.app.resourcebrowser.util.ResourceHelper;

// Referenced classes of package miui.app.resourcebrowser.service.local:
//            ResourceFolder

public class AudioResourceFolder extends ResourceFolder {

    public AudioResourceFolder(Context context, Bundle bundle, String s) {
        this(context, bundle, s, null);
    }

    public AudioResourceFolder(Context context, Bundle bundle, String s, String s1) {
        int i = 5000;
        int j = 0;
        super(context, bundle, s, s1);
        mMinDurationLimit = 0;
        mMaxDurationLimit = 0x7fffffff;
        mRingtoneType = super.mMetaData.getInt("android.intent.extra.ringtone.TYPE");
        Bundle bundle1 = super.mMetaData;
        int k;
        Bundle bundle2;
        if(mRingtoneType != 2)
            j = i;
        k = bundle1.getInt("miui.app.resourcebrowser.RINGTONE_MIN_DURATION_LIMIT", j);
        bundle2 = super.mMetaData;
        if(mRingtoneType != 2)
            i = 0x7fffffff;
        setDurationLimitation(k, bundle2.getInt("miui.app.resourcebrowser.RINGTONE_MAX_DURATION_LIMIT", i));
    }

    public static String formatDuration(int i) {
        int j = i / 1000;
        Object aobj[] = new Object[2];
        aobj[0] = Integer.valueOf(j / 60);
        aobj[1] = Integer.valueOf(j % 60);
        return String.format("%02d:%02d", aobj);
    }

    public static long getLocalRingtoneDuration(String s) {
        long l;
        try {
            if(!(new File(s)).exists()) {
                l = -1L;
            } else {
                MediaPlayer mediaplayer = new MediaPlayer();
                mediaplayer.setDataSource(s);
                mediaplayer.prepare();
                l = mediaplayer.getDuration();
                mediaplayer.release();
            }
        }
        catch(Exception exception) {
            l = -1L;
        }
        return l;
    }

    private String getTitleForDefaultUri() {
        int i = 0x60c0007;
        mRingtoneType;
        JVM INSTR tableswitch 1 4: default 36
    //                   1 76
    //                   2 82
    //                   3 36
    //                   4 88;
           goto _L1 _L2 _L3 _L1 _L4
_L1:
        break; /* Loop/switch isn't completed */
_L4:
        break MISSING_BLOCK_LABEL_88;
_L5:
        String s = removeFileNameSuffix(ExtraRingtone.getRingtoneTitle(super.mContext, mDefaultUri, false));
        String s1;
        if(TextUtils.isEmpty(s)) {
            s1 = super.mContext.getResources().getString(i);
        } else {
            Object aobj[] = new Object[2];
            aobj[0] = super.mContext.getResources().getString(i);
            aobj[1] = s;
            s1 = String.format("%s (%s)", aobj);
        }
        return s1;
_L2:
        i = 0x60c0008;
          goto _L5
_L3:
        i = 0x60c0009;
          goto _L5
        i = 0x60c000a;
          goto _L5
    }

    private boolean matchLimitation(long l) {
        boolean flag;
        if((long)mMinDurationLimit <= l && l <= (long)mMaxDurationLimit)
            flag = true;
        else
            flag = false;
        return flag;
    }

    private String removeFileNameSuffix(String s) {
        if(TextUtils.isEmpty(s)) goto _L2; else goto _L1
_L1:
        String s1;
        String as[];
        int i;
        int j;
        s1 = s.toLowerCase();
        as = COMMON_RINGTONE_SUFFIX;
        i = as.length;
        j = 0;
_L7:
        if(j >= i) goto _L2; else goto _L3
_L3:
        String s2 = as[j];
        if(!s1.endsWith(s2)) goto _L5; else goto _L4
_L4:
        s = s.substring(0, s1.lastIndexOf(s2));
_L2:
        return s;
_L5:
        j++;
        if(true) goto _L7; else goto _L6
_L6:
    }

    protected void addItem(String s) {
        long l = getLocalRingtoneDuration(s);
        super.mFileFlags.put(s, Long.valueOf(l));
    }

    protected Resource buildResource(String s) {
        long l = ((Long)super.mFileFlags.get(s)).longValue();
        if(l == 0L)
            l = getLocalRingtoneDuration(s);
        Resource resource;
        if(l < 0L || !matchLimitation(l)) {
            resource = null;
        } else {
            resource = super.buildResource(s);
            Bundle bundle = resource.getInformation();
            bundle.putString("NAME", removeFileNameSuffix((new File(s)).getName()));
            bundle.putInt("audio_duration", (int)l);
            resource.setInformation(bundle);
        }
        return resource;
    }

    public void enableDefaultOption(boolean flag) {
        if(!flag)
            break MISSING_BLOCK_LABEL_274;
        mDefaultUri = (Uri)super.mMetaData.getParcelable("android.intent.extra.ringtone.DEFAULT_URI");
        if(mDefaultUri != null) goto _L2; else goto _L1
_L1:
        mRingtoneType;
        JVM INSTR tableswitch 1 4: default 60
    //                   1 73
    //                   2 83
    //                   3 60
    //                   4 93;
           goto _L2 _L3 _L4 _L2 _L5
_L2:
        if(mDefaultUri == null) {
            mDefaultOption = null;
        } else {
            String s = ResourceHelper.getPathByUri(super.mContext, mDefaultUri);
            File file;
            long l;
            Bundle bundle;
            if(TextUtils.isEmpty(s))
                if((new File(mDefaultUri.toString())).exists())
                    s = mDefaultUri.toString();
                else
                    s = "";
            file = new File(s);
            if(file.exists())
                l = file.length();
            else
                l = 0L;
            bundle = new Bundle();
            bundle.putString("NAME", getTitleForDefaultUri());
            bundle.putString("SIZE", String.valueOf(l));
            bundle.putLong("MODIFIED_TIME", 0L);
            bundle.putString("LOCAL_PATH", mDefaultUri.toString());
            bundle.putInt("audio_duration", -1);
            mDefaultOption = new Resource();
            mDefaultOption.setInformation(bundle);
        }
_L6:
        return;
_L3:
        mDefaultUri = android.provider.Settings.System.DEFAULT_RINGTONE_URI;
          goto _L2
_L4:
        mDefaultUri = android.provider.Settings.System.DEFAULT_NOTIFICATION_URI;
          goto _L2
_L5:
        mDefaultUri = android.provider.Settings.System.DEFAULT_ALARM_ALERT_URI;
          goto _L2
        mDefaultOption = null;
          goto _L6
    }

    public void enableMuteOption(boolean flag) {
        if(flag) {
            Bundle bundle = new Bundle();
            bundle.putString("NAME", super.mContext.getString(0x60c0006));
            bundle.putString("SIZE", "0");
            bundle.putLong("MODIFIED_TIME", 0L);
            bundle.putString("LOCAL_PATH", "");
            bundle.putInt("audio_duration", -1);
            mMuteOption = new Resource();
            mMuteOption.setInformation(bundle);
        } else {
            mMuteOption = null;
        }
    }

    public List getHeadExtraResource() {
        ArrayList arraylist = new ArrayList();
        if(mMuteOption != null)
            arraylist.add(mMuteOption);
        if(mDefaultOption != null)
            arraylist.add(mDefaultOption);
        return arraylist;
    }

    public void setDurationLimitation(int i, int j) {
        mMinDurationLimit = i;
        mMaxDurationLimit = j;
    }

    private static final String COMMON_RINGTONE_SUFFIX[];
    public static final String RESOURCE_AUDIO_DURATION = "audio_duration";
    private Resource mDefaultOption;
    private Uri mDefaultUri;
    private int mMaxDurationLimit;
    private int mMinDurationLimit;
    private Resource mMuteOption;
    private int mRingtoneType;

    static  {
        String as[] = new String[2];
        as[0] = ".mp3";
        as[1] = ".ogg";
        COMMON_RINGTONE_SUFFIX = as;
    }
}
