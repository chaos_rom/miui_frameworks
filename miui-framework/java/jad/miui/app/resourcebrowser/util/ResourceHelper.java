// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser.util;

import android.app.*;
import android.content.*;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.os.FileUtils;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.view.Display;
import android.view.WindowManager;
import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import miui.app.resourcebrowser.*;
import miui.app.resourcebrowser.resource.Resource;
import miui.cache.FolderCache;
import miui.os.ExtraFileUtils;
import miui.widget.DataGroup;
import org.apache.http.entity.InputStreamEntity;

// Referenced classes of package miui.app.resourcebrowser.util:
//            ResourceDebug

public class ResourceHelper
    implements IntentConstants {

    private ResourceHelper() {
    }

    public static Bundle buildDefaultMetaData(Bundle bundle, String s, Context context) {
        if(!"android.intent.action.RINGTONE_PICKER".equals(s)) goto _L2; else goto _L1
_L1:
        LinkedHashSet linkedhashset;
        bundle.putInt("miui.app.resourcebrowser.DISPLAY_TYPE", 5);
        bundle.putBoolean("miui.app.resourcebrowser.CATEGORY_SUPPORTED", true);
        bundle.putString("miui.app.resourcebrowser.RESOURCE_SET_NAME", bundle.getString("android.intent.extra.ringtone.TITLE"));
        Uri uri = (Uri)bundle.getParcelable("android.intent.extra.ringtone.EXISTING_URI");
        Uri uri1 = (Uri)bundle.getParcelable("android.intent.extra.ringtone.DEFAULT_URI");
        if(uri != null) {
            String s1;
            String as[];
            String as1[];
            boolean flag;
            if(!uri.equals(uri1))
                flag = true;
            else
                flag = false;
            bundle.putString("miui.app.resourcebrowser.CURRENT_USING_PATH", getPathByUri(context, uri, flag));
        }
        if(bundle.getStringArray("miui.app.resourcebrowser.SOURCE_FOLDERS") != null) goto _L4; else goto _L3
_L3:
        linkedhashset = new LinkedHashSet();
        bundle.getInt("android.intent.extra.ringtone.TYPE", 7);
        JVM INSTR tableswitch 1 7: default 156
    //                   1 743
    //                   2 767
    //                   3 156
    //                   4 791
    //                   5 156
    //                   6 156
    //                   7 815;
           goto _L5 _L6 _L7 _L5 _L8 _L5 _L5 _L9
_L5:
        bundle.putStringArray("miui.app.resourcebrowser.SOURCE_FOLDERS", (String[])linkedhashset.toArray(new String[0]));
_L4:
        if(bundle.getInt("miui.app.resourcebrowser.DISPLAY_TYPE", -1) == -1)
            bundle.putInt("miui.app.resourcebrowser.DISPLAY_TYPE", 1);
        if(bundle.getString("miui.app.resourcebrowser.RESOURCE_SET_PACKAGE") == null)
            bundle.putString("miui.app.resourcebrowser.RESOURCE_SET_PACKAGE", context.getPackageName());
        if(bundle.getString("miui.app.resourcebrowser.RESOURCE_SET_NAME") == null)
            bundle.putString("miui.app.resourcebrowser.RESOURCE_SET_NAME", context.getString(0x60c0028));
        if(bundle.getInt("miui.app.resourcebrowser.RESOURCE_SET_CATEGORY", -1) == -1)
            bundle.putInt("miui.app.resourcebrowser.RESOURCE_SET_CATEGORY", 0);
        if(bundle.getString("miui.app.resourcebrowser.RESOURCE_SET_CODE") != null) goto _L11; else goto _L10
_L10:
        bundle.getInt("miui.app.resourcebrowser.RESOURCE_SET_CATEGORY");
        JVM INSTR tableswitch 0 2: default 296
    //                   0 978
    //                   1 989
    //                   2 1000;
           goto _L11 _L12 _L13 _L14
_L11:
        if(bundle.getStringArray("miui.app.resourcebrowser.PREVIEW_PREFIX") == null) {
            as1 = new String[1];
            as1[0] = (new StringBuilder()).append("preview_").append(bundle.getString("miui.app.resourcebrowser.RESOURCE_SET_CODE")).append("_").toString();
            bundle.putStringArray("miui.app.resourcebrowser.PREVIEW_PREFIX", as1);
        }
        if(bundle.getStringArray("miui.app.resourcebrowser.PREVIEW_PREFIX_INDICATOR") == null)
            bundle.putStringArray("miui.app.resourcebrowser.PREVIEW_PREFIX_INDICATOR", bundle.getStringArray("miui.app.resourcebrowser.PREVIEW_PREFIX"));
        if(bundle.getStringArray("miui.app.resourcebrowser.THUMBNAIL_PREFIX") == null)
            bundle.putStringArray("miui.app.resourcebrowser.THUMBNAIL_PREFIX", bundle.getStringArray("miui.app.resourcebrowser.PREVIEW_PREFIX"));
        if(bundle.getStringArray("miui.app.resourcebrowser.THUMBNAIL_PREFIX_INDICATOR") == null)
            bundle.putStringArray("miui.app.resourcebrowser.THUMBNAIL_PREFIX_INDICATOR", bundle.getStringArray("miui.app.resourcebrowser.THUMBNAIL_PREFIX"));
        if(bundle.getStringArray("miui.app.resourcebrowser.SOURCE_FOLDERS") == null) {
            as = new String[1];
            as[0] = (new StringBuilder()).append(ResourceConstants.MIUI_PATH).append(bundle.getString("miui.app.resourcebrowser.RESOURCE_SET_CODE")).toString();
            bundle.putStringArray("miui.app.resourcebrowser.SOURCE_FOLDERS", as);
        }
        if(bundle.getString("miui.app.resourcebrowser.DOWNLOAD_FOLDER") == null)
            bundle.putString("miui.app.resourcebrowser.DOWNLOAD_FOLDER", (new StringBuilder()).append(ResourceConstants.MIUI_PATH).append(bundle.getString("miui.app.resourcebrowser.RESOURCE_SET_CODE")).toString());
        if(bundle.getString("miui.app.resourcebrowser.CACHE_LIST_FOLDER") == null) {
            s1 = ResourceConstants.LIST_PATH;
            if(context.getFilesDir() != null)
                s1 = context.getFilesDir().getAbsolutePath();
            bundle.putString("miui.app.resourcebrowser.CACHE_LIST_FOLDER", s1);
        }
        if(bundle.getString("miui.app.resourcebrowser.RESOURCE_TYPE_PARAMETER") != null) goto _L16; else goto _L15
_L15:
        bundle.getInt("miui.app.resourcebrowser.RESOURCE_SET_CATEGORY");
        JVM INSTR tableswitch 0 2: default 584
    //                   0 1011
    //                   1 1030
    //                   2 1049;
           goto _L16 _L17 _L18 _L19
_L16:
        if(bundle.getString("miui.app.resourcebrowser.RESOURCE_FILE_EXTENSION") != null) goto _L21; else goto _L20
_L20:
        bundle.getInt("miui.app.resourcebrowser.RESOURCE_SET_CATEGORY");
        JVM INSTR tableswitch 0 2: default 624
    //                   0 1068
    //                   1 1079
    //                   2 1090;
           goto _L22 _L23 _L24 _L25
_L22:
        break; /* Loop/switch isn't completed */
_L25:
        break MISSING_BLOCK_LABEL_1090;
_L21:
        if(bundle.getString("miui.app.resourcebrowser.LOCAL_LIST_ACTIVITY_CLASS") == null) {
            bundle.putString("miui.app.resourcebrowser.LOCAL_LIST_ACTIVITY_CLASS", miui/app/resourcebrowser/LocalResourceListFragment.getName());
            if(bundle.getString("miui.app.resourcebrowser.LOCAL_LIST_ACTIVITY_PACKAGE") == null)
                bundle.putString("miui.app.resourcebrowser.LOCAL_LIST_ACTIVITY_PACKAGE", "android");
        } else
        if(bundle.getString("miui.app.resourcebrowser.LOCAL_LIST_ACTIVITY_PACKAGE") == null)
            bundle.putString("miui.app.resourcebrowser.LOCAL_LIST_ACTIVITY_PACKAGE", context.getPackageName());
        if(bundle.getString("miui.app.resourcebrowser.ONLINE_LIST_ACTIVITY_CLASS") == null) {
            bundle.putString("miui.app.resourcebrowser.ONLINE_LIST_ACTIVITY_CLASS", miui/app/resourcebrowser/OnlineResourceListFragment.getName());
            if(bundle.getString("miui.app.resourcebrowser.ONLINE_LIST_ACTIVITY_PACKAGE") == null)
                bundle.putString("miui.app.resourcebrowser.ONLINE_LIST_ACTIVITY_PACKAGE", "android");
        } else
        if(bundle.getString("miui.app.resourcebrowser.ONLINE_LIST_ACTIVITY_PACKAGE") == null)
            bundle.putString("miui.app.resourcebrowser.ONLINE_LIST_ACTIVITY_PACKAGE", context.getPackageName());
        if(bundle.getString("miui.app.resourcebrowser.DETAIL_ACTIVITY_CLASS") == null) {
            bundle.putString("miui.app.resourcebrowser.DETAIL_ACTIVITY_CLASS", miui/app/resourcebrowser/ResourceDetailActivity.getName());
            if(bundle.getString("miui.app.resourcebrowser.DETAIL_ACTIVITY_PACKAGE") == null)
                bundle.putString("miui.app.resourcebrowser.DETAIL_ACTIVITY_PACKAGE", "android");
        } else
        if(bundle.getString("miui.app.resourcebrowser.DETAIL_ACTIVITY_PACKAGE") == null)
            bundle.putString("miui.app.resourcebrowser.DETAIL_ACTIVITY_PACKAGE", context.getPackageName());
        return bundle;
_L6:
        linkedhashset.add(ResourceConstants.DOWNLOADED_RINGTONE_PATH);
        linkedhashset.add("/system/media/audio/ringtones");
          goto _L5
_L7:
        linkedhashset.add(ResourceConstants.DOWNLOADED_NOTIFICATION_PATH);
        linkedhashset.add("/system/media/audio/notifications");
          goto _L5
_L8:
        linkedhashset.add(ResourceConstants.DOWNLOADED_ALARM_PATH);
        linkedhashset.add("/system/media/audio/alarms");
          goto _L5
_L9:
        linkedhashset.add(ResourceConstants.DOWNLOADED_RINGTONE_PATH);
        linkedhashset.add(ResourceConstants.DOWNLOADED_NOTIFICATION_PATH);
        linkedhashset.add(ResourceConstants.DOWNLOADED_ALARM_PATH);
        linkedhashset.add("/system/media/audio/ringtones");
        linkedhashset.add("/system/media/audio/notifications");
        linkedhashset.add("/system/media/audio/alarms");
          goto _L5
_L2:
        if("android.intent.action.SET_WALLPAPER".equals(s)) {
            bundle.putBoolean("miui.app.resourcebrowser.USING_PICKER", false);
            bundle.putInt("miui.app.resourcebrowser.DISPLAY_TYPE", 6);
            bundle.putBoolean("miui.app.resourcebrowser.CATEGORY_SUPPORTED", true);
        } else
        if("android.intent.action.SET_LOCKSCREEN_WALLPAPER".equals(s)) {
            bundle.putBoolean("miui.app.resourcebrowser.USING_PICKER", false);
            bundle.putInt("miui.app.resourcebrowser.DISPLAY_TYPE", 7);
            bundle.putBoolean("miui.app.resourcebrowser.CATEGORY_SUPPORTED", true);
        } else
        if("android.intent.action.PICK_RESOURCE".equals(s))
            bundle.putBoolean("miui.app.resourcebrowser.USING_PICKER", true);
        else
            bundle.putBoolean("miui.app.resourcebrowser.USING_PICKER", false);
          goto _L4
_L12:
        bundle.putString("miui.app.resourcebrowser.RESOURCE_SET_CODE", "theme");
          goto _L11
_L13:
        bundle.putString("miui.app.resourcebrowser.RESOURCE_SET_CODE", "wallpaper");
          goto _L11
_L14:
        bundle.putString("miui.app.resourcebrowser.RESOURCE_SET_CODE", "ringtone");
          goto _L11
_L17:
        bundle.putString("miui.app.resourcebrowser.RESOURCE_TYPE_PARAMETER", "compound");
        bundle.putString("miui.app.resourcebrowser.RESOURCE_FILE_EXTENSION", ".mtz");
          goto _L16
_L18:
        bundle.putString("miui.app.resourcebrowser.RESOURCE_TYPE_PARAMETER", "wallpaper");
        bundle.putString("miui.app.resourcebrowser.RESOURCE_FILE_EXTENSION", ".jpg");
          goto _L16
_L19:
        bundle.putString("miui.app.resourcebrowser.RESOURCE_TYPE_PARAMETER", "ringtone");
        bundle.putString("miui.app.resourcebrowser.RESOURCE_FILE_EXTENSION", ".mp3");
          goto _L16
_L23:
        bundle.putString("miui.app.resourcebrowser.RESOURCE_FILE_EXTENSION", ".mtz");
          goto _L21
_L24:
        bundle.putString("miui.app.resourcebrowser.RESOURCE_FILE_EXTENSION", ".jpg");
          goto _L21
        bundle.putString("miui.app.resourcebrowser.RESOURCE_FILE_EXTENSION", ".mp3");
          goto _L21
    }

    public static void copyIntData(Bundle bundle, Bundle bundle1, String s) {
        bundle1.putInt(s, bundle.getInt(s));
    }

    public static void copyLongData(Bundle bundle, Bundle bundle1, String s) {
        bundle1.putLong(s, bundle.getLong(s));
    }

    public static void copyResourceInformation(Resource resource, Resource resource1) {
        Bundle bundle = resource.getInformation();
        Bundle bundle1 = resource1.getInformation();
        copyStringData(bundle, bundle1, "ONLINE_PATH");
        copyStringData(bundle, bundle1, "VERSION");
        copyIntData(bundle, bundle1, "PLATFORM_VERSION");
        copyLongData(bundle, bundle1, "MODIFIED_TIME");
        resource1.setInformation(bundle1);
    }

    public static void copyStringData(Bundle bundle, Bundle bundle1, String s) {
        bundle1.putString(s, bundle.getString(s));
    }

    public static void exit(final Activity activity) {
        AlertDialog alertdialog = (new android.app.AlertDialog.Builder(activity)).setIconAttribute(0x1010355).setTitle(0x60c0003).setMessage(0x60c0004).setPositiveButton(0x104000a, null).create();
        alertdialog.setOnDismissListener(new android.content.DialogInterface.OnDismissListener() {

            public void onDismiss(DialogInterface dialoginterface) {
                ((ActivityManager)activity.getSystemService("activity")).forceStopPackage(activity.getPackageName());
            }

            final Activity val$activity;

             {
                activity = activity1;
                super();
            }
        });
        alertdialog.show();
    }

    public static Uri getAndInsertMediaEntryByPath(Context context, String s) {
        if(!TextUtils.isEmpty(s)) goto _L2; else goto _L1
_L1:
        Uri uri = null;
_L4:
        return uri;
_L2:
        uri = getMediaEntryByPath(context, s);
        if(uri == null) {
            ContentValues contentvalues = new ContentValues();
            contentvalues.put("_data", s);
            contentvalues.put("is_music", Integer.valueOf(0));
            uri = context.getContentResolver().insert(android.provider.MediaStore.Audio.Media.getContentUriForPath(s), contentvalues);
        }
        if(true) goto _L4; else goto _L3
_L3:
    }

    public static String getContentColumnValue(Context context, Uri uri, String s) {
        String s1 = null;
        try {
            ContentResolver contentresolver = context.getContentResolver();
            String as[] = new String[1];
            as[0] = s;
            Cursor cursor = contentresolver.query(uri, as, null, null, null);
            if(cursor != null) {
                if(cursor.moveToFirst())
                    s1 = cursor.getString(0);
                cursor.close();
            }
        }
        catch(Exception exception) {
            exception.printStackTrace();
        }
        if(s1 == null)
            s1 = "";
        return s1;
    }

    public static int getDataPerLine(int i) {
        int j = 1;
        i;
        JVM INSTR tableswitch 1 11: default 60
    //                   1 62
    //                   2 62
    //                   3 62
    //                   4 62
    //                   5 62
    //                   6 67
    //                   7 72
    //                   8 72
    //                   9 72
    //                   10 67
    //                   11 67;
           goto _L1 _L2 _L2 _L2 _L2 _L2 _L3 _L4 _L4 _L4 _L3 _L3
_L1:
        return j;
_L2:
        j = 1;
        continue; /* Loop/switch isn't completed */
_L3:
        j = 2;
        continue; /* Loop/switch isn't completed */
_L4:
        j = 3;
        if(true) goto _L1; else goto _L5
_L5:
    }

    public static String getDefaultFormatPlayingRingtoneName(String s, int i, int j) {
        int k = s.lastIndexOf('.');
        if(k < 0)
            k = s.length();
        String s1 = s.substring(1 + s.lastIndexOf(File.separatorChar), k);
        Object aobj[] = new Object[3];
        aobj[0] = s1;
        aobj[1] = Integer.valueOf(i + 1);
        aobj[2] = Integer.valueOf(j);
        return String.format("%s (%d/%d)", aobj);
    }

    public static List getDefaultMusicPlayList(Context context, Resource resource) {
        ArrayList arraylist;
        String s;
        arraylist = new ArrayList();
        s = getPathByUri(context, getUriByPath(resource.getLocalPath()));
        if(!(new File(s)).exists()) goto _L2; else goto _L1
_L1:
        arraylist.add(s);
_L4:
        return arraylist;
_L2:
        String s1 = resource.getOnlineThumbnail(0);
        if(s1 != null)
            arraylist.add(s1);
        if(true) goto _L4; else goto _L3
_L3:
    }

    public static String getFileHash(String s) {
        String s1;
        BufferedInputStream bufferedinputstream;
        s1 = null;
        bufferedinputstream = null;
        BufferedInputStream bufferedinputstream1 = new BufferedInputStream(new FileInputStream(s));
        MessageDigest messagedigest;
        messagedigest = MessageDigest.getInstance("SHA1");
        byte abyte0[] = new byte[8192];
        do {
            int i = bufferedinputstream1.read(abyte0);
            if(i <= 0)
                break;
            messagedigest.update(abyte0, 0, i);
        } while(true);
          goto _L1
        NoSuchAlgorithmException nosuchalgorithmexception;
        nosuchalgorithmexception;
        bufferedinputstream = bufferedinputstream1;
_L8:
        nosuchalgorithmexception.printStackTrace();
        String s2;
        if(bufferedinputstream != null)
            try {
                bufferedinputstream.close();
            }
            catch(IOException ioexception4) {
                ioexception4.printStackTrace();
            }
_L2:
        return s1;
_L1:
        s2 = toHexString(messagedigest.digest());
        s1 = s2;
        if(bufferedinputstream1 != null)
            try {
                bufferedinputstream1.close();
            }
            catch(IOException ioexception5) {
                ioexception5.printStackTrace();
            }
        break MISSING_BLOCK_LABEL_78;
        FileNotFoundException filenotfoundexception;
        filenotfoundexception;
_L6:
        filenotfoundexception.printStackTrace();
        if(bufferedinputstream != null)
            try {
                bufferedinputstream.close();
            }
            catch(IOException ioexception1) {
                ioexception1.printStackTrace();
            }
          goto _L2
        IOException ioexception2;
        ioexception2;
_L5:
        ioexception2.printStackTrace();
        if(bufferedinputstream != null)
            try {
                bufferedinputstream.close();
            }
            catch(IOException ioexception3) {
                ioexception3.printStackTrace();
            }
          goto _L2
        Exception exception;
        exception;
_L4:
        if(bufferedinputstream != null)
            try {
                bufferedinputstream.close();
            }
            catch(IOException ioexception) {
                ioexception.printStackTrace();
            }
        throw exception;
        exception;
        bufferedinputstream = bufferedinputstream1;
        if(true) goto _L4; else goto _L3
_L3:
        ioexception2;
        bufferedinputstream = bufferedinputstream1;
          goto _L5
        filenotfoundexception;
        bufferedinputstream = bufferedinputstream1;
          goto _L6
        nosuchalgorithmexception;
        if(true) goto _L8; else goto _L7
_L7:
    }

    public static String getFileName(String s) {
        String as[] = s.split("/");
        return as[-1 + as.length];
    }

    public static String getFilePathByURL(String s, String s1) {
        ExtraFileUtils.mkdirs(new File(s), 511, -1, -1);
        String s2 = (String)mCacheFileNameMap.get(s1);
        if(TextUtils.isEmpty(s2)) {
            String as[] = s1.split("/");
            String s3;
            for(s3 = as[-1 + as.length]; s3.length() > 128; s3 = (new StringBuilder()).append(s3.substring(0, s3.length() / 2)).append(s3.hashCode()).toString());
            s2 = s3.replaceAll("\\?", "_");
            mCacheFileNameMap.put(s1, s2);
        }
        return (new StringBuilder()).append(ExtraFileUtils.standardizeFolderPath(s)).append(s2).toString();
    }

    public static miui.cache.FolderCache.FolderInfo getFolderInfoCache(String s) {
        return mFolderInfoCache.get(s);
    }

    public static String getFormattedSize(long l) {
        String s;
        if((double)l < 1048576D) {
            Object aobj1[] = new Object[1];
            aobj1[0] = Double.valueOf((double)l / 1024D);
            s = String.format("%.0fK", aobj1);
        } else {
            Object aobj[] = new Object[1];
            aobj[0] = Double.valueOf((double)l / 1048576D);
            s = String.format("%.1fM", aobj);
        }
        return s;
    }

    public static Uri getMediaEntryByPath(Context context, String s) {
        Uri uri = null;
        Uri uri1 = android.provider.MediaStore.Audio.Media.getContentUriForPath(s);
        ContentResolver contentresolver = context.getContentResolver();
        String as[] = new String[1];
        as[0] = "_id";
        String as1[] = new String[1];
        as1[0] = s;
        Cursor cursor = contentresolver.query(uri1, as, "_data=?", as1, null);
        if(cursor != null) {
            if(cursor.moveToFirst())
                uri = ContentUris.withAppendedId(uri1, cursor.getLong(0));
            cursor.close();
        }
        return uri;
    }

    public static String getPathByUri(Context context, Uri uri) {
        return getPathByUri(context, uri, true);
    }

    public static String getPathByUri(Context context, Uri uri, boolean flag) {
        if(uri != null) goto _L2; else goto _L1
_L1:
        String s = "";
_L9:
        return s;
_L2:
        String s1;
        s = uri.toString();
        s1 = uri.getScheme();
        if(!"content".equals(s1) || !flag) goto _L4; else goto _L3
_L3:
        String s2 = uri.getAuthority();
        if(!"settings".equals(s2)) goto _L6; else goto _L5
_L5:
        s = getContentColumnValue(context, uri, "value");
_L7:
        Uri uri1 = Uri.parse(s);
        if(uri1.getScheme() != null && !uri1.equals(uri))
            s = getPathByUri(context, uri1, true);
        continue; /* Loop/switch isn't completed */
_L6:
        if("media".equals(s2))
            s = getContentColumnValue(context, uri, "_data");
        if(true) goto _L7; else goto _L4
_L4:
        if("file".equals(s1))
            s = uri.getPath();
        if(true) goto _L9; else goto _L8
_L8:
    }

    public static int getPreviewWidth(int i) {
        return (i * 3) / 4;
    }

    public static int getThumbnailGap(Context context) {
        return context.getResources().getDimensionPixelSize(0x60a000b);
    }

    public static Pair getThumbnailSize(Activity activity, int i, int j) {
        int k;
        int l;
        k = 0;
        l = -1;
        i;
        JVM INSTR tableswitch 1 11: default 64
    //                   1 154
    //                   2 154
    //                   3 154
    //                   4 154
    //                   5 154
    //                   6 163
    //                   7 183
    //                   8 173
    //                   9 183
    //                   10 163
    //                   11 193;
           goto _L1 _L2 _L2 _L2 _L2 _L2 _L3 _L4 _L5 _L4 _L3 _L6
_L1:
        break; /* Loop/switch isn't completed */
_L6:
        break MISSING_BLOCK_LABEL_193;
_L7:
        int k1;
        int l1;
        if(l > 0) {
            Point point = new Point();
            activity.getWindowManager().getDefaultDisplay().getSize(point);
            int i1 = point.x;
            int j1 = getThumbnailGap(activity);
            k1 = (i1 - j - j1 * (k - 1)) / k;
            l1 = (int)activity.getResources().getFraction(l, k1, k1);
        } else
        if(l == -1) {
            k1 = -1;
            l1 = -1;
        } else {
            k1 = -2;
            l1 = -2;
        }
        return new Pair(Integer.valueOf(k1), Integer.valueOf(l1));
_L2:
        l = -1;
        k = 1;
          goto _L7
_L3:
        l = 0x6110004;
        k = 2;
          goto _L7
_L5:
        l = 0x6110003;
        k = 3;
          goto _L7
_L4:
        l = 0x6110002;
        k = 3;
          goto _L7
        l = 0x6110001;
        k = 2;
          goto _L7
    }

    public static int getThumbnailViewResource(int i) {
        int j = 0;
        i;
        JVM INSTR tableswitch 1 11: default 60
    //                   1 62
    //                   2 69
    //                   3 76
    //                   4 83
    //                   5 90
    //                   6 97
    //                   7 104
    //                   8 111
    //                   9 118
    //                   10 125
    //                   11 132;
           goto _L1 _L2 _L3 _L4 _L5 _L6 _L7 _L8 _L9 _L10 _L11 _L12
_L1:
        return j;
_L2:
        j = 0x6030009;
        continue; /* Loop/switch isn't completed */
_L3:
        j = 0x603000a;
        continue; /* Loop/switch isn't completed */
_L4:
        j = 0x603000b;
        continue; /* Loop/switch isn't completed */
_L5:
        j = 0x603000c;
        continue; /* Loop/switch isn't completed */
_L6:
        j = 0x603000d;
        continue; /* Loop/switch isn't completed */
_L7:
        j = 0x6030004;
        continue; /* Loop/switch isn't completed */
_L8:
        j = 0x6030003;
        continue; /* Loop/switch isn't completed */
_L9:
        j = 0x6030004;
        continue; /* Loop/switch isn't completed */
_L10:
        j = 0x6030008;
        continue; /* Loop/switch isn't completed */
_L11:
        j = 0x6030006;
        continue; /* Loop/switch isn't completed */
_L12:
        j = 0x6030007;
        if(true) goto _L1; else goto _L13
_L13:
    }

    public static Uri getUriByPath(String s) {
        if(!TextUtils.isEmpty(s)) goto _L2; else goto _L1
_L1:
        Uri uri = null;
_L4:
        return uri;
_L2:
        uri = Uri.parse(s);
        if(uri.getScheme() == null)
            uri = Uri.fromFile(new File(s));
        if(true) goto _L4; else goto _L3
_L3:
    }

    public static boolean isCacheValid(File file) {
        return isCacheValid(file, 0x927c0);
    }

    public static boolean isCacheValid(File file, int i) {
        boolean flag;
        if(System.currentTimeMillis() - file.lastModified() < (long)i)
            flag = true;
        else
            flag = false;
        return flag;
    }

    public static boolean isCombineView(int i) {
        boolean flag;
        if(i == 8 || i == 7 || i == 9)
            flag = true;
        else
            flag = false;
        return flag;
    }

    public static boolean isCompatiblePlatformVersion(int i, int j, int k) {
        boolean flag;
        if(j <= i && i <= k)
            flag = true;
        else
            flag = false;
        return flag;
    }

    public static boolean isDataResource(String s) {
        boolean flag;
        if(s != null && s.startsWith("/data"))
            flag = true;
        else
            flag = false;
        return flag;
    }

    public static boolean isMultipleView(int i) {
        boolean flag;
        if(i == 4)
            flag = true;
        else
            flag = false;
        return flag;
    }

    public static boolean isSingleView(int i) {
        boolean flag = true;
        if(i != flag && i != 6 && i != 2 && i != 3 && i != 5)
            flag = false;
        return flag;
    }

    public static boolean isSystemResource(String s) {
        boolean flag;
        if(s != null && s.startsWith("/system"))
            flag = true;
        else
            flag = false;
        return flag;
    }

    public static boolean isZipResource(String s) {
        boolean flag;
        flag = false;
        break MISSING_BLOCK_LABEL_2;
        while(true)  {
            do
                return flag;
            while(TextUtils.isEmpty(s) || !s.endsWith(".zip") && !s.endsWith(".mtz"));
            File file = new File(s);
            if(file.exists() && !file.isDirectory())
                flag = true;
        }
    }

    public static String moveUpdatedResourceInCacheDir(String s) {
        String s1 = s;
        if(!s.startsWith(ResourceConstants.CACHE_PATH)) goto _L2; else goto _L1
_L1:
        File file;
        File file1;
        file = new File(s);
        file1 = new File(s.substring(-1 + ResourceConstants.CACHE_PATH.length()));
        if(!file.exists())
            break MISSING_BLOCK_LABEL_129;
        if(ResourceDebug.DEBUG)
            Log.d("Theme", (new StringBuilder()).append("Move updated resource in data partition: ").append(file1.getAbsolutePath()).toString());
        file1.delete();
        BufferedInputStream bufferedinputstream = new BufferedInputStream(new FileInputStream(file), 8192);
        writeTo(bufferedinputstream, file1.getAbsolutePath());
        bufferedinputstream.close();
        file.delete();
_L4:
        s1 = file1.getAbsolutePath();
_L2:
        return s1;
        Exception exception;
        exception;
        if(ResourceDebug.DEBUG)
            Log.d("Theme", (new StringBuilder()).append("Move occur error: ").append(exception.toString()).toString());
        if(true) goto _L4; else goto _L3
_L3:
    }

    public static int resolveResourceStatus(String s) {
        byte byte0;
        if(!(new File(s)).exists())
            byte0 = 2;
        else
            byte0 = 0;
        return byte0;
    }

    public static void setMusicVolumeType(Activity activity, int i) {
        byte byte0;
        if(i == 1)
            byte0 = 2;
        else
        if(i == 2)
            byte0 = 5;
        else
        if(i == 4)
            byte0 = 4;
        else
        if(i == 32)
            byte0 = 3;
        else
            byte0 = 2;
        activity.setVolumeControlStream(byte0);
    }

    public static void setResourceStatus(String s, DataGroup datagroup, int i) {
        if(s != null) {
            String s1 = moveUpdatedResourceInCacheDir(s);
            for(int j = 0; j < datagroup.size(); j++) {
                Resource resource = (Resource)datagroup.get(j);
                if(s1.equals(resource.getLocalPath()))
                    resource.updateStatus(i);
            }

        }
    }

    public static String toHexString(byte abyte0[]) {
        StringBuilder stringbuilder = new StringBuilder();
        for(int i = 0; i < abyte0.length; i++) {
            Object aobj[] = new Object[1];
            aobj[0] = Integer.valueOf(0xff & abyte0[i]);
            stringbuilder.append(String.format("%02x", aobj));
        }

        return stringbuilder.toString();
    }

    public static void writeTo(InputStream inputstream, String s) {
        ExtraFileUtils.mkdirs((new File(s)).getParentFile(), 509, -1, -1);
        InputStreamEntity inputstreamentity = new InputStreamEntity(new BufferedInputStream(inputstream), -1L);
        BufferedOutputStream bufferedoutputstream = new BufferedOutputStream(new FileOutputStream(s));
        FileUtils.setPermissions(s, 509, -1, -1);
        inputstreamentity.writeTo(bufferedoutputstream);
        bufferedoutputstream.close();
        inputstream.close();
        (new File(s)).setLastModified(System.currentTimeMillis());
_L1:
        return;
        IOException ioexception;
        ioexception;
        ioexception.printStackTrace();
          goto _L1
    }

    private static Map mCacheFileNameMap = new HashMap();
    private static FolderCache mFolderInfoCache = new FolderCache();

}
