// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser.util;


// Referenced classes of package miui.app.resourcebrowser.util:
//            CleanCachePolicy

public class CleanCacheRunnable
    implements Runnable {

    public CleanCacheRunnable(String s) {
        setCacheDirPath(s);
    }

    public int getCleanFilesNumber() {
        return mStatDeleteNumber;
    }

    public void run() {
        if(mPolicy == null)
            mPolicy = new CleanCachePolicy.LRUCleanCacheByAccessTime(50);
        mStatDeleteNumber = mPolicy.cleanCache(mCacheDirPath);
    }

    public void setCacheDirPath(String s) {
        mCacheDirPath = s;
    }

    public void setCleanPolicy(CleanCachePolicy cleancachepolicy) {
        mPolicy = cleancachepolicy;
    }

    public String toString() {
        return (new StringBuilder()).append("Total delete ").append(mStatDeleteNumber).append(" files of ").append(mCacheDirPath).toString();
    }

    protected String mCacheDirPath;
    protected CleanCachePolicy mPolicy;
    private int mStatDeleteNumber;
}
