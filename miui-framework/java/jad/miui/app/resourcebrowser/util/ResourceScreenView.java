// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser.util;

import android.content.Context;
import android.util.AttributeSet;
import miui.widget.ScreenView;

public class ResourceScreenView extends ScreenView {
    public static interface ScreenChangeListener {

        public abstract void snapToScreen(int i);
    }


    public ResourceScreenView(Context context) {
        super(context);
    }

    public ResourceScreenView(Context context, AttributeSet attributeset) {
        super(context, attributeset);
    }

    public ResourceScreenView(Context context, AttributeSet attributeset, int i) {
        super(context, attributeset, i);
    }

    public void setScreenChangeListener(ScreenChangeListener screenchangelistener) {
        mScreenChangeListener = screenchangelistener;
    }

    protected void snapToScreen(int i, int j, boolean flag) {
        if(mScreenChangeListener != null)
            mScreenChangeListener.snapToScreen(i);
        super.snapToScreen(i, j, flag);
    }

    private ScreenChangeListener mScreenChangeListener;
}
