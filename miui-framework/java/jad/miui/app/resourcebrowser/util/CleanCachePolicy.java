// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser.util;

import android.os.FileUtils;
import android.text.TextUtils;
import java.io.File;
import java.util.*;

public abstract class CleanCachePolicy {
    public static class LRUCleanCacheByAccessTime extends CleanCachePolicy {

        protected int onCleanCacheFiles(List list) {
            if(mRemainNumber > 0 && mRemainNumber < list.size())
                Collections.sort(list, new Comparator() {

                    public volatile int compare(Object obj, Object obj1) {
                        return compare((FileEntry)obj, (FileEntry)obj1);
                    }

                    public int compare(FileEntry fileentry, FileEntry fileentry1) {
                        return (int)(fileentry.fileStatus.atime - fileentry1.fileStatus.atime);
                    }

                    final LRUCleanCacheByAccessTime this$0;

                 {
                    this$0 = LRUCleanCacheByAccessTime.this;
                    super();
                }
                });
            int i = 0;
            int j = 0;
            int k = list.size() - mRemainNumber;
            int l = 0;
            do {
label0:
                {
                    if(l < k) {
                        File file = new File(((FileEntry)list.get(l)).filePath);
                        if(file.isDirectory())
                            i++;
                        else
                            j++;
                        deleteFile(file);
                        if(i < mMaxDeleteDirNum && j < mMaxDeleteFileNum)
                            break label0;
                    }
                    return i + j;
                }
                l++;
            } while(true);
        }

        public void setMaxDeleteDirNumber(int i) {
            mMaxDeleteDirNum = i;
        }

        public void setMaxDeleteFileNumber(int i) {
            mMaxDeleteFileNum = i;
        }

        int mMaxDeleteDirNum;
        int mMaxDeleteFileNum;
        int mRemainNumber;

        public LRUCleanCacheByAccessTime(int i) {
            mMaxDeleteDirNum = 30;
            mMaxDeleteFileNum = 100;
            mRemainNumber = i;
        }
    }

    protected static class FileEntry {

        public boolean equals(Object obj) {
            boolean flag;
            if(obj instanceof FileEntry) {
                FileEntry fileentry = (FileEntry)obj;
                flag = TextUtils.equals(filePath, fileentry.filePath);
            } else {
                flag = false;
            }
            return flag;
        }

        public int hashCode() {
            return filePath.hashCode();
        }

        public String filePath;
        public android.os.FileUtils.FileStatus fileStatus;

        protected FileEntry() {
        }
    }


    public CleanCachePolicy() {
        mLeastKeepTime = 0x3f480;
    }

    protected static boolean deleteFile(File file) {
        boolean flag = true;
        if(file != null) goto _L2; else goto _L1
_L1:
        return flag;
_L2:
        if(file.isDirectory()) {
            File afile[] = file.listFiles();
            if(afile != null) {
                int i = afile.length;
                for(int j = 0; j < i; j++)
                    deleteFile(afile[j]);

            }
        }
        file.delete();
        if(file.exists())
            flag = false;
        if(true) goto _L1; else goto _L3
_L3:
    }

    protected static boolean deleteFile(String s) {
        boolean flag;
        if(s != null)
            flag = deleteFile(new File(s));
        else
            flag = false;
        return flag;
    }

    public final int cleanCache(String s) {
        preClean();
        int i = onCleanCacheFiles(onScanCacheFiles(s));
        postClean();
        return i;
    }

    protected abstract int onCleanCacheFiles(List list);

    protected List onScanCacheFiles(String s) {
        ArrayList arraylist = new ArrayList();
        if(!TextUtils.isEmpty(s)) {
            String as[] = (new File(s)).list();
            if(as != null) {
                long l = System.currentTimeMillis() / 1000L;
                int i = as.length;
                for(int j = 0; j < i; j++) {
                    String s1 = as[j];
                    FileEntry fileentry = new FileEntry();
                    fileentry.filePath = (new StringBuilder()).append(s).append(File.separator).append(s1).toString();
                    fileentry.fileStatus = new android.os.FileUtils.FileStatus();
                    FileUtils.getFileStatus(fileentry.filePath, fileentry.fileStatus);
                    if(l - fileentry.fileStatus.mtime > (long)mLeastKeepTime)
                        arraylist.add(fileentry);
                }

            }
        }
        return arraylist;
    }

    protected void postClean() {
    }

    protected void preClean() {
    }

    public void setLeastKeepTime(int i) {
        mLeastKeepTime = i;
    }

    private int mLeastKeepTime;
}
