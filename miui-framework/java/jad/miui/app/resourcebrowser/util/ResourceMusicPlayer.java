// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser.util;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;
import java.util.List;
import miui.app.resourcebrowser.resource.Resource;

// Referenced classes of package miui.app.resourcebrowser.util:
//            BatchMediaPlayer, ResourceHelper

public class ResourceMusicPlayer {
    public static interface PlayProgressListener {

        public abstract void onProgressUpdate(int i, int j);

        public abstract void onStartPlaying();

        public abstract void onStopPlaying();
    }


    public ResourceMusicPlayer(Activity activity, boolean flag) {
        mHandler = new Handler() {

            public void handleMessage(Message message) {
                if(message.what == 0 && mBatchPlayer != null && mProgressListener != null && mBatchPlayer.isPlaying()) {
                    mProgressListener.onProgressUpdate(mBatchPlayer.getPlayedDuration(), mBatchPlayer.getTotalDuration());
                    mHandler.sendEmptyMessageDelayed(0, 50L);
                }
            }

            final ResourceMusicPlayer this$0;

             {
                this$0 = ResourceMusicPlayer.this;
                super();
            }
        };
        mActivity = activity;
        mShowPlayButton = flag;
    }

    private void initPlayer() {
        mBatchPlayer = new BatchMediaPlayer(mActivity);
        mBatchPlayer.setListener(new BatchMediaPlayer.BatchPlayerListener() {

            public void finish(boolean flag) {
                mUserPlaying = false;
                mCurrentPlayingResource = null;
                if(mCurrentPlayingButton != null)
                    mCurrentPlayingButton.setImageResource(0x60200d9);
                mCurrentPlayingButton = null;
                if(mProgressListener != null)
                    mProgressListener.onStopPlaying();
                if(flag)
                    Toast.makeText(mActivity, 0x60c0020, 0).show();
            }

            public void play(String s, int i, int j) {
                if(mProgressListener != null) {
                    mProgressListener.onStartPlaying();
                    mHandler.sendEmptyMessageDelayed(0, 50L);
                }
            }

            final ResourceMusicPlayer this$0;

             {
                this$0 = ResourceMusicPlayer.this;
                super();
            }
        });
    }

    public boolean canPlay(Resource resource) {
        boolean flag;
        if(!resource.equals(mCurrentPlayingResource) && !TextUtils.isEmpty(resource.getLocalPath()))
            flag = true;
        else
            flag = false;
        return flag;
    }

    protected List getMusicPlayList(Resource resource) {
        return ResourceHelper.getDefaultMusicPlayList(mActivity, resource);
    }

    public void initPlayButtonIfNeed(final ImageView view, final Resource data) {
        if(mShowPlayButton && view != null) {
            if(data.equals(mCurrentPlayingResource))
                view.setImageResource(0x60200dc);
            else
                view.setImageResource(0x60200d9);
            view.setOnClickListener(new android.view.View.OnClickListener() {

                public void onClick(View view1) {
                    boolean flag;
                    if(!data.equals(mCurrentPlayingResource))
                        flag = true;
                    else
                        flag = false;
                    stopMusic();
                    if(flag && !TextUtils.isEmpty(data.getLocalPath()))
                        playMusic(view, data);
                }

                final ResourceMusicPlayer this$0;
                final Resource val$data;
                final ImageView val$view;

             {
                this$0 = ResourceMusicPlayer.this;
                data = resource;
                view = imageview;
                super();
            }
            });
        }
    }

    public boolean isPlaying() {
        return mUserPlaying;
    }

    public void playMusic(ImageView imageview, Resource resource) {
        if(!mShowPlayButton) {
            throw new IllegalArgumentException("ResourceMusicPlayer does not support playing button.");
        } else {
            imageview.setImageResource(0x60200dc);
            mCurrentPlayingButton = imageview;
            playMusic(resource);
            return;
        }
    }

    public void playMusic(Resource resource) {
        mCurrentPlayingResource = resource;
        if(mBatchPlayer == null)
            initPlayer();
        mBatchPlayer.setPlayList(getMusicPlayList(resource));
        mBatchPlayer.start();
        mUserPlaying = true;
    }

    public void regeistePlayProgressListener(PlayProgressListener playprogresslistener) {
        mProgressListener = playprogresslistener;
    }

    public void stopMusic() {
        if(mBatchPlayer != null)
            mBatchPlayer.stop();
        mCurrentPlayingResource = null;
        mUserPlaying = false;
    }

    private static final int INTERVAL_UPDATE_PLAY_PROGRESS = 50;
    private static final int MSG_UPDATE_PLAY_PROGRESS;
    private Activity mActivity;
    private BatchMediaPlayer mBatchPlayer;
    protected ImageView mCurrentPlayingButton;
    private Resource mCurrentPlayingResource;
    private Handler mHandler;
    private PlayProgressListener mProgressListener;
    private boolean mShowPlayButton;
    private boolean mUserPlaying;






/*
    static Resource access$302(ResourceMusicPlayer resourcemusicplayer, Resource resource) {
        resourcemusicplayer.mCurrentPlayingResource = resource;
        return resource;
    }

*/


/*
    static boolean access$402(ResourceMusicPlayer resourcemusicplayer, boolean flag) {
        resourcemusicplayer.mUserPlaying = flag;
        return flag;
    }

*/

}
