// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser.util;

import java.io.InputStream;
import java.util.HashMap;
import java.util.zip.ZipFile;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class ZipFileHelper {
    private static class DescriptionSAXHandler extends DefaultHandler {

        public void characters(char ac[], int i, int j) throws SAXException {
            value = new String(ac, i, j);
        }

        public void endElement(String s, String s1, String s2) throws SAXException {
            nvp.put(s1, value);
        }

        public HashMap getElementEntries() {
            return nvp;
        }

        private HashMap nvp;
        private String value;

        private DescriptionSAXHandler() {
            nvp = new HashMap();
        }

    }


    public ZipFileHelper() {
    }

    public static HashMap getZipResourceDescribeInfo(String s, String s1) {
        HashMap hashmap = null;
        try {
            ZipFile zipfile = new ZipFile(s);
            hashmap = getZipResourceDescribeInfo(zipfile, s1);
            zipfile.close();
        }
        catch(Exception exception) { }
        return hashmap;
    }

    public static HashMap getZipResourceDescribeInfo(ZipFile zipfile, String s) {
        HashMap hashmap;
        DescriptionSAXHandler descriptionsaxhandler;
        hashmap = null;
        descriptionsaxhandler = null;
        java.util.zip.ZipEntry zipentry = zipfile.getEntry(s);
        if(zipentry == null) goto _L2; else goto _L1
_L1:
        InputStream inputstream = zipfile.getInputStream(zipentry);
_L5:
        if(inputstream == null) goto _L4; else goto _L3
_L3:
        SAXParser saxparser;
        DescriptionSAXHandler descriptionsaxhandler1;
        saxparser = SAXParserFactory.newInstance().newSAXParser();
        descriptionsaxhandler1 = new DescriptionSAXHandler();
        saxparser.parse(inputstream, descriptionsaxhandler1);
        inputstream.close();
        descriptionsaxhandler = descriptionsaxhandler1;
_L4:
        if(descriptionsaxhandler != null)
            hashmap = descriptionsaxhandler.getElementEntries();
        return hashmap;
_L2:
        inputstream = null;
          goto _L5
        Exception exception;
        exception;
          goto _L4
        Exception exception1;
        exception1;
        descriptionsaxhandler = descriptionsaxhandler1;
          goto _L4
    }
}
