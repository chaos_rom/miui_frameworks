// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser.util;

import android.os.AsyncTask;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import miui.app.resourcebrowser.service.online.OnlineService;

// Referenced classes of package miui.app.resourcebrowser.util:
//            ResourceHelper

public class DownloadFileTask extends AsyncTask {
    public static class DownloadFileEntry {

        public boolean equals(Object obj) {
            boolean flag = true;
            if(this != obj) goto _L2; else goto _L1
_L1:
            return flag;
_L2:
            if(obj == null) {
                flag = false;
                continue; /* Loop/switch isn't completed */
            }
            if(getClass() != obj.getClass()) {
                flag = false;
                continue; /* Loop/switch isn't completed */
            }
            DownloadFileEntry downloadfileentry = (DownloadFileEntry)obj;
            if(path == null) {
                if(downloadfileentry.path != null) {
                    flag = false;
                    continue; /* Loop/switch isn't completed */
                }
            } else
            if(!path.equals(downloadfileentry.path)) {
                flag = false;
                continue; /* Loop/switch isn't completed */
            }
            if(url == null) {
                if(downloadfileentry.url != null)
                    flag = false;
            } else
            if(!url.equals(downloadfileentry.url))
                flag = false;
            if(true) goto _L1; else goto _L3
_L3:
        }

        public int getIndex() {
            return index;
        }

        public String getPath() {
            return path;
        }

        public String getUrl() {
            return url;
        }

        public int hashCode() {
            int i = 0;
            int j;
            int k;
            if(path == null)
                j = 0;
            else
                j = path.hashCode();
            k = 31 * (j + 31);
            if(url != null)
                i = url.hashCode();
            return k + i;
        }

        public void setIndex(int i) {
            index = i;
        }

        public void setPath(String s) {
            path = s;
        }

        public void setUrl(String s) {
            url = s;
        }

        private int index;
        private String path;
        private String url;

        public DownloadFileEntry() {
        }
    }


    public DownloadFileTask() {
        mId = Integer.valueOf(super.hashCode()).toString();
    }

    private DownloadFileEntry handleDownloadFile(DownloadFileEntry downloadfileentry) {
        String s;
        String s1;
        File file;
        File file1;
        s = downloadfileentry.getUrl();
        s1 = downloadfileentry.getPath();
        file = new File(s1);
        file1 = new File((new StringBuilder()).append(s1).append(".temp").toString());
        writeToFile(s, file1);
        file1.renameTo(file);
        downloadfileentry.setPath(s1);
_L2:
        return downloadfileentry;
        Exception exception;
        exception;
        exception.printStackTrace();
        downloadfileentry = null;
        if(file1.exists())
            file1.delete();
        if(true) goto _L2; else goto _L1
_L1:
    }

    private void writeToFile(String s, File file) throws IOException {
        ResourceHelper.writeTo(OnlineService.getInstance().getUrlInputStream(s), file.getAbsolutePath());
    }

    protected volatile Object doInBackground(Object aobj[]) {
        return doInBackground((DownloadFileEntry[])aobj);
    }

    protected transient List doInBackground(DownloadFileEntry adownloadfileentry[]) {
        List list = downloadFiles(adownloadfileentry);
        publishProgress(list.toArray(new DownloadFileEntry[0]));
        return list;
    }

    public transient List doInForeground(DownloadFileEntry adownloadfileentry[]) {
        return downloadFiles(adownloadfileentry);
    }

    protected transient List downloadFiles(DownloadFileEntry adownloadfileentry[]) {
        ArrayList arraylist = new ArrayList();
        for(int i = 0; i < adownloadfileentry.length; i++) {
            DownloadFileEntry downloadfileentry = handleDownloadFile(adownloadfileentry[i]);
            if(downloadfileentry != null) {
                downloadfileentry.setIndex(i);
                arraylist.add(downloadfileentry);
            }
        }

        return arraylist;
    }

    public boolean equals(Object obj) {
        boolean flag;
        if(obj == null || !(obj instanceof DownloadFileTask))
            flag = false;
        else
            flag = ((DownloadFileTask)obj).getId().equals(getId());
        return flag;
    }

    public String getId() {
        return mId;
    }

    public int hashCode() {
        return getId().hashCode();
    }

    public void setId(String s) {
        mId = s;
    }

    private static final int BUFFER_SIZE = 1024;
    private static final String SUFFIX = ".temp";
    private String mId;
}
