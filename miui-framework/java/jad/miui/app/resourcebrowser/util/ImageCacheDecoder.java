// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser.util;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import java.io.File;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import miui.util.ImageUtils;
import miui.util.InputStreamLoader;

// Referenced classes of package miui.app.resourcebrowser.util:
//            ResourceDebug, ResourceHelper, DownloadFileTask

public class ImageCacheDecoder {
    protected class BitmapCache {

        private int getNextRemoveCachePosition(int i) {
            int j = mIndexList.indexOf(Integer.valueOf(i));
            if(j < 0 && (i == mCurrentIndex || isFull())) {
                int k;
                if(mIndexList.isEmpty())
                    j = -1;
                else
                    j = 0;
                for(k = 1; k < mIndexList.size(); k++)
                    if(Math.abs(mCurrentIndex - ((Integer)mIndexList.get(k)).intValue()) > Math.abs(mCurrentIndex - ((Integer)mIndexList.get(j)).intValue()))
                        j = k;

            }
            return j;
        }

        public void add(String s, Bitmap bitmap, int i) {
            if(!mCache.containsKey(s)) {
                removeIdleCache(false);
                mCache.put(s, bitmap);
                mIdList.add(s);
                mIndexList.add(Integer.valueOf(i));
            }
        }

        public void clean() {
            Iterator iterator = mIdList.iterator();
            do {
                if(!iterator.hasNext())
                    break;
                String s = (String)iterator.next();
                Bitmap bitmap = (Bitmap)mCache.get(s);
                if(bitmap != null)
                    bitmap.recycle();
            } while(true);
            mCache.clear();
            mIdList.clear();
            mIndexList.clear();
        }

        public Bitmap getBitmap(String s) {
            return (Bitmap)mCache.get(s);
        }

        public int getCurrentUseIndex() {
            return mCurrentIndex;
        }

        public boolean inCacheScope(int i) {
            boolean flag;
            if(Math.abs(i - mCurrentIndex) <= mCacheCapacity / 2)
                flag = true;
            else
                flag = false;
            return flag;
        }

        public boolean isFull() {
            boolean flag;
            if(mCache.size() >= mCacheCapacity)
                flag = true;
            else
                flag = false;
            return flag;
        }

        public Bitmap removeIdleCache(boolean flag) {
            return removeIdleCache(flag, -999);
        }

        public Bitmap removeIdleCache(boolean flag, int i) {
            Bitmap bitmap = null;
            int j = getNextRemoveCachePosition(i);
            if(j >= 0 && j < mIndexList.size()) {
                Bitmap bitmap1 = (Bitmap)mCache.remove(mIdList.get(j));
                if(bitmap1 != null)
                    if(flag)
                        bitmap = bitmap1;
                    else
                        bitmap1.recycle();
                mIdList.remove(j);
                mIndexList.remove(j);
            }
            return bitmap;
        }

        public void setCacheSize(int i) {
            if(i > 1 && i != mCacheCapacity) {
                if((i & 1) == 0)
                    i++;
                mCacheCapacity = i;
            }
        }

        public void setCurrentUseIndex(int i) {
            mCurrentIndex = i;
        }

        private static final int MAGIC_INDEX = -999;
        private Map mCache;
        private int mCacheCapacity;
        private int mCurrentIndex;
        ArrayList mIdList;
        ArrayList mIndexList;
        final ImageCacheDecoder this$0;

        public BitmapCache(int i) {
            this$0 = ImageCacheDecoder.this;
            super();
            setCacheSize(Math.max(3, i));
            mCache = Collections.synchronizedMap(new HashMap(mCacheCapacity));
            mIdList = new ArrayList(mCacheCapacity);
            mIndexList = new ArrayList(mCacheCapacity);
        }
    }

    public static interface ImageDecodingListener {

        public abstract void handleDecodingResult(boolean flag, String s, String s1);

        public abstract void handleDownloadResult(boolean flag, String s, String s1);
    }

    private class JobRun
        implements Runnable {

        public void dispatchJob() {
            if(downloadingJob)
                mDownloadExecutorService.submit(this);
            else
                mDecodeExecutorService.submit(this);
        }

        public void run() {
            boolean flag = false;
            if(mBitmapCache.inCacheScope(useIndex)) {
                long l;
                if(ResourceDebug.DEBUG)
                    l = System.currentTimeMillis();
                else
                    l = 0L;
                if(downloadingJob) {
                    if(TextUtils.isEmpty(imageOnlinePath)) {
                        flag = false;
                    } else {
                        DownloadFileTask downloadfiletask = new DownloadFileTask();
                        DownloadFileTask.DownloadFileEntry downloadfileentry = new DownloadFileTask.DownloadFileEntry();
                        downloadfileentry.setPath(imageLocalPath);
                        downloadfileentry.setUrl(imageOnlinePath);
                        DownloadFileTask.DownloadFileEntry adownloadfileentry[] = new DownloadFileTask.DownloadFileEntry[1];
                        adownloadfileentry[0] = downloadfileentry;
                        downloadfiletask.doInForeground(adownloadfileentry);
                        File file = new File(imageLocalPath);
                        if(file.exists() && file.length() < 1024L)
                            file.delete();
                        flag = file.exists();
                        mFailToDownloadTime.remove(imageOnlinePath);
                        if(!flag)
                            mFailToDownloadTime.put(imageOnlinePath, Long.valueOf(System.currentTimeMillis()));
                    }
                } else
                if(decodeLocalImage(imageLocalPath, useIndex, true) != null)
                    flag = true;
                else
                    flag = false;
                if(ResourceDebug.DEBUG) {
                    long l1 = System.currentTimeMillis();
                    long l2 = (new File(imageLocalPath)).length();
                    String s1 = "";
                    if(downloadingJob) {
                        Object aobj[] = new Object[1];
                        aobj[0] = Float.valueOf(0.976F * (float)(l2 / (l1 - l)));
                        s1 = String.format("%.1fKB/s", aobj);
                    }
                    StringBuilder stringbuilder1 = (new StringBuilder()).append("finish loading wallpaper: ").append(useIndex).append(" ");
                    Message message;
                    String s2;
                    StringBuilder stringbuilder2;
                    String s3;
                    if(flag)
                        s2 = "success";
                    else
                        s2 = "fail   ";
                    stringbuilder2 = stringbuilder1.append(s2).append(" ");
                    if(downloadingJob)
                        s3 = "online";
                    else
                        s3 = "local ";
                    Log.d("Theme", stringbuilder2.append(s3).append(" ").append(ResourceHelper.getFormattedSize(l2)).append(" ").append(l1 - submitTimeForDebug).append(" ").append(l1 - l).append(" ").append(s1).toString());
                }
            } else
            if(ResourceDebug.DEBUG) {
                StringBuilder stringbuilder = (new StringBuilder()).append("ingore loading wallpaper: ").append(useIndex);
                String s;
                if(downloadingJob)
                    s = " online ";
                else
                    s = " local";
                Log.d("Theme", stringbuilder.append(s).toString());
            }
            mDoingJob.remove(imageLocalPath);
            if(mBitmapCache.inCacheScope(useIndex)) {
                message = mHandler.obtainMessage();
                int i;
                int j;
                if(downloadingJob)
                    i = 1;
                else
                    i = 0;
                message.what = i;
                if(flag)
                    j = 0;
                else
                    j = 1;
                message.arg1 = j;
                message.obj = new Pair(imageLocalPath, imageOnlinePath);
                mHandler.sendMessage(message);
            }
        }

        private final boolean downloadingJob;
        private String imageLocalPath;
        private String imageOnlinePath;
        private long submitTimeForDebug;
        final ImageCacheDecoder this$0;
        private int useIndex;

        public JobRun(String s, String s1, int i) {
            this$0 = ImageCacheDecoder.this;
            super();
            imageLocalPath = s;
            imageOnlinePath = s1;
            useIndex = i;
            boolean flag;
            if(!(new File(s)).exists())
                flag = true;
            else
                flag = false;
            downloadingJob = flag;
            if(ResourceDebug.DEBUG) {
                submitTimeForDebug = System.currentTimeMillis();
                StringBuilder stringbuilder = (new StringBuilder()).append("submit loading wallpaper: ").append(i);
                String s2;
                if(downloadingJob)
                    s2 = (new StringBuilder()).append(" online ").append(s1).toString();
                else
                    s2 = (new StringBuilder()).append(" local ").append(s).toString();
                Log.d("Theme", stringbuilder.append(s2).toString());
            }
        }
    }


    public ImageCacheDecoder() {
        this(3);
    }

    public ImageCacheDecoder(int i) {
        mDoingJob = new HashSet();
        mDecodeExecutorService = Executors.newFixedThreadPool(2);
        mDownloadExecutorService = Executors.newFixedThreadPool(4);
        mFailToDownloadTime = new HashMap();
        mListener = null;
        mBitmapCache = new BitmapCache(i);
    }

    public void clean(boolean flag) {
        mBitmapCache.clean();
        mFailToDownloadTime.clear();
        if(flag) {
            mDecodeExecutorService.shutdown();
            mDownloadExecutorService.shutdown();
            mHandler.removeMessages(0);
            mHandler.removeMessages(1);
        }
    }

    public void decodeImageAsync(String s, String s1, int i) {
        if(!TextUtils.isEmpty(s) && mBitmapCache.getBitmap(s) == null && !mDoingJob.contains(s) && !mDecodeExecutorService.isShutdown()) goto _L2; else goto _L1
_L1:
        return;
_L2:
        Long long1 = (Long)mFailToDownloadTime.get(s1);
        if(long1 == null || System.currentTimeMillis() - long1.longValue() >= 5000L) {
            mDoingJob.add(s);
            (new JobRun(s, s1, i)).dispatchJob();
        }
        if(true) goto _L1; else goto _L3
_L3:
    }

    public Bitmap decodeLocalImage(String s, int i, boolean flag) {
        Bitmap bitmap = getBitmap(s);
        if(bitmap == null) {
            bitmap = ImageUtils.getBitmap(new InputStreamLoader(s), mDecodedWidth, mDecodedHeight, mBitmapCache.removeIdleCache(true));
            if(bitmap != null && flag)
                mBitmapCache.add(s, bitmap, i);
        }
        return bitmap;
    }

    public Bitmap getBitmap(String s) {
        return mBitmapCache.getBitmap(s);
    }

    public int getCurrentUseBitmapIndex() {
        return mBitmapCache.getCurrentUseIndex();
    }

    public void regeisterListener(ImageDecodingListener imagedecodinglistener) {
        mListener = imagedecodinglistener;
    }

    public void setCacheCapacity(int i) {
        mBitmapCache.setCacheSize(i);
    }

    public void setCurrentUseBitmapIndex(int i) {
        mBitmapCache.setCurrentUseIndex(i);
    }

    public void setScaledSize(int i, int j) {
        mDecodedWidth = i;
        mDecodedHeight = j;
    }

    private static final int MSG_ASYNC_DECODING_FINISH = 0;
    private static final int MSG_ASYNC_DOWNLOAD_FINISH = 1;
    protected BitmapCache mBitmapCache;
    private ExecutorService mDecodeExecutorService;
    protected int mDecodedHeight;
    protected int mDecodedWidth;
    private HashSet mDoingJob;
    private ExecutorService mDownloadExecutorService;
    private HashMap mFailToDownloadTime;
    private Handler mHandler = new Handler() {

        public void handleMessage(Message message) {
            Pair pair;
            boolean flag;
            pair = (Pair)message.obj;
            if(message.arg1 == 0)
                flag = true;
            else
                flag = false;
            if(message.what != 0) goto _L2; else goto _L1
_L1:
            if(mListener != null)
                mListener.handleDecodingResult(flag, (String)pair.first, (String)pair.second);
_L4:
            return;
_L2:
            if(message.what == 1 && mListener != null)
                mListener.handleDownloadResult(flag, (String)pair.first, (String)pair.second);
            if(true) goto _L4; else goto _L3
_L3:
        }

        final ImageCacheDecoder this$0;

             {
                this$0 = ImageCacheDecoder.this;
                super();
            }
    };
    private ImageDecodingListener mListener;






}
