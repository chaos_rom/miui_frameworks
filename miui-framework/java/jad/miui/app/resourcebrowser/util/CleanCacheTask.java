// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser.util;

import android.os.Looper;
import android.os.MessageQueue;
import android.util.Log;
import java.io.File;
import java.util.*;
import miui.app.resourcebrowser.ResourceConstants;

// Referenced classes of package miui.app.resourcebrowser.util:
//            CleanCacheRunnable, CleanCachePolicy

public class CleanCacheTask {
    public static class ResourcePreviewCleanPolicy extends CleanCachePolicy {

        private boolean existResource(String s) {
            boolean flag;
            int i;
            flag = false;
            i = -1;
            if(!s.startsWith(sDecodedSystemBasePath)) goto _L2; else goto _L1
_L1:
            i = sDecodedSystemBasePath.length();
_L4:
            int j;
            if(i > 0)
                do {
                    j = s.indexOf('_', i);
                    if(j <= 0)
                        break;
                    i = j + 1;
                    if(!(new File((new StringBuilder()).append(s.substring(0, i).replace('_', '/')).append(s.substring(i)).toString())).exists())
                        continue;
                    flag = true;
                    break;
                } while(true);
            return flag;
_L2:
            if(s.startsWith(sDecodedDownloadBasePath))
                i = sDecodedDownloadBasePath.length();
            if(true) goto _L4; else goto _L3
_L3:
        }

        protected int onCleanCacheFiles(List list) {
            int i = 0;
            Iterator iterator = list.iterator();
            do {
                if(!iterator.hasNext())
                    break;
                File file = new File(((CleanCachePolicy.FileEntry)iterator.next()).filePath);
                if(!existResource(file.getName())) {
                    deleteFile(file);
                    i++;
                }
            } while(true);
            return i;
        }

        private static final String sDecodedDownloadBasePath;
        private static final String sDecodedSystemBasePath = "/system/media/".replace('/', '_');

        static  {
            sDecodedDownloadBasePath = ResourceConstants.DOWNLOADED_RESOURCE_BASE_PATH.replace('/', '_');
        }

        public ResourcePreviewCleanPolicy() {
        }
    }

    public static class ResourceCacheCleanPolicy extends CleanCachePolicy.LRUCleanCacheByAccessTime {

        protected int onCleanCacheFiles(List list) {
            int i = super.onCleanCacheFiles(list);
            for(Iterator iterator = mSubDirCacheClean.iterator(); iterator.hasNext();) {
                CleanCacheRunnable cleancacherunnable = (CleanCacheRunnable)iterator.next();
                cleancacherunnable.run();
                i += cleancacherunnable.getCleanFilesNumber();
            }

            return i;
        }

        protected List onScanCacheFiles(String s) {
            List list = super.onScanCacheFiles(s);
            for(int i = -1 + list.size(); i >= 0; i--) {
                File file = new File(((CleanCachePolicy.FileEntry)list.get(i)).filePath);
                if(file.isDirectory()) {
                    list.remove(i);
                    CleanCacheRunnable cleancacherunnable = new CleanCacheRunnable(file.getAbsolutePath());
                    cleancacherunnable.setCleanPolicy(new CleanCachePolicy.LRUCleanCacheByAccessTime(super.mRemainNumber));
                    mSubDirCacheClean.add(cleancacherunnable);
                }
            }

            return list;
        }

        List mSubDirCacheClean;

        public ResourceCacheCleanPolicy(int i) {
            super(i);
            mSubDirCacheClean = new ArrayList();
        }
    }


    private CleanCacheTask() {
        mCleanRunnable = new ArrayList();
    }

    public static CleanCacheTask getInstance() {
        if(sCleanTask != null) goto _L2; else goto _L1
_L1:
        miui/app/resourcebrowser/util/CleanCacheTask;
        JVM INSTR monitorenter ;
        if(sCleanTask == null)
            sCleanTask = new CleanCacheTask();
        miui/app/resourcebrowser/util/CleanCacheTask;
        JVM INSTR monitorexit ;
_L2:
        return sCleanTask;
        Exception exception;
        exception;
        miui/app/resourcebrowser/util/CleanCacheTask;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public void addCleanRunnable(CleanCacheRunnable cleancacherunnable) {
        if(cleancacherunnable != null)
            mCleanRunnable.add(cleancacherunnable);
    }

    protected void initCleanTask() {
        String as[] = ResourceConstants.CACHE_FILE_PATHS;
        int ai[] = ResourceConstants.CACHE_FILE_MAX_NUMBER;
        int i = 0;
        while(i < as.length)  {
            CleanCacheRunnable cleancacherunnable = new CleanCacheRunnable(as[i]);
            if(as[i].equals(ResourceConstants.PREVIEW_PATH))
                cleancacherunnable.setCleanPolicy(new ResourcePreviewCleanPolicy());
            else
                cleancacherunnable.setCleanPolicy(new ResourceCacheCleanPolicy(ai[i]));
            addCleanRunnable(cleancacherunnable);
            i++;
        }
    }

    protected void initScheduler() {
        mIdleHandler = new android.os.MessageQueue.IdleHandler() {

            public boolean queueIdle() {
                if(!mCleanRunnable.isEmpty())
                    (new Thread() {

                        public void run() {
                            clean.run();
                            Log.i("clean", (new StringBuilder()).append("clean cache: ").append(clean.toString()).toString());
                            if(!mCleanRunnable.isEmpty())
                                Looper.getMainLooper().getQueue().addIdleHandler(mIdleHandler);
                        }

                        final _cls1 this$1;
                        final CleanCacheRunnable val$clean;

                     {
                        this$1 = _cls1.this;
                        clean = cleancacherunnable;
                        super();
                    }
                    }).start();
                return false;
            }

            final CleanCacheTask this$0;

             {
                this$0 = CleanCacheTask.this;
                super();
            }
        };
        Looper.getMainLooper().getQueue().addIdleHandler(mIdleHandler);
    }

    public void run() {
        if(mIdleHandler == null) {
            initCleanTask();
            initScheduler();
        }
    }

    private static CleanCacheTask sCleanTask;
    protected List mCleanRunnable;
    private android.os.MessageQueue.IdleHandler mIdleHandler;

}
