// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser.util;

import android.app.DownloadManager;
import android.app.MiuiDownloadManager;
import android.content.*;
import android.database.Cursor;
import android.net.Uri;
import android.net.WebAddress;
import android.util.Log;
import android.webkit.MimeTypeMap;
import android.webkit.URLUtil;
import java.io.File;
import java.net.URI;
import miui.os.ExtraFileUtils;

// Referenced classes of package miui.app.resourcebrowser.util:
//            ResourceDebug

public class ResourceDownloadHandler {

    public ResourceDownloadHandler(Context context) {
        mHasRegistDownloadReceiver = false;
        mDownloadReceiver = new BroadcastReceiver() {

            public void onReceive(Context context1, Intent intent) {
                android.app.MiuiDownloadManager.Query query;
                long l;
                query = new android.app.MiuiDownloadManager.Query();
                l = intent.getLongExtra("extra_download_id", -1L);
                if(l == -1L) goto _L2; else goto _L1
_L1:
                Cursor cursor;
                long al[] = new long[1];
                al[0] = l;
                query.setFilterById(al);
                cursor = mDownloadManager.query(query);
                if(cursor == null || !cursor.moveToFirst()) goto _L4; else goto _L3
_L3:
                String s;
                String s1;
                s = Uri.decode(Uri.parse(cursor.getString(cursor.getColumnIndex("local_uri"))).getEncodedPath());
                s1 = intent.getAction();
                if(!"android.intent.action.DOWNLOAD_COMPLETE".equals(s1)) goto _L6; else goto _L5
_L5:
                if(!MiuiDownloadManager.isDownloadSuccess(cursor)) goto _L8; else goto _L7
_L7:
                handleDownloadSuccessed(s);
_L4:
                if(cursor != null)
                    cursor.close();
_L2:
                return;
_L8:
                if(intent.getIntExtra("extra_download_status", 16) == 16)
                    handleDownloadFailed(s);
                continue; /* Loop/switch isn't completed */
_L6:
                if("android.intent.action.DOWNLOAD_UPDATED".equals(s1)) {
                    int i = (int)intent.getLongExtra("extra_download_current_bytes", 0L);
                    int j = (int)intent.getLongExtra("extra_download_total_bytes", 1L);
                    handleDownloadProgress(s, i, j);
                }
                if(true) goto _L4; else goto _L9
_L9:
            }

            final ResourceDownloadHandler this$0;

             {
                this$0 = ResourceDownloadHandler.this;
                super();
            }
        };
        mContext = context;
        mDownloadManager = (DownloadManager)mContext.getSystemService("download");
    }

    public static long downloadResource(DownloadManager downloadmanager, String s, String s1, String s2) {
        long l;
        if(getUriFromUrl(s) == null) {
            l = -1L;
        } else {
            if(ResourceDebug.DEBUG)
                Log.d("Theme", (new StringBuilder()).append("Download resource url= ").append(s).append(" savePath=").append(s1).toString());
            File file = new File(s1);
            ExtraFileUtils.mkdirs(file.getParentFile(), 511, -1, -1);
            String s3 = (new StringBuilder()).append(s1).append(".temp").toString();
            (new File(s3)).delete();
            android.app.MiuiDownloadManager.Request request = new android.app.MiuiDownloadManager.Request(Uri.parse(s));
            request.setNotificationVisibility(0);
            request.setMimeType(getMimeType(s));
            request.setTitle(s2);
            request.setDestinationUri(Uri.fromFile(file));
            request.setAppointName(s3);
            request.setVisibleInDownloadsUi(true);
            request.setAppData(s1);
            l = downloadmanager.enqueue(request);
        }
        return l;
    }

    private static String getMimeType(String s) {
        String s1 = s;
        String s2 = "";
        int i = s.lastIndexOf('/');
        if(-1 != i)
            s1 = s.substring(i + 1);
        int j = s1.lastIndexOf('.');
        if(-1 != j)
            s2 = s1.substring(j + 1);
        return MimeTypeMap.getSingleton().getMimeTypeFromExtension(s2);
    }

    private static URI getUriFromUrl(String s) {
        URI uri;
        try {
            WebAddress webaddress = new WebAddress(new String(URLUtil.decode(s.getBytes())));
            String s1 = null;
            String s2 = null;
            String s3 = webaddress.getPath();
            if(s3.length() > 0) {
                int i = s3.lastIndexOf('#');
                if(i != -1) {
                    s1 = s3.substring(i + 1);
                    s3 = s3.substring(0, i);
                }
                int j = s3.lastIndexOf('?');
                if(j != -1) {
                    s2 = s3.substring(j + 1);
                    s3 = s3.substring(0, j);
                }
            }
            uri = new URI(webaddress.getScheme(), webaddress.getAuthInfo(), webaddress.getHost(), webaddress.getPort(), s3, s2, s1);
        }
        catch(Exception exception) {
            uri = null;
        }
        return uri;
    }

    public boolean cancelDownload(String s) {
        boolean flag = true;
        android.app.MiuiDownloadManager.Query query = new android.app.MiuiDownloadManager.Query();
        query.setFilterByAppData(s.replace("'", "''"));
        Cursor cursor = mDownloadManager.query(query);
        int i = 0;
        if(cursor != null) {
            if(cursor.moveToFirst()) {
                long l = cursor.getLong(cursor.getColumnIndex("_id"));
                DownloadManager downloadmanager = mDownloadManager;
                long al[] = new long[flag];
                al[0] = l;
                i = downloadmanager.remove(al);
            }
            cursor.close();
        }
        if(i != flag)
            flag = false;
        return flag;
    }

    public boolean downloadResource(String s, String s1, String s2) {
        return downloadResource(s, s1, s2, false);
    }

    public boolean downloadResource(String s, String s1, String s2, boolean flag) {
        boolean flag1 = true;
        long l = downloadResource(mDownloadManager, s, s1, s2);
        if(l >= 0L && flag) {
            Context context = mContext;
            long al[] = new long[flag1];
            al[0] = l;
            MiuiDownloadManager.operateDownloadsNeedToUpdateProgress(context, al, null);
        }
        if(l < 0L)
            flag1 = false;
        return flag1;
    }

    public void handleDownloadFailed(String s) {
    }

    public void handleDownloadProgress(String s, int i, int j) {
    }

    public void handleDownloadSuccessed(String s) {
    }

    public boolean isResourceDownloading(String s) {
        boolean flag;
        if(s.equals(mLastQueryFilePath) && System.currentTimeMillis() - mLastQueryTime.longValue() < 500L) {
            flag = mLastQueryResult;
        } else {
            android.app.MiuiDownloadManager.Query query = new android.app.MiuiDownloadManager.Query();
            query.setFilterByAppData(s.replace("'", "''"));
            Cursor cursor = mDownloadManager.query(query);
            flag = false;
            if(cursor != null) {
                if(cursor.moveToFirst() && MiuiDownloadManager.isDownloading(cursor))
                    flag = true;
                else
                    flag = false;
                cursor.close();
            }
            mLastQueryResult = flag;
            mLastQueryFilePath = s;
            mLastQueryTime = Long.valueOf(System.currentTimeMillis());
        }
        return flag;
    }

    public void registerDownloadReceiver() {
        registerDownloadReceiver(false);
    }

    public void registerDownloadReceiver(boolean flag) {
        if(!mHasRegistDownloadReceiver) {
            IntentFilter intentfilter = new IntentFilter();
            intentfilter.addAction("android.intent.action.DOWNLOAD_COMPLETE");
            if(flag)
                intentfilter.addAction("android.intent.action.DOWNLOAD_UPDATED");
            mContext.registerReceiver(mDownloadReceiver, intentfilter);
            mHasRegistDownloadReceiver = true;
        }
    }

    public void unregisterDownloadReceiver() {
        if(mHasRegistDownloadReceiver) {
            mContext.unregisterReceiver(mDownloadReceiver);
            mHasRegistDownloadReceiver = false;
        }
    }

    private Context mContext;
    private DownloadManager mDownloadManager;
    private BroadcastReceiver mDownloadReceiver;
    private boolean mHasRegistDownloadReceiver;
    private String mLastQueryFilePath;
    private boolean mLastQueryResult;
    private Long mLastQueryTime;

}
