// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser.util;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Handler;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package miui.app.resourcebrowser.util:
//            ResourceHelper

public class BatchMediaPlayer {
    public static interface BatchPlayerListener {

        public abstract void finish(boolean flag);

        public abstract void play(String s, int i, int j);
    }

    private static final class PlayState extends Enum {

        public static PlayState valueOf(String s) {
            return (PlayState)Enum.valueOf(miui/app/resourcebrowser/util/BatchMediaPlayer$PlayState, s);
        }

        public static PlayState[] values() {
            return (PlayState[])$VALUES.clone();
        }

        private static final PlayState $VALUES[];
        public static final PlayState PAUSED;
        public static final PlayState PLAYING;
        public static final PlayState UNDEFINED;

        static  {
            UNDEFINED = new PlayState("UNDEFINED", 0);
            PLAYING = new PlayState("PLAYING", 1);
            PAUSED = new PlayState("PAUSED", 2);
            PlayState aplaystate[] = new PlayState[3];
            aplaystate[0] = UNDEFINED;
            aplaystate[1] = PLAYING;
            aplaystate[2] = PAUSED;
            $VALUES = aplaystate;
        }

        private PlayState(String s, int i) {
            super(s, i);
        }
    }


    public BatchMediaPlayer(Activity activity) {
        mActivity = null;
        mPlayer = null;
        mListener = null;
        mPlayList = new ArrayList();
        mCurrentItem = -1;
        mState = PlayState.UNDEFINED;
        mhandler = new Handler();
        if(activity == null) {
            throw new IllegalArgumentException("activity cann't be null");
        } else {
            mActivity = activity;
            return;
        }
    }

    private void realPlay() {
        if(!isPaused() && mPlayer != null) {
            if(mListener != null)
                mListener.play((String)mPlayList.get(mCurrentItem), mCurrentItem, size());
            mPlayer.start();
            mState = PlayState.PLAYING;
        }
    }

    private void setPlayerDataSource() {
        try {
            int i = 1 + mCurrentItem;
            mCurrentItem = i;
            if(i < mPlayList.size()) {
                mPlayer.reset();
                mPlayer.setDataSource(mActivity, ResourceHelper.getUriByPath((String)mPlayList.get(mCurrentItem)));
                mPlayer.prepareAsync();
            } else {
                stop(false);
            }
        }
        catch(Exception exception) { }
    }

    public void addPlayUri(String s) {
        mPlayList.add(s);
    }

    public int getPlayedDuration() {
        int i;
        if(isPlaying())
            i = mPlayer.getCurrentPosition();
        else
            i = 0;
        return i;
    }

    public int getTotalDuration() {
        int i;
        if(isPlaying())
            i = mPlayer.getDuration();
        else
            i = -1;
        return i;
    }

    public boolean isPaused() {
        boolean flag;
        if(mState == PlayState.PAUSED)
            flag = true;
        else
            flag = false;
        return flag;
    }

    public boolean isPlaying() {
        boolean flag;
        if(mState == PlayState.PLAYING)
            flag = true;
        else
            flag = false;
        return flag;
    }

    public void pause() {
        if(mPlayer != null)
            mPlayer.pause();
        if(isPlaying())
            mState = PlayState.PAUSED;
    }

    public void setListener(BatchPlayerListener batchplayerlistener) {
        mListener = batchplayerlistener;
    }

    public void setPlayList(List list) {
        mPlayList.clear();
        if(list != null)
            mPlayList.addAll(list);
    }

    public int size() {
        return mPlayList.size();
    }

    public void start() {
        if(mPlayer != null) {
            mState = PlayState.PLAYING;
            realPlay();
        } else {
            mPlayer = new MediaPlayer();
            mPlayer.setOnErrorListener(new android.media.MediaPlayer.OnErrorListener() {

                public boolean onError(MediaPlayer mediaplayer, int i, int j) {
                    stop(true);
                    return true;
                }

                final BatchMediaPlayer this$0;

             {
                this$0 = BatchMediaPlayer.this;
                super();
            }
            });
            mPlayer.setOnCompletionListener(new android.media.MediaPlayer.OnCompletionListener() {

                public void onCompletion(MediaPlayer mediaplayer) {
                    long l = Math.max(Math.min(1000, 2000 - mediaplayer.getDuration()), 500L);
                    mhandler.removeCallbacks(mBatchPlayRun);
                    mhandler.postDelayed(mBatchPlayRun, l);
                }

                final BatchMediaPlayer this$0;

             {
                this$0 = BatchMediaPlayer.this;
                super();
            }
            });
            mPlayer.setOnPreparedListener(new android.media.MediaPlayer.OnPreparedListener() {

                public void onPrepared(MediaPlayer mediaplayer) {
                    if(mActivity.hasWindowFocus()) {
                        mediaplayer.seekTo(0);
                        realPlay();
                    } else {
                        stop(false);
                    }
                }

                final BatchMediaPlayer this$0;

             {
                this$0 = BatchMediaPlayer.this;
                super();
            }
            });
            mPlayer.setAudioStreamType(mActivity.getVolumeControlStream());
            setPlayerDataSource();
        }
    }

    public void stop() {
        stop(false);
    }

    public void stop(boolean flag) {
        if(mPlayer != null) {
            mPlayer.setOnPreparedListener(null);
            if(mPlayer.isPlaying())
                mPlayer.stop();
            mPlayer.release();
            mPlayer = null;
            if(mListener != null)
                mListener.finish(flag);
        }
        mCurrentItem = -1;
        mState = PlayState.UNDEFINED;
    }

    private Activity mActivity;
    private final Runnable mBatchPlayRun = new Runnable() {

        public void run() {
            setPlayerDataSource();
        }

        final BatchMediaPlayer this$0;

             {
                this$0 = BatchMediaPlayer.this;
                super();
            }
    };
    private int mCurrentItem;
    private BatchPlayerListener mListener;
    private ArrayList mPlayList;
    private MediaPlayer mPlayer;
    private PlayState mState;
    private Handler mhandler;





}
