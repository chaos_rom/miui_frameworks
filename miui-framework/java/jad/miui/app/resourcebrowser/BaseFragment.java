// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser;

import android.app.Fragment;
import android.view.Menu;
import android.view.MenuItem;
import java.util.ArrayList;
import java.util.List;

public class BaseFragment extends Fragment {

    public BaseFragment() {
        mVisiableForUser = false;
    }

    public boolean isVisiableForUser() {
        return mVisiableForUser;
    }

    public List onFragmentCreateOptionsMenu(Menu menu) {
        return new ArrayList();
    }

    public void onFragmentOptionsItemSelected(MenuItem menuitem) {
    }

    public void onFragmentPrepareOptionsMenu(Menu menu, boolean flag) {
    }

    protected void onVisiableChanged(boolean flag) {
        mVisiableForUser = flag;
    }

    private boolean mVisiableForUser;
}
