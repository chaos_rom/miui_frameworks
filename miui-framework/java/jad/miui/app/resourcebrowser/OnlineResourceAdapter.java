// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import java.io.File;
import java.security.InvalidParameterException;
import java.util.*;
import miui.app.resourcebrowser.resource.Resource;
import miui.app.resourcebrowser.service.ResourceDataParser;
import miui.app.resourcebrowser.service.ResourceJSONDataParser;
import miui.app.resourcebrowser.service.online.OnlineProtocolConstants;
import miui.app.resourcebrowser.util.DownloadFileTask;
import miui.app.resourcebrowser.util.ResourceDebug;
import miui.app.resourcebrowser.util.ResourceHelper;
import miui.os.AsyncTaskObserver;
import miui.os.ExtraFileUtils;

// Referenced classes of package miui.app.resourcebrowser:
//            ResourceAdapter, BaseFragment

public class OnlineResourceAdapter extends ResourceAdapter
    implements OnlineProtocolConstants {
    private class DThumbnailStatNode
        implements Comparable {

        public volatile int compareTo(Object obj) {
            return compareTo((DThumbnailStatNode)obj);
        }

        public int compareTo(DThumbnailStatNode dthumbnailstatnode) {
            long l;
            if(sTime != dthumbnailstatnode.sTime)
                l = sTime - dthumbnailstatnode.sTime;
            else
                l = eTime - dthumbnailstatnode.eTime;
            return (int)l;
        }

        long eTime;
        long sTime;
        final OnlineResourceAdapter this$0;
        String url;

        private DThumbnailStatNode() {
            this$0 = OnlineResourceAdapter.this;
            super();
        }

    }

    public class DownloadThumbnailTask extends DownloadFileTask {

        protected volatile Object doInBackground(Object aobj[]) {
            return doInBackground((miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry[])aobj);
        }

        protected transient List doInBackground(miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry adownloadfileentry[]) {
            miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry downloadfileentry = adownloadfileentry[0];
            if(downloadfileentry != mDownloadEntry)
                throw new InvalidParameterException("DownloadThumbnailTask parameter error.");
            List list;
            if(ResourceDebug.DEBUG) {
                int i = 1 + ((ResourceDebug) (this)).sMap;
                Log.d("Theme", (new StringBuilder()).append("Start downloading thumbnail: ").append(downloadfileentry.getUrl()).append(" parallelTask=").append(dTaskParallelNumber).toString());
                long l = System.currentTimeMillis();
                list = super.doInBackground(adownloadfileentry);
                long l1 = System.currentTimeMillis();
                dAddIntoStat(downloadfileentry.getUrl(), l, l1);
                int i = (int)(l1 - l);
                ResourceDebug.sMap.put(downloadfileentry.getUrl(), (new StringBuilder()).append(i).append("ms-").append(dTaskParallelNumber).toString());
                Log.d("Theme", (new StringBuilder()).append("End downloading thumbnail: ").append(downloadfileentry.getUrl()).append("  fileSize: ").append((new File(downloadfileentry.getPath())).length()).append(" costTime: ").append(i).append(" parallelTask: ").append(dTaskParallelNumber).toString());
                int i = -1 + ((ResourceDebug) (this)).sMap;
            } else {
                list = super.doInBackground(adownloadfileentry);
            }
            return list;
        }

        protected void onCancelled() {
            super.onCancelled();
            throw new InvalidParameterException("downloading thumbnail is canceled, but we don't handle it.");
        }

        protected volatile void onPostExecute(Object obj) {
            onPostExecute((List)obj);
        }

        protected void onPostExecute(List list) {
            if(list != null && !list.isEmpty())
                notifyDataSetChanged();
            miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry downloadfileentry = mDownloadEntry;
            Integer integer = (Integer)mDownloadedCount.get(downloadfileentry);
            if(integer == null)
                integer = Integer.valueOf(0);
            mDownloadedCount.put(downloadfileentry, Integer.valueOf(1 + integer.intValue()));
            mThumbnailDownloadingSet.remove(downloadfileentry);
            mThumbnailDownloadTaskList.remove(this);
            startOneDownloadThumbnailTask();
        }

        miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry mDownloadEntry;
        final OnlineResourceAdapter this$0;

        public DownloadThumbnailTask(miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry downloadfileentry) {
            this$0 = OnlineResourceAdapter.this;
            super();
            mDownloadEntry = downloadfileentry;
        }
    }

    public class AsyncLoadMoreResourceTask extends miui.widget.AsyncAdapter.AsyncLoadMoreDataTask {

        protected List loadMoreData(miui.widget.AsyncAdapter.AsyncLoadMoreParams asyncloadmoreparams) {
            if(!asyncloadmoreparams.upwards) goto _L2; else goto _L1
_L1:
            List list = null;
_L4:
            return list;
_L2:
            list = null;
            DownloadFileTask downloadfiletask = new DownloadFileTask();
            StringBuilder stringbuilder = (new StringBuilder()).append(mUrl);
            Object aobj[] = new Object[2];
            aobj[0] = Integer.valueOf(asyncloadmoreparams.cursor);
            aobj[1] = Integer.valueOf(30);
            String s = stringbuilder.append(String.format("&start=%s&count=%s", aobj)).toString();
            if(ResourceDebug.DEBUG)
                Log.d("Theme", (new StringBuilder()).append("Online list page request more data: ").append(s).toString());
            miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry downloadfileentry = new miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry();
            downloadfileentry.setUrl(s);
            downloadfileentry.setPath(ResourceHelper.getFilePathByURL(mListFolder, s));
            miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry adownloadfileentry[] = new miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry[1];
            adownloadfileentry[0] = downloadfileentry;
            List list1 = downloadfiletask.doInForeground(adownloadfileentry);
            String s1 = null;
            if(list1.size() > 0)
                s1 = ((miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry)list1.get(0)).getPath();
            if(s1 != null)
                list = mParser.readResources(s1, mMetaData);
            if(true) goto _L4; else goto _L3
_L3:
        }

        final OnlineResourceAdapter this$0;

        public AsyncLoadMoreResourceTask() {
            this$0 = OnlineResourceAdapter.this;
            super(OnlineResourceAdapter.this);
        }
    }


    public OnlineResourceAdapter(Context context, Bundle bundle) {
        super(context, bundle);
        mThumbnailDownloadQueue = new LinkedHashMap(0, 0.75F, true);
        mThumbnailDownloadingSet = new LinkedHashSet();
        mThumbnailDownloadTaskList = new ArrayList();
        mDownloadedCount = new HashMap();
        mParser = ResourceJSONDataParser.getInstance();
        dThumbnailTaskStatList = null;
        if(ResourceDebug.DEBUG) {
            dThumbnailTaskStatList = new ArrayList();
            dTaskParallelNumber = 0;
        }
        initParams();
    }

    public OnlineResourceAdapter(BaseFragment basefragment, Bundle bundle) {
        super(basefragment, bundle);
        mThumbnailDownloadQueue = new LinkedHashMap(0, 0.75F, true);
        mThumbnailDownloadingSet = new LinkedHashSet();
        mThumbnailDownloadTaskList = new ArrayList();
        mDownloadedCount = new HashMap();
        mParser = ResourceJSONDataParser.getInstance();
        dThumbnailTaskStatList = null;
        if(ResourceDebug.DEBUG) {
            dThumbnailTaskStatList = new ArrayList();
            dTaskParallelNumber = 0;
        }
        initParams();
    }

    private void dAddIntoStat(String s, long l, long l1) {
        if(ResourceDebug.DEBUG) goto _L2; else goto _L1
_L1:
        return;
_L2:
        DThumbnailStatNode dthumbnailstatnode;
        dthumbnailstatnode = new DThumbnailStatNode();
        dthumbnailstatnode.sTime = l;
        dthumbnailstatnode.eTime = l1;
        dthumbnailstatnode.url = s;
        ArrayList arraylist = dThumbnailTaskStatList;
        arraylist;
        JVM INSTR monitorenter ;
        dThumbnailTaskStatList.add(dthumbnailstatnode);
        if(true) goto _L1; else goto _L3
_L3:
    }

    private void dStatDownloadThumbnailSpeedInfo() {
        if(ResourceDebug.DEBUG && mThumbnailDownloadingSet.isEmpty() && !dThumbnailTaskStatList.isEmpty()) goto _L2; else goto _L1
_L1:
        return;
_L2:
        ArrayList arraylist = dThumbnailTaskStatList;
        arraylist;
        JVM INSTR monitorenter ;
        int i = 0;
        long l = 0L;
        int j = dThumbnailTaskStatList.size();
        for(int k = 0; k < j; k++) {
            DThumbnailStatNode dthumbnailstatnode = (DThumbnailStatNode)dThumbnailTaskStatList.get(k);
            i = (int)((long)i + (dthumbnailstatnode.eTime - dthumbnailstatnode.sTime));
            l = Math.max(l, dthumbnailstatnode.eTime);
        }

        int i1 = (int)(l - ((DThumbnailStatNode)dThumbnailTaskStatList.get(0)).sTime);
        Log.i("Theme", (new StringBuilder()).append("Real Stat Result: ContinuousDownloadTaskNum=").append(j).append(" AvgTaskRelativeTime=").append(i / j).append(" AvgActualTime=").append(i1 / j).toString());
        dThumbnailTaskStatList.clear();
        if(true) goto _L1; else goto _L3
_L3:
    }

    private void downloadThumbnail(miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry downloadfileentry) {
        if(downloadfileentry != null && super.mDisplayType != 5 && !mThumbnailDownloadingSet.contains(downloadfileentry)) {
            Integer integer = (Integer)mDownloadedCount.get(downloadfileentry);
            if(integer == null || integer.intValue() < 5) {
                mThumbnailDownloadQueue.put(downloadfileentry, null);
                startOneDownloadThumbnailTask();
            }
        }
    }

    private void initParams() {
        mListFolder = ExtraFileUtils.standardizeFolderPath((new StringBuilder()).append(ExtraFileUtils.standardizeFolderPath(super.mMetaData.getString("miui.app.resourcebrowser.CACHE_LIST_FOLDER"))).append(super.mResourceSetCode).toString());
    }

    private void resetResources() {
        clearResourceSet();
        notifyDataSetInvalidated();
        loadMoreData(false, true);
    }

    private void startOneDownloadThumbnailTask() {
        if(!mThumbnailDownloadQueue.isEmpty() && mThumbnailDownloadTaskList.size() < MAX_THUMBNAIL_DOWNLOAD_TASK) {
            miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry downloadfileentry = null;
            Iterator iterator;
            for(iterator = mThumbnailDownloadQueue.keySet().iterator(); iterator.hasNext();)
                downloadfileentry = (miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry)iterator.next();

            iterator.remove();
            DownloadThumbnailTask downloadthumbnailtask = new DownloadThumbnailTask(downloadfileentry);
            mThumbnailDownloadingSet.add(downloadfileentry);
            mThumbnailDownloadTaskList.add(downloadthumbnailtask);
            miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry adownloadfileentry[] = new miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry[1];
            adownloadfileentry[0] = downloadfileentry;
            downloadthumbnailtask.execute(adownloadfileentry);
            if(ResourceDebug.DEBUG)
                Log.d("Theme", (new StringBuilder()).append("remainTaskNumber=").append(mThumbnailDownloadQueue.size()).append(" executingTestNumber=").append(mThumbnailDownloadTaskList.size()).toString());
        }
        dStatDownloadThumbnailSpeedInfo();
    }

    protected List getLoadMoreDataTask() {
        ArrayList arraylist = new ArrayList();
        AsyncLoadMoreResourceTask asyncloadmoreresourcetask = new AsyncLoadMoreResourceTask();
        asyncloadmoreresourcetask.addObserver((AsyncTaskObserver)getRegisterAsyncTaskObserver());
        arraylist.add(asyncloadmoreresourcetask);
        return arraylist;
    }

    protected volatile boolean isValidKey(Object obj, Object obj1, int i) {
        return isValidKey(obj, (Resource)obj1, i);
    }

    protected boolean isValidKey(Object obj, Resource resource, int i) {
        boolean flag = true;
        String s = (String)obj;
        String s1 = resource.getOnlineThumbnail(i);
        miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry downloadfileentry = new miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry();
        downloadfileentry.setPath(s);
        downloadfileentry.setUrl(s1);
        File file = new File(s);
        if(!file.exists())
            downloadThumbnail(downloadfileentry);
        else
        if(file.lastModified() < resource.getFileModifiedTime()) {
            file.delete();
            downloadThumbnail(downloadfileentry);
        } else {
            flag = super.isValidKey(obj, resource, i);
        }
        return flag;
    }

    protected List loadCacheData(miui.widget.AsyncAdapter.AsyncLoadMoreParams asyncloadmoreparams) {
        if(!asyncloadmoreparams.upwards && asyncloadmoreparams.cursor == 0) goto _L2; else goto _L1
_L1:
        List list = null;
_L4:
        return list;
_L2:
        list = null;
        StringBuilder stringbuilder = (new StringBuilder()).append(mUrl);
        Object aobj[] = new Object[2];
        aobj[0] = Integer.valueOf(asyncloadmoreparams.cursor);
        aobj[1] = Integer.valueOf(30);
        String s = stringbuilder.append(String.format("&start=%s&count=%s", aobj)).toString();
        String s1 = ResourceHelper.getFilePathByURL(mListFolder, s);
        if((new File(s1)).exists())
            list = mParser.readResources(s1, super.mMetaData);
        if(true) goto _L4; else goto _L3
_L3:
    }

    protected void postLoadMoreData(List list) {
        if(list == null && (super.mFragment == null || super.mFragment.isVisiableForUser()))
            Toast.makeText(super.mContext, 0x60c0020, 0).show();
    }

    public void setUrl(String s) {
        mUrl = s;
        resetResources();
    }

    private static final int MAX_THUMBNAIL_DOWNLOAD_TASK;
    private int dTaskParallelNumber;
    private ArrayList dThumbnailTaskStatList;
    private HashMap mDownloadedCount;
    private String mListFolder;
    private ResourceDataParser mParser;
    private LinkedHashMap mThumbnailDownloadQueue;
    private ArrayList mThumbnailDownloadTaskList;
    private HashSet mThumbnailDownloadingSet;
    private String mUrl;

    static  {
        int i;
        if(ResourceDebug.DEBUG)
            i = ResourceDebug.getMaxThumbnailDownloadTaskNumber();
        else
            i = 3;
        MAX_THUMBNAIL_DOWNLOAD_TASK = i;
    }






/*
    static int access$304(OnlineResourceAdapter onlineresourceadapter) {
        int i = 1 + onlineresourceadapter.dTaskParallelNumber;
        onlineresourceadapter.dTaskParallelNumber = i;
        return i;
    }

*/


/*
    static int access$306(OnlineResourceAdapter onlineresourceadapter) {
        int i = -1 + onlineresourceadapter.dTaskParallelNumber;
        onlineresourceadapter.dTaskParallelNumber = i;
        return i;
    }

*/





}
