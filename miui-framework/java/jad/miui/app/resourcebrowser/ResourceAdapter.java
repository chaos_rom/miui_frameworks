// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.*;
import java.io.File;
import java.util.*;
import miui.app.resourcebrowser.resource.Resource;
import miui.app.resourcebrowser.resource.ResourceSet;
import miui.app.resourcebrowser.service.local.AudioResourceFolder;
import miui.app.resourcebrowser.util.ResourceDebug;
import miui.app.resourcebrowser.util.ResourceHelper;
import miui.app.resourcebrowser.util.ResourceMusicPlayer;
import miui.app.resourcebrowser.view.BatchResourceHandler;
import miui.widget.AsyncImageAdapter;
import miui.widget.DataGroup;

// Referenced classes of package miui.app.resourcebrowser:
//            IntentConstants, BaseFragment

public abstract class ResourceAdapter extends AsyncImageAdapter
    implements IntentConstants {

    public ResourceAdapter(Context context, Bundle bundle) {
        this(null, context, bundle);
    }

    private ResourceAdapter(BaseFragment basefragment, Context context, Bundle bundle) {
        mItemHorizontalSpacing = -1;
        mItemVerticalSpaceing = -1;
        mFragment = basefragment;
        mContext = (Activity)context;
        if(mFragment == null && mContext == null) {
            throw new RuntimeException("invalid parameters: fragment and activity can not both be null.");
        } else {
            mDecodeImageLowQuality = mContext.getResources().getBoolean(0x609000f);
            mMetaData = bundle;
            mResourceSetCode = mMetaData.getString("miui.app.resourcebrowser.RESOURCE_SET_CODE");
            mResourceSetCategory = mMetaData.getInt("miui.app.resourcebrowser.RESOURCE_SET_CATEGORY");
            refreshDataSet();
            mDisplayType = mMetaData.getInt("miui.app.resourcebrowser.DISPLAY_TYPE");
            setDataPerLine(ResourceHelper.getDataPerLine(mDisplayType));
            setAutoLoadMoreStyle(2);
            setPreloadOffset(30 / (2 * getDataPerLine()));
            mInflater = (LayoutInflater)mContext.getSystemService("layout_inflater");
            resolveThumbnailSize();
            return;
        }
    }

    public ResourceAdapter(BaseFragment basefragment, Bundle bundle) {
        this(basefragment, ((Context) (basefragment.getActivity())), bundle);
    }

    private void bindText(View view, int i, Resource resource, String s) {
        TextView textview = (TextView)view.findViewById(i);
        if(textview != null) {
            textview.setText(resource.getInformation().getString(s));
            textview.setVisibility(0);
            if(ResourceDebug.DEBUG) {
                String s1 = resource.getLocalThumbnail(0);
                if(s1 != null) {
                    File file = new File(s1);
                    if(file.exists()) {
                        String s2 = (new StringBuilder()).append(ResourceHelper.getFormattedSize(file.length())).append(" ").toString();
                        String s3;
                        if(ResourceDebug.sMap.containsKey(resource.getOnlineThumbnail(0)))
                            s3 = (new StringBuilder()).append(s2).append(ResourceDebug.sMap.get(resource.getOnlineThumbnail(0))).toString();
                        else
                            s3 = (new StringBuilder()).append(s2).append(textview.getText()).toString();
                        textview.setText(s3);
                    }
                }
            }
        }
    }

    private void resolveThumbnailSize() {
        Drawable drawable = mContext.getResources().getDrawable(0x602018e);
        Rect rect = new Rect();
        drawable.getPadding(rect);
        int i = rect.left + rect.right;
        Pair pair = ResourceHelper.getThumbnailSize(mContext, mDisplayType, i);
        mThumbnailWidth = ((Integer)pair.first).intValue();
        mThumbnailHeight = ((Integer)pair.second).intValue();
        mMetaData.putInt("miui.app.resourcebrowser.THUMBNAIL_WIDTH", mThumbnailWidth);
        mItemHorizontalSpacing = ResourceHelper.getThumbnailGap(mContext);
        mItemVerticalSpaceing = mItemHorizontalSpacing;
    }

    private void setListItemViewBackground(View view, int i, int j) {
        int k;
        if(getDataPerLine() == 1) {
            int l = getCount(j);
            if(l == 1)
                k = 0x602018d;
            else
            if(i == l - 1 || ((Resource)getItem(i + 1, j).get(0)).getDividerTitle() != null)
                k = 0x6020189;
            else
            if(i == 0)
                k = 0x6020181;
            else
                k = 0x6020185;
        } else {
            k = 0x602018e;
        }
        view.setBackgroundResource(k);
    }

    private void setResourceFlag(View view, Resource resource) {
        if(resource != null) {
            view.findViewById(0x60b004c).setVisibility(0);
            ((ImageView)view.findViewById(0x60b004b)).setImageResource(getTopFlagId(resource));
            ((ImageView)view.findViewById(0x60b0063)).setImageResource(getCenterFlagId(resource));
            ((ImageView)view.findViewById(0x60b0066)).setImageResource(getBottomFlagId(resource));
        }
    }

    private void setThumbnail(ImageView imageview, Resource resource, int i, List list, boolean flag) {
        if(imageview != null)
            if(mResourceSetCategory == 2 && mBatchHandler == null) {
                if(mMusicPlayer == null)
                    mMusicPlayer = new ResourceMusicPlayer(mContext, true);
                mMusicPlayer.initPlayButtonIfNeed(imageview, resource);
            } else
            if(list == null || list.isEmpty())
                imageview.setImageBitmap(null);
            else
                imageview.setImageBitmap((Bitmap)list.get(i));
    }

    protected View bindContentView(View view, List list, int i, int j, int k) {
        LinearLayout linearlayout = (LinearLayout)view;
        LinearLayout linearlayout2;
        if(linearlayout == null) {
            linearlayout = new LinearLayout(mContext);
            linearlayout.setOrientation(1);
            TextView textview = new TextView(mContext, null, 0x1010208);
            textview.setBackgroundResource(0x6020197);
            textview.setVisibility(8);
            linearlayout.addView(textview, mRootDataParams);
            LinearLayout linearlayout1 = new LinearLayout(mContext);
            linearlayout1.setOrientation(0);
            linearlayout1.setGravity(17);
            linearlayout.addView(linearlayout1, mRootDataParams);
            int l = getDataPerLine();
            for(int i1 = 0; i1 < l; i1++) {
                View view2 = mInflater.inflate(ResourceHelper.getThumbnailViewResource(mDisplayType), null);
                linearlayout1.addView(view2, getItemViewLayoutParams(view2, i1, l));
            }

            boolean flag;
            TextView textview1;
            int j1;
            Resource resource;
            if(getDataPerLine() <= 1)
                flag = true;
            else
                flag = false;
            linearlayout1.setEnabled(flag);
            linearlayout.setDrawingCacheEnabled(true);
        }
        textview1 = (TextView)linearlayout.getChildAt(0);
        textview1.setVisibility(8);
        linearlayout2 = (LinearLayout)linearlayout.getChildAt(1);
        setListItemViewBackground(linearlayout2, j, k);
        if(getDataPerLine() == 1) {
            linearlayout2.setTag(new Pair(Integer.valueOf(j * getDataPerLine()), Integer.valueOf(k)));
            if(mBatchHandler != null)
                linearlayout2.setOnClickListener(mBatchHandler.getResourceClickListener());
        }
        j1 = 0;
        while(j1 < getDataPerLine())  {
            View view1 = linearlayout2.getChildAt(j1);
            if(j1 < list.size()) {
                view1.setVisibility(0);
                resource = (Resource)list.get(j1);
                if(resource.getDividerTitle() != null) {
                    textview1.setVisibility(0);
                    textview1.setText(resource.getDividerTitle());
                }
                bindView(view1, resource, j1 + j * getDataPerLine(), k);
            } else {
                view1.setVisibility(4);
            }
            j1++;
        }
        if(mDisplayType == 5)
            mBatchHandler.initViewState(linearlayout2, new Pair(Integer.valueOf(j * getDataPerLine()), Integer.valueOf(k)));
        return linearlayout;
    }

    protected volatile void bindPartialContentView(View view, Object obj, int i, List list) {
        bindPartialContentView(view, (Resource)obj, i, list);
    }

    protected void bindPartialContentView(View view, Resource resource, int i, List list) {
        if(ResourceHelper.isMultipleView(mDisplayType)) {
            setThumbnail((ImageView)view.findViewById(0x60b0049), resource, 0, list, true);
            setThumbnail((ImageView)view.findViewById(0x60b0067), resource, 1, list, false);
            setThumbnail((ImageView)view.findViewById(0x60b0068), resource, 2, list, false);
        } else {
            setThumbnail((ImageView)((LinearLayout)((LinearLayout)view).getChildAt(1)).getChildAt(i).findViewById(0x60b0048), resource, 0, list, true);
        }
    }

    protected void bindView(View view, Resource resource, int i, int j) {
        if(mDisplayType != 5) goto _L2; else goto _L1
_L1:
        setResourceFlag(view, resource);
        bindText(view, 0x60b0041, resource, "NAME");
        setSubTitle((TextView)view.findViewById(0x60b004a), resource);
_L4:
        return;
_L2:
        if(mBatchHandler != null) {
            ImageView imageview = (ImageView)view.findViewById(0x60b0048);
            if(resource != null) {
                imageview.setTag(new Pair(Integer.valueOf(i), Integer.valueOf(j)));
                imageview.setOnClickListener(mBatchHandler.getResourceClickListener());
                imageview.setVisibility(0);
                setResourceFlag(view, resource);
                bindText(view, 0x60b0041, resource, "NAME");
            } else {
                ((View)imageview.getParent()).setVisibility(4);
                View view1 = view.findViewById(0x60b0041);
                if(view1 != null)
                    view1.setVisibility(4);
            }
        }
        if(true) goto _L4; else goto _L3
_L3:
    }

    protected boolean checkResourceModifyTime() {
        return true;
    }

    public void clearResourceSet() {
        if(mResourceSet != null)
            mResourceSet.clear();
        if(mBatchHandler != null)
            mBatchHandler.quitEditMode();
    }

    public void clearResourceSet(int i) {
        if(mResourceSet != null && mResourceSet.size() > i)
            ((DataGroup)mResourceSet.get(i)).clear();
        if(mBatchHandler != null)
            mBatchHandler.quitEditMode();
    }

    protected int getBottomFlagId(Resource resource) {
        int i;
        if(mResourceSetCategory == 0 || mResourceSetCategory == 1)
            i = getDownloadableFlag(resource);
        else
            i = 0;
        return i;
    }

    protected volatile List getCacheKeys(Object obj) {
        return getCacheKeys((Resource)obj);
    }

    protected List getCacheKeys(Resource resource) {
        ArrayList arraylist = new ArrayList();
        byte byte0;
        int i;
        if(ResourceHelper.isMultipleView(mDisplayType))
            byte0 = 3;
        else
            byte0 = 1;
        for(i = 0; i < byte0; i++)
            if(resource.getLocalThumbnail(i) != null)
                arraylist.add(resource.getLocalThumbnail(i));

        return arraylist;
    }

    protected int getCenterFlagId(Resource resource) {
        int i;
        if(mResourceSetCategory == 2)
            i = getDownloadableFlag(resource);
        else
            i = 0;
        return i;
    }

    protected int getDownloadableFlag(Resource resource) {
        int i;
        if(resource.getStatus() == 0) {
            if(resource.getLocalPath().equals(mCurrentUsingPath))
                i = 0x602003c;
            else
                i = 0x602003a;
        } else {
            i = 0;
        }
        return i;
    }

    protected int getFlagDownloaded() {
        return 0x602003a;
    }

    protected int getFlagUsing() {
        return 0x602003c;
    }

    protected android.widget.LinearLayout.LayoutParams getItemViewLayoutParams(View view, int i, int j) {
        android.widget.LinearLayout.LayoutParams layoutparams = new android.widget.LinearLayout.LayoutParams(mThumbnailWidth, mThumbnailHeight);
        if(j >= 2) {
            layoutparams.leftMargin = 0;
            int k;
            if(i < j - 1)
                k = mItemHorizontalSpacing;
            else
                k = 0;
            layoutparams.rightMargin = k;
            layoutparams.topMargin = mItemVerticalSpaceing;
            layoutparams.bottomMargin = 0;
        }
        return layoutparams;
    }

    protected miui.widget.AsyncAdapter.AsyncLoadPartialDataTask getLoadPartialDataTask() {
        miui.widget.AsyncImageAdapter.AsyncLoadImageTask asyncloadimagetask = new miui.widget.AsyncImageAdapter.AsyncLoadImageTask(this);
        asyncloadimagetask.setJobPool(new miui.os.DaemonAsyncTask.StackJobPool());
        asyncloadimagetask.setTargetSize(mThumbnailWidth, mThumbnailHeight);
        asyncloadimagetask.setScaled(true);
        return asyncloadimagetask;
    }

    protected final Object getRegisterAsyncTaskObserver() {
        Object obj;
        if(mFragment != null)
            obj = mFragment;
        else
            obj = mContext;
        return obj;
    }

    public ResourceSet getResourceSet() {
        return mResourceSet;
    }

    protected int getTopFlagId(Resource resource) {
        int i;
        if(mResourceSetCategory == 0)
            i = getUpdatableFlag(resource);
        else
            i = 0;
        return i;
    }

    protected int getUpdatableFlag(Resource resource) {
        int i;
        if(resource.getStatus() == 1)
            i = 0x602003b;
        else
            i = 0;
        return i;
    }

    public boolean isEnabled(int i) {
        return false;
    }

    protected volatile boolean isValidKey(Object obj, Object obj1, int i) {
        return isValidKey(obj, (Resource)obj1, i);
    }

    protected boolean isValidKey(Object obj, Resource resource, int i) {
        boolean flag = false;
        File file = new File((String)obj);
        if(file.exists())
            if(checkResourceModifyTime() && file.lastModified() < resource.getFileModifiedTime())
                file.delete();
            else
                flag = super.isValidKey(obj, resource, i);
        return flag;
    }

    public void refreshDataSet() {
        mResourceSetPackage = mMetaData.getString("miui.app.resourcebrowser.RESOURCE_SET_PACKAGE");
        mResourceSetSubpackage = mMetaData.getString("miui.app.resourcebrowser.RESOURCE_SET_SUBPACKAGE");
        mResourceSet = ResourceSet.getInstance((new StringBuilder()).append(mResourceSetPackage).append(mResourceSetSubpackage).toString());
        setDataSet(mResourceSet);
    }

    public void setMusicPlayer(ResourceMusicPlayer resourcemusicplayer) {
        mMusicPlayer = resourcemusicplayer;
    }

    public void setResourceBatchHandler(BatchResourceHandler batchresourcehandler) {
        mBatchHandler = batchresourcehandler;
    }

    protected void setSubTitle(TextView textview, Resource resource) {
        int i = resource.getInformation().getInt("audio_duration", 0);
        if(i == 0) {
            i = (int)AudioResourceFolder.getLocalRingtoneDuration(resource.getLocalPath());
            if(i > 0)
                resource.getInformation().putInt("audio_duration", i);
        }
        if(i < 0)
            textview.setVisibility(8);
        else
        if(i > 0) {
            textview.setVisibility(0);
            textview.setText(AudioResourceFolder.formatDuration(i));
        } else {
            textview.setVisibility(0);
            textview.setText(0x60c000b);
        }
    }

    public void stopMusic() {
        if(mMusicPlayer != null)
            mMusicPlayer.stopMusic();
    }

    public boolean updateCurrentUsingPath(String s) {
        boolean flag;
        if(!TextUtils.equals(mCurrentUsingPath, s)) {
            mCurrentUsingPath = s;
            mMetaData.putString("miui.app.resourcebrowser.CURRENT_USING_PATH", mCurrentUsingPath);
            flag = true;
        } else {
            flag = false;
        }
        return flag;
    }

    protected boolean useLowQualityDecoding() {
        return mDecodeImageLowQuality;
    }

    protected static final android.widget.LinearLayout.LayoutParams mRootDataParams = new android.widget.LinearLayout.LayoutParams(-1, -2);
    private BatchResourceHandler mBatchHandler;
    public Activity mContext;
    protected String mCurrentUsingPath;
    private boolean mDecodeImageLowQuality;
    public int mDisplayType;
    public BaseFragment mFragment;
    protected LayoutInflater mInflater;
    private int mItemHorizontalSpacing;
    private int mItemVerticalSpaceing;
    protected Bundle mMetaData;
    private ResourceMusicPlayer mMusicPlayer;
    protected ResourceSet mResourceSet;
    protected int mResourceSetCategory;
    protected String mResourceSetCode;
    protected String mResourceSetPackage;
    protected String mResourceSetSubpackage;
    private int mThumbnailHeight;
    private int mThumbnailWidth;

}
