// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Pair;
import android.view.*;
import android.widget.ListView;
import java.util.List;
import miui.app.SDCardMonitor;
import miui.app.resourcebrowser.resource.Resource;
import miui.app.resourcebrowser.util.ResourceHelper;
import miui.app.resourcebrowser.view.AudioBatchResourceHandler;
import miui.app.resourcebrowser.view.BatchResourceHandler;
import miui.os.AsyncTaskObserver;

// Referenced classes of package miui.app.resourcebrowser:
//            BaseFragment, IntentConstants, LocalResourceListFragment, ResourceAdapter

public abstract class ResourceListFragment extends BaseFragment
    implements AsyncTaskObserver, miui.app.SDCardMonitor.SDCardStatusListener, IntentConstants {

    public ResourceListFragment() {
    }

    protected void addMetaData(Bundle bundle) {
    }

    protected abstract ResourceAdapter getAdapter();

    protected BatchResourceHandler getBatchOperationHandler() {
        Object obj;
        if(mResourceSetCategory == 2)
            obj = new AudioBatchResourceHandler(this, mAdapter);
        else
            obj = new BatchResourceHandler(this, mAdapter);
        return ((BatchResourceHandler) (obj));
    }

    protected abstract int getContentView();

    protected View getHeaderView() {
        return null;
    }

    protected Pair getResourceDetailActivity() {
        return new Pair(mMetaData.getString("miui.app.resourcebrowser.DETAIL_ACTIVITY_PACKAGE"), mMetaData.getString("miui.app.resourcebrowser.DETAIL_ACTIVITY_CLASS"));
    }

    public boolean isPicker() {
        return mMetaData.getBoolean("miui.app.resourcebrowser.USING_PICKER");
    }

    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        mActivity = getActivity();
        Intent intent = mActivity.getIntent();
        if(intent != null)
            if(this instanceof LocalResourceListFragment)
                mMetaData = intent.getBundleExtra("META_DATA_FOR_LOCAL");
            else
                mMetaData = intent.getBundleExtra("META_DATA_FOR_ONLINE");
        if(mMetaData == null) {
            throw new RuntimeException((new StringBuilder()).append("meta-data can not be null. fragment: ").append(getClass().getName()).toString());
        } else {
            pickMetaData(mMetaData);
            addMetaData(mMetaData);
            mSDCardMonitor = SDCardMonitor.getSDCardMonitor(mActivity);
            mSDCardMonitor.addListener(this);
            setupUI();
            return;
        }
    }

    public void onActivityResult(int i, int j, Intent intent) {
        if(isPicker() && intent != null) {
            mActivity.setResult(j, intent);
            mActivity.finish();
        }
    }

    public void onCancelled() {
        mProgressBar.setVisibility(8);
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle) {
        return layoutinflater.inflate(getContentView(), null);
    }

    public void onDestroy() {
        mBatchHandler.viewStateChanged(miui.app.resourcebrowser.view.BatchResourceHandler.ViewState.DESTYOY);
        if(mSDCardMonitor != null)
            mSDCardMonitor.removeListener(this);
        super.onDestroy();
    }

    public void onPause() {
        mBatchHandler.viewStateChanged(miui.app.resourcebrowser.view.BatchResourceHandler.ViewState.PAUSE);
        super.onPause();
    }

    public volatile void onPostExecute(Object obj) {
        onPostExecute((List)obj);
    }

    public void onPostExecute(List list) {
        mProgressBar.setVisibility(8);
    }

    public void onPreExecute() {
        mProgressBar.setVisibility(0);
    }

    public volatile void onProgressUpdate(Object aobj[]) {
        onProgressUpdate((Resource[])aobj);
    }

    public transient void onProgressUpdate(Resource aresource[]) {
    }

    public void onResume() {
        super.onResume();
        mAdapter.updateCurrentUsingPath(mMetaData.getString("miui.app.resourcebrowser.CURRENT_USING_PATH"));
        mAdapter.notifyDataSetChanged();
        String as[] = mMetaData.getStringArray("miui.app.resourcebrowser.SOURCE_FOLDERS");
        for(int i = 0; i < as.length; i++)
            ResourceHelper.getFolderInfoCache(as[i]);

    }

    public void onStatusChanged(boolean flag) {
        ResourceHelper.exit(mActivity);
    }

    public void onStop() {
        mAdapter.onStop();
        super.onStop();
    }

    protected void onVisiableChanged(boolean flag) {
        super.onVisiableChanged(flag);
        BatchResourceHandler batchresourcehandler = mBatchHandler;
        miui.app.resourcebrowser.view.BatchResourceHandler.ViewState viewstate;
        if(flag)
            viewstate = miui.app.resourcebrowser.view.BatchResourceHandler.ViewState.VISIABLE;
        else
            viewstate = miui.app.resourcebrowser.view.BatchResourceHandler.ViewState.INVISIABLE;
        batchresourcehandler.viewStateChanged(viewstate);
    }

    protected void pickMetaData(Bundle bundle) {
        mResourceSetPackage = bundle.getString("miui.app.resourcebrowser.RESOURCE_SET_PACKAGE");
        mResourceSetCode = bundle.getString("miui.app.resourcebrowser.RESOURCE_SET_CODE");
        mResourceSetName = bundle.getString("miui.app.resourcebrowser.RESOURCE_SET_NAME");
        mResourceSetCategory = bundle.getInt("miui.app.resourcebrowser.RESOURCE_SET_CATEGORY");
        mDisplayType = bundle.getInt("miui.app.resourcebrowser.DISPLAY_TYPE");
        mUsingPicker = bundle.getBoolean("miui.app.resourcebrowser.USING_PICKER");
    }

    protected void setupUI() {
        mListView = (ListView)getView().findViewById(0x60b004e);
        View view = getHeaderView();
        if(view != null)
            mListView.addHeaderView(view);
        mAdapter = getAdapter();
        mBatchHandler = getBatchOperationHandler();
        mAdapter.setResourceBatchHandler(mBatchHandler);
        mListView.setAdapter(mAdapter);
        mListView.setFastScrollEnabled(true);
        mListView.setDividerHeight(0);
        mProgressBar = getView().findViewById(0x60b004f);
    }

    public void startDetailActivityForResource(Pair pair) {
        Intent intent = new Intent();
        Pair pair1 = getResourceDetailActivity();
        intent.setClassName((String)pair1.first, (String)pair1.second);
        intent.addFlags(0x4000000);
        mMetaData.putInt("miui.app.resourcebrowser.RESOURCE_INDEX", ((Integer)pair.first).intValue());
        mMetaData.putInt("miui.app.resourcebrowser.RESOURCE_GROUP", ((Integer)pair.second).intValue());
        intent.putExtra("META_DATA", mMetaData);
        startActivityForResult(intent, 1);
    }

    protected Activity mActivity;
    protected ResourceAdapter mAdapter;
    protected BatchResourceHandler mBatchHandler;
    protected int mDisplayType;
    protected ListView mListView;
    protected Bundle mMetaData;
    protected View mProgressBar;
    protected int mResourceSetCategory;
    protected String mResourceSetCode;
    protected String mResourceSetName;
    protected String mResourceSetPackage;
    protected SDCardMonitor mSDCardMonitor;
    protected boolean mUsingPicker;
}
