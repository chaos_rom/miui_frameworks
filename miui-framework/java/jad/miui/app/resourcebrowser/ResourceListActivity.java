// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser;

import android.app.Activity;
import android.util.Pair;
import miui.os.AsyncTaskObserver;

// Referenced classes of package miui.app.resourcebrowser:
//            IntentConstants

public abstract class ResourceListActivity extends Activity
    implements AsyncTaskObserver, miui.app.SDCardMonitor.SDCardStatusListener, IntentConstants {

    public ResourceListActivity() {
    }

    public abstract boolean isPicker();

    public abstract void startDetailActivityForResource(Pair pair);
}
