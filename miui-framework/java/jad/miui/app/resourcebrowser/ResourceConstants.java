// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser;

import java.io.File;
import miui.os.Environment;

public interface ResourceConstants {

    public static final String BUILTIN_ALARM_PATH = "/system/media/audio/alarms";
    public static final String BUILTIN_LOCKSCREEN_PATH = "/system/media/lockscreen";
    public static final String BUILTIN_NOTIFICATION_PATH = "/system/media/audio/notifications";
    public static final String BUILTIN_RESOURCE_BASE_PATH = "/system/media/";
    public static final String BUILTIN_RINGTONE_PATH = "/system/media/audio/ringtones";
    public static final String BUILTIN_THEME_PATH = "/system/media/theme";
    public static final String BUILTIN_WALLPAPER_PATH = "/system/media/wallpaper";
    public static final int CACHE_FILE_MAX_NUMBER[] = ai;
    public static final String CACHE_FILE_PATHS[] = as;
    public static final String CACHE_PATH = (new StringBuilder()).append(MIUI_PATH).append(".cache").append(File.separator).append("ResourceBrowser").append(File.separator).toString();
    public static final String CATEGORY_PATH = (new StringBuilder()).append(CACHE_PATH).append("category").append(File.separator).toString();
    public static final String DESCRIPTION = "description.xml";
    public static final String DETAIL_PATH = (new StringBuilder()).append(CACHE_PATH).append("detail").append(File.separator).toString();
    public static final String DOWNLOADED_ALARM_PATH = (new StringBuilder()).append(DOWNLOADED_RESOURCE_BASE_PATH).append("ringtone").toString();
    public static final String DOWNLOADED_LOCKSCREEN_PATH = (new StringBuilder()).append(DOWNLOADED_RESOURCE_BASE_PATH).append("wallpaper").toString();
    public static final String DOWNLOADED_NOTIFICATION_PATH = (new StringBuilder()).append(DOWNLOADED_RESOURCE_BASE_PATH).append("ringtone").toString();
    public static final String DOWNLOADED_RESOURCE_BASE_PATH = MIUI_PATH;
    public static final String DOWNLOADED_RINGTONE_PATH = (new StringBuilder()).append(DOWNLOADED_RESOURCE_BASE_PATH).append("ringtone").toString();
    public static final String DOWNLOADED_THEME_PATH = (new StringBuilder()).append(DOWNLOADED_RESOURCE_BASE_PATH).append("theme").toString();
    public static final String DOWNLOADED_WALLPAPER_PATH = (new StringBuilder()).append(DOWNLOADED_RESOURCE_BASE_PATH).append("wallpaper").toString();
    public static final String LIST_PATH = (new StringBuilder()).append(CACHE_PATH).append("list").append(File.separator).toString();
    public static final String MIUI_EXTERNAL_PATH = (new StringBuilder()).append(Environment.getMIUIExternalStorageDirectory().getAbsolutePath()).append(File.separator).toString();
    public static final String MIUI_INTERNAL_PATH = (new StringBuilder()).append(Environment.getMIUIInternalStorageDirectory().getAbsolutePath()).append(File.separator).toString();
    public static final String MIUI_PATH = (new StringBuilder()).append(Environment.getMIUIStorageDirectory().getAbsolutePath()).append(File.separator).toString();
    public static final String PREVIEW_PATH = (new StringBuilder()).append(CACHE_PATH).append("preview").append(File.separator).toString();
    public static final String RECOMMENDATION_PATH = (new StringBuilder()).append(CACHE_PATH).append("recommend").append(File.separator).toString();
    public static final int RESOURCE_DEFAULT_LENGTH = 30;
    public static final String THUMBNAIL_PATH = (new StringBuilder()).append(CACHE_PATH).append("thumbnail").append(File.separator).toString();
    public static final int TYPE_CATEGORY = 4;
    public static final int TYPE_DETAIL = 1;
    public static final int TYPE_LIST = 0;
    public static final int TYPE_PREVIEW = 3;
    public static final int TYPE_THUMBNAIL = 2;
    public static final String VERSION_CACHE_FILE = "version";
    public static final String VERSION_PATH = (new StringBuilder()).append(CACHE_PATH).append("version").append(File.separator).toString();
    public static final String WALLPAPER_CACHE_PATH = (new StringBuilder()).append(CACHE_PATH).append("online_wallpaper").append(File.separator).toString();

    
    {
        String as[] = new String[8];
        as[0] = LIST_PATH;
        as[1] = DETAIL_PATH;
        as[2] = THUMBNAIL_PATH;
        as[3] = PREVIEW_PATH;
        as[4] = CATEGORY_PATH;
        as[5] = RECOMMENDATION_PATH;
        as[6] = VERSION_PATH;
        as[7] = WALLPAPER_CACHE_PATH;
        int ai[] = new int[8];
        ai[0] = 100;
        ai[1] = 100;
        ai[2] = 500;
        ai[3] = 300;
        ai[4] = 100;
        ai[5] = 100;
        ai[6] = 100;
        ai[7] = 200;
    }
}
