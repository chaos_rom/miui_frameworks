// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Pair;
import android.view.*;
import android.widget.*;
import java.io.File;
import java.util.*;
import miui.app.resourcebrowser.recommended.RecommendGridItemFactory;
import miui.app.resourcebrowser.recommended.RecommendItemData;
import miui.app.resourcebrowser.recommended.UnevenGrid;
import miui.app.resourcebrowser.resource.ListMetaData;
import miui.app.resourcebrowser.resource.Resource;
import miui.app.resourcebrowser.resource.ResourceCategory;
import miui.app.resourcebrowser.resource.ResourceSet;
import miui.app.resourcebrowser.service.ResourceDataParser;
import miui.app.resourcebrowser.service.ResourceJSONDataParser;
import miui.app.resourcebrowser.service.online.OnlineProtocolConstants;
import miui.app.resourcebrowser.service.online.OnlineService;
import miui.app.resourcebrowser.util.DownloadFileTask;
import miui.app.resourcebrowser.util.ResourceHelper;
import miui.os.AsyncTaskObserver;
import miui.os.ExtraFileUtils;
import miui.widget.DataGroup;

// Referenced classes of package miui.app.resourcebrowser:
//            ResourceListFragment, ResourceAdapter, OnlineResourceAdapter, ResourceConstants

public class OnlineResourceListFragment extends ResourceListFragment
    implements AsyncTaskObserver, OnlineProtocolConstants {
    public class DownloadListMetaDataTask extends DownloadFileTask {

        protected volatile void onPostExecute(Object obj) {
            onPostExecute((List)obj);
        }

        protected void onPostExecute(List list) {
            String s = null;
            if(list.size() > 0)
                s = ((miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry)list.get(0)).getPath();
            if(s != null && isVisible())
                setListMetaData(mParser.readListMetaData(s, mMetaData));
            mDownloadSet.remove(this);
        }

        final OnlineResourceListFragment this$0;

        public DownloadListMetaDataTask() {
            this$0 = OnlineResourceListFragment.this;
            super();
        }
    }

    public class DownloadRecommendationListTask extends DownloadFileTask {

        protected volatile void onPostExecute(Object obj) {
            onPostExecute((List)obj);
        }

        protected void onPostExecute(List list) {
            String s = null;
            if(list.size() > 0)
                s = ((miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry)list.get(0)).getPath();
            if(s != null && isVisible())
                setRecommendations(mParser.readRecommendations(s, mMetaData));
            mDownloadSet.remove(this);
        }

        final OnlineResourceListFragment this$0;

        public DownloadRecommendationListTask() {
            this$0 = OnlineResourceListFragment.this;
            super();
        }
    }

    public class DownloadCategoryListTask extends DownloadFileTask {

        protected volatile void onPostExecute(Object obj) {
            onPostExecute((List)obj);
        }

        protected void onPostExecute(List list) {
            String s = null;
            if(list.size() > 0)
                s = ((miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry)list.get(0)).getPath();
            if(s != null && isVisible())
                setCategories(mParser.readCategories(s, mMetaData));
            mDownloadSet.remove(this);
        }

        final OnlineResourceListFragment this$0;

        public DownloadCategoryListTask() {
            this$0 = OnlineResourceListFragment.this;
            super();
        }
    }


    public OnlineResourceListFragment() {
        mDownloadSet = new HashSet();
        mService = OnlineService.getInstance();
        mParser = ResourceJSONDataParser.getInstance();
        mFirstVisiable = true;
    }

    private ResourceCategory getCategoryHeader() {
        ResourceCategory resourcecategory = new ResourceCategory();
        resourcecategory.setName(getString(0x60c0027));
        return resourcecategory;
    }

    private void onlyShowSeeMoreTextView(boolean flag) {
        byte byte0 = 0;
        View view = getView().findViewById(0x60b0050);
        int i;
        View view1;
        if(flag)
            i = 0;
        else
            i = 8;
        view.setVisibility(i);
        view1 = getView().findViewById(0x60b004e);
        if(flag)
            byte0 = 4;
        view1.setVisibility(byte0);
    }

    protected void addMetaData(Bundle bundle) {
        super.addMetaData(bundle);
        String s = "";
        if(mIsRecommendList)
            s = super.mMetaData.getString("miui.app.resourcebrowser.RECOMMENDATION_ID");
        bundle.putString("miui.app.resourcebrowser.RESOURCE_SET_SUBPACKAGE", (new StringBuilder()).append(".online").append(s).toString());
    }

    protected void computeOnlineResourceStatus() {
        if(!super.mAdapter.getResourceSet().isEmpty()) {
            Iterator iterator = ((DataGroup)super.mAdapter.getResourceSet().get(0)).iterator();
            while(iterator.hasNext())  {
                Resource resource = (Resource)iterator.next();
                if((new File(resource.getLocalPath())).exists())
                    resource.updateStatus(0);
                else
                    resource.updateStatus(2);
            }
        }
    }

    protected ResourceAdapter getAdapter() {
        return new OnlineResourceAdapter(this, super.mMetaData);
    }

    protected int getContentView() {
        return 0x6030018;
    }

    protected View getHeaderView() {
        Object obj;
        if(!mIsRecommendList)
            obj = mUnevenGrid;
        else
        if(!mHasSubRecommends)
            obj = mTextView;
        else
            obj = null;
        return ((View) (obj));
    }

    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        if(android.provider.Settings.System.getInt(super.mActivity.getContentResolver(), "confirm_miui_disclaimer", 0) != 1)
            startActivityForResult(new Intent("android.intent.action.MIUI_DISCLAIMER"), 1000);
    }

    public void onActivityResult(int i, int j, Intent intent) {
        if(i == 1000 && j == 0)
            super.mActivity.finish();
        else
            super.onActivityResult(i, j, intent);
    }

    public void onResume() {
        super.onResume();
        addMetaData(super.mMetaData);
        onlyShowSeeMoreTextView(false);
        super.mAdapter.refreshDataSet();
    }

    protected void onVisiableChanged(boolean flag) {
        super.onVisiableChanged(flag);
        if(flag)
            if(mFirstVisiable) {
                mFirstVisiable = false;
                onlyShowSeeMoreTextView(false);
                ((OnlineResourceAdapter)super.mAdapter).setUrl(mUrl);
            } else {
                computeOnlineResourceStatus();
                super.mAdapter.notifyDataSetChanged();
            }
    }

    protected void pickMetaData(Bundle bundle) {
        super.pickMetaData(bundle);
        mIsRecommendList = super.mMetaData.getBoolean("miui.app.resourcebrowser.IS_RECOMMENDATION_LIST");
    }

    protected void requestCategories() {
        String s;
        String s1;
        File file;
        s = mService.getCategoryUrl(super.mMetaData);
        s1 = ResourceHelper.getFilePathByURL(mCategoryFolder, s);
        file = new File(s1);
        if(!file.exists()) goto _L2; else goto _L1
_L1:
        setCategories(mParser.readCategories(s1, super.mMetaData));
        if(!ResourceHelper.isCacheValid(file)) goto _L2; else goto _L3
_L3:
        return;
_L2:
        DownloadCategoryListTask downloadcategorylisttask = new DownloadCategoryListTask();
        downloadcategorylisttask.setId("category");
        miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry downloadfileentry = new miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry();
        downloadfileentry.setUrl(s);
        downloadfileentry.setPath(s1);
        if(!mDownloadSet.contains(downloadcategorylisttask)) {
            mDownloadSet.add(downloadcategorylisttask);
            miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry adownloadfileentry[] = new miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry[1];
            adownloadfileentry[0] = downloadfileentry;
            downloadcategorylisttask.execute(adownloadfileentry);
        }
        if(true) goto _L3; else goto _L4
_L4:
    }

    protected void requestListMetaData(String s) {
        String s1;
        String s2;
        File file;
        s1 = mService.getListMetaDataUrl(super.mMetaData, s);
        s2 = ResourceHelper.getFilePathByURL(mListFolder, s1);
        file = new File(s2);
        if(!file.exists()) goto _L2; else goto _L1
_L1:
        setListMetaData(mParser.readListMetaData(s2, super.mMetaData));
        if(!ResourceHelper.isCacheValid(file)) goto _L2; else goto _L3
_L3:
        return;
_L2:
        DownloadListMetaDataTask downloadlistmetadatatask = new DownloadListMetaDataTask();
        downloadlistmetadatatask.setId("listmetadata");
        miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry downloadfileentry = new miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry();
        downloadfileentry.setUrl(s1);
        downloadfileentry.setPath(s2);
        if(!mDownloadSet.contains(downloadlistmetadatatask)) {
            mDownloadSet.add(downloadlistmetadatatask);
            miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry adownloadfileentry[] = new miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry[1];
            adownloadfileentry[0] = downloadfileentry;
            downloadlistmetadatatask.execute(adownloadfileentry);
        }
        if(true) goto _L3; else goto _L4
_L4:
    }

    protected void requestRecommendations() {
        String s;
        String s1;
        File file;
        s = mService.getRecommendationUrl(super.mMetaData);
        s1 = ResourceHelper.getFilePathByURL(mRecommendationFolder, s);
        file = new File(s1);
        if(!file.exists()) goto _L2; else goto _L1
_L1:
        setRecommendations(mParser.readRecommendations(s1, super.mMetaData));
        if(!ResourceHelper.isCacheValid(file)) goto _L2; else goto _L3
_L3:
        return;
_L2:
        DownloadRecommendationListTask downloadrecommendationlisttask = new DownloadRecommendationListTask();
        downloadrecommendationlisttask.setId("recommendation");
        miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry downloadfileentry = new miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry();
        downloadfileentry.setUrl(s);
        downloadfileentry.setPath(s1);
        if(!mDownloadSet.contains(downloadrecommendationlisttask)) {
            mDownloadSet.add(downloadrecommendationlisttask);
            miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry adownloadfileentry[] = new miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry[1];
            adownloadfileentry[0] = downloadfileentry;
            downloadrecommendationlisttask.execute(adownloadfileentry);
        }
        if(true) goto _L3; else goto _L4
_L4:
    }

    protected void setCategories(List list) {
        if(list != null) {
            mCategoryAdapter.clear();
            mCategoryAdapter.add(getCategoryHeader());
            for(int i = 0; i < list.size(); i++)
                mCategoryAdapter.add(list.get(i));

        }
    }

    protected void setListMetaData(ListMetaData listmetadata) {
        if(listmetadata != null) {
            mTextView.setText(listmetadata.getDescription());
            mTextView.setVisibility(0);
        } else {
            mTextView.setVisibility(8);
        }
    }

    protected void setRecommendations(List list) {
        if(list != null) {
            mUnevenGrid.updateData(list);
            mUnevenGrid.setVisibility(0);
        } else {
            mUnevenGrid.setVisibility(8);
        }
    }

    protected void setSubRecommendations(List list) {
        if(list != null) {
            mSubRecommendAdapter.clear();
            for(int i = 0; i < list.size(); i++)
                mSubRecommendAdapter.add(list.get(i));

        }
    }

    protected void setupUI() {
        mCategoryFolder = ExtraFileUtils.standardizeFolderPath((new StringBuilder()).append(ResourceConstants.CATEGORY_PATH).append(super.mResourceSetCode).toString());
        mRecommendationFolder = ExtraFileUtils.standardizeFolderPath((new StringBuilder()).append(ResourceConstants.RECOMMENDATION_PATH).append(super.mResourceSetCode).toString());
        mListFolder = ExtraFileUtils.standardizeFolderPath((new StringBuilder()).append(ExtraFileUtils.standardizeFolderPath(super.mMetaData.getString("miui.app.resourcebrowser.CACHE_LIST_FOLDER"))).append(super.mResourceSetCode).toString());
        List list = (List)super.mMetaData.getSerializable("miui.app.resourcebrowser.SUB_RECOMMENDATIONS");
        boolean flag;
        if(mIsRecommendList && list != null && list.size() > 0)
            flag = true;
        else
            flag = false;
        mHasSubRecommends = flag;
        if(!mIsRecommendList) {
            mUnevenGrid = new UnevenGrid(getActivity());
            mUnevenGrid.setGridItemFactory(new RecommendGridItemFactory(getActivity(), super.mMetaData));
            mUnevenGrid.setBackgroundResource(0x602018e);
            int j = ResourceHelper.getThumbnailGap(super.mActivity);
            int k = j * 2;
            mUnevenGrid.setPadding(mUnevenGrid.getPaddingLeft(), j, mUnevenGrid.getPaddingRight(), k);
            mUnevenGrid.setGridItemRatio(218, 132);
            mUnevenGrid.setGridItemGap(j);
            mUnevenGrid.setColumnCount(2);
            Point point = new Point();
            getActivity().getWindowManager().getDefaultDisplay().getSize(point);
            int l = (point.x - mUnevenGrid.getPaddingLeft() - mUnevenGrid.getPaddingRight() - j * 1) / 2;
            super.mMetaData.putInt("miui.app.resourcebrowser.RECOMMENDATION_WIDTH", l);
        } else
        if(!mHasSubRecommends) {
            mTextView = new TextView(getActivity());
            mTextView.setGravity(19);
            mTextView.setBackgroundResource(0x602018e);
        }
        super.setupUI();
        onlyShowSeeMoreTextView(true);
        if(!mIsRecommendList) {
            mUrl = mService.getCommonListUrl(super.mMetaData, null);
            super.mMetaData.putString("miui.app.resourcebrowser.LIST_URL", mUrl);
            requestRecommendations();
        } else
        if(!mHasSubRecommends) {
            String s1 = super.mMetaData.getString("miui.app.resourcebrowser.RECOMMENDATION_ID");
            mUrl = mService.getRecommendationListUrl(super.mMetaData, s1, null);
            requestListMetaData(s1);
        } else {
            getView().findViewById(0x60b0051).setVisibility(0);
            String s = super.mMetaData.getString("miui.app.resourcebrowser.RECOMMENDATION_ID");
            mUrl = mService.getRecommendationListUrl(super.mMetaData, s, null);
            mSubRecommendList = (Spinner)getView().findViewById(0x60b0080);
            mSubRecommendAdapter = new ArrayAdapter(super.mActivity, 0x1090008);
            mSubRecommendAdapter.setDropDownViewResource(0x1090009);
            mSubRecommendList.setAdapter(mSubRecommendAdapter);
            mSubRecommendList.setVisibility(0);
            mSubRecommendList.setOnItemSelectedListener(new android.widget.AdapterView.OnItemSelectedListener() {

                public void onItemSelected(AdapterView adapterview, View view, int i1, long l1) {
                    String s2 = ((RecommendItemData)mSubRecommendAdapter.getItem(i1)).itemId;
                    String s3 = null;
                    if(mCategoryList != null) {
                        ResourceCategory resourcecategory = (ResourceCategory)mCategoryList.getSelectedItem();
                        if(resourcecategory != null)
                            s3 = resourcecategory.getCode();
                        else
                            s3 = null;
                    }
                    mUrl = mService.getRecommendationListUrl(mMetaData, s2, s3);
                    ((OnlineResourceAdapter)mAdapter).setUrl(mUrl);
                }

                public void onNothingSelected(AdapterView adapterview) {
                }

                final OnlineResourceListFragment this$0;

             {
                this$0 = OnlineResourceListFragment.this;
                super();
            }
            });
            setSubRecommendations(list);
            mSubRecommendList.setSelection(0);
            if(super.mMetaData.getBoolean("miui.app.resourcebrowser.CATEGORY_SUPPORTED")) {
                mCategoryList = (Spinner)getView().findViewById(0x60b0069);
                mCategoryAdapter = new ArrayAdapter(super.mActivity, 0x1090008);
                mCategoryAdapter.setDropDownViewResource(0x1090009);
                mCategoryList.setAdapter(mCategoryAdapter);
                int i = ResourceHelper.getThumbnailGap(super.mActivity);
                ((android.widget.LinearLayout.LayoutParams)mCategoryList.getLayoutParams()).setMargins(i, 0, 0, 0);
                mCategoryList.setVisibility(0);
                mCategoryList.setOnItemSelectedListener(new android.widget.AdapterView.OnItemSelectedListener() {

                    public void onItemSelected(AdapterView adapterview, View view, int i1, long l1) {
                        String s2 = ((ResourceCategory)mCategoryAdapter.getItem(i1)).getCode();
                        String s3 = null;
                        if(mSubRecommendList != null) {
                            RecommendItemData recommenditemdata = (RecommendItemData)mSubRecommendList.getSelectedItem();
                            if(recommenditemdata != null)
                                s3 = recommenditemdata.itemId;
                            else
                                s3 = null;
                        }
                        mUrl = mService.getRecommendationListUrl(mMetaData, s3, s2);
                        ((OnlineResourceAdapter)mAdapter).setUrl(mUrl);
                    }

                    public void onNothingSelected(AdapterView adapterview) {
                    }

                    final OnlineResourceListFragment this$0;

             {
                this$0 = OnlineResourceListFragment.this;
                super();
            }
                });
                requestCategories();
            }
        }
        ((OnlineResourceAdapter)super.mAdapter).setUrl(mUrl);
    }

    public void startDetailActivityForResource(Pair pair) {
        super.mMetaData.putString("miui.app.resourcebrowser.LIST_URL", mUrl);
        super.startDetailActivityForResource(pair);
    }

    private static final int CONFIRM_MIUI_DISCLAIMER_REQUEST = 1000;
    private ArrayAdapter mCategoryAdapter;
    private String mCategoryFolder;
    private Spinner mCategoryList;
    private Set mDownloadSet;
    private boolean mFirstVisiable;
    private boolean mHasSubRecommends;
    private boolean mIsRecommendList;
    private String mListFolder;
    private ResourceDataParser mParser;
    private String mRecommendationFolder;
    private OnlineService mService;
    private ArrayAdapter mSubRecommendAdapter;
    private Spinner mSubRecommendList;
    private TextView mTextView;
    private UnevenGrid mUnevenGrid;
    private String mUrl;





/*
    static String access$202(OnlineResourceListFragment onlineresourcelistfragment, String s) {
        onlineresourcelistfragment.mUrl = s;
        return s;
    }

*/





}
