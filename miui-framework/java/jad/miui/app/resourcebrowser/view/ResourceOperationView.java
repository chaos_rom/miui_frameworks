// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser.view;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.*;

// Referenced classes of package miui.app.resourcebrowser.view:
//            ResourceOperationHandler

public class ResourceOperationView extends LinearLayout {
    public static class UIParameter {

        public int deleteBtnBgId;
        public int deleteBtnSrcId;
        public int magicBtnBgId;
        public int magicBtnSrcId;
        public int operationViewBgId;

        public UIParameter() {
            operationViewBgId = 0;
            magicBtnBgId = 0;
            magicBtnSrcId = 0;
            deleteBtnBgId = 0;
            deleteBtnSrcId = 0;
        }
    }


    public ResourceOperationView(Context context) {
        this(context, null);
    }

    public ResourceOperationView(Context context, AttributeSet attributeset) {
        this(context, attributeset, 0);
    }

    public ResourceOperationView(Context context, AttributeSet attributeset, int i) {
        super(context, attributeset, i);
        mHandler = new Handler();
    }

    private void disableAndDelayEnableView(final View v) {
        v.setEnabled(false);
        mHandler.postDelayed(new Runnable() {

            public void run() {
                v.setEnabled(true);
            }

            final ResourceOperationView this$0;
            final View val$v;

             {
                this$0 = ResourceOperationView.this;
                v = view;
                super();
            }
        }, 300L);
    }

    private UIParameter getDefaultUIParameter() {
        UIParameter uiparameter = new UIParameter();
        uiparameter.operationViewBgId = 0x60201b7;
        uiparameter.deleteBtnSrcId = 0x60201a9;
        return uiparameter;
    }

    public void init(ResourceOperationHandler resourceoperationhandler, UIParameter uiparameter) {
        if(resourceoperationhandler == null)
            throw new RuntimeException("Operated handler can not be null.");
        mUIparameter = uiparameter;
        if(mUIparameter == null)
            mUIparameter = getDefaultUIParameter();
        mActivity = resourceoperationhandler.mActivity;
        mOperationHandler = resourceoperationhandler;
        setupUI();
    }

    protected void setApplyOrPickStatus() {
        String s = null;
        if(mOperationHandler.isLocalResource() && !mOperationHandler.isDownloading())
            if(mOperationHandler.isPicker())
                s = mContext.getString(0x60c001b);
            else
                s = mContext.getString(0x60c0019);
        if(TextUtils.isEmpty(s)) {
            mApplyBtn.setVisibility(8);
        } else {
            mApplyBtn.setVisibility(0);
            mApplyBtn.setText(s);
        }
    }

    protected void setDeleteStatus() {
        mDeleteBtn.setBackgroundResource(mUIparameter.deleteBtnBgId);
        mDeleteBtn.setImageResource(mUIparameter.deleteBtnSrcId);
        ImageView imageview = mDeleteBtn;
        int i;
        if(mOperationHandler.isDeletable() && !mOperationHandler.isDownloading())
            i = 0;
        else
            i = 4;
        imageview.setVisibility(i);
    }

    protected void setDownloadOrUpdateStatus() {
        boolean flag = true;
        String s = null;
        if(mOperationHandler.isDownloading()) {
            s = mContext.getString(0x60c0018);
            flag = false;
        } else
        if(mOperationHandler.isOldVersionResource())
            s = mContext.getString(0x60c001a);
        else
        if(!mOperationHandler.isLocalResource())
            s = mContext.getString(0x60c0017);
        if(TextUtils.isEmpty(s)) {
            mDownloadBtn.setVisibility(8);
        } else {
            mDownloadBtn.setEnabled(flag);
            mDownloadBtn.setVisibility(0);
            mDownloadBtn.setText(s);
        }
    }

    protected void setDownloadProgressOrPreviewNavigationStatus() {
        if(mOperationHandler.isDownloading())
            mDownloadProgress.setVisibility(0);
        else
            mDownloadProgress.setVisibility(8);
    }

    protected void setMagicStatus() {
        if(mUIparameter.magicBtnBgId != 0 || mUIparameter.magicBtnSrcId != 0) {
            mMagicBtn.setVisibility(0);
            mMagicBtn.setBackgroundResource(mUIparameter.magicBtnBgId);
            mMagicBtn.setImageResource(mUIparameter.magicBtnSrcId);
        } else {
            mMagicBtn.setVisibility(8);
        }
    }

    protected void setResourceStatus() {
        setApplyOrPickStatus();
        setDownloadOrUpdateStatus();
        setDeleteStatus();
        setMagicStatus();
    }

    protected void setupUI() {
        findViewById(0x60b006c).setBackgroundResource(mUIparameter.operationViewBgId);
        mDownloadBtn = (TextView)findViewById(0x60b005a);
        mDownloadBtn.setOnClickListener(new android.view.View.OnClickListener() {

            public void onClick(View view) {
                mOperationHandler.handleDownloadEvent();
            }

            final ResourceOperationView this$0;

             {
                this$0 = ResourceOperationView.this;
                super();
            }
        });
        mApplyBtn = (TextView)findViewById(0x60b005b);
        mApplyBtn.setOnClickListener(new android.view.View.OnClickListener() {

            public void onClick(View view) {
                disableAndDelayEnableView(view);
                if(mOperationHandler.isPicker())
                    mOperationHandler.handlePickEvent();
                else
                    mOperationHandler.handleApplyEvent();
            }

            final ResourceOperationView this$0;

             {
                this$0 = ResourceOperationView.this;
                super();
            }
        });
        mDeleteBtn = (ImageView)findViewById(0x60b005c);
        mDeleteBtn.setOnClickListener(new android.view.View.OnClickListener() {

            public void onClick(View view) {
                (new android.app.AlertDialog.Builder(mActivity)).setTitle(0x60c0176).setIconAttribute(0x1010355).setMessage(0x60c0026).setNegativeButton(0x1040000, null).setPositiveButton(0x104000a, new android.content.DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialoginterface, int i) {
                        mOperationHandler.handleDeleteEvent();
                    }

                    final _cls4 this$1;

                     {
                        this$1 = _cls4.this;
                        super();
                    }
                }).show();
            }

            final ResourceOperationView this$0;

             {
                this$0 = ResourceOperationView.this;
                super();
            }
        });
        mMagicBtn = (ImageView)findViewById(0x60b005d);
        mMagicBtn.setOnClickListener(new android.view.View.OnClickListener() {

            public void onClick(View view) {
                disableAndDelayEnableView(view);
                mOperationHandler.handleMagicEvent();
            }

            final ResourceOperationView this$0;

             {
                this$0 = ResourceOperationView.this;
                super();
            }
        });
        mDownloadProgress = (ProgressBar)findViewById(0x60b004d);
        mDownloadProgress.setMax(100);
        mDownloadProgress.setVisibility(8);
        setResourceStatus();
    }

    protected void updateDownloadProgressBar(int i, int j) {
        int k = (int)((100D * (double)i) / (double)j);
        mDownloadProgress.setProgress(k);
    }

    protected Activity mActivity;
    protected TextView mApplyBtn;
    protected ImageView mDeleteBtn;
    protected TextView mDownloadBtn;
    protected ProgressBar mDownloadProgress;
    protected Handler mHandler;
    protected ImageView mMagicBtn;
    protected ResourceOperationHandler mOperationHandler;
    protected UIParameter mUIparameter;

}
