// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser.view;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.text.TextUtils;
import android.util.Pair;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.*;
import android.widget.Button;
import android.widget.ProgressBar;
import java.util.ArrayList;
import java.util.Iterator;
import miui.app.resourcebrowser.*;
import miui.app.resourcebrowser.resource.Resource;
import miui.app.resourcebrowser.util.ResourceHelper;
import miui.app.resourcebrowser.util.ResourceMusicPlayer;

// Referenced classes of package miui.app.resourcebrowser.view:
//            BatchResourceHandler

public class AudioBatchResourceHandler extends BatchResourceHandler {
    public class ResourceMusicUI
        implements android.view.View.OnClickListener {

        private void clearGoneViews(View view) {
            Iterator iterator = mGoneViewWhenClick.iterator();
            do {
                if(!iterator.hasNext())
                    break;
                View view1 = (View)iterator.next();
                if(view1 != view)
                    setOperatingViewsState(view1, getResource(mSelectPosition), false);
            } while(true);
            mGoneViewWhenClick.clear();
        }

        private boolean isPicker() {
            boolean flag;
            if(mFragment != null)
                flag = mFragment.isPicker();
            else
                flag = ((ResourceListActivity)mActivity).isPicker();
            return flag;
        }

        private void realSetOperateButtonState(View view, String s, int i) {
            Button button = (Button)view.findViewById(0x60b006b);
            if(i == 0) {
                button.setVisibility(8);
            } else {
                button.setText(s);
                button.setOnClickListener(mMusicUI);
                button.setVisibility(0);
                button.setTag(new Pair((Pair)view.getTag(), Integer.valueOf(i)));
            }
        }

        private void refreshPlayProgressBarState(View view, Resource resource, boolean flag) {
            ProgressBar progressbar = (ProgressBar)(ProgressBar)view.findViewById(0x60b005e);
            if(!flag || !mMusicPlayer.isPlaying() || TextUtils.isEmpty(resource.getLocalPath())) {
                progressbar.setVisibility(8);
            } else {
                progressbar.setVisibility(0);
                setProgressBarState(progressbar, true, -1);
            }
        }

        private void setApplyOrPickState(View view, boolean flag) {
            int i;
            byte byte0;
            if(flag) {
                i = 0x60c001b;
                byte0 = 2;
            } else {
                i = 0x60c0019;
                byte0 = 1;
            }
            realSetOperateButtonState(view, mActivity.getString(i), byte0);
        }

        private void setDownloadOrCancelState(View view, Resource resource, boolean flag) {
            String s;
            byte byte0;
            if(flag) {
                s = (new StringBuilder()).append(mActivity.getString(0x1040000)).append(mActivity.getString(0x60c0017)).toString();
                setDownloadProgress((ProgressBar)view.findViewById(0x60b004d), resource);
                byte0 = 4;
            } else {
                s = (new StringBuilder()).append(mActivity.getString(0x60c0017)).append("(").append(ResourceHelper.getFormattedSize(resource.getFileSize())).append(")").toString();
                byte0 = 3;
            }
            realSetOperateButtonState(view, s, byte0);
        }

        private void setOperatingViewsState(View view, Resource resource, boolean flag) {
            boolean flag1 = false;
            if(flag || isEditMode())
                view.findViewById(0x60b004c).setVisibility(4);
            else
                view.findViewById(0x60b004c).setVisibility(0);
            refreshPlayProgressBarState(view, resource, flag);
            if(!flag) {
                if(!mLocalResourcePage && resource != null && isDownloading(resource))
                    setDownloadOrCancelState(view, resource, true);
                else
                    realSetOperateButtonState(view, null, 0);
            } else {
                if(mLocalResourcePage || ResourceHelper.isSystemResource(resource.getLocalPath()) || canDeletableResource(resource))
                    flag1 = true;
                if(flag1)
                    setApplyOrPickState(view, isPicker());
                else
                    setDownloadOrCancelState(view, resource, isDownloading(resource));
            }
        }

        private void setProgressBarState(ProgressBar progressbar, boolean flag, int i) {
            if(progressbar.getVisibility() == 0) {
                progressbar.setIndeterminate(flag);
                progressbar.setMax(100);
                if(i < 0)
                    i = mLastPlayProgress;
                progressbar.setProgress(i);
            }
        }

        private void startProgressBarAnimation(View view, boolean flag) {
            ProgressBar progressbar = (ProgressBar)view.findViewById(0x60b005e);
            if(progressbar.getVisibility() == 0) {
                View view1 = view.findViewById(0x60b005f);
                AlphaAnimation alphaanimation;
                TranslateAnimation translateanimation;
                if(flag) {
                    progressbar.setVisibility(8);
                    alphaanimation = new AlphaAnimation(1.0F, 0.0F);
                    translateanimation = new TranslateAnimation(view1.getLeft() - progressbar.getLeft(), 0.0F, 0.0F, 0.0F);
                } else {
                    float f = TypedValue.applyDimension(1, 49.33F, mActivity.getResources().getDisplayMetrics());
                    alphaanimation = new AlphaAnimation(0.0F, 1.0F);
                    translateanimation = new TranslateAnimation(-f, 0.0F, 0.0F, 0.0F);
                }
                alphaanimation.setDuration(250L);
                translateanimation.setDuration(250L);
                progressbar.startAnimation(alphaanimation);
                view1.startAnimation(translateanimation);
            }
        }

        public void clickMusicUI(View view) {
            clearGoneViews(view);
            Pair pair = (Pair)view.getTag();
            setOperatingViewsState(view, getResource(pair), true);
            mLastSelectPosition = mSelectPosition;
            mSelectPosition = pair;
            mGoneViewWhenClick.add(view);
            startProgressBarAnimation(view, false);
        }

        public miui.app.resourcebrowser.util.ResourceMusicPlayer.PlayProgressListener getMusicPlayListener() {
            return new miui.app.resourcebrowser.util.ResourceMusicPlayer.PlayProgressListener() {

                public void onProgressUpdate(int i, int j) {
                    int k = (int)((1.0D * (double)(i * 100)) / (double)j);
                    mLastPlayProgress = k;
                    View view;
                    for(Iterator iterator = mGoneViewWhenClick.iterator(); iterator.hasNext(); setProgressBarState((ProgressBar)view.findViewById(0x60b005e), false, mLastPlayProgress))
                        view = (View)iterator.next();

                }

                public void onStartPlaying() {
                    mLastPlayProgress = 0;
                    View view;
                    for(Iterator iterator = mGoneViewWhenClick.iterator(); iterator.hasNext(); setProgressBarState((ProgressBar)view.findViewById(0x60b005e), false, mLastPlayProgress))
                        view = (View)iterator.next();

                }

                public void onStopPlaying() {
                    Iterator iterator = mGoneViewWhenClick.iterator();
                    do {
                        if(!iterator.hasNext())
                            break;
                        View view = (View)iterator.next();
                        Pair pair = (Pair)view.getTag();
                        if(pair.equals(mSelectPosition) || pair.equals(mLastSelectPosition))
                            startProgressBarAnimation(view, true);
                    } while(true);
                }

                final ResourceMusicUI this$1;

                 {
                    this$1 = ResourceMusicUI.this;
                    super();
                }
            };
        }

        public void initMusicUI(View view) {
            Pair pair = (Pair)view.getTag();
            Resource resource = getResource(pair);
            boolean flag = pair.equals(mSelectPosition);
            setOperatingViewsState(view, resource, flag);
            if(flag)
                mGoneViewWhenClick.add(view);
        }

        public void onClick(View view) {
            Button button;
            Pair pair1;
            int i;
            Resource resource;
            button = (Button)view;
            Pair pair = (Pair)button.getTag();
            pair1 = (Pair)pair.first;
            i = ((Integer)pair.second).intValue();
            resource = getResource(pair1);
            i;
            JVM INSTR tableswitch 1 4: default 76
        //                       1 77
        //                       2 89
        //                       3 101
        //                       4 190;
               goto _L1 _L2 _L3 _L4 _L5
_L1:
            return;
_L2:
            handleApplyEvent(resource);
            continue; /* Loop/switch isn't completed */
_L3:
            handlePickEvent(resource);
            continue; /* Loop/switch isn't completed */
_L4:
            handleDownloadEvent(resource);
            button.setText((new StringBuilder()).append(mActivity.getString(0x1040000)).append(mActivity.getString(0x60c0017)).toString());
            button.setTag(new Pair(pair1, Integer.valueOf(4)));
            ((View)button.getParent()).findViewById(0x60b004d).setVisibility(0);
            continue; /* Loop/switch isn't completed */
_L5:
            handleCancelDownloadEvent(resource);
            if(true) goto _L1; else goto _L6
_L6:
        }

        public void reset() {
            clearGoneViews(null);
            mSelectPosition = null;
        }

        private static final int ANIMATION_LAST_TIME = 250;
        private static final int BTN_STATE_APPLY = 1;
        private static final int BTN_STATE_CANCEL_DOWNLOAD = 4;
        private static final int BTN_STATE_DOWNLOAD = 3;
        private static final int BTN_STATE_NONE = 0;
        private static final int BTN_STATE_PICK = 2;
        private static final int PROGRESS_MAX = 100;
        private ArrayList mGoneViewWhenClick;
        int mLastPlayProgress;
        private Pair mLastSelectPosition;
        private Pair mSelectPosition;
        final AudioBatchResourceHandler this$0;






        public ResourceMusicUI() {
            this$0 = AudioBatchResourceHandler.this;
            super();
            mGoneViewWhenClick = new ArrayList();
        }
    }


    public AudioBatchResourceHandler(ResourceListActivity resourcelistactivity, ResourceAdapter resourceadapter) {
        super(resourcelistactivity, resourceadapter);
        init();
    }

    public AudioBatchResourceHandler(ResourceListFragment resourcelistfragment, ResourceAdapter resourceadapter) {
        super(resourcelistfragment, resourceadapter);
        init();
    }

    private void clickMusicView(View view) {
        Resource resource = getResource((Pair)view.getTag());
        if(resource != null) {
            boolean flag = mMusicPlayer.canPlay(resource);
            mMusicPlayer.stopMusic();
            if(flag)
                mMusicPlayer.playMusic(resource);
            mMusicUI.clickMusicUI(view);
        }
    }

    protected void enterEditMode(View view, Pair pair) {
        super.enterEditMode(view, pair);
        mMusicPlayer.stopMusic();
        mMusicUI.reset();
    }

    protected String getFormatPlayingRingtoneName(String s, int i, int j) {
        return ResourceHelper.getDefaultFormatPlayingRingtoneName(s, i, j);
    }

    protected void handleApplyEvent(Resource resource) {
    }

    protected void handleCancelDownloadEvent(Resource resource) {
        doCancelDownload(resource);
    }

    protected void handleDownloadEvent(Resource resource) {
        doDownloadResource(resource);
    }

    protected void handlePickEvent(Resource resource) {
        Intent intent = new Intent();
        intent.putExtra("miui.app.resourcebrowser.PICKED_RESOURCE", resource.getLocalPath());
        intent.putExtra("android.intent.extra.ringtone.PICKED_URI", ResourceHelper.getUriByPath(resource.getLocalPath()));
        super.mAdapter.mContext.setResult(-1, intent);
        super.mAdapter.mContext.finish();
    }

    protected void init() {
        mMusicPlayer = new ResourceMusicPlayer(super.mActivity, false);
        mMusicUI = new ResourceMusicUI();
        mMusicPlayer.regeistePlayProgressListener(mMusicUI.getMusicPlayListener());
        super.mListenDownloadProgress = true;
    }

    public void initViewState(View view, Pair pair) {
        super.initViewState(view, pair);
        mMusicUI.initMusicUI(view);
    }

    protected void onClick_Impl(View view) {
        if(!isEditMode() && super.mAdapter.mDisplayType == 5)
            clickMusicView(view);
        else
            super.onClick_Impl(view);
    }

    public void quitEditMode() {
        super.quitEditMode();
        mMusicPlayer.stopMusic();
        mMusicUI.reset();
    }

    public void viewStateChanged(BatchResourceHandler.ViewState viewstate) {
        super.viewStateChanged(viewstate);
        mMusicPlayer.stopMusic();
        if(viewstate != BatchResourceHandler.ViewState.PAUSE)
            mMusicUI.reset();
    }

    protected ResourceMusicPlayer mMusicPlayer;
    protected ResourceMusicUI mMusicUI;
}
