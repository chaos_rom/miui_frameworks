// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser.view;

import android.os.Parcel;

public class ResourceState {

    public ResourceState() {
    }

    public ResourceState(ResourceState resourcestate) {
        inLocalPage = resourcestate.inLocalPage;
        localPath = resourcestate.localPath;
        downloadUrl = resourcestate.downloadUrl;
        downloadSavePath = resourcestate.downloadSavePath;
        title = resourcestate.title;
        uiVersion = resourcestate.uiVersion;
        hasUpdate = resourcestate.hasUpdate;
        allowDelete = resourcestate.allowDelete;
        isPicker = resourcestate.isPicker;
    }

    protected void readDataFromParcel(Parcel parcel) {
        inLocalPage = Boolean.valueOf(parcel.readString()).booleanValue();
        localPath = parcel.readString();
        downloadUrl = parcel.readString();
        downloadSavePath = parcel.readString();
        title = parcel.readString();
        uiVersion = parcel.readInt();
        hasUpdate = Boolean.valueOf(parcel.readString()).booleanValue();
        allowDelete = Boolean.valueOf(parcel.readString()).booleanValue();
        isPicker = Boolean.valueOf(parcel.readString()).booleanValue();
        if(localPath == null)
            throw new RuntimeException("ResourceState.localPath can not be null.");
        else
            return;
    }

    public String toString() {
        StringBuilder stringbuilder = new StringBuilder();
        stringbuilder.append("\n");
        stringbuilder.append((new StringBuilder()).append("inLocalPage: ").append(inLocalPage).append("\n").toString());
        stringbuilder.append((new StringBuilder()).append("localPath: ").append(localPath).append("\n").toString());
        stringbuilder.append((new StringBuilder()).append("downloadUrl: ").append(downloadUrl).append("\n").toString());
        stringbuilder.append((new StringBuilder()).append("downloadSavePath: ").append(downloadSavePath).append("\n").toString());
        stringbuilder.append((new StringBuilder()).append("title: ").append(title).append("\n").toString());
        stringbuilder.append((new StringBuilder()).append("uiVersion: ").append(uiVersion).append("\n").toString());
        stringbuilder.append((new StringBuilder()).append("hasUpdate: ").append(hasUpdate).append("\n").toString());
        stringbuilder.append((new StringBuilder()).append("allowDelete: ").append(allowDelete).append("\n").toString());
        stringbuilder.append((new StringBuilder()).append("isPicker: ").append(isPicker).append("\n").toString());
        return stringbuilder.toString();
    }

    protected void writeDataToParcel(Parcel parcel) {
        parcel.writeString(String.valueOf(inLocalPage));
        parcel.writeString(localPath);
        parcel.writeString(downloadUrl);
        parcel.writeString(downloadSavePath);
        parcel.writeString(title);
        parcel.writeInt(uiVersion);
        parcel.writeString(String.valueOf(hasUpdate));
        parcel.writeString(String.valueOf(allowDelete));
        parcel.writeString(String.valueOf(isPicker));
    }

    public boolean allowDelete;
    public String downloadSavePath;
    public String downloadUrl;
    public boolean hasUpdate;
    public boolean inLocalPage;
    public boolean isPicker;
    public String localPath;
    public String title;
    public int uiVersion;
}
