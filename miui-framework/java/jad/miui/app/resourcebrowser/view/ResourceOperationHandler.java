// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;
import java.io.File;
import miui.app.resourcebrowser.util.ResourceDebug;
import miui.app.resourcebrowser.util.ResourceDownloadHandler;

// Referenced classes of package miui.app.resourcebrowser.view:
//            ResourceState, ResourceOperationView

public class ResourceOperationHandler {

    public ResourceOperationHandler(Activity activity, ResourceOperationView resourceoperationview, ResourceState resourcestate) {
        if(resourceoperationview == null)
            throw new RuntimeException("Operated view can not be null.");
        if(DBG)
            Log.i("Theme", resourcestate.toString());
        mResource = resourcestate;
        mOperationView = resourceoperationview;
        mActivity = activity;
        mDownloadHandler = getResourceDownloadHandler(mActivity);
        mDownloadHandler.registerDownloadReceiver();
    }

    private void doDownloadResource() {
        mDownloadHandler.downloadResource(mResource.downloadUrl, mResource.downloadSavePath, mResource.title);
    }

    private ResourceDownloadHandler getResourceDownloadHandler(Context context) {
        return new ResourceDownloadHandler(context) {

            public void handleDownloadFailed(String s) {
                handleResourceDownloadFailedEvent(s);
            }

            public void handleDownloadProgress(String s, int i, int j) {
                handleResourceDownloadProgressEvent(s, i, j);
            }

            public void handleDownloadSuccessed(String s) {
                handleResourceDownloadSuccessedEvent(s);
            }

            final ResourceOperationHandler this$0;

             {
                this$0 = ResourceOperationHandler.this;
                super(context);
            }
        };
    }

    private boolean isVirtualResource() {
        boolean flag;
        if(TextUtils.isEmpty(mResource.localPath))
            flag = TextUtils.isEmpty(mResource.downloadUrl);
        else
            flag = mResource.localPath.startsWith("content://");
        return flag;
    }

    protected void clean() {
        mDownloadHandler.unregisterDownloadReceiver();
    }

    protected void finalize() throws Throwable {
        super.finalize();
        clean();
    }

    public ResourceState getResourceState() {
        return mResource;
    }

    public void handleApplyEvent() {
        if(DBG)
            Log.i("Theme", (new StringBuilder()).append("OperationHandler.handleApplyEvent(): ").append(mResource.localPath).toString());
        refreshUI();
    }

    public void handleDeleteEvent() {
        if(DBG)
            Log.i("Theme", (new StringBuilder()).append("OperationHandler.handleDeleteEvent(): ").append(mResource.localPath).toString());
        (new File(mResource.localPath)).delete();
        refreshUI();
    }

    public void handleDownloadEvent() {
        if(DBG)
            Log.i("Theme", (new StringBuilder()).append("OperationHandler.handleDownloadEvent(): ").append(mResource.localPath).toString());
        doDownloadResource();
        refreshUI();
    }

    public void handleMagicEvent() {
        if(DBG)
            Log.i("Theme", (new StringBuilder()).append("OperationHandler.handleMagicEvent(): ").append(mResource.localPath).toString());
        refreshUI();
    }

    public void handlePickEvent() {
        if(DBG)
            Log.i("Theme", (new StringBuilder()).append("OperationHandler.handlePickEvent(): ").append(mResource.localPath).toString());
        refreshUI();
    }

    public void handleResourceDownloadFailedEvent(String s) {
        if(DBG)
            Log.i("Theme", (new StringBuilder()).append("handleDownloadFailedEvent(): ").append(s).toString());
        if(isCurrentResourceDownloadSavePath(s)) {
            Toast.makeText(mActivity, 0x60c0023, 0).show();
            refreshUI(null);
        }
    }

    public void handleResourceDownloadProgressEvent(String s, int i, int j) {
        if(DBG)
            Log.i("Theme", (new StringBuilder()).append("OperationHandler.handleResourceDownloadProgressEvent(): ").append(s).toString());
        if(isCurrentResourceDownloadSavePath(s))
            mOperationView.updateDownloadProgressBar(i, j);
    }

    public void handleResourceDownloadSuccessedEvent(String s) {
        if(DBG)
            Log.i("Theme", (new StringBuilder()).append("OperationHandler.handleDownloadSuccessedEvent(): ").append(s).toString());
        if(isCurrentResourceDownloadSavePath(s))
            refreshUI(null);
    }

    public void handleUpdateEvent() {
        if(DBG)
            Log.i("Theme", (new StringBuilder()).append("OperationHandler.handleUpdateEvent(): ").append(mResource.localPath).toString());
        doDownloadResource();
        refreshUI();
    }

    public boolean isCurrentResourceDownloadSavePath(String s) {
        return TextUtils.equals(mResource.downloadSavePath, s);
    }

    public boolean isDeletable() {
        boolean flag;
        if(mResource.allowDelete && isLocalResource() && !isVirtualResource())
            flag = true;
        else
            flag = false;
        return flag;
    }

    public boolean isDownloading() {
        return mDownloadHandler.isResourceDownloading(mResource.downloadSavePath);
    }

    public boolean isLocalResource() {
        boolean flag;
        if((new File(mResource.localPath)).exists() || isVirtualResource())
            flag = true;
        else
            flag = false;
        return flag;
    }

    public boolean isOldVersionResource() {
        return mResource.hasUpdate;
    }

    public boolean isPicker() {
        return mResource.isPicker;
    }

    public void onActivitResultEvent(int i, int j, Intent intent) {
        if(DBG)
            Log.i("Theme", "OperationHandler.onActivitResultEvent()");
    }

    public void onActivityOnDestroyEvent() {
        if(DBG)
            Log.i("Theme", "OperationHandler.onActivityOnDestroyEvent()");
        clean();
    }

    public void onActivityOnPauseEvent() {
    }

    public void onActivityOnResumeEvent() {
    }

    public void refreshUI() {
        refreshUI(null);
    }

    public void refreshUI(ResourceState resourcestate) {
        if(resourcestate != null)
            mResource = resourcestate;
        mOperationView.setResourceStatus();
    }

    protected static final boolean DBG = false;
    protected static final String TAG = "Theme";
    protected Activity mActivity;
    protected ResourceDownloadHandler mDownloadHandler;
    protected ResourceOperationView mOperationView;
    protected ResourceState mResource;

    static  {
        DBG = ResourceDebug.DEBUG;
    }
}
