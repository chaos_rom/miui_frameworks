// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Pair;
import android.view.*;
import android.widget.*;
import java.io.File;
import java.util.*;
import miui.app.resourcebrowser.*;
import miui.app.resourcebrowser.resource.Resource;
import miui.app.resourcebrowser.resource.ResourceSet;
import miui.app.resourcebrowser.util.ResourceDownloadHandler;
import miui.app.resourcebrowser.util.ResourceHelper;
import miui.widget.DataGroup;

public class BatchResourceHandler {
    public static final class ViewState extends Enum {

        public static ViewState valueOf(String s) {
            return (ViewState)Enum.valueOf(miui/app/resourcebrowser/view/BatchResourceHandler$ViewState, s);
        }

        public static ViewState[] values() {
            return (ViewState[])$VALUES.clone();
        }

        private static final ViewState $VALUES[];
        public static final ViewState DESTYOY;
        public static final ViewState INVISIABLE;
        public static final ViewState PAUSE;
        public static final ViewState UI_UPDATE;
        public static final ViewState VISIABLE;

        static  {
            VISIABLE = new ViewState("VISIABLE", 0);
            INVISIABLE = new ViewState("INVISIABLE", 1);
            UI_UPDATE = new ViewState("UI_UPDATE", 2);
            PAUSE = new ViewState("PAUSE", 3);
            DESTYOY = new ViewState("DESTYOY", 4);
            ViewState aviewstate[] = new ViewState[5];
            aviewstate[0] = VISIABLE;
            aviewstate[1] = INVISIABLE;
            aviewstate[2] = UI_UPDATE;
            aviewstate[3] = PAUSE;
            aviewstate[4] = DESTYOY;
            $VALUES = aviewstate;
        }

        private ViewState(String s, int i) {
            super(s, i);
        }
    }


    public BatchResourceHandler(ResourceListActivity resourcelistactivity, ResourceAdapter resourceadapter) {
        mEditableMode = false;
        mCheckedResource = new HashSet();
        mDownloadBytes = new HashMap();
        mListenDownloadProgress = false;
        if(resourcelistactivity == null || resourceadapter == null) {
            throw new IllegalArgumentException("BatchResourceOperationHandler() parameters can not be null!");
        } else {
            mFragment = null;
            mActivity = resourcelistactivity;
            mAdapter = resourceadapter;
            mDownloadHandler = getResourceDownloadHandler();
            mDownloadHandler.registerDownloadReceiver(true);
            return;
        }
    }

    public BatchResourceHandler(ResourceListFragment resourcelistfragment, ResourceAdapter resourceadapter) {
        mEditableMode = false;
        mCheckedResource = new HashSet();
        mDownloadBytes = new HashMap();
        mListenDownloadProgress = false;
        if(resourcelistfragment == null || resourceadapter == null)
            throw new IllegalArgumentException("BatchResourceOperationHandler() parameters can not be null!");
        mFragment = resourcelistfragment;
        mActivity = resourcelistfragment.getActivity();
        mAdapter = resourceadapter;
        mDownloadHandler = getResourceDownloadHandler();
        mDownloadHandler.registerDownloadReceiver(true);
        if(resourcelistfragment instanceof OnlineResourceListFragment)
            mLocalResourcePage = false;
        else
            mLocalResourcePage = true;
    }

    private void deleteOrDownloadResources() {
        (new AsyncTask() {

            protected volatile Object doInBackground(Object aobj[]) {
                return doInBackground((Void[])aobj);
            }

            protected transient Void doInBackground(Void avoid[]) {
                if(mLocalResourcePage) {
                    DataGroup datagroup = (DataGroup)mAdapter.getResourceSet().get(0);
                    ArrayList arraylist = new ArrayList(datagroup.size() - mCheckedResource.size());
                    String s = null;
                    for(Iterator iterator1 = datagroup.iterator(); iterator1.hasNext();) {
                        Resource resource1 = (Resource)iterator1.next();
                        if(mCheckedResource.contains(resource1)) {
                            if(resource1.getDividerTitle() != null)
                                s = resource1.getDividerTitle();
                            (new File(resource1.getLocalPath())).delete();
                        } else {
                            if(resource1.getDividerTitle() == null && s != null)
                                resource1.setDividerTitle(s);
                            s = null;
                            arraylist.add(resource1);
                        }
                    }

                    datagroup.clear();
                    datagroup.addAll(arraylist);
                } else {
                    Iterator iterator = mCheckedResource.iterator();
                    while(iterator.hasNext())  {
                        Resource resource = (Resource)iterator.next();
                        doDownloadResource(resource);
                    }
                }
                return null;
            }

            protected volatile void onPostExecute(Object obj) {
                onPostExecute((Void)obj);
            }

            protected void onPostExecute(Void void1) {
                quitEditMode();
                mProgress.dismiss();
            }

            protected void onPreExecute() {
                mProgress = new ProgressDialog(mAdapter.mContext);
                mProgress.setProgressStyle(0);
                mProgress.setMessage(mAdapter.mContext.getString(0x60c01b8));
                mProgress.setCancelable(false);
                mProgress.show();
            }

            private ProgressDialog mProgress;
            final BatchResourceHandler this$0;

             {
                this$0 = BatchResourceHandler.this;
                super();
            }
        }).execute(new Void[0]);
    }

    private void setCheckBoxState(CheckBox checkbox, boolean flag, boolean flag1) {
        if(checkbox != null) {
            int i;
            if(flag)
                i = 0;
            else
                i = 8;
            checkbox.setVisibility(i);
            checkbox.setChecked(flag1);
        }
    }

    private void setViewBatchSelectedState(View view) {
        byte byte0 = 0;
        Pair pair = (Pair)view.getTag();
        if(pair != null) {
            Resource resource = getResource(pair);
            boolean flag;
            boolean flag1;
            View view1;
            if(mEditableMode && mCheckedResource.contains(resource))
                flag = true;
            else
                flag = false;
            if(mEditableMode && canSelected(resource))
                flag1 = true;
            else
                flag1 = false;
            view.setSelected(flag);
            view1 = view.findViewById(0x60b004c);
            if(flag1)
                byte0 = 4;
            view1.setVisibility(byte0);
            setCheckBoxState((CheckBox)view.findViewById(0x1020001), flag1, flag);
            setDownloadProgress((ProgressBar)view.findViewById(0x60b004d), resource);
        }
    }

    protected boolean canDeletableResource(Resource resource) {
        String s = resource.getLocalPath();
        boolean flag;
        if(!TextUtils.isEmpty(s) && !ResourceHelper.isSystemResource(s) && (new File(s)).exists())
            flag = true;
        else
            flag = false;
        return flag;
    }

    protected boolean canDownloadResource(Resource resource) {
        boolean flag;
        if(resource.getStatus() != 0 && !mDownloadHandler.isResourceDownloading(resource.getLocalPath()))
            flag = true;
        else
            flag = false;
        return flag;
    }

    protected boolean canSelected(Resource resource) {
        boolean flag;
        if(mLocalResourcePage && canDeletableResource(resource) || !mLocalResourcePage && canDownloadResource(resource))
            flag = true;
        else
            flag = false;
        return flag;
    }

    protected void doCancelDownload(Resource resource) {
        mDownloadHandler.cancelDownload(resource.getLocalPath());
        mDownloadBytes.remove(resource.getLocalPath());
        mAdapter.notifyDataSetChanged();
    }

    protected void doDownloadResource(Resource resource) {
        if(mDownloadHandler.downloadResource(resource.getOnlinePath(), resource.getLocalPath(), resource.getTitle(), mListenDownloadProgress))
            mDownloadBytes.put(resource.getLocalPath(), Integer.valueOf(0));
    }

    protected void enterEditMode(View view, Pair pair) {
        mEditableMode = true;
        mCheckedResource.add(getResource(pair));
        setViewBatchSelectedState(view);
        mActionMode = mAdapter.mContext.startActionMode(mActionModeCb);
    }

    protected int getDownloadBytes(Resource resource) {
        Integer integer = (Integer)mDownloadBytes.get(resource.getLocalPath());
        int i;
        if(integer != null)
            i = integer.intValue();
        else
            i = -1;
        return i;
    }

    protected Resource getResource(Pair pair) {
        Resource resource;
        if(((Integer)pair.first).intValue() < mAdapter.getCount())
            resource = (Resource)mAdapter.getDataItem(((Integer)pair.first).intValue(), ((Integer)pair.second).intValue());
        else
            resource = null;
        return resource;
    }

    public android.view.View.OnClickListener getResourceClickListener() {
        return mItemClickListener;
    }

    protected ResourceDownloadHandler getResourceDownloadHandler() {
        return new ResourceDownloadHandler(mActivity) {

            public void handleDownloadFailed(String s) {
                Toast.makeText(mActivity, 0x60c0023, 0).show();
                mDownloadBytes.remove(s);
            }

            public void handleDownloadProgress(String s, int i, int j) {
                if(i != j) {
                    mDownloadBytes.put(s, Integer.valueOf(i));
                    if(System.currentTimeMillis() - mLastNotifyProgressTime > 600L) {
                        mLastNotifyProgressTime = System.currentTimeMillis();
                        mAdapter.notifyDataSetChanged();
                    }
                }
            }

            public void handleDownloadSuccessed(String s) {
                ResourceHelper.setResourceStatus(s, (DataGroup)mAdapter.getResourceSet().get(0), 0);
                mAdapter.notifyDataSetChanged();
                mAdapter.loadData();
                mDownloadBytes.remove(s);
            }

            private long mLastNotifyProgressTime;
            final BatchResourceHandler this$0;

             {
                this$0 = BatchResourceHandler.this;
                super(context);
                mLastNotifyProgressTime = 0L;
            }
        };
    }

    public android.view.View.OnLongClickListener getResourceLongClickListener() {
        return mItemLongClickListener;
    }

    public void initViewState(View view, Pair pair) {
        view.setTag(pair);
        view.setOnClickListener(mItemClickListener);
        view.setOnLongClickListener(mItemLongClickListener);
        setViewBatchSelectedState(view);
    }

    protected boolean isDownloading(Resource resource) {
        return mDownloadBytes.containsKey(resource.getLocalPath());
    }

    public boolean isEditMode() {
        return mEditableMode;
    }

    protected void onClick_Impl(View view) {
        boolean flag = true;
        Pair pair = (Pair)view.getTag();
        if(pair != null)
            if(mEditableMode) {
                Resource resource = getResource(pair);
                if(canSelected(resource)) {
                    if(view.isSelected())
                        flag = false;
                    view.setSelected(flag);
                    if(view.isSelected())
                        mCheckedResource.add(resource);
                    else
                        mCheckedResource.remove(resource);
                    if(mCheckedResource.isEmpty()) {
                        quitEditMode();
                    } else {
                        setViewBatchSelectedState(view);
                        updateSelectedTitle();
                    }
                } else {
                    Activity activity = mActivity;
                    Object aobj[] = new Object[flag];
                    String s;
                    String s1;
                    if(mLocalResourcePage)
                        s = mActivity.getString(0x60c01ba);
                    else
                        s = mActivity.getString(0x60c01bb);
                    aobj[0] = s;
                    s1 = activity.getString(0x60c01b9, aobj);
                    Toast.makeText(mAdapter.mContext, s1, 0).show();
                }
            } else
            if(mFragment != null)
                mFragment.startDetailActivityForResource(pair);
            else
                ((ResourceListActivity)mActivity).startDetailActivityForResource(pair);
    }

    protected boolean onLongClick_Impl(View view) {
        boolean flag;
        Pair pair;
        flag = false;
        pair = (Pair)view.getTag();
        if(pair != null) goto _L2; else goto _L1
_L1:
        return flag;
_L2:
        boolean flag1;
        if(mLocalResourcePage) {
            if(!mAdapter.loadingData())
                flag1 = true;
            else
                flag1 = false;
        } else {
            flag1 = true;
        }
        if(flag1 && !mEditableMode && canSelected(getResource(pair))) {
            enterEditMode(view, pair);
            flag = true;
        }
        if(true) goto _L1; else goto _L3
_L3:
    }

    public void quitEditMode() {
        if(mEditableMode) {
            mEditableMode = false;
            mActionMode.finish();
            mActionMode = null;
            mCheckedResource.clear();
            mAdapter.notifyDataSetChanged();
        }
    }

    protected void setDownloadProgress(ProgressBar progressbar, Resource resource) {
        int i = 0;
        if(progressbar != null) {
            Integer integer = (Integer)mDownloadBytes.get(resource.getLocalPath());
            if(integer != null) {
                progressbar.setVisibility(0);
                progressbar.setMax(100);
                if(resource.getFileSize() > 0L)
                    i = (int)((long)(100 * integer.intValue()) / resource.getFileSize());
                progressbar.setProgress(i);
            } else {
                progressbar.setVisibility(8);
            }
        }
    }

    protected void updateSelectedTitle() {
        Activity activity = mActivity;
        Object aobj[] = new Object[1];
        aobj[0] = Integer.valueOf(mCheckedResource.size());
        String s = activity.getString(0x60c01b6, aobj);
        mSelectTv.setText(s);
    }

    public void viewStateChanged(ViewState viewstate) {
        quitEditMode();
        if(viewstate == ViewState.DESTYOY && mDownloadHandler != null)
            mDownloadHandler.unregisterDownloadReceiver();
    }

    private ActionMode mActionMode;
    private android.view.ActionMode.Callback mActionModeCb = new android.view.ActionMode.Callback() {

        public boolean onActionItemClicked(ActionMode actionmode, MenuItem menuitem) {
            if(menuitem.getItemId() == 0x60c0176) {
                android.app.AlertDialog.Builder builder = (new android.app.AlertDialog.Builder(mActivity)).setIconAttribute(0x1010355);
                Activity activity = mActivity;
                Object aobj[] = new Object[1];
                aobj[0] = Integer.valueOf(mCheckedResource.size());
                builder.setMessage(activity.getString(0x60c01b7, aobj)).setNegativeButton(0x1040000, null).setPositiveButton(0x104000a, new android.content.DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialoginterface, int i) {
                        deleteOrDownloadResources();
                    }

                    final _cls4 this$1;

                     {
                        this$1 = _cls4.this;
                        super();
                    }
                }).show();
            }
            if(menuitem.getItemId() == 0x60c0017)
                deleteOrDownloadResources();
            return true;
        }

        public boolean onCreateActionMode(ActionMode actionmode, Menu menu) {
            int i = 0x60c0017;
            int j = 0x6020019;
            if(mLocalResourcePage) {
                i = 0x60c0176;
                j = 0x60201a9;
            }
            menu.add(0, i, 0, i).setIcon(j);
            View view = LayoutInflater.from(mActivity).inflate(0x603001e, null);
            actionmode.setCustomView(view);
            mSelectTv = (TextView)view.findViewById(0x60b0002);
            updateSelectedTitle();
            return true;
        }

        public void onDestroyActionMode(ActionMode actionmode) {
            quitEditMode();
        }

        public boolean onPrepareActionMode(ActionMode actionmode, Menu menu) {
            return false;
        }

        final BatchResourceHandler this$0;

             {
                this$0 = BatchResourceHandler.this;
                super();
            }
    };
    protected Activity mActivity;
    protected ResourceAdapter mAdapter;
    private HashSet mCheckedResource;
    protected HashMap mDownloadBytes;
    private ResourceDownloadHandler mDownloadHandler;
    private boolean mEditableMode;
    protected ResourceListFragment mFragment;
    private android.view.View.OnClickListener mItemClickListener = new android.view.View.OnClickListener() {

        public void onClick(View view) {
            onClick_Impl(view);
        }

        final BatchResourceHandler this$0;

             {
                this$0 = BatchResourceHandler.this;
                super();
            }
    };
    private android.view.View.OnLongClickListener mItemLongClickListener = new android.view.View.OnLongClickListener() {

        public boolean onLongClick(View view) {
            return onLongClick_Impl(view);
        }

        final BatchResourceHandler this$0;

             {
                this$0 = BatchResourceHandler.this;
                super();
            }
    };
    protected boolean mListenDownloadProgress;
    protected boolean mLocalResourcePage;
    private TextView mSelectTv;


/*
    static TextView access$002(BatchResourceHandler batchresourcehandler, TextView textview) {
        batchresourcehandler.mSelectTv = textview;
        return textview;
    }

*/


}
