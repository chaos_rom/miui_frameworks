// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import java.io.File;
import java.util.*;
import miui.app.resourcebrowser.resource.Resource;
import miui.app.resourcebrowser.resource.ResourceSet;
import miui.app.resourcebrowser.service.ResourceDataParser;
import miui.app.resourcebrowser.service.ResourceJSONDataParser;
import miui.app.resourcebrowser.service.local.AudioResourceFolder;
import miui.app.resourcebrowser.service.local.ImageResourceFolder;
import miui.app.resourcebrowser.service.local.ZipResourceFolder;
import miui.app.resourcebrowser.service.online.OnlineService;
import miui.app.resourcebrowser.util.DownloadFileTask;
import miui.app.resourcebrowser.util.ResourceDebug;
import miui.app.resourcebrowser.util.ResourceHelper;
import miui.os.AsyncTaskObserver;
import miui.os.ExtraFileUtils;
import miui.widget.DataGroup;

// Referenced classes of package miui.app.resourcebrowser:
//            ResourceAdapter, ResourceConstants, BaseFragment

public class LocalResourceAdapter extends ResourceAdapter {
    public class DownloadVersionTask extends DownloadFileTask {

        protected volatile void onPostExecute(Object obj) {
            onPostExecute((List)obj);
        }

        protected void onPostExecute(List list) {
            if(list.size() > 0) {
                String s = ((miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry)list.get(0)).getPath();
                if(s != null)
                    checkVersionStatus(s);
            }
        }

        final LocalResourceAdapter this$0;

        public DownloadVersionTask() {
            this$0 = LocalResourceAdapter.this;
            super();
        }
    }

    public class AsyncLoadResourceTask extends miui.widget.AsyncAdapter.AsyncLoadDataTask {

        protected volatile Object[] loadData(int i) {
            return loadData(i);
        }

        protected Resource[] loadData(int i) {
            return null;
        }

        final LocalResourceAdapter this$0;

        public AsyncLoadResourceTask() {
            this$0 = LocalResourceAdapter.this;
            super(LocalResourceAdapter.this);
        }
    }


    public LocalResourceAdapter(Context context, Bundle bundle) {
        super(context, bundle);
        mVisitors = null;
        mService = OnlineService.getInstance();
        mParser = ResourceJSONDataParser.getInstance();
        initParams();
    }

    public LocalResourceAdapter(BaseFragment basefragment, Bundle bundle) {
        super(basefragment, bundle);
        mVisitors = null;
        mService = OnlineService.getInstance();
        mParser = ResourceJSONDataParser.getInstance();
        initParams();
    }

    private void checkVersionStatus(String s) {
        List list = mParser.readUpdatableResources(s, super.mMetaData);
        if(list != null && !list.isEmpty()) {
            int i = 0;
            while(i < list.size())  {
                Resource resource = (Resource)list.get(i);
                Resource resource1 = (Resource)mFileHashMap.get(resource.getFileHash());
                if(resource1 != null) {
                    String s1 = resource.getId();
                    File file = new File(resource1.getLocalPath());
                    String s2 = file.getName();
                    String s3 = s2.substring(s2.lastIndexOf('.'));
                    if(!s1.equalsIgnoreCase(s2.substring(0, s2.length() - s3.length())) && (new File(file.getParentFile(), (new StringBuilder()).append(s1).append(s3).toString())).exists()) {
                        deleteUnusedResource(resource1);
                    } else {
                        resource1.updateOnlinePath(OnlineService.getInstance().getDownloadUrl(s1));
                        resource1.updateStatus(1);
                    }
                }
                i++;
            }
            notifyDataSetChanged();
        }
    }

    private boolean deleteUnusedResource(Resource resource) {
        ResourceSet resourceset;
        int i;
        resourceset = getResourceSet();
        i = 0;
_L3:
        if(i >= resourceset.size())
            break MISSING_BLOCK_LABEL_57;
        if(!((DataGroup)resourceset.get(i)).remove(resource)) goto _L2; else goto _L1
_L1:
        boolean flag;
        (new File(resource.getLocalPath())).delete();
        flag = true;
_L4:
        return flag;
_L2:
        i++;
          goto _L3
        flag = false;
          goto _L4
    }

    private void initParams() {
        mVersionFolder = ExtraFileUtils.standardizeFolderPath((new StringBuilder()).append(ResourceConstants.VERSION_PATH).append(super.mResourceSetCode).toString());
    }

    protected boolean checkResourceModifyTime() {
        return false;
    }

    protected List generateVisitors(String as[]) {
        ArrayList arraylist = new ArrayList();
        for(int i = 0; i < as.length; i++)
            arraylist.add(getVisitor(as[i]));

        return arraylist;
    }

    protected int getDownloadableFlag(Resource resource) {
        int i = super.getDownloadableFlag(resource);
        if(i == 0x602003a)
            i = 0;
        return i;
    }

    protected List getLoadDataTask() {
        ArrayList arraylist = new ArrayList();
        AsyncLoadResourceTask asyncloadresourcetask = new AsyncLoadResourceTask();
        asyncloadresourcetask.addObserver((AsyncTaskObserver)getRegisterAsyncTaskObserver());
        List list = getVisitors();
        for(int i = 0; i < list.size(); i++)
            asyncloadresourcetask.addVisitor((miui.widget.AsyncAdapter.AsyncLoadDataVisitor)list.get(i));

        arraylist.add(asyncloadresourcetask);
        return arraylist;
    }

    protected miui.widget.AsyncAdapter.AsyncLoadDataVisitor getVisitor(String s) {
        Object obj;
        if(super.mResourceSetCategory == 1)
            obj = new ImageResourceFolder(super.mContext, super.mMetaData, s);
        else
        if(super.mResourceSetCategory == 2)
            obj = new AudioResourceFolder(super.mContext, super.mMetaData, s);
        else
            obj = new ZipResourceFolder(super.mContext, super.mMetaData, s);
        return ((miui.widget.AsyncAdapter.AsyncLoadDataVisitor) (obj));
    }

    protected List getVisitors() {
        List list;
        if(mVisitors != null) {
            list = mVisitors;
        } else {
            mVisitors = generateVisitors(super.mMetaData.getStringArray("miui.app.resourcebrowser.SOURCE_FOLDERS"));
            list = mVisitors;
        }
        return list;
    }

    protected void postLoadData(List list) {
        if(!super.mMetaData.getBoolean("miui.app.resourcebrowser.VERSION_SUPPORTED")) goto _L2; else goto _L1
_L1:
        ArrayList arraylist;
        arraylist = new ArrayList();
        mFileHashMap = new HashMap();
        int i = 0;
        while(i < list.size())  {
            Resource resource = (Resource)list.get(i);
            String s2 = resource.getFileHash();
            if(s2 != null) {
                Resource resource1 = (Resource)mFileHashMap.get(s2);
                if(resource1 != null) {
                    Log.i("Theme", (new StringBuilder()).append("delete repeat resources: ").append(resource.getTitle()).append(" vs ").append(resource1.getTitle()).append(" ").append(resource.getLocalPath()).append(" vs ").append(resource1.getLocalPath()).toString());
                    arraylist.add(resource);
                } else {
                    mFileHashMap.put(s2, resource);
                }
            }
            i++;
        }
        if(!mFileHashMap.isEmpty()) goto _L3; else goto _L2
_L2:
        return;
_L3:
        if(!arraylist.isEmpty()) {
            for(Iterator iterator = arraylist.iterator(); iterator.hasNext(); deleteUnusedResource((Resource)iterator.next()));
            notifyDataSetChanged();
        }
        String s = mService.getUpdateUrl((String[])mFileHashMap.keySet().toArray(new String[0]));
        String s1 = (new StringBuilder()).append(mVersionFolder).append("version").toString();
        File file = new File(s1);
        if(file.exists()) {
            checkVersionStatus(s1);
            if(ResourceHelper.isCacheValid(file))
                continue; /* Loop/switch isn't completed */
        }
        if(ResourceDebug.DEBUG)
            Log.d("Theme", (new StringBuilder()).append("Local list page check update url: ").append(s).toString());
        DownloadVersionTask downloadversiontask = new DownloadVersionTask();
        downloadversiontask.setId("version");
        miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry downloadfileentry = new miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry();
        downloadfileentry.setUrl(s);
        downloadfileentry.setPath(s1);
        miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry adownloadfileentry[] = new miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry[1];
        adownloadfileentry[0] = downloadfileentry;
        downloadversiontask.execute(adownloadfileentry);
        if(true) goto _L2; else goto _L4
_L4:
    }

    public void setKeyword(String s) {
        mKeyword = s;
    }

    protected Map mFileHashMap;
    protected String mKeyword;
    private ResourceDataParser mParser;
    private OnlineService mService;
    private String mVersionFolder;
    protected List mVisitors;

}
