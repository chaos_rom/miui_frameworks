// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser.recommended;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import java.util.HashMap;
import java.util.Map;

// Referenced classes of package miui.app.resourcebrowser.recommended:
//            RecommendItemData, RecommendGridBaseItem

public class RecommendGridItemFactory
    implements UnevenGrid.GridItemFactory {

    public RecommendGridItemFactory(Context context, Bundle bundle) {
        mContext = context;
        mMetaData = bundle;
    }

    public View createGridItem(UnevenGrid.GridItemData griditemdata) {
        Object obj = null;
        if(griditemdata != null) goto _L2; else goto _L1
_L1:
        return ((View) (obj));
_L2:
        RecommendItemData recommenditemdata = (RecommendItemData)griditemdata;
        int i = recommenditemdata.itemType;
        if(i != 0) {
            obj = (RecommendGridBaseItem)LayoutInflater.from(mContext).inflate(((Integer)sGridTypeLayoutMap.get(Integer.valueOf(i))).intValue(), null, false);
            ((RecommendGridBaseItem) (obj)).bind(recommenditemdata, mMetaData);
        }
        if(true) goto _L1; else goto _L3
_L3:
    }

    private static Map sGridTypeLayoutMap;
    private Context mContext;
    private Bundle mMetaData;

    static  {
        sGridTypeLayoutMap = new HashMap();
        sGridTypeLayoutMap.put(Integer.valueOf(2), Integer.valueOf(0x6030036));
        sGridTypeLayoutMap.put(Integer.valueOf(1), Integer.valueOf(0x6030035));
    }
}
