// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser.recommended;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.View;
import java.io.Serializable;

// Referenced classes of package miui.app.resourcebrowser.recommended:
//            RecommendGridBaseItem, RecommendItemData

public class RecommendGridListItem extends RecommendGridBaseItem {

    public RecommendGridListItem(Context context, AttributeSet attributeset) {
        super(context, attributeset);
    }

    protected android.view.View.OnClickListener getOnClickListener(final Bundle metaData) {
        return new android.view.View.OnClickListener() {

            public void onClick(View view) {
                Intent intent = new Intent();
                Pair pair = getRecommendedResourceListActivity(metaData);
                intent.setClassName((String)pair.first, (String)pair.second);
                intent.addFlags(0x4000000);
                metaData.putBoolean("miui.app.resourcebrowser.IS_RECOMMENDATION_LIST", true);
                metaData.putString("miui.app.resourcebrowser.RECOMMENDATION_ID", mRecommendItemData.itemId);
                metaData.putSerializable("miui.app.resourcebrowser.SUB_RECOMMENDATIONS", (Serializable)mRecommendItemData.subItems);
                intent.putExtra("META_DATA_FOR_ONLINE", metaData);
                ((Activity)
// JavaClassFileOutputException: get_constant: invalid tag

            final RecommendGridListItem this$0;
            final Bundle val$metaData;

             {
                this$0 = RecommendGridListItem.this;
                metaData = bundle;
                super();
            }
        };
    }

    protected Pair getRecommendedResourceListActivity(Bundle bundle) {
        return new Pair(bundle.getString("miui.app.resourcebrowser.RECOMMENDATION_LIST_ACTIVITY_PACKAGE"), bundle.getString("miui.app.resourcebrowser.RECOMMENDATION_LIST_ACTIVITY_CLASS"));
    }

}
