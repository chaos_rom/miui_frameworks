// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser.recommended;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.*;
import java.io.File;
import miui.app.resourcebrowser.IntentConstants;
import miui.app.resourcebrowser.util.DownloadFileTask;
import miui.util.ImageUtils;
import miui.util.InputStreamLoader;

// Referenced classes of package miui.app.resourcebrowser.recommended:
//            RecommendItemData

public abstract class RecommendGridBaseItem extends FrameLayout
    implements IntentConstants {
    public class DownloadThumbnailTask extends DownloadFileTask {

        protected volatile void onProgressUpdate(Object aobj[]) {
            onProgressUpdate((miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry[])aobj);
        }

        protected transient void onProgressUpdate(miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry adownloadfileentry[]) {
            miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry downloadfileentry;
            if(adownloadfileentry == null || adownloadfileentry.length <= 0)
                break MISSING_BLOCK_LABEL_38;
            downloadfileentry = adownloadfileentry[0];
            if(downloadfileentry == null)
                break MISSING_BLOCK_LABEL_47;
            android.graphics.Bitmap bitmap = BitmapFactory.decodeFile(downloadfileentry.getPath());
            mImageView.setImageBitmap(bitmap);
_L1:
            return;
            Exception exception;
            exception;
            exception.printStackTrace();
              goto _L1
            Toast.makeText(RecommendGridBaseItem.this.DownloadFileTask$DownloadFileEntry, 0x60c0024, 0).show();
              goto _L1
        }

        final RecommendGridBaseItem this$0;

        public DownloadThumbnailTask() {
            this$0 = RecommendGridBaseItem.this;
            super();
        }
    }


    public RecommendGridBaseItem(Context context, AttributeSet attributeset) {
        super(context, attributeset);
    }

    public void bind(RecommendItemData recommenditemdata, Bundle bundle) {
        android.graphics.Bitmap bitmap;
        mRecommendItemData = recommenditemdata;
        mImageView.setOnClickListener(getOnClickListener(bundle));
        bitmap = null;
        if(!(new File(recommenditemdata.localThumbnail)).exists()) goto _L2; else goto _L1
_L1:
        bitmap = ImageUtils.getBitmap(new InputStreamLoader(recommenditemdata.localThumbnail), -1);
_L4:
        if(bitmap != null)
            mImageView.setImageBitmap(bitmap);
        return;
_L2:
        if(!TextUtils.isEmpty(recommenditemdata.onlineThumbnail)) {
            DownloadThumbnailTask downloadthumbnailtask = new DownloadThumbnailTask();
            miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry downloadfileentry = new miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry();
            downloadfileentry.setPath(recommenditemdata.localThumbnail);
            downloadfileentry.setUrl(recommenditemdata.onlineThumbnail);
            miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry adownloadfileentry[] = new miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry[1];
            adownloadfileentry[0] = downloadfileentry;
            downloadthumbnailtask.execute(adownloadfileentry);
        }
        if(true) goto _L4; else goto _L3
_L3:
    }

    public RecommendItemData getItemData() {
        return mRecommendItemData;
    }

    protected abstract android.view.View.OnClickListener getOnClickListener(Bundle bundle);

    protected void onFinishInflate() {
        mImageView = (ImageView)findViewById(0x60b006a);
    }

    private ImageView mImageView;
    protected RecommendItemData mRecommendItemData;


}
