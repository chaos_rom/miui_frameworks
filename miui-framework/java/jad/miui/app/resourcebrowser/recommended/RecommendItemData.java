// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser.recommended;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RecommendItemData extends UnevenGrid.GridItemData
    implements Serializable {

    public RecommendItemData() {
        subItems = new ArrayList();
    }

    public String toString() {
        return title;
    }

    public static final int TYPE_APP = 1;
    public static final int TYPE_LIST = 2;
    public static final int TYPE_UNKNOWN = 0;
    private static final long serialVersionUID = 1L;
    public String itemId;
    public int itemType;
    public String localThumbnail;
    public String onlineThumbnail;
    public List subItems;
    public String title;
    public String type;
}
