// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser.recommended;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.View;
import java.io.File;
import miui.app.resourcebrowser.resource.Resource;
import miui.app.resourcebrowser.resource.ResourceSet;
import miui.app.resourcebrowser.service.online.OnlineService;
import miui.os.ExtraFileUtils;
import miui.widget.DataGroup;

// Referenced classes of package miui.app.resourcebrowser.recommended:
//            RecommendGridBaseItem, RecommendItemData

public class RecommendGridSingleItem extends RecommendGridBaseItem {

    public RecommendGridSingleItem(Context context, AttributeSet attributeset) {
        super(context, attributeset);
    }

    protected void buildResourceSet(Bundle bundle) {
        Resource resource;
        Bundle bundle1;
        String s;
        String s1;
        String s2;
        String s3;
        String s4;
        String s5;
        resource = new Resource();
        bundle1 = new Bundle();
        s = super.mRecommendItemData.itemId;
        bundle1.putString("ID", s);
        s1 = super.mRecommendItemData.title;
        bundle1.putString("NAME", s1);
        bundle1.putString("ONLINE_PATH", OnlineService.getInstance().getDownloadUrl(super.mRecommendItemData.itemId));
        s2 = bundle.getString("miui.app.resourcebrowser.RESOURCE_FILE_EXTENSION");
        s3 = ExtraFileUtils.standardizeFolderPath(bundle.getString("miui.app.resourcebrowser.DOWNLOAD_FOLDER"));
        s4 = ExtraFileUtils.standardizeFolderPath(bundle.getString("miui.app.resourcebrowser.DOWNLOAD_FOLDER_EXTRA"));
        s5 = (new StringBuilder()).append(s3).append(s).append(s2).toString();
        if(!s2.equalsIgnoreCase(".mp3") && !s2.equalsIgnoreCase(".jpg") && !s2.equalsIgnoreCase(".ogg")) goto _L2; else goto _L1
_L1:
        if(!TextUtils.isEmpty(s1))
            s5 = (new StringBuilder()).append(s3).append(s1).append(s2).toString();
_L4:
        bundle1.putString("LOCAL_PATH", s5);
        resource.setInformation(bundle1);
        bundle.putString("miui.app.resourcebrowser.RESOURCE_SET_SUBPACKAGE", ".single");
        bundle.putInt("miui.app.resourcebrowser.RESOURCE_GROUP", 0);
        bundle.putInt("miui.app.resourcebrowser.RESOURCE_INDEX", 0);
        ResourceSet resourceset = ResourceSet.getInstance((new StringBuilder()).append(bundle.getString("miui.app.resourcebrowser.RESOURCE_SET_PACKAGE")).append(".single").toString());
        resourceset.clear();
        DataGroup datagroup = new DataGroup();
        datagroup.add(resource);
        resourceset.add(datagroup);
        return;
_L2:
        if(!TextUtils.isEmpty(s4) && (new File((new StringBuilder()).append(s4).append(s).append(s2).toString())).exists())
            s5 = (new StringBuilder()).append(s4).append(s).append(s2).toString();
        if(true) goto _L4; else goto _L3
_L3:
    }

    protected android.view.View.OnClickListener getOnClickListener(final Bundle metaData) {
        return new android.view.View.OnClickListener() {

            public void onClick(View view) {
                Intent intent = new Intent();
                Pair pair = getResourceDetailActivity(metaData);
                intent.setClassName((String)pair.first, (String)pair.second);
                intent.addFlags(0x4000000);
                buildResourceSet(metaData);
                metaData.putString("miui.app.resourcebrowser.RECOMMENDATION_ID", mRecommendItemData.itemId);
                intent.putExtra("META_DATA", metaData);
                ((Activity)
// JavaClassFileOutputException: get_constant: invalid tag

            final RecommendGridSingleItem this$0;
            final Bundle val$metaData;

             {
                this$0 = RecommendGridSingleItem.this;
                metaData = bundle;
                super();
            }
        };
    }

    protected Pair getResourceDetailActivity(Bundle bundle) {
        return new Pair(bundle.getString("miui.app.resourcebrowser.DETAIL_ACTIVITY_PACKAGE"), bundle.getString("miui.app.resourcebrowser.DETAIL_ACTIVITY_CLASS"));
    }

}
