// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser.recommended;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import java.util.*;

public class UnevenGrid extends ViewGroup {
    public static interface GridItemFactory {

        public abstract View createGridItem(GridItemData griditemdata);
    }

    public static class GridItemData {

        public int heightCount;
        public int widthCount;

        public GridItemData() {
        }
    }

    public static class LayoutParams extends android.view.ViewGroup.MarginLayoutParams {

        public static LayoutParams create(int i, int j) {
            LayoutParams layoutparams = new LayoutParams();
            layoutparams.widthCount = i;
            layoutparams.heightCount = j;
            return layoutparams;
        }

        int heightCount;
        int widthCount;

        public LayoutParams() {
            super(-1, -1);
        }

        public LayoutParams(Context context, AttributeSet attributeset) {
            super(context, attributeset);
        }
    }


    public UnevenGrid(Context context) {
        super(context);
        mGridItems = new ArrayList();
        mGridItemPositions = new HashMap();
        mEmptyGrids = new TreeSet();
        mEmptyRow = 0;
        initialize();
    }

    private void addGridItem(View view, int i, int j) {
        if(view != null) goto _L2; else goto _L1
_L1:
        return;
_L2:
        if(i > mGridMaxWidth)
            i = mGridMaxWidth;
        else
        if(i < 1)
            i = 1;
        if(j <= mGridMaxHeight)
            break; /* Loop/switch isn't completed */
        j = mGridMaxHeight;
_L5:
        view.setLayoutParams(LayoutParams.create(i, j));
        mGridItems.add(view);
        if(true) goto _L1; else goto _L3
_L3:
        if(j >= 1) goto _L5; else goto _L4
_L4:
        j = 1;
          goto _L5
    }

    private int computeAndPlace(int i, int j) {
        Iterator iterator = mEmptyGrids.iterator();
_L4:
        if(!iterator.hasNext()) goto _L2; else goto _L1
_L1:
        Integer integer = (Integer)iterator.next();
        if(!isPlaceable(integer.intValue(), i, j)) goto _L4; else goto _L3
_L3:
        int k;
        place(integer.intValue(), i, j);
        k = integer.intValue();
_L6:
        return k;
_L2:
        k = mEmptyRow * mColumnCount;
        place(k, i, j);
        if(true) goto _L6; else goto _L5
_L5:
    }

    private int getChildBottom(int i, int j) {
        return j + getChildTop(i);
    }

    private int getChildLeft(int i) {
        return (i % mColumnCount) * (mGridWidth + mGridItemGap);
    }

    private int getChildRight(int i, int j) {
        return j + getChildLeft(i);
    }

    private int getChildTop(int i) {
        return (i / mColumnCount) * (mGridHeight + mGridItemGap);
    }

    private void initialize() {
        mColumnCount = 2;
        mGridItemGap = 0;
        mGridMaxWidth = 2;
        mGridMaxHeight = 2;
        mWidthRatio = 5;
        mHeightRatio = 3;
    }

    private boolean isPlaceable(int i, int j, int k) {
        boolean flag = false;
        if(mColumnCount - i % mColumnCount >= j) goto _L2; else goto _L1
_L1:
        return flag;
_L2:
        for(int l = 0; l < k; l++) {
            for(int i1 = 0; i1 < j; i1++) {
                int j1 = i1 + (i + l * mColumnCount);
                if(j1 / mColumnCount >= mEmptyRow) {
                    flag = true;
                    continue; /* Loop/switch isn't completed */
                }
                if(!mEmptyGrids.contains(Integer.valueOf(j1)))
                    continue; /* Loop/switch isn't completed */
            }

        }

        flag = true;
        if(true) goto _L1; else goto _L3
_L3:
    }

    private void layoutItems() {
        removeAllViews();
        mGridItemPositions.clear();
        mEmptyGrids.clear();
        mEmptyRow = 0;
        View view;
        for(Iterator iterator = mGridItems.iterator(); iterator.hasNext(); addView(view)) {
            view = (View)iterator.next();
            LayoutParams layoutparams = (LayoutParams)view.getLayoutParams();
            int i = computeAndPlace(layoutparams.widthCount, layoutparams.heightCount);
            mGridItemPositions.put(view, Integer.valueOf(i));
        }

    }

    private void place(int i, int j, int k) {
        int l = -1 + (k + i / mColumnCount);
        for(int i1 = mEmptyRow; i1 <= l; i1++) {
            for(int i2 = 0; i2 < mColumnCount; i2++) {
                int j2 = i2 + i1 * mColumnCount;
                mEmptyGrids.add(Integer.valueOf(j2));
            }

        }

        if(mEmptyRow <= l)
            mEmptyRow = l + 1;
        for(int j1 = 0; j1 < k; j1++) {
            for(int k1 = 0; k1 < j; k1++) {
                int l1 = k1 + (i + j1 * mColumnCount);
                mEmptyGrids.remove(Integer.valueOf(l1));
            }

        }

    }

    protected void onLayout(boolean flag, int i, int j, int k, int l) {
        for(int i1 = 0; i1 < getChildCount(); i1++) {
            View view = getChildAt(i1);
            int j1 = view.getMeasuredWidth();
            int k1 = view.getMeasuredHeight();
            int l1 = ((Integer)mGridItemPositions.get(view)).intValue();
            view.layout(mPaddingLeft + getChildLeft(l1), mPaddingTop + getChildTop(l1), mPaddingLeft + getChildRight(l1, j1), mPaddingTop + getChildBottom(l1, k1));
        }

    }

    protected void onMeasure(int i, int j) {
        int k = android.view.View.MeasureSpec.getSize(i);
        mGridWidth = (k - mPaddingLeft - mPaddingRight - (-1 + mColumnCount) * mGridItemGap) / mColumnCount;
        mGridHeight = (mGridWidth * mHeightRatio) / mWidthRatio;
        int l = 0;
        for(int i1 = 0; i1 < getChildCount(); i1++) {
            View view = getChildAt(i1);
            LayoutParams layoutparams = (LayoutParams)view.getLayoutParams();
            int j1 = layoutparams.widthCount;
            int k1 = layoutparams.heightCount;
            int l1 = j1 * mGridWidth + (j1 - 1) * mGridItemGap;
            int i2 = k1 * mGridHeight + (k1 - 1) * mGridItemGap;
            int j2 = ((Integer)mGridItemPositions.get(view)).intValue();
            view.measure(android.view.View.MeasureSpec.makeMeasureSpec(l1, 0x40000000), android.view.View.MeasureSpec.makeMeasureSpec(i2, 0x40000000));
            l = Math.max(l, getChildBottom(j2, i2));
        }

        setMeasuredDimension(k, l + mPaddingTop + mPaddingBottom);
    }

    public void relayout() {
        layoutItems();
    }

    public void setColumnCount(int i) {
        if(i > 0)
            mColumnCount = i;
    }

    public void setGridItemFactory(GridItemFactory griditemfactory) {
        mGridItemFactory = griditemfactory;
    }

    public void setGridItemGap(int i) {
        if(i >= 0)
            mGridItemGap = i;
    }

    public void setGridItemMaxSize(int i, int j) {
        if(i > 0 && j > 0) {
            mGridMaxWidth = i;
            mGridMaxHeight = j;
        }
    }

    public void setGridItemRatio(int i, int j) {
        if(i > 0 && j > 0) {
            mWidthRatio = i;
            mHeightRatio = j;
        }
    }

    public void updateData(List list) {
        if(list != null && !list.isEmpty() && mGridItemFactory != null) {
            mGridItems.clear();
            Iterator iterator = list.iterator();
            do {
                if(!iterator.hasNext())
                    break;
                GridItemData griditemdata = (GridItemData)iterator.next();
                View view = mGridItemFactory.createGridItem(griditemdata);
                if(view != null)
                    addGridItem(view, griditemdata.widthCount, griditemdata.heightCount);
            } while(true);
            layoutItems();
        }
    }

    private int mColumnCount;
    private TreeSet mEmptyGrids;
    private int mEmptyRow;
    private int mGridHeight;
    private GridItemFactory mGridItemFactory;
    private int mGridItemGap;
    private HashMap mGridItemPositions;
    private ArrayList mGridItems;
    private int mGridMaxHeight;
    private int mGridMaxWidth;
    private int mGridWidth;
    private int mHeightRatio;
    private int mWidthRatio;
}
