// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser.resource;


public class ListMetaData {

    public ListMetaData() {
    }

    public String getCategory() {
        return mCategory;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getName() {
        return mName;
    }

    public void setCategory(String s) {
        mCategory = s;
    }

    public void setDescription(String s) {
        mDescription = s;
    }

    public void setName(String s) {
        mName = s;
    }

    private String mCategory;
    private String mDescription;
    private String mName;
}
