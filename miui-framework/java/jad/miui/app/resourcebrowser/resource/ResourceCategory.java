// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser.resource;


public class ResourceCategory
    implements Comparable {

    public ResourceCategory() {
    }

    public volatile int compareTo(Object obj) {
        return compareTo((ResourceCategory)obj);
    }

    public int compareTo(ResourceCategory resourcecategory) {
        return mCode.compareTo(resourcecategory.getCode());
    }

    public String getCode() {
        return mCode;
    }

    public String getName() {
        return mName;
    }

    public long getType() {
        return mType;
    }

    public void setCode(String s) {
        mCode = s;
    }

    public void setName(String s) {
        mName = s;
    }

    public void setType(long l) {
        mType = l;
    }

    public String toString() {
        return mName;
    }

    private String mCode;
    private String mName;
    private long mType;
}
