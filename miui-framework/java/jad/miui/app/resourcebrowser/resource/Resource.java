// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser.resource;

import android.os.Bundle;
import java.util.ArrayList;
import java.util.List;

public class Resource {

    public Resource() {
        mLocalThumbnails = new ArrayList();
        mLocalPreviews = new ArrayList();
        mOnlineThumbnails = new ArrayList();
        mOnlinePreviews = new ArrayList();
    }

    private int getIntValue(Bundle bundle, String s) {
        String s1 = bundle.getString(s);
        if(s1 == null) goto _L2; else goto _L1
_L1:
        int j = Integer.parseInt(s1);
        int i = j;
_L4:
        return i;
        Exception exception;
        exception;
_L2:
        i = 0;
        if(true) goto _L4; else goto _L3
_L3:
    }

    public boolean equals(Object obj) {
        boolean flag;
        if(!(obj instanceof Resource)) {
            flag = false;
        } else {
            Resource resource = (Resource)obj;
            flag = mLocalPath.equals(resource.mLocalPath);
        }
        return flag;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getDividerTitle() {
        return mDividerTitle;
    }

    public String getFileHash() {
        return mFileHash;
    }

    public long getFileModifiedTime() {
        return mFileModifiedTime;
    }

    public long getFileSize() {
        return mFileSize;
    }

    public String getId() {
        return mId;
    }

    public Bundle getInformation() {
        return mInformation;
    }

    public String getLocalPath() {
        return mLocalPath;
    }

    public String getLocalPreview(int i) {
        String s;
        if(i >= mLocalPreviews.size())
            s = null;
        else
            s = (String)mLocalPreviews.get(i);
        return s;
    }

    public List getLocalPreviews() {
        return mLocalPreviews;
    }

    public String getLocalThumbnail(int i) {
        String s;
        if(i >= mLocalThumbnails.size())
            s = null;
        else
            s = (String)mLocalThumbnails.get(i);
        return s;
    }

    public List getLocalThumbnails() {
        return mLocalThumbnails;
    }

    public String getOnlinePath() {
        return mOnlinePath;
    }

    public String getOnlinePreview(int i) {
        String s;
        if(i >= mOnlinePreviews.size())
            s = null;
        else
            s = (String)mOnlinePreviews.get(i);
        return s;
    }

    public List getOnlinePreviews() {
        return mOnlinePreviews;
    }

    public String getOnlineThumbnail(int i) {
        String s;
        if(i >= mOnlineThumbnails.size())
            s = null;
        else
            s = (String)mOnlineThumbnails.get(i);
        return s;
    }

    public List getOnlineThumbnails() {
        return mOnlineThumbnails;
    }

    public int getPlatformVersion() {
        return mPlatformVersion;
    }

    public int getStatus() {
        return mStatus;
    }

    public String getTitle() {
        return mTitle;
    }

    public int hashCode() {
        return mLocalPath.hashCode();
    }

    public void setDescription(String s) {
        mDescription = s;
    }

    public void setDividerTitle(String s) {
        mDividerTitle = s;
    }

    public void setFileHash(String s) {
        mFileHash = s;
    }

    public void setFileModifiedTime(long l) {
        mFileModifiedTime = l;
    }

    public void setFileSize(long l) {
        mFileSize = l;
    }

    public void setInformation(Bundle bundle) {
        mInformation = bundle;
        mId = bundle.getString("ID");
        mTitle = bundle.getString("NAME");
        mDescription = bundle.getString("DESCRIPTION");
        mLocalPath = bundle.getString("LOCAL_PATH");
        mOnlinePath = bundle.getString("ONLINE_PATH");
        mFileSize = getIntValue(bundle, "SIZE");
        ArrayList arraylist = bundle.getStringArrayList("LOCAL_THUMBNAIL");
        if(arraylist != null)
            mLocalThumbnails = arraylist;
        ArrayList arraylist1 = bundle.getStringArrayList("LOCAL_PREVIEW");
        if(arraylist1 != null)
            mLocalPreviews = arraylist1;
        ArrayList arraylist2 = bundle.getStringArrayList("ONLINE_THUMBNAIL");
        if(arraylist2 != null)
            mOnlineThumbnails = arraylist2;
        ArrayList arraylist3 = bundle.getStringArrayList("ONLINE_PREVIEW");
        if(arraylist3 != null)
            mOnlinePreviews = arraylist3;
        mStatus = bundle.getInt("STATUS");
        mPlatformVersion = bundle.getInt("PLATFORM_VERSION");
    }

    public void updateOnlinePath(String s) {
        mInformation.putString("ONLINE_PATH", s);
        mOnlinePath = s;
    }

    public void updateStatus(int i) {
        mInformation.putInt("STATUS", i);
        mStatus = i;
    }

    public static final String AUTHOR = "AUTHOR";
    public static final String CREATED_TIME = "CREATED_TIME";
    public static final String DESCRIPTION = "DESCRIPTION";
    public static final String DESIGNER = "DESIGNER";
    public static final String DOWNLOAD_COUNT = "DOWNLOAD_COUNT";
    public static final String ID = "ID";
    public static final String LOCAL_PATH = "LOCAL_PATH";
    public static final String LOCAL_PREVIEWS = "LOCAL_PREVIEW";
    public static final String LOCAL_THUMBNAILS = "LOCAL_THUMBNAIL";
    public static final String MODIFIED_TIME = "MODIFIED_TIME";
    public static final String NAME = "NAME";
    public static final String NVP = "NVP";
    public static final String ONLINE_PATH = "ONLINE_PATH";
    public static final String ONLINE_PREVIEWS = "ONLINE_PREVIEW";
    public static final String ONLINE_THUMBNAILS = "ONLINE_THUMBNAIL";
    public static final String PLATFORM_VERSION = "PLATFORM_VERSION";
    public static final int RESOURCE_LATEST_VERSION = 0;
    public static final int RESOURCE_NOT_EXIST = 2;
    public static final int RESOURCE_OLD_VERSION = 1;
    public static final String SIZE = "SIZE";
    public static final String STATUS = "STATUS";
    public static final String VERSION = "VERSION";
    private String mDescription;
    private String mDividerTitle;
    private String mFileHash;
    private long mFileModifiedTime;
    private long mFileSize;
    private String mId;
    private Bundle mInformation;
    private String mLocalPath;
    private List mLocalPreviews;
    private List mLocalThumbnails;
    private String mOnlinePath;
    private List mOnlinePreviews;
    private List mOnlineThumbnails;
    private int mPlatformVersion;
    private int mStatus;
    private String mTitle;
}
