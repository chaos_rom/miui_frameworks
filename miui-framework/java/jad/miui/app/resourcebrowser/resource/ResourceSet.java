// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser.resource;

import java.util.*;
import miui.widget.DataGroup;

public class ResourceSet extends ArrayList {

    private ResourceSet() {
        mMetaData = new HashMap();
    }

    public static ResourceSet getInstance(String s) {
        ResourceSet resourceset = (ResourceSet)instances.get(s);
        if(resourceset != null) goto _L2; else goto _L1
_L1:
        miui/app/resourcebrowser/resource/ResourceSet;
        JVM INSTR monitorenter ;
        ResourceSet resourceset1;
        resourceset = (ResourceSet)instances.get(s);
        if(resourceset != null)
            break MISSING_BLOCK_LABEL_58;
        resourceset1 = new ResourceSet();
        instances.put(s, resourceset1);
        resourceset = resourceset1;
        miui/app/resourcebrowser/resource/ResourceSet;
        JVM INSTR monitorexit ;
          goto _L2
_L4:
        Exception exception;
        throw exception;
        exception;
        continue; /* Loop/switch isn't completed */
_L2:
        return resourceset;
        exception;
        if(true) goto _L4; else goto _L3
_L3:
    }

    public Object getMetaData(String s) {
        return mMetaData.get(s);
    }

    public void set(List list, int i) {
        DataGroup datagroup = (DataGroup)get(i);
        datagroup.clear();
        datagroup.addAll(list);
    }

    public void setMetaData(String s, Object obj) {
        mMetaData.put(s, obj);
    }

    private static Map instances = new HashMap();
    private static final long serialVersionUID = 1L;
    private Map mMetaData;

}
