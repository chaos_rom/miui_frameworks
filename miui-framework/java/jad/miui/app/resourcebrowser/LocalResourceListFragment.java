// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser;

import android.os.Bundle;
import miui.os.AsyncTaskObserver;
import miui.os.ExtraFileUtils;
import miui.util.CommandLineUtils;

// Referenced classes of package miui.app.resourcebrowser:
//            ResourceListFragment, LocalResourceAdapter, ResourceConstants, ResourceAdapter

public class LocalResourceListFragment extends ResourceListFragment
    implements AsyncTaskObserver {

    public LocalResourceListFragment() {
    }

    protected void addMetaData(Bundle bundle) {
        super.addMetaData(bundle);
        bundle.putString("miui.app.resourcebrowser.RESOURCE_SET_SUBPACKAGE", ".local");
    }

    protected ResourceAdapter getAdapter() {
        return new LocalResourceAdapter(this, super.mMetaData);
    }

    protected int getContentView() {
        return 0x6030018;
    }

    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        ExtraFileUtils.addNoMedia(ResourceConstants.CACHE_PATH);
        if(ResourceConstants.MIUI_PATH.equals(ResourceConstants.MIUI_EXTERNAL_PATH)) {
            Object aobj[] = new Object[1];
            aobj[0] = ResourceConstants.PREVIEW_PATH;
            CommandLineUtils.run(true, null, "rm -r %s_data_sdcard_*", aobj);
        }
    }

    public void onResume() {
        super.onResume();
        super.mAdapter.loadData();
    }
}
