// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser;

import android.app.*;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.*;
import java.util.*;

// Referenced classes of package miui.app.resourcebrowser:
//            BaseFragment

public abstract class BaseTabActivity extends Activity
    implements android.app.ActionBar.TabListener, android.support.v4.view.ViewPager.OnPageChangeListener {
    protected class ResourcePagerAdapter extends PagerAdapter {

        public void destroyItem(ViewGroup viewgroup, int i, Object obj) {
            if(mCurTransaction == null)
                mCurTransaction = mFragmentManager.beginTransaction();
            mCurTransaction.hide((Fragment)obj);
        }

        public void finishUpdate(ViewGroup viewgroup) {
            if(mCurTransaction != null) {
                mCurTransaction.commitAllowingStateLoss();
                mCurTransaction = null;
                mFragmentManager.executePendingTransactions();
            }
        }

        public int getCount() {
            return mTabFragments.size();
        }

        public int getItemPosition(Object obj) {
            return mTabFragments.indexOf(obj);
        }

        public Object instantiateItem(ViewGroup viewgroup, int i) {
            if(mCurTransaction == null)
                mCurTransaction = mFragmentManager.beginTransaction();
            Fragment fragment = (Fragment)mTabFragments.get(i);
            mCurTransaction.show(fragment);
            return fragment;
        }

        public boolean isViewFromObject(View view, Object obj) {
            boolean flag;
            if(((Fragment)obj).getView() == view)
                flag = true;
            else
                flag = false;
            return flag;
        }

        private FragmentTransaction mCurTransaction;
        private final FragmentManager mFragmentManager;
        final BaseTabActivity this$0;

        public ResourcePagerAdapter() {
            this$0 = BaseTabActivity.this;
            super();
            mCurTransaction = null;
            mFragmentManager = getFragmentManager();
        }
    }


    public BaseTabActivity() {
        mTabFragments = new ArrayList();
        mFragmentsMenuId = new ArrayList();
    }

    private void createActionBar() {
        mActionBar = getActionBar();
        mActionBar.setNavigationMode(2);
        android.app.ActionBar.Tab tab;
        for(Iterator iterator = getActionBarTabs().iterator(); iterator.hasNext(); mActionBar.addTab(tab)) {
            tab = (android.app.ActionBar.Tab)iterator.next();
            tab.setTabListener(this);
        }

    }

    private void createPagerAdapter() {
        mPageAdapter = getResourcePagerAdapter();
        mViewPager.setAdapter(mPageAdapter);
        mViewPager.setOnPageChangeListener(this);
    }

    private void createTabFragments() {
        FragmentManager fragmentmanager = getFragmentManager();
        FragmentTransaction fragmenttransaction = fragmentmanager.beginTransaction();
        mTabFragments.clear();
        for(int i = 0; i < mActionBar.getTabCount(); i++) {
            String s = (new StringBuilder()).append("tag-").append(i).toString();
            Object obj = fragmentmanager.findFragmentByTag(s);
            if(obj == null) {
                obj = initTabFragment(i);
                fragmenttransaction.add(1, ((Fragment) (obj)), s);
                fragmenttransaction.hide(((Fragment) (obj)));
            }
            mTabFragments.add((BaseFragment)obj);
        }

        fragmenttransaction.commitAllowingStateLoss();
        fragmentmanager.executePendingTransactions();
    }

    private void selectTab(int i, boolean flag) {
        if(i != mCurrentPagePosition) {
            ((BaseFragment)mTabFragments.get(mCurrentPagePosition)).onVisiableChanged(false);
            mCurrentPagePosition = i;
            mActionBar.setSelectedNavigationItem(i);
            if(flag)
                mViewPager.setCurrentItem(i, true);
            invalidateOptionsMenu();
            ((BaseFragment)mTabFragments.get(mCurrentPagePosition)).onVisiableChanged(true);
        }
    }

    protected abstract ArrayList getActionBarTabs();

    protected ResourcePagerAdapter getResourcePagerAdapter() {
        return new ResourcePagerAdapter();
    }

    protected abstract BaseFragment initTabFragment(int i);

    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        mViewPager = new ViewPager(this);
        mViewPager.setId(1);
        setContentView(mViewPager);
        createPagerAdapter();
        createActionBar();
        createTabFragments();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        mFragmentsMenuId.clear();
        List list;
        for(Iterator iterator = mTabFragments.iterator(); iterator.hasNext(); mFragmentsMenuId.add(list))
            list = ((BaseFragment)iterator.next()).onFragmentCreateOptionsMenu(menu);

        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem menuitem) {
        for(int i = 0; i < mFragmentsMenuId.size(); i++)
            if(((List)mFragmentsMenuId.get(i)).contains(Integer.valueOf(menuitem.getItemId())))
                ((BaseFragment)mTabFragments.get(i)).onFragmentOptionsItemSelected(menuitem);

        return super.onOptionsItemSelected(menuitem);
    }

    public void onPageScrollStateChanged(int i) {
    }

    public void onPageScrolled(int i, float f, int j) {
    }

    public void onPageSelected(int i) {
        selectTab(i, false);
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean flag;
        for(int i = 0; i < mTabFragments.size(); i++) {
            BaseFragment basefragment = (BaseFragment)mTabFragments.get(i);
            if(mCurrentPagePosition == i)
                flag = true;
            else
                flag = false;
            basefragment.onFragmentPrepareOptionsMenu(menu, flag);
            if(flag)
                continue;
            for(Iterator iterator = ((List)mFragmentsMenuId.get(i)).iterator(); iterator.hasNext(); menu.findItem(((Integer)iterator.next()).intValue()).setVisible(false));
        }

        return super.onPrepareOptionsMenu(menu);
    }

    public void onTabReselected(android.app.ActionBar.Tab tab, FragmentTransaction fragmenttransaction) {
    }

    public void onTabSelected(android.app.ActionBar.Tab tab, FragmentTransaction fragmenttransaction) {
        selectTab(tab.getPosition(), true);
    }

    public void onTabUnselected(android.app.ActionBar.Tab tab, FragmentTransaction fragmenttransaction) {
    }

    private static final int VIEW_PAGE_ID = 1;
    protected ActionBar mActionBar;
    protected int mCurrentPagePosition;
    protected List mFragmentsMenuId;
    protected ResourcePagerAdapter mPageAdapter;
    protected List mTabFragments;
    protected ViewPager mViewPager;

    static  {
        AsyncTask.setDefaultExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
}
