// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser;

import android.app.ActionBar;
import android.content.Intent;
import android.os.*;
import android.util.Pair;
import java.util.ArrayList;
import miui.app.resourcebrowser.util.CleanCacheTask;
import miui.app.resourcebrowser.util.ResourceHelper;

// Referenced classes of package miui.app.resourcebrowser:
//            BaseTabActivity, IntentConstants, LocalResourceListFragment, OnlineResourceListFragment, 
//            BaseFragment

public class ResourceTabActivity extends BaseTabActivity
    implements IntentConstants {

    public ResourceTabActivity() {
    }

    protected Bundle buildDefaultMetaData(Bundle bundle, String s) {
        return ResourceHelper.buildDefaultMetaData(bundle, s, this);
    }

    protected Bundle buildLocalResourceListMetaData(Bundle bundle, String s) {
        return (Bundle)bundle.clone();
    }

    protected Bundle buildOnlineResourceListMetaData(Bundle bundle, String s) {
        return (Bundle)bundle.clone();
    }

    protected ArrayList getActionBarTabs() {
        ArrayList arraylist = new ArrayList();
        ActionBar actionbar = getActionBar();
        String s = mMetaData.getString("miui.app.resourcebrowser.RESOURCE_SET_NAME");
        android.app.ActionBar.Tab tab = actionbar.newTab();
        Object aobj[] = new Object[1];
        aobj[0] = s;
        arraylist.add(tab.setText(getString(0x60c0014, aobj)));
        android.app.ActionBar.Tab tab1 = actionbar.newTab();
        Object aobj1[] = new Object[1];
        aobj1[0] = s;
        arraylist.add(tab1.setText(getString(0x60c0015, aobj1)));
        return arraylist;
    }

    protected Pair getLocalResourceListActivity() {
        return new Pair(mMetaData.getString("miui.app.resourcebrowser.LOCAL_LIST_ACTIVITY_PACKAGE"), mMetaData.getString("miui.app.resourcebrowser.LOCAL_LIST_ACTIVITY_CLASS"));
    }

    protected Pair getOnlineResourceListActivity() {
        return new Pair(mMetaData.getString("miui.app.resourcebrowser.ONLINE_LIST_ACTIVITY_PACKAGE"), mMetaData.getString("miui.app.resourcebrowser.ONLINE_LIST_ACTIVITY_CLASS"));
    }

    protected BaseFragment initTabFragment(int i) {
        Object obj;
        if(i == 0)
            obj = new LocalResourceListFragment();
        else
        if(i == 1)
            obj = new OnlineResourceListFragment();
        else
            obj = null;
        return ((BaseFragment) (obj));
    }

    protected void onCreate(Bundle bundle) {
        Intent intent;
        Bundle bundle1;
        Bundle bundle2;
        intent = getIntent();
        bundle1 = intent.getBundleExtra("META_DATA");
        bundle2 = intent.getExtras();
        if(bundle1 != null || bundle2 == null) goto _L2; else goto _L1
_L1:
        bundle1 = bundle2;
_L4:
        mAction = intent.getAction();
        mMetaData = buildDefaultMetaData(bundle1, mAction);
        intent.putExtra("META_DATA_FOR_LOCAL", buildLocalResourceListMetaData(bundle1, mAction));
        intent.putExtra("META_DATA_FOR_ONLINE", buildOnlineResourceListMetaData(bundle1, mAction));
        super.onCreate(bundle);
        (new Handler() {

            public void handleMessage(Message message) {
                int i = mMetaData.getInt("android.intent.extra.ringtone.TYPE", -1);
                if(i >= 0)
                    ResourceHelper.setMusicVolumeType(ResourceTabActivity.this, i);
            }

            final ResourceTabActivity this$0;

             {
                this$0 = ResourceTabActivity.this;
                super();
            }
        }).sendEmptyMessageDelayed(0, 2000L);
        CleanCacheTask.getInstance().run();
        return;
_L2:
        if(bundle1 == null)
            bundle1 = new Bundle();
        else
        if(bundle2 != null) {
            bundle2.remove("META_DATA");
            bundle1.putAll(bundle2);
        }
        if(true) goto _L4; else goto _L3
_L3:
    }

    protected String mAction;
    protected Bundle mMetaData;
}
