// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import java.util.*;
import miui.app.resourcebrowser.resource.Resource;
import miui.os.AsyncTaskObserver;

// Referenced classes of package miui.app.resourcebrowser:
//            LocalResourceListFragment, ResourceListFragment, ResourceAdapter, LocalResourceAdapter, 
//            BaseFragment

public class ResourceFilterListFragment extends LocalResourceListFragment {
    public static class FilterAdapter extends LocalResourceAdapter {
        public class AsyncClockStyleLoadTask extends miui.widget.AsyncAdapter.AsyncLoadDataTask {

            protected volatile Object[] loadData(int i) {
                return loadData(i);
            }

            protected Resource[] loadData(int i) {
                return null;
            }

            public volatile void onLoadData(Object aobj[]) {
                onLoadData((Resource[])aobj);
            }

            public transient void onLoadData(Resource aresource[]) {
                if(aresource == null) goto _L2; else goto _L1
_L1:
                boolean aflag[];
                int i;
                int j;
                aflag = new boolean[aresource.length];
                i = 0;
                j = 0;
_L7:
                if(j >= aresource.length) goto _L4; else goto _L3
_L3:
                HashMap hashmap = (HashMap)aresource[j].getInformation().getSerializable("NVP");
                if(hashmap != null) goto _L6; else goto _L5
_L5:
                j++;
                  goto _L7
_L6:
                String s = (String)hashmap.get("type");
                boolean flag;
                if((mFilterCode & Integer.valueOf(s).intValue()) == 0)
                    break MISSING_BLOCK_LABEL_106;
                flag = true;
_L8:
                boolean flag1;
                aflag[j] = flag;
                flag1 = aflag[j];
                if(flag1)
                    i++;
                  goto _L5
                flag = false;
                  goto _L8
_L4:
                Resource aresource1[] = new Resource[i];
                int k = 0;
                for(int l = 0; l < aresource1.length; l++)
                    if(aflag[l]) {
                        int i1 = k + 1;
                        aresource1[k] = aresource[l];
                        k = i1;
                    }

                aresource = aresource1;
_L2:
                super.onLoadData(aresource);
                return;
                NumberFormatException numberformatexception;
                numberformatexception;
                  goto _L5
            }

            final FilterAdapter this$0;

            public AsyncClockStyleLoadTask() {
                this$0 = FilterAdapter.this;
                super(FilterAdapter.this);
            }
        }


        protected List getLoadDataTask() {
            ArrayList arraylist = new ArrayList();
            AsyncClockStyleLoadTask asyncclockstyleloadtask = new AsyncClockStyleLoadTask();
            asyncclockstyleloadtask.addObserver((AsyncTaskObserver)getRegisterAsyncTaskObserver());
            List list = getVisitors();
            for(int i = 0; i < list.size(); i++)
                asyncclockstyleloadtask.addVisitor((miui.widget.AsyncAdapter.AsyncLoadDataVisitor)list.get(i));

            arraylist.add(asyncclockstyleloadtask);
            return arraylist;
        }

        private final int mFilterCode;


        public FilterAdapter(BaseFragment basefragment, Bundle bundle, int i) {
            super(basefragment, bundle);
            mFilterCode = i;
        }
    }


    public ResourceFilterListFragment() {
    }

    protected void addMetaData(Bundle bundle) {
        super.addMetaData(bundle);
        super.mMetaData.putString("miui.app.resourcebrowser.RESOURCE_SET_SUBPACKAGE", ".local");
    }

    protected ResourceAdapter getAdapter() {
        return new FilterAdapter(this, super.mMetaData, super.mActivity.getIntent().getIntExtra("type", 0));
    }

    public static final String FILTER = "type";
}
