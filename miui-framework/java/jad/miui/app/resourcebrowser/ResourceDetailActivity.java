// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app.resourcebrowser;

import android.animation.*;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.*;
import android.widget.*;
import java.io.File;
import java.text.DateFormat;
import java.util.*;
import miui.app.SDCardMonitor;
import miui.app.resourcebrowser.resource.Resource;
import miui.app.resourcebrowser.resource.ResourceSet;
import miui.app.resourcebrowser.service.ResourceDataParser;
import miui.app.resourcebrowser.service.ResourceJSONDataParser;
import miui.app.resourcebrowser.service.local.ZipResourceInfo;
import miui.app.resourcebrowser.service.online.OnlineProtocolConstants;
import miui.app.resourcebrowser.service.online.OnlineService;
import miui.app.resourcebrowser.util.BatchMediaPlayer;
import miui.app.resourcebrowser.util.DownloadFileTask;
import miui.app.resourcebrowser.util.ImageCacheDecoder;
import miui.app.resourcebrowser.util.ResourceDebug;
import miui.app.resourcebrowser.util.ResourceHelper;
import miui.app.resourcebrowser.util.ResourceScreenView;
import miui.app.resourcebrowser.view.ResourceOperationHandler;
import miui.app.resourcebrowser.view.ResourceOperationView;
import miui.app.resourcebrowser.view.ResourceState;
import miui.os.ExtraFileUtils;
import miui.os.Shell;
import miui.widget.DataGroup;

// Referenced classes of package miui.app.resourcebrowser:
//            IntentConstants, ResourceConstants

public class ResourceDetailActivity extends Activity
    implements miui.app.SDCardMonitor.SDCardStatusListener, IntentConstants, OnlineProtocolConstants {
    public class DownloadDetailTask extends DownloadFileTask {

        protected volatile void onPostExecute(Object obj) {
            onPostExecute((List)obj);
        }

        protected void onPostExecute(List list) {
            if(!isFinishing() && list != null) goto _L2; else goto _L1
_L1:
            return;
_L2:
            String s = null;
            if(list.size() > 0)
                s = ((miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry)list.get(0)).getPath();
            if(s != null) {
                Resource resource = mParser.readResource(s, mMetaData);
                Resource resource1 = getCurrentResource(validIndex);
                if(resource != null && resource1 != null) {
                    resource1.setInformation(resource.getInformation());
                    if(validIndex == mResourceIndex) {
                        setResourceInfo();
                        bindScreenView();
                    }
                }
            }
            if(true) goto _L1; else goto _L3
_L3:
        }

        final ResourceDetailActivity this$0;
        private int validIndex;

        public DownloadDetailTask(int i) {
            this$0 = ResourceDetailActivity.this;
            super();
            validIndex = i;
        }
    }

    public class DownloadListTask extends DownloadFileTask {

        public int getOffset() {
            return offset;
        }

        protected volatile void onPostExecute(Object obj) {
            onPostExecute((List)obj);
        }

        protected void onPostExecute(List list) {
            if(!isFinishing()) goto _L2; else goto _L1
_L1:
            return;
_L2:
            String s;
            s = null;
            if(list.size() > 0)
                s = ((miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry)list.get(0)).getPath();
            if(s != null)
                break; /* Loop/switch isn't completed */
            mReachBottom = true;
            Toast.makeText(ResourceDetailActivity.this, 0x60c0020, 0).show();
_L4:
            mDownloadSet.remove(this);
            if(true) goto _L1; else goto _L3
_L3:
            List list1 = mParser.readResources(s, mMetaData);
            if(list1 == null || list1.size() == 0) {
                mReachBottom = true;
            } else {
                if(offset == 0) {
                    mResourceSet.set(list1, mResourceGroup);
                } else {
                    int i = ((DataGroup)mResourceSet.get(mResourceGroup)).size();
                    if(offset == i)
                        ((DataGroup)mResourceSet.get(mResourceGroup)).addAll(list1);
                }
                updateNavigationState();
            }
              goto _L4
            if(true) goto _L1; else goto _L5
_L5:
        }

        public void setOffset(int i) {
            offset = i;
        }

        private int offset;
        final ResourceDetailActivity this$0;

        public DownloadListTask() {
            this$0 = ResourceDetailActivity.this;
            super();
            offset = 0;
        }
    }

    private class PreviewInfo {

        String localPath;
        String onlinePath;
        final ResourceDetailActivity this$0;

        private PreviewInfo() {
            this$0 = ResourceDetailActivity.this;
            super();
        }

    }


    public ResourceDetailActivity() {
        mFullScreenParams = new android.view.ViewGroup.LayoutParams(-1, -1);
        mImageParams = new android.view.ViewGroup.LayoutParams(-1, -1);
        mDownloadSet = new HashSet();
        mHandler = new Handler();
        mService = OnlineService.getInstance();
        mParser = ResourceJSONDataParser.getInstance();
        mPreviewInfos = new ArrayList();
    }

    private void addImageView(ImageView imageview) {
        FrameLayout framelayout = new FrameLayout(this);
        framelayout.addView(imageview);
        enterNormalMode(framelayout);
        mPreview.addView(framelayout, mNormalParams);
    }

    private void bindScreenImageView() {
        initImageDecoder();
        mPreviewInfos.clear();
        if(mResourceSetCategory == 0) {
            mPreview.addView(mInfo, 0);
            mPreviewInfos.add(0, null);
            if(mPreviewOffset == 0)
                mPreviewOffset = 1;
        }
        Bundle bundle = getCurrentResourceInformation();
        ArrayList arraylist = bundle.getStringArrayList("LOCAL_PREVIEW");
        ArrayList arraylist1 = bundle.getStringArrayList("ONLINE_PREVIEW");
        if(arraylist != null) {
            boolean flag;
            long l;
            int i;
            int j;
            if(getCurrentResource().getStatus() == 2)
                flag = true;
            else
                flag = false;
            l = bundle.getLong("MODIFIED_TIME");
            i = Math.min(15, arraylist.size());
            j = 0;
            while(j < i)  {
                ImageView imageview = new ImageView(this);
                imageview.setScaleType(android.widget.ImageView.ScaleType.FIT_CENTER);
                imageview.setImageResource(0x60200c3);
                imageview.setLayoutParams(mImageParams);
                if(mScreenViewNeedBackgroud) {
                    imageview.setBackgroundResource(mScreenViewBackgroudId);
                    int k = (int)getResources().getDimension(0x60a0029);
                    imageview.setPadding(k, k, k, k);
                }
                String s = (String)arraylist.get(j);
                File file = new File(s);
                if(arraylist1 != null && flag && file.exists() && file.lastModified() < l)
                    file.delete();
                addImageView(imageview);
                PreviewInfo previewinfo = new PreviewInfo();
                previewinfo.localPath = s;
                String s1;
                if(arraylist1 != null)
                    s1 = (String)arraylist1.get(j);
                else
                    s1 = null;
                previewinfo.onlinePath = s1;
                mPreviewInfos.add(previewinfo);
                j++;
            }
            if(i == 0) {
                ImageView imageview1 = new ImageView(this);
                imageview1.setScaleType(android.widget.ImageView.ScaleType.FIT_CENTER);
                imageview1.setImageResource(0x60201ba);
                imageview1.setLayoutParams(mImageParams);
                addImageView(imageview1);
                imageview1.setClickable(false);
            } else {
                initImageForScreenView(0 + mPreviewOffset);
            }
        }
    }

    private void buildModeChangeAnimator() {
        float af[] = new float[2];
        af[0] = 1.0F;
        af[1] = 0.7F;
        PropertyValuesHolder propertyvaluesholder = PropertyValuesHolder.ofFloat("scaleX", af);
        float af1[] = new float[2];
        af1[0] = 1.0F;
        af1[1] = 0.7F;
        PropertyValuesHolder propertyvaluesholder1 = PropertyValuesHolder.ofFloat("scaleY", af1);
        float af2[] = new float[2];
        af2[0] = 1.0F;
        af2[1] = 0.0F;
        PropertyValuesHolder propertyvaluesholder2 = PropertyValuesHolder.ofFloat("alpha", af2);
        ImageView imageview = mCoverView;
        PropertyValuesHolder apropertyvaluesholder[] = new PropertyValuesHolder[3];
        apropertyvaluesholder[0] = propertyvaluesholder;
        apropertyvaluesholder[1] = propertyvaluesholder1;
        apropertyvaluesholder[2] = propertyvaluesholder2;
        mToNormalModeAnimator = ObjectAnimator.ofPropertyValuesHolder(imageview, apropertyvaluesholder).setDuration(200L);
        mToNormalModeAnimator.addListener(new AnimatorListenerAdapter() {

            public void onAnimationEnd(Animator animator) {
                mCoverView.setVisibility(8);
                onEndEnterNormalMode();
                super.onAnimationEnd(animator);
            }

            public void onAnimationStart(Animator animator) {
                String s = ((PreviewInfo)mPreviewInfos.get(mPreview.getCurrentScreenIndex())).localPath;
                android.graphics.Bitmap bitmap = mImageCacheDecoder.getBitmap(s);
                mCoverView.setImageBitmap(bitmap);
                mCoverView.setVisibility(0);
                onBeginEnterNormalMode();
                super.onAnimationStart(animator);
            }

            final ResourceDetailActivity this$0;

             {
                this$0 = ResourceDetailActivity.this;
                super();
            }
        });
        float af3[] = new float[2];
        af3[0] = 0.7F;
        af3[1] = 1.0F;
        PropertyValuesHolder propertyvaluesholder3 = PropertyValuesHolder.ofFloat("scaleX", af3);
        float af4[] = new float[2];
        af4[0] = 0.7F;
        af4[1] = 1.0F;
        PropertyValuesHolder propertyvaluesholder4 = PropertyValuesHolder.ofFloat("scaleY", af4);
        float af5[] = new float[2];
        af5[0] = 0.0F;
        af5[1] = 1.0F;
        PropertyValuesHolder propertyvaluesholder5 = PropertyValuesHolder.ofFloat("alpha", af5);
        ImageView imageview1 = mCoverView;
        PropertyValuesHolder apropertyvaluesholder1[] = new PropertyValuesHolder[3];
        apropertyvaluesholder1[0] = propertyvaluesholder3;
        apropertyvaluesholder1[1] = propertyvaluesholder4;
        apropertyvaluesholder1[2] = propertyvaluesholder5;
        mToFullScreenModeAnimator = ObjectAnimator.ofPropertyValuesHolder(imageview1, apropertyvaluesholder1).setDuration(200L);
        mToFullScreenModeAnimator.addListener(new AnimatorListenerAdapter() {

            public void onAnimationEnd(Animator animator) {
                mCoverView.setVisibility(8);
                onEndEnterFullScreenMode();
                super.onAnimationEnd(animator);
            }

            public void onAnimationStart(Animator animator) {
                String s = ((PreviewInfo)mPreviewInfos.get(mPreview.getCurrentScreenIndex())).localPath;
                android.graphics.Bitmap bitmap = mImageCacheDecoder.getBitmap(s);
                mCoverView.setImageBitmap(bitmap);
                mCoverView.setVisibility(0);
                onBeginEnterFullScreenMode();
                super.onAnimationStart(animator);
            }

            final ResourceDetailActivity this$0;

             {
                this$0 = ResourceDetailActivity.this;
                super();
            }
        });
    }

    private void decodeImageForScreenView(int i) {
        if(i >= 0 && i < mPreviewInfos.size() && mPreviewInfos.get(i) != null) {
            PreviewInfo previewinfo = (PreviewInfo)mPreviewInfos.get(i);
            String s = previewinfo.localPath;
            String s1 = previewinfo.onlinePath;
            android.graphics.Bitmap bitmap = mImageCacheDecoder.getBitmap(s);
            if(bitmap != null)
                ((ImageView)((ViewGroup)mPreview.getScreen(i)).getChildAt(0)).setImageBitmap(bitmap);
            else
                mImageCacheDecoder.decodeImageAsync(s, s1, i);
        }
    }

    private void enterFullScreenMode() {
        if(mPreview.getCurrentScreenIndex() != 0 || mResourceSetCategory != 0)
            mToFullScreenModeAnimator.start();
    }

    private void enterFullScreenMode(View view) {
        view.setLayoutParams(mFullScreenParams);
        view.setPadding(0, 0, 0, 0);
        view.setBackgroundColor(0xff000000);
        ImageView imageview = (ImageView)((ViewGroup)view).getChildAt(0);
        imageview.setAdjustViewBounds(false);
        imageview.setOnClickListener(new android.view.View.OnClickListener() {

            public void onClick(View view1) {
                enterNormalMode();
            }

            final ResourceDetailActivity this$0;

             {
                this$0 = ResourceDetailActivity.this;
                super();
            }
        });
    }

    private void enterNormalMode() {
        mToNormalModeAnimator.start();
    }

    private void enterNormalMode(View view) {
        view.setLayoutParams(mNormalParams);
        view.setPadding(6, 0, 6, 60);
        view.setBackgroundResource(0);
        ImageView imageview = (ImageView)((ViewGroup)view).getChildAt(0);
        imageview.setAdjustViewBounds(true);
        imageview.setOnClickListener(new android.view.View.OnClickListener() {

            public void onClick(View view1) {
                enterFullScreenMode();
            }

            final ResourceDetailActivity this$0;

             {
                this$0 = ResourceDetailActivity.this;
                super();
            }
        });
    }

    private int getScreenViewIndexForPreviewImage(String s) {
        int i = 0;
_L3:
        PreviewInfo previewinfo;
        if(i >= mPreviewInfos.size())
            break MISSING_BLOCK_LABEL_52;
        previewinfo = (PreviewInfo)mPreviewInfos.get(i);
        if(previewinfo == null || !TextUtils.equals(previewinfo.localPath, s)) goto _L2; else goto _L1
_L1:
        return i;
_L2:
        i++;
          goto _L3
        i = -1;
          goto _L1
    }

    private void initImageDecoder() {
        if(mImageCacheDecoder != null)
            mImageCacheDecoder.clean(true);
        mImageCacheDecoder = new ImageCacheDecoder(3);
        mImageCacheDecoder.regeisterListener(new miui.app.resourcebrowser.util.ImageCacheDecoder.ImageDecodingListener() {

            public void handleDecodingResult(boolean flag, String s, String s1) {
                if(flag) {
                    int i = getScreenViewIndexForPreviewImage(s);
                    if(i >= 0 && isVisiableScreen(i))
                        decodeImageForScreenView(i);
                }
            }

            public void handleDownloadResult(boolean flag, String s, String s1) {
                int i = getScreenViewIndexForPreviewImage(s);
                if(i >= 0 && isVisiableScreen(i))
                    if(flag)
                        mImageCacheDecoder.decodeImageAsync(s, s1, i);
                    else
                        Toast.makeText(ResourceDetailActivity.this, 0x60c0024, 0).show();
            }

            final ResourceDetailActivity this$0;

             {
                this$0 = ResourceDetailActivity.this;
                super();
            }
        });
    }

    private void initImageForScreenView(int i) {
        mImageCacheDecoder.setCurrentUseBitmapIndex(i);
        decodeImageForScreenView(i + 0);
        decodeImageForScreenView(i + 1);
        decodeImageForScreenView(i - 1);
    }

    private void initPlayer() {
        mBatchPlayer = new BatchMediaPlayer(this);
        mBatchPlayer.setListener(new miui.app.resourcebrowser.util.BatchMediaPlayer.BatchPlayerListener() {

            public void finish(boolean flag) {
                mPlayButton.setImageResource(0x60200d5);
                if(mRingtoneName != null)
                    mRingtoneName.setText(getFormatTitleBeforePlayingRingtone());
                if(flag)
                    Toast.makeText(ResourceDetailActivity.this, 0x60c0020, 0).show();
            }

            public void play(String s, int i, int j) {
                if(mRingtoneName != null)
                    mRingtoneName.setText(getFormatPlayingRingtoneName(s, i, j));
            }

            final ResourceDetailActivity this$0;

             {
                this$0 = ResourceDetailActivity.this;
                super();
            }
        });
        mBatchPlayer.setPlayList(getMusicPlayList(getCurrentResource()));
    }

    private boolean isVisiableScreen(int i) {
        boolean flag;
        if(Math.abs(i - mImageCacheDecoder.getCurrentUseBitmapIndex()) < 2)
            flag = true;
        else
            flag = false;
        return flag;
    }

    private void onBeginEnterFullScreenMode() {
        getWindow().addFlags(1024);
        getActionBar().hide();
        mPreview.setClickable(false);
    }

    private void onBeginEnterNormalMode() {
        getWindow().clearFlags(1024);
        getActionBar().show();
        for(int i = 0; i < mPreview.getScreenCount(); i++)
            enterNormalMode(mPreview.getScreen(i));

        int j = mPreview.getCurrentScreenIndex();
        if(mResourceSetCategory == 0) {
            mPreview.addView(mInfo, 0);
            mPreviewInfos.add(0, null);
            mPreview.setCurrentScreen(j + 1);
        }
        if(mHasActionBar)
            ((LinearLayout)findViewById(0x60b007e)).setPadding(0, getActionBar().getHeight(), 0, 0);
        mOperationView.setVisibility(0);
        ResourceScreenView resourcescreenview = mPreview;
        int k;
        if(mPreview.getScreenCount() > 1)
            k = 0;
        else
            k = 8;
        resourcescreenview.setSeekBarVisibility(k);
        mPreview.setBackgroundResource(0);
        mPreview.setClickable(false);
        mFullScreen = false;
    }

    private void onEndEnterFullScreenMode() {
        int i = mPreview.getCurrentScreenIndex();
        if(mResourceSetCategory == 0) {
            mPreview.removeScreen(0);
            mPreviewInfos.remove(0);
        }
        for(int j = 0; j < mPreview.getScreenCount(); j++)
            enterFullScreenMode(mPreview.getScreen(j));

        if(mResourceSetCategory == 0)
            mPreview.setCurrentScreen(i - 1);
        if(mHasActionBar)
            ((LinearLayout)findViewById(0x60b007e)).setPadding(0, 0, 0, 0);
        mOperationView.setVisibility(8);
        mPreview.setSeekBarVisibility(8);
        mPreview.setBackgroundColor(0xff000000);
        mPreview.requestFocus();
        mPreview.setClickable(true);
        mFullScreen = true;
    }

    private void onEndEnterNormalMode() {
        mPreview.requestFocus();
        mPreview.setClickable(true);
    }

    private void requestResources(int i, int j) {
        int k = i * 1;
        if((k - ((DataGroup)mResourceSet.get(mResourceGroup)).size()) / 1 == 0) goto _L2; else goto _L1
_L1:
        return;
_L2:
        StringBuilder stringbuilder = (new StringBuilder()).append(mUrl);
        Object aobj[] = new Object[2];
        aobj[0] = Integer.valueOf(k);
        aobj[1] = Integer.valueOf(j);
        String s = stringbuilder.append(String.format("&start=%s&count=%s", aobj)).toString();
        String s1 = ResourceHelper.getFilePathByURL(mListFolder, s);
        File file = new File(s1);
        if(k == 0 && file.exists()) {
            List list = mParser.readResources(s1, mMetaData);
            if(list != null)
                mResourceSet.set(list, mResourceGroup);
            if(ResourceHelper.isCacheValid(file))
                continue; /* Loop/switch isn't completed */
        }
        DownloadListTask downloadlisttask = new DownloadListTask();
        downloadlisttask.setId((new StringBuilder()).append("list_").append(k).toString());
        downloadlisttask.setOffset(k);
        if(!mDownloadSet.contains(downloadlisttask)) {
            miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry downloadfileentry = new miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry();
            downloadfileentry.setUrl(s);
            downloadfileentry.setPath(s1);
            mDownloadSet.add(downloadlisttask);
            miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry adownloadfileentry[] = new miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry[1];
            adownloadfileentry[0] = downloadfileentry;
            downloadlisttask.execute(adownloadfileentry);
        }
        if(true) goto _L1; else goto _L3
_L3:
    }

    private void stopMusic() {
        if(mBatchPlayer != null) {
            mBatchPlayer.stop();
            mBatchPlayer = null;
        }
    }

    protected void apply() {
    }

    protected void bindScreenRingtoneView() {
        View view = mInflater.inflate(0x603004d, null);
        android.widget.FrameLayout.LayoutParams layoutparams = new android.widget.FrameLayout.LayoutParams(-2, -2, 17);
        mPreview.addView(view, layoutparams);
        mPlayButton = (ImageView)view.findViewById(0x60b0057);
        mPlayButton.setOnClickListener(new android.view.View.OnClickListener() {

            public void onClick(View view1) {
                if(mBatchPlayer == null) {
                    mPlayButton.setImageResource(0x60200d2);
                    initPlayer();
                    mBatchPlayer.start();
                } else
                if(mBatchPlayer.isPlaying()) {
                    mPlayButton.setImageResource(0x60200d5);
                    mBatchPlayer.pause();
                } else {
                    mPlayButton.setImageResource(0x60200d2);
                    mBatchPlayer.start();
                }
            }

            final ResourceDetailActivity this$0;

             {
                this$0 = ResourceDetailActivity.this;
                super();
            }
        });
        if(TextUtils.isEmpty(mLocalPath))
            mPlayButton.setVisibility(4);
        if(mMetaData.getBoolean("miui.app.resourcebrowser.SHOW_RINGTONE_NAME", false)) {
            mRingtoneName = (TextView)view.findViewById(0x60b0058);
            mRingtoneName.setVisibility(0);
            mRingtoneName.setText(getFormatTitleBeforePlayingRingtone());
        }
    }

    protected void bindScreenView() {
        mPreview.removeAllScreens();
        ResourceScreenView resourcescreenview;
        int i;
        if(mResourceSetCategory == 2) {
            bindScreenRingtoneView();
            stopMusic();
        } else {
            bindScreenImageView();
        }
        resourcescreenview = mPreview;
        if(mPreview.getScreenCount() > 1)
            i = 0;
        else
            i = 8;
        resourcescreenview.setSeekBarVisibility(i);
        mPreview.setCurrentScreen(mPreviewOffset);
    }

    protected Bundle buildDefaultMetaData(Bundle bundle, String s) {
        return ResourceHelper.buildDefaultMetaData(bundle, s, this);
    }

    protected void changeCurrentResource() {
        requestResourceDetail(mResourceIndex);
        setResourceInfo();
        setResourceStatus();
        bindScreenView();
    }

    protected void delete() {
        if(!TextUtils.isEmpty(mLocalPath)) {
            String s = mLocalPath.replace('/', '_');
            Shell.remove((new StringBuilder()).append(ResourceConstants.PREVIEW_PATH).append(s).toString());
            if(mIsOnlineResourceSet) {
                getCurrentResource().getInformation().putInt("STATUS", 2);
                setResourceStatus();
            } else {
                finish();
            }
        }
    }

    protected void download() {
    }

    protected void downloadFailed(String s) {
    }

    protected void downloadSuccess(String s) {
        ResourceHelper.setResourceStatus(s, (DataGroup)mResourceSet.get(mResourceGroup), 0);
        if(mOperationHandler.isCurrentResourceDownloadSavePath(s)) {
            mOperationHandler.getResourceState().hasUpdate = false;
            setResourceInfo();
        }
    }

    protected String getConfirmedDownloadUrl(String s) {
        return s;
    }

    protected Resource getCurrentResource() {
        return getCurrentResource(mResourceIndex);
    }

    protected Resource getCurrentResource(int i) {
        List list = (List)mResourceSet.get(mResourceGroup);
        Resource resource;
        if(i >= 0 && i < list.size())
            resource = (Resource)list.get(i);
        else
            resource = null;
        return resource;
    }

    protected Bundle getCurrentResourceInformation() {
        Resource resource = getCurrentResource();
        Bundle bundle;
        if(resource == null)
            bundle = null;
        else
            bundle = resource.getInformation();
        return bundle;
    }

    protected String getFormatPlayingRingtoneName(String s, int i, int j) {
        return ResourceHelper.getDefaultFormatPlayingRingtoneName(s, i, j);
    }

    protected String getFormatTitleBeforePlayingRingtone() {
        return "";
    }

    protected List getMusicPlayList(Resource resource) {
        return ResourceHelper.getDefaultMusicPlayList(this, resource);
    }

    protected ResourceOperationHandler getOperationHandler(ResourceOperationView resourceoperationview) {
        return new ResourceOperationHandler(this, resourceoperationview, getRealResourceState()) {

            public void handleApplyEvent() {
                apply();
                super.handleApplyEvent();
            }

            public void handleDeleteEvent() {
                delete();
                super.handleDeleteEvent();
            }

            public void handleDownloadEvent() {
                download();
                super.handleDownloadEvent();
            }

            public void handlePickEvent() {
                pick();
                super.handlePickEvent();
            }

            public void handleResourceDownloadFailedEvent(String s) {
                downloadFailed(s);
                super.handleResourceDownloadFailedEvent(s);
            }

            public void handleResourceDownloadSuccessedEvent(String s) {
                downloadSuccess(s);
                super.handleResourceDownloadSuccessedEvent(s);
            }

            public void handleUpdateEvent() {
                update();
                super.handleUpdateEvent();
            }

            final ResourceDetailActivity this$0;

             {
                this$0 = ResourceDetailActivity.this;
                super(activity, resourceoperationview, resourcestate);
            }
        };
    }

    protected miui.app.resourcebrowser.view.ResourceOperationView.UIParameter getOperatorViewUIParameter() {
        return null;
    }

    protected ResourceState getRealResourceState() {
        boolean flag = true;
        ResourceState resourcestate = new ResourceState();
        boolean flag1;
        boolean flag2;
        if(!mIsOnlineResourceSet)
            flag1 = flag;
        else
            flag1 = false;
        resourcestate.inLocalPage = flag1;
        resourcestate.localPath = mLocalPath;
        resourcestate.downloadUrl = getConfirmedDownloadUrl(mOnlinePath);
        resourcestate.downloadSavePath = mLocalPath;
        resourcestate.title = mTitle;
        resourcestate.uiVersion = getCurrentResource().getPlatformVersion();
        if(getCurrentResource().getStatus() == flag)
            flag2 = flag;
        else
            flag2 = false;
        resourcestate.hasUpdate = flag2;
        if(mLocalPath.startsWith("/system"))
            flag = false;
        resourcestate.allowDelete = flag;
        resourcestate.isPicker = mMetaData.getBoolean("miui.app.resourcebrowser.USING_PICKER");
        return resourcestate;
    }

    protected void initNavigationState() {
        mPreviousItem.setOnClickListener(new android.view.View.OnClickListener() {

            public void onClick(View view) {
                navigateToPreviousResource();
            }

            final ResourceDetailActivity this$0;

             {
                this$0 = ResourceDetailActivity.this;
                super();
            }
        });
        mNextItem.setOnClickListener(new android.view.View.OnClickListener() {

            public void onClick(View view) {
                navigateToNextResource();
            }

            final ResourceDetailActivity this$0;

             {
                this$0 = ResourceDetailActivity.this;
                super();
            }
        });
        updateNavigationState();
    }

    protected void navigateToNextResource() {
        int i = ((DataGroup)mResourceSet.get(mResourceGroup)).size();
        if(mResourceIndex < i - 1) {
            mResourceIndex = 1 + mResourceIndex;
            changeCurrentResource();
            updateNavigationState();
            mMetaData.putInt("miui.app.resourcebrowser.RESOURCE_INDEX", mResourceIndex);
        }
    }

    protected void navigateToPreviousResource() {
        if(mResourceIndex > 0) {
            mResourceIndex = -1 + mResourceIndex;
            changeCurrentResource();
            updateNavigationState();
            mMetaData.putInt("miui.app.resourcebrowser.RESOURCE_INDEX", mResourceIndex);
        }
    }

    protected boolean needRealRequestDetailInfo(File file) {
        boolean flag;
        if(!ResourceHelper.isCacheValid(file))
            flag = true;
        else
            flag = false;
        return flag;
    }

    public void onBackPressed() {
        if(mFullScreen)
            enterNormalMode();
        else
            super.onBackPressed();
    }

    protected void onCreate(Bundle bundle) {
        boolean flag;
        Intent intent;
        flag = false;
        super.onCreate(bundle);
        requestWindowFeature(9);
        requestWindowFeature(10);
        setContentView(0x603004e);
        intent = getIntent();
        if(intent != null)
            mMetaData = intent.getBundleExtra("META_DATA");
        if(mMetaData == null) {
            mMetaData = buildDefaultMetaData(new Bundle(), intent.getAction());
            intent.putExtra("META_DATA", mMetaData);
        }
        pickMetaData(mMetaData);
        if(!"android.intent.action.VIEW".equals(intent.getAction())) goto _L2; else goto _L1
_L1:
        Resource resource = responseToViewAction();
        if(resource != null) goto _L4; else goto _L3
_L3:
        finish();
_L5:
        return;
_L4:
        mResourceSet.clear();
        DataGroup datagroup = new DataGroup();
        datagroup.add(resource);
        mResourceSet.add(datagroup);
        mResourceGroup = 0;
        mResourceIndex = 0;
_L6:
        if(mResourceSet.isEmpty()) {
            finish();
        } else {
            int i = mMetaData.getInt("android.intent.extra.ringtone.TYPE", -1);
            if(i >= 0)
                ResourceHelper.setMusicVolumeType(this, i);
            mSDCardMonitor = SDCardMonitor.getSDCardMonitor(this);
            mSDCardMonitor.addListener(this);
            mListFolder = ExtraFileUtils.standardizeFolderPath((new StringBuilder()).append(ExtraFileUtils.standardizeFolderPath(mMetaData.getString("miui.app.resourcebrowser.CACHE_LIST_FOLDER"))).append(mResourceSetCode).toString());
            mDetailFolder = ExtraFileUtils.standardizeFolderPath((new StringBuilder()).append(ResourceConstants.DETAIL_PATH).append(mResourceSetCode).toString());
            if(getActionBar() != null)
                flag = true;
            mHasActionBar = flag;
            if(mHasActionBar)
                getActionBar().setHomeButtonEnabled(true);
            if(bundle != null)
                mPreviewOffset = bundle.getInt("PREVIEW_INDEX");
            setupUI();
            setupNavigationButton();
            changeCurrentResource();
        }
        if(true) goto _L5; else goto _L2
_L2:
        mResourceGroup = mMetaData.getInt("miui.app.resourcebrowser.RESOURCE_GROUP");
        mResourceIndex = mMetaData.getInt("miui.app.resourcebrowser.RESOURCE_INDEX");
          goto _L6
    }

    protected void onDestroy() {
        if(mSDCardMonitor != null)
            mSDCardMonitor.removeListener(this);
        stopMusic();
        if(mImageCacheDecoder != null)
            mImageCacheDecoder.clean(true);
        if(mOperationHandler != null)
            mOperationHandler.onActivityOnDestroyEvent();
        super.onDestroy();
    }

    public boolean onOptionsItemSelected(MenuItem menuitem) {
        if(menuitem.getItemId() == 0x102002c)
            finish();
        return super.onOptionsItemSelected(menuitem);
    }

    protected void onPause() {
        stopMusic();
        if(mOperationHandler != null)
            mOperationHandler.onActivityOnPauseEvent();
        if(mPreview != null)
            mPreview.onPause();
        super.onPause();
    }

    protected void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if(mPreview != null)
            bundle.putInt("PREVIEW_INDEX", mPreview.getCurrentScreenIndex());
    }

    public void onStatusChanged(boolean flag) {
        ResourceHelper.exit(this);
    }

    public void onWindowFocusChanged(boolean flag) {
        if(flag && mHasActionBar && !mFullScreen)
            ((LinearLayout)findViewById(0x60b007e)).setPadding(0, getActionBar().getHeight(), 0, 0);
        super.onWindowFocusChanged(flag);
    }

    protected void pick() {
        Intent intent = new Intent();
        intent.putExtra("miui.app.resourcebrowser.PICKED_RESOURCE", mLocalPath);
        intent.putExtra("miui.app.resourcebrowser.TRACK_ID", mMetaData.getString("miui.app.resourcebrowser.TRACK_ID"));
        if(mResourceSetCategory == 2)
            intent.putExtra("android.intent.extra.ringtone.PICKED_URI", ResourceHelper.getUriByPath(mLocalPath));
        setResult(-1, intent);
        finish();
    }

    protected void pickMetaData(Bundle bundle) {
        mResourceSetPackage = mMetaData.getString("miui.app.resourcebrowser.RESOURCE_SET_PACKAGE");
        mResourceSetSubpackage = mMetaData.getString("miui.app.resourcebrowser.RESOURCE_SET_SUBPACKAGE");
        mResourceSetCode = mMetaData.getString("miui.app.resourcebrowser.RESOURCE_SET_CODE");
        mResourceSetCategory = mMetaData.getInt("miui.app.resourcebrowser.RESOURCE_SET_CATEGORY");
        if(mResourceSetSubpackage == null)
            mResourceSetSubpackage = ".single";
        mResourceSet = ResourceSet.getInstance((new StringBuilder()).append(mResourceSetPackage).append(mResourceSetSubpackage).toString());
        mUrl = mMetaData.getString("miui.app.resourcebrowser.LIST_URL");
        boolean flag;
        if(mUrl != null)
            flag = true;
        else
            flag = false;
        mIsOnlineResourceSet = flag;
    }

    protected void requestResourceDetail(int i) {
        Resource resource;
        String s;
        resource = getCurrentResource(i);
        if(resource != null)
            s = resource.getId();
        else
            s = null;
        if(!TextUtils.isEmpty(s) && mIsOnlineResourceSet) goto _L2; else goto _L1
_L1:
        return;
_L2:
        String s1 = mService.getDetailUrl(s);
        String s2 = (new StringBuilder()).append(mDetailFolder).append(s).toString();
        File file = new File(s2);
        if(file.exists()) {
            Resource resource1 = mParser.readResource(s2, mMetaData);
            if(resource1 != null)
                resource.setInformation(resource1.getInformation());
        }
        if(needRealRequestDetailInfo(file)) {
            DownloadDetailTask downloaddetailtask = new DownloadDetailTask(i);
            downloaddetailtask.setId((new StringBuilder()).append("detail_").append(s).toString());
            if(!mDownloadSet.contains(downloaddetailtask)) {
                mDownloadSet.add(downloaddetailtask);
                miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry downloadfileentry = new miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry();
                downloadfileentry.setUrl(s1);
                downloadfileentry.setPath(s2);
                miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry adownloadfileentry[] = new miui.app.resourcebrowser.util.DownloadFileTask.DownloadFileEntry[1];
                adownloadfileentry[0] = downloadfileentry;
                downloaddetailtask.execute(adownloadfileentry);
            }
        }
        if(true) goto _L1; else goto _L3
_L3:
    }

    protected Resource responseToViewAction() {
        Resource resource = null;
        mLocalPath = getIntent().getData().getPath();
        ZipResourceInfo zipresourceinfo = ZipResourceInfo.createZipResourceInfo(this, mLocalPath, null, new Object[0]);
        if(zipresourceinfo != null) {
            resource = new Resource();
            resource.setInformation(zipresourceinfo.getInformation());
        }
        return resource;
    }

    protected void setResourceInfo() {
        Bundle bundle = getCurrentResourceInformation();
        if(bundle != null) goto _L2; else goto _L1
_L1:
        return;
_L2:
        String s4;
        mOnlinePath = bundle.getString("ONLINE_PATH");
        mLocalPath = bundle.getString("LOCAL_PATH");
        mTitle = bundle.getString("NAME");
        if(ResourceDebug.DEBUG)
            mTitle = (new StringBuilder()).append(mResourceIndex).append("-").append(mTitle).toString();
        if(mHasActionBar)
            getActionBar().setTitle(mTitle);
        if(mResourceSetCategory != 0)
            continue; /* Loop/switch isn't completed */
        String s = bundle.getString("AUTHOR");
        if(TextUtils.isEmpty(s))
            s = getString(0x60c000c);
        ((TextView)mInfo.findViewById(0x60b0053)).setText(s);
        String s1 = bundle.getString("DESIGNER");
        if(TextUtils.isEmpty(s1))
            s1 = getString(0x60c000c);
        ((TextView)mInfo.findViewById(0x60b0054)).setText(s1);
        long l = bundle.getLong("MODIFIED_TIME");
        String s2 = "";
        if(l != 0L)
            s2 = DateFormat.getDateInstance().format(Long.valueOf(l));
        ((TextView)mInfo.findViewById(0x60b0055)).setText(s2);
        if(!mIsOnlineResourceSet)
            ((TextView)mInfo.findViewById(0x60b006f)).setText(0x60c01f9);
        String s3 = bundle.getString("VERSION");
        ((TextView)mInfo.findViewById(0x60b0056)).setText(s3);
        s4 = bundle.getString("SIZE");
        String s6 = ResourceHelper.getFormattedSize(Integer.parseInt(s4));
        s4 = s6;
_L4:
        ((TextView)mInfo.findViewById(0x60b0065)).setText(s4);
        String s5 = bundle.getString("DOWNLOAD_COUNT");
        ((TextView)mInfo.findViewById(0x60b0064)).setText(s5);
        if(true) goto _L1; else goto _L3
_L3:
        Exception exception;
        exception;
          goto _L4
    }

    protected void setResourceStatus() {
        if(mOperationHandler == null) {
            mOperationView = (ResourceOperationView)findViewById(0x60b0059);
            mOperationHandler = getOperationHandler(mOperationView);
            mOperationView.init(mOperationHandler, getOperatorViewUIParameter());
        }
        mOperationHandler.refreshUI(getRealResourceState());
    }

    protected void setScreenViewBackground(int i) {
        mScreenViewNeedBackgroud = true;
        mScreenViewBackgroudId = i;
        bindScreenView();
    }

    protected void setupNavigationButton() {
        if(mHasActionBar) {
            LinearLayout linearlayout = new LinearLayout(this);
            linearlayout.setOrientation(0);
            linearlayout.setPadding(0, 0, (int)TypedValue.applyDimension(1, 10F, getResources().getDisplayMetrics()), 0);
            ImageView imageview = new ImageView(this);
            imageview.setImageResource(0x60200c8);
            linearlayout.addView(imageview, new android.view.ViewGroup.LayoutParams(-2, -2));
            mPreviousItem = imageview;
            ImageView imageview1 = new ImageView(this);
            imageview1.setImageResource(0x60200cc);
            linearlayout.addView(imageview1, new android.view.ViewGroup.LayoutParams(-2, -2));
            mNextItem = imageview1;
            getActionBar().setDisplayShowCustomEnabled(true);
            getActionBar().setCustomView(linearlayout, new android.app.ActionBar.LayoutParams(-2, -2, 21));
            initNavigationState();
        }
    }

    protected void setupUI() {
        mInflater = (LayoutInflater)getSystemService("layout_inflater");
        mCoverView = (ImageView)findViewById(0x60b007f);
        buildModeChangeAnimator();
        mPreview = (ResourceScreenView)findViewById(0x60b0052);
        mPreview.setOverScrollRatio(0.2F);
        mPreview.setOvershootTension(0.0F);
        mPreview.setScreenAlignment(2);
        mPreview.setScreenChangeListener(new miui.app.resourcebrowser.util.ResourceScreenView.ScreenChangeListener() {

            public void snapToScreen(int i) {
                initImageForScreenView(i);
            }

            final ResourceDetailActivity this$0;

             {
                this$0 = ResourceDetailActivity.this;
                super();
            }
        });
        android.widget.FrameLayout.LayoutParams layoutparams = new android.widget.FrameLayout.LayoutParams(-2, -2, 81);
        layoutparams.setMargins(0, 0, 0, 30);
        mPreview.setSeekBarPosition(layoutparams);
        Point point = new Point();
        getWindowManager().getDefaultDisplay().getSize(point);
        mPreviewWidth = ResourceHelper.getPreviewWidth(point.x);
        mMetaData.putInt("miui.app.resourcebrowser.PREVIEW_WIDTH", mPreviewWidth);
        mNormalParams = new android.view.ViewGroup.LayoutParams(mPreviewWidth, -1);
        if(mResourceSetCategory == 0) {
            View view = mInflater.inflate(0x603001b, null);
            mInfo = new FrameLayout(this);
            mInfo.setPadding(6, 0, 6, 60);
            mInfo.addView(view, mImageParams);
            mInfo.setLayoutParams(mNormalParams);
        }
    }

    protected void update() {
    }

    protected void updateNavigationState() {
        boolean flag = true;
        int i = ((DataGroup)mResourceSet.get(mResourceGroup)).size();
        View view = mPreviousItem;
        boolean flag1;
        View view1;
        if(mResourceIndex > 0)
            flag1 = flag;
        else
            flag1 = false;
        view.setEnabled(flag1);
        view1 = mNextItem;
        if(mResourceIndex >= i - 1)
            flag = false;
        view1.setEnabled(flag);
        if(mResourceIndex >= i - 5 && !mReachBottom && mIsOnlineResourceSet && !mIsSingleResourceSet)
            requestResources(i, 30);
    }

    private final String PREVIEW_INDEX = "PREVIEW_INDEX";
    protected BatchMediaPlayer mBatchPlayer;
    protected ImageView mCoverView;
    protected String mDetailFolder;
    protected Set mDownloadSet;
    protected boolean mFullScreen;
    private android.view.ViewGroup.LayoutParams mFullScreenParams;
    protected Handler mHandler;
    protected boolean mHasActionBar;
    private ImageCacheDecoder mImageCacheDecoder;
    private android.view.ViewGroup.LayoutParams mImageParams;
    protected LayoutInflater mInflater;
    protected FrameLayout mInfo;
    protected boolean mIsOnlineResourceSet;
    protected boolean mIsSingleResourceSet;
    protected String mListFolder;
    protected String mLocalPath;
    protected Bundle mMetaData;
    protected View mNextItem;
    private android.view.ViewGroup.LayoutParams mNormalParams;
    protected String mOnlinePath;
    protected ResourceOperationHandler mOperationHandler;
    protected ResourceOperationView mOperationView;
    protected ResourceDataParser mParser;
    protected ImageView mPlayButton;
    protected ResourceScreenView mPreview;
    private List mPreviewInfos;
    protected int mPreviewOffset;
    protected int mPreviewWidth;
    protected View mPreviousItem;
    protected boolean mReachBottom;
    protected int mResourceGroup;
    protected int mResourceIndex;
    protected ResourceSet mResourceSet;
    protected int mResourceSetCategory;
    protected String mResourceSetCode;
    protected String mResourceSetPackage;
    protected String mResourceSetSubpackage;
    protected TextView mRingtoneName;
    protected SDCardMonitor mSDCardMonitor;
    private int mScreenViewBackgroudId;
    private boolean mScreenViewNeedBackgroud;
    protected miui.widget.ScrollableScreenView.OnScrollOutListener mScrollOutListener;
    protected OnlineService mService;
    protected String mTitle;
    protected ObjectAnimator mToFullScreenModeAnimator;
    protected ObjectAnimator mToNormalModeAnimator;
    protected String mUrl;













}
