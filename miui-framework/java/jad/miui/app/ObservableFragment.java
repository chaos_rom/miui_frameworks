// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import java.util.*;

// Referenced classes of package miui.app:
//            FragmentLifecycleObserver

public class ObservableFragment extends Fragment {

    public ObservableFragment() {
        mObservers = new ArrayList();
    }

    public void addObserver(FragmentLifecycleObserver fragmentlifecycleobserver) {
        if(fragmentlifecycleobserver != null)
            mObservers.add(fragmentlifecycleobserver);
    }

    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        List list = mObservers;
        list;
        JVM INSTR monitorenter ;
        for(Iterator iterator = mObservers.iterator(); iterator.hasNext(); ((FragmentLifecycleObserver)iterator.next()).onActivityCreated(bundle));
        break MISSING_BLOCK_LABEL_57;
        Exception exception;
        exception;
        throw exception;
        list;
        JVM INSTR monitorexit ;
    }

    public void onActivityResult(int i, int j, Intent intent) {
        super.onActivityResult(i, j, intent);
        List list = mObservers;
        list;
        JVM INSTR monitorenter ;
        for(Iterator iterator = mObservers.iterator(); iterator.hasNext(); ((FragmentLifecycleObserver)iterator.next()).onActivityResult(i, j, intent));
        break MISSING_BLOCK_LABEL_66;
        Exception exception;
        exception;
        throw exception;
        list;
        JVM INSTR monitorexit ;
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        List list = mObservers;
        list;
        JVM INSTR monitorenter ;
        for(Iterator iterator = mObservers.iterator(); iterator.hasNext(); ((FragmentLifecycleObserver)iterator.next()).onAttach(activity));
        break MISSING_BLOCK_LABEL_57;
        Exception exception;
        exception;
        throw exception;
        list;
        JVM INSTR monitorexit ;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        List list = mObservers;
        list;
        JVM INSTR monitorenter ;
        for(Iterator iterator = mObservers.iterator(); iterator.hasNext(); ((FragmentLifecycleObserver)iterator.next()).onCreate(bundle));
        break MISSING_BLOCK_LABEL_57;
        Exception exception;
        exception;
        throw exception;
        list;
        JVM INSTR monitorexit ;
    }

    public void onDestroy() {
        super.onDestroy();
        List list = mObservers;
        list;
        JVM INSTR monitorenter ;
        for(Iterator iterator = mObservers.iterator(); iterator.hasNext(); ((FragmentLifecycleObserver)iterator.next()).onDestroy());
        break MISSING_BLOCK_LABEL_52;
        Exception exception;
        exception;
        throw exception;
        list;
        JVM INSTR monitorexit ;
    }

    public void onPause() {
        super.onPause();
        List list = mObservers;
        list;
        JVM INSTR monitorenter ;
        for(Iterator iterator = mObservers.iterator(); iterator.hasNext(); ((FragmentLifecycleObserver)iterator.next()).onPause());
        break MISSING_BLOCK_LABEL_52;
        Exception exception;
        exception;
        throw exception;
        list;
        JVM INSTR monitorexit ;
    }

    public void onResume() {
        super.onResume();
        List list = mObservers;
        list;
        JVM INSTR monitorenter ;
        for(Iterator iterator = mObservers.iterator(); iterator.hasNext(); ((FragmentLifecycleObserver)iterator.next()).onResume());
        break MISSING_BLOCK_LABEL_52;
        Exception exception;
        exception;
        throw exception;
        list;
        JVM INSTR monitorexit ;
    }

    public void onStart() {
        super.onStart();
        List list = mObservers;
        list;
        JVM INSTR monitorenter ;
        for(Iterator iterator = mObservers.iterator(); iterator.hasNext(); ((FragmentLifecycleObserver)iterator.next()).onStart());
        break MISSING_BLOCK_LABEL_52;
        Exception exception;
        exception;
        throw exception;
        list;
        JVM INSTR monitorexit ;
    }

    public void onStop() {
        super.onStop();
        List list = mObservers;
        list;
        JVM INSTR monitorenter ;
        for(Iterator iterator = mObservers.iterator(); iterator.hasNext(); ((FragmentLifecycleObserver)iterator.next()).onStop());
        break MISSING_BLOCK_LABEL_52;
        Exception exception;
        exception;
        throw exception;
        list;
        JVM INSTR monitorexit ;
    }

    public void removeObserver(FragmentLifecycleObserver fragmentlifecycleobserver) {
        if(fragmentlifecycleobserver != null)
            mObservers.remove(fragmentlifecycleobserver);
    }

    private List mObservers;
}
