// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app;

import android.accounts.Account;
import android.content.*;
import android.database.Cursor;
import android.net.Uri;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.util.Log;
import java.util.*;
import miui.provider.BatchOperation;

public class RemoveDuplicateContacts {
    public static class ContactsInfo {

        public int getCount() {
            return mCount;
        }

        public List getEmails() {
            return mEmails;
        }

        public String getName() {
            return mName;
        }

        public List getPhones() {
            return mPhones;
        }

        public long getPhotoId() {
            return mPhotoId;
        }

        public long getRawContactId() {
            return mRawContactId;
        }

        private int mCount;
        private List mEmails;
        private String mName;
        private List mPhones;
        private long mPhotoId;
        private long mRawContactId;

        public ContactsInfo(long l, String s, List list, List list1, long l1) {
            mPhotoId = l;
            if(TextUtils.isEmpty(s))
                s = "";
            mName = s;
            mPhones = list;
            mEmails = list1;
            mRawContactId = l1;
        }

        public ContactsInfo(RawContactData rawcontactdata, int i) {
            mPhotoId = rawcontactdata.mPhotoId;
            String s;
            if(TextUtils.isEmpty(rawcontactdata.mName))
                s = "";
            else
                s = rawcontactdata.mName;
            mName = s;
            mPhones = (List)rawcontactdata.getDatas().get("vnd.android.cursor.item/phone_v2");
            mCount = i;
            mRawContactId = rawcontactdata.getRawContactId();
            mEmails = (List)rawcontactdata.getDatas().get("vnd.android.cursor.item/email_v2");
        }
    }

    public static class MergeContacts {

        public ArrayList getContacts() {
            return mContacts;
        }

        public String getName() {
            return mName;
        }

        public boolean isChecked() {
            return mChecked;
        }

        public void setChecked(boolean flag) {
            mChecked = flag;
        }

        private boolean mChecked;
        private ArrayList mContacts;
        private String mName;

        public MergeContacts(ArrayList arraylist, String s) {
            mContacts = arraylist;
            mName = s;
            mChecked = true;
        }
    }

    public static class GroupsData {

        public long id;
        public String sourceid;
        public String title;

        public GroupsData() {
        }
    }

    public static class RawContactData {

        public void addData(String s, String s1) {
            if(s1 != null) goto _L2; else goto _L1
_L1:
            return;
_L2:
            List list = (List)mDatas.get(s);
            if(list == null) {
                ArrayList arraylist = new ArrayList();
                arraylist.add(s1);
                mDatas.put(s, arraylist);
            } else
            if(!list.contains(s1))
                list.add(s1);
            if(true) goto _L1; else goto _L3
_L3:
        }

        public boolean compare(RawContactData rawcontactdata) {
            if(rawcontactdata != null) goto _L2; else goto _L1
_L1:
            boolean flag = false;
_L12:
            return flag;
_L2:
            Iterator iterator;
            if(rawcontactdata.mDatas.size() != mDatas.size()) {
                flag = false;
                continue; /* Loop/switch isn't completed */
            }
            iterator = mDatas.keySet().iterator();
_L4:
            List list;
            ArrayList arraylist;
            Iterator iterator1;
            if(!iterator.hasNext())
                break MISSING_BLOCK_LABEL_289;
            String s = (String)iterator.next();
            if(!rawcontactdata.mDatas.containsKey(s)) {
                flag = false;
                continue; /* Loop/switch isn't completed */
            }
            list = (List)mDatas.get(s);
            arraylist = new ArrayList();
            arraylist.addAll((Collection)rawcontactdata.mDatas.get(s));
            if(list.size() != arraylist.size()) {
                flag = false;
                continue; /* Loop/switch isn't completed */
            }
            if(!"vnd.android.cursor.item/phone_v2".equals(s))
                continue; /* Loop/switch isn't completed */
            iterator1 = list.iterator();
_L9:
            if(!iterator1.hasNext()) goto _L4; else goto _L3
_L3:
            String s1;
            String s2;
            Iterator iterator2;
            s1 = (String)iterator1.next();
            s2 = null;
            iterator2 = arraylist.iterator();
_L6:
            String s3;
            if(iterator2.hasNext()) {
                s3 = (String)iterator2.next();
                if(!s1.equals(arraylist))
                    continue; /* Loop/switch isn't completed */
                s2 = s3;
            }
_L7:
            if(s2 == null) {
                flag = false;
                continue; /* Loop/switch isn't completed */
            }
            break; /* Loop/switch isn't completed */
            if(!PhoneNumberUtils.compare(s1, s3)) goto _L6; else goto _L5
_L5:
            s2 = s3;
              goto _L7
            if(true) goto _L6; else goto _L8
_L8:
            arraylist.remove(s2);
              goto _L9
            if(list.equals(arraylist)) goto _L4; else goto _L10
_L10:
            flag = false;
            continue; /* Loop/switch isn't completed */
            flag = true;
            if(true) goto _L12; else goto _L11
_L11:
        }

        public HashMap getDatas() {
            return mDatas;
        }

        public long getRawContactId() {
            return mRawContactId;
        }

        public boolean isDeleted() {
            return mDeleted;
        }

        public void setDeleted(boolean flag) {
            mDeleted = flag;
        }

        public void setRawContactId(long l) {
            mRawContactId = l;
        }

        public static final int HAS_DISPLAY_PHOTO = 100;
        public static final int HAS_PHOTO = 10;
        private HashMap mDatas;
        private boolean mDeleted;
        public int mN;
        public String mName;
        public long mPhotoId;
        public long mRawContactId;
        public String mSourceId;

        public RawContactData() {
            mDatas = new HashMap();
        }
    }

    public static interface RemoveDuplicateContactsListener {

        public abstract void onBegin(int i);

        public abstract void onEnd(boolean flag);

        public abstract void onProgress(int i);
    }


    public RemoveDuplicateContacts() {
    }

    public static List getDeletedRawContacts(Account account, ContentResolver contentresolver) {
        String as[] = new String[2];
        as[0] = account.name;
        as[1] = account.type;
        ArrayList arraylist = new ArrayList();
        HashMap hashmap = getNameWithRawContactIds(contentresolver, as);
        if(hashmap.size() > 0) {
            Iterator iterator = hashmap.keySet().iterator();
            do {
                if(!iterator.hasNext())
                    break;
                String s = (String)iterator.next();
                List list = (List)hashmap.get(s);
                if(list.size() >= 2) {
                    ArrayList arraylist1 = getNeedDeletedRawContacts(contentresolver, list, s);
                    if(arraylist1 != null)
                        arraylist.addAll(arraylist1);
                }
            } while(true);
        }
        return arraylist;
    }

    private static EntityIterator getEntityByIds(ContentResolver contentresolver, List list) {
        EntityIterator entityiterator = null;
        StringBuilder stringbuilder = new StringBuilder();
        Long long1;
        for(Iterator iterator = list.iterator(); iterator.hasNext(); stringbuilder.append((new StringBuilder()).append("_id=").append(long1).toString())) {
            long1 = (Long)iterator.next();
            if(stringbuilder.length() != 0)
                stringbuilder.append(" OR ");
        }

        Cursor cursor = contentresolver.query(CONTENT_URI, null, stringbuilder.toString(), null, null);
        if(cursor != null)
            entityiterator = android.provider.ContactsContract.RawContacts.newEntityIterator(cursor);
        return entityiterator;
    }

    private static List getGroups(Account account, ContentResolver contentresolver) {
        Cursor cursor;
        ArrayList arraylist;
        String as[] = new String[3];
        as[0] = "_id";
        as[1] = "title";
        as[2] = "sourceid";
        String as1[] = new String[2];
        as1[0] = account.name;
        as1[1] = account.type;
        cursor = contentresolver.query(android.provider.ContactsContract.Groups.CONTENT_URI, as, "account_name = ? AND account_type = ? ", as1, "title,sourceid DESC");
        arraylist = new ArrayList();
        if(cursor == null)
            break MISSING_BLOCK_LABEL_158;
        GroupsData groupsdata;
        for(; cursor.moveToNext(); arraylist.add(groupsdata)) {
            groupsdata = new GroupsData();
            groupsdata.id = cursor.getLong(0);
            groupsdata.title = cursor.getString(1);
            groupsdata.sourceid = cursor.getString(2);
        }

        break MISSING_BLOCK_LABEL_151;
        Exception exception;
        exception;
        cursor.close();
        throw exception;
        cursor.close();
        return arraylist;
    }

    public static ArrayList getMergeRawContacts(Account aaccount[], ContentResolver contentresolver) {
        ArrayList arraylist;
        int i;
        int j;
        arraylist = new ArrayList();
        i = aaccount.length;
        j = 0;
_L2:
        HashMap hashmap;
        if(j >= i)
            break MISSING_BLOCK_LABEL_435;
        Account account = aaccount[j];
        String as[] = new String[2];
        as[0] = account.name;
        as[1] = account.type;
        Log.d("RemoveDuplicateContacts", (new StringBuilder()).append("start scan raw_contact of account ").append(account.name).toString());
        hashmap = getNameWithRawContactIds(contentresolver, as);
        if(hashmap.size() != 0)
            break; /* Loop/switch isn't completed */
_L4:
        j++;
        if(true) goto _L2; else goto _L1
_L1:
        Iterator iterator = hashmap.keySet().iterator();
_L6:
        if(!iterator.hasNext()) goto _L4; else goto _L3
_L3:
        String s;
        List list;
        s = (String)iterator.next();
        list = (List)hashmap.get(s);
        if(list.size() < 2) goto _L6; else goto _L5
_L5:
        EntityIterator entityiterator;
        ArrayList arraylist1;
        entityiterator = getEntityByIds(contentresolver, list);
        arraylist1 = new ArrayList();
_L14:
        long l;
        long l1;
        ArrayList arraylist2;
        ArrayList arraylist3;
        Iterator iterator1;
        if(!entityiterator.hasNext())
            break; /* Loop/switch isn't completed */
        Entity entity = (Entity)entityiterator.next();
        l = entity.getEntityValues().getAsLong("_id").longValue();
        l1 = 0L;
        arraylist2 = new ArrayList();
        arraylist3 = new ArrayList();
        iterator1 = entity.getSubValues().iterator();
_L9:
        ContentValues contentvalues;
        String s1;
        if(!iterator1.hasNext())
            break MISSING_BLOCK_LABEL_374;
        contentvalues = ((android.content.Entity.NamedContentValues)iterator1.next()).values;
        s1 = contentvalues.getAsString("mimetype");
        if(!"vnd.android.cursor.item/photo".equals(s1)) goto _L8; else goto _L7
_L7:
        l1 = contentvalues.getAsLong("_id").longValue();
          goto _L9
_L8:
        if(!"vnd.android.cursor.item/phone_v2".equals(s1)) goto _L11; else goto _L10
_L10:
        arraylist2.add(contentvalues.getAsString("data1"));
          goto _L9
        Exception exception;
        exception;
        entityiterator.close();
        throw exception;
_L11:
        if(!"vnd.android.cursor.item/email_v2".equals(s1)) goto _L9; else goto _L12
_L12:
        arraylist3.add(contentvalues.getAsString("data1"));
          goto _L9
        arraylist1.add(new ContactsInfo(l1, s, arraylist2, arraylist3, l));
        if(true) goto _L14; else goto _L13
_L13:
        entityiterator.close();
        if(arraylist1.size() > 1)
            arraylist.add(new MergeContacts(arraylist1, s));
          goto _L6
          goto _L4
        return arraylist;
    }

    private static HashMap getNameWithRawContactIds(ContentResolver contentresolver, String as[]) {
        HashMap hashmap;
        HashMap hashmap1;
        Cursor cursor;
        hashmap = null;
        hashmap1 = new HashMap();
        Uri uri = android.provider.ContactsContract.RawContacts.CONTENT_URI;
        String as1[] = new String[2];
        as1[0] = "display_name";
        as1[1] = "_id";
        cursor = contentresolver.query(uri, as1, "deleted=0 AND account_name=? AND account_type=? AND data_set IS NULL ", as, null);
        if(cursor != null) goto _L2; else goto _L1
_L1:
        return hashmap;
_L2:
        long l;
        List list;
        if(!cursor.moveToNext())
            break; /* Loop/switch isn't completed */
        String s = cursor.getString(0);
        l = cursor.getLong(1);
        list = (List)hashmap1.get(s);
        if(list == null) {
            ArrayList arraylist = new ArrayList();
            arraylist.add(Long.valueOf(l));
            hashmap1.put(s, arraylist);
            continue; /* Loop/switch isn't completed */
        }
        break MISSING_BLOCK_LABEL_147;
        Exception exception;
        exception;
        cursor.close();
        throw exception;
        list.add(Long.valueOf(l));
        if(true) goto _L2; else goto _L3
_L3:
        cursor.close();
        hashmap = hashmap1;
        if(true) goto _L1; else goto _L4
_L4:
    }

    private static ArrayList getNeedDeletedRawContacts(ContentResolver contentresolver, List list, String s) {
        if(list != null && list.size() != 0) goto _L2; else goto _L1
_L1:
        ArrayList arraylist = null;
_L16:
        return arraylist;
_L2:
        EntityIterator entityiterator;
        ArrayList arraylist1;
        System.currentTimeMillis();
        entityiterator = getEntityByIds(contentresolver, list);
        arraylist1 = new ArrayList();
_L8:
        RawContactData rawcontactdata2;
        Iterator iterator;
        if(!entityiterator.hasNext())
            break; /* Loop/switch isn't completed */
        Entity entity = (Entity)entityiterator.next();
        rawcontactdata2 = new RawContactData();
        ContentValues contentvalues = entity.getEntityValues();
        rawcontactdata2.setRawContactId(contentvalues.getAsLong("_id").longValue());
        rawcontactdata2.mName = s;
        rawcontactdata2.mSourceId = contentvalues.getAsString("sourceid");
        iterator = entity.getSubValues().iterator();
_L5:
        ContentValues contentvalues1;
        String s1;
        if(!iterator.hasNext())
            break MISSING_BLOCK_LABEL_396;
        contentvalues1 = ((android.content.Entity.NamedContentValues)iterator.next()).values;
        s1 = contentvalues1.getAsString("mimetype");
        if(!"vnd.android.cursor.item/photo".equals(s1))
            break MISSING_BLOCK_LABEL_223;
        rawcontactdata2.mPhotoId = contentvalues1.getAsLong("_id").longValue();
        if(!contentvalues1.containsKey("data14")) goto _L4; else goto _L3
_L3:
        int i1 = 100;
_L6:
        rawcontactdata2.mN = i1;
          goto _L5
        Exception exception;
        exception;
        entityiterator.close();
        throw exception;
_L4:
        i1 = 10;
          goto _L6
        if("vnd.android.cursor.item/group_membership".equals(s1)) {
            long l = contentvalues1.getAsLong("data1").longValue();
            String s3 = (String)sGroups.get(Long.valueOf(l));
            if(s3 == null)
                s3 = "";
            rawcontactdata2.addData(s1, s3);
        } else
        if("vnd.android.cursor.item/im".equals(s1))
            rawcontactdata2.addData(s1, contentvalues1.getAsString("data1"));
        else
        if(!"vnd.android.cursor.item/name".equals(s1))
            if(sOtherMimeTypes.contains(s1)) {
                String s2 = contentvalues1.getAsString("data1");
                if(!TextUtils.isEmpty(s2))
                    rawcontactdata2.addData(s1, s2);
            } else {
                Log.d("RemoveDuplicateContacts", (new StringBuilder()).append("ignore unknown mimetype ").append(s1).toString());
            }
          goto _L5
        arraylist1.add(rawcontactdata2);
        if(true) goto _L8; else goto _L7
_L7:
        int i;
        int j;
        entityiterator.close();
        System.currentTimeMillis();
        i = arraylist1.size();
        if(i < 2) {
            arraylist = null;
            continue; /* Loop/switch isn't completed */
        }
        arraylist = new ArrayList();
        j = 0;
_L10:
        RawContactData rawcontactdata;
        if(j >= i - 1)
            break MISSING_BLOCK_LABEL_629;
        rawcontactdata = (RawContactData)arraylist1.get(j);
        if(!rawcontactdata.isDeleted())
            break; /* Loop/switch isn't completed */
_L12:
        j++;
        if(true) goto _L10; else goto _L9
_L9:
        int k = j + 1;
_L13:
        RawContactData rawcontactdata1;
        if(k >= i) goto _L12; else goto _L11
_L14:
        k++;
          goto _L13
_L11:
        rawcontactdata1 = (RawContactData)arraylist1.get(k);
        if(!rawcontactdata1.isDeleted() && rawcontactdata.compare(rawcontactdata1)) {
label0:
            {
                if(rawcontactdata.mN <= rawcontactdata1.mN)
                    break label0;
                rawcontactdata1.setDeleted(true);
                arraylist.add(rawcontactdata1);
            }
        }
        break MISSING_BLOCK_LABEL_515;
label1:
        {
            if(rawcontactdata.mN >= rawcontactdata1.mN)
                break label1;
            rawcontactdata.setDeleted(true);
            arraylist.add(rawcontactdata);
        }
          goto _L12
label2:
        {
            if(rawcontactdata1.mSourceId != null)
                break label2;
            rawcontactdata1.setDeleted(true);
            arraylist.add(rawcontactdata1);
        }
          goto _L14
        rawcontactdata.setDeleted(true);
        arraylist.add(rawcontactdata);
          goto _L12
        System.currentTimeMillis();
        if(true) goto _L16; else goto _L15
_L15:
    }

    /**
     * @deprecated Method remove is deprecated
     */

    public static int remove(Account aaccount[], ContentResolver contentresolver, RemoveDuplicateContactsListener removeduplicatecontactslistener, boolean flag) {
        miui/app/RemoveDuplicateContacts;
        JVM INSTR monitorenter ;
        if(aaccount != null && contentresolver != null) goto _L2; else goto _L1
_L1:
        int i = 0;
_L12:
        miui/app/RemoveDuplicateContacts;
        JVM INSTR monitorexit ;
        return i;
_L2:
        ArrayList arraylist;
        int j;
        int k;
        System.currentTimeMillis();
        i = 0;
        arraylist = new ArrayList();
        if(removeduplicatecontactslistener != null)
            removeduplicatecontactslistener.onBegin(0);
        j = aaccount.length;
        k = 0;
_L3:
        Account account;
        if(k >= j)
            break MISSING_BLOCK_LABEL_159;
        account = aaccount[k];
        sGroups.clear();
        GroupsData groupsdata;
        for(Iterator iterator1 = getGroups(account, contentresolver).iterator(); iterator1.hasNext(); sGroups.put(Long.valueOf(groupsdata.id), groupsdata.title))
            groupsdata = (GroupsData)iterator1.next();

        break MISSING_BLOCK_LABEL_139;
        Exception exception;
        exception;
        throw exception;
        arraylist.addAll(getDeletedRawContacts(account, contentresolver));
        k++;
          goto _L3
        if(arraylist == null || arraylist.size() <= 0) goto _L5; else goto _L4
_L4:
        BatchOperation batchoperation;
        Iterator iterator;
        batchoperation = new BatchOperation(contentresolver, "com.android.contacts");
        if(removeduplicatecontactslistener != null)
            removeduplicatecontactslistener.onBegin(arraylist.size());
        iterator = arraylist.iterator();
_L13:
        if(!iterator.hasNext()) goto _L7; else goto _L6
_L6:
        RawContactData rawcontactdata;
        Uri uri;
        ContentProviderOperation contentprovideroperation;
        rawcontactdata = (RawContactData)iterator.next();
        uri = ContentUris.withAppendedId(android.provider.ContactsContract.RawContacts.CONTENT_URI, rawcontactdata.mRawContactId);
        contentprovideroperation = null;
        if(rawcontactdata.mSourceId != null) goto _L9; else goto _L8
_L8:
        contentprovideroperation = ContentProviderOperation.newDelete(uri.buildUpon().appendQueryParameter("caller_is_syncadapter", "true").build()).build();
_L11:
        if(contentprovideroperation != null)
            batchoperation.add(contentprovideroperation);
        if(batchoperation.size() > 100)
            batchoperation.execute();
        if(removeduplicatecontactslistener != null)
            removeduplicatecontactslistener.onProgress(i);
        break; /* Loop/switch isn't completed */
_L9:
        if(!flag)
            contentprovideroperation = ContentProviderOperation.newDelete(uri).build();
        if(true) goto _L11; else goto _L10
_L7:
        if(batchoperation.size() > 0)
            batchoperation.execute();
_L5:
        if(removeduplicatecontactslistener != null)
            removeduplicatecontactslistener.onEnd(true);
        System.currentTimeMillis();
          goto _L12
_L10:
        i++;
          goto _L13
    }

    public static void removeGroups(Account account, ContentResolver contentresolver) {
        List list = getGroups(account, contentresolver);
        if(list.size() > 1) goto _L2; else goto _L1
_L1:
        return;
_L2:
        HashSet hashset;
        HashSet hashset1;
        String as[];
        Iterator iterator;
        long l = 0L;
        int i = 0;
        do {
            int j = list.size();
            if(i >= j)
                break;
            if(i == 0) {
                l = ((GroupsData)list.get(i)).id;
            } else {
                String s = ((GroupsData)list.get(i - 1)).title;
                String s1 = ((GroupsData)list.get(i)).title;
                long l3 = ((GroupsData)list.get(i)).id;
                String s2 = ((GroupsData)list.get(i)).sourceid;
                if(TextUtils.equals(s, s1)) {
                    String as2[] = new String[2];
                    as2[0] = "vnd.android.cursor.item/group_membership";
                    as2[1] = String.valueOf(l3);
                    ContentValues contentvalues = new ContentValues();
                    contentvalues.put("data1", Long.valueOf(l));
                    contentresolver.update(android.provider.ContactsContract.Data.CONTENT_URI, contentvalues, "mimetype=? AND data1=?", as2);
                    Log.d("RemoveDuplicateContacts", (new StringBuilder()).append("update contacts group from ").append(l3).append(" to ").append(l).toString());
                    Uri uri;
                    if(s2 == null)
                        uri = ContentUris.withAppendedId(android.provider.ContactsContract.Groups.CONTENT_URI, l3).buildUpon().appendQueryParameter("caller_is_syncadapter", "true").build();
                    else
                        uri = ContentUris.withAppendedId(android.provider.ContactsContract.Groups.CONTENT_URI, l3);
                    contentresolver.delete(uri, null, null);
                    Log.d("RemoveDuplicateContacts", (new StringBuilder()).append("delete group ").append(l3).toString());
                } else {
                    l = ((GroupsData)list.get(i)).id;
                }
            }
            i++;
        } while(true);
        List list1 = getGroups(account, contentresolver);
        hashset = new HashSet();
        hashset1 = new HashSet();
        as = new String[2];
        as[0] = "_id";
        as[1] = "raw_contact_id";
        iterator = list1.iterator();
_L6:
        Cursor cursor;
        if(!iterator.hasNext())
            break; /* Loop/switch isn't completed */
        GroupsData groupsdata = (GroupsData)iterator.next();
        hashset.clear();
        String as1[] = new String[2];
        as1[0] = "vnd.android.cursor.item/group_membership";
        as1[1] = String.valueOf(groupsdata.id);
        cursor = contentresolver.query(android.provider.ContactsContract.Data.CONTENT_URI, as, "mimetype=? AND data1=?", as1, null);
        if(cursor == null)
            continue; /* Loop/switch isn't completed */
_L4:
        long l2;
        if(!cursor.moveToNext())
            break; /* Loop/switch isn't completed */
        long l1 = cursor.getLong(0);
        l2 = cursor.getLong(1);
        if(hashset.contains(Long.valueOf(l2))) {
            hashset1.add(Long.valueOf(l1));
            continue; /* Loop/switch isn't completed */
        }
        break MISSING_BLOCK_LABEL_530;
        Exception exception;
        exception;
        cursor.close();
        throw exception;
        hashset.add(Long.valueOf(l2));
        if(true) goto _L4; else goto _L3
_L3:
        cursor.close();
        if(true) goto _L6; else goto _L5
_L5:
        BatchOperation batchoperation = new BatchOperation(contentresolver, "com.android.contacts");
        Iterator iterator1 = hashset1.iterator();
        do {
            if(!iterator1.hasNext())
                break;
            Long long1 = (Long)iterator1.next();
            batchoperation.add(ContentProviderOperation.newDelete(ContentUris.withAppendedId(android.provider.ContactsContract.Data.CONTENT_URI, long1.longValue())).build());
            if(batchoperation.size() > 100)
                batchoperation.execute();
        } while(true);
        if(batchoperation.size() > 0)
            batchoperation.execute();
        if(true) goto _L1; else goto _L7
_L7:
    }

    private static final Uri CONTENT_URI;
    private static final boolean DBG = false;
    private static final String NAME_SELECTION = "deleted=0 AND account_name=? AND account_type=? AND data_set IS NULL ";
    public static final String TAG = "RemoveDuplicateContacts";
    private static final HashMap sGroups = new HashMap();
    private static final HashSet sOtherMimeTypes;

    static  {
        CONTENT_URI = android.provider.ContactsContract.RawContactsEntity.CONTENT_URI;
        sOtherMimeTypes = new HashSet();
        sOtherMimeTypes.add("vnd.android.cursor.item/phone_v2");
        sOtherMimeTypes.add("vnd.android.cursor.item/email_v2");
        sOtherMimeTypes.add("vnd.android.cursor.item/postal-address_v2");
        sOtherMimeTypes.add("vnd.android.cursor.item/organization");
        sOtherMimeTypes.add("vnd.android.cursor.item/website");
        sOtherMimeTypes.add("vnd.android.cursor.item/contact_event");
        sOtherMimeTypes.add("vnd.android.cursor.item/sip_address");
        sOtherMimeTypes.add("vnd.android.cursor.item/relation");
        sOtherMimeTypes.add("vnd.android.cursor.item/note");
        sOtherMimeTypes.add("vnd.android.cursor.item/nickname");
        sOtherMimeTypes.add("vnd.com.miui.cursor.item/gender");
        sOtherMimeTypes.add("vnd.com.miui.cursor.item/bloodType");
        sOtherMimeTypes.add("vnd.com.miui.cursor.item/constellation");
        sOtherMimeTypes.add("vnd.com.miui.cursor.item/animalSign");
        sOtherMimeTypes.add("vnd.com.miui.cursor.item/emotionStatus");
        sOtherMimeTypes.add("vnd.com.miui.cursor.item/interest");
        sOtherMimeTypes.add("vnd.com.miui.cursor.item/hobby");
        sOtherMimeTypes.add("vnd.com.miui.cursor.item/degree");
        sOtherMimeTypes.add("vnd.com.miui.cursor.item/schools");
        sOtherMimeTypes.add("vnd.com.miui.cursor.item/characteristic");
        sOtherMimeTypes.add("vnd.com.miui.cursor.item/xiaomiId");
        sOtherMimeTypes.add("vnd.com.miui.cursor.item/lunarBirthday");
    }
}
