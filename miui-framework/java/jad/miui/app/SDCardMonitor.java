// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.app;

import android.content.*;
import java.util.HashSet;
import java.util.Iterator;
import miui.os.Environment;

public class SDCardMonitor {
    public static interface SDCardStatusListener {

        public abstract void onStatusChanged(boolean flag);
    }

    private class SDCardReceiver extends BroadcastReceiver {

        public void onReceive(Context context, Intent intent) {
            if(Environment.isExternalStorageStateChanged(intent)) goto _L2; else goto _L1
_L1:
            return;
_L2:
            boolean flag = Environment.isExternalStorageMounted();
            if(mIsMount == null || mIsMount.booleanValue() != flag) {
                mIsMount = Boolean.valueOf(flag);
                Iterator iterator = mListeners.iterator();
                while(iterator.hasNext()) 
                    ((SDCardStatusListener)iterator.next()).onStatusChanged(mIsMount.booleanValue());
            }
            if(true) goto _L1; else goto _L3
_L3:
        }

        final SDCardMonitor this$0;

        private SDCardReceiver() {
            this$0 = SDCardMonitor.this;
            super();
        }

    }


    private SDCardMonitor(Context context) {
        mIsMount = null;
        mContext = context;
        mReceiver = new SDCardReceiver();
        mListeners = new HashSet();
    }

    public static SDCardMonitor getSDCardMonitor(Context context) {
        if(mMonitor == null)
            mMonitor = new SDCardMonitor(context.getApplicationContext());
        return mMonitor;
    }

    public static boolean isSdCardAvailable() {
        return Environment.isExternalStorageMounted();
    }

    public void addListener(SDCardStatusListener sdcardstatuslistener) {
        if(mListeners.isEmpty()) {
            IntentFilter intentfilter = new IntentFilter();
            intentfilter.addAction("android.intent.action.MEDIA_SHARED");
            intentfilter.addAction("android.intent.action.MEDIA_MOUNTED");
            intentfilter.addAction("android.intent.action.MEDIA_UNMOUNTED");
            intentfilter.addAction("android.intent.action.MEDIA_BAD_REMOVAL");
            intentfilter.addDataScheme("file");
            mContext.registerReceiver(mReceiver, intentfilter);
        }
        mListeners.add(sdcardstatuslistener);
    }

    public void removeListener(SDCardStatusListener sdcardstatuslistener) {
        mListeners.remove(sdcardstatuslistener);
        if(mListeners.isEmpty())
            mContext.unregisterReceiver(mReceiver);
    }

    private static SDCardMonitor mMonitor;
    private Context mContext;
    private Boolean mIsMount;
    private HashSet mListeners;
    protected BroadcastReceiver mReceiver;



/*
    static Boolean access$102(SDCardMonitor sdcardmonitor, Boolean boolean1) {
        sdcardmonitor.mIsMount = boolean1;
        return boolean1;
    }

*/

}
