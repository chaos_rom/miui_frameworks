// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.push;

import android.os.Bundle;
import android.text.TextUtils;
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

// Referenced classes of package miui.push:
//            StringUtils, CommonPacketExtension, XMPPError, PacketExtension

public abstract class Packet {

    public Packet() {
        xmlns = DEFAULT_XML_NS;
        packetID = null;
        to = null;
        from = null;
        chId = null;
        packetExtensions = new CopyOnWriteArrayList();
        properties = new HashMap();
        error = null;
    }

    public Packet(Bundle bundle) {
        xmlns = DEFAULT_XML_NS;
        packetID = null;
        to = null;
        from = null;
        chId = null;
        packetExtensions = new CopyOnWriteArrayList();
        properties = new HashMap();
        error = null;
        to = bundle.getString("ext_to");
        from = bundle.getString("ext_from");
        chId = bundle.getString("ext_chid");
        packetID = bundle.getString("ext_pkt_id");
        android.os.Parcelable aparcelable[] = bundle.getParcelableArray("ext_exts");
        if(aparcelable != null) {
            packetExtensions = new ArrayList(aparcelable.length);
            int i = aparcelable.length;
            for(int j = 0; j < i; j++) {
                CommonPacketExtension commonpacketextension = CommonPacketExtension.parseFromBundle((Bundle)aparcelable[j]);
                if(commonpacketextension != null)
                    packetExtensions.add(commonpacketextension);
            }

        }
        Bundle bundle1 = bundle.getBundle("ext_ERROR");
        if(bundle1 != null)
            error = new XMPPError(bundle1);
    }

    public static String getDefaultLanguage() {
        return DEFAULT_LANGUAGE;
    }

    /**
     * @deprecated Method nextID is deprecated
     */

    public static String nextID() {
        miui/push/Packet;
        JVM INSTR monitorenter ;
        String s;
        StringBuilder stringbuilder = (new StringBuilder()).append(prefix);
        long l = id;
        id = 1L + l;
        s = stringbuilder.append(Long.toString(l)).toString();
        miui/push/Packet;
        JVM INSTR monitorexit ;
        return s;
        Exception exception;
        exception;
        throw exception;
    }

    public static void setDefaultXmlns(String s) {
        DEFAULT_XML_NS = s;
    }

    public void addExtension(CommonPacketExtension commonpacketextension) {
        packetExtensions.add(commonpacketextension);
    }

    /**
     * @deprecated Method deleteProperty is deprecated
     */

    public void deleteProperty(String s) {
        this;
        JVM INSTR monitorenter ;
        Map map = properties;
        if(map != null) goto _L2; else goto _L1
_L1:
        this;
        JVM INSTR monitorexit ;
        return;
_L2:
        properties.remove(s);
        if(true) goto _L1; else goto _L3
_L3:
        Exception exception;
        exception;
        throw exception;
    }

    public boolean equals(Object obj) {
        boolean flag;
        boolean flag1;
        flag = true;
        flag1 = false;
        if(this != obj) goto _L2; else goto _L1
_L1:
        flag1 = flag;
_L4:
        return flag1;
_L2:
        if(obj != null && getClass() == obj.getClass()) {
            Packet packet = (Packet)obj;
            if((error == null ? packet.error == null : error.equals(packet.error)) && (from == null ? packet.from == null : from.equals(packet.from)) && packetExtensions.equals(packet.packetExtensions) && (packetID == null ? packet.packetID == null : packetID.equals(packet.packetID)) && (chId == null ? packet.chId == null : chId.equals(packet.chId)) && (properties == null ? packet.properties == null : properties.equals(packet.properties)) && (to == null ? packet.to == null : to.equals(packet.to))) {
                if(xmlns == null ? pack