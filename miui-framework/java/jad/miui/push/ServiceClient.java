// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.push;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import java.util.Iterator;
import java.util.List;
import org.apache.http.NameValuePair;

// Referenced classes of package miui.push:
//            PushConstants, IQ, Message, Presence

public class ServiceClient {

    private ServiceClient(Context context) {
        mContext = context.getApplicationContext();
    }

    private Intent createServiceIntent() {
        Intent intent = new Intent();
        intent.setPackage("com.xiaomi.xmsf");
        intent.setClassName("com.xiaomi.xmsf", "com.xiaomi.xmsf.push.service.XMPushService");
        String s = mContext.getPackageName();
        intent.putExtra(PushConstants.EXTRA_PACKAGE_NAME, s);
        return intent;
    }

    public static ServiceClient getInstance(Context context) {
        if(sInstance == null)
            sInstance = new ServiceClient(context);
        return sInstance;
    }

    private boolean hasNetwork() {
        int i = -1;
        ConnectivityManager connectivitymanager = (ConnectivityManager)mContext.getSystemService("connectivity");
        if(connectivitymanager != null) {
            NetworkInfo networkinfo = connectivitymanager.getActiveNetworkInfo();
            if(networkinfo != null)
                i = networkinfo.getType();
        }
        boolean flag;
        if(i >= 0)
            flag = true;
        else
            flag = false;
        return flag;
    }

    private boolean serviceInstalled() {
label0:
        {
            boolean flag = false;
            PackageManager packagemanager = mContext.getPackageManager();
            android.content.pm.PackageInfo packageinfo;
            try {
                packageinfo = packagemanager.getPackageInfo("com.xiaomi.xmsf", 4);
            }
            catch(android.content.pm.PackageManager.NameNotFoundException namenotfoundexception) {
                break label0;
            }
            if(packageinfo != null)
                flag = true;
        }
        return flag;
    }

    public boolean closeChannel() {
        boolean flag;
        if(!serviceInstalled()) {
            flag = false;
        } else {
            Intent intent = createServiceIntent();
            intent.setAction(PushConstants.ACTION_CLOSE_CHANNEL);
            mContext.startService(intent);
            flag = true;
        }
        return flag;
    }

    public boolean closeChannel(String s) {
        boolean flag;
        if(!serviceInstalled()) {
            flag = false;
        } else {
            Intent intent = createServiceIntent();
            intent.setAction(PushConstants.ACTION_CLOSE_CHANNEL);
            intent.putExtra(PushConstants.EXTRA_CHANNEL_ID, s);
            mContext.startService(intent);
            flag = true;
        }
        return flag;
    }

    public boolean forceReconnection() {
        boolean flag;
        if(!serviceInstalled() || !hasNetwork()) {
            flag = false;
        } else {
            Intent intent = createServiceIntent();
            intent.setAction(PushConstants.ACTION_FORCE_RECONNECT);
            mContext.startService(intent);
            flag = true;
        }
        return flag;
    }

    public int openChannel(String s, String s1, String s2, String s3, String s4, boolean flag, List list, 
            List list1) {
        int k;
        if(!serviceInstalled()) {
            k = 1;
        } else {
            Intent intent = createServiceIntent();
            intent.setAction(PushConstants.ACTION_OPEN_CHANNEL);
            intent.putExtra(PushConstants.EXTRA_USER_ID, s);
            intent.putExtra(PushConstants.EXTRA_CHANNEL_ID, s1);
            intent.putExtra(PushConstants.EXTRA_TOKEN, s2);
            intent.putExtra(PushConstants.EXTRA_SECURITY, s4);
            intent.putExtra(PushConstants.EXTRA_AUTH_METHOD, s3);
            intent.putExtra(PushConstants.EXTRA_KICK, flag);
            if(list != null) {
                StringBuilder stringbuilder = new StringBuilder();
                int i = 1;
                for(Iterator iterator = list.iterator(); iterator.hasNext();) {
                    NameValuePair namevaluepair1 = (NameValuePair)iterator.next();
                    stringbuilder.append(namevaluepair1.getName()).append(":").append(namevaluepair1.getValue());
                    if(i < list.size())
                        stringbuilder.append(",");
                    i++;
                }

                if(!TextUtils.isEmpty(stringbuilder))
                    intent.putExtra(PushConstants.EXTRA_CLIENT_ATTR, stringbuilder.toString());
            }
            if(list1 != null) {
                StringBuilder stringbuilder1 = new StringBuilder();
                int j = 1;
                for(Iterator iterator1 = list1.iterator(); iterator1.hasNext();) {
                    NameValuePair namevaluepair = (NameValuePair)iterator1.next();
                    stringbuilder1.append(namevaluepair.getName()).append(":").append(namevaluepair.getValue());
                    if(j < list1.size())
                        stringbuilder1.append(",");
                    j++;
                }

                if(!TextUtils.isEmpty(stringbuilder1))
                    intent.putExtra(PushConstants.EXTRA_CLOUD_ATTR, stringbuilder1.toString());
            }
            mContext.startService(intent);
            k = 0;
        }
        return k;
    }

    public void resetConnection() {
        if(serviceInstalled()) {
            Intent intent = createServiceIntent();
            intent.setAction(PushConstants.ACTION_RESET_CONNECTION);
            mContext.startService(intent);
        }
    }

    public boolean sendIQ(IQ iq) {
        boolean flag = false;
        if(serviceInstalled() && hasNetwork()) goto _L2; else goto _L1
_L1:
        return flag;
_L2:
        Intent intent = createServiceIntent();
        android.os.Bundle bundle = iq.toBundle();
        if(bundle != null) {
            intent.setAction(PushConstants.ACTION_SEND_IQ);
            intent.putExtra("ext_packet", bundle);
            mContext.startService(intent);
            flag = true;
        }
        if(true) goto _L1; else goto _L3
_L3:
    }

    public boolean sendMessage(Message message) {
        boolean flag = false;
        if(serviceInstalled() && hasNetwork()) goto _L2; else goto _L1
_L1:
        return flag;
_L2:
        Intent intent = createServiceIntent();
        android.os.Bundle bundle = message.toBundle();
        if(bundle != null) {
            intent.setAction(PushConstants.ACTION_SEND_MESSAGE);
            intent.putExtra("ext_packet", bundle);
            mContext.startService(intent);
            flag = true;
        }
        if(true) goto _L1; else goto _L3
_L3:
    }

    public boolean sendPresence(Presence presence) {
        boolean flag = false;
        if(serviceInstalled() && hasNetwork()) goto _L2; else goto _L1
_L1:
        return flag;
_L2:
        Intent intent = createServiceIntent();
        android.os.Bundle bundle = presence.toBundle();
        if(bundle != null) {
            intent.setAction(PushConstants.ACTION_SEND_PRESENCE);
            intent.putExtra("ext_packet", bundle);
            mContext.startService(intent);
            flag = true;
        }
        if(true) goto _L1; else goto _L3
_L3:
    }

    private static ServiceClient sInstance;
    private Context mContext;
}
