// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.push;

import android.os.Bundle;
import android.text.TextUtils;

// Referenced classes of package miui.push:
//            Packet, StringUtils, XMPPError

public class Message extends Packet {

    public Message() {
        type = null;
        thread = null;
        mTransient = false;
        mEncrypted = false;
    }

    public Message(Bundle bundle) {
        super(bundle);
        type = null;
        thread = null;
        mTransient = false;
        mEncrypted = false;
        type = bundle.getString("ext_msg_type");
        language = bundle.getString("ext_msg_lang");
        thread = bundle.getString("ext_msg_thread");
        mSubject = bundle.getString("ext_msg_sub");
        mBody = bundle.getString("ext_msg_body");
        mAppId = bundle.getString("ext_msg_appid");
        mTransient = bundle.getBoolean("ext_msg_trans", false);
    }

    public Message(String s) {
        type = null;
        thread = null;
        mTransient = false;
        mEncrypted = false;
        setTo(s);
    }

    public Message(String s, String s1) {
        type = null;
        thread = null;
        mTransient = false;
        mEncrypted = false;
        setTo(s);
        type = s1;
    }

    public boolean equals(Object obj) {
        boolean flag;
        boolean flag1;
        flag = true;
        flag1 = false;
        if(this != obj) goto _L2; else goto _L1
_L1:
        flag1 = flag;
_L4:
        return flag1;
_L2:
        if(obj != null && getClass() == obj.getClass()) {
            Message message = (Message)obj;
            if(super.equals(message) && (mBody == null ? message.mBody == null : mBody.equals(message.mBody)) && (language == null ? message.language == null : language.equals(message.language)) && (mSubject == null ? message.mSubject == null : mSubject.equals(message.mSubject)) && (thread == null ? message.thread == null : thread.equals(message.thread))) {
                if(type != message.type)
                    flag = false;
                flag1 = flag;
            }
        }
        if(true) goto _L4; else goto _L3
_L3:
    }

    public String getAppId() {
        return mAppId;
    }

    public String getBody() {
        return mBody;
    }

    public boolean getEncrypted() {
        return mEncrypted;
    }

    public String getLanguage() {
        return language;
    }

    public String getSubject() {
        return mSubject;
    }

    public String getThread() {
        return thread;
    }

    public String getType() {
        return type;
    }

    public int hashCode() {
        int i = 0;
        int j;
        int k;
        int l;
        int i1;
        int j1;
        int k1;
        int l1;
        int i2;
        if(type != null)
            j = type.hashCode();
        else
            j = 0;
        k = j * 31;
        if(mBody != null)
            l = mBody.hashCode();
        else
            l = 0;
        i1 = 31 * (k + l);
        if(thread != null)
            j1 = thread.hashCode();
        else
            j1 = 0;
        k1 = 31 * (i1 + j1);
        if(language != null)
            l1 = language.hashCode();
        else
            l1 = 0;
        i2 = 31 * (k1 + l1);
        if(mSubject != null)
            i = mSubject.hashCode();
        return i2 + i;
    }

    public void setAppId(String s) {
        mAppId = s;
    }

    public void setBody(String s) {
        mBody = s;
    }

    public void setEncrypted(boolean flag) {
        mEncrypted = flag;
    }

    public void setIsTransient(boolean flag) {
        mTransient = flag;
    }

    public void setLanguage(String s) {
        language = s;
    }

    public void setSubject(String s) {
        mSubject = s;
    }

    public void setThread(String s) {
        thread = s;
    }

    public void setType(String s) {
        type = s;
    }

    public Bundle toBundle() {
        Bundle bundle = super.toBundle();
        if(!TextUtils.isEmpty(type))
            bundle.putString("ext_msg_type", type);
        if(language != null)
            bundle.putString("ext_msg_lang", language);
        if(mSubject != null)
            bundle.putString("ext_msg_sub", mSubject);
        if(mBody != null)
            bundle.putString("ext_msg_body", mBody);
        if(thread != null)
            bundle.putString("ext_msg_thread", thread);
        if(mAppId != null)
            bundle.putString("ext_msg_appid", mAppId);
        if(mTransient)
            bundle.putBoolean("ext_msg_trans", true);
        return bundle;
    }

    public String toXML() {
        StringBuilder stringbuilder = new StringBuilder();
        stringbuilder.append("<message");
        if(getXmlns() != null)
            stringbuilder.append(" xmlns=\"").append(getXmlns()).append("\"");
        if(language != null)
            stringbuilder.append(" xml:lang=\"").append(getLanguage()).append("\"");
        if(getPacketID() != null)
            stringbuilder.append(" id=\"").append(getPacketID()).append("\"");
        if(getTo() != null)
            stringbuilder.append(" to=\"").append(StringUtils.escapeForXML(getTo())).append("\"");
        if(getFrom() != null)
            stringbuilder.append(" from=\"").append(StringUtils.escapeForXML(getFrom())).append("\"");
        if(getChannelId() != null)
            stringbuilder.append(" chid=\"").append(StringUtils.escapeForXML(getChannelId())).append("\"");
        if(mTransient)
            stringbuilder.append(" transient=\"true\"");
        if(!TextUtils.isEmpty(mAppId))
            stringbuilder.append(" appid=\"").append(getAppId()).append("\"");
        if(!TextUtils.isEmpty(type))
            stringbuilder.append(" type=\"").append(type).append("\"");
        if(mEncrypted)
            stringbuilder.append(" s=\"1\"");
        stringbuilder.append(">");
        if(mSubject != null) {
            stringbuilder.append("<subject>").append(StringUtils.escapeForXML(mSubject));
            stringbuilder.append("</subject>");
        }
        if(mBody != null)
            stringbuilder.append("<body>").append(StringUtils.escapeForXML(mBody)).append("</body>");
        if(thread != null)
            stringbuilder.append("<thread>").append(thread).append("</thread>");
        if("error".equalsIgnoreCase(type)) {
            XMPPError xmpperror = getError();
            if(xmpperror != null)
                stringbuilder.append(xmpperror.toXML());
        }
        stringbuilder.append(getExtensionsXML());
        stringbuilder.append("</message>");
        return stringbuilder.toString();
    }

    public static final String MSG_TYPE_CHAT = "chat";
    public static final String MSG_TYPE_ERROR = "error";
    public static final String MSG_TYPE_GROUPCHAT = "groupchat";
    public static final String MSG_TYPE_HEADLINE = "hearline";
    public static final String MSG_TYPE_NORMAL = "normal";
    private String language;
    private String mAppId;
    private String mBody;
    private boolean mEncrypted;
    private String mSubject;
    private boolean mTransient;
    private String thread;
    private String type;
}
