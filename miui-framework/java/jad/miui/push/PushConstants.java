// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.push;


public abstract class PushConstants {

    public PushConstants() {
    }

    public static final String ACTION_CHANNEL_CLOSED = "com.xiaomi.push.channel_closed";
    public static final String ACTION_CHANNEL_OPENED = "com.xiaomi.push.channel_opened";
    public static String ACTION_CLOSE_CHANNEL = "com.xiaomi.push.CLOSE_CHANNEL";
    public static String ACTION_FORCE_RECONNECT = "com.xiaomi.push.FORCE_RECONN";
    public static final String ACTION_KICKED_BY_SERVER = "com.xiaomi.push.kicked";
    public static String ACTION_OPEN_CHANNEL = "com.xiaomi.push.OPEN_CHANNEL";
    public static final String ACTION_RECEIVE_NEW_IQ = "com.xiaomi.push.new_iq";
    public static final String ACTION_RECEIVE_NEW_MESSAGE = "com.xiaomi.push.new_msg";
    public static final String ACTION_RECEIVE_NEW_PRESENCE = "com.xiaomi.push.new_pres";
    public static String ACTION_RESET_CONNECTION = "com.xiaomi.push.RESET_CONN";
    public static String ACTION_SEND_IQ = "com.xiaomi.push.SEND_IQ";
    public static String ACTION_SEND_MESSAGE = "com.xiaomi.push.SEND_MESSAGE";
    public static String ACTION_SEND_PRESENCE = "com.xiaomi.push.SEND_PRES";
    public static final String ACTION_SERVICE_STARTED = "com.xiaomi.push.service_started";
    public static final int ERROR_ACCESS_DENIED = 4;
    public static final int ERROR_AUTH_FAILED = 5;
    public static final int ERROR_MULTI_LOGIN = 6;
    public static final int ERROR_NETWORK_FAILED = 3;
    public static final int ERROR_NETWORK_NOT_AVAILABLE = 2;
    public static final int ERROR_OK = 0;
    public static final int ERROR_SERVER_ERROR = 7;
    public static final int ERROR_SERVICE_NOT_INSTALLED = 1;
    public static String EXTRA_AUTH_METHOD = "ext_auth_method";
    public static String EXTRA_CHANNEL_ID = "ext_chid";
    public static final String EXTRA_CHID = "ext_chid";
    public static String EXTRA_CLIENT_ATTR = "ext_client_attr";
    public static String EXTRA_CLOUD_ATTR = "ext_cloud_attr";
    public static final String EXTRA_ERROR = "ext_ERROR";
    public static final String EXTRA_ERROR_CODE = "ext_err_code";
    public static final String EXTRA_ERROR_CONDITION = "ext_err_cond";
    public static final String EXTRA_ERROR_MESSAGE = "ext_err_msg";
    public static final String EXTRA_ERROR_REASON = "ext_err_reason";
    public static final String EXTRA_ERROR_TYPE = "ext_err_type";
    public static final String EXTRA_EXTENSIONS = "ext_exts";
    public static final String EXTRA_EXTENSION_ELEMENT_NAME = "ext_ele_name";
    public static final String EXTRA_EXTENSION_NAMESPACE = "ext_ns";
    public static final String EXTRA_EXTENSION_TEXT = "ext_text";
    public static final String EXTRA_FROM = "ext_from";
    public static final String EXTRA_IQ_TYPE = "ext_iq_type";
    public static String EXTRA_KICK = "ext_kick";
    public static final String EXTRA_KICK_REASON = "ext_kick_reason";
    public static final String EXTRA_KICK_TYPE = "ext_kick_type";
    public static final String EXTRA_MESSAGE_APPID = "ext_msg_appid";
    public static final String EXTRA_MESSAGE_BODY = "ext_msg_body";
    public static final String EXTRA_MESSAGE_LANGUAGE = "ext_msg_lang";
    public static final String EXTRA_MESSAGE_SUBJECT = "ext_msg_sub";
    public static final String EXTRA_MESSAGE_THREAD = "ext_msg_thread";
    public static final String EXTRA_MESSAGE_TRANSIENT = "ext_msg_trans";
    public static final String EXTRA_MESSAGE_TYPE = "ext_msg_type";
    public static String EXTRA_PACKAGE_NAME = "ext_pkg_name";
    public static final String EXTRA_PACKET = "ext_packet";
    public static final String EXTRA_PACKET_ID = "ext_pkt_id";
    public static final String EXTRA_PRES_MODE = "ext_pres_mode";
    public static final String EXTRA_PRES_PRIORITY = "ext_pres_prio";
    public static final String EXTRA_PRES_STATUS = "ext_pres_status";
    public static final String EXTRA_PRES_TYPE = "ext_pres_type";
    public static final String EXTRA_REASON = "ext_reason";
    public static final String EXTRA_REASON_MSG = "ext_reason_msg";
    public static String EXTRA_SECURITY = "ext_security";
    public static final String EXTRA_SUCCEEDED = "ext_succeeded";
    public static final String EXTRA_TO = "ext_to";
    public static String EXTRA_TOKEN = "ext_token";
    public static String EXTRA_USER_ID = "ext_user_id";
    public static final String PUSH_SERVICE_CLASS_NAME = "com.xiaomi.xmsf.push.service.XMPushService";
    public static final String PUSH_SERVICE_PACKAGE_NAME = "com.xiaomi.xmsf";
    public static final String REASON_INVALID_SIGNATURE = "invalid-sig";
    public static final String REASON_INVALID_TOKEN = "invalid-token";
    public static final String REASON_TOKEN_EXPIRED = "token-expired";

}
