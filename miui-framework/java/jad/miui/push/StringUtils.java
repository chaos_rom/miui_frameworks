// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.push;

import android.util.Base64;
import java.util.Random;

public class StringUtils {

    private StringUtils() {
    }

    public static String encodeBase64(byte abyte0[]) {
        return encodeBase64(abyte0, false);
    }

    public static String encodeBase64(byte abyte0[], int i, int j, boolean flag) {
        int k;
        if(flag)
            k = 0;
        else
            k = 2;
        return Base64.encodeToString(abyte0, i, j, k);
    }

    public static String encodeBase64(byte abyte0[], boolean flag) {
        return encodeBase64(abyte0, 0, abyte0.length, flag);
    }

    public static String escapeForXML(String s) {
        if(s != null) goto _L2; else goto _L1
_L1:
        s = null;
_L4:
        return s;
_L2:
        int i = 0;
        int j = 0;
        char ac[] = s.toCharArray();
        int k = ac.length;
        StringBuilder stringbuilder = new StringBuilder((int)(1.3D * (double)k));
        while(i < k)  {
            char c = ac[i];
            if(c <= '>')
                if(c == '<') {
                    if(i > j)
                        stringbuilder.append(ac, j, i - j);
                    j = i + 1;
                    stringbuilder.append(LT_ENCODE);
                } else
                if(c == '>') {
                    if(i > j)
                        stringbuilder.append(ac, j, i - j);
                    j = i + 1;
                    stringbuilder.append(GT_ENCODE);
                } else
                if(c == '&') {
                    if(i > j)
                        stringbuilder.append(ac, j, i - j);
                    if(k <= i + 5 || ac[i + 1] != '#' || !Character.isDigit(ac[i + 2]) || !Character.isDigit(ac[i + 3]) || !Character.isDigit(ac[i + 4]) || ac[i + 5] != ';') {
                        j = i + 1;
                        stringbuilder.append(AMP_ENCODE);
                    }
                } else
                if(c == '"') {
                    if(i > j)
                        stringbuilder.append(ac, j, i - j);
                    j = i + 1;
                    stringbuilder.append(QUOTE_ENCODE);
                } else
                if(c == '\'') {
                    if(i > j)
                        stringbuilder.append(ac, j, i - j);
                    j = i + 1;
                    stringbuilder.append(APOS_ENCODE);
                }
            i++;
        }
        if(j != 0) {
            if(i > j)
                stringbuilder.append(ac, j, i - j);
            s = stringbuilder.toString();
        }
        if(true) goto _L4; else goto _L3
_L3:
    }

    public static String randomString(int i) {
        String s;
        if(i < 1) {
            s = null;
        } else {
            char ac[] = new char[i];
            for(int j = 0; j < ac.length; j++)
                ac[j] = numbersAndLetters[randGen.nextInt(71)];

            s = new String(ac);
        }
        return s;
    }

    private static final char AMP_ENCODE[] = "&amp;".toCharArray();
    private static final char APOS_ENCODE[] = "&apos;".toCharArray();
    private static final char GT_ENCODE[] = "&gt;".toCharArray();
    private static final char LT_ENCODE[] = "&lt;".toCharArray();
    private static final char QUOTE_ENCODE[] = "&quot;".toCharArray();
    private static char numbersAndLetters[] = "0123456789abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
    private static Random randGen = new Random();

}
