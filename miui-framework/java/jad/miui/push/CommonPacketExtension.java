// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.push;

import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import java.util.*;

// Referenced classes of package miui.push:
//            PacketExtension, StringUtils

public class CommonPacketExtension
    implements PacketExtension {

    public CommonPacketExtension(String s, String s1, String s2, String s3) {
        mAttributeNames = null;
        mAttributeValues = null;
        mChildrenEles = null;
        mExtensionElementName = s;
        mNamespace = s1;
        String as[] = new String[1];
        as[0] = s2;
        mAttributeNames = as;
        String as1[] = new String[1];
        as1[0] = s3;
        mAttributeValues = as1;
    }

    public CommonPacketExtension(String s, String s1, List list, List list1) {
        mAttributeNames = null;
        mAttributeValues = null;
        mChildrenEles = null;
        mExtensionElementName = s;
        mNamespace = s1;
        mAttributeNames = (String[])list.toArray(new String[list.size()]);
        mAttributeValues = (String[])list1.toArray(new String[list1.size()]);
    }

    public CommonPacketExtension(String s, String s1, List list, List list1, String s2, List list2) {
        mAttributeNames = null;
        mAttributeValues = null;
        mChildrenEles = null;
        mExtensionElementName = s;
        mNamespace = s1;
        mAttributeNames = (String[])list.toArray(new String[list.size()]);
        mAttributeValues = (String[])list1.toArray(new String[list1.size()]);
        mText = s2;
        mChildrenEles = list2;
    }

    public CommonPacketExtension(String s, String s1, String as[], String as1[]) {
        mAttributeNames = null;
        mAttributeValues = null;
        mChildrenEles = null;
        mExtensionElementName = s;
        mNamespace = s1;
        mAttributeNames = as;
        mAttributeValues = as1;
    }

    public CommonPacketExtension(String s, String s1, String as[], String as1[], String s2, List list) {
        mAttributeNames = null;
        mAttributeValues = null;
        mChildrenEles = null;
        mExtensionElementName = s;
        mNamespace = s1;
        mAttributeNames = as;
        mAttributeValues = as1;
        mText = s2;
        mChildrenEles = list;
    }

    public static CommonPacketExtension[] getArray(Parcelable aparcelable[]) {
        int i;
        CommonPacketExtension acommonpacketextension[];
        if(aparcelable == null)
            i = 0;
        else
            i = aparcelable.length;
        acommonpacketextension = new CommonPacketExtension[i];
        if(aparcelable != null) {
            for(int j = 0; j < aparcelable.length; j++)
                acommonpacketextension[j] = parseFromBundle((Bundle)aparcelable[j]);

        }
        return acommonpacketextension;
    }

    public static CommonPacketExtension parseFromBundle(Bundle bundle) {
        String s = bundle.getString("ext_ele_name");
        String s1 = bundle.getString("ext_ns");
        String s2 = bundle.getString("ext_text");
        Bundle bundle1 = bundle.getBundle("attributes");
        Set set = bundle1.keySet();
        String as[] = new String[set.size()];
        String as1[] = new String[set.size()];
        ArrayList arraylist = null;
        int i = 0;
        for(Iterator iterator = set.iterator(); iterator.hasNext();) {
            String s3 = (String)iterator.next();
            as[i] = s3;
            as1[i] = bundle1.getString(s3);
            i++;
        }

        if(bundle.containsKey("children")) {
            Parcelable aparcelable[] = bundle.getParcelableArray("children");
            arraylist = new ArrayList(aparcelable.length);
            int j = aparcelable.length;
            for(int k = 0; k < j; k++)
                arraylist.add(parseFromBundle((Bundle)aparcelable[k]));

        }
        return new CommonPacketExtension(s, s1, as, as1, s2, arraylist);
    }

    public static Parcelable[] toParcelableArray(List list) {
        return toParcelableArray((CommonPacketExtension[])list.toArray(new CommonPacketExtension[list.size()]));
    }

    public static Parcelable[] toParcelableArray(CommonPacketExtension acommonpacketextension[]) {
        Parcelable aparcelable[];
        if(acommonpacketextension == null) {
            aparcelable = null;
        } else {
            aparcelable = new Parcelable[acommonpacketextension.length];
            int i = 0;
            while(i < acommonpacketextension.length)  {
                aparcelable[i] = acommonpacketextension[i].toParcelable();
                i++;
            }
        }
        return aparcelable;
    }

    public void appendChild(CommonPacketExtension commonpacketextension) {
        if(mChildrenEles == null)
            mChildrenEles = new ArrayList();
        if(!mChildrenEles.contains(commonpacketextension))
            mChildrenEles.add(commonpacketextension);
    }

    public String getAttributeValue(String s) {
        int i;
        if(s == null)
            throw new IllegalArgumentException();
        if(mAttributeNames == null)
            break MISSING_BLOCK_LABEL_58;
        i = 0;
_L3:
        if(i >= mAttributeNames.length)
            break MISSING_BLOCK_LABEL_58;
        if(!mAttributeNames[i].equals(s)) goto _L2; else goto _L1
_L1:
        String s1 = mAttributeValues[i];
_L4:
        return s1;
_L2:
        i++;
          goto _L3
        s1 = null;
          goto _L4
    }

    public CommonPacketExtension getChildByName(String s) {
        if(!TextUtils.isEmpty(s) && mChildrenEles != null) goto _L2; else goto _L1
_L1:
        CommonPacketExtension commonpacketextension = null;
_L4:
        return commonpacketextension;
_L2:
        for(Iterator iterator = mChildrenEles.iterator(); iterator.hasNext();) {
            commonpacketextension = (CommonPacketExtension)iterator.next();
            if(commonpacketextension.mExtensionElementName.equals(s))
                continue; /* Loop/switch isn't completed */
        }

        commonpacketextension = null;
        if(true) goto _L4; else goto _L3
_L3:
    }

    public List getChildrenByName(String s) {
        Object obj;
        if(TextUtils.isEmpty(s) || mChildrenEles == null) {
            obj = null;
        } else {
            obj = new ArrayList();
            Iterator iterator = mChildrenEles.iterator();
            while(iterator.hasNext())  {
                CommonPacketExtension commonpacketextension = (CommonPacketExtension)iterator.next();
                if(commonpacketextension.mExtensionElementName.equals(s))
                    ((List) (obj)).add(commonpacketextension);
            }
        }
        return ((List) (obj));
    }

    public List getChildrenExt() {
        return mChildrenEles;
    }

    public String getElementName() {
        return mExtensionElementName;
    }

    public String getNamespace() {
        return mNamespace;
    }

    public String getText() {
        return mText;
    }

    public void setText(String s) {
        mText = s;
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        bundle.putString("ext_ele_name", mExtensionElementName);
        bundle.putString("ext_ns", mNamespace);
        bundle.putString("ext_text", mText);
        Bundle bundle1 = new Bundle();
        if(mAttributeNames != null && mAttributeNames.length > 0) {
            for(int i = 0; i < mAttributeNames.length; i++)
                bundle1.putString(mAttributeNames[i], mAttributeValues[i]);

        }
        bundle.putBundle("attributes", bundle1);
        if(mChildrenEles != null && mChildrenEles.size() > 0)
            bundle.putParcelableArray("children", toParcelableArray(mChildrenEles));
        return bundle;
    }

    public Parcelable toParcelable() {
        return toBundle();
    }

    public String toString() {
        return toXML();
    }

    public String toXML() {
        StringBuilder stringbuilder = new StringBuilder();
        stringbuilder.append("<").append(mExtensionElementName);
        if(!TextUtils.isEmpty(mNamespace))
            stringbuilder.append(" ").append("xmlns=").append("\"").append(mNamespace).append("\"");
        if(mAttributeNames != null && mAttributeNames.length > 0) {
            for(int i = 0; i < mAttributeNames.length; i++)
                if(!TextUtils.isEmpty(mAttributeValues[i]))
                    stringbuilder.append(" ").append(mAttributeNames[i]).append("=\"").append(StringUtils.escapeForXML(mAttributeValues[i])).append("\"");

        }
        if(!TextUtils.isEmpty(mText))
            stringbuilder.append(">").append(mText).append("</").append(mExtensionElementName).append(">");
        else
        if(mChildrenEles != null && mChildrenEles.size() > 0) {
            stringbuilder.append(">");
            for(Iterator iterator = mChildrenEles.iterator(); iterator.hasNext(); stringbuilder.append(((CommonPacketExtension)iterator.next()).toXML()));
            stringbuilder.append("</").append(mExtensionElementName).append(">");
        } else {
            stringbuilder.append("/>");
        }
        return stringbuilder.toString();
    }

    public static final String ATTRIBUTE_NAME = "attributes";
    public static final String CHILDREN_NAME = "children";
    private String mAttributeNames[];
    private String mAttributeValues[];
    private List mChildrenEles;
    private String mExtensionElementName;
    private String mNamespace;
    private String mText;
}
