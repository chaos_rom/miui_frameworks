// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.net.exception;


public class PaymentServiceFailureException extends Exception {

    public PaymentServiceFailureException(int i, String s) {
        super(s);
        mErrorCode = i;
    }

    public int getError() {
        return mErrorCode;
    }

    private static final long serialVersionUID = 1L;
    private int mErrorCode;
}
