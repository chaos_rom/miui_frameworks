// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.net.exception;


public class MiCloudServerException extends Exception {

    public MiCloudServerException(int i) {
        super((new StringBuilder()).append("status: ").append(i).toString());
        statusCode = i;
    }

    public static boolean isMiCloudServerException(int i) {
        boolean flag;
        if(i == 400 || i == 401 || i == 403 || i == 406 || i / 100 == 5)
            flag = true;
        else
            flag = false;
        return flag;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public int statusCode;
}
