// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.net;

import android.text.TextUtils;

public final class ExtendedAuthToken {

    private ExtendedAuthToken(String s, String s1) {
        authToken = s;
        security = s1;
    }

    public static ExtendedAuthToken build(String s, String s1) {
        return new ExtendedAuthToken(s, s1);
    }

    public static ExtendedAuthToken parse(String s) {
        ExtendedAuthToken extendedauthtoken = null;
        if(!TextUtils.isEmpty(s)) goto _L2; else goto _L1
_L1:
        return extendedauthtoken;
_L2:
        String as[] = s.split(",");
        if(as.length == 2)
            extendedauthtoken = new ExtendedAuthToken(as[0], as[1]);
        if(true) goto _L1; else goto _L3
_L3:
    }

    public boolean equals(Object obj) {
        boolean flag = true;
        if(this != obj) goto _L2; else goto _L1
_L1:
        return flag;
_L2:
        if(obj == null || getClass() != obj.getClass()) {
            flag = false;
        } else {
            ExtendedAuthToken extendedauthtoken = (ExtendedAuthToken)obj;
            if(authToken == null ? extendedauthtoken.authToken != null : !authToken.equals(extendedauthtoken.authToken))
                flag = false;
            else
            if(security == null ? extendedauthtoken.security != null : !security.equals(extendedauthtoken.security))
                flag = false;
        }
        if(true) goto _L1; else goto _L3
_L3:
    }

    public int hashCode() {
        int i = 0;
        int j;
        int k;
        if(authToken != null)
            j = authToken.hashCode();
        else
            j = 0;
        k = j * 31;
        if(security != null)
            i = security.hashCode();
        return k + i;
    }

    public String toPlain() {
        return (new StringBuilder()).append(authToken).append(",").append(security).toString();
    }

    private static final String SP = ",";
    public final String authToken;
    public final String security;
}
