// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.net;

import android.accounts.Account;
import android.content.*;
import android.os.*;
import android.telephony.TelephonyManager;
import android.util.Log;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.*;
import miui.net.exception.AccessDeniedException;
import miui.net.exception.AuthenticationFailureException;
import miui.net.exception.CipherException;
import miui.net.exception.CloudServiceFailureException;
import miui.net.exception.InvalidResponseException;
import miui.net.exception.NoActivateAccountException;
import miui.net.exception.OperationCancelledException;
import miui.telephony.CarrierSelector;
import miui.telephony.ExtraTelephonyManager;
import miui.telephony.exception.IllegalDeviceException;
import org.apache.http.NameValuePair;
import org.json.*;

// Referenced classes of package miui.net:
//            CloudCoder, SecureRequest, ICloudManagerResponse, ICloudManagerService

public class CloudManager {
    public static class PhoneInfo {

        private JSONObject toJSONObject() {
            JSONObject jsonobject;
            try {
                jsonobject = new JSONObject();
                jsonobject.put("simId", mSimId);
                jsonobject.put("phone", mPhone);
            }
            catch(JSONException jsonexception) {
                jsonexception.printStackTrace();
                jsonobject = null;
            }
            return jsonobject;
        }

        public String createPhoneInfoString(List list) {
            JSONArray jsonarray = new JSONArray();
            if(list != null) {
                Iterator iterator = list.iterator();
                do {
                    if(!iterator.hasNext())
                        break;
                    JSONObject jsonobject = ((PhoneInfo)iterator.next()).toJSONObject();
                    if(jsonobject != null)
                        jsonarray.put(jsonobject);
                } while(true);
            }
            return jsonarray.toString();
        }

        public final String mPhone;
        public final String mSimId;

        public PhoneInfo(String s, String s1) {
            mSimId = s;
            mPhone = s1;
        }
    }

    public static interface DevSettingName {

        public static final String CAPABILITY = "capability";
        public static final String DEVICENAME = "deviceName";
        public static final String MODEL = "model";
        public static final String OS_VERSION = "osVersion";
        public static final String PHONE_INFO = "_phoneInfo";
    }

    public static interface CloudManagerFuture {

        public abstract boolean cancel(boolean flag);

        public abstract Object getResult() throws IOException, OperationCancelledException, CloudServiceFailureException, NoActivateAccountException;

        public abstract Object getResult(long l, TimeUnit timeunit) throws IOException, OperationCancelledException, CloudServiceFailureException, NoActivateAccountException;

        public abstract boolean isCancelled();

        public abstract boolean isDone();
    }

    private abstract class CmTask extends FutureTask
        implements CloudManagerFuture, ServiceConnection {
        class ICloudManagerResponseImpl extends ICloudManagerResponse.Stub {

            public void onError(int i, String s) throws RemoteException {
                setException(convertErrorCodeToException(i));
            }

            public void onResult(Bundle bundle) throws RemoteException {
                set(bundle);
            }

            final CmTask this$1;

            ICloudManagerResponseImpl() {
                this$1 = CmTask.this;
                super();
            }
        }


        private Exception convertErrorCodeToException(int i) {
            i;
            JVM INSTR tableswitch 1 3: default 28
        //                       1 40
        //                       2 40
        //                       3 53;
               goto _L1 _L2 _L2 _L3
_L1:
            Object obj = new CloudServiceFailureException("Unknown activation failure");
_L5:
            return ((Exception) (obj));
_L2:
            obj = new CloudServiceFailureException("Send sms failure or activate timed out");
            continue; /* Loop/switch isn't completed */
_L3:
            obj = new NoActivateAccountException("no active Xiaomi account");
            if(true) goto _L5; else goto _L4
_L4:
        }

        private void ensureNotOnMainThread() {
            Looper looper = Looper.myLooper();
            if(looper != null && looper == mContext.getMainLooper()) {
                IllegalStateException illegalstateexception = new IllegalStateException("calling this from your main thread can lead to deadlock");
                Log.e("CloudManager", "calling this from your main thread can lead to deadlock and/or ANRs", illegalstateexception);
                throw illegalstateexception;
            } else {
                return;
            }
        }

        private Bundle internalGetResult(Long long1, TimeUnit timeunit) throws IOException, CloudServiceFailureException, OperationCancelledException, NoActivateAccountException {
            if(!isDone())
                ensureNotOnMainThread();
            if(long1 != null) goto _L2; else goto _L1
_L1:
            Bundle bundle = (Bundle)get();
            cancel(true);
_L4:
            return bundle;
_L2:
            bundle = (Bundle)get(long1.longValue(), timeunit);
            cancel(true);
            if(true) goto _L4; else goto _L3
_L3:
            CancellationException cancellationexception;
            cancellationexception;
            throw new OperationCancelledException();
            Exception exception;
            exception;
            cancel(true);
            throw exception;
            TimeoutException timeoutexception;
            timeoutexception;
            cancel(true);
_L6:
            throw new OperationCancelledException();
            InterruptedException interruptedexception;
            interruptedexception;
            cancel(true);
            if(true) goto _L6; else goto _L5
_L5:
            ExecutionException executionexception;
            executionexception;
            Throwable throwable = executionexception.getCause();
            if(throwable instanceof IOException)
                throw (IOException)throwable;
            if(throwable instanceof CloudServiceFailureException)
                throw (CloudServiceFailureException)throwable;
            if(throwable instanceof NoActivateAccountException)
                throw (NoActivateAccountException)throwable;
            if(throwable instanceof Error)
                throw (Error)throwable;
            else
                throw new CloudServiceFailureException(throwable);
        }

        protected void bind() {
            if(!bindToCloudService())
                setException(new CloudServiceFailureException());
        }

        protected boolean bindToCloudService() {
            Intent intent = new Intent("android.intent.action.XIAOMI_ACTIVATE_PHONE");
            mContext.startService(intent);
            return mContext.bindService(intent, this, 1);
        }

        protected abstract void doWork() throws RemoteException;

        protected ICloudManagerResponse getResponse() {
            return mResponse;
        }

        public Bundle getResult() throws IOException, OperationCancelledException, CloudServiceFailureException, NoActivateAccountException {
            return internalGetResult(null, null);
        }

        public Bundle getResult(long l, TimeUnit timeunit) throws IOException, OperationCancelledException, CloudServiceFailureException, NoActivateAccountException {
            return internalGetResult(Long.valueOf(l), timeunit);
        }

        public volatile Object getResult() throws IOException, OperationCancelledException, CloudServiceFailureException, NoActivateAccountException {
            return getResult();
        }

        public volatile Object getResult(long l, TimeUnit timeunit) throws IOException, OperationCancelledException, CloudServiceFailureException, NoActivateAccountException {
            return getResult(l, timeunit);
        }

        protected ICloudManagerService getService() {
            return mService;
        }

        public void onServiceConnected(ComponentName componentname, IBinder ibinder) {
            Log.d("CloudManager", (new StringBuilder()).append("onServiceConnected, component:").append(componentname).toString());
            mService = ICloudManagerService.Stub.asInterface(ibinder);
            doWork();
_L1:
            return;
            RemoteException remoteexception;
            remoteexception;
            setException(remoteexception);
              goto _L1
        }

        public void onServiceDisconnected(ComponentName componentname) {
            if(!isDone()) {
                Log.e("CloudManager", "cloud service disconnected, but task is not completed");
                setException(new CloudServiceFailureException("active service exits unexpectedly"));
            }
            mService = null;
        }

        protected void set(Bundle bundle) {
            super.set(bundle);
            unBind();
        }

        protected volatile void set(Object obj) {
            set((Bundle)obj);
        }

        protected void setException(Throwable throwable) {
            super.setException(throwable);
            unBind();
        }

        public final CloudManagerFuture start() {
            bind();
            return this;
        }

        protected void unBind() {
            mContext.unbindService(this);
            Log.d("CloudManager", "service unbinded");
        }

        private ICloudManagerResponse mResponse;
        private ICloudManagerService mService;
        final CloudManager this$0;


        protected CmTask() {
            this.this$0 = CloudManager.this;
            super(new Callable() {

                public Bundle call() throws Exception {
                    throw new IllegalStateException("this should never be called");
                }

                public volatile Object call() throws Exception {
                    return call();
                }

                final CloudManager val$this$0;

                 {
                    this$0 = cloudmanager;
                    super();
                }
            });
            mResponse = new ICloudManagerResponseImpl();
        }
    }


    private CloudManager(Context context) {
        mContext = context;
    }

    public static CloudManager get(Context context) {
        return new CloudManager(context);
    }

    public static String getDeviceId(Context context) throws IllegalDeviceException {
        return CloudCoder.hashDeviceInfo(ExtraTelephonyManager.blockingGetDeviceId(context));
    }

    public static String getResourceId(Context context) throws IllegalDeviceException {
        return getDeviceId(context);
    }

    public static String getUserAgent() {
        if(mUserAgent == null) {
            StringBuilder stringbuilder = new StringBuilder();
            stringbuilder.append(Build.MODEL);
            stringbuilder.append("; MIUI/");
            stringbuilder.append(android.os.Build.VERSION.INCREMENTAL);
            mUserAgent = stringbuilder.toString();
        }
        return mUserAgent;
    }

    public static boolean isSimSupported(Context context) {
        boolean flag;
        String s;
        flag = true;
        s = ((TelephonyManager)context.getSystemService("phone")).getSimOperator();
        if(s != null) goto _L2; else goto _L1
_L1:
        return flag;
_L2:
        if(s.length() != 5 && s.length() != 6) {
            Log.w("CloudManager", (new StringBuilder()).append("unsupported mcc mnc: ").append(s).toString());
            flag = false;
        } else {
            String s1 = s.substring(0, 3);
            String s2 = s.substring(3);
            if(CARRIER_SELECTOR.selectValue(s1, s2) == null)
                flag = false;
        }
        if(true) goto _L1; else goto _L3
_L3:
    }

    public static String selectSmsGateway(Context context) {
        TelephonyManager telephonymanager = (TelephonyManager)context.getSystemService("phone");
        CloudManager cloudmanager = get(context);
        String s;
        String s1;
        try {
            Bundle bundle = (Bundle)cloudmanager.getSmsGateway().getResult();
            String s4 = bundle.getString("sms_gw_china_mobile");
            String s5 = bundle.getString("sms_gw_china_unicom");
            String s6 = bundle.getString("sms_gw_china_telecom");
            String s7 = bundle.getString("sms_gw_global");
            CARRIER_SELECTOR.register(miui.telephony.CarrierSelector.CARRIER.CHINA_MOBILE, s4);
            CARRIER_SELECTOR.register(miui.telephony.CarrierSelector.CARRIER.CHINA_UNICOM, s5);
            CARRIER_SELECTOR.register(miui.telephony.CarrierSelector.CARRIER.CHINA_TELECOM, s6);
            CARRIER_SELECTOR.register(miui.telephony.CarrierSelector.CARRIER.DEFAULT, s7);
        }
        catch(Exception exception) {
            Log.e("CloudManager", "error when get sms gateway", exception);
        }
        s = telephonymanager.getNetworkOperator();
        Log.d("CloudManager", (new StringBuilder()).append("device mccmnc:").append(s).toString());
        if(s == null || s.length() < 5) {
            Log.w("CloudManager", (new StringBuilder()).append("illegal mcc mnc: ").append(s).toString());
            s1 = null;
        } else {
            String s2 = s.substring(0, 3);
            String s3 = s.substring(3);
            s1 = (String)CARRIER_SELECTOR.selectValue(s2, s3, true);
        }
        return s1;
    }

    public static boolean uploadDeviceInfo(Context context, String s, String s1, String s2, List list) throws AuthenticationFailureException {
        String s4;
        HashMap hashmap;
        HashMap hashmap1;
        JSONArray jsonarray;
        String s3 = getDeviceId(context);
        StringBuilder stringbuilder = (new StringBuilder()).append("http://api.device.xiaomi.net");
        Object aobj[] = new Object[2];
        aobj[0] = s;
        aobj[1] = s3;
        s4 = stringbuilder.append(String.format("/udi/v1/user/%s/device/%s/setting", aobj)).toString();
        hashmap = new HashMap();
        hashmap.put("serviceToken", s1);
        hashmap.put("userId", s);
        hashmap1 = new HashMap();
        jsonarray = new JSONArray();
        JSONObject jsonobject;
        for(Iterator iterator = list.iterator(); iterator.hasNext(); jsonarray.put(jsonobject)) {
            NameValuePair namevaluepair = (NameValuePair)iterator.next();
            jsonobject = new JSONObject();
            jsonobject.put("name", namevaluepair.getName());
            jsonobject.put("value", namevaluepair.getValue());
        }

          goto _L1
        IOException ioexception;
        ioexception;
        ioexception.printStackTrace();
_L5:
        boolean flag = false;
_L3:
        return flag;
_L1:
        Object obj;
        Object obj1;
        hashmap1.put("content", jsonarray.toString());
        SimpleRequest.MapContent mapcontent = SecureRequest.postAsMap(s4, hashmap1, hashmap, true, s2);
        obj = mapcontent.getFromBody("code");
        obj1 = mapcontent.getFromBody("description");
        if(!INT_0.equals(obj))
            break; /* Loop/switch isn't completed */
        flag = true;
        if(true) goto _L3; else goto _L2
_L2:
        Log.d("CloudManager", (new StringBuilder()).append("failed upload dev settings, code: ").append(obj).append("; description: ").append(obj1).toString());
        continue; /* Loop/switch isn't completed */
        AccessDeniedException accessdeniedexception;
        accessdeniedexception;
        accessdeniedexception.printStackTrace();
        continue; /* Loop/switch isn't completed */
        InvalidResponseException invalidresponseexception;
        invalidresponseexception;
        invalidresponseexception.printStackTrace();
        continue; /* Loop/switch isn't completed */
        CipherException cipherexception;
        cipherexception;
        cipherexception.printStackTrace();
        continue; /* Loop/switch isn't completed */
        IllegalDeviceException illegaldeviceexception;
        illegaldeviceexception;
        illegaldeviceexception.printStackTrace();
        continue; /* Loop/switch isn't completed */
        JSONException jsonexception;
        jsonexception;
        jsonexception.printStackTrace();
        if(true) goto _L5; else goto _L4
_L4:
    }

    public CloudManagerFuture cancelNotification(final int notificationId) {
        return (new CmTask() {

            protected void doWork() throws RemoteException {
                getService().cancelNotification(notificationId, getResponse());
            }

            final CloudManager this$0;
            final int val$notificationId;

             {
                this$0 = CloudManager.this;
                notificationId = i;
                super();
            }
        }).start();
    }

    public CloudManagerFuture getActivateStatus() {
        return (new CmTask() {

            protected void doWork() throws RemoteException {
                getService().getActivatedStatus(getResponse());
            }

            final CloudManager this$0;

             {
                this$0 = CloudManager.this;
                super();
            }
        }).start();
    }

    public CloudManagerFuture getActivatedPhoneNumber() {
        return (new CmTask() {

            protected void doWork() throws RemoteException {
                getService().getActivatedPhone(getResponse());
            }

            final CloudManager this$0;

             {
                this$0 = CloudManager.this;
                super();
            }
        }).start();
    }

    public CloudManagerFuture getFindDeviceToken() {
        return (new CmTask() {

            protected void doWork() throws RemoteException {
                getService().getFindDeviceToken(getResponse());
            }

            final CloudManager this$0;

             {
                this$0 = CloudManager.this;
                super();
            }
        }).start();
    }

    public CloudManagerFuture getSmsGateway() {
        return (new CmTask() {

            protected void doWork() throws RemoteException {
                getService().getSmsGateway(getResponse());
            }

            final CloudManager this$0;

             {
                this$0 = CloudManager.this;
                super();
            }
        }).start();
    }

    public CloudManagerFuture getSubSyncAutomatically(final Account account, final String authority) {
        return (new CmTask() {

            protected void doWork() throws RemoteException {
                getService().getSubSyncAutomatically(account, authority, getResponse());
            }

            final CloudManager this$0;
            final Account val$account;
            final String val$authority;

             {
                this$0 = CloudManager.this;
                account = account1;
                authority = s;
                super();
            }
        }).start();
    }

    public CloudManagerFuture getUserSecurity() {
        return (new CmTask() {

            protected void doWork() throws RemoteException {
                getService().getUserSecurity(getResponse());
            }

            final CloudManager this$0;

             {
                this$0 = CloudManager.this;
                super();
            }
        }).start();
    }

    public CloudManagerFuture invalidateUserSecurity(final String token, final String security) {
        return (new CmTask() {

            protected void doWork() throws RemoteException {
                getService().invalidateUserSecurity(token, security, getResponse());
            }

            final CloudManager this$0;
            final String val$security;
            final String val$token;

             {
                this$0 = CloudManager.this;
                token = s;
                security = s1;
                super();
            }
        }).start();
    }

    public static final int ACTIVATE_STATUS_ACTIVATED = 2;
    public static final int ACTIVATE_STATUS_ACTIVATING = 1;
    public static final int ACTIVATE_STATUS_UNACTIVATED = 3;
    private static final CarrierSelector CARRIER_SELECTOR;
    private static final boolean DEBUG = true;
    public static final String DEVICE_INFO_TOKEN = "deviceinfo";
    public static final int ERROR_CODE_ACTIVATE_TIMEOUT = 2;
    public static final int ERROR_CODE_NO_ACCOUNT = 3;
    public static final int ERROR_CODE_SEND_SMS_FAILURE = 1;
    public static final int ERROR_SIM_NOT_ACTIVATED = 1;
    private static final Integer INT_0 = Integer.valueOf(0);
    public static final String KEY_ACTIVATE_PHONE = "activate_phone";
    public static final String KEY_ACTIVATE_STATUS = "activate_status";
    public static final String KEY_FIND_DEVICE_TOKEN = "find_device_token";
    public static final String KEY_PASSTOKEN = "pass_token";
    public static final String KEY_PHONE_TICKET = "phone_ticket";
    public static final String KEY_RESULT = "result";
    public static final String KEY_SECONDARY_SYNC_ON = "secondary_sync_on";
    public static final String KEY_SMS_GATEWAY_CHINA_MOBILE = "sms_gw_china_mobile";
    public static final String KEY_SMS_GATEWAY_CHINA_TELECOM = "sms_gw_china_telecom";
    public static final String KEY_SMS_GATEWAY_CHINA_UNICOM = "sms_gw_china_unicom";
    public static final String KEY_SMS_GATEWAY_GLOBAL = "sms_gw_global";
    public static final String KEY_USER_SECURITY = "user_security";
    public static final String KEY_USER_TOKEN = "user_token";
    public static final int NOTIFICATION_ACTIVATE_ERROR = 0x10000001;
    public static final String SMS_GW_CM = "106571203855788";
    public static final String SMS_GW_CT = "10659057100335678";
    public static final String SMS_GW_CU = "1065507729555678";
    public static final String SMS_GW_GLOBAL = "+447786209730";
    private static final String TAG = "CloudManager";
    public static final String URL_ACCOUNT_BASE = "https://account.xiaomi.com/pass";
    public static final String URL_ACCOUNT_SAFE_API_BASE = "http://api.account.xiaomi.com/pass/v2/safe";
    public static final String URL_ACOUNT_API_BASE = "http://api.account.xiaomi.com/pass";
    public static final String URL_CONTACT_BASE = "http://api.micloud.xiaomi.net";
    public static final String URL_DEV_BASE = "http://api.device.xiaomi.net";
    public static final String URL_DEV_SETTING = "/udi/v1/user/%s/device/%s/setting";
    public static final String URL_FIND_DEVICE_BASE = "http://api.micloud.xiaomi.net";
    public static final String URL_GALLERY_BASE = "http://galleryapi.micloud.xiaomi.net";
    public static final String URL_MMS_BASE = "http://api.micloud.xiaomi.net";
    public static final String URL_RICH_MEDIA_BASE = "http://fileapi.micloud.xiaomi.net";
    public static final String URL_WIFI_BASE = "http://api.micloud.xiaomi.net";
    private static String mUserAgent;
    private Context mContext;

    static  {
        CARRIER_SELECTOR = new CarrierSelector();
        CARRIER_SELECTOR.register(miui.telephony.CarrierSelector.CARRIER.CHINA_MOBILE, "106571203855788");
        CARRIER_SELECTOR.register(miui.telephony.CarrierSelector.CARRIER.CHINA_UNICOM, "1065507729555678");
        CARRIER_SELECTOR.register(miui.telephony.CarrierSelector.CARRIER.CHINA_TELECOM, "10659057100335678");
        CARRIER_SELECTOR.register(miui.telephony.CarrierSelector.CARRIER.DEFAULT, "+447786209730");
    }

}
