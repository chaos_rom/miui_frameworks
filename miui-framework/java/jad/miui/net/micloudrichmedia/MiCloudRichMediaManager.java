// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.net.micloudrichmedia;

import android.accounts.*;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import java.io.IOException;
import java.util.Collection;
import miui.net.ExtendedAuthToken;
import miui.net.exception.*;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

// Referenced classes of package miui.net.micloudrichmedia:
//            Utils, UploadEntity, UploadResult

public class MiCloudRichMediaManager {

    public MiCloudRichMediaManager(Context context, Account account) {
        if(account == null || TextUtils.isEmpty(account.name) || TextUtils.isEmpty(account.type)) {
            throw new IllegalArgumentException((new StringBuilder()).append("The account should not be null").append(account).toString());
        } else {
            mAccount = account;
            mContext = context;
            mMiCloudRichUtils = new Utils(mContext, account.name);
            return;
        }
    }

    public MiCloudRichMediaManager(Context context, String s, ExtendedAuthToken extendedauthtoken) {
        mContext = context;
        mAuthToken = extendedauthtoken;
        mMiCloudRichUtils = new Utils(mContext, s);
        mMiCloudRichUtils.updateToken(extendedauthtoken);
    }

    private ExtendedAuthToken initAuthToken() throws IOException {
        if(mAuthToken == null) goto _L2; else goto _L1
_L1:
        ExtendedAuthToken extendedauthtoken = mAuthToken;
_L4:
        return extendedauthtoken;
_L2:
        AccountManagerFuture accountmanagerfuture;
        accountmanagerfuture = AccountManager.get(mContext).getAuthToken(mAccount, "micfile", null, true, null, null);
        if(accountmanagerfuture == null)
            break; /* Loop/switch isn't completed */
label0:
        {
            if((Bundle)accountmanagerfuture.getResult() == null)
                break label0;
            mAuthToken = ExtendedAuthToken.parse(((Bundle)accountmanagerfuture.getResult()).getString("authtoken"));
            if(mAuthToken == null)
                break MISSING_BLOCK_LABEL_106;
            mMiCloudRichUtils.updateToken(mAuthToken);
            extendedauthtoken = mAuthToken;
        }
        if(true) goto _L4; else goto _L3
        log("getAuthToken: future getResult is null");
_L6:
        throw new IOException("failed to get auth token");
_L3:
        try {
            log("getAuthToken: future is null");
        }
        catch(OperationCanceledException operationcanceledexception) {
            operationcanceledexception.printStackTrace();
        }
        catch(AuthenticatorException authenticatorexception) {
            authenticatorexception.printStackTrace();
        }
        catch(IOException ioexception) {
            ioexception.printStackTrace();
        }
        if(true) goto _L6; else goto _L5
_L5:
    }

    private void invalidateAuthToken() {
        log("invalidateAuthToken:MiCloud rich media token expired.");
        if(mAuthToken != null)
            AccountManager.get(mContext).invalidateAuthToken(mAccount.type, mAuthToken.toPlain());
        mAuthToken = null;
    }

    static final void log(String s) {
        Log.d("MiCloudRichMediaManager", s);
    }

    public byte[] download(String s, String s1) throws NetworkErrorException, MiCloudParameterError, IOException, MiCloudRichMediaServerException {
        initAuthToken();
        byte abyte2[] = mMiCloudRichUtils.download(s, s1);
        byte abyte0[] = abyte2;
_L1:
        return abyte0;
        CloudServiceFailureException cloudservicefailureexception1;
        cloudservicefailureexception1;
        log((new StringBuilder()).append("download:").append(cloudservicefailureexception1).toString());
_L2:
        abyte0 = null;
          goto _L1
        MiCloudServerException micloudserverexception;
        micloudserverexception;
        if(micloudserverexception.statusCode != 401 || mAccount == null)
            break MISSING_BLOCK_LABEL_167;
        invalidateAuthToken();
        byte abyte1[] = mMiCloudRichUtils.download(s, s1);
        abyte0 = abyte1;
          goto _L1
        CloudServiceFailureException cloudservicefailureexception;
        cloudservicefailureexception;
        log((new StringBuilder()).append("download:").append(cloudservicefailureexception).toString());
          goto _L2
        JSONException jsonexception1;
        jsonexception1;
        log((new StringBuilder()).append("download:").append(jsonexception1).toString());
          goto _L2
        MiCloudServerException micloudserverexception1;
        micloudserverexception1;
        throw new MiCloudRichMediaServerException(micloudserverexception1.statusCode);
        throw new MiCloudRichMediaServerException(micloudserverexception.statusCode);
        JSONException jsonexception;
        jsonexception;
        log((new StringBuilder()).append("download:").append(jsonexception).toString());
          goto _L2
    }

    public UploadResult getFileId(UploadEntity uploadentity) throws ClientProtocolException, IOException, MiCloudParameterError, MiCloudRichMediaServerException {
        return getFileId(uploadentity, null);
    }

    public UploadResult getFileId(UploadEntity uploadentity, Collection collection) throws ClientProtocolException, IOException, MiCloudParameterError, MiCloudRichMediaServerException {
        UploadResult uploadresult;
        uploadresult = null;
        initAuthToken();
        UploadResult uploadresult2 = mMiCloudRichUtils.getFileId(uploadentity, collection);
        uploadresult = uploadresult2;
_L2:
        return uploadresult;
        CloudServiceFailureException cloudservicefailureexception1;
        cloudservicefailureexception1;
        log((new StringBuilder()).append("getFileId:").append(cloudservicefailureexception1).toString());
        continue; /* Loop/switch isn't completed */
        MiCloudServerException micloudserverexception;
        micloudserverexception;
        if(micloudserverexception.statusCode == 401 && mAccount != null) {
            invalidateAuthToken();
            UploadResult uploadresult1;
            try {
                uploadresult1 = mMiCloudRichUtils.getFileId(uploadentity, collection);
            }
            catch(CloudServiceFailureException cloudservicefailureexception) {
                log((new StringBuilder()).append("getFileId:").append(cloudservicefailureexception).toString());
                continue; /* Loop/switch isn't completed */
            }
            catch(JSONException jsonexception1) {
                log((new StringBuilder()).append("getFileId:").append(jsonexception1).toString());
                continue; /* Loop/switch isn't completed */
            }
            catch(MiCloudServerException micloudserverexception1) {
                throw new MiCloudRichMediaServerException(micloudserverexception1.statusCode);
            }
            uploadresult = uploadresult1;
            continue; /* Loop/switch isn't completed */
        } else {
            throw new MiCloudRichMediaServerException(micloudserverexception.statusCode);
        }
        JSONException jsonexception;
        jsonexception;
        log((new StringBuilder()).append("getFileId:").append(jsonexception).toString());
        if(true) goto _L2; else goto _L1
_L1:
    }

    public void updateAuthToken(ExtendedAuthToken extendedauthtoken) {
        mMiCloudRichUtils.updateToken(extendedauthtoken);
    }

    public UploadResult upload(UploadEntity uploadentity) throws FileTooLargeException, MiCloudParameterError, NetworkErrorException, IOException, MiCloudRichMediaServerException {
        return upload(uploadentity, null);
    }

    public UploadResult upload(UploadEntity uploadentity, Collection collection) throws FileTooLargeException, MiCloudParameterError, NetworkErrorException, IOException, MiCloudRichMediaServerException {
        initAuthToken();
        UploadResult uploadresult2 = mMiCloudRichUtils.upload(uploadentity, collection);
        UploadResult uploadresult = uploadresult2;
_L3:
        return uploadresult;
        CloudServiceFailureException cloudservicefailureexception1;
        cloudservicefailureexception1;
        log((new StringBuilder()).append("upload:").append(cloudservicefailureexception1).toString());
_L1:
        uploadresult = null;
        continue; /* Loop/switch isn't completed */
        MiCloudServerException micloudserverexception;
        micloudserverexception;
        if(micloudserverexception.statusCode != 401 || mAccount == null)
            break MISSING_BLOCK_LABEL_139;
        invalidateAuthToken();
        UploadResult uploadresult1 = mMiCloudRichUtils.upload(uploadentity, collection);
        uploadresult = uploadresult1;
        continue; /* Loop/switch isn't completed */
        CloudServiceFailureException cloudservicefailureexception;
        cloudservicefailureexception;
        log((new StringBuilder()).append("upload:").append(cloudservicefailureexception).toString());
          goto _L1
        MiCloudServerException micloudserverexception1;
        micloudserverexception1;
        throw new MiCloudRichMediaServerException(micloudserverexception1.statusCode);
        throw new MiCloudRichMediaServerException(micloudserverexception.getStatusCode());
        if(true) goto _L3; else goto _L2
_L2:
    }

    private static final String AUTH_TOKEN_TYPE = "micfile";
    private static final boolean DBG = true;
    private static final String TAG = "MiCloudRichMediaManager";
    private Account mAccount;
    private ExtendedAuthToken mAuthToken;
    private Context mContext;
    private Utils mMiCloudRichUtils;
}
