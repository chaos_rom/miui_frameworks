// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.net.micloudrichmedia;

import android.text.TextUtils;
import java.io.UnsupportedEncodingException;
import java.util.List;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import miui.net.CloudCoder;
import org.apache.http.message.BasicNameValuePair;

// Referenced classes of package miui.net.micloudrichmedia:
//            FirstUploadChunkRequest, UploadRequest, UploadEntity

class SimpleUploadChunkRequest extends FirstUploadChunkRequest {

    public SimpleUploadChunkRequest(UploadEntity uploadentity, boolean flag, int i, int j) {
        super(uploadentity, flag, i);
        mOffset = j;
    }

    protected String getBaseUrl() {
        return "http://fileapi.micloud.xiaomi.net/mic/v1/file/user/%s/full/more";
    }

    protected List getParams(String s) throws UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException {
        if(TextUtils.isEmpty(super.mFile.getTempId()) || TextUtils.isEmpty(super.mFile.getHostingServer())) {
            throw new IllegalArgumentException("The tempid or hosting server should not be null for the non first chunk");
        } else {
            List list = super.getParams(s);
            list.add(new BasicNameValuePair("tmpid", CloudCoder.encodeString(s, super.mFile.getTempId(), "UTF-8")));
            list.add(new BasicNameValuePair("_hostingserver", super.mFile.getHostingServer()));
            list.add(new BasicNameValuePair("offset", CloudCoder.encodeString(s, String.valueOf(mOffset), "UTF-8")));
            return list;
        }
    }

    private static final String BASE_URL = "http://fileapi.micloud.xiaomi.net/mic/v1/file/user/%s/full/more";
    private int mOffset;
}
