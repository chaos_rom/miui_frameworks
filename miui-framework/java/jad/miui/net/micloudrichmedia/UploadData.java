// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.net.micloudrichmedia;

import java.io.IOException;
import miui.net.CloudCoder;

// Referenced classes of package miui.net.micloudrichmedia:
//            UploadEntity

public class UploadData extends UploadEntity {

    public UploadData(byte abyte0[], String s, String s1) throws IOException {
        super(s, s1);
        if(abyte0 == null || abyte0.length == 0)
            throw new IllegalArgumentException("The data to be uploaded should not be null");
        mData = abyte0;
        super.mHexDigest = CloudCoder.getDataSha1Digest(mData);
        if(super.mHexDigest == null)
            throw new IOException("Calculate file sha-1 digest error");
        else
            return;
    }

    byte[] getData(int i) {
        byte abyte0[];
        if(super.mOffset < getLength()) {
            if(i < getLength() - super.mOffset)
                abyte0 = new byte[i];
            else
                abyte0 = new byte[getLength() - super.mOffset];
            System.arraycopy(mData, super.mOffset, abyte0, 0, abyte0.length);
            super.mOffset = super.mOffset + abyte0.length;
        } else {
            abyte0 = null;
        }
        return abyte0;
    }

    int getLength() {
        return mData.length;
    }

    private byte mData[];
}
