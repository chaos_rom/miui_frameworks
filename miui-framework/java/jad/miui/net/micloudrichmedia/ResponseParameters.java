// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.net.micloudrichmedia;

import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package miui.net.micloudrichmedia:
//            MiCloudRichMediaManager

class ResponseParameters {

    private ResponseParameters() {
    }

    public static ResponseParameters parseResponse(JSONObject jsonobject) throws JSONException {
        MiCloudRichMediaManager.log((new StringBuilder()).append("The response json string is:").append(jsonobject).toString());
        ResponseParameters responseparameters = new ResponseParameters();
        responseparameters.mCode = jsonobject.optInt("code", -1);
        responseparameters.mResult = jsonobject.optString("result", "");
        responseparameters.mDescription = jsonobject.optString("description", "");
        responseparameters.mReason = jsonobject.optString("reason", "");
        if(responseparameters.mCode == 0)
            responseparameters.mData = jsonobject.getJSONObject("data");
        return responseparameters;
    }

    public static final int CODE_FILE_TOO_LARGE = 0x13884;
    public static final int CODE_INVALID_POSITION = 0x13883;
    public static final int CODE_MISMATCH_DIGEST = 0x13881;
    public static final int CODE_MISSING_TMPID = 0x13885;
    public static final int CODE_PARAMETER_ERROR = 10008;
    public static final int CODE_SUCCESS = 0;
    public static final int CODE_WRITE_CONFLICT = 0x13882;
    public static final String TAG_CODE = "code";
    public static final String TAG_DATA = "data";
    public static final String TAG_DATA_CKEY = "ckey";
    public static final String TAG_DATA_EXPIRE_AT = "expireAt";
    public static final String TAG_DATA_FILEID = "fileId";
    public static final String TAG_DATA_HOSTING_SERVER = "_hostingserver";
    public static final String TAG_DATA_SHA1 = "fileSha1";
    public static final String TAG_DATA_SHAREID = "shareId";
    public static final String TAG_DATA_TEMPURL = "tmpUrl";
    public static final String TAG_DATA_TMPID = "tmpid";
    public static final String TAG_DES = "description";
    public static final String TAG_REASON = "reason";
    public static final String TAG_RESULT = "result";
    public int mCode;
    public JSONObject mData;
    public String mDescription;
    public String mReason;
    public String mResult;
}
