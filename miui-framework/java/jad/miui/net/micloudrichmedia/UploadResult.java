// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.net.micloudrichmedia;

import android.text.TextUtils;
import org.json.JSONObject;

public class UploadResult {

    private UploadResult(String s, String s1, long l) {
        fileId = s;
        shareId = s1;
        expireAt = l;
    }

    public static UploadResult fromJson(JSONObject jsonobject) {
        UploadResult uploadresult = null;
        String s = jsonobject.optString("fileId", null);
        String s1 = jsonobject.optString("shareId", null);
        long l = jsonobject.optLong("expireAt", 0L);
        if(!TextUtils.isEmpty(s) || !TextUtils.isEmpty(s1))
            uploadresult = new UploadResult(s, s1, l);
        return uploadresult;
    }

    public final long expireAt;
    public final String fileId;
    public final String shareId;
}
