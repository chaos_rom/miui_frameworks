// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.net.micloudrichmedia;


public class MiCloudRichMediaSupportedFileType {

    public MiCloudRichMediaSupportedFileType() {
    }

    public static boolean isSupported(String s) {
        boolean flag;
        if("mms".equals(s) || "ico".equals(s) || "rec".equals(s) || "mixin".equals(s))
            flag = true;
        else
            flag = false;
        return flag;
    }

    public static final String ICO = "ico";
    public static final String MIXIN = "mixin";
    public static final String MMS = "mms";
    public static final String REC = "rec";
}
