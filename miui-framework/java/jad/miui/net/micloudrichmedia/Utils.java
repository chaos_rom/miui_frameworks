// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.net.micloudrichmedia;

import android.accounts.NetworkErrorException;
import android.content.*;
import android.net.*;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import java.io.IOException;
import java.util.Collection;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import miui.net.ExtendedAuthToken;
import miui.net.exception.*;
import org.apache.commons.codec.DecoderException;
import org.apache.http.client.ClientProtocolException;
import org.json.*;

// Referenced classes of package miui.net.micloudrichmedia:
//            MiCloudRichMediaManager, DownloadUrlRequest, ResponseParameters, UploadEntity, 
//            FirstUploadChunkRequest, UploadRequest, SimpleUploadChunkRequest, UploadResult, 
//            MiCloudRichMediaSupportedFileType, DownloadRequest, CheckRequest

class Utils {
    private class NetworkConnectivityChangedReceiver extends BroadcastReceiver {

        public void onReceive(Context context, Intent intent) {
            mCurrentUploadChunk = getChunkSize(context);
        }

        final Utils this$0;

        private NetworkConnectivityChangedReceiver() {
            this$0 = Utils.this;
            super();
        }

    }

    private class DownloadFileInfo {

        public String ckey;
        public String fileId;
        public String fileSha1;
        final Utils this$0;
        public String tmpUrl;

        private DownloadFileInfo() {
            this$0 = Utils.this;
            super();
        }

    }


    Utils(Context context, String s) {
        if(TextUtils.isEmpty(s)) {
            throw new IllegalArgumentException("The userId and authtoken should not be null");
        } else {
            mUserId = s;
            mContext = context;
            return;
        }
    }

    private int getChunkSize(Context context) {
        int i;
        ConnectivityManager connectivitymanager;
        android.net.NetworkInfo.State state;
        i = 20480;
        connectivitymanager = (ConnectivityManager)context.getSystemService("connectivity");
        state = connectivitymanager.getNetworkInfo(1).getState();
        if(android.net.NetworkInfo.State.CONNECTED != state) goto _L2; else goto _L1
_L1:
        i = 0x19000;
_L4:
        return i;
_L2:
        android.net.NetworkInfo.State state1 = connectivitymanager.getNetworkInfo(0).getState();
        if(android.net.NetworkInfo.State.CONNECTED == state1)
            switch(TelephonyManager.getNetworkClass(TelephonyManager.getDefault().getNetworkType())) {
            case 2: // '\002'
            case 3: // '\003'
                i = 51200;
                break;
            }
        else
            i = 0;
        if(true) goto _L4; else goto _L3
_L3:
    }

    static String getCookies(String s, String s1) {
        StringBuilder stringbuilder = new StringBuilder();
        stringbuilder.append("serviceToken=");
        stringbuilder.append(s1);
        stringbuilder.append("; userId=");
        stringbuilder.append(s);
        return stringbuilder.toString();
    }

    private DownloadFileInfo getDownloadUrl(String s, String s1) throws NetworkErrorException, JSONException, CloudServiceFailureException, MiCloudServerException, MiCloudParameterError {
        ResponseParameters responseparameters;
        MiCloudRichMediaManager.log((new StringBuilder()).append("The download file id is:").append(s).toString());
        JSONObject jsonobject = (new DownloadUrlRequest(s, s1)).request(mUserId, mExtAuthToken, mCookie);
        if(jsonobject == null)
            break MISSING_BLOCK_LABEL_301;
        responseparameters = ResponseParameters.parseResponse(jsonobject);
        if(responseparameters.mCode != 0 || responseparameters.mData == null) goto _L2; else goto _L1
_L1:
        JSONArray jsonarray = responseparameters.mData.getJSONArray("data");
        if(jsonarray == null || jsonarray.length() != 1) goto _L4; else goto _L3
_L3:
        JSONObject jsonobject1;
        String s2;
        jsonobject1 = jsonarray.getJSONObject(0);
        s2 = jsonobject1.optString("fileId", null);
        if(!TextUtils.equals(s, s2)) goto _L6; else goto _L5
_L5:
        DownloadFileInfo downloadfileinfo;
        downloadfileinfo = new DownloadFileInfo();
        downloadfileinfo.tmpUrl = jsonobject1.optString("tmpUrl", null);
        downloadfileinfo.ckey = jsonobject1.optString("ckey", null);
        downloadfileinfo.fileId = s2;
        downloadfileinfo.fileSha1 = jsonobject1.optString("fileSha1", null);
        if(downloadfileinfo.tmpUrl == null || downloadfileinfo.ckey == null || downloadfileinfo.fileId == null || downloadfileinfo.fileSha1 == null)
            break MISSING_BLOCK_LABEL_301;
_L7:
        return downloadfileinfo;
_L6:
        Object aobj1[] = new Object[2];
        aobj1[0] = s;
        aobj1[1] = s2;
        throw new CloudServiceFailureException(String.format("The local fileId %s is not accord with server fileId %s", aobj1));
_L4:
        Object aobj[] = new Object[1];
        aobj[0] = s;
        MiCloudRichMediaManager.log(String.format("The local fileId %s unable to obtain the download", aobj));
        downloadfileinfo = null;
        if(true) goto _L7; else goto _L2
_L2:
        if(responseparameters.mCode == 10008)
            throw new MiCloudParameterError(responseparameters.mDescription);
        throw new CloudServiceFailureException((new StringBuilder()).append("Cloud service error on getDownloadUrl for ").append(s).toString());
    }

    private void registerConnectivityReceiver() {
        MiCloudRichMediaManager.log("Register network connectivity changed receiver");
        if(mNetworkConnectivityReceiver == null)
            mNetworkConnectivityReceiver = new NetworkConnectivityChangedReceiver();
        IntentFilter intentfilter = new IntentFilter();
        intentfilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        mContext.registerReceiver(mNetworkConnectivityReceiver, intentfilter);
    }

    private void unregisterConnectivityReceiver() {
        MiCloudRichMediaManager.log("Unregister network connectivity changed receiver");
        mContext.unregisterReceiver(mNetworkConnectivityReceiver);
    }

    private UploadResult uploadFile(UploadEntity uploadentity, Collection collection) throws ClientProtocolException, IOException, JSONException, InvalidWritePositionException, FileTooLargeException, CloudServiceFailureException, MiCloudServerException, MiCloudParameterError {
        int i = 0;
label0:
        do {
            ResponseParameters responseparameters;
label1:
            {
label2:
                {
                    if(uploadentity.getOffset() < uploadentity.getLength()) {
                        int j = mCurrentUploadChunk;
                        MiCloudRichMediaManager.log((new StringBuilder()).append("Current chunk size is:").append(j).append(" the ").append(uploadentity.getOffset() / j).append(" chunk").toString());
                        boolean flag = uploadentity.isFirstChunk();
                        boolean flag1 = uploadentity.isLastChunk(j);
                        Object obj;
                        JSONObject jsonobject;
                        if(flag)
                            obj = new FirstUploadChunkRequest(uploadentity, flag1, j);
                        else
                            obj = new SimpleUploadChunkRequest(uploadentity, flag1, j, uploadentity.getOffset());
                        if(collection != null && !collection.isEmpty())
                            ((UploadRequest) (obj)).addParam("shareTo", TextUtils.join(",", collection));
                        jsonobject = ((FirstUploadChunkRequest) (obj)).request(mUserId, mExtAuthToken, mCookie);
                        if(jsonobject == null)
                            break label0;
                        responseparameters = ResponseParameters.parseResponse(jsonobject);
                        if(responseparameters.mCode != 0x13882)
                            i = 0;
                        if(responseparameters.mCode == 0) {
                            if(flag && !flag1) {
                                uploadentity.mTempId = responseparameters.mData.optString("tmpid", null);
                                uploadentity.mHostingServer = responseparameters.mData.optString("_hostingserver", null);
                                if(TextUtils.isEmpty(uploadentity.mTempId) || TextUtils.isEmpty(uploadentity.mHostingServer))
                                    throw new IllegalStateException("Server error: The first chunk's response does not contain the tempid or hosting server");
                            } else
                            if(flag1)
                                return UploadResult.fromJson(responseparameters.mData);
                            continue;
                        }
                        if(responseparameters.mCode == 0x13885) {
                            uploadentity.resetOffset();
                            continue;
                        }
                        if(responseparameters.mCode != 0x13882)
                            break label1;
                        if(i != 3)
                            break label2;
                    }
                    throw new IOException("Read file error");
                }
                try {
                    Thread.sleep(5000L);
                }
                catch(InterruptedException interruptedexception) {
                    interruptedexception.printStackTrace();
                    continue;
                }
                i++;
                continue;
            }
            if(responseparameters.mCode == 0x13881)
                throw new IOException("The file digest error");
            if(responseparameters.mCode == 0x13883)
                throw new InvalidWritePositionException((new StringBuilder()).append("Can't write file at offset:").append(uploadentity.getOffset()).toString());
            if(responseparameters.mCode == 0x13884)
                throw new FileTooLargeException((new StringBuilder()).append("The file size:").append(uploadentity.getLength()).append(" exceeds the limit").toString());
            if(responseparameters.mCode == 10008)
                throw new MiCloudParameterError(responseparameters.mDescription);
        } while(true);
        throw new CloudServiceFailureException("Cloud service fails when upload file");
    }

    byte[] download(String s, String s1) throws NetworkErrorException, IOException, CloudServiceFailureException, MiCloudServerException, MiCloudParameterError, JSONException {
        byte abyte1[];
label0:
        {
            if(TextUtils.isEmpty(s) || TextUtils.isEmpty(s1))
                throw new IllegalArgumentException("The download parameters should not be null");
            if(!MiCloudRichMediaSupportedFileType.isSupported(s1)) {
                Object aobj[] = new Object[1];
                aobj[0] = s1;
                throw new IllegalArgumentException(String.format("The type %s is not supported", aobj));
            }
            mCurrentUploadChunk = getChunkSize(mContext);
            if(mCurrentUploadChunk == 0)
                throw new NetworkErrorException("Network is not connected");
            byte abyte0[];
            try {
                DownloadFileInfo downloadfileinfo = getDownloadUrl(s, s1);
                if(downloadfileinfo == null) {
                    abyte1 = null;
                    break label0;
                }
                Uri uri = Uri.parse(downloadfileinfo.tmpUrl);
                StringBuilder stringbuilder = new StringBuilder();
                stringbuilder.append(uri.getScheme()).append("://");
                stringbuilder.append(uri.getAuthority());
                stringbuilder.append(uri.getPath());
                abyte0 = (new DownloadRequest(stringbuilder.toString(), downloadfileinfo.ckey, uri.getQueryParameter("fi"), downloadfileinfo.fileSha1)).download(mContext, mUserId, mExtAuthToken, mCookie);
            }
            catch(DecoderException decoderexception) {
                throw new IOException(decoderexception);
            }
            catch(IllegalBlockSizeException illegalblocksizeexception) {
                throw new IOException(illegalblocksizeexception);
            }
            catch(BadPaddingException badpaddingexception) {
                throw new IOException(badpaddingexception);
            }
            abyte1 = abyte0;
        }
        return abyte1;
    }

    UploadResult getFileId(UploadEntity uploadentity, Collection collection) throws ClientProtocolException, IOException, JSONException, CloudServiceFailureException, MiCloudServerException, MiCloudParameterError {
        MiCloudRichMediaManager.log((new StringBuilder()).append("Current chunk size is:").append(mCurrentUploadChunk).toString());
        CheckRequest checkrequest = new CheckRequest(uploadentity);
        if(collection != null && !collection.isEmpty())
            checkrequest.addParam("shareTo", TextUtils.join(",", collection));
        JSONObject jsonobject = checkrequest.request(mUserId, mExtAuthToken, mCookie);
        if(jsonobject != null) {
            ResponseParameters responseparameters = ResponseParameters.parseResponse(jsonobject);
            if(responseparameters.mCode == 0)
                return UploadResult.fromJson(responseparameters.mData);
            if(responseparameters.mCode == 10008)
                throw new MiCloudParameterError(responseparameters.mDescription);
        }
        throw new CloudServiceFailureException("Cloud service error in check file exits");
    }

    void updateToken(ExtendedAuthToken extendedauthtoken) {
        if(extendedauthtoken == null) {
            throw new IllegalArgumentException("The authtoken should not be null");
        } else {
            mCookie = getCookies(mUserId, extendedauthtoken.authToken);
            mExtAuthToken = extendedauthtoken;
            return;
        }
    }

    UploadResult upload(UploadEntity uploadentity, Collection collection) throws IOException, FileTooLargeException, NetworkErrorException, CloudServiceFailureException, MiCloudServerException, MiCloudParameterError {
        if(uploadentity != null) goto _L2; else goto _L1
_L1:
        UploadResult uploadresult;
        MiCloudRichMediaManager.log("The file should not be null");
        uploadresult = null;
_L4:
        return uploadresult;
_L2:
        mCurrentUploadChunk = getChunkSize(mContext);
        if(mCurrentUploadChunk == 0)
            throw new NetworkErrorException("Network is not connected");
        registerConnectivityReceiver();
        uploadentity.open();
        uploadresult = getFileId(uploadentity, collection);
        if(uploadresult == null || TextUtils.isEmpty(uploadresult.fileId))
            break MISSING_BLOCK_LABEL_113;
        MiCloudRichMediaManager.log((new StringBuilder()).append("The file already exist:").append(uploadresult).toString());
        uploadentity.close();
        unregisterConnectivityReceiver();
        continue; /* Loop/switch isn't completed */
        uploadresult = uploadFile(uploadentity, collection);
        if(uploadresult == null)
            break; /* Loop/switch isn't completed */
        MiCloudRichMediaManager.log((new StringBuilder()).append("The file upload success:").append(uploadresult).toString());
        uploadentity.close();
        unregisterConnectivityReceiver();
        if(true) goto _L4; else goto _L3
_L3:
        uploadentity.close();
        unregisterConnectivityReceiver();
_L5:
        throw new CloudServiceFailureException("Cloud server fails when upload files");
        JSONException jsonexception;
        jsonexception;
        jsonexception.printStackTrace();
        uploadentity.close();
        unregisterConnectivityReceiver();
          goto _L5
        InvalidWritePositionException invalidwritepositionexception;
        invalidwritepositionexception;
        invalidwritepositionexception.printStackTrace();
        uploadentity.close();
        unregisterConnectivityReceiver();
          goto _L5
        Exception exception;
        exception;
        uploadentity.close();
        unregisterConnectivityReceiver();
        throw exception;
    }

    private Context mContext;
    private String mCookie;
    private int mCurrentUploadChunk;
    private ExtendedAuthToken mExtAuthToken;
    private NetworkConnectivityChangedReceiver mNetworkConnectivityReceiver;
    private String mUserId;


/*
    static int access$202(Utils utils, int i) {
        utils.mCurrentUploadChunk = i;
        return i;
    }

*/

}
