// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.net.micloudrichmedia;

import com.google.android.collect.Lists;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.util.List;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import miui.net.CloudCoder;
import org.apache.http.message.BasicNameValuePair;

// Referenced classes of package miui.net.micloudrichmedia:
//            UploadRequest, UploadEntity

class CheckRequest extends UploadRequest {

    public CheckRequest(UploadEntity uploadentity) {
        super(uploadentity);
    }

    protected String getBaseUrl() {
        return "http://fileapi.micloud.xiaomi.net/mic/v1/file/user/%s/full/digest";
    }

    protected HttpURLConnection getConn(String s, String s1) {
        HttpURLConnection httpurlconnection = super.getConn(s, s1);
        if(httpurlconnection != null)
            httpurlconnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        return httpurlconnection;
    }

    protected List getParams(String s) throws IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
        java.util.ArrayList arraylist = Lists.newArrayList();
        arraylist.add(new BasicNameValuePair("type", CloudCoder.encodeString(s, super.mFile.getType(), "UTF-8")));
        arraylist.add(new BasicNameValuePair("digest", CloudCoder.encodeString(s, super.mFile.getHexDigest(), "UTF-8")));
        return arraylist;
    }

    private static final String BASE_URL = "http://fileapi.micloud.xiaomi.net/mic/v1/file/user/%s/full/digest";
}
