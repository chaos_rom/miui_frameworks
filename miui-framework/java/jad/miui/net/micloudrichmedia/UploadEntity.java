// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.net.micloudrichmedia;

import android.text.TextUtils;
import java.io.IOException;

// Referenced classes of package miui.net.micloudrichmedia:
//            MiCloudRichMediaSupportedFileType

public abstract class UploadEntity {

    public UploadEntity(String s, String s1) throws IOException {
        if(TextUtils.isEmpty(s) || TextUtils.isEmpty(s1))
            throw new IllegalArgumentException("The upload file parameters should not be null");
        if(!MiCloudRichMediaSupportedFileType.isSupported(s)) {
            Object aobj1[] = new Object[1];
            aobj1[0] = s;
            throw new IllegalArgumentException(String.format("The type %s is not supported", aobj1));
        }
        if(s1.length() > 5) {
            Object aobj[] = new Object[2];
            aobj[0] = s1;
            aobj[1] = Integer.valueOf(5);
            throw new IllegalArgumentException(String.format("The ext's %s length should not exceeds %d", aobj));
        } else {
            mType = s;
            mExt = s1;
            mTempId = null;
            mHostingServer = null;
            return;
        }
    }

    void close() throws IOException {
    }

    abstract byte[] getData(int i) throws IOException;

    public String getExt() {
        return mExt;
    }

    public String getHexDigest() {
        return mHexDigest;
    }

    String getHostingServer() {
        return mHostingServer;
    }

    abstract int getLength() throws IOException;

    int getOffset() {
        return mOffset;
    }

    String getTempId() {
        return mTempId;
    }

    public String getType() {
        return mType;
    }

    boolean isFirstChunk() {
        boolean flag;
        if(mOffset == 0)
            flag = true;
        else
            flag = false;
        return flag;
    }

    boolean isLastChunk(int i) throws IOException {
        boolean flag;
        if(mOffset < getLength() && i + mOffset >= getLength())
            flag = true;
        else
            flag = false;
        return flag;
    }

    void open() throws IOException {
    }

    void resetOffset() {
        mOffset = 0;
    }

    void setHostingServer(String s) {
        mHostingServer = s;
    }

    void setTempId(String s) {
        mTempId = s;
    }

    private static final int EXT_MAX_LEN = 5;
    protected String mExt;
    protected String mHexDigest;
    protected String mHostingServer;
    protected int mOffset;
    protected String mTempId;
    protected String mType;
}
