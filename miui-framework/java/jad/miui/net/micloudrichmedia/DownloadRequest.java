// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.net.micloudrichmedia;

import android.content.Context;
import android.text.TextUtils;
import com.google.android.collect.Lists;
import java.io.*;
import java.net.HttpURLConnection;
import java.util.*;
import javax.crypto.*;
import miui.net.CloudCoder;
import miui.net.ExtendedAuthToken;
import miui.net.exception.CloudServiceFailureException;
import miui.net.exception.MiCloudServerException;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

// Referenced classes of package miui.net.micloudrichmedia:
//            Request, MiCloudRichMediaManager

class DownloadRequest extends Request {

    public DownloadRequest(String s, String s1, String s2, String s3) {
        if(TextUtils.isEmpty(s2) || TextUtils.isEmpty(s) || TextUtils.isEmpty(s1) || TextUtils.isEmpty(s3)) {
            throw new IllegalArgumentException("The download requset parameters should not be null");
        } else {
            mBaseUrl = s;
            mCkey = s1;
            mFi = s2;
            mFileSha1 = s3;
            mRetryTimes = 0;
            return;
        }
    }

    private ByteArrayOutputStream getTemporaryDownloadData(Context context) {
        File file;
        ByteArrayOutputStream bytearrayoutputstream;
        FileInputStream fileinputstream;
        file = new File((new StringBuilder()).append(context.getCacheDir()).append(File.separator).append(mFileSha1).toString());
        bytearrayoutputstream = null;
        fileinputstream = null;
        if(!file.exists()) goto _L2; else goto _L1
_L1:
        Object aobj[] = new Object[1];
        aobj[0] = file.getName();
        MiCloudRichMediaManager.log(String.format("getTemporaryDownloadData:The temporary downloaded file %s exist", aobj));
        FileInputStream fileinputstream1 = new FileInputStream(file);
        ByteArrayOutputStream bytearrayoutputstream1 = new ByteArrayOutputStream();
        byte abyte0[] = new byte[1024];
        do {
            int i = fileinputstream1.read(abyte0);
            if(i == -1)
                break;
            bytearrayoutputstream1.write(abyte0, 0, i);
        } while(true);
          goto _L3
        FileNotFoundException filenotfoundexception;
        filenotfoundexception;
        fileinputstream = fileinputstream1;
        bytearrayoutputstream = bytearrayoutputstream1;
_L8:
        filenotfoundexception.printStackTrace();
        IOException ioexception4;
        if(fileinputstream != null)
            try {
                fileinputstream.close();
            }
            catch(IOException ioexception3) {
                ioexception3.printStackTrace();
            }
        file.delete();
_L2:
        return bytearrayoutputstream;
_L3:
        bytearrayoutputstream1.flush();
        if(fileinputstream1 != null)
            try {
                fileinputstream1.close();
            }
            // Misplaced declaration of an exception variable
            catch(IOException ioexception4) {
                ioexception4.printStackTrace();
            }
        file.delete();
        bytearrayoutputstream = bytearrayoutputstream1;
          goto _L2
        IOException ioexception;
        ioexception;
_L6:
        ioexception.printStackTrace();
        if(fileinputstream != null)
            try {
                fileinputstream.close();
            }
            catch(IOException ioexception2) {
                ioexception2.printStackTrace();
            }
        file.delete();
          goto _L2
        Exception exception;
        exception;
_L5:
        if(fileinputstream != null)
            try {
                fileinputstream.close();
            }
            catch(IOException ioexception1) {
                ioexception1.printStackTrace();
            }
        file.delete();
        throw exception;
        exception;
        fileinputstream = fileinputstream1;
        continue; /* Loop/switch isn't completed */
        exception;
        fileinputstream = fileinputstream1;
        if(true) goto _L5; else goto _L4
_L4:
        ioexception;
        fileinputstream = fileinputstream1;
          goto _L6
        ioexception;
        fileinputstream = fileinputstream1;
        bytearrayoutputstream = bytearrayoutputstream1;
          goto _L6
        filenotfoundexception;
        continue; /* Loop/switch isn't completed */
        filenotfoundexception;
        fileinputstream = fileinputstream1;
        if(true) goto _L8; else goto _L7
_L7:
    }

    private void saveTemporaryDownloadedData(Context context, ByteArrayOutputStream bytearrayoutputstream) {
        FileOutputStream fileoutputstream = null;
        FileOutputStream fileoutputstream1;
        Object aobj[] = new Object[1];
        aobj[0] = mFileSha1;
        MiCloudRichMediaManager.log(String.format("Save temporary downloaded data to file %s", aobj));
        fileoutputstream1 = new FileOutputStream((new StringBuilder()).append(context.getCacheDir()).append(File.separator).append(mFileSha1).toString());
        fileoutputstream1.write(bytearrayoutputstream.toByteArray());
        fileoutputstream1.flush();
        if(fileoutputstream1 == null)
            break MISSING_BLOCK_LABEL_89;
        fileoutputstream1.close();
_L1:
        return;
        IOException ioexception4;
        ioexception4;
        ioexception4.printStackTrace();
          goto _L1
        FileNotFoundException filenotfoundexception;
        filenotfoundexception;
_L5:
        filenotfoundexception.printStackTrace();
        if(fileoutputstream != null)
            try {
                fileoutputstream.close();
            }
            catch(IOException ioexception3) {
                ioexception3.printStackTrace();
            }
          goto _L1
        IOException ioexception1;
        ioexception1;
_L4:
        ioexception1.printStackTrace();
        if(fileoutputstream != null)
            try {
                fileoutputstream.close();
            }
            catch(IOException ioexception2) {
                ioexception2.printStackTrace();
            }
          goto _L1
        Exception exception;
        exception;
_L3:
        if(fileoutputstream != null)
            try {
                fileoutputstream.close();
            }
            catch(IOException ioexception) {
                ioexception.printStackTrace();
            }
        throw exception;
        exception;
        fileoutputstream = fileoutputstream1;
        if(true) goto _L3; else goto _L2
_L2:
        ioexception1;
        fileoutputstream = fileoutputstream1;
          goto _L4
        filenotfoundexception;
        fileoutputstream = fileoutputstream1;
          goto _L5
    }

    public byte[] download(Context context, String s, ExtendedAuthToken extendedauthtoken, String s1) throws IllegalBlockSizeException, BadPaddingException, IOException, CloudServiceFailureException, DecoderException, MiCloudServerException {
        HttpURLConnection httpurlconnection;
        CipherInputStream cipherinputstream;
        ByteArrayOutputStream bytearrayoutputstream;
        httpurlconnection = null;
        cipherinputstream = null;
        bytearrayoutputstream = null;
        List list = getParamsWithSignature(extendedauthtoken.security, s);
        httpurlconnection = getConn(getTargetUrl(getBaseUrl(), URLEncodedUtils.format(list, "UTF-8")), s1);
        bytearrayoutputstream = getTemporaryDownloadData(context);
        if(bytearrayoutputstream == null) goto _L2; else goto _L1
_L1:
        Object aobj2[] = new Object[1];
        aobj2[0] = Integer.valueOf(bytearrayoutputstream.size());
        httpurlconnection.addRequestProperty("RANGE", String.format("bytes=%d-", aobj2));
_L6:
        int i;
        httpurlconnection.connect();
        i = httpurlconnection.getResponseCode();
        if(i != 200 && (bytearrayoutputstream.size() <= 0 || i != 206)) goto _L4; else goto _L3
_L3:
        CipherInputStream cipherinputstream1 = new CipherInputStream(httpurlconnection.getInputStream(), CloudCoder.newRC4Cipher(Hex.decodeHex(mCkey.toCharArray()), 2));
        byte abyte0[] = new byte[1024];
        do {
            int j = cipherinputstream1.read(abyte0);
            if(j == -1)
                break;
            bytearrayoutputstream.write(abyte0, 0, j);
        } while(true);
          goto _L5
        Exception exception;
        exception;
        cipherinputstream = cipherinputstream1;
_L11:
        ByteArrayOutputStream bytearrayoutputstream1;
        byte abyte1[];
        boolean flag;
        Object aobj[];
        byte abyte2[];
        IOException ioexception2;
        IOException ioexception3;
        IOException ioexception4;
        IOException ioexception5;
        Object aobj1[];
        if(cipherinputstream != null)
            try {
                cipherinputstream.close();
            }
            catch(IOException ioexception1) {
                ioexception1.printStackTrace();
            }
        if(bytearrayoutputstream != null)
            try {
                if(bytearrayoutputstream.size() > 0)
                    saveTemporaryDownloadedData(context, bytearrayoutputstream);
                bytearrayoutputstream.close();
            }
            catch(IOException ioexception) {
                ioexception.printStackTrace();
            }
        if(httpurlconnection != null)
            httpurlconnection.disconnect();
        throw exception;
_L2:
        bytearrayoutputstream1 = new ByteArrayOutputStream();
        bytearrayoutputstream = bytearrayoutputstream1;
          goto _L6
_L5:
        abyte1 = bytearrayoutputstream.toByteArray();
        bytearrayoutputstream.reset();
        flag = TextUtils.equals(CloudCoder.getDataSha1Digest(abyte1), mFileSha1);
        if(!flag) goto _L8; else goto _L7
_L7:
        if(cipherinputstream1 != null)
            try {
                cipherinputstream1.close();
            }
            // Misplaced declaration of an exception variable
            catch(IOException ioexception5) {
                ioexception5.printStackTrace();
            }
        if(bytearrayoutputstream != null)
            try {
                if(bytearrayoutputstream.size() > 0)
                    saveTemporaryDownloadedData(context, bytearrayoutputstream);
                bytearrayoutputstream.close();
            }
            // Misplaced declaration of an exception variable
            catch(IOException ioexception4) {
                ioexception4.printStackTrace();
            }
        if(httpurlconnection != null)
            httpurlconnection.disconnect();
_L10:
        return abyte1;
_L8:
        if(mRetryTimes >= 3)
            break; /* Loop/switch isn't completed */
        mRetryTimes = 1 + mRetryTimes;
        aobj = new Object[1];
        aobj[0] = Integer.valueOf(mRetryTimes);
        MiCloudRichMediaManager.log(String.format("download:Retry %s time to download file because of mismatch sha1", aobj));
        abyte2 = download(context, s, extendedauthtoken, s1);
        abyte1 = abyte2;
        if(cipherinputstream1 != null)
            try {
                cipherinputstream1.close();
            }
            // Misplaced declaration of an exception variable
            catch(IOException ioexception3) {
                ioexception3.printStackTrace();
            }
        if(bytearrayoutputstream != null)
            try {
                if(bytearrayoutputstream.size() > 0)
                    saveTemporaryDownloadedData(context, bytearrayoutputstream);
                bytearrayoutputstream.close();
            }
            // Misplaced declaration of an exception variable
            catch(IOException ioexception2) {
                ioexception2.printStackTrace();
            }
        if(httpurlconnection != null)
            httpurlconnection.disconnect();
        if(true) goto _L10; else goto _L9
_L9:
        throw new CloudServiceFailureException("The download data sha1 is not consistant with server sha1");
_L4:
        if(MiCloudServerException.isMiCloudServerException(i)) {
            throw new MiCloudServerException(i);
        } else {
            aobj1 = new Object[2];
            aobj1[0] = httpurlconnection.getResponseMessage();
            aobj1[1] = Integer.valueOf(i);
            MiCloudRichMediaManager.log(String.format("download:The responsed message is %s, code is %d", aobj1));
            throw new CloudServiceFailureException(httpurlconnection.getResponseMessage());
        }
        exception;
          goto _L11
    }

    protected String getBaseUrl() {
        return mBaseUrl;
    }

    protected HttpURLConnection getConn(String s, String s1) {
        HttpURLConnection httpurlconnection = super.getConn(s, s1);
        if(httpurlconnection != null)
            httpurlconnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        return httpurlconnection;
    }

    protected String getHttpMethod() {
        return "GET";
    }

    protected List getParams(String s) throws UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException {
        java.util.ArrayList arraylist = Lists.newArrayList();
        arraylist.add(new BasicNameValuePair("fi", CloudCoder.encodeString(s, mFi, "UTF-8")));
        return arraylist;
    }

    public List getParamsWithSignature(String s, String s1) throws UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException {
        List list = getParams(s);
        TreeMap treemap = new TreeMap();
        if(list != null) {
            NameValuePair namevaluepair;
            for(Iterator iterator = list.iterator(); iterator.hasNext(); treemap.put(namevaluepair.getName(), namevaluepair.getValue()))
                namevaluepair = (NameValuePair)iterator.next();

        }
        list.add(new BasicNameValuePair("signature", CloudCoder.generateSignature(getHttpMethod(), getBaseUrl(), treemap, s)));
        return list;
    }

    public JSONObject request(String s, ExtendedAuthToken extendedauthtoken, String s1) {
        throw new UnsupportedOperationException("The method was not supported");
    }

    private static final String CONN_RANGE_PROPERTY = "RANGE";
    private static final String CONN_RANGE_VALUE_FORMAT = "bytes=%d-";
    private String mBaseUrl;
    private String mCkey;
    private String mFi;
    private String mFileSha1;
    private int mRetryTimes;
}
