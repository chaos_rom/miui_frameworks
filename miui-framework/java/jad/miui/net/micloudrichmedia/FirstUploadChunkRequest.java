// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.net.micloudrichmedia;

import com.google.android.collect.Lists;
import java.io.*;
import java.net.HttpURLConnection;
import java.util.List;
import java.util.UUID;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import miui.net.CloudCoder;
import miui.net.ExtendedAuthToken;
import miui.net.exception.MiCloudServerException;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package miui.net.micloudrichmedia:
//            UploadRequest, UploadEntity, MiCloudRichMediaManager

class FirstUploadChunkRequest extends UploadRequest {

    public FirstUploadChunkRequest(UploadEntity uploadentity, boolean flag, int i) {
        super(uploadentity);
        mIsLastChunk = flag;
        mChunkLength = i;
        mCkeyHint = UUID.randomUUID().toString();
    }

    protected String getBaseUrl() {
        return "http://fileapi.micloud.xiaomi.net/mic/v1/file/user/%s/full";
    }

    protected HttpURLConnection getConn(String s, String s1) {
        HttpURLConnection httpurlconnection = super.getConn(s, s1);
        if(httpurlconnection != null) {
            httpurlconnection.setRequestProperty("Connection", "Keep-Alive");
            httpurlconnection.setRequestProperty("Content-Type", "multipart/form-data;boundary=*****");
        }
        return httpurlconnection;
    }

    protected List getParams(String s) throws UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException {
        java.util.ArrayList arraylist = Lists.newArrayList();
        arraylist.add(new BasicNameValuePair("type", CloudCoder.encodeString(s, super.mFile.getType(), "UTF-8")));
        arraylist.add(new BasicNameValuePair("ckeyhint", CloudCoder.encodeString(s, mCkeyHint, "ASCII")));
        if(mIsLastChunk) {
            arraylist.add(new BasicNameValuePair("st", CloudCoder.encodeString(s, "2", "UTF-8")));
            arraylist.add(new BasicNameValuePair("ext", CloudCoder.encodeString(s, super.mFile.getExt(), "UTF-8")));
            arraylist.add(new BasicNameValuePair("digest", CloudCoder.encodeString(s, super.mFile.getHexDigest(), "UTF-8")));
        } else {
            arraylist.add(new BasicNameValuePair("st", CloudCoder.encodeString(s, "1", "UTF-8")));
        }
        return arraylist;
    }

    public JSONObject request(String s, ExtendedAuthToken extendedauthtoken, String s1) throws MiCloudServerException {
        HttpURLConnection httpurlconnection;
        DataOutputStream dataoutputstream;
        BufferedReader bufferedreader;
        JSONObject jsonobject;
        httpurlconnection = null;
        dataoutputstream = null;
        bufferedreader = null;
        jsonobject = null;
        HttpURLConnection httpurlconnection1;
        List list = getParamsWithSignature(extendedauthtoken.security, s);
        String s2 = getBaseUrl();
        Object aobj[] = new Object[1];
        aobj[0] = s;
        httpurlconnection1 = getConn(getTargetUrl(String.format(s2, aobj), URLEncodedUtils.format(list, "UTF-8")), s1);
        httpurlconnection = httpurlconnection1;
        if(httpurlconnection != null) goto _L2; else goto _L1
_L1:
        JSONObject jsonobject1;
        jsonobject1 = null;
        if(false)
            try {
                throw null;
            }
            catch(IOException ioexception14) {
                ioexception14.printStackTrace();
            }
        if(false)
            try {
                throw null;
            }
            catch(IOException ioexception13) {
                ioexception13.printStackTrace();
            }
        if(httpurlconnection != null)
            httpurlconnection.disconnect();
_L6:
        return jsonobject1;
_L2:
        DataOutputStream dataoutputstream1;
        httpurlconnection.connect();
        dataoutputstream1 = new DataOutputStream(httpurlconnection.getOutputStream());
        int i;
        dataoutputstream1.writeBytes("--*****\r\n");
        dataoutputstream1.writeBytes("Content-Disposition:form-data;name=\"uploadfile0\";filename=\"uploadfile0\"\r\n");
        dataoutputstream1.writeBytes("\r\n");
        dataoutputstream1.write(CloudCoder.encodeStream(mCkeyHint, super.mFile.getData(mChunkLength)));
        dataoutputstream1.writeBytes("\r\n--*****--\r\n");
        dataoutputstream1.flush();
        i = httpurlconnection.getResponseCode();
        if(i != 200) goto _L4; else goto _L3
_L3:
        BufferedReader bufferedreader1 = new BufferedReader(new InputStreamReader(httpurlconnection.getInputStream()));
        StringBuilder stringbuilder;
        stringbuilder = new StringBuilder();
        do {
            String s3 = bufferedreader1.readLine();
            if(s3 == null)
                break;
            stringbuilder.append(s3);
        } while(true);
          goto _L5
        IOException ioexception;
        ioexception;
        bufferedreader = bufferedreader1;
        dataoutputstream = dataoutputstream1;
_L14:
        ioexception.printStackTrace();
        JSONException jsonexception;
        JSONObject jsonobject2;
        IOException ioexception11;
        IOException ioexception12;
        Object aobj1[];
        if(dataoutputstream != null)
            try {
                dataoutputstream.close();
            }
            catch(IOException ioexception4) {
                ioexception4.printStackTrace();
            }
        if(bufferedreader != null)
            try {
                bufferedreader.close();
            }
            catch(IOException ioexception3) {
                ioexception3.printStackTrace();
            }
        if(httpurlconnection != null)
            httpurlconnection.disconnect();
_L8:
        jsonobject1 = jsonobject;
          goto _L6
_L5:
        jsonobject2 = new JSONObject(CloudCoder.decodeString(extendedauthtoken.security, stringbuilder.toString(), "UTF-8"));
        jsonobject = jsonobject2;
        bufferedreader = bufferedreader1;
_L7:
        if(dataoutputstream1 != null)
            try {
                dataoutputstream1.close();
            }
            // Misplaced declaration of an exception variable
            catch(IOException ioexception12) {
                ioexception12.printStackTrace();
            }
        if(bufferedreader != null)
            try {
                bufferedreader.close();
            }
            // Misplaced declaration of an exception variable
            catch(IOException ioexception11) {
                ioexception11.printStackTrace();
            }
        if(httpurlconnection != null)
            httpurlconnection.disconnect();
        break MISSING_BLOCK_LABEL_310;
_L4:
        if(MiCloudServerException.isMiCloudServerException(i))
            throw new MiCloudServerException(i);
        aobj1 = new Object[2];
        aobj1[0] = httpurlconnection.getResponseMessage();
        aobj1[1] = Integer.valueOf(i);
        MiCloudRichMediaManager.log(String.format("The responsed message is %s, code is %d", aobj1));
          goto _L7
        jsonexception;
        dataoutputstream = dataoutputstream1;
_L13:
        jsonexception.printStackTrace();
        if(dataoutputstream != null)
            try {
                dataoutputstream.close();
            }
            catch(IOException ioexception6) {
                ioexception6.printStackTrace();
            }
        if(bufferedreader != null)
            try {
                bufferedreader.close();
            }
            catch(IOException ioexception5) {
                ioexception5.printStackTrace();
            }
        if(httpurlconnection != null)
            httpurlconnection.disconnect();
        break MISSING_BLOCK_LABEL_310;
        IllegalBlockSizeException illegalblocksizeexception;
        illegalblocksizeexception;
_L12:
        illegalblocksizeexception.printStackTrace();
        if(dataoutputstream != null)
            try {
                dataoutputstream.close();
            }
            catch(IOException ioexception10) {
                ioexception10.printStackTrace();
            }
        if(bufferedreader != null)
            try {
                bufferedreader.close();
            }
            catch(IOException ioexception9) {
                ioexception9.printStackTrace();
            }
        if(httpurlconnection != null)
            httpurlconnection.disconnect();
          goto _L8
        BadPaddingException badpaddingexception;
        badpaddingexception;
_L11:
        badpaddingexception.printStackTrace();
        if(dataoutputstream != null)
            try {
                dataoutputstream.close();
            }
            catch(IOException ioexception8) {
                ioexception8.printStackTrace();
            }
        if(bufferedreader != null)
            try {
                bufferedreader.close();
            }
            catch(IOException ioexception7) {
                ioexception7.printStackTrace();
            }
        if(httpurlconnection != null)
            httpurlconnection.disconnect();
          goto _L8
        Exception exception;
        exception;
_L10:
        if(dataoutputstream != null)
            try {
                dataoutputstream.close();
            }
            catch(IOException ioexception2) {
                ioexception2.printStackTrace();
            }
        if(bufferedreader != null)
            try {
                bufferedreader.close();
            }
            catch(IOException ioexception1) {
                ioexception1.printStackTrace();
            }
        if(httpurlconnection != null)
            httpurlconnection.disconnect();
        throw exception;
        exception;
        dataoutputstream = dataoutputstream1;
        continue; /* Loop/switch isn't completed */
        exception;
        bufferedreader = bufferedreader1;
        dataoutputstream = dataoutputstream1;
        if(true) goto _L10; else goto _L9
_L9:
        badpaddingexception;
        dataoutputstream = dataoutputstream1;
          goto _L11
        badpaddingexception;
        bufferedreader = bufferedreader1;
        dataoutputstream = dataoutputstream1;
          goto _L11
        illegalblocksizeexception;
        dataoutputstream = dataoutputstream1;
          goto _L12
        illegalblocksizeexception;
        bufferedreader = bufferedreader1;
        dataoutputstream = dataoutputstream1;
          goto _L12
        jsonexception;
          goto _L13
        jsonexception;
        bufferedreader = bufferedreader1;
        dataoutputstream = dataoutputstream1;
          goto _L13
        ioexception;
          goto _L14
        ioexception;
        dataoutputstream = dataoutputstream1;
          goto _L14
    }

    private static final String BASE_URL = "http://fileapi.micloud.xiaomi.net/mic/v1/file/user/%s/full";
    private int mChunkLength;
    private String mCkeyHint;
    private boolean mIsLastChunk;
}
