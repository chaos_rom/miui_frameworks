// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.net.micloudrichmedia;

import java.net.HttpURLConnection;

// Referenced classes of package miui.net.micloudrichmedia:
//            Request, UploadEntity

abstract class UploadRequest extends Request {

    UploadRequest(UploadEntity uploadentity) {
        if(uploadentity == null) {
            throw new IllegalArgumentException("The file should not be null");
        } else {
            mFile = uploadentity;
            return;
        }
    }

    protected HttpURLConnection getConn(String s, String s1) {
        HttpURLConnection httpurlconnection = super.getConn(s, s1);
        if(httpurlconnection != null) {
            httpurlconnection.setDoInput(true);
            httpurlconnection.setDoOutput(true);
            httpurlconnection.setUseCaches(false);
            httpurlconnection.setInstanceFollowRedirects(false);
        }
        return httpurlconnection;
    }

    protected String getHttpMethod() {
        return "POST";
    }

    protected static final String BOUNDARY = "*****";
    protected static final String LINE_END = "\r\n";
    protected static final String TWO_HYPHENS = "--";
    protected UploadEntity mFile;
}
