// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.net.micloudrichmedia;

import android.text.TextUtils;
import com.google.android.collect.Maps;
import java.io.*;
import java.net.*;
import java.util.*;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import miui.net.*;
import miui.net.exception.MiCloudServerException;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package miui.net.micloudrichmedia:
//            MiCloudRichMediaManager

abstract class Request {

    Request() {
    }

    public Request addParam(String s, String s1) {
        mExtParams.put(s, s1);
        return this;
    }

    protected abstract String getBaseUrl();

    protected HttpURLConnection getConn(String s, String s1) {
        HttpURLConnection httpurlconnection;
        if(TextUtils.isEmpty(s)) {
            httpurlconnection = null;
        } else {
            MiCloudRichMediaManager.log((new StringBuilder()).append("The connection url is:").append(s).toString());
            httpurlconnection = null;
            try {
                httpurlconnection = (HttpURLConnection)(new URL(s)).openConnection();
                httpurlconnection.setReadTimeout(30000);
                httpurlconnection.setConnectTimeout(30000);
                httpurlconnection.setRequestMethod(getHttpMethod());
                httpurlconnection.setRequestProperty("Cookie", s1);
                httpurlconnection.setRequestProperty("User-Agent", CloudManager.getUserAgent());
            }
            catch(MalformedURLException malformedurlexception) {
                malformedurlexception.printStackTrace();
            }
            catch(IOException ioexception) {
                ioexception.printStackTrace();
            }
        }
        return httpurlconnection;
    }

    protected abstract String getHttpMethod();

    protected abstract List getParams(String s) throws UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException;

    public List getParamsWithSignature(String s, String s1) throws UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException {
        Object obj = getParams(s);
        if(obj == null)
            obj = new ArrayList();
        if(!mExtParams.isEmpty()) {
            java.util.Map.Entry entry;
            for(Iterator iterator1 = mExtParams.entrySet().iterator(); iterator1.hasNext(); ((List) (obj)).add(new BasicNameValuePair((String)entry.getKey(), CloudCoder.encodeString(s, (String)entry.getValue(), "UTF-8"))))
                entry = (java.util.Map.Entry)iterator1.next();

        }
        TreeMap treemap = new TreeMap();
        NameValuePair namevaluepair;
        for(Iterator iterator = ((List) (obj)).iterator(); iterator.hasNext(); treemap.put(namevaluepair.getName(), namevaluepair.getValue()))
            namevaluepair = (NameValuePair)iterator.next();

        String s2 = getHttpMethod();
        String s3 = getBaseUrl();
        Object aobj[] = new Object[1];
        aobj[0] = s1;
        ((List) (obj)).add(new BasicNameValuePair("signature", CloudCoder.generateSignature(s2, String.format(s3, aobj), treemap, s)));
        return ((List) (obj));
    }

    protected String getTargetUrl(String s, String s1) {
        if(!TextUtils.isEmpty(s1)) {
            Object aobj[] = new Object[2];
            aobj[0] = s;
            aobj[1] = s1;
            s = String.format("%s?%s", aobj);
        }
        return s;
    }

    public JSONObject request(String s, ExtendedAuthToken extendedauthtoken, String s1) throws MiCloudServerException {
        HttpURLConnection httpurlconnection;
        BufferedReader bufferedreader;
        JSONObject jsonobject;
        httpurlconnection = null;
        bufferedreader = null;
        jsonobject = null;
        int i;
        List list = getParamsWithSignature(extendedauthtoken.security, s);
        String s2 = getBaseUrl();
        Object aobj[] = new Object[1];
        aobj[0] = s;
        httpurlconnection = getConn(getTargetUrl(String.format(s2, aobj), URLEncodedUtils.format(list, "UTF-8")), s1);
        httpurlconnection.connect();
        i = httpurlconnection.getResponseCode();
        if(i != 200) goto _L2; else goto _L1
_L1:
        BufferedReader bufferedreader1 = new BufferedReader(new InputStreamReader(httpurlconnection.getInputStream()));
        StringBuilder stringbuilder;
        stringbuilder = new StringBuilder();
        do {
            String s3 = bufferedreader1.readLine();
            if(s3 == null)
                break;
            stringbuilder.append(s3);
        } while(true);
          goto _L3
        IOException ioexception4;
        ioexception4;
        bufferedreader = bufferedreader1;
_L12:
        ioexception4.printStackTrace();
        JSONException jsonexception;
        JSONObject jsonobject1;
        IOException ioexception6;
        Object aobj1[];
        if(bufferedreader != null)
            try {
                bufferedreader.close();
            }
            catch(IOException ioexception5) {
                ioexception5.printStackTrace();
            }
        if(httpurlconnection != null)
            httpurlconnection.disconnect();
_L5:
        return jsonobject;
_L3:
        jsonobject1 = new JSONObject(CloudCoder.decodeString(extendedauthtoken.security, stringbuilder.toString(), "UTF-8"));
        jsonobject = jsonobject1;
        bufferedreader = bufferedreader1;
_L4:
        if(bufferedreader != null)
            try {
                bufferedreader.close();
            }
            // Misplaced declaration of an exception variable
            catch(IOException ioexception6) {
                ioexception6.printStackTrace();
            }
        if(httpurlconnection != null)
            httpurlconnection.disconnect();
        break MISSING_BLOCK_LABEL_166;
_L2:
        if(MiCloudServerException.isMiCloudServerException(i))
            throw new MiCloudServerException(i);
        aobj1 = new Object[2];
        aobj1[0] = httpurlconnection.getResponseMessage();
        aobj1[1] = Integer.valueOf(i);
        MiCloudRichMediaManager.log(String.format("The responsed message is %s, code is %d", aobj1));
          goto _L4
        jsonexception;
_L10:
        jsonexception.printStackTrace();
        if(bufferedreader != null)
            try {
                bufferedreader.close();
            }
            catch(IOException ioexception3) {
                ioexception3.printStackTrace();
            }
        if(httpurlconnection != null)
            httpurlconnection.disconnect();
        break MISSING_BLOCK_LABEL_166;
        IllegalBlockSizeException illegalblocksizeexception;
        illegalblocksizeexception;
_L9:
        illegalblocksizeexception.printStackTrace();
        if(bufferedreader != null)
            try {
                bufferedreader.close();
            }
            catch(IOException ioexception2) {
                ioexception2.printStackTrace();
            }
        if(httpurlconnection != null)
            httpurlconnection.disconnect();
          goto _L5
        BadPaddingException badpaddingexception;
        badpaddingexception;
_L8:
        badpaddingexception.printStackTrace();
        if(bufferedreader != null)
            try {
                bufferedreader.close();
            }
            catch(IOException ioexception1) {
                ioexception1.printStackTrace();
            }
        if(httpurlconnection != null)
            httpurlconnection.disconnect();
          goto _L5
        Exception exception;
        exception;
_L7:
        if(bufferedreader != null)
            try {
                bufferedreader.close();
            }
            catch(IOException ioexception) {
                ioexception.printStackTrace();
            }
        if(httpurlconnection != null)
            httpurlconnection.disconnect();
        throw exception;
        exception;
        bufferedreader = bufferedreader1;
        if(true) goto _L7; else goto _L6
_L6:
        badpaddingexception;
        bufferedreader = bufferedreader1;
          goto _L8
        illegalblocksizeexception;
        bufferedreader = bufferedreader1;
          goto _L9
        jsonexception;
        bufferedreader = bufferedreader1;
          goto _L10
        ioexception4;
        if(true) goto _L12; else goto _L11
_L11:
    }

    public static final int CHUNK_SIZE_2G = 20480;
    public static final int CHUNK_SIZE_3G = 51200;
    public static final int CHUNK_SIZE_WIFI = 0x19000;
    public static final int HTTP_REQUEST_DELAY_MS = 5000;
    public static final int HTTP_REQUEST_TIMEOUT_MS = 30000;
    protected static final String URL = "http://fileapi.micloud.xiaomi.net";
    private final Map mExtParams = Maps.newHashMap();
}
