// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.net.micloudrichmedia;

import android.text.TextUtils;
import java.io.IOException;
import java.io.RandomAccessFile;
import miui.net.CloudCoder;

// Referenced classes of package miui.net.micloudrichmedia:
//            UploadEntity

public class UploadFile extends UploadEntity {

    public UploadFile(String s, String s1, String s2) throws IOException {
        super(s1, s2);
        if(TextUtils.isEmpty(s))
            throw new IllegalArgumentException("The upload file parameters should not be null");
        mFilePath = s;
        super.mHexDigest = CloudCoder.getFileSha1Digest(mFilePath);
        if(super.mHexDigest == null)
            throw new IOException("Calculate file sha-1 digest error");
        else
            return;
    }

    void close() throws IOException {
        if(mFile != null)
            mFile.close();
        mFile = null;
    }

    byte[] getData(int i) throws IOException {
        byte abyte0[];
        if(mFile != null && (long)super.mOffset < mFile.length()) {
            if(i < getLength() - super.mOffset)
                abyte0 = new byte[i];
            else
                abyte0 = new byte[getLength() - super.mOffset];
            mFile.seek(super.mOffset);
            mFile.read(abyte0);
            super.mOffset = super.mOffset + abyte0.length;
        } else {
            abyte0 = null;
        }
        return abyte0;
    }

    int getLength() throws IOException {
        int i;
        if(mFile == null)
            i = 0;
        else
            i = (int)mFile.length();
        return i;
    }

    void open() throws IOException {
        mFile = new RandomAccessFile(mFilePath, "r");
    }

    private RandomAccessFile mFile;
    private String mFilePath;
}
