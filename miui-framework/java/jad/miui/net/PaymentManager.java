// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.net;

import android.accounts.*;
import android.app.Activity;
import android.content.*;
import android.os.*;
import android.text.TextUtils;
import android.util.Log;
import java.io.IOException;
import java.security.InvalidParameterException;
import java.util.concurrent.*;
import miui.net.exception.AuthenticationFailureException;
import miui.net.exception.OperationCancelledException;
import miui.net.exception.PaymentServiceFailureException;

// Referenced classes of package miui.net:
//            IPaymentManagerResponse, IPaymentManagerService

public class PaymentManager {
    public static interface PaymentManagerFuture {

        public abstract boolean cancel(boolean flag);

        public abstract Object getResult() throws IOException, OperationCancelledException, AuthenticationFailureException, PaymentServiceFailureException;

        public abstract Object getResult(long l, TimeUnit timeunit) throws IOException, OperationCancelledException, AuthenticationFailureException, PaymentServiceFailureException;

        public abstract boolean isCancelled();

        public abstract boolean isDone();
    }

    private abstract class PmsTask extends FutureTask
        implements PaymentManagerFuture, ServiceConnection {
        class IPaymentManagerResponseImpl extends IPaymentManagerResponse.Stub {

            public void onError(int i, String s) throws RemoteException {
                if(i == 4) {
                    cancel(true);
                    unBind();
                } else {
                    setException(convertErrorCodeToException(i, s));
                }
            }

            public void onResult(Bundle bundle) throws RemoteException {
                set(bundle);
            }

            final PmsTask this$1;

            IPaymentManagerResponseImpl() {
                this$1 = PmsTask.this;
                super();
            }
        }


        private Exception convertErrorCodeToException(int i, String s) {
            Object obj;
            if(i == 3)
                obj = new IOException(s);
            else
            if(i == 5) {
                obj = new AuthenticationFailureException(s);
            } else {
                if(TextUtils.isEmpty(s))
                    s = "Unknown payment failure";
                obj = new PaymentServiceFailureException(i, s);
            }
            return ((Exception) (obj));
        }

        private void ensureNotOnMainThread() {
            Looper looper = Looper.myLooper();
            if(looper != null && looper == mContext.getMainLooper()) {
                IllegalStateException illegalstateexception = new IllegalStateException("calling this from your main thread can lead to deadlock");
                Log.e("PaymentManager", "calling this from your main thread can lead to deadlock and/or ANRs", illegalstateexception);
                throw illegalstateexception;
            } else {
                return;
            }
        }

        private Bundle internalGetResult(Long long1, TimeUnit timeunit) throws IOException, OperationCancelledException, AuthenticationFailureException, PaymentServiceFailureException {
            if(!isDone())
                ensureNotOnMainThread();
            if(long1 != null) goto _L2; else goto _L1
_L1:
            Bundle bundle = (Bundle)get();
            cancel(true);
_L4:
            return bundle;
_L2:
            bundle = (Bundle)get(long1.longValue(), timeunit);
            cancel(true);
            if(true) goto _L4; else goto _L3
_L3:
            CancellationException cancellationexception;
            cancellationexception;
            throw new OperationCancelledException("cancelled by user");
            Exception exception;
            exception;
            cancel(true);
            throw exception;
            TimeoutException timeoutexception;
            timeoutexception;
            cancel(true);
_L6:
            throw new OperationCancelledException("cancelled by exception");
            InterruptedException interruptedexception;
            interruptedexception;
            cancel(true);
            if(true) goto _L6; else goto _L5
_L5:
            ExecutionException executionexception;
            executionexception;
            Throwable throwable = executionexception.getCause();
            if(throwable instanceof IOException)
                throw (IOException)throwable;
            if(throwable instanceof PaymentServiceFailureException)
                throw (PaymentServiceFailureException)throwable;
            if(throwable instanceof AuthenticationFailureException)
                throw (AuthenticationFailureException)throwable;
            if(throwable instanceof RuntimeException)
                throw (RuntimeException)throwable;
            if(throwable instanceof Error)
                throw (Error)throwable;
            else
                throw new IllegalStateException(throwable);
        }

        protected void bind() {
            if(!bindToPaymentService())
                setException(new PaymentServiceFailureException(1, "bind to service failed"));
        }

        protected boolean bindToPaymentService() {
            Intent intent = new Intent("com.xiaomi.xmsf.action.PAYMENT");
            return mContext.bindService(intent, this, 1);
        }

        protected abstract void doWork() throws RemoteException;

        protected void done() {
            if(mCallback != null) {
                Handler handler;
                if(mHandler == null)
                    handler = mMainHandler;
                else
                    handler = mHandler;
                handler.post(new Runnable() {

                    public void run() {
                        mCallback.run(PmsTask.this);
                    }

                    final PmsTask this$1;

                 {
                    this$1 = PmsTask.this;
                    super();
                }
                });
            }
        }

        protected IPaymentManagerResponse getResponse() {
            return mResponse;
        }

        public Bundle getResult() throws IOException, OperationCancelledException, AuthenticationFailureException, PaymentServiceFailureException {
            return internalGetResult(null, null);
        }

        public Bundle getResult(long l, TimeUnit timeunit) throws IOException, OperationCancelledException, AuthenticationFailureException, PaymentServiceFailureException {
            return internalGetResult(Long.valueOf(l), timeunit);
        }

        public volatile Object getResult() throws IOException, OperationCancelledException, AuthenticationFailureException, PaymentServiceFailureException {
            return getResult();
        }

        public volatile Object getResult(long l, TimeUnit timeunit) throws IOException, OperationCancelledException, AuthenticationFailureException, PaymentServiceFailureException {
            return getResult(l, timeunit);
        }

        protected IPaymentManagerService getService() {
            return mService;
        }

        public void onServiceConnected(ComponentName componentname, IBinder ibinder) {
            Log.d("PaymentManager", (new StringBuilder()).append("onServiceConnected, component:").append(componentname).toString());
            mService = IPaymentManagerService.Stub.asInterface(ibinder);
            doWork();
_L1:
            return;
            RemoteException remoteexception;
            remoteexception;
            setException(remoteexception);
              goto _L1
        }

        public void onServiceDisconnected(ComponentName componentname) {
            if(!isDone()) {
                Log.e("PaymentManager", "payment service disconnected, but task is not completed");
                setException(new PaymentServiceFailureException(1, "active service exits unexpectedly"));
            }
            mService = null;
        }

        protected void set(Bundle bundle) {
            super.set(bundle);
            unBind();
        }

        protected volatile void set(Object obj) {
            set((Bundle)obj);
        }

        protected void setException(Throwable throwable) {
            super.setException(throwable);
            unBind();
        }

        public final PaymentManagerFuture start() {
            bind();
            return this;
        }

        protected void unBind() {
            mContext.unbindService(this);
            Log.d("PaymentManager", "service unbinded");
        }

        private final PaymentManagerCallback mCallback;
        private final Handler mHandler;
        private IPaymentManagerResponse mResponse;
        private IPaymentManagerService mService;
        final PaymentManager this$0;



        protected PmsTask(Handler handler, PaymentManagerCallback paymentmanagercallback) {
            this.this$0 = PaymentManager.this;
            super(new Callable() {

                public Bundle call() throws Exception {
                    throw new IllegalStateException("this should never be called");
                }

                public volatile Object call() throws Exception {
                    return call();
                }

                final PaymentManager val$this$0;

                 {
                    this$0 = paymentmanager;
                    super();
                }
            });
            mHandler = handler;
            mCallback = paymentmanagercallback;
            mResponse = new IPaymentManagerResponseImpl();
        }
    }

    private class PaymentCallback
        implements PaymentManagerCallback {

        public void run(PaymentManagerFuture paymentmanagerfuture) {
            if(mPaymentListener != null) goto _L2; else goto _L1
_L1:
            return;
_L2:
            Bundle bundle = (Bundle)paymentmanagerfuture.getResult();
            IOException ioexception;
            if(bundle != null) {
                mPaymentListener.onSuccess(mPaymentId, bundle);
                continue; /* Loop/switch isn't completed */
            }
            try {
                mPaymentListener.onFailed(mPaymentId, 1, "pay failed");
            }
            // Misplaced declaration of an exception variable
            catch(IOException ioexception) {
                mPaymentListener.onFailed(mPaymentId, 3, ioexception.getMessage());
            }
            catch(OperationCancelledException operationcancelledexception) {
                mPaymentListener.onFailed(mPaymentId, 4, operationcancelledexception.getMessage());
            }
            catch(AuthenticationFailureException authenticationfailureexception) {
                mPaymentListener.onFailed(mPaymentId, 5, authenticationfailureexception.getMessage());
            }
            catch(PaymentServiceFailureException paymentservicefailureexception) {
                mPaymentListener.onFailed(mPaymentId, paymentservicefailureexception.getError(), paymentservicefailureexception.getMessage());
            }
            if(true) goto _L1; else goto _L3
_L3:
        }

        private String mPaymentId;
        private PaymentListener mPaymentListener;
        private String mServiceId;
        final PaymentManager this$0;

        public PaymentCallback(String s, String s1, PaymentListener paymentlistener) {
            this$0 = PaymentManager.this;
            super();
            mServiceId = s;
            mPaymentId = s1;
            mPaymentListener = paymentlistener;
        }
    }

    private static interface PaymentManagerCallback {

        public abstract void run(PaymentManagerFuture paymentmanagerfuture);
    }

    public static interface PaymentListener {

        public abstract void onFailed(String s, int i, String s1);

        public abstract void onSuccess(String s, Bundle bundle);
    }

    private abstract class AddAccountCallback
        implements AccountManagerCallback {

        protected abstract void onFailed(int i, String s);

        protected abstract void onSuccess(Account account);

        public void run(AccountManagerFuture accountmanagerfuture) {
            if(!accountmanagerfuture.isDone())
                break MISSING_BLOCK_LABEL_40;
            Bundle bundle = (Bundle)accountmanagerfuture.getResult();
            IOException ioexception;
            AuthenticatorException authenticatorexception;
            OperationCanceledException operationcanceledexception;
            if(bundle == null) {
                Log.d("PaymentManager", "login failed : authentication failed");
                onFailed(5, "authentication failed");
            } else {
                Account aaccount[] = mAccountManager.getAccountsByType("com.xiaomi");
                if(aaccount.length == 0) {
                    Log.d("PaymentManager", "login failed : authentication failed");
                    onFailed(5, "authentication failed");
                } else {
                    onSuccess(aaccount[0]);
                }
            }
            return;
            operationcanceledexception;
            Log.d("PaymentManager", (new StringBuilder()).append("login failed : user canceled ").append(operationcanceledexception).toString());
            onFailed(4, operationcanceledexception.getMessage());
            continue; /* Loop/switch isn't completed */
            authenticatorexception;
            Log.d("PaymentManager", (new StringBuilder()).append("login failed : authenticator exception ").append(authenticatorexception).toString());
            onFailed(5, authenticatorexception.getMessage());
            continue; /* Loop/switch isn't completed */
            ioexception;
            Log.d("PaymentManager", (new StringBuilder()).append("login failed : io exception ").append(ioexception).toString());
            onFailed(3, ioexception.getMessage());
            if(true) goto _L2; else goto _L1
_L1:
            break MISSING_BLOCK_LABEL_160;
_L2:
            break MISSING_BLOCK_LABEL_40;
        }

        final PaymentManager this$0;

        private AddAccountCallback() {
            this$0 = PaymentManager.this;
            super();
        }

    }


    private PaymentManager(Context context) {
        mContext = context.getApplicationContext();
        mAccountManager = AccountManager.get(mContext);
        mMainHandler = new Handler(mContext.getMainLooper());
    }

    public static PaymentManager get(Context context) {
        return new PaymentManager(context);
    }

    private PaymentManagerFuture internalPay(final Account account, String s, String s1, final Bundle extra, final PaymentManagerCallback final_paymentmanagercallback, final Handler final_handler) {
        return (new PmsTask(s, s1) {

            protected void doWork() throws RemoteException {
                IPaymentManagerService ipaymentmanagerservice = getService();
                Bundle bundle = new Bundle();
                if(extra != null)
                    bundle.putAll(extra);
                ipaymentmanagerservice.pay(getResponse(), account, serviceId, productId, bundle);
            }

            final PaymentManager this$0;
            final Account val$account;
            final Bundle val$extra;
            final String val$productId;
            final String val$serviceId;

             {
                this$0 = PaymentManager.this;
                extra = bundle;
                account = account1;
                serviceId = s;
                productId = s1;
                super(final_handler, final_paymentmanagercallback);
            }
        }).start();
    }

    private PaymentManagerFuture internalPayForOrder(Account account, String s, final Bundle extra, final PaymentManagerCallback final_paymentmanagercallback, final Handler final_handler) {
        return (new PmsTask(account, s) {

            protected void doWork() throws RemoteException {
                IPaymentManagerService ipaymentmanagerservice = getService();
                Bundle bundle = new Bundle();
                if(extra != null)
                    bundle.putAll(extra);
                ipaymentmanagerservice.payForOrder(getResponse(), account, order, bundle);
            }

            final PaymentManager this$0;
            final Account val$account;
            final Bundle val$extra;
            final String val$order;

             {
                this$0 = PaymentManager.this;
                extra = bundle;
                account = account1;
                order = s;
                super(final_handler, final_paymentmanagercallback);
            }
        }).start();
    }

    public void gotoMiliCenter(final Activity activity) {
        Account aaccount[] = mAccountManager.getAccountsByType("com.xiaomi");
        if(aaccount.length == 0) {
            AddAccountCallback addaccountcallback = new AddAccountCallback() {

                protected void onFailed(int i, String s) {
                }

                protected void onSuccess(Account account) {
                    Intent intent1 = new Intent("com.xiaomi.xmsf.action.MILICENTER");
                    intent1.putExtra("account", account);
                    intent1.addFlags(0x10000000);
                    activity.startActivity(intent1);
                }

                final PaymentManager this$0;
                final Activity val$activity;

             {
                this$0 = PaymentManager.this;
                activity = activity1;
                super();
            }
            };
            mAccountManager.addAccount("com.xiaomi", "billcenter", null, null, activity, addaccountcallback, null);
        } else {
            Intent intent = new Intent("com.xiaomi.xmsf.action.MILICENTER");
            intent.putExtra("account", aaccount[0]);
            intent.addFlags(0x10000000);
            activity.startActivity(intent);
        }
    }

    public void pay(Activity activity, final String serviceId, final String productId, final Bundle extra, final PaymentListener paymentListener) {
        if(activity == null)
            throw new InvalidParameterException("activity cannot be null");
        if(TextUtils.isEmpty(serviceId))
            throw new InvalidParameterException("service id cannot be empty");
        if(TextUtils.isEmpty(productId))
            throw new InvalidParameterException("product id cannot be empty");
        Account aaccount[] = mAccountManager.getAccountsByType("com.xiaomi");
        if(aaccount.length == 0) {
            AddAccountCallback addaccountcallback = new AddAccountCallback() {

                protected void onFailed(int i, String s) {
                    paymentListener.onFailed(productId, i, s);
                }

                protected void onSuccess(Account account) {
                    PaymentCallback paymentcallback1 = new PaymentCallback(serviceId, productId, paymentListener);
                    internalPay(account, serviceId, productId, extra, paymentcallback1, null);
                }

                final PaymentManager this$0;
                final Bundle val$extra;
                final PaymentListener val$paymentListener;
                final String val$productId;
                final String val$serviceId;

             {
                this$0 = PaymentManager.this;
                serviceId = s;
                productId = s1;
                paymentListener = paymentlistener;
                extra = bundle;
                super();
            }
            };
            mAccountManager.addAccount("com.xiaomi", "billcenter", null, null, activity, addaccountcallback, null);
        } else {
            PaymentCallback paymentcallback = new PaymentCallback(serviceId, productId, paymentListener);
            internalPay(aaccount[0], serviceId, productId, extra, paymentcallback, null);
        }
    }

    public void payForOrder(Activity activity, final String paymentId, final String order, final Bundle extra, final PaymentListener paymentListener) {
        if(activity == null)
            throw new InvalidParameterException("activity cannot be null");
        if(TextUtils.isEmpty(paymentId))
            throw new InvalidParameterException("paymentId cannot be empty");
        Account aaccount[] = mAccountManager.getAccountsByType("com.xiaomi");
        if(aaccount.length == 0) {
            AddAccountCallback addaccountcallback = new AddAccountCallback() {

                protected void onFailed(int i, String s) {
                    paymentListener.onFailed(paymentId, i, s);
                }

                protected void onSuccess(Account account) {
                    PaymentCallback paymentcallback1 = new PaymentCallback("thd", paymentId, paymentListener);
                    internalPayForOrder(account, order, extra, paymentcallback1, null);
                }

                final PaymentManager this$0;
                final Bundle val$extra;
                final String val$order;
                final String val$paymentId;
                final PaymentListener val$paymentListener;

             {
                this$0 = PaymentManager.this;
                paymentId = s;
                paymentListener = paymentlistener;
                order = s1;
                extra = bundle;
                super();
            }
            };
            mAccountManager.addAccount("com.xiaomi", "billcenter", null, null, activity, addaccountcallback, null);
        } else {
            PaymentCallback paymentcallback = new PaymentCallback("thd", paymentId, paymentListener);
            internalPayForOrder(aaccount[0], order, extra, paymentcallback, null);
        }
    }

    private static final boolean DEBUG = true;
    public static final int ERROR_CODE_AUTHENTICATION_ERROR = 5;
    public static final int ERROR_CODE_CANCELED = 4;
    public static final int ERROR_CODE_DUPLICATE_PURCHASE = 7;
    public static final int ERROR_CODE_EXCEPTION = 1;
    public static final int ERROR_CODE_INVALID_PARAMS = 2;
    public static final int ERROR_CODE_NETWORK_ERROR = 3;
    public static final int ERROR_CODE_SERVER_ERROR = 6;
    public static final int ERROR_CODE_USER_ID_MISMATCH = 8;
    public static final String KEY_ACCOUNT = "account";
    public static final String KEY_INTENT = "intent";
    public static final String PAYMENT_KEY_ORDER_ID = "payment_order_id";
    public static final String PAYMENT_KEY_ORDER_PRICE = "payment_order_price";
    public static final String PAYMENT_KEY_ORDER_TITLE = "payment_order_title";
    public static final String PAYMENT_KEY_PAYMENT_RESULT = "payment_payment_result";
    public static final String PAYMENT_KEY_PRODUCT_ID = "payment_product_id";
    public static final String PAYMENT_KEY_QUICK_PAYMENT = "payment_quick_payment";
    public static final String PAYMENT_KEY_SERVICE_ID = "payment_service_id";
    public static final String PAYMENT_KEY_TRADE_BALANCE = "payment_trade_balance";
    public static final String PAYMENT_KEY_TRADE_ID = "payment_trade_id";
    public static final String PAYMENT_KEY_TRADE_PRICE = "payment_trade_price";
    private static final String TAG = "PaymentManager";
    public static final String URL_PAYMENT_BASE = "https://billapi.xiaomi.com/";
    public static final String URL_STAGING_PAYMENT_BASE = "http://staging.billapi.n.xiaomi.com/";
    public static final String XIAOMI_ACCOUNT_TYPE = "com.xiaomi";
    public static final String XIAOMI_PAYMENT_AUTH_TOKEN_TYPE = "billcenter";
    public static final String XIAOMI_STAGING_PAYMENT_AUTH_TOKEN_TYPE = "sbillcenter";
    private final AccountManager mAccountManager;
    private final Context mContext;
    private final Handler mMainHandler;





}
