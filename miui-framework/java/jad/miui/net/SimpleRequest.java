// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.net;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.logging.Logger;
import miui.net.exception.AccessDeniedException;
import miui.net.exception.AuthenticationFailureException;
import miui.util.IOUtils;
import miui.util.ObjectUtils;
import org.apache.http.client.utils.URLEncodedUtils;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package miui.net:
//            CloudManager

public final class SimpleRequest {
    public static class StreamContent extends HeaderContent {

        public InputStream getStream() {
            return stream;
        }

        private InputStream stream;

        public StreamContent(InputStream inputstream) {
            stream = inputstream;
        }
    }

    public static class MapContent extends HeaderContent {

        public Object getFromBody(String s) {
            return bodies.get(s);
        }

        public String toString() {
            return (new StringBuilder()).append("MapContent{bodies=").append(bodies).append('}').toString();
        }

        private Map bodies;

        public MapContent(Map map) {
            bodies = map;
        }
    }

    public static class StringContent extends HeaderContent {

        public String getBody() {
            return body;
        }

        public String toString() {
            return (new StringBuilder()).append("StringContent{body='").append(body).append('\'').append('}').toString();
        }

        private String body;

        public StringContent(String s) {
            body = s;
        }
    }

    public static class HeaderContent {

        public String getHeader(String s) {
            return (String)headers.get(s);
        }

        public Map getHeaders() {
            return headers;
        }

        public void putHeader(String s, String s1) {
            headers.put(s, s1);
        }

        public void putHeaders(Map map) {
            headers.putAll(map);
        }

        public String toString() {
            return (new StringBuilder()).append("HeaderContent{headers=").append(headers).append('}').toString();
        }

        private final Map headers = new HashMap();

        public HeaderContent() {
        }
    }


    public SimpleRequest() {
    }

    protected static String appendUrl(String s, List list) {
        if(s == null)
            throw new NullPointerException("origin is not allowed null");
        StringBuilder stringbuilder = new StringBuilder(s);
        if(list != null) {
            String s1 = URLEncodedUtils.format(list, "utf-8");
            if(s1 != null && s1.length() > 0) {
                if(s.contains("?"))
                    stringbuilder.append("&");
                else
                    stringbuilder.append("?");
                stringbuilder.append(s1);
            }
        }
        return stringbuilder.toString();
    }

    protected static MapContent convertStringToMap(StringContent stringcontent) {
        MapContent mapcontent = null;
        if(stringcontent != null) goto _L2; else goto _L1
_L1:
        return mapcontent;
_L2:
        String s;
        JSONObject jsonobject;
        s = stringcontent.getBody();
        jsonobject = null;
        JSONObject jsonobject1 = new JSONObject(s);
        jsonobject = jsonobject1;
_L4:
        if(jsonobject != null) {
            mapcontent = new MapContent(ObjectUtils.jsonToMap(jsonobject));
            mapcontent.putHeaders(stringcontent.getHeaders());
        }
        if(true) goto _L1; else goto _L3
_L3:
        JSONException jsonexception;
        jsonexception;
        jsonexception.printStackTrace();
          goto _L4
    }

    public static MapContent getAsMap(String s, Map map, Map map1, boolean flag) throws IOException, AccessDeniedException, AuthenticationFailureException {
        return convertStringToMap(getAsString(s, map, map1, flag));
    }

    public static StringContent getAsString(String s, Map map, Map map1, boolean flag) throws IOException, AccessDeniedException, AuthenticationFailureException {
        String s1;
        HttpURLConnection httpurlconnection;
        s1 = appendUrl(s, ObjectUtils.mapToPairs(map));
        httpurlconnection = makeConn(s1, map1);
        if(httpurlconnection != null) goto _L2; else goto _L1
_L1:
        StringContent stringcontent;
        log.severe("failed to create URLConnection");
        stringcontent = null;
_L6:
        return stringcontent;
_L2:
        int i;
        Map map3;
        StringBuilder stringbuilder;
        httpurlconnection.setDoInput(true);
        httpurlconnection.setRequestMethod("GET");
        httpurlconnection.connect();
        i = httpurlconnection.getResponseCode();
        if(i != 200 && i != 302)
            break MISSING_BLOCK_LABEL_280;
        Map map2 = httpurlconnection.getHeaderFields();
        CookieManager cookiemanager = new CookieManager();
        URI uri = URI.create(s1);
        cookiemanager.put(uri, map2);
        map3 = parseCookies(cookiemanager.getCookieStore().get(uri));
        map3.putAll(ObjectUtils.listToMap(map2));
        stringbuilder = new StringBuilder();
        if(!flag) goto _L4; else goto _L3
_L3:
        BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(httpurlconnection.getInputStream()), 1024);
        do {
            String s2 = bufferedreader.readLine();
            if(s2 == null)
                break;
            stringbuilder.append(s2);
        } while(true);
          goto _L5
        IOException ioexception;
        ioexception;
        ioexception.printStackTrace();
        bufferedreader.close();
_L4:
        stringcontent = new StringContent(stringbuilder.toString());
        stringcontent.putHeaders(map3);
        httpurlconnection.disconnect();
          goto _L6
_L5:
        bufferedreader.close();
          goto _L4
        ProtocolException protocolexception;
        protocolexception;
        throw new IOException("protocol error");
        Exception exception;
        exception;
        httpurlconnection.disconnect();
        throw exception;
        Exception exception1;
        exception1;
        bufferedreader.close();
        throw exception1;
        if(i == 403)
            throw new AccessDeniedException("access denied, encrypt error or user is forbidden to access the resource");
          goto _L7
_L8:
        throw new AuthenticationFailureException((new StringBuilder()).append("authentication failure for get, code: ").append(i).toString());
_L9:
        log.info((new StringBuilder()).append("http status error when GET: ").append(i).toString());
        httpurlconnection.disconnect();
        stringcontent = null;
          goto _L6
_L7:
        if(i != 401 && i != 400) goto _L9; else goto _L8
    }

    protected static String joinMap(Map map, String s) {
        String s1;
        if(map == null) {
            s1 = null;
        } else {
            StringBuilder stringbuilder = new StringBuilder();
            Set set = map.entrySet();
            int i = 0;
            for(Iterator iterator = set.iterator(); iterator.hasNext();) {
                java.util.Map.Entry entry = (java.util.Map.Entry)iterator.next();
                if(i > 0)
                    stringbuilder.append(s);
                String s2 = (String)entry.getKey();
                String s3 = (String)entry.getValue();
                stringbuilder.append(s2);
                stringbuilder.append("=");
                stringbuilder.append(s3);
                i++;
            }

            s1 = stringbuilder.toString();
        }
        return s1;
    }

    protected static HttpURLConnection makeConn(String s, Map map) {
        URL url = null;
        URL url1 = new URL(s);
        url = url1;
_L1:
        HttpURLConnection httpurlconnection;
        MalformedURLException malformedurlexception;
        if(url == null) {
            log.severe("failed to init url");
            httpurlconnection = null;
        } else {
            try {
                httpurlconnection = (HttpURLConnection)url.openConnection();
                httpurlconnection.setInstanceFollowRedirects(false);
                httpurlconnection.setConnectTimeout(30000);
                httpurlconnection.setUseCaches(false);
                httpurlconnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                httpurlconnection.setRequestProperty("User-Agent", CloudManager.getUserAgent());
                if(map != null)
                    httpurlconnection.setRequestProperty("Cookie", joinMap(map, "; "));
            }
            catch(Exception exception) {
                exception.printStackTrace();
                httpurlconnection = null;
            }
        }
        return httpurlconnection;
        malformedurlexception;
        malformedurlexception.printStackTrace();
          goto _L1
    }

    protected static Map parseCookies(List list) {
        HashMap hashmap = new HashMap();
        Iterator iterator = list.iterator();
        do {
            if(!iterator.hasNext())
                break;
            HttpCookie httpcookie = (HttpCookie)iterator.next();
            if(!httpcookie.hasExpired()) {
                String s = httpcookie.getName();
                String s1 = httpcookie.getValue();
                if(s != null)
                    hashmap.put(s, s1);
            }
        } while(true);
        return hashmap;
    }

    public static MapContent postAsMap(String s, Map map, Map map1, boolean flag) throws IOException, AccessDeniedException, AuthenticationFailureException {
        return convertStringToMap(postAsString(s, map, map1, flag));
    }

    public static StringContent postAsString(String s, Map map, Map map1, boolean flag) throws IOException, AccessDeniedException, AuthenticationFailureException {
        HttpURLConnection httpurlconnection = makeConn(s, map1);
        if(httpurlconnection != null) goto _L2; else goto _L1
_L1:
        StringContent stringcontent;
        log.severe("failed to create URLConnection");
        stringcontent = null;
_L6:
        return stringcontent;
_L2:
        String s2;
        BufferedOutputStream bufferedoutputstream;
        httpurlconnection.setDoInput(true);
        httpurlconnection.setDoOutput(true);
        httpurlconnection.setRequestMethod("POST");
        httpurlconnection.connect();
        List list = ObjectUtils.mapToPairs(map);
        if(list == null)
            break MISSING_BLOCK_LABEL_102;
        s2 = URLEncodedUtils.format(list, "utf-8");
        bufferedoutputstream = new BufferedOutputStream(httpurlconnection.getOutputStream());
        bufferedoutputstream.write(s2.getBytes("utf-8"));
        IOUtils.closeQuietly(bufferedoutputstream);
_L7:
        int i;
        Map map3;
        StringBuilder stringbuilder;
        i = httpurlconnection.getResponseCode();
        if(i != 200 && i != 302)
            break MISSING_BLOCK_LABEL_351;
        Map map2 = httpurlconnection.getHeaderFields();
        CookieManager cookiemanager = new CookieManager();
        URI uri = URI.create(s);
        cookiemanager.put(uri, map2);
        map3 = parseCookies(cookiemanager.getCookieStore().get(uri));
        map3.putAll(ObjectUtils.listToMap(map2));
        stringbuilder = new StringBuilder();
        if(!flag) goto _L4; else goto _L3
_L3:
        BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(httpurlconnection.getInputStream()), 1024);
        do {
            String s1 = bufferedreader.readLine();
            if(s1 == null)
                break;
            stringbuilder.append(s1);
        } while(true);
          goto _L5
        IOException ioexception;
        ioexception;
        ioexception.printStackTrace();
        IOUtils.closeQuietly(bufferedreader);
_L4:
        stringcontent = new StringContent(stringbuilder.toString());
        stringcontent.putHeaders(map3);
        httpurlconnection.disconnect();
          goto _L6
        Exception exception3;
        exception3;
        exception3.printStackTrace();
        IOUtils.closeQuietly(bufferedoutputstream);
          goto _L7
        ProtocolException protocolexception;
        protocolexception;
        throw new IOException("protocol error");
        Exception exception;
        exception;
        httpurlconnection.disconnect();
        throw exception;
        Exception exception2;
        exception2;
        IOUtils.closeQuietly(bufferedoutputstream);
        throw exception2;
_L5:
        IOUtils.closeQuietly(bufferedreader);
          goto _L4
        Exception exception1;
        exception1;
        IOUtils.closeQuietly(bufferedreader);
        throw exception1;
        if(i == 403)
            throw new AccessDeniedException("access denied, encrypt error or user is forbidden to access the resource");
          goto _L8
_L9:
        throw new AuthenticationFailureException((new StringBuilder()).append("authentication failure for post, code: ").append(i).toString());
_L10:
        log.info((new StringBuilder()).append("http status error when POST: ").append(i).toString());
        httpurlconnection.disconnect();
        stringcontent = null;
          goto _L6
_L8:
        if(i != 401 && i != 400) goto _L10; else goto _L9
    }

    private static final boolean DEBUG = false;
    private static final int TIMEOUT = 30000;
    public static final String UTF8 = "utf-8";
    private static final Logger log = Logger.getLogger(miui/net/SimpleRequest.getSimpleName());

}
