// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.net;

import android.util.Base64;
import java.io.IOException;
import java.util.*;
import javax.crypto.Cipher;
import miui.net.exception.AccessDeniedException;
import miui.net.exception.AuthenticationFailureException;
import miui.net.exception.CipherException;
import miui.net.exception.InvalidResponseException;
import miui.util.EasyMap;

// Referenced classes of package miui.net:
//            CloudCoder, SimpleRequest

public class SecureRequest {

    public SecureRequest() {
    }

    public static String decryptResponse(String s, String s1) throws CipherException, InvalidResponseException {
        Cipher cipher;
        String s2;
        cipher = CloudCoder.newAESCipher(s1, 2);
        if(cipher == null)
            throw new CipherException("failed to init cipher");
        s2 = null;
        String s3 = new String(cipher.doFinal(Base64.decode(s, 2)), "utf-8");
        s2 = s3;
_L2:
        if(s2 == null)
            throw new InvalidResponseException("failed to decrypt response");
        else
            return s2;
        Exception exception;
        exception;
        if(true) goto _L2; else goto _L1
_L1:
    }

    public static Map encryptParams(String s, String s1, Map map, String s2) throws CipherException {
        Cipher cipher = CloudCoder.newAESCipher(s2, 1);
        if(cipher == null)
            throw new CipherException("failed to init cipher");
        EasyMap easymap = new EasyMap();
        if(map != null && !map.isEmpty()) {
            Iterator iterator = map.entrySet().iterator();
            do {
                if(!iterator.hasNext())
                    break;
                java.util.Map.Entry entry = (java.util.Map.Entry)iterator.next();
                String s3 = (String)entry.getKey();
                String s4 = (String)entry.getValue();
                if(s3 != null && s4 != null) {
                    if(!s3.startsWith("_")) {
                        String s5;
                        try {
                            s5 = Base64.encodeToString(cipher.doFinal(s4.getBytes("utf-8")), 2);
                        }
                        catch(Exception exception) {
                            throw new CipherException("failed to encrypt request params", exception);
                        }
                        s4 = s5;
                    }
                    easymap.easyPut(s3, s4);
                }
            } while(true);
        }
        easymap.easyPut("signature", CloudCoder.generateSignature(s, s1, easymap, s2));
        return easymap;
    }

    public static SimpleRequest.MapContent getAsMap(String s, Map map, Map map1, boolean flag, String s1) throws IOException, CipherException, AccessDeniedException, InvalidResponseException, AuthenticationFailureException {
        return SimpleRequest.convertStringToMap(getAsString(s, map, map1, flag, s1));
    }

    public static SimpleRequest.StringContent getAsString(String s, Map map, Map map1, boolean flag, String s1) throws IOException, CipherException, AccessDeniedException, InvalidResponseException, AuthenticationFailureException {
        return processStringResponse(SimpleRequest.getAsString(s, encryptParams("GET", s, map, s1), map1, flag), s1);
    }

    public static SimpleRequest.MapContent postAsMap(String s, Map map, Map map1, boolean flag, String s1) throws IOException, AccessDeniedException, InvalidResponseException, CipherException, AuthenticationFailureException {
        return SimpleRequest.convertStringToMap(postAsString(s, map, map1, flag, s1));
    }

    public static SimpleRequest.StringContent postAsString(String s, Map map, Map map1, boolean flag, String s1) throws IOException, CipherException, AccessDeniedException, InvalidResponseException, AuthenticationFailureException {
        return processStringResponse(SimpleRequest.postAsString(s, encryptParams("POST", s, map, s1), map1, flag), s1);
    }

    private static SimpleRequest.StringContent processStringResponse(SimpleRequest.StringContent stringcontent, String s) throws IOException, InvalidResponseException, CipherException {
        if(stringcontent == null)
            throw new IOException("no response from server");
        String s1 = stringcontent.getBody();
        if(s1 == null) {
            throw new InvalidResponseException("invalid response from server");
        } else {
            SimpleRequest.StringContent stringcontent1 = new SimpleRequest.StringContent(decryptResponse(s1, s));
            stringcontent1.putHeaders(stringcontent.getHeaders());
            return stringcontent1;
        }
    }

    private static final String UTF8 = "utf-8";
}
