// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.net;

import android.accounts.*;
import android.app.*;
import android.content.*;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import java.io.IOException;
import miui.net.exception.MiCloudServerException;

// Referenced classes of package miui.net:
//            ExtendedAuthToken

public abstract class MiCloudSyncAdapterBase extends AbstractThreadedSyncAdapter {

    public MiCloudSyncAdapterBase(Context context, boolean flag, String s) {
        super(context, flag);
        mContext = context;
        mResolver = context.getContentResolver();
        mAuthType = s;
    }

    private void handle5xx() {
        Log.w("MiCloudSyncAdapterBase", "Http 5xx error. Suspending sync.");
        suspendSync();
    }

    private void handleBadRequest() {
        Log.w("MiCloudSyncAdapterBase", "Http bad request error. Suspending sync.");
        suspendSync();
    }

    private void handleForbidden() {
        Log.w("MiCloudSyncAdapterBase", "Http forbidden error. Turning off sync.");
        turnOffSyncAndNotify(0x60c01f0);
    }

    private void handleNotAcceptable() {
        Log.w("MiCloudSyncAdapterBase", "Http not-acceptable error. Turniong off sync.");
        turnOffSyncAndNotify(0x60c01f1);
    }

    private void handleUnauthorized() {
        Object aobj[] = new Object[1];
        aobj[0] = mAuthority;
        String s = String.format("TokenExpiredCount_%s", aobj);
        Object aobj1[] = new Object[1];
        aobj1[0] = mAuthority;
        String s1 = String.format("TokenExpiredDay_%s", aobj1);
        SharedPreferences sharedpreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        long l = sharedpreferences.getLong(s1, 0L);
        long l1 = System.currentTimeMillis() / 0x5265c00L;
        int i = 0;
        if(l1 == l)
            i = sharedpreferences.getInt(s, 0);
        int j = i + 1;
        Log.w("MiCloudSyncAdapterBase", (new StringBuilder()).append("Http unauthorized error: ").append(j).append(" times today.").toString());
        if(j >= 100) {
            Log.w("MiCloudSyncAdapterBase", "Http unauthorized error reached limit. Turning off sync.");
            turnOffSyncAndNotify(0x60c01ef);
            j = 0;
        }
        android.content.SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putLong(s1, l1);
        editor.putInt(s, j);
        editor.commit();
    }

    private void suspendSync() {
        ContentResolver.cancelSync(mAccount, mAuthority);
        AlarmManager alarmmanager = (AlarmManager)mContext.getSystemService("alarm");
        long l = 0x493e0L + System.currentTimeMillis();
        Intent intent = new Intent("com.miui.net.RESUME_SYNC");
        intent.putExtra("authority", mAuthority);
        intent.putExtra("account", mAccount);
        alarmmanager.set(1, l, PendingIntent.getBroadcast(mContext, 0, intent, 0));
        SharedPreferences sharedpreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        Object aobj[] = new Object[1];
        aobj[0] = mAuthority;
        String s = String.format("ResumeSyncTime_%s", aobj);
        android.content.SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putLong(s, l);
        editor.commit();
    }

    private void turnOffSyncAndNotify(int i) {
        PackageManager packagemanager = mContext.getPackageManager();
        CharSequence charsequence = packagemanager.resolveContentProvider(mAuthority, 0).loadLabel(packagemanager);
        Intent intent = new Intent();
        intent.setAction((new StringBuilder()).append(mAuthority).append(".SYNC_SETTINGS").toString());
        intent.putExtra("authority", mAuthority);
        intent.putExtra("account", mAccount);
        PendingIntent pendingintent = PendingIntent.getActivity(mContext, 0, intent, 0);
        android.app.Notification.Builder builder = (new android.app.Notification.Builder(mContext)).setSmallIcon(0x108007c);
        Context context = mContext;
        Object aobj[] = new Object[1];
        aobj[0] = charsequence;
        android.app.Notification notification = builder.setContentTitle(context.getString(0x60c01ee, aobj)).setContentText(mContext.getString(i)).setContentIntent(pendingintent).setAutoCancel(true).getNotification();
        ((NotificationManager)mContext.getSystemService("notification")).notify(0, notification);
        AccountManager.get(mContext).invalidateAuthToken(mAccount.type, mExtTokenStr);
        ContentResolver.setSyncAutomatically(mAccount, mAuthority, false);
        ContentResolver.cancelSync(mAccount, mAuthority);
    }

    protected abstract void onPerformMiCloudSync() throws MiCloudServerException;

    public void onPerformSync(Account account, Bundle bundle, String s, ContentProviderClient contentproviderclient, SyncResult syncresult) {
        SharedPreferences sharedpreferences;
        String s1;
        long l;
        long l1;
        mAuthority = s;
        mAccount = account;
        mSyncResult = syncresult;
        sharedpreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        Object aobj[] = new Object[1];
        aobj[0] = mAuthority;
        s1 = String.format("ResumeSyncTime_%s", aobj);
        l = sharedpreferences.getLong(s1, 0L);
        l1 = l - System.currentTimeMillis();
        if(l1 <= 0L) goto _L2; else goto _L1
_L1:
        Log.v("MiCloudSyncAdapterBase", (new StringBuilder()).append("onPerformSync: ").append(mAuthority).append(" sync suspended. ").append(l1 / 1000L).append(" seconds to resume.").toString());
_L3:
        return;
_L2:
        AccountManager accountmanager;
        if(l != 0L) {
            Log.v("MiCloudSyncAdapterBase", (new StringBuilder()).append("onPerformSync: The suspension of ").append(mAuthority).append(" sync is expired now.").toString());
            android.content.SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putLong(s1, 0L);
            editor.commit();
        }
        accountmanager = AccountManager.get(getContext());
        OperationCanceledException operationcanceledexception;
        AccountManagerFuture accountmanagerfuture;
label0:
        {
            Log.v("MiCloudSyncAdapterBase", (new StringBuilder()).append("onPerformSync: getting auth token. authority: ").append(mAuthority).toString());
            accountmanagerfuture = accountmanager.getAuthToken(account, mAuthType, null, true, null, null);
            if(accountmanagerfuture != null)
                break label0;
            Log.e("MiCloudSyncAdapterBase", "onPerformSync: Null future.");
        }
          goto _L3
        try {
            if(accountmanagerfuture.getResult() != null)
                break MISSING_BLOCK_LABEL_321;
            Log.e("MiCloudSyncAdapterBase", "onPerformSync: Null future result.");
        }
        // Misplaced declaration of an exception variable
        catch(OperationCanceledException operationcanceledexception) {
            Log.e("MiCloudSyncAdapterBase", "onPerformSync", operationcanceledexception);
        }
        catch(AuthenticatorException authenticatorexception) {
            Log.e("MiCloudSyncAdapterBase", "onPerformSync", authenticatorexception);
        }
        catch(IOException ioexception) {
            SyncStats syncstats = mSyncResult.stats;
            syncstats.numIoExceptions = 1L + syncstats.numIoExceptions;
            Log.e("MiCloudSyncAdapterBase", "onPerformSync", ioexception);
        }
          goto _L3
        mExtTokenStr = ((Bundle)accountmanagerfuture.getResult()).getString("authtoken");
        if(mExtTokenStr != null)
            break MISSING_BLOCK_LABEL_418;
        SyncStats syncstats1 = mSyncResult.stats;
        syncstats1.numIoExceptions = 1L + syncstats1.numIoExceptions;
        Log.w("MiCloudSyncAdapterBase", "onPerformSync: No ext token string.");
          goto _L3
        Log.v("MiCloudSyncAdapterBase", "onPerformSync: Got extTokenStr.");
        mExtToken = ExtendedAuthToken.parse(mExtTokenStr);
        onPerformMiCloudSync();
          goto _L3
        MiCloudServerException micloudserverexception;
        micloudserverexception;
        Log.e("MiCloudSyncAdapterBase", "onPerformSync", micloudserverexception);
        switch(micloudserverexception.statusCode) {
        case 402: 
        case 404: 
        case 405: 
        default:
            if(micloudserverexception.statusCode / 100 == 5)
                handle5xx();
            else
                Log.e("MiCloudSyncAdapterBase", (new StringBuilder()).append("Unrecognized server error ").append(micloudserverexception.statusCode).toString());
            break;

        case 400: 
            handleBadRequest();
            break;

        case 401: 
            handleUnauthorized();
            break;

        case 403: 
            handleForbidden();
            break;

        case 406: 
            handleNotAcceptable();
            break;
        }
        if(true) goto _L3; else goto _L4
_L4:
    }

    public static final String ACTION_RESUME_SYNC = "com.miui.net.RESUME_SYNC";
    private static final int BAD_REQUEST_LIMIT_PER_DAY = 100;
    public static final String PREF_RESUME_SYNC_TIME = "ResumeSyncTime_%s";
    private static final String PREF_TOKEN_EXPIRED_COUNT = "TokenExpiredCount_%s";
    private static final String PREF_TOKEN_EXPIRED_DAY = "TokenExpiredDay_%s";
    private static final int RESUME_SYNC_INTERVAL = 0x493e0;
    private static final String TAG = "MiCloudSyncAdapterBase";
    protected Account mAccount;
    protected final String mAuthType;
    protected String mAuthority;
    protected Context mContext;
    protected ExtendedAuthToken mExtToken;
    protected String mExtTokenStr;
    protected ContentResolver mResolver;
    protected SyncResult mSyncResult;
}
