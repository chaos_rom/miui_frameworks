// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.net;

import android.net.Uri;
import android.text.TextUtils;
import android.util.Base64;
import java.io.*;
import java.security.*;
import java.util.*;
import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import miui.net.exception.AccessDeniedException;
import miui.net.exception.AuthenticationFailureException;
import miui.net.exception.CipherException;
import miui.net.exception.InvalidResponseException;
import miui.util.EasyMap;

// Referenced classes of package miui.net:
//            SecureRequest

public class CloudCoder {
    public static final class CIPHER_MODE extends Enum {

        public static CIPHER_MODE valueOf(String s) {
            return (CIPHER_MODE)Enum.valueOf(miui/net/CloudCoder$CIPHER_MODE, s);
        }

        public static CIPHER_MODE[] values() {
            return (CIPHER_MODE[])$VALUES.clone();
        }

        private static final CIPHER_MODE $VALUES[];
        public static final CIPHER_MODE DECRYPT;
        public static final CIPHER_MODE ENCRYPT;

        static  {
            ENCRYPT = new CIPHER_MODE("ENCRYPT", 0);
            DECRYPT = new CIPHER_MODE("DECRYPT", 1);
            CIPHER_MODE acipher_mode[] = new CIPHER_MODE[2];
            acipher_mode[0] = ENCRYPT;
            acipher_mode[1] = DECRYPT;
            $VALUES = acipher_mode;
        }

        private CIPHER_MODE(String s, int i) {
            super(s, i);
        }
    }


    public CloudCoder() {
    }

    public static String decodeString(String s, String s1, String s2) throws IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
        if(s2 == null)
            s2 = "UTF-8";
        byte abyte0[];
        try {
            abyte0 = Base64.decode(s1.getBytes(s2), 2);
        }
        catch(IllegalArgumentException illegalargumentexception) {
            throw new IllegalArgumentException((new StringBuilder()).append(illegalargumentexception.getMessage()).append(": ").append(s1.substring(0, Math.min(256, s1.length()))).toString());
        }
        return new String(newAESCipher(s, 2).doFinal(abyte0), s2);
    }

    public static byte[] encodeStream(String s, byte abyte0[]) throws IllegalBlockSizeException, BadPaddingException, IOException {
        CipherInputStream cipherinputstream = null;
        CipherInputStream cipherinputstream1;
        Cipher cipher = newRC4Cipher(randomRc4Key128(s), 1);
        cipherinputstream1 = new CipherInputStream(new ByteArrayInputStream(abyte0), cipher);
        byte abyte1[];
        abyte1 = new byte[abyte0.length];
        if(abyte0.length != cipherinputstream1.read(abyte1))
            throw new IOException("The encoded data length is not the same with original data");
          goto _L1
        Exception exception;
        exception;
        cipherinputstream = cipherinputstream1;
_L3:
        if(cipherinputstream != null)
            cipherinputstream.close();
        throw exception;
_L1:
        if(cipherinputstream1 != null)
            cipherinputstream1.close();
        return abyte1;
        exception;
        if(true) goto _L3; else goto _L2
_L2:
    }

    public static String encodeString(String s, String s1, String s2) throws IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
        Cipher cipher = newAESCipher(s, 1);
        if(s2 == null)
            s2 = "UTF-8";
        return Base64.encodeToString(cipher.doFinal(s1.getBytes(s2)), 2);
    }

    public static String generateSignature(String s, String s1, Map map, String s2) {
        if(TextUtils.isEmpty(s2))
            throw new InvalidParameterException("security is not nullable");
        ArrayList arraylist = new ArrayList();
        if(s != null)
            arraylist.add(s.toUpperCase());
        if(s1 != null)
            arraylist.add(Uri.parse(s1).getEncodedPath());
        if(map != null && !map.isEmpty()) {
            Object aobj[];
            for(Iterator iterator1 = (new TreeMap(map)).entrySet().iterator(); iterator1.hasNext(); arraylist.add(String.format("%s=%s", aobj))) {
                java.util.Map.Entry entry = (java.util.Map.Entry)iterator1.next();
                aobj = new Object[2];
                aobj[0] = entry.getKey();
                aobj[1] = entry.getValue();
            }

        }
        arraylist.add(s2);
        boolean flag = true;
        StringBuilder stringbuilder = new StringBuilder();
        for(Iterator iterator = arraylist.iterator(); iterator.hasNext();) {
            String s3 = (String)iterator.next();
            if(!flag)
                stringbuilder.append('&');
            stringbuilder.append(s3);
            flag = false;
        }

        return hash4SHA1(stringbuilder.toString());
    }

    public static String getDataSha1Digest(byte abyte0[]) {
        String s = null;
        if(abyte0 != null && abyte0.length != 0) goto _L2; else goto _L1
_L1:
        return s;
_L2:
        String s1;
        MessageDigest messagedigest = MessageDigest.getInstance("SHA1");
        messagedigest.update(abyte0);
        s1 = getHexString(messagedigest.digest());
        s = s1;
        continue; /* Loop/switch isn't completed */
        Exception exception;
        exception;
        exception.printStackTrace();
        if(true) goto _L1; else goto _L3
_L3:
    }

    public static String getFileSha1Digest(String s) {
        String s1 = null;
        MessageDigest messagedigest = null;
        FileInputStream fileinputstream = null;
        Exception exception;
        IOException ioexception;
        IOException ioexception1;
        FileInputStream fileinputstream1;
        Exception exception2;
        byte abyte0[];
        int i;
        IOException ioexception2;
        try {
            messagedigest = MessageDigest.getInstance("SHA1");
            fileinputstream1 = new FileInputStream(new File(s));
            break MISSING_BLOCK_LABEL_29;
        }
        // Misplaced declaration of an exception variable
        catch(Exception exception) { }
        finally { }
          goto _L1
        exception;
        fileinputstream = fileinputstream1;
_L1:
        exception.printStackTrace();
        if(fileinputstream != null)
            try {
                fileinputstream.close();
            }
            // Misplaced declaration of an exception variable
            catch(IOException ioexception1) {
                ioexception1.printStackTrace();
            }
        if(false)
            throw null;
_L3:
        return s1;
        abyte0 = new byte[4096];
        do {
            i = fileinputstream1.read(abyte0);
            if(i == -1)
                break;
            messagedigest.update(abyte0, 0, i);
        } while(true);
        if(fileinputstream1 != null)
            try {
                fileinputstream1.close();
            }
            // Misplaced declaration of an exception variable
            catch(IOException ioexception2) {
                ioexception2.printStackTrace();
            }
        if(messagedigest != null)
            s1 = getHexString(messagedigest.digest());
        continue; /* Loop/switch isn't completed */
        if(true) goto _L3; else goto _L2
_L2:
        exception2;
        fileinputstream = fileinputstream1;
        if(fileinputstream != null)
            try {
                fileinputstream.close();
            }
            // Misplaced declaration of an exception variable
            catch(IOException ioexception) {
                ioexception.printStackTrace();
            }
        if(messagedigest != null)
            s1 = getHexString(messagedigest.digest());
        continue; /* Loop/switch isn't completed */
    }

    public static String getHexString(byte abyte0[]) {
        StringBuilder stringbuilder = new StringBuilder();
        int i = 0;
        while(i < abyte0.length)  {
            int j = (0xf0 & abyte0[i]) >> 4;
            int k;
            int l;
            int i1;
            if(j >= 0 && j <= 9)
                k = j + 48;
            else
                k = -10 + (j + 97);
            stringbuilder.append((char)k);
            l = 0xf & abyte0[i];
            if(l >= 0 && l <= 9)
                i1 = l + 48;
            else
                i1 = -10 + (l + 97);
            stringbuilder.append((char)i1);
            i++;
        }
        return stringbuilder.toString();
    }

    public static String hash4SHA1(String s) {
        String s1 = Base64.encodeToString(MessageDigest.getInstance("SHA1").digest(s.getBytes("UTF-8")), 2);
        return s1;
        NoSuchAlgorithmException nosuchalgorithmexception;
        nosuchalgorithmexception;
        nosuchalgorithmexception.printStackTrace();
_L2:
        throw new IllegalStateException("failed to SHA1");
        UnsupportedEncodingException unsupportedencodingexception;
        unsupportedencodingexception;
        unsupportedencodingexception.printStackTrace();
        if(true) goto _L2; else goto _L1
_L1:
    }

    public static String hashDeviceInfo(String s) {
        String s1;
        try {
            s1 = Base64.encodeToString(MessageDigest.getInstance("SHA1").digest(s.getBytes()), 8).substring(0, 16);
        }
        catch(NoSuchAlgorithmException nosuchalgorithmexception) {
            throw new IllegalStateException("failed to init SHA1 digest");
        }
        return s1;
    }

    public static Cipher newAESCipher(String s, int i) {
        SecretKeySpec secretkeyspec = new SecretKeySpec(Base64.decode(s, 2), "AES");
        Cipher cipher;
        cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(i, secretkeyspec, new IvParameterSpec("0102030405060708".getBytes()));
_L2:
        return cipher;
        NoSuchAlgorithmException nosuchalgorithmexception;
        nosuchalgorithmexception;
        nosuchalgorithmexception.printStackTrace();
_L3:
        cipher = null;
        if(true) goto _L2; else goto _L1
_L1:
        NoSuchPaddingException nosuchpaddingexception;
        nosuchpaddingexception;
        nosuchpaddingexception.printStackTrace();
          goto _L3
        InvalidAlgorithmParameterException invalidalgorithmparameterexception;
        invalidalgorithmparameterexception;
        invalidalgorithmparameterexception.printStackTrace();
          goto _L3
        InvalidKeyException invalidkeyexception;
        invalidkeyexception;
        invalidkeyexception.printStackTrace();
          goto _L3
    }

    public static Cipher newRC4Cipher(byte abyte0[], int i) {
        SecretKeySpec secretkeyspec = new SecretKeySpec(abyte0, "RC4");
        Cipher cipher;
        cipher = Cipher.getInstance("RC4");
        cipher.init(i, secretkeyspec);
_L2:
        return cipher;
        NoSuchAlgorithmException nosuchalgorithmexception;
        nosuchalgorithmexception;
        nosuchalgorithmexception.printStackTrace();
_L3:
        cipher = null;
        if(true) goto _L2; else goto _L1
_L1:
        NoSuchPaddingException nosuchpaddingexception;
        nosuchpaddingexception;
        nosuchpaddingexception.printStackTrace();
          goto _L3
        InvalidKeyException invalidkeyexception;
        invalidkeyexception;
        invalidkeyexception.printStackTrace();
          goto _L3
    }

    private static byte[] randomRc4Key128(String s) {
        byte abyte1[];
        MessageDigest messagedigest = MessageDigest.getInstance("MD5");
        messagedigest.update(s.getBytes());
        abyte1 = messagedigest.digest();
        byte abyte0[] = abyte1;
_L2:
        return abyte0;
        Exception exception;
        exception;
        exception.printStackTrace();
        abyte0 = null;
        if(true) goto _L2; else goto _L1
_L1:
    }

    public static String remoteEndecrypt(String s, String s1, String s2, String s3, CIPHER_MODE cipher_mode) throws IOException, CipherException, InvalidResponseException, AccessDeniedException, AuthenticationFailureException {
        EasyMap easymap = new EasyMap();
        String s4;
        SimpleRequest.MapContent mapcontent;
        Object obj;
        if(cipher_mode == CIPHER_MODE.ENCRYPT) {
            Object aobj1[] = new Object[1];
            aobj1[0] = s;
            s4 = String.format("http://api.account.xiaomi.com/pass/v2/safe/user/%s/getSecurityToken", aobj1);
            easymap.easyPut("plainText", s1);
        } else {
            Object aobj[] = new Object[1];
            aobj[0] = s;
            s4 = String.format("http://api.account.xiaomi.com/pass/v2/safe/user/%s/getPlanText", aobj);
            easymap.easyPut("token", s1);
        }
        mapcontent = SecureRequest.postAsMap(s4, easymap, (new EasyMap()).easyPut("userId", s).easyPut("serviceToken", s2), true, s3);
        obj = mapcontent.getFromBody("code");
        if(!INT_0.equals(obj))
            throw new InvalidResponseException((new StringBuilder()).append("failed to encrypt, code: ").append(obj).toString());
        Object obj1 = mapcontent.getFromBody("data");
        if(!(obj1 instanceof Map))
            throw new InvalidResponseException("invalid data node");
        Map map = (Map)obj1;
        String s5;
        Object obj2;
        if(cipher_mode == CIPHER_MODE.ENCRYPT)
            s5 = "cipher";
        else
            s5 = "plainText";
        obj2 = map.get(s5);
        if(!(obj2 instanceof String))
            throw new InvalidResponseException((new StringBuilder()).append("invalid result: ").append(obj2).toString());
        else
            return (String)obj2;
    }

    private static final Integer INT_0 = Integer.valueOf(0);
    private static final String RC4_ALGORITHM_NAME = "RC4";
    private static final String URL_REMOTE_DECRYPT = "http://api.account.xiaomi.com/pass/v2/safe/user/%s/getPlanText";
    private static final String URL_REMOTE_ENCRYPT = "http://api.account.xiaomi.com/pass/v2/safe/user/%s/getSecurityToken";

}
