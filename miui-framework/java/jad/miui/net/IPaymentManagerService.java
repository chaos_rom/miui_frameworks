// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.net;

import android.accounts.Account;
import android.os.*;

// Referenced classes of package miui.net:
//            IPaymentManagerResponse

public interface IPaymentManagerService
    extends IInterface {
    public static abstract class Stub extends Binder
        implements IPaymentManagerService {
        private static class Proxy
            implements IPaymentManagerService {

            public IBinder asBinder() {
                return mRemote;
            }

            public String getInterfaceDescriptor() {
                return "miui.net.IPaymentManagerService";
            }

            public void pay(IPaymentManagerResponse ipaymentmanagerresponse, Account account, String s, String s1, Bundle bundle) throws RemoteException {
                Parcel parcel;
                Parcel parcel1;
                parcel = Parcel.obtain();
                parcel1 = Parcel.obtain();
                parcel.writeInterfaceToken("miui.net.IPaymentManagerService");
                if(ipaymentmanagerresponse == null) goto _L2; else goto _L1
_L1:
                IBinder ibinder = ipaymentmanagerresponse.asBinder();
_L5:
                parcel.writeStrongBinder(ibinder);
                if(account == null) goto _L4; else goto _L3
_L3:
                parcel.writeInt(1);
                account.writeToParcel(parcel, 0);
_L6:
                parcel.writeString(s);
                parcel.writeString(s1);
                if(bundle == null)
                    break MISSING_BLOCK_LABEL_147;
                parcel.writeInt(1);
                bundle.writeToParcel(parcel, 0);
_L7:
                mRemote.transact(1, parcel, parcel1, 0);
                parcel1.readException();
                parcel1.recycle();
                parcel.recycle();
                return;
_L2:
                ibinder = null;
                  goto _L5
_L4:
                parcel.writeInt(0);
                  goto _L6
                Exception exception;
                exception;
                parcel1.recycle();
                parcel.recycle();
                throw exception;
                parcel.writeInt(0);
                  goto _L7
            }

            public void payForOrder(IPaymentManagerResponse ipaymentmanagerresponse, Account account, String s, Bundle bundle) throws RemoteException {
                Parcel parcel;
                Parcel parcel1;
                parcel = Parcel.obtain();
                parcel1 = Parcel.obtain();
                parcel.writeInterfaceToken("miui.net.IPaymentManagerService");
                if(ipaymentmanagerresponse == null) goto _L2; else goto _L1
_L1:
                IBinder ibinder = ipaymentmanagerresponse.asBinder();
_L5:
                parcel.writeStrongBinder(ibinder);
                if(account == null) goto _L4; else goto _L3
_L3:
                parcel.writeInt(1);
                account.writeToParcel(parcel, 0);
_L6:
                parcel.writeString(s);
                if(bundle == null)
                    break MISSING_BLOCK_LABEL_140;
                parcel.writeInt(1);
                bundle.writeToParcel(parcel, 0);
_L7:
                mRemote.transact(2, parcel, parcel1, 0);
                parcel1.readException();
                parcel1.recycle();
                parcel.recycle();
                return;
_L2:
                ibinder = null;
                  goto _L5
_L4:
                parcel.writeInt(0);
                  goto _L6
                Exception exception;
                exception;
                parcel1.recycle();
                parcel.recycle();
                throw exception;
                parcel.writeInt(0);
                  goto _L7
            }

            private IBinder mRemote;

            Proxy(IBinder ibinder) {
                mRemote = ibinder;
            }
        }


        public static IPaymentManagerService asInterface(IBinder ibinder) {
            Object obj;
            if(ibinder == null) {
                obj = null;
            } else {
                IInterface iinterface = ibinder.queryLocalInterface("miui.net.IPaymentManagerService");
                if(iinterface != null && (iinterface instanceof IPaymentManagerService))
                    obj = (IPaymentManagerService)iinterface;
                else
                    obj = new Proxy(ibinder);
            }
            return ((IPaymentManagerService) (obj));
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel1, int j) throws RemoteException {
            i;
            JVM INSTR lookupswitch 3: default 36
        //                       1: 62
        //                       2: 167
        //                       1598968902: 50;
               goto _L1 _L2 _L3 _L4
_L1:
            boolean flag = super.onTransact(i, parcel, parcel1, j);
_L6:
            return flag;
_L4:
            parcel1.writeString("miui.net.IPaymentManagerService");
            flag = true;
            continue; /* Loop/switch isn't completed */
_L2:
            parcel.enforceInterface("miui.net.IPaymentManagerService");
            IPaymentManagerResponse ipaymentmanagerresponse1 = IPaymentManagerResponse.Stub.asInterface(parcel.readStrongBinder());
            Account account1;
            String s1;
            String s2;
            Bundle bundle1;
            if(parcel.readInt() != 0)
                account1 = (Account)Account.CREATOR.createFromParcel(parcel);
            else
                account1 = null;
            s1 = parcel.readString();
            s2 = parcel.readString();
            if(parcel.readInt() != 0)
                bundle1 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
            else
                bundle1 = null;
            pay(ipaymentmanagerresponse1, account1, s1, s2, bundle1);
            parcel1.writeNoException();
            flag = true;
            continue; /* Loop/switch isn't completed */
_L3:
            parcel.enforceInterface("miui.net.IPaymentManagerService");
            IPaymentManagerResponse ipaymentmanagerresponse = IPaymentManagerResponse.Stub.asInterface(parcel.readStrongBinder());
            Account account;
            String s;
            Bundle bundle;
            if(parcel.readInt() != 0)
                account = (Account)Account.CREATOR.createFromParcel(parcel);
            else
                account = null;
            s = parcel.readString();
            if(parcel.readInt() != 0)
                bundle = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
            else
                bundle = null;
            payForOrder(ipaymentmanagerresponse, account, s, bundle);
            parcel1.writeNoException();
            flag = true;
            if(true) goto _L6; else goto _L5
_L5:
        }

        private static final String DESCRIPTOR = "miui.net.IPaymentManagerService";
        static final int TRANSACTION_pay = 1;
        static final int TRANSACTION_payForOrder = 2;

        public Stub() {
            attachInterface(this, "miui.net.IPaymentManagerService");
        }
    }


    public abstract void pay(IPaymentManagerResponse ipaymentmanagerresponse, Account account, String s, String s1, Bundle bundle) throws RemoteException;

    public abstract void payForOrder(IPaymentManagerResponse ipaymentmanagerresponse, Account account, String s, Bundle bundle) throws RemoteException;
}
