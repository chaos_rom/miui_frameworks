// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.net;

import java.io.IOException;
import org.apache.http.*;
import org.apache.http.client.*;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HttpContext;

// Referenced classes of package miui.net:
//            CloudManager

public class MiCloudHttpClient
    implements HttpClient {

    private MiCloudHttpClient(HttpClient httpclient) {
        mProxy = httpclient;
    }

    public static MiCloudHttpClient newInstance() {
        DefaultHttpClient defaulthttpclient = new DefaultHttpClient();
        HttpProtocolParams.setUserAgent(defaulthttpclient.getParams(), CloudManager.getUserAgent());
        return new MiCloudHttpClient(defaulthttpclient);
    }

    public Object execute(HttpHost httphost, HttpRequest httprequest, ResponseHandler responsehandler) throws IOException, ClientProtocolException {
        return mProxy.execute(httphost, httprequest, responsehandler);
    }

    public Object execute(HttpHost httphost, HttpRequest httprequest, ResponseHandler responsehandler, HttpContext httpcontext) throws IOException, ClientProtocolException {
        return mProxy.execute(httphost, httprequest, responsehandler, httpcontext);
    }

    public Object execute(HttpUriRequest httpurirequest, ResponseHandler responsehandler) throws IOException, ClientProtocolException {
        return mProxy.execute(httpurirequest, responsehandler);
    }

    public Object execute(HttpUriRequest httpurirequest, ResponseHandler responsehandler, HttpContext httpcontext) throws IOException, ClientProtocolException {
        return mProxy.execute(httpurirequest, responsehandler, httpcontext);
    }

    public HttpResponse execute(HttpHost httphost, HttpRequest httprequest) throws IOException, ClientProtocolException {
        return mProxy.execute(httphost, httprequest);
    }

    public HttpResponse execute(HttpHost httphost, HttpRequest httprequest, HttpContext httpcontext) throws IOException, ClientProtocolException {
        return mProxy.execute(httphost, httprequest);
    }

    public HttpResponse execute(HttpUriRequest httpurirequest) throws IOException, ClientProtocolException {
        return mProxy.execute(httpurirequest);
    }

    public HttpResponse execute(HttpUriRequest httpurirequest, HttpContext httpcontext) throws IOException, ClientProtocolException {
        return mProxy.execute(httpurirequest, httpcontext);
    }

    public ClientConnectionManager getConnectionManager() {
        return mProxy.getConnectionManager();
    }

    public HttpParams getParams() {
        return mProxy.getParams();
    }

    private HttpClient mProxy;
}
