// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.util;

import android.content.*;
import android.database.Cursor;
import android.net.Uri;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import java.io.*;
import java.nio.charset.Charsets;
import java.util.ArrayList;
import org.json.*;

// Referenced classes of package miui.util:
//            GZIPCodec, IOUtils

public class MiCloudMmsCodec {

    public MiCloudMmsCodec() {
    }

    public static void decodeMmsBody(ContentResolver contentresolver, long l, byte abyte0[], boolean flag) throws IOException, JSONException, RemoteException, OperationApplicationException {
        ArrayList arraylist = new ArrayList();
        ArrayList arraylist1 = new ArrayList();
        makeMmsPartWriteOps(arraylist, arraylist1, l, abyte0, flag);
        writeMmsPartFiles(contentresolver, arraylist, arraylist1);
    }

    public static byte[] encodeMmsBody(ContentResolver contentresolver, long l, boolean flag) throws JSONException, IOException {
        Cursor cursor;
        android.net.Uri.Builder builder = android.provider.Telephony.Mms.CONTENT_URI.buildUpon().appendPath(String.valueOf(l)).appendPath("part");
        String s;
        if(flag)
            s = "1";
        else
            s = "0";
        cursor = contentresolver.query(builder.appendQueryParameter("caller_is_syncadapter", s).build(), MMS_PART_PROJECTION, null, null, null);
        if(cursor == null) {
            Log.e("MiCloudMmsCodec", "encodeMmsBody: Failed to access mms info in database");
            throw new IOException();
        }
        Exception exception;
        JSONArray jsonarray;
        byte abyte0[];
        byte abyte1[];
        if(cursor.getCount() > 0) {
            jsonarray = new JSONArray();
            JSONObject jsonobject;
            for(; cursor.moveToNext(); jsonarray.put(jsonobject)) {
                long l1 = cursor.getLong(0);
                String s1 = cursor.getString(1);
                String s2 = cursor.getString(2);
                String s3 = cursor.getString(3);
                String s4 = cursor.getString(4);
                jsonobject = new JSONObject();
                jsonobject.put("content_type", s1);
                jsonobject.put("content_id", s2);
                if(s3 != null)
                    jsonobject.put("content_location", s3);
                if(!TextUtils.isEmpty(s4))
                    jsonobject.put("text", s4);
                byte abyte2[] = readMmsPart(contentresolver, l1, flag);
                if(abyte2 != null)
                    jsonobject.put("data", Base64.encodeToString(abyte2, 0));
            }

            break MISSING_BLOCK_LABEL_272;
        }
        cursor.close();
        abyte1 = null;
          goto _L1
        exception;
        cursor.close();
        throw exception;
        abyte0 = GZIPCodec.encode(jsonarray.toString().getBytes(Charsets.UTF_8));
        abyte1 = abyte0;
        cursor.close();
_L3:
        return abyte1;
_L1:
        if(true) goto _L3; else goto _L2
_L2:
    }

    public static void makeMmsPartWriteOps(ArrayList arraylist, ArrayList arraylist1, long l, byte abyte0[], boolean flag) throws IOException, JSONException {
        android.net.Uri.Builder builder = android.provider.Telephony.Mms.CONTENT_URI.buildUpon().appendPath(String.valueOf(l)).appendPath("part");
        String s;
        Uri uri;
        ContentProviderOperation contentprovideroperation;
        JSONArray jsonarray;
        int i;
        if(flag)
            s = "1";
        else
            s = "0";
        uri = builder.appendQueryParameter("caller_is_syncadapter", s).appendQueryParameter("supress_making_mms_preview", "1").build();
        contentprovideroperation = ContentProviderOperation.newDelete(uri).build();
        arraylist1.add(null);
        arraylist.add(contentprovideroperation);
        jsonarray = new JSONArray(new String(GZIPCodec.decode(abyte0), Charsets.UTF_8));
        i = 0;
        while(i < jsonarray.length())  {
            JSONObject jsonobject = jsonarray.getJSONObject(i);
            String s1 = jsonobject.getString("content_type");
            byte byte0;
            String s2;
            String s3;
            String s4;
            byte abyte1[];
            ContentValues contentvalues;
            if(s1.equals("application/smil"))
                byte0 = -1;
            else
                byte0 = 0;
            s2 = jsonobject.optString("content_id", null);
            s3 = jsonobject.optString("content_location", null);
            s4 = jsonobject.optString("text");
            abyte1 = Base64.decode(jsonobject.optString("data"), 0);
            contentvalues = new ContentValues();
            contentvalues.put("mid", Long.valueOf(l));
            contentvalues.put("seq", Integer.valueOf(byte0));
            contentvalues.put("ct", s1);
            contentvalues.put("cid", s2);
            contentvalues.put("cl", s3);
            if(s1.equals("text/plain"))
                contentvalues.put("chset", Integer.valueOf(106));
            if(s1.equals("text/plain") || s1.equals("application/smil")) {
                contentvalues.put("text", s4);
                arraylist1.add(null);
            } else {
                arraylist1.add(abyte1);
            }
            arraylist.add(ContentProviderOperation.newInsert(uri).withValues(contentvalues).build());
            i++;
        }
    }

    private static byte[] readMmsPart(ContentResolver contentresolver, long l, boolean flag) throws IOException {
        InputStream inputstream;
        Object obj;
        byte abyte0[];
        ByteArrayOutputStream bytearrayoutputstream;
        android.net.Uri.Builder builder = android.provider.Telephony.Mms.CONTENT_URI.buildUpon().appendPath("part").appendPath(String.valueOf(l));
        String s;
        Uri uri;
        FileNotFoundException filenotfoundexception1;
        if(flag)
            s = "1";
        else
            s = "0";
        uri = builder.appendQueryParameter("caller_is_syncadapter", s).build();
        inputstream = null;
        obj = null;
        inputstream = contentresolver.openInputStream(uri);
        bytearrayoutputstream = new ByteArrayOutputStream();
        if(inputstream != null) goto _L2; else goto _L1
_L1:
        Exception exception;
        try {
            throw new IOException((new StringBuilder()).append("Cannot open input stream for ").append(uri).toString());
        }
        // Misplaced declaration of an exception variable
        catch(FileNotFoundException filenotfoundexception1) {
            obj = bytearrayoutputstream;
        }
        finally {
            obj = bytearrayoutputstream;
        }
_L7:
        IOUtils.closeQuietly(inputstream);
        IOUtils.closeQuietly(((OutputStream) (obj)));
        abyte0 = null;
_L4:
        return abyte0;
_L2:
        byte abyte1[] = new byte[4096];
        do {
            int i = inputstream.read(abyte1);
            if(i <= 0)
                break;
            bytearrayoutputstream.write(abyte1, 0, i);
        } while(true);
          goto _L3
_L5:
        IOUtils.closeQuietly(inputstream);
        IOUtils.closeQuietly(((OutputStream) (obj)));
        throw exception;
_L3:
        byte abyte2[] = bytearrayoutputstream.toByteArray();
        abyte0 = abyte2;
        IOUtils.closeQuietly(inputstream);
        IOUtils.closeQuietly(bytearrayoutputstream);
          goto _L4
        exception;
          goto _L5
        FileNotFoundException filenotfoundexception;
        filenotfoundexception;
        if(true) goto _L7; else goto _L6
_L6:
    }

    public static void writeMmsPartFiles(ContentResolver contentresolver, ArrayList arraylist, ArrayList arraylist1) throws RemoteException, OperationApplicationException, IOException {
        ContentProviderResult acontentproviderresult[];
        int i;
        acontentproviderresult = contentresolver.applyBatch("mms", arraylist);
        i = 0;
_L2:
        byte abyte0[];
        if(i >= acontentproviderresult.length)
            break MISSING_BLOCK_LABEL_135;
        abyte0 = (byte[])arraylist1.get(i);
        if(abyte0 != null)
            break; /* Loop/switch isn't completed */
_L3:
        i++;
        if(true) goto _L2; else goto _L1
_L1:
        Uri uri;
        OutputStream outputstream;
        uri = acontentproviderresult[i].uri.buildUpon().appendQueryParameter("supress_making_mms_preview", "1").build();
        outputstream = contentresolver.openOutputStream(uri);
        outputstream.write(abyte0);
        Log.v("MiCloudMmsCodec", (new StringBuilder()).append("wrote ").append(abyte0.length).append(" bytes to ").append(uri).toString());
        IOUtils.closeQuietly(outputstream);
          goto _L3
        Exception exception;
        exception;
        IOUtils.closeQuietly(outputstream);
        throw exception;
        arraylist.clear();
        arraylist1.clear();
        return;
    }

    private static final String J_PART_CONTENT_ID = "content_id";
    private static final String J_PART_CONTENT_LOCATION = "content_location";
    private static final String J_PART_CONTENT_TYPE = "content_type";
    private static final String J_PART_DATA = "data";
    private static final String J_PART_TEXT = "text";
    private static final boolean LOGV = true;
    private static final String MMS_PART_PROJECTION[];
    private static final int PART_COLUMN_CONTENT_ID = 2;
    private static final int PART_COLUMN_CONTENT_LOCATION = 3;
    private static final int PART_COLUMN_CONTENT_TYPE = 1;
    private static final int PART_COLUMN_ID = 0;
    private static final int PART_COLUMN_TEXT = 4;
    private static final String TAG = "MiCloudMmsCodec";

    static  {
        String as[] = new String[5];
        as[0] = "_id";
        as[1] = "ct";
        as[2] = "cid";
        as[3] = "cl";
        as[4] = "text";
        MMS_PART_PROJECTION = as;
    }
}
