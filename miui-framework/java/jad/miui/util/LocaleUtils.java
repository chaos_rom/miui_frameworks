// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.util;

import java.util.*;

// Referenced classes of package miui.util:
//            HanziToPinyin

public class LocaleUtils {
    private static class NameSplitter {

        private static int guessCJKNameStyle(String s, int i) {
            int j = s.length();
_L3:
            if(i >= j) goto _L2; else goto _L1
_L1:
            byte byte0;
            int k;
            k = Character.codePointAt(s, i);
            if(!Character.isLetter(k))
                continue; /* Loop/switch isn't completed */
            Character.UnicodeBlock unicodeblock = Character.UnicodeBlock.of(k);
            if(isJapanesePhoneticUnicodeBlock(unicodeblock)) {
                byte0 = 4;
            } else {
                if(!isKoreanUnicodeBlock(unicodeblock))
                    continue; /* Loop/switch isn't completed */
                byte0 = 5;
            }
_L4:
            return byte0;
            i += Character.charCount(k);
              goto _L3
_L2:
            byte0 = 2;
              goto _L4
        }

        public static int guessFullNameStyle(String s) {
            if(s != null) goto _L2; else goto _L1
_L1:
            int i = 0;
_L4:
            return i;
_L2:
            int j;
            int k;
            i = 0;
            j = s.length();
            k = 0;
_L5:
            int l;
            if(k < j) {
label0:
                {
                    l = Character.codePointAt(s, k);
                    if(!Character.isLetter(l))
                        break MISSING_BLOCK_LABEL_103;
                    Character.UnicodeBlock unicodeblock = Character.UnicodeBlock.of(l);
                    if(isLatinUnicodeBlock(unicodeblock))
                        break label0;
                    if(isCJKUnicodeBlock(unicodeblock))
                        i = guessCJKNameStyle(s, k + Character.charCount(l));
                    else
                    if(isJapanesePhoneticUnicodeBlock(unicodeblock)) {
                        i = 4;
                    } else {
                        if(!isKoreanUnicodeBlock(unicodeblock))
                            break label0;
                        i = 5;
                    }
                }
            }
            if(true) goto _L4; else goto _L3
_L3:
            i = 1;
            k += Character.charCount(l);
              goto _L5
        }

        private static boolean isCJKUnicodeBlock(Character.UnicodeBlock unicodeblock) {
            boolean flag;
            if(unicodeblock == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS || unicodeblock == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A || unicodeblock == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B || unicodeblock == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION || unicodeblock == Character.UnicodeBlock.CJK_RADICALS_SUPPLEMENT || unicodeblock == Character.UnicodeBlock.CJK_COMPATIBILITY || unicodeblock == Character.UnicodeBlock.CJK_COMPATIBILITY_FORMS || unicodeblock == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS || unicodeblock == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS_SUPPLEMENT)
                flag = true;
            else
                flag = false;
            return flag;
        }

        private static boolean isJapanesePhoneticUnicodeBlock(Character.UnicodeBlock unicodeblock) {
            boolean flag;
            if(unicodeblock == Character.UnicodeBlock.KATAKANA || unicodeblock == Character.UnicodeBlock.KATAKANA_PHONETIC_EXTENSIONS || unicodeblock == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS || unicodeblock == Character.UnicodeBlock.HIRAGANA)
                flag = true;
            else
                flag = false;
            return flag;
        }

        private static boolean isKoreanUnicodeBlock(Character.UnicodeBlock unicodeblock) {
            boolean flag;
            if(unicodeblock == Character.UnicodeBlock.HANGUL_SYLLABLES || unicodeblock == Character.UnicodeBlock.HANGUL_JAMO || unicodeblock == Character.UnicodeBlock.HANGUL_COMPATIBILITY_JAMO)
                flag = true;
            else
                flag = false;
            return flag;
        }

        private static boolean isLatinUnicodeBlock(Character.UnicodeBlock unicodeblock) {
            boolean flag;
            if(unicodeblock == Character.UnicodeBlock.BASIC_LATIN || unicodeblock == Character.UnicodeBlock.LATIN_1_SUPPLEMENT || unicodeblock == Character.UnicodeBlock.LATIN_EXTENDED_A || unicodeblock == Character.UnicodeBlock.LATIN_EXTENDED_B || unicodeblock == Character.UnicodeBlock.LATIN_EXTENDED_ADDITIONAL)
                flag = true;
            else
                flag = false;
            return flag;
        }

        private NameSplitter() {
        }
    }

    private class ChineseLocaleUtils extends LocaleUtilsBase {

        public String getSortKey(String s) {
            ArrayList arraylist = HanziToPinyin.getInstance().get(s);
            String s1;
            if(arraylist != null && arraylist.size() > 0) {
                StringBuilder stringbuilder = new StringBuilder();
                for(Iterator iterator = arraylist.iterator(); iterator.hasNext();) {
                    HanziToPinyin.Token token = (HanziToPinyin.Token)iterator.next();
                    if(2 == token.type) {
                        if(stringbuilder.length() > 0)
                            stringbuilder.append(' ');
                        stringbuilder.append(token.target.charAt(0));
                        stringbuilder.append("   ");
                        if(token.target.length() > 1)
                            stringbuilder.append(token.target.substring(1));
                        stringbuilder.append(' ');
                        stringbuilder.append(token.source);
                    } else {
                        if(stringbuilder.length() > 0)
                            stringbuilder.append(' ');
                        stringbuilder.append(token.source);
                    }
                }

                s1 = stringbuilder.toString();
            } else {
                s1 = super.getSortKey(s);
            }
            return s1;
        }

        final LocaleUtils this$0;

        private ChineseLocaleUtils() {
            this$0 = LocaleUtils.this;
            super();
        }

    }

    private static interface NameStyle {

        public static final int CJK = 2;
        public static final int JAPANESE = 4;
        public static final int KOREAN = 5;
        public static final int PINYIN = 3;
        public static final int UNDEFINED = 0;
        public static final int WESTERN = 1;
    }

    private class LocaleUtilsBase {

        public String getSortKey(String s) {
            return s;
        }

        final LocaleUtils this$0;

        private LocaleUtilsBase() {
            this$0 = LocaleUtils.this;
            super();
        }

    }


    private LocaleUtils() {
        mUtils = new HashMap();
        mBase = new LocaleUtilsBase();
        setLocale(null);
    }

    /**
     * @deprecated Method get is deprecated
     */

    private LocaleUtilsBase get(Integer integer) {
        this;
        JVM INSTR monitorenter ;
        Object obj;
        obj = (LocaleUtilsBase)mUtils.get(integer);
        if(obj == null && integer.intValue() == 3) {
            obj = new ChineseLocaleUtils();
            mUtils.put(integer, obj);
        }
        if(obj == null)
            obj = mBase;
        this;
        JVM INSTR monitorexit ;
        return ((LocaleUtilsBase) (obj));
        Exception exception;
        exception;
        throw exception;
    }

    private int getAdjustedStyle(int i) {
        if(i == 2 && !JAPANESE_LANGUAGE.equals(mLanguage) && !KOREAN_LANGUAGE.equals(mLanguage))
            i = 3;
        return i;
    }

    private LocaleUtilsBase getForSort(Integer integer) {
        return get(Integer.valueOf(getAdjustedStyle(integer.intValue())));
    }

    /**
     * @deprecated Method getIntance is deprecated
     */

    public static LocaleUtils getIntance() {
        miui/util/LocaleUtils;
        JVM INSTR monitorenter ;
        LocaleUtils localeutils;
        if(sSingleton == null)
            sSingleton = new LocaleUtils();
        localeutils = sSingleton;
        miui/util/LocaleUtils;
        JVM INSTR monitorexit ;
        return localeutils;
        Exception exception;
        exception;
        throw exception;
    }

    private void setLocale(Locale locale) {
        if(locale == null)
            mLanguage = Locale.getDefault().getLanguage().toLowerCase();
        else
            mLanguage = locale.getLanguage().toLowerCase();
    }

    public String getSortKey(String s) {
        return getForSort(Integer.valueOf(NameSplitter.guessFullNameStyle(s))).getSortKey(s);
    }

    private static final String JAPANESE_LANGUAGE;
    private static final String KOREAN_LANGUAGE;
    private static LocaleUtils sSingleton;
    private LocaleUtilsBase mBase;
    private String mLanguage;
    private HashMap mUtils;

    static  {
        JAPANESE_LANGUAGE = Locale.JAPANESE.getLanguage().toLowerCase();
        KOREAN_LANGUAGE = Locale.KOREAN.getLanguage().toLowerCase();
    }
}
