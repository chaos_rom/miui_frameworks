// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.util;

import android.accounts.Account;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SyncInfo;
import android.content.SyncStatusInfo;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.format.DateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import miui.widget.SyncStatePreference;

public class MiCloudSyncUtils {

    public MiCloudSyncUtils() {
    }

    private static boolean isSyncing(List list, Account account, String s) {
        Iterator iterator = list.iterator();
_L4:
        if(!iterator.hasNext()) goto _L2; else goto _L1
_L1:
        SyncInfo syncinfo = (SyncInfo)iterator.next();
        if(!syncinfo.account.equals(account) || !syncinfo.authority.equals(s)) goto _L4; else goto _L3
_L3:
        boolean flag = true;
_L6:
        return flag;
_L2:
        flag = false;
        if(true) goto _L6; else goto _L5
_L5:
    }

    public static boolean needActivate(String s) {
        return AUTHORITIES_NEED_ACTIVATE.contains(s);
    }

    public static void requestOrCancelSync(Account account, String s, boolean flag) {
        if(flag) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("force", true);
            ContentResolver.requestSync(account, s, bundle);
        } else {
            ContentResolver.cancelSync(account, s);
        }
    }

    public static void updateSyncStatus(Context context, List list, SyncStatePreference syncstatepreference, boolean flag, boolean flag1, boolean flag2, boolean flag3, String s) {
        java.text.DateFormat dateformat = DateFormat.getDateFormat(context);
        java.text.DateFormat dateformat1 = DateFormat.getTimeFormat(context);
        Date date = new Date();
        String s1 = syncstatepreference.getAuthority();
        Account account = syncstatepreference.getAccount();
        SyncStatusInfo syncstatusinfo = ContentResolver.getSyncStatus(account, s1);
        boolean flag4 = ContentResolver.getSyncAutomatically(account, s1);
        boolean flag5;
        boolean flag6;
        boolean flag7;
        boolean flag8;
        boolean flag9;
        boolean flag10;
        if(syncstatusinfo == null)
            flag5 = false;
        else
            flag5 = syncstatusinfo.pending;
        if(syncstatusinfo == null)
            flag6 = false;
        else
            flag6 = syncstatusinfo.initialize;
        flag7 = isSyncing(list, account, s1);
        if(syncstatusinfo != null && syncstatusinfo.lastFailureTime != 0L && syncstatusinfo.getLastFailureMesgAsInt(0) != 1)
            flag8 = true;
        else
            flag8 = false;
        if(!flag4)
            flag8 = false;
        if(flag) {
            int i;
            boolean flag11;
            long l;
            if(syncstatusinfo == null)
                l = 0L;
            else
                l = syncstatusinfo.lastSuccessTime;
            if(l != 0L) {
                date.setTime(l);
                String s2 = (new StringBuilder()).append(dateformat.format(date)).append(" ").append(dateformat1.format(date)).toString();
                if(!TextUtils.isEmpty(s))
                    s = (new StringBuilder()).append(s).append(" ").toString();
                syncstatepreference.setSummary((new StringBuilder()).append(s).append(s2).toString());
            } else {
                syncstatepreference.setSummary(s);
            }
        }
        i = ContentResolver.getIsSyncable(account, s1);
        if(flag7 && i >= 0 && !flag6)
            flag9 = true;
        else
            flag9 = false;
        syncstatepreference.setActive(flag9);
        if(flag5 && i >= 0 && !flag6)
            flag10 = true;
        else
            flag10 = false;
        syncstatepreference.setPending(flag10);
        syncstatepreference.setFailed(flag8);
        syncstatepreference.setChecked(flag4);
        flag11 = ContentResolver.getMasterSyncAutomatically();
        syncstatepreference.setEnabled(flag11);
        if(flag11 && flag2) {
            boolean flag12;
            boolean flag13;
            if(!flag1)
                flag12 = true;
            else
                flag12 = false;
            syncstatepreference.setNoSim(flag12);
            syncstatepreference.setActivating(flag3);
            if(flag1 && !flag3)
                flag13 = true;
            else
                flag13 = false;
            syncstatepreference.setEnabled(flag13);
        }
    }

    private static final Set AUTHORITIES_NEED_ACTIVATE;

    static  {
        AUTHORITIES_NEED_ACTIVATE = new HashSet();
        AUTHORITIES_NEED_ACTIVATE.add("sms");
    }
}
