// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.util;

import java.io.*;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

// Referenced classes of package miui.util:
//            IOUtils

public final class GZIPCodec {

    private GZIPCodec() {
    }

    public static byte[] decode(byte abyte0[]) throws IOException {
        ByteArrayInputStream bytearrayinputstream;
        ByteArrayOutputStream bytearrayoutputstream;
        Object obj;
        bytearrayinputstream = new ByteArrayInputStream(abyte0);
        bytearrayoutputstream = new ByteArrayOutputStream();
        obj = null;
        GZIPInputStream gzipinputstream = new GZIPInputStream(bytearrayinputstream);
        byte abyte1[] = new byte[512];
        int i;
        do {
            i = gzipinputstream.read(abyte1);
            if(i > 0)
                bytearrayoutputstream.write(abyte1, 0, i);
        } while(i >= 0);
        IOUtils.closeQuietly(gzipinputstream);
        IOUtils.closeQuietly(bytearrayinputstream);
        IOUtils.closeQuietly(bytearrayoutputstream);
        return bytearrayoutputstream.toByteArray();
        Exception exception;
        exception;
_L2:
        IOUtils.closeQuietly(((java.io.InputStream) (obj)));
        IOUtils.closeQuietly(bytearrayinputstream);
        IOUtils.closeQuietly(bytearrayoutputstream);
        throw exception;
        exception;
        obj = gzipinputstream;
        if(true) goto _L2; else goto _L1
_L1:
    }

    public static byte[] encode(byte abyte0[]) throws IOException {
        ByteArrayOutputStream bytearrayoutputstream;
        Object obj;
        bytearrayoutputstream = new ByteArrayOutputStream();
        obj = null;
        GZIPOutputStream gzipoutputstream = new GZIPOutputStream(bytearrayoutputstream);
        gzipoutputstream.write(abyte0);
        IOUtils.closeQuietly(gzipoutputstream);
        IOUtils.closeQuietly(bytearrayoutputstream);
        return bytearrayoutputstream.toByteArray();
        Exception exception;
        exception;
_L2:
        IOUtils.closeQuietly(((java.io.OutputStream) (obj)));
        IOUtils.closeQuietly(bytearrayoutputstream);
        throw exception;
        exception;
        obj = gzipoutputstream;
        if(true) goto _L2; else goto _L1
_L1:
    }

    public static String getID() {
        return "gzip";
    }

    private static final int BUFFER_SIZE = 512;
}
