// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.util;

import java.util.*;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

public class ObjectUtils {

    public ObjectUtils() {
    }

    private static Object convertObj(Object obj) {
        Object obj1;
        if(obj instanceof JSONObject)
            obj1 = jsonToMap((JSONObject)obj);
        else
        if(obj instanceof JSONArray) {
            JSONArray jsonarray = (JSONArray)obj;
            int i = jsonarray.length();
            obj1 = new ArrayList();
            int j = 0;
            while(j < i)  {
                ((List) (obj1)).add(convertObj(jsonarray.opt(j)));
                j++;
            }
        } else
        if(obj == JSONObject.NULL)
            obj1 = null;
        else
            obj1 = obj;
        return obj1;
    }

    public static String flattenMap(Map map) {
        String s;
        if(map == null) {
            s = "null";
        } else {
            Set set = map.entrySet();
            StringBuilder stringbuilder = new StringBuilder();
            stringbuilder.append("{");
            for(Iterator iterator = set.iterator(); iterator.hasNext(); stringbuilder.append("),")) {
                java.util.Map.Entry entry = (java.util.Map.Entry)iterator.next();
                Object obj = entry.getKey();
                Object obj1 = entry.getValue();
                stringbuilder.append("(");
                stringbuilder.append(obj);
                stringbuilder.append(",");
                stringbuilder.append(obj1);
            }

            stringbuilder.append("}");
            s = stringbuilder.toString();
        }
        return s;
    }

    public static Map jsonToMap(JSONObject jsonobject) {
        Object obj;
        if(jsonobject == null) {
            obj = null;
        } else {
            obj = new HashMap();
            Iterator iterator = jsonobject.keys();
            while(iterator.hasNext())  {
                String s = (String)iterator.next();
                ((Map) (obj)).put(s, convertObj(jsonobject.opt(s)));
            }
        }
        return ((Map) (obj));
    }

    public static Map listToMap(Map map) {
        HashMap hashmap = new HashMap();
        if(map != null) {
            Iterator iterator = map.entrySet().iterator();
            do {
                if(!iterator.hasNext())
                    break;
                java.util.Map.Entry entry = (java.util.Map.Entry)iterator.next();
                String s = (String)entry.getKey();
                List list = (List)entry.getValue();
                if(s != null && list != null && list.size() > 0)
                    hashmap.put(s, list.get(0));
            } while(true);
        }
        return hashmap;
    }

    public static List mapToPairs(Map map) {
        Object obj;
        if(map == null) {
            obj = null;
        } else {
            obj = new ArrayList();
            Iterator iterator = map.entrySet().iterator();
            while(iterator.hasNext())  {
                java.util.Map.Entry entry = (java.util.Map.Entry)iterator.next();
                String s = (String)entry.getKey();
                String s1 = (String)entry.getValue();
                if(s1 == null)
                    s1 = "";
                ((List) (obj)).add(new BasicNameValuePair(s, s1));
            }
        }
        return ((List) (obj));
    }
}
