// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.util;

import android.text.TextUtils;
import android.util.Log;
import java.text.Collator;
import java.util.*;

// Referenced classes of package miui.util:
//            HanziToPinyinHelper

public class HanziToPinyin {
    private static class PinYinToZhuYin {
        private static class Node {

            public char ch;
            public Node children;
            public Node next;
            public String output;
            public boolean specials;

            private Node() {
            }

        }


        private static Node build() {
            boolean flag = false;
            Node node = new Node();
            node.output = "";
            int i = 0;
            while(i < sPinyinToZhuyin.length)  {
                String s = sPinyinToZhuyin[i];
                if(s.length() == 0) {
                    flag = true;
                } else {
                    Node node1 = node;
                    int j = 0;
                    for(int k = s.length(); j < k; j++) {
                        char c = Character.toLowerCase(s.charAt(j));
                        Node node2;
                        for(node2 = node1.children; node2 != null && node2.ch != c; node2 = node2.next);
                        if(node2 == null) {
                            node2 = new Node();
                            node2.ch = c;
                            node2.next = node1.children;
                            node1.children = node2;
                        }
                        node1 = node2;
                    }

                    node1.specials = flag;
                    node1.output = sPinyinToZhuyin[i + 1];
                }
                i += 2;
            }
            return node;
        }

        private static int convert(String s, int i, StringBuilder stringbuilder) {
            int j = s.length();
            Node node = null;
            int k = 0;
            int l = i;
            for(Node node1 = sRoot; l < j && node1 != null; l++) {
                char c = Character.toLowerCase(s.charAt(l));
                for(node1 = node1.children; node1 != null && node1.ch != c; node1 = node1.next);
                if(node1 != null && node1.output != null && (!node1.specials || l == j - 1)) {
                    node = node1;
                    k = l + 1;
                }
            }

            if(node != null)
                stringbuilder.append(node.output);
            return k - i;
        }

        public static CharSequence convert(String s, StringBuilder stringbuilder) {
            if(s != null && s.length() != 0) goto _L2; else goto _L1
_L1:
            return s;
_L2:
            int i;
            int j;
            if(stringbuilder == null)
                stringbuilder = new StringBuilder();
            else
                stringbuilder.setLength(0);
            i = 0;
            j = s.length();
            do {
                if(i == j)
                    break;
                int k = convert(s, i, stringbuilder);
                if(k == 0) {
                    s = "";
                    continue; /* Loop/switch isn't completed */
                }
                i += k;
            } while(true);
            s = stringbuilder;
            if(true) goto _L1; else goto _L3
_L3:
        }

        private static final String sPinyinToZhuyin[];
        private static final Node sRoot = build();

        static  {
            String as[] = new String[166];
            as[0] = "b";
            as[1] = "\u3105";
            as[2] = "p";
            as[3] = "\u3106";
            as[4] = "m";
            as[5] = "\u3107";
            as[6] = "f";
            as[7] = "\u3108";
            as[8] = "d";
            as[9] = "\u3109";
            as[10] = "t";
            as[11] = "\u310A";
            as[12] = "n";
            as[13] = "\u310B";
            as[14] = "l";
            as[15] = "\u310C";
            as[16] = "g";
            as[17] = "\u310D";
            as[18] = "k";
            as[19] = "\u310E";
            as[20] = "h";
            as[21] = "\u310F";
            as[22] = "j";
            as[23] = "\u3110";
            as[24] = "q";
            as[25] = "\u3111";
            as[26] = "x";
            as[27] = "\u3112";
            as[28] = "zh";
            as[29] = "\u3113";
            as[30] = "ch";
            as[31] = "\u3114";
            as[32] = "sh";
            as[33] = "\u3115";
            as[34] = "r";
            as[35] = "\u3116";
            as[36] = "z";
            as[37] = "\u3117";
            as[38] = "c";
            as[39] = "\u3118";
            as[40] = "s";
            as[41] = "\u3119";
            as[42] = "y";
            as[43] = "\u3127";
            as[44] = "w";
            as[45] = "\u3128";
            as[46] = "a";
            as[47] = "\u311A";
            as[48] = "o";
            as[49] = "\u311B";
            as[50] = "e";
            as[51] = "\u311C";
            as[52] = "i";
            as[53] = "\u3127";
            as[54] = "u";
            as[55] = "\u3128";
            as[56] = "v";
            as[57] = "\u3129";
            as[58] = "ao";
            as[59] = "\u3120";
            as[60] = "ai";
            as[61] = "\u311E";
            as[62] = "an";
            as[63] = "\u3122";
            as[64] = "ang";
            as[65] = "\u3124";
            as[66] = "ou";
            as[67] = "\u3121";
            as[68] = "ong";
            as[69] = "\u3128\u3125";
            as[70] = "ei";
            as[71] = "\u311F";
            as[72] = "en";
            as[73] = "\u3123";
            as[74] = "eng";
            as[75] = "\u3125";
            as[76] = "er";
            as[77] = "\u3126";
            as[78] = "ie";
            as[79] = "\u3127\u311D";
            as[80] = "iu";
            as[81] = "\u3127\u3121";
            as[82] = "in";
            as[83] = "\u3127\u3123";
            as[84] = "ing";
            as[85] = "\u3127\u3125";
            as[86] = "iong";
            as[87] = "\u3129\u3125";
            as[88] = "ui";
            as[89] = "\u3128\u311F";
            as[90] = "un";
            as[91] = "\u3128\u3123";
            as[92] = "ue";
            as[93] = "\u3129\u311D";
            as[94] = "ve";
            as[95] = "\u3129\u311D";
            as[96] = "van";
            as[97] = "\u3129\u3122";
            as[98] = "vn";
            as[99] = "\u3129\u3123";
            as[100] = "";
            as[101] = "";
            as[102] = "zhi";
            as[103] = "\u3113";
            as[104] = "chi";
            as[105] = "\u3114";
            as[106] = "shi";
            as[107] = "\u3115";
            as[108] = "ri";
            as[109] = "\u3116";
            as[110] = "zi";
            as[111] = "\u3117";
            as[112] = "ci";
            as[113] = "\u3118";
            as[114] = "si";
            as[115] = "\u3119";
            as[116] = "yi";
            as[117] = "\u3127";
            as[118] = "ye";
            as[119] = "\u3127\u311D";
            as[120] = "you";
            as[121] = "\u3127\u3121";
            as[122] = "yin";
            as[123] = "\u3127\u3123";
            as[124] = "ying";
            as[125] = "\u3127\u3125";
            as[126] = "wu";
            as[127] = "\u3128";
            as[128] = "wei";
            as[129] = "\u3128\u311F";
            as[130] = "wen";
            as[131] = "\u3128\u3123";
            as[132] = "yu";
            as[133] = "\u3129";
            as[134] = "yue";
            as[135] = "\u3129\u311D";
            as[136] = "yuan";
            as[137] = "\u3129\u3122";
            as[138] = "yun";
            as[139] = "\u3129\u3123";
            as[140] = "yong";
            as[141] = "\u3129\u3125";
            as[142] = "ju";
            as[143] = "\u3110\u3129";
            as[144] = "jue";
            as[145] = "\u3110\u3129\u311D";
            as[146] = "juan";
            as[147] = "\u3110\u3129\u3122";
            as[148] = "jun";
            as[149] = "\u3110\u3129\u3123";
            as[150] = "qu";
            as[151] = "\u3111\u3129";
            as[152] = "que";
            as[153] = "\u3111\u3129\u311D";
            as[154] = "quan";
            as[155] = "\u3111\u3129\u3122";
            as[156] = "qun";
            as[157] = "\u3111\u3129\u3123";
            as[158] = "xu";
            as[159] = "\u3112\u3129";
            as[160] = "xue";
            as[161] = "\u3112\u3129\u311D";
            as[162] = "xuan";
            as[163] = "\u3112\u3129\u3122";
            as[164] = "xun";
            as[165] = "\u3112\u3129\u3123";
            sPinyinToZhuyin = as;
        }

        private PinYinToZhuYin() {
        }
    }

    public static class Token {

        public static final int LATIN = 1;
        public static final int PINYIN = 2;
        public static final char SEPARATOR = 32;
        public static final int UNKNOWN = 3;
        public String polyPhones[];
        public String source;
        public String target;
        public int type;

        public Token() {
        }

        public Token(int i, String s, String s1) {
            type = i;
            source = s;
            target = s1;
        }
    }


    protected HanziToPinyin(boolean flag) {
        mHasChinaCollator = flag;
    }

    private void addToken(StringBuilder stringbuilder, ArrayList arraylist, int i) {
        String s = stringbuilder.toString();
        arraylist.add(new Token(i, s, s));
        stringbuilder.setLength(0);
    }

    public static CharSequence convertPinyin2Zhuyin(String s, StringBuilder stringbuilder) {
        return PinYinToZhuYin.convert(s, stringbuilder);
    }

    public static HanziToPinyin getInstance() {
        miui/util/HanziToPinyin;
        JVM INSTR monitorenter ;
        if(sInstance == null) goto _L2; else goto _L1
_L1:
        HanziToPinyin hanzitopinyin = sInstance;
          goto _L3
_L2:
        Locale alocale[];
        int i;
        alocale = Collator.getAvailableLocales();
        i = 0;
_L7:
        if(i >= alocale.length)
            break; /* Loop/switch isn't completed */
        if(!alocale[i].equals(Locale.CHINA)) goto _L5; else goto _L4
_L4:
        sInstance = new HanziToPinyin(true);
        hanzitopinyin = sInstance;
          goto _L3
        Exception exception;
        exception;
        throw exception;
_L5:
        i++;
        if(true) goto _L7; else goto _L6
_L6:
        Log.w("HanziToPinyin", "There is no Chinese collator, HanziToPinyin is disabled");
        sInstance = new HanziToPinyin(false);
        hanzitopinyin = sInstance;
        miui/util/HanziToPinyin;
        JVM INSTR monitorexit ;
_L3:
        return hanzitopinyin;
    }

    private ArrayList getPolyPhoneLastNameTokens(String s) {
        if(!TextUtils.isEmpty(s)) goto _L2; else goto _L1
_L1:
        ArrayList arraylist = null;
_L4:
        return arraylist;
_L2:
        arraylist = new ArrayList();
        if(s.length() >= 2) {
            String s2 = s.substring(0, 2);
            String as[] = (String[])sHyphenatedNamePolyPhoneMap.get(s2);
            if(as != null) {
                int i = 0;
                while(i < as.length)  {
                    Token token1 = new Token();
                    token1.type = 2;
                    token1.source = String.valueOf(s2.charAt(i));
                    token1.target = as[i];
                    arraylist.add(token1);
                    i++;
                }
                continue; /* Loop/switch isn't completed */
            }
        }
        Character character = Character.valueOf(s.charAt(0));
        String s1 = (String)sLastNamePolyPhoneMap.get(character);
        if(s1 != null) {
            Token token = new Token();
            token.type = 2;
            token.source = character.toString();
            token.target = s1;
            arraylist.add(token);
        } else {
            arraylist = null;
        }
        if(true) goto _L4; else goto _L3
_L3:
    }

    public ArrayList get(String s) {
        return get(s, true, true);
    }

    public ArrayList get(String s, boolean flag, boolean flag1) {
        ArrayList arraylist = new ArrayList();
        if(mHasChinaCollator && !TextUtils.isEmpty(s)) goto _L2; else goto _L1
_L1:
        return arraylist;
_L2:
        int i = 0;
        if(!flag1) {
            ArrayList arraylist1 = getPolyPhoneLastNameTokens(s);
            if(arraylist1 != null && arraylist1.size() > 0) {
                arraylist.addAll(arraylist1);
                i = arraylist1.size();
            }
        }
        int j = s.length();
        StringBuilder stringbuilder = new StringBuilder();
        int k = 1;
        int l = i;
        while(l < j)  {
            char c = s.charAt(l);
            if(c == ' ') {
                if(stringbuilder.length() > 0)
                    addToken(stringbuilder, arraylist, k);
                if(!flag) {
                    String s1 = String.valueOf(' ');
                    arraylist.add(new Token(3, s1, s1));
                }
                k = 3;
            } else
            if(c < '\u0100') {
                if(l > 0) {
                    char c1 = s.charAt(l - 1);
                    boolean flag2;
                    boolean flag3;
                    if(c1 >= '0' && c1 <= '9')
                        flag2 = true;
                    else
                        flag2 = false;
                    if(c >= '0' && c <= '9')
                        flag3 = true;
                    else
                        flag3 = false;
                    if(flag2 != flag3 && stringbuilder.length() > 0)
                        addToken(stringbuilder, arraylist, k);
                }
                if(k != 1 && stringbuilder.length() > 0)
                    addToken(stringbuilder, arraylist, k);
                k = 1;
                stringbuilder.append(c);
            } else
            if(c == '\u3007') {
                Token token = new Token();
                token.type = 2;
                token.target = "ling";
                if(stringbuilder.length() > 0)
                    addToken(stringbuilder, arraylist, k);
                arraylist.add(token);
                k = 2;
            } else
            if(c >= '\u4E00' && c <= '\u9FA5') {
                String as[] = HanziToPinyinHelper.getIntance().getPinyinString(c);
                Token token1 = new Token();
                token1.source = Character.toString(c);
                if(as == null) {
                    token1.type = 3;
                    token1.target = Character.toString(c);
                } else {
                    token1.type = 2;
                    token1.target = as[0];
                    if(as.length > 1)
                        token1.polyPhones = as;
                }
                if(token1.type == 2) {
                    if(stringbuilder.length() > 0)
                        addToken(stringbuilder, arraylist, k);
                    arraylist.add(token1);
                    k = 2;
                } else {
                    if(k != token1.type && stringbuilder.length() > 0)
                        addToken(stringbuilder, arraylist, k);
                    k = token1.type;
                    stringbuilder.append(c);
                }
            } else {
                if(k != 3 && stringbuilder.length() > 0)
                    addToken(stringbuilder, arraylist, k);
                k = 3;
                stringbuilder.append(c);
            }
            l++;
        }
        if(stringbuilder.length() > 0)
            addToken(stringbuilder, arraylist, k);
        if(true) goto _L1; else goto _L3
_L3:
    }

    private static final char FIRST_BASIC_UNIHAN = 19968;
    private static final char LAST_BASIC_UNIHAN = 40869;
    private static final char SPECIAL_LING = 12295;
    private static final String TAG = "HanziToPinyin";
    private static HashMap sHyphenatedNamePolyPhoneMap;
    private static HanziToPinyin sInstance;
    private static HashMap sLastNamePolyPhoneMap;
    private final boolean mHasChinaCollator;

    static  {
        sHyphenatedNamePolyPhoneMap = new HashMap();
        sLastNamePolyPhoneMap = new HashMap();
        HashMap hashmap = sHyphenatedNamePolyPhoneMap;
        String as[] = new String[2];
        as[0] = "CHAN";
        as[1] = "YU";
        hashmap.put("\u5355\u4E8E", as);
        HashMap hashmap1 = sHyphenatedNamePolyPhoneMap;
        String as1[] = new String[2];
        as1[0] = "ZHANG";
        as1[1] = "SUN";
        hashmap1.put("\u957F\u5B59", as1);
        HashMap hashmap2 = sHyphenatedNamePolyPhoneMap;
        String as2[] = new String[2];
        as2[0] = "ZI";
        as2[1] = "JU";
        hashmap2.put("\u5B50\u8F66", as2);
        HashMap hashmap3 = sHyphenatedNamePolyPhoneMap;
        String as3[] = new String[2];
        as3[0] = "MO";
        as3[1] = "QI";
        hashmap3.put("\u4E07\u4FDF", as3);
        HashMap hashmap4 = sHyphenatedNamePolyPhoneMap;
        String as4[] = new String[2];
        as4[0] = "TAN";
        as4[1] = "TAI";
        hashmap4.put("\u6FB9\u53F0", as4);
        HashMap hashmap5 = sHyphenatedNamePolyPhoneMap;
        String as5[] = new String[2];
        as5[0] = "YU";
        as5[1] = "CHI";
        hashmap5.put("\u5C09\u8FDF", as5);
        sLastNamePolyPhoneMap.put(Character.valueOf('\u4E48'), "YAO");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u4E01'), "DING");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u4FDE'), "YU");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u8D3E'), "JIA");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u6C88'), "SHEN");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u535C'), "BU");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u8584'), "BO");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u5B5B'), "BO");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u8D32'), "BEN");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u8D39'), "FEI");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u6CCA'), "BAN");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u8300'), "BI");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u756A'), "BO");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u891A'), "CHU");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u91CD'), "CHONG");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u4F20'), "CHUAN");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u53C2'), "CAN");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u5355'), "SHAN");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u79CD'), "CHONG");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u90D7'), "CHI");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u9561'), "CHAN");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u671D'), "CHAO");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u6CBB'), "CHI");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u555C'), "CHUAI");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u8870'), "CUI");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u6668'), "CHANG");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u4E11'), "CHOU");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u7633'), "CHOU");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u957F'), "CHANG");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u8F66'), "CHE");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u7FDF'), "ZHAI");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u4F43'), "DIAN");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u5200'), "DIAO");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u8C03'), "DIAO");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u9046'), "DI");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u76D6'), "GE");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u7085'), "GUI");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u866B'), "GU");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u7094'), "GUI");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u660B'), "GUI");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u82A5'), "GAI");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u8312'), "KUANG");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u90C7'), "HUAN");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u5DF7'), "XIANG");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u9ED1'), "HE");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u8F69'), "HAN");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u6496'), "HAN");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u9ED8'), "HE");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u89C1'), "JIAN");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u964D'), "JIANG");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u89D2'), "JIAO");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u7F34'), "JIAO");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u8BB0'), "JI");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u741A'), "JU");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u5267'), "JI");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u96BD'), "JUAN");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u9697'), "KUI");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u9B3C'), "KUI");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u61A8'), "KAN");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u9760'), "KU");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u4E50'), "YUE");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u516D'), "LU");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u5587'), "LA");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u96D2'), "LUO");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u4E86'), "LIAO");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u7F2A'), "MIAO");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u4F74'), "MI");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u8C2C'), "MIAO");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u4E5C'), "NIE");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u96BE'), "NING");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u533A'), "OU");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u9022'), "PANG");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u84EC'), "PENG");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u6734'), "PIAO");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u7E41'), "PO");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u4EC7'), "QIU");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u8983'), "TAN");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u79A4'), "QIAN");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u77BF'), "QU");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u53EC'), "SHAO");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u4EC0'), "SHI");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u6298'), "SHE");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u772D'), "SUI");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u89E3'), "XIE");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u7CFB'), "XI");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u5DF7'), "XIANG");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u9664'), "XU");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u5BF0'), "XIAN");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u5458'), "YUAN");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u8D20'), "YUAN");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u66FE'), "ZENG");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u67E5'), "ZHA");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u4F20'), "CHUAN");
        sLastNamePolyPhoneMap.put(Character.valueOf('\u53EC'), "SHAO");
    }
}
