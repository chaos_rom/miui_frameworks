// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.util;

import java.io.*;

public final class IOUtils {

    public IOUtils() {
    }

    public static void closeQuietly(InputStream inputstream) {
        if(inputstream == null)
            break MISSING_BLOCK_LABEL_8;
        inputstream.close();
_L2:
        return;
        IOException ioexception;
        ioexception;
        if(true) goto _L2; else goto _L1
_L1:
    }

    public static void closeQuietly(OutputStream outputstream) {
        if(outputstream == null)
            break MISSING_BLOCK_LABEL_12;
        try {
            outputstream.flush();
        }
        catch(IOException ioexception) { }
        outputstream.close();
_L2:
        return;
        IOException ioexception1;
        ioexception1;
        if(true) goto _L2; else goto _L1
_L1:
    }

    public static void closeQuietly(Reader reader) {
        if(reader == null)
            break MISSING_BLOCK_LABEL_8;
        reader.close();
_L2:
        return;
        IOException ioexception;
        ioexception;
        if(true) goto _L2; else goto _L1
_L1:
    }
}
