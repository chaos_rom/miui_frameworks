// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.util;

import android.content.Context;
import android.content.pm.*;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.Log;
import java.util.HashMap;

public class FileIconHelper {

    public FileIconHelper() {
    }

    private static void addItem(String as[], int i) {
        if(as != null) {
            int j = as.length;
            for(int k = 0; k < j; k++) {
                String s = as[k];
                sFileExtToIcons.put(s.toLowerCase(), Integer.valueOf(i));
            }

        }
    }

    private static Drawable getApkIcon(Context context, String s) {
        PackageManager packagemanager;
        PackageInfo packageinfo;
        packagemanager = context.getPackageManager();
        packageinfo = packagemanager.getPackageArchiveInfo(s, 1);
        if(packageinfo == null) goto _L2; else goto _L1
_L1:
        ApplicationInfo applicationinfo = packageinfo.applicationInfo;
        if(applicationinfo == null) goto _L2; else goto _L3
_L3:
        Drawable drawable1;
        applicationinfo.publicSourceDir = s;
        drawable1 = packagemanager.getApplicationIcon(applicationinfo);
        Drawable drawable = drawable1;
_L5:
        return drawable;
        OutOfMemoryError outofmemoryerror;
        outofmemoryerror;
        Log.e("FileIconHelper", outofmemoryerror.toString());
_L2:
        drawable = context.getResources().getDrawable(0x6020017);
        if(true) goto _L5; else goto _L4
_L4:
    }

    private static String getExtFromFilename(String s) {
        int i = s.lastIndexOf('.');
        String s1;
        if(i != -1)
            s1 = s.substring(i + 1, s.length());
        else
            s1 = "";
        return s1;
    }

    public static int getFileIcon(String s) {
        Integer integer = (Integer)sFileExtToIcons.get(s.toLowerCase());
        int i = 0x6020017;
        if(integer != null)
            i = integer.intValue();
        return i;
    }

    public static Drawable getFileIcon(Context context, String s) {
        String s1 = getExtFromFilename(s);
        Drawable drawable;
        if(s1.equals("apk"))
            drawable = getApkIcon(context, s);
        else
            drawable = context.getResources().getDrawable(getFileIcon(s1));
        return drawable;
    }

    private static final String LOG_TAG = "FileIconHelper";
    private static final String TYPE_APK = "apk";
    private static HashMap sFileExtToIcons = new HashMap();

    static  {
        String as[] = new String[1];
        as[0] = "mp3";
        addItem(as, 0x602000b);
        String as1[] = new String[1];
        as1[0] = "wma";
        addItem(as1, 0x602000c);
        String as2[] = new String[1];
        as2[0] = "wav";
        addItem(as2, 0x602000d);
        String as3[] = new String[1];
        as3[0] = "mid";
        addItem(as3, 0x602000e);
        String as4[] = new String[9];
        as4[0] = "mp4";
        as4[1] = "wmv";
        as4[2] = "mpeg";
        as4[3] = "m4v";
        as4[4] = "3gp";
        as4[5] = "3gpp";
        as4[6] = "3g2";
        as4[7] = "3gpp2";
        as4[8] = "asf";
        addItem(as4, 0x602000f);
        String as5[] = new String[6];
        as5[0] = "jpg";
        as5[1] = "jpeg";
        as5[2] = "gif";
        as5[3] = "png";
        as5[4] = "bmp";
        as5[5] = "wbmp";
        addItem(as5, 0x6020010);
        String as6[] = new String[5];
        as6[0] = "txt";
        as6[1] = "log";
        as6[2] = "xml";
        as6[3] = "ini";
        as6[4] = "lrc";
        addItem(as6, 0x6020011);
        String as7[] = new String[6];
        as7[0] = "doc";
        as7[1] = "ppt";
        as7[2] = "docx";
        as7[3] = "pptx";
        as7[4] = "xsl";
        as7[5] = "xslx";
        addItem(as7, 0x6020012);
        String as8[] = new String[1];
        as8[0] = "pdf";
        addItem(as8, 0x6020013);
        String as9[] = new String[1];
        as9[0] = "zip";
        addItem(as9, 0x6020014);
        String as10[] = new String[1];
        as10[0] = "mtz";
        addItem(as10, 0x6020015);
        String as11[] = new String[1];
        as11[0] = "rar";
        addItem(as11, 0x6020016);
    }
}
