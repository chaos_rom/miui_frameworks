// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.util;

import android.os.Environment;
import android.util.Log;
import java.io.*;
import java.util.Arrays;

// Referenced classes of package miui.util:
//            AllPinyinConstants

public class HanziToPinyinHelper {

    private HanziToPinyinHelper() {
        initResource();
    }

    /**
     * @deprecated Method getIntance is deprecated
     */

    public static HanziToPinyinHelper getIntance() {
        miui/util/HanziToPinyinHelper;
        JVM INSTR monitorenter ;
        HanziToPinyinHelper hanzitopinyinhelper;
        if(sSingleton == null)
            sSingleton = new HanziToPinyinHelper();
        hanzitopinyinhelper = sSingleton;
        miui/util/HanziToPinyinHelper;
        JVM INSTR monitorexit ;
        return hanzitopinyinhelper;
        Exception exception;
        exception;
        throw exception;
    }

    private void initResource() {
        String s = (new StringBuilder()).append(Environment.getRootDirectory()).append(File.separator).append("etc/unicode_py_index.td").toString();
        long l;
        l = System.currentTimeMillis();
        Log.d("HanziToPinyinHelper", "Read");
        mFile = new RandomAccessFile(new File(s), "r");
        byte abyte0[] = new byte[AllPinyinConstants.FILE_TAG.length];
        mFile.read(abyte0);
        if(Arrays.equals(abyte0, AllPinyinConstants.FILE_TAG))
            break MISSING_BLOCK_LABEL_159;
        Log.w("HanziToPinyinHelper", (new StringBuilder()).append("File tag not right ").append(s).toString());
        if(true && mFile != null) {
            try {
                mFile.close();
                mFile = null;
            }
            catch(Exception exception7) {
                exception7.printStackTrace();
            }
            throw new RuntimeException("Please update the data file: unicode_py_index.td");
        }
        break MISSING_BLOCK_LABEL_611;
        byte abyte1[] = new byte[mFile.readInt()];
        mFile.read(abyte1);
        if("26d188162454f2c17cf3194641c1e0fca7c2b72d".equals(new String(abyte1)))
            break MISSING_BLOCK_LABEL_265;
        Log.w("HanziToPinyinHelper", (new StringBuilder()).append("Unmatched digest for ").append(s).toString());
        if(true && mFile != null) {
            try {
                mFile.close();
                mFile = null;
            }
            catch(Exception exception6) {
                exception6.printStackTrace();
            }
            throw new RuntimeException("Please update the data file: unicode_py_index.td");
        }
        break MISSING_BLOCK_LABEL_611;
        if(1 == mFile.readInt())
            break MISSING_BLOCK_LABEL_344;
        Log.w("HanziToPinyinHelper", (new StringBuilder()).append("Unmatched version for ").append(s).toString());
        if(true && mFile != null) {
            try {
                mFile.close();
                mFile = null;
            }
            catch(Exception exception5) {
                exception5.printStackTrace();
            }
            throw new RuntimeException("Please update the data file: unicode_py_index.td");
        }
        break MISSING_BLOCK_LABEL_611;
        Log.d("HanziToPinyinHelper", (new StringBuilder()).append("[").append(System.currentTimeMillis() - l).append("]ms to load data file").toString());
        if(false && mFile != null) {
            try {
                mFile.close();
                mFile = null;
            }
            catch(Exception exception4) {
                exception4.printStackTrace();
            }
            throw new RuntimeException("Please update the data file: unicode_py_index.td");
        }
        break MISSING_BLOCK_LABEL_611;
        FileNotFoundException filenotfoundexception;
        filenotfoundexception;
        Log.w("HanziToPinyinHelper", (new StringBuilder()).append("Can't find ").append(s).toString());
        if(true && mFile != null) {
            try {
                mFile.close();
                mFile = null;
            }
            catch(Exception exception3) {
                exception3.printStackTrace();
            }
            throw new RuntimeException("Please update the data file: unicode_py_index.td");
        }
        break MISSING_BLOCK_LABEL_611;
        IOException ioexception;
        ioexception;
        Log.w("HanziToPinyinHelper", (new StringBuilder()).append("Can't read ").append(s).append("IOException").toString());
        if(true && mFile != null) {
            try {
                mFile.close();
                mFile = null;
            }
            catch(Exception exception2) {
                exception2.printStackTrace();
            }
            throw new RuntimeException("Please update the data file: unicode_py_index.td");
        }
        break MISSING_BLOCK_LABEL_611;
        Exception exception;
        exception;
        if(false && mFile != null) {
            try {
                mFile.close();
                mFile = null;
            }
            catch(Exception exception1) {
                exception1.printStackTrace();
            }
            throw new RuntimeException("Please update the data file: unicode_py_index.td");
        } else {
            throw exception;
        }
    }

    protected void finalize() throws Throwable {
        if(mFile != null)
            try {
                mFile.close();
            }
            catch(IOException ioexception) {
                Log.e("HanziToPinyinHelper", "finalize IOException");
            }
        super.finalize();
    }

    public String[] getPinyinString(char c) {
        String as[];
        int i;
        as = null;
        i = 2 * (c + -19968);
        String as1[];
        long l = i + FILE_HEADER_SIZE;
        mFile.seek(l);
        short word0 = mFile.readShort();
        if(word0 == 0)
            break MISSING_BLOCK_LABEL_93;
        as1 = AllPinyinConstants.ALL_PINYIN[word0].split(",");
        as = as1;
        break MISSING_BLOCK_LABEL_93;
        IOException ioexception;
        ioexception;
        Log.e("HanziToPinyinHelper", "IO:", new Exception());
        break MISSING_BLOCK_LABEL_93;
        NullPointerException nullpointerexception;
        nullpointerexception;
        Log.e("HanziToPinyinHelper", "Unmatch Error:Please rebuild the system to regenerate the data file: unicode_py_index.td");
        return as;
    }

    private static final int FILE_HEADER_SIZE = 0;
    private static final int FILE_VERSION = 1;
    private static final String TAG = "HanziToPinyinHelper";
    public static final String UNICODE_2_PINYIN_RESOURCE_NAME = "etc/unicode_py_index.td";
    private static HanziToPinyinHelper sSingleton;
    private RandomAccessFile mFile;

    static  {
        FILE_HEADER_SIZE = 0 + (4 + (4 + (4 + AllPinyinConstants.FILE_TAG.length + "26d188162454f2c17cf3194641c1e0fca7c2b72d".getBytes().length)));
    }
}
