// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.widget;

import android.accounts.Account;
import android.app.ActionBar;
import android.app.Activity;
import android.content.*;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.os.*;
import android.preference.*;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import java.io.IOException;
import java.util.Map;
import miui.net.*;
import miui.net.exception.*;
import miui.util.EasyMap;
import miui.util.MiCloudSyncUtils;

// Referenced classes of package miui.widget:
//            SyncStatePreference, SimpleDialogFragment

public abstract class MiCloudAdvancedSettingsBase extends PreferenceActivity {
    private static class CheckPhoneTask extends AsyncTask {
        public static final class CheckResult extends Enum {

            public static CheckResult valueOf(String s) {
                return (CheckResult)Enum.valueOf(miui/widget/MiCloudAdvancedSettingsBase$CheckPhoneTask$CheckResult, s);
            }

            public static CheckResult[] values() {
                return (CheckResult[])$VALUES.clone();
            }

            private static final CheckResult $VALUES[];
            public static final CheckResult RESULT_IO_ERROR;
            public static final CheckResult RESULT_NOT_ACTIVATED;
            public static final CheckResult RESULT_OK;

            static  {
                RESULT_OK = new CheckResult("RESULT_OK", 0);
                RESULT_NOT_ACTIVATED = new CheckResult("RESULT_NOT_ACTIVATED", 1);
                RESULT_IO_ERROR = new CheckResult("RESULT_IO_ERROR", 2);
                CheckResult acheckresult[] = new CheckResult[3];
                acheckresult[0] = RESULT_OK;
                acheckresult[1] = RESULT_NOT_ACTIVATED;
                acheckresult[2] = RESULT_IO_ERROR;
                $VALUES = acheckresult;
            }

            private CheckResult(String s, int i) {
                super(s, i);
            }
        }

        public static interface CheckPhoneCallback {

            public abstract void onCheckPhoneResult(Account account, String s, CheckResult checkresult);
        }


        protected volatile Object doInBackground(Object aobj[]) {
            return doInBackground((Void[])aobj);
        }

        protected transient CheckResult doInBackground(Void avoid[]) {
            String s;
            String s1;
            String s2;
            TelephonyManager telephonymanager = (TelephonyManager)mActivity.getSystemService("phone");
            s = telephonymanager.getSubscriberId();
            s1 = telephonymanager.getDeviceId();
            s2 = telephonymanager.getSimSerialNumber();
            String s3;
            String s4;
            s3 = MiCloudAdvancedSettingsBase.queryPhone(s1, s2);
            if(s3 != null)
                break MISSING_BLOCK_LABEL_56;
            s4 = MiCloudAdvancedSettingsBase.queryPhone(s1, s);
            s3 = s4;
            InvalidResponseException invalidresponseexception;
            CheckResult checkresult;
            IOException ioexception;
            if(s3 != null)
                checkresult = CheckResult.RESULT_OK;
            else
                checkresult = CheckResult.RESULT_NOT_ACTIVATED;
            return checkresult;
            ioexception;
            checkresult = CheckResult.RESULT_IO_ERROR;
            continue; /* Loop/switch isn't completed */
            invalidresponseexception;
            checkresult = CheckResult.RESULT_NOT_ACTIVATED;
            if(true) goto _L2; else goto _L1
_L1:
            break MISSING_BLOCK_LABEL_88;
_L2:
            break MISSING_BLOCK_LABEL_66;
        }

        protected volatile void onPostExecute(Object obj) {
            onPostExecute((CheckResult)obj);
        }

        protected void onPostExecute(CheckResult checkresult) {
            mProgressDialog.dismiss();
            if(mCheckCallback != null)
                mCheckCallback.onCheckPhoneResult(mAccount, mAuthority, checkresult);
        }

        protected void onPreExecute() {
            mProgressDialog = (new SimpleDialogFragment.AlertDialogFragmentBuilder(2)).setCancelable(false).create();
            mProgressDialog.show(mActivity.getFragmentManager(), "CheckPhoneProgress");
        }

        private final Account mAccount;
        private final Activity mActivity;
        private final String mAuthority;
        private final CheckPhoneCallback mCheckCallback;
        private SimpleDialogFragment mProgressDialog;

        public CheckPhoneTask(Activity activity, Account account, String s, CheckPhoneCallback checkphonecallback) {
            mActivity = activity;
            mAccount = account;
            mAuthority = s;
            mCheckCallback = checkphonecallback;
        }
    }


    public MiCloudAdvancedSettingsBase() {
        mActivateStatus = -1;
        mReceiver = new BroadcastReceiver() {

            public void onReceive(Context context, Intent intent) {
                boolean flag = true;
                int i = intent.getIntExtra("activate_status", -1);
                Log.d("MiCloudAdvancedSettingsBase", (new StringBuilder()).append("onActivate status changed: ").append(i).toString());
                MiCloudAdvancedSettingsBase micloudadvancedsettingsbase = MiCloudAdvancedSettingsBase.this;
                Boolean boolean1;
                if(i > 0) {
                    if(i != flag)
                        flag = false;
                    boolean1 = Boolean.valueOf(flag);
                } else {
                    boolean1 = null;
                }
                micloudadvancedsettingsbase.updateSyncState(boolean1);
            }

            final MiCloudAdvancedSettingsBase this$0;

             {
                this$0 = MiCloudAdvancedSettingsBase.this;
                super();
            }
        };
        mSyncStatusObserver = new SyncStatusObserver() {

            public void onStatusChanged(int i) {
                mHandler.post(new Runnable() {

                    public void run() {
                        updateSyncState(null);
                    }

                    final _cls7 this$1;

                     {
                        this$1 = _cls7.this;
                        super();
                    }
                });
            }

            final MiCloudAdvancedSettingsBase this$0;

             {
                this$0 = MiCloudAdvancedSettingsBase.this;
                super();
            }
        };
    }

    private void enableSync(Account account, String s, boolean flag) {
        ContentResolver.setSyncAutomatically(account, s, flag);
        if(flag) {
            if(canSync(flag, s))
                MiCloudSyncUtils.requestOrCancelSync(account, s, flag);
        } else {
            MiCloudSyncUtils.requestOrCancelSync(account, s, flag);
        }
    }

    private void handleIntent() {
        Intent intent;
        String s;
        ActionBar actionbar;
        intent = getIntent();
        s = intent.getStringExtra("authority");
        Account account = (Account)intent.getParcelableExtra("account");
        if(s == null)
            throw new IllegalStateException("No authority contained");
        if(account == null)
            throw new IllegalStateException("No account contained");
        mAccount = account;
        mMainSyncPref.setAuthority(s);
        mMainSyncPref.setAccount(account);
        actionbar = getActionBar();
        if(actionbar == null) goto _L2; else goto _L1
_L1:
        ProviderInfo providerinfo;
        actionbar.setHomeButtonEnabled(true);
        providerinfo = getPackageManager().resolveContentProvider(s, 0);
        if(providerinfo != null) goto _L4; else goto _L3
_L3:
        Log.w("MiCloudAdvancedSettingsBase", (new StringBuilder()).append("no provider info for authority:").append(s).toString());
_L6:
        return;
_L4:
        Object obj = providerinfo.loadLabel(getPackageManager());
        if(TextUtils.isEmpty(((CharSequence) (obj)))) {
            Log.w("MiCloudAdvancedSettingsBase", (new StringBuilder()).append("Provider needs a label for authority '").append(s).append("'").toString());
            obj = s;
        }
        actionbar.setTitle(((CharSequence) (obj)));
_L2:
        int i = intent.getIntExtra("extra_activate_err_code", -1);
        switch(i) {
        case 1: // '\001'
            showReactivateDialog(i, new android.content.DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialoginterface, int j) {
                    startActivate();
                }

                final MiCloudAdvancedSettingsBase this$0;

             {
                this$0 = MiCloudAdvancedSettingsBase.this;
                super();
            }
            }, new android.content.DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialoginterface, int j) {
                    turnOffUnActivatedSync();
                }

                final MiCloudAdvancedSettingsBase this$0;

             {
                this$0 = MiCloudAdvancedSettingsBase.this;
                super();
            }
            }, new android.content.DialogInterface.OnDismissListener() {

                public void onDismiss(DialogInterface dialoginterface) {
                    CloudManager.get(MiCloudAdvancedSettingsBase.this).cancelNotification(0x10000001);
                }

                final MiCloudAdvancedSettingsBase this$0;

             {
                this$0 = MiCloudAdvancedSettingsBase.this;
                super();
            }
            });
            break;
        }
        if(true) goto _L6; else goto _L5
_L5:
    }

    private void handleSyncPrefClick(SyncStatePreference syncstatepreference) {
        String s;
        Account account;
        boolean flag;
        s = syncstatepreference.getAuthority();
        account = syncstatepreference.getAccount();
        flag = ContentResolver.getSyncAutomatically(account, s);
        if(!syncstatepreference.isOneTimeSyncMode()) goto _L2; else goto _L1
_L1:
        MiCloudSyncUtils.requestOrCancelSync(account, s, true);
_L4:
        return;
_L2:
        boolean flag1 = syncstatepreference.isChecked();
        if(flag1 != flag)
            if(flag1 && needActivate()) {
                mCheckPhoneTask = new CheckPhoneTask(this, account, s, new CheckPhoneTask.CheckPhoneCallback() {

                    public void onCheckPhoneResult(final Account checkedAccount, final String checkedAuthority, CheckPhoneTask.CheckResult checkresult) {
                        if(checkresult != CheckPhoneTask.CheckResult.RESULT_OK) goto _L2; else goto _L1
_L1:
                        enableSync(checkedAccount, checkedAuthority, true);
                        updateSyncState(null);
_L4:
                        return;
_L2:
                        if(checkresult == CheckPhoneTask.CheckResult.RESULT_NOT_ACTIVATED)
                            showSendSmsForSyncDialog(new android.content.DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialoginterface, int i) {
                                    startActivate();
                                    mMainSyncPref.setEnabled(false);
                                    enableSync(checkedAccount, checkedAuthority, true);
                                }

                                final _cls5 this$1;
                                final Account val$checkedAccount;
                                final String val$checkedAuthority;

                     {
                        this$1 = _cls5.this;
                        checkedAccount = account;
                        checkedAuthority = s;
                        super();
                    }
                            }, new android.content.DialogInterface.OnDismissListener() {

                                public void onDismiss(DialogInterface dialoginterface) {
                                    updateSyncState(null);
                                }

                                final _cls5 this$1;

                     {
                        this$1 = _cls5.this;
                        super();
                    }
                            });
                        else
                        if(checkresult == CheckPhoneTask.CheckResult.RESULT_IO_ERROR) {
                            showAlertDialog(0x60c01e6, 0x60c01e7);
                            updateSyncState(null);
                        }
                        if(true) goto _L4; else goto _L3
_L3:
                    }

                    final MiCloudAdvancedSettingsBase this$0;

             {
                this$0 = MiCloudAdvancedSettingsBase.this;
                super();
            }
                });
                mCheckPhoneTask.execute(new Void[0]);
            } else {
                enableSync(account, s, flag1);
                updateSyncState(null);
            }
        if(true) goto _L4; else goto _L3
_L3:
    }

    private static String queryPhone(String s, String s1) throws IOException, InvalidResponseException {
        String s3;
        String s4;
        EasyMap easymap;
        miui.net.SimpleRequest.MapContent mapcontent;
        String s2 = CloudCoder.hashDeviceInfo(s);
        s3 = CloudCoder.hashDeviceInfo(s1);
        Object aobj[] = new Object[1];
        aobj[0] = s2;
        s4 = String.format("http://api.account.xiaomi.com/pass/activate/dev/%s/activating", aobj);
        easymap = new EasyMap("imsi", s3);
        mapcontent = null;
        miui.net.SimpleRequest.MapContent mapcontent1 = SimpleRequest.getAsMap(s4, easymap, null, true);
        mapcontent = mapcontent1;
_L2:
        if(mapcontent == null)
            throw new IOException("failed to get response from server");
        break; /* Loop/switch isn't completed */
        AccessDeniedException accessdeniedexception;
        accessdeniedexception;
        accessdeniedexception.printStackTrace();
        continue; /* Loop/switch isn't completed */
        AuthenticationFailureException authenticationfailureexception;
        authenticationfailureexception;
        authenticationfailureexception.printStackTrace();
        if(true) goto _L2; else goto _L1
_L1:
        Object obj = mapcontent.getFromBody("code");
        if(INT_0.equals(obj)) {
            Object obj1 = mapcontent.getFromBody("data");
            if(obj1 instanceof Map) {
                Map map = (Map)obj1;
                Object obj2 = map.get("phone");
                String s5;
                if(s3.equals(map.get("imsi")) && (obj2 instanceof String))
                    s5 = (String)obj2;
                else
                    s5 = null;
                return s5;
            }
        }
        throw new InvalidResponseException((new StringBuilder()).append("invalid response from server, description:").append(mapcontent.getFromBody("description")).toString());
    }

    private void showAlertDialog(int i, int j) {
        SimpleDialogFragment simpledialogfragment = (new SimpleDialogFragment.AlertDialogFragmentBuilder(1)).setTitle(getString(i)).setMessage(getString(j)).create();
        simpledialogfragment.setPositiveButton(0x104000a, null);
        simpledialogfragment.show(getFragmentManager(), "AlertDialog");
    }

    private void showSendSmsForSyncDialog(android.content.DialogInterface.OnClickListener onclicklistener, android.content.DialogInterface.OnDismissListener ondismisslistener) {
        SimpleDialogFragment simpledialogfragment = (new SimpleDialogFragment.AlertDialogFragmentBuilder(1)).setTitle(getString(0x60c01e8)).setMessage(getString(0x60c01e9)).create();
        simpledialogfragment.setNegativeButton(0x1040000, null);
        simpledialogfragment.setPositiveButton(0x60c01ea, onclicklistener);
        simpledialogfragment.setOnDismissListener(ondismisslistener);
        simpledialogfragment.show(getFragmentManager(), "SendSMSProgress");
    }

    private void startActivate() {
        startService(new Intent("com.xiaomi.xmsf.action.START_ACTIVATE"));
    }

    private void updateSyncState(Boolean boolean1) {
        if(isResumed()) {
            java.util.List list = ContentResolver.getCurrentSyncs();
            boolean flag;
            SyncStatePreference syncstatepreference;
            boolean flag1;
            boolean flag2;
            if(!TextUtils.isEmpty(((TelephonyManager)getSystemService("phone")).getSimSerialNumber()))
                flag = true;
            else
                flag = false;
            syncstatepreference = mMainSyncPref;
            flag1 = needActivate();
            if(boolean1 == null)
                flag2 = false;
            else
                flag2 = boolean1.booleanValue();
            MiCloudSyncUtils.updateSyncStatus(this, list, syncstatepreference, true, flag, flag1, flag2, getMainSyncPrefSummary());
            onSyncStateUpdate();
        }
    }

    protected abstract boolean canSync(boolean flag, String s);

    protected String getMainSyncPrefSummary() {
        return "";
    }

    protected abstract String getMainSyncPreferenceKey();

    protected abstract int getPreferencesResourceId();

    protected boolean needActivate() {
        return false;
    }

    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        addPreferencesFromResource(getPreferencesResourceId());
        mMainSyncPref = (SyncStatePreference)findPreference(getMainSyncPreferenceKey());
        handleIntent();
        mIntentFilter = new IntentFilter("com.xiaomi.xmsf.action.ACTIVATE_STATUS_CHANGED");
    }

    protected void onDestroy() {
        if(mCheckPhoneTask != null)
            mCheckPhoneTask.cancel(true);
        if(mGetActivateStatusTask != null)
            mGetActivateStatusTask.cancel(true);
        super.onDestroy();
    }

    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        handleIntent();
    }

    public boolean onOptionsItemSelected(MenuItem menuitem) {
        menuitem.getItemId();
        JVM INSTR tableswitch 16908332 16908332: default 24
    //                   16908332 30;
           goto _L1 _L2
_L1:
        return super.onOptionsItemSelected(menuitem);
_L2:
        onBackPressed();
        if(true) goto _L1; else goto _L3
_L3:
    }

    public void onPause() {
        ContentResolver.removeStatusChangeListener(mStatusChangeListenerHandle);
        unregisterReceiver(mReceiver);
        super.onPause();
    }

    public boolean onPreferenceTreeClick(PreferenceScreen preferencescreen, Preference preference) {
        boolean flag = true;
        if(preference == mMainSyncPref)
            handleSyncPrefClick(mMainSyncPref);
        else
        if(preference instanceof SyncStatePreference)
            onSubSyncStateUpdate((SyncStatePreference)preference);
        else
            flag = super.onPreferenceTreeClick(preferencescreen, preference);
        return flag;
    }

    public void onResume() {
        super.onResume();
        mStatusChangeListenerHandle = ContentResolver.addStatusChangeListener(13, mSyncStatusObserver);
        registerReceiver(mReceiver, mIntentFilter);
        if(needActivate() && (mGetActivateStatusTask == null || android.os.AsyncTask.Status.FINISHED == mGetActivateStatusTask.getStatus())) {
            AsyncTask asynctask = new AsyncTask() {

                protected transient Integer doInBackground(Void avoid1[]) {
                    miui.net.CloudManager.CloudManagerFuture cloudmanagerfuture = CloudManager.get(getApplicationContext()).getActivateStatus();
                    Integer integer1 = Integer.valueOf(((Bundle)cloudmanagerfuture.getResult()).getInt("activate_status"));
                    Integer integer = integer1;
_L2:
                    return integer;
                    Exception exception;
                    exception;
                    Log.e("MiCloudAdvancedSettingsBase", "failed to get activate status", exception);
                    integer = Integer.valueOf(0);
                    if(true) goto _L2; else goto _L1
_L1:
                }

                protected volatile Object doInBackground(Object aobj[]) {
                    return doInBackground((Void[])aobj);
                }

                protected void onPostExecute(Integer integer) {
                    boolean flag = true;
                    super.onPostExecute(integer);
                    mActivateStatus = integer.intValue();
                    if(integer.intValue() == 0) {
                        Log.w("MiCloudAdvancedSettingsBase", (new StringBuilder()).append("get error status: ").append(integer).toString());
                        finish();
                    }
                    MiCloudAdvancedSettingsBase micloudadvancedsettingsbase = MiCloudAdvancedSettingsBase.this;
                    if(mActivateStatus != flag)
                        flag = false;
                    micloudadvancedsettingsbase.updateSyncState(Boolean.valueOf(flag));
                }

                protected volatile void onPostExecute(Object obj) {
                    onPostExecute((Integer)obj);
                }

                protected void onPreExecute() {
                    super.onPreExecute();
                    mMainSyncPref.setEnabled(false);
                }

                final MiCloudAdvancedSettingsBase this$0;

             {
                this$0 = MiCloudAdvancedSettingsBase.this;
                super();
            }
            };
            Void avoid[] = new Void[1];
            avoid[0] = (Void)null;
            mGetActivateStatusTask = asynctask.execute(avoid);
        }
        updateSyncState(null);
    }

    protected void onSubSyncStateUpdate(SyncStatePreference syncstatepreference) {
    }

    protected void onSyncStateUpdate() {
    }

    public void showReactivateDialog(int i, android.content.DialogInterface.OnClickListener onclicklistener, android.content.DialogInterface.OnClickListener onclicklistener1, android.content.DialogInterface.OnDismissListener ondismisslistener) {
        SimpleDialogFragment.AlertDialogFragmentBuilder alertdialogfragmentbuilder = (new SimpleDialogFragment.AlertDialogFragmentBuilder(1)).setTitle(getString(0x60c01ec));
        i;
        JVM INSTR tableswitch 1 1: default 40
    //                   1 41;
           goto _L1 _L2
_L1:
        return;
_L2:
        alertdialogfragmentbuilder.setMessage(getString(0x60c01ed));
        SimpleDialogFragment simpledialogfragment = alertdialogfragmentbuilder.create();
        simpledialogfragment.setNegativeButton(0x1040000, onclicklistener1);
        simpledialogfragment.setPositiveButton(0x60c01ea, onclicklistener);
        simpledialogfragment.setOnDismissListener(ondismisslistener);
        simpledialogfragment.show(getFragmentManager(), "ReactivateDialog");
        if(true) goto _L1; else goto _L3
_L3:
    }

    public void turnOffUnActivatedSync() {
        SyncAdapterType asyncadaptertype[] = ContentResolver.getSyncAdapterTypes();
        int i = asyncadaptertype.length;
        for(int j = 0; j < i; j++) {
            SyncAdapterType syncadaptertype = asyncadaptertype[j];
            if(!mAccount.type.equals(syncadaptertype.accountType))
                continue;
            String s = syncadaptertype.authority;
            ContentResolver.setSyncAutomatically(mAccount, s, false);
            if(MiCloudSyncUtils.needActivate(s))
                MiCloudSyncUtils.requestOrCancelSync(mAccount, s, false);
        }

    }

    public static final String ACTION_ACTIVATE_STATUS_CHANGED = "com.xiaomi.xmsf.action.ACTIVATE_STATUS_CHANGED";
    private static final int ACTIVATE_STATUS_ERROR = 0;
    private static final int ACTIVATE_STATUS_NONE = -1;
    private static final Integer INT_0 = Integer.valueOf(0);
    private static final String TAG = "MiCloudAdvancedSettingsBase";
    private static final String URL_QUERY_PHONE = "http://api.account.xiaomi.com/pass/activate/dev/%s/activating";
    private Account mAccount;
    private int mActivateStatus;
    private CheckPhoneTask mCheckPhoneTask;
    private AsyncTask mGetActivateStatusTask;
    private final Handler mHandler = new Handler();
    private IntentFilter mIntentFilter;
    private SyncStatePreference mMainSyncPref;
    private BroadcastReceiver mReceiver;
    private Object mStatusChangeListenerHandle;
    private SyncStatusObserver mSyncStatusObserver;





/*
    static int access$102(MiCloudAdvancedSettingsBase micloudadvancedsettingsbase, int i) {
        micloudadvancedsettingsbase.mActivateStatus = i;
        return i;
    }

*/







}
