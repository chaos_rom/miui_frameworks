// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ImageTextButton extends LinearLayout {

    public ImageTextButton(Context context, AttributeSet attributeset) {
        super(context, attributeset);
        ((LayoutInflater)context.getSystemService("layout_inflater")).inflate(0x603003a, this);
        mTextView = (TextView)findViewById(0x1020014);
        TypedArray typedarray = context.obtainStyledAttributes(attributeset, android.R.styleable.TextView);
        int i = typedarray.getDimensionPixelSize(2, (int)mTextView.getTextSize());
        mTextView.setTextSize(0, i);
        android.content.res.ColorStateList colorstatelist = typedarray.getColorStateList(5);
        if(colorstatelist != null)
            mTextView.setTextColor(colorstatelist);
        android.graphics.drawable.Drawable drawable = typedarray.getDrawable(50);
        if(drawable != null)
            mTextView.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
        int j = typedarray.getDimensionPixelSize(52, 0);
        mTextView.setCompoundDrawablePadding(j);
        String s = typedarray.getString(18);
        mTextView.setText(s);
        typedarray.recycle();
    }

    public TextView getTextView() {
        return mTextView;
    }

    public void setEnabled(boolean flag) {
        mTextView.setEnabled(flag);
        super.setEnabled(flag);
    }

    private TextView mTextView;
}
