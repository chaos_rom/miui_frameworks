// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.widget;

import android.graphics.Bitmap;
import android.util.Pair;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import java.util.*;
import miui.cache.DataCache;
import miui.os.DaemonAsyncTask;
import miui.os.UniqueAsyncTask;

// Referenced classes of package miui.widget:
//            DataGroup

public abstract class AsyncAdapter extends BaseAdapter {
    public abstract class AsyncLoadPartialDataTask extends DaemonAsyncTask {

        public boolean containJob(Object obj) {
            return mDoingJobs.contains(obj);
        }

        protected boolean needsDoJob(Object obj) {
            boolean flag;
            if(!AsyncAdapter.sPartialDataCache.containsKey(obj) && !mDoingJobs.contains(obj))
                flag = true;
            else
                flag = false;
            if(flag)
                mDoingJobs.add(obj);
            return flag;
        }

        protected transient void onProgressUpdate(Pair apair[]) {
            if(apair != null && apair.length != 0) {
                Object obj = apair[0].first;
                Object obj1 = apair[0].second;
                if(obj1 != null) {
                    AsyncAdapter.sPartialDataCache.put(obj, obj1);
                    notifyDataSetChanged();
                }
                mDoingJobs.remove(obj);
                super.onProgressUpdate(apair);
            }
        }

        protected volatile void onProgressUpdate(Object aobj[]) {
            onProgressUpdate((Pair[])aobj);
        }

        Set mDoingJobs;
        final AsyncAdapter this$0;

        public AsyncLoadPartialDataTask() {
            this$0 = AsyncAdapter.this;
            super();
            mDoingJobs = Collections.synchronizedSet(new HashSet());
        }
    }

    public abstract class AsyncLoadMoreDataTask extends UniqueAsyncTask {

        protected volatile Object doInBackground(Object aobj[]) {
            return doInBackground((Void[])aobj);
        }

        protected transient List doInBackground(Void avoid[]) {
            boolean flag = false;
            List list;
            if(mLoadParams == null) {
                list = null;
            } else {
                list = loadMoreData(mLoadParams);
                if(mLoadParams.upwards) {
                    AsyncAdapter asyncadapter1 = AsyncAdapter.this;
                    if(list == null || list.size() == 0)
                        flag = true;
                    asyncadapter1.mReachTop = flag;
                } else {
                    AsyncAdapter asyncadapter = AsyncAdapter.this;
                    if(list == null || list.size() == 0)
                        flag = true;
                    asyncadapter.mReachBottom = flag;
                }
            }
            return list;
        }

        protected boolean hasEquivalentRunningTasks() {
            return false;
        }

        protected abstract List loadMoreData(AsyncLoadMoreParams asyncloadmoreparams);

        protected volatile void onPostExecute(Object obj) {
            onPostExecute((List)obj);
        }

        protected void onPostExecute(List list) {
            if(mClearData && list != null)
                getDataGroup(mGroup).clear();
            if(list != null) {
                for(int i = 0; i < list.size(); i++)
                    getDataGroup(mGroup).add(list.get(i));

            }
            notifyDataSetChanged();
            super.onPostExecute(list);
            postExecuteTask(this);
            postLoadMoreData(list);
        }

        public void setClearData(boolean flag) {
            mClearData = flag;
        }

        public void setGroup(int i) {
            mGroup = i;
        }

        public void setLoadParams(AsyncLoadMoreParams asyncloadmoreparams) {
            mLoadParams = asyncloadmoreparams;
        }

        private boolean mClearData;
        private int mGroup;
        private AsyncLoadMoreParams mLoadParams;
        final AsyncAdapter this$0;

        public AsyncLoadMoreDataTask() {
            this$0 = AsyncAdapter.this;
            super();
        }
    }

    public static class AsyncLoadMoreParams {

        public int cursor;
        public boolean upwards;

        public AsyncLoadMoreParams() {
        }
    }

    public abstract class AsyncLoadDataTask extends UniqueAsyncTask {

        private boolean realNeedExecuteTask() {
            int i = 0;
_L3:
            if(i >= mVisitors.size())
                break MISSING_BLOCK_LABEL_46;
            if(!((AsyncLoadDataVisitor)mVisitors.get(i)).dataChanged()) goto _L2; else goto _L1
_L1:
            boolean flag = true;
_L4:
            return flag;
_L2:
            i++;
              goto _L3
            flag = false;
              goto _L4
        }

        public void addVisitor(AsyncLoadDataVisitor asyncloaddatavisitor) {
            mVisitors.add(asyncloaddatavisitor);
        }

        protected volatile Object doInBackground(Object aobj[]) {
            return doInBackground((Void[])aobj);
        }

        protected transient List doInBackground(Void avoid[]) {
            if(mVisitors.size() > 0) {
                for(int j = 0; j < mVisitors.size(); j++)
                    ((AsyncLoadDataVisitor)mVisitors.get(j)).loadData(this);

            } else {
                int i = 0;
                do {
                    Object aobj[] = loadData(i);
                    if(aobj == null)
                        break;
                    Collections.addAll(mResultDataSet, aobj);
                    publishProgress(aobj);
                    i += aobj.length;
                } while(true);
            }
            return mResultDataSet;
        }

        protected boolean hasEquivalentRunningTasks() {
            return false;
        }

        protected abstract Object[] loadData(int i);

        public transient void onLoadData(Object aobj[]) {
            Collections.addAll(mResultDataSet, aobj);
            publishProgress(aobj);
        }

        protected volatile void onPostExecute(Object obj) {
            onPostExecute((List)obj);
        }

        protected void onPostExecute(List list) {
            if(mLoadUsingCache && !mFirstTimeLoad) {
                getDataGroup(mGroup).clear();
                getDataGroup(mGroup).addAll(mTempDataSet);
                notifyDataSetChanged();
            }
            setForceToLoadData(false);
            super.onPostExecute(list);
            postExecuteTask(this);
            postLoadData(list);
        }

        protected void onPreExecute() {
            boolean flag = false;
            if(!mForceToLoadData && !realNeedExecuteTask()) {
                cancel(false);
                postExecuteTask(this);
            } else {
                if(getDataGroup(mGroup).size() == 0)
                    flag = true;
                mFirstTimeLoad = flag;
                if(!mLoadUsingCache || mFirstTimeLoad)
                    getDataGroup(mGroup).clear();
                else
                    mTempDataSet.clear();
                super.onPreExecute();
            }
        }

        protected transient void onProgressUpdate(Object aobj[]) {
            int i = 0;
            while(i < aobj.length)  {
                if(!mLoadUsingCache || mFirstTimeLoad)
                    getDataGroup(mGroup).add(aobj[i]);
                else
                    mTempDataSet.add(aobj[i]);
                i++;
            }
            if(!mLoadUsingCache || mFirstTimeLoad)
                notifyDataSetChanged();
            super.onProgressUpdate(aobj);
        }

        public void setGroup(int i) {
            mGroup = i;
        }

        private boolean mFirstTimeLoad;
        private int mGroup;
        private List mResultDataSet;
        private List mTempDataSet;
        private List mVisitors;
        final AsyncAdapter this$0;

        public AsyncLoadDataTask() {
            this$0 = AsyncAdapter.this;
            super();
            mFirstTimeLoad = true;
            mResultDataSet = new ArrayList();
            mTempDataSet = new ArrayList();
            mVisitors = new ArrayList();
        }
    }

    public static interface AsyncLoadDataVisitor {

        public abstract boolean dataChanged();

        public abstract void loadData(AsyncLoadDataTask asyncloaddatatask);

        public abstract void setKeyword(String s);
    }


    public AsyncAdapter() {
        mDataSet = new ArrayList();
        mTaskSet = new HashSet();
        mTaskLocker = new byte[0];
        mLoadUsingCache = true;
        mDataPerLine = 1;
    }

    private DataGroup getDataGroup(int i) {
        if(mDataSet.size() != i) goto _L2; else goto _L1
_L1:
        this;
        JVM INSTR monitorenter ;
        if(mDataSet.size() == i)
            mDataSet.add(new DataGroup());
        this;
        JVM INSTR monitorexit ;
_L2:
        return (DataGroup)mDataSet.get(i);
        Exception exception;
        exception;
        this;
        JVM INSTR monitorexit ;
        throw exception;
    }

    private void loadData(int i, AsyncLoadDataTask asyncloaddatatask) {
        if(asyncloaddatatask != null) goto _L2; else goto _L1
_L1:
        return;
_L2:
        asyncloaddatatask.setId((new StringBuilder()).append("loadData-").append(i).toString());
        asyncloaddatatask.setGroup(i);
        if(preExecuteTask(asyncloaddatatask))
            try {
                asyncloaddatatask.execute(new Void[0]);
            }
            catch(IllegalStateException illegalstateexception) { }
        if(true) goto _L1; else goto _L3
_L3:
    }

    private void loadMoreData(boolean flag, boolean flag1, int i, AsyncLoadMoreDataTask asyncloadmoredatatask) {
        if(asyncloadmoredatatask != null) goto _L2; else goto _L1
_L1:
        return;
_L2:
        AsyncLoadMoreParams asyncloadmoreparams = new AsyncLoadMoreParams();
        asyncloadmoreparams.upwards = flag;
        if(flag || getDataCount(i) == 0)
            asyncloadmoreparams.cursor = 0;
        else
            asyncloadmoreparams.cursor = getDataCount(i);
        if(flag1) {
            List list = loadCacheData(asyncloadmoreparams);
            if(list != null) {
                for(int j = 0; j < list.size(); j++)
                    getDataGroup(i).add(list.get(j));

            }
            asyncloadmoredatatask.setClearData(true);
        }
        asyncloadmoredatatask.setLoadParams(asyncloadmoreparams);
        asyncloadmoredatatask.setId((new StringBuilder()).append("loadMoreData-").append(i).toString());
        asyncloadmoredatatask.setGroup(i);
        if(preExecuteTask(asyncloadmoredatatask))
            try {
                asyncloadmoredatatask.execute(new Void[0]);
            }
            catch(IllegalStateException illegalstateexception) { }
        if(true) goto _L1; else goto _L3
_L3:
    }

    private void postExecuteTask(UniqueAsyncTask uniqueasynctask) {
        byte abyte0[] = mTaskLocker;
        abyte0;
        JVM INSTR monitorenter ;
        mTaskSet.remove(uniqueasynctask);
        return;
    }

    private boolean preExecuteTask(UniqueAsyncTask uniqueasynctask) {
        byte abyte0[] = mTaskLocker;
        abyte0;
        JVM INSTR monitorenter ;
        boolean flag;
        if(mTaskSet.contains(uniqueasynctask)) {
            flag = false;
        } else {
            mTaskSet.add(uniqueasynctask);
            flag = true;
        }
        return flag;
    }

    protected abstract View bindContentView(View view, List list, int i, int j, int k);

    protected abstract void bindPartialContentView(View view, Object obj, int i, List list);

    protected abstract List getCacheKeys(Object obj);

    public int getCount() {
        int i = 0;
        for(int j = 0; j < mDataSet.size(); j++)
            i += getCount(j);

        return i;
    }

    protected int getCount(int i) {
        int j = getDataCount(i);
        int k;
        if(j == 0)
            k = 0;
        else
            k = 1 + (j - 1) / mDataPerLine;
        return k;
    }

    public int getDataCount(int i) {
        return getDataGroup(i).size();
    }

    public Object getDataItem(int i, int j) {
        return getDataGroup(j).get(i);
    }

    public int getDataPerLine() {
        return mDataPerLine;
    }

    public Pair getGroupPosition(int i) {
        int j;
        int k;
        j = 0;
        k = 0;
_L3:
        int l;
        if(k >= mDataSet.size())
            break MISSING_BLOCK_LABEL_65;
        l = getCount(k);
        if(i >= j + l) goto _L2; else goto _L1
_L1:
        Pair pair = new Pair(Integer.valueOf(i - j), Integer.valueOf(k));
_L4:
        return pair;
_L2:
        j += l;
        k++;
          goto _L3
        pair = null;
          goto _L4
    }

    public volatile Object getItem(int i) {
        return getItem(i);
    }

    public List getItem(int i) {
        Pair pair = getGroupPosition(i);
        return getItem(((Integer)pair.first).intValue(), ((Integer)pair.second).intValue());
    }

    protected List getItem(int i, int j) {
        int k = i * mDataPerLine;
        int l = Math.min(mDataPerLine, getDataCount(j) - k);
        ArrayList arraylist = new ArrayList();
        for(int i1 = 0; i1 < l; i1++)
            arraylist.add(getDataItem(k + i1, j));

        return arraylist;
    }

    public long getItemId(int i) {
        return (long)i;
    }

    protected List getLoadDataTask() {
        return null;
    }

    protected List getLoadMoreDataTask() {
        return null;
    }

    protected AsyncLoadPartialDataTask getLoadPartialDataTask() {
        return null;
    }

    public View getView(int i, View view, ViewGroup viewgroup) {
        if(getCount() != 0) goto _L2; else goto _L1
_L1:
        View view2 = null;
_L4:
        return view2;
_L2:
        View view1;
        Pair pair = getGroupPosition(i);
        List list = getItem(((Integer)pair.first).intValue(), ((Integer)pair.second).intValue());
        view1 = bindContentView(view, list, i, ((Integer)pair.first).intValue(), ((Integer)pair.second).intValue());
        for(int j = -1 + list.size(); j >= 0; j--)
            loadPartialData(view1, list.get(j), j);

        if(mReachTop || i != mPreloadOffset || !mAutoLoadUpwardsMore)
            break; /* Loop/switch isn't completed */
        loadMoreData(true, false);
_L6:
        view2 = view1;
        if(true) goto _L4; else goto _L3
_L3:
        if(mReachBottom || i != (-1 + getCount()) - mPreloadOffset || !mAutoLoadDownwardsMore) goto _L6; else goto _L5
_L5:
        loadMoreData(false, false);
          goto _L6
    }

    protected boolean isValidKey(Object obj, Object obj1, int i) {
        return sPartialDataCache.containsKey(obj);
    }

    protected List loadCacheData(AsyncLoadMoreParams asyncloadmoreparams) {
        return null;
    }

    public void loadData() {
        List list = getLoadDataTask();
        if(list != null) {
            int i = 0;
            while(i < list.size())  {
                loadData(i, (AsyncLoadDataTask)list.get(i));
                i++;
            }
        }
    }

    public void loadData(int i) {
        List list = getLoadDataTask();
        if(list != null && list.size() > i)
            loadData(i, (AsyncLoadDataTask)list.get(i));
    }

    public void loadMoreData(boolean flag, boolean flag1) {
        List list = getLoadMoreDataTask();
        if(list != null) {
            int i = 0;
            while(i < list.size())  {
                loadMoreData(flag, flag1, i, (AsyncLoadMoreDataTask)list.get(i));
                i++;
            }
        }
    }

    public void loadMoreData(boolean flag, boolean flag1, int i) {
        List list = getLoadMoreDataTask();
        if(list != null && list.size() > i)
            loadMoreData(flag, flag1, i, (AsyncLoadMoreDataTask)list.get(i));
    }

    protected void loadPartialData(View view, Object obj, int i) {
        if(mLoadPartialDataTask != null && mLoadPartialDataTask.getStatus() != android.os.AsyncTask.Status.FINISHED) goto _L2; else goto _L1
_L1:
        mLoadPartialDataTask = getLoadPartialDataTask();
        if(mLoadPartialDataTask != null) goto _L2; else goto _L3
_L3:
        return;
_L2:
        ArrayList arraylist;
        List list;
        int j;
        if(mLoadPartialDataTask.getStatus() == android.os.AsyncTask.Status.PENDING)
            try {
                mLoadPartialDataTask.execute(new Void[0]);
            }
            catch(IllegalStateException illegalstateexception) { }
        arraylist = new ArrayList();
        list = getCacheKeys(obj);
        j = 0;
        while(j < list.size())  {
            Object obj1 = list.get(j);
            if(isValidKey(obj1, obj, j))
                arraylist.add(sPartialDataCache.get(obj1));
            else
            if(!mLoadPartialDataTask.containJob(obj1))
                mLoadPartialDataTask.addJob(obj1);
            j++;
        }
        bindPartialContentView(view, obj, i, arraylist);
        if(true) goto _L3; else goto _L4
_L4:
    }

    public boolean loadingData() {
        boolean flag;
        if(!mTaskSet.isEmpty())
            flag = true;
        else
            flag = false;
        return flag;
    }

    public void onStop() {
        if(mLoadPartialDataTask != null)
            mLoadPartialDataTask.stop();
    }

    protected void postLoadData(List list) {
    }

    protected void postLoadMoreData(List list) {
    }

    protected void postLoadPartialData(List list) {
    }

    public void setAutoLoadMoreStyle(int i) {
        boolean flag = true;
        boolean flag1;
        if((i & 1) != 0)
            flag1 = flag;
        else
            flag1 = false;
        mAutoLoadUpwardsMore = flag1;
        if((i & 2) == 0)
            flag = false;
        mAutoLoadDownwardsMore = flag;
    }

    public void setDataPerLine(int i) {
        mDataPerLine = i;
    }

    public void setDataSet(List list) {
        mDataSet = list;
        notifyDataSetChanged();
    }

    public void setForceToLoadData(boolean flag) {
        mForceToLoadData = flag;
    }

    public void setLoadUsingCache(boolean flag) {
        mLoadUsingCache = flag;
    }

    public void setPreloadOffset(int i) {
        mPreloadOffset = i;
    }

    public void stopAllAsynLoadTask() {
        onStop();
        byte abyte0[] = mTaskLocker;
        abyte0;
        JVM INSTR monitorenter ;
        for(Iterator iterator = mTaskSet.iterator(); iterator.hasNext(); ((UniqueAsyncTask)iterator.next()).cancel(true));
        break MISSING_BLOCK_LABEL_52;
        Exception exception;
        exception;
        throw exception;
        mTaskSet.clear();
        abyte0;
        JVM INSTR monitorexit ;
    }

    public static final int BOTH = 3;
    public static final int DOWNWARDS = 2;
    public static final int NONE = 0;
    public static final int UPWARDS = 1;
    private static DataCache sPartialDataCache = new DataCache(100) {

        protected boolean removeEldestEntry(java.util.Map.Entry entry) {
            boolean flag = super.removeEldestEntry(entry);
            if(flag)
                try {
                    Bitmap bitmap = (Bitmap)(Bitmap)entry.getValue();
                    if(bitmap != null)
                        bitmap.recycle();
                }
                catch(Exception exception) { }
            return flag;
        }

        private static final long serialVersionUID = 1L;

    };
    private boolean mAutoLoadDownwardsMore;
    private boolean mAutoLoadUpwardsMore;
    private int mDataPerLine;
    private List mDataSet;
    private boolean mForceToLoadData;
    private AsyncLoadPartialDataTask mLoadPartialDataTask;
    private boolean mLoadUsingCache;
    private int mPreloadOffset;
    protected boolean mReachBottom;
    protected boolean mReachTop;
    private byte mTaskLocker[];
    private Set mTaskSet;






}
