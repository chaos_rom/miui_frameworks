// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.LinearGradient;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.widget.TextView;

public class GradientTextView extends TextView {

    public GradientTextView(Context context) {
        this(context, null);
    }

    public GradientTextView(Context context, AttributeSet attributeset) {
        this(context, attributeset, 0x1010084);
    }

    public GradientTextView(Context context, AttributeSet attributeset, int i) {
        super(context, attributeset, i);
        if(attributeset != null) {
            TypedArray typedarray = context.obtainStyledAttributes(attributeset, miui.R.styleable.GradientTextView);
            mStartColor = typedarray.getInt(0, 0);
            mEndColor = typedarray.getInt(1, 0);
            typedarray.recycle();
        }
        setLayerType(1, null);
    }

    private void updateShader() {
        TextPaint textpaint = getPaint();
        float f = getWidth();
        int ai[] = new int[2];
        ai[0] = mStartColor;
        ai[1] = mEndColor;
        textpaint.setShader(new LinearGradient(0.0F, 0.0F, f, 0.0F, ai, null, android.graphics.Shader.TileMode.CLAMP));
        invalidate();
    }

    public void onSizeChanged(int i, int j, int k, int l) {
        super.onSizeChanged(i, j, k, l);
        updateShader();
    }

    public void setGradientColor(int i, int j) {
        mStartColor = i;
        mEndColor = j;
        updateShader();
    }

    private int mEndColor;
    private int mStartColor;
}
