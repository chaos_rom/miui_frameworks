// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.*;
import android.widget.*;

// Referenced classes of package miui.widget:
//            GuidePopupWindow

public class GuidePopupView extends FrameLayout {

    public GuidePopupView(Context context) {
        super(context);
    }

    public GuidePopupView(Context context, AttributeSet attributeset) {
        super(context, attributeset);
    }

    public boolean dispatchKeyEvent(KeyEvent keyevent) {
        boolean flag = true;
        if(keyevent.getKeyCode() == 82 && mGuidePopupWindow != null) {
            if(keyevent.getAction() == flag)
                mGuidePopupWindow.dismiss();
        } else {
            flag = super.dispatchKeyEvent(keyevent);
        }
        return flag;
    }

    public int getArrowMode() {
        return mArrowMode;
    }

    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        ((ViewGroup)getParent()).requestChildFocus(this, null);
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        mArrowLeft = (ImageView)findViewById(0x60b0060);
        mArrowRight = (ImageView)findViewById(0x60b0061);
        mContentText = (TextView)findViewById(0x60b0062);
    }

    protected void onLayout(boolean flag, int i, int j, int k, int l) {
        setFrame(i, j, k, l);
        post(mLayoutRunnable);
    }

    public void setAnchor(View view) {
        mAnchor = view;
    }

    public void setArrowMode(int i) {
        mArrowMode = i;
        i;
        JVM INSTR tableswitch 0 1: default 28
    //                   0 59
    //                   1 29;
           goto _L1 _L2 _L3
_L1:
        return;
_L3:
        mArrowLeft.setImageResource(0x602003d);
        mArrowRight.setImageResource(0x602003e);
        mContentText.setBackgroundResource(0x602003f);
        continue; /* Loop/switch isn't completed */
_L2:
        mArrowLeft.setImageResource(0x6020040);
        mArrowRight.setImageResource(0x6020041);
        mContentText.setBackgroundResource(0x6020042);
        if(true) goto _L1; else goto _L4
_L4:
    }

    public void setGuidePopupWindow(GuidePopupWindow guidepopupwindow) {
        mGuidePopupWindow = guidepopupwindow;
    }

    public void setGuideText(int i) {
        mContentText.setText(i);
    }

    public void setGuideText(String s) {
        mContentText.setText(s);
    }

    public void setOffset(int i, int j) {
        mOffsetX = i;
        mOffsetY = j;
    }

    public static final int ARROW_BOTTOM_MODE = 1;
    public static final int ARROW_TOP_MODE;
    private View mAnchor;
    private ImageView mArrowLeft;
    private int mArrowMode;
    private ImageView mArrowRight;
    private TextView mContentText;
    private GuidePopupWindow mGuidePopupWindow;
    private Runnable mLayoutRunnable = new Runnable() {

        public void run() {
            int i = getWidth();
            int j = mContentText.getMeasuredWidth();
            int k = mContentText.getMeasuredHeight();
            int ai[] = new int[2];
            mAnchor.getLocationOnScreen(ai);
            int l = mAnchor.getWidth();
            int i1 = mAnchor.getHeight();
            int j1 = ai[0] + l / 2;
            int k1 = i - j1;
            int l1 = j / 2;
            int i2 = j - l1;
            int j2 = 0;
            int k2;
            int l2;
            int i3;
            int j3;
            if(mArrowMode == 1) {
                j2 = ai[1] - k;
                if(j2 < 0)
                    j2 = 0;
            } else
            if(mArrowMode == 0)
                j2 = i1 + ai[1];
            k2 = j2 + getTop();
            if(j1 >= l1 && k1 >= i2) {
                j1 -= l1;
                k1 -= i2;
            } else
            if(k1 < i2) {
                i2 = k1;
                k1 = 0;
                j1 = i - j;
                l1 = j - i2;
            } else
            if(j1 < l1) {
                l1 = j1;
                j1 = 0;
                k1 = i - j;
                i2 = j - l1;
            }
            l2 = k2 + mOffsetY;
            i3 = j1 + mOffsetX;
            j3 = k1 - mOffsetX;
            mContentText.layout(i3, l2, i3 + j, l2 + k);
            mArrowLeft.layout(i3, l2, i3 + l1, l2 + k);
            mArrowRight.layout(i - j3 - i2, l2, i - j3, l2 + k);
        }

        final GuidePopupView this$0;

             {
                this$0 = GuidePopupView.this;
                super();
            }
    };
    private int mOffsetX;
    private int mOffsetY;







}
