// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.widget;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.*;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.PopupWindow;

// Referenced classes of package miui.widget:
//            GuidePopupView

public class GuidePopupWindow extends PopupWindow
    implements android.view.View.OnTouchListener {

    public GuidePopupWindow(Context context) {
        super(context);
        mDissmissRunnable = new Runnable() {

            public void run() {
                if(isShowing()) {
                    Animation animation = AnimationUtils.loadAnimation(mContext, 0x10a0001);
                    animation.setAnimationListener(new android.view.animation.Animation.AnimationListener() {

                        public void onAnimationEnd(Animation animation1) {
                            if(isShowing())
                                dismiss();
                        }

                        public void onAnimationRepeat(Animation animation1) {
                        }

                        public void onAnimationStart(Animation animation1) {
                        }

                        final _cls1 this$1;

                     {
                        this$1 = _cls1.this;
                        super();
                    }
                    });
                    mPopupView.startAnimation(animation);
                }
            }

            final GuidePopupWindow this$0;

             {
                this$0 = GuidePopupWindow.this;
                super();
            }
        };
        mContext = context;
        init();
    }

    private void init() {
        mPopupView = (GuidePopupView)LayoutInflater.from(mContext).inflate(0x603002f, null, false);
        mPopupView.setGuidePopupWindow(this);
        setArrowMode(0);
        android.content.res.Resources resources = mContext.getResources();
        setContentView(mPopupView);
        setWidth(-1);
        setHeight(-1);
        setFocusable(true);
        setTouchable(false);
        setLayoutInScreenEnabled(true);
        setSoftInputMode(3);
        setBackgroundDrawable(new BitmapDrawable(resources));
        update();
    }

    public void enableOutSideWindowTouchDismiss(boolean flag) {
        Object obj;
        if(flag)
            obj = null;
        else
            obj = this;
        setTouchInterceptor(((android.view.View.OnTouchListener) (obj)));
    }

    public boolean onTouch(View view, MotionEvent motionevent) {
        boolean flag;
        int i;
        int j;
        int ai[];
        flag = false;
        i = (int)motionevent.getX();
        j = (int)motionevent.getY();
        ai = new int[2];
        mAnchor.getLocationInWindow(ai);
        if(motionevent.getAction() != 0 || i < ai[0] || i > mAnchor.getWidth() + ai[0] || j < ai[1] || j > mAnchor.getHeight() + ai[1]) goto _L2; else goto _L1
_L1:
        dismiss();
_L4:
        flag = true;
_L3:
        return flag;
_L2:
        if(motionevent.getAction() != 4) goto _L4; else goto _L3
    }

    public void setArrowMode(int i) {
        mPopupView.setArrowMode(i);
    }

    public void setGuideText(int i) {
        mPopupView.setGuideText(i);
    }

    public void setGuideText(String s) {
        mPopupView.setGuideText(s);
    }

    public void show(View view, int i, int j, boolean flag) {
        mPopupView.setAnchor(view);
        mPopupView.setOffset(i, j);
        mAnchor = view;
        showAtLocation(view, 51, 0, 0);
        if(flag)
            mPopupView.postDelayed(mDissmissRunnable, 5000L);
    }

    private View mAnchor;
    private Context mContext;
    private Runnable mDissmissRunnable;
    private GuidePopupView mPopupView;


}
