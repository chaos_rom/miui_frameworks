// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.widget;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import miui.util.ImageUtils;

// Referenced classes of package miui.widget:
//            AsyncAdapter

public abstract class AsyncImageAdapter extends AsyncAdapter {
    public class AsyncLoadImageTask extends AsyncAdapter.AsyncLoadPartialDataTask {

        protected Object doJob(Object obj) {
            String s;
            int i;
            int j;
            android.graphics.BitmapFactory.Options options1;
            Bitmap bitmap;
            Bitmap bitmap1;
            s = (String)obj;
            android.graphics.BitmapFactory.Options options = ImageUtils.getBitmapSize(s);
            i = options.outWidth;
            j = options.outHeight;
            if(mTargetWidth <= 0 || mTargetHeight <= 0) {
                Log.i("ResourceBrowser", "AsyncImageAdapter does not set valid parameters for target size.");
                mTargetWidth = i;
                mTargetHeight = j;
            }
            options1 = new android.graphics.BitmapFactory.Options();
            if(useLowQualityDecoding())
                options1.inPreferredConfig = android.graphics.Bitmap.Config.RGB_565;
            bitmap = null;
            bitmap1 = null;
            if(mScaled) goto _L2; else goto _L1
_L1:
            Bitmap bitmap3;
            int i1 = Math.min(i, mTargetWidth);
            int j1 = Math.min(j, mTargetHeight);
            int k1 = (i - i1) / 2;
            int l1 = (j - j1) / 2;
            bitmap1 = BitmapFactory.decodeFile(s, options1);
            bitmap3 = Bitmap.createBitmap(bitmap1, k1, l1, i1, j1);
            bitmap = bitmap3;
_L4:
            if(bitmap != bitmap1 && bitmap1 != null)
                bitmap1.recycle();
            return bitmap;
_L2:
            int k;
            int l;
            k = mTargetWidth;
            l = mTargetHeight;
            Exception exception;
            OutOfMemoryError outofmemoryerror;
            Bitmap bitmap2;
            if(mTargetWidth >= i || mTargetHeight >= j) {
                k = i;
                l = j;
            }
            options1.inSampleSize = Math.min(i / k, j / l);
            bitmap1 = BitmapFactory.decodeFile(s, options1);
            bitmap2 = ImageUtils.scaleBitmapToDesire(bitmap1, k, l, false);
            bitmap = bitmap2;
            continue; /* Loop/switch isn't completed */
            outofmemoryerror;
            outofmemoryerror.printStackTrace();
            continue; /* Loop/switch isn't completed */
            exception;
            if(true) goto _L4; else goto _L3
_L3:
        }

        public void setScaled(boolean flag) {
            mScaled = flag;
        }

        public void setTargetSize(int i, int j) {
            mTargetWidth = i;
            mTargetHeight = j;
        }

        private boolean mScaled;
        private int mTargetHeight;
        private int mTargetWidth;
        final AsyncImageAdapter this$0;

        public AsyncLoadImageTask() {
            this$0 = AsyncImageAdapter.this;
            super(AsyncImageAdapter.this);
        }
    }


    public AsyncImageAdapter() {
    }

    protected boolean useLowQualityDecoding() {
        return false;
    }
}
