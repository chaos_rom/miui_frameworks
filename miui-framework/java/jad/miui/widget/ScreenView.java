// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.widget;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.*;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.*;
import android.view.animation.Interpolator;
import android.widget.*;
import java.security.InvalidParameterException;

public class ScreenView extends ViewGroup {
    public static class SavedState extends android.view.View.BaseSavedState {

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(currentScreen);
        }

        public static final android.os.Parcelable.Creator CREATOR = new android.os.Parcelable.Creator() {

            public volatile Object createFromParcel(Parcel parcel) {
                return createFromParcel(parcel);
            }

            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel);
            }

            public volatile Object[] newArray(int i) {
                return newArray(i);
            }

            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }

        };
        int currentScreen;


        private SavedState(Parcel parcel) {
            super(parcel);
            currentScreen = -1;
            currentScreen = parcel.readInt();
        }


        SavedState(Parcelable parcelable) {
            super(parcelable);
            currentScreen = -1;
        }
    }

    private class ScaleDetectorListener
        implements android.view.ScaleGestureDetector.OnScaleGestureListener {

        public boolean onScale(ScaleGestureDetector scalegesturedetector) {
            boolean flag = true;
            float f = scalegesturedetector.getScaleFactor();
            if(mTouchState == 0 && ((float)scalegesturedetector.getTimeDelta() > 200F || f < 0.95F || f > 1.052632F))
                setTouchState(null, 4);
            if(f < 0.8F)
                onPinchIn(scalegesturedetector);
            else
            if(f > 1.2F)
                onPinchOut(scalegesturedetector);
            else
                flag = false;
            return flag;
        }

        public boolean onScaleBegin(ScaleGestureDetector scalegesturedetector) {
            boolean flag;
            if(mTouchState == 0)
                flag = true;
            else
                flag = false;
            return flag;
        }

        public void onScaleEnd(ScaleGestureDetector scalegesturedetector) {
            finishCurrentGesture();
        }

        private static final float VALID_PINCH_IN_RATIO = 0.8F;
        private static final float VALID_PINCH_OUT_RATIO = 1.2F;
        private static final float VALID_PINCH_RATIO = 0.95F;
        private static final float VALID_PINCH_TIME = 200F;
        final ScreenView this$0;

        private ScaleDetectorListener() {
            this$0 = ScreenView.this;
            super();
        }

    }

    private class SliderTouchListener
        implements android.view.View.OnTouchListener {

        public boolean onTouch(View view, MotionEvent motionevent) {
            int i;
            float f;
            int j;
            int k;
            i = view.getWidth();
            f = Math.max(0.0F, Math.min(motionevent.getX(), i - 1));
            j = getScreenCount();
            k = (int)Math.floor((f * (float)j) / (float)i);
            motionevent.getAction();
            JVM INSTR tableswitch 0 3: default 80
        //                       0 82
        //                       1 166
        //                       2 117
        //                       3 166;
               goto _L1 _L2 _L3 _L4 _L3
_L1:
            return true;
_L2:
            if(!mScroller.isFinished())
                mScroller.abortAnimation();
            setTouchState(motionevent, 3);
            continue; /* Loop/switch isn't completed */
_L4:
            setCurrentScreenInner(k);
            scrollTo((int)((f * (float)(j * mChildScreenWidth)) / (float)i - (float)(mChildScreenWidth / 2)), 0);
            continue; /* Loop/switch isn't completed */
_L3:
            snapToScreen(k);
            updateSeekPoints(mCurrentScreen, mNextScreen);
            if(true) goto _L1; else goto _L5
_L5:
        }

        final ScreenView this$0;

        private SliderTouchListener() {
            this$0 = ScreenView.this;
            super();
        }

    }

    protected class SlideBar extends FrameLayout
        implements Indicator {

        protected void dispatchDraw(Canvas canvas) {
            super.dispatchDraw(canvas);
            if(mSlidePoint != null)
                mSlidePoint.draw(canvas, mPos);
        }

        public void fastOffset(int i) {
            mRight = (i + mRight) - mLeft;
            mLeft = i;
        }

        public int getSlideWidth() {
            return getMeasuredWidth() - mPadding.left - mPadding.right;
        }

        protected boolean setFrame(int i, int j, int k, int l) {
            boolean flag = super.setFrame(i, j, k, l);
            if(mSlidePoint != null) {
                mPos.bottom = l - j - mPadding.bottom;
                mPos.top = mPos.bottom - mSlidePoint.getHeight();
            }
            return flag;
        }

        public void setPosition(int i, int j) {
            mPos.left = i + mPadding.left;
            mPos.right = j + mPadding.left;
        }

        private Rect mPadding;
        private Rect mPos;
        private NinePatch mSlidePoint;
        private Bitmap mSlidePointBmp;
        final ScreenView this$0;

        public SlideBar(Context context) {
            this$0 = ScreenView.this;
            super(context);
            mPos = new Rect();
            mPadding = new Rect();
            mSlidePointBmp = BitmapFactory.decodeResource(context.getResources(), 0x60200ea);
            if(mSlidePointBmp != null) goto _L2; else goto _L1
_L1:
            return;
_L2:
            byte abyte0[] = mSlidePointBmp.getNinePatchChunk();
            if(abyte0 != null) {
                mSlidePoint = new NinePatch(mSlidePointBmp, abyte0, null);
                FrameLayout framelayout = new FrameLayout(mContext);
                framelayout.setBackgroundResource(0x60200eb);
                addView(framelayout, new android.widget.FrameLayout.LayoutParams(-1, -2, 80));
                mPadding.left = framelayout.getPaddingLeft();
                mPadding.top = framelayout.getPaddingTop();
                mPadding.right = framelayout.getPaddingRight();
                mPadding.bottom = framelayout.getPaddingBottom();
                mPos.top = mPadding.top;
                mPos.bottom = mPos.top + mSlidePointBmp.getHeight();
            }
            if(true) goto _L1; else goto _L3
_L3:
        }
    }

    protected class SeekBarIndicator extends LinearLayout
        implements Indicator {

        public void fastOffset(int i) {
            mRight = (i + mRight) - mLeft;
            mLeft = i;
        }

        final ScreenView this$0;

        public SeekBarIndicator(Context context) {
            this$0 = ScreenView.this;
            super(context);
            setDrawingCacheEnabled(true);
        }
    }

    protected class ArrowIndicator extends ImageView
        implements Indicator {

        public void fastOffset(int i) {
            mRight = (i + mRight) - mLeft;
            mLeft = i;
        }

        final ScreenView this$0;

        public ArrowIndicator(Context context) {
            this$0 = ScreenView.this;
            super(context);
        }
    }

    private static interface Indicator {

        public abstract void fastOffset(int i);
    }

    private class ScreenViewOvershootInterpolator
        implements Interpolator {

        public void disableSettle() {
            mTension = 0.0F;
        }

        public float getInterpolation(float f) {
            float f1 = f - 1.0F;
            return 1.0F + f1 * f1 * (f1 * (1.0F + mTension) + mTension);
        }

        public void setDistance(int i, int j) {
            float f;
            if(i > 0)
                f = mOvershootTension / (float)i;
            else
                f = mOvershootTension;
            mTension = f;
        }

        private float mTension;
        final ScreenView this$0;


/*
        static float access$002(ScreenViewOvershootInterpolator screenviewovershootinterpolator, float f) {
            screenviewovershootinterpolator.mTension = f;
            return f;
        }

*/

        public ScreenViewOvershootInterpolator() {
            this$0 = ScreenView.this;
            super();
            mTension = mOvershootTension;
        }
    }


    public ScreenView(Context context) {
        ViewGroup(context);
        mFirstLayout = true;
        mArrowLeftOnResId = 0x60200e3;
        mArrowLeftOffResId = 0x60200e4;
        mArrowRightOnResId = 0x60200e5;
        mArrowRightOffResId = 0x60200e6;
        mSeekPointResId = 0x60200e7;
        mVisibleRange = 1;
        mScreenWidth = 0;
        mNextScreen = -1;
        mOverScrollRatio = 0.3333333F;
        mScreenCounter = 0;
        mTouchState = 0;
        isFromcomputeScroll = false;
        mAllowLongPress = true;
        mActivePointerId = -1;
        mConfirmHorizontalScrollRatio = 0.5F;
        mScreenSnapDuration = 300;
        mScreenTransitionType = 0;
        mOvershootTension = 1.3F;
        mGestureVelocityTracker = new GestureVelocityTracker();
        DEFAULT_CAMERA_DISTANCE = 1280F * Resources.getSystem().getDisplayMetrics().density;
        initScreenView();
    }

    public ScreenView(Context context, AttributeSet attributeset) {
        ScreenView(context, attributeset, 0);
    }

    public ScreenView(Context context, AttributeSet attributeset, int i) {
        ViewGroup(context, attributeset, i);
        mFirstLayout = true;
        mArrowLeftOnResId = 0x60200e3;
        mArrowLeftOffResId = 0x60200e4;
        mArrowRightOnResId = 0x60200e5;
        mArrowRightOffResId = 0x60200e6;
        mSeekPointResId = 0x60200e7;
        mVisibleRange = 1;
        mScreenWidth = 0;
        mNextScreen = -1;
        mOverScrollRatio = 0.3333333F;
        mScreenCounter = 0;
        mTouchState = 0;
        isFromcomputeScroll = false;
        mAllowLongPress = true;
        mActivePointerId = -1;
        mConfirmHorizontalScrollRatio = 0.5F;
        mScreenSnapDuration = 300;
        mScreenTransitionType = 0;
        mOvershootTension = 1.3F;
        mGestureVelocityTracker = new GestureVelocityTracker();
        DEFAULT_CAMERA_DISTANCE = 1280F * Resources.getSystem().getDisplayMetrics().density;
        initScreenView();
    }

    private ImageView createSeekPoint() {
        ImageView imageview = new ImageView(mContext);
        imageview.setScaleType(android.widget.ImageView.ScaleType.CENTER);
        imageview.setImageResource(mSeekPointResId);
        return imageview;
    }

    private void initScreenView() {
        setAlwaysDrawnWithCacheEnabled(true);
        setClipToPadding(true);
        mScrollInterpolator = new ScreenViewOvershootInterpolator();
        mScroller = new Scroller(mContext, mScrollInterpolator);
        setCurrentScreenInner(0);
        ViewConfiguration viewconfiguration = ViewConfiguration.get(mContext);
        mTouchSlop = viewconfiguration.getScaledTouchSlop();
        setMaximumSnapVelocity(viewconfiguration.getScaledMaximumFlingVelocity());
        mScaleDetector = new ScaleGestureDetector(mContext, new ScaleDetectorListener());
    }

    private void onTouchEventUnique(MotionEvent motionevent) {
        mGestureVelocityTracker.addMovement(motionevent);
        if(mTouchState == 0 || 4 == mTouchState)
            mScaleDetector.onTouchEvent(motionevent);
    }

    private void refreshScrollBound() {
        mScrollLeftBound = (int)((float)(-mChildScreenWidth) * mOverScrollRatio) - mScrollOffset;
        if(!mScrollWholeScreen)
            mScrollRightBound = (int)((float)mChildScreenWidth * ((float)getScreenCount() + mOverScrollRatio) - (float)mScreenWidth) + mScrollOffset;
        else
            mScrollRightBound = (int)((float)(((-1 + getScreenCount()) / mVisibleRange) * mScreenWidth) + (float)mChildScreenWidth * mOverScrollRatio);
    }

    private boolean scrolledFarEnough(MotionEvent motionevent) {
        boolean flag = false;
        float f = Math.abs(motionevent.getX(0) - mLastMotionX);
        if(f > Math.abs(motionevent.getY(0) - mLastMotionY) * mConfirmHorizontalScrollRatio && f > (float)(mTouchSlop * motionevent.getPointerCount()))
            flag = true;
        return flag;
    }

    private void snapByVelocity(int i) {
        if(mChildScreenWidth > 0 && getCurrentScreen() != null) {
            int j = (int)mGestureVelocityTracker.getXVelocity(1000, mMaximumVelocity, i);
            int k = mGestureVelocityTracker.getFlingDirection(Math.abs(j));
            if(k == 1 && mCurrentScreen > 0)
                snapToScreen(mCurrentScreen - mVisibleRange, j, true);
            else
            if(k == 2 && mCurrentScreen < -1 + getScreenCount())
                snapToScreen(mCurrentScreen + mVisibleRange, j, true);
            else
            if(k == 3) {
                snapToScreen(mCurrentScreen, j, true);
            } else {
                int l = mChildScreenWidth;
                int i1;
                int j1;
                if(mScrollWholeScreen)
                    i1 = mVisibleRange;
                else
                    i1 = 1;
                j1 = l * i1;
                snapToScreen((mScrollX + (j1 >> 1)) / mChildScreenWidth, 0, true);
            }
        }
    }

    private void updateArrowIndicatorResource(int i) {
        if(mArrowLeft != null) {
            ArrowIndicator arrowindicator = mArrowLeft;
            int j;
            ArrowIndicator arrowindicator1;
            int k;
            if(i <= 0)
                j = mArrowLeftOffResId;
            else
                j = mArrowLeftOnResId;
            arrowindicator.setImageResource(j);
            arrowindicator1 = mArrowRight;
            if(i >= getScreenCount() * mChildScreenWidth - mScreenWidth - mScrollOffset)
                k = mArrowRightOffResId;
            else
                k = mArrowRightOnResId;
            arrowindicator1.setImageResource(k);
        }
    }

    private void updateIndicatorPositions(int i) {
        if(getWidth() <= 0) goto _L2; else goto _L1
_L1:
        int j;
        int k;
        int l;
        int i1;
        j = getScreenCount();
        k = getWidth();
        l = getHeight();
        i1 = 0;
_L16:
        if(i1 >= mIndicatorCount) goto _L2; else goto _L3
_L3:
        View view;
        android.widget.FrameLayout.LayoutParams layoutparams;
        int j1;
        int k1;
        int l1;
        int i2;
        int j2;
        view = getChildAt(i1 + j);
        layoutparams = (android.widget.FrameLayout.LayoutParams)view.getLayoutParams();
        j1 = view.getMeasuredWidth();
        k1 = view.getMeasuredHeight();
        l1 = 0;
        i2 = 0;
        j2 = layoutparams.gravity;
        if(j2 == -1) goto _L5; else goto _L4
_L4:
        int k2;
        int l2;
        k2 = j2 & 7;
        l2 = j2 & 0x70;
        k2;
        JVM INSTR tableswitch 1 5: default 140
    //                   1 257
    //                   2 140
    //                   3 247
    //                   4 140
    //                   5 280;
           goto _L6 _L7 _L6 _L8 _L6 _L9
_L6:
        l1 = layoutparams.leftMargin;
_L14:
        l2;
        JVM INSTR lookupswitch 3: default 184
    //                   16: 305
    //                   48: 295
    //                   80: 329;
           goto _L10 _L11 _L12 _L13
_L10:
        i2 = ((android.view.ViewGroup.MarginLayoutParams) (layoutparams)).topMargin;
_L5:
        if(!view.isLayoutRequested() && view.getHeight() > 0 && view.getWidth() > 0 || isFromcomputeScroll) {
            ((Indicator)view).fastOffset(i + l1);
            view.invalidate();
        } else {
            view.layout(i + l1, i2, j1 + (i + l1), i2 + k1);
        }
        i1++;
        continue; /* Loop/switch isn't completed */
_L8:
        l1 = layoutparams.leftMargin;
          goto _L14
_L7:
        l1 = ((k - j1) / 2 + layoutparams.leftMargin) - layoutparams.rightMargin;
          goto _L14
_L9:
        l1 = k - j1 - layoutparams.rightMargin;
          goto _L14
_L12:
        i2 = ((android.view.ViewGroup.MarginLayoutParams) (layoutparams)).topMargin;
          goto _L5
_L11:
        i2 = ((l - k1) / 2 + ((android.view.ViewGroup.MarginLayoutParams) (layoutparams)).topMargin) - ((android.view.ViewGroup.MarginLayoutParams) (layoutparams)).bottomMargin;
          goto _L5
_L13:
        i2 = l - k1 - ((android.view.ViewGroup.MarginLayoutParams) (layoutparams)).bottomMargin;
          goto _L5
_L2:
        return;
        if(true) goto _L16; else goto _L15
_L15:
    }

    private void updateScreenOffset() {
        mScreenAlignment;
        JVM INSTR tableswitch 0 3: default 36
    //                   0 50
    //                   1 61
    //                   2 69
    //                   3 87;
           goto _L1 _L2 _L3 _L4 _L5
_L1:
        mScrollOffset = mScrollOffset + mPaddingLeft;
        return;
_L2:
        mScrollOffset = mScreenOffset;
        continue; /* Loop/switch isn't completed */
_L3:
        mScrollOffset = 0;
        continue; /* Loop/switch isn't completed */
_L4:
        mScrollOffset = (mScreenWidth - mChildScreenWidth) / 2;
        continue; /* Loop/switch isn't completed */
_L5:
        mScrollOffset = mScreenWidth - mChildScreenWidth;
        if(true) goto _L1; else goto _L6
_L6:
    }

    private void updateSeekPoints(int i, int j) {
        if(mScreenSeekBar != null) {
            int k = getScreenCount();
            for(int l = 0; l < mVisibleRange && i + l < k; l++)
                mScreenSeekBar.getChildAt(i + l).setSelected(false);

            for(int i1 = 0; i1 < mVisibleRange && j + i1 < k; i1++)
                mScreenSeekBar.getChildAt(j + i1).setSelected(true);

        }
    }

    private void updateSlidePointPosition(int i) {
        int j = getScreenCount();
        if(mSlideBar != null && j > 0) {
            int k = mSlideBar.getSlideWidth();
            int l = Math.max((k / j) * mVisibleRange, 48);
            int i1 = j * mChildScreenWidth;
            int j1;
            if(i1 <= k)
                j1 = 0;
            else
                j1 = (i * (k - l)) / (i1 - k);
            mSlideBar.setPosition(j1, j1 + l);
            if(isHardwareAccelerated())
                mSlideBar.invalidate();
        }
    }

    public void addIndicator(View view, android.widget.FrameLayout.LayoutParams layoutparams) {
        mIndicatorCount = 1 + mIndicatorCount;
        addView(view, -1, layoutparams);
    }

    public void addIndicatorAt(View view, android.widget.FrameLayout.LayoutParams layoutparams, int i) {
        int j = Math.max(-1, Math.min(i, mIndicatorCount));
        if(j >= 0)
            j += getScreenCount();
        mIndicatorCount = 1 + mIndicatorCount;
        addView(view, j, layoutparams);
    }

    public void addView(View view, int i, android.view.ViewGroup.LayoutParams layoutparams) {
        int j = getScreenCount();
        int k;
        if(i < 0)
            k = j;
        else
            k = Math.min(i, j);
        if(mScreenSeekBar != null)
            mScreenSeekBar.addView(createSeekPoint(), k, SEEK_POINT_LAYOUT_PARAMS);
        mScreenCounter = 1 + mScreenCounter;
        refreshScrollBound();
        addView(view, k, layoutparams);
    }

    public boolean allowLongPress() {
        return mAllowLongPress;
    }

    public void computeScroll() {
        isFromcomputeScroll = true;
        if(!mScroller.computeScrollOffset()) goto _L2; else goto _L1
_L1:
        int i = mScroller.getCurrX();
        mScrollX = i;
        mTouchX = i;
        mSmoothingTime = (float)System.nanoTime() / 1E+09F;
        mScrollY = mScroller.getCurrY();
        postInvalidate();
_L4:
        updateIndicatorPositions(mScrollX);
        updateSlidePointPosition(mScrollX);
        updateArrowIndicatorResource(mScrollX);
        isFromcomputeScroll = false;
        return;
_L2:
        if(mNextScreen != -1) {
            setCurrentScreenInner(Math.max(0, Math.min(mNextScreen, -1 + getScreenCount())));
            mNextScreen = -1;
        } else
        if(mTouchState == 1) {
            float f = (float)System.nanoTime() / 1E+09F;
            float f1 = (float)Math.exp((f - mSmoothingTime) / SMOOTHING_CONSTANT);
            float f2 = mTouchX - (float)mScrollX;
            mScrollX = (int)((float)mScrollX + f2 * f1);
            mSmoothingTime = f;
            if(f2 > 1.0F || f2 < -1F)
                postInvalidate();
        }
        if(true) goto _L4; else goto _L3
_L3:
    }

    public boolean dispatchUnhandledMove(View view, int i) {
        boolean flag = true;
        if(i != 17) goto _L2; else goto _L1
_L1:
        if(mCurrentScreen <= 0) goto _L4; else goto _L3
_L3:
        snapToScreen(-1 + mCurrentScreen);
_L6:
        return flag;
_L2:
        if(i == 66 && mCurrentScreen < -1 + getScreenCount()) {
            snapToScreen(1 + mCurrentScreen);
            continue; /* Loop/switch isn't completed */
        }
_L4:
        flag = dispatchUnhandledMove(view, i);
        if(true) goto _L6; else goto _L5
_L5:
    }

    protected boolean drawChild(Canvas canvas, View view, long l) {
        updateChildStaticTransformation(view);
        return drawChild(canvas, view, l);
    }

    protected void finishCurrentGesture() {
        mCurrentGestureFinished = true;
        setTouchState(null, 0);
    }

    public View getCurrentScreen() {
        return getScreen(mCurrentScreen);
    }

    public int getCurrentScreenIndex() {
        if(mNextScreen != -1)
            mCurrentScreen = mNextScreen;
        return mCurrentScreen;
    }

    public View getScreen(int i) {
        View view;
        if(i < 0 || i >= getScreenCount())
            view = null;
        else
            view = getChildAt(i);
        return view;
    }

    public final int getScreenCount() {
        return mScreenCounter;
    }

    public int getScreenTransitionType() {
        return mScreenTransitionType;
    }

    protected int getTouchState() {
        return mTouchState;
    }

    public int getVisibleRange() {
        return mVisibleRange;
    }

    protected void onAttachedToWindow() {
        onAttachedToWindow();
        computeScroll();
    }

    public boolean onInterceptTouchEvent(MotionEvent motionevent) {
        boolean flag = false;
        0xff & motionevent.getAction();
        JVM INSTR tableswitch 0 3: default 40
    //                   0 112
    //                   1 191
    //                   2 83
    //                   3 191;
           goto _L1 _L2 _L3 _L4 _L3
_L1:
        if(2 != (0xff & motionevent.getAction()))
            onTouchEventUnique(motionevent);
        if(mCurrentGestureFinished || mTouchState != 0 && mTouchState != 3)
            flag = true;
        return flag;
_L4:
        onTouchEventUnique(motionevent);
        if(mTouchState == 0 && scrolledFarEnough(motionevent))
            setTouchState(motionevent, 1);
        continue; /* Loop/switch isn't completed */
_L2:
        motionevent.setAction(3);
        mScaleDetector.onTouchEvent(motionevent);
        motionevent.setAction(0);
        mCurrentGestureFinished = false;
        mTouchIntercepted = false;
        mLastMotionX = motionevent.getX();
        mLastMotionY = motionevent.getY();
        if(mScroller.isFinished()) {
            mAllowLongPress = true;
        } else {
            mScroller.abortAnimation();
            setTouchState(motionevent, 1);
        }
        continue; /* Loop/switch isn't completed */
_L3:
        setTouchState(motionevent, 0);
        if(true) goto _L1; else goto _L5
_L5:
    }

    protected void onLayout(boolean flag, int i, int j, int k, int l) {
        setFrame(i, j, k, l);
        int _tmp = i + mPaddingLeft;
        int _tmp1 = k - mPaddingRight;
        updateIndicatorPositions(mScrollX);
        int i1 = getScreenCount();
        int j1 = 0;
        for(int k1 = 0; k1 < i1; k1++) {
            View view = getChildAt(k1);
            if(view.getVisibility() != 8) {
                view.layout(j1, mPaddingTop + mScreenPaddingTop, j1 + view.getMeasuredWidth(), mPaddingTop + mScreenPaddingTop + view.getMeasuredHeight());
                j1 += view.getMeasuredWidth();
            }
        }

        if(mScrollWholeScreen && mCurrentScreen % mVisibleRange > 0)
            setCurrentScreen(mCurrentScreen - mCurrentScreen % mVisibleRange);
    }

    protected void onMeasure(int i, int j) {
        mWidthMeasureSpec = i;
        mHeightMeasureSpec = j;
        int k = 0;
        int l = 0;
        int i1 = getScreenCount();
        for(int j1 = 0; j1 < mIndicatorCount; j1++) {
            View view1 = getChildAt(j1 + i1);
            android.view.ViewGroup.LayoutParams layoutparams1 = view1.getLayoutParams();
            view1.measure(getChildMeasureSpec(i, mPaddingLeft + mPaddingRight, layoutparams1.width), getChildMeasureSpec(j, mPaddingTop + mScreenPaddingTop + mPaddingBottom + mScreenPaddingBottom, layoutparams1.height));
            l = Math.max(l, view1.getMeasuredWidth());
            k = Math.max(k, view1.getMeasuredHeight());
        }

        int k1 = 0;
        int l1 = 0;
        for(int i2 = 0; i2 < i1; i2++) {
            View view = getChildAt(i2);
            android.view.ViewGroup.LayoutParams layoutparams = view.getLayoutParams();
            view.measure(getChildMeasureSpec(i, mPaddingLeft + mPaddingRight, layoutparams.width), getChildMeasureSpec(j, mPaddingTop + mScreenPaddingTop + mPaddingBottom + mScreenPaddingBottom, layoutparams.height));
            l1 = Math.max(l1, view.getMeasuredWidth());
            k1 = Math.max(k1, view.getMeasuredHeight());
        }

        int j2 = Math.max(l1, l);
        int k2 = Math.max(k1, k);
        int l2 = j2 + (mPaddingLeft + mPaddingRight);
        int i3 = k2 + (mPaddingTop + mScreenPaddingTop + mPaddingBottom + mScreenPaddingBottom);
        setMeasuredDimension(resolveSize(l2, i), resolveSize(i3, j));
        if(i1 > 0) {
            mChildScreenWidth = l1;
            mScreenWidth = android.view.View.MeasureSpec.getSize(i) - mPaddingLeft - mPaddingRight;
            updateScreenOffset();
            setOverScrollRatio(mOverScrollRatio);
            if(mChildScreenWidth > 0)
                mVisibleRange = Math.max(1, (mScreenWidth + mChildScreenWidth / 2) / mChildScreenWidth);
        }
        if(mFirstLayout && mVisibleRange > 0) {
            mFirstLayout = false;
            setHorizontalScrollBarEnabled(false);
            setCurrentScreen(mCurrentScreen);
            setHorizontalScrollBarEnabled(true);
        }
    }

    public void onPause() {
        if(!mScroller.isFinished()) {
            setCurrentScreen((int)Math.floor((mScroller.getCurrX() + mChildScreenWidth / 2) / mChildScreenWidth));
            mScroller.abortAnimation();
        }
    }

    protected void onPinchIn(ScaleGestureDetector scalegesturedetector) {
    }

    protected void onPinchOut(ScaleGestureDetector scalegesturedetector) {
    }

    protected void onRestoreInstanceState(Parcelable parcelable) {
        SavedState savedstate = (SavedState)parcelable;
        onRestoreInstanceState(savedstate.getSuperState());
        if(savedstate.currentScreen != -1)
            setCurrentScreen(savedstate.currentScreen);
    }

    public void onResume() {
    }

    protected Parcelable onSaveInstanceState() {
        SavedState savedstate = new SavedState(onSaveInstanceState());
        savedstate.currentScreen = mCurrentScreen;
        return savedstate;
    }

    public void onSecondaryPointerDown(MotionEvent motionevent, int i) {
        mLastMotionX = motionevent.getX(motionevent.findPointerIndex(i));
        mTouchX = mScrollX;
        mSmoothingTime = (float)System.nanoTime() / 1E+09F;
        mGestureVelocityTracker.init(i);
        mGestureVelocityTracker.addMovement(motionevent);
        mTouchState = 1;
    }

    public void onSecondaryPointerMove(MotionEvent motionevent, int i) {
        float f = motionevent.getX(motionevent.findPointerIndex(i));
        float f1 = mLastMotionX - f;
        mLastMotionX = f;
        if(f1 != 0.0F)
            scrollTo((int)(f1 + mTouchX), 0);
        else
            awakenScrollBars();
        mGestureVelocityTracker.addMovement(motionevent);
    }

    public void onSecondaryPointerUp(MotionEvent motionevent, int i) {
        snapByVelocity(i);
        mGestureVelocityTracker.recycle();
        mTouchState = 0;
    }

    public boolean onTouchEvent(MotionEvent motionevent) {
        int i = 0;
        if(!mCurrentGestureFinished) goto _L2; else goto _L1
_L1:
        return true;
_L2:
        if(mTouchIntercepted)
            onTouchEventUnique(motionevent);
        0xff & motionevent.getAction();
        JVM INSTR tableswitch 0 6: default 72
    //                   0 72
    //                   1 198
    //                   2 80
    //                   3 198
    //                   4 72
    //                   5 72
    //                   6 223;
           goto _L3 _L3 _L4 _L5 _L4 _L3 _L3 _L6
_L6:
        break MISSING_BLOCK_LABEL_223;
_L3:
        break; /* Loop/switch isn't completed */
_L5:
        break; /* Loop/switch isn't completed */
_L8:
        mTouchIntercepted = true;
        if(true) goto _L1; else goto _L7
_L7:
        if(mTouchState == 0 && scrolledFarEnough(motionevent))
            setTouchState(motionevent, 1);
        if(mTouchState == 1) {
            int k = motionevent.findPointerIndex(mActivePointerId);
            if(k == -1) {
                setTouchState(motionevent, 1);
                k = motionevent.findPointerIndex(mActivePointerId);
            }
            float f = motionevent.getX(k);
            float f1 = mLastMotionX - f;
            mLastMotionX = f;
            if(f1 != 0.0F)
                scrollTo(Math.round(f1 + mTouchX), 0);
            else
                awakenScrollBars();
        }
          goto _L8
_L4:
        if(mTouchState == 1)
            snapByVelocity(mActivePointerId);
        setTouchState(motionevent, 0);
          goto _L8
        int j = (0xff00 & motionevent.getAction()) >> 8;
        if(motionevent.getPointerId(j) == mActivePointerId) {
            if(j == 0)
                i = 1;
            mLastMotionX = motionevent.getX(i);
            mActivePointerId = motionevent.getPointerId(i);
            mGestureVelocityTracker.init(mActivePointerId);
        }
          goto _L8
    }

    public void removeAllScreens() {
        removeScreensInLayout(0, getScreenCount());
        requestLayout();
        invalidate();
    }

    public void removeAllViewsInLayout() {
        mIndicatorCount = 0;
        mScreenCounter = 0;
        removeAllViewsInLayout();
    }

    public void removeIndicator(View view) {
        int i = indexOfChild(view);
        if(i < getScreenCount()) {
            throw new InvalidParameterException("The view passed through the parameter must be indicator.");
        } else {
            mIndicatorCount = -1 + mIndicatorCount;
            removeViewAt(i);
            return;
        }
    }

    public void removeScreen(int i) {
        if(i >= getScreenCount())
            throw new InvalidParameterException("The view specified by the index must be a screen.");
        if(i != mCurrentScreen) goto _L2; else goto _L1
_L1:
        if(mScrollWholeScreen) goto _L4; else goto _L3
_L3:
        setCurrentScreen(Math.max(0, i - 1));
_L2:
        if(mScreenSeekBar != null)
            mScreenSeekBar.removeViewAt(i);
        mScreenCounter = -1 + mScreenCounter;
        removeViewAt(i);
        return;
_L4:
        if(i != 0 && i == -1 + getScreenCount())
            snapToScreen(i - 1);
        if(true) goto _L2; else goto _L5
_L5:
    }

    public void removeScreensInLayout(int i, int j) {
        if(i >= 0 && i < getScreenCount()) {
            int k = Math.min(j, getScreenCount() - i);
            if(mScreenSeekBar != null)
                mScreenSeekBar.removeViewsInLayout(i, k);
            mScreenCounter = 0;
            removeViewsInLayout(i, k);
        }
    }

    public void removeView(View view) {
        throw new UnsupportedOperationException("ScreenView doesn't support remove view directly.");
    }

    public void removeViewAt(int i) {
        throw new UnsupportedOperationException("ScreenView doesn't support remove view directly.");
    }

    public void removeViewInLayout(View view) {
        throw new UnsupportedOperationException("ScreenView doesn't support remove view directly.");
    }

    public void removeViews(int i, int j) {
        throw new UnsupportedOperationException("ScreenView doesn't support remove view directly.");
    }

    public void removeViewsInLayout(int i, int j) {
        throw new UnsupportedOperationException("ScreenView doesn't support remove view directly.");
    }

    public boolean requestChildRectangleOnScreen(View view, Rect rect, boolean flag) {
        int i = indexOfChild(view);
        boolean flag1;
        if(i < getScreenCount()) {
            if(i != mCurrentScreen || !mScroller.isFinished()) {
                snapToScreen(i);
                flag1 = true;
            } else {
                flag1 = false;
            }
        } else {
            flag1 = requestChildRectangleOnScreen(view, rect, flag);
        }
        return flag1;
    }

    protected void resetTransformation(View view) {
        view.setAlpha(1.0F);
        view.setTranslationX(0.0F);
        view.setTranslationY(0.0F);
        view.setPivotX(0.0F);
        view.setPivotY(0.0F);
        view.setRotation(0.0F);
        view.setRotationX(0.0F);
        view.setRotationY(0.0F);
        view.setCameraDistance(DEFAULT_CAMERA_DISTANCE);
        view.setScaleX(1.0F);
        view.setScaleY(1.0F);
    }

    public void scrollTo(int i, int j) {
        mTouchX = Math.max(mScrollLeftBound, Math.min(i, mScrollRightBound));
        mSmoothingTime = (float)System.nanoTime() / 1E+09F;
        scrollTo((int)mTouchX, j);
    }

    public void scrollToScreen(int i) {
        if(mScrollWholeScreen)
            i -= i % mVisibleRange;
        measure(mWidthMeasureSpec, mHeightMeasureSpec);
        scrollTo(i * mChildScreenWidth - mScrollOffset, 0);
    }

    public void setAllowLongPress(boolean flag) {
        mAllowLongPress = flag;
    }

    public void setArrowIndicatorMarginRect(Rect rect) {
        if(rect == null) goto _L2; else goto _L1
_L1:
        android.widget.FrameLayout.LayoutParams layoutparams;
        android.widget.FrameLayout.LayoutParams layoutparams1;
        if(mArrowLeft == null) {
            layoutparams = new LayoutParams(-2, -2, 19);
            mArrowLeft = new ArrowIndicator(mContext);
            mArrowLeft.setImageResource(mArrowLeftOnResId);
            addIndicator(mArrowLeft, layoutparams);
            layoutparams1 = new LayoutParams(-2, -2, 21);
            mArrowRight = new ArrowIndicator(mContext);
            mArrowRight.setImageResource(mArrowRightOnResId);
            addIndicator(mArrowRight, layoutparams1);
        } else {
            layoutparams = (android.widget.FrameLayout.LayoutParams)mArrowLeft.getLayoutParams();
            layoutparams1 = (android.widget.FrameLayout.LayoutParams)mArrowRight.getLayoutParams();
        }
        layoutparams.setMargins(rect.left, rect.top, 0, rect.bottom);
        layoutparams1.setMargins(0, rect.top, rect.right, rect.bottom);
_L4:
        return;
_L2:
        if(mArrowLeft != null) {
            removeIndicator(mArrowLeft);
            removeIndicator(mArrowRight);
            mArrowLeft = null;
            mArrowRight = null;
        }
        if(true) goto _L4; else goto _L3
_L3:
    }

    public void setArrowIndicatorResource(int i, int j, int k, int l) {
        mArrowLeftOnResId = i;
        mArrowLeftOffResId = j;
        mArrowRightOnResId = k;
        mArrowRightOffResId = l;
    }

    public void setConfirmHorizontalScrollRatio(float f) {
        mConfirmHorizontalScrollRatio = f;
    }

    public void setCurrentScreen(int i) {
        int j;
        if(mScrollWholeScreen) {
            int k = Math.max(0, Math.min(i, -1 + getScreenCount()));
            j = k - k % mVisibleRange;
        } else {
            j = Math.max(0, Math.min(i, getScreenCount() - mVisibleRange));
        }
        setCurrentScreenInner(j);
        if(!mFirstLayout) {
            if(!mScroller.isFinished())
                mScroller.abortAnimation();
            scrollToScreen(mCurrentScreen);
            invalidate();
        }
    }

    protected void setCurrentScreenInner(int i) {
        updateSeekPoints(mCurrentScreen, i);
        mCurrentScreen = i;
        mNextScreen = -1;
    }

    public void setIndicatorBarVisibility(int i) {
        setSeekBarVisibility(i);
        setSlideBarVisibility(i);
    }

    public void setMaximumSnapVelocity(int i) {
        mMaximumVelocity = i;
    }

    public void setOnLongClickListener(android.view.View.OnLongClickListener onlongclicklistener) {
        mLongClickListener = onlongclicklistener;
        int i = getScreenCount();
        for(int j = 0; j < i; j++)
            getChildAt(j).setOnLongClickListener(onlongclicklistener);

    }

    public void setOverScrollRatio(float f) {
        mOverScrollRatio = f;
        refreshScrollBound();
    }

    public void setOvershootTension(float f) {
        mOvershootTension = f;
        if(mScrollInterpolator != null)
            mScrollInterpolator.mTension = f;
    }

    public void setScreenAlignment(int i) {
        mScreenAlignment = i;
    }

    public void setScreenOffset(int i) {
        mScreenOffset = i;
        mScreenAlignment = 0;
        requestLayout();
    }

    public void setScreenPadding(Rect rect) {
        if(rect == null) {
            throw new InvalidParameterException("The padding parameter can not be null.");
        } else {
            mScreenPaddingTop = rect.top;
            mScreenPaddingBottom = rect.bottom;
            setPadding(rect.left, 0, rect.right, 0);
            return;
        }
    }

    public void setScreenSnapDuration(int i) {
        mScreenSnapDuration = i;
    }

    public void setScreenTransitionType(int i) {
        if(i == mScreenTransitionType) goto _L2; else goto _L1
_L1:
        mScreenTransitionType = i;
        mScreenTransitionType;
        JVM INSTR tableswitch 0 8: default 68
    //                   0 69
    //                   1 85
    //                   2 85
    //                   3 100
    //                   4 116
    //                   5 131
    //                   6 68
    //                   7 146
    //                   8 161;
           goto _L2 _L3 _L4 _L4 _L5 _L6 _L7 _L2 _L8 _L9
_L2:
        return;
_L3:
        setOvershootTension(1.3F);
        setScreenSnapDuration(300);
        continue; /* Loop/switch isn't completed */
_L4:
        setOvershootTension(0.0F);
        setScreenSnapDuration(270);
        continue; /* Loop/switch isn't completed */
_L5:
        setOvershootTension(1.3F);
        setScreenSnapDuration(300);
        continue; /* Loop/switch isn't completed */
_L6:
        setOvershootTension(0.0F);
        setScreenSnapDuration(330);
        continue; /* Loop/switch isn't completed */
_L7:
        setOvershootTension(0.0F);
        setScreenSnapDuration(330);
        continue; /* Loop/switch isn't completed */
_L8:
        setOvershootTension(0.0F);
        setScreenSnapDuration(270);
        continue; /* Loop/switch isn't completed */
_L9:
        setOvershootTension(1.3F);
        setScreenSnapDuration(330);
        if(true) goto _L2; else goto _L10
_L10:
    }

    public void setScrollWholeScreen(boolean flag) {
        mScrollWholeScreen = flag;
    }

    public void setSeekBarPosition(android.widget.FrameLayout.LayoutParams layoutparams) {
        if(layoutparams == null) goto _L2; else goto _L1
_L1:
        if(mScreenSeekBar == null) {
            mScreenSeekBar = new SeekBarIndicator(mContext);
            mScreenSeekBar.setGravity(16);
            mScreenSeekBar.setAnimationCacheEnabled(false);
            addIndicator(mScreenSeekBar, layoutparams);
        } else {
            mScreenSeekBar.setLayoutParams(layoutparams);
        }
_L4:
        return;
_L2:
        if(mScreenSeekBar != null) {
            removeIndicator(mScreenSeekBar);
            mScreenSeekBar = null;
        }
        if(true) goto _L4; else goto _L3
_L3:
    }

    public void setSeekBarVisibility(int i) {
        if(mScreenSeekBar != null)
            mScreenSeekBar.setVisibility(i);
    }

    public void setSeekPointResource(int i) {
        mSeekPointResId = i;
    }

    public void setSlideBarPosition(android.widget.FrameLayout.LayoutParams layoutparams) {
        if(layoutparams == null) goto _L2; else goto _L1
_L1:
        if(mSlideBar == null) {
            mSlideBar = new SlideBar(mContext);
            mSlideBar.setOnTouchListener(new SliderTouchListener());
            mSlideBar.setAnimationCacheEnabled(false);
            addIndicator(mSlideBar, layoutparams);
        } else {
            mSlideBar.setLayoutParams(layoutparams);
        }
_L4:
        return;
_L2:
        if(mSlideBar != null) {
            removeIndicator(mSlideBar);
            mSlideBar = null;
        }
        if(true) goto _L4; else goto _L3
_L3:
    }

    public void setSlideBarVisibility(int i) {
        if(mSlideBar != null)
            mSlideBar.setVisibility(i);
    }

    public void setTouchSlop(int i) {
        mTouchSlop = i;
    }

    protected void setTouchState(MotionEvent motionevent, int i) {
        mTouchState = i;
        ViewParent viewparent = getParent();
        boolean flag;
        if(mTouchState != 0)
            flag = true;
        else
            flag = false;
        viewparent.requestDisallowInterceptTouchEvent(flag);
        if(mTouchState != 0) goto _L2; else goto _L1
_L1:
        mActivePointerId = -1;
        mAllowLongPress = false;
        mGestureVelocityTracker.recycle();
_L4:
        return;
_L2:
        if(motionevent != null)
            mActivePointerId = motionevent.getPointerId(0);
        if(mAllowLongPress) {
            mAllowLongPress = false;
            View view = getChildAt(mCurrentScreen);
            if(view != null)
                view.cancelLongPress();
        }
        if(mTouchState == 1) {
            mLastMotionX = motionevent.getX(motionevent.findPointerIndex(mActivePointerId));
            mTouchX = mScrollX;
            mSmoothingTime = (float)System.nanoTime() / 1E+09F;
        }
        if(true) goto _L4; else goto _L3
_L3:
    }

    public void snapToScreen(int i) {
        snapToScreen(i, 0, false);
    }

    protected void snapToScreen(int i, int j, boolean flag) {
        if(mScreenWidth > 0) goto _L2; else goto _L1
_L1:
        return;
_L2:
        int k;
        int l;
        int i1;
        if(mScrollWholeScreen) {
            mNextScreen = Math.max(0, Math.min(i, -1 + getScreenCount()));
            mNextScreen = mNextScreen - mNextScreen % mVisibleRange;
        } else {
            mNextScreen = Math.max(0, Math.min(i, getScreenCount() - mVisibleRange));
        }
        k = Math.max(1, Math.abs(mNextScreen - mCurrentScreen));
        if(!mScroller.isFinished())
            mScroller.abortAnimation();
        l = Math.abs(j);
        if(flag)
            mScrollInterpolator.setDistance(k, l);
        else
            mScrollInterpolator.disableSettle();
        i1 = mNextScreen * mChildScreenWidth - mScrollOffset - mScrollX;
        if(i1 != 0) {
            int j1 = (Math.abs(i1) * mScreenSnapDuration) / mScreenWidth;
            if(l > 0)
                j1 += (int)(0.4F * ((float)j1 / ((float)l / 2500F)));
            int k1 = Math.max(mScreenSnapDuration, j1);
            if(k <= 1)
                k1 = Math.min(k1, 2 * mScreenSnapDuration);
            mScroller.startScroll(mScrollX, 0, i1, 0, k1);
            invalidate();
        }
        if(true) goto _L1; else goto _L3
_L3:
    }

    protected void updateChildStaticTransformation(View view) {
        float f;
        float f1;
        float f3;
        float f4;
        float f5;
        f = view.getMeasuredWidth();
        f1 = view.getMeasuredHeight();
        float f2 = (float)getMeasuredWidth() / 2.0F;
        f3 = f / 2.0F;
        f4 = f1 / 2.0F;
        f5 = ((f2 + (float)mScrollX) - (float)view.getLeft() - f3) / f;
        mScreenTransitionType;
        JVM INSTR tableswitch 0 9: default 112
    //                   0 113
    //                   1 121
    //                   2 129
    //                   3 229
    //                   4 322
    //                   5 422
    //                   6 112
    //                   7 547
    //                   8 658
    //                   9 760;
           goto _L1 _L2 _L3 _L4 _L5 _L6 _L7 _L1 _L8 _L9 _L10
_L1:
        return;
_L2:
        resetTransformation(view);
        continue; /* Loop/switch isn't completed */
_L3:
        resetTransformation(view);
        continue; /* Loop/switch isn't completed */
_L4:
        if(f5 == 0.0F || Math.abs(f5) > 1.0F) {
            resetTransformation(view);
        } else {
            view.setAlpha(0.3F + 0.7F * (1.0F - Math.abs(f5)));
            view.setTranslationX(0.0F);
            view.setTranslationY(0.0F);
            view.setScaleX(1.0F);
            view.setScaleY(1.0F);
            view.setPivotX(0.0F);
            view.setPivotY(0.0F);
            view.setRotation(0.0F);
            view.setRotationX(0.0F);
            view.setRotationY(0.0F);
            view.setCameraDistance(DEFAULT_CAMERA_DISTANCE);
        }
        continue; /* Loop/switch isn't completed */
_L5:
        if(f5 == 0.0F || Math.abs(f5) > 1.0F) {
            resetTransformation(view);
        } else {
            view.setAlpha(1.0F);
            view.setTranslationX(0.0F);
            view.setTranslationY(0.0F);
            view.setScaleX(1.0F);
            view.setScaleY(1.0F);
            view.setPivotX(f3);
            view.setPivotY(f1);
            view.setRotation(30F * -f5);
            view.setRotationX(0.0F);
            view.setRotationY(0.0F);
            view.setCameraDistance(DEFAULT_CAMERA_DISTANCE);
        }
        continue; /* Loop/switch isn't completed */
_L6:
        if(f5 == 0.0F || Math.abs(f5) > 1.0F) {
            resetTransformation(view);
        } else {
            view.setAlpha(1.0F);
            view.setTranslationX(0.0F);
            view.setTranslationY(0.0F);
            view.setScaleX(1.0F);
            view.setScaleY(1.0F);
            if(f5 < 0.0F)
                f = 0.0F;
            view.setPivotX(f);
            view.setPivotY(f4);
            view.setRotation(0.0F);
            view.setRotationX(0.0F);
            view.setRotationY(-90F * f5);
            view.setCameraDistance(5000F);
        }
        continue; /* Loop/switch isn't completed */
_L7:
        if(f5 == 0.0F || Math.abs(f5) > 1.0F) {
            resetTransformation(view);
        } else {
            view.setAlpha(1.0F - Math.abs(f5));
            view.setTranslationY(0.0F);
            view.setTranslationX(f * f5 - 0.3F * (f * Math.abs(f5)));
            float f7 = 1.0F + 0.3F * f5;
            view.setScaleX(f7);
            view.setScaleY(f7);
            view.setPivotX(0.0F);
            view.setPivotY(f4);
            view.setRotation(0.0F);
            view.setRotationX(0.0F);
            view.setRotationY(45F * -f5);
            view.setCameraDistance(5000F);
        }
        continue; /* Loop/switch isn't completed */
_L8:
        if(f5 <= 0.0F) {
            resetTransformation(view);
        } else {
            view.setAlpha(1.0F - f5);
            float f6 = 0.6F + 0.4F * (1.0F - f5);
            view.setTranslationX(3F * (f * (1.0F - f6)));
            view.setTranslationY(0.5F * (f1 * (1.0F - f6)));
            view.setScaleX(f6);
            view.setScaleY(f6);
            view.setPivotX(0.0F);
            view.setPivotY(0.0F);
            view.setRotation(0.0F);
            view.setRotationX(0.0F);
            view.setRotationY(0.0F);
            view.setCameraDistance(DEFAULT_CAMERA_DISTANCE);
        }
        continue; /* Loop/switch isn't completed */
_L9:
        if(f5 == 0.0F || Math.abs(f5) > 1.0F) {
            resetTransformation(view);
        } else {
            view.setAlpha(1.0F - Math.abs(f5));
            view.setTranslationX(f * f5);
            view.setTranslationY(0.0F);
            view.setScaleX(1.0F);
            view.setScaleY(1.0F);
            view.setPivotX(f3);
            view.setPivotY(f4);
            view.setRotation(0.0F);
            view.setRotationX(0.0F);
            view.setRotationY(90F * -f5);
            view.setCameraDistance(5000F);
        }
        continue; /* Loop/switch isn't completed */
_L10:
        updateChildStaticTransformationByScreen(view, f5);
        if(true) goto _L1; else goto _L11
_L11:
    }

    protected void updateChildStaticTransformationByScreen(View view, float f) {
    }

    private static final float BASELINE_FLING_VELOCITY = 2500F;
    private static final float DEFAULT_OVER_SHOOT_TENSION = 1.3F;
    private static final int DEFAULT_SCREEN_SNAP_DURATION = 300;
    private static final float FLING_VELOCITY_INFLUENCE = 0.4F;
    protected static final int INDICATOR_MEASURE_SPEC = 0;
    private static final int INVALID_POINTER = -1;
    protected static final int INVALID_SCREEN = -1;
    public static final int MAX_TOUCH_STATE = 4;
    protected static final int MINIMAL_SLIDE_BAR_POINT_WIDTH = 48;
    private static final float NANOTIME_DIV = 1E+09F;
    public static final int SCREEN_ALIGN_CENTER = 2;
    public static final int SCREEN_ALIGN_CUSTOMIZED = 0;
    public static final int SCREEN_ALIGN_LEFT = 1;
    public static final int SCREEN_ALIGN_RIGHT = 3;
    public static final int SCREEN_TRANSITION_TYPE_CLASSIC = 0;
    public static final int SCREEN_TRANSITION_TYPE_CLASSIC_NO_OVER_SHOOT = 1;
    public static final int SCREEN_TRANSITION_TYPE_CROSSFADE = 2;
    public static final int SCREEN_TRANSITION_TYPE_CUBE = 4;
    public static final int SCREEN_TRANSITION_TYPE_CUSTOM = 9;
    public static final int SCREEN_TRANSITION_TYPE_FALLDOWN = 3;
    public static final int SCREEN_TRANSITION_TYPE_LEFTPAGE = 5;
    public static final int SCREEN_TRANSITION_TYPE_RIGHTPAGE = 6;
    public static final int SCREEN_TRANSITION_TYPE_ROTATE = 8;
    public static final int SCREEN_TRANSITION_TYPE_STACK = 7;
    protected static final android.widget.LinearLayout.LayoutParams SEEK_POINT_LAYOUT_PARAMS = new LayoutParams(-1, -1, 1.0F);
    private static final float SMOOTHING_CONSTANT = 0F;
    private static final float SMOOTHING_SPEED = 0.75F;
    private static final int SNAP_VELOCITY = 300;
    private static final String TAG = "ScreenView";
    protected static final int TOUCH_STATE_PINCHING = 4;
    protected static final int TOUCH_STATE_REST = 0;
    protected static final int TOUCH_STATE_SCROLLING = 1;
    protected static final int TOUCH_STATE_SLIDING = 3;
    protected final float DEFAULT_CAMERA_DISTANCE;
    private boolean isFromcomputeScroll;
    protected int mActivePointerId;
    private boolean mAllowLongPress;
    private ArrowIndicator mArrowLeft;
    private int mArrowLeftOffResId;
    private int mArrowLeftOnResId;
    private ArrowIndicator mArrowRight;
    private int mArrowRightOffResId;
    private int mArrowRightOnResId;
    protected int mChildScreenWidth;
    private float mConfirmHorizontalScrollRatio;
    private boolean mCurrentGestureFinished;
    protected int mCurrentScreen;
    protected boolean mFirstLayout;
    GestureVelocityTracker mGestureVelocityTracker;
    protected int mHeightMeasureSpec;
    private int mIndicatorCount;
    protected float mLastMotionX;
    protected float mLastMotionY;
    protected android.view.View.OnLongClickListener mLongClickListener;
    private int mMaximumVelocity;
    protected int mNextScreen;
    protected float mOverScrollRatio;
    private float mOvershootTension;
    private ScaleGestureDetector mScaleDetector;
    protected int mScreenAlignment;
    private int mScreenCounter;
    protected int mScreenOffset;
    protected int mScreenPaddingBottom;
    protected int mScreenPaddingTop;
    protected SeekBarIndicator mScreenSeekBar;
    private int mScreenSnapDuration;
    private int mScreenTransitionType;
    protected int mScreenWidth;
    private ScreenViewOvershootInterpolator mScrollInterpolator;
    protected int mScrollLeftBound;
    protected int mScrollOffset;
    protected int mScrollRightBound;
    protected boolean mScrollWholeScreen;
    protected Scroller mScroller;
    private int mSeekPointResId;
    protected SlideBar mSlideBar;
    private float mSmoothingTime;
    private boolean mTouchIntercepted;
    private int mTouchSlop;
    private int mTouchState;
    private float mTouchX;
    protected int mVisibleRange;
    protected int mWidthMeasureSpec;

    static  {
        INDICATOR_MEASURE_SPEC = android.view.View.MeasureSpec.makeMeasureSpec(0, 0);
        SMOOTHING_CONSTANT = (float)(0.016D / Math.log(0.75D));
    }





}
