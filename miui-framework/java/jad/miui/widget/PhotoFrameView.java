// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.*;
import android.util.AttributeSet;
import android.util.FloatMath;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import java.io.IOException;

public class PhotoFrameView extends View {
    public static interface OnSizeChangedListener {

        public abstract void onSizeChanged(PhotoFrameView photoframeview);
    }

    public class InsideBoundStrategy
        implements BoundStrategy, android.view.animation.Animation.AnimationListener {

        private void startAdjust(boolean flag) {
            setInteractive(false);
            JustifyAnimation justifyanimation = new JustifyAnimation(flag);
            justifyanimation.setDuration(400L);
            justifyanimation.setAnimationListener(this);
            startAnimation(justifyanimation);
        }

        public void adjustCropArea(Matrix matrix, boolean flag) {
            int i = getWidth();
            int j = getHeight();
            float f = mBitmapDisplayed.getWidth();
            float f1 = mBitmapDisplayed.getHeight();
            RectF rectf = getImageBounds();
            matrix.reset();
            if(mRotation != 0)
                matrix.postRotate(mRotation, f / 2.0F, f1 / 2.0F);
            float f2 = 0.0F;
            float f3 = 0.0F;
            float f4;
            float f5;
            float f6;
            if(flag)
                f4 = getFitCenterScale();
            else
                f4 = getScale();
            if(!isVertical()) {
                f2 = (f4 * (f1 - f)) / 2.0F;
                f3 = (f4 * (f - f1)) / 2.0F;
                float f7 = f1;
                f1 = f;
                f = f7;
            }
            if(flag) {
                if((f * f4) / 2.0F > (float)i / 2.0F)
                    f5 = f2 + ((float)i - f * f4) / 2.0F;
                else
                    f5 = f2 + ((float)getWidth() - f * f4) / 2.0F;
                if((f1 * f4) / 2.0F > (float)j / 2.0F - (float)mTop)
                    f6 = f3 + ((float)j - f1 * f4) / 2.0F;
                else
                    f6 = f3 + (((float)getWidth() - f1 * f4) / 2.0F + (float)mTop);
            } else {
                if(rectf.right - rectf.left > (float)getWidth()) {
                    if(rectf.left > 0.0F)
                        f5 = f2 + 0.0F;
                    else
                    if(rectf.right < (float)getWidth())
                        f5 = f2 + ((float)getWidth() - f * f4);
                    else
                        f5 = f2 + rectf.left;
                } else {
                    f5 = f2 + ((float)getWidth() - f * f4) / 2.0F;
                }
                if(rectf.bottom - rectf.top > (float)getWidth()) {
                    if(rectf.top > (float)mTop)
                        f6 = f3 + (float)mTop;
                    else
                    if(rectf.bottom < (float)(getWidth() + mTop))
                        f6 = f3 + ((float)(getWidth() + mTop) - f1 * f4);
                    else
                        f6 = f3 + rectf.top;
                } else {
                    f6 = f3 + (((float)getWidth() - f1 * f4) / 2.0F + (float)mTop);
                }
            }
            matrix.postScale(f4, f4);
            matrix.postTranslate(f5, f6);
        }

        public float adjustDx(RectF rectf, float f) {
            if(rectf.left > 0.0F || rectf.right < (float)getWidth())
                f /= 2.0F;
            return f;
        }

        public float adjustDy(RectF rectf, float f) {
            if(rectf.top > (float)mTop || rectf.bottom < (float)(mTop + getWidth()))
                f /= 2.0F;
            return f;
        }

        public void adjustIfNeeded(RectF rectf, float f, boolean flag) {
            if(!flag) goto _L2; else goto _L1
_L1:
            if(f < getFitCenterScale())
                startAdjust(true);
_L4:
            return;
_L2:
            if(rectf.left > 0.0F || rectf.right < (float)getWidth() || rectf.top > (float)mTop || rectf.bottom < (float)(mTop + getWidth()))
                startAdjust(false);
            if(true) goto _L4; else goto _L3
_L3:
        }

        public Matrix getCropedMatrix() {
            float af[] = new float[9];
            mDisplayMatrix.getValues(af);
            Matrix matrix = new Matrix();
            matrix.setValues(af);
            float f = 96F / (float)getWidth();
            matrix.postScale(f, f, 0.0F, 0.0F);
            matrix.postTranslate(0.0F, f * (float)(-mTop));
            return matrix;
        }

        public int getCropedPhotoHeight() {
            return 96;
        }

        public int getCropedPhotoWidth() {
            return 96;
        }

        public void onAnimationEnd(Animation animation) {
            resetMatrix(((JustifyAnimation)animation).getResetScale());
            setInteractive(true);
        }

        public void onAnimationRepeat(Animation animation) {
        }

        public void onAnimationStart(Animation animation) {
        }

        public static final int DEFAULT_INSIDE_TOP;
        final int mTop;
        final PhotoFrameView this$0;

        public InsideBoundStrategy(int i) {
            this$0 = PhotoFrameView.this;
            super();
            mTop = i;
        }
    }

    public class OutsideBoundStrategy
        implements BoundStrategy {

        public void adjustCropArea(Matrix matrix, boolean flag) {
            float f = mBitmapDisplayed.getWidth();
            float f1 = mBitmapDisplayed.getHeight();
            matrix.reset();
            if(mRotation != 0)
                matrix.postRotate(mRotation, f / 2.0F, f1 / 2.0F);
            float f2 = getFitCenterScale();
            matrix.postScale(f2, f2);
            matrix.postTranslate(((float)getWidth() - f * f2) / 2.0F, ((float)getHeight() - f1 * f2) / 2.0F);
        }

        public float adjustDx(RectF rectf, float f) {
            if(f >= 0.0F || f + rectf.right >= 0.0F) goto _L2; else goto _L1
_L1:
            f = -rectf.right;
_L4:
            return f;
_L2:
            if(f > 0.0F && f + rectf.left > (float)getWidth())
                f = (float)getWidth() - rectf.left;
            if(true) goto _L4; else goto _L3
_L3:
        }

        public float adjustDy(RectF rectf, float f) {
            if(f >= 0.0F || f + rectf.bottom >= 0.0F) goto _L2; else goto _L1
_L1:
            f = -rectf.bottom;
_L4:
            return f;
_L2:
            if(f > 0.0F && f + rectf.top > (float)getHeight())
                f = (float)getHeight() - rectf.top;
            if(true) goto _L4; else goto _L3
_L3:
        }

        public void adjustIfNeeded(RectF rectf, float f, boolean flag) {
        }

        public Matrix getCropedMatrix() {
            float af[] = new float[9];
            mDisplayMatrix.getValues(af);
            convertMatrixReference(af, false);
            Matrix matrix = new Matrix();
            matrix.setValues(af);
            return matrix;
        }

        public int getCropedPhotoHeight() {
            return getHeight();
        }

        public int getCropedPhotoWidth() {
            return getWidth();
        }

        final PhotoFrameView this$0;

        public OutsideBoundStrategy() {
            this$0 = PhotoFrameView.this;
            super();
        }
    }

    public static interface BoundStrategy {

        public abstract void adjustCropArea(Matrix matrix, boolean flag);

        public abstract float adjustDx(RectF rectf, float f);

        public abstract float adjustDy(RectF rectf, float f);

        public abstract void adjustIfNeeded(RectF rectf, float f, boolean flag);

        public abstract Matrix getCropedMatrix();

        public abstract int getCropedPhotoHeight();

        public abstract int getCropedPhotoWidth();
    }

    class JustifyAnimation extends Animation {

        protected void applyTransformation(float f, Transformation transformation) {
            float f1 = (mStartScale + f * mScaleDelta) / getScale();
            mDisplayMatrix.postScale(f1, f1);
            float f2 = mStarts[0] + f * mOffsets[0];
            float f3 = mStarts[1] + f * mOffsets[1];
            float af[] = mTemps;
            af[0] = 0.0F;
            af[1] = 0.0F;
            mDisplayMatrix.mapPoints(af);
            mDisplayMatrix.postTranslate(f2 - af[0], f3 - af[1]);
            invalidate();
        }

        public boolean getResetScale() {
            return mResetScale;
        }

        private final float mOffsets[];
        private final boolean mResetScale;
        private final float mScaleDelta;
        private final float mStartScale;
        private final float mStarts[];
        private final float mTemps[];
        final PhotoFrameView this$0;

        JustifyAnimation(boolean flag) {
            this$0 = PhotoFrameView.this;
            super();
            float af[] = new float[2];
            af[0] = 0.0F;
            af[1] = 0.0F;
            mStarts = af;
            float af1[] = new float[2];
            af1[0] = 0.0F;
            af1[1] = 0.0F;
            mOffsets = af1;
            float af2[] = new float[2];
            af2[0] = 0.0F;
            af2[1] = 0.0F;
            mTemps = af2;
            mResetScale = flag;
            mStartScale = getScale();
            Matrix matrix = new Matrix();
            fitCenter(matrix, flag);
            float f;
            float af3[];
            float af4[];
            if(flag)
                f = matrix.mapRadius(1.0F) - mStartScale;
            else
                f = 0.0F;
            mScaleDelta = f;
            mDisplayMatrix.mapPoints(mStarts);
            matrix.mapPoints(mOffsets);
            af3 = mOffsets;
            af3[0] = af3[0] - mStarts[0];
            af4 = mOffsets;
            af4[1] = af4[1] - mStarts[1];
        }
    }


    public PhotoFrameView(Context context) {
        this(context, null);
    }

    public PhotoFrameView(Context context, AttributeSet attributeset) {
        this(context, attributeset, 0);
    }

    public PhotoFrameView(Context context, AttributeSet attributeset, int i) {
        super(context, attributeset, i);
        mDown = new PointF();
        mLastMove = new PointF();
        mMidPoint = new PointF();
        mMode = 0;
        mOldDist = 20F;
        mLastDown = new PointF();
        mCurrentDownTime = 0L;
        mLastDownTime = -1L;
        mAutoAdjustMinZoom = true;
        mInteractive = true;
        mDisplayMatrix = new Matrix();
        mRotation = 0;
        mPrepareValues = null;
        mMatrixValuesTemp = new float[9];
        mMatrixTemp = new Matrix();
        mBitmapDisplayed = null;
        mFrameBitmap = null;
        mTransformedFilterBitmap = null;
        mFilterBitmap = null;
        mMinZoom = 0.1F;
        mFilterPaint = new Paint();
        mMaskPaint = new Paint();
        mPaintFlags = new PaintFlagsDrawFilter(0, 7);
        mTempBitmap = null;
        TypedArray typedarray = context.obtainStyledAttributes(attributeset, miui.R.styleable.PhotoFrameView, i, 0);
        int j = typedarray.getInt(0, 0);
        if(j == 0)
            setBoundStrategy(new OutsideBoundStrategy());
        else
        if(j == 1)
            setBoundStrategy(new InsideBoundStrategy(typedarray.getDimensionPixelOffset(2, 0)));
        else
            throw new UnsupportedOperationException((new StringBuilder()).append("unsupported type=").append(j).toString());
        typedarray.recycle();
        mFilterPaint.setXfermode(new PorterDuffXfermode(android.graphics.PorterDuff.Mode.DST_IN));
        mMaskPaint.setAlpha(175);
        mMaskPaint.setXfermode(new PorterDuffXfermode(android.graphics.PorterDuff.Mode.DST_OUT));
    }

    private void center(boolean flag, boolean flag1) {
        Bitmap bitmap = mBitmapDisplayed;
        if(bitmap != null) goto _L2; else goto _L1
_L1:
        invalidate();
_L4:
        return;
_L2:
        RectF rectf;
        float f2;
        int i;
        rectf = new RectF(0.0F, 0.0F, bitmap.getWidth(), bitmap.getHeight());
        mDisplayMatrix.mapRect(rectf);
        float f = rectf.height();
        float f1 = rectf.width();
        f2 = 0.0F;
        float f3 = 0.0F;
        if(flag1) {
            int j = getHeight();
            if(f < (float)j)
                f3 = ((float)j - f) / 2.0F - rectf.top;
            else
            if(rectf.top > 0.0F)
                f3 = -rectf.top;
            else
            if(rectf.bottom < (float)j)
                f3 = (float)getHeight() - rectf.bottom;
        }
        if(flag) {
            i = getWidth();
            if(f1 >= (float)i)
                break; /* Loop/switch isn't completed */
            f2 = ((float)i - f1) / 2.0F - rectf.left;
        }
_L5:
        mDisplayMatrix.postTranslate(f2, f3);
        invalidate();
        if(true) goto _L4; else goto _L3
_L3:
        if(rectf.left > 0.0F)
            f2 = -rectf.left;
        else
        if(rectf.right < (float)i)
            f2 = (float)i - rectf.right;
          goto _L5
        if(true) goto _L4; else goto _L6
_L6:
    }

    private boolean convertMatrixReference(float af[], boolean flag) {
        boolean flag1 = true;
        if(getWidth() != 0) goto _L2; else goto _L1
_L1:
        flag1 = false;
_L4:
        return flag1;
_L2:
        if(mFrameBitmap != null) {
            Matrix matrix = new Matrix();
            matrix.setValues(af);
            int i = (getWidth() - mFrameBitmap.getWidth()) / 2;
            int j = (getHeight() - mFrameBitmap.getHeight()) / 2;
            if(!flag) {
                i = -i;
                j = -j;
            }
            matrix.postTranslate(i, j);
            matrix.getValues(af);
        }
        if(true) goto _L4; else goto _L3
_L3:
    }

    public static Bitmap generatePhoto(Bitmap bitmap, Bitmap bitmap1, Bitmap bitmap2, Matrix matrix, int i, int j) throws IOException {
        int k;
        int l;
        Bitmap bitmap3;
        Canvas canvas;
        if(bitmap != null)
            k = bitmap.getHeight();
        else
            k = i;
        if(bitmap != null)
            l = bitmap.getWidth();
        else
            l = j;
        bitmap3 = Bitmap.createBitmap(l, k, android.graphics.Bitmap.Config.ARGB_8888);
        canvas = new Canvas(bitmap3);
        canvas.setDrawFilter(new PaintFlagsDrawFilter(0, 3));
        if(bitmap2 != null)
            if(matrix != null)
                canvas.drawBitmap(bitmap2, matrix, null);
            else
                canvas.drawBitmap(bitmap2, 0.0F, 0.0F, null);
        if(bitmap1 != null) {
            Paint paint = new Paint();
            paint.setXfermode(new PorterDuffXfermode(android.graphics.PorterDuff.Mode.DST_IN));
            canvas.drawBitmap(bitmap1, 0.0F, 0.0F, paint);
        }
        if(bitmap != null)
            canvas.drawBitmap(bitmap, 0.0F, 0.0F, null);
        return bitmap3;
    }

    private boolean isDoubleClick(MotionEvent motionevent) {
        boolean flag;
        if(mLastDownTime > 0L && motionevent.getEventTime() - mLastDownTime < 500L && Math.abs(motionevent.getX() - mLastDown.x) < 20F && Math.abs(motionevent.getY() - mLastDown.y) < 20F)
            flag = true;
        else
            flag = false;
        return flag;
    }

    private boolean isSingleClick(MotionEvent motionevent) {
        boolean flag;
        if(mCurrentDownTime > 0L && motionevent.getEventTime() - mCurrentDownTime < 100L && Math.abs(motionevent.getX() - mDown.x) < 20F && Math.abs(motionevent.getY() - mDown.y) < 20F)
            flag = true;
        else
            flag = false;
        return flag;
    }

    private boolean isVertical() {
        boolean flag;
        if(mRotation % 180 == 0)
            flag = true;
        else
            flag = false;
        return flag;
    }

    private static void midPoint(PointF pointf, MotionEvent motionevent) {
        float f = motionevent.getX(0) + motionevent.getX(1);
        float f1 = motionevent.getY(0) + motionevent.getY(1);
        pointf.set(f / 2.0F, f1 / 2.0F);
    }

    private void panBy(float f, float f1) {
        mDisplayMatrix.postTranslate(f, f1);
        invalidate();
    }

    private void setImageBitmap(Bitmap bitmap, int i) {
        if(bitmap != null)
            setImageBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight(), i);
        else
            setImageBitmap(bitmap, 0, 0, i);
    }

    private void setImageBitmap(Bitmap bitmap, int i, int j, int k) {
        mBitmapDisplayed = bitmap;
        if(mAutoAdjustMinZoom)
            updateMinZoom();
        invalidate();
    }

    private static float spacing(MotionEvent motionevent) {
        float f = motionevent.getX(0) - motionevent.getX(1);
        float f1 = motionevent.getY(0) - motionevent.getY(1);
        return FloatMath.sqrt(f * f + f1 * f1);
    }

    private void updateMinZoom() {
        float f = 1.0F;
        Bitmap bitmap = mBitmapDisplayed;
        if(bitmap != null) {
            int i;
            if(bitmap.getWidth() < bitmap.getHeight())
                i = bitmap.getWidth();
            else
                i = bitmap.getHeight();
            if((float)i > 20F)
                f = 20F / (float)i;
            else
                f = 1.0F;
        }
        mMinZoom = f;
    }

    private void updateTempBitmap() {
        int i = getHeight();
        int j = getWidth();
        if(mTempBitmap == null || mTempBitmap.getHeight() != i || mTempBitmap.getWidth() != j)
            mTempBitmap = Bitmap.createBitmap(j, i, android.graphics.Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(mTempBitmap);
        canvas.setDrawFilter(mPaintFlags);
        canvas.drawColor(0, android.graphics.PorterDuff.Mode.CLEAR);
        Bitmap bitmap = mBitmapDisplayed;
        if(bitmap != null) {
            canvas.save();
            if(mDisplayMatrix != null)
                canvas.concat(mDisplayMatrix);
            canvas.drawBitmap(bitmap, 0.0F, 0.0F, null);
            canvas.restore();
        }
        int k = getWidth();
        int l = getHeight();
        int i1 = 0;
        int j1 = 0;
        int k1 = 0;
        int l1 = 0;
        if(mTransformedFilterBitmap != null) {
            i1 = (k - mTransformedFilterBitmap.getWidth()) / 2;
            j1 = (l - mTransformedFilterBitmap.getHeight()) / 2;
            canvas.drawBitmap(mTransformedFilterBitmap, i1, j1, mFilterPaint);
            k1 = mTransformedFilterBitmap.getHeight();
            l1 = mTransformedFilterBitmap.getWidth();
        }
        if(mFrameBitmap != null) {
            i1 = (k - mFrameBitmap.getWidth()) / 2;
            j1 = (l - mFrameBitmap.getHeight()) / 2;
            canvas.drawBitmap(mFrameBitmap, i1, j1, null);
            if(k1 <= 0) {
                k1 = mFrameBitmap.getHeight();
                l1 = mFrameBitmap.getWidth();
            }
        }
        if(i1 > 0 && j1 > 0 && i1 + l1 < k && j1 + k1 < l) {
            canvas.clipRect(i1, j1, i1 + l1, j1 + k1, android.graphics.Region.Op.DIFFERENCE);
            canvas.drawRect(0.0F, 0.0F, k, l, mMaskPaint);
        }
    }

    private void zoomTo(float f, float f1, float f2) {
        float f3 = f / getScale();
        mDisplayMatrix.postScale(f3, f3, f1, f2);
        center(false, false);
    }

    void fitCenter(Matrix matrix) {
        fitCenter(matrix, true);
    }

    void fitCenter(Matrix matrix, boolean flag) {
        if(mBitmapDisplayed != null && mBoundStrategy != null)
            mBoundStrategy.adjustCropArea(matrix, flag);
    }

    public Bitmap generateCropedPhoto() throws IOException {
        Bitmap bitmap;
        if(mBoundStrategy != null)
            bitmap = generatePhoto(null, null, mBitmapDisplayed, mBoundStrategy.getCropedMatrix(), mBoundStrategy.getCropedPhotoHeight(), mBoundStrategy.getCropedPhotoWidth());
        else
            bitmap = generatePhoto();
        return bitmap;
    }

    public Bitmap generatePhoto() throws IOException {
        mDisplayMatrix.getValues(mMatrixValuesTemp);
        convertMatrixReference(mMatrixValuesTemp, false);
        mMatrixTemp.setValues(mMatrixValuesTemp);
        return generatePhoto(mFrameBitmap, mFilterBitmap, mBitmapDisplayed, mMatrixTemp, getHeight(), getWidth());
    }

    public float getFitCenterScale() {
        int i = getWidth();
        int j = getHeight();
        float f;
        float f1;
        float f2;
        float f3;
        float f4;
        if(mFrameBitmap != null) {
            f = mFrameBitmap.getWidth();
            f1 = mFrameBitmap.getHeight();
        } else {
            f = i;
            f1 = j;
        }
        f2 = mBitmapDisplayed.getWidth();
        f3 = mBitmapDisplayed.getHeight();
        if(isVertical())
            f4 = Math.min(f / f2, f1 / f3);
        else
            f4 = Math.min(f / f3, f1 / f2);
        return f4;
    }

    public Bitmap getFrameBitmap() {
        return mFrameBitmap;
    }

    public Bitmap getImageBitmap() {
        return mBitmapDisplayed;
    }

    public RectF getImageBounds() {
        Bitmap bitmap = mBitmapDisplayed;
        if(bitmap != null) goto _L2; else goto _L1
_L1:
        RectF rectf = null;
_L4:
        return rectf;
_L2:
        rectf = new RectF(0.0F, 0.0F, bitmap.getWidth(), bitmap.getHeight());
        mDisplayMatrix.mapRect(rectf);
        if(mFrameBitmap != null) {
            int i = (getWidth() - mFrameBitmap.getWidth()) / 2;
            rectf.left = rectf.left + (float)i;
            rectf.right = rectf.right - (float)i;
            int j = (getHeight() - mFrameBitmap.getHeight()) / 2;
            rectf.top = rectf.top + (float)j;
            rectf.bottom = rectf.bottom - (float)j;
        }
        if(true) goto _L4; else goto _L3
_L3:
    }

    public void getMatrixValues(float af[]) {
        mDisplayMatrix.getValues(af);
        convertMatrixReference(af, false);
    }

    public int getRotateDegrees() {
        return mRotation;
    }

    float getScale() {
        return mDisplayMatrix.mapRadius(1.0F);
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.setDrawFilter(mPaintFlags);
        updateTempBitmap();
        canvas.drawBitmap(mTempBitmap, 0.0F, 0.0F, null);
    }

    protected void onLayout(boolean flag, int i, int j, int k, int l) {
        super.onLayout(flag, i, j, k, l);
        if(flag)
            if(mPrepareValues == null) {
                fitCenter(mMatrixTemp);
                mDisplayMatrix.set(mMatrixTemp);
            } else {
                System.arraycopy(mPrepareValues, 0, mMatrixValuesTemp, 0, 9);
                convertMatrixReference(mMatrixValuesTemp, true);
                mDisplayMatrix.setValues(mMatrixValuesTemp);
            }
    }

    protected void onSizeChanged(int i, int j, int k, int l) {
        super.onSizeChanged(i, j, k, l);
        if(mSizeChangedListener != null)
            mSizeChangedListener.onSizeChanged(this);
    }

    public boolean onTouchEvent(MotionEvent motionevent) {
        boolean flag = false;
        0xff & motionevent.getAction();
        JVM INSTR tableswitch 0 6: default 52
    //                   0 56
    //                   1 179
    //                   2 267
    //                   3 52
    //                   4 52
    //                   5 138
    //                   6 259;
           goto _L1 _L2 _L3 _L4 _L1 _L1 _L5 _L6
_L1:
        flag = true;
_L8:
        return flag;
_L2:
        if(mInteractive) {
            mLastDown.set(mDown.x, mDown.y);
            mLastDownTime = mCurrentDownTime;
            mCurrentDownTime = motionevent.getEventTime();
            mDown.set(motionevent.getX(), motionevent.getY());
            mLastMove.set(motionevent.getX(), motionevent.getY());
            mMode = 1;
            continue; /* Loop/switch isn't completed */
        }
        continue; /* Loop/switch isn't completed */
_L5:
        if(mInteractive) {
            mOldDist = spacing(motionevent);
            if(mOldDist > 20F) {
                midPoint(mMidPoint, motionevent);
                mMode = 2;
            }
            continue; /* Loop/switch isn't completed */
        }
        continue; /* Loop/switch isn't completed */
_L3:
        mMode = 0;
        if(isSingleClick(motionevent)) {
            if(isDoubleClick(motionevent))
                resetMatrix();
        } else
        if(mBoundStrategy != null) {
            RectF rectf1 = getImageBounds();
            float f5 = getScale();
            BoundStrategy boundstrategy = mBoundStrategy;
            if(f5 < getFitCenterScale())
                flag = true;
            boundstrategy.adjustIfNeeded(rectf1, f5, flag);
        }
        continue; /* Loop/switch isn't completed */
_L6:
        mMode = 0;
        continue; /* Loop/switch isn't completed */
_L4:
        if(!mInteractive)
            continue; /* Loop/switch isn't completed */
        if(mMode == 1 && motionevent.getEventTime() - mCurrentDownTime > 100L) {
            float f3 = motionevent.getX() - mLastMove.x;
            float f4 = motionevent.getY() - mLastMove.y;
            mLastMove.set(motionevent.getX(), motionevent.getY());
            RectF rectf = getImageBounds();
            if(rectf != null) {
                if(mBoundStrategy != null) {
                    f3 = mBoundStrategy.adjustDx(rectf, f3);
                    f4 = mBoundStrategy.adjustDy(rectf, f4);
                }
                panBy(f3, f4);
            }
            continue; /* Loop/switch isn't completed */
        }
        if(mMode != 2)
            continue; /* Loop/switch isn't completed */
        if(motionevent.getX() <= (float)getRight() && motionevent.getX() >= (float)getLeft())
            break; /* Loop/switch isn't completed */
        flag = true;
        if(true) goto _L8; else goto _L7
_L7:
        float f = spacing(motionevent);
        if(f > 20F) {
            float f1 = f / mOldDist;
            mOldDist = f;
            float f2 = f1 * getScale();
            if(f2 < mMinZoom)
                f2 = mMinZoom;
            zoomTo(f2, mMidPoint.x, mMidPoint.y);
        }
        if(true) goto _L1; else goto _L9
_L9:
    }

    public boolean photoCropedFromLarge() {
        float f = getScale();
        float f1 = Math.min(mBitmapDisplayed.getWidth(), mBitmapDisplayed.getHeight());
        boolean flag;
        if(f1 > 400F && f1 * f > 400F)
            flag = true;
        else
            flag = false;
        return flag;
    }

    public void recyleAllBitmap() {
        if(mTransformedFilterBitmap != null) {
            mTransformedFilterBitmap.recycle();
            mTransformedFilterBitmap = null;
        }
        if(mFilterBitmap != null) {
            mFilterBitmap.recycle();
            mFilterBitmap = null;
        }
        if(mFrameBitmap != null) {
            mFrameBitmap.recycle();
            mFrameBitmap = null;
        }
        if(mBitmapDisplayed != null) {
            mBitmapDisplayed.recycle();
            mBitmapDisplayed = null;
        }
    }

    public void resetMatrix() {
        resetMatrix(true);
    }

    public void resetMatrix(boolean flag) {
        if(mBitmapDisplayed != null) {
            fitCenter(mMatrixTemp, flag);
            mDisplayMatrix.set(mMatrixTemp);
            invalidate();
        }
    }

    public void setAutoAdjustMinZoom(boolean flag) {
        mAutoAdjustMinZoom = flag;
    }

    public void setBoundStrategy(BoundStrategy boundstrategy) {
        mBoundStrategy = boundstrategy;
    }

    public void setFilterBitmap(Bitmap bitmap) {
        mFilterBitmap = bitmap;
        if(bitmap == null) {
            mTransformedFilterBitmap = null;
        } else {
            int i = bitmap.getWidth();
            int j = bitmap.getHeight();
            int ai[] = new int[i * j];
            bitmap.getPixels(ai, 0, i, 0, 0, i, j);
            for(int k = 0; k < ai.length; k++)
                if((0xff000000 & ai[k]) == 0)
                    ai[k] = 0x50000000;

            mTransformedFilterBitmap = Bitmap.createBitmap(i, j, android.graphics.Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(mTransformedFilterBitmap);
            canvas.setDrawFilter(mPaintFlags);
            canvas.drawBitmap(ai, 0, i, 0, 0, i, j, true, null);
            invalidate();
        }
    }

    public void setFrameBitmap(Bitmap bitmap) {
        mFrameBitmap = bitmap;
        invalidate();
    }

    public void setImageBitmap(Bitmap bitmap) {
        setImageBitmap(bitmap, 0);
    }

    public void setInteractive(boolean flag) {
        mInteractive = flag;
    }

    public void setMatrixValues(float af[]) {
        if(getWidth() == 0) {
            float af1[] = new float[9];
            System.arraycopy(af, 0, af1, 0, 9);
            mPrepareValues = af1;
        } else {
            mPrepareValues = null;
            System.arraycopy(af, 0, mMatrixValuesTemp, 0, 9);
            convertMatrixReference(mMatrixValuesTemp, true);
            mDisplayMatrix.setValues(mMatrixValuesTemp);
            invalidate();
        }
    }

    public void setMinZoom(float f) {
        mMinZoom = f;
    }

    public void setOnSizeChangedListener(OnSizeChangedListener onsizechangedlistener) {
        mSizeChangedListener = onsizechangedlistener;
    }

    public void setRotateDegrees(int i, boolean flag) {
        mRotation = i % 360;
        if(flag) {
            fitCenter(mMatrixTemp);
            mDisplayMatrix.set(mMatrixTemp);
            invalidate();
        }
    }

    private static final int ALPHA_DEGREE = 80;
    private static final int CONTACT_LARGE_PHOTO_MIN_LENTH = 400;
    private static final int CONTACT_THUMBNAIL_BIT = 96;
    private static final float DISTANCE_OF_FINGERS = 20F;
    private static final long DOUBLE_CLICK_INTERVAL = 500L;
    private static final int DRAG = 1;
    public static final int INSIDE_BOUND_TYPE = 1;
    public static final int MATRIX_VALUES_SIZE = 9;
    private static final float MIN_BITMAP_SIZE = 20F;
    private static final int NONE = 0;
    public static final int OUTSIDE_BOUND_TYPE = 0;
    private static final long SINGLE_CLICK_INTERVAL = 100L;
    private static final int ZOOM = 2;
    private boolean mAutoAdjustMinZoom;
    private Bitmap mBitmapDisplayed;
    private BoundStrategy mBoundStrategy;
    private long mCurrentDownTime;
    final Matrix mDisplayMatrix;
    private final PointF mDown;
    private Bitmap mFilterBitmap;
    private Paint mFilterPaint;
    private Bitmap mFrameBitmap;
    private boolean mInteractive;
    private final PointF mLastDown;
    private long mLastDownTime;
    private final PointF mLastMove;
    private Paint mMaskPaint;
    private final Matrix mMatrixTemp;
    private final float mMatrixValuesTemp[];
    private final PointF mMidPoint;
    private float mMinZoom;
    private int mMode;
    private float mOldDist;
    private PaintFlagsDrawFilter mPaintFlags;
    private float mPrepareValues[];
    private int mRotation;
    private OnSizeChangedListener mSizeChangedListener;
    private Bitmap mTempBitmap;
    private Bitmap mTransformedFilterBitmap;




}
