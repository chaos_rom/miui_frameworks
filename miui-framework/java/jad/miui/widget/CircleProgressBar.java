// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.widget;

import android.animation.*;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.*;
import android.graphics.drawable.*;
import android.util.AttributeSet;
import android.view.animation.LinearInterpolator;
import android.widget.ProgressBar;

public class CircleProgressBar extends ProgressBar {
    public static interface OnProgressChangedListener {

        public abstract void onProgressChanged();
    }


    public CircleProgressBar(Context context) {
        this(context, null);
    }

    public CircleProgressBar(Context context, AttributeSet attributeset) {
        this(context, attributeset, 0);
    }

    public CircleProgressBar(Context context, AttributeSet attributeset, int i) {
        super(context, attributeset, i);
        mRotateVelocity = 300;
        mPaint = new Paint();
        mPaint.setColor(0xff000000);
        if(System.currentTimeMillis() == 0L)
            setPrevAlpha(getPrevAlpha());
        setIndeterminate(false);
    }

    private int calcDuration(int i) {
        return (i * 1000) / mRotateVelocity;
    }

    private void drawLayer(Canvas canvas, Drawable drawable, Drawable drawable1, Drawable drawable2, float f, int i) {
        if(drawable != null) {
            drawable.setAlpha(i);
            drawable.draw(canvas);
        }
        if(canvas.isHardwareAccelerated()) {
            canvas.saveLayer(drawable2.getBounds().left, drawable2.getBounds().top, drawable2.getBounds().right, drawable2.getBounds().bottom, null, 16);
            canvas.drawArc(mArcRect, -90F, 360F * f, true, mPaint);
            drawable2.setAlpha(i);
            drawable2.draw(canvas);
            canvas.restore();
        } else {
            if(mBitmapForSoftLayer == null) {
                mBitmapForSoftLayer = Bitmap.createBitmap(drawable2.getBounds().width(), drawable2.getBounds().height(), android.graphics.Bitmap.Config.ARGB_8888);
                mCanvasForSoftLayer = new Canvas(mBitmapForSoftLayer);
            }
            mBitmapForSoftLayer.eraseColor(0);
            mCanvasForSoftLayer.save();
            mCanvasForSoftLayer.translate(-drawable2.getBounds().left, -drawable2.getBounds().top);
            mCanvasForSoftLayer.drawArc(mArcRect, -90F, 360F * f, true, mPaint);
            drawable2.setAlpha(i);
            drawable2.draw(mCanvasForSoftLayer);
            mCanvasForSoftLayer.restore();
            canvas.drawBitmap(mBitmapForSoftLayer, drawable2.getBounds().left, drawable2.getBounds().top, null);
        }
        if(drawable1 != null) {
            drawable1.setAlpha(i);
            drawable1.draw(canvas);
        }
    }

    private Drawable getBackDrawable(int i) {
        Drawable drawable;
        if(mLevelsBackDrawable == null)
            drawable = null;
        else
            drawable = mLevelsBackDrawable[i];
        return drawable;
    }

    private Drawable[] getDrawables(int ai[]) {
        Drawable adrawable[];
        if(ai == null) {
            adrawable = null;
        } else {
            Resources resources = getContext().getResources();
            adrawable = new Drawable[ai.length];
            int i = 0;
            while(i < ai.length)  {
                adrawable[i] = resources.getDrawable(ai[i]);
                adrawable[i].setBounds(0, 0, adrawable[i].getIntrinsicWidth(), adrawable[i].getIntrinsicHeight());
                i++;
            }
        }
        return adrawable;
    }

    private Drawable getForeDrawable(int i) {
        Drawable drawable;
        if(mLevelsForeDrawable == null)
            drawable = null;
        else
            drawable = mLevelsForeDrawable[i];
        return drawable;
    }

    private int getIntrinsicHeight() {
        int i = getMiddleDrawable(0).getIntrinsicHeight();
        if(mLevelsForeDrawable != null)
            i = Math.max(i, mLevelsForeDrawable[0].getIntrinsicHeight());
        if(mLevelsBackDrawable != null)
            i = Math.max(i, mLevelsBackDrawable[0].getIntrinsicHeight());
        return i;
    }

    private int getIntrinsicWidth() {
        int i = getMiddleDrawable(0).getIntrinsicWidth();
        if(mLevelsForeDrawable != null)
            i = Math.max(i, mLevelsForeDrawable[0].getIntrinsicWidth());
        if(mLevelsBackDrawable != null)
            i = Math.max(i, mLevelsBackDrawable[0].getIntrinsicWidth());
        return i;
    }

    private Drawable getMiddleDrawable(int i) {
        Drawable drawable;
        if(mLevelsMiddleDrawable == null)
            drawable = null;
        else
            drawable = mLevelsMiddleDrawable[i];
        return drawable;
    }

    private float getRate() {
        return (float)getProgress() / (float)getMax();
    }

    protected void drawableStateChanged() {
        super.drawableStateChanged();
        int i = getProgressLevelCount();
        for(int j = 0; j < i; j++) {
            if(mLevelsBackDrawable != null)
                mLevelsBackDrawable[j].setState(getDrawableState());
            if(mLevelsMiddleDrawable != null)
                mLevelsMiddleDrawable[j].setState(getDrawableState());
            if(mLevelsForeDrawable != null)
                mLevelsForeDrawable[j].setState(getDrawableState());
        }

        invalidate();
    }

    public int getPrevAlpha() {
        return mPrevAlpha;
    }

    public int getProgressLevelCount() {
        int i;
        if(mProgressLevels == null)
            i = 1;
        else
            i = 1 + mProgressLevels.length;
        return i;
    }

    /**
     * @deprecated Method onDraw is deprecated
     */

    protected void onDraw(Canvas canvas) {
        this;
        JVM INSTR monitorenter ;
        drawLayer(canvas, getBackDrawable(mCurrentLevel), getForeDrawable(mCurrentLevel), getMiddleDrawable(mCurrentLevel), getRate(), 255 - mPrevAlpha);
        if(mPrevAlpha >= 10)
            drawLayer(canvas, getBackDrawable(mPrevLevel), getForeDrawable(mPrevLevel), getMiddleDrawable(mPrevLevel), getRate(), mPrevAlpha);
        this;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        throw exception;
    }

    /**
     * @deprecated Method onMeasure is deprecated
     */

    protected void onMeasure(int i, int j) {
        this;
        JVM INSTR monitorenter ;
        setMeasuredDimension(getIntrinsicWidth(), getIntrinsicHeight());
        this;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        throw exception;
    }

    public void setDrawablesForLevels(int ai[], int ai1[], int ai2[]) {
        setDrawablesForLevels(getDrawables(ai), getDrawables(ai1), getDrawables(ai2));
    }

    public void setDrawablesForLevels(Drawable adrawable[], Drawable adrawable1[], Drawable adrawable2[]) {
        mLevelsBackDrawable = adrawable;
        mLevelsMiddleDrawable = adrawable1;
        mLevelsForeDrawable = adrawable2;
        if(adrawable != null) {
            int k1 = adrawable.length;
            for(int l1 = 0; l1 < k1; l1++)
                adrawable[l1].mutate();

        }
        if(adrawable1 != null) {
            int i1 = adrawable1.length;
            for(int j1 = 0; j1 < i1; j1++)
                adrawable1[j1].mutate();

        }
        if(adrawable2 != null) {
            int k = adrawable2.length;
            for(int l = 0; l < k; l++)
                adrawable2[l].mutate();

        }
        int i = adrawable1.length;
        int j = 0;
        while(j < i)  {
            Drawable drawable = adrawable1[j];
            if(drawable instanceof BitmapDrawable)
                ((BitmapDrawable)drawable).getPaint().setXfermode(new PorterDuffXfermode(android.graphics.PorterDuff.Mode.SRC_IN));
            else
            if(drawable instanceof NinePatchDrawable)
                ((NinePatchDrawable)drawable).getPaint().setXfermode(new PorterDuffXfermode(android.graphics.PorterDuff.Mode.SRC_IN));
            else
                throw new IllegalArgumentException("'middles' must a bitmap or nine patch drawable.");
            j++;
        }
        mArcRect = new RectF(-5 + adrawable1[0].getBounds().left, -5 + adrawable1[0].getBounds().top, 5 + adrawable1[0].getBounds().right, 5 + adrawable1[0].getBounds().bottom);
    }

    public void setOnProgressChangedListener(OnProgressChangedListener onprogresschangedlistener) {
        mProgressChangedListener = onprogresschangedlistener;
    }

    public void setPrevAlpha(int i) {
        mPrevAlpha = i;
        invalidate();
    }

    /**
     * @deprecated Method setProgress is deprecated
     */

    public void setProgress(int i) {
        this;
        JVM INSTR monitorenter ;
        int j;
        super.setProgress(i);
        j = -1;
        if(mProgressLevels != null) goto _L2; else goto _L1
_L1:
        j = 0;
_L5:
        if(j != mCurrentLevel) {
            mPrevLevel = mCurrentLevel;
            mCurrentLevel = j;
            setPrevAlpha(255);
            int ai[] = new int[1];
            ai[0] = 0;
            ObjectAnimator objectanimator = ObjectAnimator.ofInt(this, "prevAlpha", ai);
            objectanimator.setDuration(300L);
            objectanimator.setInterpolator(new LinearInterpolator());
            objectanimator.start();
        }
        if(mProgressChangedListener != null)
            mProgressChangedListener.onProgressChanged();
        this;
        JVM INSTR monitorexit ;
        return;
_L2:
        int k;
        int l;
        k = mProgressLevels.length;
        l = 0;
_L6:
        if(l >= k) goto _L4; else goto _L3
_L3:
        int i1 = mProgressLevels[l];
        if(i >= i1)
            break MISSING_BLOCK_LABEL_160;
        j = l;
_L4:
        if(j == -1)
            j = k;
          goto _L5
        l++;
          goto _L6
        Exception exception;
        exception;
        throw exception;
          goto _L5
    }

    public void setProgressByAnimator(int i) {
        setProgressByAnimator(i, null);
    }

    public void setProgressByAnimator(int i, android.animation.Animator.AnimatorListener animatorlistener) {
        stopProgressAnimator();
        int j = Math.abs((int)(360F * ((float)(i - getProgress()) / (float)getMax())));
        int ai[] = new int[1];
        ai[0] = i;
        mChangeProgressAnimator = ObjectAnimator.ofInt(this, "progress", ai);
        mChangeProgressAnimator.setDuration(calcDuration(j));
        mChangeProgressAnimator.setInterpolator(getInterpolator());
        if(animatorlistener != null)
            mChangeProgressAnimator.addListener(animatorlistener);
        mChangeProgressAnimator.start();
    }

    public void setProgressLevels(int ai[]) {
        mProgressLevels = ai;
    }

    public void setRotateVelocity(int i) {
        mRotateVelocity = i;
    }

    public void stopProgressAnimator() {
        if(mChangeProgressAnimator != null && mChangeProgressAnimator.isRunning())
            mChangeProgressAnimator.cancel();
    }

    private static final int ALPHA_NEED_DRAW_MIN_VALUE = 10;
    private static final int DEFAULT_FADE_OUT_DURATION = 300;
    private static final int DEFAULT_ROTATE_VELOCITY = 300;
    private RectF mArcRect;
    private Bitmap mBitmapForSoftLayer;
    private Canvas mCanvasForSoftLayer;
    private Animator mChangeProgressAnimator;
    private int mCurrentLevel;
    private Drawable mLevelsBackDrawable[];
    private Drawable mLevelsForeDrawable[];
    private Drawable mLevelsMiddleDrawable[];
    private Paint mPaint;
    private int mPrevAlpha;
    private int mPrevLevel;
    private OnProgressChangedListener mProgressChangedListener;
    private int mProgressLevels[];
    private int mRotateVelocity;
}
