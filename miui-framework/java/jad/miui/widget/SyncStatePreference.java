// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.widget;

import android.accounts.Account;
import android.content.Context;
import android.preference.CheckBoxPreference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Checkable;
import android.widget.TextView;

// Referenced classes of package miui.widget:
//            AnimatedImageView

public class SyncStatePreference extends CheckBoxPreference {

    public SyncStatePreference(Context context, Account account, String s) {
        super(context, null);
        mIsActive = false;
        mIsPending = false;
        mFailed = false;
        mNoSim = false;
        mActivating = false;
        mOneTimeSyncMode = false;
        mAccount = account;
        mAuthority = s;
        setLayoutResource(0x603003d);
        setWidgetLayoutResource(0x603003c);
    }

    public SyncStatePreference(Context context, AttributeSet attributeset) {
        super(context, attributeset);
        mIsActive = false;
        mIsPending = false;
        mFailed = false;
        mNoSim = false;
        mActivating = false;
        mOneTimeSyncMode = false;
        setLayoutResource(0x603003d);
        setWidgetLayoutResource(0x603003c);
        mAccount = null;
        mAuthority = null;
    }

    public Account getAccount() {
        return mAccount;
    }

    public String getAuthority() {
        return mAuthority;
    }

    public boolean isOneTimeSyncMode() {
        return mOneTimeSyncMode;
    }

    public void onBindView(View view) {
        TextView textview;
        super.onBindView(view);
        AnimatedImageView animatedimageview = (AnimatedImageView)view.findViewById(0x60b0081);
        View view1 = view.findViewById(0x60b0082);
        boolean flag;
        int i;
        boolean flag1;
        int j;
        View view2;
        View view3;
        if(mIsActive || mIsPending)
            flag = true;
        else
            flag = false;
        if(flag)
            i = 0;
        else
            i = 8;
        animatedimageview.setVisibility(i);
        animatedimageview.setAnimating(mIsActive);
        if(mFailed && !flag)
            flag1 = true;
        else
            flag1 = false;
        if(flag1)
            j = 0;
        else
            j = 8;
        view1.setVisibility(j);
        view2 = view.findViewById(0x1020001);
        if(mOneTimeSyncMode) {
            view2.setVisibility(8);
            TextView textview1 = (TextView)view.findViewById(0x1020010);
            Context context = getContext();
            Object aobj[] = new Object[1];
            aobj[0] = getSummary();
            textview1.setText(context.getString(0x60c01db, aobj));
        } else {
            view2.setVisibility(0);
        }
        if(!mNoSim && !mActivating) goto _L2; else goto _L1
_L1:
        textview = (TextView)view.findViewById(0x1020010);
        if(!mNoSim) goto _L4; else goto _L3
_L3:
        textview.setText(getContext().getString(0x60c01dc));
_L2:
        view3 = view.findViewById(0x1020001);
        if(view3 != null && (view3 instanceof Checkable))
            ((Checkable)view3).setChecked(isChecked());
        return;
_L4:
        if(mActivating)
            textview.setText(getContext().getString(0x60c01dd));
        if(true) goto _L2; else goto _L5
_L5:
    }

    protected void onClick() {
        if(!mOneTimeSyncMode)
            super.onClick();
    }

    public void setAccount(Account account) {
        mAccount = account;
    }

    public void setActivating(boolean flag) {
        mActivating = flag;
    }

    public void setActive(boolean flag) {
        mIsActive = flag;
        notifyChanged();
    }

    public void setAuthority(String s) {
        mAuthority = s;
    }

    public void setFailed(boolean flag) {
        mFailed = flag;
        notifyChanged();
    }

    public void setNoSim(boolean flag) {
        mNoSim = flag;
    }

    public void setOneTimeSyncMode(boolean flag) {
        mOneTimeSyncMode = flag;
        notifyChanged();
    }

    public void setPending(boolean flag) {
        mIsPending = flag;
        notifyChanged();
    }

    private static final String TAG = "SyncStatePreference";
    private Account mAccount;
    private boolean mActivating;
    private String mAuthority;
    private boolean mFailed;
    private boolean mIsActive;
    private boolean mIsPending;
    private boolean mNoSim;
    private boolean mOneTimeSyncMode;
}
